# include  "include.h"
# include  "secv6mid.h"
# include  "secv6low.h"
# include  "secv6con.h"
# include  "secv6ogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "fssecvcli.h"

/****************************************************************************
 Function   : fsipv6SecScalarsGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsipv6SecScalarsGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_fsipv6SecScalars_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_fsipv6SecScalars_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_fsipv6SecScalars_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_fsipv6SecScalars_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_fsipv6SecScalars_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case FSIPV6SECGLOBALSTATUS:
        {
            i1_ret_val = nmhGetFsipv6SecGlobalStatus (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECVERSION:
        {
            i1_ret_val = nmhGetFsipv6SecVersion (&u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECGLOBALDEBUG:
        {
            i1_ret_val = nmhGetFsipv6SecGlobalDebug (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECMAXSA:
        {
            i1_ret_val = nmhGetFsipv6SecMaxSA (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsipv6SecScalarsSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsipv6SecScalarsSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSIPV6SECGLOBALSTATUS:
        {
            i1_ret_val = nmhSetFsipv6SecGlobalStatus (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECGLOBALDEBUG:
        {
            i1_ret_val = nmhSetFsipv6SecGlobalDebug (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECMAXSA:
        {
            i1_ret_val = nmhSetFsipv6SecMaxSA (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSIPV6SECVERSION:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsipv6SecScalarsTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsipv6SecScalarsTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    switch (u1_arg)
    {

        case FSIPV6SECGLOBALSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecGlobalStatus (&u4ErrorCode,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECGLOBALDEBUG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecGlobalDebug (&u4ErrorCode,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECMAXSA:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecMaxSA (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSIPV6SECVERSION:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsIpv6SecSelectorEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsIpv6SecSelectorEntryGet (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsipv6SecSelectorTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsipv6SelIfIndex = FALSE;
    INT4                i4_next_fsipv6SelIfIndex = FALSE;

    INT4                i4_fsipv6SelProtoIndex = FALSE;
    INT4                i4_next_fsipv6SelProtoIndex = FALSE;

    INT4                i4_fsipv6SelAccessIndex = FALSE;
    INT4                i4_next_fsipv6SelAccessIndex = FALSE;

    INT4                i4_fsipv6SelPort = FALSE;
    INT4                i4_next_fsipv6SelPort = FALSE;

    INT4                i4_fsipv6SelPktDirection = FALSE;
    INT4                i4_next_fsipv6SelPktDirection = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SelIfIpAddress = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsipv6SecSelectorTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN + INTEGER_LEN + INTEGER_LEN +
                INTEGER_LEN + INTEGER_LEN;

            if (LEN_fsipv6SecSelectorTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsipv6SelIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_fsipv6SelProtoIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_fsipv6SelAccessIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_fsipv6SelPort =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_fsipv6SelPktDirection =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsipv6SecSelectorTable
                     (i4_fsipv6SelIfIndex, i4_fsipv6SelProtoIndex,
                      i4_fsipv6SelAccessIndex, i4_fsipv6SelPort,
                      i4_fsipv6SelPktDirection)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SelIfIndex;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SelProtoIndex;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SelAccessIndex;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SelPort;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SelPktDirection;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsipv6SecSelectorTable
                     (&i4_fsipv6SelIfIndex, &i4_fsipv6SelProtoIndex,
                      &i4_fsipv6SelAccessIndex, &i4_fsipv6SelPort,
                      &i4_fsipv6SelPktDirection)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SelIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SelProtoIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SelAccessIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SelPort;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SelPktDirection;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SelIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SelProtoIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SelAccessIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SelPort =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SelPktDirection =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsipv6SecSelectorTable (i4_fsipv6SelIfIndex,
                                                            &i4_next_fsipv6SelIfIndex,
                                                            i4_fsipv6SelProtoIndex,
                                                            &i4_next_fsipv6SelProtoIndex,
                                                            i4_fsipv6SelAccessIndex,
                                                            &i4_next_fsipv6SelAccessIndex,
                                                            i4_fsipv6SelPort,
                                                            &i4_next_fsipv6SelPort,
                                                            i4_fsipv6SelPktDirection,
                                                            &i4_next_fsipv6SelPktDirection))
                    == SNMP_SUCCESS)
                {
                    i4_fsipv6SelIfIndex = i4_next_fsipv6SelIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SelIfIndex;
                    i4_fsipv6SelProtoIndex = i4_next_fsipv6SelProtoIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SelProtoIndex;
                    i4_fsipv6SelAccessIndex = i4_next_fsipv6SelAccessIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SelAccessIndex;
                    i4_fsipv6SelPort = i4_next_fsipv6SelPort;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SelPort;
                    i4_fsipv6SelPktDirection = i4_next_fsipv6SelPktDirection;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SelPktDirection;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIPV6SELIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SelIfIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SelIfIndex;
            }
            break;
        }
        case FSIPV6SELPROTOINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SelProtoIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SelProtoIndex;
            }
            break;
        }
        case FSIPV6SELACCESSINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SelAccessIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SelAccessIndex;
            }
            break;
        }
        case FSIPV6SELPORT:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SelPort;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SelPort;
            }
            break;
        }
        case FSIPV6SELPKTDIRECTION:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SelPktDirection;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SelPktDirection;
            }
            break;
        }
        case FSIPV6SELFILTERFLAG:
        {
            i1_ret_val =
                nmhGetFsipv6SelFilterFlag (i4_fsipv6SelIfIndex,
                                           i4_fsipv6SelProtoIndex,
                                           i4_fsipv6SelAccessIndex,
                                           i4_fsipv6SelPort,
                                           i4_fsipv6SelPktDirection,
                                           &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SELPOLICYINDEX:
        {
            i1_ret_val =
                nmhGetFsipv6SelPolicyIndex (i4_fsipv6SelIfIndex,
                                            i4_fsipv6SelProtoIndex,
                                            i4_fsipv6SelAccessIndex,
                                            i4_fsipv6SelPort,
                                            i4_fsipv6SelPktDirection,
                                            &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SELIFIPADDRESS:
        {
            poctet_retval_fsipv6SelIfIpAddress =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_fsipv6SelIfIpAddress == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SelIfIpAddress (i4_fsipv6SelIfIndex,
                                            i4_fsipv6SelProtoIndex,
                                            i4_fsipv6SelAccessIndex,
                                            i4_fsipv6SelPort,
                                            i4_fsipv6SelPktDirection,
                                            poctet_retval_fsipv6SelIfIpAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SelIfIpAddress;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SelIfIpAddress);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SELSTATUS:
        {
            i1_ret_val =
                nmhGetFsipv6SelStatus (i4_fsipv6SelIfIndex,
                                       i4_fsipv6SelProtoIndex,
                                       i4_fsipv6SelAccessIndex,
                                       i4_fsipv6SelPort,
                                       i4_fsipv6SelPktDirection,
                                       &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsIpv6SecSelectorEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsIpv6SecSelectorEntrySet (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsipv6SelIfIndex = FALSE;

    INT4                i4_fsipv6SelProtoIndex = FALSE;

    INT4                i4_fsipv6SelAccessIndex = FALSE;

    INT4                i4_fsipv6SelPort = FALSE;

    INT4                i4_fsipv6SelPktDirection = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsipv6SelIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsipv6SelProtoIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsipv6SelAccessIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsipv6SelPort =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsipv6SelPktDirection =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSIPV6SELFILTERFLAG:
        {
            i1_ret_val =
                nmhSetFsipv6SelFilterFlag (i4_fsipv6SelIfIndex,
                                           i4_fsipv6SelProtoIndex,
                                           i4_fsipv6SelAccessIndex,
                                           i4_fsipv6SelPort,
                                           i4_fsipv6SelPktDirection,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SELPOLICYINDEX:
        {
            i1_ret_val =
                nmhSetFsipv6SelPolicyIndex (i4_fsipv6SelIfIndex,
                                            i4_fsipv6SelProtoIndex,
                                            i4_fsipv6SelAccessIndex,
                                            i4_fsipv6SelPort,
                                            i4_fsipv6SelPktDirection,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SELIFIPADDRESS:
        {
            i1_ret_val =
                nmhSetFsipv6SelIfIpAddress (i4_fsipv6SelIfIndex,
                                            i4_fsipv6SelProtoIndex,
                                            i4_fsipv6SelAccessIndex,
                                            i4_fsipv6SelPort,
                                            i4_fsipv6SelPktDirection,
                                            p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SELSTATUS:
        {
            i1_ret_val =
                nmhSetFsipv6SelStatus (i4_fsipv6SelIfIndex,
                                       i4_fsipv6SelProtoIndex,
                                       i4_fsipv6SelAccessIndex,
                                       i4_fsipv6SelPort,
                                       i4_fsipv6SelPktDirection,
                                       p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSIPV6SELIFINDEX:
            /*  Read Only Variables. */
        case FSIPV6SELPROTOINDEX:
            /*  Read Only Variables. */
        case FSIPV6SELACCESSINDEX:
            /*  Read Only Variables. */
        case FSIPV6SELPORT:
            /*  Read Only Variables. */
        case FSIPV6SELPKTDIRECTION:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsIpv6SecSelectorEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsIpv6SecSelectorEntryTest (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsipv6SelIfIndex = FALSE;

    INT4                i4_fsipv6SelProtoIndex = FALSE;

    INT4                i4_fsipv6SelAccessIndex = FALSE;

    INT4                i4_fsipv6SelPort = FALSE;

    INT4                i4_fsipv6SelPktDirection = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsipv6SelIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsipv6SelProtoIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsipv6SelAccessIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsipv6SelPort =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsipv6SelPktDirection =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsipv6SecSelectorTable(i4_fsipv6SelIfIndex , i4_fsipv6SelProtoIndex , i4_fsipv6SelAccessIndex , i4_fsipv6SelPort , i4_fsipv6SelPktDirection)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSIPV6SELFILTERFLAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SelFilterFlag (&u4ErrorCode, i4_fsipv6SelIfIndex,
                                              i4_fsipv6SelProtoIndex,
                                              i4_fsipv6SelAccessIndex,
                                              i4_fsipv6SelPort,
                                              i4_fsipv6SelPktDirection,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SELPOLICYINDEX:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SelPolicyIndex (&u4ErrorCode,
                                               i4_fsipv6SelIfIndex,
                                               i4_fsipv6SelProtoIndex,
                                               i4_fsipv6SelAccessIndex,
                                               i4_fsipv6SelPort,
                                               i4_fsipv6SelPktDirection,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SELIFIPADDRESS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SelIfIpAddress (&u4ErrorCode,
                                               i4_fsipv6SelIfIndex,
                                               i4_fsipv6SelProtoIndex,
                                               i4_fsipv6SelAccessIndex,
                                               i4_fsipv6SelPort,
                                               i4_fsipv6SelPktDirection,
                                               p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SELSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SelStatus (&u4ErrorCode, i4_fsipv6SelIfIndex,
                                          i4_fsipv6SelProtoIndex,
                                          i4_fsipv6SelAccessIndex,
                                          i4_fsipv6SelPort,
                                          i4_fsipv6SelPktDirection,
                                          p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSIPV6SELIFINDEX:
        case FSIPV6SELPROTOINDEX:
        case FSIPV6SELACCESSINDEX:
        case FSIPV6SELPORT:
        case FSIPV6SELPKTDIRECTION:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsIpv6SecAccessEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsIpv6SecAccessEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsipv6SecAccessTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsipv6SecAccessIndex = FALSE;
    INT4                i4_next_fsipv6SecAccessIndex = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecSrcNet = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecDestNet = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsipv6SecAccessTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsipv6SecAccessTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsipv6SecAccessIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsipv6SecAccessTable
                     (i4_fsipv6SecAccessIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SecAccessIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsipv6SecAccessTable
                     (&i4_fsipv6SecAccessIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SecAccessIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SecAccessIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsipv6SecAccessTable
                     (i4_fsipv6SecAccessIndex,
                      &i4_next_fsipv6SecAccessIndex)) == SNMP_SUCCESS)
                {
                    i4_fsipv6SecAccessIndex = i4_next_fsipv6SecAccessIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SecAccessIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIPV6SECACCESSINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SecAccessIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SecAccessIndex;
            }
            break;
        }
        case FSIPV6SECACCESSSTATUS:
        {
            i1_ret_val =
                nmhGetFsipv6SecAccessStatus (i4_fsipv6SecAccessIndex,
                                             &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECSRCNET:
        {
            poctet_retval_fsipv6SecSrcNet =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_fsipv6SecSrcNet == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecSrcNet (i4_fsipv6SecAccessIndex,
                                       poctet_retval_fsipv6SecSrcNet);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecSrcNet;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecSrcNet);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECSRCADDRPREFIXLEN:
        {
            i1_ret_val =
                nmhGetFsipv6SecSrcAddrPrefixLen (i4_fsipv6SecAccessIndex,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECDESTNET:
        {
            poctet_retval_fsipv6SecDestNet =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_fsipv6SecDestNet == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecDestNet (i4_fsipv6SecAccessIndex,
                                        poctet_retval_fsipv6SecDestNet);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecDestNet;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecDestNet);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECDESTADDRPREFIXLEN:
        {
            i1_ret_val =
                nmhGetFsipv6SecDestAddrPrefixLen (i4_fsipv6SecAccessIndex,
                                                  &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsIpv6SecAccessEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsIpv6SecAccessEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsipv6SecAccessIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsipv6SecAccessIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSIPV6SECACCESSSTATUS:
        {
            i1_ret_val =
                nmhSetFsipv6SecAccessStatus (i4_fsipv6SecAccessIndex,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECSRCNET:
        {
            i1_ret_val =
                nmhSetFsipv6SecSrcNet (i4_fsipv6SecAccessIndex,
                                       p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECSRCADDRPREFIXLEN:
        {
            i1_ret_val =
                nmhSetFsipv6SecSrcAddrPrefixLen (i4_fsipv6SecAccessIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECDESTNET:
        {
            i1_ret_val =
                nmhSetFsipv6SecDestNet (i4_fsipv6SecAccessIndex,
                                        p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECDESTADDRPREFIXLEN:
        {
            i1_ret_val =
                nmhSetFsipv6SecDestAddrPrefixLen (i4_fsipv6SecAccessIndex,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSIPV6SECACCESSINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsIpv6SecAccessEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsIpv6SecAccessEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsipv6SecAccessIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsipv6SecAccessIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsipv6SecAccessTable(i4_fsipv6SecAccessIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSIPV6SECACCESSSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAccessStatus (&u4ErrorCode,
                                                i4_fsipv6SecAccessIndex,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECSRCNET:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecSrcNet (&u4ErrorCode, i4_fsipv6SecAccessIndex,
                                          p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECSRCADDRPREFIXLEN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecSrcAddrPrefixLen (&u4ErrorCode,
                                                    i4_fsipv6SecAccessIndex,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECDESTNET:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecDestNet (&u4ErrorCode,
                                           i4_fsipv6SecAccessIndex,
                                           p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECDESTADDRPREFIXLEN:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecDestAddrPrefixLen (&u4ErrorCode,
                                                     i4_fsipv6SecAccessIndex,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSIPV6SECACCESSINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsIpv6SecPolicyEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsIpv6SecPolicyEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsipv6SecPolicyTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsipv6SecPolicyIndex = FALSE;
    INT4                i4_next_fsipv6SecPolicyIndex = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecPolicySaBundle = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsipv6SecPolicyTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsipv6SecPolicyTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsipv6SecPolicyIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsipv6SecPolicyTable
                     (i4_fsipv6SecPolicyIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SecPolicyIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsipv6SecPolicyTable
                     (&i4_fsipv6SecPolicyIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SecPolicyIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SecPolicyIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsipv6SecPolicyTable
                     (i4_fsipv6SecPolicyIndex,
                      &i4_next_fsipv6SecPolicyIndex)) == SNMP_SUCCESS)
                {
                    i4_fsipv6SecPolicyIndex = i4_next_fsipv6SecPolicyIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SecPolicyIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIPV6SECPOLICYINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SecPolicyIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SecPolicyIndex;
            }
            break;
        }
        case FSIPV6SECPOLICYFLAG:
        {
            i1_ret_val =
                nmhGetFsipv6SecPolicyFlag (i4_fsipv6SecPolicyIndex,
                                           &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECPOLICYMODE:
        {
            i1_ret_val =
                nmhGetFsipv6SecPolicyMode (i4_fsipv6SecPolicyIndex,
                                           &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECPOLICYSABUNDLE:
        {
            poctet_retval_fsipv6SecPolicySaBundle =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
            if (poctet_retval_fsipv6SecPolicySaBundle == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecPolicySaBundle (i4_fsipv6SecPolicyIndex,
                                               poctet_retval_fsipv6SecPolicySaBundle);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecPolicySaBundle;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecPolicySaBundle);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECPOLICYOPTIONSINDEX:
        {
            i1_ret_val =
                nmhGetFsipv6SecPolicyOptionsIndex (i4_fsipv6SecPolicyIndex,
                                                   &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECPOLICYSTATUS:
        {
            i1_ret_val =
                nmhGetFsipv6SecPolicyStatus (i4_fsipv6SecPolicyIndex,
                                             &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsIpv6SecPolicyEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsIpv6SecPolicyEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsipv6SecPolicyIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsipv6SecPolicyIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSIPV6SECPOLICYFLAG:
        {
            i1_ret_val =
                nmhSetFsipv6SecPolicyFlag (i4_fsipv6SecPolicyIndex,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECPOLICYMODE:
        {
            i1_ret_val =
                nmhSetFsipv6SecPolicyMode (i4_fsipv6SecPolicyIndex,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECPOLICYSABUNDLE:
        {
            i1_ret_val =
                nmhSetFsipv6SecPolicySaBundle (i4_fsipv6SecPolicyIndex,
                                               p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECPOLICYOPTIONSINDEX:
        {
            i1_ret_val =
                nmhSetFsipv6SecPolicyOptionsIndex (i4_fsipv6SecPolicyIndex,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECPOLICYSTATUS:
        {
            i1_ret_val =
                nmhSetFsipv6SecPolicyStatus (i4_fsipv6SecPolicyIndex,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSIPV6SECPOLICYINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsIpv6SecPolicyEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsIpv6SecPolicyEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsipv6SecPolicyIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsipv6SecPolicyIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsipv6SecPolicyTable(i4_fsipv6SecPolicyIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSIPV6SECPOLICYFLAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecPolicyFlag (&u4ErrorCode,
                                              i4_fsipv6SecPolicyIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECPOLICYMODE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecPolicyMode (&u4ErrorCode,
                                              i4_fsipv6SecPolicyIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECPOLICYSABUNDLE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecPolicySaBundle (&u4ErrorCode,
                                                  i4_fsipv6SecPolicyIndex,
                                                  p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECPOLICYOPTIONSINDEX:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecPolicyOptionsIndex (&u4ErrorCode,
                                                      i4_fsipv6SecPolicyIndex,
                                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECPOLICYSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecPolicyStatus (&u4ErrorCode,
                                                i4_fsipv6SecPolicyIndex,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSIPV6SECPOLICYINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsipv6SecAssocEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsipv6SecAssocEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsipv6SecAssocTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsipv6SecAssocIndex = FALSE;
    INT4                i4_next_fsipv6SecAssocIndex = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecAssocDstAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecAssocAhKey = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecAssocEspKey = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecAssocEspKey2 = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecAssocEspKey3 = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsipv6SecAssocTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsipv6SecAssocTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsipv6SecAssocIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsipv6SecAssocTable
                     (i4_fsipv6SecAssocIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SecAssocIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsipv6SecAssocTable
                     (&i4_fsipv6SecAssocIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SecAssocIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SecAssocIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsipv6SecAssocTable (i4_fsipv6SecAssocIndex,
                                                         &i4_next_fsipv6SecAssocIndex))
                    == SNMP_SUCCESS)
                {
                    i4_fsipv6SecAssocIndex = i4_next_fsipv6SecAssocIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SecAssocIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIPV6SECASSOCINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SecAssocIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SecAssocIndex;
            }
            break;
        }
        case FSIPV6SECASSOCDSTADDR:
        {
            poctet_retval_fsipv6SecAssocDstAddr =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_fsipv6SecAssocDstAddr == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecAssocDstAddr (i4_fsipv6SecAssocIndex,
                                             poctet_retval_fsipv6SecAssocDstAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecAssocDstAddr;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecAssocDstAddr);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCPROTOCOL:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocProtocol (i4_fsipv6SecAssocIndex,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCSPI:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocSpi (i4_fsipv6SecAssocIndex,
                                         &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCMODE:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocMode (i4_fsipv6SecAssocIndex,
                                          &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCAHALGO:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocAhAlgo (i4_fsipv6SecAssocIndex,
                                            &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCAHKEY:
        {
            poctet_retval_fsipv6SecAssocAhKey =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (21);
            if (poctet_retval_fsipv6SecAssocAhKey == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecAssocAhKey (i4_fsipv6SecAssocIndex,
                                           poctet_retval_fsipv6SecAssocAhKey);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecAssocAhKey;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecAssocAhKey);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCESPALGO:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocEspAlgo (i4_fsipv6SecAssocIndex,
                                             &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCESPKEY:
        {
            poctet_retval_fsipv6SecAssocEspKey =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (9);
            if (poctet_retval_fsipv6SecAssocEspKey == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecAssocEspKey (i4_fsipv6SecAssocIndex,
                                            poctet_retval_fsipv6SecAssocEspKey);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecAssocEspKey;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecAssocEspKey);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCESPKEY2:
        {
            poctet_retval_fsipv6SecAssocEspKey2 =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (9);
            if (poctet_retval_fsipv6SecAssocEspKey2 == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecAssocEspKey2 (i4_fsipv6SecAssocIndex,
                                             poctet_retval_fsipv6SecAssocEspKey2);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecAssocEspKey2;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecAssocEspKey2);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCESPKEY3:
        {
            poctet_retval_fsipv6SecAssocEspKey3 =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (9);
            if (poctet_retval_fsipv6SecAssocEspKey3 == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecAssocEspKey3 (i4_fsipv6SecAssocIndex,
                                             poctet_retval_fsipv6SecAssocEspKey3);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecAssocEspKey3;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecAssocEspKey3);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCLIFETIMEINBYTES:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocLifetimeInBytes (i4_fsipv6SecAssocIndex,
                                                     &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCLIFETIME:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocLifetime (i4_fsipv6SecAssocIndex,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCANTIREPLAY:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocAntiReplay (i4_fsipv6SecAssocIndex,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECASSOCSTATUS:
        {
            i1_ret_val =
                nmhGetFsipv6SecAssocStatus (i4_fsipv6SecAssocIndex,
                                            &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsipv6SecAssocEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsipv6SecAssocEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsipv6SecAssocIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsipv6SecAssocIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSIPV6SECASSOCDSTADDR:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocDstAddr (i4_fsipv6SecAssocIndex,
                                             p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCPROTOCOL:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocProtocol (i4_fsipv6SecAssocIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCSPI:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocSpi (i4_fsipv6SecAssocIndex,
                                         p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCMODE:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocMode (i4_fsipv6SecAssocIndex,
                                          p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCAHALGO:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocAhAlgo (i4_fsipv6SecAssocIndex,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCAHKEY:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocAhKey (i4_fsipv6SecAssocIndex,
                                           p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCESPALGO:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocEspAlgo (i4_fsipv6SecAssocIndex,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCESPKEY:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocEspKey (i4_fsipv6SecAssocIndex,
                                            p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCESPKEY2:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocEspKey2 (i4_fsipv6SecAssocIndex,
                                             p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCESPKEY3:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocEspKey3 (i4_fsipv6SecAssocIndex,
                                             p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCLIFETIMEINBYTES:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocLifetimeInBytes (i4_fsipv6SecAssocIndex,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCLIFETIME:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocLifetime (i4_fsipv6SecAssocIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCANTIREPLAY:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocAntiReplay (i4_fsipv6SecAssocIndex,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSIPV6SECASSOCSTATUS:
        {
            i1_ret_val =
                nmhSetFsipv6SecAssocStatus (i4_fsipv6SecAssocIndex,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSIPV6SECASSOCINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsipv6SecAssocEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsipv6SecAssocEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsipv6SecAssocIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsipv6SecAssocIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsipv6SecAssocTable(i4_fsipv6SecAssocIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSIPV6SECASSOCDSTADDR:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocDstAddr (&u4ErrorCode,
                                                i4_fsipv6SecAssocIndex,
                                                p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCPROTOCOL:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocProtocol (&u4ErrorCode,
                                                 i4_fsipv6SecAssocIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCSPI:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocSpi (&u4ErrorCode,
                                            i4_fsipv6SecAssocIndex,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCMODE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocMode (&u4ErrorCode,
                                             i4_fsipv6SecAssocIndex,
                                             p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCAHALGO:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocAhAlgo (&u4ErrorCode,
                                               i4_fsipv6SecAssocIndex,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCAHKEY:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocAhKey (&u4ErrorCode,
                                              i4_fsipv6SecAssocIndex,
                                              p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCESPALGO:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocEspAlgo (&u4ErrorCode,
                                                i4_fsipv6SecAssocIndex,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCESPKEY:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocEspKey (&u4ErrorCode,
                                               i4_fsipv6SecAssocIndex,
                                               p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCESPKEY2:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocEspKey2 (&u4ErrorCode,
                                                i4_fsipv6SecAssocIndex,
                                                p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCESPKEY3:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocEspKey3 (&u4ErrorCode,
                                                i4_fsipv6SecAssocIndex,
                                                p_value->pOctetStrValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCLIFETIMEINBYTES:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocLifetimeInBytes (&u4ErrorCode,
                                                        i4_fsipv6SecAssocIndex,
                                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCLIFETIME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocLifetime (&u4ErrorCode,
                                                 i4_fsipv6SecAssocIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCANTIREPLAY:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocAntiReplay (&u4ErrorCode,
                                                   i4_fsipv6SecAssocIndex,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSIPV6SECASSOCSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsipv6SecAssocStatus (&u4ErrorCode,
                                               i4_fsipv6SecAssocIndex,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSIPV6SECASSOCINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsIpv6SecIfStatsEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsIpv6SecIfStatsEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsipv6SecIfStatsTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsipv6SecIfIndex = FALSE;
    INT4                i4_next_fsipv6SecIfIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsipv6SecIfStatsTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsipv6SecIfStatsTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsipv6SecIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsipv6SecIfStatsTable
                     (i4_fsipv6SecIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SecIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsipv6SecIfStatsTable
                     (&i4_fsipv6SecIfIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SecIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SecIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsipv6SecIfStatsTable (i4_fsipv6SecIfIndex,
                                                           &i4_next_fsipv6SecIfIndex))
                    == SNMP_SUCCESS)
                {
                    i4_fsipv6SecIfIndex = i4_next_fsipv6SecIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SecIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIPV6SECIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SecIfIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SecIfIndex;
            }
            break;
        }
        case FSIPV6SECIFINPKTS:
        {
            i1_ret_val =
                nmhGetFsipv6SecIfInPkts (i4_fsipv6SecIfIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECIFOUTPKTS:
        {
            i1_ret_val =
                nmhGetFsipv6SecIfOutPkts (i4_fsipv6SecIfIndex, &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECIFPKTSAPPLY:
        {
            i1_ret_val =
                nmhGetFsipv6SecIfPktsApply (i4_fsipv6SecIfIndex,
                                            &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECIFPKTSDISCARD:
        {
            i1_ret_val =
                nmhGetFsipv6SecIfPktsDiscard (i4_fsipv6SecIfIndex,
                                              &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECIFPKTSBYPASS:
        {
            i1_ret_val =
                nmhGetFsipv6SecIfPktsBypass (i4_fsipv6SecIfIndex,
                                             &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsIpv6SecAhEspStatsEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsIpv6SecAhEspStatsEntryGet (tSNMP_OID_TYPE * p_in_db,
                             tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                             UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsipv6SecAhEspStatsTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsipv6SecAhEspIfIndex = FALSE;
    INT4                i4_next_fsipv6SecAhEspIfIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsipv6SecAhEspStatsTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsipv6SecAhEspStatsTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsipv6SecAhEspIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsipv6SecAhEspStatsTable
                     (i4_fsipv6SecAhEspIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SecAhEspIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsipv6SecAhEspStatsTable
                     (&i4_fsipv6SecAhEspIfIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SecAhEspIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SecAhEspIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsipv6SecAhEspStatsTable
                     (i4_fsipv6SecAhEspIfIndex,
                      &i4_next_fsipv6SecAhEspIfIndex)) == SNMP_SUCCESS)
                {
                    i4_fsipv6SecAhEspIfIndex = i4_next_fsipv6SecAhEspIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SecAhEspIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIPV6SECAHESPIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SecAhEspIfIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SecAhEspIfIndex;
            }
            break;
        }
        case FSIPV6SECINAHPKTS:
        {
            i1_ret_val =
                nmhGetFsipv6SecInAhPkts (i4_fsipv6SecAhEspIfIndex,
                                         &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECOUTAHPKTS:
        {
            i1_ret_val =
                nmhGetFsipv6SecOutAhPkts (i4_fsipv6SecAhEspIfIndex,
                                          &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECAHPKTSALLOW:
        {
            i1_ret_val =
                nmhGetFsipv6SecAhPktsAllow (i4_fsipv6SecAhEspIfIndex,
                                            &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECAHPKTSDISCARD:
        {
            i1_ret_val =
                nmhGetFsipv6SecAhPktsDiscard (i4_fsipv6SecAhEspIfIndex,
                                              &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECINESPPKTS:
        {
            i1_ret_val =
                nmhGetFsipv6SecInEspPkts (i4_fsipv6SecAhEspIfIndex,
                                          &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECOUTESPPKTS:
        {
            i1_ret_val =
                nmhGetFsipv6SecOutEspPkts (i4_fsipv6SecAhEspIfIndex,
                                           &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECESPPKTSALLOW:
        {
            i1_ret_val =
                nmhGetFsipv6SecEspPktsAllow (i4_fsipv6SecAhEspIfIndex,
                                             &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECESPPKTSDISCARD:
        {
            i1_ret_val =
                nmhGetFsipv6SecEspPktsDiscard (i4_fsipv6SecAhEspIfIndex,
                                               &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsIpv6SecAhEspIntruEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsIpv6SecAhEspIntruEntryGet (tSNMP_OID_TYPE * p_in_db,
                             tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                             UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsipv6SecAhEspIntruTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsipv6SecAhEspIntruIndex = FALSE;
    INT4                i4_next_fsipv6SecAhEspIntruIndex = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecAhEspIntruSrcAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsipv6SecAhEspIntruDestAddr = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsipv6SecAhEspIntruTable_INDEX =
                p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsipv6SecAhEspIntruTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsipv6SecAhEspIntruIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsipv6SecAhEspIntruTable
                     (i4_fsipv6SecAhEspIntruIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsipv6SecAhEspIntruIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsipv6SecAhEspIntruTable
                     (&i4_fsipv6SecAhEspIntruIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsipv6SecAhEspIntruIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsipv6SecAhEspIntruIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsipv6SecAhEspIntruTable
                     (i4_fsipv6SecAhEspIntruIndex,
                      &i4_next_fsipv6SecAhEspIntruIndex)) == SNMP_SUCCESS)
                {
                    i4_fsipv6SecAhEspIntruIndex =
                        i4_next_fsipv6SecAhEspIntruIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsipv6SecAhEspIntruIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSIPV6SECAHESPINTRUINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsipv6SecAhEspIntruIndex;
            }
            else
            {
                i4_return_val = i4_next_fsipv6SecAhEspIntruIndex;
            }
            break;
        }
        case FSIPV6SECAHESPINTRUIFINDEX:
        {
            i1_ret_val =
                nmhGetFsipv6SecAhEspIntruIfIndex (i4_fsipv6SecAhEspIntruIndex,
                                                  &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECAHESPINTRUSRCADDR:
        {
            poctet_retval_fsipv6SecAhEspIntruSrcAddr =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_fsipv6SecAhEspIntruSrcAddr == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecAhEspIntruSrcAddr (i4_fsipv6SecAhEspIntruIndex,
                                                  poctet_retval_fsipv6SecAhEspIntruSrcAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecAhEspIntruSrcAddr;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecAhEspIntruSrcAddr);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECAHESPINTRUDESTADDR:
        {
            poctet_retval_fsipv6SecAhEspIntruDestAddr =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_fsipv6SecAhEspIntruDestAddr == NULL)
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsipv6SecAhEspIntruDestAddr (i4_fsipv6SecAhEspIntruIndex,
                                                   poctet_retval_fsipv6SecAhEspIntruDestAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsipv6SecAhEspIntruDestAddr;
            }
            else
            {
                free_octetstring (poctet_retval_fsipv6SecAhEspIntruDestAddr);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECAHESPINTRUPROTO:
        {
            i1_ret_val =
                nmhGetFsipv6SecAhEspIntruProto (i4_fsipv6SecAhEspIntruIndex,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSIPV6SECAHESPINTRUTIME:
        {
            i1_ret_val =
                nmhGetFsipv6SecAhEspIntruTime (i4_fsipv6SecAhEspIntruIndex,
                                               &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */
