/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: sec6kpor.c,v 1.3 2011/11/11 09:33:17 siva Exp $
 *
 * Description : This file contains the funtions defined for the
 *               kernel space
 *
 *******************************************************************/
#include "secv6com.h"
#include "secv6.h"
#include "arsec.h"

extern INT4         gi4SecDevFd;;

/***********************************************************************/
/*  Function Name : Secv6ProcessIKEMsg                                 */
/*  Description   : This function posts a message to IPSecv6 by        */
/*                  using a ioctl call when IPSecv6 is in kernel       */
/*                :                                                    */
/*  Input(s)      : pMsg - Points to the Message rcvd from IKE         */
/*                :                                                    */
/*  Output(s)     :None                                                */
/*  Return        :None                                                */
/***********************************************************************/
VOID
Secv6ProcessIKEMsg (tIkeIPSecQMsg * pMsg)
{
    tSecv6KernIface     Secv6KernIface;

    Secv6KernIface.u1MsgType = IPSECV6_IKE_REQ;
    MEMCPY (&(Secv6KernIface.uMsg.IkeIPSecMsg), pMsg, sizeof (tIkeIPSecQMsg));

    if (ioctl (gi4SecDevFd, IPSECV6_IOCTL, &Secv6KernIface) != IOCTL_SUCCESS)
    {
        return;
    }

    return;
}

/************************************************************************/
/*  Function Name   : Secv6GetSecv6AssocOverHead                        */
/*  Description     : This function returns the Sec overhead            */
/*                  :                                                   */
/*  Input(s)        : SrcAddr:Refers to Ipv6 src Addr                   */
/*                    DestAddr:Refers to Ipv6 dest Addr                 */
/*                  : u2Index:Refers to the InterfaceIndex              */
/*                  : u2Protocol Refers to the Protocol                 */
/*  Output(s)       :                                                   */
/*  Returns         : Returns SecOverHead                               */
/************************************************************************/
UINT2
Secv6GetSecAssocOverHead (tIp6Addr SrcAddr, tIp6Addr DestAddr, UINT2 u2Index,
                          UINT1 u1Protocol)
{
    tSecv6KernIface     Secv6KernIface;

    Secv6KernIface.u1MsgType = IPSECV6_ASSOC_OVERHEAD_REQ;
    MEMCPY (&(Secv6KernIface.uMsg.SecAssocOverHead.SrcAddr), &SrcAddr,
            sizeof (SrcAddr));
    MEMCPY (&(Secv6KernIface.uMsg.SecAssocOverHead.DestAddr), &DestAddr,
            sizeof (SrcAddr));
    Secv6KernIface.uMsg.SecAssocOverHead.u2Index = u2Index;
    Secv6KernIface.uMsg.SecAssocOverHead.u1Protocol = u1Protocol;

    if (ioctl (gi4SecDevFd, IPSECV6_IOCTL, &Secv6KernIface) != IOCTL_SUCCESS)
    {
        return 0;
    }

    return (Secv6KernIface.uMsg.SecAssocOverHead.u2OverHead);
}

/*************************************************************************/
/*  Function Name : Secv6DeleteAllSecAssocEntries                        */
/*  Description   : This function deletes all security association       */
/*                : structures maintained in Secv6Assoclist              */
/*                :                                                      */
/*  Input(s)      : None                                                 */
/*                                                                       */
/*  Output(s)     : None                                                 */
/*                                                                       */
/*  Return Values : SEC_SUCCESS or SEC_FAILURE                           */
/*************************************************************************/
INT1
Secv6DeleteAllSecAssocEntries (VOID)
{
    tSecv6KernIface     Secv6KernIface;

    Secv6KernIface.u1MsgType = IPSECV6_DELETE_SA_ENTRIES;

    if (ioctl (gi4SecDevFd, IPSECV6_IOCTL, &Secv6KernIface) != IOCTL_SUCCESS)
    {
        return SEC_FAILURE;
    }

    return SEC_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Secv6AccListExist                                 */
/*  Description     : This function returns whether Access List exist   */
/*                  : for the given Source and Destination              */
/*  Input(s)        : SrcAddr  - Refers to Ipv6 Source Address          */
/*                  : DestAddr - Refers to Ipv6 Destination Address     */
/*  Output(s)       :                                                   */
/*  Returns         : Returns SEC_FAILURE or SEC_SUCCESS                */
/************************************************************************/
INT1
Secv6AccListExist (tIp6Addr SrcAddr, tIp6Addr DestAddr)
{

    UNUSED_PARAM (SrcAddr);
    UNUSED_PARAM (DestAddr);
    return (SEC_FAILURE);
}

/*---------------------------------------------------------------
 *  Function Name : Secv6InProcess
 *  Description   : This function will be stub If the ipsecv6 is in 
 *                  kernel. This will be compiled for userspace.
 *  Input(s)      : pBuf - Buffer contains IP Secured packet.
 *                  pIf6 - Buffer contains interface info.
 *                  pu4ProcessedLe - 
 *  Output(s)     : pBuf - Buffer contains IP packet.
 *                  pu4ProcessedLe - 
 *  Global(s)     : None.
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------*/

INT1
Secv6InProcess (tBufChainHeader * pBuf, tIp6If * pIf6, UINT1 *pNhr,
                UINT4 *pu4ProcessedLen)
{

    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pIf6);
    UNUSED_PARAM (pNhr);
    UNUSED_PARAM (pu4ProcessedLen);

    return SEC_SUCCESS;
}

/* ---------------------------------------------------------------
 *  Function Name : Secv6OutProcess
 *  Description   : This function will be stub If the ipsecv6 is in
 *                  kernel. This will be compiled for userspace.
 *  Input(s)      : pBuf - Buffer contains IP packet.
 *                  pIf6 - Buffer contains interface info.
 *                  pu4Len - 
 *  Output(s)     : pBuf - Buffer contains IP Secured packet.
 *                  pu4Len - 
 *  Global(s)     : None.
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------*/

INT1
Secv6OutProcess (tBufChainHeader * pBuf, tIp6If * pIf6, UINT4 *pu4Len)
{
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (pIf6);
    UNUSED_PARAM (pu4Len);

    return SEC_SUCCESS;
}

/************************************************************************/
/*  Function Name   : Secv6Initialize                                   */
/*  Description     : This function will be stub If the ipsecv6 is in   */
/*                    kernel. This will be compiled for userspace       */
/*                  :                                                   */
/*  Input(s)        : None                                              */
/*  Output(s)       : None                                              */
/*  Returns         : SEC_SUCCESS                                       */
/************************************************************************/
INT1
Secv6Initialize (VOID)
{
    lrInitComplete (OSIX_SUCCESS);
    return (SEC_SUCCESS);
}

/***************************************************************************
 * Function Name    :  Secv6UtilGetGlobalStatus
 * Description      :  This function returns the global IPSECv6 status
 *
 * Input (s)        :  None.
 *
 * Output (s)       :  None.
 * Returns          :  SEC_ENABLE/SEC_DISABLE.
 ***************************************************************************/
INT1
Secv6UtilGetGlobalStatus (VOID)
{
    return SEC_DISABLE;
}

INT4
Secv6SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    UNUSED_PARAM (pu1ModName);
    return OSIX_SUCCESS;
}
