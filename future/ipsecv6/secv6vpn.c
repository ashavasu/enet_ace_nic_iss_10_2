/*********************************************************************** 
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved                *
 *                                                                     *
 * $Id: secv6vpn.c,v 1.6 2013/10/29 11:42:37 siva Exp $                                                               *
 *                                                                     *
 * Description: This has APIs exported for the VPN module.             *
 *                                                                     *
 ***********************************************************************/
#ifndef __SECV6VPN_C__
#define __SECV6VPN_C__

#include "secv6com.h"
#include "vpn.h"
#include "fssnmp.h"
#include "vpncli.h"
#include "secv6low.h"
#include "fsike.h"

#define ESP_KEY_MAX     68
#define ESP_SUB_KEY_MAX 16

PRIVATE INT4
 
     Secv6VpnFillSecAssocParams (tVpnPolicy * pVpnPolicy, UINT4 u4PktDirection);
PRIVATE INT4
 
 
 
 Secv6VpnFillSecAssocAddrParams (UINT4 u4SecAssocIndex,
                                 tIp6Addr * SecAssocDestAddr,
                                 UINT4 u4SecAssocSpi,
                                 tIp6Addr * SecAssocSrcAddr);
PRIVATE INT4        Secv6VpnFillAuthSecAssocParams (tVpnPolicy * pVpnPolicy,
                                                    UINT4 u4PktDirection);
PRIVATE INT4        Secv6VpnFillAuthParams (UINT4 u4SecAssocIndex,
                                            UINT1 u1SecAssocProtocol,
                                            UINT1 u1SecAssocAhAlgo,
                                            UINT1 *au1SecAssocAhKey);
PRIVATE INT4        Secv6VpnFillEncrParams (UINT4 u4SecAssocIndex,
                                            UINT1 u1SecAssocProtocol,
                                            UINT1 u1SecAssocEspAlgo,
                                            UINT1 *au1EspKey);
PRIVATE INT4        Secv6VpnFillPolicyParams (tVpnPolicy * pVpnPolicy,
                                              UINT1 u1PolicyMode,
                                              UINT4 u4PktDirection);
PRIVATE INT4        Secv6VpnFillAccessParams (tVpnPolicy * pVpnPolicy,
                                              UINT4 u4PktDirection);

PRIVATE INT4
 
 
 
 Secv6VpnFillAccessNetworkParams (UINT4 u4AccessIndex, tIp6Addr * pSrcAddress,
                                  UINT4 u4SrcAddrPrefixLen,
                                  tIp6Addr * pDestAddress,
                                  UINT4 u4DestAddrPrefixLen);
PRIVATE INT4        Secv6VpnFillSelectorParams (tVpnPolicy * pVpnPolicy,
                                                UINT4 u4PktDirection);

PRIVATE VOID
       Secv6FillSelectorParamsFail (INT4 i4PolicyIndex, UINT4 u4PolicyType);

PRIVATE INT4
       Secv6SetIkeVersion (UINT4 u4PolicyIndex, UINT1 u1IkeVersion);
#ifdef IKE_WANTED
PRIVATE VOID
 
 
 
 Secv6DeleteAssoc (tIp6Addr SrcAddr, tIp6Addr DestAddr, UINT4 u4IfIndex,
                   UINT4 u4Protocol, UINT2 u2LocPort, UINT2 u2RemPort);
#endif
PRIVATE INT4
 
 
 
 Secv6SetAccessListInfo (UINT4 u4AccessListIndex, UINT2 u2LocalStartPort,
                         UINT2 u2LocalEndPort, UINT2 u2RemoteStartPort,
                         UINT2 u2RemoteEndPort, UINT4 u4Protocol);

/****************************************************************************
  Function    :  Secv6VpnFillIpsecParams
  Description :  This function populates the IPSec tables with the VPN Policy 
                 values.
  Input       :  tVpnPolicy * - pointer to the VPN policy.
  Output      :  None.
  Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
Secv6VpnFillIpsecParams (tVpnPolicy * pVpnPolicy)
{

    UINT4               u4VpnPolicyType = 0;

    SECv6_TAKE_SEM ();

    if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        if (Secv6VpnFillSecAssocParams (pVpnPolicy, VPN_INBOUND)
            == SNMP_FAILURE)
        {
            SECv6_GIVE_SEM ();
            return (OSIX_FAILURE);
        }
        if (Secv6VpnFillSecAssocParams (pVpnPolicy, VPN_OUTBOUND)
            == SNMP_FAILURE)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            SECv6_GIVE_SEM ();
            return (OSIX_FAILURE);
        }

        if (Secv6VpnFillAuthSecAssocParams (pVpnPolicy, VPN_INBOUND)
            == SNMP_FAILURE)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv6SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
            SECv6_GIVE_SEM ();
            return (OSIX_FAILURE);
        }

        if (Secv6VpnFillAuthSecAssocParams (pVpnPolicy, VPN_OUTBOUND)
            == SNMP_FAILURE)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv6SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
            SECv6_GIVE_SEM ();
            return (OSIX_FAILURE);
        }
    }

    if (Secv6VpnFillAccessParams (pVpnPolicy, VPN_INBOUND) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_FILL_IPSEC_ACCESS_FAIL);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv6SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }
        SECv6_GIVE_SEM ();
        return (OSIX_FAILURE);
    }
    if (Secv6SetAccessListInfo (pVpnPolicy->u4VpnPolicyIndex,
                                pVpnPolicy->LocalProtectNetwork.u2StartPort,
                                pVpnPolicy->LocalProtectNetwork.u2EndPort,
                                pVpnPolicy->RemoteProtectNetwork.u2StartPort,
                                pVpnPolicy->RemoteProtectNetwork.u2EndPort,
                                pVpnPolicy->u4VpnProtocol) == SEC_FAILURE)
    {
        nmhSetFsipv6SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        CLI_SET_ERR (CLI_VPN_FILL_IPSEC_ACCESS_FAIL);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv6SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }
        SECv6_GIVE_SEM ();
        return (OSIX_FAILURE);
    }

    if (Secv6VpnFillAccessParams (pVpnPolicy, VPN_OUTBOUND) == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_VPN_FILL_IPSEC_ACCESS_FAIL);
        nmhSetFsipv6SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv6SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }
        SECv6_GIVE_SEM ();
        return (OSIX_FAILURE);
    }
    if (Secv6SetAccessListInfo (((pVpnPolicy->u4VpnPolicyIndex) + 1),
                                pVpnPolicy->RemoteProtectNetwork.u2StartPort,
                                pVpnPolicy->RemoteProtectNetwork.u2EndPort,
                                pVpnPolicy->LocalProtectNetwork.u2StartPort,
                                pVpnPolicy->LocalProtectNetwork.u2EndPort,
                                pVpnPolicy->u4VpnProtocol) == SEC_FAILURE)
    {
        nmhSetFsipv6SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv6SecAccessStatus ((INT4)
                                     ((pVpnPolicy->u4VpnPolicyIndex) + 1),
                                     DESTROY);
        CLI_SET_ERR (CLI_VPN_FILL_IPSEC_ACCESS_FAIL);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv6SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }
        SECv6_GIVE_SEM ();
        return (OSIX_FAILURE);
    }

    /* setting the Mode as Manual when configuring Policy Params for XAUTH
       in IPSec DB */
    if ((pVpnPolicy->u4VpnPolicyType == VPN_XAUTH) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_CERTIFICATE) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_PRESHAREDKEY) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_RA_CERT) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_XAUTH_CERT))
    {
        u4VpnPolicyType = VPN_IKE_PRESHAREDKEY;
    }
    else
    {
        u4VpnPolicyType = pVpnPolicy->u4VpnPolicyType;
    }

    if (Secv6VpnFillPolicyParams (pVpnPolicy, (UINT1) u4VpnPolicyType,
                                  VPN_INBOUND) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv6SecAccessStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv6SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }

        SECv6_GIVE_SEM ();
        return (OSIX_FAILURE);
    }
    if (Secv6VpnFillPolicyParams (pVpnPolicy, (UINT1) u4VpnPolicyType,
                                  VPN_OUTBOUND) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecPolicyStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv6SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv6SecAccessStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
        if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
        {
            nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                        DESTROY);
            nmhSetFsipv6SecAssocStatus ((INT4)
                                        (pVpnPolicy->u4VpnPolicyIndex + 1),
                                        DESTROY);
        }

        SECv6_GIVE_SEM ();
        return (OSIX_FAILURE);
    }

    if (Secv6VpnFillSelectorParams (pVpnPolicy, VPN_INBOUND) == SNMP_FAILURE)
    {
        Secv6FillSelectorParamsFail ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     pVpnPolicy->u4VpnPolicyType);
        SECv6_GIVE_SEM ();
        return (OSIX_FAILURE);
    }
    if (Secv6VpnFillSelectorParams (pVpnPolicy, VPN_OUTBOUND) == SNMP_FAILURE)
    {
        nmhSetFsipv6SelStatus ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) pVpnPolicy->u4VpnPolicyIndex,
                               VPN_DEFAULT_PORT, VPN_INBOUND, DESTROY);
        Secv6FillSelectorParamsFail ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     pVpnPolicy->u4VpnPolicyType);
        SECv6_GIVE_SEM ();
        return (OSIX_FAILURE);
    }

    SECv6_GIVE_SEM ();
    return OSIX_SUCCESS;
}

/****************************************************************************
  Function    :  Secv6VpnDeleteIpsecParams
  Description :  This function deletes the created vpn policy.
  Input       :  tVpnPolicy * - pointer to the VPN policy.
  Output      :  None.
  Returns     :  OSIX_SUCCESS
****************************************************************************/

INT4
Secv6VpnDeleteIpsecParams (tVpnPolicy * pVpnPolicy)
{

    SECv6_TAKE_SEM ();

    if (pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL)
    {
        nmhSetFsipv6SecAssocStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                    DESTROY);
        nmhSetFsipv6SecAssocStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                    DESTROY);
    }
    else
    {
#ifdef IKE_WANTED
        Secv6DeleteAssoc (pVpnPolicy->LocalProtectNetwork.IpAddr.Ipv6Addr,
                          pVpnPolicy->RemoteProtectNetwork.IpAddr.Ipv6Addr,
                          pVpnPolicy->u4IfIndex, pVpnPolicy->u4VpnProtocol,
                          pVpnPolicy->LocalProtectNetwork.u2StartPort,
                          pVpnPolicy->RemoteProtectNetwork.u2StartPort);

        Secv6DeleteAssoc (pVpnPolicy->RemoteProtectNetwork.IpAddr.Ipv6Addr,
                          pVpnPolicy->LocalProtectNetwork.IpAddr.Ipv6Addr,
                          pVpnPolicy->u4IfIndex, pVpnPolicy->u4VpnProtocol,
                          pVpnPolicy->RemoteProtectNetwork.u2StartPort,
                          pVpnPolicy->LocalProtectNetwork.u2StartPort);
#endif
    }

    if ((pVpnPolicy->u4VpnPolicyType == VPN_IPSEC_MANUAL) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_CERTIFICATE) ||
        (pVpnPolicy->u4VpnPolicyType == VPN_IKE_PRESHAREDKEY))
    {
        nmhSetFsipv6SelStatus ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) pVpnPolicy->u4VpnPolicyIndex,
                               VPN_DEFAULT_PORT, VPN_INBOUND, DESTROY);

        nmhSetFsipv6SelStatus ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                               VPN_DEFAULT_PORT, VPN_OUTBOUND, DESTROY);

        nmhSetFsipv6SecPolicyStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv6SecPolicyStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);

        nmhSetFsipv6SecAccessStatus ((INT4) pVpnPolicy->u4VpnPolicyIndex,
                                     DESTROY);
        nmhSetFsipv6SecAccessStatus ((INT4) (pVpnPolicy->u4VpnPolicyIndex + 1),
                                     DESTROY);
    }

    SECv6_GIVE_SEM ();
    return OSIX_SUCCESS;
}

/****************************************************************************
  Function    :  Secv6VpnGetIpsecPktStats
  Description :  This function get the number of packets dropped/secured
                 either of inbound or outbound in all the interfaces.
  Input       :  i4StatType - Type of the stats. (dropped or secured)
                 pu4Stats - No of packets dropped or secured.
  Output      :  None.
  Returns     :  None.
****************************************************************************/
VOID
Secv6VpnGetIpsecPktStats (INT4 i4StatType, UINT4 *pu4Stats)
{
    INT4                i4IfIndex = 0;
    UINT4               u4count = 0;

    *pu4Stats = 0;

    SECv6_TAKE_SEM ();

    for (i4IfIndex = 0; i4IfIndex < SEC_MAX_STAT_COUNT; i4IfIndex++)
    {
        switch (i4StatType)
        {
            case SEC_INBOUND:
                nmhGetFsipv6SecIfInPkts (i4IfIndex, &u4count);
                break;

            case SEC_OUTBOUND:
                nmhGetFsipv6SecIfOutPkts (i4IfIndex, &u4count);
                break;

            default:
                return;
        }
        *pu4Stats = *pu4Stats + u4count;
    }

    SECv6_GIVE_SEM ();
}

/****************************************************************************
  Function    :  Secv6VpnGetIpsecSecurityStats
  Description :  This function get the number of packets received/transmitted
                 on the all the interfaces.
  Input       :  i4StatType - Type of the stats. (Rx/Tx)
                 pu4Stats - No of packets received/transmitted.
  Output      :  None.
  Returns     :  None.
****************************************************************************/
VOID
Secv6VpnGetIpsecSecurityStats (INT4 i4StatType, UINT4 *pu4Stats)
{
    INT4                i4IfIndex = 0;
    UINT4               u4count = 0;

    *pu4Stats = 0;

    SECv6_TAKE_SEM ();

    for (i4IfIndex = 0; i4IfIndex < SEC_MAX_STAT_COUNT; i4IfIndex++)
    {
        switch (i4StatType)
        {
            case SEC_APPLY:
                nmhGetFsipv6SecIfPktsApply (i4IfIndex, &u4count);
                break;

            case SEC_FILTER:
                nmhGetFsipv6SecIfPktsDiscard (i4IfIndex, &u4count);
                break;

            default:
                return;
        }
        *pu4Stats = *pu4Stats + u4count;
    }

    SECv6_GIVE_SEM ();
}

/****************************************************************************
  Function    : Secv6VpnFillSecAssocParams  
  Description : This function Sets local and remote termination addresses,
                outbound or inbound SPI,  transport or tunnel mode values.
  Input       : tVpnPolicy *   - pointer to the VPN policy.
                u4PktDirection - VPN_INBOUND or VPN_OUTBOUND
  Output      : None. 
  Returns     : SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE INT4
Secv6VpnFillSecAssocParams (tVpnPolicy * pVpnPolicy, UINT4 u4PktDirection)
{

    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT4               u4SecAssocIndex = pVpnPolicy->u4VpnPolicyIndex;

    if (u4PktDirection == VPN_OUTBOUND)
    {
        u4SecAssocIndex = (pVpnPolicy->u4VpnPolicyIndex) + 1;
    }

    if (nmhTestv2Fsipv6SecAssocStatus
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhSetFsipv6SecAssocStatus ((INT4) u4SecAssocIndex, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (u4PktDirection == VPN_INBOUND)
    {
        if (Secv6VpnFillSecAssocAddrParams (u4SecAssocIndex,
                                            &(pVpnPolicy->LocalTunnTermAddr.
                                              Ipv6Addr),
                                            pVpnPolicy->uVpnKeyMode.
                                            IpsecManualKey.u4VpnAhInboundSpi,
                                            &(pVpnPolicy->RemoteTunnTermAddr.
                                              Ipv6Addr)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (Secv6VpnFillSecAssocAddrParams (u4SecAssocIndex,
                                            &(pVpnPolicy->RemoteTunnTermAddr.
                                              Ipv6Addr),
                                            pVpnPolicy->uVpnKeyMode.
                                            IpsecManualKey.u4VpnAhOutboundSpi,
                                            &(pVpnPolicy->LocalTunnTermAddr.
                                              Ipv6Addr)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    if (nmhTestv2Fsipv6SecAssocMode (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
                                     pVpnPolicy->u1VpnMode) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecAssocMode ((INT4) u4SecAssocIndex, pVpnPolicy->u1VpnMode);

    if (nmhTestv2Fsipv6SecAssocAntiReplay
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         VPN_POLICY_ANTI_REPLAY_STATUS (pVpnPolicy)) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecAssocAntiReplay ((INT4) u4SecAssocIndex,
                                    VPN_POLICY_ANTI_REPLAY_STATUS (pVpnPolicy));

    return SNMP_SUCCESS;
}

/****************************************************************************
   Function    :  Secv6VpnFillSecAssocAddrParams 
   Description :  This function sets Local and remote addresses and 
                  also packet direction values.
   Input       :  u4SecAssocIndex - VPNPolicy Index,
                  SecAssocDestAddr- Destination address
                  u4SecAssocSpi   - packet direction
                  SecAssocSrcAddr - Source Address
   Output      :  None.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE INT4
Secv6VpnFillSecAssocAddrParams (UINT4 u4SecAssocIndex,
                                tIp6Addr * SecAssocDestAddr,
                                UINT4 u4SecAssocSpi, tIp6Addr * SecAssocSrcAddr)
{

    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    tSecv6Assoc        *pSecAssocIf = NULL;
    UINT1               au1Secv6Addr[SEC_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE Ip6Addr;

    pSecAssocIf = Secv6AssocGetEntry (u4SecAssocIndex);
    if (pSecAssocIf == NULL)
    {
        nmhSetFsipv6SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&pSecAssocIf->SecAssocSrcAddr, SecAssocSrcAddr);

    Ip6AddrCopy ((tIp6Addr *) (VOID *) au1Secv6Addr, SecAssocDestAddr);
    Ip6Addr.pu1_OctetList = au1Secv6Addr;
    Ip6Addr.i4_Length = SEC_ADDR_LEN;

    if (nmhTestv2Fsipv6SecAssocDstAddr
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex, &Ip6Addr) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecAssocDstAddr ((INT4) u4SecAssocIndex, &Ip6Addr);

    if (nmhTestv2Fsipv6SecAssocSpi (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
                                    (INT4) u4SecAssocSpi) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAssocStatus ((INT4) u4SecAssocIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecAssocSpi ((INT4) u4SecAssocIndex, (INT4) u4SecAssocSpi);

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  Secv6VpnFillAuthSecAssocParams
  Description :  This function sets the values in the tSecv6Assoc table. 
  Input       :  tVpnPolicy *   - pointer to the VPN policy
                 u4PktDirection - VPN_INBOUND or VPN_OUTBOUND
  Output      :  None.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE INT4
Secv6VpnFillAuthSecAssocParams (tVpnPolicy * pVpnPolicy, UINT4 u4PktDirection)
{

    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT4               u4SecAssocIndex = pVpnPolicy->u4VpnPolicyIndex;

    if (u4PktDirection == VPN_OUTBOUND)
    {
        u4SecAssocIndex = (pVpnPolicy->u4VpnPolicyIndex) + 1;
    }

    /* If the SA entry already has ESP and if it is being set AH then
       make Encryption algorithm for the entry as 0 */
    if (nmhTestv2Fsipv6SecAssocProtocol
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         (UINT1) pVpnPolicy->u4VpnSecurityProtocol) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecAssocProtocol ((INT4) u4SecAssocIndex,
                                  (UINT1) pVpnPolicy->u4VpnSecurityProtocol);

    if (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnAuthAlgo != 0)
    {
        if (Secv6VpnFillAuthParams (u4SecAssocIndex,
                                    (UINT1) pVpnPolicy->u4VpnSecurityProtocol,
                                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                    u1VpnAuthAlgo,
                                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                    au1VpnAhKey) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    /* Fill if encryption params specified */
    if (pVpnPolicy->uVpnKeyMode.IpsecManualKey.u1VpnEncryptionAlgo != 0)
    {
        if (Secv6VpnFillEncrParams (u4SecAssocIndex,
                                    (UINT1) pVpnPolicy->u4VpnSecurityProtocol,
                                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                    u1VpnEncryptionAlgo,
                                    pVpnPolicy->uVpnKeyMode.IpsecManualKey.
                                    au2VpnEspKey) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  Secv6VpnFillAuthParams
  Description :  This function  sets  Values AH algorithm (HMAC-MD5 | 
                 HMAC-SHA1), AH key value based on VPN security protocol. 
  Input       :  u4SecAssocIndex    - VPN policy index.
                 u1SecAssocProtocol - VPN Security Protocol
                 u1SecAssocAhAlgo   - Authentication algorithm
                 *au1SecAssocAhKey  - Authentication key value.
  Output      :  None. 
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE INT4
Secv6VpnFillAuthParams (UINT4 u4SecAssocIndex, UINT1 u1SecAssocProtocol,
                        UINT1 u1SecAssocAhAlgo, UINT1 *au1SecAssocAhKey)
{

    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE AhKeyValue;
    UINT1               au1OutKey[SEC_HMAC_SHA_MAX_LEN + 1];
    UINT1               u1Len = 0;

    IPSEC_MEMSET (au1OutKey, 0, sizeof (au1OutKey));

    if (u1SecAssocProtocol == VPN_AH)
    {
        if (nmhTestv2Fsipv6SecAssocAhAlgo
            (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
             u1SecAssocAhAlgo) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        nmhSetFsipv6SecAssocAhAlgo ((INT4) u4SecAssocIndex, u1SecAssocAhAlgo);
    }
    else if ((u1SecAssocProtocol == VPN_ESP)
             && (u1SecAssocAhAlgo != VPN_NULLAHALGO))
    {
        if (nmhTestv2Fsipv6SecAssocAhAlgo (&u4SnmpErrorStatus,
                                           (INT4) u4SecAssocIndex,
                                           u1SecAssocAhAlgo) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsipv6SecAssocAhAlgo
            ((INT4) u4SecAssocIndex, u1SecAssocAhAlgo) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

    }

    if ((u1SecAssocAhAlgo != VPN_MD5) && (u1SecAssocAhAlgo != VPN_NULLAHALGO))
    {
        u1Len = (UINT1) STRLEN (au1SecAssocAhKey);
        IPSEC_MEMSET (au1OutKey, 0, sizeof (au1OutKey));
        if (Secv6ConstructKey (au1SecAssocAhKey, u1Len, au1OutKey)
            == SEC_FAILURE)
        {
            return SNMP_FAILURE;
        }
        AhKeyValue.i4_Length = (INT4) STRLEN (au1OutKey);
        AhKeyValue.pu1_OctetList = au1OutKey;

        if (nmhTestv2Fsipv6SecAssocAhKey (&u4SnmpErrorStatus,
                                          (INT4) u4SecAssocIndex,
                                          &AhKeyValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (nmhSetFsipv6SecAssocAhKey ((INT4) u4SecAssocIndex, &AhKeyValue)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  Secv6VpnFillEncrParams
  Description :  This function  sets  Values like ESP algorithm 
                 (DES|3-DES|AES128|192|256) and ESP key value (des/3des/aes)
                 based on VPN Security protocol.
  Input       :  u4SecAssocIndex    - VPN policy index.
                 u1SecAssocProtocol - VPN Security Protocol
                 u1SecAssocEspAlgo  - Encryption algorithm
                 au1EspKey - Encryption Key value.
  Output      :  None.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE INT4
Secv6VpnFillEncrParams (UINT4 u4SecAssocIndex, UINT1 u1SecAssocProtocol,
                        UINT1 u1SecAssocEspAlgo, UINT1 *au1EspKey)
{

    UINT4               EspKeyLen = 0;
    UINT4               EspKeyIndex = 0;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE EspKeyValue;
    UINT1               au1SecAssocEspKey[IPSEC_MANUAL_KEY_LEN];
    UINT1               au1SecAssocEspKey2[IPSEC_MANUAL_KEY_LEN];
    UINT1               au1SecAssocEspKey3[IPSEC_MANUAL_KEY_LEN];
    UINT1               au1OutKey[SEC_ESP_AES_KEY3_LEN + 1];
    UINT1               u1Len = 0;

    IPSEC_MEMSET (au1OutKey, 0, 33);

    if (u1SecAssocProtocol != SEC_ESP)
    {
        return SNMP_FAILURE;
    }

    MEMSET (au1SecAssocEspKey, 0, IPSEC_MANUAL_KEY_LEN);
    MEMSET (au1SecAssocEspKey2, 0, IPSEC_MANUAL_KEY_LEN);
    MEMSET (au1SecAssocEspKey3, 0, IPSEC_MANUAL_KEY_LEN);

    /* In IPSecv6 AES-128, AES-192, AES-256 are treated as single algo
     * Based on the key length the AES sub-type is identified */
    if ((u1SecAssocEspAlgo == VPN_AES_192) ||
        (u1SecAssocEspAlgo == VPN_AES_256))
    {
        u1SecAssocEspAlgo = VPN_AES_128;
    }

    /* Store the incoming ESP key into 3 seperate arrays of 16 each as 
     * the IPSEC low level takes the input in such a way. */
    if (u1SecAssocEspAlgo == VPN_3DES_CBC)
    {
        for (EspKeyIndex = 0; EspKeyIndex < ESP_SUB_KEY_MAX; EspKeyIndex++)
        {
            au1SecAssocEspKey[EspKeyIndex] = au1EspKey[EspKeyIndex];
        }

        EspKeyLen = ESP_START_SECOND_KEY;
        au1SecAssocEspKey[ESP_FIRST_KEY_TERMINATION] = '\0';

        for (EspKeyIndex = 0; EspKeyIndex < ESP_SUB_KEY_MAX; EspKeyIndex++)
        {
            au1SecAssocEspKey2[EspKeyIndex] = au1EspKey[EspKeyLen];
            EspKeyLen++;
        }

        au1SecAssocEspKey2[ESP_SECOND_KEY_TERMINATION] = '\0';
        EspKeyLen = ESP_START_THIRD_KEY;

        for (EspKeyIndex = 0; EspKeyIndex < ESP_SUB_KEY_MAX; EspKeyIndex++)
        {
            au1SecAssocEspKey3[EspKeyIndex] = au1EspKey[EspKeyLen];
            EspKeyLen++;
        }
        au1SecAssocEspKey3[EspKeyIndex] = '\0';
    }

    if (u1SecAssocEspAlgo != VPN_3DES_CBC)
    {
        STRNCPY (au1SecAssocEspKey, au1EspKey, (IPSEC_MANUAL_KEY_LEN - 1));
        au1SecAssocEspKey[IPSEC_MANUAL_KEY_LEN - 1] = '\0';
    }

    nmhSetFsipv6SecAssocProtocol ((INT4) u4SecAssocIndex, u1SecAssocProtocol);

    if (nmhTestv2Fsipv6SecAssocEspAlgo
        (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
         (INT4) u1SecAssocEspAlgo) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecAssocEspAlgo ((INT4) u4SecAssocIndex, u1SecAssocEspAlgo);

    if (u1SecAssocEspAlgo != SEC_NULLESPALGO)
    {
        u1Len = (UINT1) STRLEN (au1SecAssocEspKey);
        IPSEC_MEMSET (au1OutKey, 0, 33);

        if (Secv6ConstructKey (au1SecAssocEspKey, u1Len, au1OutKey)
            == SEC_FAILURE)
        {
            return SNMP_FAILURE;
        }

        EspKeyValue.i4_Length = (INT4) STRLEN (au1OutKey);
        EspKeyValue.pu1_OctetList = au1OutKey;

        if (nmhTestv2Fsipv6SecAssocEspKey
            (&u4SnmpErrorStatus, (INT4) u4SecAssocIndex,
             &EspKeyValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (nmhSetFsipv6SecAssocEspKey ((INT4) u4SecAssocIndex, &EspKeyValue) ==
            SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (u1SecAssocEspAlgo == SEC_3DES_CBC)
        {
            u1Len = (UINT1) STRLEN (au1SecAssocEspKey2);
            IPSEC_MEMSET (au1OutKey, 0, 33);

            if (Secv6ConstructKey (au1SecAssocEspKey2, u1Len, au1OutKey)
                == SEC_FAILURE)
            {
                return SNMP_FAILURE;
            }

            EspKeyValue.i4_Length = (INT4) STRLEN (au1OutKey);
            EspKeyValue.pu1_OctetList = au1OutKey;

            if (nmhTestv2Fsipv6SecAssocEspKey2 (&u4SnmpErrorStatus,
                                                (INT4) u4SecAssocIndex,
                                                &EspKeyValue) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (nmhSetFsipv6SecAssocEspKey2
                ((INT4) u4SecAssocIndex, &EspKeyValue) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            u1Len = (UINT1) STRLEN (au1SecAssocEspKey3);
            IPSEC_MEMSET (au1OutKey, 0, 33);

            if (Secv6ConstructKey (au1SecAssocEspKey3, u1Len, au1OutKey)
                == SEC_FAILURE)
            {
                return SNMP_FAILURE;
            }

            EspKeyValue.i4_Length = (INT4) STRLEN (au1OutKey);
            EspKeyValue.pu1_OctetList = au1OutKey;

            if (nmhTestv2Fsipv6SecAssocEspKey3 (&u4SnmpErrorStatus,
                                                (INT4) u4SecAssocIndex,
                                                &EspKeyValue) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (nmhSetFsipv6SecAssocEspKey3
                ((INT4) u4SecAssocIndex, &EspKeyValue) == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  Secv6VpnFillPolicyParams
  Description :  This function sets values like policy flag, policy mode
                 and ike version.
  Input       :  tVpnPolicy *   - pointer to the VPN policy.
                 u1PolicyMode   - VPN policy type used (ipsec,ike,xauth,psk)
                 u4PktDirection - VPN_INBOUND or VPN_OUTBOUND
  Output      :  None.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE INT4
Secv6VpnFillPolicyParams (tVpnPolicy * pVpnPolicy, UINT1 u1PolicyMode,
                          UINT4 u4PktDirection)
{
    INT4                i4Len = 0;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    tSNMP_OCTET_STRING_TYPE PolicySaBundle;
    UINT1               au1PolicySaBundle[VPN_SYS_MAX_NUM_TUNNELS];
    UINT4               u4PolicyIndex = pVpnPolicy->u4VpnPolicyIndex;
    INT4                i4PolicyFlag = (INT4) pVpnPolicy->u4VpnPolicyFlag;

    if (u4PktDirection == VPN_OUTBOUND)
    {
        u4PolicyIndex = (pVpnPolicy->u4VpnPolicyIndex) + 1;
    }

    if (nmhTestv2Fsipv6SecPolicyStatus
        (&u4SnmpErrorStatus, (INT4) u4PolicyIndex,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhSetFsipv6SecPolicyStatus ((INT4) u4PolicyIndex, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->u4VpnPolicyFlag == SEC_FILTER) ||
        (pVpnPolicy->u4VpnPolicyFlag == SEC_ALLOW))
    {
        i4PolicyFlag = SEC_BYPASS;
    }

    if (nmhTestv2Fsipv6SecPolicyFlag (&u4SnmpErrorStatus, (INT4) u4PolicyIndex,
                                      i4PolicyFlag) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecPolicyFlag ((INT4) u4PolicyIndex, i4PolicyFlag);

    if (nmhTestv2Fsipv6SecPolicyMode (&u4SnmpErrorStatus,
                                      (INT4) u4PolicyIndex, u1PolicyMode)
        == SNMP_FAILURE)
    {
        nmhSetFsipv6SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
        return SNMP_FAILURE;
    }

    nmhSetFsipv6SecPolicyMode ((INT4) u4PolicyIndex, u1PolicyMode);

    if (u1PolicyMode == VPN_MANUAL)
    {
        IPSEC_MEMSET (au1PolicySaBundle, 0, VPN_SYS_MAX_NUM_TUNNELS);
        i4Len = SPRINTF ((CHR1 *) au1PolicySaBundle, "%u", u4PolicyIndex);
        au1PolicySaBundle[i4Len] = '\0';
        PolicySaBundle.pu1_OctetList = au1PolicySaBundle;
        PolicySaBundle.i4_Length = i4Len;
        if (nmhSetFsipv6SecPolicySaBundle
            ((INT4) u4PolicyIndex, &PolicySaBundle) == SNMP_FAILURE)
        {
            nmhSetFsipv6SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
            return SNMP_FAILURE;
        }
    }

    if (Secv6SetIkeVersion (u4PolicyIndex, pVpnPolicy->u1VpnIkeVer)
        == SEC_FAILURE)
    {
        nmhSetFsipv6SecPolicyStatus ((INT4) u4PolicyIndex, DESTROY);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function    :  Secv6VpnFillAccessParams
  Description :  This function set values related like Source and destination 
                 addresses and their prefix lengths.
  Input       :  tVpnPolicy * - pointer to the VPN policy.
                 u4PktDirection - VPN_INBOUND or VPN_OUTBOUND
  Output      :  None.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE INT4
Secv6VpnFillAccessParams (tVpnPolicy * pVpnPolicy, UINT4 u4PktDirection)
{
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT4               u4AccessIndex = pVpnPolicy->u4VpnPolicyIndex;

    if (u4PktDirection == VPN_OUTBOUND)
    {
        u4AccessIndex = (pVpnPolicy->u4VpnPolicyIndex) + 1;
    }

    if (nmhTestv2Fsipv6SecAccessStatus
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsipv6SecAccessStatus ((INT4) u4AccessIndex, CREATE_AND_GO)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (u4PktDirection == VPN_OUTBOUND)
    {
        if (Secv6VpnFillAccessNetworkParams (u4AccessIndex,
                                             &(pVpnPolicy->LocalProtectNetwork.
                                               IpAddr.Ipv6Addr),
                                             pVpnPolicy->LocalProtectNetwork.
                                             u4AddrPrefixLen,
                                             &(pVpnPolicy->RemoteProtectNetwork.
                                               IpAddr.Ipv6Addr),
                                             pVpnPolicy->RemoteProtectNetwork.
                                             u4AddrPrefixLen) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (Secv6VpnFillAccessNetworkParams (u4AccessIndex,
                                             &(pVpnPolicy->RemoteProtectNetwork.
                                               IpAddr.Ipv6Addr),
                                             pVpnPolicy->RemoteProtectNetwork.
                                             u4AddrPrefixLen,
                                             &(pVpnPolicy->LocalProtectNetwork.
                                               IpAddr.Ipv6Addr),
                                             pVpnPolicy->LocalProtectNetwork.
                                             u4AddrPrefixLen) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
    Function    :  Secv6VpnFillAccessNetworkParams
    Description :  This function set values related to access list parameters
    Input       :  u4AccessIndex       -VPN Policy Index
                   pSrcAddress          -IPv6 Source Address 
                   u4SrcAddrPrefixLen  -Source Address Prefix Length
                   pDestAddress         -IPv6 destination address
                   u4DestAddrPrefixLen -Destination address Prefix Length
    Output      :  None.
    Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/

PRIVATE INT4
Secv6VpnFillAccessNetworkParams (UINT4 u4AccessIndex, tIp6Addr * pSrcAddress,
                                 UINT4 u4SrcAddrPrefixLen,
                                 tIp6Addr * pDestAddress,
                                 UINT4 u4DestAddrPrefixLen)
{
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT1               au1Secv6Addr[SEC_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE OctetStr;

    MEMCPY (au1Secv6Addr, pSrcAddress, sizeof (tIp6Addr));
    OctetStr.pu1_OctetList = au1Secv6Addr;
    OctetStr.i4_Length = SEC_ADDR_LEN;

    if (nmhTestv2Fsipv6SecSrcNet (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
                                  &OctetStr) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecSrcNet ((INT4) u4AccessIndex, &OctetStr);

    if (nmhTestv2Fsipv6SecSrcAddrPrefixLen
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         (INT4) u4SrcAddrPrefixLen) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecSrcAddrPrefixLen ((INT4) u4AccessIndex,
                                     (INT4) u4SrcAddrPrefixLen);

    MEMCPY (au1Secv6Addr, pDestAddress, sizeof (tIp6Addr));

    OctetStr.pu1_OctetList = au1Secv6Addr;
    OctetStr.i4_Length = SEC_ADDR_LEN;

    if (nmhTestv2Fsipv6SecDestNet (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
                                   &OctetStr) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecDestNet ((INT4) u4AccessIndex, &OctetStr);

    if (nmhTestv2Fsipv6SecDestAddrPrefixLen
        (&u4SnmpErrorStatus, (INT4) u4AccessIndex,
         (INT4) u4DestAddrPrefixLen) == SNMP_FAILURE)
    {
        nmhSetFsipv6SecAccessStatus ((INT4) u4AccessIndex, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SecDestAddrPrefixLen ((INT4) u4AccessIndex,
                                      (INT4) u4DestAddrPrefixLen);

    return SNMP_SUCCESS;
}

/****************************************************************************
   Function    :  Secv6VpnFillSelectorParams
   Description :  This function set values like interface id, IP protocol,
                  packet direction, policy flag , pointers to vpn policy entry 
                  and access list entry,
   Input       :  tVpnPolicy * - pointer to the VPN policy.
                  u4PktDirection - VPN_INBOUND or VPN_OUTBOUND
   Output      :  None.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/

PRIVATE INT4
Secv6VpnFillSelectorParams (tVpnPolicy * pVpnPolicy, UINT4 u4PktDirection)
{
    UINT1               au1Secv6Addr[SEC_ADDR_LEN];
    tSNMP_OCTET_STRING_TYPE OctetStr;
    UINT4               u4SnmpErrorStatus = SNMP_FAILURE;
    UINT4               u4Port = VPN_DEFAULT_PORT;
    UINT4               u4VpnPolicyEntry = pVpnPolicy->u4VpnPolicyIndex;
    UINT4               u4AccessIndex = pVpnPolicy->u4VpnPolicyIndex;
    UINT4               u4VpnPolicyFlag = 0;

    if (u4PktDirection == VPN_OUTBOUND)
    {
        u4VpnPolicyEntry = (pVpnPolicy->u4VpnPolicyIndex + 1);
        u4AccessIndex = (pVpnPolicy->u4VpnPolicyIndex + 1);
    }

    if (nmhTestv2Fsipv6SelStatus
        (&u4SnmpErrorStatus, (INT4) pVpnPolicy->u4IfIndex,
         (INT4) pVpnPolicy->u4VpnProtocol, (INT4) u4AccessIndex, (INT4) u4Port,
         (INT4) u4PktDirection, CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (nmhSetFsipv6SelStatus
        ((INT4) pVpnPolicy->u4IfIndex, (INT4) pVpnPolicy->u4VpnProtocol,
         (INT4) u4AccessIndex, (INT4) u4Port, (INT4) u4PktDirection,
         CREATE_AND_GO) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((pVpnPolicy->u4VpnPolicyFlag == SEC_APPLY) ||
        (pVpnPolicy->u4VpnPolicyFlag == SEC_BYPASS))
    {
        u4VpnPolicyFlag = SEC_ALLOW;
    }

    if (nmhTestv2Fsipv6SelFilterFlag (&u4SnmpErrorStatus,
                                      (INT4) pVpnPolicy->u4IfIndex,
                                      (INT4) pVpnPolicy->u4VpnProtocol,
                                      (INT4) u4AccessIndex, (INT4) u4Port,
                                      (INT4) u4PktDirection,
                                      (INT4) u4VpnPolicyFlag) == SNMP_FAILURE)
    {
        nmhSetFsipv6SelStatus ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) u4AccessIndex, (INT4) u4Port,
                               (INT4) u4PktDirection, DESTROY);
        return SNMP_FAILURE;
    }
    nmhSetFsipv6SelFilterFlag ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) u4AccessIndex, (INT4) u4Port,
                               (INT4) u4PktDirection, (INT4) u4VpnPolicyFlag);

    if (nmhTestv2Fsipv6SelPolicyIndex (&u4SnmpErrorStatus,
                                       (INT4) pVpnPolicy->u4IfIndex,
                                       (INT4) pVpnPolicy->u4VpnProtocol,
                                       (INT4) u4AccessIndex, (INT4) u4Port,
                                       (INT4) u4PktDirection,
                                       (INT4) u4VpnPolicyEntry) == SNMP_FAILURE)
    {
        nmhSetFsipv6SelStatus ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) u4AccessIndex, (INT4) u4Port,
                               (INT4) u4PktDirection, DESTROY);
        return SNMP_FAILURE;
    }
    if (nmhSetFsipv6SelPolicyIndex ((INT4) pVpnPolicy->u4IfIndex,
                                    (INT4) pVpnPolicy->u4VpnProtocol,
                                    (INT4) u4AccessIndex, (INT4) u4Port,
                                    (INT4) u4PktDirection,
                                    (INT4) u4VpnPolicyEntry) == SNMP_FAILURE)
    {
        nmhSetFsipv6SelStatus ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) u4AccessIndex, (INT4) u4Port,
                               (INT4) u4PktDirection, DESTROY);
        return SNMP_FAILURE;
    }

    MEMCPY (au1Secv6Addr, &(pVpnPolicy->LocalTunnTermAddr.uIpAddr.Ip6Addr),
            sizeof (tIp6Addr));

    OctetStr.pu1_OctetList = au1Secv6Addr;
    OctetStr.i4_Length = SEC_ADDR_LEN;

    if (nmhTestv2Fsipv6SelIfIpAddress (&u4SnmpErrorStatus,
                                       (INT4) pVpnPolicy->u4IfIndex,
                                       (INT4) pVpnPolicy->u4VpnProtocol,
                                       (INT4) u4AccessIndex, (INT4) u4Port,
                                       (INT4) u4PktDirection,
                                       &OctetStr) == SNMP_FAILURE)
    {
        nmhSetFsipv6SelStatus ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) u4AccessIndex, (INT4) u4Port,
                               (INT4) u4PktDirection, DESTROY);
        return SNMP_FAILURE;
    }

    if (nmhSetFsipv6SelIfIpAddress ((INT4) pVpnPolicy->u4IfIndex,
                                    (INT4) pVpnPolicy->u4VpnProtocol,
                                    (INT4) u4AccessIndex, (INT4) u4Port,
                                    (INT4) u4PktDirection,
                                    &OctetStr) == SNMP_FAILURE)
    {
        nmhSetFsipv6SelStatus ((INT4) pVpnPolicy->u4IfIndex,
                               (INT4) pVpnPolicy->u4VpnProtocol,
                               (INT4) u4AccessIndex, (INT4) u4Port,
                               (INT4) u4PktDirection, DESTROY);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

#ifdef IKE_WANTED
/****************************************************************************
   Function    :  Secv6DeleteAssoc
   Description :  This function deletes the created SA entry from the SAD
   Input       :  SrcAddr   - Source Address of the Packet
                  DestAddr  - Destination Address of the Packet
                  u4IfIndex - Interface Index.
                  u2LocPort - Local Port.
                  u2remPort - RemotePort.
                  u2Protocol- Protocol present in the Packet(tcp,udp,icmp,any)
   Output      :  None.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE VOID
Secv6DeleteAssoc (tIp6Addr SrcAddr, tIp6Addr DestAddr, UINT4 u4IfIndex,
                  UINT4 u4Protocol, UINT2 u2LocPort, UINT2 u2RemPort)
{
    tSecv6Policy       *pSecv6Policy = NULL;
    tSecv6Selector     *pSelEntry = NULL;
    UINT4               u4AccessIndex = 0;
    UINT4               u1PktDirection = SEC_OUTBOUND;

    /* At present unused */
    UNUSED_PARAM (u2RemPort);
    UNUSED_PARAM (u2LocPort);

    u4AccessIndex = Secv6GetAccessIndex (SrcAddr, DestAddr);

    if (u4AccessIndex != SEC_FAILURE)
    {
        pSelEntry = Secv6SelGetFormattedEntry (u4IfIndex, u4Protocol,
                                               u4AccessIndex, u2LocPort,
                                               (INT4) u1PktDirection);
        if (pSelEntry != NULL)
        {
            pSecv6Policy = pSelEntry->pPolicyEntry;
            if (pSecv6Policy->paSaEntry[0] != NULL)
            {
                Secv6IntimateIkeDeletionOfSa (pSecv6Policy,
                                              pSecv6Policy->paSaEntry[0]->
                                              u4SecAssocIndex);
            }
        }
    }
}
#endif

/****************************************************************************
   Function    :  Secv6SetIkeVersion
   Description :  This function is used to set the IKE version.
   Input       :  u4PolicyIndex - Policy Index.
                  u1IkeVersion  - IKE Version.
   Output      :  None. 
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PRIVATE INT4
Secv6SetIkeVersion (UINT4 u4PolicyIndex, UINT1 u1IkeVersion)
{
    tSecv6Policy       *pSecPolicyIf = NULL;

    pSecPolicyIf = Secv6PolicyGetEntry (u4PolicyIndex);
    if (pSecPolicyIf == NULL)
    {
        return (SEC_FAILURE);
    }
    pSecPolicyIf->u1IkeVersion = u1IkeVersion;
    return (SEC_SUCCESS);
}

/****************************************************************************
   Function    :  Secv6SetAccessListInfo
   Description :  This function  set the port and protocol information of 
                  local end remote Networks.
   Input       :  u4AccessListIndex - VPN policy index.
                  u2LocalStartPort  - Local Start Port
                  u2LocalEndPort    - Local End Port
                  u2RemoteStartPort - Remote Start Port
                  u2RemoteEndPort   - Remote End Port
                  u4Protocol - IP Protocol
   Output      :  None.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

PRIVATE INT4
Secv6SetAccessListInfo (UINT4 u4AccessListIndex, UINT2 u2LocalStartPort,
                        UINT2 u2LocalEndPort, UINT2 u2RemoteStartPort,
                        UINT2 u2RemoteEndPort, UINT4 u4Protocol)
{
    tSecv6Access       *pSecAccIf = NULL;

    /* Get an entry from the outaccess list */
    pSecAccIf = Secv6AccessGetEntry (u4AccessListIndex);
    if (pSecAccIf != NULL)
    {
        pSecAccIf->u4Protocol = u4Protocol;
        pSecAccIf->u2LocalStartPort = u2LocalStartPort;
        pSecAccIf->u2LocalEndPort = u2LocalEndPort;
        pSecAccIf->u2RemoteStartPort = u2RemoteStartPort;
        pSecAccIf->u2RemoteEndPort = u2RemoteEndPort;
        return SEC_SUCCESS;
    }
    return SEC_FAILURE;
}

/****************************************************************************
   Function    :  Secv6FillSelectorParamsFail
   Description :  This function destroys the SAD, Policy and access tables
                  when set operation on selector params failed.
   Input       :  u4PolicyIndex   - Policy Index.
                  u4VpnPolicyType - Policy type.
   Output      :  None.
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
PRIVATE VOID
Secv6FillSelectorParamsFail (INT4 i4PolicyIndex, UINT4 u4PolicyType)
{
    nmhSetFsipv6SecPolicyStatus (i4PolicyIndex, DESTROY);
    nmhSetFsipv6SecPolicyStatus ((i4PolicyIndex + 1), DESTROY);

    nmhSetFsipv6SecAccessStatus (i4PolicyIndex, DESTROY);
    nmhSetFsipv6SecAccessStatus ((i4PolicyIndex + 1), DESTROY);

    if (u4PolicyType == VPN_IPSEC_MANUAL)
    {
        nmhSetFsipv6SecAssocStatus (i4PolicyIndex, DESTROY);
        nmhSetFsipv6SecAssocStatus ((i4PolicyIndex + 1), DESTROY);
    }
}

/***************************************************************************
  Function Name   : Secv6VpnGetIpsecSaEntryCount                              
  Description     : This function returns the total active ipv6 SAs      
  Input(s)        : u4IPSecSAsActive :Refers to the SA count             
  Output(s)       : None                                                 
  Returns         : None                                                 
**************************************************************************/

VOID
Secv6VpnGetIpsecSaEntryCount (UINT4 *u4IPSecSAsActive)
{
    SECv6_TAKE_SEM ();

    *u4IPSecSAsActive = TMO_SLL_Count (&Secv6AssocList);

    SECv6_GIVE_SEM ();
}
#endif /* __SECV6VPN_C__ */
