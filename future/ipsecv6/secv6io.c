/**********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secv6io.c,v 1.9 2014/01/25 13:54:47 siva Exp $
 *
 * Description: This has functions for SecIO SubModule
 *
 ***********************************************************************/

#include "secv6com.h"

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6InProcess
 *  Description   : This function gets the incomming packet and
 *                  calls SecInMain based on Policy and selector. 
 *  Input(s)      : pBuf - Buffer contains IP Secured packet.
 *                  pIf6 - Buffer contains interface info. 
 *  Output(s)     : pBuf - Buffer contains IP packet.
 *  Global(s)     : None.
 *  Return Values : SEC_SUCCESS or SEC_FAILURE or SEC_IKE
 * ---------------------------------------------------------------
 */

INT1
Secv6InProcess (tBufChainHeader * pBuf, tIp6If * pIf6, UINT1 *pNhr,
                UINT4 *pu4ProcessedLen)
{
    INT1                i1Status;
    UINT2               u2PortNumber = SEC_ANY_PORT;
    INT4                i4Type;
    UINT4               u4Count = 0, u4Counter, u4Time, u4SPI = 0;
    UINT4               pu4SaIndies[SEC_MAX_BUNDLE_SA];
    UINT4               pu4RsvSaIndies[SEC_MAX_BUNDLE_SA];
    UINT4               u4Interface = pIf6->u4Index;
    UINT1               u1PktFlag;
    tSecv6Assoc        *pSaEntry = NULL;
    tIp6Hdr             Ip6Hdr;
    UINT1               u1Nhr = *pNhr;
    UINT2               u2Len;
    UINT4               u4Len;
    UINT4               u4IfIndex;
    UINT2               u2Offset;
    UINT4               u4Index;
    tSecv6Selector     *pSelEntry = NULL;
    UINT1               u1Type;
#ifdef IKE_WANTED
    UINT2               u2DstPort;
    UINT2               u2IPHdrLen;
#endif

    if (pBuf == NULL)
    {
        SECv6_TRC (SECv6_BUFFER, "Secv6InProcess :Input Buffer is NULL\n");
        return SEC_FAILURE;
    }

    /* Read IPv6 Header form the Packet */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ip6Hdr,
                             SEC_IPV6_HEADER_HEAD_OFFSET,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_BUFFER,
                   "Secv6InProcess : Unable to read Ipv6 header from Packet\n");
        return SEC_FAILURE;
    }

    if (Ip6Hdr.u1Nh == SEC_ICMPv6)
    {
        if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u1Type, 40, 1) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6InProcess: Unable to read port Number from Packet\n");
            return SEC_FAILURE;
        }
        switch (u1Type)
        {
            case SEC_ND6_ROUTER_SOLICITATION:
            case SEC_ND6_ROUTER_ADVERTISEMENT:
            case SEC_ND6_NEIGHBOR_SOLICITATION:
            case SEC_ND6_NEIGHBOR_ADVERTISEMENT:
            case SEC_ND6_REDIRECT:
                return (SEC_SUCCESS);
                break;
            default:
                break;
        }
    }

    /* This WHILE loop will decode the secured packet untill it is
     * not to itself */
    u2Len = (UINT2) (*pu4ProcessedLen);
    do
    {
        /* Read SPI value */
        switch (u1Nhr)
        {
            case SEC_AH:
                if (gSecv6Status == SEC_DISABLE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "IPSec-Disabled\n");
                    return SEC_FAILURE;
                }
                if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u4SPI,
                                         (UINT4) (u2Len + 4),
                                         SEC_SPI_LEN) == BUF_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH,
                               "Secv6InProcess : Unable to read SPI from AH Pakcet\n");
                    return SEC_FAILURE;
                }
                break;
            case SEC_ESP:
                if (gSecv6Status == SEC_DISABLE)
                {
                    SECv6_TRC (SECv6_DATA_PATH, "IPSec-Disabled\n");
                    return SEC_FAILURE;
                }
                if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u4SPI,
                                         u2Len, SEC_SPI_LEN) == BUF_FAILURE)
                {
                    SECv6_TRC (SECv6_DATA_PATH,
                               "Secv6InProcess : Unable to read SPI from ESP Pakcet\n");
                    return SEC_FAILURE;
                }
                break;

#ifdef IKE_WANTED
            case SEC_UDP:

                u2Offset = (UINT2) (IPSEC_BUF_READ_OFFSET (pBuf));
                IPSEC_BUF_READ_OFFSET (pBuf) = 0;

                if (Ip6Secv6GetHlProtocol (pBuf, &u1Nhr, &u2IPHdrLen)
                    != IP6_SUCCESS)
                {
                    SECv6_TRC (SECv6_DATA_PATH,
                               "Secv6InProcess : Unable to read Nh from Packet \n");
                }

                IPSEC_BUF_READ_OFFSET (pBuf) = u2Offset;

                if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u2DstPort,
                                         u2IPHdrLen,
                                         SEC_PORT_NUMBER_LEN) == BUF_FAILURE)
                {
                    SECv6_TRC (SECv6_BUFFER, "Secv6InProcess : Unable to"
                               "read Port Number from Packet \n");
                    SECv6_GIVE_SEM ();
                    return SEC_FAILURE;
                }

                u2DstPort = (UINT2) (OSIX_NTOHS (u2DstPort));

                if ((u2DstPort == IKE_UDP_PORT) ||
                    (u2DstPort == IKE_NATT_UDP_PORT))
                {
                    SECv6_TRC (SECv6_DATA_PATH,
                               "Traffic  matched with IKE : Bypassed\n");
                    return SEC_IKE;
                }
                break;
#endif
            default:
                SECv6_TAKE_SEM ();
                u4Index = Secv6GetAccessIndex (Ip6Hdr.srcAddr, Ip6Hdr.dstAddr);
                SECv6_GIVE_SEM ();
                if (u4Index != SEC_FAILURE)
                {
                    SECv6_TAKE_SEM ();
                    pSelEntry =
                        Secv6SelGetFormattedEntry (u4Interface, u1Nhr, u4Index,
                                                   u2PortNumber, SEC_INBOUND);
                    SECv6_GIVE_SEM ();
                    if (pSelEntry == NULL)
                    {
                        return SEC_SUCCESS;
                    }
                    if (pSelEntry->pPolicyEntry->u1PolicyFlag == SEC_APPLY)
                    {
                        SECv6_TRC1 (SECv6_DATA_PATH,
                                    "Secv6InProcess : Not a IPSec Protocol Packet (%d)\n",
                                    u1Nhr);
                        return SEC_FAILURE;
                    }
                }
                return SEC_SUCCESS;
        }

        SECv6_TAKE_SEM ();

        /* Find SA Entry Pointer */
        pSaEntry = Secv6AssocInLookUp (IPSEC_NTOHL (u4SPI),
                                       Ip6Hdr.dstAddr, u1Nhr);
        if (pSaEntry == NULL)
        {
#ifdef IKE_WANTED
            /* If Ike is Enabled intimate IKE of the Absence of SA for 
               Decoding the packet */
            Secv6IntimateIkeInvalidSpi (IPSEC_NTOHL (u4SPI),
                                        Ip6Hdr.dstAddr, Ip6Hdr.srcAddr,
                                        Ip6Hdr.u1Nh);
#endif
            SECv6_TRC (SECv6_DATA_PATH, "Secv6InProcess : In Look Up Fails\n");
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        }

        /* Copy the SA Entry Indices to a local Array to 
         * Verify the Packet with Selector and Policy in
         * SecAssocVerify */

#ifdef IKE_WANTED
        else if (Secv6CheckNoOfInBoundBytesSecured (pSaEntry, pBuf) ==
                 SEC_BYTES_SECURED_EXCEEDED)
        {
            Secv6DeleteSa (pSaEntry->u4SecAssocIndex);
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6HandleIncomingPkt: No Of Bytes Decoded Exceeded "
                       "ConfiguredLifeTimeInBytes\n");
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        }
#endif
        pu4SaIndies[u4Count] = pSaEntry->u4SecAssocIndex;
        u4Count++;
        u4Len = (UINT4) u2Len;
        if (Secv6InMain (pBuf, pSaEntry, &u4Len) == NULL)
        {
            SECv6_TRC (SECv6_DATA_PATH, "Secv6InProcess : SecInMain Fails\n");
            /* AH/ESP Intruders Add */
            IPSEC_OSIX_GETSYSTIME (&u4Time);
            Secv6UpdateAhEspStats (u4Interface, pSaEntry->u1SecAssocProtocol,
                                   SEC_INBOUND, SEC_FILTER);
            Secv6UpdateAhEspIntruStats (u4Interface, Ip6Hdr.srcAddr,
                                        Ip6Hdr.dstAddr, Ip6Hdr.u1Nh, u4Time);
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        }

        /* ESP/AH Stat Add */
        Secv6UpdateAhEspStats (u4Interface, pSaEntry->u1SecAssocProtocol,
                               SEC_INBOUND, SEC_ALLOW);
        /* Read IPv6 Header form the Packet */

        if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ip6Hdr,
                                 SEC_IPV6_HEADER_HEAD_OFFSET,
                                 SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_BUFFER,
                       "Secv6InProcess : Unable to read IPv6 header from the Packet "
                       "after Decode \n ");
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        }
        i4Type = Ip6AddrType (&(Ip6Hdr.dstAddr));
        if ((i4Type == IPSEC_ADDR_UNICAST) || (i4Type == ADDR6_LLOCAL))
        {
            if (Ip6Secv6IsOurAddr (&(Ip6Hdr.dstAddr), &(u4IfIndex))
                == IP6_SUCCESS)
                u1PktFlag = SEC_MY_PACKET;

            else

                u1PktFlag = SEC_NOT_MY_PACKET;
        }
        else

            u1PktFlag = SEC_NOT_MY_PACKET;

        u2Offset = (UINT2) (IPSEC_BUF_READ_OFFSET (pBuf));
        IPSEC_BUF_READ_OFFSET (pBuf) = 0;
        if (Ip6Secv6GetHlProtocol (pBuf, &u1Nhr, &u2Len) != IP6_SUCCESS)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6InProcess : Unable to read Nh from Packet \n");
        }
        IPSEC_BUF_READ_OFFSET (pBuf) = u2Offset;
        SECv6_GIVE_SEM ();
    }
    while ((u1Nhr == SEC_AH
            || u1Nhr == SEC_ESP) && (u1PktFlag == SEC_MY_PACKET));

    SECv6_TAKE_SEM ();
    /* Reorder SA Entries in SA Bundle */
    for (u4Counter = 0; u4Counter < u4Count; u4Counter++)
        pu4RsvSaIndies[u4Counter] = pu4SaIndies[u4Count - (u4Counter + 1)];
    pu4RsvSaIndies[u4Count] = 0;

    if (u1Nhr == SEC_TCP || u1Nhr == SEC_UDP)
    {
        if (IPSEC_COPY_FROM_BUF
            (pBuf, (UINT1 *) &u2PortNumber,
             u2Len, SEC_PORT_NUMBER_LEN) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_BUFFER,
                       "Secv6InProcess : Unable to read Port Number from Packet \n");
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        }
        u2PortNumber = (UINT2) (OSIX_NTOHS (u2PortNumber));
    }

    /* Verify Packet with Selector and Policy */

    i1Status =
        (INT1) (Secv6AssocVerify (Ip6Hdr.srcAddr, Ip6Hdr.dstAddr,
                                  u1Nhr, u4Interface, pu4RsvSaIndies,
                                  (UINT4) u2PortNumber));

    /* General Stat Add */
    Secv6UpdateIfStats (u4Interface, SEC_INBOUND, (UINT1) i1Status);
    SECv6_PKT_DUMP (SECv6_DUMP, pBuf, SEC_IPV6_HEADER_SIZE, "InBound");
    switch (i1Status)
    {
        case SEC_BYPASS:
            SECv6_TRC (SECv6_DATA_PATH, "Secv6InProcess : Packet Bypass \n");
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        case SEC_FILTER:
            SECv6_TRC (SECv6_DATA_PATH, "Secv6InProcess : Packet Filter \n");
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        case SEC_ALLOW:
            SECv6_TRC (SECv6_DATA_PATH, "Secv6InProcess : Packet Allow \n");
            break;
        case SEC_APPLY:
            SECv6_TRC (SECv6_DATA_PATH, "Secv6InProcess : Packet Apply \n");
            break;
        default:
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6InProcess : Association Verify Fails \n");
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
    }
    *pNhr = Ip6Hdr.u1Nh;
    if (u2Len != (*pu4ProcessedLen))
        *pu4ProcessedLen = SEC_IPV6_HEADER_SIZE;

    SECv6_GIVE_SEM ();
    return SEC_SUCCESS;
}

/*
 * ---------------------------------------------------------------
 *  Function Name : Secv6OutProcess
 *  Description   : This function gets the outgoing packet and
 *                  calls SecOutMain based on Policy and selector.
 *  Input(s)      : pBuf - Buffer contains IP packet.
 *                  pIf6 - Buffer contains interface info.
 *  Output(s)     : pBuf - Buffer contains IP Secured packet.
 *  Global(s)     : None.
 *  Return Values : SEC_SUCCESS or SEC_FAILURE
 * ---------------------------------------------------------------
 */

INT1
Secv6OutProcess (tBufChainHeader * pBuf, tIp6If * pIf6, UINT4 *pu4Len)
{
    INT4                i4Count = 0;
    UINT4               u4Counter = 0;
    UINT2               u2PortNumber = SEC_ANY_PORT;
    UINT4               u4Interface = pIf6->u4Index;
    tSecv6Policy       *pSecv6Policy = NULL;
    tIp6Hdr             Ip6Hdr;
    UINT1               u1OptLen = 0;
    UINT1               u1NxtHdr = 0;
    UINT1               u1Type;
    if (gSecv6Status == SEC_DISABLE)
    {
        SECv6_TRC (SECv6_DATA_PATH,
                   "Secv6OutProcess : IPSec is in Disable mode \n");
        return SEC_SUCCESS;
    }

    if (pBuf == NULL)
    {
        SECv6_TRC (SECv6_BUFFER, "Secv6OutProcess : Input Buffer is NULL \n");
        return SEC_FAILURE;
    }

    /* Read Header form the Packet */
    if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &Ip6Hdr,
                             SEC_IPV6_HEADER_HEAD_OFFSET,
                             SEC_IPV6_HEADER_SIZE) == BUF_FAILURE)
    {
        SECv6_TRC (SECv6_BUFFER,
                   "Secv6OutProcess : Unable to read header from packet \n");
        return SEC_FAILURE;
    }

    u1NxtHdr = Ip6Hdr.u1Nh;
    /* To be modified based on Extn Headers */
    if (Ip6Hdr.u1Nh == SEC_TCP || Ip6Hdr.u1Nh == SEC_UDP)
    {
        if (IPSEC_COPY_FROM_BUF
            (pBuf, (UINT1 *) &u2PortNumber,
             SEC_IPV6_PORT_NUMBER_OFFSET, SEC_PORT_NUMBER_LEN) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6OutProcess : Unable to read Port Number from Packet \n");
            return SEC_FAILURE;
        }
        u2PortNumber = (UINT2) (OSIX_NTOHS (u2PortNumber));
    }
    else if (Ip6Hdr.u1Nh != SEC_ICMPv6)
    {
        if (Secv6GetHLProtocol
            (pBuf, &Ip6Hdr, Ip6Hdr.u1Nh, &u1NxtHdr) != SEC_SUCCESS)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6OutProcess : Unable to get option length from Packet \n");
            return SEC_FAILURE;
        }

        if (u1NxtHdr == SEC_TCP || u1NxtHdr == SEC_UDP)
        {
            if (IPSEC_COPY_FROM_BUF
                (pBuf, (UINT1 *) &u2PortNumber,
                 (UINT4) (SEC_IPV6_PORT_NUMBER_OFFSET + u1OptLen),
                 SEC_PORT_NUMBER_LEN) == BUF_FAILURE)
            {
                SECv6_TRC (SECv6_DATA_PATH,
                           "Secv6OutProcess : Unable to read Port Number from Packet \n");
                return SEC_FAILURE;
            }
            u2PortNumber = (UINT2) (OSIX_NTOHS (u2PortNumber));

        }
    }

    if (Ip6Hdr.u1Nh == SEC_ICMPv6)
    {
        if (IPSEC_COPY_FROM_BUF (pBuf, (UINT1 *) &u1Type, 40, 1) == BUF_FAILURE)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6OutProcess: Unable to read port Number from Packet\n");
            return SEC_FAILURE;
        }
        switch (u1Type)
        {
            case SEC_ND6_ROUTER_SOLICITATION:
            case SEC_ND6_ROUTER_ADVERTISEMENT:
            case SEC_ND6_NEIGHBOR_SOLICITATION:
            case SEC_ND6_NEIGHBOR_ADVERTISEMENT:
            case SEC_ND6_REDIRECT:
                return (SEC_SUCCESS);
                break;
            default:
                break;
        }
    }
    SECv6_TAKE_SEM ();

    /* Get Association Entry(Bundle) from Data base */
    i4Count =
        Secv6AssocOutLookUp (Ip6Hdr.srcAddr, Ip6Hdr.dstAddr,
                             u1NxtHdr, u4Interface, &pSecv6Policy, u2PortNumber,
                             pBuf);
    if (i4Count == SEC_FAILURE)
    {
        SECv6_TRC (SECv6_DATA_PATH, "Secv6OutProcess : Packet Bypassed \n");
        Secv6UpdateIfStats (u4Interface, SEC_OUTBOUND, SEC_BYPASS);
        SECv6_GIVE_SEM ();
        return SEC_SUCCESS;
    }

    if (i4Count == SEC_REJECT)
    {
        SECv6_TRC (SECv6_DATA_PATH, "Secv6OutProcess : Packet Rejected \n");
        Secv6UpdateIfStats (u4Interface, SEC_OUTBOUND, SEC_FILTER);
        SECv6_GIVE_SEM ();
        return SEC_FAILURE;
    }
    /* Apply Security to the packet based on SA Entry */
    while ((i4Count > 0) && (u4Counter < SEC_MAX_BUNDLE_SA))
    {
        if (Secv6OutMain
            (pBuf, pSecv6Policy->paSaEntry[u4Counter],
             (UINT4) u4Interface) == NULL)
        {
            SECv6_TRC (SECv6_DATA_PATH,
                       "Secv6OutProcess : SecOutMain Fails \n");
            SECv6_GIVE_SEM ();
            return SEC_FAILURE;
        }
        Secv6UpdateIfStats (u4Interface, SEC_OUTBOUND, SEC_APPLY);
        Secv6UpdateAhEspStats (u4Interface,
                               pSecv6Policy->paSaEntry[u4Counter]->
                               u1SecAssocProtocol, SEC_OUTBOUND, SEC_ALLOW);
        u4Counter++;
        i4Count--;
    }
    SECv6_PKT_DUMP (SECv6_DUMP, pBuf, SEC_IPV6_HEADER_SIZE, "OutBound");
    *pu4Len = IPSEC_BUF_GetValidBytes (pBuf);
    SECv6_GIVE_SEM ();
    return SEC_SUCCESS;
}
