
TOTAL_OPNS = $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)

############################################################################
#                         Directories                                      #
############################################################################

PBB_BASE_DIR = ${BASE_DIR}/bbbridge/pbb
PBB_SRC_DIR  = ${PBB_BASE_DIR}/src
PBB_INC_DIR  = ${PBB_BASE_DIR}/inc
PBB_OBJ_DIR  = ${PBB_BASE_DIR}/obj
############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${PBB_INC_DIR} -I${COMMON_INC_DIR} -I${VLAN_INC_DIR} -I${SNMP_DIR} -I${L2IWF_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################
