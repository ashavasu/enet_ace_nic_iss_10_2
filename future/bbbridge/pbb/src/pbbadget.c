#include "pbbinc.h"

/* -------------------------------------------------------------
 *
 * Function: PbbNpSyncProcessSyncMsg
 *
 * -------------------------------------------------------------
 */
PUBLIC VOID
PbbNpSyncProcessSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    INT4                NpApiId = 0;
    unNpSync            NpSync;

    MEMSET (&NpSync, 0, sizeof (unNpSync));

    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpApiId);

    switch (NpApiId)
    {

        case NPSYNC_FS_MI_PBB_NP_INIT_HW:
            break;

        case NPSYNC_FS_MI_PBB_NP_DE_INIT_HW:
            break;

        case NPSYNC_FS_MI_PBB_HW_SET_VIP_ATTRIBUTES:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortIdx.u4VipIfIndex);
            break;

        case NPSYNC_FS_MI_PBB_HW_DEL_VIP_ATTRIBUTES:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortIdx.u4VipIfIndex);
            break;

        case NPSYNC_FS_MI_PBB_HW_SET_VIP_PIP_MAP:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortIdx.u4VipIfIndex);
            break;

        case NPSYNC_FS_MI_PBB_HW_DEL_VIP_PIP_MAP:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortIdx.u4VipIfIndex);
            break;

        case NPSYNC_FS_MI_PBB_HW_SET_BACKBONE_SERVICE_INST_ENTRY:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.IsidIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.IsidIdx.u4CbpIfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.IsidIdx.u4BSid);
            break;

        case NPSYNC_FS_MI_PBB_HW_DEL_BACKBONE_SERVICE_INST_ENTRY:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.IsidIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.IsidIdx.u4CbpIfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.IsidIdx.u4BSid);
            break;

        case NPSYNC_FS_MI_BRG_HW_CREATE_CONTROL_PKT_FILTER:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.ContextIdx.u4ContextId);
            break;

        case NPSYNC_FS_MI_BRG_HW_DELETE_CONTROL_PKT_FILTER:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.ContextIdx.u4ContextId);
            break;

        case NPSYNC_FS_MI_PBB_HW_SET_ALL_TO_ONE_BUNDLING_SERVICE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbHwSetAllToOneBundlingService.
                                  u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbHwSetAllToOneBundlingService.
                                  u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbHwSetAllToOneBundlingService.
                                  u4Isid);
            break;

        case NPSYNC_FS_MI_PBB_HW_DEL_ALL_TO_ONE_BUNDLING_SERVICE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbHwDelAllToOneBundlingService.
                                  u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbHwDelAllToOneBundlingService.
                                  u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbHwDelAllToOneBundlingService.
                                  u4Isid);
            break;

        default:
            break;

    }                            /* switch */

    PbbHwAuditCreateOrFlushBuffer (&NpSync, NpApiId, 0);

    return;
}

/* -------------------------------------------------------------
 *
 * Function: PbbEventSyncProcessSyncMsg
 *
 * -------------------------------------------------------------
 */

/* Manually written GET for the PCP VLAN NPAPI called in PBB */
VOID
PbbEventSyncProcessSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    unNpSync            NpSync;
    INT4                EventId = 0;

    MEMSET (&NpSync, 0, sizeof (unNpSync));

    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, EventId);

    switch (EventId)
    {
        case EVTSYNC_FS_MI_PBB_HW_SET_PORT_USE_DEI:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4IfIndex);
            break;

        case EVTSYNC_FS_MI_PBB_HW_SET_PORT_REQ_DROP_ENCODING:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4IfIndex);
            break;

        case EVTSYNC_FS_MI_PBB_HW_SET_PORT_PCP_SELECTION:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4IfIndex);
            break;

        case EVTSYNC_FS_MI_PBB_HW_SET_PCP_ENCOD_TBL:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4IfIndex);
            break;

        case EVTSYNC_FS_MI_PBB_HW_SET_PCP_DECOD_TBL:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4IfIndex);
            break;

        case EVTSYNC_FS_MI_PBB_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4IfIndex);
            break;

        case EVTSYNC_FS_MI_PBB_DELETE_PORT:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpSync.PortIdx.u4IfIndex);
            break;

        case EVTSYNC_FS_MI_PBB_SET_GLOBAL_OUI:
            NPSYNC_RM_GET_N_BYTE (pMsg, NpSync.PbbGlbOui.au1OUI, pu2OffSet,
                                  PBB_OUI_LENGTH);
            break;

        case EVTSYNC_FS_MI_PBB_COPY_PORT_PROPERTIES:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortProperty.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortProperty.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortProperty.u4DstPort);
            break;

        case EVTSYNC_FS_MI_PBB_REMOVE_PORT_PROPERTIES:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortProperty.u4ContextId);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortProperty.u4IfIndex);
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.PbbPortProperty.u4DstPort);
            break;

        default:
            break;
    }

    PbbHwAuditCreateOrFlushBuffer (&NpSync, 0, EventId);

}
