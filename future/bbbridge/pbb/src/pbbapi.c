/*******************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbapi.c,v 1.25 2014/02/07 13:28:05 siva Exp $
 *
 * Description: This file contains PBB routines used by other modules.
 *
 *******************************************************************/

#include "pbbinc.h"
#include "brgnp.h"

#ifndef __PBBAPI_C__
#define __PBBAPI_C__

tPbbGlobData        gPbbGlobData;
/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbMain                                                */
/*                                                                            */
/*  Description     : This the main routine for the PBB module.              */
/*                                                                            */
/*  Input(s)        : pi1Param                                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbMain (INT1 *pi1Param)
{

    tPbbQMsg           *pPbbQMsg = NULL;
    UINT4               u4Events = PBB_INIT_VAL;

    UNUSED_PARAM (pi1Param);

    if (PbbTaskInit () != PBB_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        PBB_INIT_COMPLETE (OSIX_FAILURE);
        PBB_TRC_ARG2 (INIT_SHUT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return;
    }

    if (PBB_GET_TASK_ID (PBB_SELF, PBB_TASK, &(PBB_TASK_ID)) != OSIX_SUCCESS)
    {
        PbbMainDeInit ();
        PBB_INIT_COMPLETE (OSIX_FAILURE);
        PBB_TRC_ARG2 (INIT_SHUT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return;
    }

    /* Indicate the status of initialization to the main routine */
    PBB_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SYSLOG_WANTED
    gPbbGlobData.i4PbbSysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "PBB", SYSLOG_CRITICAL_LEVEL);
#endif

    while (PBB_TRUE)
    {

        if ((PBB_RECEIVE_EVENT (PBB_TASK_ID, (PBB_CFG_MSG_EVENT
                                              | PBB_RED_BULK_UPD_EVENT),
                                OSIX_WAIT, &u4Events)) == OSIX_SUCCESS)
        {
            if (u4Events & PBB_CFG_MSG_EVENT)
            {
                while (PBB_RECV_FROM_QUEUE (PBB_CFG_QUEUE_ID,
                                            (UINT1 *) &pPbbQMsg,
                                            OSIX_DEF_MSG_LEN,
                                            OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    PBB_LOCK ();
                    PbbProcessCfgEvent (pPbbQMsg);
                    /* Relese the context here */
                    PbbReleaseContext ();

                    PBB_UNLOCK ();
                    PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);
                }
            }

#ifdef L2RED_WANTED
            if (u4Events & PBB_RED_BULK_UPD_EVENT)
            {
                PBB_LOCK ();
                PbbRedHandleBulkUpdateEvent ();
                PBB_UNLOCK ();
            }
#endif
        }

    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCreatePort                                    */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever any port is created by */
/*                          mgmt module                                      */
/*                                                                           */
/*    Input(s)            : u2IfIndex    - The Number of the Port            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCreatePort (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort)
{
    INT4                i4RetVal = PBB_INIT_VAL;

    i4RetVal = PbbPostPortCreateMessage (u4ContextId,
                                         i4IfIndex,
                                         u2LocalPort, PBB_PORT_CREATE_MSG);

    if (i4RetVal == PBB_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
    }
    else
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbDeletePort                                    */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port is deleted   */
/*                          by management module.                            */
/*                                                                           */
/*    Input(s)            : u4IfIndex - If index of the port to be deleted.  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbDeletePort (INT4 i4IfIndex)
{

    INT4                i4RetVal = PBB_INIT_VAL;

    i4RetVal = PbbPostCfgMessage (PBB_PORT_DELETE_MSG, i4IfIndex);

    if (i4RetVal == PBB_SUCCESS)
    {
        L2_SYNC_TAKE_SEM ();
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return i4RetVal;

    }
    PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: Failed\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCreateContext                                */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever a context created, this*/
/*                          function post a message to PBB for creating a   */
/*                          context                                          */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCreateContext (UINT4 u4ContextId)
{
    /* post message to PBB */
    tPbbQMsg           *pPbbQMsg = NULL;

    if (gPbbGlobData.u1IsPbbInitialised == PBB_FALSE)
    {
        /* PBB Task is not initialised  */
        return PBB_SUCCESS;
    }

    /* If Pbb is shutdown, then don't send the context creation indication to 
     * it. This kind of situation is possible, if PBB start fails (mempool 
     * creation fails) during system restoration and when I/B bridge mode is 
     * set for a context. In this situation, if we post this message, then PBB 
     * module may try to allocate memory from uninitialized mempool and that 
     * can lead to a crash. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        return PBB_SUCCESS;
    }

    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (pPbbQMsg == NULL)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = PBB_CREATE_CONTEXT_MSG;
    pPbbQMsg->u4ContextId = u4ContextId;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Relase the buffer since Msg is already posted */
        return PBB_FAILURE;
    }
    L2MI_SYNC_TAKE_SEM ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbDeleteContext                                */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever a context deleted, this*/
/*                          function ports a Delete context Message to Pbb   */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PbbDeleteContext (UINT4 u4ContextId)
{
    /* post message to PBB */
    tPbbQMsg           *pPbbQMsg = NULL;

    if (gPbbGlobData.u1IsPbbInitialised == PBB_FALSE)
    {
        /* Pbb Task is not initialised  */
        return PBB_SUCCESS;
    }

    /* If Pbb is shutdown, then don't send the delete context indication to 
     * it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        return PBB_SUCCESS;
    }
    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (pPbbQMsg == NULL)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = PBB_DELETE_CONTEXT_MSG;
    pPbbQMsg->u4ContextId = u4ContextId;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Relase the buffer since Msg is already posted */
        return PBB_FAILURE;
    }
    L2MI_SYNC_TAKE_SEM ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbUpdateContextName                            */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever the name of the        */
/*                          context is modified. function post a message to  */
/*                          PBB for updating the name of the context.       */
/*                                                                           */
/*    Input(s)            : u4ContextId  - context Id                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PbbUpdateContextName (UINT4 u4ContextId)
{
    /* post message to Pbb */
    tPbbQMsg           *pPbbQMsg = NULL;

    if (gPbbGlobData.u1IsPbbInitialised == PBB_FALSE)
    {
        /* PBB Task is not initialised  */
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_SUCCESS;
    }

/* If Pbb is shutdown, then don't send this indication to it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        return PBB_SUCCESS;
    }

    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (pPbbQMsg == NULL)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = PBB_UPDATE_CONTEXT_NAME;
    pPbbQMsg->u4ContextId = u4ContextId;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Relase the buffer since Msg is already posted */
        return PBB_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbLock                                             */
/*                                                                           */
/* Description        : This function is used to take the PBB mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS or PBB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PbbLock (VOID)
{
    INT4                i4RetVal = 0;
    if (PBB_TAKE_SEM (PBB_SEM_ID) != OSIX_SUCCESS)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* PbbSelectContext and PbbReleaseContext does not have stubs in case of
     * SI mode. Also context selection will not be done implicitly even in SI
     * mode. 
     * Therefore in SI mode also before calling SI nmh we have to do select 
     * context. 
     * Registering select and release context with SNMP is not possible as, 
     * select context for default context in Lock function.*/

    i4RetVal = PbbSelectContext (PBB_DEF_CONTEXT_ID);

    UNUSED_PARAM (i4RetVal);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbUnLock                                           */
/*                                                                           */
/* Description        : This function is used to give the PBB mutual        */
/*                      exclusion SEMa4 to avoid simultaneous access to      */
/*                      protocol data structures by the protocol task and    */
/*                      configuration task/thread.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS or PBB_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
INT4
PbbUnLock (VOID)
{
    PbbReleaseContext ();
    PBB_RELEASE_SEM (PBB_SEM_ID);
    return SNMP_SUCCESS;
}

#ifdef NPAPI_WANTED
/***************************************************************************/
/* Function Name    : PbbCopyPortPropertiesToHw ()                        */
/*                                                                         */
/* Description      : This function programs the hardware with all         */
/*                    the Port properties for the port "u2DstPort".        */
/*                    The properties to be copied to "u2DstPort" is        */
/*                    obtained from "u2Port".                              */
/*                    "u2SrcPort" must have been created in PBB. This     */
/*                    function does not program the Dynamic PBB and Mcast */
/*                    membership and hence must not be called for ports    */
/*                    that are in OPER UP state. Dynamic Learning is NOT   */
/*                    done when the port is in OPER DOWN state.            */
/*                                                                         */
/* Input(s)         : u2DstPort - The Port whose properties have to be     */
/*                                programmed in the Hardware.              */
/*                                                                         */
/*                  : u2SrcPort - The Port from where the port properties  */
/*                                have to be obtained.                     */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : PBB_SUCCESS on success,                             */
/*                    PBB_FAILURE otherwise.                              */
/***************************************************************************/
INT1
PbbCopyPortPropertiesToHw (UINT4 u4DstPort, UINT4 u4SrcPort)
{

    tPbbQMsg           *pPbbQMsg = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    if (gPbbGlobData.u1IsPbbInitialised == PBB_FALSE)
    {
        /* PBB Task is not initialised  */
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_SUCCESS;
    }

    /* If Pbb is shutdown, then don't send this indication to it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        return PBB_SUCCESS;
    }

    if (PbbVcmGetContextInfoFromIfIndex (u4DstPort, &u4ContextId,
                                         &u2LocalPort) == VCM_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (PBB_CONTEXT_PTR (u4ContextId) == NULL)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        return PBB_FAILURE;
    }
    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (pPbbQMsg == NULL)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = PBB_COPY_PORT_INFO_MSG;
    pPbbQMsg->i4IfIndex = u4SrcPort;
    pPbbQMsg->u4DstPort = u4DstPort;
    pPbbQMsg->u4ContextId = u4ContextId;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_TASK_ID == PBB_INIT_VAL)
    {

        if (PBB_GET_TASK_ID (PBB_SELF, PBB_TASK, &(PBB_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: Failed\n", __FILE__,
                          PBB_FUNCTION_NAME);
            /* No Need to Free Message Since it is already Queued */
            return PBB_FAILURE;
        }

    }
    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        /* No Need to Free Message Since it is already Queued */
        return PBB_FAILURE;
    }
    L2_SYNC_TAKE_SEM ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/***************************************************************************/
/* Function Name    : PbbRemovePortPropertiesFromHw  ()                    */
/*                                                                         */
/* Description      : This function removes from the Hardware all the      */
/*                    port properties of "u2DstPort". The exact properties */
/*                    which need to be removed is obtained from the        */
/*                    port "u2SrcPort". This function is typically called  */
/*                    when a physical port is removed from a Port Channel. */
/*                    When the physical port is removed from a Port        */
/*                    Channel, the Port Channel properties must be removed */
/*                    from the physical port. This function will be        */
/*                    invoked with "u2DstPort" as the Physical Port Id and */
/*                    "u2SrcPort" as the Port Channel Id.                  */
/*                    "u2SrcPort" must have been created in PBB.           */
/*                                                                         */
/*                    This function will also be invoked whenever a Port   */
/*                    is deleted in PBB. In which, u2SrcPort and          */
/*                    u2DstPort will be same.                              */
/*                                                                         */
/* Input(s)         : u2DstPort - Port whose properties have to be removed */
/*                                from the Hardware.                       */
/*                                                                         */
/*                    u2SrcPort - Port from where the properties to be     */
/*                                removed, have to be obtained.            */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : PBB on success,                             */
/*                    PBB otherwise.                              */
/***************************************************************************/
INT1
PbbRemovePortPropertiesFromHw (UINT4 u4DstPort, UINT4 u4SrcPort)
{

    tPbbQMsg           *pPbbQMsg = NULL;
    UINT4               u4ContextId;
    UINT2               u2LocalPort;

    if (gPbbGlobData.u1IsPbbInitialised == PBB_FALSE)
    {
        /* Pbb Task is not initialised  */
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_SUCCESS;
    }

    /* If Pbb is shutdown, then don't send this indication to it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        return PBB_SUCCESS;
    }

    if (PbbVcmGetContextInfoFromIfIndex (u4DstPort, &u4ContextId,
                                         &u2LocalPort) == VCM_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (PBB_CONTEXT_PTR (u4ContextId) == NULL)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (pPbbQMsg == NULL)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = PBB_REMOVE_PORT_INFO_MSG;
    pPbbQMsg->i4IfIndex = u4SrcPort;
    pPbbQMsg->u4DstPort = u4DstPort;
    pPbbQMsg->u4ContextId = u4ContextId;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_TASK_ID == PBB_INIT_VAL)
    {
        if (PBB_GET_TASK_ID (PBB_SELF, PBB_TASK, &(PBB_TASK_ID)) !=
            OSIX_SUCCESS)
        {
            PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: Failed\n", __FILE__,
                          PBB_FUNCTION_NAME);
            /* No Need to Free Message Sice it is already Queued */
            return PBB_FAILURE;
        }
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Free Message Sice it is already Queued */
        return PBB_FAILURE;
    }
    L2_SYNC_TAKE_SEM ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);

    return PBB_SUCCESS;
}
#endif

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbUpdatePortStatus                              */
/*                                                                           */
/*    Description         : Invoked by L2IWF  whenever any port status is    */
/*                          updated by management module.                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex -The number of the port that is deleted*/
/*                          portstatus                                       */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbUpdatePortStatus (INT4 i4IfIndex, UINT1 u1PortStatus)
{
    tPbbQMsg           *pPbbQMsg = NULL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (gPbbGlobData.u1IsPbbInitialised == PBB_FALSE)
    {
        /* PBB Task is not initialised  */
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_SUCCESS;
    }

    /* If Pbb is shutdown, then don't send this indication to it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        return PBB_SUCCESS;
    }
    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (pPbbQMsg == NULL)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));
    if (PbbVcmGetContextInfoFromIfIndex (i4IfIndex, &u4ContextId, &u2LocalPort)
        == VCM_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);
        return PBB_FAILURE;
    }
    pPbbQMsg->u2MsgType = PBB_UPDATE_PORT_STATUS;
    pPbbQMsg->u4ContextId = u4ContextId;
    pPbbQMsg->u1PortStatus = u1PortStatus;
    pPbbQMsg->i4IfIndex = i4IfIndex;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Relase the buffer since Msg is already posted */
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbMapPortIndication                             */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever any port is mapped  by */
/*                          mgmt module                                      */
/*                                                                           */
/*    Input(s)            : u2IfIndex    - The Number of the Port            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PbbMapPortIndication (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort)
{
    INT4                i4RetVal = PBB_INIT_VAL;

    i4RetVal = PbbPostMapPortIndication (u4ContextId,
                                         i4IfIndex,
                                         u2LocalPort, PBB_MAP_PORT_INDICATION);

    if (i4RetVal == PBB_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
    }
    else
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
    }

    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbUnMapPortIndication                             */
/*                                                                           */
/*    Description         : Invoked by L2IWF whenever any port is unmapped  by*/
/*                          mgmt module                                      */
/*                                                                           */
/*    Input(s)            : u2IfIndex    - The Number of the Port            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PbbUnMapPortIndication (INT4 i4IfIndex)
{
    INT4                i4RetVal = PBB_INIT_VAL;

    i4RetVal =
        PbbPostUnMapPortIndication (PBB_UNMAP_PORT_INDICATION, i4IfIndex);

    if (i4RetVal == PBB_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();

        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return i4RetVal;
    }
    PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

    return i4RetVal;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbVlanAuditStatusInd                            */
/*                                                                           */
/*    Description         : Invoked by VLAN whenever its Audit gets finished.*/
/*                          or when bridge mode audit is done by vlan.       */
/*                          After receiving Vlan Hw Audit complete msg Pbb   */
/*                          will start audit for its Delete Port.            */
/*                          After receiving the bridge mode change indication*/
/*                          from vlan, Pbb will configure all the            */
/*                          configurations related to that context.          */
/*                                                                           */
/*    Input(s)            : pPbbVlanStatus - u4VlanAudStatus - Audit Status  */
/*                              Possible values: PBB_VLAN_AUDIT_BRG_MODE_CHG */
/*                                               PBB_VLAN_AUDIT_COMPLETED    */
/*                        :                  u4ContetxId - ContextId of the  */
/*                        :     Context where bridge mode audit is being done*/
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*****************************************************************************/
INT4
PbbVlanAuditStatusInd (tPbbVlanAuditStatus * pPbbVlanStatus)
{
    INT4                i4RetVal = PBB_INIT_VAL;

    i4RetVal = PbbPostVlanAuditStatusIndic (pPbbVlanStatus);

    if (i4RetVal == PBB_SUCCESS)
    {
        return i4RetVal;
    }

    return i4RetVal;
}

#ifdef CLI_WANTED
/****************************************************************************/
/*     FUNCTION NAME    : PbbSetPipPcpDecodingTable                         */
/*                                                                          */
/*     DESCRIPTION      : This function will configure the priority and drop*/
/*                        eligible indicator for recieved PCP on Particular */
/*                        Port in PCP Decoding Table                        */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                        i4SelRow - selection row identifier               */
/*                        i4Pcp  - Priority code point                      */
/*                        i4Priority - Configured priority                  */
/*                        i4Dei - Drop eligible indicator                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
PbbSetPipPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                           INT4 i4SelRow, INT4 i4Pcp, INT4 i4Priority,
                           INT4 i4Dei)
{
    cli_process_pbb_cmd (CliHandle, CLI_PBB_PORT_PCP_DECODE, NULL, i4IfIndex,
                         i4SelRow, i4Pcp, i4Priority, i4Dei);
    return TRUE;

}

/****************************************************************************/
/*     FUNCTION NAME    : PbbIsPortTypePip                                  */
/*                                                                          */
/*     DESCRIPTION      : This function will configure the priority and drop*/
/*                        eligible indicator for recieved PCP on Particular */
/*                        Port in PCP Decoding Table                        */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
PbbIsPortTypePip (INT4 i4IfIndex)
{
    tCfaIfInfo          CfaIfInfo;
    if (PbbCfaGetIfInfo (i4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return PBB_FAILURE;
    }

    if (CfaIfInfo.u1BrgPortType == PBB_PROVIDER_INSTANCE_PORT)
    {
        return TRUE;
    }
    return FALSE;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbSetPipPcpEncodingTable                         */
/*                                                                          */
/*     DESCRIPTION      : This function will configure the PCP in the       */
/*                        Priority Encoding Table                           */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                        i4SelRow - selection row identifier               */
/*                        i4Pcp  - configured Priority code point           */
/*                        i4Priority - priority                             */
/*                        i4Dei - Drop eligible indicator                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbSetPipPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                           INT4 i4SelRow, INT4 i4Priority, INT4 i4Pcp,
                           INT4 i4Dei)
{
    cli_process_pbb_cmd (CliHandle, CLI_PBB_PORT_PCP_ENCODE, NULL, i4IfIndex,
                         i4SelRow, i4Priority, i4Pcp, i4Dei);
    return TRUE;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbResetPipPcpDecodingTable                       */
/*                                                                          */
/*     DESCRIPTION      : This function resets the PCP decoding table values*/
/*                        to default values.                                */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                        i4SelRow - selection row identifier               */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbResetPipPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                             INT4 i4SelRow)
{
    cli_process_pbb_cmd (CliHandle, CLI_PBB_PORT_NO_PCP_DECODE, NULL, i4IfIndex,
                         i4SelRow);
    return TRUE;

}

/****************************************************************************/
/*     FUNCTION NAME    : PbbResetPipPcpEncodingTable                       */
/*                                                                          */
/*     DESCRIPTION      : This function resets the PCP encoding table values*/
/*                        to default values.                                */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                        i4SelRow - selection row identifier               */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbResetPipPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                             INT4 i4SelRow)
{
    cli_process_pbb_cmd (CliHandle, CLI_PBB_PORT_NO_PCP_ENCODE, NULL, i4IfIndex,
                         i4SelRow);
    return TRUE;

}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbSetPipReqDropEncoding                           */
/*                                                                           */
/*     DESCRIPTION      : This function configures the Req drop encoding for */
/*                        Port.                                              */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex  - Interface Index                       */
/*                        i4ReqDropEncoding - TRUE /FALSE                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PbbSetPipReqDropEncoding (tCliHandle CliHandle, INT4 i4IfIndex,
                          INT4 i4ReqDropEncoding)
{
    cli_process_pbb_cmd (CliHandle, CLI_PBB_PORT_REQ_DROP_ENC, NULL, i4IfIndex,
                         i4ReqDropEncoding);
    return TRUE;

}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbSetPipPcpSelRow                                 */
/*                                                                           */
/*     DESCRIPTION      : This function configures the for Pcp Selection row */
/*                        for a Port.                                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex  - Interface Index                       */
/*                        i4PcpSelRow - PBB_8P0D_SEL_ROW /                   */
/*                                      PBB_7P1D_SEL_ROW /                   */
/*                                      PBB_6P2D_SEL_ROW /                   */
/*                                      PBB_5P3D_SEL_ROW                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PbbSetPipPcpSelRow (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4PcpSelRow)
{
    cli_process_pbb_cmd (CliHandle, CLI_PBB_PORT_PCP_SEL_ROW, NULL, i4IfIndex,
                         i4PcpSelRow);
    return TRUE;

}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbSetPipUseDei                                    */
/*                                                                           */
/*     DESCRIPTION      : This function configures the Use Dei for Port      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex  - Interface Index                       */
/*                        i4UseDei - TRUE /FALSE                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PbbSetPipUseDei (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4UseDei)
{
    cli_process_pbb_cmd (CliHandle, CLI_PBB_PORT_USE_DEI, NULL, i4IfIndex,
                         i4UseDei);
    return TRUE;

}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowPipPcpEncodingTable                        */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP Decoding table     */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
PbbShowPipPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                            UINT1 *pu1ContextName)
{
    cli_process_pbb_show_cmd (CliHandle, CLI_PBB_SHOW_PORT_PCP_ENCODE,
                              i4IfIndex, pu1ContextName);
    return TRUE;

}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowPipPcpDecodingTable                        */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP Decoding table     */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbShowPipPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                            UINT1 *pu1ContextName)
{
    cli_process_pbb_show_cmd (CliHandle, CLI_PBB_SHOW_PORT_PCP_DECODE,
                              i4IfIndex, pu1ContextName);
    return TRUE;

}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowPipPortConfig                              */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PIP Config             */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbShowPipPortConfig (tCliHandle CliHandle, INT4 i4IfIndex,
                      UINT1 *pu1ContextName)
{
    cli_process_pbb_show_cmd (CliHandle, CLI_PBB_SHOW_PORT_CONFIG,
                              i4IfIndex, pu1ContextName);
    return TRUE;

}
#endif
/****************************************************************************
 Function    :  PbbGetIsidInfoFromFrame
 Description :  To get Isid tag information from the protocl frame 
 Input       :  Context Id - context id
                 u2LocalPort -  port on which frame received
                pBuf - PDU buffer
                u4IsidTagOffSet - offset where I-tag info present in buf
                
 Output      :  pPbbTag
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT4
PbbGetIsidInfoFromFrame (UINT4 u4ContextId, UINT2 u2LocalPort,
                         tCRU_BUF_CHAIN_DESC * pBuf, tPbbTag * pPbbTag,
                         UINT4 u4IsidTagOffSet)
{
    INT4                i4retVal = PBB_FAILURE;
    /* take the lock */
    PBB_LOCK ();
    i4retVal = PbbGetIsidInfoDataFromFrame (u4ContextId,
                                            u2LocalPort,
                                            pBuf, pPbbTag, u4IsidTagOffSet);
    PBB_UNLOCK ();
    return i4retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPipVipWithIsid                             */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetPipVipWithIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                      UINT4 u4Isid, UINT2 *pu2Vip, UINT2 *pu2Pip)
{
    INT1                i1retVal = PBB_FAILURE;
    /* take the lock */
    PBB_LOCK ();
    i1retVal = PbbGetPipVipDataWithIsid (u4ContextId,
                                         u2LocalPort, u4Isid, pu2Vip, pu2Pip);
    PBB_UNLOCK ();
    return i1retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipFromIsid                                */
/*                                                                          */
/*    Description        :This function is used to Get VIP for an ISID      */
/*                                                                          */
/*    Input(s)           : u4Context - Context ID                           */
/*                         u4Isid - ISID Value                              */
/*    Output(s)          : pu4Vip   - VIP for the given ISID                */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipFromIsid (UINT4 u4Context, UINT4 u4Isid, UINT2 *pu2Vip)
{
    INT1                i1RetVal = PBB_SUCCESS;

    PBB_LOCK ();

    i1RetVal = PbbGetVIPValue (u4Context, u4Isid, pu2Vip);

    PBB_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetCbpListForIsid                             */
/*                                                                          */
/*    Description        :This function is used to Get VIP for an ISID      */
/*                                                                          */
/*    Input(s)           : u4Context - Context ID                           */
/*                         u4Isid - ISID Value                              */
/*    Output(s)          : pRetValCbpList   - CBP List for the given ISID   */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetCbpListForIsid (UINT4 u4ContextId, UINT4 u4Isid,
                      tSNMP_OCTET_STRING_TYPE * pRetValCbpList)
{
    INT1                i1RetVal = PBB_SUCCESS;

    PBB_LOCK ();

    i1RetVal = PbbGetCbpForIsid (u4ContextId, u4Isid, pRetValCbpList);

    PBB_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbGetBvidForIsid                                 */
/*                                                                          */
/*     DESCRIPTION      : This function will be called by VLAN to get       */
/*                         shutdown  status                                 */
/*                                                                          */
/*     INPUT            :                                                   */
/*     OUTPUT           :                                                   */
/*                                                                          */
/*     RETURNS          : PBB_TRUE/PBB_FALSE                                */
/*                                                                          */
/****************************************************************************/
INT4
PbbGetBvidForIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                   UINT4 u4Isid, tVlanId * pBvid)
{
    tPbbBackBoneSerInstEntry *pPbbIsidPortEntry = NULL;

    PBB_LOCK ();

    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }

    if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pPbbIsidPortEntry) !=
        PBB_SUCCESS)
    {
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    *pBvid = pPbbIsidPortEntry->u2BVlanId;
    PBB_UNLOCK ();
    return PBB_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbGetIsidForVip                                 */
/*                                                                          */
/*     DESCRIPTION      : This function will return ISID associated witrh VIP*/
/*                         shutdown  status                                 */
/*                                                                          */
/*     INPUT            :                                                   */
/*     OUTPUT           :                                                   */
/*                                                                          */
/*     RETURNS          : PBB_TRUE/PBB_FALSE                                */
/*                                                                          */
/****************************************************************************/
INT1
PbbGetIsidForVip (UINT4 u4Vip, UINT4 *pu4Isid)
{
    INT1                i1retVal = PBB_FAILURE;
    PBB_LOCK ();
    i1retVal = PbbGetIsidDataForVip (u4Vip, pu4Isid);
    PBB_UNLOCK ();
    return i1retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetLocalIsidFromRelayIsid                     */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetLocalIsidFromRelayIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                              UINT4 u4RelayIsid, UINT4 *pu4LocalIsid)
{
    INT1                i1retVal = PBB_FAILURE;
    PBB_LOCK ();
    i1retVal = PbbGetLocalIsidDataFromRelayIsid (u4ContextId,
                                                 u2LocalPort,
                                                 u4RelayIsid, pu4LocalIsid);
    PBB_UNLOCK ();
    return i1retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbIsIsidMemberPort                              */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbIsIsidMemberPort (UINT4 u4ContextId, UINT4 u4Isid, UINT2 u2LocalPort)
{
    INT1                i1retVal = PBB_FAILURE;
    PBB_LOCK ();
    i1retVal = PbbIsIsidMemberPortPresent (u4ContextId, u4Isid, u2LocalPort);
    PBB_UNLOCK ();
    return i1retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetCnpMemberPortsForIsid                      */
/*                                                                          */
/*    Description        : This function is used to get member ports of ISID*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT4
PbbGetCnpMemberPortsForIsid (UINT4 u4ContextId,
                             UINT4 u4Isid, tSNMP_OCTET_STRING_TYPE * pPortList)
{
    INT4                i4retVal = PBB_FAILURE;
    PBB_LOCK ();
    i4retVal = PbbGetCnpMemberPortListForIsid (u4ContextId, u4Isid, pPortList);
    PBB_UNLOCK ();
    return i4retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipIsidWithPortList                        */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipIsidWithPortList (UINT4 u4ContextId,
                           UINT2 u2LocalPort,
                           tLocalPortList InPortList,
                           UINT2 *pu2Vip,
                           UINT2 *pu2Pip, UINT4 *pu4Isid, tMacAddr * pBDaAddr)
{
    INT1                i1retVal = PBB_FAILURE;
    PBB_LOCK ();
    i1retVal = PbbGetVipIsidDataWithPortList (u4ContextId,
                                              u2LocalPort,
                                              InPortList,
                                              pu2Vip,
                                              pu2Pip, pu4Isid, pBDaAddr);
    PBB_UNLOCK ();
    return i1retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMemberPortsForIsid                         */
/*                                                                          */
/*    Description        : This function is used to get member ports of ISID*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT4
PbbGetMemberPortsForIsid (UINT4 u4ContextId,
                          UINT4 u4Isid, tSNMP_OCTET_STRING_TYPE * pPortList)
{
    INT4                i4retVal = PBB_FAILURE;
    PBB_LOCK ();
    i4retVal = PbbGetMemberPortListForIsid (u4ContextId, u4Isid, pPortList);
    PBB_UNLOCK ();
    return i4retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetBCompBDA                                   */
/*                                                                          */
/*    Description        : This function is used to get the BDA for the     */
/*                         PDU being generated from CBP for B-COMP          */
/*                                                                          */
/*    Input(s)           : u4ContextId - Context ID                         */
/*                         u2LocalPort - local port  of the CBP             */
/*                         u4Isid - ISID                                    */
/*                         u1Dir - Direction of the frame                   */
/*                                 PBB_INGRESS - for frame coming from      */
/*                                               PIP                        */
/*                                 PBB_EGRESS  - for frame coming from      */
/*                                               PNP using the reply        */
/*                         b1UseDefaultBDA - Boolean to indicate whether    */
/*                                           configured BDA can be used     */
/*    Output(s)          : BDA - BDA mac address                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/
VOID
PbbGetBCompBDA (UINT4 u4ContextId, UINT2 u2LocalPort, UINT4 u4Isid,
                tMacAddr BDA, UINT1 u1Dir, BOOL1 b1UseDefaultBDA)
{
    tSNMP_OCTET_STRING_TYPE OUI;
    tPbbPortRBtreeNode  key;
    tPbbPortRBtreeNode *pRBNode;
    UINT4               u4IfIndex = 0;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    tMacAddr            NullMacAddr;
    tMacAddr            DefaultBDA;
    INT1                i1RetVal = PBB_SUCCESS;
    PBB_MEMSET (au1OUI, PBB_INIT_VAL, PBB_OUI_LENGTH);
    PBB_MEMSET (NullMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (DefaultBDA, PBB_INIT_VAL, sizeof (tMacAddr));
    OUI.i4_Length = PBB_OUI_LENGTH;
    OUI.pu1_OctetList = au1OUI;

    PBB_LOCK ();
    if (u1Dir == PBB_INGRESS)
    {
        if (b1UseDefaultBDA == OSIX_TRUE)
        {
            do
            {
                if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
                {
                    break;
                }

                if (PbbGetIsidPortDefBDA (u2LocalPort, u4Isid, &DefaultBDA)
                    != PBB_SUCCESS)
                {
                    break;
                }
                /* Check if default destination mac address is configured */
                if (PBB_MEMCMP (DefaultBDA, NullMacAddr, sizeof (tMacAddr)
                                != 0))
                {
                    PBB_MEMCPY (BDA, DefaultBDA, sizeof (tMacAddr));
                    PbbReleaseContext ();
                    PBB_UNLOCK ();
                    return;
                }
            }
            while (0);
        }
        /* Use the Global OUI + ISID for going inside PBBN */
        if (PbbGetPbbGlbOUI (&OUI) != PBB_SUCCESS)
        {
            PBB_UNLOCK ();
            return;
        }
    }
    else
    {
        /* Use the Local OUI + ISID for going towards I-COMP. */
        if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
        {
            PBB_UNLOCK ();
            return;
        }
        key.u2LocalPort = u2LocalPort;
        pRBNode =
            (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                              PortTable, &key);
        if (pRBNode == NULL)
        {
            PbbReleaseContext ();
            PBB_UNLOCK ();
            return;
        }
        u4IfIndex = pRBNode->u4IfIndex;
        if (PbbGetIsidOUI (u4ContextId, u4Isid, u4IfIndex, &OUI) != PBB_SUCCESS)
        {
            /* Use the global OUI if local OUI is not configured */
            i1RetVal = PbbGetPbbGlbOUI (&OUI);
        }
        PbbReleaseContext ();
    }
    PBBGetBDAFromOUI (&OUI, u4Isid, BDA);
    PBB_UNLOCK ();
    UNUSED_PARAM (i1RetVal);
    return;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbGetPipIsidWithVip                              */
/*                                                                          */
/*     DESCRIPTION      : This function will be called by VLAN to get       */
/*                         shutdown  status                                 */
/*                                                                          */
/*     INPUT            :                                                   */
/*     OUTPUT           :                                                   */
/*                                                                          */
/*     RETURNS          : PBB_TRUE/PBB_FALSE                                */
/*                                                                          */
/****************************************************************************/
INT4
PbbGetPipIsidWithVip (UINT4 u4ContextId,
                      UINT2 u2Vip,
                      UINT4 *pu4PipIndex, UINT4 *pu4Isid, tMacAddr * pBDaAddr)
{
    INT4                i4retVal = PBB_FAILURE;
    PBB_LOCK ();
    i4retVal = PbbGetPipIsidDataWithVip (u4ContextId,
                                         u2Vip, pu4PipIndex, pu4Isid, pBDaAddr);
    PBB_UNLOCK ();
    return i4retVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetDerivedInstanceMacAddress                  */
/*                                                                           */
/*    Description         : This function is used to get derived instance mac*/
/*                          address                                          */
/*    Input(s)            : Instance Id                                      */
/*                                                                           */
/*    Output(s)           : Derived Mac Address                              */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_FAILURE/PBB_SUCCESS                    */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetDerivedInstanceMacAddress (UINT4 u4InstanceId,
                                 UINT1 *pu1InstanceMacAddressType)
{
    PBB_LOCK ();
    if (PBB_INSTANCE_PTR (u4InstanceId) == NULL)
    {
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);
    *pu1InstanceMacAddressType = PBB_CURR_INSTANCE_PTR ()->u1MacAddressType;
    PBB_UNLOCK ();
    return PBB_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : PbbConfPortIsidLckStatus
 *
 *    Description          : This function updates LCK status for Data 
 *                           corresponding to Service Instance
 *
 *    Input(s)             : u4IfIndex: Interface Index 
 *                           u4Isid : Isid for which status to be updated
 *                           b1Status: Status 
 *
 *    Output(s)            : None.
 *
 *    Returns              : PBB_SUCCESS/PBB_FAILURE.
 *****************************************************************************/
INT4
PbbConfPortIsidLckStatus (UINT4 u4IfIndex, UINT4 u4Isid, BOOL1 b1Status)
{
    INT4                i4RetVal = PBB_FAILURE;
    PBB_LOCK ();
    i4RetVal = PbbHwSetPortIsidLckStatus (u4IfIndex, u4Isid, b1Status);
    PBB_UNLOCK ();
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       :  PbbGetPortIsidStats                             */
/*                                                                           */
/*    Description         : This function is called by ECFM module to        */
/*                          get the transmission counters for a vlan         */
/*                          present on a particular port.                    */
/*                                                                           */
/*    Input(s)            : u4IfIndex - Port Index                           */
/*                          u4Isid    - Isid                                 */
/*                                                                           */
/*    Output(s)           : pu4TxFCl - Transmission Counter                  */
/*                          pu4RxFCl - Reception Counter                     */
/*                                                                           */
/*    Returns            : NONE                                              */
/*                                                                           */
/*****************************************************************************/
VOID
PbbGetPortIsidStats (UINT4 u4IfIndex, UINT4 u4Isid,
                     UINT4 *pu4TxFCl, UINT4 *pu4RxFCl)
{
    PBB_LOCK ();
    PbbHwGetPortIsidStats (u4IfIndex, u4Isid, PBB_PORT_IN_FRAMES, pu4RxFCl);
    PbbHwGetPortIsidStats (u4IfIndex, u4Isid, PBB_PORT_OUT_FRAMES, pu4TxFCl);
    PBB_UNLOCK ();
    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextIsidForCBP                            */
/*                                                                          */
/*    Description        : This function is used to get ISID mapped to a CBP*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNextIsidForCBP (UINT4 u4ContextId, UINT2 u2LocalPort, UINT4 u4CurrIsid,
                      UINT4 *pu4NextIsid)
{
    tPbbIsidPipVipNode *pListNode = NULL;
    tPbbIsidPipVipNode *pIsidDllNode = NULL;
    tPbbIsidPipVipNode *pIsidtempDllNode = NULL;
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    PBB_LOCK ();

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        PbbReleaseContext ();
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    /* Get the first entry */
    if (u4CurrIsid == 0)
    {
        pIsidDllNode = (tPbbIsidPipVipNode *) PBB_DLL_FIRST
            (&(pRBCurNode->unPbbPortData.CbpData.IsidList));
        if (pIsidDllNode == NULL)
        {
            PbbReleaseContext ();
            PBB_UNLOCK ();
            return PBB_FAILURE;
        }
        *pu4NextIsid = pIsidDllNode->unIsidVip.u4Isid;
        PbbReleaseContext ();
        PBB_UNLOCK ();
        return PBB_SUCCESS;
    }
    PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.CbpData.IsidList),
                  pListNode, tPbbIsidPipVipNode *)
    {
        pIsidtempDllNode = (tPbbIsidPipVipNode *) pListNode;
        if (u4CurrIsid == pIsidtempDllNode->unIsidVip.u4Isid)
            break;
    }

    if (pIsidtempDllNode == NULL)
    {
        PbbReleaseContext ();
        PBB_UNLOCK ();
        return PBB_FAILURE;

    }

    pIsidDllNode = (tPbbIsidPipVipNode *) PBB_DLL_NEXT
        (&(pRBCurNode->unPbbPortData.CbpData.IsidList),
         (&(pIsidtempDllNode->link)));
    if (pIsidDllNode == NULL)
    {
        PbbReleaseContext ();
        PBB_UNLOCK ();
        return PBB_FAILURE;

    }
    *pu4NextIsid = pIsidDllNode->unIsidVip.u4Isid;
    PbbReleaseContext ();
    PBB_UNLOCK ();
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbStartPbbModule                                */
/*                                                                           */
/*    Description         : This function is used to start the PBB module    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_FAILURE/PBB_SUCCESS                    */
/*                                                                           */
/*****************************************************************************/
INT1
PbbStartPbbModule (VOID)
{
    INT1                i1RetVal = PBB_SUCCESS;

    PBB_LOCK ();

    /* Intialize the PBB Red Info */
    PbbRedInitGlobalInfo ();
    PbbRedInitRedundancyInfo ();

    /* Register PBB with RM */
    i1RetVal = (INT1) PbbRedRegisterWithRM ();

    if (i1RetVal == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Registration with RM failed \n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);
        PbbRedDeInitGlobalInfo ();
        PbbRedDeInitRedundancyInfo ();
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }

    /* Set the Shutdown status as False */
    i1RetVal = PbbHandleShutdownStatus (PBB_SNMP_FALSE);

    PBB_UNLOCK ();

    return i1RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbShutdownPbbModule                             */
/*                                                                           */
/*    Description         : This function is used to start the PBB module    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_FAILURE/PBB_SUCCESS                    */
/*                                                                           */
/*****************************************************************************/
INT1
PbbShutdownPbbModule (VOID)
{
    INT1                i1RetVal = PBB_SUCCESS;

    PBB_LOCK ();

    /* Set the Shutdown status as False */
    PbbHandleShutdownStatus (PBB_SNMP_TRUE);

#ifdef L2RED_WANTED
    /* Deinitialize the RED Data */

    /* If PBB Audit task is running delete the task */
    if (PBB_AUDIT_TASK_ID () != 0)
    {
        /* If audit is currently going on, setting this audit flag,
         * will make the audit to stop.  When the auditing is stopped,
         * remaining hardware audit table entries (SLL entries) will 
         * be deleted in the PbbRedDeInitGlobalInfo function. Sending 
         * event to AUDIT task, only will delete the audit task*/
        PBB_RED_VLAN_AUDIT_FLAG () = PBB_RED_AUDIT_STOP;

        if (PBB_SEND_EVENT
            (PBB_AUDIT_TASK_ID (),
             PBB_RED_AUDIT_TASK_DELETE_EVENT) == OSIX_FAILURE)
        {
            return PBB_FAILURE;
        }

    }

    PbbRedDeInitGlobalInfo ();

    PbbRedDeInitRedundancyInfo ();

    if (PbbRmDeRegisterProtocols () == RM_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  "
                      "PBB RM Derigtration Failed\n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);
    }
#endif

    PBB_UNLOCK ();

    return i1RetVal;
}
#endif
