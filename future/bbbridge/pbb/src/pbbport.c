/********************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbport.c,v 1.14 2013/09/27 07:28:52 siva Exp $
 *
 * Description: This file contains the external APIs.                    
 *
 *******************************************************************/

#include "pbbinc.h"
#include "fspbbcli.h"
#include "fs1ahcli.h"

/*****************************************************************************/
/*    Function Name       : PbbCfaGetNumberOfBebPorts                        */
/*    Description         : This is a wrapper function to get the number of  */
/*                          backbone edge bridge ports                       */
/*    Input(s)            : None                                             */
/*    Output(s)           : Number of BEB ports                              */
/*    Returns             : Number of BEB ports                              */
/*****************************************************************************/

INT4
PbbCfaGetNumberOfBebPorts (VOID)
{
    return CfaGetNumberOfBebPorts ();
}

/*****************************************************************************/
/*    Function Name       : PbbCfaGetFreeInterfaceIndex                      */
/*    Description         : This is a wrapper function to get the free if    */
/*                          index                                            */
/*    Input(s)            : Context Id                                       */
/*    Output(s)           : Free If Index                                    */
/*                          Free Local Port                                  */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                          */
/*****************************************************************************/

INT1
PbbCfaGetFreeInterfaceIndex (INT4 *pi4RetValFsPbbNextAvailablePipIfIndex,
                             UINT1 u1IfType)
{
    UNUSED_PARAM (u1IfType);
    *pi4RetValFsPbbNextAvailablePipIfIndex = 0;
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbCfaSetIfMainBrgPortType
 */
/*    Description         : This is a wrapper function to get the 
      running Vip number
 */
/*    Input(s)            : Context Id                                     
 */
/*    Output(s)           : Free If Index
      Free Local Port
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbCfaSetIfMainBrgPortType (INT4 i4IfMainIndex, INT4 i4SetValIfMainBrgPortType)
{
    INT4                i4retVal = SNMP_SUCCESS;

    PBB_UNLOCK ();

    i4retVal =
        CfaSetIfMainBrgPortType (i4IfMainIndex, i4SetValIfMainBrgPortType);

    PBB_LOCK ();

    if (i4retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbCfaCreateVip
 */
/*    Description         : This is a wrapper function to get the 
      running Vip number
 */
/*    Input(s)            : Context Id                                     
 */
/*    Output(s)           : Free If Index
      Free Local Port
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbCfaCreateVip (INT4 *pi4FreeIfIndex)
{
    INT4                i4retVal = CFA_FAILURE;

    PBB_UNLOCK ();
    PBB_PERF_RESET_TIME (gu4ISIDCreateTimeTaken);
    PBB_PERF_MARK_START_TIME ();
    i4retVal = CfaCreateVip (pi4FreeIfIndex);
    PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
    PBB_LOCK ();

    if (i4retVal == CFA_FAILURE)
    {
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;

}

/*****************************************************************************/
/*    Function Name       : PbbMapVcmPort
 */
/*    Description         : This is a wrapper function for mapping VCM port to 
      context
 */
/*    Input(s)            : Context Id                                     
 */
/*    Output(s)           : Free If Index
      Free Local Port
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/
INT1
PbbMapVcmPort (INT4 i4FreeIfIndex, UINT4 u4ContextId)
{
    UINT4               u4ErrorCode = PBB_INIT_VAL;
    PBB_UNLOCK ();
    if (VcmIsIfMapExist (i4FreeIfIndex) == VCM_TRUE)
    {
        PBB_LOCK ();
        return PBB_FAILURE;
    }
    if (SNMP_SUCCESS != VcmTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                  i4FreeIfIndex,
                                                  PBB_CREATE_AND_WAIT))
    {
        PBB_LOCK ();
        return PBB_FAILURE;
    }

    if (VcmSetFsVcIfRowStatus (i4FreeIfIndex,
                               (INT4) PBB_CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        PBB_LOCK ();
        return PBB_FAILURE;
    }

    if (VcmTestFsVcId (&u4ErrorCode, i4FreeIfIndex,
                       u4ContextId) != SNMP_SUCCESS)
    {
        if (SNMP_SUCCESS != VcmTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                      i4FreeIfIndex,
                                                      PBB_DESTROY))
        {
            PBB_LOCK ();
            return PBB_FAILURE;
        }

        if (VcmSetFsVcIfRowStatus (i4FreeIfIndex,
                                   (INT4) PBB_DESTROY) != SNMP_SUCCESS)
        {
            PBB_LOCK ();
            return PBB_FAILURE;
        }

        PBB_LOCK ();
        return PBB_FAILURE;
    }

    if (VcmSetFsVcId (i4FreeIfIndex, (INT4) u4ContextId) != SNMP_SUCCESS)
    {
        PBB_LOCK ();
        return PBB_FAILURE;
    }

    if (VcmTestv2FsVcIfRowStatus (&u4ErrorCode, i4FreeIfIndex, PBB_ACTIVE)
        != SNMP_SUCCESS)
    {
        if (SNMP_SUCCESS != VcmTestv2FsVcIfRowStatus (&u4ErrorCode,
                                                      i4FreeIfIndex,
                                                      PBB_DESTROY))
        {
            PBB_LOCK ();
            return PBB_FAILURE;
        }

        if (VcmSetFsVcIfRowStatus (i4FreeIfIndex,
                                   (INT4) PBB_DESTROY) != SNMP_SUCCESS)
        {
            PBB_LOCK ();
            return PBB_FAILURE;
        }
        PBB_LOCK ();
        return PBB_FAILURE;
    }

    if (VcmSetFsVcIfRowStatus (i4FreeIfIndex, PBB_ACTIVE) != SNMP_SUCCESS)
    {
        PBB_LOCK ();
        return PBB_FAILURE;
    }
    PBB_LOCK ();
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbCfaGetIfPipName
 */
/*    Description         : This is a wrapper function to get the 
      Pip Name of a Interface index
 */
/*    Input(s)            : Interface Index Value                            */
/*    Output(s)           : Mac address
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbCfaGetIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pPbbPipName)
{
    INT4                i4retVal = CfaGetIfPipName (i4IfIndex, pPbbPipName);

    if (i4retVal == CFA_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbCfaSetIfPipName                               */
/*    Description         : This is a wrapper function to set the            */
/*                          Pip Name of a Interface index                    */
/*    Input(s)            : Interface Index Value                            */
/*    Output(s)           : Mac address                                      */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                          */
/*****************************************************************************/

INT1
PbbCfaSetIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pPbbPipName)
{
    INT4                i4retVal = CFA_FAILURE;

    PBB_UNLOCK ();

    i4retVal = CfaSetIfPipName (i4IfIndex, pPbbPipName);

    PBB_LOCK ();

    if (i4retVal == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_MACADDR_INVALID);
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbCfaTestIfPipName
 */
/*    Description         : This is a wrapper function to test the 
      Pip Name of a Interface index
 */
/*    Input(s)            : Interface Index Value                            */
/*    Output(s)           : Mac address
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbCfaTestIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pPbbPipName)
{
    INT4                i4retVal = CfaTestIfPipName (i4IfIndex, pPbbPipName);
    if (i4retVal == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_PIPNAME_INVALID);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbCfaGetIfHwAddr                                */
/*    Description         : This is a wrapper function to get the Mac address*/
/*                          of a Interface index                             */
/*    Input(s)            : Interface Index Value                            */
/*    Output(s)           : Mac address                                      */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                          */
/*****************************************************************************/

INT1
PbbCfaGetIfHwAddr (INT4 i4IfIndex, tMacAddr * pu1HwAddr)
{
    INT4                i4retVal = CfaGetIfHwAddr (i4IfIndex, *pu1HwAddr);
    if (i4retVal == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_MACADDR_NOT_PRESENT);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbCfaSetIfHwAddri
 */
/*    Description         : This is a wrapper function to get the 
      Mac address of a Interface index
 */
/*    Input(s)            : Interface Index Value
 *                           Mac address
 Length of Mac Address
 */
/*    Output(s)           : Mac address
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbCfaSetIfHwAddr (INT4 i4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Length)
{
    INT4                i4retVal = CFA_FAILURE;

    PBB_UNLOCK ();

    i4retVal = CfaSetIfPipHwAddr (i4IfIndex, pu1HwAddr, u2Length);

    PBB_LOCK ();

    if (i4retVal == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_MACADDR_INVALID);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbCfaTestIfHwAddri
 */
/*    Description         : This is a wrapper function to test the 
      Mac address of a Interface index
 */
/*    Input(s)            : Interface Index Value
 *                           Mac address
 Length of Mac Address
 */
/*    Output(s)           : Mac address
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbCfaTestIfHwAddr (INT4 i4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Length)
{
    INT4                i4retVal =
        CfaTestIfPipHwAddr (i4IfIndex, pu1HwAddr, u2Length);
    if (i4retVal == CFA_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_MACADDR_INVALID);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : PbbCfaGetIfInfo
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the external modules.
 *
 *    Input(s)             : u4IfIndex, *pIfInfo 
 *
 *    Output(s)            : None.
 *
 *    Returns            :CFA_SUCCESS if u4IfIndex is valid 
 *                        CFA_FAILURE if u4IfIndex is Invalid 
 *****************************************************************************/
INT1
PbbCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    return (CfaGetIfInfo (u4IfIndex, pIfInfo));
}

/*****************************************************************************/
/* Function Name      : PbbVcmGetContextPortList                             */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      interfaces mapped to this context                    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : Interface List                                       */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
PbbVcmGetContextPortList (UINT4 u4ContextId, tPortList PortList)
{
    if (VcmGetContextPortList (u4ContextId, PortList) == VCM_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbVcmGetAliasName                                  */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Alias name of the given context.                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : pu1Alias    - Alias Name                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
PbbVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    INT4                i4retVal = VcmGetAliasName (u4ContextId, pu1Alias);
    if (i4retVal == VCM_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbVcmGetContextInfoFromIfIndex                  */
/*    Description         : This is a wrapper function to find the local 
 *                           port and context id from If Index
 */
/*    Input(s)            : Interface Index Value                            */
/*    Output(s)           : Local port number
 *                           Context Id
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbVcmGetContextInfoFromIfIndex (INT4 i4IfIndex,
                                 UINT4 *pu4ContextId, UINT2 *pu2LocalPortId)
{
    INT4                i4retVal = VcmGetContextInfoFromIfIndex (i4IfIndex,
                                                                 pu4ContextId,
                                                                 pu2LocalPortId);
    if (i4retVal == VCM_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbVcmGetIfMapHlPortId                           */
/*    Description         : This is a wrapper function to find the local port*/
/*                          number from interface index value                */
/*    Input(s)            : Interface Index Value                            */
/*    Output(s)           : Local port number     y                          */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbVcmGetIfMapHlPortId (INT4 i4IfIndex, UINT2 *pu2LocalPortId)
{
    INT4                i4retVal =
        VcmGetIfMapHlPortId (i4IfIndex, pu2LocalPortId);
    if (i4retVal == VCM_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbVcmGetIfIndexFromLocalPort                    */
/*    Description         : This is a wrapper function to find the 
      Interface index
 */
/*                          number from interface index value                */
/*    Input(s)            : Local port number
 */
/*    Output(s)           : Interface Index Value
 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbVcmGetIfIndexFromLocalPort (UINT4 u4ContextId,
                               UINT2 u2LocalPortId, INT4 *pi4IfIndex)
{
    INT4                i4retVal =
        VcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                    (UINT4 *) pi4IfIndex);
    if (i4retVal == VCM_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

INT1
PbbVcmGetSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : VlanVcmGetSystemModeExt                              */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      mode of the system (SI / MI).                        */
/*                                                                           */
/* Input(s)           : u2ProtocolId - Protocol Identifier                   */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
PbbVcmGetSystemModeExt (UINT2 u2ProtocolId)
{
    return (VcmGetSystemModeExt (u2ProtocolId));
}

/*****************************************************************************/
/* Function Name      : PbbVcmIsSwitchExist                                 */
/*                                                                           */
/* Description        : This function calls the VCM Module to check whether  */
/*                      the context exist or not.                            */
/*                                                                           */
/* Input(s)           : pu1Alias - Alias Name                                */
/*                                                                           */
/* Output(s)          : pu4VcNum - Context Identifier                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PbbVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    return (VcmIsSwitchExist (pu1Alias, pu4VcNum));
}

/*****************************************************************************
 *
 *    Function Name        : PbbTestv2FsVcIfRowStatus
 *
 *    Description          : This function is calls VCM function to test destroy 
 *                           of mapping of switch to  VIP rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/
INT4
PbbTestv2FsVcIfRowStatus (UINT4 *pu4ErrorCode, INT4 u4IfIndex, INT4 u4RowStatus)
{
    return VcmTestv2FsVcIfRowStatus (pu4ErrorCode, u4IfIndex, u4RowStatus);
}

/*****************************************************************************
 *
 *    Function Name        : PbbTestv2IfMainRowStatus
 *
 *    Description          : This function is calls CFA function to test destroy of
 *                           VIP ifindex rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/
INT4
PbbTestv2IfMainRowStatus (UINT4 *pu4ErrorCode, INT4 u4IfIndex, INT4 u4RowStatus)
{
    return CfaTestv2IfMainRowStatus (pu4ErrorCode, u4IfIndex, u4RowStatus);
}

/*****************************************************************************
 *
 *    Function Name        : PbbSetFsVcIfRowStatus
 *
 *    Description          : This function is calls VCM function to set destroy 
 *                           of mapping of switch to  VIP rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/
INT4
PbbSetFsVcIfRowStatus (INT4 u4IfIndex, INT4 u4RowStatus)
{
    INT4                i4retVal;
    PBB_UNLOCK ();
    i4retVal = VcmSetFsVcIfRowStatus (u4IfIndex, u4RowStatus);
    PBB_LOCK ();
    return i4retVal;
}

/*****************************************************************************
 *
 *    Function Name        : PbbSetIfMainRowStatus
 *
 *    Description          : This function is calls CFA function to set destroy of
 *                           VIP ifindex rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/
INT4
PbbSetIfMainRowStatus (INT4 u4IfIndex, INT4 u4RowStatus)
{
    INT4                i4retVal;
    PBB_UNLOCK ();
    PBB_PERF_MARK_START_TIME ();
    i4retVal = CfaSetIfMainRowStatus (u4IfIndex, u4RowStatus);
    PBB_PERF_MARK_END_TIME (gu4ISIDDeleteTimeTaken);
    PBB_PERF_PRINT_TIME (gu4ISIDDeleteTimeTaken);
    PBB_PERF_PRINT_TOTTIME (gu4ISIDDeleteTimeTaken, gu4DelVlanIsidTimeTaken);
    PBB_LOCK ();
    return i4retVal;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetDefaultInstanceMacAddress                  */
/*                                                                          */
/*    Description        : This function is invoked to get the Instance     */
/*                         Default MAC address                              */
/*                                                                          */
/*    Input(s)           : u4InstanceId - Instance Identifier               */
/*                                                                          */
/*    Output(s)          : PbbDefaultInstanceMacAddress - Default MAC Addr  */
/*                                                                          */
/*    Returns            : None.                                            */
/****************************************************************************/
VOID
PbbGetDefaultInstanceMacAddress (UINT4 u4InstanceId,
                                 tMacAddr DefInstanceMacAddr)
{
    tMacAddr            au1InstanceMacAddr;
    INT4                i4Index;

    CfaGetSysMacAddress (au1InstanceMacAddr);

    /* Increment the Incstance ID by PBB_MAX_CONTEXTS to ensure uniqueness */
    u4InstanceId += ((UINT4) PBB_MAX_CONTEXTS);

    /* Add the Instance ID to the NVRAM Mac address to get the
       Instance mac address for the Instance */

    for (i4Index = PBB_MAC_ADDR_LEN - 1; i4Index >= 0; i4Index--)
    {
        if ((((UINT4) 255) - au1InstanceMacAddr[i4Index]) > u4InstanceId)
        {
            au1InstanceMacAddr[i4Index] += u4InstanceId;
            break;
        }
        else
        {
            u4InstanceId -= (((UINT4) 255) - au1InstanceMacAddr[i4Index]);
            au1InstanceMacAddr[i4Index] = (UINT1) 255;
        }
    }

    MEMCPY (DefInstanceMacAddr, au1InstanceMacAddr, PBB_MAC_ADDR_LEN);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbL2IwfSetProtocolTunnelStatusOnPort           */
/*                                                                           */
/*    Description         : This function sets the tunnel status (Tunnel/    */
/*                          Peer/Discard/ for different protocols (Dot1x,    */
/*                          LACP, STP, GVRP, GMRP and IGMP) on a port        */
/*                                                                           */
/*    Input(s)            : u2Port         - Port number                     */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on a port.                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
PbbL2IwfSetProtocolTunnelStatusOnPort (UINT4 u4ContextId, UINT2 u2Port,
                                       UINT2 u2Protocol, UINT1 u1TunnelStatus)
{
    return (L2IwfSetProtocolTunnelStatusOnPort (u4ContextId, u2Port,
                                                u2Protocol, u1TunnelStatus));
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbL2IwfDeleteIsid                               */
/*                                                                           */
/*    Description         : This function sends delete indication of ISID    */
/*                          to ECFM                                          */
/*                                                                           */
/*    Input(s)            : u2Port         - Port number                     */
/*                          u2Protocol     - L2 Protocol                     */
/*                          u1TunnelStatus - Protocol tunnel status          */
/*                                           on a port.                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : L2IWF_SUCCESS / L2IWF_FAILURE                    */
/*                                                                           */
/*****************************************************************************/
INT4
PbbL2IwfDeleteIsid (UINT4 u4ContextId, UINT4 u4Isid)
{
    return (L2IwfDeleteIsid (u4ContextId, u4Isid));
}

#ifdef L2RED_WANTED
/*****************************************************************************/
/* Function Name      : PbbRmEnqMsgToRmFromAppl                              */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                         UINT4 u4DestEntId)
{
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
}

/*****************************************************************************/
/* Function Name      : PbbRmGetNodeState                                    */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbRmGetNodeState (VOID)
{
    return (RmGetNodeState ());
}

/*****************************************************************************/
/* Function Name      : PbbRmGetStandbyNodeCount                             */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
PbbRmGetStandbyNodeCount (VOID)
{
    return (RmGetStandbyNodeCount ());
}

/*****************************************************************************/
/* Function Name      : PbbRmGetStaticConfigStatus                           */
/*                                                                           */
/* Description        : This function calls the RM module to get the         */
/*                      status of static configuration restoration.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
PbbRmGetStaticConfigStatus (VOID)
{
    return (RmGetStaticConfigStatus ());
}

/*****************************************************************************/
/* Function Name      : PbbRmHandleProtocolEvent                             */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
PbbRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
    return (RmApiHandleProtocolEvent (pEvt));
}

/*****************************************************************************/
/* Function Name      : PbbRmRegisterProtocols                               */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbRmRegisterProtocols (tRmRegParams * pRmReg)
{
    return (RmRegisterProtocols (pRmReg));
}

/*****************************************************************************/
/* Function Name      : PbbRmDeRegisterProtocols                             */
/*                                                                           */
/* Description        : This function deregisters PBB with RM.               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbRmDeRegisterProtocols (VOID)
{
    return (RmDeRegisterProtocols (RM_PBB_APP_ID));
}

/*****************************************************************************/
/* Function Name      : PbbRmReleaseMemoryForMsg                             */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    return (RmReleaseMemoryForMsg (pu1Block));
}

/*****************************************************************************/
/* Function Name      : PbbRmSetBulkUpdatesStatus                            */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PbbRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
    return (RmSetBulkUpdatesStatus (u4AppId));
}
#endif
