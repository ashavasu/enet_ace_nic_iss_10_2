/*******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbstubs.c,v 1.10 2018/02/09 09:41:16 siva Exp $
 *
 * Description: This file contains stubs for PBB stack compilation.
 *
 *******************************************************************/
#include "pbbinc.h"
#include "pbbport.h"

#ifndef VCM_WANTED
/*****************************************************************************
 *
 *    Function Name        : VcmSetFsVcId
 *
 *    Description          : This function is called from PBB to set the VC id  
 *                           mapping with Vip if index
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/

INT1
VcmSetFsVcId (INT4 i4FsVcmIfIndex, INT4 i4SetValFsVcId)
{
    UNUSED_PARAM (i4FsVcmIfIndex);
    UNUSED_PARAM (i4SetValFsVcId);
    return VLAN_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : VcmTestFsVcId
 *
 *    Description          : This function is called from PBB to test the VC id  
 *                           mapping with Vip if index
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/
INT1
VcmTestFsVcId (UINT4 *pu4ErrorCode, INT4 i4FsVcmIfIndex, INT4 i4TestValFsVcId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsVcmIfIndex);
    UNUSED_PARAM (i4TestValFsVcId);
    return VLAN_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : VcmSetFsVcIfRowStatus
 *
 *    Description          : This function is called from PBB to set destroy 
 *                           of mapping of switch to  VIP rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/
INT4
VcmSetFsVcIfRowStatus (INT4 u4IfIndex, INT4 u4RowStatus)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4RowStatus);
    return VLAN_SUCCESS;

}

/*****************************************************************************
 *
 *    Function Name        : VcmTestv2FsVcIfRowStatus
 *
 *    Description          : This function is called from PBB to test destroy 
 *                           of mapping of switch to  VIP rowstatus.
 *
 *    Input(s)             : None.
 *
 *    Output(s)            : None.
 *
 *    Returns              : None.     
 *
 *****************************************************************************/

INT4
VcmTestv2FsVcIfRowStatus (UINT4 *pu4ErrorCode, INT4 u4IfIndex, INT4 u4RowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4RowStatus);
    return VLAN_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfMapVcId                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will return Context-id which this    */
/*                        interface is mapped.                               */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pu4ContextId   - Context Id.                       */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIfMapVcId (UINT4 u4IfIndex, UINT4 *pu4ContextId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu4ContextId);
    return VLAN_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmIsIfMapExist                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will check whether the mapping entry */
/*                        is exist for the given Interface.                  */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface.                        */
/*                                                                           */
/*     OUTPUT           : None.                                              */
/*                                                                           */
/*     RETURNS          : VCM_TRUE / VCM_FALSE                               */
/*                                                                           */
/*****************************************************************************/

INT4
VcmIsIfMapExist (UINT4 u4IfIndex)
{
    UNUSED_PARAM (u4IfIndex);
    return VLAN_SUCCESS;
}

 /*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : VcmGetIfMapHlPortId                                */
/*                                                                           */
/*     DESCRIPTION      : This function will return corresponding Local port */
/*                        Id for this Interface.                             */
/*                                                                           */
/*     INPUT            : u4IfIndex      - Interface Index.                  */
/*                                                                           */
/*     OUTPUT           : pu2LocalPortId - Local Port Id.                    */
/*                                                                           */
/*     RETURNS          : VCM_SUCCESS / VCM_FAILURE                          */
/*                                                                           */
/*****************************************************************************/
INT4
VcmGetIfMapHlPortId (UINT4 u4IfIndex, UINT2 *pu2LocalPortId)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu2LocalPortId);
    return VLAN_SUCCESS;
}

#endif
#ifndef CLI_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_pbb_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : CLI's main process handler                         */
/*                                                                           */
/*     INPUT            : CliHandle   - handle of CLI                        */
/*                                                                           */
/*     OUTPUT           : u4Command   - cli command                          */
/*                                                                           */
/*     RETURNS          : SUCCESS or FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
cli_process_pbb_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Command);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_pbb_show_cmd                           */
/*                                                                           */
/*     DESCRIPTION      : CLI's main process handler for show commands       */
/*                                                                           */
/*     INPUT            : CliHandle   - handle of CLI                        */
/*                                                                           */
/*     OUTPUT           : u4Command   - cli show command                     */
/*                                                                           */
/*     RETURNS          : SUCCESS or FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_pbb_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (u4Command);
    return CLI_SUCCESS;

}

#endif

#ifndef CFA_WANTED

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaGetNumberOfBebPorts                             */
/*                                                                           */
/*     DESCRIPTION      : This function is called from PBB module to Get     */
/*                        the total number of beb ports                      */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Id in which VIP will exist   */
/*                                                                           */
/*     OUTPUT           : *pu2LocalPortId - Pointer to Local port Id of the  */
/*                         created interface                                 */
/*                        *pu4IfIndex - Pointer to IfIndex of the created    */
/*                         interface                                         */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaGetNumberOfBebPorts (VOID)
{
    return CFA_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : CfaCreateVip                                       */
/*                                                                           */
/*     DESCRIPTION      : This function is called from PBB module to handle  */
/*                        creation of Virtual Intance Port                   */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Id in which VIP will exist   */
/*                                                                           */
/*     OUTPUT           : *pu2LocalPortId - Pointer to Local port Id of the  */
/*                         created interface                                 */
/*                        *pu4IfIndex - Pointer to IfIndex of the created    */
/*                         interface                                         */
/*                                                                           */
/*     RETURNS          : CFA_SUCCESS/CFA_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
CfaCreateVip (INT4 *pi4IfIndex)
{
    UNUSED_PARAM (pi4IfIndex);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfPipName                                      */
/*                                                                           */
/* Description        : This function get the Provider Instance Port name    */
/*                      for the interface                                    */
/*                                                                           */
/* Input(s)           : i4IfIndex:Interface index                            */
/*                                                                           */
/* Output             : pCfaPipName:Pointer to PIP nameNone                  */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pCfaPipName)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pCfaPipName);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfPipName                                      */
/*                                                                           */
/* Description        : This function sets the Provider Instance Port name   */
/*                      for the interface                                    */
/*                                                                           */
/* Input(s)           : i4IfIndex:Interface index                            */
/*                      pCfaPipName:Pointer to PIP name                      */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaSetIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pCfaPipName)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pCfaPipName);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaTestIfPipName                                     */
/*                                                                           */
/* Description        : This function tests the Provider Instance Port name  */
/*                      for the interface                                    */
/*                                                                           */
/* Input(s)           : i4IfIndex:Interface index                            */
/*                      pCfaPipName:Pointer to PIP name                      */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaTestIfPipName (INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pCfaPipName)
{
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pCfaPipName);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaGetIfHwAddr                                       */
/*                                                                           */
/* Description        : This function gets the MacAddress of the interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                                                                           */
/* Output(s)          : pu1HwAddr:Pointer to Mac Address                     */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaGetIfHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1HwAddr);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaSetIfPipHwAddr                                       */
/*                                                                           */
/* Description        : This function sets the MacAddress of the interface   */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      pu1HwAddr:Pointer to Mac Address                     */
/*                      u2Length :Length of the Mac address                  */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS                                          */
/*****************************************************************************/
INT4
CfaSetIfPipHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Length)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1HwAddr);
    UNUSED_PARAM (u2Length);
    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : CfaTestIfPipHwAddr                                      */
/*                                                                           */
/* Description        : This function tests the MacAddress of the interface  */
/*                                                                           */
/* Input(s)           : u4IfIndex:Interface index                            */
/*                      pu1HwAddr:Pointer to Mac Address                     */
/*                      u2Length:Length of Mac Address                       */
/*                                                                           */
/* Output             : None                                                 */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : CFA_SUCCESS/CFA_FAILURE                              */
/*****************************************************************************/
INT4
CfaTestIfPipHwAddr (UINT4 u4IfIndex, UINT1 *pu1HwAddr, UINT2 u2Length)
{
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1HwAddr);
    UNUSED_PARAM (u2Length);
    return CFA_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name       : CfaGetFreeInterfaceIndex
 *
 *    Description         : This function returns the first available
 *                          free interface index of the required type
 *
 *    Input(s)            : u1IfType - Type of free interface required
 *
 *    Output(s)           : pu4Index - Pointer to interface index
 *
 *    Global Variables Referred : None.
 *               
 *    Global Variables Modified : None. 
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if free index is available
 *                         otherwise OSIX_FAILURE.
 *
 *****************************************************************************/
UINT4
CfaGetFreeInterfaceIndex (UINT4 *pu4Index, UINT4 u4IfType)
{
    UNUSED_PARAM (pu4Index);
    UNUSED_PARAM (u4IfType);
    return OSIX_SUCCESS;
}
#endif
