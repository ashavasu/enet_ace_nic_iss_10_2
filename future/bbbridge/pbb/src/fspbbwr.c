/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fspbbwr.c,v 1.3 2011/09/12 06:42:18 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/

# include  "lr.h"
# include  "pbbinc.h"
# include  "fssnmp.h"
# include  "fspbbwr.h"
# include  "fspbbdb.h"

VOID
RegisterFSPBB ()
{
    SNMPRegisterMibWithLock (&fspbbOID, &fspbbEntry, PbbLock, PbbUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fspbbOID, (const UINT1 *) "fspbb");
}

VOID
UnRegisterFSPBB ()
{
    SNMPUnRegisterMib (&fspbbOID, &fspbbEntry);
    SNMPDelSysorEntry (&fspbbOID, (const UINT1 *) "fspbb");
}

INT4
FsPbbShutdownStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbShutdownStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbGlbOUIGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbGlbOUI (pMultiData->pOctetStrValue));
}

INT4
FsPbbMaxNoOfISIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbMaxNoOfISID (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbMaxNoOfISIDPerContextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbMaxNoOfISIDPerContext (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbMaxPortsPerISIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbMaxPortsPerISID (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbMaxPortsPerISIDPerContextGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbMaxPortsPerISIDPerContext
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbMaxCurrentNoOfISIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbMaxCurrentNoOfISID (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbMaxCurrentISIDPerContextGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbMaxCurrentISIDPerContext (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbMaxCurrentPortsPerISIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbMaxCurrentPortsPerISID (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbMaxCurrPortsPerISIDContextGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbMaxCurrPortsPerISIDContext
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbTraceInputGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTraceInput (pMultiData->pOctetStrValue));
}

INT4
FsPbbTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTraceOption (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbShutdownStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbShutdownStatus (pMultiData->i4_SLongValue));
}

INT4
FsPbbGlbOUISet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbGlbOUI (pMultiData->pOctetStrValue));
}

INT4
FsPbbMaxNoOfISIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbMaxNoOfISID (pMultiData->i4_SLongValue));
}

INT4
FsPbbMaxNoOfISIDPerContextSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbMaxNoOfISIDPerContext (pMultiData->i4_SLongValue));
}

INT4
FsPbbMaxPortsPerISIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbMaxPortsPerISID (pMultiData->i4_SLongValue));
}

INT4
FsPbbMaxPortsPerISIDPerContextSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbMaxPortsPerISIDPerContext (pMultiData->i4_SLongValue));
}

INT4
FsPbbTraceInputSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTraceInput (pMultiData->pOctetStrValue));
}

INT4
FsPbbShutdownStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbShutdownStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbGlbOUITest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbGlbOUI (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsPbbMaxNoOfISIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbMaxNoOfISID (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbMaxNoOfISIDPerContextTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbMaxNoOfISIDPerContext
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbMaxPortsPerISIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbMaxPortsPerISID
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbMaxPortsPerISIDPerContextTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbMaxPortsPerISIDPerContext
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbTraceInputTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTraceInput (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsPbbShutdownStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbShutdownStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbGlbOUIDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbGlbOUI (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbMaxNoOfISIDDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbMaxNoOfISID (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbMaxNoOfISIDPerContextDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbMaxNoOfISIDPerContext
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbMaxPortsPerISIDDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbMaxPortsPerISID
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbMaxPortsPerISIDPerContextDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbMaxPortsPerISIDPerContext
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTraceInputDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTraceInput (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbISIDOUITable (tSnmpIndex * pFirstMultiIndex,
                               tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbISIDOUITable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbISIDOUITable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbOUIGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbISIDOUITable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbOUI (pMultiIndex->pIndex[0].i4_SLongValue,
                            pMultiIndex->pIndex[1].u4_ULongValue,
                            pMultiIndex->pIndex[2].i4_SLongValue,
                            pMultiData->pOctetStrValue));

}

INT4
FsPbbOUIRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbISIDOUITable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbOUIRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbOUISet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbOUI (pMultiIndex->pIndex[0].i4_SLongValue,
                            pMultiIndex->pIndex[1].u4_ULongValue,
                            pMultiIndex->pIndex[2].i4_SLongValue,
                            pMultiData->pOctetStrValue));

}

INT4
FsPbbOUIRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbOUIRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsPbbOUITest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbOUI (pu4Error,
                               pMultiIndex->pIndex[0].i4_SLongValue,
                               pMultiIndex->pIndex[1].u4_ULongValue,
                               pMultiIndex->pIndex[2].i4_SLongValue,
                               pMultiData->pOctetStrValue));

}

INT4
FsPbbOUIRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbOUIRowStatus (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiIndex->pIndex[2].i4_SLongValue,
                                        pMultiData->i4_SLongValue));

}

INT4
FsPbbISIDOUITableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbISIDOUITable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbPortPisidTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbPortPisidTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbPortPisidTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbPortPisidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbPortPisidTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbPortPisid (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbPIsidRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbPortPisidTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbPIsidRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbPortPisidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbPortPisid (pMultiIndex->pIndex[0].i4_SLongValue,
                                  pMultiIndex->pIndex[1].i4_SLongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
FsPbbPIsidRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbPIsidRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiIndex->pIndex[1].i4_SLongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsPbbPortPisidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbPortPisid (pu4Error,
                                     pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiIndex->pIndex[1].i4_SLongValue,
                                     pMultiData->i4_SLongValue));

}

INT4
FsPbbPIsidRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbPIsidRowStatus (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsPbbPortPisidTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbPortPisidTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbPortTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbPortPcpSelectionRowGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbPortPcpSelectionRow
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbPortUseDeiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbPortUseDei (pMultiIndex->pIndex[0].i4_SLongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbPortReqDropEncodingGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbPortReqDropEncoding
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbPortPcpSelectionRowSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbPortPcpSelectionRow
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsPbbPortUseDeiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbPortUseDei (pMultiIndex->pIndex[0].i4_SLongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
FsPbbPortReqDropEncodingSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbPortReqDropEncoding
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsPbbPortPcpSelectionRowTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbPortPcpSelectionRow (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsPbbPortUseDeiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbPortUseDei (pu4Error,
                                      pMultiIndex->pIndex[0].i4_SLongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
FsPbbPortReqDropEncodingTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbPortReqDropEncoding (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsPbbPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbPortTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbPcpDecodingTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbPcpDecodingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbPcpDecodingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbPcpDecodingPriorityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbPcpDecodingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbPcpDecodingPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbPcpDecodingDropEligibleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbPcpDecodingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbPcpDecodingDropEligible
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbPcpDecodingPrioritySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbPcpDecodingPriority
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsPbbPcpDecodingDropEligibleSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbPcpDecodingDropEligible
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsPbbPcpDecodingPriorityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbPcpDecodingPriority (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsPbbPcpDecodingDropEligibleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbPcpDecodingDropEligible (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[2].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsPbbPcpDecodingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbPcpDecodingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbPcpEncodingTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbPcpEncodingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbPcpEncodingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbPcpEncodingPcpValueGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbPcpEncodingTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbPcpEncodingPcpValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbPcpEncodingPcpValueSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbPcpEncodingPcpValue
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].i4_SLongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsPbbPcpEncodingPcpValueTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbPcpEncodingPcpValue (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[3].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsPbbPcpEncodingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbPcpEncodingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbInstanceTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbInstanceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbInstanceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbInstanceMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbInstanceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->pOctetStrValue->i4_Length = 6;
    return (nmhGetFsPbbInstanceMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        (tMacAddr *) pMultiData->
                                        pOctetStrValue->pu1_OctetList));

}

INT4
FsPbbInstanceNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbInstanceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbInstanceName (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsPbbInstanceIComponentsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbInstanceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbInstanceIComponents
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPbbInstanceBComponentsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbInstanceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbInstanceBComponents
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPbbInstanceBebPortsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbInstanceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbInstanceBebPorts (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->u4_ULongValue)));

}

INT4
FsPbbInstanceRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbInstanceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbInstanceRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbInstanceMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbInstanceMacAddr (pMultiIndex->pIndex[0].i4_SLongValue,
                                        (*(tMacAddr *) pMultiData->
                                         pOctetStrValue->pu1_OctetList)));

}

INT4
FsPbbInstanceNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbInstanceName (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->pOctetStrValue));

}

INT4
FsPbbInstanceRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbInstanceRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsPbbInstanceMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    return (nmhTestv2FsPbbInstanceMacAddr (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           (*(tMacAddr *) pMultiData->
                                            pOctetStrValue->pu1_OctetList)));

}

INT4
FsPbbInstanceNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbInstanceName (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->pOctetStrValue));

}

INT4
FsPbbInstanceRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbInstanceRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsPbbInstanceTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbInstanceTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbInstanceMappingTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbInstanceMappingTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbInstanceMappingTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbContextToInstanceIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbInstanceMappingTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbContextToInstanceId
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbContextToInstanceIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbContextToInstanceId
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsPbbContextToInstanceIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbContextToInstanceId (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsPbbInstanceMappingTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbInstanceMappingTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
