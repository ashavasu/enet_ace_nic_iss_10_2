/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbutil.c,v 1.36 2014/03/19 13:36:12 siva Exp $
 * Description: This file contains utility routines used in PBB module.
 *
 *******************************************************************/
#include "pbbinc.h"
#include "fspbbcli.h"
#include "fs1ahcli.h"

/****************************************************************************/
/*     FUNCTION NAME    : PbbSetPcpDecodingTable                            */
/*                                                                          */
/*     DESCRIPTION      : This function will configure the priority and drop*/
/*                        eligible indicator for recieved PCP on Particular */
/*                        Port in PCP Decoding Table                        */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                        i4SelRow - selection row identifier               */
/*                        i4Pcp  - Priority code point                      */
/*                        i4Priority - Configured priority                  */
/*                        i4Dei - Drop eligible indicator                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*     $Id: pbbutil.c,v 1.36 2014/03/19 13:36:12 siva Exp $                 */
/****************************************************************************/

INT4
PbbSetPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                        INT4 i4SelRow, INT4 i4Pcp, INT4 i4Priority, INT4 i4Dei)
{
    UINT4               u4ErrCode = PBB_INIT_VAL;
    INT4                i4PrevDei = PBB_INIT_VAL;
    INT4                i4PrevPriority = PBB_INIT_VAL;

    /* Explanation for setting the PCP Decoding table.
     * Example:
     * cases where the normal test and set alone will fail:
     * case1: PCP    Priority    DE
     *         4        5         F
     *         5        6         T
     * If we try to config the valid input (PCP: 5, Pri-5, DE-F), then
     * this routine will return failure.
     * Say if test drop eligible is done first and then the test priority, 
     * the following case 2 may fail.
     * case2: PCP    Priority    DE
     *         4        5         F
     *         5        5         F
     * If we try to config the valid input (PCP: 5, Pri-6, DE-T), then
     * this routine will return failure.
     * For taking care of above scenario,this function is implemented
     * as following:
     *     - Step 1: Test drop eligble
     *               If success set drop eligble.
     *               Test priority
     *               If success set priority 
     *       Step 2: If step 1 fails:
     *               Revert the settings done in step 1.
     *               Test priority
     *               If success set priority
     *               Test drop eligble.
     *               If success set drop eligible.
     *       Step 3: If step 2 fails, revert the settings done in step2 and
     *               return failure. Otherwise return success.*/

    if (nmhGetFsPbbPcpDecodingDropEligible (i4IfIndex, i4SelRow, i4Pcp,
                                            &i4PrevDei) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhGetFsPbbPcpDecodingPriority (i4IfIndex, i4SelRow, i4Pcp,
                                        &i4PrevPriority) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    /*STEP : 1 */

    if (nmhTestv2FsPbbPcpDecodingDropEligible (&u4ErrCode, i4IfIndex, i4SelRow,
                                               i4Pcp, i4Dei) == SNMP_SUCCESS)
    {
        if (nmhSetFsPbbPcpDecodingDropEligible
            (i4IfIndex, i4SelRow, i4Pcp, i4Dei) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsPbbPcpDecodingPriority (&u4ErrCode, i4IfIndex, i4SelRow,
                                               i4Pcp,
                                               i4Priority) == SNMP_SUCCESS)
        {
            if (nmhSetFsPbbPcpDecodingPriority
                (i4IfIndex, i4SelRow, i4Pcp, i4Priority) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            UNUSED_PARAM (CliHandle);

            return CLI_SUCCESS;
        }

        /* Setting in order, drop eligible first and then priority failed.
         * So revert the setting during the operation, and try other
         * order. */
        if (nmhSetFsPbbPcpDecodingDropEligible
            (i4IfIndex, i4SelRow, i4Pcp, i4PrevDei) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /*STEP : 2 */
    if (nmhTestv2FsPbbPcpDecodingPriority (&u4ErrCode, i4IfIndex, i4SelRow,
                                           i4Pcp, i4Priority) == SNMP_SUCCESS)

    {
        if (nmhSetFsPbbPcpDecodingPriority (i4IfIndex, i4SelRow, i4Pcp,
                                            i4Priority) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsPbbPcpDecodingDropEligible (&u4ErrCode, i4IfIndex,
                                                   i4SelRow, i4Pcp,
                                                   i4Dei) == SNMP_SUCCESS)
        {
            if (nmhSetFsPbbPcpDecodingDropEligible (i4IfIndex, i4SelRow,
                                                    i4Pcp,
                                                    i4Dei) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }

            UNUSED_PARAM (CliHandle);
            return CLI_SUCCESS;
        }

        /* Test failed in second order (priority set first and then
         * drop eligible). So revert the settings. */
        /*Restoring the configured Priority */
        if (nmhSetFsPbbPcpDecodingPriority (i4IfIndex, i4SelRow, i4Pcp,
                                            i4PrevPriority) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        CLI_SET_ERR (CLI_PBB_CONF_ERR);
        return CLI_FAILURE;
    }

    /*STEP : 3 */
    CLI_SET_ERR (CLI_PBB_CONF_ERR);
    return CLI_FAILURE;

}

/****************************************************************************/
/*     FUNCTION NAME    : PbbSetPcpEncodingTable                            */
/*                                                                          */
/*     DESCRIPTION      : This function will configure the PCP in the       */
/*                        Priority Encoding Table                           */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                        i4SelRow - selection row identifier               */
/*                        i4Pcp  - configured Priority code point           */
/*                        i4Priority - priority                             */
/*                        i4Dei - Drop eligible indicator                   */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbSetPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                        INT4 i4SelRow, INT4 i4Priority, INT4 i4Pcp, INT4 i4Dei)
{
    UINT4               u4ErrCode = PBB_INIT_VAL;

    if (nmhTestv2FsPbbPcpEncodingPcpValue
        (&u4ErrCode, i4IfIndex, i4SelRow, i4Priority, i4Dei,
         i4Pcp) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_CONF_ERR);
        return CLI_FAILURE;
    }

    if (nmhSetFsPbbPcpEncodingPcpValue (i4IfIndex, i4SelRow,
                                        i4Priority, i4Dei,
                                        i4Pcp) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbResetPcpDecodingTable                          */
/*                                                                          */
/*     DESCRIPTION      : This function resets the PCP decoding table values*/
/*                        to default values.                                */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                        i4SelRow - selection row identifier               */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbResetPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4SelRow)
{
    INT4                i4Priority;
    INT4                i4DropEligible;
    UINT1               u1Pcp;

    for (u1Pcp = PBB_INIT_VAL; u1Pcp < PBB_MAX_NUM_PCP; u1Pcp++)
    {
        PbbGetDecodingPriority ((UINT1) i4SelRow, u1Pcp, &i4Priority);

        PbbGetDecodingDropEligible ((UINT1) i4SelRow, u1Pcp, &i4DropEligible);
        if (i4DropEligible == PBB_FALSE)
        {
            i4DropEligible = PBB_VLAN_SNMP_FALSE;
        }
        else
        {
            i4DropEligible = PBB_VLAN_SNMP_TRUE;
        }

        if (nmhSetFsPbbPcpDecodingPriority (i4IfIndex, i4SelRow, (INT4) u1Pcp,
                                            i4Priority) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetFsPbbPcpDecodingDropEligible
            (i4IfIndex, i4SelRow, (INT4) u1Pcp, i4DropEligible) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbResetPcpEncodingTable                          */
/*                                                                          */
/*     DESCRIPTION      : This function resets the PCP encoding table values*/
/*                        to default values.                                */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                        i4SelRow - selection row identifier               */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbResetPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4SelRow)
{
    INT4                i4Pcp;
    UINT1               u1Priority;

    for (u1Priority = PBB_INIT_VAL; u1Priority <= PBB_MAX_PRIORITY;
         u1Priority++)
    {
        PbbGetEncodingPcpVal ((UINT1) i4SelRow, u1Priority,
                              PBB_DE_FALSE, &i4Pcp);

        if (nmhSetFsPbbPcpEncodingPcpValue (i4IfIndex, i4SelRow, (INT4)
                                            u1Priority, PBB_VLAN_SNMP_FALSE,
                                            i4Pcp) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        PbbGetEncodingPcpVal ((UINT1) i4SelRow, (UINT1) u1Priority,
                              PBB_DE_TRUE, &i4Pcp);

        if (nmhSetFsPbbPcpEncodingPcpValue (i4IfIndex, i4SelRow, (INT4)
                                            u1Priority, PBB_VLAN_SNMP_TRUE,
                                            i4Pcp) != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbSetPortReqDropEncoding                          */
/*                                                                           */
/*     DESCRIPTION      : This function configures the Req drop encoding for */
/*                        Port.                                              */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex  - Interface Index                       */
/*                        i4ReqDropEncoding - TRUE /FALSE                    */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PbbSetPortReqDropEncoding (tCliHandle CliHandle, INT4 i4IfIndex,
                           INT4 i4ReqDropEncoding)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPbbPortReqDropEncoding (&u4ErrorCode, i4IfIndex,
                                           i4ReqDropEncoding) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_CONF_ERR);
        return CLI_FAILURE;
    }

    if (nmhSetFsPbbPortReqDropEncoding (i4IfIndex, i4ReqDropEncoding)
        != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbSetPortPcpSelRow                                */
/*                                                                           */
/*     DESCRIPTION      : This function configures the for Pcp Selection row */
/*                        for a Port.                                        */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex  - Interface Index                       */
/*                        i4PcpSelRow - PBB_8P0D_SEL_ROW /                   */
/*                                      PBB_7P1D_SEL_ROW /                   */
/*                                      PBB_6P2D_SEL_ROW /                   */
/*                                      PBB_5P3D_SEL_ROW                     */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PbbSetPortPcpSelRow (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4PcpSelRow)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPbbPortPcpSelectionRow (&u4ErrorCode, i4IfIndex,
                                           i4PcpSelRow) != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_CONF_ERR);
        return CLI_FAILURE;
    }

    if (nmhSetFsPbbPortPcpSelectionRow (i4IfIndex, i4PcpSelRow) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbSetPortUseDei                                   */
/*                                                                           */
/*     DESCRIPTION      : This function configures the Use Dei for Port      */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex  - Interface Index                       */
/*                        i4UseDei - TRUE /FALSE                             */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PbbSetPortUseDei (tCliHandle CliHandle, INT4 i4IfIndex, INT4 i4UseDei)
{
    UINT4               u4ErrorCode;

    if (nmhTestv2FsPbbPortUseDei (&u4ErrorCode, i4IfIndex, i4UseDei)
        != SNMP_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_CONF_ERR);
        return CLI_FAILURE;
    }

    if (nmhSetFsPbbPortUseDei (i4IfIndex, i4UseDei) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/**************************************************************************/
/* Function Name       : PbbGetPortListForCurrContext                     */
/*                                                                        */
/* Description         : This function extracts the Portlist (local) from */
/*                       the given Portlist (IfIndex based) for the       */
/*                       current context.                                 */
/*                                                                        */
/* Input(s)            : IfPortList    - IfIndex based Portlist           */
/*                                                                        */
/* Output(s)           : LocalPortList - Local port list                  */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
PbbGetPortListForCurrContext (tMbsmPortInfo * pPortInfo,
                              tLocalPortList LocalPortList)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    UINT1               u1IsSetInPortListStatus = OSIX_FALSE;
    for (u2ByteIndex = PBB_INIT_VAL; u2ByteIndex < BRG_PORT_LIST_SIZE;
         u2ByteIndex++)
    {
        u1PortFlag = MBSM_PORT_INFO_PORTLIST (pPortInfo)[u2ByteIndex];
        if (u1PortFlag == PBB_INIT_VAL)
        {
            continue;
        }
        for (u2BitIndex = PBB_INIT_VAL; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                         && (u1PortFlag != PBB_INIT_VAL));
             u2BitIndex++)
        {

            if ((u1PortFlag & PBB_BIT8) != PBB_INIT_VAL)
            {
                i4IfIndex = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                     u2BitIndex + 1);

                OSIX_BITLIST_IS_BIT_SET (MBSM_PORT_INFO_PORTLIST_STATUS
                                         (pPortInfo),
                                         i4IfIndex + CFA_MIN_PSW_IF_INDEX - 1,
                                         sizeof (tMbsmPortInfo),
                                         u1IsSetInPortListStatus);
                if (OSIX_FALSE == u1IsSetInPortListStatus)
                {
                    if (PbbVcmGetContextInfoFromIfIndex (i4IfIndex,
                                                         &u4ContextId,
                                                         &u2LocalPortId) ==
                        PBB_SUCCESS)
                    {
                        if (u4ContextId == PBB_CURR_CONTEXT_ID ())
                        {
                            PBB_SET_MEMBER_PORT (LocalPortList, u2LocalPortId);
                        }
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
}

/**************************************************************************/
/* Function Name       : PbbGetAggPortListForCurrContext                  */
/*                                                                        */
/* Description         : This function extracts the Portlist for          */
/*                       LA Agg ports                                     */
/*                                                                        */
/*                                                                        */
/* Input(s)            : None                                             */
/*                                                                        */
/* Output(s)           : LocalPortList - Local port list                  */
/*                                                                        */
/* Global Variables                                                       */
/* Referred            : None                                             */
/*                                                                        */
/* Global Variables                                                       */
/* Modified            : None                                             */
/*                                                                        */
/* Exceptions or OS                                                       */
/* Error Handling      : None                                             */
/*                                                                        */
/* Use of Recursion    : None                                             */
/*                                                                        */
/* Returns             : None                                             */
/*                                                                        */
/**************************************************************************/
VOID
PbbGetAggPortListForCurrContext (tLocalPortList LocalPortList)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    for (i4IfIndex = BRG_MAX_PHY_PORTS + 1;
         i4IfIndex <= BRG_MAX_PHY_PLUS_LOG_PORTS; i4IfIndex++)
    {

        if (PbbVcmGetContextInfoFromIfIndex (i4IfIndex,
                                             &u4ContextId,
                                             &u2LocalPortId) == PBB_SUCCESS)
        {
            if (u4ContextId == PBB_CURR_CONTEXT_ID ())
            {
                PBB_SET_MEMBER_PORT (LocalPortList, u2LocalPortId);
            }
        }
    }
}

/*****************************************************************************/
/*    Function Name       : PbbGetVIPValue                                   */
/*    Description         : This function returns the Vip value for an ISID  */
/*    Input(s)            : None                                             */
/*    Output(s)           : Context Id
                            Isid
                            Vip                                              */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                          */
/*****************************************************************************/
INT1
PbbGetVIPValue (UINT4 u4ContextId, UINT4 u4Isid, UINT2 *pu2Vip)
{
    INT1                i1retVal = PbbSelectContext (u4ContextId);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }

    if (PBB_BRIDGE_MODE () != PBB_BCOMPONENT_BRIDGE_MODE)
    {
        i1retVal = PbbGetVipForIsid (u4Isid, pu2Vip);
    }
    else
    {
        return PBB_FAILURE;
    }
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetFirstActiveContext                         */
/*                                                                           */
/*    Description         : This function is used to get the first Active    */
/*                          context present in the system.                   */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu4ContextId - Context Id.                       */
/*                                                                           */
/*    Global Variables Referred : gapVlanContextInfo                         */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetFirstActiveContext (UINT4 *pu4ContextId)
{
    UINT4               u4ContextId = PBB_INIT_VAL;

    for (u4ContextId = PBB_INIT_VAL; u4ContextId < PBB_MAX_CONTEXTS;
         u4ContextId++)
    {
        if (gPbbGlobData.apPbbContextInfo[u4ContextId] != NULL)
        {
            *pu4ContextId = u4ContextId;
            return PBB_SUCCESS;
        }
    }
    CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
    return PBB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetNextActiveContext                          */
/*                                                                           */
/*    Description         : This function is used to get the next Active     */
/*                          context present in the system.                   */
/*                                                                           */
/*    Input(s)            : u4CurrContextId - Current Context Id.            */
/*                                                                           */
/*    Output(s)           : pu4NextContextId - Next Context Id.              */
/*                                                                           */
/*    Global Variables Referred : gPbbGlobData.apPbbContextInfo              */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetNextActiveContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4ContextId = PBB_INIT_VAL;

    for (u4ContextId = u4CurrContextId + 1;
         u4ContextId < PBB_MAX_CONTEXTS; u4ContextId++)
    {
        if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
        {
            *pu4NextContextId = u4ContextId;
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateContextId                             */
/*                                                                          */
/*    Description        : This function is used to validate context id     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidateContextId (UINT4 u4contextid)
{
    u4contextid = u4contextid;

    if (PbbSelectContext (u4contextid) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCompareCBPPort                                */
/*                                                                           */
/*    Description         : This function is used to compare the two CBP     */
/*                          Port entries                                     */
/*                                                                           */
/*    Input(s)            : pCBPNode1,pCBPNode1 - Two node enteries          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pCBPNode1 > pCBPNode2                        */
/*                          -1 - pCBPNode1 < pCBPNode2                       */
/*                          0 - RBNode1 == RBNode12                          */
/*                                                                           */
/*****************************************************************************/
INT1
PbbCompareCBPPort (UINT2 *pu2CBPPort1, UINT2 *pu2CBPPort2)
{
    if (*pu2CBPPort1 < *pu2CBPPort2)
    {
        return PBB_LESSER;
    }
    else if (*pu2CBPPort1 > *pu2CBPPort2)
    {
        return PBB_GREATER;
    }
    return PBB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCompareCnpPort                                */
/*                                                                           */
/*    Description         : This function is used to compare the two CNP     */
/*                          Port entries                                     */
/*                                                                           */
/*    Input(s)            : pCBPNode1,pCBPNode1 - Two node enteries          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pCBPNode1 > pCBPNode2                        */
/*                          -1 - pCBPNode1 < pCBPNode2                       */
/*                          0 - RBNode1 == RBNode12                          */
/*                                                                           */
/*****************************************************************************/
INT1
PbbCompareCnpPort (UINT2 *pu2CnpPort1, UINT2 *pu2CnpPort2)
{
    if (*pu2CnpPort1 < *pu2CnpPort2)
    {
        return PBB_LESSER;
    }
    else if (*pu2CnpPort1 > *pu2CnpPort2)
    {
        return PBB_GREATER;
    }
    return PBB_EQUAL;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbAllocMemDllNode                               */
/*                                                                          */
/*    Description        : This function Allocates memory for               */
/*                           ISID CBP DLL Node                                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
tPbbCbpPortList    *
PbbAllocMemDllNode ()
{
    tPbbCbpPortList    *pCBPDllNode = NULL;

    pCBPDllNode =
        (tPbbCbpPortList *) (VOID *) PBB_GET_BUF (PBB_ISID_CBP_DLL_BUFF,
                                                  sizeof (tPbbCbpPortList));
    if (pCBPDllNode == NULL)
    {
        return NULL;
    }
    return pCBPDllNode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbAllocMemCnpDllNode                            */
/*                                                                          */
/*    Description        : This function Allocates memory for               */
/*                           ISID CNP DLL Node                                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
tPbbCnpPortList    *
PbbAllocMemCnpDllNode ()
{
    tPbbCnpPortList    *pCnpDllNode = NULL;

    pCnpDllNode =
        (tPbbCnpPortList *) (VOID *) PBB_GET_BUF (PBB_ISID_CNP_DLL_BUFF,
                                                  sizeof (tPbbCnpPortList));
    if (pCnpDllNode == NULL)
    {
        return NULL;
    }
    return pCnpDllNode;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetFreeVIPPort                                */
/*    Description         : This function is used to get the free VIP value  */
/*                          corresponding to an ISID.                        */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu4Vip -VIP value                                */
/*                                                                           */
/*    Global Variables Referred : gPbbGlobData.apPbbContextInfo              */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetFreeVipPort (UINT4 u4ContextId, UINT4 u4Isid, UINT2 *pu2Vip)
{
    INT4                i4FreeIfIndex = PBB_INIT_VAL;
    UINT2               u2FreeLocalPort = PBB_INIT_VAL;
    /*validating Context Id */
    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }
    /*Get free local port fot a context */
    if (PbbCfaCreateVip (&i4FreeIfIndex) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    if (PbbMapVcmPort (i4FreeIfIndex, u4ContextId) != PBB_SUCCESS)
    {
        if (PbbSetIfMainRowStatus (i4FreeIfIndex, PBB_DESTROY) == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }
        return PBB_FAILURE;
    }
    if (PbbVcmGetContextInfoFromIfIndex ((UINT4) i4FreeIfIndex, &u4ContextId,
                                         &u2FreeLocalPort) == VCM_FAILURE)
    {
        return PBB_FAILURE;
    }

    if (PbbCfaSetIfMainBrgPortType
        (i4FreeIfIndex, (UINT1) PBB_VIRTUAL_INSTANCE_PORT) != SNMP_SUCCESS)
    {
        if (PbbSetFsVcIfRowStatus ((INT4) i4FreeIfIndex,
                                   (INT4) PBB_DESTROY) == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }
        if (PbbSetIfMainRowStatus (i4FreeIfIndex, PBB_DESTROY) == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }
        return PBB_FAILURE;
    }
    /*Assigning VIP */
    *pu2Vip = u2FreeLocalPort;
    UNUSED_PARAM (u4Isid);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbAllocMemIsidVipDllNode                        */
/*                                                                          */
/*    Description        : This function Allocates memory for               */
/*                           ISID CBP DLL Node                                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

tPbbIsidPipVipNode *
PbbAllocMemIsidVipDllNode ()
{
    tPbbIsidPipVipNode *pIsidDllNode = NULL;

    pIsidDllNode =
        (tPbbIsidPipVipNode *) (VOID *) PBB_GET_BUF (PBB_ISID_PIP_VIP_DLL_BUFF,
                                                     sizeof
                                                     (tPbbIsidPipVipNode));
    if (pIsidDllNode == NULL)
    {
        return NULL;
    }
    return pIsidDllNode;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCompareIsidEntry                              */
/*                                                                           */
/*    Description         : This function is used to compare the two Isid/
 *                          VIP entries                                      */
/*    Input(s)            : pu4Isid,pu4Isid - Two node enteries              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pIsid > pIsid                                */
/*                          -1 - pIsid < pIsid                               */
/*                          0 - pIsid == pIsid                               */
/*                                                                           */
/*****************************************************************************/
INT1
PbbCompareIsidEntry (UINT4 *pu4IsidVip1, UINT4 *pu4IsidVip2)
{

    if (*pu4IsidVip1 < *pu4IsidVip2)
    {
        return PBB_LESSER;
    }
    else if (*pu4IsidVip1 > *pu4IsidVip2)
    {
        return PBB_GREATER;
    }
    return PBB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCompareVipEntry                           */
/*                                                                           */
/*    Description         : This function is used to compare the two Isid/
 *                          VIP entries
*/
/*    Input(s)            : pu4IsidVip1,pu4IsidVip2 - Two node enteries     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pVip1 > pVip2                                */
/*                          -1 - pVip1 < pVip2                               */
/*                          0 - pVip1 == pVip2                               */
/*                                                                           */
/*****************************************************************************/
INT1
PbbCompareVipEntry (UINT2 *pu2Vip1, UINT2 *pu2Vip2)
{

    if (*pu2Vip1 < *pu2Vip2)
    {
        return PBB_LESSER;
    }
    else if (*pu2Vip1 > *pu2Vip2)
    {
        return PBB_GREATER;
    }
    return PBB_EQUAL;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetComponentIdOfIsid                          */
/*                                                                          */
/*    Description        : This function is used to get context id for a ISID*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetComponentIdOfIsid (UINT4 i4Isid, UINT4 *i4ComponentId)
{
    UINT4               u4Context = PBB_INIT_VAL;
    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);

        if (gPbbGlobData.au1PbbStatus[u4Context] == PBB_ENABLED)
        {
            if (PbbValidateIsid (i4Isid) == PBB_SUCCESS)
            {
                *i4ComponentId = u4Context;
                return PBB_SUCCESS;

            }
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidInContext                              */
/*                                                                          */
/*    Description        : This function is used to get  ISID               */
/*                          currently present in this context               */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidInContext (UINT4 u4ContextId, UINT4 u4CurrIsid, UINT4 *pu4Nextisid)
{
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4NextIsid = PBB_INIT_VAL;
    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }
    if (u4CurrIsid == PBB_INIT_VAL)
    {
        if (PbbGetFirstIsid (&u4Isid) != PBB_SUCCESS)
        {
            return PBB_FAILURE;

        }
        *pu4Nextisid = u4Isid;
    }
    else
    {
        if (PbbGetNextIsid (u4CurrIsid, &u4NextIsid) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        *pu4Nextisid = u4NextIsid;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDelVipInfo                                    */
/*                                                                          */
/*    Description        : This function is used to delete                  */
/*                          VIP node from RBtree                            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDelVipInfo (UINT2 u2LocalPort)
{
    UINT4               u4Isid = PBB_ISID_INVALID_VALUE;
    if (PbbGetVipIsidonLocal (u2LocalPort, &u4Isid) == PBB_SUCCESS)
    {
        if (u4Isid != PBB_ISID_INVALID_VALUE)
        {
            if (PbbDeleteIsidNodeFromVip (u4Isid) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
        }
    }
    if (PbbDeleteVipRBNodes (u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetBackboneEdgeBridgeAddress                  */
/*                                                                          */
/*    Description        :This function returns the value for Bridge Address*/
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetBackboneEdgeBridgeAddress (tMacAddr SwitchMac)
{
    CfaGetSysMacAddress (SwitchMac);
    PBB_MEMCPY (gPbbGlobData.PbbTaskInfo.BridgeMacAddr, SwitchMac,
                sizeof (tMacAddr));

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetBackboneEdgeBridgeName                  */
/*                                                                          */
/*    Description        :This function returns the value for Bridge Name   */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetBackboneEdgeBridgeName (tSNMP_OCTET_STRING_TYPE *
                              pRetValArPbbBackboneEdgeBridgeName)
{
    pRetValArPbbBackboneEdgeBridgeName->i4_Length =
        PBB_STRLEN (gPbbGlobData.PbbTaskInfo.au1BridgeName);
    if (PBB_STRLEN (gPbbGlobData.PbbTaskInfo.au1BridgeName) <
        PBB_BRIDGE_NAME_LEN)
    {
        PBB_MEMCPY (pRetValArPbbBackboneEdgeBridgeName->pu1_OctetList,
                    gPbbGlobData.PbbTaskInfo.au1BridgeName,
                    pRetValArPbbBackboneEdgeBridgeName->i4_Length);
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNumberOfIComponents                        */
/*                                                                          */
/*    Description        :This function Set the value for Bridge Name       */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetNumberOfIComponents (UINT4 *pu4NumofIComp)
{
    *pu4NumofIComp = gPbbGlobData.PbbTaskInfo.u4NumberofIcomp;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNumberOfBComponents                        */
/*                                                                          */
/*    Description        :This function Set the value for Bridge Name       */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetNumberOfBComponents (UINT4 *pu4NumofBComp)
{
    *pu4NumofBComp = gPbbGlobData.PbbTaskInfo.u4NumberofBcomp;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetBackboneEdgeBridgeName                     */
/*                                                                          */
/*    Description        :This function Set the value for Bridge Name       */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetBackboneEdgeBridgeName (tSNMP_OCTET_STRING_TYPE *
                              pSetValArPbbBackboneEdgeBridgeName)
{
    INT4                i4Length = PBB_INIT_VAL;
    i4Length = pSetValArPbbBackboneEdgeBridgeName->i4_Length;
    PBB_MEMSET (gPbbGlobData.PbbTaskInfo.au1BridgeName,
                PBB_INIT_VAL, PBB_BRIDGE_NAME_LEN);
    PBB_MEMCPY (gPbbGlobData.PbbTaskInfo.au1BridgeName,
                pSetValArPbbBackboneEdgeBridgeName->pu1_OctetList, i4Length);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPbbGlbOUI                                  */
/*                                                                          */
/*    Description        :This function return the value for Global OUI     */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetPbbGlbOUI (tSNMP_OCTET_STRING_TYPE * pRetValArPbbGlbOUI)
{

    PBB_MEMCPY (pRetValArPbbGlbOUI->pu1_OctetList,
                PBB_GLOBAL_OUI, PBB_OUI_LENGTH);
    pRetValArPbbGlbOUI->i4_Length = PBB_OUI_LENGTH;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPbbGlbOUI                                  */
/*                                                                          */
/*    Description        :This function Set the value for Global OUI        */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetPbbGlbOUI (tSNMP_OCTET_STRING_TYPE * pSetValArPbbGlbOUI)
{

    PBB_MEMCPY (PBB_GLOBAL_OUI, pSetValArPbbGlbOUI->pu1_OctetList,
                pSetValArPbbGlbOUI->i4_Length);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetShutdownStatus                             */
/*                                                                          */
/*    Description        :This function return the value for shutdown status*/
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetShutdownStatus (INT4 *pi4RetValArPbbShutdownStatus)
{
    *pi4RetValArPbbShutdownStatus = gPbbGlobData.au1PbbShutDownStatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetShutdownStatus                             */
/*                                                                          */
/*    Description        :This function set the value for shutdown status   */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetShutdownStatus (INT4 i4SetValArPbbShutdownStatus)
{
    gPbbGlobData.au1PbbShutDownStatus = (UINT1) i4SetValArPbbShutdownStatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMaxNoOfISID                                */
/*                                                                          */
/*    Description        :This function return the value                    */
/*                          for Maximum number of ISID                      */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetMaxNoOfISID (INT4 *pi4RetValArPbbMaxNoOfISID)
{
    *pi4RetValArPbbMaxNoOfISID = gPbbGlobData.PbbTaskInfo.u4MaxIsid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMaxNoOfISIDPerContext                      */
/*                                                                          */
/*    Description        :This function return the value                    */
/*                          for Maximum number of  Per context ISID         */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetMaxNoOfISIDPerContext (INT4 *pi4RetValArPbbMaxNoOfISIDPerContext)
{
    *pi4RetValArPbbMaxNoOfISIDPerContext =
        gPbbGlobData.PbbTaskInfo.u4MaxNumIsidPerContext;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMaxCurrPortsPerISID                        */
/*                                                                          */
/*    Description        :This function return the value                    */
/*                          for Maximum current number of Port Per ISID     */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetMaxCurrPortsPerISID (INT4 *pi4RetValArPbbMaxCurrPortsPerISID)
{
    *pi4RetValArPbbMaxCurrPortsPerISID =
        gPbbGlobData.PbbTaskInfo.u4MaxPortperIsid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMaxPortsPerISID                            */
/*                                                                          */
/*    Description        :This function return the value                    */
/*                          for Maximum number of Port Per ISID             */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetMaxPortsPerISID (INT4 *pi4RetValArPbbMaxPortsPerISID)
{
    *pi4RetValArPbbMaxPortsPerISID = gPbbGlobData.PbbTaskInfo.u4MaxPortperIsid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMaxCurrPortPerISIDPerCntxt                  */
/*                                                                          */
/*    Description        :This function return the value for Maximum 
 *                        current number of Port Per ISID per Context 
 */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetMaxCurrPortPerISIDPerCntxt (INT4
                                  *pi4RetValArPbbMaxCurrPortsPerISIDPerContext)
{
    *pi4RetValArPbbMaxCurrPortsPerISIDPerContext =
        gPbbGlobData.PbbTaskInfo.u4MaxPortperIsidPerContext;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMaxPortsPerISIDPerContext                  */
/*                                                                          */
/*    Description        :This function return the value                    */
/*                          for Maximum number of Port Per ISID per Context */
/*                                                                          */
/*    Input(s)           : SwitchMac                                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetMaxPortsPerISIDPerContext (INT4 *pi4RetValArPbbMaxPortsPerISIDPerContext)
{
    *pi4RetValArPbbMaxPortsPerISIDPerContext =
        gPbbGlobData.PbbTaskInfo.u4MaxPortperIsidPerContext;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbGetVipListForVlanPvid                          */
/*                                                                          */
/*     DESCRIPTION      : This function will Set Customer Mac Limit         */
/*                                                                          */
/*     INPUT            : u4ContextId context id whose vip list is to       */
/*                        returned                                          */
/*     OUTPUT           : au1VipList- vip list to be returned to vlan       */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbGetVipListForVlanPvid (UINT4 u4ContextId, UINT1 *au1VipList)
{
    UINT4               u4FirstContextId = PBB_INIT_VAL;
    UINT4               u4FirstPortId = PBB_INIT_VAL;
    UINT4               u4NextPortId = PBB_INIT_VAL;
    UINT4               u4NextContextId = PBB_INIT_VAL;

    PBB_LOCK ();
    nmhGetFirstIndexFsPbbVipTable ((INT4 *) &u4NextContextId,
                                   (INT4 *) &u4NextPortId);
    while (u4NextContextId != u4ContextId)
    {
        u4FirstContextId = u4NextContextId;
        u4FirstPortId = u4NextPortId;
        if (nmhGetNextIndexFsPbbVipTable
            ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
             (INT4) u4FirstPortId, (INT4 *) &u4NextPortId) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
    }
    while (u4NextContextId == u4ContextId)
    {
        PBB_SET_MEMBER_PORT (au1VipList, u4NextPortId);
        u4FirstContextId = u4NextContextId;
        u4FirstPortId = u4NextPortId;

        if (nmhGetNextIndexFsPbbVipTable
            ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
             (INT4) u4FirstPortId, (INT4 *) &u4NextPortId) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
    }
    PBB_UNLOCK ();
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbIsShutDown                                     */
/*                                                                          */
/*     DESCRIPTION      : This function will be called by VLAN to get       */
/*                         shutdown  status                                 */
/*                                                                          */
/*     INPUT            :                                                   */
/*     OUTPUT           :                                                   */
/*                                                                          */
/*     RETURNS          : PBB_TRUE/PBB_FALSE                                */
/*                                                                          */
/****************************************************************************/
INT1
PbbIsShutDown ()
{
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        return PBB_TRUE;
    }
    else
    {
        return PBB_FALSE;
    }
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbGetPipWithPortList                             */
/*                                                                          */
/*     DESCRIPTION      : This function will be called by VLAN to get       */
/*                         shutdown  status                                 */
/*                                                                          */
/*     INPUT            :                                                   */
/*     OUTPUT           :                                                   */
/*                                                                          */
/*     RETURNS          : PBB_TRUE/PBB_FALSE                                */
/*                                                                          */
/****************************************************************************/
INT4
PbbGetPipWithPortList (UINT4 u4ContextId, UINT2 u2LocalPort,
                       tLocalPortList InPortList, tLocalPortList OutPortList)
{
    u4ContextId = u4ContextId;
    u2LocalPort = u2LocalPort;
    InPortList = InPortList;
    OutPortList = OutPortList;
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbVlanGetMemberVlanList                         */
/*                                                                           */
/*    Description         : This function sets the VIP as the Member port     */
/*                            for the given Vlan Range                         */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                        : pVlanIdList - VLAN List for which the VIP is to  */
/*                                          to be added                         */
/*                        : u2VipPort -   VIP                                 */
/*                        : u1IsTagged -  PBB_TRUE = Tagged                     */
/*                                           PBB_FALSE = Untagged                 */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_SUCCESS -                                 */
/*                                                                           */
/*****************************************************************************/

INT1
PbbVlanGetMemberVlanList (UINT4 u4ContextId,
                          tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                          UINT2 u2VipPort, UINT1 u1IsTagged)
{
    if (u1IsTagged == PBB_TRUE)
    {
        u1IsTagged = L2_TAGGED_MEMBER_PORT;
    }
    else
    {
        u1IsTagged = L2_UNTAGGED_MEMBER_PORT;
    }
    L2IwfGetPortVlanList (u4ContextId, pVlanIdList, u2VipPort, u1IsTagged);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidOfVip
 */
/*                                                                          */
/*    Description        : This function is called by Vlan module to get 
                           the Isid associated with a Vip
 */
/*                                                                          */
/*    Input(s)           : u2LocalPort u1PortStatus                         */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT4
PbbGetIsidOfVip (UINT4 *pu4Isid, UINT4 u4IfIndex, UINT4 u4ContextId)
{
    PBB_LOCK ();
    nmhGetFsPbbVipISid (u4ContextId, u4IfIndex, pu4Isid);
    if (*pu4Isid == 1)
    {
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    PBB_UNLOCK ();
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbSendFrameToPort
 Description :  To get Isid tag information from the protocl frame 
 Input       :  Context Id - context id
                 u2LocalPort -  port on which frame received
                pBuf - PDU buffer
                u4IsidTagOffSet - offset where I-tag info present in buf
                
 Output      :  pPbbTag
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT4
PbbSendFrameToPort (tCRU_BUF_CHAIN_DESC * pFrame,
                    tVlanOutIfMsg * pVlanOutIfMsg,
                    UINT4 u4ContextId, UINT2 u2Port,
                    UINT4 u4Isid, UINT1 u1RegenPri)
{
    UINT4               u4IfIndex;
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u1RegenPri);
    /* select context */
    PBB_LOCK ();
    if (PbbSelectContext (u4ContextId) == VLAN_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pFrame, FALSE);
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    /* get Ifindex for the port */
    if (VcmGetIfIndexFromLocalPort (u4ContextId, u2Port,
                                    &u4IfIndex) != VCM_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pFrame, FALSE);
        PbbReleaseContext ();
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }

    if (pVlanOutIfMsg->u1FrameType == VLAN_CFM_FRAME)
    {
        L2IwfHandleOutgoingPktOnPort (pFrame, u4IfIndex,
                                      pVlanOutIfMsg->u2Length, CFA_PROT_BPDU,
                                      CFA_ENCAP_NONE);

    }
    else
    {

        L2IwfHandleOutgoingPktOnPort (pFrame, u4IfIndex,
                                      pVlanOutIfMsg->u2Length, 0,
                                      CFA_ENCAP_NONE);
    }

    PbbReleaseContext ();
    PBB_UNLOCK ();
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetAllPorts                                  */
/*                                                                           */
/*    Description         : This function scans through the L2IWF port table */
/*                          and issues create port indications to PBB       */
/*                          module.                                          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetAllPorts (UINT4 u4ContextId)
{
    UINT4               u4IfIndex;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    INT4                i4RetVal;
    UINT2               u2LocalPort;
    UINT2               u2PrevPort;
    UINT1               u1OperStatus;

    u2PrevPort = 0;
    PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4ContextId);

    if (PbbL2IwfGetBridgeMode (PBB_CURR_CONTEXT_ID (), &u4BridgeMode) !=
        PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_BRIDGE_MODE () = u4BridgeMode;
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        return PBB_SUCCESS;
    }
    while (PBB_L2IWF_GETNEXT_VALID_PORT (PBB_CURR_CONTEXT_ID (),
                                         u2PrevPort, &u2LocalPort,
                                         &u4IfIndex) == L2IWF_SUCCESS)
    {
        if (PbbL2IwfIsPortInPortChannel ((INT4) u4IfIndex) == L2IWF_SUCCESS)
        {
            u2PrevPort = u2LocalPort;
            continue;
        }
        i4RetVal = PbbValidatePort (u4IfIndex);

        if (i4RetVal == PBB_FAILURE)
        {

            /* For ports that are newly created in Pbb, set the default
             * PB port type. */

            /* Set the default bridge port type for this bridge in CFA.
             * As Pbb Create port will call Cfa to get the bridge port type,
             * we have to update Cfa before that. */

            i4RetVal = PbbHandleCreatePort (PBB_CURR_CONTEXT_ID (), u4IfIndex,
                                            u2LocalPort);

            if (i4RetVal == PBB_FAILURE)
            {

                u2PrevPort = u2LocalPort;
                continue;
            }
        }

        PbbL2IwfGetPortOperStatus (PBB_MODULE, (UINT2) u4IfIndex,
                                   &u1OperStatus);

        if (u1OperStatus == CFA_IF_UP)
        {
            PbbSetPortStatus (PBB_CURR_CONTEXT_ID (), u2LocalPort,
                              PBB_PORT_STATUS_UP);
        }

        u2PrevPort = u2LocalPort;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDelVipData                                    */
/*                                                                          */
/*    Description        : This function is used to delete                  */
/*                          VIP Data from RBtree                            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDelVipData (INT4 i4ifIndex)
{
    tPbbICompVipRBtreeNode *pRBNodeCur = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2Vip = u2LocalPort;
    pRBNodeCur =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBNodeCur == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNodeCur->pu4PbbIsidVipInfo;
    PBB_MEMSET (pIsidVipInfo->DefBackDestMac, PBB_INIT_VAL, sizeof (tMacAddr));
    pIsidVipInfo->unPbbCompData.ICompData.u1VipType = PBB_VIP_TYPE_BOTH;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetDefaultDestMacInServiceMapEntry            */
/*                                                                          */
/*    Description        : This function is used to configure the, Default  */
/*                         backbone destination address for a CBP in        */
/*                         Service mapping table.                           */
/*                                                                          */
/*                         Before configuring the MacAddress, RowStatus will*/
/*                         be made NotInService.                            */
/*                                                                          */
/*    Input(s)           : u4ContextId   - ContextID                        */
/*                         u4IfIndex     - IfIndex of the CBP Port          */
/*                         u4Isid        - Service Instance Identifier      */
/*                         SetValFsPbbDestinationMacAddr - Mac Address to   */
/*                                                         be configured.   */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/*                                                                          */
/****************************************************************************/
INT1
PbbSetDefaultDestMacInServiceMapEntry (UINT4 u4ContextId, UINT4 u4IfIndex,
                                       UINT4 u4Isid,
                                       UINT1 *SetValFsPbbDestinationMacAddr)
{
    UINT4               u4ErrCode;
    INT4                i4RetValCBPRowStatus;
    UINT1               u1bool = PBB_TRUE;

    if (nmhGetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4IfIndex, u4Isid,
                                               &i4RetValCBPRowStatus)
        == SNMP_FAILURE)
    {                            /*CBP not created */
        return PBB_FAILURE;
    }

    /*set RS to not in service */
    if (i4RetValCBPRowStatus == ACTIVE)
    {
        if (nmhTestv2FsPbbCBPServiceMappingRowStatus (&u4ErrCode, u4ContextId,
                                                      u4IfIndex, u4Isid,
                                                      PBB_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }
        if (nmhSetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4IfIndex,
                                                   u4Isid, PBB_NOT_IN_SERVICE)
            == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }
    }

    /*modify dest mac address */
    if (nmhTestv2FsPbbCBPServiceMappingDefaultBackboneDest
        (&u4ErrCode, u4ContextId, u4IfIndex, u4Isid,
         SetValFsPbbDestinationMacAddr) == SNMP_SUCCESS)
    {
        /* MODIFY DestMacAddr */
        if (nmhSetFsPbbCBPServiceMappingDefaultBackboneDest
            (u4ContextId, u4IfIndex, u4Isid,
             SetValFsPbbDestinationMacAddr) == SNMP_FAILURE)
        {
            u1bool = PBB_FALSE;
        }
    }
    else
    {
        u1bool = PBB_FALSE;
    }

    if (i4RetValCBPRowStatus == ACTIVE)
    {
        /* Set the Row Status back to previous RowStatus both in case of 
         * failure as well as success */
        if (nmhTestv2FsPbbCBPServiceMappingRowStatus (&u4ErrCode, u4ContextId,
                                                      u4IfIndex, u4Isid,
                                                      i4RetValCBPRowStatus)
            == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }

        if (nmhSetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4IfIndex,
                                                   u4Isid, i4RetValCBPRowStatus)
            == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }
    }

    return ((u1bool == PBB_FALSE) ? PBB_FAILURE : PBB_SUCCESS);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPbbGlbOuiInContext                         */
/*                                                                          */
/*    Description        : This function is used to Set                     */
/*                          Global Oui In context                           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetPbbGlbOuiInContext ()
{
    tPbbICompVipRBtreeNode *pRBCurrNode;
    tPbbICompVipRBtreeNode *pRBNextNode;
    tPbbBackBoneSerInstEntry *pRBCurrIsidPortNode = NULL;
    tPbbBackBoneSerInstEntry *pRBNextIsidPortNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;
    INT4                i4RowStatus = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    for (u4ContextId = 0; u4ContextId < PBB_MAX_CONTEXTS; u4ContextId++)
    {
        if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4ContextId);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                pRBCurrNode = (tPbbICompVipRBtreeNode *)
                    RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->VipTable);
                if (pRBCurrNode == NULL)
                {
                    continue;
                }
                pIsidVipInfo =
                    (tPbbIsidVipInfo *) (VOID *) pRBCurrNode->pu4PbbIsidVipInfo;
                u2Vip = pRBCurrNode->u2Vip;
                u4Isid = pIsidVipInfo->u4Isid;
                if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                   u2Vip,
                                                   &i4IfIndex) != PBB_SUCCESS)
                {
                    continue;
                }

                if (nmhGetFsPbbVipRowStatus (u4ContextId, i4IfIndex,
                                             &i4RowStatus) == SNMP_FAILURE)
                {
                    continue;
                }

                if (PbbSelectContext (u4ContextId) == PBB_SUCCESS)
                {
                    if (i4RowStatus == PBB_ACTIVE)
                    {

                        if (PbbSetICompHwServiceInst (u4ContextId,
                                                      i4IfIndex,
                                                      u4Isid) != PBB_SUCCESS)
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    continue;
                }

                while (1)
                {
                    pIsidVipInfo = NULL;
                    pRBNextNode =
                        (tPbbICompVipRBtreeNode *)
                        RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                       pRBCurrNode,
                                       (tRBCompareFn) PbbCompareVipRBNodes);
                    if (pRBNextNode == NULL)
                    {
                        break;
                    }
                    pIsidVipInfo =
                        (tPbbIsidVipInfo *) (VOID *) pRBNextNode->
                        pu4PbbIsidVipInfo;
                    u2Vip = pRBNextNode->u2Vip;
                    u4Isid = pIsidVipInfo->u4Isid;
                    if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                       u2Vip,
                                                       &i4IfIndex) !=
                        PBB_SUCCESS)
                    {
                        break;
                    }
                    if (nmhGetFsPbbVipRowStatus (u4ContextId, i4IfIndex,
                                                 &i4RowStatus) == SNMP_FAILURE)
                    {
                        break;
                    }

                    if (PbbSelectContext (u4ContextId) == PBB_SUCCESS)
                    {
                        if (i4RowStatus == PBB_ACTIVE)
                        {

                            if (PbbSetICompHwServiceInst (u4ContextId,
                                                          i4IfIndex,
                                                          u4Isid) !=
                                PBB_SUCCESS)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }

                    pRBCurrNode = pRBNextNode;
                    pRBNextNode = NULL;

                }                /* End of While */
            }
            else if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
            {

                pRBCurrIsidPortNode =
                    (tPbbBackBoneSerInstEntry *)
                    RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->IsidPortTable);
                if (pRBCurrIsidPortNode == NULL)
                {
                    continue;
                }
                u2LocalPort = pRBCurrIsidPortNode->u2LocalPort;
                u4Isid = pRBCurrIsidPortNode->u4Isid;
                if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                   u2LocalPort,
                                                   &i4IfIndex) != PBB_SUCCESS)
                {
                    continue;
                }
                if (nmhGetFsPbbCBPServiceMappingRowStatus
                    (u4ContextId, i4IfIndex, u4Isid,
                     &i4RowStatus) == SNMP_FAILURE)
                {
                    continue;
                }

                if (PbbSelectContext (u4ContextId) == PBB_SUCCESS)
                {
                    if (i4RowStatus == PBB_ACTIVE)
                    {
                        if (PbbSetBCompHwServiceInst (u4ContextId,
                                                      u4Isid,
                                                      i4IfIndex) != PBB_SUCCESS)
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    continue;
                }

                while (1)
                {
                    pRBNextIsidPortNode =
                        (tPbbBackBoneSerInstEntry *)
                        RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->IsidPortTable,
                                       pRBCurrIsidPortNode,
                                       (tRBCompareFn)
                                       PbbCompareIsidPortRBNodes);
                    if (pRBNextIsidPortNode == NULL)
                    {
                        break;
                    }
                    u2LocalPort = pRBNextIsidPortNode->u2LocalPort;
                    u4Isid = pRBNextIsidPortNode->u4Isid;
                    if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                       u2LocalPort,
                                                       &i4IfIndex) !=
                        PBB_SUCCESS)
                    {
                        break;
                    }
                    if (nmhGetFsPbbCBPServiceMappingRowStatus
                        (u4ContextId, i4IfIndex, u4Isid,
                         &i4RowStatus) == SNMP_FAILURE)
                    {
                        break;
                    }

                    if (PbbSelectContext (u4ContextId) == PBB_SUCCESS)
                    {
                        if (i4RowStatus == PBB_ACTIVE)
                        {
                            if (PbbSetBCompHwServiceInst (u4ContextId,
                                                          u4Isid,
                                                          i4IfIndex) !=
                                PBB_SUCCESS)
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        break;
                    }
                    pRBCurrIsidPortNode = pRBNextIsidPortNode;
                    pRBNextIsidPortNode = NULL;
                }                /* End of While */

            }

        }
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateLocalIsid                             */
/*                                                                          */
/*    Description        : This function is used to Validate                */
/*                         the Local ISID                                   */
/*                                                                          */
/*    Input(s)           : u4LocalIsid - Local Isid to be verified          */
/*                         u4Isid - Backbone Isid                           */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidateLocalIsid (UINT4 u4LocalIsid, UINT4 u4Isid)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry *pRBNextNode = NULL;

    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                                     IsidPortTable);

    if (pRBCurNode == NULL)
    {
        return PBB_SUCCESS;

    }

    if (u4LocalIsid == PBB_ISID_INVALID_VALUE)
    {
        return PBB_SUCCESS;
    }

    if (pRBCurNode->u4Isid != u4Isid)
    {
        if (pRBCurNode->u4LocalIsid == u4LocalIsid)
        {
            return PBB_FAILURE;
        }
    }

    while (1)
    {
        pRBNextNode =
            (tPbbBackBoneSerInstEntry *)
            RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->IsidPortTable, pRBCurNode,
                           (tRBCompareFn) PbbCompareIsidPortRBNodes);

        if (pRBNextNode == NULL)
        {
            /* Element not present in the key */
            return PBB_SUCCESS;
        }

        if (pRBNextNode->u4Isid != u4Isid)
        {
            if (pRBNextNode->u4LocalIsid == u4LocalIsid)
            {
                return PBB_FAILURE;
            }
        }

        pRBCurNode = pRBNextNode;
        pRBNextNode = NULL;
    }

    return PBB_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PbbUtilGetTraceInputValue
 *
 *    DESCRIPTION      : This function gives the trace string depending upon
 *                       the trace option.
 *
 *    INPUT            : pu1TraceInput - trace string.
 *                       u4TraceOption  - trace option.
 *
 *    OUTPUT           : Gives the string format trace option.
 *
 *    RETURNS          : trace string length.
 *
 ****************************************************************************/

UINT4
PbbUtilGetTraceInputValue (UINT1 *pu1TraceInput, UINT4 u4TraceOption)
{
    UINT4               u4TrcLen = 0;
    UINT1               u2BitNumber = 0;
    UINT1               u1MaxBits = 0;

    /* store starting address of trace input buffer pointer */
    UINT1              *pu1TmpTraceInput = pu1TraceInput;

    /* trace string */
    UINT1               au1TraceString[32][11] =
        { " init-shut", " mgmt", " data-path", " ctrl", " pkt-dump",
        " resource", " all-fail", " buf",
        " critical", " redundancy", " null"
    };

    /* All trace */
    if ((u4TraceOption & PBB_ALL_TRC) == PBB_ALL_TRC)
    {
        STRNCPY (pu1TraceInput, "all", STRLEN ("all"));
        u4TrcLen = STRLEN ("all");
        return u4TrcLen;
    }
    u1MaxBits = sizeof (u4TraceOption) * BITS_PER_BYTE;

    for (u2BitNumber = 0; u2BitNumber < u1MaxBits; u2BitNumber++)
    {
        if ((u4TraceOption >> u2BitNumber) & OSIX_TRUE)
        {
            /* if it is the first trace input string being copied to the
             * buffer, then " "(space) is not required in the beginning of the
             * string, so copy from 2nd byte of the string */
            if (pu1TraceInput == pu1TmpTraceInput)
            {
                STRNCPY (pu1TraceInput, (au1TraceString[u2BitNumber] + 1),
                         (STRLEN (au1TraceString[u2BitNumber]) - 1));

                pu1TraceInput += (STRLEN (au1TraceString[u2BitNumber]) - 1);

                u4TrcLen += (STRLEN (au1TraceString[u2BitNumber]) - 1);
                continue;
            }
            STRNCPY (pu1TraceInput, au1TraceString[u2BitNumber],
                     STRLEN (au1TraceString[u2BitNumber]));
            pu1TraceInput += STRLEN (au1TraceString[u2BitNumber]);
            u4TrcLen += STRLEN (au1TraceString[u2BitNumber]);
        }
    }

    return u4TrcLen;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : PbbUtilGetTraceOptionValue
 *
 *    DESCRIPTION      : This function process given trace input and sets the
 *                       corresponding option bit.
 *
 *    INPUT            : pu1TraceInput - trace string.
 *
 *    OUTPUT           : None.
 *
 *    RETURNS          : Option Value.
 *
 ****************************************************************************/
UINT4
PbbUtilGetTraceOptionValue (UINT1 *pu1TraceInput, INT4 i4Tracelen)
{
#define PBB_MAX_TRACE_TOKENS      32
#define PBB_MAX_TRACE_TOKEN_SIZE  12
#define PBB_TRACE_TOKEN_DELIMITER ' '    /* space */
    UINT4               u4TraceOption = PBB_INVALID_TRC;
    UINT1               aau1Tokens[PBB_MAX_TRACE_TOKENS]
        [PBB_MAX_TRACE_TOKEN_SIZE];
    UINT1              *apu1Tokens[PBB_MAX_TRACE_TOKENS];
    UINT1               u1Count = 0;
    UINT1               u1TokenCount = 0;

    MEMSET (aau1Tokens, 0, sizeof (aau1Tokens));

    /* Enable */
    if (!STRNCMP (pu1TraceInput, "enable ", STRLEN ("enable ")))
    {
        pu1TraceInput += STRLEN ("enable ");
        i4Tracelen -= STRLEN ("enable ");
    }
    /* Disable */
    else if (!STRNCMP (pu1TraceInput, "disable ", STRLEN ("disable ")))
    {
        pu1TraceInput += STRLEN ("disable ");
        i4Tracelen -= STRLEN ("disable ");
    }
    else
    {
        return PBB_INVALID_TRC;
    }

    if (!STRNCMP (pu1TraceInput, "all", STRLEN ("all")))
    {
        /* For traces,
         * All trace         : all
         * All failure trace : all-fail 
         * the first 3 characters are common(all), so verify that
         * the trace length is equal to 3(strlen("all")), if yes then set
         * all tlv trace and return, else if the trace length is greater
         * than STRLEN ("all"), then verify whether the trace is "all-fail" 
         * or not. If the trace is not "all-fail" then the trace is considered
         * as invalid invalid trace so return invalid trace */
        if (i4Tracelen == STRLEN ("all"))
        {
            u4TraceOption = PBB_ALL_TRC;
            return u4TraceOption;
        }
        else if (i4Tracelen > ((INT4) STRLEN ("all")))
        {
            if (STRNCMP (pu1TraceInput, "all-fail", STRLEN ("all-fail")) != 0)
            {
                return PBB_INVALID_TRC;
            }
        }
    }

    /* assign memory address for all the pointers in token array(apu1Tokens) */
    for (u1Count = 0; u1Count < PBB_MAX_TRACE_TOKENS; u1Count++)
    {
        apu1Tokens[u1Count] = aau1Tokens[u1Count];
    }

    /* get the tokens from the trace input buffer */
    PbbUtilSplitStrToTokens (pu1TraceInput, i4Tracelen,
                             (UINT1) PBB_TRACE_TOKEN_DELIMITER,
                             (UINT2) PBB_MAX_TRACE_TOKENS, apu1Tokens,
                             &u1TokenCount);

    /* get tokens one by one from the token array and set the 
     * trace options based on the tokens */
    for (u1Count = 0; u1Count < u1TokenCount; u1Count++)
    {
        /* set the trace option based on the give token */
        PbbUtilSetTraceOption (apu1Tokens[u1Count], &u4TraceOption);
        /* if invalid trace option is returned by the function 
         * PbbUtilSetTraceOption, then dont continue the for loop, just
         * return with invalid trace option */
        if (u4TraceOption == PBB_INVALID_TRC)
        {
            return u4TraceOption;
        }
    }
    return u4TraceOption;
}

/******************************************************************************
 * Function Name      : PbbUtilSplitStrToTokens 
 *
 * Description        : This function splits the given string into tokens
 *                      based on the given token delimiter and stores the
 *                      tokens in token array and returns the token array.
 *
 * Input(s)           : pu1InputStr  - Pointer to input string
 *                      i4Strlen     - String length
 *                      u1Delimiter  - Delimiter by which the token has to be
 *                                     seperated
 *                      u2MaxToken    - Max token count
 *                      apu1Token     - Poiner to token array
 *                      pu1TokenCount - Token count
 *
 * Output(s)          : apu1Token     - Poiner to token array
 *                      pu1TokenCount - Token count
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
PbbUtilSplitStrToTokens (UINT1 *pu1InputStr, INT4 i4Strlen,
                         UINT1 u1Delimiter, UINT2 u2MaxToken,
                         UINT1 *apu1Token[], UINT1 *pu1TokenCount)
{
    UINT1               u1TokenSize = 0;
    UINT1               u1TokenCount = 0;

    while ((i4Strlen > 0) && (u1TokenCount < u2MaxToken))
    {
        /* reset token size to zero */
        u1TokenSize = 0;
        /* scan for delimiter */
        while ((i4Strlen > 0) && (*(pu1InputStr + u1TokenSize) != u1Delimiter))
        {
            i4Strlen--;
            u1TokenSize++;
        }
        /* since the loop breaks whenever delimiter is found the string length
         * and token size shoud be updated once(outside the loop) by the size 
         * of delimiter(which is 1byte) */
        i4Strlen--;
        u1TokenSize++;
        /* copy the token excluding delimiter */
        STRNCPY (apu1Token[u1TokenCount], pu1InputStr,
                 (u1TokenSize - sizeof (u1Delimiter)));
        /* move the input string pointer to point to next token */
        pu1InputStr += u1TokenSize;
        /* increment the token count */
        u1TokenCount++;
    }
    /* update the token count */
    *pu1TokenCount = u1TokenCount;
    return;
}

/******************************************************************************
 * Function Name      : PbbUtilSetTraceOption 
 *
 * Description        : This function sets the trace option based on the given
 *                      trace token
 *
 * Input(s)           : apu1Token      - Poiner to token array 
 *                      pu4TraceOption - Pointer to trace option 
 *
 * Output(s)          : pu4TraceOption - Pointer to trace option 
 *
 * Return Value(s)    : None
 *****************************************************************************/
VOID
PbbUtilSetTraceOption (UINT1 *pu1Token, UINT4 *pu4TraceOption)
{
    if (!STRNCMP (pu1Token, "init-shut", STRLEN ("init-shut")))
    {
        *pu4TraceOption |= INIT_SHUT_TRC;
    }
    else if (!STRNCMP (pu1Token, "mgmt", STRLEN ("mgmt")))
    {
        *pu4TraceOption |= MGMT_TRC;
    }
    else if (!STRNCMP (pu1Token, "data-path", STRLEN ("data-path")))
    {
        *pu4TraceOption |= DATA_PATH_TRC;
    }
    else if (!STRNCMP (pu1Token, "ctrl", STRLEN ("ctrl")))
    {
        *pu4TraceOption |= CONTROL_PLANE_TRC;
    }
    else if (!STRNCMP (pu1Token, "pkt-dump", STRLEN ("pkt-dump")))
    {
        *pu4TraceOption |= DUMP_TRC;
    }
    else if (!STRNCMP (pu1Token, "resource", STRLEN ("resource")))
    {
        *pu4TraceOption |= OS_RESOURCE_TRC;
    }
    else if (!STRNCMP (pu1Token, "all-fail", STRLEN ("all-fail")))
    {
        *pu4TraceOption |= ALL_FAILURE_TRC;
    }
    else if (!STRNCMP (pu1Token, "buf", STRLEN ("buf")))
    {
        *pu4TraceOption |= BUFFER_TRC;
    }
    else if (!STRNCMP (pu1Token, "critical", STRLEN ("critical")))
    {
        *pu4TraceOption |= PBB_CRITICAL_TRC;
    }
    else if (!STRNCMP (pu1Token, "redundancy", STRLEN ("redundancy")))
    {
        *pu4TraceOption |= PBB_RED_TRC;
    }
    else
    {
        *pu4TraceOption = PBB_INVALID_TRC;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbHandleGlbShutdown                             */
/*                                                                           */
/*    Description         : This function scans through all the contexts     */
/*                          and issues a delet ctx so as to invoke cleanup   */
/*                          npapis for global shitdown.                      */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS.                                      */
/*                                                                           */
/*****************************************************************************/
VOID
PbbHandleGlbShutdown (VOID)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4InstanceId = PBB_INIT_VAL;

    for (u4ContextId = 0; u4ContextId < PBB_MAX_CONTEXTS; u4ContextId++)
    {
        if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4ContextId);
            PbbHandleDeleteContext (PBB_CURR_CONTEXT_ID ());
        }
    }

    for (u4InstanceId = 0; u4InstanceId < PBB_MAX_INSTANCES; u4InstanceId++)
    {
        if (PBB_INSTANCE_PTR (u4InstanceId) != NULL)
        {
            PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);
            PBB_RELEASE_BUF (PBB_INSTANCE_BUFF, PBB_CURR_INSTANCE_PTR ());
            PBB_INSTANCE_PTR (u4InstanceId) = NULL;
            PBB_CURR_INSTANCE_PTR () = NULL;
        }
    }
    return;
}

INT4
PBBGetBDAFromOUI (tSNMP_OCTET_STRING_TYPE * pOUI, UINT4 u4Isid,
                  tMacAddr BackboneDest)
{
    UINT4               u4SizeofMac = PBB_SIZE_OF_MAC_ADDR;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT1               u1ByteIndex = PBB_INIT_VAL;

    PBB_MEMCPY (BackboneDest, pOUI->pu1_OctetList, PBB_OUI_LENGTH);
    BackboneDest[0] = BackboneDest[0] | 0x01;
    for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
    {
        u1ByteIndex = 0xFF & u4Isid;
        BackboneDest[u4Index] = u1ByteIndex;
        u4Isid = u4Isid >> 8;
        u1ByteIndex = PBB_INIT_VAL;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetDecodingPriority                        */
/*                                                                           */
/*    Description         : This function is used by CLI routine to get the  */
/*                          Default Priority in a PCP Decoding Table for a   */
/*                          particular row and Pcp value                     */
/*                                                                           */
/*    Input(s)            : u1SelRow - Pcp Selection Row                     */
/*                          u1PcpVal - Priority Code Point Value             */
/*                                                                           */
/*    Output(s)           : *pi4Priority - Default Priority val              */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PbbGetDecodingPriority (UINT1 u1PcpSelRow, UINT1 u1PcpVal, INT4 *pi4Priority)
{

    *pi4Priority = gPbbGlobData.au1PcpDecPriority[u1PcpSelRow - 1][u1PcpVal];
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetDecodingDropEligible                    */
/*                                                                           */
/*    Description         : This function is used by CLI routine to get the  */
/*                          Default Drop eligible in a PCP Decoding Table for a*/
/*                          particular row and Pcp value                     */
/*                                                                           */
/*    Input(s)            : u1SelRow - Pcp Selection Row                     */
/*                          u1PcpVal - Priority Code Point Value             */
/*                                                                           */
/*    Output(s)           : *pi4DropEligible - Default DropEligible          */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/

VOID
PbbGetDecodingDropEligible (UINT1 u1PcpSelRow, UINT1 u1PcpVal,
                            INT4 *pi4DropEligible)
{
    *pi4DropEligible =
        gPbbGlobData.au1PcpDecDropEligible[u1PcpSelRow - 1][u1PcpVal];
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetEncodingPcpVal                          */
/*                                                                           */
/*    Description         : This function is used by CLI routine to get the  */
/*                          Default Pcp Value in a PCP Encoding Table for a  */
/*                          particular row , Priority and Drop Eligible      */
/*                                                                           */
/*    Input(s)            : u1SelRow - Pcp Selection Row                     */
/*                          u1Priority - Priority Value                      */
/*                          u1DropEligible - DropEligible Value              */
/*                                                                           */
/*    Output(s)           : *pi4PcpVal - Default Pcp                         */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PbbGetEncodingPcpVal (UINT1 u1PcpSelRow, UINT1 u1Priority, UINT1 u1DropEligible,
                      INT4 *pi4PcpVal)
{

    *pi4PcpVal =
        gPbbGlobData.au1PcpEncValue[u1PcpSelRow -
                                    1][u1Priority][u1DropEligible];
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbSetPipVipListOperStatus                       */
/*                                                                           */
/*    Description         : This function is used get the Vip List for the   */
/*                          given Pip. And set the Vip Oper Status of each   */
/*                          Vip                                              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                          u2Pip - Pip Local Port                           */
/*                          u1PortStatus - Pip Oper Status                   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_SUCCESS / PBB_FAILURE                  */
/*                                                                           */
/*****************************************************************************/
INT4
PbbSetPipVipListOperStatus (UINT4 u4ContextId, UINT2 u2Pip, UINT1 u1PortStatus)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbIsidPipVipNode *pCurEntry = NULL;
    tTMO_DLL_NODE      *pNode = NULL;
    tPbbPortRBtreeNode  key;
    UINT2               u2Vip = PBB_INIT_VAL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));

    key.u2LocalPort = u2Pip;
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.PipData.VipList),
                  pNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbIsidPipVipNode *) pNode;

        u2Vip = pCurEntry->unIsidVip.u2Vip;

        /* Set the Vip Oper Status */
        PbbSetVipOperStatus (u4ContextId, PBB_INIT_VAL, u2Vip, u1PortStatus);
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbSetVipOperStatus                              */
/*                                                                           */
/*    Description         : This function is used for setting the VIP Oper   */
/*                          status flag in L2iwf Based on the following      */
/*                          Conditions: 1) Vip - ISID Mapping                */
/*                          2) Vip - Pip Mapping                             */
/*                          3) Pip - Operstatus                              */
/*                       -  If the Admin status of VIP is up and the above   */
/*                          conditions are met then trigger CFA to set the   */
/*                          Oper Status of VIP.                              */
/*                       -  If the Oper status of VIP is up and the above    */
/*                          conditions are not met then trigger CFA to reset */
/*                          the Oper Status of VIP.                          */
/*                                                                           */
/*    Input(s)            : i4ContextId - Context Id                         */
/*                          i4VipIndex - Vip Interface Index                 */
/*                          u2Vip - Vip Local Port                           */
/*                          u1PipPortStatus - Pip Port Status                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_SUCEESS/ PBB_FAILURE                   */
/*                                                                           */
/*****************************************************************************/
INT4
PbbSetVipOperStatus (INT4 i4ContextId, INT4 i4VipIndex, UINT2 u2Vip,
                     UINT1 u1PipPortStatus)
{
    tCfaIfInfo          CfaIfInfo;
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbPortRBtreeNode *pRBNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbICompVipRBtreeNode vipKey;
    tPbbPortRBtreeNode  pipKey;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2Pip = PBB_INIT_VAL;
    UINT1               u1VipPipRowstatus = PBB_INIT_VAL;
    UINT1               u1VipRowstatus = PBB_INIT_VAL;
    UINT1               u1VipOperStatusFlag = CFA_FALSE;

    if ((i4VipIndex == PBB_INIT_VAL) && (u2Vip == PBB_INIT_VAL))
    {
        return PBB_FAILURE;
    }

    PBB_MEMSET (&vipKey, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    PBB_MEMSET (&CfaIfInfo, PBB_INIT_VAL, sizeof (tCfaIfInfo));

    /* In case Local Port is not available  */
    if (u2Vip == PBB_INIT_VAL)
    {
        if (PbbVcmGetIfMapHlPortId (i4VipIndex, &u2Vip) == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }
    }

    /* In case Interface Index is not available  */
    if (i4VipIndex == PBB_INIT_VAL)
    {
        if (PbbVcmGetIfIndexFromLocalPort (i4ContextId, u2Vip, &i4VipIndex)
            == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }
    }

    vipKey.u2Vip = u2Vip;
    pPbbVipCurrEntry =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &vipKey);
    if (pPbbVipCurrEntry == NULL)
    {
        /* Element not present in the vipKey */
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;

    if (pIsidVipInfo == NULL)
    {
        return PBB_FAILURE;
    }

    u4Isid = pIsidVipInfo->u4Isid;
    u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
    u1VipRowstatus = pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus;
    u1VipPipRowstatus = pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus;

    /* Get the Pip Oper Status */
    if (u1PipPortStatus == PBB_INIT_VAL)
    {
        PBB_MEMSET (&pipKey, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
        pipKey.u2LocalPort = u2Pip;
        pRBNode =
            (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                              PortTable, &pipKey);
        if (pRBNode == NULL)
        {
            return PBB_FAILURE;
        }

        u1PipPortStatus = pRBNode->u1PortStatus;
    }

    /* Check the Conditions for setting the OperStatus Flag */
    if ((u2Pip != PBB_INIT_VAL) &&
        (u4Isid != PBB_ISID_INVALID_VALUE) &&
        (u1VipRowstatus == PBB_ACTIVE) &&
        (u1VipPipRowstatus == PBB_ACTIVE) &&
        (u1PipPortStatus == PBB_PORT_STATUS_UP))
    {
        u1VipOperStatusFlag = CFA_TRUE;
    }

    /* Set the Flag in L2IWF */
    if (PbbL2IwfSetVipOperStatusFlag (i4VipIndex, u1VipOperStatusFlag)
        == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }

    if (PbbCfaGetIfInfo (i4VipIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (u1VipOperStatusFlag == CFA_TRUE)
    {
        if (CfaIfInfo.u1IfAdminStatus == CFA_IF_UP)
        {
            /* If the Oper status is DOWN then send the trigger
               to set the OperStatus as UP */
            if (CfaIfInfo.u1IfOperStatus == CFA_IF_DOWN)
            {
                if (PbbL2IwfUpdateVipOperStatus (i4VipIndex,
                                                 CFA_IF_UP) != PBB_SUCCESS)
                {
                    return PBB_FAILURE;
                }
            }
        }
    }
    else
    {
        /* If the Oper status is already UP then send the trigger
           to reset the OperStatus as DOWN */
        if (CfaIfInfo.u1IfOperStatus == CFA_IF_UP)
        {
            if (PbbL2IwfUpdateVipOperStatus (i4VipIndex,
                                             CFA_IF_DOWN) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
        }
    }

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateInstanceId                            */
/*                                                                          */
/*    Description        : This function is used to validate instanceid     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidateInstanceId (UINT4 u4InstanceId)
{
    if (u4InstanceId >= PBB_MAX_INSTANCES)
    {
        PBB_TRC_ARG4 (MGMT_TRC, "%s :: %s() :: fails since input backbone "
                      "instance id : %d is greater than maximum number of "
                      "instances(%d)\n", __FILE__, PBB_FUNCTION_NAME,
                      u4InstanceId, PBB_MAX_CONTEXTS);
        return PBB_FAILURE;
    }
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);

    if (PBB_CURR_INSTANCE_PTR () == NULL)
    {
        PBB_TRC_ARG3 (MGMT_TRC, "%s :: %s() :: fails since instance id"
                      ": %d is not created in PBB\n",
                      __FILE__, PBB_FUNCTION_NAME, u4InstanceId);
        return PBB_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : IsPbbInstanceMappedToContext                     */
/*                                                                           */
/*    Description         : This function is used to check the given         */
/*                          is mapped to any context or not.                 */
/*                                                                           */
/*    Input(s)            : u4InstanceId - Instance id                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_SUCCESS / PBB_FAILURE                  */
/*                                                                           */
/*****************************************************************************/
INT1
PbbIsInstanceMappedToContext (UINT4 u4InstanceId)
{
    UINT4               u4Context = PBB_INIT_VAL;
    for (; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_TRUE)
            {
                if (PBB_CURR_CONTEXT_PTR ()->u4InstanceId == u4InstanceId)
                {
                    return PBB_SUCCESS;
                }
            }
        }
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetInstanceIdFromAlias                        */
/*                                                                           */
/*    Description         : This function is used to get the instance id     */
/*                          from the alias name                              */
/*                                                                           */
/*    Input(s)            : Alias Name                                       */
/*                                                                           */
/*    Output(s)           : u4InstanceId - Instance id                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_SUCCESS / PBB_FAILURE                  */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetInstanceIdFromAlias (tSNMP_OCTET_STRING_TYPE * pu1InstanceName,
                           UINT4 *pu4InstanceId)
{
    UINT4               u4Instance = PBB_INIT_VAL;
    for (; u4Instance < PBB_MAX_INSTANCES; u4Instance++)
    {
        if (PBB_INSTANCE_PTR (u4Instance) != NULL)
        {
            PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4Instance);
            if (PBB_MEMCMP
                (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
                 pu1InstanceName->pu1_OctetList,
                 sizeof (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr)) ==
                PBB_INIT_VAL)
            {
                *pu4InstanceId = u4Instance;
                return PBB_SUCCESS;
            }
        }
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetFreeInstanceId                             */
/*                                                                           */
/*    Description         : This function is used to get the free instance id*/
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : u4InstanceId - Instance id                       */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_SUCCESS / PBB_FAILURE                  */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetFreeInstanceId (UINT4 *pu4InstanceId)
{
    UINT4               u4Instance = PBB_INIT_VAL;
    u4Instance++;
    for (; u4Instance < PBB_MAX_INSTANCES; u4Instance++)
    {
        if (PBB_INSTANCE_PTR (u4Instance) == NULL)
        {
            *pu4InstanceId = u4Instance;
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbIncrInstanceNumOfIcomp                        */
/*                                                                           */
/*    Description         : This function is used to increment the number    */
/*                          of I components for instance of given context    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PbbIncrInstanceNumOfIcomp ()
{
    PBB_CURR_INSTANCE_PTR () =
        PBB_INSTANCE_PTR (PBB_CURR_CONTEXT_PTR ()->u4InstanceId);
    if (PBB_CURR_INSTANCE_PTR () != NULL)
    {
        PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfIcomp++;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbIncrInstanceNumOfBcomp                        */
/*                                                                           */
/*    Description         : This function is used to increment the number    */
/*                          of B components for instance of given context    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PbbIncrInstanceNumOfBcomp ()
{
    PBB_CURR_INSTANCE_PTR () =
        PBB_INSTANCE_PTR (PBB_CURR_CONTEXT_PTR ()->u4InstanceId);
    if (PBB_CURR_INSTANCE_PTR () != NULL)
    {
        PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfBcomp++;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbDecrInstanceNumOfIcomp                        */
/*                                                                           */
/*    Description         : This function is used to decrement the number    */
/*                          of I components for instance of given context    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PbbDecrInstanceNumOfIcomp ()
{
    PBB_CURR_INSTANCE_PTR () =
        PBB_INSTANCE_PTR (PBB_CURR_CONTEXT_PTR ()->u4InstanceId);
    if (PBB_CURR_INSTANCE_PTR () != NULL)
    {
        PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfIcomp--;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbDecrInstanceNumOfBcomp                        */
/*                                                                           */
/*    Description         : This function is used to decrement the number    */
/*                          of B components for instance of given context    */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : None                                       */
/*                                                                           */
/*****************************************************************************/
VOID
PbbDecrInstanceNumOfBcomp ()
{
    PBB_CURR_INSTANCE_PTR () =
        PBB_INSTANCE_PTR (PBB_CURR_CONTEXT_PTR ()->u4InstanceId);
    if (PBB_CURR_INSTANCE_PTR () != NULL)
    {
        PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfBcomp--;
    }
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbUpdateInstanceId                              */
/*                                                                           */
/*    Description         : This function is used to update the instance id  */
/*                          for a context                                    */
/*                                                                           */
/*    Input(s)            : Context Id                                       */
/*                          Instance Id                                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling     : None.                                      */
/*                                                                           */
/*    Use of Recursion          : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_SUCCESS / PBB_FAILURE                  */
/*                                                                           */
/*****************************************************************************/
INT1
PbbUpdateInstanceId (UINT4 u4ContextId, UINT4 u4InstanceId)
{
    UINT4               u4OldInstanceId = PBB_INIT_VAL;
    if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    /* Update the old instance's number of I/B components and BEB ports */
    u4OldInstanceId = PBB_CURR_CONTEXT_PTR ()->u4InstanceId;

    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4OldInstanceId);
    if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        PbbDecrInstanceNumOfIcomp ();
    }
    else
    {
        PbbDecrInstanceNumOfBcomp ();
    }
    PBB_CURR_INSTANCE_PTR () = NULL;
    if (PbbDeleteContextNodeFromDLL (u4OldInstanceId,
                                     u4ContextId) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }

    /* Update the new instance's number of I/B components and BEB ports */

    PBB_CURR_CONTEXT_PTR ()->u4InstanceId = u4InstanceId;
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);
    if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        PbbIncrInstanceNumOfIcomp ();
    }
    else
    {
        PbbIncrInstanceNumOfBcomp ();
    }
    PBB_CURR_INSTANCE_PTR () = NULL;
    PBB_CURR_CONTEXT_PTR () = NULL;
    if (PbbCreateContextEntryDll (u4InstanceId, u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCompareContext                                */
/*                                                                           */
/*    Description         : This function is used to compare the two Context */
/*                          Values                                           */
/*                                                                           */
/*    Input(s)            : pContext1,pContext2 - Two node enteries          */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pContext1 > pContext2                        */
/*                          -1 - pContext1 < pContext2                       */
/*                          0 - pContext1 == pContext2                       */
/*                                                                           */
/*****************************************************************************/
INT1
PbbCompareContext (UINT4 *pu4Context1, UINT4 *pu4Context2)
{
    if (*pu4Context1 < *pu4Context2)
    {
        return PBB_LESSER;
    }
    else if (*pu4Context1 > *pu4Context2)
    {
        return PBB_GREATER;
    }
    return PBB_EQUAL;
}

/*****************************************************************************/
/*    Function Name       : PbbHandleShutdownStatus                          */
/*                                                                           */
/*    Description         : This function is used to start or shutdown the   */
/*                          PBB module based on the value of shutdown status */
/*                                                                           */
/*    Input(s)            : i4ShutdownStatus - Shutdown Status               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : PBB_SUCCESS / PBB_FAILURE                        */
/*****************************************************************************/
INT1
PbbHandleShutdownStatus (INT4 i4ShutdownStatus)
{
    UINT1               u1Status = L2IWF_FALSE;

    if (i4ShutdownStatus == PBB_SNMP_TRUE)
    {
        /*Sending indication to NPSIM */
        PbbHandleGlbShutdown ();

        if (PbbNpShutdown () != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }

        /*Deinitialize the global info */
        PbbDeInitGlobalInfo ();
        u1Status = L2IWF_TRUE;
        if (PbbL2IwfSetPbbShutdownStatus (u1Status) != PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

        if (PbbSetShutdownStatus (i4ShutdownStatus) != PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
    else
    {
        if (PbbNpNoShutdown () != PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
        if (PbbStartModule () != PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
        if (PbbSetShutdownStatus (i4ShutdownStatus) != PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
        u1Status = L2IWF_FALSE;
        if (PbbL2IwfSetPbbShutdownStatus (u1Status) != PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME);

    return PBB_SUCCESS;
}
