#include "pbbinc.h"

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditBufferEntry                                  */
/*                                                                            */
/*  Description     : This function performs audit of the PBB parameters      */
/*                    between the hardware and the software.                  */
/*                    This function synchronizes the PBB parameters between   */
/*                    the hardware and the software.                          */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditBufferEntry (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4NpapiId = PBB_INIT_VAL;
    UINT4               u4EventId = PBB_INIT_VAL;

    MEMSET (&NpSync, 0, sizeof (unNpSync));

    PBB_NPSYNC_BLK () = OSIX_TRUE;
    u4NpapiId = (INT4) (pBuf->u4NpApiId);
    u4EventId = (INT4) (pBuf->u4EventId);

    NpSync = pBuf->unNpData;

    switch (u4NpapiId)
    {
        case NPSYNC_FS_MI_PBB_HW_SET_VIP_ATTRIBUTES:
        case NPSYNC_FS_MI_PBB_HW_DEL_VIP_ATTRIBUTES:
        {
            PbbRedAuditICompServInst (NpSync.PbbPortIdx.u4ContextId,
                                      NpSync.PbbPortIdx.u4VipIfIndex);
        }
            break;

        case NPSYNC_FS_MI_PBB_HW_SET_VIP_PIP_MAP:
        case NPSYNC_FS_MI_PBB_HW_DEL_VIP_PIP_MAP:
        {
            PbbRedAuditVipToPipMap (NpSync.PbbPortIdx.u4ContextId,
                                    NpSync.PbbPortIdx.u4VipIfIndex);
        }
            break;

        case NPSYNC_FS_MI_PBB_HW_SET_BACKBONE_SERVICE_INST_ENTRY:
        case NPSYNC_FS_MI_PBB_HW_DEL_BACKBONE_SERVICE_INST_ENTRY:
        {
            PbbRedAuditBCompServInst (NpSync.IsidIdx.u4ContextId,
                                      NpSync.IsidIdx.u4CbpIfIndex,
                                      NpSync.IsidIdx.u4BSid);
        }
            break;

        case NPSYNC_FS_MI_PBB_HW_SET_ALL_TO_ONE_BUNDLING_SERVICE:
        {
            PbbRedAuditPisid (NpSync.
                              FsMiPbbHwSetAllToOneBundlingService.
                              u4ContextId,
                              NpSync.
                              FsMiPbbHwSetAllToOneBundlingService.
                              u4IfIndex,
                              NpSync.
                              FsMiPbbHwSetAllToOneBundlingService.u4Isid);
        }
            break;

        case NPSYNC_FS_MI_PBB_HW_DEL_ALL_TO_ONE_BUNDLING_SERVICE:
        {
            PbbRedAuditPisid (NpSync.
                              FsMiPbbHwDelAllToOneBundlingService.
                              u4ContextId,
                              NpSync.
                              FsMiPbbHwDelAllToOneBundlingService.
                              u4IfIndex,
                              NpSync.
                              FsMiPbbHwDelAllToOneBundlingService.u4Isid);
        }
            break;

        case NPSYNC_FS_MI_PBB_NP_INIT_HW:
        {
            PbbRedAuditInitHw ();
        }
            break;

        case NPSYNC_FS_MI_PBB_NP_DE_INIT_HW:
        {
            PbbRedAuditDeInitHw ();
        }
            break;

        case NPSYNC_FS_MI_BRG_HW_CREATE_CONTROL_PKT_FILTER:
        case NPSYNC_FS_MI_BRG_HW_DELETE_CONTROL_PKT_FILTER:
        {
            PbbRedAuditControlPktFilter (pBuf);
        }
            break;

        default:
        {
            switch (u4EventId)
            {
                case EVTSYNC_FS_MI_PBB_HW_SET_PORT_USE_DEI:
                {
                    PbbRedAuditForDei (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_HW_SET_PCP_ENCOD_TBL:
                {
                    PbbRedAuditForPcpEncTable (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_HW_SET_PCP_DECOD_TBL:
                {
                    PbbRedAuditForPcpDecTable (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_HW_SET_PORT_REQ_DROP_ENCODING:
                {
                    PbbRedAuditForPortDropEnc (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_HW_SET_PORT_PCP_SELECTION:
                {
                    PbbRedAuditForPcpSelection (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_HW_SET_PROVIDER_BRIDGE_PORT_TYPE:
                {
                    PbbRedAuditForPipPortType (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_DELETE_PORT:
                {
                    PbbRedAuditDeletePort (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_SET_GLOBAL_OUI:
                {
                    PbbRedAuditSetGlbOui (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_COPY_PORT_PROPERTIES:
                {
                    PbbRedAuditCopyPortProperties (pBuf);
                }
                    break;

                case EVTSYNC_FS_MI_PBB_REMOVE_PORT_PROPERTIES:
                {
                    PbbRedAuditRemPortProperties (pBuf);
                }
                    break;

                default:
                    break;
            }
        }
            break;
    }

    PBB_NPSYNC_BLK () = OSIX_FALSE;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditBCompServInst                                */
/*                                                                            */
/*  Description     : This function performs audit of the PBB B Component     */
/*                    parameters created in the Hardware                      */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Id                                */
/*                  : u4CbpIfIndex - CBP If Index                             */
/*                  : u4BSid = ISID Configured                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditBCompServInst (UINT4 u4ContextId, UINT4 u4CbpIfIndex, UINT4 u4BSid)
{
    tPbbBackBoneSerInstEntry *pIsidPortData = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1DelEntry = PBB_FALSE;

    if (PbbSelectContext (u4ContextId) == PBB_SUCCESS)
    {
        if (PbbVcmGetIfMapHlPortId (u4CbpIfIndex, &u2LocalPort) != PBB_SUCCESS)
        {
            u1DelEntry = PBB_TRUE;
        }
    }
    else
    {
        u1DelEntry = PBB_TRUE;
    }

    if (u1DelEntry == PBB_FALSE)
    {
        PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
        key.u2LocalPort = u2LocalPort;
        key.u4Isid = u4BSid;

        pIsidPortData = (tPbbBackBoneSerInstEntry *)
            RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidPortTable, &key);

        if ((pIsidPortData != NULL) &&
            (pIsidPortData->u1CBPRowstatus == PBB_ACTIVE))
        {
            PbbSetBCompHwServiceInst (u4ContextId, u4BSid, u4CbpIfIndex);

        }
        else
        {
            u1DelEntry = PBB_TRUE;
        }
    }

    if (u1DelEntry == PBB_TRUE)
    {
        PbbDelBCompHwServiceInst (u4ContextId, u4CbpIfIndex, u4BSid);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditPisid                                        */
/*                                                                            */
/*  Description     : This function performs audit of the PISID parameters    */
/*                    added on the Hardware                                   */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Id                                */
/*                  : u4IfIndex - CNP If Index                                */
/*                  : u4Pisid = PISID Configured                              */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditPisid (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 u4Isid)
{
    tPbbPortRBtreeNode *pPortData = NULL;
    UINT4               u4RowStatus = PBB_ACTIVE;
    UINT4               u4Pisid = PBB_ISID_INVALID_VALUE;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1DelEntry = PBB_FALSE;
    UINT1               u1SetEntry = PBB_FALSE;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        u1DelEntry = PBB_TRUE;
    }

    if (u1DelEntry == PBB_FALSE)
    {
        if (PbbVcmGetIfMapHlPortId (u4IfIndex, &u2LocalPort) != PBB_SUCCESS)
        {
            u1DelEntry = PBB_TRUE;
        }
        else
        {
            if (PbbGetPortData (u2LocalPort, &pPortData) != PBB_SUCCESS)
            {
                u1DelEntry = PBB_TRUE;
            }
        }
    }

    if (u1DelEntry == PBB_FALSE)
    {
        u4RowStatus = (UINT4) pPortData->unPbbPortData.CnpData.u1PisidRowStatus;

        if (u4RowStatus == PBB_ACTIVE)
        {
            u4Pisid = pPortData->unPbbPortData.CnpData.u4Pisid;
            u1SetEntry = PBB_TRUE;
        }
    }

    /* Delete the PISID without checking the u1DelEntry, because 
       for setting the new entry we have to delete the old entry */

    PbbDelHwPortPisid (u4ContextId, u4IfIndex, u4Isid);

    if (u1SetEntry == PBB_TRUE)
    {
        PbbSetHwPortPisid (u4ContextId, u4IfIndex, u4Pisid);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditInitHw                                       */
/*                                                                            */
/*  Description     : This function performs audit of the Shutdown parameters */
/*                    in Hardware and Software. If PBB is shutdown then       */
/*                    deinit the hardware                                     */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditInitHw (VOID)
{
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PbbNpShutdown ();
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditDeInitHw                                     */
/*                                                                            */
/*  Description     : This function performs audit of the Shutdown parameters */
/*                    in Hardware and Software.If PBB is not shutdown then    */
/*                    Initialize the hardware                                 */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditDeInitHw (VOID)
{
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_FALSE)
    {
        PbbNpNoShutdown ();
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditForDei                                       */
/*                                                                            */
/*  Description     : This function performs audit of the DEI Bit Value       */
/*                    parameters between the hardware and the software        */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditForDei (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpUseDei = PBB_INIT_VAL;

    NpSync = pBuf->unNpData;

    u4ContextId = NpSync.PortIdx.u4ContextId;
    u4IfIndex = NpSync.PortIdx.u4IfIndex;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }
    if (PbbVcmGetIfMapHlPortId (u4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return;
    }
    i1retVal = PbbGetPcpUseDei (u2LocalPort, &u1PcpUseDei);

    if (i1retVal == PBB_FAILURE)
    {
        return;
    }

    PbbHwSetPortUseDei (u4ContextId, u4IfIndex, u1PcpUseDei);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditForPcpDecTable                               */
/*                                                                            */
/*  Description     : This function performs audit of the PCP Dec Table Value */
/*                    parameters between the hardware and the software        */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditForPcpDecTable (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    tHwPbbPcpInfo       PcpTblEntry;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpSelRow = PBB_INIT_VAL;
    UINT1               u1Priority = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;
    UINT1               u1PcpVal = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;

    NpSync = pBuf->unNpData;

    PBB_MEMSET (&PcpTblEntry, PBB_INIT_VAL, sizeof (tHwPbbPcpInfo));

    u4ContextId = NpSync.PortIdx.u4ContextId;
    u4IfIndex = NpSync.PortIdx.u4IfIndex;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }
    if (PbbVcmGetIfMapHlPortId (u4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return;
    }

    for (u1PcpSelRow = PBB_8P0D_SEL_ROW; u1PcpSelRow <= PBB_5P3D_SEL_ROW;
         u1PcpSelRow++)
    {
        for (u1PcpVal = PBB_INIT_VAL; u1PcpVal < PBB_MAX_NUM_PCP; u1PcpVal++)
        {
            PcpTblEntry.u2PcpSelRow = (UINT2) u1PcpSelRow;
            PcpTblEntry.u2PcpValue = (UINT2) u1PcpVal;
            i1retVal = PbbGetPcpDecodingDE (u2LocalPort,
                                            u1PcpSelRow,
                                            u1PcpVal, &u1DropEligible);
            if (i1retVal == PBB_FAILURE)
            {
                return;
            }
            PcpTblEntry.u1DropEligible = u1DropEligible;
            i1retVal = PbbGetPcpDecodingPriority (u2LocalPort,
                                                  u1PcpSelRow,
                                                  u1PcpVal, &u1Priority);
            if (i1retVal == PBB_FAILURE)
            {
                return;
            }
            PcpTblEntry.u2Priority = (UINT2) u1Priority;

            i1retVal = PbbHwSetPcpDecodTbl (u4ContextId,
                                            u4IfIndex, PcpTblEntry);
            if (i1retVal == PBB_FAILURE)
            {
                return;
            }
        }
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditForPcpEncTable                               */
/*                                                                            */
/*  Description     : This function performs audit of the PCP Enc Table Value */
/*                    parameters between the hardware and the software        */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditForPcpEncTable (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    tHwPbbPcpInfo       PcpTblEntry;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpSelRow = PBB_INIT_VAL;
    UINT1               u1Priority = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;
    UINT1               u1PcpVal = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;

    NpSync = pBuf->unNpData;
    PBB_MEMSET (&PcpTblEntry, PBB_INIT_VAL, sizeof (tHwPbbPcpInfo));

    u4ContextId = NpSync.PortIdx.u4ContextId;
    u4IfIndex = NpSync.PortIdx.u4IfIndex;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }
    if (PbbVcmGetIfMapHlPortId (u4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return;
    }

    for (u1PcpSelRow = PBB_8P0D_SEL_ROW; u1PcpSelRow < PBB_5P3D_SEL_ROW;
         u1PcpSelRow++)
    {
        for (u1Priority = PBB_INIT_VAL; u1Priority < PBB_MAX_NUM_PRIORITY;
             u1Priority++)
        {
            for (u1DropEligible = PBB_INIT_VAL;
                 u1DropEligible < PBB_MAX_NUM_DROP_ELIGIBLE; u1DropEligible++)
            {
                i1retVal = PbbGetPcpEncodingPcpVal (u2LocalPort,
                                                    u1PcpSelRow,
                                                    u1Priority,
                                                    u1DropEligible, &u1PcpVal);
                if (i1retVal == PBB_FAILURE)
                {
                    return;
                }
                PcpTblEntry.u2PcpSelRow = (UINT2) u1PcpSelRow;
                PcpTblEntry.u2PcpValue = (UINT2) u1PcpVal;
                PcpTblEntry.u2Priority = (UINT2) u1Priority;
                PcpTblEntry.u1DropEligible = (UINT1) u1DropEligible;

                i1retVal = PbbHwSetPcpEncodTbl (u4ContextId,
                                                u4IfIndex, PcpTblEntry);
                if (i1retVal == PBB_FAILURE)
                {
                    return;
                }
            }
        }
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditForPortDropEnc                               */
/*                                                                            */
/*  Description     : This function performs audit of the Port Drop Encoding  */
/*                    parameters between the hardware and the software        */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditForPortDropEnc (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpReqDropEnc = PBB_INIT_VAL;

    NpSync = pBuf->unNpData;

    u4ContextId = NpSync.PortIdx.u4ContextId;
    u4IfIndex = NpSync.PortIdx.u4IfIndex;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }
    if (PbbVcmGetIfMapHlPortId (u4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return;
    }

    i1retVal = PbbGetPcpReqDropEncoding (u2LocalPort, &u1PcpReqDropEnc);
    if (i1retVal == PBB_FAILURE)
    {
        return;
    }

    PbbHwSetPortReqDropEncoding (u4ContextId, u4IfIndex, u1PcpReqDropEnc);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditForPcpSelection                              */
/*                                                                            */
/*  Description     : This function performs audit of the PCP Selection Value */
/*                    parameters between the hardware and the software        */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditForPcpSelection (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpSelRow = PBB_INIT_VAL;

    NpSync = pBuf->unNpData;

    u4ContextId = NpSync.PortIdx.u4ContextId;
    u4IfIndex = NpSync.PortIdx.u4IfIndex;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }

    if (PbbVcmGetIfMapHlPortId (u4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return;
    }

    i1retVal = PbbGetPcpSelRow (u2LocalPort, &u1PcpSelRow);
    if (i1retVal == PBB_FAILURE)
    {
        return;
    }
    PbbHwSetPortPcpSelection (u4ContextId, u4IfIndex, u1PcpSelRow);
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditForPipPortType                               */
/*                                                                            */
/*  Description     : This function performs audit of the port type for a     */
/*                    given port. If the port is found as PIP then set its    */
/*                    port type.                                              */
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditForPipPortType (tPbbBufferEntry * pBuf)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    unNpSync            NpSync;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));

    NpSync = pBuf->unNpData;

    u4ContextId = NpSync.PortIdx.u4ContextId;
    u4IfIndex = NpSync.PortIdx.u4IfIndex;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }

    if (PbbVcmGetIfMapHlPortId (u4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return;
    }

    key.u2LocalPort = u2LocalPort;
    /*Searching Port in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return;
    }

    if (pRBCurNode->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
    {
        PbbHwSetProviderBridgePortType (u4ContextId,
                                        u4IfIndex, pRBCurNode->u1PortType);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditICompServInst                                */
/*                                                                            */
/*  Description     : This function performs audit of the PBB I Component     */
/*                    parameters created in the Hardware                      */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Id                                */
/*                  : u4VipIndex - Vip If Index                               */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditICompServInst (UINT4 u4ContextId, UINT4 u4VipIndex)
{
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;
    UINT1               u1DelEntry = PBB_FALSE;

    if (PbbSelectContext (u4ContextId) == PBB_SUCCESS)
    {
        if (PbbVcmGetIfMapHlPortId (u4VipIndex, &u2Vip) != PBB_SUCCESS)
        {
            return;
        }
    }
    else
    {
        return;
    }

    if (PbbGetVipData (u2Vip, &pRBCurNode) != PBB_SUCCESS)
    {
        u1DelEntry = PBB_TRUE;
    }

    if (u1DelEntry == PBB_FALSE)
    {
        pIsidVipInfo = (tPbbIsidVipInfo *) pRBCurNode->pu4PbbIsidVipInfo;
        if (pIsidVipInfo != NULL)
        {
            u4Isid = (UINT4) pIsidVipInfo->u4Isid;

            if (pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus
                == PBB_ACTIVE)
            {
                /* Set the entry in the hardware */
                PbbSetICompHwServiceInst (u4ContextId, u4VipIndex, u4Isid);
                PbbSetVipOperStatus (u4ContextId, u4VipIndex,
                                     u2Vip, PBB_INIT_VAL);
            }
            else
            {
                u1DelEntry = PBB_TRUE;
            }
        }
        else
        {
            u1DelEntry = PBB_TRUE;
        }
    }

    /* Delete the entry from the hardware */
    if (u1DelEntry == PBB_TRUE)
    {
        PbbDelICompHwServiceInst (u4ContextId, u4VipIndex);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditVipToPipMap                                  */
/*                                                                            */
/*  Description     : This function performs audit of the PBB I Component     */
/*                    parameters between the hardware and the software        */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context Id                                */
/*                  : u4VipIfIndex - Vip If Index                             */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditVipToPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2PipPort = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;
    UINT1               u1DelEntry = PBB_FALSE;

    if (PbbSelectContext (u4ContextId) == PBB_SUCCESS)
    {
        if (PbbVcmGetIfMapHlPortId (u4VipIfIndex, &u2Vip) == PBB_SUCCESS)
        {
            if (PbbGetVipData (u2Vip, &pRBCurNode) != PBB_SUCCESS)
            {
                u1DelEntry = PBB_TRUE;
            }
        }
        else
        {
            return;
        }
    }
    else
    {
        return;
    }

    if (u1DelEntry == PBB_FALSE)
    {
        pIsidVipInfo = (tPbbIsidVipInfo *) pRBCurNode->pu4PbbIsidVipInfo;
        if (pIsidVipInfo != NULL)
        {
            u4Isid = (UINT4) pIsidVipInfo->u4Isid;
            u2PipPort = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;

            if (pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus
                == PBB_ACTIVE)
            {
                /* Reset the Vip to Pip Map */
                PbbHwAddVipToPip (u4ContextId, u4Isid, u2Vip, u2PipPort);
                PbbSetVipOperStatus (u4ContextId, u4VipIfIndex,
                                     u2Vip, PBB_INIT_VAL);
            }
            else
            {
                u1DelEntry = PBB_TRUE;
            }
        }
        else
        {
            u1DelEntry = PBB_TRUE;
        }
    }

    if (u1DelEntry == PBB_TRUE)
    {
        /* Delete the entry from the Hardware */
        PbbHwDeleteLocalVipToPip (u4ContextId, u4VipIfIndex, u4Isid, u2PipPort);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditControlPktFilter                             */
/*                                                                            */
/*  Description     : This function performs audit for the Control filters    */
/*                    installed in the Hardware                               */
/*                                                                            */
/*  Input(s)        : pBuf - PBB Buffer Entry                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditControlPktFilter (tPbbBufferEntry * pBuf)
{
    UINT4               u4ContextId = PBB_INIT_VAL;

    u4ContextId = pBuf->unNpData.ContextIdx.u4ContextId;

    PbbRedAuditFilters (u4ContextId);

    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditFilters                                      */
/*                                                                            */
/*  Description     : This function performs audit for the Control filters    */
/*                    to be installed in the Hardware                         */
/*                                                                            */
/*  Input(s)        : pBuf - PBB Buffer Entry                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditFilters (UINT4 u4ContextId)
{
    if (PbbSelectContext (u4ContextId) == PBB_SUCCESS)
    {
        if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_TRUE)
        {
            PbbCreateCompBridgeEcfmFilter ();

            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                PbbBrgHwCreateAstControlPktFilter ();
            }

            if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
            {
                /* Here we have to check > 0 because we do not know
                   whether the filter was installed or not */
                if ((PBB_CURR_CONTEXT_PTR ()->u2InternalCbpCount) > 0)
                {
                    PbbCreateIBCompBridgeEcfmFilter ();
                    PbbBrgHwCreateAstControlPktFilter ();
                }

                if ((PBB_CURR_CONTEXT_PTR ()->u2InternalCbpCount) == 0)
                {
                    PbbDeleteIBCompBridgeEcfmFilter ();
                    PbbBrgHwDestroyAstControlPktFilter ();
                }
            }
        }
        else
        {
            PbbDeleteIBCompBridgeEcfmFilter ();
            PbbBrgHwDestroyAstControlPktFilter ();
            PbbDeleteCompBridgeEcfmFilter ();
        }
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditDeletePort                                   */
/*                                                                            */
/*  Description     : This function performs audit for re-applying the        */
/*                    delete port event                                       */
/*                                                                            */
/*  Input(s)        : pBuf - PBB Buffer Entry                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditDeletePort (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    NpSync = pBuf->unNpData;

    u4ContextId = NpSync.PortIdx.u4ContextId;
    u4IfIndex = NpSync.PortIdx.u4IfIndex;

    if (PbbVcmGetIfMapHlPortId (u4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return;
    }

    /* Re- Apply the port configurations present in the
       S/W to the H/W */
    PbbSetPortProperties (u4ContextId, u4IfIndex, u2LocalPort);

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditDeleteContext                                */
/*                                                                            */
/*  Description     : This function performs audit for re-applying the        */
/*                    delete Context event                                    */
/*                                                                            */
/*  Input(s)        : u4ContextId - Context id                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditDeleteContext (UINT4 u4ContextId)
{
    INT4                i4Ifindex = PBB_INIT_VAL;
    UINT2               u2Port = PBB_INIT_VAL;
    UINT2               u2NextPort = PBB_INIT_VAL;

    PBB_LOCK ();
    if (PBB_RED_AUDIT_FLAG () != PBB_RED_AUDIT_START)
    {
        PBB_UNLOCK ();
        return;
    }

    /* Re- Apply the configurations for this COntext */
    PbbRedAuditFilters (u4ContextId);

    PBB_UNLOCK ();
    PBB_LOCK ();

    /* Check for AUDIT_FLAG. If its still set, then continue */
    if (PBB_RED_AUDIT_FLAG () != PBB_RED_AUDIT_START)
    {
        PBB_UNLOCK ();
        return;
    }

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        PBB_UNLOCK ();
        return;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () != PBB_TRUE)
    {
        PbbReleaseContext ();
        PBB_UNLOCK ();
        return;
    }

    PbbReleaseContext ();
    PBB_UNLOCK ();

    while (1)
    {
        i4Ifindex = PBB_INIT_VAL;

        PBB_LOCK ();

        /* Check for AUDIT_FLAG. If its still set, then continue */
        if (PBB_RED_AUDIT_FLAG () != PBB_RED_AUDIT_START)
        {
            PBB_UNLOCK ();
            return;
        }

        if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
        {
            PBB_UNLOCK ();
            return;
        }

        PbbGetNextLocalPort (u2Port, &u2NextPort);
        if (u2NextPort == PBB_INIT_VAL)
        {
            PbbReleaseContext ();
            PBB_UNLOCK ();
            break;
        }

        if (PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2NextPort,
                                           &i4Ifindex) == PBB_SUCCESS)
        {
            /* Re- Apply the port configurations present in the
               S/W to the H/W */
            PbbSetPortProperties (u4ContextId, i4Ifindex, u2NextPort);
        }

        PbbReleaseContext ();
        PBB_UNLOCK ();
        u2Port = u2NextPort;
    }

    return;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditSetGlbOui                                    */
/*                                                                            */
/*  Description     : This function performs audit for re-applying the        */
/*                    delete Context event                                    */
/*                                                                            */
/*  Input(s)        : pBuf - PBB Buffer Entry                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditSetGlbOui (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;

    NpSync = pBuf->unNpData;

    /* Set the Global OUI */
    PBB_MEMCPY (PBB_GLOBAL_OUI, NpSync.PbbGlbOui.au1OUI, PBB_OUI_LENGTH);

    /* Re-Apply the configuaration of the Global Oui
       in all the context */
    PbbSetPbbGlbOuiInContext ();
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditCopyPortProperties                           */
/*                                                                            */
/*  Description     : This function performs audit for re-applying the        */
/*                    Copy Port Properties event                              */
/*                                                                            */
/*  Input(s)        : pBuf - PBB Buffer Entry                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditCopyPortProperties (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalDstPort = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    NpSync = pBuf->unNpData;

    u4ContextId = NpSync.PbbPortProperty.u4ContextId;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }

    if (PbbVcmGetIfMapHlPortId (NpSync.PbbPortProperty.u4IfIndex, &u2LocalPort)
        != PBB_SUCCESS)
    {
        return;
    }

    if (PbbVcmGetIfMapHlPortId
        (NpSync.PbbPortProperty.u4DstPort, &u2LocalDstPort) != PBB_SUCCESS)
    {
        return;
    }

    /* Re-Apply the configuaration */
    PbbHandleCopyPortPropertiesToHw (u2LocalDstPort, u2LocalPort);

    PbbReleaseContext ();
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditRemPortProperties                            */
/*                                                                            */
/*  Description     : This function performs audit for re-applying the        */
/*                    Remove Port Properties event                            */
/*                                                                            */
/*  Input(s)        : pBuf - PBB Buffer Entry                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditRemPortProperties (tPbbBufferEntry * pBuf)
{
    unNpSync            NpSync;
    UINT4               u4ContextId = PBB_INIT_VAL;

    NpSync = pBuf->unNpData;

    u4ContextId = NpSync.PbbPortProperty.u4ContextId;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }

    /* Re-Apply the configuaration */
    PbbHandleRemovePortPropertiesFromHw (NpSync.PbbPortProperty.u4DstPort,
                                         NpSync.PbbPortProperty.u4IfIndex);

    PbbReleaseContext ();

}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbSetPortProperties                                    */
/*                                                                            */
/*  Description     : This function re-applies the port configurations in     */
/*                    Hardware.                                               */
/*                                                                            */
/*  Input(s)        : pBuf - PBB Buffer Entry                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbSetPortProperties (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT4               u4Isid = PBB_ISID_INVALID_VALUE;
    UINT2               u2Pip = PBB_INIT_VAL;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return;
    }

    /* Check if the port is a VIP */
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode != NULL)
    {
        pIsidVipInfo = (tPbbIsidVipInfo *) pRBCurNode->pu4PbbIsidVipInfo;
        if (pIsidVipInfo == NULL)
        {
            return;
        }

        u4Isid = pIsidVipInfo->u4Isid;
        u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
        if (pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus ==
            PBB_ACTIVE)
        {
            PbbHwAddVipToPip (u4ContextId, u4Isid, u2LocalPort, u2Pip);
        }

        if (pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus == PBB_ACTIVE)
        {
            PbbSetICompHwServiceInst (u4ContextId, i4IfIndex, u4Isid);
        }

        PbbSetVipOperStatus (u4ContextId, i4IfIndex, u2LocalPort, PBB_INIT_VAL);
    }
    else
    {
        /* The port is CNP, PIP, CBP. Copy their port properties
           to the Hardware */
        PbbSetPortPropertiesinHw (u4ContextId, i4IfIndex, u2LocalPort);
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbSetPortPropertiesinHw                                */
/*                                                                            */
/*  Description     : This function re-applies the port configurations in     */
/*                    Hardware.                                               */
/*                                                                            */
/*  Input(s)        : pBuf - PBB Buffer Entry                                 */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbSetPortPropertiesinHw (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2SrcPort)
{
    tPbbIsidPipVipNode *pCurEntry = NULL;
    tPbbICompVipRBtreeNode *pVipNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tPbbICompVipRBtreeNode Vipkey;
    tPbbPortRBtreeNode  key;
    UINT4               u4Pisid = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;
    UINT2               u2Pip = PBB_INIT_VAL;

    MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));

    key.u2LocalPort = u2SrcPort;
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return;
    }

    if ((pRBCurNode->u1PortType == PBB_CNP_PORTBASED_PORT) ||
        (pRBCurNode->u1PortType == PBB_CNP_STAGGED_PORT) ||
        (pRBCurNode->u1PortType == PBB_CNP_CTAGGED_PORT))
    {
        u4Pisid = pRBCurNode->unPbbPortData.CnpData.u4Pisid;
        if (pRBCurNode->unPbbPortData.CnpData.u1PisidRowStatus == PBB_ACTIVE)
        {
            /* Copy the Source port Pisid for Destination port */
            PbbSetHwPortPisid (u4ContextId, i4IfIndex, u4Pisid);
        }
    }
    else if (pRBCurNode->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.CbpData.IsidList),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbIsidPipVipNode *) pLstNode;
            u4Isid = pCurEntry->unIsidVip.u4Isid;

            if (PbbValidateIsidPort (u4Isid,
                                     (INT4) pRBCurNode->u4IfIndex)
                != PBB_SUCCESS)
            {
                continue;
            }

            PbbSetBCompHwServiceInstOnDest (u4ContextId, u4Isid,
                                            (INT4) pRBCurNode->u4IfIndex,
                                            i4IfIndex);
        }
    }
    else if (pRBCurNode->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
    {
        /* Set the Port Type in the Hardware */
        PbbHwSetProviderBridgePortType (u4ContextId,
                                        i4IfIndex, pRBCurNode->u1PortType);

        /* Set the Vip Pip Map in the Hardware */
        PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.PipData.VipList),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pVipNode = NULL;
            pIsidVipInfo = NULL;

            pCurEntry = (tPbbIsidPipVipNode *) pLstNode;
            u2Vip = pCurEntry->unIsidVip.u2Vip;

            MEMSET (&Vipkey, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));

            Vipkey.u2Vip = u2Vip;
            pVipNode = (tPbbICompVipRBtreeNode *)
                RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable, &Vipkey);
            if (pVipNode == NULL)
            {
                /* Element not present in the key */
                continue;
            }

            pIsidVipInfo = (tPbbIsidVipInfo *) pVipNode->pu4PbbIsidVipInfo;
            if (pIsidVipInfo == NULL)
            {
                continue;
            }

            u4Isid = pIsidVipInfo->u4Isid;
            u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;

            if (pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus ==
                PBB_ACTIVE)
            {
                PbbHwAddVipToPip (u4ContextId, u4Isid, u2Vip, u2Pip);
                PbbSetVipOperStatus (u4ContextId, PBB_INIT_VAL,
                                     u2Vip, PBB_INIT_VAL);
            }
        }

        PbbHwSetPipPcpAttributesOnDest (u4ContextId, u2SrcPort, i4IfIndex);
    }
}
