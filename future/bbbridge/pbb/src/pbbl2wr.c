/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  File Name : pbbl2wr.c
 *
 * Description: This file contains L2IWF function wrappers 
 *              used in PBB module.
 *
 *******************************************************************/

#include "pbbinc.h"
#include "pbbport.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbL2IwfGetPortChannelForPort                     */
/*                                                                          */
/*    Description        : This is a wrapper function to get the aggregated
                           port number for a port
 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbL2IwfGetPortChannelForPort (INT4 i4IfIndex, UINT2 *pu2AggId)
{
    if (L2IwfGetPortChannelForPort ((UINT4) i4IfIndex,
                                    pu2AggId) == L2IWF_SUCCESS)
    {
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbL2IwfIsPortInPortChannel                       */
/*                                                                          */
/*    Description        : This is a wrapper function to check a port is i
                           an aggregated port
 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbL2IwfIsPortInPortChannel (INT4 i4IfIndex)
{
    if (L2IwfIsPortInPortChannel ((UINT4) i4IfIndex) == L2IWF_SUCCESS)
    {
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PbbL2IwfGetBridgeMode                               */
/*                                                                           */
/* Description        : This function calls the L2IWF module to get the      */
/*                      bridge mode.                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu4BridgeMode  - bridge mode.                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT1
PbbL2IwfGetBridgeMode (UINT4 u4ContextId, UINT4 *pu4BridgeMode)
{
    if (L2IwfGetBridgeMode (u4ContextId, pu4BridgeMode) != L2IWF_SUCCESS)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbL2IwfSetPbbShutdownStatus                         */
/*                                                                           */
/* Description        : This function calls the L2IWF module to Set the      */
/*                      ShutDown Status.                                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : pu4BridgeMode  - bridge mode.                        */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/

INT1
PbbL2IwfSetPbbShutdownStatus (UINT1 u1ShutdownStatus)
{
    if (L2IwfSetPbbShutdownStatus (u1ShutdownStatus) != L2IWF_SUCCESS)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbL2IwfGetPortOperStatus                           */
/*                                                                           */
/* Description        : This routine determines the operational status of a  */
/*                      port indicated to a given module by it's lower layer */
/*                      modules (determines the lower layer port oper status)*/
/*                                                                           */
/* Input(s)           : i4ModuleId - Module for which the lower layer        */
/*                                   oper status is to be determined         */
/*                      u2IfIndex - Global IfIndex of the port whose lower   */
/*                                  layer oper status is to be determined    */
/*                                                                           */
/* Output(s)          : u1OperStatus - Lower layer port oper status for a    */
/*                                     given module                          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
PbbL2IwfGetPortOperStatus (INT4 i4ModuleId, UINT2 u2IfIndex,
                           UINT1 *pu1OperStatus)
{
    return (L2IwfGetPortOperStatus (i4ModuleId, u2IfIndex, pu1OperStatus));
}

/*****************************************************************************/
/* Function Name      : PbbL2IwfGetNextValidPortForContext                   */
/*                                                                           */
/* Description        : This routine returns the next active port in the     */
/*                      context as ordered by the HL Port ID. This is used   */
/*                      by the L2 modules to know the active ports in the    */
/*                      system when they are started from the shutdown state */
/*                                                                           */
/* Input(s)           : u2LocalPortId - HL Port Index whose next port is to  */
/*                                      be determined                        */
/*                                                                           */
/* Output(s)          : u2NextLocalPort - The next active HL Port ID         */
/*                      u2NextIfIndex - IfIndex of the next HL Port          */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS or L2IWF_FAILURE                       */
/*****************************************************************************/
INT4
PbbL2IwfGetNextValidPortForContext (UINT4 u4ContextId, UINT2 u2LocalPortId,
                                    UINT2 *pu2NextLocalPort,
                                    UINT4 *pu4NextIfIndex)
{
    tL2NextPortInfo     L2NextPortInfo;

    L2NextPortInfo.u4ContextId = u4ContextId;
    L2NextPortInfo.u2LocalPortId = u2LocalPortId;
    L2NextPortInfo.pu2NextLocalPort = pu2NextLocalPort;
    L2NextPortInfo.pu4NextIfIndex = pu4NextIfIndex;
    L2NextPortInfo.u4ModuleId = L2IWF_PROTOCOL_ID_PBB;

    return (L2IwfGetNextValidPortForProtoCxt (&L2NextPortInfo));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbL2IwfSetVipOperStatusFlag                     */
/*                                                                          */
/*    Description        : This is a wrapper function to set the OperStatus */
/*                         flag in the L2IWF port entry                     */
/*                                                                          */
/*    Input(s)           : i4IfIndex - VIP Interface Index                  */
/*                       : u1VipOperStatusFlag - CFA_TRUE / CFA_FALSE       */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbL2IwfSetVipOperStatusFlag (INT4 i4IfIndex, UINT1 u1VipOperStatusFlag)
{
    if (L2IwfSetVipOperStatusFlag ((UINT4) i4IfIndex,
                                   u1VipOperStatusFlag) == L2IWF_SUCCESS)
    {
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbL2IwfUpdateVipOperStatus                      */
/*                                                                          */
/*    Description        : This is a wrapper function to set the OperStatus */
/*                         flag in the L2IWF port entry                     */
/*                                                                          */
/*    Input(s)           : i4IfIndex - VIP Interface Index                  */
/*                       : u1VipOperStatus - CFA_IF_UP / CFA_IF_DOWN        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbL2IwfUpdateVipOperStatus (INT4 i4IfIndex, UINT1 u1VipOperStatus)
{
    if (L2IwfUpdateVipOperStatus ((UINT4) i4IfIndex,
                                  u1VipOperStatus) != L2IWF_SUCCESS)
    {
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbL2IwfGetPortVlanList                              */
/*                                                                           */
/* Description        : This routine is called to get the vlan list.if       */
/*                      argument u1VlanPortType is Tagged then this function */
/*                      will return the List of vlans for which this port is */
/*                      untagged Member.If the u1VlanPortType is untagged or */
/*                      mamber port then this functiion will return the list */
/*                      of vlans for which port is untagged memberor both    */
/*                      tagged & untagged List                               */
/*                                                                           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id                           */
/*                      u2LocalPortId- Local Port Number                     */
/*                      u1VlanPortType - Port Vlan Type                      */
/*                      u1VlanPortType values may be                         */
/*                                   1. UnTagged member port -0              */
/*                                   2. tagged member port - 1               */
/*                                   3. member port (tagged or untagged)-2   */
/*                                                                           */
/* Output(s)          : pVlanIdList  -Vlan List                              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
PbbL2IwfGetPortVlanList (UINT4 u4ContextId,
                         tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                         UINT2 u2LocalPort, UINT1 u1VlanPortType)
{
    return (L2IwfGetPortVlanList (u4ContextId, pVlanIdList,
                                  u2LocalPort, u1VlanPortType));
}

/*****************************************************************************/
/* Function Name      : PbbL2IwfSetAllToOneBndlStatus                        */
/*                                                                           */
/* Description        : This routine is called to set the All to one bundling*/
/*                      status in L2IWF status.                              */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Id                           */
/*                      u1Status - All to one bundling status                */
/*                                                                           */
/* Output(s)          : pVlanIdList  -Vlan List                              */
/*                                                                           */
/* Return Value(s)    : L2IWF_SUCCESS/ L2IWF_FAILURE                         */
/*****************************************************************************/

INT4
PbbL2IwfSetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 u1Status)
{
    return (L2IwfSetAllToOneBndlStatus (u4ContextId, u1Status));
}
