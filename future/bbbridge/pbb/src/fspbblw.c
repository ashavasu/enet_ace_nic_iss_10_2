/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fspbblw.c,v 1.26 2014/03/19 13:36:12 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
#include   "pbbinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbShutdownStatus
Input       :  The Indices

The Object 
retValFsPbbShutdownStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbShutdownStatus (INT4 *pi4RetValFsPbbShutdownStatus)
{
    if (PbbGetShutdownStatus (pi4RetValFsPbbShutdownStatus) != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbGlbOUI
Input       :  The Indices

The Object 
retValFsPbbGlbOUI
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbGlbOUI (tSNMP_OCTET_STRING_TYPE * pRetValFsPbbGlbOUI)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        pRetValFsPbbGlbOUI->pu1_OctetList[0] = PBB_INIT_VAL;
        pRetValFsPbbGlbOUI->i4_Length = PBB_INIT_VAL;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        pRetValFsPbbGlbOUI->pu1_OctetList[0] = PBB_INIT_VAL;
        pRetValFsPbbGlbOUI->i4_Length = PBB_INIT_VAL;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }

    if (PbbGetPbbGlbOUI (pRetValFsPbbGlbOUI) != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbMaxNoOfISID
Input       :  The Indices

The Object 
retValFsPbbMaxNoOfISID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbMaxNoOfISID (INT4 *pi4RetValFsPbbMaxNoOfISID)
{
    if (PbbGetMaxNoOfISID (pi4RetValFsPbbMaxNoOfISID) != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbMaxNoOfISIDPerContext
Input       :  The Indices

The Object 
retValFsPbbMaxNoOfISIDPerContext
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbMaxNoOfISIDPerContext (INT4 *pi4RetValFsPbbMaxNoOfISIDPerContext)
{
    if (PbbGetMaxNoOfISIDPerContext (pi4RetValFsPbbMaxNoOfISIDPerContext)
        != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsPbbMaxPortsPerISID
Input       :  The Indices

The Object 
retValFsPbbMaxPortsPerISID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbMaxPortsPerISID (INT4 *pi4RetValFsPbbMaxPortsPerISID)
{
    if (PbbGetMaxPortsPerISID (pi4RetValFsPbbMaxPortsPerISID) != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbMaxPortsPerISIDPerContext
Input       :  The Indices

The Object 
retValFsPbbMaxPortsPerISIDPerContext
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbMaxPortsPerISIDPerContext (INT4
                                      *pi4RetValFsPbbMaxPortsPerISIDPerContext)
{
    if (PbbGetMaxPortsPerISIDPerContext
        (pi4RetValFsPbbMaxPortsPerISIDPerContext) != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbMaxCurrentNoOfISID
Input       :  The Indices

The Object 
retValFsPbbMaxCurrentNoOfISID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbMaxCurrentNoOfISID (INT4 *pi4RetValFsPbbMaxCurrentNoOfISID)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        *pi4RetValFsPbbMaxCurrentNoOfISID = 0;
        return SNMP_SUCCESS;
    }

    if (PbbGetMaxNoOfISID (pi4RetValFsPbbMaxCurrentNoOfISID) != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsPbbMaxCurrentISIDPerContext
Input       :  The Indices

The Object 
retValFsPbbMaxCurrentISIDPerContext
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbMaxCurrentISIDPerContext (INT4
                                     *pi4RetValFsPbbMaxCurrentNoOfISIDPerContext)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        *pi4RetValFsPbbMaxCurrentNoOfISIDPerContext = 0;
        return SNMP_SUCCESS;
    }

    if (PbbGetMaxNoOfISIDPerContext (pi4RetValFsPbbMaxCurrentNoOfISIDPerContext)
        != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbMaxCurrentPortsPerISID
Input       :  The Indices

The Object 
retValFsPbbMaxCurrentPortsPerISID
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbMaxCurrentPortsPerISID (INT4 *pi4RetValFsPbbMaxCurrentPortsPerISID)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        *pi4RetValFsPbbMaxCurrentPortsPerISID = 0;
        return SNMP_SUCCESS;
    }

    if (PbbGetMaxPortsPerISID (pi4RetValFsPbbMaxCurrentPortsPerISID)
        != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbMaxCurrPortsPerISIDContext
Input       :  The Indices

The Object 
retValFsPbbMaxCurrPortsPerISIDContext
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbMaxCurrPortsPerISIDContext (INT4
                                       *pi4RetValFsPbbMaxCurrentPortsPerISIDPerContext)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        *pi4RetValFsPbbMaxCurrentPortsPerISIDPerContext = 0;
        return SNMP_SUCCESS;
    }

    if (PbbGetMaxPortsPerISIDPerContext
        (pi4RetValFsPbbMaxCurrentPortsPerISIDPerContext) != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbTraceInput
Input       :  The Indices

The Object 
retValFsPbbTraceInput
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbTraceInput (tSNMP_OCTET_STRING_TYPE * pRetValFsPbbTraceInput)
{
    UINT4               u4TraceOption = 0;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        /* get the actaul trace option and add ciritical trace option, then
         * get the corresponding trace input and return the trace input */
        u4TraceOption = gPbbGlobData.u4TraceOption;
        /* default trace option - critical trace */
        u4TraceOption |= (UINT4) PBB_CRITICAL_TRC;
        /* get trace input string corresponding to the given
         * trace option */
        pRetValFsPbbTraceInput->i4_Length = PbbUtilGetTraceInputValue
            (pRetValFsPbbTraceInput->pu1_OctetList, u4TraceOption);
        return SNMP_SUCCESS;
    }

    pRetValFsPbbTraceInput->i4_Length = PbbUtilGetTraceInputValue
        (pRetValFsPbbTraceInput->pu1_OctetList, gPbbGlobData.u4TraceOption);
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsPbbTraceOption
Input       :  The Indices

The Object 
retValFsPbbTraceOption
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbTraceOption (INT4 *pi4RetValFsPbbTraceOption)
{
    INT4                i4TraceOption = 0;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        /* get the actaul trace option and add critical trace 
         * option(default) */
        i4TraceOption = (INT4) gPbbGlobData.u4TraceOption;
        /* default trace option - critical trace */
        i4TraceOption |= (INT4) PBB_CRITICAL_TRC;
        /* get trace input string corresponding to the given
         * trace option */
        *pi4RetValFsPbbTraceOption = i4TraceOption;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }
    *pi4RetValFsPbbTraceOption = (INT4) gPbbGlobData.u4TraceOption;
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbShutdownStatus
Input       :  The Indices

The Object 
setValFsPbbShutdownStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbShutdownStatus (INT4 i4SetValFsPbbShutdownStatus)
{
    INT1                i1RetVal = PBB_SUCCESS;

    /*Return success if the current value and recieve value are same */
    if (gPbbGlobData.au1PbbShutDownStatus == i4SetValFsPbbShutdownStatus)
    {
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }

    i1RetVal = PbbHandleShutdownStatus (i4SetValFsPbbShutdownStatus);

    if (i1RetVal == PBB_SUCCESS)
    {
        i1RetVal = SNMP_SUCCESS;
    }
    else
    {
        i1RetVal = SNMP_FAILURE;
    }

    return i1RetVal;
}

/****************************************************************************
Function    :  nmhSetFsPbbGlbOUI
Input       :  The Indices

The Object 
setValFsPbbGlbOUI
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbGlbOUI (tSNMP_OCTET_STRING_TYPE * pSetValFsPbbGlbOUI)
{
    if (PbbSetPbbGlbOUI (pSetValFsPbbGlbOUI) != PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

#if defined (L2RED_WANTED) && defined (NPAPI_WANTED)
    EvtSyncFsMiPbbSetGlbOui (PBB_GLOBAL_OUI);
    PBB_NPSYNC_BLK () = OSIX_TRUE;
#endif

    if (PbbSetPbbGlbOuiInContext () != PBB_SUCCESS)
    {
#if defined (L2RED_WANTED) && defined(NPAPI_WANTED)
        if (PBB_NODE_STATUS () != PBB_NODE_IDLE)
        {
            PBB_NPSYNC_BLK () = OSIX_FALSE;
        }
#endif
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() ::  Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

#if defined (L2RED_WANTED) && defined(NPAPI_WANTED)
    if (PBB_NODE_STATUS () != PBB_NODE_IDLE)
    {
        PBB_NPSYNC_BLK () = OSIX_FALSE;
    }
#endif
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbMaxNoOfISID
Input       :  The Indices

The Object 
setValFsPbbMaxNoOfISID
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbMaxNoOfISID (INT4 i4SetValFsPbbMaxNoOfISID)
{
    UNUSED_PARAM (i4SetValFsPbbMaxNoOfISID);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsPbbMaxNoOfISIDPerContext
Input       :  The Indices

The Object 
setValFsPbbMaxNoOfISIDPerContext
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbMaxNoOfISIDPerContext (INT4 i4SetValFsPbbMaxNoOfISIDPerContext)
{
    UNUSED_PARAM (i4SetValFsPbbMaxNoOfISIDPerContext);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsPbbMaxPortsPerISID
Input       :  The Indices

The Object 
setValFsPbbMaxPortsPerISID
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbMaxPortsPerISID (INT4 i4SetValFsPbbMaxPortsPerISID)
{
    UNUSED_PARAM (i4SetValFsPbbMaxPortsPerISID);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsPbbMaxPortsPerISIDPerContext
Input       :  The Indices

The Object 
setValFsPbbMaxPortsPerISIDPerContext
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbMaxPortsPerISIDPerContext (INT4
                                      i4SetValFsPbbMaxPortsPerISIDPerContext)
{
    UNUSED_PARAM (i4SetValFsPbbMaxPortsPerISIDPerContext);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetFsPbbTraceInput
Input       :  The Indices

The Object 
setValFsPbbTraceInput
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbTraceInput (tSNMP_OCTET_STRING_TYPE * pSetValFsPbbTraceInput)
{
    UINT4               u4TrcOption = 0;
    UINT4               u4TrcLen = 0;

    u4TrcOption = PbbUtilGetTraceOptionValue
        (pSetValFsPbbTraceInput->pu1_OctetList,
         pSetValFsPbbTraceInput->i4_Length);

    if (!STRNCMP (pSetValFsPbbTraceInput->pu1_OctetList, "enable",
                  STRLEN ("enable")))
    {
        gPbbGlobData.u4TraceOption |= u4TrcOption;
    }
    else
    {
        gPbbGlobData.u4TraceOption &= ~u4TrcOption;
    }

    /* set trace input */
    MEMSET (gPbbGlobData.au1TraceInput, 0, PBB_TRC_MAX_SIZE);
    u4TrcLen = PbbUtilGetTraceInputValue (gPbbGlobData.au1TraceInput,
                                          gPbbGlobData.u4TraceOption);
    UNUSED_PARAM (u4TrcLen);

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbShutdownStatus
Input       :  The Indices

The Object 
testValFsPbbShutdownStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbShutdownStatus (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPbbShutdownStatus)
{
    UINT4               u4ContextId;
    /* Validate the value of shut down status */
    if ((i4TestValFsPbbShutdownStatus != PBB_SNMP_TRUE) &&
        (i4TestValFsPbbShutdownStatus != PBB_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsPbbShutdownStatus == PBB_SNMP_TRUE)
    {
        for (u4ContextId = 0; u4ContextId < PBB_MAX_CONTEXTS; u4ContextId++)
        {
            if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
            {
                if (((PBB_CONTEXT_PTR (u4ContextId))->u4BridgeMode ==
                     PBB_ICOMPONENT_BRIDGE_MODE) ||
                    ((PBB_CONTEXT_PTR (u4ContextId))->u4BridgeMode ==
                     PBB_BCOMPONENT_BRIDGE_MODE))
                    return SNMP_FAILURE;
            }
        }
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbGlbOUI
Input       :  The Indices

The Object 
testValFsPbbGlbOUI
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbGlbOUI (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pTestValFsPbbGlbOUI)
{

    /*Check the intialization status of PBB module */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check OUI string and length to be set */
    if (pTestValFsPbbGlbOUI->i4_Length != PBB_OUI_LENGTH)
    {
        CLI_SET_ERR (CLI_PBB_GLB_OUI_LENGTH_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsPbbGlbOUI->pu1_OctetList == NULL)
    {
        CLI_SET_ERR (CLI_PBB_GLB_OUI_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbMaxNoOfISID
Input       :  The Indices

The Object 
testValFsPbbMaxNoOfISID
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbMaxNoOfISID (UINT4 *pu4ErrorCode, INT4 i4TestValFsPbbMaxNoOfISID)
{
    UNUSED_PARAM (i4TestValFsPbbMaxNoOfISID);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbMaxNoOfISIDPerContext
Input       :  The Indices

The Object 
testValFsPbbMaxNoOfISIDPerContext
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbMaxNoOfISIDPerContext (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsPbbMaxNoOfISIDPerContext)
{
    UNUSED_PARAM (i4TestValFsPbbMaxNoOfISIDPerContext);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbMaxPortsPerISID
Input       :  The Indices

The Object 
testValFsPbbMaxPortsPerISID
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbMaxPortsPerISID (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsPbbMaxPortsPerISID)
{
    UNUSED_PARAM (i4TestValFsPbbMaxPortsPerISID);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbMaxPortsPerISIDPerContext
Input       :  The Indices

The Object 
testValFsPbbMaxPortsPerISIDPerContext
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbMaxPortsPerISIDPerContext (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsPbbMaxPortsPerISIDPerContext)
{
    UNUSED_PARAM (i4TestValFsPbbMaxPortsPerISIDPerContext);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbTraceInput
Input       :  The Indices

The Object 
testValFsPbbTraceInput
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbTraceInput (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pTestValFsPbbTraceInput)
{
    UINT4               u4TrcOption = 0;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if ((pTestValFsPbbTraceInput->i4_Length > PBB_TRC_MAX_SIZE) ||
        (pTestValFsPbbTraceInput->i4_Length < PBB_TRC_MIN_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    u4TrcOption = PbbUtilGetTraceOptionValue
        (pTestValFsPbbTraceInput->pu1_OctetList,
         pTestValFsPbbTraceInput->i4_Length);
    if (u4TrcOption == PBB_INVALID_TRC)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbGlbOUI
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbGlbOUI (UINT4 *pu4ErrorCode,
                     tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsPbbShutdownStatus
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbShutdownStatus (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsPbbMaxNoOfISID
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbMaxNoOfISID (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsPbbMaxNoOfISIDPerContext
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbMaxNoOfISIDPerContext (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsPbbMaxPortsPerISID
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbMaxPortsPerISID (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsPbbMaxPortsPerISIDPerContext
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbMaxPortsPerISIDPerContext (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhDepv2FsPbbTraceInput
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbTraceInput (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbISIDOUITable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbISIDOUITable
Input       :  The Indices
FsPbbContextId
FsPbbCBPServiceMappingBackboneSid
IfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbISIDOUITable (INT4 i4FsPbbContextId,
                                           UINT4
                                           u4FsPbbCBPServiceMappingBackboneSid,
                                           INT4 i4IfIndex)
{
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate the System Initialized status */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate the value of Context id */
    if (PbbValidateContextId (i4FsPbbContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate for the Bridge mode to be PBB */
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Validate the value of Isid */
    if (PbbValidateIsid (u4FsPbbCBPServiceMappingBackboneSid) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Validate the value of If Index */

    if (PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PbbValidatePort (i4IfIndex) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbISIDOUITable
Input       :  The Indices
FsPbbContextId
FsPbbVipISid
IfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbISIDOUITable (INT4 *pi4FsPbbContextId,
                                   UINT4 *pu4FsPbbCBPServiceMappingBackboneSid,
                                   INT4 *pi4IfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                PBB_BCOMPONENT_BRIDGE_MODE)
            {
                return (nmhGetNextIndexFsPbbISIDOUITable (u4Context,
                                                          pi4FsPbbContextId,
                                                          u4Isid,
                                                          pu4FsPbbCBPServiceMappingBackboneSid,
                                                          i4IfIndex,
                                                          pi4IfIndex));
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbISIDOUITable
Input       :  The Indices
FsPbbContextId
nextFsPbbContextId
FsPbbCBPServiceMappingBackboneSid
nextFsPbbCBPServiceMappingBackboneSid
IfIndex
nextIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbISIDOUITable (INT4 i4FsPbbContextId,
                                  INT4 *pi4NextFsPbbContextId,
                                  UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                  UINT4
                                  *pu4NextFsPbbCBPServiceMappingBackboneSid,
                                  INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT4               u4NextIsid = PBB_INIT_VAL;
    INT4                i4NextIfIndex = PBB_INIT_VAL;
    UINT2               u2NextPort = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    for (u4Context = i4FsPbbContextId; u4Context < PBB_MAX_CONTEXTS;
         u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode !=
                PBB_BCOMPONENT_BRIDGE_MODE)
            {
                PBB_CURR_CONTEXT_PTR () = NULL;
                continue;
            }

            if (i4IfIndex != 0)
            {
                if (PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort)
                    != PBB_SUCCESS)
                {
                    return PBB_FAILURE;
                }
            }

            while (PbbGetNextIsidPort (u4FsPbbCBPServiceMappingBackboneSid,
                                       &u4NextIsid,
                                       u2LocalPort, &u2NextPort) == PBB_SUCCESS)
            {

                if (PbbGetValidIsidPortOUI (u4NextIsid, u2NextPort) ==
                    PBB_SUCCESS)
                {
                    if (PbbVcmGetIfIndexFromLocalPort (u4Context,
                                                       u2NextPort,
                                                       &i4NextIfIndex) !=
                        PBB_SUCCESS)
                    {
                        continue;
                    }
                    *pi4NextFsPbbContextId = u4Context;
                    *pu4NextFsPbbCBPServiceMappingBackboneSid = u4NextIsid;
                    *pi4NextIfIndex = i4NextIfIndex;
                    PBB_CURR_CONTEXT_PTR () = NULL;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
                else
                {
                    u4FsPbbCBPServiceMappingBackboneSid = u4NextIsid;
                    u2LocalPort = u2NextPort;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbOUI
Input       :  The Indices
FsPbbContextId
FsPbbCBPServiceMappingBackboneSid
IfIndex

The Object 
retValFsPbbOUI
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbOUI (INT4 i4FsPbbContextId,
                UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pRetValFsPbbOUI)
{
    if (nmhValidateIndexInstanceFsPbbISIDOUITable (i4FsPbbContextId,
                                                   u4FsPbbCBPServiceMappingBackboneSid,
                                                   i4IfIndex) == SNMP_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (PbbGetIsidOUI (i4FsPbbContextId,
                       u4FsPbbCBPServiceMappingBackboneSid,
                       i4IfIndex, pRetValFsPbbOUI) == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbOUIRowStatus
Input       :  The Indices
FsPbbContextId
FsPbbCBPServiceMappingBackboneSid
IfIndex

The Object 
retValFsPbbOUIRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbOUIRowStatus (INT4 i4FsPbbContextId,
                         UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                         INT4 i4IfIndex, INT4 *pi4RetValFsPbbOUIRowStatus)
{
    if (nmhValidateIndexInstanceFsPbbISIDOUITable (i4FsPbbContextId,
                                                   u4FsPbbCBPServiceMappingBackboneSid,
                                                   i4IfIndex) == SNMP_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (PbbGetIsidOUIRowStatus (i4FsPbbContextId,
                                u4FsPbbCBPServiceMappingBackboneSid,
                                i4IfIndex,
                                pi4RetValFsPbbOUIRowStatus) == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (*pi4RetValFsPbbOUIRowStatus == PBB_INVALID_ROWSTATUS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbOUI
Input       :  The Indices
FsPbbContextId
FsPbbCBPServiceMappingBackboneSid
IfIndex

The Object 
setValFsPbbOUI
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbOUI (INT4 i4FsPbbContextId,
                UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pSetValFsPbbOUI)
{
    if (PbbSelectContext (i4FsPbbContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSetIsidOUI (i4FsPbbContextId,
                       u4FsPbbCBPServiceMappingBackboneSid,
                       i4IfIndex, pSetValFsPbbOUI) == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbOUIRowStatus
Input       :  The Indices
FsPbbContextId
FsPbbCBPServiceMappingBackboneSid
IfIndex

The Object 
setValFsPbbOUIRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbOUIRowStatus (INT4 i4FsPbbContextId,
                         UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                         INT4 i4IfIndex, INT4 i4SetValFsPbbOUIRowStatus)
{
    if (PbbSelectContext (i4FsPbbContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSetIsidOUIRowStatus (i4FsPbbContextId,
                                u4FsPbbCBPServiceMappingBackboneSid,
                                i4IfIndex,
                                i4SetValFsPbbOUIRowStatus) == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbOUI
Input       :  The Indices
FsPbbContextId
FsPbbCBPServiceMappingBackboneSid
IfIndex

The Object 
testValFsPbbOUI
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbOUI (UINT4 *pu4ErrorCode,
                   INT4 i4FsPbbContextId,
                   UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                   INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pTestValFsPbbOUI)
{
    INT4                i4OuiRowStatus = PBB_INIT_VAL;
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbISIDOUITable (i4FsPbbContextId,
                                                   u4FsPbbCBPServiceMappingBackboneSid,
                                                   i4IfIndex);
    if (PBB_FAILURE == i4retVal)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbGetIsidOUIRowStatus (i4FsPbbContextId,
                                u4FsPbbCBPServiceMappingBackboneSid,
                                i4IfIndex, &i4OuiRowStatus) == PBB_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if ((i4OuiRowStatus) == PBB_INVALID_ROWSTATUS
        || (i4OuiRowStatus == PBB_ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PbbReleaseContext ();
        return SNMP_FAILURE;

    }
    i4retVal = PbbTestIsidOUI (pu4ErrorCode,
                               i4FsPbbContextId,
                               u4FsPbbCBPServiceMappingBackboneSid,
                               i4IfIndex, pTestValFsPbbOUI);
    if (PBB_FAILURE == i4retVal)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsPbbOUIRowStatus
Input       :  The Indices
FsPbbContextId
FsPbbCBPServiceMappingBackboneSid
IfIndex

The Object 
testValFsPbbOUIRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbOUIRowStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsPbbContextId,
                            UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                            INT4 i4IfIndex, INT4 i4TestValFsPbbOUIRowStatus)
{
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbISIDOUITable (i4FsPbbContextId,
                                                   u4FsPbbCBPServiceMappingBackboneSid,
                                                   i4IfIndex);
    if (PBB_FAILURE == i4retVal)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_FAILURE == PBB_VALID_ROW_STATUS (i4TestValFsPbbOUIRowStatus))
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i4retVal = PbbTestIsidOUIRowStatus (i4FsPbbContextId,
                                        u4FsPbbCBPServiceMappingBackboneSid,
                                        i4IfIndex, i4TestValFsPbbOUIRowStatus);
    if (PBB_FAILURE == i4retVal)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbISIDOUITable
Input       :  The Indices
FsPbbContextId
FsPbbCBPServiceMappingBackboneSid
IfIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbISIDOUITable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbPortPisidTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbPortPisidTable
Input       :  The Indices
FsPbbContextId
IfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbPortPisidTable (INT4 i4FsPbbContextId,
                                             INT4 i4IfIndex)
{
    if (PbbValidateContextId (i4FsPbbContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbValidateCNPPort (i4IfIndex) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbPortPisidTable
Input       :  The Indices
FsPbbContextId
IfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbPortPisidTable (INT4 *pi4FsPbbContextId, INT4 *pi4IfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    INT4                i4ifIndex = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstCnpPort (&i4ifIndex) == PBB_SUCCESS)
                {
                    *pi4FsPbbContextId = u4Context;
                    *pi4IfIndex = i4ifIndex;
                    PbbReleaseContext ();
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbPortPisidTable
Input       :  The Indices
FsPbbContextId
nextFsPbbContextId
IfIndex
nextIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbPortPisidTable (INT4 i4FsPbbContextId,
                                    INT4 *pi4NextFsPbbContextId,
                                    INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT4               u4Pisid = PBB_INIT_VAL;
    INT4                i4NextIfIndex = PBB_INIT_VAL;

    for (u4Context = i4FsPbbContextId; u4Context < PBB_MAX_CONTEXTS;
         u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode !=
                PBB_ICOMPONENT_BRIDGE_MODE)
            {
                PBB_CURR_CONTEXT_PTR () = NULL;
                continue;
            }
            if (PbbGetNextCnpPort (i4IfIndex, &i4NextIfIndex) == PBB_SUCCESS)
            {
                if (PbbGetPortPisid (i4IfIndex, &u4Pisid) == PBB_SUCCESS)
                {
                    if (u4Pisid != PBB_INIT_VAL)
                    {
                        *pi4NextFsPbbContextId = u4Context;
                        *pi4NextIfIndex = i4NextIfIndex;
                        PBB_CURR_CONTEXT_PTR () = NULL;
                        PBB_TRC_ARG2 (MGMT_TRC,
                                      "%s :: %s() ::  returns successfully\n",
                                      __FILE__, PBB_FUNCTION_NAME);
                        return SNMP_SUCCESS;

                    }
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbPortPisid
Input       :  The Indices
FsPbbContextId
IfIndex

The Object 
retValFsPbbPortPisid
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPortPisid (INT4 i4FsPbbContextId,
                      INT4 i4IfIndex, INT4 *pi4RetValFsPbbPortPisid)
{
    UINT4               u4Pisid = PBB_INIT_VAL;
    UINT4               u4RowStatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbPortPisidTable (i4FsPbbContextId,
                                                     i4IfIndex) != SNMP_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PbbReleaseContext ();
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PbbGetPortPisid (i4IfIndex, &u4Pisid) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    if (PbbGetPortPisidRowStatus (i4IfIndex, &u4RowStatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (u4RowStatus == PBB_INVALID_ROWSTATUS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbbPortPisid = u4Pisid;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbPIsidRowStatus
Input       :  The Indices
FsPbbContextId
IfIndex

The Object 
retValFsPbbPIsidRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPIsidRowStatus (INT4 i4FsPbbContextId,
                           INT4 i4IfIndex, INT4 *pi4RetValFsPbbPIsidRowStatus)
{
    UINT4               u4RowStatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbPortPisidTable (i4FsPbbContextId,
                                                     i4IfIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Check Bridge mode */
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PbbReleaseContext ();
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PbbGetPortPisidRowStatus (i4IfIndex, &u4RowStatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (u4RowStatus == PBB_INVALID_ROWSTATUS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsPbbPIsidRowStatus = u4RowStatus;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbPortPisid
Input       :  The Indices
FsPbbContextId
IfIndex

The Object 
setValFsPbbPortPisid
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPortPisid (INT4 i4FsPbbContextId,
                      INT4 i4IfIndex, INT4 i4SetValFsPbbPortPisid)
{
    if (PbbSelectContext (i4FsPbbContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbCreateCnpEntryDll (i4SetValFsPbbPortPisid, i4IfIndex) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PbbSetPortPisid (i4IfIndex, i4SetValFsPbbPortPisid) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbPIsidRowStatus
Input       :  The Indices
FsPbbContextId
IfIndex

The Object 
setValFsPbbPIsidRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPIsidRowStatus (INT4 i4FsPbbContextId,
                           INT4 i4IfIndex, INT4 i4SetValFsPbbPIsidRowStatus)
{
    UINT4               u4Pisid = 0;
    if (PbbSelectContext (i4FsPbbContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSetVipIsidPIsidRowStatus (i4IfIndex,
                                     i4SetValFsPbbPIsidRowStatus) !=
        PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (PbbGetPortPisid (i4IfIndex, &u4Pisid) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (i4SetValFsPbbPIsidRowStatus == PBB_ACTIVE)
    {
        if (PbbSetHwPortPisid (i4FsPbbContextId,
                               i4IfIndex, u4Pisid) != PBB_SUCCESS)
        {
            PbbReleaseContext ();
            return SNMP_FAILURE;

        }
    }
    if (i4SetValFsPbbPIsidRowStatus == PBB_DESTROY ||
        i4SetValFsPbbPIsidRowStatus == PBB_NOT_IN_SERVICE)
    {
        if (PbbDelHwPortPisid (i4FsPbbContextId,
                               i4IfIndex, u4Pisid) != PBB_SUCCESS)
        {
            PbbReleaseContext ();
            return SNMP_FAILURE;

        }
        if (i4SetValFsPbbPIsidRowStatus == PBB_DESTROY)
        {
            if (PbbDeleteCnpNodeFromCnpDLL (u4Pisid, (UINT4) i4IfIndex) ==
                PBB_FAILURE)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
        }
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbPortPisid
Input       :  The Indices
FsPbbContextId
IfIndex

The Object 
testValFsPbbPortPisid
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPortPisid (UINT4 *pu4ErrorCode,
                         INT4 i4FsPbbContextId,
                         INT4 i4IfIndex, INT4 i4TestValFsPbbPortPisid)
{
    INT4                i4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbPortPisidTable (i4FsPbbContextId,
                                                     i4IfIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetPortPisidRowStatus (i4IfIndex,
                                  (UINT4 *) &i4Rowstatus) == PBB_SUCCESS)
    {
        if ((i4Rowstatus == PBB_ACTIVE) ||
            (i4Rowstatus == PBB_INVALID_ROWSTATUS))
        {
            CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
            PbbReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFsPbbPortPisid < PBB_MIN_ISID) ||
        (i4TestValFsPbbPortPisid > PBB_MAX_ISID_VALUE))
    {
        CLI_SET_ERR (CLI_PBB_INVALID_ISID_VALUE);
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (PbbValidateIsid (i4TestValFsPbbPortPisid))
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbPIsidRowStatus
Input       :  The Indices
FsPbbContextId
IfIndex

The Object 
testValFsPbbPIsidRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPIsidRowStatus (UINT4 *pu4ErrorCode,
                              INT4 i4FsPbbContextId,
                              INT4 i4IfIndex, INT4 i4TestValFsPbbPIsidRowStatus)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    UINT4               u4Pisid = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbPortPisidTable (i4FsPbbContextId,
                                                     i4IfIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsPbbPIsidRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
        {
            if (PbbGetPortPisidRowStatus (i4IfIndex,
                                          &u4Rowstatus) == PBB_SUCCESS)
            {
                if (u4Rowstatus != PBB_INVALID_ROWSTATUS)
                {
                    CLI_SET_ERR (CLI_PBB_VALID_ROWSTATUS);
                    PbbReleaseContext ();
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return SNMP_FAILURE;
                }
                PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                              __FILE__, PBB_FUNCTION_NAME);
                return SNMP_SUCCESS;
            }
            else
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
        }
            break;
        case PBB_ACTIVE:
        {
            if (PbbGetVIPIsidPIsid (i4IfIndex, &u4Pisid) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            else
            {
                if (u4Pisid == PBB_INIT_VAL)
                {
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }
            }
        }
            break;
        case PBB_NOT_IN_SERVICE:
        case PBB_DESTROY:

        {
            PbbReleaseContext ();
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            return SNMP_SUCCESS;

        }
            break;
        case PBB_CREATE_AND_GO:
        {
            PbbReleaseContext ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
            break;
        default:
            PbbReleaseContext ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbPortPisidTable
Input       :  The Indices
FsPbbContextId
IfIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbPortPisidTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbPortTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbPortTable
Input       :  The Indices
FsPbbPipIfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbPortTable (INT4 i4FsPbbPipIfIndex)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate the System Initialized status */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate that the local port exists for interface index */
    i4retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate that the Pip port exists */
    i4retVal = PbbValidatePipPortExists (u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbPortTable
Input       :  The Indices
FsPbbPipIfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbPortTable (INT4 *pi4FsPbbPipIfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                PBB_ICOMPONENT_BRIDGE_MODE)
            {

                if (PbbGetFirstPbbPipIfIndex (pi4FsPbbPipIfIndex) ==
                    PBB_SUCCESS)
                {
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }

            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbPortTable
Input       :  The Indices
FsPbbPipIfIndex
nextFsPbbPipIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbPortTable (INT4 i4FsPbbPipIfIndex,
                               INT4 *pi4NextFsPbbPipIfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT1               u1Flag = PBB_TRUE;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (u1Flag == PBB_TRUE &&
                    PbbGetPipIfIndex (i4FsPbbPipIfIndex) == PBB_SUCCESS)
                {
                    if (PbbGetNextPbbPipIfIndex
                        (i4FsPbbPipIfIndex,
                         pi4NextFsPbbPipIfIndex) == PBB_SUCCESS)
                    {
                        PBB_TRC_ARG2 (MGMT_TRC,
                                      "%s :: %s() ::  returns successfully\n",
                                      __FILE__, PBB_FUNCTION_NAME);
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        u1Flag = PBB_FALSE;
                    }
                }
                else if (u1Flag == PBB_FALSE)
                {
                    if (PbbGetFirstPbbPipIfIndex (pi4NextFsPbbPipIfIndex) ==
                        PBB_SUCCESS)
                    {
                        PBB_TRC_ARG2 (MGMT_TRC,
                                      "%s :: %s() ::  returns successfully\n",
                                      __FILE__, PBB_FUNCTION_NAME);
                        return SNMP_SUCCESS;
                    }
                }

            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbPortPcpSelectionRow
Input       :  The Indices
FsPbbPipIfIndex

The Object 
retValFsPbbPortPcpSelectionRow
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPortPcpSelectionRow (INT4 i4FsPbbPipIfIndex,
                                INT4 *pi4RetValFsPbbPortPcpSelectionRow)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPortTable (i4FsPbbPipIfIndex);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port for interface index */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1retVal =
        PbbGetPcpSelRow (u2LocalPort,
                         (UINT1 *) pi4RetValFsPbbPortPcpSelectionRow);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbPortUseDei
Input       :  The Indices
FsPbbPipIfIndex

The Object 
retValFsPbbPortUseDei
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPortUseDei (INT4 i4FsPbbPipIfIndex, INT4 *pi4RetValFsPbbPortUseDei)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPortTable (i4FsPbbPipIfIndex);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port for interface index */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1retVal =
        PbbGetPcpUseDei (u2LocalPort, (UINT1 *) pi4RetValFsPbbPortUseDei);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbPortReqDropEncoding
Input       :  The Indices
FsPbbPipIfIndex

The Object 
retValFsPbbPortReqDropEncoding
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPortReqDropEncoding (INT4 i4FsPbbPipIfIndex,
                                INT4 *pi4RetValFsPbbPortReqDropEncoding)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPortTable (i4FsPbbPipIfIndex);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port for interface index */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1retVal =
        PbbGetPcpReqDropEncoding (u2LocalPort,
                                  (UINT1 *) pi4RetValFsPbbPortReqDropEncoding);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbPortPcpSelectionRow
Input       :  The Indices
FsPbbPipIfIndex

The Object 
setValFsPbbPortPcpSelectionRow
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPortPcpSelectionRow (INT4 i4FsPbbPipIfIndex,
                                INT4 i4SetValFsPbbPortPcpSelectionRow)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpSelRow = PBB_INIT_VAL;
    /* Get the local port for interface index */
    INT1                i1retVal =
        PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                         &u4ContextId,
                                         &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    i1retVal = PbbGetPcpSelRow (u2LocalPort, &u1PcpSelRow);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (u1PcpSelRow == (UINT1) i4SetValFsPbbPortPcpSelectionRow)
    {
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }
    i1retVal = PbbHwSetPortPcpSelection (u4ContextId,
                                         (UINT4) i4FsPbbPipIfIndex,
                                         (UINT1)
                                         i4SetValFsPbbPortPcpSelectionRow);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i1retVal =
        PbbSetPcpSelRow (u2LocalPort, (UINT1) i4SetValFsPbbPortPcpSelectionRow);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbPortUseDei
Input       :  The Indices
FsPbbPipIfIndex

The Object 
setValFsPbbPortUseDei
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPortUseDei (INT4 i4FsPbbPipIfIndex, INT4 i4SetValFsPbbPortUseDei)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpUseDei = PBB_INIT_VAL;
    /* Get the local port for interface index */
    INT1                i1retVal =
        PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                         &u4ContextId,
                                         &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    i1retVal = PbbGetPcpUseDei (u2LocalPort, &u1PcpUseDei);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (u1PcpUseDei == (UINT1) i4SetValFsPbbPortUseDei)
    {
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }
    i1retVal = PbbHwSetPortUseDei (u4ContextId,
                                   (UINT4) i4FsPbbPipIfIndex,
                                   (UINT1) i4SetValFsPbbPortUseDei);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i1retVal = PbbSetPcpUseDei (u2LocalPort, (UINT1) i4SetValFsPbbPortUseDei);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbPortReqDropEncoding
Input       :  The Indices
FsPbbPipIfIndex

The Object 
setValFsPbbPortReqDropEncoding
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPortReqDropEncoding (INT4 i4FsPbbPipIfIndex,
                                INT4 i4SetValFsPbbPortReqDropEncoding)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpReqDropEnc = PBB_INIT_VAL;
    /* Get the local port for interface index */
    INT1                i1retVal =
        PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                         &u4ContextId,
                                         &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    i1retVal = PbbGetPcpReqDropEncoding (u2LocalPort, &u1PcpReqDropEnc);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (u1PcpReqDropEnc == (UINT1) i4SetValFsPbbPortReqDropEncoding)
    {
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }
    i1retVal = PbbHwSetPortReqDropEncoding (u4ContextId,
                                            (UINT4) i4FsPbbPipIfIndex,
                                            (UINT1)
                                            i4SetValFsPbbPortReqDropEncoding);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    i1retVal =
        PbbSetPcpReqDropEncoding (u2LocalPort,
                                  (UINT1) i4SetValFsPbbPortReqDropEncoding);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbPortPcpSelectionRow
Input       :  The Indices
FsPbbPipIfIndex

The Object 
testValFsPbbPortPcpSelectionRow
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPortPcpSelectionRow (UINT4 *pu4ErrorCode,
                                   INT4 i4FsPbbPipIfIndex,
                                   INT4 i4TestValFsPbbPortPcpSelectionRow)
{
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPortTable (i4FsPbbPipIfIndex);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_IS_PCP_ROW_VALID (i4TestValFsPbbPortPcpSelectionRow) == PBB_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbPortUseDei
Input       :  The Indices
FsPbbPipIfIndex

The Object 
testValFsPbbPortUseDei
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPortUseDei (UINT4 *pu4ErrorCode,
                          INT4 i4FsPbbPipIfIndex, INT4 i4TestValFsPbbPortUseDei)
{
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPortTable (i4FsPbbPipIfIndex);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_IS_PCP_USE_DEI_VALID (i4TestValFsPbbPortUseDei) == PBB_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsPbbPortReqDropEncoding
Input       :  The Indices
FsPbbPipIfIndex

The Object 
testValFsPbbPortReqDropEncoding
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPortReqDropEncoding (UINT4 *pu4ErrorCode, INT4 i4FsPbbPipIfIndex,
                                   INT4 i4TestValFsPbbPortReqDropEncoding)
{
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPortTable (i4FsPbbPipIfIndex);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_IS_PCP_REQ_DROP_ENC_VALID (i4TestValFsPbbPortReqDropEncoding) ==
        PBB_FALSE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbPortTable
Input       :  The Indices
FsPbbPipIfIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbPortTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/* LOW LEVEL Routines for Table : FsPbbPcpDecodingTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbPcpDecodingTable
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbPcpDecodingTable (INT4 i4FsPbbPipIfIndex,
                                               INT4 i4FsPbbPcpDecodingPcpSelRow,
                                               INT4 i4FsPbbPcpDecodingPcpValue)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;
    /* Validate the System Initialized status */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate that the local port exists for interface index */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate that the Pip port exists */
    i1retVal = PbbValidatePipPortExists (u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Validate the value of Pcp Selection Row */
    if (PBB_IS_PCP_ROW_VALID (i4FsPbbPcpDecodingPcpSelRow) == PBB_FALSE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Validate the value of Pcp Priority Value */
    if (PBB_IS_PCP_VALUE_VALID (i4FsPbbPcpDecodingPcpValue) == PBB_FALSE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbPcpDecodingTable
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbPcpDecodingTable (INT4 *pi4FsPbbPipIfIndex,
                                       INT4 *pi4FsPbbPcpDecodingPcpSelRow,
                                       INT4 *pi4FsPbbPcpDecodingPcpValue)
{
    UINT4               u4Context = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstPbbPipIfIndex (pi4FsPbbPipIfIndex) ==
                    PBB_SUCCESS)
                {
                    *pi4FsPbbPcpDecodingPcpSelRow = PBB_8P0D_SEL_ROW;
                    *pi4FsPbbPcpDecodingPcpValue = PBB_MIN_PCP_VAL;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    PBB_CURR_CONTEXT_PTR () = NULL;
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbPcpDecodingTable
Input       :  The Indices
FsPbbPipIfIndex
nextFsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
nextFsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue
nextFsPbbPcpDecodingPcpValue
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbPcpDecodingTable (INT4 i4FsPbbPipIfIndex,
                                      INT4 *pi4NextFsPbbPipIfIndex,
                                      INT4 i4FsPbbPcpDecodingPcpSelRow,
                                      INT4 *pi4NextFsPbbPcpDecodingPcpSelRow,
                                      INT4 i4FsPbbPcpDecodingPcpValue,
                                      INT4 *pi4NextFsPbbPcpDecodingPcpValue)
{
    UINT4               u4Context = PBB_INIT_VAL;
    INT4                i4NextIfIndex = PBB_INIT_VAL;
    UINT1               u1PcpSelRow = PBB_INIT_VAL;
    UINT1               u1PcpVal = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    for (; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetNextPcpDecodingIndices (i4FsPbbPipIfIndex,
                                                  &i4NextIfIndex,
                                                  (UINT1)
                                                  i4FsPbbPcpDecodingPcpSelRow,
                                                  &u1PcpSelRow,
                                                  (UINT1)
                                                  i4FsPbbPcpDecodingPcpValue,
                                                  &u1PcpVal) == PBB_SUCCESS)
                {
                    *pi4NextFsPbbPipIfIndex = i4NextIfIndex;
                    *pi4NextFsPbbPcpDecodingPcpSelRow = (INT4) u1PcpSelRow;
                    *pi4NextFsPbbPcpDecodingPcpValue = (INT4) u1PcpVal;
                    PBB_CURR_CONTEXT_PTR () = NULL;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbPcpDecodingPriority
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue

The Object 
retValFsPbbPcpDecodingPriority
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPcpDecodingPriority (INT4 i4FsPbbPipIfIndex,
                                INT4 i4FsPbbPcpDecodingPcpSelRow,
                                INT4 i4FsPbbPcpDecodingPcpValue,
                                INT4 *pi4RetValFsPbbPcpDecodingPriority)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPcpDecodingTable (i4FsPbbPipIfIndex,
                                                       i4FsPbbPcpDecodingPcpSelRow,
                                                       i4FsPbbPcpDecodingPcpValue);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1retVal = PbbGetPcpDecodingPriority (u2LocalPort,
                                          (UINT1) i4FsPbbPcpDecodingPcpSelRow,
                                          (UINT1) i4FsPbbPcpDecodingPcpValue,
                                          (UINT1 *)
                                          pi4RetValFsPbbPcpDecodingPriority);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsPbbPcpDecodingDropEligible
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue

The Object 
retValFsPbbPcpDecodingDropEligible
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPcpDecodingDropEligible (INT4 i4FsPbbPipIfIndex,
                                    INT4 i4FsPbbPcpDecodingPcpSelRow,
                                    INT4 i4FsPbbPcpDecodingPcpValue,
                                    INT4 *pi4RetValFsPbbPcpDecodingDropEligible)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPcpDecodingTable (i4FsPbbPipIfIndex,
                                                       i4FsPbbPcpDecodingPcpSelRow,
                                                       i4FsPbbPcpDecodingPcpValue);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1retVal = PbbGetPcpDecodingDE (u2LocalPort,
                                    (UINT1) i4FsPbbPcpDecodingPcpSelRow,
                                    (UINT1) i4FsPbbPcpDecodingPcpValue,
                                    (UINT1 *)
                                    pi4RetValFsPbbPcpDecodingDropEligible);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (*pi4RetValFsPbbPcpDecodingDropEligible == PBB_DE_FALSE)
    {
        *pi4RetValFsPbbPcpDecodingDropEligible = PBB_VLAN_SNMP_FALSE;
    }
    else
    {
        *pi4RetValFsPbbPcpDecodingDropEligible = PBB_VLAN_SNMP_TRUE;
    }

    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbPcpDecodingPriority
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue

The Object 
setValFsPbbPcpDecodingPriority
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPcpDecodingPriority (INT4 i4FsPbbPipIfIndex,
                                INT4 i4FsPbbPcpDecodingPcpSelRow,
                                INT4 i4FsPbbPcpDecodingPcpValue,
                                INT4 i4SetValFsPbbPcpDecodingPriority)
{
    tHwPbbPcpInfo       PcpTblEntry;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;

    PBB_MEMSET (&PcpTblEntry, PBB_INIT_VAL, sizeof (tHwPbbPcpInfo));
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    PcpTblEntry.u2PcpSelRow = (UINT2) i4FsPbbPcpDecodingPcpSelRow;
    PcpTblEntry.u2PcpValue = (UINT2) i4FsPbbPcpDecodingPcpValue;
    PcpTblEntry.u2Priority = (UINT2) i4SetValFsPbbPcpDecodingPriority;
    i1retVal = PbbGetPcpDecodingDE (u2LocalPort,
                                    i4FsPbbPcpDecodingPcpSelRow,
                                    i4FsPbbPcpDecodingPcpValue,
                                    &u1DropEligible);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PcpTblEntry.u1DropEligible = u1DropEligible;
    i1retVal = PbbHwSetPcpDecodTbl (u4ContextId,
                                    (UINT4) i4FsPbbPipIfIndex, PcpTblEntry);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i1retVal = PbbSetPcpDecodingPriority (u2LocalPort,
                                          (UINT1) i4FsPbbPcpDecodingPcpSelRow,
                                          (UINT1) i4FsPbbPcpDecodingPcpValue,
                                          (UINT1)
                                          i4SetValFsPbbPcpDecodingPriority);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbPcpDecodingDropEligible
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue

The Object 
setValFsPbbPcpDecodingDropEligible
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPcpDecodingDropEligible (INT4 i4FsPbbPipIfIndex,
                                    INT4 i4FsPbbPcpDecodingPcpSelRow,
                                    INT4 i4FsPbbPcpDecodingPcpValue,
                                    INT4 i4SetValFsPbbPcpDecodingDropEligible)
{
    tHwPbbPcpInfo       PcpTblEntry;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1Priority = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;

    PBB_MEMSET (&PcpTblEntry, PBB_INIT_VAL, sizeof (tHwPbbPcpInfo));
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    if (i4SetValFsPbbPcpDecodingDropEligible == PBB_VLAN_SNMP_FALSE)
    {
        i4SetValFsPbbPcpDecodingDropEligible = PBB_DE_FALSE;
    }
    else
    {
        i4SetValFsPbbPcpDecodingDropEligible = PBB_DE_TRUE;
    }

    PcpTblEntry.u2PcpSelRow = (UINT2) i4FsPbbPcpDecodingPcpSelRow;
    PcpTblEntry.u2PcpValue = (UINT2) i4FsPbbPcpDecodingPcpValue;
    PcpTblEntry.u1DropEligible = (UINT1) i4SetValFsPbbPcpDecodingDropEligible;
    i1retVal = PbbGetPcpDecodingPriority (u2LocalPort,
                                          i4FsPbbPcpDecodingPcpSelRow,
                                          i4FsPbbPcpDecodingPcpValue,
                                          &u1Priority);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PcpTblEntry.u2Priority = (UINT2) u1Priority;
    i1retVal = PbbHwSetPcpDecodTbl (u4ContextId,
                                    (UINT4) i4FsPbbPipIfIndex, PcpTblEntry);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i1retVal = PbbSetPcpDecodingDE (u2LocalPort,
                                    (UINT1) i4FsPbbPcpDecodingPcpSelRow,
                                    (UINT1) i4FsPbbPcpDecodingPcpValue,
                                    (UINT1)
                                    i4SetValFsPbbPcpDecodingDropEligible);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbPcpDecodingPriority
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue

The Object 
testValFsPbbPcpDecodingPriority
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPcpDecodingPriority (UINT4 *pu4ErrorCode,
                                   INT4 i4FsPbbPipIfIndex,
                                   INT4 i4FsPbbPcpDecodingPcpSelRow,
                                   INT4 i4FsPbbPcpDecodingPcpValue,
                                   INT4 i4TestValFsPbbPcpDecodingPriority)
{
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;
    UINT1               u1PcpPriority = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;

    i1retVal = nmhValidateIndexInstanceFsPbbPcpDecodingTable (i4FsPbbPipIfIndex,
                                                              i4FsPbbPcpDecodingPcpSelRow,
                                                              i4FsPbbPcpDecodingPcpValue);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_IS_PRIORITY_VALID (i4TestValFsPbbPcpDecodingPriority) == PBB_FALSE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i1retVal = PbbGetPcpDecodingPriority (u2LocalPort,
                                          (UINT1) i4FsPbbPcpDecodingPcpSelRow,
                                          (UINT1) i4FsPbbPcpDecodingPcpValue,
                                          &u1PcpPriority);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u1PcpPriority == i4TestValFsPbbPcpDecodingPriority)
    {
        PbbReleaseContext ();
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }
    i1retVal = PbbGetPcpDecodingDE (u2LocalPort,
                                    (UINT1) i4FsPbbPcpDecodingPcpSelRow,
                                    (UINT1) i4FsPbbPcpDecodingPcpValue,
                                    &u1DropEligible);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i1retVal = PbbValidatePcpDecodingValues (u2LocalPort,
                                             (UINT1)
                                             i4FsPbbPcpDecodingPcpSelRow,
                                             (UINT1) i4FsPbbPcpDecodingPcpValue,
                                             (UINT1)
                                             i4TestValFsPbbPcpDecodingPriority,
                                             u1DropEligible);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbPcpDecodingDropEligible
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue

The Object 
testValFsPbbPcpDecodingDropEligible
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPcpDecodingDropEligible (UINT4 *pu4ErrorCode,
                                       INT4 i4FsPbbPipIfIndex,
                                       INT4 i4FsPbbPcpDecodingPcpSelRow,
                                       INT4 i4FsPbbPcpDecodingPcpValue,
                                       INT4
                                       i4TestValFsPbbPcpDecodingDropEligible)
{
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT1               u1Priority = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;

    i1retVal = nmhValidateIndexInstanceFsPbbPcpDecodingTable (i4FsPbbPipIfIndex,
                                                              i4FsPbbPcpDecodingPcpSelRow,
                                                              i4FsPbbPcpDecodingPcpValue);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_IS_PCP_DE_VALID (i4TestValFsPbbPcpDecodingDropEligible)
        == PBB_FALSE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsPbbPcpDecodingDropEligible == PBB_VLAN_SNMP_TRUE)
    {
        i4TestValFsPbbPcpDecodingDropEligible = PBB_DE_TRUE;
    }
    else
    {
        i4TestValFsPbbPcpDecodingDropEligible = PBB_DE_FALSE;
    }
    i1retVal = PbbGetPcpDecodingDE (u2LocalPort,
                                    (UINT1) i4FsPbbPcpDecodingPcpSelRow,
                                    (UINT1) i4FsPbbPcpDecodingPcpValue,
                                    &u1DropEligible);
    if (u1DropEligible == i4TestValFsPbbPcpDecodingDropEligible)
    {
        PbbReleaseContext ();
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }

    i1retVal = PbbGetPcpDecodingPriority (u2LocalPort,
                                          (UINT1) i4FsPbbPcpDecodingPcpSelRow,
                                          (UINT1) i4FsPbbPcpDecodingPcpValue,
                                          &u1Priority);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i1retVal = PbbValidatePcpDecodingValues (u2LocalPort,
                                             (UINT1)
                                             i4FsPbbPcpDecodingPcpSelRow,
                                             (UINT1) i4FsPbbPcpDecodingPcpValue,
                                             u1Priority,
                                             (UINT1)
                                             i4TestValFsPbbPcpDecodingDropEligible);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbPcpDecodingTable
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpDecodingPcpSelRow
FsPbbPcpDecodingPcpValue
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbPcpDecodingTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbPcpEncodingTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbPcpEncodingTable
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpEncodingPcpSelRow
FsPbbPcpEncodingPriority
FsPbbPcpEncodingDropEligible
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbPcpEncodingTable (INT4 i4FsPbbPipIfIndex,
                                               INT4 i4FsPbbPcpEncodingPcpSelRow,
                                               INT4 i4FsPbbPcpEncodingPriority,
                                               INT4
                                               i4FsPbbPcpEncodingDropEligible)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;
    /* Validate the System Initialized status */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate that the local port exists for interface index */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate that the Pip port exists */
    i1retVal = PbbValidatePipPortExists (u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Validate the value of Pcp Selection Row */
    if (PBB_IS_PCP_ROW_VALID (i4FsPbbPcpEncodingPcpSelRow) == PBB_FALSE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Validate the value of Pcp Value */
    if (PBB_IS_PRIORITY_VALID (i4FsPbbPcpEncodingPriority) == PBB_FALSE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Validate the value of Pcp Value */
    if (PBB_IS_PCP_DE_VALID (i4FsPbbPcpEncodingDropEligible) == PBB_FALSE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbPcpEncodingTable
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpEncodingPcpSelRow
FsPbbPcpEncodingPriority
FsPbbPcpEncodingDropEligible
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbPcpEncodingTable (INT4 *pi4FsPbbPipIfIndex,
                                       INT4 *pi4FsPbbPcpEncodingPcpSelRow,
                                       INT4 *pi4FsPbbPcpEncodingPriority,
                                       INT4 *pi4FsPbbPcpEncodingDropEligible)
{
    UINT4               u4Context = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstPbbPipIfIndex (pi4FsPbbPipIfIndex) ==
                    PBB_SUCCESS)
                {
                    *pi4FsPbbPcpEncodingPcpSelRow = PBB_8P0D_SEL_ROW;
                    *pi4FsPbbPcpEncodingPriority = PBB_MIN_PRIORITY;
                    *pi4FsPbbPcpEncodingDropEligible = PBB_VLAN_SNMP_TRUE;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    PBB_CURR_CONTEXT_PTR () = NULL;
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbPcpEncodingTable
Input       :  The Indices
FsPbbPipIfIndex
nextFsPbbPipIfIndex
FsPbbPcpEncodingPcpSelRow
nextFsPbbPcpEncodingPcpSelRow
FsPbbPcpEncodingPriority
nextFsPbbPcpEncodingPriority
FsPbbPcpEncodingDropEligible
nextFsPbbPcpEncodingDropEligible
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbPcpEncodingTable (INT4 i4FsPbbPipIfIndex,
                                      INT4 *pi4NextFsPbbPipIfIndex,
                                      INT4 i4FsPbbPcpEncodingPcpSelRow,
                                      INT4 *pi4NextFsPbbPcpEncodingPcpSelRow,
                                      INT4 i4FsPbbPcpEncodingPriority,
                                      INT4 *pi4NextFsPbbPcpEncodingPriority,
                                      INT4 i4FsPbbPcpEncodingDropEligible,
                                      INT4 *pi4NextFsPbbPcpEncodingDropEligible)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT1               u1PcpSelRow = PBB_INIT_VAL;
    INT4                i4NextIfIndex = PBB_INIT_VAL;
    UINT1               u1PcpPriority = PBB_INIT_VAL;
    UINT1               u1PcpDE = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    for (; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetNextPcpEncodingIndices (i4FsPbbPipIfIndex,
                                                  &i4NextIfIndex,
                                                  (UINT1)
                                                  i4FsPbbPcpEncodingPcpSelRow,
                                                  &u1PcpSelRow,
                                                  (UINT1)
                                                  i4FsPbbPcpEncodingPriority,
                                                  &u1PcpPriority,
                                                  (UINT1)
                                                  i4FsPbbPcpEncodingDropEligible,
                                                  &u1PcpDE) == PBB_SUCCESS)
                {
                    *pi4NextFsPbbPipIfIndex = i4NextIfIndex;
                    *pi4NextFsPbbPcpEncodingPcpSelRow = (INT4) u1PcpSelRow;
                    *pi4NextFsPbbPcpEncodingPriority = (INT4) u1PcpPriority;
                    *pi4NextFsPbbPcpEncodingDropEligible = (INT4) u1PcpDE;
                    PBB_CURR_CONTEXT_PTR () = NULL;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;

                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbPcpEncodingPcpValue
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpEncodingPcpSelRow
FsPbbPcpEncodingPriority
FsPbbPcpEncodingDropEligible

The Object 
retValFsPbbPcpEncodingPcpValue
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPcpEncodingPcpValue (INT4 i4FsPbbPipIfIndex,
                                INT4 i4FsPbbPcpEncodingPcpSelRow,
                                INT4 i4FsPbbPcpEncodingPriority,
                                INT4 i4FsPbbPcpEncodingDropEligible,
                                INT4 *pi4RetValFsPbbPcpEncodingPcpValue)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPcpEncodingTable (i4FsPbbPipIfIndex,
                                                       i4FsPbbPcpEncodingPcpSelRow,
                                                       i4FsPbbPcpEncodingPriority,
                                                       i4FsPbbPcpEncodingDropEligible);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (i4FsPbbPcpEncodingDropEligible == PBB_VLAN_SNMP_TRUE)
    {
        i4FsPbbPcpEncodingDropEligible = PBB_DE_TRUE;
    }
    else
    {
        i4FsPbbPcpEncodingDropEligible = PBB_DE_FALSE;
    }
    i1retVal = PbbGetPcpEncodingPcpVal (u2LocalPort,
                                        (UINT1) i4FsPbbPcpEncodingPcpSelRow,
                                        (UINT1) i4FsPbbPcpEncodingPriority,
                                        (UINT1) i4FsPbbPcpEncodingDropEligible,
                                        (UINT1 *)
                                        pi4RetValFsPbbPcpEncodingPcpValue);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbPcpEncodingPcpValue
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpEncodingPcpSelRow
FsPbbPcpEncodingPriority
FsPbbPcpEncodingDropEligible

The Object 
setValFsPbbPcpEncodingPcpValue
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPcpEncodingPcpValue (INT4 i4FsPbbPipIfIndex,
                                INT4 i4FsPbbPcpEncodingPcpSelRow,
                                INT4 i4FsPbbPcpEncodingPriority,
                                INT4 i4FsPbbPcpEncodingDropEligible,
                                INT4 i4SetValFsPbbPcpEncodingPcpValue)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;
    tHwPbbPcpInfo       PcpTblEntry;
    PBB_MEMSET (&PcpTblEntry, PBB_INIT_VAL, sizeof (tHwPbbPcpInfo));
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }

    if (i4FsPbbPcpEncodingDropEligible == PBB_VLAN_SNMP_TRUE)
    {
        i4FsPbbPcpEncodingDropEligible = PBB_DE_TRUE;
    }
    else
    {
        i4FsPbbPcpEncodingDropEligible = PBB_DE_FALSE;
    }
    PcpTblEntry.u2PcpSelRow = (UINT2) i4FsPbbPcpEncodingPcpSelRow;
    PcpTblEntry.u2PcpValue = (UINT2) i4SetValFsPbbPcpEncodingPcpValue;
    PcpTblEntry.u2Priority = (UINT2) i4FsPbbPcpEncodingPriority;
    PcpTblEntry.u1DropEligible = (UINT1) i4FsPbbPcpEncodingDropEligible;
    i1retVal = PbbHwSetPcpEncodTbl (u4ContextId,
                                    (UINT4) i4FsPbbPipIfIndex, PcpTblEntry);
    i1retVal = PbbSetPcpEncodingPcpVal (u2LocalPort,
                                        (UINT1) i4FsPbbPcpEncodingPcpSelRow,
                                        (UINT1) i4FsPbbPcpEncodingPriority,
                                        (UINT1) i4FsPbbPcpEncodingDropEligible,
                                        (UINT1)
                                        i4SetValFsPbbPcpEncodingPcpValue);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbPcpEncodingPcpValue
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpEncodingPcpSelRow
FsPbbPcpEncodingPriority
FsPbbPcpEncodingDropEligible

The Object 
testValFsPbbPcpEncodingPcpValue
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPcpEncodingPcpValue (UINT4 *pu4ErrorCode,
                                   INT4 i4FsPbbPipIfIndex,
                                   INT4 i4FsPbbPcpEncodingPcpSelRow,
                                   INT4 i4FsPbbPcpEncodingPriority,
                                   INT4 i4FsPbbPcpEncodingDropEligible,
                                   INT4 i4TestValFsPbbPcpEncodingPcpValue)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1PcpVal = PBB_INIT_VAL;
    UINT1               u1Priority = PBB_INIT_VAL;
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbPcpEncodingTable (i4FsPbbPipIfIndex,
                                                       i4FsPbbPcpEncodingPcpSelRow,
                                                       i4FsPbbPcpEncodingPriority,
                                                       i4FsPbbPcpEncodingDropEligible);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the local port and context id */
    i1retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i1retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_IS_PCP_VALUE_VALID (i4TestValFsPbbPcpEncodingPcpValue) == PBB_FALSE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    i1retVal = PbbGetPcpEncodingPcpVal (u2LocalPort,
                                        (UINT1) i4FsPbbPcpEncodingPcpSelRow,
                                        (UINT1) i4FsPbbPcpEncodingPriority,
                                        (UINT1) i4FsPbbPcpEncodingDropEligible,
                                        &u1PcpVal);
    if (i1retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u1PcpVal == (UINT1) i4TestValFsPbbPcpEncodingPcpValue)
    {
        PbbReleaseContext ();
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }

    /* 
     * For any two priority values with the same drop_eligible value, the
     * lower priority shall not map to a higher PCP than the higher
     * priority.
     */
    for (u1Priority = PBB_MIN_PRIORITY; u1Priority < PBB_MAX_PRIORITY;
         u1Priority++)
    {
        i1retVal = PbbGetPcpEncodingPcpVal (u2LocalPort,
                                            (UINT1) i4FsPbbPcpEncodingPcpSelRow,
                                            (UINT1) u1Priority,
                                            (UINT1)
                                            i4FsPbbPcpEncodingDropEligible,
                                            &u1PcpVal);
        if (i1retVal == PBB_FAILURE)
        {
            PbbReleaseContext ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if ((UINT1) i4FsPbbPcpEncodingPriority < u1Priority)
        {
            /* The configured priority is less than some of the other 
             * priorities with same drop eligible.
             */
            if ((UINT1) i4TestValFsPbbPcpEncodingPcpValue > u1PcpVal)
            {
                /* The PCP value is greater */
                CLI_SET_ERR (CLI_PBB_PCP_VALUE_INVALID);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }

    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbPcpEncodingTable
Input       :  The Indices
FsPbbPipIfIndex
FsPbbPcpEncodingPcpSelRow
FsPbbPcpEncodingPriority
FsPbbPcpEncodingDropEligible
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbPcpEncodingTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbInstanceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbbInstanceTable
 Input       :  The Indices
                FsPbbInstanceId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbInstanceTable (INT4 i4FsPbbInstanceId)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate the value of Instance id */
    if (PbbValidateInstanceId (i4FsPbbInstanceId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbbInstanceTable
 Input       :  The Indices
                FsPbbInstanceId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbInstanceTable (INT4 *pi4FsPbbInstanceId)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    return nmhGetNextIndexFsPbbInstanceTable (-1, pi4FsPbbInstanceId);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbbInstanceTable
 Input       :  The Indices
                FsPbbInstanceId
                nextFsPbbInstanceId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbInstanceTable (INT4 i4FsPbbInstanceId,
                                   INT4 *pi4NextFsPbbInstanceId)
{
    UINT4               u4Instance = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    for (u4Instance = (UINT4) i4FsPbbInstanceId + 1;
         u4Instance < PBB_MAX_INSTANCES; u4Instance++)
    {
        if (PBB_INSTANCE_PTR (u4Instance) != NULL)
        {
            *pi4NextFsPbbInstanceId = (INT4) u4Instance;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbbInstanceMacAddr
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                retValFsPbbInstanceMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbInstanceMacAddr (INT4 i4FsPbbInstanceId,
                            tMacAddr * pRetValFsPbbInstanceMacAddr)
{
    /* Validate the instance id */
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbInstanceTable (i4FsPbbInstanceId);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PBB_MEMCPY (pRetValFsPbbInstanceMacAddr,
                PBB_CURR_INSTANCE_PTR ()->InstanceMacAddr, sizeof (tMacAddr));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbInstanceName
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                retValFsPbbInstanceName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbInstanceName (INT4 i4FsPbbInstanceId,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsPbbInstanceName)
{
    /* Validate the instance id */
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbInstanceTable (i4FsPbbInstanceId);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pRetValFsPbbInstanceName->i4_Length =
        PBB_STRLEN (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr);
    PBB_STRNCPY (pRetValFsPbbInstanceName->pu1_OctetList,
                 PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
                 pRetValFsPbbInstanceName->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbInstanceIComponents
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                retValFsPbbInstanceIComponents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbInstanceIComponents (INT4 i4FsPbbInstanceId,
                                UINT4 *pu4RetValFsPbbInstanceIComponents)
{
    /* Validate the instance id */
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbInstanceTable (i4FsPbbInstanceId);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsPbbInstanceIComponents =
        PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfIcomp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbInstanceBComponents
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                retValFsPbbInstanceBComponents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbInstanceBComponents (INT4 i4FsPbbInstanceId,
                                UINT4 *pu4RetValFsPbbInstanceBComponents)
{
    /* Validate the instance id */
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbInstanceTable (i4FsPbbInstanceId);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsPbbInstanceBComponents =
        PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfBcomp;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbInstanceBebPorts
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                retValFsPbbInstanceBebPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbInstanceBebPorts (INT4 i4FsPbbInstanceId,
                             UINT4 *pu4RetValFsPbbInstanceBebPorts)
{
    /* Validate the instance id */
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbInstanceTable (i4FsPbbInstanceId);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    i1retVal = PbbNumOfBebPortsForInstance ((UINT4) i4FsPbbInstanceId,
                                            pu4RetValFsPbbInstanceBebPorts);

    if (i1retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbInstanceRowStatus
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                retValFsPbbInstanceRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbInstanceRowStatus (INT4 i4FsPbbInstanceId,
                              INT4 *pi4RetValFsPbbInstanceRowStatus)
{
    /* Validate the instance id */
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbInstanceTable (i4FsPbbInstanceId);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsPbbInstanceRowStatus =
        PBB_CURR_INSTANCE_PTR ()->u1InstanceRowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbbInstanceMacAddr
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                setValFsPbbInstanceMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbInstanceMacAddr (INT4 i4FsPbbInstanceId,
                            tMacAddr SetValFsPbbInstanceMacAddr)
{
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (i4FsPbbInstanceId);
    PBB_CURR_INSTANCE_PTR ()->u1MacAddressType = PBB_INSTANCE_MAC_USER_DEF;
    PBB_MEMCPY (PBB_CURR_INSTANCE_PTR ()->InstanceMacAddr,
                SetValFsPbbInstanceMacAddr, sizeof (tMacAddr));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbInstanceName
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                setValFsPbbInstanceName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbInstanceName (INT4 i4FsPbbInstanceId,
                         tSNMP_OCTET_STRING_TYPE * pSetValFsPbbInstanceName)
{
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (i4FsPbbInstanceId);
    if (PBB_MEMCMP (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
                    pSetValFsPbbInstanceName->pu1_OctetList,
                    pSetValFsPbbInstanceName->i4_Length) == PBB_INIT_VAL)
    {
        return SNMP_SUCCESS;
    }
    PBB_MEMSET (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
                PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN);
    PBB_STRNCPY (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
                 pSetValFsPbbInstanceName->pu1_OctetList,
                 pSetValFsPbbInstanceName->i4_Length);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbInstanceRowStatus
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                setValFsPbbInstanceRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbInstanceRowStatus (INT4 i4FsPbbInstanceId,
                              INT4 i4SetValFsPbbInstanceRowStatus)
{

    switch (i4SetValFsPbbInstanceRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
            if (PBB_INSTANCE_PTR (i4FsPbbInstanceId) != NULL)
            {
                return SNMP_FAILURE;
            }
            if (PbbCreateInstanceNode ((UINT4) i4FsPbbInstanceId) ==
                PBB_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;
        case PBB_NOT_IN_SERVICE:
            PBB_CURR_INSTANCE_PTR () =
                PBB_INSTANCE_PTR ((UINT4) i4FsPbbInstanceId);
            PBB_CURR_INSTANCE_PTR ()->u1InstanceRowStatus = PBB_NOT_IN_SERVICE;
            break;
        case PBB_ACTIVE:
            PBB_CURR_INSTANCE_PTR () =
                PBB_INSTANCE_PTR ((UINT4) i4FsPbbInstanceId);
            PBB_CURR_INSTANCE_PTR ()->u1InstanceRowStatus = PBB_ACTIVE;
            break;
        case PBB_DESTROY:
            PbbDeleteInstanceNode ((UINT4) i4FsPbbInstanceId);
            break;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbbInstanceMacAddr
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                testValFsPbbInstanceMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbInstanceMacAddr (UINT4 *pu4ErrorCode,
                               INT4 i4FsPbbInstanceId, tMacAddr
                               TestValFsPbbInstanceMacAddr)
{
    tMacAddr            zeroMacAddr;
    /* Validate the instance id */
    INT1                i1retVal = SNMP_FAILURE;

    PBB_MEMSET (zeroMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));

    i1retVal = nmhValidateIndexInstanceFsPbbInstanceTable (i4FsPbbInstanceId);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        UNUSED_PARAM (TestValFsPbbInstanceMacAddr);
        return SNMP_FAILURE;
    }
    if (PBB_MEMCMP (zeroMacAddr,
                    TestValFsPbbInstanceMacAddr,
                    sizeof (tMacAddr)) == PBB_INIT_VAL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_IS_BCASTADDR (TestValFsPbbInstanceMacAddr) == PBB_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_INSTANCE_BROAD_MAC_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_IS_MCASTADDR (TestValFsPbbInstanceMacAddr) == PBB_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_INSTANCE_MULT_MAC_ERR);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_CURR_INSTANCE_PTR ()->u1InstanceRowStatus == PBB_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbInstanceName
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                testValFsPbbInstanceName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbInstanceName (UINT4 *pu4ErrorCode,
                            INT4 i4FsPbbInstanceId,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsPbbInstanceName)
{
    /* Validate the instance id */
    UINT4               u4InstanceId = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;

    i1retVal = nmhValidateIndexInstanceFsPbbInstanceTable (i4FsPbbInstanceId);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pTestValFsPbbInstanceName->i4_Length > PBB_INSTANCE_ALIAS_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    if (PBB_MEMCMP (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
                    pTestValFsPbbInstanceName->pu1_OctetList,
                    pTestValFsPbbInstanceName->i4_Length) == PBB_INIT_VAL)
    {
        return SNMP_SUCCESS;
    }
    if (PBB_CURR_INSTANCE_PTR ()->u1InstanceRowStatus == PBB_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbGetInstanceIdFromAlias (pTestValFsPbbInstanceName, &u4InstanceId)
        == PBB_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbInstanceRowStatus
 Input       :  The Indices
                FsPbbInstanceId

                The Object 
                testValFsPbbInstanceRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbInstanceRowStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4FsPbbInstanceId,
                                 INT4 i4TestValFsPbbInstanceRowStatus)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    switch (i4TestValFsPbbInstanceRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
            if (i4FsPbbInstanceId == PBB_INIT_VAL)
            {
                CLI_SET_ERR (CLI_PBB_DEF_INSTANCE_CREATED_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            /* Validate the value of Instance id */
            if (PbbValidateInstanceId (i4FsPbbInstanceId) != PBB_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case PBB_NOT_IN_SERVICE:
        case PBB_ACTIVE:
            /* Validate the value of Instance id */
            if (PbbValidateInstanceId (i4FsPbbInstanceId) == PBB_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case PBB_DESTROY:
            if (i4FsPbbInstanceId == PBB_INIT_VAL)
            {
                CLI_SET_ERR (CLI_PBB_DEF_INSTANCE_DELETE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            /* Validate the value of Instance id */
            if (PbbValidateInstanceId (i4FsPbbInstanceId) == PBB_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (PbbIsInstanceMappedToContext ((UINT4) i4FsPbbInstanceId) ==
                PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INSTANCE_DELETE_ERR);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbbInstanceTable
 Input       :  The Indices
                FsPbbInstanceId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbInstanceTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbInstanceMappingTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbbInstanceMappingTable
 Input       :  The Indices
                FsPbbContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbInstanceMappingTable (INT4 i4FsPbbContextId)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate the value of Instance id */
    if (PbbValidateContextId (i4FsPbbContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PbbReleaseContext ();
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbbInstanceMappingTable
 Input       :  The Indices
                FsPbbContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbInstanceMappingTable (INT4 *pi4FsPbbContextId)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    return nmhGetNextIndexFsPbbInstanceMappingTable (-1, pi4FsPbbContextId);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbbInstanceMappingTable
 Input       :  The Indices
                FsPbbContextId
                nextFsPbbContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbInstanceMappingTable (INT4 i4FsPbbContextId,
                                          INT4 *pi4NextFsPbbContextId)
{
    UINT4               u4Context = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    for (u4Context = (UINT4) i4FsPbbContextId + 1;
         u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_TRUE)
            {
                *pi4NextFsPbbContextId = (INT4) u4Context;
                PBB_CURR_CONTEXT_PTR () = NULL;
                return SNMP_SUCCESS;
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbbContextToInstanceId
 Input       :  The Indices
                FsPbbContextId

                The Object 
                retValFsPbbContextToInstanceId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbContextToInstanceId (INT4 i4FsPbbContextId,
                                INT4 *pi4RetValFsPbbContextToInstanceId)
{
    /* Validate the instance id */
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbInstanceMappingTable (i4FsPbbContextId);
    if (i1retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsPbbContextToInstanceId = PBB_CURR_CONTEXT_PTR ()->u4InstanceId;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbbContextToInstanceId
 Input       :  The Indices
                FsPbbContextId

                The Object 
                setValFsPbbContextToInstanceId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbContextToInstanceId (INT4 i4FsPbbContextId,
                                INT4 i4SetValFsPbbContextToInstanceId)
{
    if (PbbUpdateInstanceId ((UINT4) i4FsPbbContextId,
                             (UINT4) i4SetValFsPbbContextToInstanceId) !=
        PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext ((UINT4) i4FsPbbContextId) == PBB_SUCCESS)
    {
    }
    else
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbbContextToInstanceId
 Input       :  The Indices
                FsPbbContextId

                The Object 
                testValFsPbbContextToInstanceId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbContextToInstanceId (UINT4 *pu4ErrorCode,
                                   INT4 i4FsPbbContextId,
                                   INT4 i4TestValFsPbbContextToInstanceId)
{
    /* Validate the instance id */
    INT1                i1retVal =
        nmhValidateIndexInstanceFsPbbInstanceMappingTable (i4FsPbbContextId);
    if (i1retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4TestValFsPbbContextToInstanceId < PBB_INIT_VAL ||
        (UINT4) i4TestValFsPbbContextToInstanceId > PBB_MAX_INSTANCES - 1)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_INSTANCE_PTR (i4TestValFsPbbContextToInstanceId) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbbInstanceMappingTable
 Input       :  The Indices
                FsPbbContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbInstanceMappingTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
