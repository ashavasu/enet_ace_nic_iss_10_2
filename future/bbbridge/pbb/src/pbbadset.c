/*******************************************************************
 * Copyright (C) 2012 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbadset.c,v 1.3 2012/08/02 10:00:59 siva Exp $
 *
 *******************************************************************/

#include "pbbinc.h"

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbNpInitHw
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbNpInitHw (VOID)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsMiPbbNpInitHwSync (RM_PBB_APP_ID,
                                           NPSYNC_FS_MI_PBB_NP_INIT_HW);
            }
        }
#undef FsMiPbbNpInitHw
        i4RetVal = FsMiPbbNpInitHw ();
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsMiPbbNpInitHwSync (RM_PBB_APP_ID,
                                               NPSYNC_FS_MI_PBB_NP_INIT_HW);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync, NPSYNC_FS_MI_PBB_NP_INIT_HW,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbNpDeInitHw
 *
 * -------------------------------------------------------------
 */
PUBLIC VOID
NpSyncFsMiPbbNpDeInitHw (VOID)
{
    unNpSync            NpSync;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsMiPbbNpDeInitHwSync (RM_PBB_APP_ID,
                                             NPSYNC_FS_MI_PBB_NP_DE_INIT_HW);
            }
        }
#undef FsMiPbbNpDeInitHw
        FsMiPbbNpDeInitHw ();
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_NP_DE_INIT_HW, 0);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetVipAttributes
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetVipAttributes (UINT4 u4ContextId,
                                 UINT4 u4VipIfIndex, tVipAttribute VipAttribute)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PbbPortIdx.u4ContextId = u4ContextId;
    NpSync.PbbPortIdx.u4VipIfIndex = u4VipIfIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncPbbPortIdxSync (NpSync.PbbPortIdx, RM_PBB_APP_ID,
                                      NPSYNC_FS_MI_PBB_HW_SET_VIP_ATTRIBUTES);
            }
        }
#undef FsMiPbbHwSetVipAttributes
        i4RetVal =
            FsMiPbbHwSetVipAttributes (u4ContextId, u4VipIfIndex, VipAttribute);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncPbbPortIdxSync (NpSync.PbbPortIdx, RM_PBB_APP_ID,
                                          NPSYNC_FS_MI_PBB_HW_SET_VIP_ATTRIBUTES);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_HW_SET_VIP_ATTRIBUTES,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwDelVipAttributes
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwDelVipAttributes (UINT4 u4ContextId, UINT4 u4VipIfIndex)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PbbPortIdx.u4ContextId = u4ContextId;
    NpSync.PbbPortIdx.u4VipIfIndex = u4VipIfIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncPbbPortIdxSync (NpSync.PbbPortIdx, RM_PBB_APP_ID,
                                      NPSYNC_FS_MI_PBB_HW_DEL_VIP_ATTRIBUTES);
            }
        }
#undef FsMiPbbHwDelVipAttributes
        i4RetVal = FsMiPbbHwDelVipAttributes (u4ContextId, u4VipIfIndex);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncPbbPortIdxSync (NpSync.PbbPortIdx, RM_PBB_APP_ID,
                                          NPSYNC_FS_MI_PBB_HW_DEL_VIP_ATTRIBUTES);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_HW_DEL_VIP_ATTRIBUTES,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetVipPipMap
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetVipPipMap (UINT4 u4ContextId,
                             UINT4 u4VipIfIndex, tVipPipMap VipPipMap)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PbbPortIdx.u4ContextId = u4ContextId;
    NpSync.PbbPortIdx.u4VipIfIndex = u4VipIfIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncPbbPortIdxSync (NpSync.PbbPortIdx, RM_PBB_APP_ID,
                                      NPSYNC_FS_MI_PBB_HW_SET_VIP_PIP_MAP);
            }
        }
#undef FsMiPbbHwSetVipPipMap
        i4RetVal = FsMiPbbHwSetVipPipMap (u4ContextId, u4VipIfIndex, VipPipMap);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncPbbPortIdxSync (NpSync.PbbPortIdx, RM_PBB_APP_ID,
                                          NPSYNC_FS_MI_PBB_HW_SET_VIP_PIP_MAP);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_HW_SET_VIP_PIP_MAP,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwDelVipPipMap
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwDelVipPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PbbPortIdx.u4ContextId = u4ContextId;
    NpSync.PbbPortIdx.u4VipIfIndex = u4VipIfIndex;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncPbbPortIdxSync (NpSync.PbbPortIdx, RM_PBB_APP_ID,
                                      NPSYNC_FS_MI_PBB_HW_DEL_VIP_PIP_MAP);
            }
        }
#undef FsMiPbbHwDelVipPipMap
        i4RetVal = FsMiPbbHwDelVipPipMap (u4ContextId, u4VipIfIndex);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncPbbPortIdxSync (NpSync.PbbPortIdx, RM_PBB_APP_ID,
                                          NPSYNC_FS_MI_PBB_HW_DEL_VIP_PIP_MAP);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_HW_DEL_VIP_PIP_MAP,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetBackboneServiceInstEntry
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetBackboneServiceInstEntry (UINT4 u4ContextId,
                                            UINT4 u4CbpIfIndex,
                                            UINT4 u4BSid,
                                            tBackboneServiceInstEntry
                                            BackboneServiceInstEntry)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.IsidIdx.u4ContextId = u4ContextId;
    NpSync.IsidIdx.u4CbpIfIndex = u4CbpIfIndex;
    NpSync.IsidIdx.u4BSid = u4BSid;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncIsidIdxSync (NpSync.IsidIdx, RM_PBB_APP_ID,
                                   NPSYNC_FS_MI_PBB_HW_SET_BACKBONE_SERVICE_INST_ENTRY);
            }
        }
#undef FsMiPbbHwSetBackboneServiceInstEntry
        i4RetVal =
            FsMiPbbHwSetBackboneServiceInstEntry (u4ContextId, u4CbpIfIndex,
                                                  u4BSid,
                                                  BackboneServiceInstEntry);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncIsidIdxSync (NpSync.IsidIdx, RM_PBB_APP_ID,
                                       NPSYNC_FS_MI_PBB_HW_SET_BACKBONE_SERVICE_INST_ENTRY);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_HW_SET_BACKBONE_SERVICE_INST_ENTRY,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwDelBackboneServiceInstEntry
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwDelBackboneServiceInstEntry (UINT4 u4ContextId,
                                            UINT4 u4CbpIfIndex, UINT4 u4BSid)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.IsidIdx.u4ContextId = u4ContextId;
    NpSync.IsidIdx.u4CbpIfIndex = u4CbpIfIndex;
    NpSync.IsidIdx.u4BSid = u4BSid;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncIsidIdxSync (NpSync.IsidIdx, RM_PBB_APP_ID,
                                   NPSYNC_FS_MI_PBB_HW_DEL_BACKBONE_SERVICE_INST_ENTRY);
            }
        }
#undef FsMiPbbHwDelBackboneServiceInstEntry
        i4RetVal =
            FsMiPbbHwDelBackboneServiceInstEntry (u4ContextId, u4CbpIfIndex,
                                                  u4BSid);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncIsidIdxSync (NpSync.IsidIdx, RM_PBB_APP_ID,
                                       NPSYNC_FS_MI_PBB_HW_DEL_BACKBONE_SERVICE_INST_ENTRY);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_HW_DEL_BACKBONE_SERVICE_INST_ENTRY,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiBrgHwCreateControlPktFilter
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiBrgHwCreateControlPktFilter (UINT4 u4ContextId,
                                       tFilterEntry FilterEntry)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.ContextIdx.u4ContextId = u4ContextId;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncContextIdxSync (NpSync.ContextIdx, RM_PBB_APP_ID,
                                      NPSYNC_FS_MI_BRG_HW_CREATE_CONTROL_PKT_FILTER);
            }
        }
#undef FsMiBrgHwCreateControlPktFilter
        i4RetVal = FsMiBrgHwCreateControlPktFilter (u4ContextId, FilterEntry);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncContextIdxSync (NpSync.ContextIdx, RM_PBB_APP_ID,
                                          NPSYNC_FS_MI_BRG_HW_CREATE_CONTROL_PKT_FILTER);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_BRG_HW_CREATE_CONTROL_PKT_FILTER,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiBrgHwDeleteControlPktFilter
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiBrgHwDeleteControlPktFilter (UINT4 u4ContextId,
                                       tFilterEntry FilterEntry)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.ContextIdx.u4ContextId = u4ContextId;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncContextIdxSync (NpSync.ContextIdx, RM_PBB_APP_ID,
                                      NPSYNC_FS_MI_BRG_HW_DELETE_CONTROL_PKT_FILTER);
            }
        }
#undef FsMiBrgHwDeleteControlPktFilter
        i4RetVal = FsMiBrgHwDeleteControlPktFilter (u4ContextId, FilterEntry);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncContextIdxSync (NpSync.ContextIdx, RM_PBB_APP_ID,
                                          NPSYNC_FS_MI_BRG_HW_DELETE_CONTROL_PKT_FILTER);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_BRG_HW_DELETE_CONTROL_PKT_FILTER,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetAllToOneBundlingService
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetAllToOneBundlingService (UINT4 u4ContextId,
                                           UINT4 u4IfIndex, UINT4 u4Isid)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsMiPbbHwSetAllToOneBundlingService.u4ContextId = u4ContextId;
    NpSync.FsMiPbbHwSetAllToOneBundlingService.u4IfIndex = u4IfIndex;
    NpSync.FsMiPbbHwSetAllToOneBundlingService.u4Isid = u4Isid;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsMiPbbHwSetAllToOneBundlingServiceSync (NpSync.
                                                               FsMiPbbHwSetAllToOneBundlingService,
                                                               RM_PBB_APP_ID,
                                                               NPSYNC_FS_MI_PBB_HW_SET_ALL_TO_ONE_BUNDLING_SERVICE);
            }
        }
#undef FsMiPbbHwSetAllToOneBundlingService
        i4RetVal =
            FsMiPbbHwSetAllToOneBundlingService (u4ContextId, u4IfIndex,
                                                 u4Isid);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsMiPbbHwSetAllToOneBundlingServiceSync (NpSync.
                                                                   FsMiPbbHwSetAllToOneBundlingService,
                                                                   RM_PBB_APP_ID,
                                                                   NPSYNC_FS_MI_PBB_HW_SET_ALL_TO_ONE_BUNDLING_SERVICE);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_HW_SET_ALL_TO_ONE_BUNDLING_SERVICE,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwDelAllToOneBundlingService
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwDelAllToOneBundlingService (UINT4 u4ContextId,
                                           UINT4 u4IfIndex, UINT4 u4Isid)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsMiPbbHwDelAllToOneBundlingService.u4ContextId = u4ContextId;
    NpSync.FsMiPbbHwDelAllToOneBundlingService.u4IfIndex = u4IfIndex;
    NpSync.FsMiPbbHwDelAllToOneBundlingService.u4Isid = u4Isid;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsMiPbbHwDelAllToOneBundlingServiceSync (NpSync.
                                                               FsMiPbbHwDelAllToOneBundlingService,
                                                               RM_PBB_APP_ID,
                                                               NPSYNC_FS_MI_PBB_HW_DEL_ALL_TO_ONE_BUNDLING_SERVICE);
            }
        }
#undef FsMiPbbHwDelAllToOneBundlingService
        i4RetVal =
            FsMiPbbHwDelAllToOneBundlingService (u4ContextId, u4IfIndex,
                                                 u4Isid);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsMiPbbHwDelAllToOneBundlingServiceSync (NpSync.
                                                                   FsMiPbbHwDelAllToOneBundlingService,
                                                                   RM_PBB_APP_ID,
                                                                   NPSYNC_FS_MI_PBB_HW_DEL_ALL_TO_ONE_BUNDLING_SERVICE);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           NPSYNC_FS_MI_PBB_HW_DEL_ALL_TO_ONE_BUNDLING_SERVICE,
                                           0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbNpInitHwSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsMiPbbNpInitHwSync (UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE;
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbNpDeInitHwSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsMiPbbNpDeInitHwSync (UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE;
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetAllToOneBundlingServiceSync
 *
 * -------------------------------------------------------------
 */
VOID 
     
     
     
     
     
     
     
    NpSyncFsMiPbbHwSetAllToOneBundlingServiceSync
    (tNpSyncFsMiPbbHwSetAllToOneBundlingService PbbHwSetAllToOneBundlingService,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              PbbHwSetAllToOneBundlingService.u4ContextId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              PbbHwSetAllToOneBundlingService.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              PbbHwSetAllToOneBundlingService.u4Isid);

        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwDelAllToOneBundlingServiceSync
 *
 * -------------------------------------------------------------
 */
VOID 
     
     
     
     
     
     
     
    NpSyncFsMiPbbHwDelAllToOneBundlingServiceSync
    (tNpSyncFsMiPbbHwDelAllToOneBundlingService PbbHwDelAllToOneBundlingService,
     UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              PbbHwDelAllToOneBundlingService.u4ContextId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              PbbHwDelAllToOneBundlingService.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet,
                              PbbHwDelAllToOneBundlingService.u4Isid);

        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* Manually Written Events Sync up */

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetPortUseDei
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetPortUseDei (UINT4 u4ContextId,
                              UINT4 u4IfIndex, UINT1 u1UseDei)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PortIdx.u4ContextId = u4ContextId;
    NpSync.PortIdx.u4IfIndex = u4IfIndex;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                       EVTSYNC_FS_MI_PBB_HW_SET_PORT_USE_DEI);
            }
        }
#undef FsMiVlanHwSetPortUseDei
        i4RetVal = VlanFsMiVlanHwSetPortUseDei (u4ContextId, u4IfIndex, u1UseDei);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PORT_USE_DEI);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PORT_USE_DEI);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetPortReqDropEncoding
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetPortReqDropEncoding (UINT4 u4ContextId,
                                       UINT4 u4IfIndex, UINT1 u1ReqDrpEncoding)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PortIdx.u4ContextId = u4ContextId;
    NpSync.PortIdx.u4IfIndex = u4IfIndex;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                       EVTSYNC_FS_MI_PBB_HW_SET_PORT_REQ_DROP_ENCODING);
            }
        }
#undef FsMiVlanHwSetPortReqDropEncoding
        i4RetVal =
            VlanFsMiVlanHwSetPortReqDropEncoding (u4ContextId, u4IfIndex,
                                              u1ReqDrpEncoding);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PORT_REQ_DROP_ENCODING);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PORT_REQ_DROP_ENCODING);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetPortPcpSelection
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetPortPcpSelection (UINT4 u4ContextId,
                                    UINT4 u4IfIndex, UINT2 u2PcpSelection)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PortIdx.u4ContextId = u4ContextId;
    NpSync.PortIdx.u4IfIndex = u4IfIndex;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                       EVTSYNC_FS_MI_PBB_HW_SET_PORT_PCP_SELECTION);
            }
        }
#undef FsMiVlanHwSetPortPcpSelection
        i4RetVal =
            VlanFsMiVlanHwSetPortPcpSelection (u4ContextId, u4IfIndex,
                                           u2PcpSelection);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PORT_PCP_SELECTION);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PORT_PCP_SELECTION);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetPcpEncodTbl
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetPcpEncodTbl (UINT4 u4ContextId,
                               UINT4 u4IfIndex,
                               tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PortIdx.u4ContextId = u4ContextId;
    NpSync.PortIdx.u4IfIndex = u4IfIndex;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                       EVTSYNC_FS_MI_PBB_HW_SET_PCP_ENCOD_TBL);
            }
        }
#undef FsMiVlanHwSetPcpEncodTbl
        i4RetVal =
            VlanFsMiVlanHwSetPcpEncodTbl (u4ContextId, u4IfIndex, NpPbVlanPcpInfo);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PCP_ENCOD_TBL);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PCP_ENCOD_TBL);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetPcpDecodTbl
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetPcpDecodTbl (UINT4 u4ContextId,
                               UINT4 u4IfIndex,
                               tHwVlanPbPcpInfo NpPbVlanPcpInfo)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PortIdx.u4ContextId = u4ContextId;
    NpSync.PortIdx.u4IfIndex = u4IfIndex;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                       EVTSYNC_FS_MI_PBB_HW_SET_PCP_DECOD_TBL);
            }
        }
#undef FsMiVlanHwSetPcpDecodTbl
        i4RetVal =
            VlanFsMiVlanHwSetPcpDecodTbl (u4ContextId, u4IfIndex, NpPbVlanPcpInfo);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PCP_DECOD_TBL);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PCP_DECOD_TBL);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbHwSetProviderBridgePortType
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbHwSetProviderBridgePortType (UINT4 u4ContextId,
                                          UINT4 u4IfIndex, UINT4 u4PortType)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PortIdx.u4ContextId = u4ContextId;
    NpSync.PortIdx.u4IfIndex = u4IfIndex;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                       EVTSYNC_FS_MI_PBB_HW_SET_PROVIDER_BRIDGE_PORT_TYPE);
            }
        }
#undef FsMiVlanHwSetProviderBridgePortType
        i4RetVal =
            VlanFsMiVlanHwSetProviderBridgePortType (u4ContextId, u4IfIndex,
                                                 u4PortType);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBB_NPSYNC_BLK () == OSIX_FALSE)
                {
                    PbbEvtSyncPortIdxSync (NpSync.PortIdx,
                                           RM_PBB_APP_ID,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PROVIDER_BRIDGE_PORT_TYPE);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0,
                                           EVTSYNC_FS_MI_PBB_HW_SET_PROVIDER_BRIDGE_PORT_TYPE);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: EvtSyncFsMiPbbDeletePort
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
EvtSyncFsMiPbbDeletePort (UINT4 u4ContextId, UINT4 u4IfIndex)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PortIdx.u4ContextId = u4ContextId;
    NpSync.PortIdx.u4IfIndex = u4IfIndex;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortIdxSync (NpSync.PortIdx, RM_PBB_APP_ID,
                                       EVTSYNC_FS_MI_PBB_DELETE_PORT);
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0, EVTSYNC_FS_MI_PBB_DELETE_PORT);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: EvtSyncFsMiPbbSetGlbOui
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
EvtSyncFsMiPbbSetGlbOui (UINT1 *pau1OUI)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    MEMCPY (NpSync.PbbGlbOui.au1OUI, pau1OUI, PBB_OUI_LENGTH);

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncGlbOuiSync (NpSync.PbbGlbOui, RM_PBB_APP_ID,
                                      EVTSYNC_FS_MI_PBB_SET_GLOBAL_OUI);
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0, EVTSYNC_FS_MI_PBB_SET_GLOBAL_OUI);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: EvtSyncFsMiPbbCopyPortProperty
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
EvtSyncFsMiPbbCopyPortProperty (UINT4 u4ContextId, UINT4 u4IfIndex,
                                UINT4 u4DstPort)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PbbPortProperty.u4ContextId = u4ContextId;
    NpSync.PbbPortProperty.u4IfIndex = u4IfIndex;
    NpSync.PbbPortProperty.u4DstPort = u4DstPort;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortPropertySync (NpSync.PbbPortProperty,
                                            RM_PBB_APP_ID,
                                            EVTSYNC_FS_MI_PBB_COPY_PORT_PROPERTIES);
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0,
                                           EVTSYNC_FS_MI_PBB_COPY_PORT_PROPERTIES);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: EvtSyncFsMiPbbRemPortProperty
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
EvtSyncFsMiPbbRemPortProperty (UINT4 u4ContextId, UINT4 u4IfIndex,
                               UINT4 u4DstPort)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.PbbPortProperty.u4ContextId = u4ContextId;
    NpSync.PbbPortProperty.u4IfIndex = u4IfIndex;
    NpSync.PbbPortProperty.u4DstPort = u4DstPort;

    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBB_NPSYNC_BLK () == OSIX_FALSE)
            {
                PbbEvtSyncPortPropertySync (NpSync.PbbPortProperty,
                                            RM_PBB_APP_ID,
                                            EVTSYNC_FS_MI_PBB_REMOVE_PORT_PROPERTIES);
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBB_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbHwAuditCreateOrFlushBuffer (&NpSync,
                                           0,
                                           EVTSYNC_FS_MI_PBB_REMOVE_PORT_PROPERTIES);
        }
    }

    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: PbbEvtSyncPortIdxSync
 *
 * -------------------------------------------------------------
 */
VOID
PbbEvtSyncPortIdxSync (tNpSyncPortIdx PortIdx, UINT4 u4AppId, UINT4 u4EventId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, EVENTSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4EventId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PortIdx.u4ContextId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PortIdx.u4IfIndex);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: PbbEvtSyncGlbOuiSync
 *
 * -------------------------------------------------------------
 */
VOID
PbbEvtSyncGlbOuiSync (tNpSyncPbbGlbOui PbbGlbOui, UINT4 u4AppId,
                      UINT4 u4EventId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        (sizeof (UINT4) * PBB_OUI_LENGTH);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, EVENTSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4EventId);
        NPSYNC_RM_PUT_N_BYTE (pMsg, PbbGlbOui.au1OUI, &u2OffSet,
                              PBB_OUI_LENGTH);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: PbbEvtSyncPortPropertySync
 *
 * -------------------------------------------------------------
 */
VOID
PbbEvtSyncPortPropertySync (tNpSyncPbbPortProperty PortProp, UINT4 u4AppId,
                            UINT4 u4EventId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, EVENTSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4EventId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PortProp.u4ContextId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PortProp.u4IfIndex);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PortProp.u4DstPort);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}
