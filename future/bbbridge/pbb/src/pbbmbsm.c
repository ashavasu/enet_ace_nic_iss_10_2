/*******************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbmbsm.c,v 1.10 2013/06/06 12:03:45 siva Exp $
 *
 * Description: This file contains PBB routines used by other modules.
 *
 *******************************************************************/

#ifdef MBSM_WANTED
#include  "pbbinc.h"
#include "pbbprot.h"
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbMbsmUpdateOnCardInsertion                    */
/*                                                                           */
/*    Description         : This function walks all the pbb tables and      */
/*                          programs the Hardware on Line card insertion     */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                              tMbsmSlotInfo * pSlotInfo)
{
    tLocalPortList      SlotLocalPorts;
    tLocalPortList      NullPortList;
    UINT4               u4ContextId;
    UINT4               u4PrevContextId;

    /* Validate that the PBB module is initialised */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        return MBSM_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        return MBSM_SUCCESS;
    }
    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (PbbMbsmHwInit (pSlotInfo) != PBB_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }

    PBB_MEMSET (NullPortList, 0, sizeof (tLocalPortList));
    if (PbbGetFirstActiveContext (&u4ContextId) == PBB_SUCCESS)
    {
        do
        {
            u4PrevContextId = u4ContextId;

            if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
            {
                continue;
            }

            /* Validate for the Bridge mode to be PBB */
            if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
            {
                continue;
            }
            PBB_MEMSET (SlotLocalPorts, 0, sizeof (tLocalPortList));
            /* SlotLocalPorts - tLocalPortList of the slot that is inserted
             * now and mapped to the current context
             */
            PbbGetPortListForCurrContext (pPortInfo, SlotLocalPorts);

            PbbGetAggPortListForCurrContext (SlotLocalPorts);

            if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
            {
                if ((PBB_CURR_CONTEXT_PTR ()->u2InternalCbpCount) > 0)
                {
                    if (PbbMbsmCreateIBCompBridgeEcfmFilter
                        (u4ContextId, pSlotInfo) == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_PBB, "Card Insertion: "
                                  "PbbMbsmSetPortPropertiesToHw Failed \n");

                        PbbReleaseContext ();
                        return MBSM_FAILURE;
                    }

                    if (PbbBrgMbsmHwCreateAstControlPktFilter (u4ContextId,
                                                               pSlotInfo)
                        == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_PBB, "Card Insertion: "
                                  "PbbMbsmSetPortPropertiesToHw Failed \n");

                        PbbReleaseContext ();
                        return MBSM_FAILURE;
                    }
                }
                if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
                {
                    if (PbbBrgMbsmHwCreateAstControlPktFilter (u4ContextId,
                                                               pSlotInfo)
                        == MBSM_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_PBB, "Card Insertion: "
                                  "PbbMbsmSetPortPropertiesToHw Failed \n");

                        PbbReleaseContext ();
                        return MBSM_FAILURE;
                    }
                }
                if (PbbMbsmCreateCompBridgeEcfmFilter (u4ContextId,
                                                       pSlotInfo) ==
                    MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_PBB, "Card Insertion: "
                              "PbbMbsmSetPortPropertiesToHw Failed \n");

                    PbbReleaseContext ();
                    return MBSM_FAILURE;
                }
            }
            if (PBB_MEMCMP (SlotLocalPorts,
                            NullPortList,
                            sizeof (tLocalPortList)) != PBB_INIT_VAL)
            {
                if (PbbMbsmSetPortPropertiesToHw
                    (pSlotInfo, SlotLocalPorts) == MBSM_FAILURE)
                {
                    MBSM_DBG (MBSM_PROTO_TRC, MBSM_PBB, "Card Insertion: "
                              "PbbMbsmSetPortPropertiesToHw Failed \n");

                    PbbReleaseContext ();
                    return MBSM_FAILURE;
                }
            }

            PbbReleaseContext ();
        }
        while (PbbGetNextActiveContext (u4PrevContextId,
                                        &u4ContextId) == PBB_SUCCESS);
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbMbsmSetPortPropertiesToHw                    */
/*                                                                           */
/*    Description         : This function walks the pbb port table and       */
/*                          programs the Hardware with all the pbb port      */
/*                          properties                                       */
/*                                                                           */
/*    Input(s)            : pMBSMPortInfo - Inserted port list               */
/*                          pSlotInfo - Information about inserted slot      */
/*                          LocalPorts - Local Portlist of current context   */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbMbsmSetPortPropertiesToHw (tMbsmSlotInfo * pSlotInfo,
                              tLocalPortList LocalPorts)
{
    tLocalPortList      PortList;
    tPbbPortRBtreeNode *pPortEntry = NULL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    UINT4               u4ContextId = 0;
    UINT2               u2Port = 0;
    UINT2               u2PortNum = 0;
    UINT2               u2LaAggIndex = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1PortFlag = 0;

    MEMCPY (PortList, LocalPorts, sizeof (tLocalPortList));

    /*
     *   Scan the pbb port table and program all the port
     *   properties for each port into the inserted card
     */
    for (u2ByteIndex = 0; u2ByteIndex < PBB_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = PortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & PBB_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                                   u2Port,
                                                   &i4IfIndex) == PBB_FAILURE)
                {
                    return MBSM_FAILURE;
                }
                if (PBB_SUCCESS == PbbL2IwfIsPortInPortChannel (i4IfIndex))
                {
                    PbbL2IwfGetPortChannelForPort (i4IfIndex, &u2LaAggIndex);
                    /* Get The Local Port Number for Portchannel */
                    PbbVcmGetContextInfoFromIfIndex ((UINT4) u2LaAggIndex,
                                                     &u4ContextId, &u2PortNum);
                }
                else
                {
                    u2PortNum = u2Port;
                }

                PBB_GET_PORT_ENTRY (u2PortNum, pPortEntry);

                if (pPortEntry == NULL)
                {
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                    continue;
                }
                /* Set port porperties to H/W */
                if ((pPortEntry->u1PortType != PBB_CNP_PORTBASED_PORT) &&
                    (pPortEntry->u1PortType != PBB_CNP_STAGGED_PORT) &&
                    (pPortEntry->u1PortType != PBB_CNP_CTAGGED_PORT))
                {
                    if (PbbMbsmHwSetPortInfo
                        (PBB_CURR_CONTEXT_ID (), pPortEntry->u4IfIndex,
                         pSlotInfo) == PBB_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_PBB,
                                  "Card Insertion: FsMiPbbMbsmHwSetPortProperties"
                                  " Failed \n");
                        return MBSM_FAILURE;
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    /*
     *   Scan the pbb port table and program all the port
     *   properties for each port into the inserted card
     */
    for (u2ByteIndex = 0; u2ByteIndex < PBB_PORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = PortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {

            if ((u1PortFlag & PBB_BIT8) != 0)
            {
                u2Port = (UINT2) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                                   u2Port,
                                                   &i4IfIndex) == PBB_FAILURE)
                {
                    return MBSM_FAILURE;
                }
                if (PBB_SUCCESS == PbbL2IwfIsPortInPortChannel (i4IfIndex))
                {
                    PbbL2IwfGetPortChannelForPort (i4IfIndex, &u2LaAggIndex);
                    /* Get The Local Port Number for Portchannel */
                    PbbVcmGetContextInfoFromIfIndex ((UINT4) u2LaAggIndex,
                                                     &u4ContextId, &u2PortNum);
                }
                else
                {
                    u2PortNum = u2Port;
                }

                PBB_GET_PORT_ENTRY (u2PortNum, pPortEntry);

                if (pPortEntry == NULL)
                {
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                    continue;
                }
                /* Set port porperties to H/W */
                if ((pPortEntry->u1PortType == PBB_CNP_PORTBASED_PORT) ||
                    (pPortEntry->u1PortType == PBB_CNP_STAGGED_PORT) ||
                    (pPortEntry->u1PortType == PBB_CNP_CTAGGED_PORT))
                {
                    if (PbbMbsmHwSetPortInfo
                        (PBB_CURR_CONTEXT_ID (), pPortEntry->u4IfIndex,
                         pSlotInfo) == PBB_FAILURE)
                    {
                        MBSM_DBG (MBSM_PROTO_TRC, MBSM_PBB,
                                  "Card Insertion: FsMiPbbMbsmHwSetPortProperties"
                                  " Failed \n");
                        return MBSM_FAILURE;
                    }
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbMbsmUpdateOnCardRemoval                       */
/*                                                                           */
/*    Description         : This function walks all the pbb tables and       */
/*                          programs the Hardware on Line card removal       */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbMbsmUpdateOnCardRemoval (tMbsmPortInfo * pPortInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pSlotInfo);
    return MBSM_SUCCESS;
}

/***************************************************************************/
/* Function Name    : PbbMbsmPostMessage ()                               */
/*                                                                         */
/* Description      : This function is an API for mbsm module to enqueue   */
/*                    packets when a new LC is inserted or removed         */
/*                                                                         */
/* Input(s)         : pProtoMsg - Queue message.                           */
/*                                                                         */
/*                    i4Event   - Event type.                              */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : PBB_SUCCESS on success,                             */
/*                    PBB_FAILURE otherwise.                              */
/***************************************************************************/
INT1
PbbMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{

    tPbbQMsg           *pPbbQMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    pPbbQMsg = (tPbbQMsg *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (pPbbQMsg == NULL)
    {
        PBB_TRC_ARG1 ((OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                      " Message Allocation failed for Pbb "
                      "Config Message Type %u\r\n", PBB_REMOVE_PORT_INFO_MSG);
        return MBSM_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, 0, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = (UINT2) i4Event;

    if (pProtoMsg != NULL)
    {
        if (!(pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg =
              MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
        {
            PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);
            return MBSM_FAILURE;
        }

        MEMCPY (pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
                sizeof (tMbsmProtoMsg));
    }

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG1 ((OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                      " Send To Q failed for  Pbb Config Message "
                      "Type %u \r\n", PBB_REMOVE_PORT_INFO_MSG);

        if (pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg != NULL)
        {
            MEM_FREE (pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg);
        }

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return MBSM_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG1 ((OS_RESOURCE_TRC | ALL_FAILURE_TRC),
                      " Send Event failed for  Pbb Config Message "
                      "Type %u \r\n", PBB_REMOVE_PORT_INFO_MSG);

        /* No Need to Free Message Sice it is already Queued */
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************
 * Function Name : PbbMbsmUpdtForLoadSharing 
 * Description   : Function to update the PBB for load sharing depending on
 *                 enable/disable flag
 * Input(s)      : u1Flag - Load sharing flag 
 * Output(s)     : None  
 * Returns       : MBSM_SUCCESS/MBSM_FAILURE 
 *****************************************************************************/
INT1
PbbMbsmUpdtForLoadSharing (UINT1 u1Flag)
{
    if (u1Flag == MBSM_LOAD_SHARING_ENABLE)
    {
        return MBSM_SUCCESS;
    }
    else if (u1Flag == MBSM_LOAD_SHARING_DISABLE)
    {
        return MBSM_SUCCESS;
    }
    else
    {
        return MBSM_FAILURE;
    }
    return (MBSM_SUCCESS);
}

#endif /* MBSM_WANTED */
