
/*******************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbred.c,v 1.11 2014/03/19 13:36:12 siva Exp $
 *
 * Description: This file contains PBB redundancy related routines 
 *
 *******************************************************************/

#include "pbbinc.h"
#include "pbbglob.h"

/*****************************************************************************/
/* Function Name      : PbbRedRegisterWithRM                                 */
/*                                                                           */
/* Description        : Registers PBB module with RM to send and receive     */
/*                      update messages.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then PBB_SUCCESS          */
/*                      Otherwise PBB_FAILURE                                */
/*****************************************************************************/
INT4
PbbRedRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_PBB_APP_ID;
    RmRegParams.pFnRcvPkt = PbbRedHandleUpdateEvents;

    if (PbbRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Registration with RM FAILED\n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);

        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedInitBufferPool                                    */
/*                                                                            */
/*  Description     : This function initilizes the mempool for the buffer     */
/*                    entries                                                 */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : PBB_SUCCESS / PBB_FAILURE                               */
/******************************************************************************/
INT4
PbbRedInitBufferPool (VOID)
{

    PBB_RED_BUF_MEMPOOL () = PBB_INIT_VAL;

    return PBB_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedDeInitBufferPool                                  */
/*                                                                            */
/*  Description     : This function Deinitilizes the mempool for the buffer   */
/*                    entries                                                 */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : PBB_SUCCESS / PBB_FAILURE                               */
/******************************************************************************/
INT4
PbbRedDeInitBufferPool (VOID)
{
#ifdef NPAPI_WANTED
    tPbbBufferEntry    *pBuf = NULL;
    tPbbBufferEntry    *pTempBuf = NULL;

    if (PBB_RED_BUF_MEMPOOL () != 0)
    {
        /* Delete the Buffer entries */
        TMO_DYN_SLL_Scan (&(PBB_RED_BUF_SLL ()), pBuf, pTempBuf,
                          tPbbBufferEntry *)
        {
            TMO_SLL_Delete (&(PBB_RED_BUF_SLL ()), (tTMO_SLL_NODE *) pBuf);
        }
    }
#endif
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedHandleUpdateEvents                             */
/*                                                                           */
/* Description        : This function will be invoked by the RM module to    */
/*                      pass events like GO_ACTIVE, GO_STANDBY etc. to PBB   */
/*                      module.                                              */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueued, valid if u1Event is    */
/*                                VALID_UPDATE_MESSAGE.                      */
/*                      u2DataLen - Size of the update message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - If the message is enq'ed successfully  */
/*                      PBB_FAILURE otherwise                                */
/*****************************************************************************/
INT4
PbbRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tPbbQMsg           *pPbbQMsg = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to PBB task. */
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Message from RM ignored -"
                      "Buffer missing\n", PBB_FILE_NAME, PBB_FUNCTION_NAME);

        return RM_FAILURE;
    }

    if (gPbbGlobData.u1IsPbbInitialised == PBB_FALSE)
    {
        /* PBB Task is not initialised  */
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PbbRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (pPbbQMsg == NULL)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PbbRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    MEMSET (pPbbQMsg, 0, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = PBB_RM_MSG;

    pPbbQMsg->RmData.u4Events = (UINT4) u1Event;
    pPbbQMsg->RmData.pRmMsg = pData;
    pPbbQMsg->RmData.u2DataLen = u2DataLen;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: RM Message enqueue FAILED\n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PbbRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, (UINT1 *) pPbbQMsg);

        return RM_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: RM Event send FAILED\n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);

        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedProcessUpdateMsg                               */
/*                                                                           */
/* Description        : This function handles the received update messages   */
/*                      from the RM module. This function invokes appropriate*/
/*                      functions to handle the update message.              */
/*                                                                           */
/* Input(s)           : pPbbQMsg - Pointer to the PBB Queue message.         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedProcessUpdateMsg (tPbbQMsg * pPbbQMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    INT4                i4RetVal = PBB_SUCCESS;
    tRmProtoAck         ProtoAck;
    UINT4               u4PbbPrevNodeState = PBB_NODE_IDLE;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_PBB_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pPbbQMsg->RmData.u4Events)
    {
        case GO_ACTIVE:

            if (PBB_NODE_STATUS () == PBB_NODE_ACTIVE)
            {
                break;
            }
            u4PbbPrevNodeState = PBB_NODE_STATUS ();

            if ((u4PbbPrevNodeState == PBB_NODE_ACTIVE) ||
                (u4PbbPrevNodeState == PBB_NODE_SWITCHOVER_IN_PROGRESS))
            {
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: Node already ACTIVE or"
                              "becoming ACTIVE \n",
                              PBB_FILE_NAME, PBB_FUNCTION_NAME);
                return;
            }

            if (u4PbbPrevNodeState == PBB_NODE_IDLE)
            {
                i4RetVal = PbbRedMakeNodeActiveFromIdle ();
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else if (u4PbbPrevNodeState == PBB_NODE_STANDBY)
            {
                i4RetVal = PbbRedMakeNodeActiveFromStandby ();
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }

            if (PBB_RED_BULK_REQ_RECD () == PBB_TRUE)
            {
                /* Send Bulk Update request now */
                PBB_RED_BULK_REQ_RECD () = PBB_FALSE;
                gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort = 0;
                gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext = 0;
                PbbRedHandleStandbyUpEvent ();
            }

            PbbRmHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:

            PBB_RED_BULK_REQ_RECD () = PBB_FALSE;

            if (PBB_NODE_STATUS () == PBB_NODE_STANDBY)
            {
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: Node already STANDBY",
                              PBB_FILE_NAME, PBB_FUNCTION_NAME);
                return;
            }

            if (PBB_NODE_STATUS () == PBB_NODE_IDLE)
            {
                /* Set the NP Programming Blk flag so that no NPAPI
                   are fired during the static bulk update */
                PBB_NPSYNC_BLK () = OSIX_TRUE;

                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: Ignoring this event,"
                              "node will become standby once the static"
                              "configurations are restored \n",
                              PBB_FILE_NAME, PBB_FUNCTION_NAME);
            }
            else if ((PBB_NODE_STATUS () == PBB_NODE_ACTIVE) ||
                     (PBB_NODE_STATUS () == PBB_NODE_SWITCHOVER_IN_PROGRESS))
            {
                i4RetVal = PbbRedMakeNodeStandbyFromActive ();
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                PbbRmHandleProtocolEvent (&ProtoEvt);
            }

            break;

        case RM_INIT_HW_AUDIT:
            if (PBB_NODE_STATUS () == PBB_NODE_ACTIVE)
            {
                /* Start Audit Task. */
                PbbRedCreateAuditTask ();
            }
            break;

        case RM_STANDBY_UP:
            pData = (tRmNodeInfo *) pPbbQMsg->RmData.pRmMsg;
            PBB_RED_NUM_STANDBY_NODES () = pData->u1NumStandby;
            PbbRmReleaseMemoryForMsg ((UINT1 *) pData);
            PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: STANDBY UP Event received. "
                          "Num Peers = %d \n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME,
                          PBB_RED_NUM_STANDBY_NODES ());

            if (PBB_RED_BULK_REQ_RECD () == PBB_TRUE)
            {
                PBB_RED_BULK_REQ_RECD () = PBB_FALSE;
                /* Bulk request msg is received before RM_STANDBY_UP
                 * event.So we are sending bulk updates now.
                 */
                gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort = 0;
                gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext = 0;
                PbbRedHandleStandbyUpEvent ();
            }
            break;

        case RM_STANDBY_DOWN:
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: STANDBY DOWN Event received \n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
            pData = (tRmNodeInfo *) pPbbQMsg->RmData.pRmMsg;
            PBB_RED_NUM_STANDBY_NODES () = pData->u1NumStandby;
            PbbRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pPbbQMsg->RmData.pRmMsg, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pPbbQMsg->RmData.pRmMsg,
                                 pPbbQMsg->RmData.u2DataLen);

            ProtoAck.u4AppId = RM_PBB_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            PbbRedHandleUpdates (pPbbQMsg->RmData.pRmMsg,
                                 pPbbQMsg->RmData.u2DataLen);

            RM_FREE (pPbbQMsg->RmData.pRmMsg);

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:

            if (PBB_NODE_STATUS () == PBB_NODE_IDLE)
            {
                if (PbbRmGetNodeState () == RM_STANDBY)
                {
                    i4RetVal = PbbRedMakeNodeStandbyFromIdle ();
                }

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                PbbRmHandleProtocolEvent (&ProtoEvt);
            }
            break;

        case L2_INITIATE_BULK_UPDATES:
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: Received L2_INITIATE_BULK_UPDATES\n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
            PbbRedSendBulkReqMsg ();
            break;

        default:
            break;
    }
    UNUSED_PARAM (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : PbbRedMakeNodeActiveFromIdle                         */
/*                                                                           */
/* Description        : This function makes the node active from the idle    */
/*                      state. This function is called when the node becomes */
/*                      active and there are no other nodes.                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/

INT4
PbbRedMakeNodeActiveFromIdle (VOID)
{
    PBB_NODE_STATUS () = PBB_NODE_ACTIVE;

    PBB_RED_NUM_STANDBY_NODES () = PbbRmGetStandbyNodeCount ();

    PBB_RED_AUDIT_FLAG () = PBB_RED_AUDIT_STOP;    /* No need for audit */

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedMakeNodeActiveFromStandby                      */
/*                                                                           */
/* Description        : This function makes the node active frrom the        */
/*                      standby state.                                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
PbbRedMakeNodeActiveFromStandby (VOID)
{

    PBB_NODE_STATUS () = PBB_NODE_ACTIVE;

    PBB_TRC_ARG2 (MGMT_TRC,
                  "%s :: %s() :: Node status Standby to Active \n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME);

    PBB_RED_NUM_STANDBY_NODES () = PbbRmGetStandbyNodeCount ();

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedHandleStandbyUpEvent                           */
/*                                                                           */
/* Description        : This function handles the standby UP event from the  */
/*                      RM module. This function sends the bulk updates      */
/*                      containing the list of PBB ports Oper Status         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedHandleStandbyUpEvent (VOID)
{

    if (PBB_NODE_STATUS () != PBB_NODE_ACTIVE)
    {
        PBB_RED_BULK_REQ_RECD () = PBB_TRUE;
        return;
    }

    PbbRedSendBulkUpdates ();
}

/*****************************************************************************/
/* Function Name      : PbbRedSendBulkUpdates                                */
/*                                                                           */
/* Description        : This function sends bulk updates to the standby node.*/
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedSendBulkUpdates (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4BulkUpdPortCnt;
    UINT4               u4PortNo;
    UINT4               u4ContextId;
    UINT2               u2PortUpdLen;
    UINT2               u2BufSize;
    UINT2               u2OffSet = 0;
    UINT2               u2NextPort;
    UINT1               u1PortStatus;

    ProtoEvt.u4AppId = RM_PBB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (PBB_IS_MODULE_INITIALISED () != PBB_TRUE)
    {
        /* PBB completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        PbbRmSetBulkUpdatesStatus (RM_PBB_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        PbbRedSendBulkUpdTailMsg ();

        return;
    }
    u4BulkUpdPortCnt = PBB_RED_NO_OF_PORTS_PER_SUB_UPDATE;

    for (u4ContextId = gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext;
         u4ContextId < PBB_MAX_CONTEXTS; u4ContextId++)
    {
        if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
        {
            gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext++;
            continue;
        }
        else
        {
            if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
            {
                gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext++;
                continue;
            }
        }

        u4PortNo = 0;

        if (gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort == 0)
        {
            /* Get the first port in the selected Context */
            PbbGetNextLocalPort ((UINT2) u4PortNo, &u2NextPort);
            gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort = (UINT4) u2NextPort;
        }

        u2PortUpdLen = PBB_RED_TYPE_FIELD_SIZE + PBB_RED_LEN_FIELD_SIZE +
            PBB_RED_CONTEXT_FIELD_SIZE + PBB_RED_PORT_NO_LEN +
            PBB_RED_PORT_OPER_STATUS_SIZE;

        u2BufSize = PBB_RED_MAX_MSG_SIZE;

        if (pMsg == NULL)
        {
            if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
            {
                PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: RM alloc failed. "
                              "Bulk updates not sent \n",
                              PBB_FILE_NAME, PBB_FUNCTION_NAME);
                ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                PbbRmHandleProtocolEvent (&ProtoEvt);
                PbbReleaseContext ();
                return;
            }
        }

        /*
         *    PORT Update message
         *    
         *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<- 1 bytes ->|
         *    -----------------------------------------------------
         *    | Msg. Type |   Length    | PortId     | Oper Status |
         *    |     (1)   |1 + 2 + 4 + 1|            |             |
         *    |----------------------------------------------------
         *
         */

        u2OffSet = 0;
        u4PortNo = gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort;

        while ((PBB_IS_PORT_VALID (u4PortNo) == PBB_TRUE) &&
               (u4BulkUpdPortCnt > 0))
        {
            PbbGetNextLocalPort ((UINT2) u4PortNo, &u2NextPort);
            gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort = (UINT4) u2NextPort;

            PbbGetOperStatus ((UINT2) u4PortNo, &u1PortStatus);
            if (u1PortStatus != PBB_PORT_STATUS_UP)
            {
                u4PortNo = (UINT4) u2NextPort;
                continue;
            }

            if ((u2BufSize - u2OffSet) < u2PortUpdLen)
            {
                /* no room for the current message */
                PbbRedSendMsgToRm (pMsg, u2OffSet);

                pMsg = RM_ALLOC_TX_BUF (u2BufSize);
                if (pMsg == NULL)
                {
                    PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                                  "%s :: %s() :: RM alloc failed. "
                                  "Bulk updates not sent \n",
                                  PBB_FILE_NAME, PBB_FUNCTION_NAME);
                    PbbReleaseContext ();
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    PbbRmHandleProtocolEvent (&ProtoEvt);
                    return;
                }
                u2OffSet = 0;
            }

            PBB_RM_PUT_1_BYTE (pMsg, &u2OffSet, PBB_PORT_OPER_STATUS_MESSAGE);
            PBB_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2PortUpdLen);
            PBB_RM_PUT_4_BYTE (pMsg, &u2OffSet, PBB_CURR_CONTEXT_ID ());
            PBB_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4PortNo);
            PBB_RM_PUT_1_BYTE (pMsg, &u2OffSet, u1PortStatus);

            u4PortNo = (UINT4) u2NextPort;
            u4BulkUpdPortCnt--;
        }
        PbbReleaseContext ();

        if (u4BulkUpdPortCnt == 0)
        {
            break;
        }
        else if (!(PBB_IS_PORT_VALID (u4PortNo)))
        {
            gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort = 0;
            gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext++;
        }
    }

    if (u2OffSet != 0)
    {
        /* Send the buffer out */
        if (PbbRedSendMsgToRm (pMsg, u2OffSet) == PBB_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            PbbRmHandleProtocolEvent (&ProtoEvt);
        }
    }
    else if (pMsg)
    {
        /* Empty buffer created without any update messages filled */
        RM_FREE (pMsg);
    }

    if (gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext < PBB_MAX_CONTEXTS)
    {
        /* Send an event to start the next sub bulk update. */
        if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_RED_BULK_UPD_EVENT)
            == OSIX_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            PbbRmHandleProtocolEvent (&ProtoEvt);
            PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: Sent Event Failed\n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);
        }
        return;
    }
    else
    {
        /* PBB completes it's sync up during standby boot-up and this
         * needs to be informed to RMGR
         */
        PbbRmSetBulkUpdatesStatus (RM_PBB_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        PbbRedSendBulkUpdTailMsg ();
    }
}

/*****************************************************************************/
/* Function Name      : PbbRedMakeNodeStandbyFromActive                     */
/*                                                                           */
/* Description        : This function is called when the node becomes a      */
/*                      Standby node either from ACTIVE or from              */
/*                      SWITCH_OVER_IN_PROGRESS state.                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
PbbRedMakeNodeStandbyFromActive (VOID)
{

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: Node status Active to Standby\n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME);

    /* Set NUM Peers to zero */
    PBB_RED_NUM_STANDBY_NODES () = 0;

    /* Set node status to STANDBY here so that NPAPI will not
     * get triggered */
    PBB_NODE_STATUS () = PBB_NODE_STANDBY;

    /* Stop auditing if it is running. */

    if (PBB_AUDIT_TASK_ID () != 0)
    {
        /* If audit task is running, send CLEAN_AND_STOP event, so that 
         * auditing will be stopped, if its running and remaining Hardware 
         * audit table (SLL entries) will be cleaned on DELETE_TASK event */

        PBB_RED_AUDIT_FLAG () = PBB_RED_AUDIT_CLEAN_AND_STOP;

        /* Audit task spawned on Standby->Active event, delete it on
         * GO_STANDBY event */
        if (PBB_SEND_EVENT
            (PBB_AUDIT_TASK_ID (),
             PBB_RED_AUDIT_TASK_DELETE_EVENT) == OSIX_FAILURE)
        {
            PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: Failed\n", __FILE__,
                          PBB_FUNCTION_NAME);
            /* No Need to Relase the buffer since Msg is already posted */
            return PBB_FAILURE;

        }
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedCreateAuditTask                                */
/*                                                                           */
/* Description        : This function will invoke the audit                  */
/*                      start function to start the audit process.           */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedCreateAuditTask (VOID)
{
    INT4                i4RetVal;

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: Sending event to AUDIT"
                  "task to start the audit. \n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME);

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        /* PBB is shutdown. Reset the information present in the Hardware */
        PbbNpShutdown ();
        return;
    }
    else
    {
        /* Start performing the audit */
        PBB_RED_AUDIT_FLAG () = PBB_RED_AUDIT_START;

        if (PBB_AUDIT_TASK_ID () == 0)
        {
            /* Doing audit for the first time */
            i4RetVal = PBB_SPAWN_TASK (PBB_AUDIT_TASK,
                                       PBB_AUDIT_TASK_PRIORITY,
                                       OSIX_DEFAULT_STACK_SIZE,
                                       (OsixTskEntry) PbbRedAuditMain,
                                       0, &(PBB_AUDIT_TASK_ID ()));

        }

        if (PBB_AUDIT_TASK_ID () != 0)
        {
            PBB_SEND_EVENT (PBB_AUDIT_TASK_ID (), PBB_RED_AUDIT_START_EVENT);
        }
    }

    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: PBB_RED_AUDIT_START_EVENT "
                  "sent to the PBB Audit Task \n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME);
    UNUSED_PARAM (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : PbbRedHandleUpdates                                  */
/*                                                                           */
/* Description        : This function handles the update messages from the   */
/*                      RM module. This function invokes appropriate fns.    */
/*                      to store the received update messsage.               */
/*                                                                           */
/* Input(s)           : pMsg - Message received from the RM Module           */
/*                      u2DataLen - Length of the message                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedHandleUpdates (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UINT4               u4Port;
    UINT4               u4ContextId = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2Length;
    UINT2               u2RemMsgLen;
    UINT2               u2MinLen;
    UINT1               u1MsgType;
    UINT1               u1PortOperStatus;
    tRmProtoEvt         ProtoEvt;
#ifdef NPAPI_WANTED
    UINT2               u2RedBufOffset;
#endif

    ProtoEvt.u4AppId = RM_PBB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MinLen = PBB_RED_TYPE_FIELD_SIZE + PBB_RED_LEN_FIELD_SIZE +
        PBB_RED_CONTEXT_FIELD_SIZE;

    PBB_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    if (u1MsgType == PBB_BULK_UPD_TAIL_MESSAGE)
    {
        PBB_TRC_ARG2 (MGMT_TRC,
                      "%s :: %s() :: Bulk update tail message received \n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);
        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
        PbbRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    if (u1MsgType == PBB_BULK_REQ_MESSAGE)
    {
        PBB_TRC_ARG2 (MGMT_TRC,
                      "%s :: %s() :: Bulk request message received from the"
                      "standby node. Treating as if STANDBY_UP"
                      " event is received \n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);

        if (PBB_RED_NUM_STANDBY_NODES () == 0)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            PBB_RED_BULK_REQ_RECD () = PBB_TRUE;
            return;
        }

        PBB_RED_BULK_REQ_RECD () = PBB_FALSE;
        /* On recieving PBB_BULK_REQ_MESSAGE, Bulk updation process
         * should be restarted.
         */
        gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort = 0;
        gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext = 0;
        PbbRedHandleStandbyUpEvent ();
        return;
    }

    if (u1MsgType == NPSYNC_MESSAGE)
    {
        u2OffSet = 0;
        PBB_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
        PBB_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (PBB_NODE_STATUS () != PBB_NODE_STANDBY)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: Ignoring NPSYC message "
                          "Received. Node not STANDBY \n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);

            /* Not a Standby node, hence don't process the received 
             * sync message. */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            RmApiHandleProtocolEvent (&ProtoEvt);
            return;
        }
#ifdef NPAPI_WANTED
        u2RedBufOffset = u2OffSet;
#endif
#ifdef NPAPI_WANTED
        /* Process the NPAPI Sync up message */
        PbbNpSyncProcessSyncMsg (pMsg, &u2RedBufOffset);
#endif
        return;
    }

    if (u1MsgType == EVENTSYNC_MESSAGE)
    {
        u2OffSet = 0;
        PBB_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
        PBB_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (PBB_NODE_STATUS () != PBB_NODE_STANDBY)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: Ignoring EVENTSYNC message "
                          "Received. Node not STANDBY \n",
                          PBB_FILE_NAME, PBB_FUNCTION_NAME);

            /* Not a Standby node, hence don't process the received 
             * sync message. */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;
            ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
            RmApiHandleProtocolEvent (&ProtoEvt);
            return;
        }

#ifdef NPAPI_WANTED
        u2RedBufOffset = u2OffSet;

        /* Process the NPAPI Sync up message */
        PbbEventSyncProcessSyncMsg (pMsg, &u2RedBufOffset);
#endif
        return;
    }

    u2OffSet = 0;

    if (PBB_NODE_STATUS () != PBB_NODE_STANDBY)
    {
        return;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        /* PBB is shutdown */
        return;
    }

    while ((u2OffSet + u2MinLen) < u2DataLen)
    {
        PBB_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
        PBB_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* doesnot have room for type and length field itself. 
             * Skip to next attribute */
            u2OffSet += u2Length;
            continue;
        }

        PBB_RM_GET_4_BYTE (pMsg, &u2OffSet, u4ContextId);

        u2RemMsgLen = u2Length - u2MinLen;

        if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
        {
            u2OffSet = u2OffSet + u2RemMsgLen;
            continue;
        }

        if (u1MsgType == PBB_PORT_OPER_STATUS_MESSAGE)
        {
            if (u2RemMsgLen != (PBB_RED_PORT_NO_LEN +
                                PBB_RED_PORT_OPER_STATUS_SIZE))
            {
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: Incorrect Length received"
                              "Skipping the Port Oper Status update \n",
                              PBB_FILE_NAME, PBB_FUNCTION_NAME);
                ProtoEvt.u4Error = RM_PROCESS_FAIL;
                PbbRmHandleProtocolEvent (&ProtoEvt);
                u2OffSet += u2RemMsgLen;
                break;
            }

            if ((u2OffSet + u2RemMsgLen) <= u2DataLen)
            {
                /* room exists completely for this update... */
                PBB_RM_GET_4_BYTE (pMsg, &u2OffSet, u4Port);
                PBB_RM_GET_1_BYTE (pMsg, &u2OffSet, u1PortOperStatus);

                if (PbbRedHandlePortOperStatusUpdate (u4Port,
                                                      u1PortOperStatus)
                    == PBB_FAILURE)
                {
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    PbbRmHandleProtocolEvent (&ProtoEvt);
                }
            }
            else
            {
                u2OffSet = u2DataLen;
            }
        }
        else
        {
            u2OffSet += u2Length;    /* Skip the attribute */
        }
        PbbReleaseContext ();
    }
}

/*****************************************************************************/
/* Function Name      : PbbRedHandlePortOperStatusUpdate                     */
/*                                                                           */
/* Description        : This function sets the Operational Status received   */
/*                      for a port in the update message from                */
/*                      the active node.                                     */
/*                                                                           */
/* Input(s)           : u4Port   - Port number.                              */
/*                    : u1PortStatus - Port Oper status.                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS/PBB_FAILURE                              */
/*                                                                           */
/*****************************************************************************/
INT4
PbbRedHandlePortOperStatusUpdate (UINT4 u4Port, UINT1 u1PortStatus)
{
    INT4                i4IfIndex = PBB_INIT_VAL;
    UINT4               u4ContextId;
    INT1                i1RetVal = PBB_FAILURE;

    u4ContextId = PBB_CURR_CONTEXT_ID ();
    i1RetVal = PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                              (UINT2) u4Port, &i4IfIndex);

    if (i1RetVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }

    if (PbbSetPortStatus (u4ContextId, i4IfIndex, u1PortStatus) != PBB_SUCCESS)
    {
        PBB_TRC_ARG4 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s :: PBB Set Port if index : %d status : %d"
                      " failed\n", __FILE__, PBB_FUNCTION_NAME,
                      i4IfIndex, u1PortStatus);
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedMakeNodeStandbyFromIdle                        */
/*                                                                           */
/* Description        : This function makes the node standby from the idle   */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
PbbRedMakeNodeStandbyFromIdle (VOID)
{

    PBB_TRC_ARG2 (MGMT_TRC,
                  "%s :: %s() :: Node status Idle to Standby\n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME);

    PBB_RED_AUDIT_FLAG () = PBB_RED_AUDIT_STOP;

    PBB_NODE_STATUS () = PBB_NODE_STANDBY;

    PBB_RED_NUM_STANDBY_NODES () = 0;

    /* Static Bulk update is completed so
       reset the flag NP Programming Block flag */
    PBB_NPSYNC_BLK () = OSIX_FALSE;

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedSendBulkReqMsg                                 */
/*                                                                           */
/* Description        : This function sends bulk request message to the      */
/*                      active node.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedSendBulkReqMsg (VOID)
{
    tRmMsg             *pMsg;
    UINT2               u2OffSet;

    if (PBB_NODE_STATUS () != PBB_NODE_STANDBY)
    {
        PBB_TRC_ARG2 (MGMT_TRC,
                      "%s :: %s() :: Node status not Standby, "
                      "not sending the Bulk Req \n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);
        return;
    }

    /*
     *    PBB Bulk Request message           
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     (7)   |   3        |
     *    |-------------------------
     *
     */

    if ((pMsg = RM_ALLOC_TX_BUF (PBB_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        PBB_TRC_ARG2 (MGMT_TRC | OS_RESOURCE_TRC,
                      "%s :: %s() :: RM alloc failed. "
                      "Bulk Req not sent \n", PBB_FILE_NAME, PBB_FUNCTION_NAME);
        return;
    }

    PBB_TRC_ARG2 (MGMT_TRC,
                  "%s :: %s() :: Sending the Bulk Req \n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME);

    u2OffSet = 0;

    PBB_RM_PUT_1_BYTE (pMsg, &u2OffSet, PBB_BULK_REQ_MESSAGE);

    PBB_RM_PUT_2_BYTE (pMsg, &u2OffSet, PBB_RED_BULK_REQ_MSG_SIZE);

    PbbRedSendMsgToRm (pMsg, u2OffSet);
}

/******************************************************************************/
/*  Function Name   : PbbRedHandleBulkUpdateEvent                             */
/*                                                                            */
/*  Description     : It Handles the bulk update event. This event is used    */
/*                    to start the next sub bulk update. So                   */
/*                    PbbRedSendBulkUpdates is triggered.                     */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedHandleBulkUpdateEvent (VOID)
{
    if (PBB_NODE_STATUS () == PBB_NODE_ACTIVE)
    {
        PbbRedSendBulkUpdates ();
    }
}

/******************************************************************************/
/*  Function Name   : PbbRedHandleVlanAudEndEvent                             */
/*                                                                            */
/*  Description     : It sets the Vlan Audit End Flag. This ensures that the  */
/*                    Vlan have finished its auditing and PBB can start its   */
/*                    Delete Port and Delete Context Auditing.                */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedHandleVlanAudEndEvent (UINT4 u4VlanAudStatus, UINT4 u4ContextId)
{

    if (PBB_NODE_STATUS () == PBB_NODE_ACTIVE)
    {
        if (u4VlanAudStatus == PBB_VLAN_AUDIT_COMPLETED)
        {
            PBB_RED_VLAN_AUDIT_FLAG () = PBB_TRUE;

            /* Re-Send the event to PBB Audit Task to start the Audit 
               if the Delete port Buffer entries 
               are still remaining */
            if (PBB_AUDIT_TASK_ID () != 0)
            {
                PBB_SEND_EVENT (PBB_AUDIT_TASK_ID (),
                                PBB_RED_AUDIT_START_EVENT);
            }
        }

        if (u4VlanAudStatus == PBB_VLAN_AUDIT_BRG_MODE_CHG)
        {
            /* Send the event to PBB Audit Task to 
             * start the Audit for the Bridge Mode Change Handling
             */
            if (PBB_AUDIT_TASK_ID () != 0)
            {
                PBB_RED_AUD_BRG_MODE_CHG_CONTEXT () = u4ContextId;
                PBB_SEND_EVENT (PBB_AUDIT_TASK_ID (),
                                PBB_RED_AUDIT_BRG_MODE_CHG_EVENT);
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : PbbRedSendBulkUpdTailMsg                             */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE.                           */
/*****************************************************************************/
INT4
PbbRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet;

    ProtoEvt.u4AppId = RM_PBB_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message. 

     *        <------------1 Byte----------><--2 Byte-->
     ***************************************************
     *        *                            *           *
     * RM Hdr * PBB_BULK_UPD_TAIL_MESSAGE *Msg Length *
     *        *                            *           *
     ***************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (PBB_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        PBB_TRC_ARG2 (MGMT_TRC | OS_RESOURCE_TRC,
                      "%s :: %s() :: RM alloc failed. "
                      "Bulk Upd Tail msg not sent \n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);

        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        PbbRmHandleProtocolEvent (&ProtoEvt);
        return PBB_FAILURE;
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: "
                  "Sending Bulk Update tail message \n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME);

    u2OffSet = 0;

    PBB_RM_PUT_1_BYTE (pMsg, &u2OffSet, PBB_BULK_UPD_TAIL_MESSAGE);
    PBB_RM_PUT_2_BYTE (pMsg, &u2OffSet, PBB_RED_BULK_UPD_TAIL_MSG_SIZE);

    if (PbbRedSendMsgToRm (pMsg, u2OffSet) == PBB_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        PbbRmHandleProtocolEvent (&ProtoEvt);
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedGetNodeStatus                                  */
/*                                                                           */
/* Description        : This function returns the node status after getting  */
/*                      it from the RM module.                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : UINT4                                                */
/*****************************************************************************/
UINT4
PbbRedGetNodeStatus (VOID)
{
    UINT4               u4PbbNodeStatus;
    UINT4               u4RmNodeState;

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: "
                  "Node status from RM is ", PBB_FILE_NAME, PBB_FUNCTION_NAME);

    if (PBB_RED_GET_STATIC_CONFIG_STATUS () == RM_STATIC_CONFIG_NOT_RESTORED)
    {
        u4PbbNodeStatus = PBB_NODE_IDLE;
        return u4PbbNodeStatus;
    }

    u4RmNodeState = PbbRmGetNodeState ();

    switch (u4RmNodeState)
    {
        case RM_INIT:
            PBB_TRC (MGMT_TRC, "IDLE \n");
            u4PbbNodeStatus = PBB_NODE_IDLE;
            break;

        case RM_ACTIVE:
            PBB_TRC (MGMT_TRC, "ACTIVE \n");
            u4PbbNodeStatus = PBB_NODE_ACTIVE;
            break;

        case RM_STANDBY:
            PBB_TRC (MGMT_TRC, "STANDBY \n");
            u4PbbNodeStatus = PBB_NODE_STANDBY;
            break;

        default:
            u4PbbNodeStatus = PBB_NODE_IDLE;    /* Shud not come here */
            PBB_TRC (MGMT_TRC, "IDLE \n");
            break;
    }

    return u4PbbNodeStatus;
}

/*****************************************************************************/
/* Function Name      : PbbRedInitGlobalInfo                                 */
/*                                                                           */
/* Description        : Initializes redundancy global variables.             */
/*                      This function will get invoked during BOOTUP.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE                          */
/*****************************************************************************/
INT4
PbbRedInitGlobalInfo (VOID)
{
    MEMSET (&gPbbGlobData.PbbRedInfo, 0, sizeof (tPbbRedGlobalInfo));
    MEMSET (&gPbbGlobData.PbbRedTaskInfo, 0, sizeof (tPbbRedTaskInfo));

    PBB_NODE_STATUS () = PBB_NODE_IDLE;
    gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort = 0;
    gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext = 0;
    PBB_RED_BULK_REQ_RECD () = PBB_FALSE;
    PBB_RED_VLAN_AUDIT_FLAG () = PBB_FALSE;
    PBB_RED_NUM_STANDBY_NODES () = 0;
    PBB_NPSYNC_BLK () = OSIX_FALSE;
    PBB_RED_AUD_BRG_MODE_CHG_CONTEXT () = PBB_MAX_CONTEXTS + 1;
    /* Create SLL for PbbRedBuffer Table */
    TMO_SLL_Init (&(PBB_RED_BUF_SLL ()));

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedDeInitGlobalInfo                               */
/*                                                                           */
/* Description        : DeInitializes redundancy global variables.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS                                          */
/*****************************************************************************/
INT4
PbbRedDeInitGlobalInfo (VOID)
{

#ifdef NPAPI_WANTED
    tPbbBufferEntry    *pBuf = NULL;
    tPbbBufferEntry    *pTempBuf = NULL;

    /* Delete the Buffer entries */
    TMO_DYN_SLL_Scan (&(PBB_RED_BUF_SLL ()), pBuf, pTempBuf, tPbbBufferEntry *)
    {
        TMO_SLL_Delete (&(PBB_RED_BUF_SLL ()), (tTMO_SLL_NODE *) pBuf);
    }
#endif

    MEMSET (&gPbbGlobData.PbbRedInfo, 0, sizeof (tPbbRedGlobalInfo));
    MEMSET (&gPbbGlobData.PbbRedTaskInfo, 0, sizeof (tPbbRedTaskInfo));

    PBB_NODE_STATUS () = PBB_NODE_IDLE;
    gPbbGlobData.PbbRedInfo.u4BulkUpdNextPort = 0;
    gPbbGlobData.PbbRedInfo.u4BulkUpdNextContext = 0;
    PBB_RED_BULK_REQ_RECD () = PBB_FALSE;
    PBB_RED_VLAN_AUDIT_FLAG () = PBB_FALSE;
    PBB_RED_NUM_STANDBY_NODES () = 0;
    PBB_NPSYNC_BLK () = OSIX_FALSE;
    PBB_RED_AUD_BRG_MODE_CHG_CONTEXT () = PBB_MAX_CONTEXTS + 1;
    /* Create SLL for PbbRedBuffer Table */
    TMO_SLL_Init (&(PBB_RED_BUF_SLL ()));

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedInitRedundancyInfo                             */
/*                                                                           */
/* Description        : This function initialises the required information   */
/*                      to support redundancy based on the current node      */
/*                      status. This function will be invoked when the PBB   */
/*                      module is started afresh.                            */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE                            */
/*****************************************************************************/
INT4
PbbRedInitRedundancyInfo (VOID)
{
    switch (PBB_NODE_STATUS ())
    {
        case PBB_NODE_ACTIVE:

            PBB_RED_NUM_STANDBY_NODES () = PbbRmGetStandbyNodeCount ();

            break;

        case PBB_NODE_STANDBY:

            /*Do Nothing */

            break;

        default:
            break;
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedDeInitRedundancyInfo                           */
/*                                                                           */
/* Description        : This function deinitialises all the information      */
/*                      allocated to support redundancy based on the node    */
/*                      status. This function will be invoked when the PBB   */
/*                      module is shutdown.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedDeInitRedundancyInfo (VOID)
{
    switch (PBB_NODE_STATUS ())
    {
        case PBB_NODE_ACTIVE:

            PBB_RED_NUM_STANDBY_NODES () = 0;

            break;

        case PBB_NODE_STANDBY:
            break;

        default:
            break;
    }
}

/*****************************************************************************/
/* Function Name      : PbbRedDeleteAuditTask                                */
/*                                                                           */
/* Description        : This function will delete the Audit Task if running  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedDeleteAuditTask (VOID)
{
    tPbbBufferEntry    *pBuf = NULL;
    tPbbBufferEntry    *pTempBuf = NULL;
    tOsixTaskId         TempTaskId;

    if (PBB_RED_AUDIT_FLAG () == PBB_RED_AUDIT_CLEAN_AND_STOP)
    {
        /* This scenario would come in the following scenario,
         *   -- Audit task spawned, in STANDBY->ACTIVE scenario
         *   -- Audit is going on
         *   -- Before Audit completes, ACTIVE->STANDBY event comes,
         *
         *   Then, delete the remaining audit entries in SLL */

        TMO_DYN_SLL_Scan (&(PBB_RED_BUF_SLL ()), pBuf, pTempBuf,
                          tPbbBufferEntry *)
        {
            TMO_SLL_Delete (&(PBB_RED_BUF_SLL ()), &(pBuf->Node));
            MemReleaseMemBlock (PBB_RED_BUF_MEMPOOL (), (UINT1 *) pBuf);
        }
    }

    PBB_RED_AUDIT_FLAG () = PBB_RED_AUDIT_STOP;

    /* Store PBB AUDIT Task ID in temporary variable, before deleting the
     * task */

    TempTaskId = PBB_AUDIT_TASK_ID ();

    /* Make the Audit task id to zero */
    PBB_AUDIT_TASK_ID () = 0;

    /* Delete the task in suicidal manner */
    PBB_DELETE_TASK (TempTaskId);

    /* Task would have terminated in the previous step. No step 
     * should be done after this */
}

/******************************************************************************/
/*  Function Name   : PbbRedSendMsgToRm                                       */
/*                                                                            */
/*  Description     : This function enqueues the update messages to the RM    */
/*                    module.                                                 */
/*                                                                            */
/*  Input(s)        : pMsg  - Message to be sent to RM module                 */
/*                    u2Len - Length of the Message                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : PBB_SUCESS/PBB_FAILURE                                  */
/******************************************************************************/
INT4
PbbRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Len)
{
    UINT4               u4RetVal;

    u4RetVal = PbbRmEnqMsgToRmFromAppl (pMsg, u2Len, RM_PBB_APP_ID,
                                        RM_PBB_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedAuditMain                                         */
/*                                                                            */
/*  Description     : This the main routine for the PBB Audit submodule.      */
/*                                                                            */
/*  Input(s)        : pi1Param                                                */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedAuditMain (INT1 *pi1Param)
{
    UINT4               u4Events;

    UNUSED_PARAM (pi1Param);
    while (1)
    {
        if ((PBB_RECEIVE_EVENT (PBB_AUDIT_TASK_ID (),
                                (PBB_RED_AUDIT_START_EVENT |
                                 PBB_RED_AUDIT_TASK_DELETE_EVENT |
                                 PBB_RED_AUDIT_BRG_MODE_CHG_EVENT),
                                OSIX_WAIT, &u4Events)) == OSIX_SUCCESS)
        {
            if (u4Events & PBB_RED_AUDIT_TASK_DELETE_EVENT)
            {
#ifdef NPAPI_WANTED
                /* Delete audit task, in suicide manner */
                PbbRedDeleteAuditTask ();
#endif
                /* Thread would have terminated here */
            }

            if (u4Events & PBB_RED_AUDIT_START_EVENT)
            {
#ifdef NPAPI_WANTED
                PbbRedStartAudit ();
#endif
            }

            if (u4Events & PBB_RED_AUDIT_BRG_MODE_CHG_EVENT)
            {
#ifdef NPAPI_WANTED
                PbbRedAuditDeleteContext (PBB_RED_AUD_BRG_MODE_CHG_CONTEXT ());
#endif
            }

        }
    }
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedStartAudit                                        */
/*                                                                            */
/*  Description     : This function performs audit of the PBB parameters      */
/*                    between the hardware and the software. This function    */
/*                    will be invoked whenever the AUDIT_START_EVENT is       */
/*                    received from the PBB module.                           */
/*                    This function synchronizes the PBB parameters between   */
/*                    the hardware and the software.                          */
/*                                                                            */
/*  Input(s)        : None.                                                   */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedStartAudit (VOID)
{
    tPbbBufferEntry    *pBuf = NULL;
    tPbbBufferEntry    *pTempBuf = NULL;
    UINT4               u4EventId = PBB_INIT_VAL;

    /* Scan the Buffer entries and delete it */
    TMO_DYN_SLL_Scan (&(PBB_RED_BUF_SLL ()), pBuf, pTempBuf, tPbbBufferEntry *)
    {
        PBB_LOCK ();

        if (PBB_RED_AUDIT_FLAG () != PBB_RED_AUDIT_START)
        {
            /**************************************************************** *
             * Only if the AUDIT_START flag is set, do auditing.              *
             *                                                                *
             * This flag will be reset in the following scenarios,            *
             *   * If GO_STANDBY event comes when Audit is going on, then     *
             *     audit flag will be set to AUDIT_CLEAN_AND_STOP. In this    *
             *     case, audit has to be stopped. Remaining SLL entries will  *
             *     be deleted, when the Audit task is deleted.                *
             *   * When PBB Module shut is done, this flag will be set to     *
             *     AUDIT_STOP. All the SLL entries would have been deleted    *
             *     in the ShutDown function. Stop Auditing now                *
             ******************************************************************/
            PBB_UNLOCK ();
            return;
        }

        u4EventId = pBuf->u4EventId;

        /* Process Delete Port and Delete Context only when Vlan Audit
           Flag is set */
        if (!((u4EventId == EVTSYNC_FS_MI_PBB_DELETE_PORT) &&
              (PBB_RED_VLAN_AUDIT_FLAG () != PBB_TRUE)))
        {
            PbbRedAuditBufferEntry (pBuf);
            TMO_SLL_Delete (&(PBB_RED_BUF_SLL ()), (tTMO_SLL_NODE *) pBuf);
            MemReleaseMemBlock (PBB_RED_BUF_MEMPOOL (), (UINT1 *) pBuf);
        }

        PBB_UNLOCK ();
        pBuf = NULL;
    }
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbHwAuditCreateOrFlushBuffer                           */
/*                                                                            */
/*  Description     : This function performs cehcks whether a buffer entry    */
/*                    is already present or not. If not present then create it*/
/*                                                                            */
/*  Input(s)        : pBuf - Buffer Entry                                     */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/

VOID
PbbHwAuditCreateOrFlushBuffer (unNpSync * pNpSync, UINT4 u4NpApiId,
                               UINT4 u4EventId)
{
    tPbbBufferEntry    *pBuf = NULL;
    tPbbBufferEntry    *pTempBuf = NULL;
    tPbbBufferEntry    *pPrevNode = NULL;
    UINT1               u1Match = PBB_FALSE;

    if ((u4NpApiId == 0) && (u4EventId == 0))
    {
        return;
    }

    /* Either NPAPIID or the Event ID will only
       come in this function */
    if ((u4NpApiId > 0) && (u4EventId > 0))
    {
        return;
    }

    /* Scan the Buffer entries and delete it */
    TMO_DYN_SLL_Scan (&(PBB_RED_BUF_SLL ()), pBuf, pTempBuf, tPbbBufferEntry *)
    {
        if (((u4NpApiId != 0) && (u4NpApiId == pBuf->u4NpApiId)) ||
            ((u4EventId != 0) && (u4EventId == pBuf->u4EventId)))

        {
            if (MEMCMP (&(pBuf->unNpData), pNpSync, sizeof (unNpSync)) == 0)
            {
                TMO_SLL_Delete (&(PBB_RED_BUF_SLL ()), (tTMO_SLL_NODE *) pBuf);
                MemReleaseMemBlock (PBB_RED_BUF_MEMPOOL (), (UINT1 *) pBuf);
                u1Match = PBB_TRUE;
                break;
            }
        }

        pPrevNode = pBuf;
    }

    if (u1Match == PBB_FALSE)
    {
        pTempBuf = NULL;
        /* allocate for the tunnel table entry */
        pTempBuf = (tPbbBufferEntry *) MemAllocMemBlk (PBB_RED_BUF_MEMPOOL ());

        if (pTempBuf == NULL)
        {
            return;
        }

        MEMSET (pTempBuf, PBB_INIT_VAL, sizeof (tPbbBufferEntry));

        TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pTempBuf);

        MEMCPY (&(pTempBuf->unNpData), pNpSync, sizeof (unNpSync));
        pTempBuf->u4NpApiId = u4NpApiId;
        pTempBuf->u4EventId = u4EventId;

        if (pPrevNode == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&(PBB_RED_BUF_SLL ()), NULL,
                            (tTMO_SLL_NODE *) pTempBuf);
        }
        else
        {
            /* Node to be inserted in last position */

            TMO_SLL_Insert (&(PBB_RED_BUF_SLL ()),
                            (tTMO_SLL_NODE *) pPrevNode,
                            (tTMO_SLL_NODE *) pTempBuf);
        }
    }
}
#endif

/*****************************************************************************/
/* Function Name      : PbbRedSyncPortOperStatus                             */
/*                                                                           */
/* Description        : This function will send port oper status change to   */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : u4Context - Virtual ContextId                        */
/*                      i4IfIndex - Inerface Index                           */
/*                    : u1Status - Port Oper status.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE.                           */
/*****************************************************************************/
INT4
PbbRedSyncPortOperStatus (UINT4 u4Context, INT4 i4IfIndex, UINT1 u1Status)
{
    tRmMsg             *pMsg;
    UINT4               u4Port = 0;
    UINT2               u2OffSet;
    UINT2               u2MsgLen;
    UINT2               u2LocalPort;

    if (PBB_NODE_STATUS () != PBB_NODE_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        return PBB_SUCCESS;
    }

    if (PBB_RED_NUM_STANDBY_NODES () == 0)
    {
        /* Standby node is not present, so no need to send the
         * sync up message. */
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: "
                      "Red sync up msgs can not be sent if the "
                      "Standby Node is down \n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);
        return PBB_SUCCESS;
    }

    if (PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    u4Port = (UINT4) u2LocalPort;

    /* Form a port oper status sync message

     *    <- 1 byte ->|<- 2 bytes->|<-4 bytes ->|<- 1 bytes ->|
     *    -----------------------------------------------------
     *    | Msg. Type |   Length    | PortId     | Oper Status |
     *    |     (1)   |1 + 2 + 4 + 1|            |             |
     *    |----------------------------------------------------

     * The RM Hdr shall be included by RM.
     */

    u2MsgLen = PBB_RED_TYPE_FIELD_SIZE + PBB_RED_LEN_FIELD_SIZE +
        PBB_RED_CONTEXT_FIELD_SIZE + PBB_RED_PORT_NO_LEN +
        PBB_RED_PORT_OPER_STATUS_SIZE;

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        PBB_TRC_ARG3 (MGMT_TRC | OS_RESOURCE_TRC,
                      "%s :: %s() :: RM alloc failed. "
                      " Oper status msg for port %d not sent \n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME, u4Port);

        return PBB_FAILURE;
    }

    PBB_TRC_ARG3 (MGMT_TRC, "%s :: %s() :: Sending port oper status "
                  "msg for port %d \n",
                  PBB_FILE_NAME, PBB_FUNCTION_NAME, u4Port);

    u2OffSet = 0;

    PBB_RM_PUT_1_BYTE (pMsg, &u2OffSet, PBB_PORT_OPER_STATUS_MESSAGE);
    PBB_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgLen);
    PBB_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4Context);
    PBB_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4Port);
    PBB_RM_PUT_1_BYTE (pMsg, &u2OffSet, u1Status);

    PbbRedSendMsgToRm (pMsg, u2OffSet);

    return PBB_SUCCESS;
}
