/*******************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbnpwr.c,v 1.22 2014/03/19 13:36:12 siva Exp $
 *
 * Description: This file contains Npapi wrappers used in PBB moodule.
 *
 *******************************************************************/

#include "pbbinc.h"

/*****************************************************************************/
/* Function Name      : PbbCreateCompBridgeEcfmFilter                           */
/*                                                                           */
/* Description        : This function is called to install filters for       */
/*                      copying the control packets to CPU that are from the */
/*                      customer network across the PBBN.                    */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On Success                            */
/*                      PBB_FAILURE - On failure                            */
/*****************************************************************************/
INT1
PbbCreateCompBridgeEcfmFilter (VOID)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry EcfmFilterEntry;
    tPbbCtrlPktFilterEntry *pEcfmFilterEntry = &EcfmFilterEntry;
    UINT4               u4ContextId = PBB_CURR_CONTEXT_PTR ()->u4ContextId;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {

        PBB_MEMSET (&EcfmFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));

        pEcfmFilterEntry->i4IsidOffset = -1;
        pEcfmFilterEntry->i4MacOffset = -1;

        pEcfmFilterEntry->u2EtherType = (UINT2) PBB_ECFM_LLC_CFM_TYPE;
        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_1;
        pEcfmFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgHwCreateControlPktFilter (u4ContextId,
                                             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_2;
        if (FsMiBrgHwCreateControlPktFilter (u4ContextId,
                                             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_3;
        if (FsMiBrgHwCreateControlPktFilter (u4ContextId,
                                             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_4;
        if (FsMiBrgHwCreateControlPktFilter (u4ContextId,
                                             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#endif
    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Creating the PBB ECFM filter successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbDeleteCompBridgeEcfmFilter                           */
/*                                                                           */
/* Description        : This function is called to delete specified filters. */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PbbDeleteCompBridgeEcfmFilter (VOID)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry EcfmFilterEntry;
    tPbbCtrlPktFilterEntry *pEcfmFilterEntry = &EcfmFilterEntry;
    UINT4               u4ContextId = PBB_CURR_CONTEXT_PTR ()->u4ContextId;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        PBB_MEMSET (pEcfmFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));
        pEcfmFilterEntry->i4IsidOffset = -1;
        pEcfmFilterEntry->i4MacOffset = -1;

        pEcfmFilterEntry->u2EtherType = (UINT2) PBB_ECFM_LLC_CFM_TYPE;
        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_1;
        pEcfmFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgHwDeleteControlPktFilter (u4ContextId,
                                             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwDeleteControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_2;
        if (FsMiBrgHwDeleteControlPktFilter (u4ContextId,
                                             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwDeleteControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_3;
        if (FsMiBrgHwDeleteControlPktFilter (u4ContextId,
                                             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwDeleteControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_4;
        if (FsMiBrgHwDeleteControlPktFilter (u4ContextId,
                                             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwDeleteControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return;
        }

    }
#endif
    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Deleting the PBB ECFM filter successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return;
}

/*****************************************************************************/
/* Function Name      : PbbCreateIBCompBridgeEcfmFilter                         */
/*                                                                           */
/* Description        : This function is called to install filters for       */
/*                      copying the control packets to CPU that are from the */
/*                      customer network across the PBBN.                    */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On Success                            */
/*                      PBB_FAILURE - On failure                            */
/*****************************************************************************/
INT1
PbbCreateIBCompBridgeEcfmFilter (VOID)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry EcfmFilterEntry;
    tPbbCtrlPktFilterEntry *pEcfmFilterEntry = &EcfmFilterEntry;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {

        PBB_MEMSET (pEcfmFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));

        pEcfmFilterEntry->i4IsidOffset = -1;
        pEcfmFilterEntry->i4MacOffset = -1;

        pEcfmFilterEntry->u2EtherType = (UINT2) PBB_ECFM_LLC_CFM_TYPE;
        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_5;
        pEcfmFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgHwCreateControlPktFilter
            (PBB_CURR_CONTEXT_PTR ()->u4ContextId,
             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#endif
    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Creating the PBB ECFM IB component filter successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbDeleteCompBridgeEcfmFilter                           */
/*                                                                           */
/* Description        : This function is called to delete specified filters. */
/*                                                                           */
/* Input(s)           : u4IfIndex - Interface Index                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PbbDeleteIBCompBridgeEcfmFilter (VOID)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry EcfmFilterEntry;
    tPbbCtrlPktFilterEntry *pEcfmFilterEntry = &EcfmFilterEntry;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {

        PBB_MEMSET (pEcfmFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));
        pEcfmFilterEntry->i4IsidOffset = -1;
        pEcfmFilterEntry->i4MacOffset = -1;

        pEcfmFilterEntry->u2EtherType = (UINT2) PBB_ECFM_LLC_CFM_TYPE;
        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_5;
        pEcfmFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgHwDeleteControlPktFilter
            (PBB_CURR_CONTEXT_PTR ()->u4ContextId,
             *pEcfmFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwDeleteControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return;
        }
    }
#endif
    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Deleting the PBB ECFM IB component filter"
                  " successfully\n", __FILE__, PBB_FUNCTION_NAME);
    return;
}

/*****************************************************************************/
/*    Function Name             : PbbBrgHwCreateAstControlPktFilter           */
/*                                                                           */
/*    Description               : This API is used to program the hardware   */
/*                                to create a filter that will take the      */
/*                                control PDUs coming at PNPs of an IB Bridge*/
/*                                directly to I-Component                    */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS or PBB_FAILURE.                */
/*                                                                           */
/*****************************************************************************/
INT1
PbbBrgHwCreateAstControlPktFilter (VOID)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry AstFilterEntry;
    tPbbCtrlPktFilterEntry *pAstFilterEntry = &AstFilterEntry;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        PBB_MEMSET (pAstFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));

        pAstFilterEntry->i4IsidOffset = -1;
        pAstFilterEntry->i4EtherTypeOffset = -1;
        pAstFilterEntry->i4MacOffset = -1;

        if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            pAstFilterEntry->i4MacOffset = PBB_AST_ICOMP_OFFSET;
        }
        else if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                 PBB_BCOMPONENT_BRIDGE_MODE)
        {
            pAstFilterEntry->i4MacOffset = PBB_AST_BCOMP_OFFSET;
        }
        pAstFilterEntry->DstMacAddress[0] = 0x01;
        pAstFilterEntry->DstMacAddress[1] = 0x80;
        pAstFilterEntry->DstMacAddress[2] = 0xC2;
        pAstFilterEntry->DstMacAddress[3] = 0x00;
        pAstFilterEntry->DstMacAddress[4] = 0x00;
        pAstFilterEntry->DstMacAddress[5] = 0x08;
        pAstFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgHwCreateControlPktFilter
            (PBB_CURR_CONTEXT_PTR ()->u4ContextId,
             *pAstFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

    }
#endif
    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Creating the PBB STP filter successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name             : PbbBrgHwDestroyAstControlPktFilter         */
/*                                                                           */
/*    Description               : This API is used to program the hardware   */
/*                                to destroy filter                          */
/*                                                                           */
/*    Input(s)                  : None.                                      */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : PBB_SUCCESS or PBB_FAILURE.                */
/*                                                                           */
/*****************************************************************************/
INT1
PbbBrgHwDestroyAstControlPktFilter (VOID)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry AstFilterEntry;
    tPbbCtrlPktFilterEntry *pAstFilterEntry = &AstFilterEntry;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        PBB_MEMSET (pAstFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));

        pAstFilterEntry->i4IsidOffset = -1;
        pAstFilterEntry->i4EtherTypeOffset = -1;
        pAstFilterEntry->i4MacOffset = -1;

        if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            pAstFilterEntry->i4MacOffset = PBB_AST_ICOMP_OFFSET;
        }
        else if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                 PBB_BCOMPONENT_BRIDGE_MODE)
        {
            pAstFilterEntry->i4MacOffset = PBB_AST_BCOMP_OFFSET;
        }
        pAstFilterEntry->DstMacAddress[0] = 0x01;
        pAstFilterEntry->DstMacAddress[1] = 0x80;
        pAstFilterEntry->DstMacAddress[2] = 0xC2;
        pAstFilterEntry->DstMacAddress[3] = 0x00;
        pAstFilterEntry->DstMacAddress[4] = 0x00;
        pAstFilterEntry->DstMacAddress[5] = 0x08;
        pAstFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgHwDeleteControlPktFilter
            (PBB_CURR_CONTEXT_PTR ()->u4ContextId,
             *pAstFilterEntry) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwDeleteControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#endif
    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Deleting the PBB STP filter successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbHwSetPipPcpAttributesOnDest                      */
/*                                                                           */
/* Description        : This function sets the Pcp attricutes for a Pip 
 *                      at the destination if index
 */
/* Input(s)           : Context id
 *                      Src local port
 *                      Destination if index
 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/

INT1
PbbHwSetPipPcpAttributesOnDest (UINT4 u4ContextId,
                                UINT2 u2SrcPort, INT4 i4DestIfIndex)
{
    UINT1               u1PcpSelRow = PBB_INIT_VAL;
    UINT1               u1PcpUseDei = PBB_INIT_VAL;
    UINT1               u1PcpReqDropEnc = PBB_INIT_VAL;
    UINT1               u1PcpVal = PBB_INIT_VAL;
    UINT1               u1Priority = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;
    tHwPbbPcpInfo       PcpTblEntry;
    INT1                i1retVal = PbbGetPcpSelRow (u2SrcPort, &u1PcpSelRow);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&PcpTblEntry, PBB_INIT_VAL, sizeof (tHwPbbPcpInfo));
    i1retVal = PbbHwSetPortPcpSelection (u4ContextId,
                                         (UINT4) i4DestIfIndex, u1PcpSelRow);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbGetPcpUseDei (u2SrcPort, &u1PcpUseDei);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbHwSetPortUseDei (u4ContextId,
                                   (UINT4) i4DestIfIndex, u1PcpUseDei);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbGetPcpReqDropEncoding (u2SrcPort, &u1PcpReqDropEnc);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbHwSetPortReqDropEncoding (u4ContextId,
                                            (UINT4) i4DestIfIndex,
                                            u1PcpReqDropEnc);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    for (u1PcpSelRow = PBB_8P0D_SEL_ROW; u1PcpSelRow <= PBB_5P3D_SEL_ROW;
         u1PcpSelRow++)
    {
        for (u1PcpVal = PBB_INIT_VAL; u1PcpVal < PBB_MAX_NUM_PCP; u1PcpVal++)
        {
            PcpTblEntry.u2PcpSelRow = (UINT2) u1PcpSelRow;
            PcpTblEntry.u2PcpValue = (UINT2) u1PcpVal;
            i1retVal = PbbGetPcpDecodingDE (u2SrcPort,
                                            u1PcpSelRow,
                                            u1PcpVal, &u1DropEligible);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            PcpTblEntry.u1DropEligible = u1DropEligible;
            i1retVal = PbbGetPcpDecodingPriority (u2SrcPort,
                                                  u1PcpSelRow,
                                                  u1PcpVal, &u1Priority);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            PcpTblEntry.u2Priority = (UINT2) u1Priority;
            i1retVal = PbbHwSetPcpDecodTbl (u4ContextId,
                                            (UINT4) i4DestIfIndex, PcpTblEntry);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
        }
    }
    for (u1PcpSelRow = PBB_8P0D_SEL_ROW; u1PcpSelRow < PBB_5P3D_SEL_ROW;
         u1PcpSelRow++)
    {
        for (u1Priority = PBB_INIT_VAL; u1Priority < PBB_MAX_NUM_PRIORITY;
             u1Priority++)
        {
            for (u1DropEligible = PBB_INIT_VAL;
                 u1DropEligible < PBB_MAX_NUM_DROP_ELIGIBLE; u1DropEligible++)
            {
                i1retVal = PbbGetPcpEncodingPcpVal (u2SrcPort,
                                                    u1PcpSelRow,
                                                    u1Priority,
                                                    u1DropEligible, &u1PcpVal);
                if (i1retVal == PBB_FAILURE)
                {
                    return PBB_FAILURE;
                }
                PcpTblEntry.u2PcpSelRow = (UINT2) u1PcpSelRow;
                PcpTblEntry.u2PcpValue = (UINT2) u1PcpVal;
                PcpTblEntry.u2Priority = (UINT2) u1Priority;
                PcpTblEntry.u1DropEligible = (UINT1) u1DropEligible;
                i1retVal = PbbHwSetPcpEncodTbl (u4ContextId,
                                                (UINT4) i4DestIfIndex,
                                                PcpTblEntry);
                if (i1retVal == PBB_FAILURE)
                {
                    return PBB_FAILURE;
                }
            }
        }
    }
    return PBB_SUCCESS;
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : PbbHwSetPortUseDei.                               */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PbbHwSetPortUseDei (UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei)
{
#ifdef NPAPI_WANTED
    if (u1UseDei == PBB_VLAN_SNMP_TRUE)
    {
        u1UseDei = FNP_TRUE;
    }
    else
    {
        u1UseDei = FNP_FALSE;
    }
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
#ifdef L2RED_WANTED
        if (NpSyncFsMiPbbHwSetPortUseDei (u4ContextId,
                                          u4IfIndex, u1UseDei) != FNP_SUCCESS)
#else
        if (VlanFsMiVlanHwSetPortUseDei (u4ContextId,
                                         u4IfIndex, u1UseDei) != FNP_SUCCESS)
#endif
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiVlanHwSetPortUseDei failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1UseDei);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Use Dei set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : PbbHwSetPortReqDropEncoding.                      */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PbbHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                             UINT1 u1ReqDrpEncoding)
{
#ifdef NPAPI_WANTED
    if (u1ReqDrpEncoding == PBB_VLAN_SNMP_TRUE)
    {
        u1ReqDrpEncoding = FNP_TRUE;
    }
    else
    {
        u1ReqDrpEncoding = FNP_FALSE;
    }
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
#ifdef L2RED_WANTED
        if (NpSyncFsMiPbbHwSetPortReqDropEncoding (u4ContextId,
                                                   u4IfIndex,
                                                   u1ReqDrpEncoding)
            != FNP_SUCCESS)
#else
        if (VlanFsMiVlanHwSetPortReqDropEncoding (u4ContextId,
                                                  u4IfIndex,
                                                  u1ReqDrpEncoding) !=
            FNP_SUCCESS)
#endif
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiVlanHwSetPortReqDropEncoding failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ReqDrpEncoding);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Require Drop encoding set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : PbbHwSetPortPcpSelRow.                            */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex       - Port Index.                        */
/*                      u1PcpSelection  - It can take the following values:  */
/*                                            8P0D, 7P1D, 6P2D, 5P3D         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PbbHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                          UINT1 u1PcpSelection)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
#ifdef L2RED_WANTED
        if (NpSyncFsMiPbbHwSetPortPcpSelection (u4ContextId,
                                                u4IfIndex,
                                                (UINT2) u1PcpSelection) !=
            FNP_SUCCESS)
#else
        if (VlanFsMiVlanHwSetPortPcpSelection (u4ContextId,
                                               u4IfIndex,
                                               (UINT2) u1PcpSelection) !=
            FNP_SUCCESS)
#endif
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiVlanHwSetPortPcpSelection failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1PcpSelection);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Pcp selection value set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbHwSetPcpEncodTbl                               */
/*                                                                          */
/*    Description        : This function is used to set Pcp encoding table   */
/*                                                                          */
/*    Input(s)           : Context id
                           If index
                           Pcp decoding values
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbHwSetPcpEncodTbl (UINT4 u4ContextId,
                     UINT4 u4IfIndex, tHwPbbPcpInfo PcpTblEntry)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
#ifdef L2RED_WANTED
        if (NpSyncFsMiPbbHwSetPcpEncodTbl (u4ContextId,
                                           u4IfIndex, PcpTblEntry)
            != FNP_SUCCESS)
#else
        if (VlanFsMiVlanHwSetPcpEncodTbl (u4ContextId,
                                          u4IfIndex,
                                          PcpTblEntry) != FNP_SUCCESS)
#endif
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiVlanHwSetPcpEncodTbl failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (PcpTblEntry);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Pcp encoding table entry set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbHwSetPcpDecodTbl                               */
/*                                                                          */
/*    Description        : This function is used to set Pcp decoding table   */
/*                                                                          */
/*    Input(s)           : Context id
                           If index
                           Pcp decoding values
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbHwSetPcpDecodTbl (UINT4 u4ContextId,
                     UINT4 u4IfIndex, tHwPbbPcpInfo PcpTblEntry)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
#ifdef L2RED_WANTED
        if (NpSyncFsMiPbbHwSetPcpDecodTbl (u4ContextId,
                                           u4IfIndex, PcpTblEntry)
            != FNP_SUCCESS)
#else
        if (VlanFsMiVlanHwSetPcpDecodTbl (u4ContextId,
                                          u4IfIndex,
                                          PcpTblEntry) != FNP_SUCCESS)
#endif
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiVlanHwSetPcpDecodTbl failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (PcpTblEntry);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Pcp decoding table entry set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbHwAddVipToPip                         */
/*                                                                          */
/*    Description        : This function is used to set vip pip for a isid
*/
/*                                                                          */
/*    Input(s)           : Context id
                           Isid
                           Vip
                           Pip
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbHwAddVipToPip (UINT4 u4ContextId, UINT4 u4Isid, UINT2 u2Vip, UINT2 u2Pip)
{
    INT4                i4VipIfIndex = PBB_INIT_VAL;
    INT4                i4PipIfIndex = PBB_INIT_VAL;

#ifdef NPAPI_WANTED
    if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                       u2Vip, &i4VipIfIndex) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return PBB_FAILURE;
    }
    if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                       u2Pip, &i4PipIfIndex) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        tVipPipMap          vippipmap;
        vippipmap.u4PipIfIndex = (UINT4) i4PipIfIndex;
        if ((u4Isid >= PBB_MIN_ISID) && (u4Isid <= PBB_MAX_ISID_VALUE))
        {
            vippipmap.u1IsIsidValid = FNP_TRUE;
            vippipmap.u4Isid = u4Isid;
        }
        else
        {
            vippipmap.u1IsIsidValid = FNP_FALSE;
            vippipmap.u4Isid = PBB_ISID_INVALID_VALUE;
        }
        if (FsMiPbbHwSetVipPipMap (u4ContextId, (UINT4) i4VipIfIndex,
                                   vippipmap) == FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwSetVipPipMap failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u2Vip);
    UNUSED_PARAM (u2Pip);
    UNUSED_PARAM (i4VipIfIndex);
    UNUSED_PARAM (i4PipIfIndex);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Vip to Pip map set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbHwDeleteLocalVipToPip                         */
/*                                                                          */
/*    Description        : This function is used to reset vip pip for a isid
*/
/*                                                                          */
/*    Input(s)           : Context id
                           Isid
                           Vip
                           Pip
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbHwDeleteLocalVipToPip (UINT4 u4ContextId, INT4 i4IfIndex, UINT4 u4Isid,
                          UINT2 u2Pip)
{
#ifdef NPAPI_WANTED
    INT4                i4VipIfIndex = PBB_INIT_VAL;
    i4VipIfIndex = i4IfIndex;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (FsMiPbbHwDelVipPipMap (u4ContextId, (UINT4) i4VipIfIndex) ==
            FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwDelVipPipMap failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u2Pip);
#endif
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u2Pip);
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Vip to Pip map deleting at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbHwDeleteVipToPip                         */
/*                                                                          */
/*    Description        : This function is used to reset vip pip for a isid
*/
/*                                                                          */
/*    Input(s)           : Context id
                           Isid
                           Vip
                           Pip
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbHwDeleteVipToPip (UINT4 u4ContextId, UINT4 u4Isid, UINT2 u2Vip, UINT2 u2Pip)
{
    INT4                i4VipIfIndex = PBB_INIT_VAL;
#ifdef NPAPI_WANTED
    if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                       u2Vip, &i4VipIfIndex) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return PBB_FAILURE;
    }

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (FsMiPbbHwDelVipPipMap (u4ContextId, (UINT4) i4VipIfIndex) ==
            FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwDelVipPipMap failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4VipIfIndex);
    UNUSED_PARAM (u2Vip);
#endif
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u2Pip);
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Vip to Pip map deleting at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDelBCompHwServiceInst                         */
/*                                                                          */
/*    Description        : This function is used to Del CBP ISID port data  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDelBCompHwServiceInst (UINT4 u4ContextId, INT4 i4IfIndex, UINT4 u4Isid)
{

#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (FsMiPbbHwDelBackboneServiceInstEntry (u4ContextId,
                                                  i4IfIndex,
                                                  u4Isid) == FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwDelBackboneServiceInstEntry failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4Isid);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Deleting Backbone service instance entry at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDelICompHwServiceInst                                  */
/*                                                                          */
/*    Description        : This function is used to Del CBP ISID port data  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDelICompHwServiceInst (UINT4 u4ContextId, INT4 i4IfMainIndex)
{

#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {

        if (FsMiPbbHwDelVipAttributes (u4ContextId, i4IfMainIndex) ==
            FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwDelBackboneServiceInstEntry failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4IfMainIndex);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Deleting Vip attributes from hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetBCompHwServiceInstOnDest                   */
/*                                                                          */
/*    Description        : This function is used to Set B comp NP data      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetBCompHwServiceInstOnDest (UINT4 u4ContextId,
                                UINT4 u4Isid,
                                INT4 i4ifIndex, INT4 i4DestIfIndex)
{
#ifdef NPAPI_WANTED
    tBackboneServiceInstEntry ServiceInst;
#endif
    tMacAddr            DefaultBackboneDest;
    tMacAddr            BSIGAddress;
    tMacAddr            tempMacAddr;
    UINT4               u4BVlanId = PBB_INIT_VAL;
    UINT4               u4BSigIsid = PBB_INIT_VAL;
    UINT4               u4LocalIsid = PBB_ISID_INVALID_VALUE;
#ifdef NPAPI_WANTED
    tSNMP_OCTET_STRING_TYPE pGlbOUI;
    tSNMP_OCTET_STRING_TYPE pOUI;
    UINT4               u4SizeofMac = PBB_SIZE_OF_MAC_ADDR;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT4               u4ServiceInstIsid = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1ByteIndex = PBB_INIT_VAL;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    UINT1               au1GlbOUI[PBB_OUI_LENGTH];
#endif
    UINT1               u1ServiceMapType = PBB_VIP_TYPE_BOTH;
#ifdef NPAPI_WANTED
    PBB_MEMSET (au1OUI, PBB_INIT_VAL, PBB_OUI_LENGTH);
#endif
    PBB_MEMSET (DefaultBackboneDest, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (BSIGAddress, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (tempMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
#ifdef NPAPI_WANTED
    pGlbOUI.i4_Length = PBB_OUI_LENGTH;
    pGlbOUI.pu1_OctetList = au1GlbOUI;
    pOUI.i4_Length = PBB_OUI_LENGTH;
    pOUI.pu1_OctetList = au1OUI;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (PbbGetIsidPortBVlanId (i4ifIndex,
                                   u4Isid, &u4BVlanId) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        ServiceInst.BVid = u4BVlanId;
        if (PbbGetIsidPortLocalIsid (i4ifIndex,
                                     u4Isid, &u4LocalIsid) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (u4LocalIsid != PBB_ISID_INVALID_VALUE)
        {
            ServiceInst.u4LocalSid = u4LocalIsid;
            u4BSigIsid = u4LocalIsid;
        }
        else
        {
            u4BSigIsid = u4Isid;
            ServiceInst.u4LocalSid = u4Isid;
        }
        if (PbbGetIsidPortDefBDA (u2LocalPort, u4Isid, &DefaultBackboneDest)
            != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (PbbGetIsidOUI (u4ContextId,
                           u4Isid, i4ifIndex, &pOUI) == PBB_FAILURE)
        {
            if (PbbGetPbbGlbOUI (&pOUI) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
        }
        if (PBB_MEMCMP (DefaultBackboneDest, tempMacAddr,
                        sizeof (tMacAddr)) == PBB_INIT_VAL)
        {

            if (PbbGetPbbGlbOUI (&pGlbOUI) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
            PBB_MEMCPY (DefaultBackboneDest, pGlbOUI.pu1_OctetList,
                        PBB_OUI_LENGTH);
            u4ServiceInstIsid = u4Isid;
            DefaultBackboneDest[0] = DefaultBackboneDest[0] | 0x01;
            for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
            {
                u1ByteIndex = 0xFF & u4ServiceInstIsid;
                DefaultBackboneDest[u4Index] = u1ByteIndex;
                u4ServiceInstIsid = u4ServiceInstIsid >> 8;
                u1ByteIndex = PBB_INIT_VAL;
            }
        }
        PBB_MEMCPY (BSIGAddress, pOUI.pu1_OctetList, PBB_OUI_LENGTH);
        u1ByteIndex = PBB_INIT_VAL;
        BSIGAddress[0] = BSIGAddress[0] | 0x01;
        for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
        {
            u1ByteIndex = 0xFF & u4BSigIsid;
            BSIGAddress[u4Index] = u1ByteIndex;
            u4BSigIsid = u4BSigIsid >> 8;
            u1ByteIndex = PBB_INIT_VAL;
        }
        PBB_MEMCPY (ServiceInst.DefDestMac, DefaultBackboneDest,
                    sizeof (tMacAddr));
        PBB_MEMCPY (ServiceInst.BSIGMacAddress, BSIGAddress, sizeof (tMacAddr));

        if (PbbGetIsidPortServiceMapType (i4ifIndex,
                                          u4Isid,
                                          &u1ServiceMapType) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        ServiceInst.u1ServiceType = u1ServiceMapType;

        if (FsMiPbbHwSetBackboneServiceInstEntry (u4ContextId,
                                                  (UINT4) i4DestIfIndex,
                                                  u4Isid,
                                                  ServiceInst) == FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwSetBackboneServiceInstEntry failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (i4ifIndex);
    UNUSED_PARAM (i4DestIfIndex);
    UNUSED_PARAM (u4BVlanId);
    UNUSED_PARAM (u1ServiceMapType);
    UNUSED_PARAM (u4LocalIsid);
    UNUSED_PARAM (u4BSigIsid);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Backbone Service Instance entry set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetBCompHwServiceInst                         */
/*                                                                          */
/*    Description        : This function is used to Set B comp NP data      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetBCompHwServiceInst (UINT4 u4ContextId, UINT4 u4Isid, INT4 i4ifIndex)
{
#ifdef NPAPI_WANTED
    tBackboneServiceInstEntry ServiceInst;
#endif
    tMacAddr            DefaultBackboneDest;
    tMacAddr            BSIGAddress;
    tMacAddr            tempMacAddr;
    UINT4               u4BVlanId = PBB_INIT_VAL;
    UINT4               u4BSigIsid = PBB_INIT_VAL;
    UINT4               u4LocalIsid = PBB_ISID_INVALID_VALUE;
#ifdef NPAPI_WANTED
    tSNMP_OCTET_STRING_TYPE pOUI;
    tSNMP_OCTET_STRING_TYPE pGlbOUI;
    UINT4               u4SizeofMac = PBB_SIZE_OF_MAC_ADDR;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT4               u4ServiceInstIsid = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               u1ByteIndex = PBB_INIT_VAL;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    UINT1               au1GlbOUI[PBB_OUI_LENGTH];
#endif
    UINT1               u1ServiceMapType = PBB_VIP_TYPE_BOTH;
#ifdef NPAPI_WANTED
    PBB_MEMSET (au1OUI, PBB_INIT_VAL, PBB_OUI_LENGTH);
#endif
    PBB_MEMSET (DefaultBackboneDest, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (BSIGAddress, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (tempMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));

#ifdef NPAPI_WANTED
    pOUI.i4_Length = PBB_OUI_LENGTH;
    pOUI.pu1_OctetList = au1OUI;
    pGlbOUI.i4_Length = PBB_OUI_LENGTH;
    pGlbOUI.pu1_OctetList = au1GlbOUI;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (PbbGetIsidPortBVlanId (i4ifIndex,
                                   u4Isid, &u4BVlanId) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        ServiceInst.BVid = u4BVlanId;
        if (PbbGetIsidPortLocalIsid (i4ifIndex,
                                     u4Isid, &u4LocalIsid) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (u4LocalIsid != PBB_ISID_INVALID_VALUE)
        {
            ServiceInst.u4LocalSid = u4LocalIsid;
            u4BSigIsid = u4LocalIsid;
        }
        else
        {
            u4BSigIsid = u4Isid;
            ServiceInst.u4LocalSid = u4Isid;
        }

        if (PbbGetIsidPortDefBDA (u2LocalPort, u4Isid, &DefaultBackboneDest)
            != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (PbbGetIsidOUI (u4ContextId,
                           u4Isid, i4ifIndex, &pOUI) == PBB_FAILURE)
        {
            if (PbbGetPbbGlbOUI (&pOUI) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
        }
        if (PBB_MEMCMP (DefaultBackboneDest, tempMacAddr,
                        sizeof (tMacAddr)) == PBB_INIT_VAL)
        {
            if (PbbGetPbbGlbOUI (&pGlbOUI) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
            PBB_MEMCPY (DefaultBackboneDest, pGlbOUI.pu1_OctetList,
                        PBB_OUI_LENGTH);
            u4ServiceInstIsid = u4Isid;
            DefaultBackboneDest[0] = DefaultBackboneDest[0] | 0x01;
            for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
            {
                u1ByteIndex = 0xFF & u4ServiceInstIsid;
                DefaultBackboneDest[u4Index] = u1ByteIndex;
                u4ServiceInstIsid = u4ServiceInstIsid >> 8;
                u1ByteIndex = PBB_INIT_VAL;
            }
        }
        PBB_MEMCPY (BSIGAddress, pOUI.pu1_OctetList, PBB_OUI_LENGTH);
        u1ByteIndex = PBB_INIT_VAL;
        BSIGAddress[0] = BSIGAddress[0] | 0x01;
        for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
        {
            u1ByteIndex = 0xFF & u4BSigIsid;
            BSIGAddress[u4Index] = u1ByteIndex;
            u4BSigIsid = u4BSigIsid >> 8;
            u1ByteIndex = PBB_INIT_VAL;
        }
        PBB_MEMCPY (ServiceInst.DefDestMac, DefaultBackboneDest,
                    sizeof (tMacAddr));
        PBB_MEMCPY (ServiceInst.BSIGMacAddress, BSIGAddress, sizeof (tMacAddr));

        if (PbbGetIsidPortServiceMapType (i4ifIndex,
                                          u4Isid,
                                          &u1ServiceMapType) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        ServiceInst.u1ServiceType = u1ServiceMapType;

        if (FsMiPbbHwSetBackboneServiceInstEntry (u4ContextId,
                                                  (UINT4) i4ifIndex,
                                                  u4Isid,
                                                  ServiceInst) == FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwSetBackboneServiceInstEntry failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (i4ifIndex);
    UNUSED_PARAM (u4BVlanId);
    UNUSED_PARAM (u1ServiceMapType);
    UNUSED_PARAM (u4LocalIsid);
    UNUSED_PARAM (u4BSigIsid);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Backbone Service Instance entry set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetICompHwServiceInst                         */
/*                                                                          */
/*    Description        : This function is used to Set I comp NP data      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetICompHwServiceInst (UINT4 u4ContextId, INT4 i4ifIndex, UINT4 u4Isid)
{
#ifdef NPAPI_WANTED
    tVipAttribute       ServiceInst;
    tMacAddr            DefaultBackboneDest;
    tMacAddr            tempMacAddr;
    tMacAddr            BSIGAddress;
    tSNMP_OCTET_STRING_TYPE pOUI;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT4               u4ServiceInstIsid = PBB_INIT_VAL;
    UINT4               u4SizeofMac = PBB_SIZE_OF_MAC_ADDR;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    UINT1               u1ByteIndex = PBB_INIT_VAL;
    UINT1               u1ServiceType = PBB_VIP_TYPE_BOTH;

    PBB_MEMSET (au1OUI, PBB_INIT_VAL, PBB_OUI_LENGTH);
    PBB_MEMSET (DefaultBackboneDest, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (tempMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (BSIGAddress, PBB_INIT_VAL, sizeof (tMacAddr));
    pOUI.i4_Length = PBB_OUI_LENGTH;
    pOUI.pu1_OctetList = au1OUI;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        ServiceInst.u4Isid = u4Isid;
        if (PbbGetVipType (i4ifIndex, &u1ServiceType) != PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_INVALID_VIP);
            return SNMP_FAILURE;
        }
        ServiceInst.u1ServiceType = u1ServiceType;

        if (PbbGetVipDefaultDstBMAC (i4ifIndex, &DefaultBackboneDest) !=
            PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_INVALID_VIP);
            PbbReleaseContext ();
            return SNMP_FAILURE;
        }
        if (PbbGetPbbGlbOUI (&pOUI) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (PBB_MEMCMP (DefaultBackboneDest, tempMacAddr,
                        sizeof (tMacAddr)) == PBB_INIT_VAL)
        {

            PBB_MEMCPY (DefaultBackboneDest, pOUI.pu1_OctetList,
                        PBB_OUI_LENGTH);
            u4ServiceInstIsid = u4Isid;
            DefaultBackboneDest[0] = DefaultBackboneDest[0] | 0x01;
            for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
            {
                u1ByteIndex = 0xFF & u4ServiceInstIsid;
                DefaultBackboneDest[u4Index] = u1ByteIndex;
                u4ServiceInstIsid = u4ServiceInstIsid >> 8;
                u1ByteIndex = PBB_INIT_VAL;
            }
        }
        u1ByteIndex = PBB_INIT_VAL;
        u4Index = PBB_INIT_VAL;
        PBB_MEMCPY (BSIGAddress, pOUI.pu1_OctetList, PBB_OUI_LENGTH);
        BSIGAddress[0] = BSIGAddress[0] | 0x01;
        for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
        {
            u1ByteIndex = 0xFF & u4Isid;
            BSIGAddress[u4Index] = u1ByteIndex;
            u4Isid = u4Isid >> 8;
            u1ByteIndex = PBB_INIT_VAL;
        }
        PBB_MEMCPY (ServiceInst.DefDestMacAddress, DefaultBackboneDest,
                    sizeof (tMacAddr));
        PBB_MEMCPY (ServiceInst.BSIGMacAddress, BSIGAddress, sizeof (tMacAddr));

        if (FsMiPbbHwSetVipAttributes (u4ContextId, (UINT4) i4ifIndex,
                                       ServiceInst) == FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwSetVipAttributes failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (i4ifIndex);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Vip attributes set at hardware successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbNpShutdown                                    */
/*    Description         : This function is used to send Shut down          */
/*                           indication to NP  .                             */
/*                                                                           */
/*    Input(s)            :                                                  */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbNpShutdown ()
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        FsMiPbbNpDeInitHw ();
        PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: FsMiPbbNpDeInitHw succedeed \n", __FILE__,
                      PBB_FUNCTION_NAME);
    }
#endif
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbNpNoShutdown                                  */
/*    Description         :ICompHwServiceIns This function is used to send   */
/*                         No Shut down indication to NP.                    */
/*                                                                           */
/*    Input(s)            :                                                  */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbNpNoShutdown ()
{
#ifdef NPAPI_WANTED
    /*  PBB_IS_NP_PROGRAMMING_ALLOWED () check is not needed here because
     *  initialisation needs to be done for both active and
     *  standby node (on the lines of Vlan Module).
     */
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (FsMiPbbNpInitHw () == FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbNpDeInitHw failed \n", __FILE__,
                          PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: PBB no shutdown set at hardware successfully \n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbSetHwPortPisid                                */
/*    Description         : This function is used to set pisid for Port      */
/*                                                                           */
/*    Input(s)            : u4ContextId.                                     */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbSetHwPortPisid (UINT4 u4ContextId, INT4 i4ifIndex, UINT4 u4Pisid)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (FsMiPbbHwSetAllToOneBundlingService (u4ContextId,
                                                 i4ifIndex,
                                                 u4Pisid) == FNP_FAILURE)
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwSetAllToOneBundlingService failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4ifIndex);
    UNUSED_PARAM (u4Pisid);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Pisid set successfully at hardware \n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbDelHwPortPisid                                */
/*    Description         : This function is used to send delete             */
/*                          indication for pisid Port                        */
/*                                                                           */
/*    Input(s)            : u4ContextId.                                     */
/*                                                                           */
/*    Output(s)           :                                                  */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbDelHwPortPisid (UINT4 u4ContextId, INT4 i4ifIndex, UINT4 u4Pisid)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (FsMiPbbHwDelAllToOneBundlingService (u4ContextId,
                                                 (UINT4) i4ifIndex,
                                                 u4Pisid) == FNP_FAILURE)
        {

            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiPbbHwDelAllToOneBundlingService failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4ifIndex);
    UNUSED_PARAM (u4Pisid);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Pisid deleted successfully at hardware \n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbHwSetProviderBridgePortType.                     */
/*                                                                           */
/* Description        : This function is called when the port type is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On PBB_SUCCESS                       */
/*                      PBB_FAILURE - On failure                            */
/*****************************************************************************/
INT1
PbbHwSetProviderBridgePortType (UINT4 u4ContextId, INT4 i4IfIndex,
                                UINT1 u1PortType)
{
    UINT4               u4PortType = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    u4PortType = (UINT4) u1PortType;
    u4IfIndex = i4IfIndex;
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
#ifdef L2RED_WANTED
        if (NpSyncFsMiPbbHwSetProviderBridgePortType (u4ContextId,
                                                      u4IfIndex,
                                                      u4PortType)
            == FNP_FAILURE)
#else
        if (VlanFsMiVlanHwSetProviderBridgePortType (u4ContextId,
                                                     u4IfIndex,
                                                     u4PortType) == FNP_FAILURE)
#endif
        {
            PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiVlanHwSetProviderBridgePortType failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4PortType);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | DATA_PATH_TRC | ALL_FAILURE_TRC,
                  "%s :: %s() :: Port type PIP configured successfully at hardware \n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbHwSetPortIsidLckStatus                              */
/*                                                                           */
/* Description        : This function is called to set the lock status of a  */
/*                      and a isid                                           */
/*                                                                           */
/* Input(s)           : u4IfIndex: Interface Index                           */
/*                      u4Isid: Isid for which status to be updated          */
/*                      u1Lockstatus: Lock Status                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On Success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PbbHwSetPortIsidLckStatus (UINT4 u4IfIndex, UINT4 u4Isid, UINT1 u1Lockstatus)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        UINT4               u4ContextId = 0;
        UINT2               u2LocalPort = 0;
        if (PbbVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                             &u2LocalPort) == VCM_FAILURE)
        {
            return PBB_FAILURE;
        }
        if (FsMiPbbHwSetPortIsidLckStatus (u4ContextId,
                                           u4IfIndex, u4Isid, u1Lockstatus)
            != FNP_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u1Lockstatus);
#endif
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbHwGetPortIsidStats                                  */
/*                                                                           */
/* Description        : This function is called to get the number  of        */
/*                      recieved and transmitted frames on a port for an isid*/
/*                                                                           */
/* Input(s)           : u4IfIndex: Interface Index                           */
/*                      u4Isid: Isid for which status to be updated          */
/*                      pu4TxFCl: pointer to number of transmitted frames    */
/*                      pu4RxFCl: pointer to number of recieved frames       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On Success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PbbHwGetPortIsidStats (UINT4 u4IfIndex,
                       UINT4 u4Isid, UINT1 u1StatsType, UINT4 *pu4Stats)
{
#ifdef NPAPI_WANTED
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPort = 0;
    if (PbbVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                         &u2LocalPort) == VCM_FAILURE)
    {
        return PBB_FAILURE;
    }
    if (FsMiPbbHwGetPortIsidStats
        (u4ContextId, u4IfIndex, u4Isid, u1StatsType, pu4Stats) != FNP_SUCCESS)
    {
        return PBB_FAILURE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (u1StatsType);
    UNUSED_PARAM (pu4Stats);
#endif
    return PBB_SUCCESS;
}

#ifdef MBSM_WANTED

/*****************************************************************************/
/* Function Name      : PbbMbsmHwSetProviderBridgePortType.                     */
/*                                                                           */
/* Description        : This function is called when the port type is        */
/*                      configured for a port.                               */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u4PortType - CUSTOMER_NETWORK_PORT/CUSTOMER_EDGE_PORT*/
/*                      PROVIDER_NETWORK_PORT                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On PBB_SUCCESS                       */
/*                      PBB_FAILURE - On failure                            */
/*****************************************************************************/
INT4
PbbMbsmHwSetProviderBridgePortType (UINT4 u4ContextId, INT4 i4IfIndex,
                                    UINT1 u1PortType, tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4PortType = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    u4PortType = (UINT4) u1PortType;
    u4IfIndex = i4IfIndex;
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (VlanFsMiVlanMbsmHwSetProviderBridgePortType (u4ContextId,
                                                         u4IfIndex,
                                                         u4PortType,
                                                         pSlotInfo) ==
            FNP_FAILURE)
        {
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u4PortType);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/* Use-DEI parameter. */
/*****************************************************************************/
/* Function Name      : PbbMbsmHwSetPortUseDei.                             */
/*                                                                           */
/* Description        : This function sets the Use_DEI parameter for the     */
/*                      given port.                                          */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1UseDei  - True/False for Use_DEI parameter for     */
/*                                  this port.                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PbbMbsmHwSetPortUseDei (UINT4 u4ContextId,
                        UINT4 u4IfIndex,
                        UINT1 u1UseDei, tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    if (u1UseDei == PBB_VLAN_SNMP_TRUE)
    {
        u1UseDei = FNP_TRUE;
    }
    else
    {
        u1UseDei = FNP_FALSE;
    }
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (VlanFsMiVlanMbsmHwSetPortUseDei (u4ContextId,
                                             u4IfIndex,
                                             u1UseDei,
                                             pSlotInfo) != FNP_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1UseDei);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/* Require Drop Encoding parameter. */
/*****************************************************************************/
/* Function Name      : PbbMbsmHwSetPortReqDropEncoding.                    */
/*                                                                           */
/* Description        : This function sets the Require Drop Encoding         */
/*                      parameter for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex - Port Index.                              */
/*                      u1ReqDropEncoding  - True/False value for Require    */
/*                                           Drop encoding parameter for     */
/*                                           this port.                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PbbMbsmHwSetPortReqDropEncoding (UINT4 u4ContextId, UINT4 u4IfIndex,
                                 UINT1 u1ReqDrpEncoding,
                                 tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    if (u1ReqDrpEncoding == PBB_VLAN_SNMP_TRUE)
    {
        u1ReqDrpEncoding = FNP_TRUE;
    }
    else
    {
        u1ReqDrpEncoding = FNP_FALSE;
    }
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (VlanFsMiVlanMbsmHwSetPortReqDropEncoding (u4ContextId,
                                                      u4IfIndex,
                                                      u1ReqDrpEncoding,
                                                      pSlotInfo) != FNP_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }
#else

    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1ReqDrpEncoding);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/* Port Priority Code Point Selection Row. */
/*****************************************************************************/
/* Function Name      : PbbMbsmHwSetPortPcpSelRow.                          */
/*                                                                           */
/* Description        : This function sets the Port Priority Code Point      */
/*                      Selection for the given port.                        */
/*                                                                           */
/* Input(s)           : u4IfIndex       - Port Index.                        */
/*                      u1PcpSelection  - It can take the following values:  */
/*                                            8P0D, 7P1D, 6P2D, 5P3D         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/
INT4
PbbMbsmHwSetPortPcpSelection (UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT1 u1PcpSelection, tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (VlanFsMiVlanMbsmHwSetPortPcpSelection (u4ContextId,
                                                   u4IfIndex,
                                                   (UINT2) u1PcpSelection,
                                                   pSlotInfo) != FNP_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1PcpSelection);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbMbsmHwSetPcpEncodTbl                          */
/*                                                                          */
/*    Description        : This function is used to set Pcp encoding table   */
/*                                                                          */
/*    Input(s)           : Context id
                           If index
                           Pcp decoding values
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbMbsmHwSetPcpEncodTbl (UINT4 u4ContextId,
                         UINT4 u4IfIndex,
                         tHwPbbPcpInfo PcpTblEntry, tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (VlanFsMiVlanMbsmHwSetPcpEncodTbl (u4ContextId,
                                              u4IfIndex,
                                              PcpTblEntry,
                                              pSlotInfo) != FNP_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (PcpTblEntry);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbMbsmHwSetPcpDecodTbl                               */
/*                                                                          */
/*    Description        : This function is used to set Pcp decoding table   */
/*                                                                          */
/*    Input(s)           : Context id
                           If index
                           Pcp decoding values
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbMbsmHwSetPcpDecodTbl (UINT4 u4ContextId,
                         UINT4 u4IfIndex,
                         tHwPbbPcpInfo PcpTblEntry, tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (VlanFsMiVlanMbsmHwSetPcpDecodTbl (u4ContextId,
                                              u4IfIndex,
                                              PcpTblEntry,
                                              pSlotInfo) != FNP_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (PcpTblEntry);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbMbsmHwSetPipPcpAttributes
 */
/*                                                                           */
/* Description        : This function sets the Pcp attricutes for a Pip 
 *                      at the destination if index
 */
/* Input(s)           : Context id
 *                      Src local port
 *                      Destination if index
 *                      Mbsm Slot Information
 */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On success                             */
/*                      PBB_FAILURE - On failure                             */
/*****************************************************************************/

INT1
PbbMbsmHwSetPipPcpAttributes (UINT4 u4ContextId,
                              UINT2 u2SrcPort, tMbsmSlotInfo * pSlotInfo)
{
    UINT1               u1PcpSelRow = PBB_INIT_VAL;
    UINT1               u1PcpUseDei = PBB_INIT_VAL;
    UINT1               u1PcpReqDropEnc = PBB_INIT_VAL;
    UINT1               u1PcpVal = PBB_INIT_VAL;
    UINT1               u1Priority = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;
    tHwPbbPcpInfo       PcpTblEntry;
    INT4                i4IfIndex = PBB_INIT_VAL;
    INT1                i1retVal = PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                                  u2SrcPort,
                                                                  &i4IfIndex);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&PcpTblEntry, PBB_INIT_VAL, sizeof (tHwPbbPcpInfo));
    i1retVal = PbbGetPcpSelRow (u2SrcPort, &u1PcpSelRow);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbMbsmHwSetPortPcpSelection (u4ContextId,
                                             (UINT4) i4IfIndex,
                                             u1PcpSelRow, pSlotInfo);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbGetPcpUseDei (u2SrcPort, &u1PcpUseDei);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbMbsmHwSetPortUseDei (u4ContextId,
                                       (UINT4) i4IfIndex,
                                       u1PcpUseDei, pSlotInfo);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbGetPcpReqDropEncoding (u2SrcPort, &u1PcpReqDropEnc);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i1retVal = PbbMbsmHwSetPortReqDropEncoding (u4ContextId,
                                                (UINT4) i4IfIndex,
                                                u1PcpReqDropEnc, pSlotInfo);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    for (u1PcpSelRow = PBB_INIT_VAL; u1PcpSelRow < PBB_MAX_NUM_PCP_SEL_ROW;
         u1PcpSelRow++)
    {
        for (u1PcpVal = PBB_INIT_VAL; u1PcpVal < PBB_MAX_NUM_PCP; u1PcpVal++)
        {
            PcpTblEntry.u2PcpSelRow = (UINT2) u1PcpSelRow;
            PcpTblEntry.u2PcpValue = (UINT2) u1PcpVal;
            i1retVal = PbbGetPcpDecodingDE (u2SrcPort,
                                            u1PcpSelRow,
                                            u1PcpVal, &u1DropEligible);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            PcpTblEntry.u1DropEligible = u1DropEligible;
            i1retVal = PbbGetPcpDecodingPriority (u2SrcPort,
                                                  u1PcpSelRow,
                                                  u1PcpVal, &u1Priority);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            PcpTblEntry.u2Priority = (UINT2) u1Priority;
            i1retVal = PbbMbsmHwSetPcpDecodTbl (u4ContextId,
                                                (UINT4) i4IfIndex,
                                                PcpTblEntry, pSlotInfo);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
        }
    }
    for (u1PcpSelRow = PBB_INIT_VAL; u1PcpSelRow < PBB_MAX_NUM_PCP_SEL_ROW;
         u1PcpSelRow++)
    {
        for (u1Priority = PBB_INIT_VAL; u1Priority < PBB_MAX_NUM_PRIORITY;
             u1Priority++)
        {
            for (u1DropEligible = PBB_INIT_VAL;
                 u1DropEligible < PBB_MAX_NUM_DROP_ELIGIBLE; u1DropEligible++)
            {
                i1retVal = PbbGetPcpEncodingPcpVal (u2SrcPort,
                                                    u1PcpSelRow,
                                                    u1Priority,
                                                    u1DropEligible, &u1PcpVal);
                if (i1retVal == PBB_FAILURE)
                {
                    return PBB_FAILURE;
                }
                PcpTblEntry.u2PcpSelRow = (UINT2) u1PcpSelRow;
                PcpTblEntry.u2PcpValue = (UINT2) u1PcpVal;
                PcpTblEntry.u2Priority = (UINT2) u1Priority;
                PcpTblEntry.u1DropEligible = (UINT1) u1DropEligible;
                i1retVal = PbbMbsmHwSetPcpEncodTbl (u4ContextId,
                                                    (UINT4) i4IfIndex,
                                                    PcpTblEntry, pSlotInfo);
                if (i1retVal == PBB_FAILURE)
                {
                    return PBB_FAILURE;
                }
            }
        }
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : pbbMbsmSetICompHwServiceInst                     */
/*                                                                          */
/*    Description        : This function is used to Set I comp NP data      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
pbbMbsmSetICompHwServiceInst (UINT4 u4ContextId,
                              INT4 i4ifIndex,
                              UINT4 u4Isid, tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    tVipAttribute       ServiceInst;
    tMacAddr            DefaultBackboneDest;
    tMacAddr            tempMacAddr;
    tMacAddr            BSIGAddress;
    tSNMP_OCTET_STRING_TYPE pOUI;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT4               u4ServiceInstIsid = PBB_INIT_VAL;
    UINT4               u4SizeofMac = PBB_SIZE_OF_MAC_ADDR;
    UINT2               u2Vip = PBB_INIT_VAL;
    INT4                i4Pip = PBB_INIT_VAL;
    UINT1               u1RowStatus = PBB_INIT_VAL;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    UINT1               u1ByteIndex = PBB_INIT_VAL;
    UINT1               u1ServiceType = PBB_VIP_TYPE_BOTH;

    PBB_MEMSET (au1OUI, PBB_INIT_VAL, PBB_OUI_LENGTH);
    PBB_MEMSET (DefaultBackboneDest, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (tempMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (BSIGAddress, PBB_INIT_VAL, sizeof (tMacAddr));
    pOUI.i4_Length = PBB_OUI_LENGTH;
    pOUI.pu1_OctetList = au1OUI;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        /* Set the Vip Pip */
        if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2Vip) == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }
        if (PbbGetVipPipRowStatus (u4ContextId, u2Vip, (UINT4 *) &u1RowStatus)
            == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }
        if (u1RowStatus == PBB_ACTIVE)
        {
            if (PbbGetVipPipInterfaceIndex (u4ContextId,
                                            u2Vip, &i4Pip) == PBB_SUCCESS)
            {
                tVipPipMap          vippipmap;
                vippipmap.u4PipIfIndex = i4Pip;
                if ((u4Isid < PBB_MAX_ISID_VALUE) && (u4Isid >= PBB_MIN_ISID))
                {
                    vippipmap.u1IsIsidValid = FNP_TRUE;
                    vippipmap.u4Isid = u4Isid;
                }
                else
                {
                    vippipmap.u1IsIsidValid = FNP_FALSE;
                }
                if (FsMiPbbMbsmHwSetVipPipMap (u4ContextId,
                                               (UINT4) i4ifIndex,
                                               vippipmap,
                                               pSlotInfo) == FNP_FAILURE)
                {
                    return PBB_FAILURE;
                }
            }
        }

        ServiceInst.u4Isid = u4Isid;
        if (PbbGetVipType (i4ifIndex, &u1ServiceType) != PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_INVALID_VIP);
            return SNMP_FAILURE;
        }
        ServiceInst.u1ServiceType = u1ServiceType;

        if (PbbGetVipDefaultDstBMAC (i4ifIndex, &DefaultBackboneDest) !=
            PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_INVALID_VIP);
            PbbReleaseContext ();
            return SNMP_FAILURE;
        }
        if (PbbGetPbbGlbOUI (&pOUI) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (PBB_MEMCMP (DefaultBackboneDest, tempMacAddr,
                        sizeof (tMacAddr)) == PBB_INIT_VAL)
        {

            PBB_MEMCPY (DefaultBackboneDest, pOUI.pu1_OctetList,
                        PBB_OUI_LENGTH);
            u4ServiceInstIsid = u4Isid;
            DefaultBackboneDest[0] = DefaultBackboneDest[0] | 0x01;
            for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
            {
                u1ByteIndex = 0xFF & u4ServiceInstIsid;
                DefaultBackboneDest[u4Index] = u1ByteIndex;
                u4ServiceInstIsid = u4ServiceInstIsid >> 8;
                u1ByteIndex = PBB_INIT_VAL;
            }
        }
        u1ByteIndex = PBB_INIT_VAL;
        u4Index = PBB_INIT_VAL;
        PBB_MEMCPY (BSIGAddress, pOUI.pu1_OctetList, PBB_OUI_LENGTH);
        BSIGAddress[0] = BSIGAddress[0] | 0x01;
        for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
        {
            u1ByteIndex = 0xFF & u4Isid;
            BSIGAddress[u4Index] = u1ByteIndex;
            u4Isid = u4Isid >> 8;
            u1ByteIndex = PBB_INIT_VAL;
        }
        PBB_MEMCPY (ServiceInst.DefDestMacAddress, DefaultBackboneDest,
                    sizeof (tMacAddr));
        PBB_MEMCPY (ServiceInst.BSIGMacAddress, BSIGAddress, sizeof (tMacAddr));

        if (FsMiPbbMbsmHwSetVipAttributes (u4ContextId, (UINT4) i4ifIndex,
                                           ServiceInst,
                                           pSlotInfo) == FNP_FAILURE)
        {
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (i4ifIndex);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : pbbMbsmSetBCompHwServiceInst                         */
/*                                                                          */
/*    Description        : This function is used to Set B comp NP data      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
pbbMbsmSetBCompHwServiceInst (UINT4 u4ContextId,
                              UINT4 u4Isid,
                              INT4 i4ifIndex, tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    tBackboneServiceInstEntry ServiceInst;
    tMacAddr            DefaultBackboneDest;
    tMacAddr            BSIGAddress;
    tMacAddr            tempMacAddr;
    tSNMP_OCTET_STRING_TYPE pOUI;
    tSNMP_OCTET_STRING_TYPE pGlbOUI;
    UINT4               u4SizeofMac = PBB_SIZE_OF_MAC_ADDR;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT4               u4ServiceInstIsid = PBB_INIT_VAL;
    UINT4               u4BVlanId = PBB_INIT_VAL;
    UINT4               u4BSigIsid = PBB_INIT_VAL;
    UINT4               u4LocalIsid = PBB_ISID_INVALID_VALUE;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    UINT1               au1GlbOUI[PBB_OUI_LENGTH];
    UINT1               u1ServiceMapType = PBB_VIP_TYPE_BOTH;
    UINT1               u1ByteIndex = PBB_INIT_VAL;

    PBB_MEMSET (au1OUI, PBB_INIT_VAL, PBB_OUI_LENGTH);
    PBB_MEMSET (DefaultBackboneDest, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (BSIGAddress, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (tempMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    pOUI.i4_Length = PBB_OUI_LENGTH;
    pOUI.pu1_OctetList = au1OUI;
    pGlbOUI.i4_Length = PBB_OUI_LENGTH;
    pGlbOUI.pu1_OctetList = au1GlbOUI;
    UNUSED_PARAM (au1GlbOUI);

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (PbbGetIsidPortBVlanId (i4ifIndex,
                                   u4Isid, &u4BVlanId) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        ServiceInst.BVid = u4BVlanId;
        if (PbbGetIsidPortLocalIsid (i4ifIndex,
                                     u4Isid, &u4LocalIsid) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (u4LocalIsid != PBB_ISID_INVALID_VALUE)
        {
            ServiceInst.u4LocalSid = u4LocalIsid;
            u4BSigIsid = u4LocalIsid;
        }
        else
        {
            u4BSigIsid = u4Isid;
            ServiceInst.u4LocalSid = u4Isid;
        }

        if (PbbGetIsidPortDefBDA (u2LocalPort, u4Isid, &DefaultBackboneDest)
            != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }

        if (PbbGetIsidOUI (u4ContextId,
                           u4Isid, i4ifIndex, &pOUI) == PBB_FAILURE)
        {
            if (PbbGetPbbGlbOUI (&pOUI) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
        }
        if (PBB_MEMCMP (DefaultBackboneDest, tempMacAddr,
                        sizeof (tMacAddr)) == PBB_INIT_VAL)
        {
            if (PbbGetPbbGlbOUI (&pGlbOUI) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
            PBB_MEMCPY (DefaultBackboneDest, pGlbOUI.pu1_OctetList,
                        PBB_OUI_LENGTH);
            u4ServiceInstIsid = u4Isid;
            DefaultBackboneDest[0] = DefaultBackboneDest[0] | 0x01;
            for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
            {
                u1ByteIndex = 0xFF & u4ServiceInstIsid;
                DefaultBackboneDest[u4Index] = u1ByteIndex;
                u4ServiceInstIsid = u4ServiceInstIsid >> 8;
                u1ByteIndex = PBB_INIT_VAL;
            }
        }
        PBB_MEMCPY (BSIGAddress, pOUI.pu1_OctetList, PBB_OUI_LENGTH);
        u1ByteIndex = PBB_INIT_VAL;
        BSIGAddress[0] = BSIGAddress[0] | 0x01;
        for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
        {
            u1ByteIndex = 0xFF & u4BSigIsid;
            BSIGAddress[u4Index] = u1ByteIndex;
            u4BSigIsid = u4BSigIsid >> 8;
            u1ByteIndex = PBB_INIT_VAL;
        }
        PBB_MEMCPY (ServiceInst.DefDestMac, DefaultBackboneDest,
                    sizeof (tMacAddr));
        PBB_MEMCPY (ServiceInst.BSIGMacAddress, BSIGAddress, sizeof (tMacAddr));

        if (PbbGetIsidPortServiceMapType (i4ifIndex,
                                          u4Isid,
                                          &u1ServiceMapType) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        ServiceInst.u1ServiceType = u1ServiceMapType;

        if (FsMiPbbMbsmHwSetBackboneServiceInstEntry (u4ContextId,
                                                      (UINT4) i4ifIndex,
                                                      u4Isid,
                                                      ServiceInst,
                                                      pSlotInfo) == FNP_FAILURE)
        {
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4Isid);
    UNUSED_PARAM (i4ifIndex);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbMbsmHwSetPortInfo
*/
/*                                                                          */
/*    Description        : This is a NPAPI wrapper function to set the 
                           Port information for a particular context 
                           at a slot given by mbsm
*/
/*                                                                          */
/*    Input(s)           : Context Id
                           Slot Information 
                           Isid Vip information
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbMbsmHwSetPortInfo (UINT4 u4ContextId,
                      INT4 i4IfIndex, tMbsmSlotInfo * pSlotInfo)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    INT4                i4retVal = PbbSelectContext (u4ContextId);
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i4retVal = PbbVcmGetContextInfoFromIfIndex (i4IfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i4retVal != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (u4ContextId != PBB_CURR_CONTEXT_ID ())
    {
        return PBB_FAILURE;
    }
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
#ifdef NPAPI_WANTED
    tPbbIsidPipVipNode *pIsidVipNode = NULL;
    tPbbBackBoneSerInstEntry *pIsidPortEntry = NULL;
    tPbbIsidPipVipNode *pPipVipMapNode = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;
    INT4                i4VipIfIndex = PBB_INIT_VAL;
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (pPortEntry->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
        {
            if (PbbMbsmHwSetProviderBridgePortType (u4ContextId,
                                                    i4IfIndex,
                                                    PBB_PROVIDER_INSTANCE_PORT,
                                                    pSlotInfo) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
            if (pPortEntry->unPbbPortData.PipData.u1PipRowStatus == PBB_ACTIVE)
            {
                PBB_DLL_SCAN (&(pPortEntry->unPbbPortData.PipData.VipList),
                              pPipVipMapNode, tPbbIsidPipVipNode *)
                {
                    u2Vip = pPipVipMapNode->unIsidVip.u2Vip;
                    if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                       u2Vip,
                                                       &i4VipIfIndex) ==
                        PBB_FAILURE)
                    {
                        continue;
                    }
                    if (PbbGetVipISid (i4VipIfIndex, &u4Isid) == PBB_FAILURE)
                    {
                        continue;
                    }
                    /* Vip port number is present in this PipVip map */
                    /* Get the Vip entry */
                    if (pbbMbsmSetICompHwServiceInst (u4ContextId,
                                                      i4VipIfIndex,
                                                      u4Isid,
                                                      pSlotInfo) == PBB_FAILURE)
                    {
                        continue;
                    }
                }
                if (PbbMbsmHwSetPipPcpAttributes (u4ContextId,
                                                  u2LocalPort,
                                                  pSlotInfo) == PBB_FAILURE)
                {
                    return PBB_FAILURE;
                }
            }
        }
        else if (((pPortEntry->u1PortType == PBB_CNP_PORTBASED_PORT) ||
                  (pPortEntry->u1PortType == PBB_CNP_STAGGED_PORT) ||
                  (pPortEntry->u1PortType == PBB_CNP_CTAGGED_PORT)) &&
                 pPortEntry->unPbbPortData.CnpData.u4Pisid !=
                 PBB_ISID_INVALID_VALUE)
        {
            if (FsMiPbbMbsmHwSetAllToOneBundlingService (u4ContextId,
                                                         i4IfIndex,
                                                         pPortEntry->
                                                         unPbbPortData.CnpData.
                                                         u4Pisid,
                                                         pSlotInfo) ==
                FNP_FAILURE)
            {

                return PBB_FAILURE;
            }
        }
        else if (pPortEntry->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
        {
            PBB_DLL_SCAN (&(pPortEntry->unPbbPortData.CbpData.IsidList),
                          pIsidVipNode, tPbbIsidPipVipNode *)
            {
                u4Isid = pIsidVipNode->unIsidVip.u4Isid;
                PBB_GET_ISID_PORT_ENTRY (u2LocalPort, u4Isid, pIsidPortEntry);
                if (pIsidPortEntry == NULL)
                {
                    continue;
                }
                if (pIsidPortEntry->u1CBPRowstatus != PBB_ACTIVE)
                {
                    continue;
                }
                if (pbbMbsmSetBCompHwServiceInst (PBB_CURR_CONTEXT_ID (),
                                                  u4Isid,
                                                  i4IfIndex,
                                                  pSlotInfo) == PBB_FAILURE)
                {
                    continue;
                }
            }
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbMbsmHwSetPortInfo
*/
/*                                                                          */
/*    Description        : This is a NPAPI wrapper function to set the 
                           Port information for a particular context 
                           at a slot given by mbsm
*/
/*                                                                          */
/*    Input(s)           : Context Id
                           Slot Information 
                           Isid Vip information
*/
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbMbsmHwInit (tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        if (FsMiPbbMbsmHwNpInit (pSlotInfo) != FNP_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (pSlotInfo);
#endif
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbMbsmCreateCompBridgeEcfmFilter                    */
/*                                                                           */
/* Description        : This function is called to install filters for       */
/*                      copying the control packets to CPU that are from the */
/*                      customer network across the PBBN when Received the   */
/*                      Card Insert Event.                                   */
/*                                                                           */
/* Input(s)           : u4ContextId = Current Context Id                     */
/*                      pSlotInfo = Slot Info                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On Success                            */
/*                      PBB_FAILURE - On failure                            */
/*****************************************************************************/
INT1
PbbMbsmCreateCompBridgeEcfmFilter (UINT4 u4ContextId, tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry EcfmFilterEntry;
    tPbbCtrlPktFilterEntry *pEcfmFilterEntry = &EcfmFilterEntry;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {

        PBB_MEMSET (pEcfmFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));

        pEcfmFilterEntry->i4IsidOffset = -1;
        pEcfmFilterEntry->i4MacOffset = -1;

        pEcfmFilterEntry->u2EtherType = (UINT2) PBB_ECFM_LLC_CFM_TYPE;
        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_1;
        pEcfmFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgMbsmHwCreateControlPktFilter (u4ContextId,
                                                 pEcfmFilterEntry,
                                                 pSlotInfo) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_2;
        if (FsMiBrgMbsmHwCreateControlPktFilter (u4ContextId,
                                                 pEcfmFilterEntry,
                                                 pSlotInfo) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_3;
        if (FsMiBrgMbsmHwCreateControlPktFilter (u4ContextId,
                                                 pEcfmFilterEntry,
                                                 pSlotInfo) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_4;
        if (FsMiBrgMbsmHwCreateControlPktFilter (u4ContextId,
                                                 pEcfmFilterEntry,
                                                 pSlotInfo) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
#endif

    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Creating the PBB ECFM filter successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbMbsmCreateIBCompBridgeEcfmFilter                  */
/*                                                                           */
/* Description        : This function is called to install filters for       */
/*                      copying the control packets to CPU that are from the */
/*                      customer network across the PBBN when the Card Insert*/
/*                      Event will come.                                     */
/*                                                                           */
/* Input(s)           : u4ContextId = Current Context Id                     */
/*                      pSlotInfo = Slot Info                                */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS - On Success                            */
/*                      PBB_FAILURE - On failure                            */
/*****************************************************************************/
INT1
PbbMbsmCreateIBCompBridgeEcfmFilter (UINT4 u4ContextId,
                                     tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry EcfmFilterEntry;
    tPbbCtrlPktFilterEntry *pEcfmFilterEntry = &EcfmFilterEntry;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        PBB_MEMSET (pEcfmFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));

        pEcfmFilterEntry->i4IsidOffset = -1;
        pEcfmFilterEntry->i4MacOffset = -1;

        pEcfmFilterEntry->u2EtherType = (UINT2) PBB_ECFM_LLC_CFM_TYPE;
        pEcfmFilterEntry->i4EtherTypeOffset = PBB_ECFM_LLC_CFM_TYPE_OFFSET_5;
        pEcfmFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgMbsmHwCreateControlPktFilter
            (u4ContextId, pEcfmFilterEntry, pSlotInfo) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Creating the PBB ECFM IB component filter successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name             : PbbBrgMbsmHwCreateAstControlPktFilter      */
/*                                                                           */
/*    Description               : This API is used to program the hardware   */
/*                                to create a filter that will take the      */
/*                                control PDUs coming at PNPs of an IB Bridge*/
/*                                directly to I-Component when the Card      */
/*                                Insert will come                           */
/*                                                                           */
/*    Input(s)                   : u4ContextId = Current Context Id          */
/*                                 pSlotInfo = Slot Info                     */
/*                                                                           */
/*                                                                           */
/*    Output(s)                 : None.                                      */
/*                                                                           */
/*    Returns                   : RST_SUCCESS or PBB_FAILURE.                */
/*                                                                           */
/*****************************************************************************/
INT1
PbbBrgMbsmHwCreateAstControlPktFilter (UINT4 u4ContextId,
                                       tMbsmSlotInfo * pSlotInfo)
{
#ifdef NPAPI_WANTED
    tPbbCtrlPktFilterEntry AstFilterEntry;
    tPbbCtrlPktFilterEntry *pAstFilterEntry = &AstFilterEntry;

    if (PBB_IS_NP_PROGRAMMING_ALLOWED () == PBB_TRUE)
    {
        PBB_MEMSET (pAstFilterEntry, 0, sizeof (tPbbCtrlPktFilterEntry));

        pAstFilterEntry->i4IsidOffset = -1;
        pAstFilterEntry->i4EtherTypeOffset = -1;
        pAstFilterEntry->i4MacOffset = -1;

        if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            pAstFilterEntry->i4MacOffset = PBB_AST_ICOMP_OFFSET;
        }
        else if (PBB_CURR_CONTEXT_PTR ()->u4BridgeMode ==
                 PBB_BCOMPONENT_BRIDGE_MODE)
        {
            pAstFilterEntry->i4MacOffset = PBB_AST_BCOMP_OFFSET;
        }
        pAstFilterEntry->DstMacAddress[0] = 0x01;
        pAstFilterEntry->DstMacAddress[1] = 0x80;
        pAstFilterEntry->DstMacAddress[2] = 0xC2;
        pAstFilterEntry->DstMacAddress[3] = 0x00;
        pAstFilterEntry->DstMacAddress[4] = 0x00;
        pAstFilterEntry->DstMacAddress[5] = 0x08;
        pAstFilterEntry->u1Action = PBB_COPY_TO_CPU;

        if (FsMiBrgMbsmHwCreateControlPktFilter
            (u4ContextId, pAstFilterEntry, pSlotInfo) != FNP_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: FsMiBrgHwCreateControlPktFilter failed \n",
                          __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (pSlotInfo);
#endif
    PBB_TRC_ARG2 (MGMT_TRC | CONTROL_PLANE_TRC,
                  "%s :: %s() :: Creating the PBB STP filter successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

#endif /* End of MBSM specific NPAPI wrappers */
