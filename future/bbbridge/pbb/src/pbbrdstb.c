/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *
 * Description: This file contains stubs for PBB stack compilation.
 *
 *******************************************************************/
#include "pbbinc.h"

/******************************************************************************/
/*  Function Name   : PbbRedHandleBulkUpdateEvent                             */
/*                                                                            */
/*  Description     : It Handles the bulk update event. This event is used    */
/*                    to start the next sub bulk update. So                   */
/*                    PbbRedSendBulkUpdates is triggered.                     */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbRedHandleBulkUpdateEvent (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : PbbRedRegisterWithRM                                 */
/*                                                                           */
/* Description        : Registers PBB module with RM to send and receive     */
/*                      update messages.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then PBB_SUCCESS          */
/*                      Otherwise PBB_FAILURE                                */
/*****************************************************************************/
INT4
PbbRedRegisterWithRM (VOID)
{
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedGetNodeStatus                                  */
/*                                                                           */
/* Description        : This function returns the node status after getting  */
/*                      it from the RM module.                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : UINT4                                                */
/*****************************************************************************/
UINT4
PbbRedGetNodeStatus (VOID)
{
    /* Node active always */
    return PBB_NODE_ACTIVE;
}

/*****************************************************************************/
/* Function Name      : PbbRedInitGlobalInfo                                 */
/*                                                                           */
/* Description        : Initializes redundancy global variables.             */
/*                      This function will get invoked during BOOTUP.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE                          */
/*****************************************************************************/
INT4
PbbRedInitGlobalInfo (VOID)
{
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedDeInitGlobalInfo                               */
/*                                                                           */
/* Description        : DeInitializes redundancy global variables.           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS                                          */
/*****************************************************************************/
INT4
PbbRedDeInitGlobalInfo (VOID)
{
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedInitRedundancyInfo                             */
/*                                                                           */
/* Description        : This function initialises the required information   */
/*                      to support redundancy based on the current node      */
/*                      status. This function will be invoked when the PBB   */
/*                      module is started afresh.                            */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE                            */
/*****************************************************************************/
INT4
PbbRedInitRedundancyInfo (VOID)
{
    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbRedDeInitRedundancyInfo                           */
/*                                                                           */
/* Description        : This function deinitialises all the information      */
/*                      allocated to support redundancy based on the node    */
/*                      status. This function will be invoked when the PBB   */
/*                      module is shutdown.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbRedDeInitRedundancyInfo (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : PbbRedSyncPortOperStatus                             */
/*                                                                           */
/* Description        : This function will send port oper status change to   */
/*                      standby node.                                        */
/*                                                                           */
/* Input(s)           : u4Context - Virtual ContextId                        */
/*                      i4IfIndex - Inerface Index                           */
/*                    : u1Status - Port Oper status.                         */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE.                           */
/*****************************************************************************/
INT4
PbbRedSyncPortOperStatus (UINT4 u4Context, INT4 i4IfIndex, UINT1 u1Status)
{
    UNUSED_PARAM (u4Context);
    UNUSED_PARAM (i4IfIndex);
    UNUSED_PARAM (u1Status);
    return PBB_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedInitBufferPool                                    */
/*                                                                            */
/*  Description     : This function initilizes the mempool for the buffer     */
/*                    entries                                                 */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : PBB_SUCCESS / PBB_FAILURE                               */
/******************************************************************************/
INT4
PbbRedInitBufferPool (VOID)
{
    return PBB_SUCCESS;
}

/******************************************************************************/
/*                                                                            */
/*  Function Name   : PbbRedDeInitBufferPool                                  */
/*                                                                            */
/*  Description     : This function Deinitilizes the mempool for the buffer   */
/*                    entries                                                 */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : PBB_SUCCESS / PBB_FAILURE                               */
/******************************************************************************/
INT4
PbbRedDeInitBufferPool (VOID)
{
    return PBB_SUCCESS;
}
