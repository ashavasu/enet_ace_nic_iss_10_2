/********************************************************************
           
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs1ahlw.c,v 1.29 2014/03/19 13:36:11 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "pbbinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbBackboneEdgeBridgeAddress
Input       :  The Indices

The Object 
retValFsPbbBackboneEdgeBridgeAddress
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbBackboneEdgeBridgeAddress (tMacAddr *
                                      pRetValFsPbbBackboneEdgeBridgeAddress)
{
    tMacAddr            SwitchMac;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_MEMSET (SwitchMac, PBB_INIT_VAL, sizeof (tMacAddr));
    if (PbbGetBackboneEdgeBridgeAddress (SwitchMac) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    PBB_MEMCPY (pRetValFsPbbBackboneEdgeBridgeAddress,
                SwitchMac, sizeof (tMacAddr));
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbBackboneEdgeBridgeName
Input       :  The Indices

The Object 
retValFsPbbBackboneEdgeBridgeName
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbBackboneEdgeBridgeName (tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsPbbBackboneEdgeBridgeName)
{

    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        pRetValFsPbbBackboneEdgeBridgeName->pu1_OctetList[0] = PBB_INIT_VAL;
        pRetValFsPbbBackboneEdgeBridgeName->i4_Length = PBB_INIT_VAL;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }

    if (PbbGetBackboneEdgeBridgeName (pRetValFsPbbBackboneEdgeBridgeName)
        != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbNumberOfIComponents
Input       :  The Indices

The Object 
retValFsPbbNumberOfIComponents
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbNumberOfIComponents (UINT4 *pu4RetValFsPbbNumberOfIComponents)
{
    UINT4               u4NumofIComp = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PbbGetNumberOfIComponents (&u4NumofIComp) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsPbbNumberOfIComponents = u4NumofIComp;

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbNumberOfBComponents
Input       :  The Indices

The Object 
retValFsPbbNumberOfBComponents
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbNumberOfBComponents (UINT4 *pu4RetValFsPbbNumberOfBComponents)
{
    UINT4               u4NumofBComp = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PbbGetNumberOfBComponents (&u4NumofBComp) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsPbbNumberOfBComponents = u4NumofBComp;
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbNumberOfBebPorts
Input       :  The Indices

The Object 
retValFsPbbNumberOfBebPorts
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbNumberOfBebPorts (UINT4 *pu4RetValFsPbbNumberOfBebPorts)
{
    INT4                i4NumofBebPorts = PBB_INIT_VAL;
    i4NumofBebPorts = PbbCfaGetNumberOfBebPorts ();

    *pu4RetValFsPbbNumberOfBebPorts = i4NumofBebPorts;
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbNextAvailablePipIfIndex
Input       :  The Indices

The Object 
retValFsPbbNextAvailablePipIfIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbNextAvailablePipIfIndex (INT4 *pi4RetValFsPbbNextAvailablePipIfIndex)
{
    if (PbbCfaGetFreeInterfaceIndex (pi4RetValFsPbbNextAvailablePipIfIndex,
                                     CFA_ENET) == PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
    }
    else
    {
        *pi4RetValFsPbbNextAvailablePipIfIndex = 0;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbBackboneEdgeBridgeName
Input       :  The Indices

The Object 
setValFsPbbBackboneEdgeBridgeName
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbBackboneEdgeBridgeName (tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsPbbBackboneEdgeBridgeName)
{
    if (PbbSetBackboneEdgeBridgeName (pSetValFsPbbBackboneEdgeBridgeName)
        != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_BACKBONE_EDGE_BRIDGE_NAME_INVALID);
        return SNMP_FAILURE;
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbBackboneEdgeBridgeName
Input       :  The Indices

The Object 
testValFsPbbBackboneEdgeBridgeName
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbBackboneEdgeBridgeName (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsPbbBackboneEdgeBridgeName)
{
    /* Check the System Status */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }

    if ((pTestValFsPbbBackboneEdgeBridgeName->i4_Length >
         PBB_MAX_BRIDGE_NAME) ||
        (pTestValFsPbbBackboneEdgeBridgeName->i4_Length <= PBB_INIT_VAL))
    {
        CLI_SET_ERR (CLI_PBB_BACKBONE_EDGE_BRIDGE_NAME_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pTestValFsPbbBackboneEdgeBridgeName->pu1_OctetList == NULL)
    {
        CLI_SET_ERR (CLI_PBB_BACKBONE_EDGE_BRIDGE_NAME_INVALID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbBackboneEdgeBridgeName
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbBackboneEdgeBridgeName (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbVipTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbVipTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbVipTable (INT4
                                       i4Fsdot1ahContextId, INT4 i4IfMainIndex)
{
    if (PbbValidateContextId (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PbbValidateVip (i4IfMainIndex) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbVipTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbVipTable (INT4 *pi4Fsdot1ahContextId, INT4 *pi4IfMainIndex)
{
    UINT4               u4Vip = PBB_INIT_VAL;
    UINT4               u4Context = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstVip (&u4Vip) == PBB_SUCCESS)
                {
                    *pi4Fsdot1ahContextId = u4Context;
                    *pi4IfMainIndex = u4Vip;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbVipTable
Input       :  The Indices
Fsdot1ahContextId
nextFsdot1ahContextId
IfMainIndex
nextIfMainIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbVipTable (INT4 i4Fsdot1ahContextId,
                              INT4 *pi4NextFsdot1ahContextId,
                              INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    INT4                i4ifIndex = PBB_INIT_VAL;
    UINT4               u4VipRowStatus = PBB_INIT_VAL;
    UINT2               u2NextVip = PBB_INIT_VAL;

    if (PbbSelectContext (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    while (PbbGetNextVip
           (i4Fsdot1ahContextId, &u4Context, i4IfMainIndex,
            &u2NextVip) == PBB_SUCCESS)
    {
        if (PbbVcmGetIfIndexFromLocalPort (u4Context,
                                           u2NextVip,
                                           &i4ifIndex) != PBB_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (PbbGetVipRowStatus (i4ifIndex, &u4VipRowStatus) == PBB_FAILURE)
        {
            return SNMP_FAILURE;
        }
        if (u4VipRowStatus == PBB_INVALID_ROWSTATUS)
        {
            i4Fsdot1ahContextId = u4Context;
            i4IfMainIndex = i4ifIndex;
            continue;
        }
        *pi4NextFsdot1ahContextId = u4Context;
        *pi4NextIfMainIndex = i4ifIndex;
        PBB_CURR_CONTEXT_PTR () = NULL;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbVipPipIfIndex
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbVipPipIfIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbVipPipIfIndex (INT4 i4Fsdot1ahContextId,
                          INT4 i4IfMainIndex, INT4 *pi4RetValFsPbbVipPipIfIndex)
{
    INT4                i4Pip = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;

    }
    if (PbbGetVipPipIfIndex (i4IfMainIndex, &i4Pip) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsPbbVipPipIfIndex = i4Pip;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbVipISid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbVipISid
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbVipISid (INT4 i4Fsdot1ahContextId,
                    INT4 i4IfMainIndex, UINT4 *pu4RetValFsPbbVipISid)
{
    UINT4               u4Isid = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;

    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;

    }

    if (PbbGetVipISid (i4IfMainIndex, &u4Isid) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    *pu4RetValFsPbbVipISid = u4Isid;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbVipDefaultDstBMAC
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbVipDefaultDstBMAC
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbVipDefaultDstBMAC (INT4 i4Fsdot1ahContextId,
                              INT4 i4IfMainIndex,
                              tMacAddr * pRetValFsPbbVipDefaultDstBMAC)
{
    tMacAddr            VipDefaultDstBMAC;
    PBB_MEMSET (VipDefaultDstBMAC, PBB_INIT_VAL, sizeof (tMacAddr));
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;

    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;

    }

    if (PbbGetVipDefaultDstBMAC (i4IfMainIndex, &VipDefaultDstBMAC) !=
        PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_MEMCPY (pRetValFsPbbVipDefaultDstBMAC,
                VipDefaultDstBMAC, sizeof (tMacAddr));
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbVipType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbVipType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbVipType (INT4 i4Fsdot1ahContextId,
                    INT4 i4IfMainIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValFsPbbVipType)
{
    UINT4               u4RowStatus = PBB_INIT_VAL;
    UINT1               u1VipType = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;

    }
    if (PbbGetVipRowStatus (i4IfMainIndex, &u4RowStatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (u4RowStatus == PBB_INVALID_ROWSTATUS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    if (PbbGetVipType (i4IfMainIndex, &u1VipType) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    pRetValFsPbbVipType->pu1_OctetList[0] = u1VipType;
    pRetValFsPbbVipType->i4_Length = sizeof (UINT1);
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbVipRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbVipRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbVipRowStatus (INT4 i4Fsdot1ahContextId,
                         INT4 i4IfMainIndex, INT4 *pi4RetValFsPbbVipRowStatus)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;

    }

    if (PbbGetVipRowStatus (i4IfMainIndex, &u4Rowstatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_INVALID_ROWSTATUS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;

    }
    *pi4RetValFsPbbVipRowStatus = u4Rowstatus;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbVipISid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
setValFsPbbVipISid
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbVipISid (INT4 i4Fsdot1ahContextId,
                    INT4 i4IfMainIndex, UINT4 u4SetValFsPbbVipISid)
{
    UINT4               u4Isid = PBB_INIT_VAL;
    if (nmhGetFsPbbVipISid
        (i4Fsdot1ahContextId, i4IfMainIndex, &u4Isid) == SNMP_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (u4Isid == u4SetValFsPbbVipISid)
    {
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;

    }
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    if (PbbSetPbbVipISid (i4IfMainIndex, u4SetValFsPbbVipISid) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return SNMP_FAILURE;
    }
    if (PbbCreateIsidNodeFromVip (i4IfMainIndex, u4SetValFsPbbVipISid) !=
        PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbVipDefaultDstBMAC
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
setValFsPbbVipDefaultDstBMAC
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbVipDefaultDstBMAC (INT4 i4Fsdot1ahContextId,
                              INT4 i4IfMainIndex,
                              tMacAddr SetValFsPbbVipDefaultDstBMAC)
{
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    if (PbbSetVipDefaultDstBMAC (i4IfMainIndex,
                                 SetValFsPbbVipDefaultDstBMAC) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbVipType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
setValFsPbbVipType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbVipType (INT4 i4Fsdot1ahContextId,
                    INT4 i4IfMainIndex,
                    tSNMP_OCTET_STRING_TYPE * pSetValFsPbbVipType)
{
    UINT1               u1VipType = PBB_INIT_VAL;
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    u1VipType = pSetValFsPbbVipType->pu1_OctetList[0];
    if (PbbSetPbbVipType (i4IfMainIndex, u1VipType) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbVipRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
setValFsPbbVipRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbVipRowStatus (INT4 i4Fsdot1ahContextId,
                         INT4 i4IfMainIndex, INT4 i4SetValFsPbbVipRowStatus)
{
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    switch (i4SetValFsPbbVipRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
        {
            PBB_PERF_MARK_START_TIME ();
            if (PbbValidateVip (i4IfMainIndex) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }

            if (PbbSetVipRowstatus (i4IfMainIndex,
                                    i4SetValFsPbbVipRowStatus) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }

        }
            PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
            break;
        case PBB_ACTIVE:
        {
            PBB_PERF_MARK_START_TIME ();
            if (PbbSetVipRowstatus (i4IfMainIndex,
                                    i4SetValFsPbbVipRowStatus) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbGetVipISid (i4IfMainIndex, &u4Isid) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (u4Isid != PBB_ISID_INVALID_VALUE)
            {

                if (PbbSetICompHwServiceInst
                    (i4Fsdot1ahContextId, i4IfMainIndex, u4Isid) != PBB_SUCCESS)
                {
                    CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }
            }
            L2IwfCreateIsid (i4Fsdot1ahContextId, u4Isid);
            PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
            PBB_PERF_PRINT_VLANTIME (gu4VlanIsidTimeTaken);
            PBB_PERF_PRINT_TIME (gu4ISIDCreateTimeTaken);
            PBB_PERF_PRINT_TOTTIME (gu4ISIDCreateTimeTaken,
                                    gu4VlanIsidTimeTaken);

        }
            break;
        case PBB_DESTROY:
        {
            PBB_PERF_MARK_START_TIME ();
            if (PbbGetVipISid (i4IfMainIndex, &u4Isid) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort)
                != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbSetVipPipRowStatus (i4Fsdot1ahContextId,
                                       u2LocalPort,
                                       i4SetValFsPbbVipRowStatus) !=
                PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbGetVipIsidonLocal (u2LocalPort, &u4Isid) == PBB_SUCCESS)
            {
                if (u4Isid != PBB_ISID_INVALID_VALUE)
                {
                    if (PbbDeleteCnpNodeDll (i4IfMainIndex) != PBB_SUCCESS)
                    {
                        PbbReleaseContext ();
                        return SNMP_FAILURE;

                    }
                    if (PbbDeleteIsidNodeFromVip (u4Isid) != PBB_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                }
            }
            if (PbbSetVipRowstatus (i4IfMainIndex,
                                    PBB_INVALID_ROWSTATUS) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }

            if (PbbDelVipData (i4IfMainIndex) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }

            if (PbbDelICompHwServiceInst (i4Fsdot1ahContextId,
                                          i4IfMainIndex) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }

            PbbL2IwfDeleteIsid (i4Fsdot1ahContextId, u4Isid);

            PBB_PERF_MARK_END_TIME (gu4ISIDDeleteTimeTaken);
        }
            break;
        case PBB_NOT_IN_SERVICE:
        {
            if (PbbSetVipRowstatus (i4IfMainIndex,
                                    i4SetValFsPbbVipRowStatus) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbDelICompHwServiceInst (i4Fsdot1ahContextId,
                                          i4IfMainIndex) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
        }
            break;
        default:
            return SNMP_FAILURE;
    }

    /* Set the Vip Oper Status */
    if ((i4SetValFsPbbVipRowStatus == PBB_NOT_IN_SERVICE) ||
        (i4SetValFsPbbVipRowStatus == PBB_ACTIVE))
    {
        PbbSetVipOperStatus (i4Fsdot1ahContextId, i4IfMainIndex, PBB_INIT_VAL,
                             PBB_INIT_VAL);
    }

    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbVipISid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
testValFsPbbVipISid
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbVipISid (UINT4 *pu4ErrorCode,
                       INT4 i4Fsdot1ahContextId,
                       INT4 i4IfMainIndex, UINT4 u4TestValFsPbbVipISid)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    INT4                i4PipIfIndex = PBB_INIT_VAL;
    UINT4               u4NumOfPortsPerIsidPresent = PBB_INIT_VAL;
    UINT4               u4MaxPortsPerIsid = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetVipRowStatus (i4IfMainIndex, &u4Rowstatus) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_ACTIVE)
    {
        CLI_SET_ERR (CLI_PBB_ROWSTATUS_ACTIVE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (nmhGetFsPbbVipISid
        (i4Fsdot1ahContextId, i4IfMainIndex, &u4Isid) == SNMP_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (u4Isid == u4TestValFsPbbVipISid)
    {
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;

    }
    if (PbbSelectContext (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (PbbValidateIsid (u4TestValFsPbbVipISid) == PBB_SUCCESS)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4TestValFsPbbVipISid <= PBB_MIN_ISID_VALUE) ||
        (u4TestValFsPbbVipISid > PBB_MAX_ISID_VALUE))
    {
        CLI_SET_ERR (CLI_PBB_INVALID_ISID_VALUE);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbGetVipPipIfIndex (i4IfMainIndex, &i4PipIfIndex) == PBB_SUCCESS)
    {
        if (PbbGetMaxNumPortPerIsid (&u4NumOfPortsPerIsidPresent,
                                     u4TestValFsPbbVipISid,
                                     i4Fsdot1ahContextId) != PBB_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (PbbGetMaxPortsPerISID ((INT4 *) &u4MaxPortsPerIsid) != PBB_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        if (u4NumOfPortsPerIsidPresent >= u4MaxPortsPerIsid)
        {
            CLI_SET_ERR (CLI_PBB_PORT_PER_ISID_EXCEED);
            PbbReleaseContext ();
            return SNMP_FAILURE;
        }
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbVipDefaultDstBMAC
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
testValFsPbbVipDefaultDstBMAC
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbVipDefaultDstBMAC (UINT4 *pu4ErrorCode,
                                 INT4 i4Fsdot1ahContextId,
                                 INT4 i4IfMainIndex,
                                 tMacAddr TestValFsPbbVipDefaultDstBMAC)
{
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    PBB_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (PBB_IS_BCASTADDR (TestValFsPbbVipDefaultDstBMAC) == PBB_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_MACADDR);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PbbGetVipRowStatus (i4IfMainIndex, &u4Rowstatus) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_ACTIVE)
    {
        CLI_SET_ERR (CLI_PBB_ROWSTATUS_ACTIVE);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbVipType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
testValFsPbbVipType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbVipType (UINT4 *pu4ErrorCode,
                       INT4 i4Fsdot1ahContextId,
                       INT4 i4IfMainIndex,
                       tSNMP_OCTET_STRING_TYPE * pTestValFsPbbVipType)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetVipRowStatus (i4IfMainIndex, &u4Rowstatus) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_INVALID_ROWSTATUS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_ACTIVE)
    {
        CLI_SET_ERR (CLI_PBB_SERVICE_TYPE_ERR);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsPbbVipType->i4_Length == PBB_INIT_VAL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIPTYPE);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((pTestValFsPbbVipType->pu1_OctetList[0] != PBB_VIP_TYPE_BOTH) &&
        (pTestValFsPbbVipType->pu1_OctetList[0] != PBB_VIP_TYPE_EGRESS) &&
        (pTestValFsPbbVipType->pu1_OctetList[0] != PBB_VIP_TYPE_INGRESS))
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIPTYPE);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbVipRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
testValFsPbbVipRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbVipRowStatus (UINT4 *pu4ErrorCode,
                            INT4 i4Fsdot1ahContextId,
                            INT4 i4IfMainIndex, INT4 i4TestValFsPbbVipRowStatus)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsPbbVipTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsPbbVipRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
        {
            PBB_PERF_MARK_START_TIME ();

            if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_COPMTYPE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                PbbReleaseContext ();
                PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
                return SNMP_FAILURE;
            }

            if (PbbGetVipRowStatus (i4IfMainIndex, &u4Rowstatus) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
                return SNMP_FAILURE;
            }

            if (u4Rowstatus != PBB_INVALID_ROWSTATUS)
            {
                CLI_SET_ERR (CLI_PBB_VALID_ROWSTATUS);
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
                return SNMP_FAILURE;
            }
            PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
        }
            break;
        case PBB_ACTIVE:
        {
            PBB_PERF_MARK_START_TIME ();
            if (PbbGetVipRowStatus (i4IfMainIndex, &u4Rowstatus) != PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
                return SNMP_FAILURE;
            }
            if (u4Rowstatus == PBB_INVALID_ROWSTATUS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
                return SNMP_FAILURE;
            }
            PBB_PERF_MARK_END_TIME (gu4ISIDCreateTimeTaken);
        }
            break;
        case PBB_NOT_IN_SERVICE:
        case PBB_DESTROY:
            PBB_PERF_MARK_START_TIME ();
            PBB_PERF_RESET_TIME (gu4ISIDDeleteTimeTaken);
            {
                PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                              __FILE__, PBB_FUNCTION_NAME);
                PBB_PERF_MARK_END_TIME (gu4ISIDDeleteTimeTaken);
                return SNMP_SUCCESS;

            }
            break;
        case PBB_CREATE_AND_GO:
        {
            CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS_VALUE);
            PbbReleaseContext ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
            break;
        default:
            CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS_VALUE);
            PbbReleaseContext ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbVipTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbVipTable (UINT4 *pu4ErrorCode,
                       tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbISidToVipTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbISidToVipTable
Input       :  The Indices
FsPbbISidToVipISid
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbISidToVipTable (UINT4 u4FsPbbISidToVipISid)
{
    UINT4               u4Context = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbValidateIsid (u4FsPbbISidToVipISid) == PBB_SUCCESS)
                {
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;

                }

            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbISidToVipTable
Input       :  The Indices
FsPbbISidToVipISid
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbISidToVipTable (UINT4 *pu4FsPbbISidToVipISid)
{
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4Context = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                return (nmhGetNextIndexFsPbbISidToVipTable (u4Isid,
                                                            pu4FsPbbISidToVipISid));
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbISidToVipTable
Input       :  The Indices
FsPbbISidToVipISid
nextFsPbbISidToVipISid
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbISidToVipTable (UINT4 u4FsPbbISidToVipISid,
                                    UINT4 *pu4NextFsPbbISidToVipISid)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT4               u4CurrIsid = PBB_INIT_VAL;
    UINT4               u4NextIsid = PBB_INIT_VAL;
    u4CurrIsid = u4FsPbbISidToVipISid;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetNextIsid (u4CurrIsid, &u4NextIsid) == PBB_SUCCESS)
                {
                    *pu4NextFsPbbISidToVipISid = u4NextIsid;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
                PBB_CURR_CONTEXT_PTR () = NULL;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbISidToVipComponentId
Input       :  The Indices
FsPbbISidToVipISid

The Object 
retValFsPbbISidToVipComponentId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbISidToVipComponentId (UINT4 u4FsPbbISidToVipISid,
                                 UINT4 *pu4RetValFsPbbISidToVipComponentId)
{
    UINT4               u4Context = PBB_INIT_VAL;
    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            if (PbbValidateIsid (u4FsPbbISidToVipISid) == PBB_SUCCESS)
            {
                *pu4RetValFsPbbISidToVipComponentId = u4Context;
                PbbReleaseContext ();
                PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                              __FILE__, PBB_FUNCTION_NAME);
                return SNMP_SUCCESS;
            }
        }
    }
    PbbReleaseContext ();
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFsPbbISidToVipPort
Input       :  The Indices
FsPbbISidToVipISid

The Object 
retValFsPbbISidToVipPort
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbISidToVipPort (UINT4 u4FsPbbISidToVipISid,
                          INT4 *pi4RetValFsPbbISidToVipPort)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    u4Isid = u4FsPbbISidToVipISid;
    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
        if (PBB_CURR_CONTEXT_PTR () != NULL)
        {
            if (PbbGetVipForIsid (u4Isid, &u2Vip) == PBB_SUCCESS)
            {
                if (PbbVcmGetIfIndexFromLocalPort (u4Context,
                                                   u2Vip,
                                                   &i4IfIndex) != PBB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }

                *pi4RetValFsPbbISidToVipPort = i4IfIndex;
                PbbReleaseContext ();
                PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                              __FILE__, PBB_FUNCTION_NAME);
                return SNMP_SUCCESS;
            }
        }
    }
    PbbReleaseContext ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsPbbPipTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbPipTable
Input       :  The Indices
FsPbbPipIfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbPipTable (INT4 i4FsPbbPipIfIndex)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate the System Initialized status */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate that the local port exists for interface index */
    i4retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate that the Pip port exists */
    i4retVal = PbbValidatePipPortExists (u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbPipTable
Input       :  The Indices
FsPbbPipIfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbPipTable (INT4 *pi4FsPbbPipIfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {

                if (PbbGetFirstPbbPipIfIndex (pi4FsPbbPipIfIndex) ==
                    PBB_SUCCESS)
                {
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }

            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbPipTable
Input       :  The Indices
FsPbbPipIfIndex
nextFsPbbPipIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbPipTable (INT4 i4FsPbbPipIfIndex,
                              INT4 *pi4NextFsPbbPipIfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT1               u1Flag = PBB_TRUE;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (u1Flag == PBB_TRUE &&
                    PbbGetPipIfIndex (i4FsPbbPipIfIndex) == PBB_SUCCESS)
                {
                    if (PbbGetNextPbbPipIfIndex
                        (i4FsPbbPipIfIndex,
                         pi4NextFsPbbPipIfIndex) == PBB_SUCCESS)
                    {
                        PBB_TRC_ARG2 (MGMT_TRC,
                                      "%s :: %s() ::  returns successfully\n",
                                      __FILE__, PBB_FUNCTION_NAME);
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        u1Flag = PBB_FALSE;
                    }
                }
                else if (u1Flag == PBB_FALSE)
                {
                    if (PbbGetFirstPbbPipIfIndex (pi4NextFsPbbPipIfIndex) ==
                        PBB_SUCCESS)
                    {
                        PBB_TRC_ARG2 (MGMT_TRC,
                                      "%s :: %s() ::  returns successfully\n",
                                      __FILE__, PBB_FUNCTION_NAME);
                        return SNMP_SUCCESS;
                    }
                }

            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbPipBMACAddress
Input       :  The Indices
FsPbbPipIfIndex

The Object 
retValFsPbbPipBMACAddress
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPipBMACAddress (INT4 i4FsPbbPipIfIndex,
                           tMacAddr * pRetValFsPbbPipBMACAddress)
{
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbPipTable (i4FsPbbPipIfIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the B MAC address value */
    i4retVal = PbbCfaGetIfHwAddr (i4FsPbbPipIfIndex,
                                  pRetValFsPbbPipBMACAddress);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbPipName
Input       :  The Indices
FsPbbPipIfIndex

The Object 
retValFsPbbPipName
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPipName (INT4 i4FsPbbPipIfIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValFsPbbPipName)
{
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbPipTable (i4FsPbbPipIfIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the Pip Name value */
    i4retVal = PbbCfaGetIfPipName (i4FsPbbPipIfIndex, pRetValFsPbbPipName);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbPipIComponentId
Input       :  The Indices
FsPbbPipIfIndex

The Object 
retValFsPbbPipIComponentId
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPipIComponentId (INT4 i4FsPbbPipIfIndex,
                            UINT4 *pu4RetValFsPbbPipIComponentId)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbPipTable (i4FsPbbPipIfIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate that the local port exists for interface index */
    i4retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsPbbPipIComponentId = u4ContextId;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbPipRowStatus
Input       :  The Indices
FsPbbPipIfIndex

The Object 
retValFsPbbPipRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPipRowStatus (INT4 i4FsPbbPipIfIndex,
                         INT4 *pi4RetValFsPbbPipRowStatus)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbPipTable (i4FsPbbPipIfIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate that the local port exists for interface index */
    i4retVal = PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                                &u4ContextId, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the Pip Row Status */
    i4retVal = PbbGetPipRowStatus (u2LocalPort, pi4RetValFsPbbPipRowStatus);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbPipBMACAddress
Input       :  The Indices
FsPbbPipIfIndex

The Object 
setValFsPbbPipBMACAddress
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPipBMACAddress (INT4 i4FsPbbPipIfIndex,
                           tMacAddr SetValFsPbbPipBMACAddress)
{
    INT4                i4retVal = PbbCfaSetIfHwAddr (i4FsPbbPipIfIndex,
                                                      SetValFsPbbPipBMACAddress,
                                                      sizeof (tMacAddr));
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbPipName
Input       :  The Indices
FsPbbPipIfIndex

The Object 
setValFsPbbPipName
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPipName (INT4 i4FsPbbPipIfIndex,
                    tSNMP_OCTET_STRING_TYPE * pSetValFsPbbPipName)
{
    INT4                i4retVal = PbbCfaSetIfPipName (i4FsPbbPipIfIndex,
                                                       pSetValFsPbbPipName);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbPipRowStatus
Input       :  The Indices
FsPbbPipIfIndex

The Object 
setValFsPbbPipRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbPipRowStatus (INT4 i4FsPbbPipIfIndex, INT4 i4SetValFsPbbPipRowStatus)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    /* Get the local port number for the interface index */
    i4retVal =
        PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex, &u4ContextId,
                                         &u2LocalPort);
    if (i4retVal != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    i4retVal = PbbSetPipRowStatus (u2LocalPort, i4SetValFsPbbPipRowStatus);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbPipBMACAddress
Input       :  The Indices
FsPbbPipIfIndex

The Object 
testValFsPbbPipBMACAddress
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPipBMACAddress (UINT4 *pu4ErrorCode,
                              INT4 i4FsPbbPipIfIndex,
                              tMacAddr TestValFsPbbPipBMACAddress)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbPipTable (i4FsPbbPipIfIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Get the local port number for the interface index */
    i4retVal =
        PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex, &u4ContextId,
                                         &u2LocalPort);
    if (i4retVal != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate that the row status is not active  */
    i4retVal = PbbIsPipRowStatusNotActive (u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    /* Test the Mac Address of the pip */
    i4retVal = PbbCfaTestIfHwAddr (i4FsPbbPipIfIndex,
                                   TestValFsPbbPipBMACAddress,
                                   sizeof (tMacAddr));
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_MACADDR_INVALID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbPipName
Input       :  The Indices
FsPbbPipIfIndex

The Object 
testValFsPbbPipName
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPipName (UINT4 *pu4ErrorCode,
                       INT4 i4FsPbbPipIfIndex,
                       tSNMP_OCTET_STRING_TYPE * pTestValFsPbbPipName)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbPipTable (i4FsPbbPipIfIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Get the local port number for the interface index */
    i4retVal =
        PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex, &u4ContextId,
                                         &u2LocalPort);
    if (i4retVal != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    /* Test the value of Pip Name */
    i4retVal = PbbCfaTestIfPipName (i4FsPbbPipIfIndex, pTestValFsPbbPipName);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_PIPNAME_INVALID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbPipRowStatus
Input       :  The Indices
FsPbbPipIfIndex

The Object 
testValFsPbbPipRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbPipRowStatus (UINT4 *pu4ErrorCode,
                            INT4 i4FsPbbPipIfIndex,
                            INT4 i4TestValFsPbbPipRowStatus)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    INT4                i4retVal =
        nmhValidateIndexInstanceFsPbbPipTable (i4FsPbbPipIfIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Get the local port number for the interface index */
    i4retVal =
        PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex, &u4ContextId,
                                         &u2LocalPort);
    if (i4retVal != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate the row status value */
    if (PBB_VALID_ROW_STATUS (i4TestValFsPbbPipRowStatus) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS_VALUE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Select the context */
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Test further on row status  */
    i4retVal = PbbTestPipRowStatus (pu4ErrorCode,
                                    u2LocalPort, i4TestValFsPbbPipRowStatus);
    if (i4retVal == PBB_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbPipTable
Input       :  The Indices
FsPbbPipIfIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbPipTable (UINT4 *pu4ErrorCode,
                       tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbVipToPipMappingTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbVipToPipMappingTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbVipToPipMappingTable (INT4
                                                   i4Fsdot1ahContextId,
                                                   INT4 i4IfMainIndex)
{
    INT4                i4retVal = PBB_INIT_VAL;
    /* Validate the System Initialized status */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate the value of Component id */
    i4retVal = PbbValidateContextId (i4Fsdot1ahContextId);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate for the Bridge mode to be PBB */
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Validate the value of VIP */
    i4retVal = PbbValidateVip (i4IfMainIndex);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbVipToPipMappingTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbVipToPipMappingTable (INT4
                                           *pi4Fsdot1ahContextId,
                                           INT4 *pi4IfMainIndex)
{
    UINT4               u4Vip = PBB_INIT_VAL;
    UINT4               u4Context = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstVip (&u4Vip) == PBB_SUCCESS)
                {
                    *pi4Fsdot1ahContextId = u4Context;
                    *pi4IfMainIndex = u4Vip;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbVipToPipMappingTable
Input       :  The Indices
Fsdot1ahContextId
nextFsdot1ahContextId
IfMainIndex
nextIfMainIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbVipToPipMappingTable (INT4
                                          i4Fsdot1ahContextId,
                                          INT4
                                          *pi4NextFsdot1ahContextId,
                                          INT4 i4IfMainIndex,
                                          INT4 *pi4NextIfMainIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    INT4                i4ifIndex = PBB_INIT_VAL;
    UINT2               u2NextVip = PBB_INIT_VAL;

    if (PbbSelectContext (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PbbGetNextVip
        (i4Fsdot1ahContextId, &u4Context, i4IfMainIndex,
         &u2NextVip) == PBB_SUCCESS)
    {
        if (PbbVcmGetIfIndexFromLocalPort (u4Context,
                                           u2NextVip,
                                           &i4ifIndex) != PBB_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pi4NextFsdot1ahContextId = u4Context;
        *pi4NextIfMainIndex = i4ifIndex;
        PBB_CURR_CONTEXT_PTR () = NULL;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbVipToPipMappingPipIfIndex
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbVipToPipMappingPipIfIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbVipToPipMappingPipIfIndex (INT4 i4Fsdot1ahContextId,
                                      INT4 i4IfMainIndex,
                                      INT4
                                      *pi4RetValFsPbbVipToPipMappingPipIfIndex)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate the indices */
    i4retVal =
        nmhValidateIndexInstanceFsPbbVipToPipMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i4retVal = PbbGetVipPipInterfaceIndex (i4Fsdot1ahContextId,
                                           u2LocalPort,
                                           pi4RetValFsPbbVipToPipMappingPipIfIndex);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbVipToPipMappingStorageType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbVipToPipMappingStorageType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbVipToPipMappingStorageType (INT4
                                       i4Fsdot1ahContextId,
                                       INT4 i4IfMainIndex,
                                       INT4
                                       *pi4RetValFsPbbVipToPipMappingStorageType)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate the indices */
    i4retVal =
        nmhValidateIndexInstanceFsPbbVipToPipMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Get the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Get the Vip Pip Storage type */
    i4retVal = PbbGetVipPipStorageType (i4Fsdot1ahContextId,
                                        u2LocalPort,
                                        (UINT4 *)
                                        pi4RetValFsPbbVipToPipMappingStorageType);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetFsPbbVipToPipMappingRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbVipToPipMappingRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbVipToPipMappingRowStatus (INT4 i4Fsdot1ahContextId,
                                     INT4 i4IfMainIndex,
                                     INT4
                                     *pi4RetValFsPbbVipToPipMappingRowStatus)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate the indices */
    i4retVal =
        nmhValidateIndexInstanceFsPbbVipToPipMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i4retVal = PbbGetVipPipRowStatus (i4Fsdot1ahContextId,
                                      u2LocalPort,
                                      (UINT4 *)
                                      pi4RetValFsPbbVipToPipMappingRowStatus);
    if (i4retVal != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (*pi4RetValFsPbbVipToPipMappingRowStatus == PBB_INVALID_ROWSTATUS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbVipToPipMappingPipIfIndex
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
setValFsPbbVipToPipMappingPipIfIndex
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbVipToPipMappingPipIfIndex (INT4 i4Fsdot1ahContextId,
                                      INT4 i4IfMainIndex,
                                      INT4
                                      i4SetValFsPbbVipToPipMappingPipIfIndex)
{
    UINT4               u4Isid = PBB_INIT_VAL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Get the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i4retVal =
        PbbSetVipPipInterfaceIndex (i4Fsdot1ahContextId, u4Isid,
                                    u2LocalPort,
                                    i4SetValFsPbbVipToPipMappingPipIfIndex);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsPbbVipToPipMappingStorageType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
setValFsPbbVipToPipMappingStorageType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbVipToPipMappingStorageType (INT4
                                       i4Fsdot1ahContextId,
                                       INT4 i4IfMainIndex,
                                       INT4
                                       i4SetValFsPbbVipToPipMappingStorageType)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    /* Get the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i4retVal = PbbSetVipPipStorageType (i4Fsdot1ahContextId,
                                        u2LocalPort,
                                        (UINT1)
                                        i4SetValFsPbbVipToPipMappingStorageType);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetFsPbbVipToPipMappingRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
setValFsPbbVipToPipMappingRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbVipToPipMappingRowStatus (INT4 i4Fsdot1ahContextId,
                                     INT4 i4IfMainIndex,
                                     INT4 i4SetValFsPbbVipToPipMappingRowStatus)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return SNMP_FAILURE;
    }
    /* Get the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    i4retVal = PbbSetVipPipRowStatus (i4Fsdot1ahContextId,
                                      u2LocalPort,
                                      i4SetValFsPbbVipToPipMappingRowStatus);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbVipToPipMappingPipIfIndex
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
testValFsPbbVipToPipMappingPipIfIndex
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbVipToPipMappingPipIfIndex (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4Fsdot1ahContextId,
                                         INT4 i4IfMainIndex,
                                         INT4
                                         i4TestValFsPbbVipToPipMappingPipIfIndex)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT2               u2LocalPortVip = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    /* Validate the indices */
    i4retVal =
        nmhValidateIndexInstanceFsPbbVipToPipMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the value of Vip from interface index */

    i4retVal = PbbVcmGetContextInfoFromIfIndex (i4IfMainIndex,
                                                &u4ContextId, &u2LocalPortVip);
    if (i4retVal == PBB_FAILURE || u4ContextId != PBB_CURR_CONTEXT_ID ())
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate that the Pip port entry pre exists */

    i4retVal =
        PbbVcmGetContextInfoFromIfIndex
        (i4TestValFsPbbVipToPipMappingPipIfIndex, &u4ContextId, &u2LocalPort);

    if (i4retVal == PBB_FAILURE || u4ContextId != PBB_CURR_CONTEXT_ID ())
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate that the Pip port exists */
    i4retVal = PbbValidatePipPortExists (u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Validate that the row status is not active  */
    i4retVal =
        PbbIsVipPipRowStatusNotActive (i4Fsdot1ahContextId, u2LocalPortVip);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbVipToPipMappingStorageType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
testValFsPbbVipToPipMappingStorageType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbVipToPipMappingStorageType (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4Fsdot1ahContextId,
                                          INT4 i4IfMainIndex,
                                          INT4
                                          i4TestValFsPbbVipToPipMappingStorageType)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate the indices */
    i4retVal =
        nmhValidateIndexInstanceFsPbbVipToPipMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_VALID_STORAGE_TYPE (i4TestValFsPbbVipToPipMappingStorageType)
        == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Validate that the row status is not active  */
    i4retVal = PbbIsVipPipRowStatusNotActive (i4Fsdot1ahContextId, u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbVipToPipMappingRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
testValFsPbbVipToPipMappingRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbVipToPipMappingRowStatus (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4Fsdot1ahContextId,
                                        INT4 i4IfMainIndex,
                                        INT4
                                        i4TestValFsPbbVipToPipMappingRowStatus)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    /* Validate the indices */
    i4retVal =
        nmhValidateIndexInstanceFsPbbVipToPipMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex);
    if (i4retVal == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_VALID_ROW_STATUS (i4TestValFsPbbVipToPipMappingRowStatus)
        == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    /* Get the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    /* Validate that the row status is not active  */
    i4retVal = PbbTestVipPipRowStatus (pu4ErrorCode,
                                       i4Fsdot1ahContextId,
                                       u2LocalPort,
                                       i4TestValFsPbbVipToPipMappingRowStatus);
    if (i4retVal == PBB_FAILURE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbVipToPipMappingTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbVipToPipMappingTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbCBPServiceMappingTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbCBPServiceMappingTable (INT4
                                                     i4Fsdot1ahContextId,
                                                     INT4 i4IfMainIndex,
                                                     UINT4
                                                     u4FsPbbCBPServiceMappingBackboneSid)
{
    if (PbbValidateContextId (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PbbValidateCbpPort (i4IfMainIndex) == PBB_FAILURE)
    {
        return SNMP_FAILURE;

    }

    if (PbbValidateIsidPort (u4FsPbbCBPServiceMappingBackboneSid,
                             i4IfMainIndex) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbCBPServiceMappingTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsPbbCBPServiceMappingTable (INT4
                                             *pi4Fsdot1ahContextId,
                                             INT4 *pi4IfMainIndex,
                                             UINT4
                                             *pu4FsPbbCBPServiceMappingBackboneSid)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstIsidPort (&u4Isid, &u2LocalPort) == PBB_SUCCESS)
                {
                    if (PbbVcmGetIfIndexFromLocalPort (u4Context,
                                                       u2LocalPort,
                                                       &i4IfIndex) !=
                        PBB_SUCCESS)
                    {
                        return SNMP_FAILURE;
                    }
                    *pu4FsPbbCBPServiceMappingBackboneSid = u4Isid;
                    *pi4Fsdot1ahContextId = u4Context;
                    *pi4IfMainIndex = i4IfIndex;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbCBPServiceMappingTable
Input       :  The Indices
Fsdot1ahContextId
nextFsdot1ahContextId
IfMainIndex
nextIfMainIndex
FsPbbCBPServiceMappingBackboneSid
nextFsPbbCBPServiceMappingBackboneSid
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbCBPServiceMappingTable (INT4
                                            i4Fsdot1ahContextId,
                                            INT4
                                            *pi4NextFsdot1ahContextId,
                                            INT4 i4IfMainIndex,
                                            INT4 *pi4NextIfMainIndex,
                                            UINT4
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            UINT4
                                            *pu4NextFsPbbCBPServiceMappingBackboneSid)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT4               u4NextIsid = PBB_INIT_VAL;
    INT4                i4ifIndex = PBB_INIT_VAL;
    UINT2               u2NextPort = PBB_INIT_VAL;

    if (PbbSelectContext (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PbbGetNextValidIsidPort
        (i4Fsdot1ahContextId, &u4Context,
         u4FsPbbCBPServiceMappingBackboneSid, &u4NextIsid, i4IfMainIndex,
         &u2NextPort) == PBB_SUCCESS)
    {
        if (PbbVcmGetIfIndexFromLocalPort (u4Context,
                                           u2NextPort,
                                           &i4ifIndex) != PBB_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        *pi4NextFsdot1ahContextId = u4Context;
        *pi4NextIfMainIndex = i4ifIndex;
        *pu4NextFsPbbCBPServiceMappingBackboneSid = u4NextIsid;
        PBB_CURR_CONTEXT_PTR () = NULL;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbCBPServiceMappingBVid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
retValFsPbbCBPServiceMappingBVid
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbCBPServiceMappingBVid (INT4 i4Fsdot1ahContextId,
                                  INT4 i4IfMainIndex,
                                  UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                  INT4 *pi4RetValFsPbbCBPServiceMappingBVid)
{
    UINT4               u4BVlanId = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetIsidPortBVlanId (i4IfMainIndex,
                               u4FsPbbCBPServiceMappingBackboneSid,
                               &u4BVlanId) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsPbbCBPServiceMappingBVid = u4BVlanId;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbCBPServiceMappingDefaultBackboneDest
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
retValFsPbbCBPServiceMappingDefaultBackboneDest
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbCBPServiceMappingDefaultBackboneDest (INT4
                                                 i4Fsdot1ahContextId,
                                                 INT4 i4IfMainIndex,
                                                 UINT4
                                                 u4FsPbbCBPServiceMappingBackboneSid,
                                                 tMacAddr *
                                                 pRetValFsPbbCBPServiceMappingDefaultBackboneDest)
{
    tMacAddr            DefaultBackboneDest;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    PBB_MEMSET (DefaultBackboneDest, PBB_INIT_VAL, sizeof (tMacAddr));

    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    /* Validate that the local port exists for interface index */
    if (PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort) == PBB_FAILURE)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (PbbGetIsidPortDefBDA (u2LocalPort, u4FsPbbCBPServiceMappingBackboneSid,
                              &DefaultBackboneDest) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    PBB_MEMCPY (pRetValFsPbbCBPServiceMappingDefaultBackboneDest,
                DefaultBackboneDest, sizeof (tMacAddr));
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbCBPServiceMappingType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
retValFsPbbCBPServiceMappingType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbCBPServiceMappingType (INT4 i4Fsdot1ahContextId,
                                  INT4 i4IfMainIndex,
                                  UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValFsPbbCBPServiceMappingType)
{
    UINT1               u1ServiceMapType = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetIsidPortServiceMapType (i4IfMainIndex,
                                      u4FsPbbCBPServiceMappingBackboneSid,
                                      &u1ServiceMapType) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    pRetValFsPbbCBPServiceMappingType->pu1_OctetList[0] = u1ServiceMapType;
    pRetValFsPbbCBPServiceMappingType->i4_Length = 1;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFsPbbCBPServiceMappingLocalSid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
retValFsPbbCBPServiceMappingLocalSid
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbCBPServiceMappingLocalSid (INT4 i4Fsdot1ahContextId,
                                      INT4 i4IfMainIndex,
                                      UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                      UINT4
                                      *pu4RetValFsPbbCBPServiceMappingLocalSid)
{
    UINT4               u4LocalIsid = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetIsidPortLocalIsid (i4IfMainIndex,
                                 u4FsPbbCBPServiceMappingBackboneSid,
                                 &u4LocalIsid) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    if (u4LocalIsid == PBB_ISID_INVALID_VALUE)
    {
        *pu4RetValFsPbbCBPServiceMappingLocalSid =
            u4FsPbbCBPServiceMappingBackboneSid;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4RetValFsPbbCBPServiceMappingLocalSid = u4LocalIsid;
        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
Function    :  nmhGetFsPbbCBPServiceMappingRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
retValFsPbbCBPServiceMappingRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbCBPServiceMappingRowStatus (INT4
                                       i4Fsdot1ahContextId,
                                       INT4 i4IfMainIndex,
                                       UINT4
                                       u4FsPbbCBPServiceMappingBackboneSid,
                                       INT4
                                       *pi4RetValFsPbbCBPServiceMappingRowStatus)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                    u4FsPbbCBPServiceMappingBackboneSid,
                                    &u4Rowstatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsPbbCBPServiceMappingRowStatus = u4Rowstatus;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbCBPServiceMappingBVid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
setValFsPbbCBPServiceMappingBVid
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbCBPServiceMappingBVid (INT4 i4Fsdot1ahContextId,
                                  INT4 i4IfMainIndex,
                                  UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                  INT4 i4SetValFsPbbCBPServiceMappingBVid)
{
    PBB_PERF_MARK_START_TIME ();
    PBB_PERF_RESET_TIME (gu4DeleteBVLANTunnelTimeTaken);
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSetIsidPortBVlanId (i4IfMainIndex,
                               u4FsPbbCBPServiceMappingBackboneSid,
                               i4SetValFsPbbCBPServiceMappingBVid) !=
        PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    PBB_PERF_MARK_END_TIME (gu4CreateBVLANTunnelTimeTaken);
    PBB_PERF_MARK_END_TIME (gu4DeleteBVLANTunnelTimeTaken);
    PBB_PERF_PRINT_TIME (gu4CreateBVLANTunnelTimeTaken);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbCBPServiceMappingDefaultBackboneDest
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
setValFsPbbCBPServiceMappingDefaultBackboneDest
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbCBPServiceMappingDefaultBackboneDest (INT4
                                                 i4Fsdot1ahContextId,
                                                 INT4 i4IfMainIndex,
                                                 UINT4
                                                 u4FsPbbCBPServiceMappingBackboneSid,
                                                 tMacAddr
                                                 SetValFsPbbCBPServiceMappingDefaultBackboneDest)
{
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (PbbSetIsidPortDefBDA
        (i4IfMainIndex, u4FsPbbCBPServiceMappingBackboneSid,
         SetValFsPbbCBPServiceMappingDefaultBackboneDest) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbCBPServiceMappingType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
setValFsPbbCBPServiceMappingType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbCBPServiceMappingType (INT4 i4Fsdot1ahContextId,
                                  INT4 i4IfMainIndex,
                                  UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pSetValFsPbbCBPServiceMappingType)
{
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSetIsidPortCBPServiceMapType (i4IfMainIndex,
                                         u4FsPbbCBPServiceMappingBackboneSid,
                                         pSetValFsPbbCBPServiceMappingType) !=
        PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbCBPServiceMappingLocalSid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
setValFsPbbCBPServiceMappingLocalSid
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbCBPServiceMappingLocalSid (INT4 i4Fsdot1ahContextId,
                                      INT4 i4IfMainIndex,
                                      UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                      UINT4
                                      u4SetValFsPbbCBPServiceMappingLocalSid)
{
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSetIsidPortLocalIsid (i4IfMainIndex,
                                 u4FsPbbCBPServiceMappingBackboneSid,
                                 u4SetValFsPbbCBPServiceMappingLocalSid)
        != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsPbbCBPServiceMappingRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
setValFsPbbCBPServiceMappingRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbCBPServiceMappingRowStatus (INT4
                                       i4Fsdot1ahContextId,
                                       INT4 i4IfMainIndex,
                                       UINT4
                                       u4FsPbbCBPServiceMappingBackboneSid,
                                       INT4
                                       i4SetValFsPbbCBPServiceMappingRowStatus)
{
    tMacAddr            DefaultDstBMAC;
    UINT4               u4NumofPortPerIsidPerContext = PBB_INIT_VAL;
    UINT4               u4NumOfPortsPerIsidPresent = PBB_INIT_VAL;
    PBB_MEMSET (DefaultDstBMAC, 0, sizeof (tMacAddr));

    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    switch (i4SetValFsPbbCBPServiceMappingRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
        {
            if (PbbValidateIsid (u4FsPbbCBPServiceMappingBackboneSid)
                == PBB_SUCCESS)
            {
                if (PbbGetNumPortPerIsidPerContext
                    (i4Fsdot1ahContextId,
                     u4FsPbbCBPServiceMappingBackboneSid,
                     &u4NumofPortPerIsidPerContext) != PBB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                if (u4NumofPortPerIsidPerContext >=
                    gPbbGlobData.PbbTaskInfo.u4MaxPortperIsidPerContext)
                {
                    CLI_SET_ERR (CLI_PBB_PORT_PER_ISID_CTXT_EXCEED);
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }
                if (PbbGetMaxNumPortPerIsid (&u4NumOfPortsPerIsidPresent,
                                             u4FsPbbCBPServiceMappingBackboneSid,
                                             i4Fsdot1ahContextId)
                    != PBB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                if (u4NumOfPortsPerIsidPresent >=
                    gPbbGlobData.PbbTaskInfo.u4MaxPortperIsid)
                {
                    CLI_SET_ERR (CLI_PBB_PORT_PER_ISID_EXCEED);
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }

                if (PbbInsertCBPIsidRBTreeNode
                    (u4FsPbbCBPServiceMappingBackboneSid,
                     i4IfMainIndex) != PBB_SUCCESS)
                {
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }
                if (PbbInsertCBPIsidDll (u4FsPbbCBPServiceMappingBackboneSid,
                                         i4IfMainIndex) != PBB_SUCCESS)
                {
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }

            }
            else
            {
                if (PbbCreateIsidRBTreeNode
                    (u4FsPbbCBPServiceMappingBackboneSid) != PBB_SUCCESS)
                {
                    PbbReleaseContext ();
                    return SNMP_FAILURE;

                }
                if (PbbGetNumPortPerIsidPerContext
                    (i4Fsdot1ahContextId,
                     u4FsPbbCBPServiceMappingBackboneSid,
                     &u4NumofPortPerIsidPerContext) != PBB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                if (u4NumofPortPerIsidPerContext >=
                    gPbbGlobData.PbbTaskInfo.u4MaxPortperIsidPerContext)
                {
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }
                if (PbbGetMaxNumPortPerIsid (&u4NumOfPortsPerIsidPresent,
                                             u4FsPbbCBPServiceMappingBackboneSid,
                                             i4Fsdot1ahContextId)
                    != PBB_SUCCESS)
                {
                    return SNMP_FAILURE;
                }
                if (u4NumOfPortsPerIsidPresent >=
                    gPbbGlobData.PbbTaskInfo.u4MaxPortperIsid)
                {
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }

                if (PbbInsertCBPIsidRBTreeNode
                    (u4FsPbbCBPServiceMappingBackboneSid,
                     i4IfMainIndex) != PBB_SUCCESS)
                {
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }
                if (PbbInsertCBPIsidDll (u4FsPbbCBPServiceMappingBackboneSid,
                                         i4IfMainIndex) != PBB_SUCCESS)
                {
                    PbbReleaseContext ();
                    return SNMP_FAILURE;
                }

            }

            if (PbbSetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            i4SetValFsPbbCBPServiceMappingRowStatus)
                != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }

        }
            break;
        case PBB_ACTIVE:
        {
            if (PbbSetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            i4SetValFsPbbCBPServiceMappingRowStatus)
                != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbSetBCompHwServiceInst (i4Fsdot1ahContextId,
                                          u4FsPbbCBPServiceMappingBackboneSid,
                                          i4IfMainIndex) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            L2IwfCreateIsid (i4Fsdot1ahContextId,
                             u4FsPbbCBPServiceMappingBackboneSid);
        }
            break;
        case PBB_DESTROY:
        {
            if (PbbDeleteCBPNodeDLL (u4FsPbbCBPServiceMappingBackboneSid,
                                     i4IfMainIndex) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;

            }
            if (PbbDeleteIsidNodeDll (u4FsPbbCBPServiceMappingBackboneSid,
                                      i4IfMainIndex) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;

            }
            if (PbbSetIsidOUIRowStatus (i4Fsdot1ahContextId,
                                        u4FsPbbCBPServiceMappingBackboneSid,
                                        i4IfMainIndex,
                                        i4SetValFsPbbCBPServiceMappingRowStatus)
                == PBB_FAILURE)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbSetVipIsidDefaultBDAddr (u4FsPbbCBPServiceMappingBackboneSid,
                                            DefaultDstBMAC) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbSetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            i4SetValFsPbbCBPServiceMappingRowStatus)
                != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbDeleteIsidPortRBNodes (i4IfMainIndex,
                                          u4FsPbbCBPServiceMappingBackboneSid)
                != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbDelBCompHwServiceInst (i4Fsdot1ahContextId,
                                          i4IfMainIndex,
                                          u4FsPbbCBPServiceMappingBackboneSid)
                != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbDecrNumPortPerIsidPerContext
                (u4FsPbbCBPServiceMappingBackboneSid) != PBB_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (PbbGetNumPortPerIsidPerContext
                (i4Fsdot1ahContextId,
                 u4FsPbbCBPServiceMappingBackboneSid,
                 &u4NumofPortPerIsidPerContext) == PBB_SUCCESS)
            {
                if (u4NumofPortPerIsidPerContext == 0)
                {
                    if (PbbDeleteIsidRBNodes
                        (u4FsPbbCBPServiceMappingBackboneSid) != PBB_SUCCESS)
                    {
                        return PBB_FAILURE;
                    }
                    PbbL2IwfDeleteIsid (i4Fsdot1ahContextId,
                                        u4FsPbbCBPServiceMappingBackboneSid);
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
            break;
        case PBB_NOT_IN_SERVICE:
        {
            if (PbbSetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            i4SetValFsPbbCBPServiceMappingRowStatus)
                != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }
            if (PbbDelBCompHwServiceInst (i4Fsdot1ahContextId,
                                          i4IfMainIndex,
                                          u4FsPbbCBPServiceMappingBackboneSid)
                != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }

        }
            break;
        default:
            PbbReleaseContext ();
            return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbCBPServiceMappingBVid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
testValFsPbbCBPServiceMappingBVid
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbCBPServiceMappingBVid (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsdot1ahContextId,
                                     INT4 i4IfMainIndex,
                                     UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                     INT4 i4TestValFsPbbCBPServiceMappingBVid)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);

        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                    u4FsPbbCBPServiceMappingBackboneSid,
                                    &u4Rowstatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_ACTIVE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsPbbCBPServiceMappingBVid > PBB_MAX_BVLAN_VALUE) ||
        (i4TestValFsPbbCBPServiceMappingBVid < PBB_INIT_VAL))
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    PBB_PERF_MARK_END_TIME (gu4CreateBVLANTunnelTimeTaken);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbCBPServiceMappingDefaultBackboneDest
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
testValFsPbbCBPServiceMappingDefaultBackboneDest
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbCBPServiceMappingDefaultBackboneDest (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4Fsdot1ahContextId,
                                                    INT4 i4IfMainIndex,
                                                    UINT4
                                                    u4FsPbbCBPServiceMappingBackboneSid,
                                                    tMacAddr
                                                    TestValFsPbbCBPServiceMappingDefaultBackboneDest)
{
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);

        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    PBB_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

    if (PBB_IS_BCASTADDR (TestValFsPbbCBPServiceMappingDefaultBackboneDest)
        == PBB_TRUE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_PBB_BROAD_MAC_NOT_ALLOWED);
        return SNMP_FAILURE;
    }

    if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                    u4FsPbbCBPServiceMappingBackboneSid,
                                    &u4Rowstatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_ACTIVE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2FsPbbCBPServiceMappingType
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
testValFsPbbCBPServiceMappingType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbCBPServiceMappingType (UINT4 *pu4ErrorCode,
                                     INT4 i4Fsdot1ahContextId,
                                     INT4 i4IfMainIndex,
                                     UINT4 u4FsPbbCBPServiceMappingBackboneSid,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pTestValFsPbbCBPServiceMappingType)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                    u4FsPbbCBPServiceMappingBackboneSid,
                                    &u4Rowstatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_ACTIVE)
    {
        CLI_SET_ERR (CLI_PBB_SERVICE_TYPE_ERR);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pTestValFsPbbCBPServiceMappingType->i4_Length !=
        PBB_VALID_SERVICE_MAP_LENGTH)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValFsPbbCBPServiceMappingType->pu1_OctetList[0] !=
         PBB_EGRESS_INGRESS) &&
        (pTestValFsPbbCBPServiceMappingType->pu1_OctetList[0] !=
         PBB_EGRESS) &&
        (pTestValFsPbbCBPServiceMappingType->pu1_OctetList[0] != PBB_INGRESS))
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbCBPServiceMappingLocalSid
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
testValFsPbbCBPServiceMappingLocalSid
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbCBPServiceMappingLocalSid (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4Fsdot1ahContextId,
                                         INT4 i4IfMainIndex,
                                         UINT4
                                         u4FsPbbCBPServiceMappingBackboneSid,
                                         UINT4
                                         u4TestValFsPbbCBPServiceMappingLocalSid)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCBPServiceMappingTable
        (i4Fsdot1ahContextId, i4IfMainIndex,
         u4FsPbbCBPServiceMappingBackboneSid) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                    u4FsPbbCBPServiceMappingBackboneSid,
                                    &u4Rowstatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (u4Rowstatus == PBB_ACTIVE)
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbValidateLocalIsid (u4TestValFsPbbCBPServiceMappingLocalSid,
                              u4FsPbbCBPServiceMappingBackboneSid) !=
        PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_TRANS_ISID_ERR);
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }
    if ((u4TestValFsPbbCBPServiceMappingLocalSid >
         PBB_MAX_TRANSLATE_ISID_VAL) ||
        (u4TestValFsPbbCBPServiceMappingLocalSid == PBB_INIT_VAL))
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsPbbCBPServiceMappingRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid

The Object 
testValFsPbbCBPServiceMappingRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbCBPServiceMappingRowStatus (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4Fsdot1ahContextId,
                                          INT4 i4IfMainIndex,
                                          UINT4
                                          u4FsPbbCBPServiceMappingBackboneSid,
                                          INT4
                                          i4TestValFsPbbCBPServiceMappingRowStatus)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;

    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PbbValidateContextId (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((u4FsPbbCBPServiceMappingBackboneSid <= PBB_MIN_ISID_VALUE) ||
        (u4FsPbbCBPServiceMappingBackboneSid > PBB_MAX_ISID_VALUE))
    {
        PbbReleaseContext ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;

    }
    if (PbbValidateCbpPort (i4IfMainIndex) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_PORT_NOT_CBP);
        return SNMP_FAILURE;
    }

    switch (i4TestValFsPbbCBPServiceMappingRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
        {
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance >=
                gPbbGlobData.PbbTaskInfo.u4MaxIsid)
            {
                return SNMP_FAILURE;
            }
            if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            &u4Rowstatus) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (u4Rowstatus != PBB_INVALID_ROWSTATUS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            PbbReleaseContext ();
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            return SNMP_SUCCESS;
        }
        case PBB_ACTIVE:
        {
            if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            &u4Rowstatus) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (u4Rowstatus == PBB_INVALID_ROWSTATUS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            PbbReleaseContext ();

            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            return SNMP_SUCCESS;
        }
        case PBB_NOT_IN_SERVICE:
        {
            if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            &u4Rowstatus) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (u4Rowstatus != PBB_ACTIVE)
            {
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            return SNMP_SUCCESS;
        }
        case PBB_DESTROY:
        {
            if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            &u4Rowstatus) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (u4Rowstatus == PBB_INVALID_ROWSTATUS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            PbbReleaseContext ();
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            return SNMP_SUCCESS;
        }
        case PBB_CREATE_AND_GO:
        {

            if (PbbGetIsidPortCbpRowstatus (i4IfMainIndex,
                                            u4FsPbbCBPServiceMappingBackboneSid,
                                            &u4Rowstatus) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (u4Rowstatus == PBB_INVALID_ROWSTATUS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                PbbReleaseContext ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            PbbReleaseContext ();
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            return SNMP_SUCCESS;
        }
        default:
            PbbReleaseContext ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbCBPServiceMappingTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
FsPbbCBPServiceMappingBackboneSid
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbCBPServiceMappingTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbCbpTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbCbpTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbCbpTable (INT4
                                       i4Fsdot1ahContextId, INT4 i4IfMainIndex)
{

    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PbbValidateContextId (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PbbValidateCbpPort (i4IfMainIndex) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbCbpTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbCbpTable (INT4 *pi4Fsdot1ahContextId, INT4 *pi4IfMainIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstPort (pi4IfMainIndex) == PBB_SUCCESS)
                {
                    if (PbbValidateCbpPort (*pi4IfMainIndex) != PBB_SUCCESS)
                    {
                        PBB_CURR_CONTEXT_PTR () = NULL;
                        continue;
                    }
                    *pi4Fsdot1ahContextId = u4Context;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbCbpTable
Input       :  The Indices
Fsdot1ahContextId
nextFsdot1ahContextId
IfMainIndex
nextIfMainIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbCbpTable (INT4 i4Fsdot1ahContextId,
                              INT4 *pi4NextFsdot1ahContextId,
                              INT4 i4IfMainIndex, INT4 *pi4NextIfMainIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    INT4                i4ifIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PBB_CONTEXT_PTR (i4Fsdot1ahContextId) != NULL)
    {
        PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (i4Fsdot1ahContextId);
        if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
        {
            if (PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort)
                == PBB_SUCCESS)
            {
                if (PbbGetNextPort (u2LocalPort, &i4ifIndex) == PBB_SUCCESS)
                {
                    if (PbbValidateCbpPort (i4ifIndex) == PBB_SUCCESS)
                    {
                        *pi4NextFsdot1ahContextId = i4Fsdot1ahContextId;
                        *pi4NextIfMainIndex = i4ifIndex;
                        PBB_CURR_CONTEXT_PTR () = NULL;
                        PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns "
                                      "successfully\n", __FILE__,
                                      PBB_FUNCTION_NAME);
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
    }
    PBB_CURR_CONTEXT_PTR () = NULL;
    u4Context = i4Fsdot1ahContextId + 1;

    for (; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstPort (pi4NextIfMainIndex) == PBB_SUCCESS)
                {
                    if (PbbValidateCbpPort (*pi4NextIfMainIndex) != PBB_SUCCESS)
                    {
                        PBB_CURR_CONTEXT_PTR () = NULL;
                        continue;
                    }
                    *pi4NextFsdot1ahContextId = u4Context;
                    PBB_TRC_ARG2 (MGMT_TRC,
                                  "%s :: %s() ::  returns successfully\n",
                                  __FILE__, PBB_FUNCTION_NAME);
                    return SNMP_SUCCESS;
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbCbpRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
retValFsPbbCbpRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbCbpRowStatus (INT4 i4Fsdot1ahContextId,
                         INT4 i4IfMainIndex, INT4 *pi4RetValFsPbbCbpRowStatus)
{
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCbpTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    if (PbbGetPortCbpTableRowstatus (i4IfMainIndex, &u4Rowstatus) !=
        PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsPbbCbpRowStatus = u4Rowstatus;
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsPbbCbpRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
setValFsPbbCbpRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsPbbCbpRowStatus (INT4 i4Fsdot1ahContextId,
                         INT4 i4IfMainIndex, INT4 i4SetValFsPbbCbpRowStatus)
{
    if (PbbSelectContext (i4Fsdot1ahContextId) == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (PbbSetPortCbpTableRowstatus (i4IfMainIndex,
                                     i4SetValFsPbbCbpRowStatus) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsPbbCbpRowStatus
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex

The Object 
testValFsPbbCbpRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2FsPbbCbpRowStatus (UINT4 *pu4ErrorCode,
                            INT4 i4Fsdot1ahContextId,
                            INT4 i4IfMainIndex, INT4 i4TestValFsPbbCbpRowStatus)
{
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_NOT_INITIALIZED);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (nmhValidateIndexInstanceFsPbbCbpTable
        (i4Fsdot1ahContextId, i4IfMainIndex) != SNMP_SUCCESS)
    {
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PbbReleaseContext ();
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsPbbCbpRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
        case PBB_NOT_IN_SERVICE:
        case PBB_DESTROY:
        case PBB_CREATE_AND_GO:
        {
            PbbReleaseContext ();
            return SNMP_FAILURE;
        }
            break;
        case PBB_ACTIVE:
        {
            PbbReleaseContext ();
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            return SNMP_SUCCESS;
        }
        default:
            PbbReleaseContext ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsPbbCbpTable
Input       :  The Indices
Fsdot1ahContextId
IfMainIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsPbbCbpTable (UINT4 *pu4ErrorCode,
                       tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbPipToVipMappingTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceFsPbbPipToVipMappingTable
Input       :  The Indices
Fsdot1ahContextId
FsPbbPipIfIndex
IfIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbPipToVipMappingTable (INT4
                                                   i4Fsdot1ahContextId,
                                                   INT4 i4FsPbbPipIfIndex,
                                                   INT4 i4IfIndex)
{
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate the System Initialized status */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate the value of Component id */
    i4retVal = PbbValidateContextId (i4Fsdot1ahContextId);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate for the Bridge mode to be PBB */
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /* Validate that the local port exists for interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4FsPbbPipIfIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate that the Pip port exists */
    i4retVal = PbbValidatePipPortExists (u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate the value of VIP */
    i4retVal = PbbValidateVip (i4IfIndex);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Validate that the given Vip exists for given Pip */
    i4retVal = PbbValidateViptoPip (i4IfIndex, u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexFsPbbPipToVipMappingTable
Input       :  The Indices
Fsdot1ahContextId
FsPbbPipIfIndex
IfIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbPipToVipMappingTable (INT4
                                           *pi4Fsdot1ahContextId,
                                           INT4 *pi4FsPbbPipIfIndex,
                                           INT4 *pi4IfIndex)
{
    UINT4               u4Vip = PBB_INIT_VAL;
    UINT4               u4Pip = PBB_INIT_VAL;
    UINT4               u4Context = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {

        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                if (PbbGetFirstVip (&u4Vip) == PBB_SUCCESS)
                {
                    if (nmhGetFsPbbVipToPipMappingPipIfIndex (u4Context,
                                                              (INT4) u4Vip,
                                                              (INT4 *) &u4Pip)
                        == SNMP_SUCCESS)
                    {
                        *pi4Fsdot1ahContextId = u4Context;
                        *pi4IfIndex = u4Vip;
                        *pi4FsPbbPipIfIndex = (INT4) u4Pip;

                        PBB_TRC_ARG2 (MGMT_TRC,
                                      "%s :: %s() ::  returns successfully\n",
                                      __FILE__, PBB_FUNCTION_NAME);
                        return SNMP_SUCCESS;
                    }
                }
            }
            PBB_CURR_CONTEXT_PTR () = NULL;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetNextIndexFsPbbPipToVipMappingTable
Input       :  The Indices
Fsdot1ahContextId
nextFsdot1ahContextId
FsPbbPipIfIndex
nextFsPbbPipIfIndex
IfIndex
nextIfIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbPipToVipMappingTable (INT4
                                          i4Fsdot1ahContextId,
                                          INT4
                                          *pi4NextFsdot1ahContextId,
                                          INT4 i4FsPbbPipIfIndex,
                                          INT4 *pi4NextFsPbbPipIfIndex,
                                          INT4 i4IfIndex, INT4 *pi4NextIfIndex)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT2               u2NextVip = PBB_INIT_VAL;
    UINT4               u4NextPip = PBB_INIT_VAL;
    UINT4               u4NextVipIfIndex = PBB_INIT_VAL;

    if ((INT4) i4Fsdot1ahContextId < 0)
    {
        if (nmhGetFirstIndexFsPbbPipToVipMappingTable ((INT4 *) &u4Context,
                                                       (INT4 *)
                                                       &u4NextVipIfIndex,
                                                       (INT4 *) &u4NextPip) ==
            SNMP_SUCCESS)
        {
            *pi4NextFsdot1ahContextId = u4Context;
            *pi4NextFsPbbPipIfIndex = (INT4) u4NextPip;
            *pi4NextIfIndex = u4NextVipIfIndex;
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            return SNMP_SUCCESS;
        }
        return SNMP_FAILURE;
    }

    if (PbbSelectContext (i4Fsdot1ahContextId) != PBB_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4IfIndex < 0)
    {
        if (PbbGetFirstVip (&u4NextVipIfIndex) == PBB_SUCCESS)
        {
            if (nmhGetFsPbbVipToPipMappingPipIfIndex
                (i4Fsdot1ahContextId, (INT4) u4NextVipIfIndex,
                 (INT4 *) &u4NextPip) == SNMP_SUCCESS)
            {
                *pi4NextFsdot1ahContextId = i4Fsdot1ahContextId;
                *pi4NextFsPbbPipIfIndex = (INT4) u4NextPip;
                *pi4NextIfIndex = (INT4) u4NextVipIfIndex;
                PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                              __FILE__, PBB_FUNCTION_NAME);
                PbbReleaseContext ();
                return SNMP_SUCCESS;
            }
        }
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    if (i4FsPbbPipIfIndex <= 0)
    {
        if (nmhGetFsPbbVipToPipMappingPipIfIndex (u4Context,
                                                  (INT4) i4IfIndex,
                                                  (INT4 *) &u4NextPip)
            == SNMP_SUCCESS)
        {
            *pi4NextFsdot1ahContextId = i4Fsdot1ahContextId;
            *pi4NextFsPbbPipIfIndex = (INT4) u4NextPip;
            *pi4NextIfIndex = i4IfIndex;
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            PbbReleaseContext ();
            return SNMP_SUCCESS;
        }
        PbbReleaseContext ();
        return SNMP_FAILURE;
    }

    while (PbbGetNextVip
           (i4Fsdot1ahContextId, &u4Context, i4IfIndex,
            &u2NextVip) == PBB_SUCCESS)
    {
        if (PbbVcmGetIfIndexFromLocalPort (u4Context,
                                           u2NextVip,
                                           &i4IfIndex) != PBB_SUCCESS)
        {
            PbbReleaseContext ();
            return SNMP_FAILURE;
        }
        if (nmhGetFsPbbVipToPipMappingPipIfIndex (u4Context,
                                                  (INT4) i4IfIndex,
                                                  (INT4 *) &u4NextPip)
            == SNMP_SUCCESS)
        {
            *pi4NextFsdot1ahContextId = u4Context;
            *pi4NextFsPbbPipIfIndex = (INT4) u4NextPip;
            *pi4NextIfIndex = i4IfIndex;
            PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                          __FILE__, PBB_FUNCTION_NAME);
            PbbReleaseContext ();
            return SNMP_SUCCESS;
        }
        i4Fsdot1ahContextId = u4Context;
    }
    PbbReleaseContext ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsPbbPipToVipMappingStatus
Input       :  The Indices
Fsdot1ahContextId
FsPbbPipIfIndex
IfIndex

The Object 
retValFsPbbPipToVipMappingStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsPbbPipToVipMappingStatus (INT4 i4Fsdot1ahContextId,
                                  INT4 i4FsPbbPipIfIndex,
                                  INT4 i4IfIndex,
                                  INT4 *pi4RetValFsPbbPipToVipMappingStatus)
{
    if (nmhValidateIndexInstanceFsPbbPipToVipMappingTable
        (i4Fsdot1ahContextId, i4FsPbbPipIfIndex, i4IfIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsPbbPipToVipMappingStatus = PBB_TRUE;
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() ::  returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return SNMP_SUCCESS;

}
