#ifndef __PBBCLI_C__
#define __PBBCLI_C__
/* SOURCE FILE HEADER :
 *$Id: pbbcli.c,v 1.43 2014/03/19 13:36:12 siva Exp $

 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : pbbcli.c                                        |
 * |                                                                          |
 * |  PRINCIPAL AUTHOR      : PBB TEAM                                        |
 * |                                                                          |
 * |  SUBSYSTEM NAME        : PBB                                             |
 * |                                                                          |
 * |  MODULE NAME           : PBB configuration                               |
 * |                                                                          |
 * |  LANGUAGE              : C                                               |
 * |                                                                          |
 * |  TARGET ENVIRONMENT    : Linux                                           |
 * |                                                                          |
 * |  DATE OF FIRST RELEASE : 21st July 2008                                  |
 * |                                                                          |
 * |  DESCRIPTION           : CLI commands handler file                       | 
 * |                                                                          |
 * |                                                                          |
 *  ---------------------------------------------------------------------------
 *
 */

#include "pbbinc.h"
#include "fs1ahcli.h"
#include "fspbbcli.h"
PRIVATE INT4        PbbShowRunningSwitchConfig
PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));
PRIVATE INT4        PbbShowRunningServiceInst
PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));
PRIVATE INT4        PbbShowRunningIsidInContext
PROTO ((tCliHandle CliHandle, UINT4 u4Isid, UINT4 u4ContextId, UINT4 u4Vip));
PRIVATE VOID        PbbShowRunningConfigInterface
PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));
PRIVATE VOID        PbbShowRunningPortConfig
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4        PbbShowRunningPcpDecodingTable
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE VOID        PbbShowRunningPcpEncodingTable
PROTO ((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE VOID        PbbShowRunningIfConfig
PROTO ((tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4IfIndex));

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_pbb_cmd                                */
/*                                                                           */
/*     DESCRIPTION      : CLI's main process handler                         */
/*                                                                           */
/*     INPUT            : CliHandle   - handle of CLI                        */
/*                                                                           */
/*     OUTPUT           : u4Command   - cli command                          */
/*                                                                           */
/*     RETURNS          : SUCCESS or FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_pbb_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UINT1              *pau1MemberPorts = NULL;
    va_list             ap;
    UINT4              *args[PBB_MAX_ARGS];
    UINT1              *pu1Inst = NULL;
    INT1                argno = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4LocalPortId = 0;
    UINT4               u4ContextId;
    tMacAddr            au1InMacAddr;
    INT4                i4BvlanId = 0;
    UINT2               u2LocalIsid;
    UINT1               au1Oui[PBB_OUI_LENGTH];
    UINT4               u4ErrCode = 0;
    tSNMP_OCTET_STRING_TYPE oui;

    CliRegisterLock (CliHandle, PbbLock, PbbUnLock);
    PBB_LOCK ();

    CLI_MEMSET (au1InMacAddr, 0, sizeof (au1InMacAddr));

    if (PbbCliSelectContextOnMode
        (CliHandle, u4Command, &u4ContextId, &u4LocalPortId) == PBB_FAILURE)
    {
        PBB_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);
    if (pu1Inst != 0)
    {
        u4IfIndex = CLI_PTR_TO_U4 (pu1Inst);
    }
    /*if the command is inside Interface mode then get u4IfIndex */
    u4IfIndex = CLI_GET_IFINDEX ();

    /* Walk through the rest of the arguements and store in args array. 
     * Store 20 arguements at the max. This is because pbb commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == PBB_MAX_ARGS)
            break;
    }
    va_end (ap);
    pau1MemberPorts = FsUtilAllocBitList (PBB_IFPORT_LIST_SIZE);
    if (pau1MemberPorts == NULL)
    {
        PBB_UNLOCK ();
        PbbReleaseContext ();
        CliUnRegisterLock (CliHandle);
        PBB_TRC (ALL_FAILURE_TRC,
                 "cli_process_pbb_cmd: Error in allocating memory "
                 "for pau1MemberPorts\r\n");
        return CLI_FAILURE;
    }
    MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);

    switch (u4Command)
    {
        case CLI_PBB_ISID_MODE:
            /* args[0] - ISID to enter into its mode */
            i4RetStatus = PbbISIDMode (CliHandle, *((UINT4 *) args[0]));
            break;

        case CLI_PBB_NO_ISID:
            /* args[0] - ISID to enter into its mode */
            i4RetStatus = PbbDeleteISID (CliHandle, *((UINT4 *) args[0]));
            break;

        case CLI_PBB_SHUT:
            i4RetStatus = PbbShutdown (CliHandle, PBB_SNMP_TRUE);
            break;

        case CLI_PBB_NO_SHUT:
            i4RetStatus = PbbShutdown (CliHandle, PBB_SNMP_FALSE);
            break;

        case CLI_PBB_PISID:
            /* args[0] - PISID */
            i4RetStatus = PbbSetPisidOnPort (CliHandle, *((UINT4 *) args[0]),
                                             (UINT4) u4IfIndex);
            break;

        case CLI_PBB_NO_PISID:
            /* args[0] - PISID */
            i4RetStatus = PbbDelPortPisid (CliHandle, (UINT4) u4IfIndex);
            break;

        case CLI_PBB_ISID_ACTIVE:
            i4RetStatus = PbbIsidActive (CliHandle, u4ContextId,
                                         CLI_PTR_TO_U4 (args[0]), PBB_ACTIVE);
            break;

        case CLI_PBB_ISID_NO_ACTIVE:
            i4RetStatus = PbbIsidActive (CliHandle, u4ContextId,
                                         CLI_PTR_TO_U4 (args[0]),
                                         PBB_NOT_IN_SERVICE);
            break;

        case CLI_PBB_GLB_OUI:

            CLI_MEMSET (au1Oui, 0, PBB_OUI_LENGTH);
            oui.i4_Length = PBB_OUI_LENGTH;
            oui.pu1_OctetList = au1Oui;

            CLI_CONVERT_DOT_STR_TO_ARRAY ((UINT1 *) args[0], oui.pu1_OctetList);
            i4RetStatus = PbbGlbOUI (CliHandle, &oui);

            break;

        case CLI_PBB_GLB_NO_OUI:
            CLI_MEMSET (au1Oui, 0, PBB_OUI_LENGTH);
            oui.i4_Length = PBB_OUI_LENGTH;
            oui.pu1_OctetList = au1Oui;

            CLI_CONVERT_DOT_STR_TO_ARRAY ((UINT1 *) PBB_DEFAULT_GBL_OUI,
                                          oui.pu1_OctetList);
            i4RetStatus = PbbGlbOUI (CliHandle, &oui);
            break;
        case CLI_PBB_OUI:
            /*  args[0] - OUI
             *  args[1] - IfType for Fast Ethernet
             *  args[2] - Interface List
             *  args[3] - IfType for Gigabit Ethernet
             *  args[4] - Interface List
             *  args[5] - IfType for Internal
             *  args[6] - Interface List
             *  args[7] - IfType for Port Channel
             *  args[8] - Port Channel List
             */
            CLI_MEMSET (au1Oui, 0, PBB_OUI_LENGTH);
            oui.i4_Length = PBB_OUI_LENGTH;
            oui.pu1_OctetList = au1Oui;

            CLI_CONVERT_DOT_STR_TO_ARRAY ((UINT1 *) args[0], oui.pu1_OctetList);

            CLI_MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);

            if (args[2] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[1], (INT1 *) args[2],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            if (args[4] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[3], (INT1 *) args[4],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }
            }

            if (args[6] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[5], (INT1 *) args[6],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }
            }

            if (args[8] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[7], (INT1 *) args[8],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }
            }

            i4RetStatus = PbbCreateOUI (CliHandle, u4ContextId,
                                        &oui, pau1MemberPorts);

            break;

        case CLI_PBB_NO_OUI:
            /* args[0] - IfType for Fast Ethernet
             *  args[1] - Interface List
             *  args[2] - IfType for Gigabit Ethernet
             *  args[3] - Interface List
             *  args[4] - IfType for Internal
             *  args[5] - Interface List
             *  args[6] - IfType for Port Channel
             *  args[7] - Port Channel List
             */

            CLI_MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);

            if (args[1] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            if (args[3] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }
            }

            if (args[5] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[4], (INT1 *) args[5],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }
            }

            if (args[7] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }
            }
            i4RetStatus =
                PbbDeleteOUIList (CliHandle, u4ContextId, pau1MemberPorts);
            break;
        case CLI_PBB_NO_OUI_ALL:
            i4RetStatus = PbbDeleteOUIAll (CliHandle, u4ContextId);
            break;

        case CLI_PBB_TRANSLATE_ISID:
            /* args[0] - Local Isid
             * args[1] - IfType
             * args[2] - Interface List
             * args[3] - IfType
             * args[4] - Interface List
             * args[5] - IfType
             * args[6] - Interface List
             * args[7] - IfType
             * args[8] - Port Channel List
             */

            CLI_MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);

            /* Gigabit Ethernet Ports */
            if (args[2] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[1], (INT1 *) args[2],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }

            /* Fast Ethernet Ports */
            if (args[4] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[3], (INT1 *) args[4],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Inernal Ports */
            if (args[6] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[5], (INT1 *) args[6],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Check if portchannel ports are given */
            if (args[8] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[7], (INT1 *) args[8],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }
            }
            i4RetStatus = PbbTranslateIsid (CliHandle,
                                            *(UINT4 *) args[0],
                                            pau1MemberPorts);

            break;

        case CLI_PBB_NO_TRANSLATE_ISID:
            /* args[0] - IfType
             * args[1] - Interface List
             * args[2] - IfType
             * args[3] - Interface List
             * args[4] - IfType
             * args[5] - Interface List
             * args[6] - IfType
             * args[7] - Port Channel List
             */

            CLI_MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);
            u2LocalIsid = 1;

            /* Gigabit Ethernet Ports */
            if (args[1] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }

            /* Fast Ethernet Ports */
            if (args[3] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Internal Ports */
            if (args[5] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[4], (INT1 *) args[5],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }
            }

            /* Check if portchannel ports are given */
            if (args[7] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }
            }
            i4RetStatus = PbbTranslateIsid (CliHandle,
                                            u2LocalIsid, pau1MemberPorts);

            break;

        case CLI_PBB_DESTINATION_MAC:
            /* args[0] - destination MAC address */

            StrToMac ((UINT1 *) args[1], au1InMacAddr);
            i4RetStatus = PbbSetDestMacAddr (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                             au1InMacAddr);
            break;

        case CLI_PBB_NO_DESTINATION_MAC:
            i4RetStatus = PbbSetDestMacAddr (CliHandle, CLI_PTR_TO_U4 (args[0]),
                                             au1InMacAddr);
            break;

        case CLI_PBB_ISID_BVLAN_PORT:
            /* args[0] - Bvlan ID
             * args[1] - IfType
             * args[2] - Interface List
             * args[3] - IfType
             * args[4] - Interface List
             * args[5] - Internal
             * args[6] - Internal Interface List
             * args[7] - Port Channel
             * args[8] - Port Channel List
             */
            CLI_MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);

            if (args[2] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[1], (INT1 *) args[2],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }
            if (args[4] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[3], (INT1 *) args[4],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }
            if (args[6] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[5], (INT1 *) args[6],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[8] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[7], (INT1 *) args[8],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }

            }
            i4RetStatus = PbbBvlanMapping (CliHandle,
                                           pau1MemberPorts, *(INT4 *) args[0]);

            break;

        case CLI_PBB_ISID_NO_BVLAN_PORT:
            /* i4BvlanId - 0
             * args[0] - IfType
             * args[1] - Interface List
             * args[2] - IfType
             * args[3] - Interface List
             * args[4] - Internal
             * args[5] - Internal Interface List
             * args[6] - Port Channel
             * args[7] - Port Channel List
             */
            CLI_MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);
            i4BvlanId = 0;

            if (args[1] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }
            if (args[3] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }
            if (args[5] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[4], (INT1 *) args[5],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[7] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }

            }

            i4RetStatus = PbbBvlanMapping (CliHandle,
                                           pau1MemberPorts, i4BvlanId);

            break;

        case CLI_PBB_ISID_PORTS:
            /* args[0] - IfType
             * args[1] - Interface List
             * args[2] - IfType
             * args[3] - Interface List
             * args[4] - Internal
             * args[5] - Internal Interface List
             * args[6] - Port Channel
             * args[7] - Port Channel List
             * args[7] - component type
             */

            CLI_MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);

            if (args[1] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[0], (INT1 *) args[1],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }
            if (args[3] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }
            if (args[5] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[4], (INT1 *) args[5],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[7] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }

            }

            i4RetStatus = PbbIsidMemberPorts (CliHandle,
                                              pau1MemberPorts,
                                              CLI_PTR_TO_I4 (args[8]));

            break;
        case CLI_PBB_PORT_PCP_DECODE:
            i4RetStatus = PbbSetPcpDecodingTable (CliHandle,
                                                  CLI_PTR_TO_I4 (args[0]),
                                                  CLI_PTR_TO_I4 (args[1]),
                                                  CLI_PTR_TO_I4 (args[2]),
                                                  CLI_PTR_TO_I4 (args[3]),
                                                  CLI_PTR_TO_I4 (args[4]));
            break;

        case CLI_PBB_PORT_PCP_ENCODE:
            i4RetStatus = PbbSetPcpEncodingTable (CliHandle,
                                                  CLI_PTR_TO_I4 (args[0]),
                                                  CLI_PTR_TO_I4 (args[1]),
                                                  CLI_PTR_TO_I4 (args[2]),
                                                  CLI_PTR_TO_I4 (args[3]),
                                                  CLI_PTR_TO_I4 (args[4]));

            break;
        case CLI_PBB_PORT_NO_PCP_DECODE:
            i4RetStatus = PbbResetPcpDecodingTable (CliHandle,
                                                    CLI_PTR_TO_I4 (args[0]),
                                                    CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_PBB_PORT_NO_PCP_ENCODE:
            i4RetStatus = PbbResetPcpEncodingTable (CliHandle,
                                                    CLI_PTR_TO_I4 (args[0]),
                                                    CLI_PTR_TO_I4 (args[1]));
            break;
        case CLI_PBB_PORT_REQ_DROP_ENC:
            i4RetStatus = PbbSetPortReqDropEncoding (CliHandle,
                                                     CLI_PTR_TO_I4 (args[0]),
                                                     CLI_PTR_TO_I4 (args[1]));

            break;

        case CLI_PBB_PORT_PCP_SEL_ROW:
            i4RetStatus = PbbSetPortPcpSelRow (CliHandle,
                                               CLI_PTR_TO_I4 (args[0]),
                                               CLI_PTR_TO_I4 (args[1]));
            break;

        case CLI_PBB_PORT_USE_DEI:
            i4RetStatus = PbbSetPortUseDei (CliHandle, CLI_PTR_TO_I4 (args[0]),
                                            CLI_PTR_TO_I4 (args[1]));
            break;
        case CLI_PBB_MAP_BACKBONE_INSTANCE:
            i4RetStatus = PbbCreateBackboneInstanceMapping (CliHandle,
                                                            u4ContextId,
                                                            (UINT1 *) args[0]);
            break;
        case CLI_PBB_BACKBONE_INSTANCE:
            /* args[0] - PBB instance name */
            /* args[1] - PBB instance MAC address */
            StrToMac ((UINT1 *) args[1], au1InMacAddr);
            i4RetStatus = PbbCreateBackboneInstance (CliHandle,
                                                     (UINT1 *) args[0],
                                                     au1InMacAddr);
            break;
        case CLI_PBB_NO_BACKBONE_INSTANCE:
            i4RetStatus = PbbDeleteBackboneInstance (CliHandle,
                                                     (UINT1 *) args[0]);
            break;
        case CLI_PBB_SERVICE_TYPE:
            /* args[2] - IfType
             * args[3] - Interface List
             * args[4] - IfType
             * args[5] - Interface List
             * args[6] - Internal
             * args[7] - Internal Interface List
             * args[8] - Port Channel
             * args[9] - Port Channel List
             */

            CLI_MEMSET (pau1MemberPorts, 0, PBB_IFPORT_LIST_SIZE);

            if (args[3] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[2], (INT1 *) args[3],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }
            if (args[5] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[4], (INT1 *) args[5],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }
            if (args[7] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[6], (INT1 *) args[7],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port(s) \r\n");
                    break;
                }

            }

            /* Check if portchannel ports are given */
            if (args[9] != NULL)
            {
                i4RetStatus =
                    CfaCliGetIfList ((INT1 *) args[8], (INT1 *) args[9],
                                     pau1MemberPorts, PBB_IFPORT_LIST_SIZE);
                if (i4RetStatus == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Invalid Port"
                               " Channel Port(s)\r\n");
                    break;
                }

            }
            i4RetStatus =
                PbbSetServiceType (CliHandle, u4ContextId,
                                   CLI_PTR_TO_I4 (args[0]),
                                   CLI_PTR_TO_I4 (args[1]), pau1MemberPorts);
        case PBB_CLI_DEBUG_SHOW:

            i4RetStatus = PbbCliShowDebugging (CliHandle);
            break;

        case PBB_CLI_DEBUG:

            i4RetStatus = PbbCliUpdTraceInput (CliHandle, (UINT1 *) (args[0]));
            break;
        default:
            /* Given command does not match with any SET command. */
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            PbbReleaseContext ();
            PBB_UNLOCK ();
            FsUtilReleaseBitList (pau1MemberPorts);
            CliUnRegisterLock (CliHandle);
            return CLI_FAILURE;
    }
    FsUtilReleaseBitList (pau1MemberPorts);
    PbbReleaseContext ();

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_PBB_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", PbbCliErrString[u4ErrCode]);
        }
    }
    CLI_SET_ERR (0);

    CLI_SET_CMD_STATUS (i4RetStatus);
    PBB_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : cli_process_pbb_show_cmd                           */
/*                                                                           */
/*     DESCRIPTION      : CLI's main process handler for show commands       */
/*                                                                           */
/*     INPUT            : CliHandle   - handle of CLI                        */
/*                                                                           */
/*     OUTPUT           : u4Command   - cli show command                     */
/*                                                                           */
/*     RETURNS          : SUCCESS or FAILURE                                 */
/*                                                                           */
/*****************************************************************************/

INT4
cli_process_pbb_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[PBB_MAX_ARGS];
    INT1                argno = PBB_INIT_VAL;
    UINT4               u4ErrCode;
    UINT4               u4IfIndex = PBB_INIT_VAL;
    INT4                i4RetStatus = PBB_INIT_VAL;
    INT4                i4Inst = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4TempContextId = PBB_CLI_INVALID_CONTEXT;
    UINT2               u2LocalPortId = PBB_INIT_VAL;
    UINT1              *pu1ContextName = NULL;

    if (PbbIsShutDown () == PBB_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\n\r%% PBB switching is shutdown\r\n");
        return CLI_FAILURE;
    }
    CliRegisterLock (CliHandle, PbbLock, PbbUnLock);

    PBB_LOCK ();

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing 
     * IfIndex as the first argument in variable argument list. Like that 
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    /* Walk through the rest of the arguements and store in args array. 
     * Store 16 arguements at the max. This is because pbb commands do not
     * take more than sixteen inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == PBB_MAX_ARGS)
            break;
    }
    va_end (ap);
    if (u4Command == CLI_PBB_SHOW_OUI)
        i4RetStatus = PbbCliShowOui (CliHandle);
    else if (u4Command == CLI_PBB_SHOW_PORT_PCP_ENCODE)
        i4RetStatus =
            PbbShowPcpEncodingTable (CliHandle, u4IfIndex, pu1ContextName);
    else if (u4Command == CLI_PBB_SHOW_PORT_PCP_DECODE)
        i4RetStatus =
            PbbShowPcpDecodingTable (CliHandle, u4IfIndex, pu1ContextName);
    else if (u4Command == CLI_PBB_SHOW_PORT_CONFIG)
        i4RetStatus = PbbShowPortConfig (CliHandle, u4IfIndex, pu1ContextName);
    else if (u4Command == CLI_PBB_SHOW_BACKBONE_INSTANCE)
        i4RetStatus =
            PbbShowBackboneInstanceConfig (CliHandle, (UINT1 *) args[0]);

    else
    {
        while (PbbCliGetContextForShowCmd
               (CliHandle, pu1ContextName, u4IfIndex,
                u4TempContextId, &u4ContextId, &u2LocalPortId) == PBB_SUCCESS)
        {
            switch (u4Command)
            {
                case CLI_PBB_SHOW_ISID:
                    if (args[0] == NULL)
                        i4RetStatus = PbbShowIsid (CliHandle, 0, u4ContextId);
                    else
                        i4RetStatus =
                            PbbShowIsid (CliHandle,
                                         *(UINT4 *) (VOID *) (args[0]),
                                         u4ContextId);
                    break;

                case CLI_PBB_SHOW_PISID:
                    i4RetStatus = PbbShowPisid (CliHandle, u4ContextId,
                                                u4IfIndex);
                    break;
                case CLI_PBB_SHOW_BACKBONE_MAP:
                    i4RetStatus =
                        PbbShowBackboneInstanceMap (CliHandle, u4ContextId);
                    break;

                default:
                    /* Given command does not match with 
                       any of the SHOW commands */
                    CliPrintf (CliHandle, "\r%% Unknown command \r\n");

                    PBB_UNLOCK ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;

            }

            /* If SwitchName or Interface is given as input for show command 
             * then we have to come out of the Loop */
            if ((pu1ContextName != NULL) || (u4IfIndex != 0))
            {
                break;
            }
            u4TempContextId = u4ContextId;
        }                        /*end of while */
    }                            /*end of if-else CLI_PBB_SHOW_OUI */

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_PBB_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", PbbCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    PBB_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbGetVcmIsidCfgPrompt                             */
/*                                                                           */
/*     DESCRIPTION      : This function Checks for isid validity and returns */
/*                        the prompt to be displayed.                        */
/*                                                                           */
/*     INPUT            : pi1ModeName - Mode to be configured.               */
/*                                                                           */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.              */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT1
PbbGetVcmIsidCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index;
    UINT4               u4Len = STRLEN (CLI_PBB_MODE);

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_PBB_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4Index = CLI_ATOI (pi1ModeName);

    CLI_SET_ISID (u4Index);

    STRCPY (pi1DispStr, "(config-switch-si)#");

    return TRUE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbDeleteISID                                      */
/*                                                                           */
/*     DESCRIPTION      : This function will delete isid                     */
/*                                                                           */
/*     INPUT            : u4Isid   - Service Identifier                      */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbDeleteISID (tCliHandle CliHandle, UINT4 u4Isid)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;
    UINT4               u4Vip = PBB_INIT_VAL;
    UINT4               u4ErrCode = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    INT4                i4RetStatus = PBB_INIT_VAL;

    u4ContextId = CLI_GET_CXT_ID ();

    if (PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId) != PBB_FAILURE)
    {
        PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                       (INT4 *) &u4Vip);
        /* VIP-ISID Rowstatus = DESTROY */
        if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode, u4ContextId, u4Vip,
                                        PBB_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /*unmapping of contextId with VIP */
        if (SNMP_SUCCESS != PbbTestv2FsVcIfRowStatus (&u4ErrCode,
                                                      (INT4) u4Vip,
                                                      PBB_DESTROY))
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                     PBB_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (PbbSetFsVcIfRowStatus ((INT4) u4Vip,
                                   (INT4) PBB_DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Failed to Destroy IfMap Entry\r\n");
            return CLI_FAILURE;
        }

        /* port deletion of VIP */

        if (PbbTestv2IfMainRowStatus (&u4ErrCode, u4Vip,
                                      PBB_DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to delete interface\r\n");
            return (CLI_FAILURE);
        }

        if (PbbSetIfMainRowStatus (u4Vip, PBB_DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Failed to Destroy IfEntry\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        tSNMP_OCTET_STRING_TYPE SnmpPorts;
        UINT1              *pPortList1 = NULL;
        UINT2               u2ByteIndex = PBB_INIT_VAL;
        UINT2               u2BitIndex = PBB_INIT_VAL;
        UINT1               u1PortFlag = PBB_INIT_VAL;

        u2LocalPortId = 0;
        pPortList1 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pPortList1 == NULL)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "PbbDeleteISID: Error in allocating memory "
                     "for pPortList1\r\n");
            return CLI_FAILURE;
        }

        /*Delete all previous CBPs mapped to this isid */
        CLI_MEMSET (pPortList1, 0, sizeof (tLocalPortList));
        SnmpPorts.pu1_OctetList = pPortList1;
        SnmpPorts.i4_Length = sizeof (tLocalPortList);
        i4RetStatus = PbbGetCbpForIsid (u4ContextId, u4Isid, &SnmpPorts);

        if (i4RetStatus != (INT4) PBB_FAILURE)
        {
            for (u2ByteIndex = 0; u2ByteIndex < PBB_PORT_LIST_SIZE;
                 u2ByteIndex++)
            {
                u1PortFlag = SnmpPorts.pu1_OctetList[u2ByteIndex];
                for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & PBB_BIT8) != 0)
                    {
                        u2LocalPortId = (UINT4)
                            ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                             u2BitIndex + 1);
                        PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                       u2LocalPortId,
                                                       (INT4 *) &u4Port);

                        if (nmhTestv2FsPbbCBPServiceMappingRowStatus
                            (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                             PBB_DESTROY) == SNMP_FAILURE)
                        {
                            UtilPlstReleaseLocalPortList (pPortList1);
                            return CLI_FAILURE;
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }
            for (u2ByteIndex = 0; u2ByteIndex < PBB_PORT_LIST_SIZE;
                 u2ByteIndex++)
            {
                u1PortFlag = SnmpPorts.pu1_OctetList[u2ByteIndex];
                for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & PBB_BIT8) != 0)
                    {
                        u2LocalPortId = (UINT4)
                            ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                             u2BitIndex + 1);
                        PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                       u2LocalPortId,
                                                       (INT4 *) &u4Port);
                        if (nmhSetFsPbbCBPServiceMappingRowStatus
                            (u4ContextId, u4Port, u4Isid,
                             PBB_DESTROY) == SNMP_FAILURE)
                        {
                            UtilPlstReleaseLocalPortList (pPortList1);
                            return CLI_FAILURE;
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }
        }
        UtilPlstReleaseLocalPortList (pPortList1);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbISIDMode                                        */
/*                                                                           */
/*     DESCRIPTION      : This function will enter into isid database config */
/*                        mode.                                              */
/*                                                                           */
/*     INPUT            : u4Isid   - Service Identifier                      */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbISIDMode (tCliHandle CliHandle, UINT4 u4Isid)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4BridgeMode = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    INT4                i4ContextId = PBB_INIT_VAL;
    INT4                i4RetValFsPbbShutdownStatus = PBB_INIT_VAL;
    CLI_MEMSET (au1Cmd, PBB_INIT_VAL, MAX_PROMPT_LEN);
    i4ContextId = CLI_GET_CXT_ID ();
    if (i4ContextId == CLI_ERROR)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    u4ContextId = (UINT4) i4ContextId;
    if (nmhGetFsPbbShutdownStatus (&i4RetValFsPbbShutdownStatus) ==
        SNMP_SUCCESS)
    {
        if (i4RetValFsPbbShutdownStatus == PBB_SNMP_TRUE)
        {
            CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
            return CLI_FAILURE;
        }
        if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
        {
            u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;
        }

        if (u4BridgeMode != PBB_ICOMPONENT_BRIDGE_MODE &&
            u4BridgeMode != PBB_BCOMPONENT_BRIDGE_MODE)
        {
            CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
            return CLI_FAILURE;

        }
    }
    else
    {
        return CLI_FAILURE;
    }
    /* ENTER ISID Mode */
    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_PBB_MODE, u4Isid);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "/r%% Unable to enter into service instance \
                configuration mode\r\n");
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCliSelectContextOnMode                        */
/*                                                                           */
/*    Description         : This function is used to check the Mode of       */
/*                          the command and also if it a Config Mode         */
/*                          command it will do SelectContext for the         */
/*                          Context.                                         */
/*                                                                           */
/*    Input(s)            : u4Cmd - CLI Command.                             */
/*                                                                           */
/*    Output(s)           : CliHandle - Contains error messages.             */
/*                          pu4ContextId - Context Id.                       */
/*                          pu4LocalPort - Local Port Number.                */
/*                          pu2Flag      - Show command or Not.              */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PbbCliSelectContextOnMode (tCliHandle CliHandle, UINT4 u4Cmd,
                           UINT4 *pu4Context, UINT4 *pu4LocalPort)
{
    UINT4               u4ContextId;
    INT4                i4PortId = 0;
    UINT1               u1IntfCmdFlag = PBB_FALSE;
    /* This flag is used in MI case, to know whether 
       the command is a interface mode command 
       or pbb mode command. */
    *pu4Context = PBB_DEF_CONTEXT_ID;

    /* Get the Context-Id from the pCliContext structure */
    u4ContextId = CLI_GET_CXT_ID ();

    /* Check if the command is a switch mode command */
    if (u4ContextId != PBB_CLI_INVALID_CONTEXT)
    {
        /* Switch-mode Command */
        *pu4Context = u4ContextId;
    }
    else
    {
        /* This flag (u1IntfCmdFlag) is used only in case of MI. 
         * If the command is a pbb mode command then the context-id won't be
         * PBB_CLI_INVALID_CONTEXT, So this is an interface mode command.
         * Now by refering this flag we have to get the context-id and 
         * local port number from the IfIndex (CLI_GET_IFINDEX). */
        u1IntfCmdFlag = PBB_TRUE;
    }

    i4PortId = CLI_GET_IFINDEX ();

    /* In SI both the i4PortId and Localport are same. So no need to call
     * pbbGetContextInfoFromIfIndex. */
    *pu4LocalPort = (UINT4) i4PortId;
    if (PbbVcmGetSystemMode (PBB_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (u1IntfCmdFlag == PBB_TRUE)
        {
            /* This is an Interface Mode command
             * Get the context Id from the IfIndex */
            if (i4PortId != -1)
            {
                if (VcmGetContextInfoFromIfIndex
                    ((UINT2) i4PortId, pu4Context, (UINT2 *) pu4LocalPort)
                    != PBB_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r%% Interface not mapped to "
                               "any of the context.\r\n ");
                    return PBB_FAILURE;
                }
            }
        }
    }

    UNUSED_PARAM (u4Cmd);

    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbSetPisidOnPort                                  */
/*                                                                           */
/*     DESCRIPTION      : This function configures/re-sets port's PISID      */
/*                                                                           */
/*     INPUT            : u4Pisid  - PISID                                   */
/*                        u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbSetPisidOnPort (tCliHandle CliHandle, UINT4 u4Pisid, UINT4 u4PortId)
{
    UINT4               u4ErrCode = PBB_INIT_VAL;
    INT4                i4PrevPisid = PBB_INIT_VAL;
    INT4                i4RetValFsPbbPIsidRowStatus = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;
    UINT1               u1isRowCreated = PBB_TRUE;
    UINT1               u1Bool = PBB_TRUE;

    if (PbbVcmGetContextInfoFromIfIndex (u4PortId, &u4ContextId, &u2LocalPortId)
        == PBB_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsPbbPIsidRowStatus (u4ContextId, u4PortId,
                                   &i4RetValFsPbbPIsidRowStatus) ==
        SNMP_FAILURE)
    {
        /* PISID NOT present - CREATE */
        if (nmhTestv2FsPbbPIsidRowStatus (&u4ErrCode, u4ContextId, u4PortId,
                                          PBB_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* CREATE PISID */
        if (nmhSetFsPbbPIsidRowStatus (u4ContextId, u4PortId,
                                       PBB_CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        u1isRowCreated = PBB_TRUE;
    }
    else
    {
        if (i4RetValFsPbbPIsidRowStatus == PBB_ACTIVE)
        {
            if (nmhTestv2FsPbbPIsidRowStatus (&u4ErrCode, u4ContextId, u4PortId,
                                              PBB_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* MODIFY PISID */
            if (nmhSetFsPbbPIsidRowStatus (u4ContextId, u4PortId,
                                           PBB_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        u1isRowCreated = PBB_FALSE;
    }

    if (u1isRowCreated != PBB_TRUE)
    {
        if (nmhGetFsPbbPortPisid (u4ContextId, u4PortId,
                                  &i4PrevPisid) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsPbbPortPisid (&u4ErrCode, u4ContextId, u4PortId, u4Pisid)
        == SNMP_FAILURE)
    {
        u1Bool = PBB_FALSE;
    }
    else if (nmhSetFsPbbPortPisid (u4ContextId, u4PortId, u4Pisid)
             == SNMP_FAILURE)
    {
        u1Bool = PBB_FALSE;
    }

    if (u1Bool != PBB_FALSE)
    {
        /* ACTIVE PISID row status */
        if (nmhTestv2FsPbbPIsidRowStatus (&u4ErrCode, u4ContextId, u4PortId,
                                          PBB_ACTIVE) == SNMP_FAILURE)
        {
            u1Bool = PBB_FALSE;
        }
        if (u1Bool != PBB_FALSE)
            if (nmhSetFsPbbPIsidRowStatus (u4ContextId, u4PortId, PBB_ACTIVE)
                == SNMP_FAILURE)
            {
                u1Bool = PBB_FALSE;
            }
    }
    if (u1Bool == PBB_FALSE)
    {                            /*Rollback */
        if (u1isRowCreated == PBB_TRUE)
        {                        /*destroy row status */
            if (nmhTestv2FsPbbPIsidRowStatus (&u4ErrCode, u4ContextId,
                                              u4PortId,
                                              PBB_DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsPbbPIsidRowStatus (u4ContextId, u4PortId,
                                           PBB_DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

        }
        else
        {                        /*rollback pisid */
            if (nmhTestv2FsPbbPortPisid (&u4ErrCode,
                                         u4ContextId, u4PortId, i4PrevPisid)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsPbbPortPisid (u4ContextId,
                                      u4PortId, i4PrevPisid) == SNMP_FAILURE)
            {
                return CLI_FAILURE;

            }
            /*rollback rowstatus */
            if (i4RetValFsPbbPIsidRowStatus == PBB_ACTIVE)
            {
                if (nmhTestv2FsPbbPIsidRowStatus (&u4ErrCode,
                                                  u4ContextId, u4PortId,
                                                  i4RetValFsPbbPIsidRowStatus)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsPbbPIsidRowStatus (u4ContextId, u4PortId,
                                               i4RetValFsPbbPIsidRowStatus) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }

        }

    }
    if (u1Bool == PBB_FALSE)
        return CLI_FAILURE;

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbDelPortPisid                                    */
/*                                                                           */
/*     DESCRIPTION      : This function deletes port's PISID                 */
/*                                                                           */
/*     INPUT            : u4PortId  - Port identifier                        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbDelPortPisid (tCliHandle CliHandle, UINT4 u4PortId)
{
    UINT4               u4ErrCode = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    if (PbbVcmGetContextInfoFromIfIndex (u4PortId, &u4ContextId, &u2LocalPortId)
        == PBB_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2FsPbbPIsidRowStatus (&u4ErrCode, u4ContextId, u4PortId,
                                      PBB_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* DELETE PISID */
    if (nmhSetFsPbbPIsidRowStatus (u4ContextId, u4PortId, PBB_DESTROY)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbSetDestMacAddr                                  */
/*                                                                           */
/*     DESCRIPTION      : This function configures per ISID's DestMacAddr    */
/*                                                                           */
/*     INPUT            : DestinationMacAddr                                 */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbSetDestMacAddr (tCliHandle CliHandle, UINT4 u4IfIndex,
                   UINT1 *SetValFsPbbDestinationMacAddr)
{
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UINT1              *pPortList1 = NULL;
    UINT4               u4ErrCode = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4Vip = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    INT4                i4RetStatus = PBB_INIT_VAL;
    INT4                i4RetValFsPbbVipRowStatus = PBB_INIT_VAL;
    UINT2               u2LocalPortId = 0;
    UINT1               u1bool = PBB_TRUE;
    BOOL1               bResult = OSIX_FALSE;

    u4Isid = CLI_GET_ISID ();
    u4ContextId = CLI_GET_CXT_ID ();

    i4RetStatus = PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId);

    if (i4RetStatus == PBB_SUCCESS)
    {                            /*I component - VIP entry */
        if (u4IfIndex != 0)
        {
            CliPrintf (CliHandle, "\r%% Port option has to be used only in"
                       " B-Component \r\n");
            return CLI_FAILURE;
        }

        PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                       (INT4 *) &u4Vip);
        if (nmhGetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                     &i4RetValFsPbbVipRowStatus) ==
            SNMP_FAILURE)
        {
            /*Vip RS not present - return failure */
            return CLI_FAILURE;
        }
        if (i4RetValFsPbbVipRowStatus == PBB_ACTIVE)
        {
            if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode, u4ContextId, u4Vip,
                                            PBB_NOT_IN_SERVICE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip, PBB_NOT_IN_SERVICE)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        /* MODIFY DefaultDstBMAC */
        if (nmhTestv2FsPbbVipDefaultDstBMAC (&u4ErrCode, u4ContextId,
                                             u4Vip,
                                             SetValFsPbbDestinationMacAddr)
            == SNMP_FAILURE)
        {
            u1bool = PBB_FALSE;
        }
        if (u1bool == PBB_TRUE)
        {
            if (nmhSetFsPbbVipDefaultDstBMAC (u4ContextId, u4Vip,
                                              SetValFsPbbDestinationMacAddr)
                == SNMP_FAILURE)
            {
                u1bool = PBB_FALSE;
            }
        }

        /*set to previous row status */
        if (i4RetValFsPbbVipRowStatus == PBB_ACTIVE)
        {
            if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode, u4ContextId, u4Vip,
                                            i4RetValFsPbbVipRowStatus)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                         i4RetValFsPbbVipRowStatus)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        if (u1bool == PBB_FALSE)
            return CLI_FAILURE;
    }
    else
    {                            /*B component - CBP entry */
        if (u4IfIndex == 0)        /* No specific interface is mentioned */
        {
            pPortList1 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

            if (pPortList1 == NULL)
            {
                PBB_TRC (ALL_FAILURE_TRC,
                         "PbbSetDestMacAddr : Error in allocating memory "
                         "for pPortList1\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pPortList1, 0, sizeof (tLocalPortList));
            SnmpPorts.pu1_OctetList = pPortList1;
            SnmpPorts.i4_Length = sizeof (tLocalPortList);

            i4RetStatus = PbbGetCbpForIsid (u4ContextId, u4Isid, &SnmpPorts);

            if (i4RetStatus != PBB_SUCCESS)
            {
                UtilPlstReleaseLocalPortList (pPortList1);
                return CLI_FAILURE;
            }

            for (u2LocalPortId = 1; u2LocalPortId <= PBB_MAX_PORTS;
                 u2LocalPortId++)
            {
                OSIX_BITLIST_IS_BIT_SET (SnmpPorts.pu1_OctetList,
                                         u2LocalPortId, PBB_PORT_LIST_SIZE,
                                         bResult);
                if (bResult == OSIX_FALSE)
                {
                    continue;
                }

                PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                               (INT4 *) &u4Port);

                if (PbbSetDefaultDestMacInServiceMapEntry
                    (u4ContextId, u4Port, u4Isid,
                     SetValFsPbbDestinationMacAddr) == PBB_FAILURE)
                {
                    UtilPlstReleaseLocalPortList (pPortList1);
                    return CLI_FAILURE;
                }
            }                    /*end  of else bcomp */
        }
        else
        {                        /* Specific interface is given, configure for that interface alone */

            if (PbbSetDefaultDestMacInServiceMapEntry
                (u4ContextId, u4IfIndex, u4Isid,
                 SetValFsPbbDestinationMacAddr) == PBB_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbIsidMemberPorts                                 */
/*                                                                           */
/*     DESCRIPTION      : This function sets member ports for an isid        */
/*                                                                           */
/*     INPUT            : MemberPortList - member ports list                 */
/*                        i4ComponentType    -  icomp/bcomp                  */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbIsidMemberPorts (tCliHandle CliHandle,
                    tPortList MemberPortList, INT4 i4ComponentType)
{
    UINT4               u4ErrCode = PBB_NO_ERROR;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    INT4                i4ContextId = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INIT_VAL;
    UINT4               u4Vip = PBB_INIT_VAL;
    INT1                i1IsVipCreated = PBB_FALSE;
    INT4                i4RetValFsPbbVipRowStatus = PBB_INVALID_ROWSTATUS;
    INT4                i4Result = PBB_SUCCESS;
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    u4Isid = CLI_GET_ISID ();
    i4ContextId = CLI_GET_CXT_ID ();
    if (i4ContextId == CLI_ERROR)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    u4ContextId = (UINT4) i4ContextId;
    if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
    {
        u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;

        if (i4ComponentType == PBB_ICOMPONENT &&
            u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
        {
            CliPrintf (CliHandle,
                       "\r%% PBB: Bridge mode is PBB B-component\r\n");
            return CLI_FAILURE;
        }
        else if (i4ComponentType == PBB_BCOMPONENT &&
                 u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            CliPrintf (CliHandle,
                       "\r%% PBB: Bridge mode is PBB I-component\r\n");
            return CLI_FAILURE;
        }
    }

    if (i4ComponentType == PBB_ICOMPONENT)
    {
        if (PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId) != PBB_FAILURE)
        {                        /*existing isid */
            /*test VIP not ready to change isid */
            PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                           u2LocalPortId, (INT4 *) &u4Vip);
            if (nmhGetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                         &i4RetValFsPbbVipRowStatus) ==
                SNMP_FAILURE)
            {
                /*Vip RS not present - return failure */
                return CLI_FAILURE;
            }
            else
            {
                if (i4RetValFsPbbVipRowStatus == PBB_ACTIVE)
                {
                    if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode,
                                                    u4ContextId, u4Vip,
                                                    PBB_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }

                    /*set VIP not ready to change isid */
                    if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                                 PBB_NOT_IN_SERVICE)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
            }
            i1IsVipCreated = PBB_FALSE;
        }
        else
        {                        /*new isid */

            i4Result = PbbGetFreeVipPort (u4ContextId, u4Isid, &u2LocalPortId);
            if (i4Result == PBB_SUCCESS)
            {
                PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                               (INT4 *) &u4Vip);

                if (nmhGetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                             &i4RetValFsPbbVipRowStatus) !=
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                /* VIP NOT present - CREATE */
                if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode, u4ContextId, u4Vip,
                                                PBB_CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                /* CREATE VIP */
                if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                             PBB_CREATE_AND_WAIT)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                i1IsVipCreated = PBB_TRUE;
            }
        }
        i4Result = PbbCreatePipIsidMapping (CliHandle, MemberPortList,
                                            u4Vip, u4Isid);

        if (i4Result == PBB_SUCCESS)
        {
            /*set ISID inside VIP table */
            if (nmhTestv2FsPbbVipISid (&u4ErrCode, u4ContextId, u4Vip,
                                       u4Isid) == SNMP_FAILURE)
            {
                i4Result = PBB_FAILURE;
            }

            else if (nmhSetFsPbbVipISid (u4ContextId, u4Vip,
                                         u4Isid) == SNMP_FAILURE)
            {
                i4Result = PBB_FAILURE;
            }
        }
        if (i4Result == PBB_FAILURE)
        {                        /*Rollback VIP creation */
            if (i1IsVipCreated == PBB_TRUE)
            {
                /*destroy VIP */
                /* VIP-ISID Rowstatus = DESTROY */
                if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode, u4ContextId, u4Vip,
                                                PBB_DESTROY) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                             PBB_DESTROY) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                /*unmapping of contextId with VIP */
                if (PbbTestv2FsVcIfRowStatus (&u4ErrCode,
                                              (INT4) u4Vip,
                                              PBB_DESTROY) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (PbbSetFsVcIfRowStatus ((INT4) u4Vip,
                                           (INT4) PBB_DESTROY) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Failed to Destroy IfMap Entry\r\n");
                    return CLI_FAILURE;
                }
                /* port deletion of VIP */

                if (PbbTestv2IfMainRowStatus (&u4ErrCode, u4Vip,
                                              PBB_DESTROY) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to delete interface\r\n");
                    return (CLI_FAILURE);
                }

                if (PbbSetIfMainRowStatus (u4Vip, PBB_DESTROY) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Failed to Destroy IfEntry\r\n");
                    return CLI_FAILURE;
                }
            }
            else
            {
                /*Rollback VIP RS to prev value */
                if (i4RetValFsPbbVipRowStatus == PBB_ACTIVE)
                {
                    if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode,
                                                    u4ContextId, u4Vip,
                                                    i4RetValFsPbbVipRowStatus)
                        == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }

                    /*Rollback VIP RS to prev value */
                    if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                                 i4RetValFsPbbVipRowStatus)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
            }
            return CLI_FAILURE;
        }                        /*end of if rollback required */
        else
        {
            if (i1IsVipCreated == PBB_FALSE)
            {
                if (i4RetValFsPbbVipRowStatus == PBB_ACTIVE)
                {

                    if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode,
                                                    u4ContextId, u4Vip,
                                                    i4RetValFsPbbVipRowStatus)
                        == SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }

                    if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                                 i4RetValFsPbbVipRowStatus)
                        == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
            }
            /*else - active of vip will be done in isid active */
        }
    }
    else
    {
        i4Result =
            PbbCreateCbpIsidMapping (MemberPortList, u4Isid, u4ContextId);
        if (i4Result == PBB_SUCCESS)
            return CLI_SUCCESS;
        else
            return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbCreateCbpIsidMapping                            */
/*                                                                           */
/*     DESCRIPTION      : This function create cbp to isid mapping           */
/*                                                                           */
/*     INPUT            : MemberPortList - member ports list                 */
/*                        u4Isid - isid                                      */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : PBB_SUCCESS/PBB_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCreateCbpIsidMapping (tPortList MemberPortList,
                         UINT4 u4Isid, UINT4 u4ContextId)
{
    UINT4               u4ErrCode = PBB_NO_ERROR;
    UINT4               u4Port = PBB_INIT_VAL;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    INT4                i4RetValFsPbbCBPServiceMappingRowStatus = PBB_INIT_VAL;
    INT4                i4RetStatus = PBB_INIT_VAL;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UINT1              *pPortList1 = NULL;
    UINT4               u4LocalPortId = PBB_INIT_VAL;
    UINT4               u4NumOfPorts = PBB_INIT_VAL;
    UINT4               u4NumOfCbpPorts = PBB_INIT_VAL;
    UINT4               u4MaxNumofPortPerIsidPerContext = PBB_INIT_VAL;
    UINT4               u4MaxNumOfPortsPerIsid = PBB_INIT_VAL;

    /* Get the number of ports that are to be mapped with this Isid */
    for (u2ByteIndex = 0; u2ByteIndex < PBB_IFPORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = MemberPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & PBB_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                u4NumOfPorts++;
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }                        /*end of for2 */
    }                            /*end of for1 */

    pPortList1 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

    if (pPortList1 == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "PbbCreateCbpIsidMapping : Error in allocating memory "
                 "for pPortList1\r\n");
        return CLI_FAILURE;
    }

    CLI_MEMSET (pPortList1, 0, sizeof (tLocalPortList));
    SnmpPorts.pu1_OctetList = pPortList1;
    SnmpPorts.i4_Length = sizeof (tLocalPortList);

    i4RetStatus = PbbGetCbpForIsid (u4ContextId, u4Isid, &SnmpPorts);
    if (i4RetStatus != PBB_FAILURE)
    {
        /* Get the number of CBP ports that are mapped with this Isid */
        for (u2ByteIndex = 0; u2ByteIndex < PBB_PORT_LIST_SIZE; u2ByteIndex++)
        {
            u1PortFlag = SnmpPorts.pu1_OctetList[u2ByteIndex];
            for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & PBB_BIT8) != 0)
                {
                    u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                      u2BitIndex + 1);
                    u4NumOfCbpPorts++;
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }                    /*end of for2 */
        }                        /*end of for1 */

        PbbGetMaxCurrPortPerISIDPerCntxt ((INT4 *)
                                          &u4MaxNumofPortPerIsidPerContext);
        if (u4NumOfPorts > u4MaxNumofPortPerIsidPerContext)
        {
            CLI_SET_ERR (CLI_PBB_PORT_PER_ISID_CTXT_EXCEED);
            UtilPlstReleaseLocalPortList (pPortList1);
            return CLI_FAILURE;
        }
        PbbGetMaxCurrPortsPerISID ((INT4 *) &u4MaxNumOfPortsPerIsid);
        if (u4NumOfPorts > u4MaxNumOfPortsPerIsid)
        {
            CLI_SET_ERR (CLI_PBB_PORT_PER_ISID_EXCEED);
            UtilPlstReleaseLocalPortList (pPortList1);
            return CLI_FAILURE;
        }

        /*Delete all previous CBPs mapped to this isid */

        for (u2ByteIndex = 0; u2ByteIndex < PBB_PORT_LIST_SIZE; u2ByteIndex++)
        {
            u1PortFlag = SnmpPorts.pu1_OctetList[u2ByteIndex];
            for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & PBB_BIT8) != 0)
                {
                    u4LocalPortId = (UINT4)
                        ((u2ByteIndex * PBB_PORTS_PER_BYTE) + u2BitIndex + 1);
                    PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                   u4LocalPortId,
                                                   (INT4 *) &u4Port);

                    if (nmhTestv2FsPbbCBPServiceMappingRowStatus
                        (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                         PBB_DESTROY) == SNMP_FAILURE)
                    {
                        UtilPlstReleaseLocalPortList (pPortList1);
                        return PBB_FAILURE;
                    }

                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
        for (u2ByteIndex = 0; u2ByteIndex < PBB_PORT_LIST_SIZE; u2ByteIndex++)
        {
            u1PortFlag = SnmpPorts.pu1_OctetList[u2ByteIndex];
            for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                  && (u1PortFlag != 0)); u2BitIndex++)
            {
                if ((u1PortFlag & PBB_BIT8) != 0)
                {
                    u4LocalPortId = (UINT4)
                        ((u2ByteIndex * PBB_PORTS_PER_BYTE) + u2BitIndex + 1);
                    PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                   u4LocalPortId,
                                                   (INT4 *) &u4Port);

                    if (nmhSetFsPbbCBPServiceMappingRowStatus
                        (u4ContextId, u4Port, u4Isid,
                         PBB_DESTROY) == SNMP_FAILURE)
                    {
                        UtilPlstReleaseLocalPortList (pPortList1);
                        return PBB_FAILURE;
                    }

                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
        UtilPlstReleaseLocalPortList (pPortList1);
    }
    /*Delete all previous CBPs mapped to this isid */

    for (u2ByteIndex = 0; u2ByteIndex < PBB_IFPORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = MemberPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & PBB_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                if (nmhGetFsPbbCBPServiceMappingRowStatus
                    (u4ContextId, u4Port, u4Isid,
                     &i4RetValFsPbbCBPServiceMappingRowStatus) == SNMP_FAILURE)
                {
                    if (nmhTestv2FsPbbCBPServiceMappingRowStatus
                        (&u4ErrCode, u4ContextId, u4Port,
                         u4Isid, PBB_CREATE_AND_WAIT) == SNMP_FAILURE)
                    {
                        return PBB_FAILURE;
                    }
                }
                /*else already created - nothing to do */
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }                        /*end of for2 */
    }                            /*end of for1 */

    for (u2ByteIndex = 0; u2ByteIndex < PBB_IFPORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = MemberPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & PBB_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                if (nmhSetFsPbbCBPServiceMappingRowStatus
                    (u4ContextId, u4Port,
                     u4Isid, PBB_CREATE_AND_WAIT) == SNMP_FAILURE)
                {
                    return PBB_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }                        /*end of for2 */
    }                            /*end of for1 */
    /*active will be done in isid active command */
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbCreatePipIsidMapping                            */
/*                                                                           */
/*     DESCRIPTION      : This function create pip to vip mapping            */
/*                                                                           */
/*     INPUT            : MemberPortList - member ports list                 */
/*                        u4Vip  - vip                                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : PBB_SUCCESS/PBB_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCreatePipIsidMapping (tCliHandle CliHandle,
                         tPortList MemberPortList, UINT4 u4Vip, UINT4 u4Isid)
{
    UINT4               u4ErrCode = PBB_NO_ERROR;
    UINT4               u4ContextId = PBB_INIT_VAL;
    INT4                i4PrevPipPort = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    INT4                i4NumPip = PBB_INIT_VAL;
    INT1                i1Bool = TRUE;
    UINT4               u4PipPort = PBB_INIT_VAL;
    INT1                i1CreatePipVipMap = PBB_INIT_VAL;
    INT4                i4RetValFsPbbVipToPipMappingRowStatus;

    u4ContextId = CLI_GET_CXT_ID ();

    UNUSED_PARAM (u4Isid);
    for (u2ByteIndex = 0; u2ByteIndex < PBB_IFPORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = MemberPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & PBB_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                u4PipPort = u4Port;

                i4NumPip++;
                if (i4NumPip > 1)
                {
                    CliPrintf (CliHandle, "\r%%More than one port for an \
                            I-component cannot be given\r\n", u4Isid);
                    return PBB_FAILURE;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }                        /*end of for2 */
    }                            /*end of for1 */

    if (i4NumPip == 0)
    {                            /*delete PIP-ISID mapping */
        if (nmhTestv2FsPbbVipToPipMappingRowStatus (&u4ErrCode, u4ContextId,
                                                    u4Vip,
                                                    PBB_DESTROY) ==
            SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }
        if (nmhSetFsPbbVipToPipMappingRowStatus (u4ContextId, u4Vip,
                                                 PBB_DESTROY) == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }
    }
    else
    {
        /*get rowstatus of vip to pip mapping */
        if (nmhGetFsPbbVipToPipMappingRowStatus (u4ContextId, u4Vip,
                                                 &i4RetValFsPbbVipToPipMappingRowStatus)
            == SNMP_FAILURE)
        {
            /*create rowstatus of vip to pip mapping */
            if (nmhTestv2FsPbbVipToPipMappingRowStatus (&u4ErrCode, u4ContextId,
                                                        u4Vip,
                                                        PBB_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                return PBB_FAILURE;
            }
            if (nmhSetFsPbbVipToPipMappingRowStatus (u4ContextId, u4Vip,
                                                     PBB_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                return PBB_FAILURE;
            }

            i1CreatePipVipMap = PBB_TRUE;
        }
        else
        {
            if (i4RetValFsPbbVipToPipMappingRowStatus == PBB_ACTIVE)
            {
                /*modify rowstatus of vip to pip mapping */
                if (nmhTestv2FsPbbVipToPipMappingRowStatus
                    (&u4ErrCode, u4ContextId, u4Vip,
                     PBB_NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    return PBB_FAILURE;
                }
                if (nmhSetFsPbbVipToPipMappingRowStatus (u4ContextId, u4Vip,
                                                         PBB_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    return PBB_FAILURE;
                }
            }

            i1CreatePipVipMap = PBB_FALSE;

        }

        /*get old VIP to PIP map */
        if (i1CreatePipVipMap != PBB_TRUE)
        {
            if (nmhGetFsPbbVipToPipMappingPipIfIndex (u4ContextId, u4Vip,
                                                      &i4PrevPipPort)
                == SNMP_FAILURE)
            {
                return PBB_FAILURE;
            }
        }

        /*set VIP to PIP map */
        if (nmhTestv2FsPbbVipToPipMappingPipIfIndex (&u4ErrCode, u4ContextId,
                                                     u4Vip,
                                                     u4PipPort) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_PBB_PORT_ISID_PIP_INVALID);
            i1Bool = FALSE;
        }
        else if (nmhSetFsPbbVipToPipMappingPipIfIndex (u4ContextId, u4Vip,
                                                       u4PipPort)
                 == SNMP_FAILURE)
        {
            i1Bool = FALSE;
        }

        if (i1Bool != FALSE)
        {
            /*activate  PIP to VIP map table */
            if (nmhTestv2FsPbbVipToPipMappingRowStatus (&u4ErrCode, u4ContextId,
                                                        u4Vip,
                                                        PBB_ACTIVE) ==
                SNMP_FAILURE)
            {
                i1Bool = FALSE;
            }
            else if (nmhSetFsPbbVipToPipMappingRowStatus (u4ContextId, u4Vip,
                                                          PBB_ACTIVE) ==
                     SNMP_FAILURE)
            {
                i1Bool = FALSE;
            }

        }
        if (i1Bool == FALSE)
        {                        /*Rollback */
            if (i1CreatePipVipMap == PBB_TRUE)
            {                    /*destroy row status */
                if (nmhTestv2FsPbbVipToPipMappingRowStatus
                    (&u4ErrCode, u4ContextId, u4Vip,
                     PBB_DESTROY) == SNMP_FAILURE)
                {
                    return PBB_FAILURE;
                }
                if (nmhSetFsPbbVipToPipMappingRowStatus (u4ContextId, u4Vip,
                                                         PBB_DESTROY) ==
                    SNMP_FAILURE)
                {
                    return PBB_FAILURE;
                }

            }
            else
            {                    /*rollback pip ifindex */
                if (nmhTestv2FsPbbVipToPipMappingPipIfIndex (&u4ErrCode,
                                                             u4ContextId,
                                                             u4Vip,
                                                             i4PrevPipPort) ==
                    SNMP_FAILURE)
                {
                    return PBB_FAILURE;
                }
                if (nmhSetFsPbbVipToPipMappingPipIfIndex (u4ContextId, u4Vip,
                                                          i4PrevPipPort)
                    == SNMP_FAILURE)
                {
                    return PBB_FAILURE;
                }
                /*rollback row status */
                if (i4RetValFsPbbVipToPipMappingRowStatus == PBB_ACTIVE)
                {
                    if (nmhTestv2FsPbbVipToPipMappingRowStatus (&u4ErrCode,
                                                                u4ContextId,
                                                                u4Vip,
                                                                i4RetValFsPbbVipToPipMappingRowStatus)
                        == SNMP_FAILURE)
                    {
                        return PBB_FAILURE;
                    }
                    if (nmhSetFsPbbVipToPipMappingRowStatus (u4ContextId, u4Vip,
                                                             i4RetValFsPbbVipToPipMappingRowStatus)
                        == SNMP_FAILURE)
                    {
                        return PBB_FAILURE;
                    }
                }

            }

        }
        if (i1Bool == FALSE)
            return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbBvlanMapping                                    */
/*                                                                           */
/*     DESCRIPTION      : This function create cbp to bvlan mapping          */
/*                                                                           */
/*     INPUT            : MemberPortList - member ports list of cbps         */
/*                        i4BvlanId - bvlan identifier                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbBvlanMapping (tCliHandle CliHandle, tPortList MemberPortList, INT4 i4BvlanId)
{
    UINT4               u4ErrCode = PBB_NO_ERROR;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    UINT4               u4PortNum = PBB_INIT_VAL;
    tCbpStatusInfo     *pCbpStatus = NULL;
    UINT4               u4CbpIndex = PBB_INIT_VAL;
    INT1                i1Bool = TRUE;
    INT1                i1BvidSet = TRUE;
    INT4                i4RetValCBPRowStatus = PBB_INIT_VAL;

    if (PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                             PbbCbpStatusMemPoolId, pCbpStatus) == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Allocating memory for pCbpStatus \n");
        return CLI_FAILURE;
    }

    PBB_MEMSET (pCbpStatus, 0,
                sizeof (tCbpStatusInfo) * PBB_MAX_PORTS_PER_ISID);
    u4Isid = CLI_GET_ISID ();
    u4ContextId = CLI_GET_CXT_ID ();

    for (u2ByteIndex = 0; u2ByteIndex < PBB_IFPORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = MemberPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & PBB_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                /*Check if CBP already created or not */
                if (nmhGetFsPbbCBPServiceMappingRowStatus
                    (u4ContextId, u4Port, u4Isid,
                     &((pCbpStatus + u4CbpIndex)->i4RetValCBPRowStatus)) ==
                    SNMP_FAILURE)
                {
                    i1Bool = FALSE;
                    break;
                }
                (pCbpStatus + u4CbpIndex)->u4CbpNum = u4Port;

                /* Store the Previous Configuration Data */
                if (nmhGetFsPbbCBPServiceMappingBVid (u4ContextId, u4Port,
                                                      u4Isid,
                                                      &((pCbpStatus +
                                                         u4CbpIndex)->
                                                        i4BvlanId)) ==
                    SNMP_FAILURE)
                {
                    i1Bool = FALSE;
                    break;
                }

                /* Set the Row Status to NOT_IN_SERVICE */
                if ((pCbpStatus + u4CbpIndex)->i4RetValCBPRowStatus ==
                    PBB_ACTIVE)
                {
                    if (nmhTestv2FsPbbCBPServiceMappingRowStatus
                        (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                         PBB_NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        i1Bool = FALSE;
                        break;
                    }

                    if (nmhSetFsPbbCBPServiceMappingRowStatus
                        (u4ContextId, u4Port, u4Isid,
                         PBB_NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        i1Bool = FALSE;
                        break;
                    }
                }

                if (nmhTestv2FsPbbCBPServiceMappingBVid
                    (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                     i4BvlanId) == SNMP_FAILURE)
                {
                    i1Bool = FALSE;
                    break;
                }

                u4CbpIndex = u4CbpIndex + 1;
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }

        if (i1Bool == FALSE)
        {
            break;
        }
    }

    u4PortNum = u4CbpIndex;

    if (i1Bool != FALSE)
    {
        /* Set the BVID Values */
        for (u4CbpIndex = 0; u4CbpIndex < u4PortNum; u4CbpIndex++)
        {
            u4Port = (pCbpStatus + u4CbpIndex)->u4CbpNum;

            if (nmhSetFsPbbCBPServiceMappingBVid (u4ContextId, u4Port,
                                                  u4Isid,
                                                  i4BvlanId) == SNMP_FAILURE)
            {
                i1Bool = FALSE;
                i1BvidSet = FALSE;
                break;
            }
        }
    }
    if (i1BvidSet == FALSE)
    {                            /* rollback bvid to previous values */

        for (u4CbpIndex = 0; u4CbpIndex < u4PortNum; u4CbpIndex++)
        {
            u4Port = (pCbpStatus + u4CbpIndex)->u4CbpNum;
            if (nmhTestv2FsPbbCBPServiceMappingBVid (&u4ErrCode, u4ContextId,
                                                     u4Port, u4Isid,
                                                     (pCbpStatus + u4CbpIndex)->
                                                     i4BvlanId) == SNMP_FAILURE)
            {
                i1Bool = FALSE;
                break;
            }

            if (nmhSetFsPbbCBPServiceMappingBVid (u4ContextId, u4Port,
                                                  u4Isid,
                                                  (pCbpStatus + u4CbpIndex)->
                                                  i4BvlanId) == SNMP_FAILURE)
            {
                i1Bool = FALSE;
                break;
            }
        }
    }

    /*set the Row status to previous value - 
       both in case of failure and success */
    for (u4CbpIndex = 0; u4CbpIndex < u4PortNum; u4CbpIndex++)
    {
        u4Port = (pCbpStatus + u4CbpIndex)->u4CbpNum;
        if (((pCbpStatus + u4CbpIndex)->i4RetValCBPRowStatus == PBB_ACTIVE) &&
            (i4RetValCBPRowStatus != PBB_ACTIVE))
        {
            if (nmhTestv2FsPbbCBPServiceMappingRowStatus
                (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                 PBB_ACTIVE) == SNMP_FAILURE)
            {
                if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                           PbbCbpStatusMemPoolId,
                                           (UINT1 *) pCbpStatus) == MEM_FAILURE)
                {
                    PBB_TRC (ALL_FAILURE_TRC,
                             "Error in Releasing  memory for pCbpStatus \n");
                }
                return CLI_FAILURE;
            }

            if (nmhSetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4Port,
                                                       u4Isid,
                                                       PBB_ACTIVE) ==
                SNMP_FAILURE)
            {
                if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                           PbbCbpStatusMemPoolId,
                                           (UINT1 *) pCbpStatus) == MEM_FAILURE)
                {
                    PBB_TRC (ALL_FAILURE_TRC,
                             "Error in Releasing  memory for pCbpStatus \n");
                }
                return CLI_FAILURE;
            }
        }

    }
    if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                               PbbCbpStatusMemPoolId,
                               (UINT1 *) pCbpStatus) == MEM_FAILURE)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Releasing  memory for pCbpStatus \n");
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    if (i1Bool != FALSE)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShutdown                                       */
/*                                                                          */
/*     DESCRIPTION      : This function will start/shut PBB module          */
/*                                                                          */
/*     INPUT            : i4Status - Pbb Module status                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbShutdown (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode;

    if (nmhTestv2FsPbbShutdownStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
        {
            CLI_SET_ERR (CLI_PBB_I_B_COMP_PRESENT);
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsPbbShutdownStatus (i4Status) == SNMP_FAILURE)
        /* if i4Status == PBB_SNMP_FALSE, then the PBB module will
         * be started and enabled */
    {
        if (i4Status == PBB_SNMP_FALSE)
        {
            CLI_SET_ERR (CLI_PBB_NO_SHUT_MEM_ALLOC_ERR);
        }
        else
        {
            CLI_FATAL_ERROR (CliHandle);
        }
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbIsidActive                                      */
/*                                                                           */
/*     DESCRIPTION      : This function will enter into isid database config */
/*                        mode. The ISID will be made active                 */
/*                                                                           */
/*     INPUT            : u4Isid   - ISID Identifier to made active        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbIsidActive (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4IfIndex,
               INT4 i4Status)
{
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    UINT1              *pPortList1 = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4Vip = PBB_INIT_VAL;
    UINT4               u4ErrCode = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    INT4                i4IsidStaticRowStatus = PBB_INIT_VAL;
    INT4                i4RetStatus = PBB_INIT_VAL;
    UINT2               u2LocalPortId = 0;
    BOOL1               bResult = OSIX_FALSE;

    u4Isid = CLI_GET_ISID ();

    if (PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId) == PBB_SUCCESS)
    {
        if (u4IfIndex != 0)
        {
            CliPrintf (CliHandle, "\r%% Port option has to be used only in"
                       " B-Component \r\n");
            return CLI_FAILURE;
        }

        PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                       u2LocalPortId, (INT4 *) &u4Vip);

        if ((nmhGetFsPbbVipRowStatus (u4ContextId, u4Vip,
                                      &i4IsidStaticRowStatus)) == SNMP_FAILURE)
        {
            /* A row in the ISID table must have been created */
            CliPrintf (CliHandle, "\r%% ISID %d is not created \r\n", u4Isid);
            return CLI_FAILURE;
        }

        if (i4IsidStaticRowStatus != i4Status)
        {
            if (nmhTestv2FsPbbVipRowStatus (&u4ErrCode, u4ContextId,
                                            u4Vip, i4Status) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsPbbVipRowStatus (u4ContextId, u4Vip, i4Status)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        return CLI_SUCCESS;
    }                            /*end of I component */

    /*B component */
    /*Member ports CBP */
    if (u4IfIndex == 0)
    {
        pPortList1 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pPortList1 == NULL)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "PbbIsidActive : Error in allocating memory "
                     "for pPortList1\r\n");
            return CLI_FAILURE;
        }

        CLI_MEMSET (pPortList1, 0, sizeof (tLocalPortList));
        SnmpPorts.pu1_OctetList = pPortList1;
        SnmpPorts.i4_Length = sizeof (tLocalPortList);

        i4RetStatus = PbbGetCbpForIsid (u4ContextId, u4Isid, &SnmpPorts);

        if (i4RetStatus == PBB_FAILURE)
        {
            UtilPlstReleaseLocalPortList (pPortList1);
            return CLI_SUCCESS;
        }

        for (u2LocalPortId = 1; u2LocalPortId <= PBB_MAX_PORTS; u2LocalPortId++)
        {
            OSIX_BITLIST_IS_BIT_SET (SnmpPorts.pu1_OctetList, u2LocalPortId,
                                     PBB_PORT_LIST_SIZE, bResult);

            if (bResult == OSIX_FALSE)
            {
                continue;
            }
            if (PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                               (INT4 *) &u4Port))
            {
                UtilPlstReleaseLocalPortList (pPortList1);
                return CLI_FAILURE;
            }
            if (nmhTestv2FsPbbCBPServiceMappingRowStatus (&u4ErrCode,
                                                          u4ContextId,
                                                          u4Port, u4Isid,
                                                          i4Status)
                == SNMP_FAILURE)
            {
                UtilPlstReleaseLocalPortList (pPortList1);
                return CLI_FAILURE;
            }
        }

        for (u2LocalPortId = 1; u2LocalPortId <= PBB_MAX_PORTS; u2LocalPortId++)
        {
            OSIX_BITLIST_IS_BIT_SET (SnmpPorts.pu1_OctetList, u2LocalPortId,
                                     PBB_PORT_LIST_SIZE, bResult);

            if (bResult == OSIX_FALSE)
            {
                continue;
            }

            PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                           (INT4 *) &u4Port);

            if (nmhSetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4Port,
                                                       u4Isid,
                                                       i4Status)
                == SNMP_FAILURE)
            {
                UtilPlstReleaseLocalPortList (pPortList1);
                return CLI_FAILURE;
            }
        }
        UtilPlstReleaseLocalPortList (pPortList1);
    }
    else
    {
        /* Specific interface is given, configure the RowStatus for that 
         * interface alone */
        if (nmhTestv2FsPbbCBPServiceMappingRowStatus (&u4ErrCode, u4ContextId,
                                                      u4IfIndex, u4Isid,
                                                      i4Status) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4IfIndex,
                                                   u4Isid, i4Status)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    /*end of B component */
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbGlbOUI                                          */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the Organization      */
/*                        Unique Identifier for the Bridge.                  */
/*                                                                           */
/*     INPUT            : u4GblOui   - Organization Unique Identifier        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbGlbOUI (tCliHandle CliHandle, tSNMP_OCTET_STRING_TYPE * pOui)
{
    UINT4               u4ErrCode;

    if (nmhTestv2FsPbbGlbOUI (&u4ErrCode, pOui) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbbGlbOUI (pOui) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbCreateOUI                                       */
/*                                                                           */
/*     DESCRIPTION      : This function will configure the Organization      */
/*                        Unique Identifier for an ISID.                     */
/*                                                                           */
/*     INPUT            : u4Oui   - Organization Unique Identifier           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCreateOUI (tCliHandle CliHandle, UINT4 u4ContextId,
              tSNMP_OCTET_STRING_TYPE * pOui, UINT1 *pu1Ports)
{
    UINT1              *pau1IsidPorts = NULL;
    UINT4               u4ErrCode;
    UINT4               u4Isid;
    UINT4               u4CbpIndex = PBB_INIT_VAL;
    UINT4               u4PortIndex = PBB_INIT_VAL;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    UINT4               u4Port;
    UINT4               u1BoolOuiFail = PBB_TRUE;
    UINT1               u1Bool = PBB_TRUE;
    tCbpStatusInfo     *pCbpStatus = NULL;
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE *pPbbOui = NULL;

    pau1IsidPorts = FsUtilAllocBitList (PBB_IFPORT_LIST_SIZE);
    if (pau1IsidPorts == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "PbbCreateOUI: Error in allocating memory "
                 "for pau1IsidPorts\r\n");
        return CLI_FAILURE;
    }
    CLI_MEMSET (pau1IsidPorts, PBB_INIT_VAL, PBB_IFPORT_LIST_SIZE);

    if (PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                             PbbCbpStatusMemPoolId, pCbpStatus) == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Allocating memory for pCbpStatus \n");
        FsUtilReleaseBitList (pau1IsidPorts);
        return CLI_FAILURE;
    }

    PBB_MEMSET (pCbpStatus, 0, (sizeof (tCbpStatusInfo) *
                                PBB_MAX_PORTS_PER_ISID));
    u4Isid = CLI_GET_ISID ();
    u4ContextId = CLI_GET_CXT_ID ();

    PortList.i4_Length = PBB_IFPORT_LIST_SIZE;
    PortList.pu1_OctetList = pau1IsidPorts;

    MEMCPY (PortList.pu1_OctetList, pu1Ports, PortList.i4_Length);

    /*test for loop */
    for (u2ByteIndex = PBB_INIT_VAL; u2ByteIndex < PBB_IFPORT_LIST_SIZE;
         u2ByteIndex++)
    {
        u1PortFlag = PortList.pu1_OctetList[u2ByteIndex];
        for (u2BitIndex = PBB_INIT_VAL; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                         && (u1PortFlag != PBB_FALSE));
             u2BitIndex++)
        {
            if ((u1PortFlag & PBB_BIT8) != PBB_FALSE)
            {
                u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                (pCbpStatus + u4CbpIndex)->u4CbpNum = u4Port;
                if (nmhGetFsPbbOUIRowStatus (u4ContextId, u4Isid, u4Port,
                                             &((pCbpStatus + u4CbpIndex)->
                                               i4RetValCBPRowStatus))
                    == SNMP_FAILURE)
                {
                    if (nmhTestv2FsPbbOUIRowStatus (&u4ErrCode, u4ContextId,
                                                    u4Isid, u4Port,
                                                    PBB_CREATE_AND_WAIT) ==
                        SNMP_FAILURE)
                    {
                        if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                                   PbbPoolIds.
                                                   PbbCbpStatusMemPoolId,
                                                   (UINT1 *) pCbpStatus) ==
                            MEM_FAILURE)
                        {
                            PBB_TRC (ALL_FAILURE_TRC,
                                     "Error in Releasing  memory for pCbpStatus \n");
                        }
                        FsUtilReleaseBitList (pau1IsidPorts);
                        return CLI_FAILURE;
                    }
                    (pCbpStatus + u4CbpIndex)->u1isRowCreated = PBB_TRUE;
                }
                else
                {
                    if ((pCbpStatus + u4CbpIndex)->i4RetValCBPRowStatus
                        == PBB_ACTIVE)
                    {
                        if (nmhTestv2FsPbbOUIRowStatus (&u4ErrCode,
                                                        u4ContextId,
                                                        u4Isid, u4Port,
                                                        PBB_NOT_IN_SERVICE) ==
                            SNMP_FAILURE)
                        {
                            if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                                       PbbPoolIds.
                                                       PbbCbpStatusMemPoolId,
                                                       (UINT1 *) pCbpStatus) ==
                                MEM_FAILURE)
                            {
                                PBB_TRC (ALL_FAILURE_TRC,
                                         "Error in Releasing  memory for"
                                         "pCbpStatus \n");
                            }
                            FsUtilReleaseBitList (pau1IsidPorts);
                            return CLI_FAILURE;
                        }
                    }
                    (pCbpStatus + u4CbpIndex)->u1isRowCreated = PBB_FALSE;
                }
                u4CbpIndex = u4CbpIndex + 1;

            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }

    }
    FsUtilReleaseBitList (pau1IsidPorts);

    u4PortIndex = u4CbpIndex;
    /*set for rowstatus */
    for (u4CbpIndex = PBB_INIT_VAL; u4CbpIndex < u4PortIndex; u4CbpIndex++)
    {
        if ((pCbpStatus + u4CbpIndex)->u1isRowCreated == PBB_TRUE)
        {
            if (nmhSetFsPbbOUIRowStatus (u4ContextId,
                                         u4Isid, (pCbpStatus + u4CbpIndex)->
                                         u4CbpNum, PBB_CREATE_AND_WAIT) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                           PbbCbpStatusMemPoolId,
                                           (UINT1 *) pCbpStatus) == MEM_FAILURE)
                {
                    PBB_TRC (ALL_FAILURE_TRC,
                             "Error in Releasing  memory for pCbpStatus \n");
                }
                return CLI_FAILURE;
            }
        }
        else
        {
            if ((pCbpStatus + u4CbpIndex)->i4RetValCBPRowStatus == PBB_ACTIVE)
            {
                if (nmhSetFsPbbOUIRowStatus (u4ContextId,
                                             u4Isid,
                                             (pCbpStatus +
                                              u4CbpIndex)->u4CbpNum,
                                             PBB_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                               PbbPoolIds.PbbCbpStatusMemPoolId,
                                               (UINT1 *) pCbpStatus) ==
                        MEM_FAILURE)
                    {
                        PBB_TRC (ALL_FAILURE_TRC,
                                 "Error in Releasing  memory for pCbpStatus \n");
                    }
                    return CLI_FAILURE;
                }
            }
        }
    }
    pPbbOui = allocmem_octetstring (PBB_MAX_PORTS_PER_ISID);
    if (pPbbOui == NULL)
    {
        PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                               PbbCbpStatusMemPoolId, (UINT1 *) pCbpStatus);

        return (CLI_FAILURE);
    }
    MEMSET (pPbbOui->pu1_OctetList, 0, pPbbOui->i4_Length);

    /*test for oui */
    u1Bool = PBB_TRUE;
    for (u4CbpIndex = PBB_INIT_VAL; u4CbpIndex < u4PortIndex; u4CbpIndex++)
    {
        CLI_MEMSET ((pCbpStatus + u4CbpIndex)->au1Oui, PBB_INIT_VAL,
                    PBB_OUI_LENGTH);
        pPbbOui[u4CbpIndex].i4_Length = PBB_OUI_LENGTH;
        pPbbOui[u4CbpIndex].pu1_OctetList = (pCbpStatus + u4CbpIndex)->au1Oui;

        if (nmhGetFsPbbOUI (u4ContextId, u4Isid,
                            (pCbpStatus + u4CbpIndex)->u4CbpNum,
                            pPbbOui) == SNMP_FAILURE)
        {
            if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                       PbbCbpStatusMemPoolId,
                                       (UINT1 *) pCbpStatus) == MEM_FAILURE)
            {
                PBB_TRC (ALL_FAILURE_TRC,
                         "Error in Releasing  memory for pCbpStatus \n");
            }
            free_octetstring (pPbbOui);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsPbbOUI (&u4ErrCode, u4ContextId, u4Isid,
                               (pCbpStatus + u4CbpIndex)->u4CbpNum,
                               pOui) == SNMP_FAILURE)
        {
            u1Bool = PBB_FALSE;
            break;
        }
    }

    /*set for oui */
    if (u1Bool == PBB_TRUE)
    {
        for (u4CbpIndex = PBB_INIT_VAL; u4CbpIndex < u4PortIndex; u4CbpIndex++)
        {
            if (nmhSetFsPbbOUI (u4ContextId, u4Isid,
                                (pCbpStatus + u4CbpIndex)->u4CbpNum,
                                pOui) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                           PbbCbpStatusMemPoolId,
                                           (UINT1 *) pCbpStatus) == MEM_FAILURE)
                {
                    PBB_TRC (ALL_FAILURE_TRC,
                             "Error in Releasing  memory for pCbpStatus \n");
                }
                free_octetstring (pPbbOui);
                return CLI_FAILURE;
            }
        }
    }

    if (u1Bool == PBB_TRUE)
    {
        for (u4CbpIndex = PBB_INIT_VAL; u4CbpIndex < u4PortIndex; u4CbpIndex++)
        {
            if (nmhTestv2FsPbbOUIRowStatus (&u4ErrCode, u4ContextId,
                                            u4Isid,
                                            (pCbpStatus + u4CbpIndex)->u4CbpNum,
                                            PBB_ACTIVE) == SNMP_FAILURE)
            {
                u1Bool = PBB_FALSE;
                u1BoolOuiFail = PBB_FALSE;
                break;
            }
        }
        for (u4CbpIndex = PBB_INIT_VAL; u4CbpIndex < u4PortIndex; u4CbpIndex++)
        {

            if (nmhSetFsPbbOUIRowStatus (u4ContextId,
                                         u4Isid, (pCbpStatus + u4CbpIndex)->
                                         u4CbpNum, PBB_ACTIVE) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                           PbbCbpStatusMemPoolId,
                                           (UINT1 *) pCbpStatus) == MEM_FAILURE)
                {
                    PBB_TRC (ALL_FAILURE_TRC,
                             "Error in Releasing  memory for pCbpStatus \n");
                }
                free_octetstring (pPbbOui);
                return CLI_FAILURE;
            }
        }

    }

    if (u1Bool != PBB_TRUE)
    {                            /*rollback */
        for (u4CbpIndex = PBB_INIT_VAL; u4CbpIndex < u4PortIndex; u4CbpIndex++)
        {
            if ((pCbpStatus + u4CbpIndex)->u1isRowCreated == PBB_TRUE)
            {
                if (nmhTestv2FsPbbOUIRowStatus (&u4ErrCode, u4ContextId,
                                                u4Isid,
                                                (pCbpStatus + u4CbpIndex)->
                                                u4CbpNum, PBB_DESTROY) ==
                    SNMP_FAILURE)
                {
                    if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                               PbbPoolIds.PbbCbpStatusMemPoolId,
                                               (UINT1 *) pCbpStatus) ==
                        MEM_FAILURE)
                    {
                        PBB_TRC (ALL_FAILURE_TRC,
                                 "Error in Releasing  memory for pCbpStatus \n");
                    }
                    free_octetstring (pPbbOui);
                    return CLI_FAILURE;
                }

                if (nmhSetFsPbbOUIRowStatus (u4ContextId,
                                             u4Isid,
                                             (pCbpStatus +
                                              u4CbpIndex)->u4CbpNum,
                                             PBB_DESTROY) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                               PbbPoolIds.PbbCbpStatusMemPoolId,
                                               (UINT1 *) pCbpStatus) ==
                        MEM_FAILURE)
                    {
                        PBB_TRC (ALL_FAILURE_TRC,
                                 "Error in Releasing  memory for pCbpStatus \n");
                    }
                    free_octetstring (pPbbOui);
                    return CLI_FAILURE;
                }

            }
            else
            {                    /*existing oui */
                if (u1BoolOuiFail == PBB_FALSE)
                {

                    if (nmhTestv2FsPbbOUI (&u4ErrCode,
                                           u4ContextId, u4Isid,
                                           (pCbpStatus + u4CbpIndex)->u4CbpNum,
                                           pPbbOui) == SNMP_FAILURE)
                    {
                        if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                                   PbbPoolIds.
                                                   PbbCbpStatusMemPoolId,
                                                   (UINT1 *) pCbpStatus) ==
                            MEM_FAILURE)
                        {
                            PBB_TRC (ALL_FAILURE_TRC,
                                     "Error in Releasing  memory for pCbpStatus \n");
                        }
                        free_octetstring (pPbbOui);
                        return CLI_FAILURE;
                    }
                    if (nmhSetFsPbbOUI (u4ContextId, u4Isid,
                                        (pCbpStatus + u4CbpIndex)->u4CbpNum,
                                        pPbbOui) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                                   PbbPoolIds.
                                                   PbbCbpStatusMemPoolId,
                                                   (UINT1 *) pCbpStatus) ==
                            MEM_FAILURE)
                        {
                            PBB_TRC (ALL_FAILURE_TRC,
                                     "Error in Releasing  memory for pCbpStatus \n");
                        }
                        free_octetstring (pPbbOui);
                        return CLI_FAILURE;
                    }

                }
                if (((pCbpStatus + u4CbpIndex)->i4RetValCBPRowStatus ==
                     PBB_ACTIVE))
                {
                    if (nmhTestv2FsPbbOUIRowStatus (&u4ErrCode,
                                                    u4ContextId, u4Isid,
                                                    (pCbpStatus +
                                                     u4CbpIndex)->u4CbpNum,
                                                    (pCbpStatus +
                                                     u4CbpIndex)->
                                                    i4RetValCBPRowStatus) ==
                        SNMP_FAILURE)
                    {
                        if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                                   PbbPoolIds.
                                                   PbbCbpStatusMemPoolId,
                                                   (UINT1 *) pCbpStatus) ==
                            MEM_FAILURE)
                        {
                            PBB_TRC (ALL_FAILURE_TRC,
                                     "Error in Releasing  memory for pCbpStatus \n");
                        }
                        free_octetstring (pPbbOui);
                        return CLI_FAILURE;
                    }

                    if (nmhSetFsPbbOUIRowStatus (u4ContextId,
                                                 u4Isid,
                                                 (pCbpStatus +
                                                  u4CbpIndex)->u4CbpNum,
                                                 (pCbpStatus +
                                                  u4CbpIndex)->
                                                 i4RetValCBPRowStatus) ==
                        SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.
                                                   PbbPoolIds.
                                                   PbbCbpStatusMemPoolId,
                                                   (UINT1 *) pCbpStatus) ==
                            MEM_FAILURE)
                        {
                            PBB_TRC (ALL_FAILURE_TRC,
                                     "Error in Releasing  memory for pCbpStatus \n");
                        }
                        free_octetstring (pPbbOui);
                        return CLI_FAILURE;
                    }
                }
            }
        }

    }

    UNUSED_PARAM (CliHandle);
    if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                               PbbCbpStatusMemPoolId,
                               (UINT1 *) pCbpStatus) == MEM_FAILURE)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Releasing  memory for pCbpStatus \n");
        free_octetstring (pPbbOui);
        return CLI_FAILURE;
    }
    free_octetstring (pPbbOui);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbDeleteOUIList                                       */
/*                                                                           */
/*     DESCRIPTION      : This function will delete the Organization         */
/*                        Unique Identifier for ISID on all ports.           */
/*                                                                           */
/*     INPUT            : pu1Ports - Interface List                          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbDeleteOUIList (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1Ports)
{

    UINT1              *pau1IsidPorts = NULL;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    UINT4               u4Port;
    tCbpStatusInfo     *pPortsList = NULL;
    tSNMP_OCTET_STRING_TYPE PortList;

    pau1IsidPorts = FsUtilAllocBitList (PBB_IFPORT_LIST_SIZE);
    if (pau1IsidPorts == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "PbbDeleteOUIList: Error in allocating memory "
                 "for pau1IsidPorts\r\n");
        return CLI_FAILURE;
    }
    CLI_MEMSET (pau1IsidPorts, PBB_INIT_VAL, PBB_IFPORT_LIST_SIZE);

    if (PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                             PbbCbpStatusMemPoolId, pPortsList) == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Allocating memory for pPortsList \n");
        FsUtilReleaseBitList (pau1IsidPorts);
        return CLI_FAILURE;
    }

    PBB_MEMSET (pPortsList, 0,
                (sizeof (tCbpStatusInfo) * PBB_MAX_PORTS_PER_ISID));
    u4ContextId = CLI_GET_CXT_ID ();

    PortList.i4_Length = PBB_IFPORT_LIST_SIZE;
    PortList.pu1_OctetList = pau1IsidPorts;

    MEMCPY (PortList.pu1_OctetList, pu1Ports, PortList.i4_Length);

    /*test for loop */
    for (u2ByteIndex = PBB_INIT_VAL; u2ByteIndex < PBB_IFPORT_LIST_SIZE;
         u2ByteIndex++)
    {
        u1PortFlag = PortList.pu1_OctetList[u2ByteIndex];
        for (u2BitIndex = PBB_INIT_VAL; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                         && (u1PortFlag != PBB_FALSE));
             u2BitIndex++)
        {
            if ((u1PortFlag & PBB_BIT8) != PBB_FALSE)
            {
                u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);
                (pPortsList + u4Index)->u4CbpNum = u4Port;
                u4Index = u4Index + 1;
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }

    }
    FsUtilReleaseBitList (pau1IsidPorts);

    if (PbbDeleteOUI (CliHandle, u4ContextId, pPortsList, u4Index) ==
        CLI_FAILURE)
    {
        if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                   PbbCbpStatusMemPoolId,
                                   (UINT1 *) pPortsList) == MEM_FAILURE)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "Error in Releasing  memory for pPortsList \n");
        }
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                               PbbCbpStatusMemPoolId,
                               (UINT1 *) pPortsList) == MEM_FAILURE)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Releasing  memory for pPortsList \n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbDeleteOUIAll                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will delete the Organization         */
/*                        Unique Identifier for ISID on all ports.           */
/*                                                                           */
/*     INPUT            : u4Oui   - Organization Unique Identifier           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbDeleteOUIAll (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4Index = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4PrevIsid = PBB_INIT_VAL;
    UINT4               u4PrevContextId = PBB_INIT_VAL;
    UINT4               u4PortId = PBB_INIT_VAL;
    UINT4               u4PrevPortId = PBB_INIT_VAL;
    tCbpStatusInfo     *pPortsList = NULL;

    if (PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                             PbbCbpStatusMemPoolId, pPortsList) == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Allocating memory for pPortsList \n");
        return CLI_FAILURE;
    }

    PBB_MEMSET (pPortsList, 0,
                (sizeof (tCbpStatusInfo) * PBB_MAX_PORTS_PER_ISID));
    u4Isid = CLI_GET_ISID ();

    if (PbbGetFirstOUIPortInIsidContext (u4ContextId, u4Isid, &u4PortId)
        == PBB_SUCCESS)
    {
        do
        {
            (pPortsList + u4Index)->u4CbpNum = u4PortId;
            u4Index = u4Index + 1;
            u4PrevIsid = u4Isid;
            u4PrevContextId = u4ContextId;
            u4PrevPortId = u4PortId;

            if (nmhGetNextIndexFsPbbISIDOUITable
                (u4PrevContextId, (INT4 *) &u4ContextId,
                 u4PrevIsid, &u4Isid, u4PrevPortId, (INT4 *) &u4PortId)
                == SNMP_FAILURE)
                break;

        }
        while (u4PrevContextId == u4ContextId || u4PrevIsid == u4Isid);
    }
    if (PbbDeleteOUI (CliHandle, u4ContextId, pPortsList, u4Index) ==
        CLI_FAILURE)
    {
        if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                   PbbCbpStatusMemPoolId,
                                   (UINT1 *) pPortsList) == MEM_FAILURE)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "Error in Releasing  memory for pPortsList \n");
        }
        return CLI_FAILURE;
    }
    UNUSED_PARAM (CliHandle);
    if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                               PbbCbpStatusMemPoolId,
                               (UINT1 *) pPortsList) == MEM_FAILURE)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Releasing  memory for pPortsList \n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbDeleteOUI                                    */
/*                                                                           */
/*     DESCRIPTION      : This function will delete the Organization         */
/*                        Unique Identifier for ISID on all ports.           */
/*                                                                           */
/*     INPUT            : u4Oui   - Organization Unique Identifier           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbDeleteOUI (tCliHandle CliHandle, UINT4 u4ContextId, tCbpStatus PortsList[],
              UINT4 u4TotalPort)
{
    UINT4               u4ErrCode;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4PortIndex = PBB_INIT_VAL;
    u4Isid = CLI_GET_ISID ();
    for (u4PortIndex = PBB_INIT_VAL; u4PortIndex < u4TotalPort; u4PortIndex++)
    {
        if (nmhTestv2FsPbbOUIRowStatus (&u4ErrCode, u4ContextId, u4Isid,
                                        PortsList[u4PortIndex].u4CbpNum,
                                        PBB_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    for (u4PortIndex = PBB_INIT_VAL; u4PortIndex < u4TotalPort; u4PortIndex++)
    {
        if (nmhSetFsPbbOUIRowStatus
            (u4ContextId, u4Isid, PortsList[u4PortIndex].u4CbpNum,
             PBB_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbCliShowOui                                      */
/*                                                                           */
/*     DESCRIPTION      : This function shows the set value of OUI           */
/*                                                                           */
/*     INPUT            :                                                    */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCliShowOui (tCliHandle CliHandle)
{
    UINT1               au1OuiVal[PBB_OUI_LENGTH];
    UINT1               au1OuiStrVal[PBB_CLI_OUI_STR_LEN];
    tSNMP_OCTET_STRING_TYPE LocalClientOui;
    /* Display OUI for the Provider Backbone Bridge  */
    CLI_MEMSET (au1OuiVal, 0, PBB_OUI_LENGTH);
    CLI_MEMSET (&LocalClientOui, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    LocalClientOui.i4_Length = PBB_OUI_LENGTH;
    LocalClientOui.pu1_OctetList = au1OuiVal;
    nmhGetFsPbbGlbOUI (&LocalClientOui);
    CLI_MEMSET (au1OuiStrVal, 0, PBB_CLI_OUI_STR_LEN);
    CliOctetToStr (LocalClientOui.pu1_OctetList, LocalClientOui.i4_Length,
                   au1OuiStrVal, PBB_CLI_OUI_STR_LEN);
    CliPrintf (CliHandle, "\r\nConfigured OUI : %s\r\n", au1OuiStrVal);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbShowPisid                                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays PISID for an ISID          */
/*                                                                           */
/*     INPUT            : u4PortId - Port identifier                         */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbShowPisid (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PortId)
{
    UINT1               au1NameStr[PBB_MAX_PORT_NAME_LENGTH];
    INT4                i4RetVal = PBB_INIT_VAL;
    INT4                i4RetValFsPbbPortPisid = PBB_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;
    UINT4               u4CurrentPortId = PBB_INIT_VAL;
    UINT4               u4PrevContextId = PBB_INIT_VAL;
    UINT4               u4NextPortId = PBB_INIT_VAL;
    UINT4               u4NextContextId = PBB_INIT_VAL;

    if (u4PortId > PBB_MAX_PORTS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Interface.\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nPISID table \r\n");
    CliPrintf (CliHandle, "--------------------- \r\n");
    CliPrintf (CliHandle, "Port     PISID    \r\n");
    CliPrintf (CliHandle, "-----    ------   \r\n");

    if (u4PortId == 0)
    {
        i4RetVal
            = nmhGetFirstIndexFsPbbPortPisidTable ((INT4 *) &u4NextContextId,
                                                   (INT4 *) &u4NextPortId);
        while (i4RetVal == SNMP_SUCCESS && u4ContextId != u4NextContextId)
        {
            u4PrevContextId = u4NextContextId;
            u4CurrentPortId = u4NextPortId;
            i4RetVal =
                nmhGetNextIndexFsPbbPortPisidTable ((INT4) u4PrevContextId,
                                                    (INT4 *) &u4NextContextId,
                                                    (INT4) u4CurrentPortId,
                                                    (INT4 *) &u4NextPortId);
        }
        if (i4RetVal == SNMP_SUCCESS)
        {
            do
            {
                u4CurrentPortId = u4NextPortId;
                if (nmhGetFsPbbPortPisid (u4ContextId, u4CurrentPortId,
                                          &i4RetValFsPbbPortPisid) ==
                    SNMP_SUCCESS)
                {
                    CLI_MEMSET (au1NameStr, 0, sizeof (au1NameStr));
                    if (CfaCliGetIfName (u4CurrentPortId, (INT1 *) au1NameStr)
                        == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "%% Invalid Port %d\r\n",
                                   u4CurrentPortId);

                        return CLI_FAILURE;
                    }

                    CliPrintf (CliHandle, "%s    ", au1NameStr);
                    CliPrintf (CliHandle, "%-4d    ", i4RetValFsPbbPortPisid);
                    i4PagingStatus = CliPrintf (CliHandle, "\r\n");
                    if (i4PagingStatus == CLI_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                }

            }
            while (nmhGetNextIndexFsPbbPortPisidTable ((INT4) u4ContextId,
                                                       (INT4 *)
                                                       &u4NextContextId,
                                                       (INT4) u4CurrentPortId,
                                                       (INT4 *) &u4NextPortId)
                   == SNMP_SUCCESS && u4ContextId == u4NextContextId);
        }

    }
    else
    {
        if (nmhValidateIndexInstanceFsPbbPortPisidTable
            (u4ContextId, u4PortId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\nInvalid Port ID\r\n");
            return CLI_FAILURE;
        }

        if (nmhGetFsPbbPortPisid (u4ContextId, u4PortId,
                                  &i4RetValFsPbbPortPisid) == SNMP_SUCCESS)
        {
            CLI_MEMSET (au1NameStr, 0, sizeof (au1NameStr));
            if (CfaCliGetIfName (u4PortId, (INT1 *) au1NameStr) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "%% Invalid Port %d\r\n", u4PortId);

                return CLI_FAILURE;
            }

            CliPrintf (CliHandle, "%s    ", au1NameStr);
            CliPrintf (CliHandle, "%-8d    ", i4RetValFsPbbPortPisid);
        }
    }
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbShowIsid                                        */
/*                                                                           */
/*     DESCRIPTION      : This fnction displayes all info related to the isid*/
/*                                                                           */
/*     INPUT            : u4Isid -       Isid                                */
/*                      : u4ContextId  - Context ID                          */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbShowIsid (tCliHandle CliHandle, UINT4 u4Isid, UINT4 u4ContextId)
{
    UINT4               u4FirstContextId = PBB_INIT_VAL;
    UINT4               u4FirstPortId = PBB_INIT_VAL;
    UINT4               u4NextPortId = PBB_INIT_VAL;
    UINT4               u4NextContextId = PBB_INIT_VAL;
    UINT4               u4FirstIsid = PBB_INIT_VAL;
    UINT4               u4NextIsid = PBB_INIT_VAL;
    UINT4               u4CurrIsid = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;

    CliPrintf (CliHandle, "\r\nService Instance table \r\n");
    CliPrintf (CliHandle, "-------------------------- \r\n");

    if (u4Isid != 0)
    {
        if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
        {
            u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;
        }

        if (u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            if (nmhGetFirstIndexFsPbbVipTable ((INT4 *) &u4NextContextId,
                                               (INT4 *) &u4NextPortId) ==
                SNMP_SUCCESS)
            {
                while (u4NextContextId != u4ContextId)
                {
                    u4FirstContextId = u4NextContextId;
                    u4FirstPortId = u4NextPortId;
                    if (nmhGetNextIndexFsPbbVipTable
                        ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                         (INT4) u4FirstPortId,
                         (INT4 *) &u4NextPortId) == SNMP_FAILURE)
                        return CLI_SUCCESS;
                }
                while (u4NextContextId == u4ContextId)
                {
                    nmhGetFsPbbVipISid (u4NextContextId, u4NextPortId,
                                        &u4CurrIsid);
                    if (u4Isid == u4CurrIsid)
                    {
                        PbbShowIsidInContext (CliHandle, u4Isid, u4ContextId,
                                              u4NextPortId);
                        break;
                    }
                    u4FirstContextId = u4NextContextId;
                    u4FirstPortId = u4NextPortId;
                    if (nmhGetNextIndexFsPbbVipTable
                        ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                         (INT4) u4FirstPortId,
                         (INT4 *) &u4NextPortId) == SNMP_FAILURE)
                        return CLI_SUCCESS;

                }
            }
        }
        else if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
        {
            if (nmhGetFirstIndexFsPbbCBPServiceMappingTable
                ((INT4 *) &u4NextContextId,
                 (INT4 *) &u4NextPortId, &u4NextIsid) == SNMP_SUCCESS)
            {
                while (u4NextContextId != u4ContextId || u4Isid != u4NextIsid)
                {
                    u4FirstContextId = u4NextContextId;
                    u4FirstPortId = u4NextPortId;
                    u4FirstIsid = u4NextIsid;
                    if (nmhGetNextIndexFsPbbCBPServiceMappingTable
                        ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                         (INT4) u4FirstPortId, (INT4 *) &u4NextPortId,
                         u4FirstIsid, &u4NextIsid) == SNMP_FAILURE)
                        return CLI_SUCCESS;
                }
                PbbShowIsidInContext (CliHandle, u4NextIsid, u4NextContextId,
                                      0);
            }
        }
    }
    else
    {
        if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
        {
            u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;
        }
        if (u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            if (nmhGetFirstIndexFsPbbVipTable ((INT4 *) &u4NextContextId,
                                               (INT4 *) &u4NextPortId) ==
                SNMP_SUCCESS)
            {
                while (u4NextContextId != u4ContextId)
                {
                    u4FirstContextId = u4NextContextId;
                    u4FirstPortId = u4NextPortId;
                    if (nmhGetNextIndexFsPbbVipTable
                        ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                         (INT4) u4FirstPortId,
                         (INT4 *) &u4NextPortId) == SNMP_FAILURE)
                        return CLI_SUCCESS;
                }
                while (u4NextContextId == u4ContextId)
                {
                    nmhGetFsPbbVipISid (u4NextContextId, u4NextPortId, &u4Isid);
                    if (u4Isid != 1)
                    {
                        if (PbbShowIsidInContext (CliHandle, u4Isid,
                                                  u4ContextId, u4NextPortId)
                            == CLI_FAILURE)
                        {
                            break;
                        }
                    }
                    u4FirstContextId = u4NextContextId;
                    u4FirstPortId = u4NextPortId;
                    if (nmhGetNextIndexFsPbbVipTable
                        ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                         (INT4) u4FirstPortId,
                         (INT4 *) &u4NextPortId) == SNMP_FAILURE)
                        return CLI_SUCCESS;

                }
            }
        }
        else if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
        {
            if (nmhGetFirstIndexFsPbbCBPServiceMappingTable
                ((INT4 *) &u4NextContextId,
                 (INT4 *) &u4NextPortId, &u4NextIsid) == SNMP_SUCCESS)
            {
                while (u4NextContextId != u4ContextId)
                {
                    u4FirstContextId = u4NextContextId;
                    u4FirstPortId = u4NextPortId;
                    u4FirstIsid = u4NextIsid;
                    if (nmhGetNextIndexFsPbbCBPServiceMappingTable
                        ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                         (INT4) u4FirstPortId, (INT4 *) &u4NextPortId,
                         u4FirstIsid, &u4NextIsid) == SNMP_FAILURE)
                        return CLI_SUCCESS;
                }
                u4FirstIsid = 0;
                while (u4NextContextId == u4ContextId)
                {
                    if (u4FirstIsid != u4NextIsid)
                    {
                        if (PbbShowIsidInContext (CliHandle, u4NextIsid,
                                                  u4NextContextId, 0)
                            == CLI_FAILURE)
                        {
                            break;
                        }
                    }
                    u4FirstContextId = u4NextContextId;
                    u4FirstPortId = u4NextPortId;
                    u4FirstIsid = u4NextIsid;
                    if (nmhGetNextIndexFsPbbCBPServiceMappingTable
                        ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                         (INT4) u4FirstPortId, (INT4 *) &u4NextPortId,
                         u4FirstIsid, &u4NextIsid) == SNMP_FAILURE)
                        return CLI_SUCCESS;
                }
            }
        }
    }
    CliPrintf (CliHandle, "\r\n-------------------------- \r\n\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbShowIsidInContext                               */
/*                                                                           */
/*     DESCRIPTION      : This fnction displayes all info related to the isid*/
/*                                                                           */
/*     INPUT            : u4Isid -       Isid                                */
/*                      : u4ContextId  - Context ID                          */
/*                        u4VipIfIndex - Vip IfIndex                         */
/*                                       In case of B-Component, VipIfIndex  */
/*                                       will be passed as zero and will be  */
/*                                       ignored.                            */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbShowIsidInContext (tCliHandle CliHandle, UINT4 u4Isid, UINT4 u4ContextId,
                      UINT4 u4VipIfIndex)
{
    UINT1              *pPortList1 = NULL;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tMacAddr            NextMacAddr;
    UINT4               u4PipPort = PBB_INIT_VAL;
    UINT4               u4VlanId = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    UINT4               u4LocalIsid = PBB_INIT_VAL;
    UINT4               u4Bvid = PBB_INIT_VAL;
    UINT2               u2LocalPortId = 0;
    UINT4               u4ByteIndex = PBB_INIT_VAL;
    UINT4               u4BitIndex = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    INT4                i4PagingStatus = CLI_SUCCESS;
    INT4                i4IsidStaticRowStatus = PBB_INIT_VAL;
    INT4                i4RetValCBPRowStatus = PBB_INIT_VAL;
    INT4                i4RetStatus = PBB_INIT_VAL;
    INT4                i4RowStatus = PBB_INVALID_ROWSTATUS;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT2               u2VlanFlag = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    UINT1               au1TmpAll[VLAN_LIST_SIZE];
    UINT1               au1NameStr[PBB_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[PBB_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1OuiVal[PBB_OUI_LENGTH];
    UINT1               au1OuiStrVal[PBB_CLI_OUI_STR_LEN];
    UINT1               au1ContextName[PBB_SWITCH_ALIAS_LEN];
    UINT1               u1ServiceType = PBB_INIT_VAL;

    tSNMP_OCTET_STRING_TYPE LocalClientOui;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tSNMP_OCTET_STRING_TYPE VlanListAll;
    tSNMP_OCTET_STRING_TYPE ServiceType;

    CLI_MEMSET (au1OuiVal, 0, PBB_OUI_LENGTH);
    CLI_MEMSET (&LocalClientOui, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    LocalClientOui.i4_Length = PBB_OUI_LENGTH;
    LocalClientOui.pu1_OctetList = au1OuiVal;

    ServiceType.i4_Length = 1;
    ServiceType.pu1_OctetList = &u1ServiceType;

    CLI_MEMSET (au1String, 0, PBB_CLI_MAX_MAC_STRING_SIZE);
    CLI_MEMSET (NextMacAddr, 0, sizeof (tMacAddr));

    CLI_MEMSET (au1ContextName, 0, PBB_SWITCH_ALIAS_LEN);
    PbbVcmGetAliasName (u4ContextId, au1ContextName);

    if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
    {
        u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;
    }

    if (u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        /* Get the Local port number of the Vip, mapped to this ISID. */
        i4RetStatus = PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId);
        if (i4RetStatus == PBB_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\nISID      : ");
            CliPrintf (CliHandle, "%-4d     ", u4Isid);
            CliPrintf (CliHandle, "\r\nComponent : ");
            CliPrintf (CliHandle, "%s     ", au1ContextName);
            nmhGetFsPbbVipRowStatus (u4ContextId, u4VipIfIndex,
                                     &i4IsidStaticRowStatus);

            CliPrintf (CliHandle, "\r\nStatus    : ");
            if (i4IsidStaticRowStatus == PBB_ACTIVE)
                CliPrintf (CliHandle, "ACTIVE ");
            else
                CliPrintf (CliHandle, "DISABLED");

            if (i4RetStatus == PBB_SUCCESS)
            {
                CliPrintf (CliHandle, "\r\n\r\n---------------   ");
                CliPrintf (CliHandle, "\r\nPIP Member Port");
                CliPrintf (CliHandle, "\r\n---------------   ");
                CliPrintf (CliHandle, "\r\nPort Id      : ");
                i4RetStatus = nmhGetFsPbbVipToPipMappingPipIfIndex
                    (u4ContextId, (INT4) u4VipIfIndex, (INT4 *) &u4PipPort);
                if (i4RetStatus == SNMP_SUCCESS)
                {
                    CLI_MEMSET (au1NameStr, 0, sizeof (au1NameStr));
                    if (CfaCliGetIfName (u4PipPort,
                                         (INT1 *) au1NameStr) == CLI_FAILURE)
                    {
                        i4PagingStatus =
                            CliPrintf (CliHandle, "%% Invalid Port %d\r\n",
                                       u4PipPort);
                        if (i4PagingStatus == CLI_FAILURE)
                        {
                            return CLI_FAILURE;
                        }
                        return CLI_SUCCESS;
                    }

                    CLI_MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
                    CliPrintf (CliHandle, "%s    ", au1NameStr);
                    i4RetStatus = nmhGetFsPbbPipBMACAddress (u4PipPort,
                                                             &NextMacAddr);
                    if (i4RetStatus == SNMP_SUCCESS)
                    {
                        CLI_MEMSET (au1String, 0, PBB_CLI_MAX_MAC_STRING_SIZE);
                        PrintMacAddress (NextMacAddr, au1String);
                        CliPrintf (CliHandle, "\r\nPIP Mac addr : ");
                        CliPrintf (CliHandle, "%s", au1String);
                    }

                }
                CLI_MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
                i4RetStatus = nmhGetFsPbbVipDefaultDstBMAC (u4ContextId,
                                                            u4VipIfIndex,
                                                            &NextMacAddr);
                if (i4RetStatus == SNMP_SUCCESS)
                {
                    PBB_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));
                    if ((PBB_MEMCMP (au1ZeroMacAddr, NextMacAddr,
                                     PBB_MAC_ADDR_LEN)) != PBB_INIT_VAL)
                    {

                        CLI_MEMSET (au1String, 0, PBB_CLI_MAX_MAC_STRING_SIZE);
                        PrintMacAddress (NextMacAddr, au1String);
                        CliPrintf (CliHandle, "\r\nDefault Dest Mac addr : ");
                        CliPrintf (CliHandle, "%s", au1String);
                    }
                }
                i4RetStatus = nmhGetFsPbbVipType (u4ContextId, u4VipIfIndex,
                                                  &ServiceType);
                if (ServiceType.pu1_OctetList[0] == PBB_VIP_TYPE_INGRESS)
                {
                    CliPrintf (CliHandle, "\r\nService Type : Ingress");
                }
                else if (ServiceType.pu1_OctetList[0] == PBB_VIP_TYPE_EGRESS)
                {
                    CliPrintf (CliHandle, "\r\nService Type : Egress");
                }
                else if (ServiceType.pu1_OctetList[0] == PBB_VIP_TYPE_BOTH)
                {
                    CliPrintf (CliHandle, "\r\nService Type : Ingress-Egress");
                }

                /*VLAN mapping */
                CLI_MEMSET (au1TmpAll, 0, VLAN_LIST_SIZE);
                VlanListAll.pu1_OctetList = &au1TmpAll[0];
                VlanListAll.i4_Length = VLAN_LIST_SIZE;

                CliPrintf (CliHandle, "\r\nVLAN with Tagged PIP : ");

                PbbVlanGetMemberVlanList (u4ContextId, &VlanListAll,
                                          u2LocalPortId, PBB_TRUE);
                for (u4ByteIndex = 0; u4ByteIndex < VLAN_LIST_SIZE;
                     u4ByteIndex++)
                {
                    if (VlanListAll.pu1_OctetList[u4ByteIndex] != 0)
                    {
                        u2VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];

                        for (u4BitIndex = 0;
                             ((u4BitIndex < PBB_PORTS_PER_BYTE) &&
                              (u2VlanFlag != 0)); u4BitIndex++)
                        {
                            if ((u2VlanFlag & PBB_BIT8) != 0)
                            {
                                u4VlanId = (UINT2)
                                    ((u4ByteIndex * PBB_PORTS_PER_BYTE) +
                                     u4BitIndex + 1);
                                i4PagingStatus =
                                    CliPrintf (CliHandle, "%-4d;    ",
                                               u4VlanId);
                                if (i4PagingStatus == CLI_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                            }
                            u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                        }
                    }
                }
                CLI_MEMSET (au1TmpAll, 0, VLAN_LIST_SIZE);
                VlanListAll.pu1_OctetList = &au1TmpAll[0];
                VlanListAll.i4_Length = VLAN_LIST_SIZE;

                CliPrintf (CliHandle, "\r\nVLAN with Un-tagged PIP : ");
                PbbVlanGetMemberVlanList (u4ContextId, &VlanListAll,
                                          u2LocalPortId, PBB_FALSE);
                for (u4ByteIndex = 0; u4ByteIndex < VLAN_LIST_SIZE;
                     u4ByteIndex++)
                {
                    if (VlanListAll.pu1_OctetList[u4ByteIndex] != 0)
                    {
                        u2VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];

                        for (u4BitIndex = 0;
                             ((u4BitIndex < PBB_PORTS_PER_BYTE) &&
                              (u2VlanFlag != 0)); u4BitIndex++)
                        {
                            if ((u2VlanFlag & PBB_BIT8) != 0)
                            {
                                u4VlanId =
                                    (UINT2)
                                    ((u4ByteIndex * PBB_PORTS_PER_BYTE)
                                     + u4BitIndex + 1);
                                i4PagingStatus =
                                    CliPrintf (CliHandle, "%-4d;    ",
                                               u4VlanId);
                                if (i4PagingStatus == CLI_FAILURE)
                                {
                                    return CLI_FAILURE;
                                }
                            }
                            u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                        }
                    }

                }
            }
        }
    }                            /*end of I component */

    if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
    {                            /*B component */
        CliPrintf (CliHandle, "\r\nISID      : ");
        CliPrintf (CliHandle, "%-4d     ", u4Isid);
        CliPrintf (CliHandle, "\r\nComponent : ");
        CliPrintf (CliHandle, "%s     ", au1ContextName);
        /*Member ports CBP */
        CliPrintf (CliHandle, "\r\n\r\n----------------- ");
        CliPrintf (CliHandle, "\r\nCBP Member Ports:");
        CliPrintf (CliHandle, "\r\n-----------------");
        pPortList1 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pPortList1 == NULL)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "PbbShowIsidInContext : Error in allocating memory "
                     "for pPortList1\r\n");
            return CLI_FAILURE;
        }

        CLI_MEMSET (pPortList1, 0, sizeof (tLocalPortList));
        SnmpPorts.pu1_OctetList = pPortList1;
        SnmpPorts.i4_Length = sizeof (tLocalPortList);

        if (PbbGetCbpForIsid (u4ContextId, u4Isid, &SnmpPorts) == PBB_SUCCESS)
        {
            for (u2ByteIndex = 0; u2ByteIndex < PBB_PORT_LIST_SIZE;
                 u2ByteIndex++)
            {
                u1PortFlag = SnmpPorts.pu1_OctetList[u2ByteIndex];
                for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & PBB_BIT8) != 0)
                    {
                        u2LocalPortId = (UINT4)
                            ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                             u2BitIndex + 1);
                        PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                       u2LocalPortId,
                                                       (INT4 *) &u4Port);

                        CLI_MEMSET (au1NameStr, 0, sizeof (au1NameStr));
                        if (CfaCliGetIfName (u4Port,
                                             (INT1 *) au1NameStr) ==
                            CLI_FAILURE)
                        {
                            i4PagingStatus = CliPrintf (CliHandle,
                                                        "\r\n%% Invalid Port %d\r\n",
                                                        u4Port);
                            UtilPlstReleaseLocalPortList (pPortList1);
                            if (i4PagingStatus == CLI_FAILURE)
                            {
                                return CLI_FAILURE;
                            }
                            return CLI_SUCCESS;
                        }

                        CliPrintf (CliHandle, "\r\n\r\nPort Id: %s    ",
                                   au1NameStr);
                        if (nmhGetFsPbbCBPServiceMappingRowStatus
                            (u4ContextId, u4Port, u4Isid,
                             &i4RetValCBPRowStatus) != SNMP_FAILURE)
                        {
                            CliPrintf (CliHandle, "\r\nStatus : ");
                            if (i4RetValCBPRowStatus == PBB_ACTIVE)
                                CliPrintf (CliHandle, "ACTIVE ");
                            else
                                CliPrintf (CliHandle, "DISABLED");

                        }

                        nmhGetFsPbbCBPServiceMappingBVid (u4ContextId,
                                                          (INT4) u4Port, u4Isid,
                                                          (INT4 *) &u4Bvid);
                        if (u4Bvid != 0)
                        {
                            CliPrintf (CliHandle, "\r\nBVlan ID on the Port: ");
                            CliPrintf (CliHandle, "%-4d    ", u4Bvid);
                        }

                        nmhGetFsPbbCBPServiceMappingLocalSid (u4ContextId,
                                                              u4Port, u4Isid,
                                                              &u4LocalIsid);
                        CliPrintf (CliHandle, "\r\nLocal ISID on the Port: ");
                        if (u4LocalIsid == 1)
                            CliPrintf (CliHandle, "INVALID");
                        else
                            CliPrintf (CliHandle, "%-4d    ", u4LocalIsid);

                        i4RetStatus = nmhGetFsPbbCBPServiceMappingType
                            (u4ContextId, (INT4) u4Port, u4Isid, &ServiceType);
                        if (ServiceType.pu1_OctetList[0] == PBB_INGRESS)
                        {
                            CliPrintf (CliHandle, "\r\nService Type : Ingress");
                        }
                        else if (ServiceType.pu1_OctetList[0] == PBB_EGRESS)
                        {
                            CliPrintf (CliHandle, "\r\nService Type : Egress");
                        }
                        else if (ServiceType.pu1_OctetList[0] ==
                                 PBB_EGRESS_INGRESS)
                        {
                            CliPrintf (CliHandle,
                                       "\r\nService Type : Ingress-Egress");
                        }

                        CLI_MEMSET (NextMacAddr, 0, sizeof (tMacAddr));
                        i4RetStatus =
                            nmhGetFsPbbCBPServiceMappingDefaultBackboneDest
                            (u4ContextId, u4Port, u4Isid, &NextMacAddr);
                        if (i4RetStatus == SNMP_SUCCESS)
                        {

                            PBB_MEMSET (au1ZeroMacAddr, 0, sizeof (tMacAddr));

                            if ((PBB_MEMCMP (au1ZeroMacAddr, NextMacAddr,
                                             PBB_MAC_ADDR_LEN)) != PBB_INIT_VAL)
                            {
                                CLI_MEMSET (au1String, 0,
                                            PBB_CLI_MAX_MAC_STRING_SIZE);
                                PrintMacAddress (NextMacAddr, au1String);
                                CliPrintf (CliHandle,
                                           "\r\nDefault Dest Mac addr: ");
                                CliPrintf (CliHandle, " %s", au1String);
                            }
                        }

                        if (nmhGetFsPbbOUIRowStatus (u4ContextId, u4Isid,
                                                     u4Port, &i4RowStatus)
                            == SNMP_SUCCESS)
                        {
                            if (i4RowStatus == PBB_ACTIVE)
                            {
                                CLI_MEMSET (au1OuiStrVal, 0,
                                            PBB_CLI_OUI_STR_LEN);
                                CLI_MEMSET (LocalClientOui.pu1_OctetList, 0,
                                            PBB_OUI_LENGTH);
                                nmhGetFsPbbOUI (u4ContextId, u4Isid, u4Port,
                                                &LocalClientOui);
                                CliOctetToStr (LocalClientOui.pu1_OctetList,
                                               LocalClientOui.i4_Length,
                                               au1OuiStrVal,
                                               PBB_CLI_OUI_STR_LEN);
                                i4PagingStatus =
                                    CliPrintf (CliHandle, "\r\nOUI : %s\r\n",
                                               au1OuiStrVal);

                                if (i4PagingStatus == CLI_FAILURE)
                                {
                                    UtilPlstReleaseLocalPortList (pPortList1);
                                    return CLI_FAILURE;
                                }
                            }
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }
            }                    /*end of member port CBP */
        }                        /*end of if getcbp success */
        UtilPlstReleaseLocalPortList (pPortList1);
    }                            /*end of B component */

    i4PagingStatus = CliPrintf (CliHandle, "\r\n");
    if (i4PagingStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbGetFirstOUIPortInIsidContext                    */
/*                                                                           */
/*     DESCRIPTION      : This function gets first port inside isid context  */
/*                            for which oui is set                           */
/*                                                                           */
/*     INPUT            : u4ContextId  - context identifier                  */
/*                      : u4Isid       - isid                                */
/*                                                                           */
/*     OUTPUT           : pu4Port  - port identifier                        */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
PbbGetFirstOUIPortInIsidContext (UINT4 u4ContextId,
                                 UINT4 u4Isid, UINT4 *pu4Port)
{
    UINT4               u4CurrPort = PBB_INIT_VAL;
    UINT4               u4NextCxtId = PBB_INIT_VAL;
    UINT4               u4NextIsid = PBB_INIT_VAL;

    if (nmhGetNextIndexFsPbbISIDOUITable ((INT4) u4ContextId,
                                          (INT4 *) &u4NextCxtId,
                                          u4Isid, &u4NextIsid,
                                          (INT4) u4CurrPort,
                                          (INT4 *) pu4Port) == SNMP_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTranslateIsid                                     */
/*                                                                           */
/*     DESCRIPTION      : This function set Local ISID for a CBP             */
/*                                                                           */
/*     INPUT            : u4LocalIsid  - Local Isid                          */
/*                      : pu1MemberPorts  - Ports List                       */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTranslateIsid (tCliHandle CliHandle,
                  UINT4 u4LocalIsid, tPortList MemberPortList)
{
    UINT4               u4ErrCode = PBB_NO_ERROR;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    UINT4               u4PortNum = PBB_INIT_VAL;
    UINT4               u4CbpIndex = PBB_INIT_VAL;
    UINT1               u1Bool = PBB_TRUE;
    UINT1               i1IsidSet = PBB_TRUE;
    tCbpStatusInfo     *pCbpStatus = NULL;

    if (PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                             PbbCbpStatusMemPoolId, pCbpStatus) == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Allocating memory for pCbpStatus \n");
        return CLI_FAILURE;
    }
    PBB_MEMSET (pCbpStatus, 0,
                (sizeof (tCbpStatusInfo) * PBB_MAX_PORTS_PER_ISID));
    u4Isid = CLI_GET_ISID ();
    u4ContextId = CLI_GET_CXT_ID ();

    UNUSED_PARAM (CliHandle);

    for (u2ByteIndex = 0; u2ByteIndex < PBB_IFPORT_LIST_SIZE; u2ByteIndex++)
    {
        u1PortFlag = MemberPortList[u2ByteIndex];
        for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                              && (u1PortFlag != 0)); u2BitIndex++)
        {
            if ((u1PortFlag & PBB_BIT8) != 0)
            {
                u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                  u2BitIndex + 1);

                if (nmhGetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4Port,
                                                           u4Isid,
                                                           &((pCbpStatus +
                                                              u4CbpIndex)->
                                                             i4RetValCBPRowStatus))
                    == SNMP_FAILURE)
                {
                    u1Bool = FALSE;
                    break;
                }
                (pCbpStatus + u4CbpIndex)->u4CbpNum = u4Port;

                /* Store the Previous Configuration Data */
                if (nmhGetFsPbbCBPServiceMappingLocalSid (u4ContextId, u4Port,
                                                          u4Isid,
                                                          &((pCbpStatus +
                                                             u4CbpIndex)->
                                                            u4LocalIsid)) ==
                    SNMP_FAILURE)
                {
                    u1Bool = PBB_FALSE;
                    break;
                }

                /* Set the Row Status to NOT_IN_SERVICE */
                if ((pCbpStatus + u4CbpIndex)->i4RetValCBPRowStatus ==
                    PBB_ACTIVE)
                {
                    if (nmhTestv2FsPbbCBPServiceMappingRowStatus
                        (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                         PBB_NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        u1Bool = PBB_FALSE;
                        break;
                    }

                    if (nmhSetFsPbbCBPServiceMappingRowStatus
                        (u4ContextId, u4Port, u4Isid,
                         PBB_NOT_IN_SERVICE) == SNMP_FAILURE)
                    {
                        u1Bool = PBB_FALSE;
                        break;
                    }
                }

                u4CbpIndex = u4CbpIndex + 1;

                if (nmhTestv2FsPbbCBPServiceMappingLocalSid
                    (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                     u4LocalIsid) == SNMP_FAILURE)
                {
                    u1Bool = PBB_FALSE;
                    break;
                }
            }
            u1PortFlag = (UINT1) (u1PortFlag << 1);
        }

        if (u1Bool == PBB_FALSE)
        {
            break;
        }
    }

    u4PortNum = u4CbpIndex;

    if (u1Bool != FALSE)
    {
        /* Set the Local Isid Values */
        for (u4CbpIndex = 0; u4CbpIndex < u4PortNum; u4CbpIndex++)
        {
            u4Port = (pCbpStatus + u4CbpIndex)->u4CbpNum;

            if (nmhSetFsPbbCBPServiceMappingLocalSid (u4ContextId, u4Port,
                                                      u4Isid,
                                                      u4LocalIsid) ==
                SNMP_FAILURE)
            {
                u1Bool = PBB_FALSE;
                i1IsidSet = PBB_FALSE;
                break;
            }
        }
    }
    /* Restore the previous Configuration Data */
    if (i1IsidSet == PBB_FALSE)
    {
        for (u4CbpIndex = 0; u4CbpIndex < u4PortNum; u4CbpIndex++)
        {
            u4Port = (pCbpStatus + u4CbpIndex)->u4CbpNum;
            u4LocalIsid = (pCbpStatus + u4CbpIndex)->u4LocalIsid;

            if (nmhTestv2FsPbbCBPServiceMappingLocalSid
                (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                 u4LocalIsid) == SNMP_FAILURE)
            {
                u1Bool = FALSE;
                break;

            }

            if (nmhSetFsPbbCBPServiceMappingLocalSid (u4ContextId, u4Port,
                                                      u4Isid,
                                                      u4LocalIsid) ==
                SNMP_FAILURE)
            {
                u1Bool = FALSE;
                break;

            }

        }
    }
    /*set the Row status to previous value - 
       both in case of failure and success */
    for (u4CbpIndex = 0; u4CbpIndex < u4PortNum; u4CbpIndex++)
    {
        u4Port = (pCbpStatus + u4CbpIndex)->u4CbpNum;
        if (((pCbpStatus + u4CbpIndex)->i4RetValCBPRowStatus == PBB_ACTIVE))
        {
            if (nmhTestv2FsPbbCBPServiceMappingRowStatus
                (&u4ErrCode, u4ContextId, u4Port, u4Isid,
                 PBB_ACTIVE) == SNMP_FAILURE)
            {
                if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                           PbbCbpStatusMemPoolId,
                                           (UINT1 *) pCbpStatus) == MEM_FAILURE)
                {
                    PBB_TRC (ALL_FAILURE_TRC,
                             "Error in Releasing  memory for pCbpStatus \n");
                }
                return CLI_FAILURE;
            }

            if (nmhSetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4Port,
                                                       u4Isid,
                                                       PBB_ACTIVE) ==
                SNMP_FAILURE)
            {
                if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                           PbbCbpStatusMemPoolId,
                                           (UINT1 *) pCbpStatus) == MEM_FAILURE)
                {
                    PBB_TRC (ALL_FAILURE_TRC,
                             "Error in Releasing  memory for pCbpStatus \n");
                }
                return CLI_FAILURE;
            }
        }
    }
    if (PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                               PbbCbpStatusMemPoolId,
                               (UINT1 *) pCbpStatus) == MEM_FAILURE)
    {
        PBB_TRC (ALL_FAILURE_TRC,
                 "Error in Releasing  memory for pCbpStatus \n");
        return CLI_FAILURE;
    }

    if (u1Bool != FALSE)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCliGetContextForShowCmd                      */
/*                                                                           */
/*    Description         : This function is called only when the show       */
/*                          command is given. It handles 3 different         */
/*                          ways of show commands.                           */
/*                            1. If switch name is given, we have to get     */
/*                               the context Id from it.                     */
/*                            2. If Interface Index is present, from that    */
/*                               we have to get the context Id.              */
/*                            3. Else we have to go for each and every       */
/*                               context.                                    */
/*                          For the above all thing we have to check for     */
/*                          the Module status of the context.                */
/*                                                                           */
/*    Input(s)            : u4CurrContext    - Context-Id.                   */
/*                          pu1Name          - Switch-Name.                  */
/*                          u4IfIndex        - Interface Index.              */
/*                                                                           */
/*    Output(s)           : CliHandle        - Contains error messages.      */
/*                          pu4NextContextId - Context Id.                   */
/*                          pu2LocalPort     - Local Port Number.            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCliGetContextForShowCmd (tCliHandle CliHandle, UINT1 *pu1Name,
                            UINT4 u4IfIndex, UINT4 u4CurrContext,
                            UINT4 *pu4NextContext, UINT2 *pu2LocalPort)
{
    UINT4               u4ContextId;
    INT4                i4PagingStatus = CLI_SUCCESS;
    UINT1               au1ContextName[PBB_SWITCH_ALIAS_LEN];

    *pu2LocalPort = 0;
    UNUSED_PARAM (u4CurrContext);

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1Name != NULL)
    {
        if (PbbVcmIsSwitchExist (pu1Name, &u4ContextId) != VCM_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1Name);
            return PBB_FAILURE;
        }
    }
    /* If IfIndex is given then get the Context-Id from it */
    else if (u4IfIndex != 0)
    {
        if (PbbVcmGetContextInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                             pu2LocalPort) != PBB_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Interface not mapped to "
                       "any of the context.\r\n ");
            return PBB_FAILURE;
        }
    }
    else
    {
        /* Case 1: At first entry for this funtion, If Switch-name is not given, 
           then get the first active Context. For this if we give 
           0xffffffff as current context for pbbGetNextActiveContext 
           function it will return the First Active context. 
           Case 2: At the Next frequent entries it will as per we want. */
        if (PbbGetNextActiveContext (u4CurrContext,
                                     &u4ContextId) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }

    do
    {
        u4CurrContext = u4ContextId;

        /* To avoid "Switch <switch-name>" print in SI */
        if (PbbVcmGetSystemModeExt (PBB_PROTOCOL_ID) == VCM_MI_MODE)
        {
            /* if switch name is not given we have to get the switch-name 
             * from context-id for Display purpose */
            PBB_MEMSET (au1ContextName, 0, PBB_SWITCH_ALIAS_LEN);

            if (pu1Name == NULL)
            {
                PbbVcmGetAliasName (u4ContextId, au1ContextName);
            }
            else
            {
                PBB_STRNCPY (au1ContextName, pu1Name, PBB_STRLEN (pu1Name));
                au1ContextName[PBB_STRLEN (pu1Name)] = '\0';
            }
            i4PagingStatus = CliPrintf (CliHandle, "\r\n\rSwitch %s\r\n",
                                        au1ContextName);
            if (i4PagingStatus == CLI_FAILURE)
            {
                return PBB_FAILURE;
            }
        }
        *pu4NextContext = u4ContextId;
    }
    while (PbbGetNextActiveContext (u4CurrContext,
                                    &u4ContextId) == PBB_SUCCESS);
    return PBB_FAILURE;
}

INT4
PbbCliGetVipIndex (UINT4 *pu4Value)
{
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    PBB_LOCK ();
    u4Isid = CLI_GET_ISID ();
    u4ContextId = CLI_GET_CXT_ID ();

    if (PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId) == PBB_FAILURE)
    {
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                   (INT4 *) pu4Value);

    PBB_UNLOCK ();
    return PBB_SUCCESS;
}

INT4
PbbCliGetVipIndexOfIsid (UINT4 u4ContextId, UINT4 u4Isid, UINT4 *pu4Value)
{
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    PBB_LOCK ();

    if (PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId) == PBB_FAILURE)
    {
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                   (INT4 *) pu4Value);

    PBB_UNLOCK ();
    return PBB_SUCCESS;
}

INT4
PbbCliGetVip (UINT1 *pu1Isid, UINT4 *pu4Value)
{
    UINT4               u4ContextId;
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    PBB_LOCK ();
    u4ContextId = CLI_GET_CXT_ID ();
    if (PbbGetVIPValue
        (u4ContextId, *((UINT4 *) (VOID *) pu1Isid),
         &u2LocalPortId) == PBB_FAILURE)
    {
        PBB_UNLOCK ();
        return PBB_FAILURE;
    }
    PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                   (INT4 *) pu4Value);

    PBB_UNLOCK ();
    return PBB_SUCCESS;
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : PbbCliShowDebugging
 *
 *     DESCRIPTION      : Shows PBB debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/

INT4
PbbCliShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DbgLevel = PBB_INIT_VAL;
    INT4                i4ShutDownStatus = PBB_INIT_VAL;

    nmhGetFsPbbShutdownStatus (&i4ShutDownStatus);
    if (i4ShutDownStatus == PBB_SNMP_TRUE)
    {
        /* return if pbb module is shutdown */
        return CLI_FAILURE;
    }
    nmhGetFsPbbTraceOption (&i4DbgLevel);

    if (i4DbgLevel == PBB_INIT_VAL)
    {
        return CLI_SUCCESS;
    }
    CliPrintf (CliHandle, "PBB :\r\n");
    if (i4DbgLevel == PBB_ALL_TRC)
    {
        CliPrintf (CliHandle, "  PBB all debugging is on\r\n");
        return CLI_SUCCESS;
    }
    if ((i4DbgLevel & INIT_SHUT_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB init and shutdown debugging is on\r\n");
    }
    if ((i4DbgLevel & MGMT_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB management debugging is on\r\n");
    }
    if ((i4DbgLevel & DATA_PATH_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB Data path debugging is on\r\n");
    }
    if ((i4DbgLevel & CONTROL_PLANE_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB control plane debugging is on\r\n");
    }
    if ((i4DbgLevel & DUMP_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB packet dump debugging is on\r\n");
    }
    if ((i4DbgLevel & OS_RESOURCE_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB resources debugging is on\r\n");
    }
    if ((i4DbgLevel & ALL_FAILURE_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB error debugging is on\r\n");
    }
    if ((i4DbgLevel & BUFFER_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB buffer debugging is on\r\n");
    }
    if ((i4DbgLevel & PBB_CRITICAL_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB critical debugging is on\r\n");
    }
    if ((i4DbgLevel & PBB_RED_TRC) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "  PBB redundancy debugging is on\r\n");
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : PbbCliUpdTraceInput 
 *
 *     DESCRIPTION      : This function updates(sets/resets) the trace input
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        pu1TraceInput   - Pointer to trace input string
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
PbbCliUpdTraceInput (tCliHandle CliHandle, UINT1 *pu1TraceInput)
{
    UINT4               u4ErrorCode = PBB_INIT_VAL;
    tSNMP_OCTET_STRING_TYPE TraceInput;

    MEMSET (&TraceInput, PBB_INIT_VAL, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = STRLEN (pu1TraceInput);

    if (nmhTestv2FsPbbTraceInput (&u4ErrorCode, &TraceInput) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsPbbTraceInput (&TraceInput) != SNMP_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * 
 *     FUNCTION NAME    : PbbSetServiceType 
 *
 *     DESCRIPTION      : This function updates(sets/resets) the service type
 *
 *     INPUT            : CliHandle       - CliContext ID
 *                        
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/
INT4
PbbSetServiceType (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4CompType,
                   INT4 i4ServiceType, tPortList MemberPortList)
{
    UINT1              *pau1IsidPorts = NULL;
    UINT4               u4ErrCode = PBB_NO_ERROR;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4Vip = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INIT_VAL;
    INT4                i4Result = PBB_SUCCESS;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    UINT1               u1ServiceType = PBB_INIT_VAL;
    UINT4               u4Port;
    tSNMP_OCTET_STRING_TYPE PortList;
    tSNMP_OCTET_STRING_TYPE ServiceType;
    u4Isid = CLI_GET_ISID ();

    u1ServiceType = (UINT1) i4ServiceType;
    ServiceType.i4_Length = 1;
    ServiceType.pu1_OctetList = &u1ServiceType;

    if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
    {
        u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;

        if (i4CompType == PBB_ICOMPONENT &&
            u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
        {
            CliPrintf (CliHandle,
                       "\r%% PBB: Bridge mode is PBB B-component\r\n");
            return CLI_FAILURE;
        }
        else if (i4CompType == PBB_BCOMPONENT &&
                 u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            CliPrintf (CliHandle,
                       "\r%% PBB: Bridge mode is PBB I-component\r\n");
            return CLI_FAILURE;
        }
    }
    if (i4CompType == PBB_ICOMPONENT)
    {
        if (PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId) != PBB_FAILURE)
        {
            PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                           u2LocalPortId, (INT4 *) &u4Vip);
            if (nmhTestv2FsPbbVipType (&u4ErrCode, u4ContextId, u4Vip,
                                       &ServiceType) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsPbbVipType (u4ContextId, u4Vip,
                                    &ServiceType) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }

    }
    else
    {
        pau1IsidPorts = FsUtilAllocBitList (PBB_IFPORT_LIST_SIZE);
        if (pau1IsidPorts == NULL)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "PbbCreateOUI: Error in allocating memory "
                     "for pau1IsidPorts\r\n");
            return CLI_FAILURE;
        }
        CLI_MEMSET (pau1IsidPorts, PBB_INIT_VAL, PBB_IFPORT_LIST_SIZE);
        PortList.i4_Length = PBB_IFPORT_LIST_SIZE;
        PortList.pu1_OctetList = pau1IsidPorts;

        MEMCPY (PortList.pu1_OctetList, MemberPortList, PortList.i4_Length);

        /*test for loop */
        for (u2ByteIndex = PBB_INIT_VAL; u2ByteIndex < PBB_IFPORT_LIST_SIZE;
             u2ByteIndex++)
        {
            u1PortFlag = PortList.pu1_OctetList[u2ByteIndex];
            for (u2BitIndex = PBB_INIT_VAL; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                             && (u1PortFlag != PBB_FALSE));
                 u2BitIndex++)
            {
                if ((u1PortFlag & PBB_BIT8) != PBB_FALSE)
                {
                    u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                      u2BitIndex + 1);
                    i4Result = nmhTestv2FsPbbCBPServiceMappingType (&u4ErrCode,
                                                                    u4ContextId,
                                                                    u4Port,
                                                                    u4Isid,
                                                                    &ServiceType);
                    if (i4Result == SNMP_FAILURE)
                    {
                        FsUtilReleaseBitList (pau1IsidPorts);
                        return CLI_FAILURE;
                    }

                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
        for (u2ByteIndex = PBB_INIT_VAL; u2ByteIndex < PBB_IFPORT_LIST_SIZE;
             u2ByteIndex++)
        {
            u1PortFlag = PortList.pu1_OctetList[u2ByteIndex];
            for (u2BitIndex = PBB_INIT_VAL; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                             && (u1PortFlag != PBB_FALSE));
                 u2BitIndex++)
            {
                if ((u1PortFlag & PBB_BIT8) != PBB_FALSE)
                {
                    u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                      u2BitIndex + 1);
                    i4Result = nmhSetFsPbbCBPServiceMappingType (u4ContextId,
                                                                 u4Port, u4Isid,
                                                                 &ServiceType);
                    if (i4Result == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        FsUtilReleaseBitList (pau1IsidPorts);
                        return CLI_FAILURE;
                    }

                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
        FsUtilReleaseBitList (pau1IsidPorts);
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbShowBackboneInstanceMap                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays backbone instance           */
/*                        associated with a context                          */
/*                                                                           */
/*     INPUT            : u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbShowBackboneInstanceMap (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4InstanceNum = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    INT4                i4RetVal;

    tSNMP_OCTET_STRING_TYPE pbbInstanceName;
    UINT1               au1NameVal[PBB_INSTANCE_ALIAS_LEN];
    /* Display PBB Instance ID  */
    CLI_MEMSET (au1NameVal, 0, PBB_INSTANCE_ALIAS_LEN);
    CLI_MEMSET (&pbbInstanceName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    pbbInstanceName.i4_Length = PBB_INSTANCE_ALIAS_LEN;
    pbbInstanceName.pu1_OctetList = au1NameVal;

    if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
    {
        u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;
    }

    if (!(u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE ||
          u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE))
    {
        return CLI_SUCCESS;
    }
    i4RetVal = nmhGetFsPbbContextToInstanceId (u4ContextId,
                                               (INT4 *) &u4InstanceNum);
    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    i4RetVal = nmhGetFsPbbInstanceName (u4InstanceNum, &pbbInstanceName);
    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "Backbone Instance: %s\r\n", pbbInstanceName);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbShowBackboneInstanceConfig                      */
/*                                                                           */
/*     DESCRIPTION      : This function displays information of              */
/*                        a backbone instance                                */
/*                                                                           */
/*     INPUT            : pu1InstanceName - Backbone Instance Name           */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbShowBackboneInstanceConfig (tCliHandle CliHandle, UINT1 *pu1InstanceName)
{
    UINT1               au1ContextList[PBB_CONTEXT_LIST_SIZE];
    UINT1               au1String[PBB_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1InstanceName[PBB_INSTANCE_ALIAS_LEN];
    UINT1               au1ContextName[PBB_CONTEXT_ALIAS_LEN + 1];
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4CurrInstanceNum = PBB_INIT_VAL;
    UINT4               u4NextInstanceNum = PBB_INIT_VAL;
    UINT4               u4InstanceNum = PBB_INIT_VAL;
    UINT4               u4BComponents = PBB_INIT_VAL;
    UINT4               u4IComponents = PBB_INIT_VAL;
    UINT4               u4BebPorts = PBB_INIT_VAL;
    INT4                i4RetVal = PBB_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;
    tMacAddr            pbbInstanceMacAddr;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1ContextFlag = PBB_INIT_VAL;
    UINT1               u1Flag = PBB_TRUE;
    tSNMP_OCTET_STRING_TYPE pbbInstanceName;
    tSNMP_OCTET_STRING_TYPE pbbContextList;
    CLI_MEMSET (au1InstanceName, PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN);
    CLI_MEMSET (au1ContextList, PBB_INIT_VAL, PBB_CONTEXT_LIST_SIZE);

    if (pu1InstanceName != NULL)
    {
        pbbInstanceName.i4_Length = CLI_STRLEN (pu1InstanceName);
        pbbInstanceName.pu1_OctetList = pu1InstanceName;
        i4RetVal = PbbGetInstanceIdFromAlias (&pbbInstanceName, &u4InstanceNum);
        if (i4RetVal != PBB_SUCCESS)
        {
            CliPrintf (CliHandle, "%% Error: No such PBB instance exists\r\n");
            return CLI_FAILURE;

        }
    }
    else
    {
        pbbInstanceName.i4_Length = PBB_INSTANCE_ALIAS_LEN;
        pbbInstanceName.pu1_OctetList = au1InstanceName;
    }
    pbbContextList.i4_Length = PBB_SIZING_CONTEXT_LIST_COUNT;
    pbbContextList.pu1_OctetList = au1ContextList;
    i4RetVal = nmhGetFirstIndexFsPbbInstanceTable ((INT4 *) &u4NextInstanceNum);
    if (i4RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\nBackbone Instance Table \r\n");
    CliPrintf (CliHandle, "---------------------------------------\r\n");
    do
    {
        u4CurrInstanceNum = u4NextInstanceNum;
        if ((pu1InstanceName == NULL) ||
            ((pu1InstanceName != NULL) && (u4InstanceNum == u4CurrInstanceNum)))
        {
            u1Flag = PBB_TRUE;
            CLI_MEMSET (au1ContextList, PBB_INIT_VAL, PBB_CONTEXT_LIST_SIZE);
            CLI_MEMSET (au1InstanceName, PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN);
            nmhGetFsPbbInstanceMacAddr (u4CurrInstanceNum, &pbbInstanceMacAddr);
            nmhGetFsPbbInstanceName (u4CurrInstanceNum, &pbbInstanceName);
            nmhGetFsPbbInstanceBComponents (u4CurrInstanceNum, &u4BComponents);
            nmhGetFsPbbInstanceIComponents (u4CurrInstanceNum, &u4IComponents);
            nmhGetFsPbbInstanceBebPorts (u4CurrInstanceNum, &u4BebPorts);
            PbbGetContextListForInstance (u4CurrInstanceNum, &pbbContextList);

            CliPrintf (CliHandle, "\nInstance Name      : %s\r\n",
                       pbbInstanceName);
            CLI_MEMSET (au1String, 0, PBB_CLI_MAX_MAC_STRING_SIZE);
            PrintMacAddress (pbbInstanceMacAddr, au1String);

            CliPrintf (CliHandle, "\rMac Address        : %s\r\n", au1String);
            CliPrintf (CliHandle, "\rNo.of I-Components : %d\r\n",
                       u4IComponents);
            CliPrintf (CliHandle, "\rNo.of B-Components : %d\r\n",
                       u4BComponents);
            CliPrintf (CliHandle, "\rNo.of Beb Ports    : %d\r\n", u4BebPorts);

            for (u2ByteIndex = 0;
                 u2ByteIndex < PBB_SIZING_CONTEXT_LIST_COUNT; u2ByteIndex++)
            {
                u1ContextFlag = pbbContextList.pu1_OctetList[u2ByteIndex];
                for (u2BitIndex = 0; ((u2BitIndex < PBB_CONTEXTS_PER_BYTE)
                                      && (u1ContextFlag != 0)); u2BitIndex++)
                {
                    if ((u1ContextFlag & PBB_BIT8) != 0)
                    {
                        u4ContextId = ((UINT4)
                                       ((u2ByteIndex * PBB_CONTEXTS_PER_BYTE) +
                                        u2BitIndex + 1)) - 1;
                        if (PbbVcmGetSystemModeExt (PBB_PROTOCOL_ID) ==
                            VCM_MI_MODE)
                        {
                            /* if switch name is not given we have to get the 
                             * switch-name from context-id for Display purpose
                             */
                            PBB_MEMSET (au1ContextName, 0,
                                        PBB_CONTEXT_ALIAS_LEN);
                            PbbVcmGetAliasName (u4ContextId, au1ContextName);
                            if (u1Flag == PBB_TRUE)
                            {
                                CliPrintf (CliHandle, "\rAssociated Virtual"
                                           " Context(s):");

                                CliPrintf (CliHandle, " Switch %s",
                                           au1ContextName);
                            }
                            else
                            {
                                CliPrintf (CliHandle, ", Switch %s",
                                           au1ContextName);

                            }
                            u1Flag = PBB_FALSE;

                        }
                    }
                    u1ContextFlag = (UINT1) (u1ContextFlag << 1);
                }
            }
            i4PagingStatus = CliPrintf (CliHandle, "\r\n\n");
            if (i4PagingStatus == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }
    while (nmhGetNextIndexFsPbbInstanceTable ((INT4) u4CurrInstanceNum,
                                              (INT4 *) &u4NextInstanceNum) !=
           SNMP_FAILURE);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbCreateBackboneInstance                          */
/*                                                                           */
/*     DESCRIPTION      : This function creates a backbone instance          */
/*                                                                           */
/*     INPUT            : pu1InstanceName - Backbone Instance Name           */
/*                        MacAddr         - Backbone Instance Mac Address    */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCreateBackboneInstance (tCliHandle CliHandle,
                           UINT1 *pu1InstanceName, tMacAddr MacAddr)
{
    UINT4               u4ErrorCode = PBB_INIT_VAL;
    UINT4               u4InstanceNum = PBB_INIT_VAL;
    INT4                i4RetVal;
    tMacAddr            au1InMacAddr;
    UINT1               au1InstanceName[PBB_INSTANCE_ALIAS_LEN];
    INT4                i4RowStatus = PBB_INVALID_ROWSTATUS;
    UINT1               u1IsActive = PBB_FALSE;
    tSNMP_OCTET_STRING_TYPE pbbInstanceName;

    CLI_MEMSET (au1InstanceName, PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN);

    CLI_MEMSET (au1InMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));

    pbbInstanceName.i4_Length = CLI_STRLEN (pu1InstanceName);
    pbbInstanceName.pu1_OctetList = pu1InstanceName;

    i4RetVal = PbbGetInstanceIdFromAlias (&pbbInstanceName, &u4InstanceNum);

    /*In case entry for this PBB Instance already exists, 
       we update the mac address for the given PBB instance */
    if (i4RetVal == PBB_SUCCESS)
    {
        if (CLI_MEMCMP (MacAddr, au1InMacAddr, sizeof (tMacAddr)) !=
            PBB_INIT_VAL)
        {
            if (nmhGetFsPbbInstanceRowStatus (u4InstanceNum,
                                              &i4RowStatus) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (i4RowStatus == PBB_ACTIVE)
            {
                if (nmhTestv2FsPbbInstanceRowStatus (&u4ErrorCode,
                                                     u4InstanceNum,
                                                     PBB_NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                else if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum,
                                                       PBB_NOT_IN_SERVICE) ==
                         SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
                u1IsActive = PBB_TRUE;
            }
            if (nmhTestv2FsPbbInstanceMacAddr (&u4ErrorCode, u4InstanceNum,
                                               MacAddr) == SNMP_FAILURE)
            {
                if (u1IsActive == PBB_TRUE)
                {
                    if (nmhTestv2FsPbbInstanceRowStatus (&u4ErrorCode,
                                                         u4InstanceNum,
                                                         PBB_ACTIVE) ==
                        SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                    else if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum,
                                                           PBB_ACTIVE) ==
                             SNMP_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                }
                return CLI_FAILURE;
            }
            if (u1IsActive == PBB_TRUE)
            {
                if (nmhTestv2FsPbbInstanceRowStatus (&u4ErrorCode,
                                                     u4InstanceNum,
                                                     PBB_ACTIVE) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
            if (nmhSetFsPbbInstanceMacAddr (u4InstanceNum, MacAddr) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (u1IsActive == PBB_TRUE)
            {
                if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum,
                                                  PBB_ACTIVE) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }

    }
    /*In case a new entry for this PBB Instance is to be created */
    else
    {
        if (PbbGetFreeInstanceId (&u4InstanceNum) != PBB_SUCCESS)
        {
            CliPrintf (CliHandle, "%% Error: No more free instances\r\n");
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbbInstanceRowStatus (&u4ErrorCode, u4InstanceNum,
                                             PBB_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum, PBB_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhTestv2FsPbbInstanceName (&u4ErrorCode, (INT4) u4InstanceNum,
                                        &pbbInstanceName) == SNMP_FAILURE)
        {
            /*Delete the row in case name cannot be set */
            if (nmhTestv2FsPbbInstanceRowStatus (&u4ErrorCode, u4InstanceNum,
                                                 PBB_DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum, PBB_DESTROY)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "%% Error: while naming PBB instance\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsPbbInstanceName (u4InstanceNum, &pbbInstanceName)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        /*Check if mac-address is given then set */
        if (CLI_MEMCMP (MacAddr,
                        au1InMacAddr, sizeof (tMacAddr)) != PBB_INIT_VAL)
        {
            if (nmhTestv2FsPbbInstanceMacAddr (&u4ErrorCode, u4InstanceNum,
                                               MacAddr) == SNMP_FAILURE)
            {
                /*Delete the row in case name cannot be set */
                if (nmhTestv2FsPbbInstanceRowStatus
                    (&u4ErrorCode, u4InstanceNum, PBB_DESTROY) == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum, PBB_DESTROY)
                    == SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }
                return CLI_FAILURE;
            }
            if (nmhSetFsPbbInstanceMacAddr (u4InstanceNum, MacAddr) ==
                SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
        if (nmhTestv2FsPbbInstanceRowStatus (&u4ErrorCode,
                                             u4InstanceNum,
                                             PBB_ACTIVE) == SNMP_FAILURE)
        {
            /*Delete the row in case name cannot be set */
            if (nmhTestv2FsPbbInstanceRowStatus (&u4ErrorCode, u4InstanceNum,
                                                 PBB_DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum, PBB_DESTROY)
                == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            return CLI_FAILURE;
        }
        else if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum,
                                               PBB_ACTIVE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbDeleteBackboneInstance                          */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a backbone instance          */
/*                                                                           */
/*     INPUT            : None                                               */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbDeleteBackboneInstance (tCliHandle CliHandle, UINT1 *pu1InstanceName)
{
    UINT4               u4ErrorCode = PBB_INIT_VAL;
    UINT4               u4InstanceNum = PBB_INIT_VAL;
    INT4                i4RetVal;
    UINT1               au1InstanceName[PBB_INSTANCE_ALIAS_LEN];
    tSNMP_OCTET_STRING_TYPE pbbInstanceName;
    CLI_MEMSET (au1InstanceName, PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN);

    pbbInstanceName.i4_Length = CLI_STRLEN (pu1InstanceName);
    pbbInstanceName.pu1_OctetList = pu1InstanceName;

    i4RetVal = PbbGetInstanceIdFromAlias (&pbbInstanceName, &u4InstanceNum);

    /*PBB Instance exists */
    if (i4RetVal == PBB_SUCCESS)
    {
        if (nmhTestv2FsPbbInstanceRowStatus (&u4ErrorCode, u4InstanceNum,
                                             PBB_DESTROY) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsPbbInstanceRowStatus (u4InstanceNum, PBB_DESTROY)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "%% Error: No such PBB instance\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbCreateBackboneInstanceMapping                   */
/*                                                                           */
/*     DESCRIPTION      : This function creates a the mapping of a           */
/*                        backbone instance with a context                   */
/*                                                                           */
/*     INPUT            : pu1InstanceName - Backbone Instance Name           */
/*                                                                           */
/*     OUTPUT           : CliHandle  - Contains error messages               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCreateBackboneInstanceMapping (tCliHandle CliHandle,
                                  UINT4 u4ContextId, UINT1 *pu1InstanceName)
{
    UINT4               u4ErrorCode = PBB_INIT_VAL;
    UINT4               u4InstanceNum = PBB_INIT_VAL;
    INT4                i4RetVal;
    UINT1               au1InstanceName[PBB_INSTANCE_ALIAS_LEN];
    tSNMP_OCTET_STRING_TYPE pbbInstanceName;
    CLI_MEMSET (au1InstanceName, PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN);

    pbbInstanceName.i4_Length = CLI_STRLEN (pu1InstanceName);
    pbbInstanceName.pu1_OctetList = pu1InstanceName;

    i4RetVal = PbbGetInstanceIdFromAlias (&pbbInstanceName, &u4InstanceNum);

    /*PBB Instance exists */
    if (i4RetVal == PBB_SUCCESS)
    {
        i4RetVal = nmhTestv2FsPbbContextToInstanceId (&u4ErrorCode,
                                                      u4ContextId,
                                                      u4InstanceNum);
        if (i4RetVal == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        i4RetVal = nmhSetFsPbbContextToInstanceId (u4ContextId, u4InstanceNum);
        if (i4RetVal == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

    }
    else
    {
        CliPrintf (CliHandle, "%% Error: No such PBB instance\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowPcpEncodingTable                           */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP Decoding table     */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

INT4
PbbShowPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                         UINT1 *pu1ContextName)
{

    INT4                i4CurrIfIndex = PBB_INIT_VAL;
    INT4                i4CurrPcpSelRow = PBB_INIT_VAL;
    INT4                i4CurrPriority = PBB_INIT_VAL;
    INT4                i4CurrDropEligible = PBB_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;
    INT4                i4CurrPcpValue = PBB_INIT_VAL;

    INT4                i4NextIfIndex = PBB_INIT_VAL;
    INT4                i4NextPcpSelRow = PBB_INIT_VAL;
    INT4                i4NextPriority = PBB_INIT_VAL;
    INT4                i4NextDropEligible = PBB_INIT_VAL;

    UINT1               u1Priority = PBB_INIT_VAL;
    UINT1               u1PrintSelRowFlag = PBB_TRUE;
    UINT1               u1PrintPortFlag = PBB_TRUE;
    UINT1               au1NameStr[PBB_MAX_PORT_NAME_LENGTH];
    UINT4               u4TempContextId = PBB_CLI_INVALID_CONTEXT;
    UINT4               u4CurrContextId = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;
    i4CurrIfIndex = i4IfIndex;
    i4CurrPcpSelRow = PBB_INIT_VAL;
    i4CurrPriority = PBB_INIT_VAL;
    i4CurrDropEligible = PBB_INVALID_DE;

    if ((PbbCliGetContextForShowCmd
         (CliHandle, pu1ContextName, i4IfIndex,
          u4TempContextId, &u4CurrContextId, &u2LocalPortId) == PBB_FAILURE)
        && (pu1ContextName != NULL))
    {
        return CLI_FAILURE;

    }

    while (nmhGetNextIndexFsPbbPcpEncodingTable (i4CurrIfIndex, &i4NextIfIndex,
                                                 i4CurrPcpSelRow,
                                                 &i4NextPcpSelRow,
                                                 i4CurrPriority,
                                                 &i4NextPriority,
                                                 i4CurrDropEligible,
                                                 &i4NextDropEligible) ==
           SNMP_SUCCESS)
    {

        if (PbbVcmGetContextInfoFromIfIndex
            ((UINT4) i4NextIfIndex, &u4TempContextId,
             &u2LocalPortId) == PBB_SUCCESS)
        {
            if ((u4CurrContextId == PBB_INIT_VAL)
                || (u4TempContextId == u4CurrContextId))
            {
                if (i4IfIndex != 0)
                {
                    if (i4CurrIfIndex != i4NextIfIndex)
                    {
                        break;
                    }
                }
                if (i4NextIfIndex != i4CurrIfIndex)
                {
                    u1PrintPortFlag = PBB_TRUE;
                }

                if (u1PrintPortFlag == PBB_TRUE)
                {
                    if (CfaCliGetIfName ((UINT4) i4NextIfIndex,
                                         (INT1 *) au1NameStr) == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "%% Invalid Port %d\r\n",
                                   (UINT4) i4NextIfIndex);

                        return CLI_FAILURE;
                    }
                    CliPrintf (CliHandle, "\n\nPort %s    \n", au1NameStr);

                    /*Printing the Priority & DE Values */
                    CliPrintf (CliHandle, "DropEligible: ");
                    for (u1Priority = PBB_INIT_VAL;
                         u1Priority <= PBB_MAX_PRIORITY; u1Priority++)
                    {
                        CliPrintf (CliHandle, "%d%-3s", u1Priority, "DE");
                        CliPrintf (CliHandle, "%-2d", u1Priority);

                    }
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "Priority    :\r\n");
                    CliPrintf (CliHandle,
                               "---------------------------------------------------------------\r\n");
                    u1PrintPortFlag = PBB_FALSE;
                }

                if (u1PrintSelRowFlag == PBB_TRUE)
                {
                    switch (i4NextPcpSelRow)
                    {
                        case PBB_8P0D_SEL_ROW:
                            CliPrintf (CliHandle, "8POD        : ");
                            break;
                        case PBB_7P1D_SEL_ROW:
                            CliPrintf (CliHandle, "7P1D        : ");
                            break;
                        case PBB_6P2D_SEL_ROW:
                            CliPrintf (CliHandle, "6P2D        : ");
                            break;
                        case PBB_5P3D_SEL_ROW:
                            CliPrintf (CliHandle, "5P3D        : ");
                            break;
                        default:
                            break;
                    }
                    u1PrintSelRowFlag = PBB_FALSE;
                }

                nmhGetFsPbbPcpEncodingPcpValue (i4NextIfIndex, i4NextPcpSelRow,
                                                i4NextPriority,
                                                i4NextDropEligible,
                                                &i4CurrPcpValue);

                i4PagingStatus = CliPrintf (CliHandle, "%-3d", i4CurrPcpValue);
                if (i4PagingStatus == CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if ((i4NextPriority == PBB_MAX_PRIORITY) &&
                    (i4NextDropEligible == PBB_VLAN_SNMP_FALSE))
                {
                    u1PrintSelRowFlag = PBB_TRUE;
                    CliPrintf (CliHandle, "\r\n");
                }
            }
        }

        i4CurrIfIndex = i4NextIfIndex;
        i4CurrPcpSelRow = i4NextPcpSelRow;
        i4CurrPriority = i4NextPriority;
        i4CurrDropEligible = i4NextDropEligible;

    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowPcpDecodingTable                           */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP Decoding table     */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbShowPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex,
                         UINT1 *pu1ContextName)
{

    INT4                i4CurrIfIndex = PBB_INIT_VAL;
    INT4                i4CurrPcpSelRow = PBB_INIT_VAL;
    INT4                i4CurrPcpValue = PBB_INIT_VAL;

    INT4                i4CurrPriority = PBB_INIT_VAL;
    INT4                i4CurrDropEligible = PBB_INIT_VAL;

    INT4                i4NextIfIndex = PBB_INIT_VAL;
    INT4                i4NextPcpSelRow = PBB_INIT_VAL;
    INT4                i4NextPcpValue = PBB_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;

    UINT1               u1PcpValue = PBB_INIT_VAL;
    UINT1               u1PrintSelRowFlag = PBB_TRUE;
    UINT1               u1PrintPortFlag = PBB_TRUE;
    UINT1               au1NameStr[PBB_MAX_PORT_NAME_LENGTH];
    UINT4               u4TempContextId = PBB_CLI_INVALID_CONTEXT;
    UINT4               u4CurrContextId = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    i4CurrIfIndex = i4IfIndex;
    i4CurrPcpSelRow = PBB_INIT_VAL;
    i4CurrPcpValue = PBB_INIT_VAL;

    if ((PbbCliGetContextForShowCmd
         (CliHandle, pu1ContextName, i4IfIndex,
          u4TempContextId, &u4CurrContextId, &u2LocalPortId) == PBB_FAILURE)
        && (pu1ContextName != NULL))
    {
        return CLI_FAILURE;

    }
    while (nmhGetNextIndexFsPbbPcpDecodingTable (i4CurrIfIndex, &i4NextIfIndex,
                                                 i4CurrPcpSelRow,
                                                 &i4NextPcpSelRow,
                                                 i4CurrPcpValue,
                                                 &i4NextPcpValue) ==
           SNMP_SUCCESS)
    {
        if (PbbVcmGetContextInfoFromIfIndex
            ((UINT4) i4NextIfIndex, &u4TempContextId,
             &u2LocalPortId) == PBB_SUCCESS)
        {
            if ((u4CurrContextId == PBB_INIT_VAL)
                || (u4TempContextId == u4CurrContextId))
            {
                if (i4IfIndex != 0)
                {
                    if (i4CurrIfIndex != i4NextIfIndex)
                    {
                        break;
                    }
                }
                if (i4CurrIfIndex != i4NextIfIndex)
                {
                    u1PrintPortFlag = PBB_TRUE;
                }

                if (u1PrintPortFlag == PBB_TRUE)
                {
                    if (CfaCliGetIfName ((UINT4) i4NextIfIndex,
                                         (INT1 *) au1NameStr) == CLI_FAILURE)
                    {
                        CliPrintf (CliHandle, "%% Invalid Port %d\r\n",
                                   (UINT4) i4NextIfIndex);

                        return CLI_FAILURE;
                    }
                    CliPrintf (CliHandle, "\n\nPort %s    \n", au1NameStr);
                    /*Printing the PCP Values */
                    CliPrintf (CliHandle, "PCP   : ");
                    for (u1PcpValue = PBB_INIT_VAL;
                         u1PcpValue <= PBB_MAX_PRIORITY; u1PcpValue++)
                    {
                        CliPrintf (CliHandle, "%-5d", u1PcpValue);
                    }
                    CliPrintf (CliHandle,
                               "\r\n----------------------------------------------\r\n");

                    u1PrintPortFlag = PBB_FALSE;
                }

                if (u1PrintSelRowFlag == PBB_TRUE)
                {
                    switch (i4NextPcpSelRow)
                    {
                        case PBB_8P0D_SEL_ROW:
                            CliPrintf (CliHandle, "8POD  : ");
                            break;
                        case PBB_7P1D_SEL_ROW:
                            CliPrintf (CliHandle, "7P1D  : ");
                            break;
                        case PBB_6P2D_SEL_ROW:
                            CliPrintf (CliHandle, "6P2D  : ");
                            break;
                        case PBB_5P3D_SEL_ROW:
                            CliPrintf (CliHandle, "5P3D  : ");
                            break;
                        default:
                            break;
                    }
                    u1PrintSelRowFlag = PBB_FALSE;
                }

                nmhGetFsPbbPcpDecodingPriority (i4NextIfIndex, i4NextPcpSelRow,
                                                i4NextPcpValue,
                                                &i4CurrPriority);

                nmhGetFsPbbPcpDecodingDropEligible (i4NextIfIndex,
                                                    i4NextPcpSelRow,
                                                    i4NextPcpValue,
                                                    &i4CurrDropEligible);

                if (i4CurrDropEligible == PBB_TRUE)
                {
                    i4PagingStatus =
                        CliPrintf (CliHandle, "%d%-4s", i4CurrPriority, "DE");
                }
                else
                {
                    i4PagingStatus =
                        CliPrintf (CliHandle, "%-5d", i4CurrPriority);
                }

                if (i4PagingStatus == CLI_FAILURE)
                {
                    return CLI_FAILURE;
                }
                if (i4NextPcpValue == PBB_MAX_PRIORITY)
                {
                    u1PrintSelRowFlag = PBB_TRUE;
                    CliPrintf (CliHandle, "\r\n");
                }
            }
        }
        i4CurrIfIndex = i4NextIfIndex;
        i4CurrPcpSelRow = i4NextPcpSelRow;
        i4CurrPcpValue = i4NextPcpValue;

    }

    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowPortConfig                                 */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP Decoding table     */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbShowPortConfig (tCliHandle CliHandle, INT4 i4IfIndex, UINT1 *pu1ContextName)
{

    INT4                i4CurrIfIndex = PBB_INIT_VAL;
    INT4                i4NextIfIndex = PBB_INIT_VAL;

    INT4                i4UseDei = PBB_INIT_VAL;
    INT4                i4PcpSelectionRow = PBB_INIT_VAL;
    INT4                i4ReqDropEncoding = PBB_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;

    UINT1               u1Flag = PBB_TRUE;
    UINT1               u1PrintPortFlag = PBB_TRUE;
    UINT1               au1NameStr[PBB_MAX_PORT_NAME_LENGTH];
    INT1                i1retVal = PBB_INIT_VAL;
    UINT4               u4TempContextId = PBB_CLI_INVALID_CONTEXT;
    UINT4               u4CurrContextId = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    if ((pu1ContextName != NULL) &&
        (PbbCliGetContextForShowCmd
         (CliHandle, pu1ContextName, i4IfIndex,
          u4TempContextId, &u4CurrContextId, &u2LocalPortId) == PBB_FAILURE))
    {
        return CLI_FAILURE;

    }
    if (i4IfIndex != PBB_INIT_VAL)
    {
        i4CurrIfIndex = i4IfIndex;
        u1Flag = PBB_FALSE;

    }
    else
    {
        i1retVal = nmhGetFirstIndexFsPbbPortTable (&i4CurrIfIndex);
        if (i1retVal == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, " Pbb Port config information \r\n");
        }
    }
    do
    {
        if (PbbVcmGetContextInfoFromIfIndex
            ((UINT4) i4CurrIfIndex, &u4TempContextId,
             &u2LocalPortId) == PBB_SUCCESS)
        {
            if ((u4CurrContextId == PBB_INIT_VAL)
                || (u4TempContextId == u4CurrContextId))
            {

                if (i4CurrIfIndex != i4NextIfIndex)
                {
                    u1PrintPortFlag = PBB_TRUE;
                }

                if (PbbIsPortTypePip (i4CurrIfIndex) == TRUE)
                {

                    if (u1PrintPortFlag == PBB_TRUE)
                    {
                        if (CfaCliGetIfName ((UINT4) i4CurrIfIndex,
                                             (INT1 *) au1NameStr) ==
                            CLI_FAILURE)
                        {
                            CliPrintf (CliHandle, "%% Invalid Port %d\r\n",
                                       (UINT4) i4CurrIfIndex);

                            return CLI_FAILURE;
                        }
                        CliPrintf (CliHandle, "\n\nPort %s    \n", au1NameStr);
                    }

                    i4PagingStatus = CliPrintf (CliHandle,
                                                " Port Type                           : Provider Instance Port\n");
                    if (i4PagingStatus == CLI_FAILURE)
                    {
                        return CLI_FAILURE;
                    }
                    if (nmhGetFsPbbPortReqDropEncoding
                        (i4CurrIfIndex, &i4ReqDropEncoding) != SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                    CliPrintf (CliHandle,
                               " Require Drop Encoding               :");
                    if (i4ReqDropEncoding == PBB_TRUE)
                    {
                        CliPrintf (CliHandle, " True\r\n");
                    }
                    else
                    {
                        CliPrintf (CliHandle, " False \r\n");
                    }

                    if (nmhGetFsPbbPortUseDei (i4CurrIfIndex, &i4UseDei) !=
                        SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                    CliPrintf (CliHandle,
                               " Use_Dei                             :");
                    if (i4UseDei == PBB_VLAN_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, " True\r\n");
                    }
                    else if (i4UseDei == PBB_VLAN_SNMP_FALSE)
                    {
                        CliPrintf (CliHandle, " False\r\n");
                    }
                    /*Printing the PCP Values */
                    if (nmhGetFsPbbPortPcpSelectionRow
                        (i4CurrIfIndex, &i4PcpSelectionRow) != SNMP_SUCCESS)
                    {
                        return CLI_FAILURE;
                    }
                    CliPrintf (CliHandle,
                               " PCP Selection Row                   : ");
                    switch (i4PcpSelectionRow)
                    {
                        case PBB_8P0D_SEL_ROW:
                            CliPrintf (CliHandle, "8P0D \r\n");
                            break;
                        case PBB_7P1D_SEL_ROW:
                            CliPrintf (CliHandle, "7P1D \r\n");
                            break;
                        case PBB_6P2D_SEL_ROW:
                            CliPrintf (CliHandle, "6P2D \r\n");
                            break;
                        case PBB_5P3D_SEL_ROW:
                            CliPrintf (CliHandle, "5P3D \r\n");
                            break;
                        default:
                            CliPrintf (CliHandle, " \r\n");
                            break;
                    }
                }
            }
        }
        i4NextIfIndex = i4CurrIfIndex;
        if (u1Flag == PBB_FALSE)
        {
            break;

        }
    }
    while (nmhGetNextIndexFsPbbPortTable (i4NextIfIndex, &i4CurrIfIndex) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbGlobalShowRunningConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  PBB's ISS Level       */
/*                        Configuration e.g. OUI Configuration               */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PbbGlobalShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE LocalClientOui;
    tSNMP_OCTET_STRING_TYPE pbbInstanceName;
    UINT4               u4NextInstanceNum = PBB_INIT_VAL;
    UINT4               u4CurrInstanceNum = PBB_INIT_VAL;
    INT4                i4InstanceRowStatus = PBB_INVALID_ROWSTATUS;
    INT4                i4RetVal = PBB_INIT_VAL;
    INT4                i4RetValFsPbbShutdownStatus = PBB_INIT_VAL;
    UINT1               au1Oui[PBB_OUI_LENGTH];
    UINT1               au1OuiStrVal[PBB_CLI_OUI_STR_LEN];
    UINT1               au1OuiStrDef[PBB_CLI_OUI_STR_LEN];
    UINT1               au1InstanceName[PBB_INSTANCE_ALIAS_LEN];
    UINT1               au1String[PBB_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1OuiDef[PBB_OUI_LENGTH];
    tMacAddr            pbbDefaultInstanceMacAddr;
    tMacAddr            pbbInstanceMacAddr;
    UINT1               au1DefNameVal[PBB_INSTANCE_ALIAS_LEN] =
        PBB_DEFAULT_INSTANCE_NAME;
    PBB_SET_DEFAULT_GLB_OUI (au1OuiDef);

    CliRegisterLock (CliHandle, PbbLock, PbbUnLock);
    PBB_LOCK ();
    PBB_MEMSET (au1Oui, PBB_INIT_VAL, PBB_OUI_LENGTH);
    PBB_MEMSET (au1OuiStrDef, PBB_INIT_VAL, PBB_OUI_LENGTH);
    PBB_MEMSET (&LocalClientOui, PBB_INIT_VAL,
                sizeof (tSNMP_OCTET_STRING_TYPE));
    PBB_MEMSET (pbbInstanceMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (pbbDefaultInstanceMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    LocalClientOui.i4_Length = PBB_OUI_LENGTH;
    LocalClientOui.pu1_OctetList = au1Oui;

    /*Get the status & display if not equal to Initial value */
    nmhGetFsPbbShutdownStatus (&i4RetValFsPbbShutdownStatus);
    if (i4RetValFsPbbShutdownStatus == PBB_SNMP_FALSE)
    {
        CliPrintf (CliHandle, "no shutdown provider-backbone-bridge\r\n");
        /* Get OUI Value & Display if other than Zero */
        nmhGetFsPbbGlbOUI (&LocalClientOui);
        PBB_MEMSET (au1OuiStrVal, 0, PBB_CLI_OUI_STR_LEN);
        CliOctetToStr (LocalClientOui.pu1_OctetList, LocalClientOui.i4_Length,
                       au1OuiStrVal, PBB_CLI_OUI_STR_LEN);
        CliOctetToStr (au1OuiDef, PBB_OUI_LENGTH, au1OuiStrDef,
                       PBB_CLI_OUI_STR_LEN);
        if (PBB_MEMCMP
            (&au1OuiStrVal, &au1OuiStrDef, PBB_CLI_OUI_STR_LEN) != PBB_INIT_VAL)

        {
            CliPrintf (CliHandle, "set provider-backbone-bridge oui %s\r\n",
                       au1OuiStrVal);
        }
        /*Backbone Instances */
        pbbInstanceName.i4_Length = PBB_INSTANCE_ALIAS_LEN;
        pbbInstanceName.pu1_OctetList = au1InstanceName;
        i4RetVal =
            nmhGetFirstIndexFsPbbInstanceTable ((INT4 *) &u4NextInstanceNum);
        if (i4RetVal == SNMP_SUCCESS)
        {
            do
            {
                u4CurrInstanceNum = u4NextInstanceNum;
                nmhGetFsPbbInstanceRowStatus (u4CurrInstanceNum,
                                              &i4InstanceRowStatus);
                if (i4InstanceRowStatus == PBB_ACTIVE)
                {
                    PBB_MEMSET (au1InstanceName, 0, PBB_INSTANCE_ALIAS_LEN);
                    nmhGetFsPbbInstanceMacAddr (u4CurrInstanceNum,
                                                &pbbInstanceMacAddr);
                    nmhGetFsPbbInstanceName (u4CurrInstanceNum,
                                             &pbbInstanceName);
                    PbbGetDefaultInstanceMacAddress (u4CurrInstanceNum,
                                                     pbbDefaultInstanceMacAddr);
                    /*Check for MAC default values */
                    if (PBB_MEMCMP (pbbDefaultInstanceMacAddr,
                                    pbbInstanceMacAddr, MAC_ADDR_LEN)
                        != PBB_INIT_VAL)
                    {
                        PBB_MEMSET (au1String, 0, PBB_CLI_MAX_MAC_STRING_SIZE);
                        PrintMacAddress (pbbInstanceMacAddr, au1String);
                        CliPrintf (CliHandle,
                                   "backbone instance %s mac-address %s\r\n",
                                   pbbInstanceName.pu1_OctetList, au1String);
                    }
                    else if (((u4CurrInstanceNum == PBB_DEF_INSTANCE_ID)
                              && (PBB_MEMCMP (pbbInstanceName.pu1_OctetList,
                                              au1DefNameVal,
                                              pbbInstanceName.i4_Length)
                                  != PBB_INIT_VAL)) ||
                             (u4CurrInstanceNum != PBB_DEF_INSTANCE_ID))
                    {
                        CliPrintf (CliHandle, "backbone instance %s\r\n",
                                   pbbInstanceName.pu1_OctetList);
                    }
                }
            }
            while (nmhGetNextIndexFsPbbInstanceTable ((INT4) u4CurrInstanceNum,
                                                      (INT4 *)
                                                      &u4NextInstanceNum) !=
                   SNMP_FAILURE);
        }
    }
    PBB_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbShowRunningConfig                               */
/*                                                                           */
/*     DESCRIPTION      : This function  displays the  PBB Configuration     */
/*                                                                           */
/*     INPUT            : tCliHandle-Handle to the Cli Context               */
/*                        u4ContextId - Current Context Id                   */
/*                        u4Module - Module Id                               */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
PbbShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Module)
{
    UINT4               u4SysMode = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INIT_VAL;
    UINT1               au1ContextName[PBB_SWITCH_ALIAS_LEN];

    PbbL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode);
    if (!(u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE ||
          u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE))
    {
        return CLI_SUCCESS;
    }
    CliRegisterLock (CliHandle, PbbLock, PbbUnLock);
    PBB_LOCK ();
    u4SysMode = PbbVcmGetSystemModeExt (PBB_PROTOCOL_ID);
    if (u4SysMode == VCM_MI_MODE)
    {
        PBB_MEMSET (au1ContextName, PBB_INIT_VAL, PBB_SWITCH_ALIAS_LEN);
        PbbVcmGetAliasName (u4ContextId, au1ContextName);
        CliPrintf (CliHandle, "switch  %s \r\n", au1ContextName);
        if (PbbShowRunningSwitchConfig (CliHandle, u4ContextId) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "! \r\n");
            if (u4Module == ISS_PBB_SHOW_RUNNING_CONFIG)
            {
                PbbShowRunningConfigInterface (CliHandle, u4ContextId);
            }
        }
        else
        {
            CliPrintf (CliHandle, "! \r\n");
        }
    }
    PBB_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbShowRunningSwitchConfig                         */
/*                                                                           */
/*     DESCRIPTION      : This function displays scalar objects and service  */
/*                        instance for PBB show running configuration.       */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PbbShowRunningSwitchConfig (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE pbbInstanceName;
    UINT4               u4InstanceNum = PBB_INIT_VAL;
    INT4                i4RetVal = PBB_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;
    UINT1               au1NameVal[PBB_INSTANCE_ALIAS_LEN];
    UINT1               au1DefNameVal[PBB_INSTANCE_ALIAS_LEN] = "default";

    /* Display PBB Instance Map  */
    CLI_MEMSET (au1NameVal, PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN);
    CLI_MEMSET (&pbbInstanceName, PBB_INIT_VAL,
                sizeof (tSNMP_OCTET_STRING_TYPE));
    pbbInstanceName.i4_Length = PBB_INSTANCE_ALIAS_LEN;
    pbbInstanceName.pu1_OctetList = au1NameVal;

    i4RetVal = nmhGetFsPbbContextToInstanceId (u4ContextId,
                                               (INT4 *) &u4InstanceNum);
    i4RetVal = nmhGetFsPbbInstanceName (u4InstanceNum, &pbbInstanceName);

    if (PBB_MEMCMP (pbbInstanceName.pu1_OctetList, au1DefNameVal,
                    pbbInstanceName.i4_Length) != PBB_INIT_VAL)
    {
        CliPrintf (CliHandle, "map backbone instance %s\r\n", pbbInstanceName);
    }
    /*Service Instance */
    i4PagingStatus = PbbShowRunningServiceInst (CliHandle, u4ContextId);
    UNUSED_PARAM (i4RetVal);
    return i4PagingStatus;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbShowRunningServiceInst                          */
/*                                                                           */
/*     DESCRIPTION      : This function displays Service Instance config     */
/*                        for PBB show running configuration.                */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        u4ContextId - Context Identifier                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PbbShowRunningServiceInst (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4FirstContextId = PBB_INIT_VAL;
    UINT4               u4FirstPortId = PBB_INIT_VAL;
    UINT4               u4CurrContextId = PBB_INIT_VAL;
    UINT4               u4CurrPortId = PBB_INIT_VAL;
    UINT4               u4NextPortId = PBB_INIT_VAL;
    UINT4               u4NextContextId = PBB_INIT_VAL;
    UINT4               u4FirstIsid = PBB_INIT_VAL;
    UINT4               u4NextIsid = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    INT4                i4RetVal = CLI_SUCCESS;

    u4CurrContextId = u4ContextId;
    u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;
    if (u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        while (nmhGetNextIndexFsPbbVipTable
               ((INT4) u4CurrContextId, (INT4 *) &u4NextContextId,
                (INT4) u4CurrPortId, (INT4 *) &u4NextPortId) == SNMP_SUCCESS)
        {
            if (u4NextContextId == u4ContextId)
            {
                nmhGetFsPbbVipISid (u4NextContextId, u4NextPortId, &u4Isid);
                if (u4Isid != 1)
                {
                    if (PbbShowRunningIsidInContext (CliHandle, u4Isid,
                                                     u4ContextId, u4NextPortId)
                        == CLI_FAILURE)
                    {
                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                }
                u4CurrContextId = u4NextContextId;
                u4CurrPortId = u4NextPortId;
            }
            else
            {
                break;
            }
        }
    }
    else if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
    {
        if (nmhGetFirstIndexFsPbbCBPServiceMappingTable
            ((INT4 *) &u4NextContextId, (INT4 *) &u4NextPortId,
             &u4NextIsid) == SNMP_SUCCESS)
        {
            while (u4NextContextId != u4ContextId)
            {
                u4FirstContextId = u4NextContextId;
                u4FirstPortId = u4NextPortId;
                u4FirstIsid = u4NextIsid;
                if (nmhGetNextIndexFsPbbCBPServiceMappingTable
                    ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                     (INT4) u4FirstPortId, (INT4 *) &u4NextPortId,
                     u4FirstIsid, &u4NextIsid) == SNMP_FAILURE)
                {
                    return CLI_SUCCESS;
                }
            }
            u4FirstIsid = 0;
            while (u4NextContextId == u4ContextId)
            {
                if (u4FirstIsid != u4NextIsid)
                {
                    if (PbbShowRunningIsidInContext (CliHandle,
                                                     u4NextIsid,
                                                     u4NextContextId, 0) ==
                        CLI_FAILURE)
                    {
                        i4RetVal = CLI_FAILURE;
                        break;
                    }
                }
                u4FirstContextId = u4NextContextId;
                u4FirstPortId = u4NextPortId;
                u4FirstIsid = u4NextIsid;
                if (nmhGetNextIndexFsPbbCBPServiceMappingTable
                    ((INT4) u4FirstContextId, (INT4 *) &u4NextContextId,
                     (INT4) u4FirstPortId, (INT4 *) &u4NextPortId,
                     u4FirstIsid, &u4NextIsid) == SNMP_FAILURE)
                {
                    return CLI_SUCCESS;
                }
            }
        }
    }
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbShowRunningIsidInContext                        */
/*                                                                           */
/*     DESCRIPTION      : This fnction displayes all info related to the isid*/
/*                                                                           */
/*     INPUT            : u4Isid -       Isid                                */
/*                      : u4ContextId  - Context ID                          */
/*                      : u4vip  - Context ID                                */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
PbbShowRunningIsidInContext (tCliHandle CliHandle, UINT4 u4Isid,
                             UINT4 u4ContextId, UINT4 u4VipIfIndex)
{
    tSNMP_OCTET_STRING_TYPE LocalClientOui;
    tSNMP_OCTET_STRING_TYPE SnmpPorts;
    tSNMP_OCTET_STRING_TYPE SnmpCbpPorts;
    tSNMP_OCTET_STRING_TYPE SnmpIngressPorts;
    tSNMP_OCTET_STRING_TYPE SnmpEgressPorts;
    tSNMP_OCTET_STRING_TYPE ServiceType;
    tPortList          *pPortListCbp = NULL;
    tPortList          *pPortListIngress = NULL;
    tPortList          *pPortListEgress = NULL;
    UINT4               u4PipPort = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    UINT4               u4LocalIsid = PBB_INIT_VAL;
    UINT4               u4Bvid = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    INT4                i4PagingStatus = CLI_SUCCESS;
    INT4                i4IsidStaticRowStatus = PBB_INIT_VAL;
    INT4                i4RetValCBPRowStatus = PBB_INIT_VAL;
    INT4                i4RowStatus = PBB_INVALID_ROWSTATUS;
    INT4                i4RetStatus = PBB_INIT_VAL;
    UINT1               u1PortEgressFlag = PBB_INIT_VAL;
    UINT1               u1PortIngressFlag = PBB_INIT_VAL;
    UINT1               au1NameStr[PBB_MAX_PORT_NAME_LENGTH];
    UINT1               au1String[PBB_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1OuiVal[PBB_OUI_LENGTH];
    UINT1               au1OuiStrVal[PBB_CLI_OUI_STR_LEN];
    UINT1               u1ServiceType = PBB_INIT_VAL;
    BOOL1               bResult = OSIX_FALSE;
    tMacAddr            au1ZeroMacAddr[MAC_ADDR_LEN];
    tMacAddr            NextMacAddr;
    UINT1              *pLocalPortList1 = NULL;

    CLI_MEMSET (au1OuiVal, PBB_INIT_VAL, PBB_OUI_LENGTH);
    CLI_MEMSET (&LocalClientOui, PBB_INIT_VAL,
                sizeof (tSNMP_OCTET_STRING_TYPE));

    LocalClientOui.i4_Length = PBB_OUI_LENGTH;
    LocalClientOui.pu1_OctetList = au1OuiVal;

    ServiceType.i4_Length = 1;
    ServiceType.pu1_OctetList = &u1ServiceType;

    PBB_MEMSET (au1String, PBB_INIT_VAL, PBB_CLI_MAX_MAC_STRING_SIZE);
    PBB_MEMSET (NextMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (au1ZeroMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    /*Service Instance */
    CliPrintf (CliHandle, "service instance %d", u4Isid);
    u4BridgeMode = PBB_CONTEXT_PTR (u4ContextId)->u4BridgeMode;
    if (u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        /*Member Ports PIP */
        i4RetStatus = PbbGetVIPValue (u4ContextId, u4Isid, &u2LocalPortId);
        if (i4RetStatus == PBB_SUCCESS)
        {
            /*Provider Instance Port */
            i4RetStatus = nmhGetFsPbbVipToPipMappingPipIfIndex
                (u4ContextId, (INT4) u4VipIfIndex, (INT4 *) &u4PipPort);
            if (i4RetStatus == SNMP_SUCCESS)
            {
                CLI_MEMSET (au1NameStr, PBB_INIT_VAL, sizeof (au1NameStr));
                if (CfaCliConfGetIfName (u4PipPort,
                                         (INT1 *) au1NameStr) == CLI_SUCCESS)
                {
                    CliPrintf (CliHandle, "\r\n ports icomp %s", au1NameStr);
                }
            }

            i4RetStatus = nmhGetFsPbbVipType (u4ContextId, u4VipIfIndex,
                                              &ServiceType);
            if (ServiceType.pu1_OctetList[0] == PBB_VIP_TYPE_INGRESS)
            {
                CliPrintf (CliHandle, "\r\n set service-type icomp ingress");
            }
            else if (ServiceType.pu1_OctetList[0] == PBB_VIP_TYPE_EGRESS)
            {
                CliPrintf (CliHandle, "\r\n set service-type icomp egress");
            }

            /*Destination MAC Address */
            i4RetStatus = nmhGetFsPbbVipDefaultDstBMAC (u4ContextId,
                                                        u4VipIfIndex,
                                                        &NextMacAddr);
            if (i4RetStatus == SNMP_SUCCESS)
            {
                if ((PBB_MEMCMP (au1ZeroMacAddr, NextMacAddr,
                                 PBB_MAC_ADDR_LEN)) != PBB_INIT_VAL)
                {

                    PrintMacAddress (NextMacAddr, au1String);
                    CliPrintf (CliHandle, "\r\n set destination-mac-address %s",
                               au1String);
                }
            }
            /*Service Instance status */
            nmhGetFsPbbVipRowStatus (u4ContextId, u4VipIfIndex,
                                     &i4IsidStaticRowStatus);
            if (i4IsidStaticRowStatus == PBB_ACTIVE)
            {
                CliPrintf (CliHandle,
                           "\r\n set service-instance status enable");
            }
        }
    }                            /*end of I component */

    /*B component */
    else if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
    {
        pLocalPortList1 = UtilPlstAllocLocalPortList (sizeof (tLocalPortList));

        if (pLocalPortList1 == NULL)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "PbbShowRunningIsidInContext : Error in allocating memory "
                     "for pLocalPortList1\r\n");
            return PBB_FAILURE;
        }

        CLI_MEMSET (pLocalPortList1, PBB_INIT_VAL, sizeof (tLocalPortList));
        SnmpPorts.pu1_OctetList = pLocalPortList1;
        SnmpPorts.i4_Length = sizeof (tLocalPortList);

        pPortListCbp = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
        if (pPortListCbp == NULL)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist \n");
            UtilPlstReleaseLocalPortList (pLocalPortList1);
            return PBB_FAILURE;
        }
        pPortListIngress =
            (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
        if (pPortListIngress == NULL)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist \n");
            FsUtilReleaseBitList ((UINT1 *) pPortListCbp);
            UtilPlstReleaseLocalPortList (pLocalPortList1);
            return PBB_FAILURE;
        }
        pPortListEgress = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));
        if (pPortListEgress == NULL)
        {
            PBB_TRC (ALL_FAILURE_TRC,
                     "Error in Allocating memory for bitlist \n");
            FsUtilReleaseBitList ((UINT1 *) pPortListCbp);
            FsUtilReleaseBitList ((UINT1 *) pPortListIngress);
            UtilPlstReleaseLocalPortList (pLocalPortList1);
            return PBB_FAILURE;
        }
        CLI_MEMSET ((*pPortListCbp), PBB_INIT_VAL, sizeof (tPortList));
        CLI_MEMSET ((*pPortListIngress), PBB_INIT_VAL, sizeof (tPortList));
        CLI_MEMSET ((*pPortListEgress), PBB_INIT_VAL, sizeof (tPortList));
        SnmpCbpPorts.pu1_OctetList = (UINT1 *) pPortListCbp;
        SnmpCbpPorts.i4_Length = sizeof (tPortList);
        SnmpIngressPorts.pu1_OctetList = (UINT1 *) pPortListIngress;
        SnmpIngressPorts.i4_Length = sizeof (tPortList);
        SnmpEgressPorts.pu1_OctetList = (UINT1 *) pPortListEgress;
        SnmpEgressPorts.i4_Length = sizeof (tPortList);

        if (PbbGetCbpForIsid (u4ContextId, u4Isid, &SnmpPorts) == PBB_SUCCESS)
        {
            for (u2LocalPortId = 1; u2LocalPortId <= PBB_MAX_PORTS;
                 u2LocalPortId++)
            {
                OSIX_BITLIST_IS_BIT_SET (pLocalPortList1, u2LocalPortId, sizeof
                                         (tLocalPortList), bResult);
                if (bResult == OSIX_TRUE)
                {

                    PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                                   u2LocalPortId,
                                                   (INT4 *) &u4Port);
                    if (u4Port > 0)
                    {
                        PBB_SET_MEMBER_PORT ((*pPortListCbp), (UINT2) u4Port);
                        i4RetStatus = nmhGetFsPbbCBPServiceMappingType
                            (u4ContextId, (INT4) u4Port, u4Isid, &ServiceType);
                        if (ServiceType.pu1_OctetList[0] == PBB_INGRESS)
                        {
                            PBB_SET_MEMBER_PORT ((*pPortListIngress),
                                                 (UINT2) u4Port);
                            u1PortIngressFlag = 1;
                        }
                        else if (ServiceType.pu1_OctetList[0] == PBB_EGRESS)
                        {
                            PBB_SET_MEMBER_PORT ((*pPortListEgress),
                                                 (UINT2) u4Port);
                            u1PortEgressFlag = 1;
                        }
                    }
                }
            }
            CliConfOctetToIfName (CliHandle, "\r\n ports bcomp ", NULL,
                                  &SnmpCbpPorts, (UINT4 *) (&i4PagingStatus));
            if (u1PortIngressFlag != PBB_INIT_VAL)
            {
                CliConfOctetToIfName (CliHandle, "\r\n set service-type bcomp ",
                                      NULL, &SnmpIngressPorts,
                                      (UINT4 *) (&i4PagingStatus));

                CliPrintf (CliHandle, " ingress");
            }
            if (u1PortEgressFlag != PBB_INIT_VAL)
            {
                CliConfOctetToIfName (CliHandle, "\r\n set service-type bcomp ",
                                      NULL, &SnmpEgressPorts,
                                      (UINT4 *) (&i4PagingStatus));

                CliPrintf (CliHandle, " egress");
            }
        }
        FsUtilReleaseBitList ((UINT1 *) pPortListCbp);
        FsUtilReleaseBitList ((UINT1 *) pPortListIngress);
        FsUtilReleaseBitList ((UINT1 *) pPortListEgress);

        for (u2LocalPortId = 1; u2LocalPortId <= PBB_MAX_PORTS; u2LocalPortId++)
        {
            OSIX_BITLIST_IS_BIT_SET (pLocalPortList1, u2LocalPortId, sizeof
                                     (tLocalPortList), bResult);
            if (bResult != OSIX_TRUE)
            {
                continue;
            }

            PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2LocalPortId,
                                           (INT4 *) &u4Port);

            CLI_MEMSET (au1NameStr, 0, sizeof (au1NameStr));
            CfaCliConfGetIfName (u4Port, (INT1 *) au1NameStr);

            i4RetStatus = nmhGetFsPbbCBPServiceMappingDefaultBackboneDest
                (u4ContextId, u4Port, u4Isid, &NextMacAddr);

            if (i4RetStatus == SNMP_SUCCESS)
            {
                if ((PBB_MEMCMP (au1ZeroMacAddr, NextMacAddr,
                                 PBB_MAC_ADDR_LEN)) != PBB_INIT_VAL)
                {
                    PrintMacAddress (NextMacAddr, au1String);
                    CliPrintf (CliHandle, "\r\n set destination-mac-address "
                               "%.17s %s", au1String, au1NameStr);
                }
            }

            if (nmhGetFsPbbCBPServiceMappingRowStatus (u4ContextId, u4Port,
                                                       u4Isid,
                                                       &i4RetValCBPRowStatus)
                != SNMP_FAILURE)
            {
                if (i4RetValCBPRowStatus == PBB_ACTIVE)
                {
                    CliPrintf (CliHandle, "\r\n set service-instance "
                               "status enable %s", au1NameStr);
                }
            }

            nmhGetFsPbbCBPServiceMappingBVid (u4ContextId, (INT4) u4Port,
                                              u4Isid, (INT4 *) &u4Bvid);
            if (u4Bvid != 0)
            {
                CliPrintf (CliHandle, "\r\n member-ports vlan %d %s",
                           u4Bvid, au1NameStr);
            }

            nmhGetFsPbbCBPServiceMappingLocalSid (u4ContextId, u4Port, u4Isid,
                                                  &u4LocalIsid);

            if ((u4LocalIsid != 1) && (u4LocalIsid != u4Isid))
            {
                CliPrintf (CliHandle, "\r\n translate-isid %d %s",
                           u4LocalIsid, au1NameStr);
            }

            if (nmhGetFsPbbOUIRowStatus (u4ContextId, u4Isid, u4Port,
                                         &i4RowStatus) == SNMP_SUCCESS)
            {
                if (i4RowStatus == PBB_ACTIVE)
                {
                    CLI_MEMSET (au1OuiStrVal, 0, PBB_CLI_OUI_STR_LEN);
                    CLI_MEMSET (LocalClientOui.pu1_OctetList, 0,
                                PBB_OUI_LENGTH);
                    nmhGetFsPbbOUI (u4ContextId, u4Isid, u4Port,
                                    &LocalClientOui);
                    CliOctetToStr (LocalClientOui.pu1_OctetList,
                                   LocalClientOui.i4_Length,
                                   au1OuiStrVal, PBB_CLI_OUI_STR_LEN);
                    CliPrintf (CliHandle, "\r\n set service-instance"
                               " oui %s %s", au1OuiStrVal, au1NameStr);

                }
            }
        }
        UtilPlstReleaseLocalPortList (pLocalPortList1);
    }                            /*end of B component */

    i4PagingStatus = CliPrintf (CliHandle, "\r\n!\r\n");
    if (i4PagingStatus == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbShowRunningConfigInterfaceDetails               */
/*                                                                           */
/*     DESCRIPTION      : This function displays interface objects in PBB    */
/*                        for show running configuration.                    */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                        i4IfIndex - Interface value                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
VOID
PbbShowRunningConfigInterfaceDetails (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPortId = PBB_INIT_VAL;

    /* Needed this check when called from Cfa */
    if (PBB_IS_MODULE_INITIALISED () != PBB_TRUE)
    {
        return;
    }
    CliRegisterLock (CliHandle, PbbLock, PbbUnLock);
    PBB_LOCK ();
    if (PbbVcmGetContextInfoFromIfIndex
        (i4IfIndex, &u4ContextId, &u2LocalPortId) == PBB_FAILURE)
    {
        PBB_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return;
    }
    PbbShowRunningIfConfig (CliHandle, u4ContextId, i4IfIndex);
    PBB_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbShowRunningConfigInterface                      */
/*                                                                           */
/*     DESCRIPTION      : This function scans the interface table for  PBB   */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : u4ContextId - Current context Id                   */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
PRIVATE VOID
PbbShowRunningConfigInterface (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT4               u4IfIndex = PBB_INIT_VAL;
    UINT2               u2CurrLocalPortId = PBB_INIT_VAL;
    UINT2               u2NextPortId = PBB_INIT_VAL;
    UINT1               au1NameStr[PBB_MAX_PORT_NAME_LENGTH];
    while (PbbL2IwfGetNextValidPortForContext (u4ContextId,
                                               u2CurrLocalPortId, &u2NextPortId,
                                               &u4IfIndex) == L2IWF_SUCCESS)
    {
        CLI_MEMSET (au1NameStr, PBB_INIT_VAL, sizeof (au1NameStr));
        if (CfaCliConfGetIfName (u4IfIndex, (INT1 *) au1NameStr) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
            PbbShowRunningIfConfig (CliHandle, u4ContextId, u4IfIndex);
            CliPrintf (CliHandle, "!\r\n");
        }
        u2CurrLocalPortId = u2NextPortId;
    }
}

/*****************************************************************************/
/*     FUNCTION NAME    : PbbShowRunningIfConfig                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays the PBB related config for  */
/*                        particular interface.                              */
/*                                                                           */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*                      : u4ContextId - Current context Id                   */
/*                      : i4IfInedx   - Interface Index                      */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : VOID                                               */
/*****************************************************************************/
PRIVATE VOID
PbbShowRunningIfConfig (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4IfIndex)
{
    INT4                i4RetValFsPbbPortPisid = PBB_INIT_VAL;
    if (nmhGetFsPbbPortPisid (u4ContextId, i4IfIndex,
                              &i4RetValFsPbbPortPisid) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, " switchport pisid %d \r\n",
                   i4RetValFsPbbPortPisid);
    }
    if (PbbIsPortTypePip (i4IfIndex) == TRUE)
    {
        PbbShowRunningPortConfig (CliHandle, i4IfIndex);
        if (PbbShowRunningPcpDecodingTable (CliHandle, i4IfIndex) ==
            CLI_SUCCESS)
        {
            PbbShowRunningPcpEncodingTable (CliHandle, i4IfIndex);
        }
    }
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowRunningPortConfig                          */
/*                                                                          */
/*     DESCRIPTION      : This function will display port configuration.    */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : VOID                                              */
/*                                                                          */
/****************************************************************************/
PRIVATE VOID
PbbShowRunningPortConfig (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4UseDei = PBB_INIT_VAL;
    INT4                i4PcpSelectionRow = PBB_INIT_VAL;
    INT4                i4ReqDropEncoding = PBB_INIT_VAL;

    /*Printing the PCP Values */
    if (nmhGetFsPbbPortPcpSelectionRow
        (i4IfIndex, &i4PcpSelectionRow) == SNMP_SUCCESS)
    {
        switch (i4PcpSelectionRow)
        {
            case PBB_8P0D_SEL_ROW:
                break;
            case PBB_7P1D_SEL_ROW:
                CliPrintf (CliHandle,
                           "switchport provider-bridge pcp-selection-row 7P1D\r\n");
                break;
            case PBB_6P2D_SEL_ROW:
                CliPrintf (CliHandle,
                           "switchport provider-bridge pcp-selection-row 6P2D\r\n");
                break;
            case PBB_5P3D_SEL_ROW:
                CliPrintf (CliHandle,
                           "switchport provider-bridge pcp-selection-row 5P3D\r\n");
                break;
            default:
                CliPrintf (CliHandle, " \r\n");
                break;
        }
    }
    if (nmhGetFsPbbPortReqDropEncoding
        (i4IfIndex, &i4ReqDropEncoding) == SNMP_SUCCESS)
    {
        if (i4ReqDropEncoding == PBB_TRUE)
        {
            CliPrintf (CliHandle, "switchport provider-bridge"
                       " require-drop-encoding true\r\n");
        }
    }
    if (nmhGetFsPbbPortUseDei (i4IfIndex, &i4UseDei) == SNMP_SUCCESS)
    {
        if (i4UseDei == PBB_VLAN_SNMP_TRUE)
        {
            CliPrintf (CliHandle,
                       "switchport provider-bridge use-dei true\r\n");
        }
    }
    return;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowRunningPcpDecodingTable                    */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP decoding table     */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/

PRIVATE INT4
PbbShowRunningPcpDecodingTable (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4CurrIfIndex = PBB_INIT_VAL;
    INT4                i4CurrPcpSelRow = PBB_INIT_VAL;
    INT4                i4CurrPcpValue = PBB_INIT_VAL;

    INT4                i4CurrPriority = PBB_INIT_VAL;
    INT4                i4CurrDropEligible = PBB_INIT_VAL;
    INT4                i4DefaultPriority = PBB_INIT_VAL;
    INT4                i4DefaultDropEligible = PBB_INIT_VAL;

    INT4                i4NextIfIndex = PBB_INIT_VAL;
    INT4                i4NextPcpSelRow = PBB_INIT_VAL;
    INT4                i4NextPcpValue = PBB_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;

    i4CurrIfIndex = i4IfIndex;
    i4CurrPcpSelRow = PBB_INIT_VAL;
    i4CurrPcpValue = PBB_INIT_VAL;

    while (nmhGetNextIndexFsPbbPcpDecodingTable (i4CurrIfIndex,
                                                 &i4NextIfIndex,
                                                 i4CurrPcpSelRow,
                                                 &i4NextPcpSelRow,
                                                 i4CurrPcpValue,
                                                 &i4NextPcpValue) ==
           SNMP_SUCCESS)
    {
        if (i4CurrIfIndex != i4NextIfIndex)
        {
            break;
        }
        nmhGetFsPbbPcpDecodingPriority (i4NextIfIndex, i4NextPcpSelRow,
                                        i4NextPcpValue, &i4CurrPriority);
        nmhGetFsPbbPcpDecodingDropEligible (i4NextIfIndex,
                                            i4NextPcpSelRow,
                                            i4NextPcpValue,
                                            &i4CurrDropEligible);
        PbbGetDecodingPriority ((UINT1) i4NextPcpSelRow,
                                (UINT1) i4NextPcpValue, &i4DefaultPriority);

        PbbGetDecodingDropEligible ((UINT1) i4NextPcpSelRow,
                                    (UINT1) i4NextPcpValue,
                                    &i4DefaultDropEligible);
        if (i4DefaultDropEligible == PBB_DE_FALSE)
        {
            i4DefaultDropEligible = PBB_VLAN_SNMP_FALSE;
        }
        if ((i4CurrPriority != i4DefaultPriority) || (i4DefaultDropEligible !=
                                                      i4CurrDropEligible))
        {
            CliPrintf (CliHandle, "pcp-decoding ");
            switch (i4NextPcpSelRow)
            {
                case PBB_8P0D_SEL_ROW:
                    CliPrintf (CliHandle, "8P0D ");
                    break;
                case PBB_7P1D_SEL_ROW:
                    CliPrintf (CliHandle, "7P1D ");
                    break;
                case PBB_6P2D_SEL_ROW:
                    CliPrintf (CliHandle, "6P2D ");
                    break;
                case PBB_5P3D_SEL_ROW:
                    CliPrintf (CliHandle, "5P3D ");
                    break;
                default:
                    break;
            }
            CliPrintf (CliHandle, "pcp %d ", i4NextPcpValue);
            CliPrintf (CliHandle, "priority %d ", i4CurrPriority);
            if (i4CurrDropEligible == PBB_VLAN_SNMP_TRUE)
            {
                i4PagingStatus = CliPrintf (CliHandle,
                                            "drop-eligible true\r\n");
            }
            else
            {
                i4PagingStatus = CliPrintf (CliHandle,
                                            "drop-eligible false\r\n");

            }
            if (i4PagingStatus == CLI_FAILURE)
            {
                break;
            }
        }
        i4CurrIfIndex = i4NextIfIndex;
        i4CurrPcpSelRow = i4NextPcpSelRow;
        i4CurrPcpValue = i4NextPcpValue;
    }
    return i4PagingStatus;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbShowRunningPcpEncodingTable                    */
/*                                                                          */
/*     DESCRIPTION      : This function will Display PCP encoding table     */
/*                                                                          */
/*     INPUT            : i4IfIndex  - Interface Index                      */
/*                                                                          */
/*     OUTPUT           : CliHandle - Contains error messages               */
/*                                                                          */
/*     RETURNS          : VOID                                              */
/*                                                                          */
/****************************************************************************/

PRIVATE VOID
PbbShowRunningPcpEncodingTable (tCliHandle CliHandle, INT4 i4IfIndex)
{

    INT4                i4CurrIfIndex = PBB_INIT_VAL;
    INT4                i4CurrPcpSelRow = PBB_INIT_VAL;
    INT4                i4CurrPriority = PBB_INIT_VAL;
    INT4                i4CurrDropEligible = PBB_INIT_VAL;
    INT4                i4CurrPcpValue = PBB_INIT_VAL;
    INT4                i4DefPcpValue = PBB_INIT_VAL;

    INT4                i4NextIfIndex = PBB_INIT_VAL;
    INT4                i4NextPcpSelRow = PBB_INIT_VAL;
    INT4                i4NextPriority = PBB_INIT_VAL;
    INT4                i4NextDropEligible = PBB_INIT_VAL;
    INT1                i1RetVal = PBB_INIT_VAL;
    INT4                i4PagingStatus = CLI_SUCCESS;

    i4CurrIfIndex = i4IfIndex;
    i4CurrPcpSelRow = PBB_INIT_VAL;
    i4CurrPriority = PBB_INIT_VAL;
    i4CurrDropEligible = PBB_INVALID_DE;

    while (nmhGetNextIndexFsPbbPcpEncodingTable (i4CurrIfIndex,
                                                 &i4NextIfIndex,
                                                 i4CurrPcpSelRow,
                                                 &i4NextPcpSelRow,
                                                 i4CurrPriority,
                                                 &i4NextPriority,
                                                 i4CurrDropEligible,
                                                 &i4NextDropEligible) ==
           SNMP_SUCCESS)
    {

        if (i4CurrIfIndex != i4NextIfIndex)
        {
            break;
        }
        i1RetVal = nmhGetFsPbbPcpEncodingPcpValue (i4NextIfIndex,
                                                   i4NextPcpSelRow,
                                                   i4NextPriority,
                                                   i4NextDropEligible,
                                                   &i4CurrPcpValue);
        if (i1RetVal == SNMP_SUCCESS)
        {
            if (i4NextDropEligible == PBB_VLAN_SNMP_TRUE)
            {
                PbbGetEncodingPcpVal (i4NextPcpSelRow,
                                      i4NextPriority,
                                      i4NextDropEligible, &i4DefPcpValue);
            }
            else
            {
                PbbGetEncodingPcpVal (i4NextPcpSelRow,
                                      i4NextPriority,
                                      PBB_DE_FALSE, &i4DefPcpValue);
            }
            if (i4CurrPcpValue != i4DefPcpValue)
            {
                CliPrintf (CliHandle, "pcp-encoding ");
                switch (i4NextPcpSelRow)
                {
                    case PBB_8P0D_SEL_ROW:
                        CliPrintf (CliHandle, "8P0D ");
                        break;
                    case PBB_7P1D_SEL_ROW:
                        CliPrintf (CliHandle, "7P1D ");
                        break;
                    case PBB_6P2D_SEL_ROW:
                        CliPrintf (CliHandle, "6P2D ");
                        break;
                    case PBB_5P3D_SEL_ROW:
                        CliPrintf (CliHandle, "5P3D ");
                        break;
                    default:
                        break;
                }
                CliPrintf (CliHandle, "priority %d ", i4NextPriority);
                if (i4NextDropEligible == PBB_VLAN_SNMP_TRUE)
                {
                    CliPrintf (CliHandle, "drop-eligible true ");
                }
                else
                {
                    CliPrintf (CliHandle, "drop-eligible false ");
                }
                i4PagingStatus = CliPrintf (CliHandle, "pcp %d\r\n",
                                            i4CurrPcpValue);
                if (i4PagingStatus == CLI_FAILURE)
                {
                    break;
                }
            }
        }
        i4CurrIfIndex = i4NextIfIndex;
        i4CurrPcpSelRow = i4NextPcpSelRow;
        i4CurrPriority = i4NextPriority;
        i4CurrDropEligible = i4NextDropEligible;
    }
}
#endif
