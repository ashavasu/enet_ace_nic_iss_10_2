/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbsz.c,v 1.3 2013/11/29 11:04:11 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _PBBSZ_C
#include "pbbinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
PbbSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < PBB_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsPBBSizingParams[i4SizingId].u4StructSize,
                                     FsPBBSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(PBBMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            PbbSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
PbbSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsPBBSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, PBBMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
PbbSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < PBB_MAX_SIZING_ID; i4SizingId++)
    {
        if (PBBMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (PBBMemPoolIds[i4SizingId]);
            PBBMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
