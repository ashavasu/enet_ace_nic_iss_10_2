/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                        */
/* $Id: pbbbuf.c,v 1.4 2014/03/19 13:36:12 siva Exp $                                                                           */
/*  FILE NAME             : Pbbbuf.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : PBB Module                                      */
/*  MODULE NAME           : PBB                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 24 Aug 2008                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains routines to allocate and      */
/*                          release buffer for PBB module.                  */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : PBB TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#include "pbbinc.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetBuf                                       */
/*                                                                           */
/*    Description         : This function allocates memory of the given      */
/*                          type.                                            */
/*                                                                           */
/*    Input(s)            : u1BufType - Type of memory needed.               */
/*                          u4Size    - Size in bytes                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanMem                                   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : Pointer to the allocated buffer if SUCCESS        */
/*                         NULL if allocation fails                          */
/*                                                                           */
/*****************************************************************************/
UINT1              *
PbbGetBuf (UINT1 u1BufType, UINT4 u4Size)
{
    UINT1              *pu1Buf = NULL;
    u4Size = u4Size;

    switch (u1BufType)
    {
        case PBB_ISID_BUFF:

            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbIsidPoolId, pu1Buf);

            break;
        case PBB_VIP_BUFF:

            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbVipPoolId, pu1Buf);

            break;

        case PBB_PORT_BUFF:
            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbPortPoolId, pu1Buf);

            break;

        case PBB_PORT_ISID_BUFF:

            PBB_ALLOC_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbIsidPortPoolId, pu1Buf);

            break;

        case PBB_QMSG_BUFF:

            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbQMsgPoolId, pu1Buf);

            break;

        case PBB_CONTEXT_INFO:

            PBB_ALLOC_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbContextPoolId, pu1Buf);

            break;

        case PBB_ISID_VIP_INFO:

            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbIsidVipInfoPoolId, pu1Buf);

            break;
        case PBB_ISID_CBP_DLL_BUFF:

            PBB_ALLOC_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbCBPDllPoolId, pu1Buf);

            break;
        case PBB_ISID_CNP_DLL_BUFF:

            PBB_ALLOC_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbCnpDllPoolId, pu1Buf);

            break;

        case PBB_ISID_PIP_VIP_DLL_BUFF:

            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbPipVipMapPoolId, pu1Buf);

            break;
        case PBB_PIP_PCP_BUFF:

            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbPipPcpPoolId, pu1Buf);

            break;

        case PBB_INSTANCE_BUFF:

            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbInstancePoolId, pu1Buf);
            break;

        case PBB_CONTEXT_DLL_BUFF:

            PBB_ALLOC_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                 PbbContextDllPoolId, pu1Buf);
            break;
        default:
            break;
    }

    return pu1Buf;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbReleaseBuf                                   */
/*                                                                           */
/*    Description         : This function releases the allocated memory to   */
/*                          the free pool specified by the buffer type.      */
/*                                                                           */
/*    Input(s)            : u1BufType - Type of memory.                      */
/*                          pu1Buf    - Buffer to be released.               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gVlanMem                                   */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None.                                             */
/*                                                                           */
/*****************************************************************************/
VOID
PbbReleaseBuf (UINT1 u1BufType, UINT1 *pu1Buf)
{
    INT4                i4RetVal = PBB_INIT_VAL;

    switch (u1BufType)
    {
        case PBB_ISID_BUFF:

            i4RetVal = PBB_RELEASE_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbIsidPoolId, pu1Buf);
            break;

        case PBB_VIP_BUFF:

            i4RetVal = PBB_RELEASE_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbVipPoolId, pu1Buf);
            break;

        case PBB_PORT_BUFF:

            i4RetVal = PBB_RELEASE_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbPortPoolId, pu1Buf);
            break;

        case PBB_PORT_ISID_BUFF:

            i4RetVal = PBB_RELEASE_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbIsidPortPoolId, pu1Buf);
            break;

        case PBB_QMSG_BUFF:

            i4RetVal = PBB_RELEASE_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbQMsgPoolId, pu1Buf);

            break;

        case PBB_CONTEXT_INFO:

            i4RetVal = PBB_RELEASE_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbContextPoolId, pu1Buf);

            break;

        case PBB_ISID_VIP_INFO:

            i4RetVal =
                PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                       PbbIsidVipInfoPoolId, pu1Buf);

            break;
        case PBB_ISID_CBP_DLL_BUFF:

            i4RetVal = PBB_RELEASE_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbCBPDllPoolId, pu1Buf);

            break;
        case PBB_ISID_CNP_DLL_BUFF:

            i4RetVal = PBB_RELEASE_MEM_BLOCK
                (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbCnpDllPoolId, pu1Buf);

            break;

        case PBB_ISID_PIP_VIP_DLL_BUFF:

            i4RetVal =
                PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                       PbbPipVipMapPoolId, pu1Buf);

            break;

        case PBB_PIP_PCP_BUFF:

            i4RetVal =
                PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                       PbbPipPcpPoolId, pu1Buf);

            break;
        case PBB_INSTANCE_BUFF:
            i4RetVal =
                PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                       PbbInstancePoolId, pu1Buf);
            break;

        case PBB_CONTEXT_DLL_BUFF:
            i4RetVal =
                PBB_RELEASE_MEM_BLOCK (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                                       PbbContextDllPoolId, pu1Buf);
            break;
        default:
            break;
    }
    UNUSED_PARAM (i4RetVal);
}
