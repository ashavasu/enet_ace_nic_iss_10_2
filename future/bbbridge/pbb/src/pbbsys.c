/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbsys.c,v 1.37 2015/03/25 06:10:10 siva Exp $
 *
 * Description: This file contains PBB related routines.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                     */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : pbbsys.c                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : PBB Module                                      */
/*  MODULE NAME           : PBB                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 20 Mar 2008                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains PBB related routines.        */
/*****************************************************************************/
#ifndef __PBBSYS_C__
#define __PBBSYS_C__

#include "pbbinc.h"
#include "pbbglob.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbPostCfgMessage                                */
/*                                                                           */
/*    Description         : Posts the message to Pbb Config Q.              */
/*                                                                           */
/*    Input(s)            : u1MsgType -  Meesage Type                        */
/*                          u2Port    -  Port Index.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / PBB_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbPostCfgMessage (UINT2 u2MsgType, INT4 i4IfIndex)
{
    tPbbQMsg           *pPbbQMsg = NULL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PBB_IS_MODULE_INITIALISED () != PBB_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    /* If Pbb is shutdown, then don't send this indication to it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB is Shutdown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (PbbVcmGetContextInfoFromIfIndex (i4IfIndex,
                                         &u4ContextId,
                                         &u2LocalPort) == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (NULL == pPbbQMsg)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));
    pPbbQMsg->u2MsgType = u2MsgType;
    pPbbQMsg->u4ContextId = u4ContextId;
    pPbbQMsg->i4IfIndex = i4IfIndex;
    pPbbQMsg->u2LocalPort = u2LocalPort;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Relase the buffer since Msg is already posted */
        return PBB_FAILURE;
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbPostPortCreateMessage                        */
/*                                                                           */
/*    Description         : Posts the message to PBB Config Q.              */
/*                                                                           */
/*    Input(s)            : u4ContextId -  Context Identifier                */
/*                          u4IfIndex   -  Interface index (physical)        */
/*                          u2Port    -  Port Index (local port number).     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / VPBBFAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PbbPostPortCreateMessage (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort,
                          UINT2 u2MsgType)
{
    tPbbQMsg           *pPbbQMsg = NULL;

    if (PBB_IS_MODULE_INITIALISED () != PBB_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    /* If Pbb is shutdown, then don't send this indication to it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB is Shutdown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (NULL == pPbbQMsg)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = u2MsgType;
    pPbbQMsg->u4ContextId = u4ContextId;
    pPbbQMsg->i4IfIndex = i4IfIndex;
    pPbbQMsg->u2LocalPort = u2LocalPort;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Relase the buffer since Msg is already posted */
        return PBB_FAILURE;
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbProcessCfgEvent                              */
/*                                                                           */
/*    Description         : Function Process the received configuration event*/
/*                                                                           */
/*    Input(s)            : pPbbQMsg - Pointer to the Q Message             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
PbbProcessCfgEvent (tPbbQMsg * pPbbQMsg)
{
    INT4                i4RetVal = PBB_INIT_VAL;
    INT4                i4SetValFsPbbShutdownStatus = PBB_SNMP_TRUE;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    UINT4               u4ContextId = PBB_INIT_VAL;
#ifdef NPAPI_WANTED
    UINT2               u2LocalDstPort = PBB_INIT_VAL;
#endif
#ifdef MBSM_WANTED
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
    INT4                i4ProtoId = PBB_INIT_VAL;
#endif /* MBSM_WANTED */
    u2LocalPort = PBB_INIT_VAL;

    if (PBB_IS_MODULE_INITIALISED () == PBB_TRUE)
    {
        if (PbbL2IwfGetBridgeMode (pPbbQMsg->u4ContextId, &u4BridgeMode) ==
            PBB_SUCCESS)
        {
            if (!((u4BridgeMode == PBB_CUSTOMER_BRIDGE_MODE) ||
                  (u4BridgeMode == PBB_PROVIDER_BRIDGE_MODE) ||
                  (u4BridgeMode == PBB_PROVIDER_EDGE_BRIDGE_MODE) ||
                  (u4BridgeMode == PBB_PROVIDER_CORE_BRIDGE_MODE) ||
                  (u4BridgeMode == PBB_INVALID_BRIDGE_MODE)))
            {
                if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
                {
                    i4SetValFsPbbShutdownStatus = PBB_SNMP_FALSE;
                    nmhSetFsPbbShutdownStatus (i4SetValFsPbbShutdownStatus);
                    if (PbbGetAllPorts (u4ContextId) != PBB_SUCCESS)
                    {
                        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                                      "%s :: %s() :: Failed since PbbGetAllPorts()failed "
                                      "\n", __FILE__, PBB_FUNCTION_NAME);
                    }

                }
            }
        }
    }

    switch (PBB_QMSG_TYPE (pPbbQMsg))
    {
        case PBB_CREATE_CONTEXT_MSG:

            i4RetVal = PbbHandleCreateContext (pPbbQMsg->u4ContextId);

            if (i4RetVal == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d creation failed  \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);

            }
            L2MI_SYNC_GIVE_SEM ();
            break;

        case PBB_DELETE_CONTEXT_MSG:

            i4RetVal = PbbHandleDeleteContext (pPbbQMsg->u4ContextId);

            if (i4RetVal == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d deletion failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
            }

            L2MI_SYNC_GIVE_SEM ();
            break;

        case PBB_UPDATE_CONTEXT_NAME:

            if (PbbSelectContext (pPbbQMsg->u4ContextId) == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d selection failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
                break;
            }

            i4RetVal = PbbHandleUpdateContextName (pPbbQMsg->u4ContextId);

            if (i4RetVal == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d name update failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
            }
            break;

        case PBB_PORT_CREATE_MSG:

            /* switch context */
            if (PbbSelectContext (pPbbQMsg->u4ContextId) == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d selection failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
                L2_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal = PbbHandleCreatePort (pPbbQMsg->u4ContextId,
                                            pPbbQMsg->i4IfIndex,
                                            pPbbQMsg->u2LocalPort);

            if (i4RetVal == PBB_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB create port if index %d failed   \n",
                              __FILE__, PBB_FUNCTION_NAME, pPbbQMsg->i4IfIndex);
                break;
            }

            L2_SYNC_GIVE_SEM ();
            break;

        case PBB_PORT_DELETE_MSG:

            /* switch context */
            if (PbbSelectContext (pPbbQMsg->u4ContextId) == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d selection failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);

                L2_SYNC_GIVE_SEM ();
                break;
            }

#if defined (L2RED_WANTED) && defined(NPAPI_WANTED)
            EvtSyncFsMiPbbDeletePort (pPbbQMsg->u4ContextId,
                                      (UINT4) pPbbQMsg->i4IfIndex);
            PBB_NPSYNC_BLK () = OSIX_TRUE;
#endif

            i4RetVal = PbbHandleDeletePort (pPbbQMsg->u4ContextId,
                                            pPbbQMsg->i4IfIndex,
                                            pPbbQMsg->u2LocalPort);

#if defined (L2RED_WANTED) && defined(NPAPI_WANTED)
            if (PBB_NODE_STATUS () != PBB_NODE_IDLE)
            {
                PBB_NPSYNC_BLK () = OSIX_FALSE;
            }
#endif

            if (i4RetVal == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB create delete if index %d failed   \n",
                              __FILE__, PBB_FUNCTION_NAME, pPbbQMsg->i4IfIndex);

                L2_SYNC_GIVE_SEM ();
                break;
            }

            L2_SYNC_GIVE_SEM ();

            break;

        case PBB_MAP_PORT_INDICATION:

            /* switch context */
            if (PbbSelectContext (pPbbQMsg->u4ContextId) == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d selection failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
                L2MI_SYNC_GIVE_SEM ();
                break;
            }

            i4RetVal = PbbHandleMapPortInd (pPbbQMsg->u4ContextId,
                                            pPbbQMsg->i4IfIndex,
                                            pPbbQMsg->u2LocalPort);

            if (i4RetVal == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB Agg port create if index %d failed"
                              "   \n", __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->i4IfIndex);
            }

            L2MI_SYNC_GIVE_SEM ();
            break;

        case PBB_UNMAP_PORT_INDICATION:

            /* switch context */
            if (PbbSelectContext (pPbbQMsg->u4ContextId) == PBB_FAILURE)
            {
                L2MI_SYNC_GIVE_SEM ();
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d selection failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
                break;
            }

            i4RetVal = PbbHandleUnMapPortInd (pPbbQMsg->u4ContextId,
                                              pPbbQMsg->i4IfIndex,
                                              pPbbQMsg->u2LocalPort);
            if (i4RetVal == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB Agg port delete if index %d failed"
                              "   \n", __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->i4IfIndex);
            }
            L2MI_SYNC_GIVE_SEM ();

            break;

        case PBB_UPDATE_PORT_STATUS:
            /* switch context */
            if (PbbSelectContext (pPbbQMsg->u4ContextId) == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d selection failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
                break;
            }

            if (PbbSetPortStatus (pPbbQMsg->u4ContextId,
                                  pPbbQMsg->i4IfIndex,
                                  pPbbQMsg->u1PortStatus) != PBB_SUCCESS)
            {
                PBB_TRC_ARG4 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB Set Port if index : %d status : %d"
                              " failed\n", __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->i4IfIndex, pPbbQMsg->u1PortStatus);
                break;
            }

            i4RetVal = PbbRedSyncPortOperStatus (pPbbQMsg->u4ContextId,
                                                 pPbbQMsg->i4IfIndex,
                                                 pPbbQMsg->u1PortStatus);
            if (i4RetVal == PBB_FAILURE)
            {
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: PBBRED: "
                              "Failed to sync oper status for Port %d in "
                              "Standby Node \n", __FILE__, PBB_FUNCTION_NAME,
                              u2LocalPort);
            }

            break;

#ifdef MBSM_WANTED
        case MBSM_MSG_CARD_INSERT:

            i4ProtoId = pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg->i4ProtoCookie;
            pSlotInfo = &(pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmSlotInfo);
            pPortInfo = &(pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmPortInfo);

            i4RetVal = PbbMbsmUpdateOnCardInsertion (pPortInfo, pSlotInfo);

            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
            MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
            MbsmSendAckFromProto (&MbsmProtoAckMsg);
            MEM_FREE (pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg);
            break;

        case MBSM_MSG_CARD_REMOVE:

            i4ProtoId = pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg->i4ProtoCookie;
            pSlotInfo = &(pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmSlotInfo);
            pPortInfo = &(pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmPortInfo);

            i4RetVal = PbbMbsmUpdateOnCardRemoval (pPortInfo, pSlotInfo);

            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
            MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
            MbsmSendAckFromProto (&MbsmProtoAckMsg);
            MEM_FREE (pPbbQMsg->MbsmCardUpdate.pMbsmProtoMsg);
            break;

        case MBSM_MSG_LOAD_SHARING_ENABLE:
            PbbMbsmUpdtForLoadSharing (MBSM_LOAD_SHARING_ENABLE);
            break;

        case MBSM_MSG_LOAD_SHARING_DISABLE:
            PbbMbsmUpdtForLoadSharing (MBSM_LOAD_SHARING_DISABLE);
            break;
#endif

#ifdef L2RED_WANTED
        case PBB_RM_MSG:

            PbbRedProcessUpdateMsg (pPbbQMsg);
            break;

        case PBB_VLAN_AUDIT_STATUS_IND:

            PbbRedHandleVlanAudEndEvent (pPbbQMsg->u4VlanAudStatus,
                                         pPbbQMsg->u4ContextId);

            break;
#endif /* L2RED_WANTED */

#ifdef NPAPI_WANTED
        case PBB_COPY_PORT_INFO_MSG:

            if (PbbVcmGetContextInfoFromIfIndex (pPbbQMsg->i4IfIndex,
                                                 &u4ContextId,
                                                 &u2LocalPort) == PBB_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: Failed\n", __FILE__,
                              PBB_FUNCTION_NAME);

                break;
            }

            if (PbbVcmGetContextInfoFromIfIndex (pPbbQMsg->u4DstPort,
                                                 &u4ContextId,
                                                 &u2LocalDstPort) ==
                PBB_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: Failed\n", __FILE__,
                              PBB_FUNCTION_NAME);
                break;
            }
            /* we presume that both the port belongs to same context */
            /* switch context */
            if (PbbSelectContext (pPbbQMsg->u4ContextId) == PBB_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d selection failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
                break;
            }

#ifdef L2RED_WANTED
            /* Send event Sync up to the Standby node */
            EvtSyncFsMiPbbCopyPortProperty (pPbbQMsg->u4ContextId,
                                            pPbbQMsg->i4IfIndex,
                                            pPbbQMsg->u4DstPort);
            PBB_NPSYNC_BLK () = OSIX_TRUE;
#endif

            PbbHandleCopyPortPropertiesToHw (u2LocalDstPort, u2LocalPort);

#ifdef L2RED_WANTED
            if (PBB_NODE_STATUS () != PBB_NODE_IDLE)
            {
                PBB_NPSYNC_BLK () = OSIX_FALSE;
            }
#endif
            L2_SYNC_GIVE_SEM ();

            break;
        case PBB_REMOVE_PORT_INFO_MSG:

            /* we presume that both the port belongs to same context */
            /* switch context */
            if (PbbSelectContext (pPbbQMsg->u4ContextId) == PBB_FAILURE)
            {
                L2_SYNC_GIVE_SEM ();
                PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s :: PBB context %d selection failed   \n",
                              __FILE__, PBB_FUNCTION_NAME,
                              pPbbQMsg->u4ContextId);
                break;
            }

#ifdef L2RED_WANTED
            EvtSyncFsMiPbbRemPortProperty (pPbbQMsg->u4ContextId,
                                           pPbbQMsg->i4IfIndex,
                                           pPbbQMsg->u4DstPort);

            PBB_NPSYNC_BLK () = OSIX_TRUE;
#endif

            PbbHandleRemovePortPropertiesFromHw (pPbbQMsg->u4DstPort,
                                                 pPbbQMsg->i4IfIndex);

#ifdef L2RED_WANTED
            if (PBB_NODE_STATUS () != PBB_NODE_IDLE)
            {
                PBB_NPSYNC_BLK () = OSIX_FALSE;
            }
#endif
            L2_SYNC_GIVE_SEM ();

            break;
#endif
        default:
            return;
    }

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbHandleCreatePort                             */
/*                                                                           */
/*    Description         : Creates a Port in PBB module                    */
/*                                                                           */
/*    Input(s)            : u2Port    - The Number of the Port               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gau1PriTrfClassMap                         */
/*                                                                           */
/*    Global Variables Modified : gpPbbContextInfo->u2PbbStartPortInd,     */
/*                                gpPbbContextInfo->u2PbbLastPortInd       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbHandleCreatePort (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort)
{
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    INT4                i4SetValFsPbbShutdownStatus = PBB_SNMP_TRUE;
    UINT1               i4SetValFsPbbCbpRowStatus = PBB_ACTIVE;
    PBB_MEMSET (&CfaIfInfo, PBB_INIT_VAL, sizeof (tCfaIfInfo));
    /* Check the  PBB intialization */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    if (PbbL2IwfGetBridgeMode (PBB_CURR_CONTEXT_ID (), &u4BridgeMode) !=
        PBB_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: PBB System"
                      " is not able to get the Bridge mode from l2iwf\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    PBB_BRIDGE_MODE () = u4BridgeMode;

    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: failed since"
                      " bridge mode is not PBB\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        i4SetValFsPbbShutdownStatus = PBB_SNMP_FALSE;
        if (nmhSetFsPbbShutdownStatus (i4SetValFsPbbShutdownStatus) !=
            SNMP_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }

    /*Validating Context */

    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbCfaGetIfInfo (i4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: failed to get"
                      " cfa interface parameters\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (CfaIfInfo.u1BrgPortType == PBB_VIRTUAL_INSTANCE_PORT)
    {
        if (PbbCreateVipRBTreeNode (i4IfIndex) != PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: failed "
                          "to create VIP rb tree node \n", __FILE__,
                          PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
    }
    else
    {
        if (PbbValidatePort (i4IfIndex) == PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: failed "
                          "to validate Port \n", __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
        if ((CfaIfInfo.u1BrgPortType == PBB_CUSTOMER_BACKBONE_PORT) ||
            (CfaIfInfo.u1BrgPortType == PBB_PROVIDER_INSTANCE_PORT) ||
            (CfaIfInfo.u1BrgPortType == PBB_CNP_PORTBASED_PORT) ||
            (CfaIfInfo.u1BrgPortType == PBB_CNP_STAGGED_PORT) ||
            (CfaIfInfo.u1BrgPortType == PBB_CNP_CTAGGED_PORT))
        {
            if (PbbCreatePortRBTreeNode (i4IfIndex, u2LocalPort) != PBB_SUCCESS)
            {
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: failed "
                              "to create Port rb tree node \n", __FILE__,
                              PBB_FUNCTION_NAME);
                return PBB_FAILURE;
            }
        }
        else
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: failed "
                          "since port is not a PBB port\n", __FILE__,
                          PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }
        if (CfaIfInfo.u1BrgPortType == PBB_CUSTOMER_BACKBONE_PORT)
        {
            if (PbbSetPortCbpTableRowstatus (i4IfIndex,
                                             i4SetValFsPbbCbpRowStatus) !=
                PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }

        }
        if (CfaIfInfo.u1BrgPortType == PBB_PROVIDER_INSTANCE_PORT)
        {
            if (PbbHwSetProviderBridgePortType (u4ContextId,
                                                i4IfIndex,
                                                CfaIfInfo.u1BrgPortType) !=
                PBB_SUCCESS)
            {
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: failed "
                              "setting Pip port type at H/W\n", __FILE__,
                              PBB_FUNCTION_NAME);
                return PBB_FAILURE;
            }

        }

    }

    if (PbbL2IwfGetBridgeMode
        (PBB_CURR_CONTEXT_PTR ()->u4ContextId, &u4BridgeMode) == PBB_SUCCESS)
    {
        PBB_CURR_CONTEXT_PTR ()->u4BridgeMode = u4BridgeMode;
        /* check if this is the first internal port in B-Component */
        if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE &&
            CfaIfInfo.i4IsInternalPort == CFA_TRUE)
        {
            if (++(PBB_CURR_CONTEXT_PTR ()->u2InternalCbpCount) == 1)
            {
                PbbCreateIBCompBridgeEcfmFilter ();
                PbbBrgHwCreateAstControlPktFilter ();
            }
        }
        /*end of if */
    }
    /*Initialize the Tunnel status maintained in L2IWF */

    PbbL2IwfSetProtocolTunnelStatusOnPort (u4ContextId,
                                           u2LocalPort, L2_PROTO_DOT1X,
                                           PBB_TUNNEL_PROTOCOL_PEER);
    PbbL2IwfSetProtocolTunnelStatusOnPort (u4ContextId,
                                           u2LocalPort, L2_PROTO_LACP,
                                           PBB_TUNNEL_PROTOCOL_PEER);

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbHandleDeletePort                             */
/*                                                                           */
/*    Description         : Deletes the specified port from PBB  Module.     */
/*                          by management module.                            */
/*                                                                           */
/*    Input(s)            : u2Port - The number of the port that is deleted. */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None                                       */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB _SUCCESS                                      */
/*                         PBB _FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbHandleDeletePort (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort)
{
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4BridgeMode = 0;

    /* Check the  PBB intialization */
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {
        return PBB_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        return PBB_FAILURE;
    }
    /* Select the context first */
    if (PbbValidateContextId (u4ContextId) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }

    if (PbbDeletePortInfo (i4IfIndex, u2LocalPort, u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbCfaGetIfInfo (i4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: failed to get"
                      " cfa interface parameters\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (PbbL2IwfGetBridgeMode
        (PBB_CURR_CONTEXT_PTR ()->u4ContextId, &u4BridgeMode) == PBB_SUCCESS)
    {
        /* check if this is the first internal port in B-Component */
        if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE &&
            CfaIfInfo.i4IsInternalPort == CFA_TRUE)
        {
            if (--(PBB_CURR_CONTEXT_PTR ()->u2InternalCbpCount) == PBB_INIT_VAL)
            {
                PbbDeleteIBCompBridgeEcfmFilter ();
                PbbBrgHwDestroyAstControlPktFilter ();
            }
        }
        /*end of if */
    }

    return PBB_SUCCESS;
}

/************************************************************************/
/* Function Name    : PbbTaskInit                                      */
/*                                                                      */
/* Description      : Creates PBB Task Queue, Config Queue and Sema4   */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : PBB_SUCCESS / PBB_FAILURE                       */
/************************************************************************/
INT4
PbbTaskInit (VOID)
{

    tMemPoolId          u4MemPoolId;
    UINT4               u4Oui = PBB_INIT_VAL;
    UINT1               au1PbbPortBitMaskMap[] =
        { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };
    UINT1               au1PbbBcastAddress[] =
        { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

    UINT1               au1InitPcpDecPriority[PBB_MAX_NUM_PCP_SEL_ROW]
        [PBB_MAX_NUM_PCP] = { {0, 1, 2, 3, 4, 5, 6, 7},
    {0, 1, 2, 3, 4, 4, 6, 7},
    {0, 1, 2, 2, 4, 4, 6, 7},
    {0, 0, 2, 2, 4, 4, 6, 7},
    };

    UINT1               au1InitPcpDecDropEligible[PBB_MAX_NUM_PCP_SEL_ROW]
        [PBB_MAX_NUM_PCP] = { {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 1, 0, 0, 0},
    {0, 0, 1, 0, 1, 0, 0, 0},
    {1, 0, 1, 0, 1, 0, 0, 0},
    };

    UINT1               au1InitPcpEncValue[PBB_MAX_NUM_PCP_SEL_ROW]
        [PBB_MAX_NUM_PRIORITY][PBB_MAX_NUM_DROP_ELIGIBLE] = {
        {{0, 0}, {1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6}, {7, 7},},
        {{0, 0}, {1, 1}, {2, 2}, {3, 3}, {5, 4}, {5, 4}, {6, 6}, {7, 7},},
        {{0, 0}, {1, 1}, {3, 2}, {3, 2}, {5, 4}, {5, 4}, {6, 6}, {7, 7},},
        {{1, 0}, {1, 0}, {3, 2}, {3, 2}, {5, 4}, {5, 4}, {6, 6}, {7, 7},}
    };
    INT4                i4RetVal = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    INT4                i4SetValFsPbbShutdownStatus = PBB_SNMP_TRUE;
#ifdef SNMP_2_WANTED
    /* Register the protocol MIBs with SNMP */
    PbbRegisterMIB ();
#endif
    PBB_MEMSET (&(gPbbGlobData.PbbTaskInfo), PBB_INIT_VAL,
                sizeof (tPbbTaskInfo));
    PBB_MEMSET (&u4MemPoolId, PBB_INIT_VAL, sizeof (tMemPoolId));

    PBB_MEMSET (&gFsPbbSizingInfo, 0, sizeof (tFsModSizingInfo));
    PBB_MEMCPY (gFsPbbSizingInfo.ModName, "PBB", STRLEN ("PBB"));
    gFsPbbSizingInfo.u4ModMemPreAllocated = 0;
    gFsPbbSizingInfo.ModSizingParams = gFsPbbSizingParams;

    FsUtlSzCalculateModulePreAllocatedMemory (&gFsPbbSizingInfo);

    if (PBB_CREATE_SEM (PBB_SEM_NAME, 1, PBB_INIT_VAL, &(PBB_SEM_ID)) !=
        OSIX_SUCCESS)
    {
        PBB_TRC_ARG2 ((OS_RESOURCE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC),
                      "%s :: %s() :: Failed to Create PBB Mutex Sema4 \r\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (PBB_CREATE_QUEUE (PBB_CFG_QUEUE, OSIX_MAX_Q_MSG_LEN,
                          PBB_CFG_Q_DEPTH, &(PBB_CFG_QUEUE_ID)) != OSIX_SUCCESS)
    {
        PBB_TRC_ARG2 ((OS_RESOURCE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC),
                      "%s :: %s() :: Failed to create queue for PBB Config\n",
                      __FILE__, PBB_FUNCTION_NAME);

        PBB_DELETE_SEM (PBB_SEM_ID);
        return PBB_FAILURE;
    }

    if (PbbRedInitBufferPool () != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    /*Creating Mempools */
    if (PbbSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: failed since"
                      " PbbSizingMemCreateMemPools() failed\n", __FILE__,
                      PBB_FUNCTION_NAME);

        PBB_DELETE_QUEUE (PBB_CFG_QUEUE_ID);
        PBB_DELETE_SEM (PBB_SEM_ID);
        /* Deinitialise all protocol related memory pools */
        PbbSizingMemDeleteMemPools ();
        return PBB_FAILURE;

    }
    /*Assigning respective mempools Ids */
    PbbMainAssignMempoolIds ();

    /*Initialzing global data */
    PBB_MEMSET (PBB_GLOBAL_OUI, PBB_INIT_VAL, PBB_OUI_LENGTH);
    for (u4Oui = 0; u4Oui < PBB_OUI_LENGTH; u4Oui++)
    {
        gPbbGlobData.PbbTaskInfo.au1OUI[0] = 0;
        gPbbGlobData.PbbTaskInfo.au1OUI[1] = 30;
        gPbbGlobData.PbbTaskInfo.au1OUI[2] = 131;
    }
    gPbbGlobData.au1PbbShutDownStatus = PBB_SNMP_TRUE;
    gPbbGlobData.PbbTaskInfo.u4MaxIsid = PBB_MAX_NUM_OF_ICOMP_ISID
        + PBB_MAX_NUM_OF_BCOMP_ISID;
    gPbbGlobData.PbbTaskInfo.u4MaxNumIsidPerContext =
        PBB_MAX_NUM_OF_ISID_PER_CONTEXT;
    gPbbGlobData.PbbTaskInfo.u4MaxPortperIsid = PBB_MAX_NUM_OF_PORT_PER_ISID;
    gPbbGlobData.PbbTaskInfo.u4NumberofIcomp = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4NumberofBcomp = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4MaxPortperIsidPerContext =
        PBB_MAX_NUM_OF_PORT_PER_ISID_PER_CONTEXT;

    PBB_MEMSET (gPbbGlobData.PbbTaskInfo.au1BridgeName, PBB_INIT_VAL,
                PBB_BRIDGE_NAME_LEN);
    gPbbGlobData.PbbTaskInfo.au1BridgeName[0] = 'D';
    gPbbGlobData.PbbTaskInfo.au1BridgeName[1] = 'e';
    gPbbGlobData.PbbTaskInfo.au1BridgeName[2] = 'f';
    gPbbGlobData.PbbTaskInfo.au1BridgeName[3] = 'a';
    gPbbGlobData.PbbTaskInfo.au1BridgeName[4] = 'u';
    gPbbGlobData.PbbTaskInfo.au1BridgeName[5] = 'l';
    gPbbGlobData.PbbTaskInfo.au1BridgeName[6] = 't';
    gPbbGlobData.u1IsPbbInitialised = PBB_TRUE;
    PBB_MEMCPY (gPbbGlobData.au1PbbPortBitMaskMap,
                au1PbbPortBitMaskMap, PBB_PORTS_PER_BYTE);
    PBB_MEMCPY (gPbbGlobData.PbbBcastAddress,
                au1PbbBcastAddress, ETHERNET_ADDR_SIZE);
    PBB_MEMCPY (gPbbGlobData.au1PcpDecPriority,
                au1InitPcpDecPriority,
                (PBB_MAX_NUM_PCP_SEL_ROW * PBB_MAX_NUM_PCP));
    PBB_MEMCPY (gPbbGlobData.au1PcpDecDropEligible,
                au1InitPcpDecDropEligible,
                (PBB_MAX_NUM_PCP_SEL_ROW * PBB_MAX_NUM_PCP));
    PBB_MEMCPY (gPbbGlobData.au1PcpEncValue,
                au1InitPcpEncValue,
                (PBB_MAX_NUM_PCP_SEL_ROW * PBB_MAX_NUM_PRIORITY *
                 PBB_MAX_NUM_DROP_ELIGIBLE));

    gPbbGlobData.u4TraceOption |= PBB_CRITICAL_TRC;
    PbbUtilGetTraceInputValue (gPbbGlobData.au1TraceInput,
                               gPbbGlobData.u4TraceOption);

    PBB_NODE_STATUS () = PbbRedGetNodeStatus ();

    /* Intialize the PBB Red Info */
    PbbRedInitGlobalInfo ();
    PbbRedInitRedundancyInfo ();

    /* Register PBB with RM */
    i4RetVal = PbbRedRegisterWithRM ();

    if (i4RetVal == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Registration with RM failed \n",
                      PBB_FILE_NAME, PBB_FUNCTION_NAME);

        PbbRedDeInitGlobalInfo ();
        PbbRedDeInitRedundancyInfo ();
        PbbRedDeInitBufferPool ();
        return PBB_FAILURE;
    }

    if (PBB_IS_MODULE_INITIALISED () == PBB_TRUE)
    {
        if (PbbL2IwfGetBridgeMode (PBB_DEF_CONTEXT_ID, &u4BridgeMode) ==
            PBB_SUCCESS)
        {
            if (!((u4BridgeMode == PBB_CUSTOMER_BRIDGE_MODE) ||
                  (u4BridgeMode == PBB_PROVIDER_BRIDGE_MODE) ||
                  (u4BridgeMode == PBB_PROVIDER_EDGE_BRIDGE_MODE) ||
                  (u4BridgeMode == PBB_PROVIDER_CORE_BRIDGE_MODE) ||
                  (u4BridgeMode == PBB_INVALID_BRIDGE_MODE)))
            {
                if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
                {
                    i4SetValFsPbbShutdownStatus = PBB_SNMP_FALSE;
                    nmhSetFsPbbShutdownStatus (i4SetValFsPbbShutdownStatus);
                }
            }
        }
    }
    /* If the default bridge mode in the issnvram is not PBB, only then we 
       will create the default context */
    if (i4SetValFsPbbShutdownStatus == PBB_SNMP_TRUE)
    {
        i4RetVal = PbbHandleCreateContext (PBB_DEF_CONTEXT_ID);
    }

    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/************************************************************************/
/* Function Name    : PbbMainDeInit                                    */
/*                                                                      */
/* Description      : Deletes Pbb Task Queue, Config Queue, Sema4,     */
/*                    Memory pools and global informations              */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None.                                             */
/************************************************************************/
VOID
PbbMainDeInit (VOID)
{
    /*Deleting all mempools */
    PbbSizingMemDeleteMemPools ();
    gPbbGlobData.PbbTaskInfo.u4MaxIsid = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4MaxNumIsidPerContext = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4MaxPortperIsid = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4NumberofIcomp = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4NumberofBcomp = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance = PBB_INIT_VAL;
    gPbbGlobData.PbbTaskInfo.u4MaxPortperIsidPerContext =
        PBB_MAX_NUM_OF_PORT_PER_ISID_PER_CONTEXT;

    PBB_MEMSET (gPbbGlobData.PbbTaskInfo.au1BridgeName, PBB_INIT_VAL,
                PBB_BRIDGE_NAME_LEN);

    gPbbGlobData.u1IsPbbInitialised = PBB_FALSE;
    PBB_DELETE_QUEUE (PBB_CFG_QUEUE_ID);

    PBB_DELETE_SEM (PBB_SEM_ID);
    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbInit                                         */
/*                                                                           */
/*    Description         : Initialises the PBB tables and creates memory   */
/*                          pool for all the tables.                         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS on success                           */
/*                         PBB_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbInit ()
{

    if (PBB_MODULE_STATUS () != PBB_ENABLED)
    {
        PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_SUCCESS;
    }

    if (PbbTableInit () == PBB_FAILURE)
    {
        PbbDeInit ();
        PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: failed since Pbb tables"
                      " creation failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);

    return PBB_SUCCESS;
}

/************************************************************************/
/* Function Name    : PbbInitGlobalInfo                                */
/*                                                                      */
/* Description      : Initializes Global Task Info                      */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
INT1
PbbInitGlobalInfo (VOID)
{
    UINT4               u4Context = PBB_INIT_VAL;
    UINT4               u4BridgeMode = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        gPbbGlobData.au1PbbStatus[u4Context] = PBB_DISABLED;
    }

    if (PbbInitMemPools () == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: failed since"
                      " PbbInitMemPools() failed\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (PBB_DEF_CONTEXT_ID);
    if (PBB_CURR_CONTEXT_PTR () == NULL)
    {
        /*  The default bridge mode is PBB, then create the default context 
           by taking the buffer from the Context Pool  */
        PBB_CURR_CONTEXT_PTR () = (tPbbContextInfo *) (VOID *)
            PBB_GET_BUF (PBB_CONTEXT_INFO, sizeof (tPbbContextInfo));
        if (PBB_CURR_CONTEXT_PTR () == NULL)
        {
            PBB_TRC_ARG2 (INIT_SHUT_TRC | BUFFER_TRC | OS_RESOURCE_TRC,
                          "%s :: %s() :: Failed since context buffer allocation "
                          "failed\n", __FILE__, PBB_FUNCTION_NAME);
            PbbDeInitGlobalInfo ();
            return PBB_FAILURE;
        }
        PBB_CONTEXT_PTR (PBB_DEF_CONTEXT_ID) = PBB_CURR_CONTEXT_PTR ();
    }
    PBB_MEMSET (PBB_CURR_CONTEXT_PTR (), PBB_INIT_VAL,
                sizeof (tPbbContextInfo));
    PBB_CURR_CONTEXT_PTR ()->u4ContextId = PBB_DEF_CONTEXT_ID;
    PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent = PBB_INIT_VAL;
    PBB_CURR_CONTEXT_PTR ()->u4PbbDbg = PBB_INIT_VAL;
    PbbL2IwfGetBridgeMode (PBB_DEF_CONTEXT_ID, &u4BridgeMode);
    PBB_CURR_CONTEXT_PTR ()->u4BridgeMode = u4BridgeMode;
    gPbbGlobData.au1PbbStatus[PBB_DEF_CONTEXT_ID] = PBB_ENABLED;
    PBB_MEMSET (PBB_CURR_CONTEXT_STR (), PBB_INIT_VAL,
                PBB_CONTEXT_ALIAS_LEN + 1);
    if (PbbVcmGetSystemModeExt (PBB_PROTOCOL_ID) == VCM_MI_MODE)
    {
        PbbVcmGetAliasName (PBB_CURR_CONTEXT_ID (), PBB_CURR_CONTEXT_STR ());
        STRCAT (PBB_CURR_CONTEXT_STR (), ":");
    }

    /* creating the default backbone instance */
    PBB_CURR_INSTANCE_PTR () = (tPbbInstanceInfo *) (VOID *)
        PBB_GET_BUF (PBB_INSTANCE_BUFF, sizeof (tPbbInstanceInfo));

    if (PBB_CURR_INSTANCE_PTR () == NULL)
    {
        PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC | BUFFER_TRC | OS_RESOURCE_TRC,
                      "%s :: %s() :: Failed since instance buffer allocation "
                      "failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    PBB_MEMSET (PBB_CURR_INSTANCE_PTR (),
                PBB_INIT_VAL, sizeof (tPbbInstanceInfo));
    PBB_INSTANCE_PTR (PBB_DEF_INSTANCE_ID) = PBB_CURR_INSTANCE_PTR ();
    PBB_CURR_INSTANCE_PTR ()->u4InstanceId = PBB_DEF_INSTANCE_ID;
    PBB_MEMSET (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
                PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN + 1);

    PBB_STRNCPY (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr, "default",
                 PBB_STRLEN ("default"));
    PbbGetDefaultInstanceMacAddress (PBB_DEF_INSTANCE_ID,
                                     PBB_CURR_INSTANCE_PTR ()->InstanceMacAddr);
    PBB_CURR_INSTANCE_PTR ()->u1MacAddressType = PBB_INSTANCE_MAC_DERIVED;
    PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfIcomp = PBB_INIT_VAL;
    PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfBcomp = PBB_INIT_VAL;
    PBB_CURR_INSTANCE_PTR ()->u1InstanceRowStatus = PBB_ACTIVE;
    PBB_DLL_INIT (&(PBB_CURR_INSTANCE_PTR ()->ContextDll));
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_TRUE)
    {
        PBB_CURR_CONTEXT_PTR ()->u4InstanceId = PBB_DEF_INSTANCE_ID;
        if (PbbCreateContextEntryDll (PBB_DEF_INSTANCE_ID,
                                      PBB_DEF_CONTEXT_ID) != PBB_SUCCESS)
        {
            /* Failed while adding the context id in the Context List    */
            /* for default instance                                      */
            return PBB_FAILURE;
        }
        if (PbbGetAllPorts (PBB_DEF_CONTEXT_ID) != PBB_SUCCESS)
        {
            PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                          "%s :: %s() :: Failed since PbbGetAllPorts()failed "
                          "\n", __FILE__, PBB_FUNCTION_NAME);
            return PBB_FAILURE;
        }

        PbbCreateCompBridgeEcfmFilter ();
        if (u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            PbbBrgHwCreateAstControlPktFilter ();
            gPbbGlobData.PbbTaskInfo.u4NumberofIcomp++;
            if (PBB_CURR_CONTEXT_PTR ()->u4InstanceId != PBB_INVALID_INSTANCE)
            {
                PbbIncrInstanceNumOfIcomp ();
            }
        }

        if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
        {
            gPbbGlobData.PbbTaskInfo.u4NumberofBcomp++;
            if (PBB_CURR_CONTEXT_PTR ()->u4InstanceId != PBB_INVALID_INSTANCE)
            {
                PbbIncrInstanceNumOfBcomp ();
            }
        }
    }
    else
    {
        PBB_CURR_CONTEXT_PTR ()->u4InstanceId = PBB_INVALID_INSTANCE;
        PBB_CURR_INSTANCE_PTR () = NULL;
    }
    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbDeInitMemPools                             */
/*                                                                           */
/*    Description         : This function deletes all the mem pools.         */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : gPbbMem                                   */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
PbbDeInitMemPools (VOID)
{

    if (gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbIsidVipInfoPoolId !=
        PBB_INIT_VAL)
    {
        PBB_DELETE_MEM_POOL (gPbbGlobData.PbbTaskInfo.PbbPoolIds.
                             PbbIsidVipInfoPoolId);
        gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbIsidVipInfoPoolId = PBB_INIT_VAL;
    }
    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);

}

/************************************************************************/
/* Function Name    : PbbDeInitGlobalInfo                              */
/*                                                                      */
/* Description      : Deinitializes Global Task Info                    */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
INT1
PbbDeInitGlobalInfo (VOID)
{
    UINT4               u4Context = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        gPbbGlobData.au1PbbStatus[u4Context] = PBB_DISABLED;

    }

    gPbbGlobData.au1PbbShutDownStatus = PBB_SNMP_TRUE;
    /* Deinitialise all protocol related memory pools */
    PbbDeInitMemPools ();
    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/************************************************************************/
/* Function Name    : PbbDeInitializeContext                            */
/*                                                                      */
/* Description      : Deinitializes of Context                          */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
PbbDeInitializeContext (VOID)
{
    UINT4               u4Context = PBB_INIT_VAL;

    for (u4Context = PBB_INIT_VAL; u4Context < PBB_MAX_CONTEXTS; u4Context++)
    {
        if (PBB_CONTEXT_PTR (u4Context) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4Context);
            PBB_RELEASE_BUF (PBB_CONTEXT_INFO, PBB_CURR_CONTEXT_PTR ());
            PBB_CONTEXT_PTR (u4Context) = NULL;
            PBB_CURR_CONTEXT_PTR () = NULL;
            gPbbGlobData.au1PbbStatus[u4Context] = PBB_DISABLED;
        }

    }
    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
}

/************************************************************************/
/* Function Name    : PbbDeInitializeInstance                           */
/*                                                                      */
/* Description      : Deinitializes of Instance                         */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None                                              */
/************************************************************************/
VOID
PbbDeInitializeInstance (VOID)
{
    UINT4               u4Instance = PBB_INIT_VAL;

    for (u4Instance = PBB_INIT_VAL; u4Instance < PBB_MAX_INSTANCES;
         u4Instance++)
    {
        if (PBB_INSTANCE_PTR (u4Instance) != NULL)
        {
            PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4Instance);
            PBB_RELEASE_BUF (PBB_INSTANCE_BUFF, PBB_CURR_INSTANCE_PTR ());
            PBB_INSTANCE_PTR (u4Instance) = NULL;
            PBB_CURR_INSTANCE_PTR () = NULL;
        }
    }
    PBB_TRC_ARG2 (INIT_SHUT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbSelectContext                                */
/*                                                                           */
/*    Description         : This function switches to given context          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : PBB_CURR_CONTEXT_PTR (), PBB_CONTEXT_PTR.  */
/*                                                                           */
/*    Global Variables Modified : PBB_CURR_CONTEXT_PTR ().                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / PBB_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PbbSelectContext (UINT4 u4ContextId)
{
    if (u4ContextId >= PBB_MAX_CONTEXTS)
    {
        PBB_TRC_ARG4 (MGMT_TRC, "%s :: %s() :: fails since input context id"
                      ": %d is greater than maximum number of contexts(%d)\n",
                      __FILE__, PBB_FUNCTION_NAME, u4ContextId,
                      PBB_MAX_CONTEXTS);
        return PBB_FAILURE;
    }
    PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4ContextId);

    if (PBB_CURR_CONTEXT_PTR () == NULL)
    {
        PBB_TRC_ARG3 (MGMT_TRC, "%s :: %s() :: fails since context id"
                      ": %d is not created in PBB\n",
                      __FILE__, PBB_FUNCTION_NAME, u4ContextId);
        return PBB_FAILURE;
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbReleaseContext                               */
/*                                                                           */
/*    Description         : This function makes the switched context to NULL */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : PBB_CURR_CONTEXT_PTR ().                  */
/*                                                                           */
/*    Global Variables Modified : PBB_CURR_CONTEXT_PTR ().                  */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                       .                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbReleaseContext (VOID)
{
    PBB_CURR_CONTEXT_PTR () = NULL;
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbHandleCreateContext                          */
/*                                                                           */
/*    Description         : This function creates context, and initalises the*/
/*                          per context information                          */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / PBB_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PbbHandleCreateContext (UINT4 u4ContextId)
{
    UINT4               u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    /* allocate context pointer from memory pool, and store it in the 
       context pointers array */
    PBB_CURR_CONTEXT_PTR () = (tPbbContextInfo *) (VOID *)
        PBB_GET_BUF (PBB_CONTEXT_INFO, sizeof (tPbbContextInfo));
    if (PBB_CURR_CONTEXT_PTR () == NULL)
    {
        PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC | BUFFER_TRC | OS_RESOURCE_TRC,
                      "%s :: %s() :: Failed since context buffer allocation "
                      "failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    PBB_CONTEXT_PTR (u4ContextId) = PBB_CURR_CONTEXT_PTR ();
    PBB_CURR_CONTEXT_PTR ()->u4ContextId = u4ContextId;
    PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent = PBB_INIT_VAL;
    PBB_CURR_CONTEXT_PTR ()->u4PbbDbg = PBB_INIT_VAL;
    PBB_CURR_CONTEXT_PTR ()->u4BridgeMode = PBB_INVALID_BRIDGE_MODE;
    PBB_CURR_CONTEXT_PTR ()->u2InternalCbpCount = PBB_INIT_VAL;
    PBB_CURR_CONTEXT_PTR ()->u4InstanceId = PBB_INVALID_INSTANCE;
    PBB_CURR_CONTEXT_PTR ()->u2PisidCount = PBB_INIT_VAL;

    PBB_MEMSET (PBB_CURR_CONTEXT_STR (), PBB_INIT_VAL,
                PBB_CONTEXT_ALIAS_LEN + 1);
    gPbbGlobData.au1PbbStatus[u4ContextId] = PBB_ENABLED;

    if (PbbVcmGetSystemModeExt (PBB_PROTOCOL_ID) == VCM_MI_MODE)
    {
        PbbVcmGetAliasName (PBB_CURR_CONTEXT_ID (), PBB_CURR_CONTEXT_STR ());
        STRCAT (PBB_CURR_CONTEXT_STR (), ":");
    }

    if (PbbInit () != PBB_SUCCESS)
    {
        PbbDeInit ();
        PBB_RELEASE_BUF (PBB_CONTEXT_INFO, (UINT1 *) PBB_CURR_CONTEXT_PTR ());
        PBB_CURR_CONTEXT_PTR () = NULL;
        PBB_CONTEXT_PTR (u4ContextId) = PBB_CURR_CONTEXT_PTR ();
        PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed since PbbInit()failed "
                      "\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (PbbL2IwfGetBridgeMode (u4ContextId, &u4BridgeMode) == PBB_SUCCESS)
    {
        if (!((u4BridgeMode == PBB_CUSTOMER_BRIDGE_MODE) ||
              (u4BridgeMode == PBB_PROVIDER_BRIDGE_MODE) ||
              (u4BridgeMode == PBB_PROVIDER_EDGE_BRIDGE_MODE) ||
              (u4BridgeMode == PBB_PROVIDER_CORE_BRIDGE_MODE) ||
              (u4BridgeMode == PBB_INVALID_BRIDGE_MODE)))
        {
            PBB_CURR_CONTEXT_PTR ()->u4InstanceId = PBB_DEF_INSTANCE_ID;
            PBB_CURR_CONTEXT_PTR ()->u4BridgeMode = u4BridgeMode;
            if (PbbCreateContextEntryDll (PBB_DEF_INSTANCE_ID,
                                          u4ContextId) != PBB_SUCCESS)
            {
                /* Failed while adding the context id in the Context List    */
                /* for default instance                                      */
                return PBB_FAILURE;
            }
            if (PbbGetAllPorts (u4ContextId) != PBB_SUCCESS)
            {
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: Failed since PbbGetAllPorts()failed "
                              "\n", __FILE__, PBB_FUNCTION_NAME);
                return PBB_FAILURE;
            }

            PbbCreateCompBridgeEcfmFilter ();
            if (u4BridgeMode == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                PbbBrgHwCreateAstControlPktFilter ();
                gPbbGlobData.PbbTaskInfo.u4NumberofIcomp++;
                if (PBB_CURR_CONTEXT_PTR ()->u4InstanceId !=
                    PBB_INVALID_INSTANCE)
                {
                    PbbIncrInstanceNumOfIcomp ();
                }
            }

            if (u4BridgeMode == PBB_BCOMPONENT_BRIDGE_MODE)
            {
                gPbbGlobData.PbbTaskInfo.u4NumberofBcomp++;
                if (PBB_CURR_CONTEXT_PTR ()->u4InstanceId !=
                    PBB_INVALID_INSTANCE)
                {
                    PbbIncrInstanceNumOfBcomp ();
                }
            }
        }
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbHandleDeleteContext                          */
/*                                                                           */
/*    Description         : This function deletes given context              */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / PBB_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PbbHandleDeleteContext (UINT4 u4ContextId)
{

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s :: PBB context %d selection failed   \n",
                      __FILE__, PBB_FUNCTION_NAME, u4ContextId);
        return PBB_FAILURE;
    }
    if ((PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE))
    {
        PbbBrgHwDestroyAstControlPktFilter ();
        PbbDeleteCompBridgeEcfmFilter ();
        if (PbbDeleteICompPisiddataFromHw (u4ContextId) != PBB_SUCCESS)
        {
        }
        if (PbbDeleteICompdataFromHw (u4ContextId) != PBB_SUCCESS)
        {
        }
        gPbbGlobData.PbbTaskInfo.u4NumberofIcomp--;
        if (PBB_CURR_CONTEXT_PTR ()->u4InstanceId != PBB_INVALID_INSTANCE)
        {
            PbbDecrInstanceNumOfIcomp ();
            if (PbbDeleteContextNodeFromDLL
                (PBB_CURR_CONTEXT_PTR ()->u4InstanceId,
                 PBB_CURR_CONTEXT_PTR ()->u4ContextId) == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
        }
    }
    else if ((PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE))
    {
        PbbDeleteCompBridgeEcfmFilter ();

        if (PbbDeleteBCompdataFromHw (u4ContextId) != PBB_SUCCESS)
        {
        }
        gPbbGlobData.PbbTaskInfo.u4NumberofBcomp--;
        if (PBB_CURR_CONTEXT_PTR ()->u4InstanceId != PBB_INVALID_INSTANCE)
        {
            PbbDecrInstanceNumOfBcomp ();
            if (PbbDeleteContextNodeFromDLL
                (PBB_CURR_CONTEXT_PTR ()->u4InstanceId,
                 PBB_CURR_CONTEXT_PTR ()->u4ContextId) == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
        }
    }
    PbbDeInit ();
    MEMSET (PBB_CURR_CONTEXT_PTR (), PBB_INIT_VAL, sizeof (tPbbContextInfo));
    PBB_RELEASE_BUF (PBB_CONTEXT_INFO, PBB_CURR_CONTEXT_PTR ());
    PBB_CURR_CONTEXT_PTR () = NULL;
    PBB_CONTEXT_PTR (u4ContextId) = PBB_CURR_CONTEXT_PTR ();
    gPbbGlobData.au1PbbStatus[u4ContextId] = PBB_DISABLED;
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbHandleUpdateContextName                      */
/*                                                                           */
/*    Description         : This function updated the context name of the    */
/*                          given context.                                   */
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Identifier                 */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / PBB_FAILURE.                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PbbHandleUpdateContextName (UINT4 u4ContextId)
{
    MEMSET (PBB_CURR_CONTEXT_STR (), PBB_INIT_VAL, PBB_CONTEXT_ALIAS_LEN + 1);

    if (PbbVcmGetAliasName (u4ContextId,
                            PBB_CURR_CONTEXT_STR ()) != VCM_SUCCESS)
    {
        return PBB_FAILURE;
    }
    STRCAT (PBB_CURR_CONTEXT_STR (), ":");
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);

    return PBB_SUCCESS;
}

#ifdef SNMP_2_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbRegisterMIB                                  */
/*                                                                           */
/*    Description         : This function registers the corresponding MIB's. */
/*                          In case of SI mode, register SI mibs. Register   */
/*                          MI mibs in case of SI as well as in MI mode.     */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified :                                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
PbbRegisterMIB (VOID)
{
    RegisterFS1AH ();
    RegisterFSPBB ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbUnRegisterMIB                                  */
/*                                                                           */
/*    Description         : This function Unregisters the corresponding 
 *                          MIB's. 
 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred :                                            */
/*                                                                           */
/*    Global Variables Modified :                                            */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
PbbUnRegisterMIB (VOID)
{
    UnRegisterFS1AH ();
    UnRegisterFSPBB ();
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
}
#endif

/*****************************************************************************/
/* Function Name      : PbbHandleStartModule                                */
/*                                                                           */
/* Description        : This function will start PBB module                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE.                         */
/*****************************************************************************/
INT1
PbbHandleStartModule (VOID)
{

    if (PbbInitGlobalInfo () == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed since PbbInitGlobalInfo()failed "
                      "\n", PBB_FILE_NAME, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (PbbInit () == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed since PbbInit()failed "
                      "\n", PBB_FILE_NAME, PBB_FUNCTION_NAME);
        PbbDeInitializeInstance ();
        PbbDeInitializeContext ();
        PbbDeInitGlobalInfo ();
        return PBB_FAILURE;
    }

    /* release context info */
    PbbReleaseContext ();
    PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC,
                  "%s :: %s() :: returns successfully\n", PBB_FILE_NAME,
                  PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbInitMemPools                               */
/*                                                                           */
/*    Description         : Creates memory pool for ISID,port,ISID+port,VIP  */
/*                         tables.                                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS on success                           */
/*                         PBB_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbInitMemPools (VOID)
{
    tMemPoolId          u4MemPoolId;
    PBB_MEMSET (&u4MemPoolId, PBB_INIT_VAL, sizeof (tMemPoolId));

    /*memory pool created to store ISID and VIP info data base */
    if (PBB_CREATE_MEM_POOL
        (sizeof (tPbbIsidVipInfo),
         gFsPbbSizingParams[PBB_ISID_VIP_INFO_SIZING_ID].u4PreAllocatedUnits,
         MEM_DEFAULT_MEMORY_TYPE, &u4MemPoolId) == MEM_FAILURE)
    {
        PBB_TRC_ARG2 ((OS_RESOURCE_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC),
                      "%s :: %s() :: Failed to create memory pool for Vip-Isid"
                      " information database \n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;

    }
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbIsidVipInfoPoolId = u4MemPoolId;
    PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC,
                  "%s :: %s() :: returns successfully\n", __FILE__,
                  PBB_FUNCTION_NAME);

    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbTableDeInit                                */
/*                                                                           */
/*    Description         : Deletes the RB tree data structures for          */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS on success                           */
/*                         PBB_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
VOID
PbbTableDeInit (VOID)
{

    if (PBB_CURR_CONTEXT_PTR ()->IsidTable != NULL)
    {
        PbbDeleteIsidRBTree ();
        PBB_CURR_CONTEXT_PTR ()->IsidTable = NULL;
    }
    if (PBB_CURR_CONTEXT_PTR ()->IsidPortTable != NULL)
    {
        PbbDeleteIsidPortRBTree ();
        PBB_CURR_CONTEXT_PTR ()->IsidPortTable = NULL;
    }

    if (PBB_CURR_CONTEXT_PTR ()->PortTable != NULL)
    {
        PbbDeletePortRBTree ();
        PBB_CURR_CONTEXT_PTR ()->PortTable = NULL;
    }

    if (PBB_CURR_CONTEXT_PTR ()->VipTable != NULL)
    {
        PbbDeleteVipRBTree ();
        PBB_CURR_CONTEXT_PTR ()->VipTable = NULL;
    }
    PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC,
                  "%s :: %s() :: returns successfully\n", __FILE__,
                  PBB_FUNCTION_NAME);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbDeInit                                        */
/*                                                                           */
/*    Description         : Calls function to delete data structures         */
/*                          initialised for PBB tables,                      */
/*                          ISID Table,ISID Port Table, Port Table, VIP table*/
/*                          IComp Table.                                      */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS on success                           */
/*                         PBB_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
VOID
PbbDeInit (VOID)
{
    PbbTableDeInit ();
    return;
    PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC,
                  "%s :: %s() :: returns successfully\n", __FILE__,
                  PBB_FUNCTION_NAME);
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbTableInit                                  */
/*                                                                           */
/*    Description         : Initialises the RB tree data structures for tables*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS on success                           */
/*                         PBB_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbTableInit (VOID)
{

    PBB_CURR_CONTEXT_PTR ()->IsidTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPbbIsidRBtreeNode, RBNode),
                              (tRBCompareFn) PbbCompareIsidRBNodes);

    if (PBB_CURR_CONTEXT_PTR ()->IsidTable == NULL)
    {
        PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC, "%s :: %s() :: failed since "
                      "Isid Rb tree creation failed\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_CURR_CONTEXT_PTR ()->IsidPortTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPbbBackBoneSerInstEntry, RBNode),
                              (tRBCompareFn) PbbCompareIsidPortRBNodes);

    if (PBB_CURR_CONTEXT_PTR ()->IsidPortTable == NULL)
    {
        PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC, "%s :: %s() :: failed since "
                      "Isid-Port Rb tree creation failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_CURR_CONTEXT_PTR ()->PortTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPbbPortRBtreeNode, RBNode),
                              (tRBCompareFn) PbbComparePortRBNodes);

    if (PBB_CURR_CONTEXT_PTR ()->PortTable == NULL)
    {
        PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC, "%s :: %s() :: failed since "
                      "Port Rb tree creation failed\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_CURR_CONTEXT_PTR ()->VipTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tPbbICompVipRBtreeNode, RBNode),
                              (tRBCompareFn) PbbCompareVipRBNodes);

    if (PBB_CURR_CONTEXT_PTR ()->VipTable == NULL)
    {
        PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC, "%s :: %s() :: failed since "
                      "Vip Rb tree creation failed\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC,
                  "%s :: %s() :: returns successfully\n", __FILE__,
                  PBB_FUNCTION_NAME);
    return PBB_SUCCESS;

}

#ifdef NPAPI_WANTED
/***************************************************************************/
/* Function Name    : PbbHandleCopyPortPropertiesToHw ()                  */
/*                                                                         */
/* Description      : This function programs the hardware with all         */
/*                    the Port properties for the port "u2DstPort".        */
/*                    The properties to be copied to "u2DstPort" is        */
/*                    obtained from "u2Port".                              */
/*                    "u2SrcPort" must have been created in Pbb. This     */
/*                    function does not program the Dynamic Pbb and Mcast */
/*                    membership and hence must not be called for ports    */
/*                    that are in OPER UP state. Dynamic Learning is NOT   */
/*                    done when the port is in OPER DOWN state.            */
/*                                                                         */
/* Input(s)         : u2DstPort - The Port whose properties have to be     */
/*                                programmed in the Hardware.              */
/*                                                                         */
/*                  : u2SrcPort - The Port from where the port properties  */
/*                                have to be obtained.                     */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : PBB_SUCCESS on success,                             */
/*                    PBB_FAILURE otherwise.                              */
/***************************************************************************/
INT1
PbbHandleCopyPortPropertiesToHw (UINT2 u2DstPort, UINT2 u2SrcPort)
{
    tPbbIsidPipVipNode *pCurEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    tMacAddr            DestMac;
    UINT4               u4Pisid = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    INT4                i4VipIfIndex = PBB_INIT_VAL;
    PBB_MEMSET (DestMac, PBB_INIT_VAL, sizeof (tMacAddr));

    UNUSED_PARAM (u2SrcPort);
    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {

        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }
    /*Checking for bridge mode type */
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return SNMP_FAILURE;
    }

    key.u2LocalPort = u2SrcPort;
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    /* Get the destination port If index */
    if (PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                       u2DstPort, &i4IfIndex) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if ((pRBCurNode->u1PortType == PBB_CNP_PORTBASED_PORT) ||
        (pRBCurNode->u1PortType == PBB_CNP_STAGGED_PORT) ||
        (pRBCurNode->u1PortType == PBB_CNP_CTAGGED_PORT))
    {
        u4Pisid = pRBCurNode->unPbbPortData.CnpData.u4Pisid;
        if (pRBCurNode->unPbbPortData.CnpData.u1PisidRowStatus == PBB_ACTIVE)
        {
            /* Copy the Source port Pisid for Destination port */
            if (PbbSetHwPortPisid (PBB_CURR_CONTEXT_ID (),
                                   i4IfIndex, u4Pisid) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
        }
    }

    else if (pRBCurNode->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.CbpData.IsidList),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbIsidPipVipNode *) pLstNode;
            u4Isid = pCurEntry->unIsidVip.u4Isid;

            if (PbbValidateIsidPort (u4Isid,
                                     (INT4) pRBCurNode->u4IfIndex)
                != PBB_SUCCESS)
            {
                continue;
            }

            if (PbbSetBCompHwServiceInstOnDest (PBB_CURR_CONTEXT_ID (),
                                                u4Isid,
                                                (INT4) pRBCurNode->u4IfIndex,
                                                i4IfIndex) != PBB_SUCCESS)
            {
                continue;
            }
        }
    }
    else if (pRBCurNode->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
    {
        PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.PipData.VipList),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbIsidPipVipNode *) pLstNode;
            u2Vip = pCurEntry->unIsidVip.u2Vip;

            if (PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                               u2Vip,
                                               &i4VipIfIndex) == PBB_FAILURE)
            {
                continue;
            }
            if (PbbValidateVip (i4VipIfIndex) != PBB_SUCCESS)
            {
                continue;
            }
            if (PbbGetVipISid (i4VipIfIndex, &u4Isid) == PBB_FAILURE)
            {
                u4Isid = PBB_ISID_INVALID_VALUE;
            }
            if (PbbHwAddVipToPip (PBB_CURR_CONTEXT_ID (),
                                  u4Isid, u2Vip, u2DstPort) != PBB_SUCCESS)
            {
                continue;
            }
        }
        if (PbbHwSetPipPcpAttributesOnDest (PBB_CURR_CONTEXT_ID (),
                                            u2SrcPort,
                                            i4IfIndex) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/***************************************************************************/
/* Function Name    : PbbHandleRemovePortPropertiesFromHw ()               */
/*                                                                         */
/* Description      : This function removes the hardware with all          */
/*                    the Port properties for the port "u2DstPort".        */
/*                    The properties to be removed from "u2DstPort" is     */
/*                    obtained from "u2Port".                              */
/*                                                                         */
/* Input(s)         : u4DstPort - The Port whose properties have to be     */
/*                                removed in the Hardware.                 */
/*                                                                         */
/*                  : u4SrcPort - The Port from where the port properties  */
/*                                have to be obtained.                     */
/*                                                                         */
/* Output(s)        : None.                                                */
/*                                                                         */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                         */
/* Use of Recursion : None.                                                */
/*                                                                         */
/* Returns          : PBB_SUCCESS on success,                             */
/*                    PBB_FAILURE otherwise.                              */
/***************************************************************************/
INT1
PbbHandleRemovePortPropertiesFromHw (UINT4 u4DstPort, UINT4 u4SrcPort)
{

    tPbbIsidPipVipNode *pCurEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tPbbPortRBtreeNode *pRBCurNode;
    tPbbPortRBtreeNode  key;
    UINT4               u4Pisid = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;
    UINT2               u2SrcPort = PBB_INIT_VAL;
    INT4                i4VipIfIndex = PBB_INIT_VAL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));

    if (PBB_IS_MODULE_INITIALISED () == PBB_FALSE)
    {

        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    /* Check the shutdown Status */
    if (gPbbGlobData.au1PbbShutDownStatus == PBB_SNMP_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System ShutDown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    /*Checking for bridge mode type */
    if (PBB_PROVIDER_BACKBONE_BRIDGE () == PBB_FALSE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_BRIDGE_MODE);
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: failed since bridge Mode is not PBB\n",
                      __FILE__, PBB_FUNCTION_NAME);
        CLI_SET_ERR (CLI_PBB_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }

    if (PbbVcmGetIfMapHlPortId (u4SrcPort, &u2SrcPort) == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    key.u2LocalPort = u2SrcPort;
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    if ((pRBCurNode->u1PortType == PBB_CNP_PORTBASED_PORT) ||
        (pRBCurNode->u1PortType == PBB_CNP_STAGGED_PORT) ||
        (pRBCurNode->u1PortType == PBB_CNP_CTAGGED_PORT))
    {
        if (pRBCurNode->unPbbPortData.CnpData.u1PisidRowStatus == PBB_ACTIVE)
        {
            u4Pisid = pRBCurNode->unPbbPortData.CnpData.u4Pisid;

            /* Delete the PISID for the Source Port */
            if (PbbDelHwPortPisid (PBB_CURR_CONTEXT_ID (),
                                   u4DstPort, u4Pisid) != PBB_SUCCESS)
            {
                return PBB_FAILURE;

            }
        }
    }
    else if (pRBCurNode->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.CbpData.IsidList),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbIsidPipVipNode *) pLstNode;
            u4Isid = pCurEntry->unIsidVip.u4Isid;

            if (PbbValidateIsidPort (u4Isid, u4SrcPort) != PBB_SUCCESS)
            {
                continue;
            }

            /* Delete it from the Destination Port */
            if (PbbDelBCompHwServiceInst (PBB_CURR_CONTEXT_ID (),
                                          u4DstPort, u4Isid) != PBB_SUCCESS)
            {
                continue;
            }
        }

    }
    else if (pRBCurNode->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
    {
        PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.PipData.VipList),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbIsidPipVipNode *) pLstNode;
            u2Vip = pCurEntry->unIsidVip.u2Vip;

            if (PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                               u2Vip,
                                               &i4VipIfIndex) == PBB_FAILURE)
            {
                continue;
            }
            if (PbbValidateVip (i4VipIfIndex) != PBB_SUCCESS)
            {
                continue;
            }
            if (PbbHwDeleteVipToPip (PBB_CURR_CONTEXT_ID (),
                                     u4Isid,
                                     (UINT4) u2Vip,
                                     pRBCurNode->u2LocalPort) != PBB_SUCCESS)
            {
                continue;
            }
        }
    }
    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;

}
#endif

/*****************************************************************************/
/* Function Name      : PbbSetPortStatus                                     */
/*                                                                           */
/* Description        : This function will set the status of port            */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE.                           */
/*****************************************************************************/
INT1
PbbSetPortStatus (UINT4 u4Context, INT4 i4IfIndex, UINT1 u1PortStatus)
{
    tCfaIfInfo          CfaIfInfo;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbSelectContext (u4Context) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (PbbCfaGetIfInfo (i4IfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: Failed to"
                      " get Cfa Interface Parameters.\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    if (PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (CfaIfInfo.u1BrgPortType == PBB_VIRTUAL_INSTANCE_PORT)
    {
        if (PbbSetVipPortStatus (u2LocalPort, u1PortStatus) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        else
        {
            return PBB_SUCCESS;
        }
    }
    if (PbbValidatePort (i4IfIndex) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (PbbSetPortRBtreeStatus (u2LocalPort, u1PortStatus) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (CfaIfInfo.u1BrgPortType == PBB_PROVIDER_INSTANCE_PORT)
    {
        /* Set the Oper Status of VIP mapped to the PIP */
        PbbSetPipVipListOperStatus (u4Context, u2LocalPort, u1PortStatus);
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbStartModule                                       */
/*                                                                           */
/* Description        : This function is is used to start the PBB module     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBB_SUCCESS / PBB_FAILURE.                         */
/*****************************************************************************/
INT1
PbbStartModule (VOID)
{
    if (PbbHandleStartModule () == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC, "%s :: %s() :: failed since"
                      "PbbHandleStartModule() failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    PBB_TRC_ARG2 (INIT_SHUT_TRC | MGMT_TRC,
                  "%s :: %s() :: returns successfully\n", __FILE__,
                  PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbPostPortCreateMessage                        */
/*                                                                           */
/*    Description         : Posts the message to PBB Config Q.              */
/*                                                                           */
/*    Input(s)            : u4ContextId -  Context Identifier                */
/*                          u4IfIndex   -  Interface index (physical)        */
/*                          u2Port    -  Port Index (local port number).     */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / VPBBFAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT4
PbbPostMapPortIndication (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort,
                          UINT2 u2MsgType)
{
    tPbbQMsg           *pPbbQMsg = NULL;

    if (PBB_IS_MODULE_INITIALISED () != PBB_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    /* If Pbb is shutdown, then don't send the port map indication to 
     * it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB is Shutdown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (NULL == pPbbQMsg)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));

    pPbbQMsg->u2MsgType = u2MsgType;
    pPbbQMsg->u4ContextId = u4ContextId;
    pPbbQMsg->i4IfIndex = i4IfIndex;
    pPbbQMsg->u2LocalPort = u2LocalPort;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Relase the buffer since Msg is already posted */
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbHandleMapPortInd                             */
/*                                                                           */
/*    Description         : Creates a Port in PBB module                    */
/*                                                                           */
/*    Input(s)            : Context, ifIndex, localPort                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbHandleMapPortInd (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort)
{
    if (PbbHandleCreatePort (u4ContextId, i4IfIndex, u2LocalPort) !=
        PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbHandleUnMapPortInd                             */
/*                                                                           */
/*    Description         : Delete   a Port in PBB module                    */
/*                                                                           */
/*    Input(s)            : Context, ifIndex, localPort                      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS                                      */
/*                         PBB_FAILURE                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

INT1
PbbHandleUnMapPortInd (UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort)
{
#if defined (L2RED_WANTED) && defined(NPAPI_WANTED)
    EvtSyncFsMiPbbDeletePort (u4ContextId, i4IfIndex);
    PBB_NPSYNC_BLK () = OSIX_TRUE;
#endif

    if (PbbHandleDeletePort (u4ContextId, i4IfIndex, u2LocalPort) !=
        PBB_SUCCESS)
    {
#if defined (L2RED_WANTED) && defined(NPAPI_WANTED)
        if (PBB_NODE_STATUS () != PBB_NODE_IDLE)
        {
            PBB_NPSYNC_BLK () = OSIX_FALSE;
        }
#endif
        return PBB_FAILURE;
    }

#if defined (L2RED_WANTED) && defined(NPAPI_WANTED)
    if (PBB_NODE_STATUS () != PBB_NODE_IDLE)
    {
        PBB_NPSYNC_BLK () = OSIX_FALSE;
    }
#endif

    PBB_TRC_ARG2 (MGMT_TRC, "%s :: %s() :: returns successfully\n",
                  __FILE__, PBB_FUNCTION_NAME);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbPostCfgMessage                                */
/*                                                                           */
/*    Description         : Posts the message to Pbb Config Q.              */
/*                                                                           */
/*    Input(s)            : u1MsgType -  Meesage Type                        */
/*                          u2Port    -  Port Index.                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / PBB_FAILURE                       */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbPostUnMapPortIndication (UINT2 u2MsgType, INT4 i4IfIndex)
{
    tPbbQMsg           *pPbbQMsg = NULL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PBB_IS_MODULE_INITIALISED () != PBB_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    /* If Pbb is shutdown, then don't send the port unmap indication to 
     * it. */

    if (L2IwfGetPbbShutdownStatus () == L2IWF_TRUE)
    {
        /* PBB is shutdown. */
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB is Shutdown\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    if (PbbVcmGetContextInfoFromIfIndex (i4IfIndex,
                                         &u4ContextId,
                                         &u2LocalPort) == PBB_FAILURE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: Failed\n",
                      __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (NULL == pPbbQMsg)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));
    pPbbQMsg->u2MsgType = u2MsgType;
    pPbbQMsg->u4ContextId = u4ContextId;
    pPbbQMsg->i4IfIndex = i4IfIndex;
    pPbbQMsg->u2LocalPort = u2LocalPort;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);
        /* No Need to Relase the buffer since Msg is already posted */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbPostVlanAuditStatusIndic                      */
/*                                                                           */
/*    Description         : Posts the Vlan Audit Status message to Pbb .     */
/*                                                                           */
/*    Input(s)            : u1MsgType -  Meesage Type                        */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS / PBB_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbPostVlanAuditStatusIndic (tPbbVlanAuditStatus * pPbbVlanStatus)
{
    tPbbQMsg           *pPbbQMsg = NULL;

    if (PBB_IS_MODULE_INITIALISED () != PBB_TRUE)
    {
        PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: PBB System Not Initialized\n", __FILE__,
                      PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }

    pPbbQMsg =
        (tPbbQMsg *) (VOID *) PBB_GET_BUF (PBB_QMSG_BUFF, sizeof (tPbbQMsg));

    if (NULL == pPbbQMsg)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | BUFFER_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        return PBB_FAILURE;
    }

    PBB_MEMSET (pPbbQMsg, PBB_INIT_VAL, sizeof (tPbbQMsg));
    pPbbQMsg->u2MsgType = PBB_VLAN_AUDIT_STATUS_IND;
    pPbbQMsg->u4VlanAudStatus = pPbbVlanStatus->u4VlanAudStatus;
    pPbbQMsg->u4ContextId = pPbbVlanStatus->u4ContextId;

    if (PBB_SEND_TO_QUEUE (PBB_CFG_QUEUE_ID,
                           (UINT1 *) &pPbbQMsg,
                           OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        PBB_RELEASE_BUF (PBB_QMSG_BUFF, pPbbQMsg);

        return PBB_FAILURE;
    }

    if (PBB_SEND_EVENT (PBB_TASK_ID, PBB_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBB_TRC_ARG2 (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                      "%s :: %s() :: Failed\n", __FILE__, PBB_FUNCTION_NAME);

        /* No Need to Relase the buffer since Msg is already posted */
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbMainAssignMempoolIds                              */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's                                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
PbbMainAssignMempoolIds (VOID)
{
/*tPbbQMsg*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbQMsgPoolId =
        PBBMemPoolIds[MAX_PBB_Q_MESG_SIZING_ID];

/*tPbbContextInfo*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbContextPoolId =
        PBBMemPoolIds[MAX_PBB_CONTEXT_INFO_SIZING_ID];

/*tPbbBufferEntry*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbRedBuffMemPoolId =
        PBBMemPoolIds[MAX_PBB_RED_BUFFER_ENTRIES_SIZING_ID];

/*tPbbIsidRBtreeNode*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbIsidPoolId =
        PBBMemPoolIds[MAX_PBB_ISID_RBTREE_NODES_SIZING_ID];

/*tPbbBackBoneSerInstEntry*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbIsidPortPoolId =
        PBBMemPoolIds[MAX_PBB_BACKBONE_SERV_INST_ENTRIES_SIZING_ID];

/*tPbbPortRBtreeNode*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbPortPoolId =
        PBBMemPoolIds[MAX_PBB_PORT_RBTREE_NODES_SIZING_ID];

/*tPbbICompVipRBtreeNode*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbVipPoolId =
        PBBMemPoolIds[MAX_PBB_ICOMP_VIP_RBTREE_NODES_SIZING_ID];

/*tPbbCbpPortList*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbCBPDllPoolId =
        PBBMemPoolIds[MAX_PBB_CBP_PORT_LIST_COUNT_SIZING_ID];

/*tPbbCnpPortList*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbCnpDllPoolId =
        PBBMemPoolIds[MAX_PBB_CNP_PORT_LIST_COUNT_SIZING_ID];

/*tPbbIsidPipVipNode*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbPipVipMapPoolId =
        PBBMemPoolIds[MAX_PBB_ISID_PIP_VIP_NODES_SIZING_ID];

/*tPbbPcpData*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbPipPcpPoolId =
        PBBMemPoolIds[MAX_PBB_PCP_DATA_COUNT_SIZING_ID];

/*tPbbInstanceInfo*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbInstancePoolId =
        PBBMemPoolIds[MAX_PBB_INSTANCE_INFO_SIZING_ID];

/*tPbbContextNode*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbContextDllPoolId =
        PBBMemPoolIds[MAX_PBB_CONTEXTS_SIZING_ID];

/*tCbpStatusInfo*/
    gPbbGlobData.PbbTaskInfo.PbbPoolIds.PbbCbpStatusMemPoolId =
        PBBMemPoolIds[MAX_PBB_PORTS_PER_ISID_ENTRIES_SIZING_ID];
}

#endif
