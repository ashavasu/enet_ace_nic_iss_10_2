/*******************************************************************
 * Copyright (C) 2008 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbdata.c,v 1.48 2014/03/19 13:36:12 siva Exp $
 *
 * Description: This file contains data management functions for 
 *              PBB moodule.
 *
 *******************************************************************/

#include "pbbinc.h"
#include "fs1ahcli.h"
#include "fspbbcli.h"

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteVipLocalNodeDll                             */
/*                                                                          */
/*    Description        : This function is used to delete Vip Node from DLL*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteVipLocalNodeDll (UINT2 u2Vip, UINT2 u2LocalPort)
{
    tPbbIsidPipVipNode *pListNode = NULL;
    tPbbIsidPipVipNode *pVipDllNode = NULL;
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;

    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }

    PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.PipData.VipList),
                  pListNode, tPbbIsidPipVipNode *)
    {
        pVipDllNode = (tPbbIsidPipVipNode *) pListNode;
        if (PbbCompareVipEntry (&u2Vip,
                                &(pVipDllNode->unIsidVip.u2Vip)) == PBB_EQUAL)
        {
            PbbDelVipEntryDll (&(pVipDllNode->link), u2LocalPort);
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************
 Function    :  PbbSetVipLocalPipRowStatus
 Description :  To set the Pip row status
 Input       :  Component Id
                Vip
                Row Status                
 Output      :  None.
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbSetVipLocalPipRowStatus (UINT4 u4ContextId, INT4 i4IfIndex,
                            UINT2 u2Vip, UINT4 i4VipPipRowStatus)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UNUSED_PARAM (i4VipPipRowStatus);
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4ContextId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }

    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;

    u2LocalPort = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
    pIsidVipInfo->unPbbCompData.ICompData.u2Pip = PBB_DEFAULT_PORT_VAL;
    pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus =
        PBB_INVALID_ROWSTATUS;

    /* Reset this Vip from the Pip Vip Map */
    if (PbbValidatePipPortExists (u2LocalPort) == PBB_SUCCESS)
    {
        if (PbbResetVipInPip (u2Vip, u2LocalPort) == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }
        u4Isid = pIsidVipInfo->u4Isid;
        /* Reset this vip pip map at H/W */
        if (PbbHwDeleteLocalVipToPip
            (u4ContextId, i4IfIndex, u4Isid, u2LocalPort) == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipISidonLocal                             */
/*                                                                          */
/*    Description        : This function is used to get Isid assciated       */
/*                             with VIP On Local Port                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipIsidonLocal (UINT2 u2LocalPort, UINT4 *pu4Isid)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    *pu4Isid = pIsidVipInfo->u4Isid;

    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbDeletePortInfo
*/
/*    Description         : This is a function to delete the port related
                            information from PBB module
*/
/*    Input(s)            : Interface Index Value
*/
/*    Output(s)           : None
*/
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/
INT1
PbbDeletePortInfo (INT4 i4IfIndex, UINT2 u2LocalPort, UINT4 u4ContextId)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbIsidPipVipNode *pIsidListMapNode = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;
    UINT4               u4PisidRowStatus = PBB_INVALID_ROWSTATUS;

    if (PbbValidateLocalVip (u2LocalPort) == PBB_SUCCESS)
    {
        if (PbbSetVipLocalPipRowStatus (u4ContextId, i4IfIndex,
                                        u2LocalPort,
                                        PBB_DESTROY) != PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_INVALID_VIP);
            PbbReleaseContext ();
            return PBB_FAILURE;
        }
        if (PbbGetVipIsidonLocal (u2LocalPort, &u4Isid) != PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_INVALID_VIP);
            PbbReleaseContext ();
            return PBB_FAILURE;

        }
        if (u4Isid != PBB_ISID_INVALID_VALUE)
        {
            if (PbbDelICompHwServiceInst (u4ContextId, i4IfIndex) !=
                PBB_SUCCESS)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_VIP);
                PbbReleaseContext ();
                return PBB_FAILURE;

            }
        }
        if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }

        if (PbbDelVipInfo (u2LocalPort) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        return PBB_SUCCESS;
    }
    else
    {
        PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
        if (pPortEntry == NULL)
        {
            return PBB_FAILURE;
        }
        if (pPortEntry->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
        {
            /* Remove the Pip related information from the Port table */
            i1retVal = PbbRemovePipVipMaps (pPortEntry, u4ContextId);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            PbbFreePipPcpTable ((tPbbPcpData *)
                                (pPortEntry->unPbbPortData.PipData.pu4PcpData),
                                1);

            if (PbbHwSetProviderBridgePortType (u4ContextId,
                                                i4IfIndex,
                                                PBB_CUSTOMER_BRIDGE_PORT) !=
                PBB_SUCCESS)
            {
                PBB_TRC_ARG2 (MGMT_TRC | ALL_FAILURE_TRC,
                              "%s :: %s() :: failed "
                              "setting port type at H/W\n", __FILE__,
                              PBB_FUNCTION_NAME);
                return PBB_FAILURE;
            }
        }
        else if (pPortEntry->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
        {
            PBB_DLL_SCAN (&(pPortEntry->unPbbPortData.CbpData.IsidList),
                          pIsidListMapNode, tPbbIsidPipVipNode *)
            {
                /* Remove the corresponding Isid Port entry */
                u4Isid = pIsidListMapNode->unIsidVip.u4Isid;

                /* Delete the CBP service mapping table entry */
                i1retVal = PbbDelCBPMappingRowStatus (u4ContextId,
                                                      u4Isid,
                                                      i4IfIndex, u2LocalPort);

                if (i1retVal == PBB_SUCCESS)
                {
                    pIsidListMapNode = NULL;
                }

                if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
                {
                    return PBB_FAILURE;
                }
            }
        }
        else
        {
            /* The port-type is CNP */
            if (PbbGetPortPisidRowStatus (i4IfIndex,
                                          &u4PisidRowStatus) == PBB_SUCCESS)
            {
                if (u4PisidRowStatus != PBB_INVALID_ROWSTATUS)
                {
                    i1retVal = nmhSetFsPbbPIsidRowStatus (u4ContextId,
                                                          i4IfIndex,
                                                          PBB_DESTROY);
                    if (i1retVal == PBB_FAILURE)
                    {
                        return PBB_FAILURE;
                    }
                    if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
                    {
                        return PBB_FAILURE;
                    }

                }
            }

        }

    }
    /* Remove the PIP / CBP / CNP port entry from Port Table */
    if (PbbDeletePortRBNode (u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    UNUSED_PARAM (u4ContextId);
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbRemovePipVipMaps 
*/
/*    Description         : This is a function to remove a pip vip maps 
                            from the port table*/
/*    Input(s)            : Pip port entry 
*/
/*    Output(s)           : None                                          */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbRemovePipVipMaps (tPbbPortRBtreeNode * pPbbPipCurrEntry, UINT4 u4ContextId)
{
    tPbbIsidPipVipNode *pVipListMapNode = NULL;
    tPbbICompVipRBtreeNode *pVipEntry = NULL;
    INT4                i4VipIfIndex = PBB_INIT_VAL;
    UINT2               u2Vip = PBB_INIT_VAL;

    if (pPbbPipCurrEntry == NULL)
    {
        return PBB_FAILURE;
    }
    PBB_DLL_SCAN (&(pPbbPipCurrEntry->unPbbPortData.PipData.VipList),
                  pVipListMapNode, tPbbIsidPipVipNode *)
    {
        i4VipIfIndex = PBB_INIT_VAL;
        u2Vip = pVipListMapNode->unIsidVip.u2Vip;
        if (PbbGetVipData (u2Vip, &pVipEntry) == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }

        PbbVcmGetIfIndexFromLocalPort (u4ContextId, u2Vip, &i4VipIfIndex);

        if (nmhSetFsPbbVipToPipMappingRowStatus (u4ContextId,
                                                 i4VipIfIndex,
                                                 PBB_DESTROY) == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }

        if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }

        pVipListMapNode = NULL;
    }

    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbGetFirstIsidEntry                           */
/*    Description         : This function returns the first least entry from */
/*                          the Isid table RB tree                            */
/*    Input(s)            : None                                             */
/*    Output(s)           : pIsidEntry - Isid entry                          */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetFirstIsidEntry (tPbbIsidRBtreeNode * pIsidEntry)
{
    tPbbIsidRBtreeNode *pTempIsidEntry = NULL;
    pTempIsidEntry = (tPbbIsidRBtreeNode *)
        RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->IsidTable);
    if (pTempIsidEntry != NULL)
    {
        *pIsidEntry = *pTempIsidEntry;
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextIsidEntry                            */
/*    Description         : This function returns the next Isid entry from   */
/*                          the RB tree                                      */
/*    Input(s)            : IsidEntry                                        */
/*    Output(s)           : pNextIsidEntry - Next Isid entry                 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetNextIsidEntry (tPbbIsidRBtreeNode IsidEntry, tPbbIsidRBtreeNode
                     * pNextIsidEntry)
{
    tPbbIsidRBtreeNode *pTempNextIsidEntry = NULL;
    pTempNextIsidEntry =
        (tPbbIsidRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                              IsidTable,
                                              (tRBElem *) & IsidEntry, NULL);
    if (pTempNextIsidEntry != NULL)
    {
        *pNextIsidEntry = *pTempNextIsidEntry;
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetFirstVipEntry                           */
/*    Description         : This function returns the first least entry from */
/*                          the Vip table RB tree                            */
/*    Input(s)            : None                                             */
/*    Output(s)           : pVipEntry - Isid entry                          */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetFirstVipEntry (tPbbICompVipRBtreeNode * pVipEntry)
{
    tPbbICompVipRBtreeNode *pTempVipEntry;
    pTempVipEntry = (tPbbICompVipRBtreeNode *)
        RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->VipTable);
    if (pTempVipEntry != NULL)
    {
        *pVipEntry = *pTempVipEntry;
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextVipEntry                            */
/*    Description         : This function returns the next Vip entry from   */
/*                          the RB tree                                      */
/*    Input(s)            : VipEntry                                        */
/*    Output(s)           : pNextVipEntry - Next Vip entry                 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetNextVipEntry (tPbbICompVipRBtreeNode VipEntry, tPbbICompVipRBtreeNode
                    * pNextVipEntry)
{
    tPbbICompVipRBtreeNode *pTempNextVipEntry;
    pTempNextVipEntry =
        (tPbbICompVipRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                  VipTable,
                                                  (tRBElem *) & VipEntry, NULL);
    if (pTempNextVipEntry != NULL)
    {
        *pNextVipEntry = *pTempNextVipEntry;
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidPortOuiRowstatus                       */
/*                                                                          */
/*    Description        : This function is used to Get CBP rowstatus       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidPortOuiRowstatus (INT4 i4ifIndex, UINT4 u4Isid, UINT4 *u4Rowstatus)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pRBCurNode) != PBB_SUCCESS)
    {
        *u4Rowstatus = PBB_INVALID_ROWSTATUS;
        return PBB_SUCCESS;
    }
    *u4Rowstatus = (UINT4) pRBCurNode->u1OUIRowstatus;
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbTestIsidOUIRowStatus
 Description :  To test the OUI row status for a Isid
 Input       :  Error code
                Context Id
                Isid
                Interface Index
                OUI row status
                
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbTestIsidOUIRowStatus (UINT4 u4ContextId,
                         UINT4 u4Isid, INT4 i4IfIndex, INT4 i4OUIRowStatus)
{
    tPbbPortRBtreeNode *pPbbPortCurrEntry = NULL;
    UINT4               u4Rowstatus = PBB_INIT_VAL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4ContextId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }

    /* Get the local port number for the interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /* Fetch the Port entry from the Port table */
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPortCurrEntry);
    if (pPbbPortCurrEntry == NULL)
    {
        return PBB_FAILURE;
    }
    if (pPbbPortCurrEntry->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        /* Fetch the Isid Port entry from the Isid table */
        if (PbbValidateIsidPort (u4Isid, i4IfIndex) != PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_PORT_NOT_MAP_TO_ISID);
            return PBB_FAILURE;
        }
        if (PbbGetIsidPortOuiRowstatus (i4IfIndex,
                                        u4Isid, &u4Rowstatus) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }

    }
    else
    {
        CLI_SET_ERR (CLI_PBB_PORT_NOT_CBP);
        return PBB_FAILURE;
    }
    switch (i4OUIRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
            if (PBB_INVALID_ROWSTATUS != u4Rowstatus)
            {
                CLI_SET_ERR (CLI_PBB_VALID_ROWSTATUS);
                return PBB_FAILURE;
            }
            break;

        case PBB_ACTIVE:
            if (PBB_INVALID_ROWSTATUS == u4Rowstatus)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                return PBB_FAILURE;
            }
            break;

        case PBB_NOT_IN_SERVICE:
            if (PBB_INVALID_ROWSTATUS == u4Rowstatus)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                return PBB_FAILURE;
            }
            break;

        case PBB_DESTROY:
            if (PBB_INVALID_ROWSTATUS == u4Rowstatus)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                return PBB_FAILURE;
            }
            break;

        default:
            CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS_VALUE);
            return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

 /****************************************************************************
 Function    :  PbbTestIsidOUI
 Description :  To test the OUI for a Isid
 Input       :  Error code
                Context Id
                Isid
                Interface Index
                OUI                
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbTestIsidOUI (UINT4 *pu4ErrorCode,
                UINT4 u4ContextId,
                UINT4 u4Isid, INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pOUI)
{
    tPbbPortRBtreeNode *pPbbPortCurrEntry = NULL;
    tPbbBackBoneSerInstEntry *pPbbIsidPortEntry = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4ContextId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return PBB_FAILURE;
    }

    /* Get the local port number for the interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return PBB_FAILURE;
    }
    /* Fetch the Port entry from the Port table */
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPortCurrEntry);
    if (pPbbPortCurrEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return PBB_FAILURE;
    }
    if (pPbbPortCurrEntry->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        /* Fetch the Isid Port entry from the Isid table */
        if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pPbbIsidPortEntry)
            != PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_PORT_NOT_MAP_TO_ISID);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return PBB_FAILURE;
        }

        if (pPbbIsidPortEntry == NULL)
        {
            CLI_SET_ERR (CLI_PBB_PORT_NOT_MAP_TO_ISID);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return PBB_FAILURE;
        }
    }
    else
    {
        CLI_SET_ERR (CLI_PBB_PORT_NOT_CBP);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return PBB_FAILURE;
    }

    if ((PBB_OUI_LENGTH < pOUI->i4_Length) || (pOUI->i4_Length < PBB_INIT_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return PBB_FAILURE;

    }
    return PBB_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetIsidPortOuiRowstatus                       */
/*                                                                          */
/*    Description        :This function is used to Set  rowstatus for an OUI*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetIsidPortOuiRowstatus (INT4 i4ifIndex, UINT4 u4Isid, UINT4 u4Rowstatus)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pRBCurNode->u1OUIRowstatus = (UINT1) u4Rowstatus;
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbSetIsidOUIRowStatus
 Description :  To set the OUI row status for a Isid
 Input       :  Context Id
                Isid
                Interface Index
                OUI row status
                
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbSetIsidOUIRowStatus (UINT4 u4ContextId,
                        UINT4 u4Isid, INT4 i4IfIndex, INT4 i4OUIRowStatus)
{
    tPbbBackBoneSerInstEntry *pPbbIsidPortEntry = NULL;
    tPbbIsidRBtreeNode *pPbbIsidEntry = NULL;
    tMacAddr            tempMacAddr;
    tSNMP_OCTET_STRING_TYPE pOUI;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    PBB_MEMSET (tempMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (au1OUI, PBB_INIT_VAL, PBB_OUI_LENGTH);

    pOUI.i4_Length = PBB_OUI_LENGTH;
    pOUI.pu1_OctetList = au1OUI;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4ContextId);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }

    /* Get the local port number for the interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /* Fetch the Isid entry from the Isid table */
    if (PbbGetIsidData (u4Isid, &pPbbIsidEntry) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (pPbbIsidEntry == NULL)
    {
        return PBB_FAILURE;
    }

    /* Fetch the Isid Port entry from the Isid Port table */
    if (PbbGetIsidPortData (u2LocalPort,
                            u4Isid, &pPbbIsidPortEntry) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (pPbbIsidPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    switch (i4OUIRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
            if (PbbSetIsidPortOuiRowstatus (i4IfIndex,
                                            u4Isid,
                                            i4OUIRowStatus) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
            break;

        case PBB_ACTIVE:
            if (PbbSetIsidPortOuiRowstatus (i4IfIndex,
                                            u4Isid,
                                            i4OUIRowStatus) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
            /* Set the value of DA at H/W using OUI and Isid */
            if (PbbSetBCompHwServiceInst (u4ContextId,
                                          u4Isid, i4IfIndex) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }

            break;

        case PBB_NOT_IN_SERVICE:
        {
            if (PbbSetIsidPortOuiRowstatus (i4IfIndex,
                                            u4Isid,
                                            i4OUIRowStatus) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }

            break;
        }

        case PBB_DESTROY:
        {
            i4OUIRowStatus = PBB_INVALID_ROWSTATUS;

            if (PbbSetIsidPortOuiRowstatus (i4IfIndex,
                                            u4Isid,
                                            i4OUIRowStatus) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
            if (PbbSetIsidOUI (u4ContextId,
                               u4Isid, i4IfIndex, &pOUI) == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            /* After Removing the OUI set the default dest mac to its default value */
            if (PbbSetBCompHwServiceInst (u4ContextId,
                                          u4Isid, i4IfIndex) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
        }
            break;
        default:
            return PBB_FAILURE;

    }
    return PBB_SUCCESS;

}

/****************************************************************************
 Function    :  PbbSetIsidOUI
 Description :  To set the OUI for a Isid
 Input       :  Context Id
                Isid
                OUI
                Interface Index
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbSetIsidOUI (UINT4 u4ContextId,
               UINT4 u4Isid, INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pOUI)
{
    tPbbPortRBtreeNode *pPbbPortCurrEntry = NULL;
    tPbbBackBoneSerInstEntry *pPbbIsidPortEntry = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4ContextId);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }

    /* Get the local port number for the interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /* Fetch the Port entry from the Port table */
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPortCurrEntry);
    if (pPbbPortCurrEntry == NULL)
    {
        return PBB_FAILURE;
    }
    if (pPbbPortCurrEntry->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        /* Fetch the Isid Port entry from the Isid table */
        if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pPbbIsidPortEntry)
            != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (pPbbIsidPortEntry == NULL)
        {
            return PBB_FAILURE;
        }
        PBB_MEMCPY (pPbbIsidPortEntry->au1IsidPortOUI,
                    pOUI->pu1_OctetList, pOUI->i4_Length);
    }
    else
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbGetIsidOUIRowStatus
 Description :  To get the OUI row status for a Isid
 Input       :  Context Id
                Isid
                Interface Index
                
 Output      :  OUI Row Status
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbGetIsidOUIRowStatus (UINT4 u4ContextId,
                        UINT4 u4Isid, INT4 i4IfIndex, INT4 *pOUIRowStatus)
{
    tPbbPortRBtreeNode *pPbbPortCurrEntry = NULL;
    UINT4               u4RowStatus;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4ContextId);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /* Get the local port number for the interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /* Fetch the Port entry from the Port table */
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPortCurrEntry);
    if (pPbbPortCurrEntry == NULL)
    {
        return PBB_FAILURE;
    }
    if (pPbbPortCurrEntry->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        /* Fetch the Isid Port entry from the Isid table */
        if (PbbGetIsidPortOuiRowstatus (i4IfIndex, u4Isid, &u4RowStatus)
            != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        *pOUIRowStatus = u4RowStatus;
    }
    else
    {
        CLI_SET_ERR (CLI_PBB_PORT_NOT_CBP);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbGetIsidOUI
 Description :  To get the OUI for a Isid
 Input       :  Context Id
                Isid
                Interface Index
                
 Output      :  OUI
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbGetIsidOUI (UINT4 u4ContextId,
               UINT4 u4Isid, INT4 i4IfIndex, tSNMP_OCTET_STRING_TYPE * pOUI)
{
    tPbbPortRBtreeNode *pPbbPortCurrEntry = NULL;
    tPbbBackBoneSerInstEntry *pPbbIsidPortEntry = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT1               au1tempOui[PBB_OUI_LENGTH];

    PBB_MEMSET (au1tempOui, PBB_INIT_VAL, PBB_OUI_LENGTH);
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4ContextId);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /* Get the local port number for the interface index */
    i4retVal = PbbVcmGetIfMapHlPortId (i4IfIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /* Fetch the Port entry from the Port table */
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPortCurrEntry);
    if (pPbbPortCurrEntry == NULL)
    {
        return PBB_FAILURE;
    }
    if (pPbbPortCurrEntry->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        /* Fetch the Isid Port entry from the Isid table */
        if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pPbbIsidPortEntry)
            != PBB_SUCCESS)
        {
            CLI_SET_ERR (CLI_PBB_PORT_NOT_MAP_TO_ISID);
            return PBB_FAILURE;
        }
        if (pPbbIsidPortEntry == NULL)
        {
            CLI_SET_ERR (CLI_PBB_PORT_NOT_MAP_TO_ISID);
            return PBB_FAILURE;
        }
        if (pPbbIsidPortEntry->u1OUIRowstatus == PBB_INVALID_ROWSTATUS)
        {
            return PBB_FAILURE;
        }
        if (PBB_MEMCMP (au1tempOui,
                        pPbbIsidPortEntry->au1IsidPortOUI,
                        PBB_OUI_LENGTH) == PBB_INIT_VAL)
        {
            PBB_MEMCPY (pOUI->pu1_OctetList, PBB_GLOBAL_OUI, PBB_OUI_LENGTH);
            return PBB_SUCCESS;
        }
        PBB_MEMCPY (pOUI->pu1_OctetList,
                    pPbbIsidPortEntry->au1IsidPortOUI, PBB_OUI_LENGTH);
        pOUI->i4_Length = PBB_OUI_LENGTH;
    }
    else
    {
        CLI_SET_ERR (CLI_PBB_PORT_NOT_CBP);
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;

}

/****************************************************************************
 Function    :  PbbValidateViptoPip
 Description :  To add the Pip Vip entries at Hw for each Pip Vip map
 Input       :  Pip port entry
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbValidateViptoPip (INT4 i4VipIfIndex, UINT2 u2LocalPort)
{
    tPbbICompVipRBtreeNode *pVipEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2Vip = PBB_INIT_VAL;
    INT1                i1retVal =
        PbbVcmGetIfMapHlPortId (i4VipIfIndex, &u2Vip);
    if (i1retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    if (PbbGetVipData ((UINT4) u2Vip, &pVipEntry) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pVipEntry->pu4PbbIsidVipInfo;
    if (pIsidVipInfo->unPbbCompData.ICompData.u2Pip != u2LocalPort)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbResetVipInPip
 Description :  To reset the Vip in the Pip Vip map
 Input       :  Vip
                Pip port number
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/

INT1
PbbResetVipInPip (UINT2 u2Vip, UINT2 u2LocalPort)
{
    tPbbPortRBtreeNode *pPbbPipEntry = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPipEntry);
    if ((pPbbPipEntry == NULL) ||
        (pPbbPipEntry->u1PortType != PBB_PROVIDER_INSTANCE_PORT))
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    if (PbbDeleteVipLocalNodeDll (u2Vip, u2LocalPort) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;

}

/****************************************************************************
 Function    :  PbbSetVipInPip
 Description :  To set the Vip in the Pip Vip map
 Input       :  Vip
                Pip port number
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/

INT1
PbbSetVipInPip (UINT2 u2Vip, UINT2 u2LocalPort)
{
    tPBB_DLL           *pPipVipMap = NULL;
    tPbbIsidPipVipNode *pPipVipMapNode = NULL;
    tPbbPortRBtreeNode *pPbbPipEntry = NULL;
    UINT2               u2TempVip = PBB_INIT_VAL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPipEntry);
    if ((pPbbPipEntry == NULL) ||
        (pPbbPipEntry->u1PortType != PBB_PROVIDER_INSTANCE_PORT))
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    pPipVipMap = &(pPbbPipEntry->unPbbPortData.PipData.VipList);
    /* Checking if the Vip is already present in Pip Vip Map */
    PBB_DLL_SCAN (pPipVipMap, pPipVipMapNode, tPbbIsidPipVipNode *)
    {
        u2TempVip = pPipVipMapNode->unIsidVip.u2Vip;
        /* Check if the Vip already exists in the Pip Vip Map */
        if (u2Vip == u2TempVip)
        {
            return PBB_SUCCESS;
        }
    }
    if (PbbInsertPipVipDll (u2Vip, pPbbPipEntry->u4IfIndex) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbGetVipPipInterfaceIndex
 Description :  To get the Pip interface index for a Vip 
 Input       :  Component Id
                Vip 
                
 Output      :  Pip interface index
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbGetVipPipInterfaceIndex (UINT4 u4CompId, UINT2 u2Vip, INT4 *pi4PipIfIndex)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbPortRBtreeNode *pPbbPortEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4CompId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }
    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;
    u2LocalPort = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPortEntry);
    if ((pPbbPortEntry == NULL) ||
        (pPbbPortEntry->u1PortType != PBB_PROVIDER_INSTANCE_PORT))
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    *pi4PipIfIndex = (INT4) pPbbPortEntry->u4IfIndex;
    return PBB_SUCCESS;

}

/****************************************************************************
 Function    :  PbbSetVipPipStorageType
 Description :  To set the Pip Storage type
 Input       :  Component Id
                Vip 
                Pip Storage Type
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbSetVipPipStorageType (UINT4 u4CompId, UINT2 u2Vip, UINT1 u1PipStorageType)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4CompId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }
    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;
    pIsidVipInfo->unPbbCompData.ICompData.u1PipStorageType = u1PipStorageType;
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbSetVipPipInterfaceIndex
 Description :  To Set the Pip interface index for a Vip 
 Input       :  Component Id
                Vip
                PipIfIndex
                
 Output      :  Pip interface index
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbSetVipPipInterfaceIndex (UINT4 u4CompId, UINT4 u4Isid, UINT2 u2Vip,
                            INT4 i4PipIfIndex)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    UNUSED_PARAM (u4Isid);

    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4CompId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }
    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    i4retVal = PbbVcmGetIfMapHlPortId (i4PipIfIndex, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;
    pIsidVipInfo->unPbbCompData.ICompData.u2Pip = u2LocalPort;

    if (PbbSetVipInPip (u2Vip, u2LocalPort) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    if (pIsidVipInfo->u4NumPortsPerIsidPerContextPresent == PBB_INIT_VAL)
    {
        pIsidVipInfo->u4NumPortsPerIsidPerContextPresent++;
    }
    return PBB_SUCCESS;

}

/****************************************************************************
 Function    :  PbbSetVipPipRowStatus
 Description :  To set the Pip row status
 Input       :  Component Id
                Vip
                Row Status                
 Output      :  None.
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbSetVipPipRowStatus (UINT4 u4CompId, UINT2 u2Vip, UINT4 i4VipPipRowStatus)
{
    tPbbPortRBtreeNode *pPbbPortEntry = NULL;
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT2               u2Pip = PBB_INIT_VAL;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4CompId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }

    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;

    if (pIsidVipInfo != NULL)
    {
        if (pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus ==
            i4VipPipRowStatus)
        {
            return PBB_SUCCESS;
        }
    }

    if (pIsidVipInfo == NULL)
    {
        return PBB_FAILURE;
    }

    switch (i4VipPipRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
            pIsidVipInfo->unPbbCompData.ICompData.u2Pip = PBB_DEFAULT_PORT_VAL;
            pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus =
                PBB_CREATE_AND_WAIT;
            break;

        case PBB_ACTIVE:
            pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus =
                PBB_ACTIVE;

            /* Set this vip pip map at H/W */
            u4Isid = pIsidVipInfo->u4Isid;
            u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
            if (u2Pip == PBB_INIT_VAL)
            {
                break;
            }
            PBB_GET_PORT_ENTRY (u2Pip, pPbbPortEntry);
            if ((pPbbPortEntry == NULL) ||
                (pPbbPortEntry->u1PortType != PBB_PROVIDER_INSTANCE_PORT))
            {
                CLI_SET_ERR (CLI_PBB_INVALID_PORT);
                return PBB_FAILURE;
            }
            if (PbbHwAddVipToPip (u4CompId, u4Isid, u2Vip, u2Pip)
                == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            break;
        case PBB_NOT_IN_SERVICE:
            pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus =
                PBB_NOT_IN_SERVICE;
            /* Reset this vip pip map at H/W */
            u4Isid = pIsidVipInfo->u4Isid;
            u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
            if (u2Pip == PBB_INIT_VAL)
            {
                break;
            }
            PBB_GET_PORT_ENTRY (u2Pip, pPbbPortEntry);
            if ((pPbbPortEntry == NULL) ||
                (pPbbPortEntry->u1PortType != PBB_PROVIDER_INSTANCE_PORT))
            {
                CLI_SET_ERR (CLI_PBB_INVALID_PORT);
                return PBB_FAILURE;
            }
            if (PbbHwDeleteVipToPip (u4CompId, u4Isid, u2Vip, u2Pip)
                == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }

            break;

        case PBB_DESTROY:
            u2LocalPort = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
            pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus =
                PBB_INVALID_ROWSTATUS;
            /* Set the Vip Oper Status */
            PbbSetVipOperStatus (u4CompId, PBB_INIT_VAL, u2Vip, PBB_INIT_VAL);
            pIsidVipInfo->unPbbCompData.ICompData.u2Pip = PBB_DEFAULT_PORT_VAL;
            /* Reset this Vip from the Pip Vip Map */

            if (PbbValidatePipPortExists (u2LocalPort) == PBB_SUCCESS)
            {
                if (PbbResetVipInPip (u2Vip, u2LocalPort) == PBB_FAILURE)
                {
                    return PBB_FAILURE;
                }
                u4Isid = pIsidVipInfo->u4Isid;
                /* Reset this vip pip map at H/W */
                if (PbbHwDeleteVipToPip (u4CompId, u4Isid, u2Vip, u2LocalPort)
                    == PBB_FAILURE)
                {
                    return PBB_FAILURE;
                }
                if (PbbDecrNumPortPerIsidPerContext (u4Isid) != PBB_SUCCESS)
                {
                    return PBB_FAILURE;
                }

            }
            break;

        default:
            return PBB_FAILURE;
    }
    if (i4VipPipRowStatus != PBB_DESTROY)
    {
        /* Set the Vip Oper Status */
        PbbSetVipOperStatus (u4CompId, PBB_INIT_VAL, u2Vip, PBB_INIT_VAL);
    }

    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbTestVipPipRowStatus
 Description :  To test the Pip row status
 Input       :  Error code
                Component Id
                Vip
                Row Status                
 Output      :  None.
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbTestVipPipRowStatus (UINT4 *pu4ErrorCode,
                        UINT4 u4CompId, UINT2 u2Vip, UINT4 i4VipPipRowStatus)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    INT4                i4RowStatus = PBB_INIT_VAL;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT4               u4NumOfPortsPerIsidPresent = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4CompId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }

    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;
    i4RowStatus =
        (INT4) pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus;
    switch (i4VipPipRowStatus)
    {
        case PBB_CREATE_AND_WAIT:
            if (PBB_INVALID_ROWSTATUS != i4RowStatus)
            {
                CLI_SET_ERR (CLI_PBB_VALID_ROWSTATUS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return PBB_FAILURE;
            }

            if (PbbGetVipIsidonLocal ((UINT2) u2Vip, &u4Isid) != PBB_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            if (u4Isid == PBB_ISID_INVALID_VALUE)
            {
                break;
            }
            if (PbbGetMaxNumPortPerIsid (&u4NumOfPortsPerIsidPresent,
                                         u4Isid, u4CompId) != PBB_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            if (u4NumOfPortsPerIsidPresent >=
                gPbbGlobData.PbbTaskInfo.u4MaxPortperIsid)
            {
                CLI_SET_ERR (CLI_PBB_PORT_PER_ISID_EXCEED);
                PbbReleaseContext ();
                return SNMP_FAILURE;
            }

            break;

        case PBB_ACTIVE:
            if (PBB_INVALID_ROWSTATUS == i4RowStatus)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return PBB_FAILURE;
            }
            break;

        case PBB_NOT_IN_SERVICE:
            if (PBB_INVALID_ROWSTATUS == i4RowStatus)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return PBB_FAILURE;
            }
            break;

        case PBB_DESTROY:
            if (PBB_INVALID_ROWSTATUS == i4RowStatus)
            {
                CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return PBB_FAILURE;
            }
            break;
        default:
            return PBB_FAILURE;

    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbIsVipPipRowStatusNotActive
 Description :  To Test the Vip Pip ros status
 Input       :  Error code
                Component Id
                Vip
                BMac address                
 Output      :  None.
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbIsVipPipRowStatusNotActive (UINT4 u4CompId, UINT2 u2Vip)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4CompId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }

    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    /* Validate that the Row status is not active */
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;
    if (pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus == PBB_ACTIVE)
    {
        CLI_SET_ERR (CLI_PBB_ROWSTATUS_ACTIVE);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbGetVipPipRowStatus
 Description :  To get the Vip Pip row status from the Vip table
 Input       :  Component Id
                Vip 
                
 Output      :  Pip row status
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbGetVipPipRowStatus (UINT4 u4CompId, UINT2 u2Vip, UINT4 *pu2PipRowStatus)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4CompId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }

    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;
    *pu2PipRowStatus =
        (INT4) pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus;
    return PBB_SUCCESS;

}

/****************************************************************************
 Function    :  PbbGetVipPipStorageType
 Description :  To get the Pip Storage type
 Input       :  Component Id
                Vip 
                
 Output      :  Pip Storage type
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbGetVipPipStorageType (UINT4 u4CompId, UINT2 u2Vip, UINT4 *pu2PipStorageType)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    INT4                i4retVal = PBB_INIT_VAL;
    /* Select the component i.e Context */
    i4retVal = PbbSelectContext (u4CompId);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }
    /* Fetch the vip port entry from the Vip table */
    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pPbbVipCurrEntry->pu4PbbIsidVipInfo;
    *pu2PipStorageType = pIsidVipInfo->unPbbCompData.ICompData.u1PipStorageType;
    return PBB_SUCCESS;

}

/****************************************************************************
 Function    :  PbbValidatePipPortExists
 Description :  To validate that the given local port is PIP
 Input       :  Local Port Number
 Output      :  None
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT4
PbbValidatePipPortExists (UINT2 u2LocalPort)
{
    tPbbPortRBtreeNode *pPbbPipCurrEntry = NULL;
    /* Fetch the pip port entry from the port table  */
    if (PbbGetPortData (u2LocalPort, &pPbbPipCurrEntry) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    if ((pPbbPipCurrEntry == NULL) ||
        (pPbbPipCurrEntry->u1PortType != PBB_PROVIDER_INSTANCE_PORT))
    {
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbIsPipRowStatusNotActive
 Description :  To Test the Pip BMac address
 Input       :  Error code
                Local Port Number
                BMac address                
 Output      :  None.
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbIsPipRowStatusNotActive (UINT2 u2LocalPort)
{
    tPbbPortRBtreeNode *pPbbPipCurrEntry;
    /* Fetch the pip port entry from the port table  */
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPipCurrEntry);
    if (pPbbPipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    /* Validate that the Row status is not active */
    if (pPbbPipCurrEntry->unPbbPortData.PipData.u1PipRowStatus == PBB_ACTIVE)
    {
        CLI_SET_ERR (CLI_PBB_ROWSTATUS_ACTIVE);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbTestPipRowStatus
 Description :  To test the Pip row status
 Input       :  Error code
                Local Port Number
                Row Status                
 Output      :  None.
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbTestPipRowStatus (UINT4 *pu4ErrorCode,
                     UINT2 u2LocalPort, INT4 i4PipRowStatus)
{
    tPbbPortRBtreeNode *pPbbPipCurrEntry = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPipCurrEntry);
    if ((pPbbPipCurrEntry == NULL)
        || (pPbbPipCurrEntry->u1PortType != PBB_PROVIDER_INSTANCE_PORT))
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return PBB_FAILURE;
    }
    switch (i4PipRowStatus)
    {

        case PBB_ACTIVE:
        case PBB_NOT_IN_SERVICE:
            return PBB_SUCCESS;
        case PBB_CREATE_AND_WAIT:
        case PBB_DESTROY:
            CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
            return PBB_FAILURE;
        default:
            CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
            return PBB_FAILURE;

    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbSetPipRowStatus
 Description :  To set the Pip row status
 Input       :  Local Port Number
                Row Status                
 Output      :  None.
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbSetPipRowStatus (UINT2 u2LocalPort, INT4 i4PipRowStatus)
{
    tPbbPortRBtreeNode *pPbbPipCurrEntry = NULL;
    if (PbbGetPortData (u2LocalPort, &pPbbPipCurrEntry) == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    if (pPbbPipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    if (i4PipRowStatus == PBB_DESTROY)
    {
        pPbbPipCurrEntry->unPbbPortData.PipData.u1PipRowStatus =
            PBB_INVALID_ROWSTATUS;
    }
    else
    {
        pPbbPipCurrEntry->unPbbPortData.PipData.u1PipRowStatus =
            (UINT1) i4PipRowStatus;
    }
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbGetPipRowStatus
 Description :  To get the Pip row status
 Input       :  Local Port Number 
                
 Output      :  Row status.
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbGetPipRowStatus (UINT2 u2LocalPort, INT4 *pi4PipRowStatus)
{
    tPbbPortRBtreeNode *pPbbPipCurrEntry = NULL;
    /* Fetch the pip port entry from the port table */
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPipCurrEntry);
    if (pPbbPipCurrEntry == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    if (pPbbPipCurrEntry->unPbbPortData.PipData.u1PipRowStatus ==
        PBB_INVALID_ROWSTATUS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
        return PBB_FAILURE;
    }
    *pi4PipRowStatus =
        (INT4) pPbbPipCurrEntry->unPbbPortData.PipData.u1PipRowStatus;
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbGetFirstPortEntry                           */
/*    Description         : This function returns the first least entry from */
/*                          the Port table RB tree                            */
/*    Input(s)            : None                                             */
/*    Output(s)           : pPortEntry - Port entry                          */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetFirstPortEntry (tPbbPortRBtreeNode * pPortEntry)
{
    tPbbPortRBtreeNode *pPortCurrEntry = NULL;
    pPortCurrEntry = (tPbbPortRBtreeNode *)
        RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->PortTable);
    if (pPortCurrEntry != NULL)
    {
        *pPortEntry = *pPortCurrEntry;
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextPortEntry                            */
/*    Description         : This function returns the next Port entry from   */
/*                          the RB tree                                      */
/*    Input(s)            : pPortEntry                                       */
/*    Output(s)           : pNextPortEntry - Next Port entry                 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetNextPortEntry (tPbbPortRBtreeNode * pPortEntry, tPbbPortRBtreeNode
                     ** pNextPortEntry)
{
    tPbbPortRBtreeNode *pTempPortEntry = NULL;
    pTempPortEntry =
        (tPbbPortRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                              PortTable,
                                              (tRBElem *) pPortEntry, NULL);
    if (pTempPortEntry != NULL)
    {
        *pNextPortEntry = pTempPortEntry;
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetFirstPbbPipIfIndex                           */
/*    Description         : This function returns the first PIP Interface
                            index from the port table                      */
/*    Input(s)            : None                                             */
/*    Output(s)           : pPipIfIndex - IfIndex                         */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/
INT1
PbbGetFirstPbbPipIfIndex (INT4 *pi4PipIfIndex)
{
    tPbbPortRBtreeNode *pRBCurrNode = NULL;
    tPbbPortRBtreeNode *pRBNextNode = NULL;

    pRBCurrNode = (tPbbPortRBtreeNode *)
        RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->PortTable);
    if (pRBCurrNode == NULL)
    {
        return PBB_FAILURE;
    }
    if (pRBCurrNode->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
    {
        *pi4PipIfIndex = pRBCurrNode->u4IfIndex;
        return PBB_SUCCESS;
    }
    else
    {
        while (1)
        {
            pRBNextNode =
                (tPbbPortRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                      PortTable, pRBCurrNode,
                                                      (tRBCompareFn)
                                                      PbbComparePortRBNodes);
            if (pRBNextNode == NULL)
            {
                return PBB_FAILURE;
            }
            if (pRBNextNode->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
            {
                *pi4PipIfIndex = pRBNextNode->u4IfIndex;
                return PBB_SUCCESS;
            }
            pRBCurrNode = pRBNextNode;
            pRBNextNode = NULL;
        }
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextPipIfIndex                           */
/*    Description         : This function returns the next PIP Interface
 *                           index from the Pip port table                  */
/*    Input(s)            : None                                             */
/*    Output(s)           : PipIfIndex - IfIndex
 *                          pNextPipIfIndex - Next IfIndex                   */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetNextPbbPipIfIndex (INT4 i4PipIfIndex, INT4 *pi4NextPipIfIndex)
{
    tPbbPortRBtreeNode *pRBCurrNode = NULL;
    tPbbPortRBtreeNode *pRBNextNode = NULL;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4PipIfIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2LocalPort = u2LocalPort;

    pRBCurrNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurrNode == NULL)
    {
        return PBB_FAILURE;
    }
    while (1)
    {
        pRBNextNode =
            (tPbbPortRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                  PortTable, pRBCurrNode,
                                                  (tRBCompareFn)
                                                  PbbComparePortRBNodes);
        if (pRBNextNode == NULL)
        {
            return PBB_FAILURE;
        }
        if (pRBNextNode->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
        {
            *pi4NextPipIfIndex = pRBNextNode->u4IfIndex;
            return PBB_SUCCESS;
        }
        pRBCurrNode = pRBNextNode;
        pRBNextNode = NULL;

    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetFirstPipPortEntry                           */
/*    Description         : This function returns the first least entry from */
/*                          the Pip port table                            */
/*    Input(s)            : None                                             */
/*    Output(s)           : pPipEntry - Pip entry                          */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetFirstPipPortEntry (tPbbPortRBtreeNode * pPipEntry)
{
    tPbbPortRBtreeNode  PbbCurrPortEntry;
    tPbbPortRBtreeNode *pPbbNextPortEntry = NULL;
    INT4                i4retval = PbbGetFirstPortEntry (&PbbCurrPortEntry);
    if (i4retval == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    pPbbNextPortEntry = &PbbCurrPortEntry;
    do
    {
        PbbCurrPortEntry = *pPbbNextPortEntry;
        if (PbbCurrPortEntry.u1PortType == PBB_PROVIDER_INSTANCE_PORT)
        {
            *pPipEntry = PbbCurrPortEntry;
            return PBB_SUCCESS;
        }
    }
    while (PbbGetNextPortEntry (&PbbCurrPortEntry, &pPbbNextPortEntry) !=
           PBB_FAILURE);
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextPipPortEntry                            */
/*    Description         : This function returns the next Pip entry from   */
/*                          the Pip table                                    */
/*    Input(s)            : Current PipEntry                              */
/*    Output(s)           : pNextPipEntry - Next Pip entry                 */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetNextPipPortEntry (tPbbPortRBtreeNode PipEntry, tPbbPortRBtreeNode
                        * pNextPipEntry)
{
    tPbbPortRBtreeNode *pPbbCurrPortEntry = NULL;
    tPbbPortRBtreeNode *pPbbNextPortEntry = NULL;
    pPbbCurrPortEntry = &PipEntry;
    if (PbbGetNextPortEntry (pPbbCurrPortEntry, &pPbbNextPortEntry) ==
        PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    pPbbNextPortEntry = pPbbCurrPortEntry;
    do
    {
        pPbbCurrPortEntry = pPbbNextPortEntry;
        if (pPbbCurrPortEntry->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
        {
            pNextPipEntry = pPbbCurrPortEntry;
            return PBB_SUCCESS;
        }
    }
    while (PbbGetNextPortEntry (pPbbCurrPortEntry, &pPbbNextPortEntry) !=
           PBB_FAILURE);
    UNUSED_PARAM (pNextPipEntry);
    return PBB_FAILURE;

}

/*****************************************************************************/
/*    Function Name       : PbbGetPipIfIndex                                 */
/*    Description         : This function validates the pip if index
                            from the port table                              */
/*    Input(s)            : None                                             */
/*    Output(s)           : pPipIfIndex - IfIndex                            */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                          */
/*****************************************************************************/
INT1
PbbGetPipIfIndex (INT4 i4FsPbbPipIfIndex)
{
    tPbbPortRBtreeNode *pRBNode;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;

    if (PbbVcmGetContextInfoFromIfIndex (i4FsPbbPipIfIndex,
                                         &u4ContextId,
                                         &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (u4ContextId != PBB_CURR_CONTEXT_ID ())
    {
        return PBB_FAILURE;
    }

    key.u2LocalPort = u2LocalPort;
    pRBNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbGetFirstPipIfIndex                           */
/*    Description         : This function returns the first PIP Interface
                            index from the port table                      */
/*    Input(s)            : None                                             */
/*    Output(s)           : pPipIfIndex - IfIndex                         */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/
INT1
PbbGetFirstPipIfIndex (INT4 *pi4PipIfIndex)
{
    tPbbPortRBtreeNode  PbbPipFirstEntry;
    INT4                i4retval = PBB_FAILURE;

    PBB_MEMSET (&PbbPipFirstEntry, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));

    /* Fetch the first Pip port entry from the port table */
    i4retval = PbbGetFirstPipPortEntry (&PbbPipFirstEntry);

    if (i4retval == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_PIP_PORT_NOT_PRESENT);
        return PBB_FAILURE;
    }
    *pi4PipIfIndex = (INT4) PbbPipFirstEntry.u4IfIndex;
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextPipIfIndex                           */
/*    Description         : This function returns the next PIP Interface
 *                           index from the Pip port table                  */
/*    Input(s)            : None                                             */
/*    Output(s)           : PipIfIndex - IfIndex
 *                          pNextPipIfIndex - Next IfIndex                   */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetNextPipIfIndex (INT4 i4PipIfIndex, INT4 *pi4NextPipIfIndex)
{
    tPbbPortRBtreeNode *pPbbPipCurrEntry = NULL;
    tPbbPortRBtreeNode  PbbPipNextEntry;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate that the local port exists for interface index */
    INT4                i4retVal =
        PbbVcmGetIfMapHlPortId (i4PipIfIndex, &u2LocalPort);
    PBB_MEMSET (&PbbPipNextEntry, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_PORT);
        return PBB_FAILURE;
    }
    /* Fetch the pip entry from the port table for local port number */
    PBB_GET_PORT_ENTRY (u2LocalPort, pPbbPipCurrEntry);
    if ((pPbbPipCurrEntry == NULL) ||
        (pPbbPipCurrEntry->u1PortType != PBB_PROVIDER_INSTANCE_PORT))
    {
        return PBB_FAILURE;
    }
    /* Fetch the next pip port entry with respect to current pip port entry */
    i4retVal = PbbGetNextPipPortEntry (*pPbbPipCurrEntry, &PbbPipNextEntry);
    if (i4retVal == PBB_FAILURE)
    {
        CLI_SET_ERR (CLI_PBB_PIP_PORT_NOT_PRESENT);
        return PBB_FAILURE;
    }
    *pi4NextPipIfIndex = (INT4) PbbPipNextEntry.u4IfIndex;
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbGetFirstVipIfIndex                           */
/*    Description         : This function returns the first Vip
                            from the Vip table                      */
/*    Input(s)            : None                                             */
/*    Output(s)           : pVip -  Vip If index                      */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetFirstVipIfIndex (INT4 *pi4Vip)
{
    tPbbICompVipRBtreeNode PbbVipFirstEntry;
    INT4                i4retval = PBB_FAILURE;

    /* Fetch the first Pip port entry from the port table */
    i4retval = PbbGetFirstVipEntry (&PbbVipFirstEntry);

    if (i4retval == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i4retval = PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                              PbbVipFirstEntry.u2Vip, pi4Vip);
    if (i4retval == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbGetOperStatus                                 */
/*    Description         : This function returns the Operational status     */
/*                          of the given local port                          */
/*    Input(s)            : u2Port - Local Port                              */
/*                        : pu1PortStatus - Oper status of the port          */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                          */
/*****************************************************************************/
INT1
PbbGetOperStatus (UINT2 u2Port, UINT1 *pu1PortStatus)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbICompVipRBtreeNode *pVipEntry = NULL;

    *pu1PortStatus = PBB_PORT_STATUS_DOWN;

    if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        PBB_GET_VIP_ENTRY (u2Port, pVipEntry);
        if (pVipEntry != NULL)
        {
            pIsidVipInfo =
                (tPbbIsidVipInfo *) (VOID *) pVipEntry->pu4PbbIsidVipInfo;
            if (pIsidVipInfo)
            {
                *pu1PortStatus = pIsidVipInfo->unPbbCompData.
                    ICompData.u1VipPortStatus;
            }
            return PBB_SUCCESS;
        }
    }

    /* CNP or PIP or CBP */
    PBB_GET_PORT_ENTRY (u2Port, pRBCurNode);
    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }

    *pu1PortStatus = pRBCurNode->u1PortStatus;
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextLocalPort                              */
/*    Description         : This function returns the next available port    */
/*                          from the selected Context                        */
/*    Input(s)            : u2Port - Local Port                              */
/*    Output(s)           : pu2NextPort -  Next Local Port                   */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                          */
/*****************************************************************************/

INT1
PbbGetNextLocalPort (UINT2 u2Port, UINT2 *pu2NextPort)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode *pPbbNextPortEntry = NULL;
    tPbbICompVipRBtreeNode PbbVipFirstEntry;
    tPbbPortRBtreeNode  PbbPortEntry;
    INT4                i4Retval = PBB_FAILURE;
    UINT1               u1IsLastPortEntry = PBB_FALSE;

    *pu2NextPort = 0;

    /* Get the First Local Port in the same Context */
    if (u2Port == 0)
    {
        i4Retval = PbbGetFirstPortEntry (&PbbPortEntry);

        if (i4Retval == PBB_SUCCESS)
        {
            *pu2NextPort = PbbPortEntry.u2LocalPort;
        }
        else
        {
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                i4Retval = PbbGetFirstVipEntry (&PbbVipFirstEntry);

                if (i4Retval == PBB_SUCCESS)
                {
                    *pu2NextPort = PbbVipFirstEntry.u2Vip;
                }
            }
        }

        return i4Retval;
    }

    /* Get the Next CNP or PIP or CBP */
    PBB_GET_PORT_ENTRY (u2Port, pRBCurNode);
    if (pRBCurNode != NULL)
    {
        PbbGetNextPortEntry (pRBCurNode, &pPbbNextPortEntry);
        if (pPbbNextPortEntry == NULL)
        {
            u1IsLastPortEntry = PBB_TRUE;
        }
        else
        {
            *pu2NextPort = pPbbNextPortEntry->u2LocalPort;
            return PBB_SUCCESS;
        }
    }

    /* Get the First VIP */
    if (u1IsLastPortEntry == PBB_TRUE)
    {
        if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            i4Retval = PbbGetFirstVipEntry (&PbbVipFirstEntry);

            if (i4Retval == PBB_SUCCESS)
            {
                *pu2NextPort = PbbVipFirstEntry.u2Vip;
            }
        }

        return i4Retval;
    }

    /* Get the Next VIP in the same Context */
    if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        i4Retval = PbbValidateLocalVip (u2Port);
        if (i4Retval == PBB_SUCCESS)
        {
            i4Retval = PbbGetNextLocalVip (u2Port, pu2NextPort);
        }
    }

    return i4Retval;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetNextActiveIContext                         */
/*                                                                           */
/*    Description         : This function is used to get the next Active     */
/*                          I context present in the system.                   */
/*                                                                           */
/*    Input(s)            : u4CurrContextId - Current Context Id.            */
/*                                                                           */
/*    Output(s)           : pu4NextContextId - Next Context Id.              */
/*                                                                           */
/*    Global Variables Referred : gPbbGlobData.apPbbContextInfo              */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetNextActiveIContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4ContextId = PBB_INIT_VAL;

    for (u4ContextId = u4CurrContextId + 1;
         u4ContextId < PBB_MAX_CONTEXTS; u4ContextId++)
    {
        if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4ContextId);
            if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
            {
                *pu4NextContextId = u4ContextId;
                return PBB_SUCCESS;
            }
        }
    }
    return PBB_FAILURE;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextVipIfIndex                             */
/*    Description         : This function returns the next Vip from the 
                            Vip table                      */
/*    Input(s)            : None                                             */
/*    Output(s)           : Vip - Vip number If index
*                           pNextVip - Next Vip If Index                   */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                        */
/*****************************************************************************/

INT1
PbbGetNextVipIfIndex (INT4 i4Vip, INT4 *pNextVip)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbICompVipRBtreeNode PbbVipNextEntry;
    INT4                i4retVal = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Get the local port number */
    i4retVal = PbbVcmGetIfMapHlPortId (i4Vip, &u2LocalPort);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /*Fetch the pip entry from the port table for this local port number */
    PBB_GET_VIP_ENTRY (u2LocalPort, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        return PBB_FAILURE;
    }
    /* Fetch the next pip port entry with respect to current pip port entry */
    i4retVal = PbbGetNextVipEntry (*pPbbVipCurrEntry, &PbbVipNextEntry);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    i4retVal = PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                              PbbVipNextEntry.u2Vip, pNextVip);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : PbbGetNextLocalVip                               */
/*    Description         : This function returns the next Vip from the      */
/*                          Vip table                                        */
/*    Input(s)            : None                                             */
/*    Output(s)           : u2Vip - Vip Port                                 */
/*                          pu2NextVip - Next Vip Port                       */
/*    Returns             : PBB_SUCCESS/PBB_FAILURE                          */
/*****************************************************************************/
INT1
PbbGetNextLocalVip (UINT2 u2Vip, UINT2 *pu2NextVip)
{
    tPbbICompVipRBtreeNode *pPbbVipCurrEntry = NULL;
    tPbbICompVipRBtreeNode PbbVipNextEntry;
    INT4                i4retVal = PBB_INIT_VAL;

    PBB_GET_VIP_ENTRY (u2Vip, pPbbVipCurrEntry);
    if (pPbbVipCurrEntry == NULL)
    {
        return PBB_FAILURE;
    }
    i4retVal = PbbGetNextVipEntry (*pPbbVipCurrEntry, &PbbVipNextEntry);
    if (i4retVal == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    *pu2NextVip = PbbVipNextEntry.u2Vip;
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCompareIsidRBNodes                            */
/*                                                                           */
/*    Description         : This function is used to compare the two Isid    */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : pRBNode1, pRBNode2 Two ISID enteries             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - RBNode1 > RBNode2                            */
/*                        : -1 - RBNode1 < RBNode2                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCompareIsidRBNodes (tPbbIsidRBtreeNode * pRBNode1,
                       tPbbIsidRBtreeNode * pRBNode2)
{
    if (pRBNode1->u4Isid < pRBNode2->u4Isid)
    {
        return PBB_LESSER;
    }
    else if (pRBNode1->u4Isid > pRBNode2->u4Isid)
    {
        return PBB_GREATER;
    }
    return PBB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCompareIsidPortRBNodes                        */
/*                                                                           */
/*    Description         : This function is used to compare the two Isid    */
/*                          Port entries                                     */
/*                                                                           */
/*    Input(s)            :  pRBNode1, pRBNode2 Two ISID port enteries       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - RBNode1 > RBNode2                            */
/*                          -1 - RBNode1 < RBNode2                           */
/*                          0 - RBNode1 == RBNode12                          */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCompareIsidPortRBNodes (tPbbBackBoneSerInstEntry * pRBNode1,
                           tPbbBackBoneSerInstEntry * pRBNode2)
{
    if (pRBNode1->u4Isid < pRBNode2->u4Isid)
    {
        return PBB_LESSER;
    }
    else if (pRBNode1->u4Isid > pRBNode2->u4Isid)
    {
        return PBB_GREATER;
    }
    if (pRBNode1->u2LocalPort < pRBNode2->u2LocalPort)
    {
        return PBB_LESSER;
    }
    else if (pRBNode1->u2LocalPort > pRBNode2->u2LocalPort)
    {
        return PBB_GREATER;
    }

    return PBB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbCompareVipRBNodes                             */
/*                                                                           */
/*    Description         : This function is used to compare the two VIP     */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : RBNode1,RBNode2 - Two VIP enteries               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - pRBNode1 > pRBNode2                          */
/*                          -1 - pRBNode1 < pRBNode2                         */
/*                          0 - pRBNode1 == pRBNode12                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbCompareVipRBNodes (tPbbICompVipRBtreeNode * pRBNode1,
                      tPbbICompVipRBtreeNode * pRBNode2)
{
    if (pRBNode1->u2Vip < pRBNode2->u2Vip)
    {
        return PBB_LESSER;
    }
    else if (pRBNode1->u2Vip > pRBNode2->u2Vip)
    {
        return PBB_GREATER;
    }
    return PBB_EQUAL;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbComparePortRBNodes                             */
/*                                                                           */
/*    Description         : This function is used to compare the two VIP     */
/*                          entries                                          */
/*                                                                           */
/*    Input(s)            : RBNode1,RBNode2 - Two port enteries              */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : 1 - RBNode1 > RBNode2                            */
/*                          -1 - RBNode1 < RBNode2                           */
/*                          0 - RBNode1 == RBNode12                          */
/*                                                                           */
/*****************************************************************************/
INT4
PbbComparePortRBNodes (tPbbPortRBtreeNode * RBNode1,
                       tPbbPortRBtreeNode * RBNode2)
{

    if (RBNode1->u2LocalPort < RBNode2->u2LocalPort)
    {
        return PBB_LESSER;
    }
    else if (RBNode1->u2LocalPort > RBNode2->u2LocalPort)
    {
        return PBB_GREATER;
    }
    return PBB_EQUAL;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbFreeIsidRBTreeNode                             */
/*                                                                          */
/*    Description        : This function frees the memory allocated for VIP */
/*                           ISID RB tree                                     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbFreeIsidRBTreeNode (tPbbIsidRBtreeNode * ptobefreed, UINT4 u4arg)
{

    UINT2               u2Vip = PBB_INIT_VAL;
    tPbbICompVipRBtreeNode *pVipNode = NULL;
    tPbbICompVipRBtreeNode key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));

    u4arg = u4arg;
    if (ptobefreed == NULL)
    {
        return PBB_FAILURE;
    }
    if (ptobefreed->pu4PbbIsidVipInfo != NULL)
    {
        if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
        {
            /* Get the Vip entry */
            if (PbbGetVipForIsid (ptobefreed->u4Isid, &u2Vip) == PBB_SUCCESS)
            {
                key.u2Vip = u2Vip;
                pVipNode = (tPbbICompVipRBtreeNode *)
                    RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable, &key);
                if (pVipNode != NULL && pVipNode->pu4PbbIsidVipInfo == NULL)
                {
                    ptobefreed->pu4PbbIsidVipInfo = NULL;
                    return PBB_SUCCESS;
                }
            }
        }
        /*Freeing the allocated buffer */
        PBB_RELEASE_BUF (PBB_ISID_VIP_INFO,
                         (UINT1 *) (ptobefreed->pu4PbbIsidVipInfo));
        ptobefreed->pu4PbbIsidVipInfo = NULL;
        /* Delete the isid vip info pointer from Vip rb tree  */
        if (pVipNode != NULL)
        {
            pVipNode->pu4PbbIsidVipInfo = NULL;
        }
    }
    PBB_RELEASE_BUF (PBB_ISID_BUFF, (UINT1 *) (ptobefreed));
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteIsidRBTree                              */
/*                                                                          */
/*    Description        : This function is used to delete ISID             */
/*                         configuration saved all the  RBT node.           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
PbbDeleteIsidRBTree (VOID)
{
    RBTreeDrain (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                 (tRBKeyFreeFn) PbbFreeIsidRBTreeNode, 1);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbFreeIsidPortRBTreeNode                        */
/*                                                                          */
/*    Description        : This function frees the memory allocated for     */
/*                           ISID Port RB tree                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbFreeIsidPortRBTreeNode (tPbbBackBoneSerInstEntry * ptobefreed, UINT4 u4arg)
{

    u4arg = u4arg;
    if (ptobefreed == NULL)
    {
        return PBB_FAILURE;
    }
    PBB_RELEASE_BUF (PBB_PORT_ISID_BUFF, (UINT1 *) (ptobefreed));
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteIsidPortRBTree                          */
/*                                                                          */
/*    Description        : This function is used to delete ISID, Port       */
/*                         configuration saved all the  RBT node.           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
PbbDeleteIsidPortRBTree (VOID)
{

    RBTreeDrain (PBB_CURR_CONTEXT_PTR ()->IsidPortTable,
                 (tRBKeyFreeFn) PbbFreeIsidPortRBTreeNode, 1);
    return PBB_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbFreePortRBTreeNode                            */
/*                                                                          */
/*    Description        : This function frees the memory allocated for Port*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbFreePortRBTreeNode (tPbbPortRBtreeNode * ptobefreed, UINT4 u4arg)
{

    u4arg = u4arg;
    if (ptobefreed == NULL)
    {
        return PBB_FAILURE;
    }
    PBB_RELEASE_BUF (PBB_PORT_BUFF, (UINT1 *) (ptobefreed));
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbFreePipPcpTable                               */
/*                                                                          */
/*    Description        : This function frees the memory allocated for Pip
 *                         Pcp table                                         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbFreePipPcpTable (tPbbPcpData * ptobefreed, UINT4 u4arg)
{

    u4arg = u4arg;
    if (ptobefreed == NULL)
    {
        return PBB_FAILURE;
    }
    PBB_RELEASE_BUF (PBB_PIP_PCP_BUFF, (UINT1 *) (ptobefreed));
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeletePortRBTree                              */
/*                                                                          */
/*    Description        : This function is used to delete Port             */
/*                         configuration saved all the  RBT node.           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
PbbDeletePortRBTree (VOID)
{

    RBTreeDrain (PBB_CURR_CONTEXT_PTR ()->PortTable,
                 (tRBKeyFreeFn) PbbFreePortRBTreeNode, 1);
    return PBB_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbFreeVipRBTreeNode                             */
/*                                                                          */
/*    Description        : This function frees the memory allocated for VIP */
/*                           RB tree                                          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbFreeVipRBTreeNode (tPbbICompVipRBtreeNode * ptobefreed, UINT4 u4arg)
{

    tPbbIsidRBtreeNode *pIsidNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    u4arg = u4arg;
    if (ptobefreed == NULL)
    {
        return PBB_FAILURE;
    }

    if (ptobefreed->pu4PbbIsidVipInfo != NULL)
    {
        /* Get the Isid entry */
        pIsidVipInfo =
            (tPbbIsidVipInfo *) (VOID *) ptobefreed->pu4PbbIsidVipInfo;
        key.u4Isid = pIsidVipInfo->u4Isid;
        pIsidNode =
            (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                              IsidTable, &key);
        if (pIsidNode != NULL && pIsidNode->pu4PbbIsidVipInfo == NULL)
        {
            ptobefreed->pu4PbbIsidVipInfo = NULL;
            return PBB_SUCCESS;
        }

        /*Freeing the allocated buffer */
        PBB_RELEASE_BUF (PBB_ISID_VIP_INFO,
                         (UINT1 *) (ptobefreed->pu4PbbIsidVipInfo));
        ptobefreed->pu4PbbIsidVipInfo = NULL;
        /* Delete the isid vip info pointer from Isid rb tree  */
        if (pIsidNode != NULL)
        {
            pIsidNode->pu4PbbIsidVipInfo = NULL;
        }
    }
    PBB_RELEASE_BUF (PBB_VIP_BUFF, (UINT1 *) (ptobefreed));

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteVipRBTree                               */
/*                                                                          */
/*    Description        : This function is used to delete Port             */
/*                         configuration saved all the  RBT node.           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

INT1
PbbDeleteVipRBTree (VOID)
{

    RBTreeDrain (PBB_CURR_CONTEXT_PTR ()->VipTable,
                 (tRBKeyFreeFn) PbbFreeVipRBTreeNode, 1);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidatePort                                  */
/*                                                                          */
/*    Description        : This function is used to validate Port           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidatePort (INT4 i4ifIndex)
{
    tPbbPortRBtreeNode *pSearchRes = NULL;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2LocalPort = u2LocalPort;

    /*Searching Port in RB tree */
    pSearchRes =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pSearchRes == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidatePort                                  */
/*                                                                          */
/*    Description        : This function is used to validate Port           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbValidateLocalPort (UINT2 u2LocalPort)
{
    tPbbPortRBtreeNode *pSearchRes = NULL;
    tPbbPortRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));

    key.u2LocalPort = u2LocalPort;

    /*Searching Port in RB tree */
    pSearchRes =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pSearchRes == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateLocalVip                                  */
/*                                                                          */
/*    Description        : This function is used to validate Vip based on local port           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbValidateLocalVip (UINT2 u2LocalPort)
{
    tPbbICompVipRBtreeNode *pSearchRes = NULL;
    tPbbICompVipRBtreeNode key;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;

    /*Searching Port in RB tree */
    pSearchRes =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);

    if (pSearchRes == NULL)
    {
        /* Element not present in the key */
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateIsidPort                              */
/*                                                                          */
/*    Description        : This function is used to validate ISID and Port  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidateIsidPort (UINT4 u4Isid, INT4 i4ifIndex)
{
    tPbbBackBoneSerInstEntry *pSearchRes = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));

    if (PbbVcmGetContextInfoFromIfIndex (i4ifIndex, &u4ContextId,
                                         &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (u4ContextId != PBB_CURR_CONTEXT_ID ())
    {
        return PBB_FAILURE;
    }

    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;

    pSearchRes =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);

    if (pSearchRes == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteIsidRBNodes                             */
/*                                                                          */
/*    Description        : This function is used to delete a ISID node      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDeleteIsidRBNodes (UINT4 u4Isid)
{

    tPbbIsidRBtreeNode *ptobefreed = NULL;
    tPbbIsidRBtreeNode  key;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));

    key.u4Isid = u4Isid;
    ptobefreed = RBTreeRem (PBB_CURR_CONTEXT_PTR ()->IsidTable, &key);

    if (ptobefreed == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    if (PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent > PBB_INIT_VAL)
    {
        PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent--;
    }
    if (gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance > PBB_INIT_VAL)
    {
        gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance--;
    }
    PbbFreeIsidRBTreeNode (ptobefreed, 1);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteVipRBNodes                              */
/*                                                                          */
/*    Description        : This function is used to delete a VIP RB node    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteVipRBNodes (UINT2 u2Vip)
{
    tPbbICompVipRBtreeNode *ptobefreed = NULL;
    tPbbICompVipRBtreeNode key;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2Vip;

    ptobefreed = RBTreeRem (PBB_CURR_CONTEXT_PTR ()->VipTable, &key);
    if (ptobefreed == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    if (PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent > PBB_INIT_VAL)
    {
        PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent--;
    }
    if (gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance > PBB_INIT_VAL)
    {
        gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance--;
    }
    PbbFreeVipRBTreeNode (ptobefreed, 1);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeletePortRBNode                              */
/*                                                                          */
/*    Description        : This function is used to delete a VIP RB node    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeletePortRBNode (UINT2 u2LocalPort)
{

    tPbbPortRBtreeNode *ptobefreed = NULL;
    tPbbPortRBtreeNode  key;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    ptobefreed = RBTreeRem (PBB_CURR_CONTEXT_PTR ()->PortTable, &key);
    if (ptobefreed == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    PbbFreePortRBTreeNode (ptobefreed, 1);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteIsidPortRBNodes                         */
/*                                                                          */
/*    Description        :This function is used to delete a ISID Port RB node*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDeleteIsidPortRBNodes (INT4 i4ifIndex, UINT4 u4Isid)
{
    tPbbBackBoneSerInstEntry *ptobefreed = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;

    ptobefreed = RBTreeRem (PBB_CURR_CONTEXT_PTR ()->IsidPortTable, &key);
    if (ptobefreed == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    PbbFreeIsidPortRBTreeNode (ptobefreed, 1);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteIsidLocalPortRBNodes                    */
/*                                                                          */
/*    Description        :This function is used to delete a ISID Port RB node*/
/*                         Based on Local Port                              */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDeleteIsidLocalPortRBNodes (UINT2 u2LocalPort, UINT4 u4Isid)
{
    tPbbBackBoneSerInstEntry *ptobefreed = NULL;
    tPbbBackBoneSerInstEntry key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));

    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;

    ptobefreed = RBTreeRem (PBB_CURR_CONTEXT_PTR ()->IsidPortTable, &key);
    if (ptobefreed == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    PbbFreeIsidPortRBTreeNode (ptobefreed, 1);
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbAllocIsidRBNode                               */
/*                                                                          */
/*    Description        : This function Allocates memory for               */
/*                           ISID RB tree Node                                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

tPbbIsidRBtreeNode *
PbbAllocIsidRBNode ()
{
    tPbbIsidRBtreeNode *pRBNode = NULL;

    pRBNode =
        (tPbbIsidRBtreeNode *) (VOID *) PBB_GET_BUF (PBB_ISID_BUFF,
                                                     sizeof
                                                     (tPbbIsidRBtreeNode));
    if (pRBNode == NULL)
    {
        return NULL;
    }
    return pRBNode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbCreateIsidRBTreeNode                          */
/*                                                                          */
/*    Description        : This function Creates a ISID Node in RB tree     */
/*                           ISID RB tree                                     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbCreateIsidRBTreeNode (UINT4 u4Isid)
{
    UINT4               u4Result = PBB_INIT_VAL;
    tPbbIsidRBtreeNode *pRBNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;

    if (PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent >=
        gPbbGlobData.PbbTaskInfo.u4MaxNumIsidPerContext)
    {
        /*To Add a trace */
        return PBB_FAILURE;
    }
    pRBNode = PbbAllocIsidRBNode ();
    if (pRBNode == NULL)
    {
        return PBB_FAILURE;
    }
    pRBNode->u4Isid = u4Isid;
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNode->pu4PbbIsidVipInfo;
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) PBB_GET_BUF (PBB_ISID_VIP_INFO,
                                                  sizeof (tPbbIsidVipInfo));
    if (pIsidVipInfo == NULL)
    {
        PbbFreeIsidRBTreeNode (pRBNode, 1);
        return PBB_FAILURE;
    }
    pRBNode->pu4PbbIsidVipInfo = &(pIsidVipInfo->u4Isid);
    PBB_MEMSET (pIsidVipInfo, PBB_INIT_VAL, sizeof (tPbbIsidVipInfo));
    pIsidVipInfo->u4Isid = pRBNode->u4Isid;
    pIsidVipInfo->u4NumPortsPerIsidPerContextPresent = PBB_INIT_VAL;
    PBB_MEMSET (pIsidVipInfo->DefBackDestMac, PBB_INIT_VAL, sizeof (tMacAddr));
    pIsidVipInfo->unPbbCompData.ICompData.u1VipPortStatus =
        PBB_PORT_STATUS_DOWN;
    if (PBB_BRIDGE_MODE () != PBB_BCOMPONENT_BRIDGE_MODE)
    {
        PBB_DLL_INIT (&(pIsidVipInfo->unPbbCompData.ICompData.CnpPortTableDll));
        pIsidVipInfo->unPbbCompData.ICompData.u2Vip = PBB_INIT_VAL;
        pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus =
            PBB_INVALID_ROWSTATUS;
        pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus =
            PBB_INVALID_ROWSTATUS;
        pIsidVipInfo->unPbbCompData.ICompData.u2Pip = PBB_INIT_VAL;
        pIsidVipInfo->unPbbCompData.ICompData.u1VipType = PBB_VIP_TYPE_BOTH;
        pIsidVipInfo->unPbbCompData.ICompData.u1PipStorageType =
            PBB_STORAGE_TYPE_NONVOLATILE;
    }
    else
    {
        PBB_DLL_INIT (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll));
    }
    u4Result = RBTreeAdd (PBB_CURR_CONTEXT_PTR ()->IsidTable, pRBNode);
    if (u4Result == RB_FAILURE)
    {
        /* free the memory allocated for RBNode */
        PbbFreeIsidRBTreeNode (pRBNode, 1);
        return PBB_FAILURE;
    }
    gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance++;
    PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent++;
    return PBB_SUCCESS;
}

/*********************************************************************
*  Function Name : PbbAddCBPPortEntryDll 
*  Description   : Function to add the CBP port entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
PbbAddCBPPortEntryDll (tTMO_DLL_NODE * pNode, UINT4 u4Isid)
{
    tPbbCbpPortList    *pCurEntry = NULL;
    tPbbCbpPortList    *pInEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tTMO_DLL_NODE      *pPrevNode = NULL;
    tPbbIsidRBtreeNode *pRBCurIsidNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbIsidRBtreeNode  key1;
    PBB_MEMSET (&key1, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key1.u4Isid = u4Isid;

    pInEntry = (tPbbCbpPortList *) pNode;

    key1.u4Isid = u4Isid;
    pRBCurIsidNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key1);
    if (pRBCurIsidNode == NULL)
    {
        return;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pRBCurIsidNode->pu4PbbIsidVipInfo;

    PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll),
                  pLstNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbCbpPortList *) pLstNode;

        if (PbbCompareCBPPort (&(pInEntry->u2CBPPort),
                               &(pCurEntry->u2CBPPort)) == PBB_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    PBB_DLL_INSERT_NODE (&
                         (pIsidVipInfo->unPbbCompData.BCompData.
                          CbpPortTableDll), pPrevNode, pNode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbInsertCBPIsidRBTreeNode                       */
/*                                                                          */
/*    Description        : This function Insert a CBP port in ISID list     */
/*                           ISID RB tree                                     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbInsertCBPIsidRBTreeNode (UINT4 u4Isid, INT4 u4ifIndex)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry key;
    tPbbCbpPortList    *pCBPDllNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    if (PbbVcmGetIfMapHlPortId (u4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u4Isid = u4Isid;
    key.u2LocalPort = u2LocalPort;

    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);
    if (pRBCurNode == NULL)
    {

        if (PbbCreateIsidPortRBTreeNode (u4ifIndex, u4Isid) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        pCBPDllNode = PbbAllocMemDllNode ();
        if (pCBPDllNode == NULL)
        {
            return PBB_FAILURE;
        }
        pCBPDllNode->u2CBPPort = u2LocalPort;
        PbbAddCBPPortEntryDll (&(pCBPDllNode->link), u4Isid);
    }
    else
    {
        return PBB_SUCCESS;
    }
    if (PbbIncrNumPortPerIsidPerContext (u4Isid) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}

/*********************************************************************
*  Function Name : PbbAddCBPPortEntryDll 
*  Description   : Function to add the CBP port entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
PbbAddCnpPortEntryDll (tTMO_DLL_NODE * pNode, UINT4 u4Isid)
{
    tPbbCnpPortList    *pCurEntry = NULL;
    tPbbCnpPortList    *pInEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tTMO_DLL_NODE      *pPrevNode = NULL;
    tPbbIsidRBtreeNode *pRBCurIsidNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbIsidRBtreeNode  key1;
    PBB_MEMSET (&key1, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key1.u4Isid = u4Isid;

    pInEntry = (tPbbCnpPortList *) pNode;

    key1.u4Isid = u4Isid;
    pRBCurIsidNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key1);
    if (pRBCurIsidNode == NULL)
    {
        return;
    }
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) pRBCurIsidNode->pu4PbbIsidVipInfo;

    PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.ICompData.CnpPortTableDll),
                  pLstNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbCnpPortList *) pLstNode;

        if (PbbCompareCnpPort (&(pInEntry->u2CnpPort),
                               &(pCurEntry->u2CnpPort)) == PBB_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    PBB_DLL_INSERT_NODE (&
                         (pIsidVipInfo->unPbbCompData.ICompData.
                          CnpPortTableDll), pPrevNode, pNode);
}

/*********************************************************************
*  Function Name : PbbCreateCnpEntryDll 
*  Description   : Function to add the CNP port entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/
INT1
PbbCreateCnpEntryDll (UINT4 u4Isid, INT4 i4IfMainIndex)
{
    tPbbCnpPortList    *pCnpDllNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    pCnpDllNode = PbbAllocMemCnpDllNode ();
    if (pCnpDllNode == NULL)
    {
        return PBB_FAILURE;
    }
    pCnpDllNode->u2CnpPort = u2LocalPort;
    PbbAddCnpPortEntryDll (&(pCnpDllNode->link), u4Isid);
    return PBB_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbAllocVipRBNode                              */
/*                                                                          */
/*    Description        : This function Allocates memory for               */
/*                           Vip RB tree Node                                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

tPbbICompVipRBtreeNode *
PbbAllocVipRBNode ()
{
    tPbbICompVipRBtreeNode *pRBNode = NULL;

    pRBNode =
        (tPbbICompVipRBtreeNode *) (VOID *) PBB_GET_BUF (PBB_VIP_BUFF,
                                                         sizeof
                                                         (tPbbICompVipRBtreeNode));
    if (pRBNode == NULL)
    {
        return NULL;
    }
    return pRBNode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbCreateVipRBTreeNode                           */
/*                                                                          */
/*    Description        : This function Creates a VIP Node in VIP RB tree  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbCreateVipRBTreeNode (INT4 i4ifIndex)
{
    tPbbICompVipRBtreeNode *pRBNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT4               u4Result = PBB_INIT_VAL;
    UINT2               u2LocalVip = PBB_INIT_VAL;
    if (gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance >=
        gPbbGlobData.PbbTaskInfo.u4MaxIsid)
    {
        return PBB_FAILURE;
    }
    if (PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent >=
        gPbbGlobData.PbbTaskInfo.u4MaxNumIsidPerContext)
    {
        return PBB_FAILURE;
    }

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalVip) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    pRBNode = PbbAllocVipRBNode ();
    if (pRBNode == NULL)
    {
        return PBB_FAILURE;
    }
    pRBNode->u2Vip = (UINT4) u2LocalVip;
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNode->pu4PbbIsidVipInfo;
    pIsidVipInfo =
        (tPbbIsidVipInfo *) (VOID *) PBB_GET_BUF (PBB_ISID_VIP_INFO,
                                                  sizeof (tPbbIsidVipInfo));
    if (pIsidVipInfo == NULL)
    {
        PbbFreeVipRBTreeNode (pRBNode, 1);
        return PBB_FAILURE;
    }

    pRBNode->pu4PbbIsidVipInfo = &(pIsidVipInfo->u4Isid);
    PBB_MEMSET (pIsidVipInfo, PBB_INIT_VAL, sizeof (tPbbIsidVipInfo));
    pIsidVipInfo->u4NumPortsPerIsidPerContextPresent = PBB_INIT_VAL;
    PBB_MEMSET (pIsidVipInfo->DefBackDestMac, PBB_INIT_VAL, sizeof (tMacAddr));
    pIsidVipInfo->unPbbCompData.ICompData.u1VipPortStatus =
        PBB_PORT_STATUS_DOWN;

    /* Creating VIP with default values */
    if (PBB_BRIDGE_MODE () != PBB_BCOMPONENT_BRIDGE_MODE)
    {
        PBB_DLL_INIT (&(pIsidVipInfo->unPbbCompData.ICompData.CnpPortTableDll));
        pIsidVipInfo->u4Isid = PBB_ISID_INVALID_VALUE;
        pIsidVipInfo->unPbbCompData.ICompData.u2Vip = (UINT4) u2LocalVip;
        pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus =
            PBB_INVALID_ROWSTATUS;
        pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus =
            PBB_INVALID_ROWSTATUS;
        pIsidVipInfo->unPbbCompData.ICompData.u2Pip = PBB_INIT_VAL;
        pIsidVipInfo->unPbbCompData.ICompData.u1VipType = PBB_VIP_TYPE_BOTH;
        pIsidVipInfo->unPbbCompData.ICompData.u1PipStorageType =
            PBB_STORAGE_TYPE_NONVOLATILE;
    }
    else
    {
        PBB_DLL_INIT (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll));
    }

    u4Result = RBTreeAdd (PBB_CURR_CONTEXT_PTR ()->VipTable, pRBNode);
    if (u4Result == RB_FAILURE)
    {
        /* free the memory allocated for RBNode */
        PbbFreeVipRBTreeNode (pRBNode, 1);
        return PBB_FAILURE;
    }
    gPbbGlobData.PbbTaskInfo.u4NumOfIsidInstance++;
    PBB_CURR_CONTEXT_PTR ()->u4NumIsidPerContextPresent++;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbAllocPortRBNode                               */
/*                                                                          */
/*    Description        : This function Allocates memory for               */
/*                           Port RB tree Node                                */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

tPbbPortRBtreeNode *
PbbAllocPortRBNode ()
{
    tPbbPortRBtreeNode *pRBNode = NULL;

    pRBNode = (tPbbPortRBtreeNode *) (VOID *)
        PBB_GET_BUF (PBB_PORT_BUFF, sizeof (tPbbPortRBtreeNode));
    if (pRBNode == NULL)
    {
        return NULL;
    }
    return pRBNode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbAllocPipPcpTable                              */
/*                                                                          */
/*    Description        : This function Allocates memory for               */
/*                           Pip Pcp tables                                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

tPbbPcpData        *
PbbAllocPipPcpTable ()
{
    tPbbPcpData        *pPcpTable = NULL;

    pPcpTable = (tPbbPcpData *)
        PBB_GET_BUF (PBB_PIP_PCP_BUFF, sizeof (tPbbPcpData));
    if (pPcpTable == NULL)
    {
        return NULL;
    }
    return pPcpTable;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbCreatePortRBTreeNode                          */
/*                                                                          */
/*    Description        : This function Creates a Port RB tree Node        */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbCreatePortRBTreeNode (INT4 i4ifIndex, UINT2 u2LocalPort)
{
    tPbbPortRBtreeNode *pRBNode = NULL;
    tCfaIfInfo          CfaIfInfo;
    tPbbPcpData        *pPcpTable = NULL;
    UINT4               u4Result = PBB_INIT_VAL;
    PBB_MEMSET (&CfaIfInfo, PBB_INIT_VAL, sizeof (tCfaIfInfo));

    if (PbbCfaGetIfInfo (i4ifIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        return PBB_FAILURE;
    }

    pRBNode = PbbAllocPortRBNode ();
    if (pRBNode == NULL)
    {
        return PBB_FAILURE;
    }
    pRBNode->u2LocalPort = u2LocalPort;
    pRBNode->u4IfIndex = (UINT4) i4ifIndex;
    pRBNode->u1PortType = CfaIfInfo.u1BrgPortType;
    pRBNode->u1PortStatus = CfaIfInfo.u1IfOperStatus;
    if (pRBNode->u1PortType == PBB_CNP_PORTBASED_PORT ||
        pRBNode->u1PortType == PBB_CNP_STAGGED_PORT ||
        pRBNode->u1PortType == PBB_CNP_CTAGGED_PORT)
    {
        pRBNode->unPbbPortData.CnpData.u4Pisid = PBB_ISID_INVALID_VALUE;
        pRBNode->unPbbPortData.CnpData.u1PisidRowStatus = PBB_INVALID_ROWSTATUS;
    }
    else if (pRBNode->u1PortType == PBB_PROVIDER_INSTANCE_PORT)
    {
        pPcpTable = PbbAllocPipPcpTable ();
        if (pPcpTable == NULL)
        {
            PbbFreePortRBTreeNode (pRBNode, 1);
            return PBB_FAILURE;
        }
        pRBNode->unPbbPortData.PipData.pu4PcpData =
            (UINT4 *) (VOID *) pPcpTable;
        if (PbbInitPcpValues
            ((tPbbPcpData *) (pRBNode->unPbbPortData.PipData.pu4PcpData)) ==
            PBB_FAILURE)
        {
            PbbFreePortRBTreeNode (pRBNode, 1);
            PbbFreePipPcpTable (pPcpTable, 1);
            return PBB_FAILURE;
        }
        PBB_DLL_INIT (&(pRBNode->unPbbPortData.PipData.VipList));
        pRBNode->unPbbPortData.PipData.u1PipRowStatus = PBB_ACTIVE;
    }
    else if (pRBNode->u1PortType == CFA_CUSTOMER_BACKBONE_PORT)
    {
        pRBNode->unPbbPortData.CbpData.u1CBPTableRowstatus = PBB_INIT_VAL;
        PBB_DLL_INIT (&(pRBNode->unPbbPortData.CbpData.IsidList));
    }

    /*Adding a RB tree node */
    u4Result = RBTreeAdd (PBB_CURR_CONTEXT_PTR ()->PortTable, pRBNode);
    if (u4Result == RB_FAILURE)
    {
        /* free the memory allocated for RBNode */
        PbbFreePortRBTreeNode (pRBNode, 1);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbAllocIsidPortRBNode                           */
/*                                                                          */
/*    Description        : This function Allocates memory for               */
/*                           Isid Port RB tree Node                           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

tPbbBackBoneSerInstEntry *
PbbAllocIsidPortRBNode ()
{
    tPbbBackBoneSerInstEntry *pRBNode = NULL;

    pRBNode = (tPbbBackBoneSerInstEntry *) (VOID *)
        PBB_GET_BUF (PBB_PORT_ISID_BUFF, sizeof (tPbbBackBoneSerInstEntry));
    if (pRBNode == NULL)
    {
        return NULL;
    }
    return pRBNode;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbCreateIsidPortRBTreeNode                      */
/*                                                                          */
/*    Description        : This function Creates a ISID, Port RB tree Node  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbCreateIsidPortRBTreeNode (INT4 i4ifIndex, UINT4 u4Isid)
{
    tPbbBackBoneSerInstEntry *pRBNode = NULL;
    UINT4               u4Result = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    pRBNode = PbbAllocIsidPortRBNode ();
    if (pRBNode == NULL)
    {
        return PBB_FAILURE;
    }
    pRBNode->u4Isid = u4Isid;
    pRBNode->u2LocalPort = u2LocalPort;
    pRBNode->u2BVlanId = PBB_INIT_VAL;
    pRBNode->u4LocalIsid = PBB_ISID_INVALID_VALUE;
    pRBNode->u1CbpMappingType = PBB_EGRESS_INGRESS;
    pRBNode->u1CBPRowstatus = PBB_INVALID_ROWSTATUS;
    pRBNode->u1OUIRowstatus = PBB_INVALID_ROWSTATUS;
    PBB_MEMSET (pRBNode->au1IsidPortOUI, PBB_INIT_VAL, PBB_OUI_LENGTH);
    PBB_MEMSET (pRBNode->DefBackDestMac, PBB_INIT_VAL, sizeof (tMacAddr));

    u4Result = RBTreeAdd (PBB_CURR_CONTEXT_PTR ()->IsidPortTable, pRBNode);
    if (u4Result == RB_FAILURE)
    {
        /* free the memory allocated for RBNode */
        PbbFreeIsidPortRBTreeNode (pRBNode, 1);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetCbpForIsid                                 */
/*    Description         : This function is used to get the CBP List        */
/*                          corresponding to an ISID.                        */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : pu4Vip -VIP value                                */
/*                                                                           */
/*    Global Variables Referred : gPbbGlobData.apPbbContextInfo              */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : PBB_SUCCESS/PBB_FAILURE                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetCbpForIsid (UINT4 u4ContextId,
                  UINT4 u4Isid, tSNMP_OCTET_STRING_TYPE * pRetValCbpList)
{
    tPbbIsidRBtreeNode *pRBNodeCur = NULL;
    tPbbCbpPortList    *pCurEntry = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    pRetValCbpList->i4_Length = PBB_PORT_LIST_SIZE;
    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }

    if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        return PBB_FAILURE;
    }

    pRBNodeCur =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pRBNodeCur == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNodeCur->pu4PbbIsidVipInfo;
    PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll),
                  pLstNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbCbpPortList *) pLstNode;
        PBB_SET_MEMBER_PORT (pRetValCbpList->pu1_OctetList,
                             pCurEntry->u2CBPPort);
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateIsid                                  */
/*                                                                          */
/*    Description        : This function is used to validate ISID           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidateIsid (UINT4 u4Isid)
{
    tPbbIsidRBtreeNode *pSearchRes;
    tPbbIsidRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;
    /*Searching ISID in RB tree */
    pSearchRes =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pSearchRes == NULL)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_ISID);
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetFirstIsid                                  */
/*                                                                          */
/*    Description        : This function is used to Get first ISID          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetFirstIsid (UINT4 *u4Isid)
{
    tPbbIsidRBtreeNode *pRBCurNode = NULL;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                               IsidTable);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    *u4Isid = pRBCurNode->u4Isid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextIsid                                  */
/*                                                                          */
/*    Description        : This function is used to Get Next ISID           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNextIsid (UINT4 u4CurrIsid, UINT4 *u4NextIsid)
{
    tPbbIsidRBtreeNode *pRBCurNode = NULL;
    tPbbIsidRBtreeNode *pRBNextNode = NULL;
    tPbbIsidRBtreeNode *pRBFirstNode = NULL;
    tPbbIsidRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4CurrIsid;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        pRBFirstNode = (tPbbIsidRBtreeNode *)
            RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->IsidTable);
        if (pRBFirstNode == NULL)
        {
            return PBB_FAILURE;
        }
        *u4NextIsid = pRBFirstNode->u4Isid;
        return PBB_SUCCESS;
    }
    pRBNextNode =
        (tPbbIsidRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                              IsidTable, pRBCurNode,
                                              (tRBCompareFn)
                                              PbbCompareIsidRBNodes);

    if (pRBNextNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    *u4NextIsid = pRBNextNode->u4Isid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipForIsid                                  */
/*                                                                          */
/*    Description        : This function is used to Get VIP for an ISID     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipForIsid (UINT4 u4Isid, UINT2 *u2Vip)
{
    tPbbIsidRBtreeNode *pRBCurNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbIsidRBtreeNode  key;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    *u2Vip = pIsidVipInfo->unPbbCompData.ICompData.u2Vip;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidPortData                               */
/*                                                                          */
/*    Description        : This function is used to Get VIP for an ISID     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidPortData (UINT2 u2LocalPort,
                    UINT4 u4Isid, tPbbBackBoneSerInstEntry ** pIsidPortData)
{
    tPbbBackBoneSerInstEntry key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;

    /*getting first ISID in RB tree */
    *pIsidPortData =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);

    if (*pIsidPortData == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidPortDefBDA                             */
/*                                                                          */
/*    Description        : This function is used to get the default         */
/*                         backbone destination address of the CBP to use.  */
/*                                                                          */
/*    Input(s)           : u2LocalPort - Local port id of the CBP port      */
/*                         u4Isid      - ISID                               */
/*                                                                          */
/*    Output(s)          : DAMacAddr - Default dest B-MAC for the CBP       */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidPortDefBDA (UINT2 u2LocalPort, UINT4 u4Isid, tMacAddr * DAMacAddr)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;

    if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    PBB_MEMCPY (DAMacAddr, pRBCurNode->DefBackDestMac, sizeof (tMacAddr));

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidPortBVlanId                            */
/*                                                                          */
/*    Description        : This function is used to Get BVLAN for CBP port  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidPortBVlanId (INT4 i4ifIndex, UINT4 u4Isid, UINT4 *pu4BVlanId)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    *pu4BVlanId = (UINT4) pRBCurNode->u2BVlanId;

    return PBB_SUCCESS;
}

 /****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidPortServiceMapType                     */
/*                                                                          */
/*    Description        : This function is used to Get Service Map for 
 *                         CBP port     
 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidPortServiceMapType (INT4 i4ifIndex,
                              UINT4 u4Isid, UINT1 *u1ServiceMapType)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    *u1ServiceMapType = pRBCurNode->u1CbpMappingType;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidPortLocalIsid                          */
/*                                                                          */
/*    Description        : This function is used to Get Local Isid          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidPortLocalIsid (INT4 i4ifIndex, UINT4 u4Isid, UINT4 *u4LocalIsid)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    *u4LocalIsid = pRBCurNode->u4LocalIsid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetIsidPortLocalIsid                          */
/*                                                                          */
/*    Description        : This function is used to Set Local Isid          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetIsidPortLocalIsid (INT4 i4ifIndex, UINT4 u4Isid, UINT4 u4LocalIsid)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pRBCurNode->u4LocalIsid = u4LocalIsid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidPortCbpRowstatus                       */
/*                                                                          */
/*    Description        : This function is used to Get CBP rowstatus       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidPortCbpRowstatus (INT4 i4ifIndex, UINT4 u4Isid, UINT4 *u4Rowstatus)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pRBCurNode) != PBB_SUCCESS)
    {
        *u4Rowstatus = PBB_INVALID_ROWSTATUS;
        return PBB_SUCCESS;
    }
    *u4Rowstatus = (UINT4) pRBCurNode->u1CBPRowstatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetIsidPortCbpRowstatus                       */
/*                                                                          */
/*    Description        :This function is used to Set  rowstatus for an ISID*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetIsidPortCbpRowstatus (INT4 i4ifIndex, UINT4 u4Isid, UINT4 u4Rowstatus)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    if (u4Rowstatus == PBB_DESTROY)
    {
        pRBCurNode->u1CBPRowstatus = PBB_INVALID_ROWSTATUS;
        return PBB_SUCCESS;
    }
    pRBCurNode->u1CBPRowstatus = (UINT1) u4Rowstatus;
    return PBB_SUCCESS;
}

/*********************************************************************
*  Function Name : PbbDelCBPEntryDll 
*  Description   : Function to delete an entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/
VOID
PbbDelCBPEntryDll (tTMO_DLL_NODE * pNode, UINT4 u4Isid)
{
    tPbbIsidRBtreeNode *pRBCurNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    PBB_DLL_DEL (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll),
                 pNode);
    PBB_RELEASE_BUF (PBB_ISID_CBP_DLL_BUFF, (UINT1 *) (pNode));

    return;
}

/*********************************************************************
*  Function Name : PbbDelCBPEntryDll 
*  Description   : Function to delete an entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/
VOID
PbbDelCnpEntryDll (tTMO_DLL_NODE * pNode, UINT4 u4Isid)
{
    tPbbIsidRBtreeNode *pRBCurNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    PBB_DLL_DEL (&(pIsidVipInfo->unPbbCompData.ICompData.CnpPortTableDll),
                 pNode);
    PBB_RELEASE_BUF (PBB_ISID_CNP_DLL_BUFF, (UINT1 *) (pNode));

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteCBPNodeDLL                           */
/*                                                                          */
/*    Description        : This function is used to delete CBP Node from DLL */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDeleteCBPNodeDLL (UINT4 u4Isid, INT4 i4ifIndex)
{
    tTMO_DLL_NODE      *pListNode = NULL;
    tPbbCbpPortList    *pCBPPortDll = NULL;
    tPbbIsidRBtreeNode *pRBCurNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT2               u2LocalPortAddr = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    u2LocalPortAddr = u2LocalPort;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll),
                  pListNode, tTMO_DLL_NODE *)
    {
        pCBPPortDll = (tPbbCbpPortList *) pListNode;
        if (PbbCompareCBPPort (&u2LocalPortAddr,
                               &(pCBPPortDll->u2CBPPort)) == PBB_EQUAL)
        {
            PbbDelCBPEntryDll (&(pCBPPortDll->link), u4Isid);
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteCBPNodeLocalDLL                           */
/*                                                                          */
/*    Description        : This function is used to delete CBP Node from DLL */
/*                         based on local port                              */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDeleteCBPNodeLocalDLL (UINT4 u4Isid, UINT2 u2LocalPort)
{
    tTMO_DLL_NODE      *pListNode = NULL;
    tPbbCbpPortList    *pCBPPortDll = NULL;
    tPbbIsidRBtreeNode *pRBCurNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPortAddr = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    u2LocalPortAddr = u2LocalPort;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll),
                  pListNode, tTMO_DLL_NODE *)
    {
        pCBPPortDll = (tPbbCbpPortList *) pListNode;
        if (PbbCompareCBPPort (&u2LocalPortAddr,
                               &(pCBPPortDll->u2CBPPort)) == PBB_EQUAL)
        {
            PbbDelCBPEntryDll (&(pCBPPortDll->link), u4Isid);
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteCnpNodeDll                           */
/*                                                                          */
/*    Description        : This function is used to delete CNP Node from DLL */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteCnpNodeDll (INT4 i4ifIndex)
{
    tTMO_DLL_NODE      *pListNode = NULL;
    tPbbCnpPortList    *pCnpPortDll = NULL;
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbPortRBtreeNode *pPortEntry = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT4               u4CnpIfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        UNUSED_PARAM (pListNode);
        UNUSED_PARAM (pRBCurNode);
        UNUSED_PARAM (pCnpPortDll);
        UNUSED_PARAM (pIsidVipInfo);
        UNUSED_PARAM (pPortEntry);
        UNUSED_PARAM (u4Isid);
        UNUSED_PARAM (u4ContextId);
        UNUSED_PARAM (u4CnpIfIndex);
        return PBB_FAILURE;
    }
    key.u2Vip = u2LocalPort;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        UNUSED_PARAM (pListNode);
        UNUSED_PARAM (pCnpPortDll);
        UNUSED_PARAM (pIsidVipInfo);
        UNUSED_PARAM (pPortEntry);
        UNUSED_PARAM (u4Isid);
        UNUSED_PARAM (u4ContextId);
        UNUSED_PARAM (u4CnpIfIndex);
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    u4Isid = pIsidVipInfo->u4Isid;
    u4ContextId = PBB_CURR_CONTEXT_ID ();
    PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.ICompData.CnpPortTableDll),
                  pListNode, tTMO_DLL_NODE *)
    {
        pCnpPortDll = (tPbbCnpPortList *) pListNode;
        PBB_GET_PORT_ENTRY (pCnpPortDll->u2CnpPort, pPortEntry);
        if (pPortEntry == NULL)
        {
            UNUSED_PARAM (u4ContextId);
            UNUSED_PARAM (u4CnpIfIndex);
            return PBB_FAILURE;
        }
        u4CnpIfIndex = pPortEntry->u4IfIndex;

        if (nmhSetFsPbbPIsidRowStatus (u4ContextId, u4CnpIfIndex, PBB_DESTROY)
            == SNMP_FAILURE)
        {
            return PBB_FAILURE;
        }

        if (PbbSelectContext (u4ContextId))
        {
            return PBB_FAILURE;
        }
        return PBB_SUCCESS;
    }
    return PBB_SUCCESS;
}

/*********************************************************************
*  Function Name : PbbAddVipEntryDll 
*  Description   : Function to add the Vip entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
PbbAddVipEntryDll (tTMO_DLL_NODE * pNode, INT4 i4LocalPort)
{
    tPbbIsidPipVipNode *pCurEntry = NULL;
    tPbbIsidPipVipNode *pInEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tTMO_DLL_NODE      *pPrevNode = NULL;
    tPbbPortRBtreeNode *pRBPortNode = NULL;
    tPbbPortRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = i4LocalPort;

    pInEntry = (tPbbIsidPipVipNode *) pNode;

    pRBPortNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBPortNode == NULL)
    {
        return;
    }
    PBB_DLL_SCAN (&(pRBPortNode->unPbbPortData.PipData.VipList),
                  pLstNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbIsidPipVipNode *) pLstNode;

        if (PbbCompareVipEntry (&(pInEntry->unIsidVip.u2Vip),
                                &(pCurEntry->unIsidVip.u2Vip)) == PBB_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    PBB_DLL_INSERT_NODE (&(pRBPortNode->unPbbPortData.PipData.VipList),
                         pPrevNode, pNode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteVipNodeDll                             */
/*                                                                          */
/*    Description        : This function is used to delete Vip Node from DLL*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteVipNodeDll (UINT2 u2Vip, INT4 i4ifIndex)
{
    tPbbIsidPipVipNode *pListNode = NULL;
    tPbbIsidPipVipNode *pVipDllNode = NULL;
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2LocalPort = u2LocalPort;

    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }

    PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.PipData.VipList),
                  pListNode, tPbbIsidPipVipNode *)
    {
        pVipDllNode = (tPbbIsidPipVipNode *) pListNode;
        if (PbbCompareVipEntry (&u2Vip,
                                &(pVipDllNode->unIsidVip.u2Vip)) == PBB_EQUAL)
        {
            PbbDelVipEntryDll (&(pVipDllNode->link), u2LocalPort);
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/*********************************************************************
*  Function Name : PbbAddIsidEntryDll 
*  Description   : Function to add the CBP port entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
PbbAddIsidEntryDll (tTMO_DLL_NODE * pNode, INT4 i4LocalPort)
{
    tPbbIsidPipVipNode *pCurEntry = NULL;
    tPbbIsidPipVipNode *pInEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tTMO_DLL_NODE      *pPrevNode = NULL;
    tPbbPortRBtreeNode *pRBPortNode = NULL;
    tPbbPortRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = i4LocalPort;

    pInEntry = (tPbbIsidPipVipNode *) pNode;

    pRBPortNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBPortNode == NULL)
    {
        return;
    }
    PBB_DLL_SCAN (&(pRBPortNode->unPbbPortData.CbpData.IsidList),
                  pLstNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbIsidPipVipNode *) pLstNode;

        if (PbbCompareIsidEntry (&(pInEntry->unIsidVip.u4Isid),
                                 &(pCurEntry->unIsidVip.u4Isid)) == PBB_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    PBB_DLL_INSERT_NODE (&(pRBPortNode->unPbbPortData.CbpData.IsidList),
                         pPrevNode, pNode);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbInsertPipVipDll                              */
/*                                                                          */
/*    Description        : This function Insert a Vip in Pip Vip Map list   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbInsertPipVipDll (UINT2 u2Vip, INT4 u4ifIndex)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    tPbbIsidPipVipNode *pIsidDllNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (u4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);
    if (pRBCurNode == NULL)
    {
        return FAILURE;

    }
    else
    {
        pIsidDllNode = PbbAllocMemIsidVipDllNode ();
        if (pIsidDllNode == NULL)
        {
            return PBB_FAILURE;
        }
        pIsidDllNode->unIsidVip.u2Vip = u2Vip;
        PbbAddVipEntryDll (&(pIsidDllNode->link), u2LocalPort);
        return PBB_SUCCESS;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbInsertCBPIsidDll                              */
/*                                                                          */
/*    Description        : This function Insert a CBP port in ISID list     */
/*                           ISID RB tree                                     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbInsertCBPIsidDll (UINT4 u4Isid, INT4 u4ifIndex)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    tPbbIsidPipVipNode *pIsidDllNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (u4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;

    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);
    if (pRBCurNode == NULL)
    {
        return FAILURE;

    }
    else
    {
        pIsidDllNode = PbbAllocMemIsidVipDllNode ();
        if (pIsidDllNode == NULL)
        {
            return PBB_FAILURE;
        }
        pIsidDllNode->unIsidVip.u4Isid = u4Isid;
        PbbAddIsidEntryDll (&(pIsidDllNode->link), u2LocalPort);
        return PBB_SUCCESS;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetIsidPortBVlanId                            */
/*                                                                          */
/*    Description        : This function is used to Set  BVLAN for an  ISID */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetIsidPortBVlanId (INT4 i4ifIndex, UINT4 u4Isid, UINT4 u4BVlanId)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pRBCurNode->u2BVlanId = (UINT2) u4BVlanId;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetIsidPortDefBDA                             */
/*                                                                          */
/*    Description        : This function is used to Set Default backbone    */
/*                         destination address for the CBP Port             */
/*                                                                          */
/*    Input(s)           : u4IfIndex - IfIndex of the CBP port              */
/*                         u4Isid    - ISID                                 */
/*                         DAMacAddr - Default dest B-MAC for the CBP       */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetIsidPortDefBDA (INT4 i4ifIndex, UINT4 u4Isid, tMacAddr DefaultDstBMAC)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));

    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;

    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);

    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }

    PBB_MEMCPY (pRBCurNode->DefBackDestMac, DefaultDstBMAC, sizeof (tMacAddr));

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetIsidPortCBPServiceMapType                  */
/*                                                                          */
/*    Description        : This function is used to Set Service MapType  
 *                         for an ISID  
 */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetIsidPortCBPServiceMapType (INT4 i4ifIndex,
                                 UINT4 u4Isid,
                                 tSNMP_OCTET_STRING_TYPE * pServiceMappingType)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    key.u2LocalPort = u2LocalPort;
    key.u4Isid = u4Isid;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pRBCurNode->u1CbpMappingType = pServiceMappingType->pu1_OctetList[0];
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetIsidData                                   */
/*                                                                          */
/*    Description        : This function is used to Get ISID data           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetIsidData (UINT4 u4Isid, tPbbIsidRBtreeNode ** pIsidData)
{
    tPbbIsidRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    /*getting first ISID in RB tree */
    *pIsidData =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (*pIsidData == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipData                                    */
/*                                                                          */
/*    Description        : This function is used to Get VIP data            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipData (UINT2 u2Vip, tPbbICompVipRBtreeNode ** pVipData)
{
    tPbbICompVipRBtreeNode key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2Vip;
    *pVipData =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);

    if (*pVipData == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPortData                                  */
/*                                                                          */
/*    Description        : This function is used to Get Port data     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetPortData (UINT2 u2LocalPort, tPbbPortRBtreeNode ** pPortData)
{
    tPbbPortRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;

    /*getting first ISID in RB tree */
    *pPortData =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (*pPortData == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetFirstIsidPort                              */
/*                                                                          */
/*    Description        : This function is used to Get first CBP Port      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetFirstIsidPort (UINT4 *u4Isid, UINT2 *u2LocalPort)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;

    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                                     IsidPortTable);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    *u2LocalPort = pRBCurNode->u2LocalPort;
    *u4Isid = pRBCurNode->u4Isid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextIsidPort                               */
/*                                                                          */
/*    Description        : This function is used to Get Next entry from     */
/*                         ISIDPort Table                                   */
/*                                                                          */
/*    Input(s)           : u4CurrIsid - Current ISID                        */
/*                         u2LocalPort - Current Local Port of CBP          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNextIsidPort (UINT4 u4CurrIsid,
                    UINT4 *u4NextIsid, UINT2 u2LocalPort, UINT2 *pu2NextPort)
{
    tPbbBackBoneSerInstEntry *pRBNextNode = NULL;
    tPbbBackBoneSerInstEntry PbbBackBoneSerInstKey;

    PBB_MEMSET (&PbbBackBoneSerInstKey, PBB_INIT_VAL,
                sizeof (tPbbBackBoneSerInstEntry));
    PbbBackBoneSerInstKey.u4Isid = u4CurrIsid;
    PbbBackBoneSerInstKey.u2LocalPort = u2LocalPort;

    pRBNextNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                    IsidPortTable,
                                                    &PbbBackBoneSerInstKey,
                                                    (tRBCompareFn)
                                                    PbbCompareIsidPortRBNodes);

    if (pRBNextNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    *u4NextIsid = pRBNextNode->u4Isid;
    *pu2NextPort = pRBNextNode->u2LocalPort;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextPort                                   */
/*                                                                          */
/*    Description        : This function is used to Get Next Port           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNextPort (UINT2 u2CurrPort, INT4 *pi4NextifIndex)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode *pRBNextNode = NULL;
    tPbbPortRBtreeNode  key;
    INT4                i4ifIndex = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2CurrPort;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        if (PbbGetFirstPort (&i4ifIndex) == PBB_SUCCESS)
        {
            *pi4NextifIndex = i4ifIndex;
            return PBB_SUCCESS;
        }
        else
        {
            return PBB_FAILURE;
        }
    }
    pRBNextNode =
        (tPbbPortRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                              PortTable, pRBCurNode,
                                              (tRBCompareFn)
                                              PbbComparePortRBNodes);
    if (pRBNextNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    *pi4NextifIndex = (INT4) pRBNextNode->u4IfIndex;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextCnpPort                                */
/*                                                                          */
/*    Description        : This function is used to Get Next Cnp Port       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNextCnpPort (INT4 i4IfMainIndex, INT4 *pi4NextifIndex)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode *pRBNextNode = NULL;
    tPbbPortRBtreeNode  key;
    INT4                i4ifIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    if (PbbVcmGetIfMapHlPortId (i4IfMainIndex, &u2LocalPort) != PBB_SUCCESS)
    {
    }
    key.u2LocalPort = u2LocalPort;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        if (PbbGetFirstCnpPort (&i4ifIndex) == PBB_SUCCESS)
        {
            *pi4NextifIndex = i4ifIndex;
            return PBB_SUCCESS;
        }
        else
        {
            return PBB_FAILURE;
        }
    }

    while (1)
    {
        pRBNextNode =
            (tPbbPortRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                  PortTable, pRBCurNode,
                                                  (tRBCompareFn)
                                                  PbbComparePortRBNodes);
        if (pRBNextNode == NULL)
        {
            return PBB_FAILURE;
        }
        else
        {
            if (pRBNextNode->u1PortType == PBB_CNP_PORTBASED_PORT ||
                pRBNextNode->u1PortType == PBB_CNP_STAGGED_PORT ||
                pRBNextNode->u1PortType == PBB_CNP_CTAGGED_PORT)
            {
                *pi4NextifIndex = (INT4) pRBNextNode->u4IfIndex;
                return PBB_SUCCESS;

            }
            else
            {
                pRBCurNode = pRBNextNode;
                pRBNextNode = NULL;
                continue;

            }
        }

    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPortPisid                                  */
/*                                                                          */
/*    Description        : This function is used to Get Pisid corresponding */
/*                            to  port                                      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetPortPisid (INT4 i4ifIndex, UINT4 *pu4Isid)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    /*getting first ISID Port in RB tree */
    if (PbbGetPortData (u2LocalPort, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    *pu4Isid = pRBCurNode->unPbbPortData.CnpData.u4Pisid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPortPisid                                  */
/*                                                                          */
/*    Description        : This function is used to Set PISID               */
/*                            for a Port                                    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPortPisid (INT4 u4ifIndex, UINT4 u4Pisid)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (u4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pRBCurNode->unPbbPortData.CnpData.u4Pisid = u4Pisid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPortPisidRowStatus                         */
/*                                                                          */
/*    Description        : This function is used to Get Row status corresponding 
                            to  port*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetPortPisidRowStatus (INT4 i4ifIndex, UINT4 *pu4RowStatus)
{
    UINT2               u2LocalPort = PBB_INIT_VAL;
    tPbbPortRBtreeNode *pRBCurNode = NULL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetPortData (u2LocalPort, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    *pu4RowStatus = (UINT4) pRBCurNode->unPbbPortData.CnpData.u1PisidRowStatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVIPIsidPIsid                               */
/*                                                                          */
/*    Description        : This function is used to Get PISID corresponding */
/*                           to ISID                                        */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVIPIsidPIsid (INT4 i4ifIndex, UINT4 *u4Pisid)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetPortData (u2LocalPort, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    *u4Pisid = pRBCurNode->unPbbPortData.CnpData.u4Pisid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetVipIsidPIsidRowStatus                      */
/*                                                                          */
/*    Description        : This function is used to Set PISID row status    */
/*                            for a Port                                    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetVipIsidPIsidRowStatus (INT4 i4ifIndex, UINT4 u4RowStatus)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    if (u4RowStatus == PBB_ACTIVE)
    {
        /* Increment the count of number of PISID in this Context */
        ++(PBB_CURR_CONTEXT_PTR ()->u2PisidCount);
        PbbL2IwfSetAllToOneBndlStatus (PBB_CURR_CONTEXT_ID (), OSIX_TRUE);

    }
    if (u4RowStatus == PBB_DESTROY)
    {
        u4RowStatus = PBB_INVALID_ROWSTATUS;

        /* Decrement the count of number of PISID in this Context */
        if ((PBB_CURR_CONTEXT_PTR ()->u2PisidCount) > 0)
        {
            --(PBB_CURR_CONTEXT_PTR ()->u2PisidCount);
        }

        if ((PBB_CURR_CONTEXT_PTR ()->u2PisidCount) == PBB_INIT_VAL)
        {
            PbbL2IwfSetAllToOneBndlStatus (PBB_CURR_CONTEXT_ID (), OSIX_FALSE);
        }
    }

    pRBCurNode->unPbbPortData.CnpData.u1PisidRowStatus = (UINT1) u4RowStatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetFirstPort                                  */
/*                                                                          */
/*    Description        : This function is used to Get first Port from     */
/*                           Port RB tree                                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetFirstPort (INT4 *pi4ifIndex)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                               PortTable);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    *pi4ifIndex = (INT4) pRBCurNode->u4IfIndex;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetFirstPort                                  */
/*                                                                          */
/*    Description        : This function is used to Get first Port from     */
/*                           Port RB tree                                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetFirstCnpPort (INT4 *pi4ifIndex)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode *pRBNextNode = NULL;

    /*getting first Port in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                               PortTable);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    if (pRBCurNode->u1PortType == PBB_CNP_PORTBASED_PORT ||
        pRBCurNode->u1PortType == PBB_CNP_STAGGED_PORT ||
        pRBCurNode->u1PortType == PBB_CNP_CTAGGED_PORT)
    {
        *pi4ifIndex = (INT4) pRBCurNode->u4IfIndex;
        return PBB_SUCCESS;

    }
    while (1)
    {
        pRBNextNode =
            (tPbbPortRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                  PortTable, pRBCurNode,
                                                  (tRBCompareFn)
                                                  PbbComparePortRBNodes);
        if (pRBNextNode == NULL)
        {
            return PBB_FAILURE;
        }
        else
        {
            if (pRBNextNode->u1PortType == PBB_CNP_PORTBASED_PORT ||
                pRBNextNode->u1PortType == PBB_CNP_STAGGED_PORT ||
                pRBNextNode->u1PortType == PBB_CNP_CTAGGED_PORT)
            {
                *pi4ifIndex = (INT4) pRBNextNode->u4IfIndex;
                return PBB_SUCCESS;

            }
            else
            {
                pRBCurNode = pRBNextNode;
                pRBNextNode = NULL;
                continue;

            }
        }

    }

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetFirstVip                                   */
/*                                                                          */
/*    Description        : This function is used to Get first VIP           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetFirstVip (UINT4 *pu4Vip)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode *pRBNextNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalVip = PBB_INIT_VAL;
    INT4                i4ifIndex = PBB_INIT_VAL;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                                   VipTable);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    u2LocalVip = pRBCurNode->u2Vip;
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;

    while (pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus ==
           PBB_INVALID_ROWSTATUS)
    {
        pRBNextNode =
            (tPbbICompVipRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                      VipTable, pRBCurNode,
                                                      (tRBCompareFn)
                                                      PbbCompareVipRBNodes);
        if (pRBNextNode == NULL)
        {
            return PBB_FAILURE;
        }
        u2LocalVip = pRBNextNode->u2Vip;
        pIsidVipInfo =
            (tPbbIsidVipInfo *) (VOID *) pRBNextNode->pu4PbbIsidVipInfo;
        pRBCurNode = pRBNextNode;
    }
    if (PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                       u2LocalVip, &i4ifIndex) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    *pu4Vip = i4ifIndex;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextVip                                    */
/*                                                                          */
/*    Description        : This function is used to Get Next VIP            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetNextVip (UINT4 u4ContextId,
               UINT4 *pu4NextContextid, INT4 i4CurrVip, UINT2 *u2NextVip)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode *pRBGetFirstNode = NULL;
    tPbbICompVipRBtreeNode *pRBNextNode = NULL;
    tPbbICompVipRBtreeNode key;
    UINT4               u4CurrPort = PBB_INIT_VAL;
    UINT4               u4NextContext = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    u4CurrPort = i4CurrVip;
    if (PbbVcmGetIfMapHlPortId (u4CurrPort, &u2LocalPort) != PBB_SUCCESS)
    {
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);

    if (pRBCurNode == NULL)
    {
        pRBGetFirstNode = RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->VipTable);
        if (pRBGetFirstNode == NULL)
        {
            return PBB_FAILURE;
        }
        *u2NextVip = pRBGetFirstNode->u2Vip;
        *pu4NextContextid = u4ContextId;
        return PBB_SUCCESS;
    }
    else
    {
        pRBNextNode =
            (tPbbICompVipRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                      VipTable, pRBCurNode,
                                                      (tRBCompareFn)
                                                      PbbCompareVipRBNodes);
        if (pRBNextNode == NULL)
        {
            while (PbbGetNextActiveIContext (u4ContextId, &u4NextContext) ==
                   PBB_SUCCESS)
            {
                PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4NextContext);
                pRBGetFirstNode =
                    RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->VipTable);
                if (pRBGetFirstNode == NULL)
                {
                    PBB_CURR_CONTEXT_PTR () = NULL;
                    u4ContextId = u4NextContext;
                    u4NextContext = PBB_INIT_VAL;
                    continue;
                }
                else
                {
                    *u2NextVip = pRBGetFirstNode->u2Vip;
                    *pu4NextContextid = u4NextContext;
                    return PBB_SUCCESS;
                }
            }

        }
        else
        {
            *u2NextVip = pRBNextNode->u2Vip;
            *pu4NextContextid = u4ContextId;
            return PBB_SUCCESS;
        }

    }
    return PBB_FAILURE;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipRowStatus                               */
/*                                                                          */
/*    Description        : This function is used to Get VIP Row Status      */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipRowStatus (INT4 i4ifIndex, UINT4 *pu4VipRowStatus)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2Vip = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2Vip) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (PbbGetVipData ((UINT4) u2Vip, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    *pu4VipRowStatus =
        (UINT4) pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetVipRowstatus                               */
/*                                                                          */
/*    Description        : This function is used to Set row status          */
/*                            for an VIP                                    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbSetVipRowstatus (INT4 i4ifIndex, UINT4 u4RowStatus)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    if (u4RowStatus == PBB_DESTROY)
    {
        pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus =
            PBB_INVALID_ROWSTATUS;
        return PBB_SUCCESS;
    }
    pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus = (UINT1) u4RowStatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetVipIsidDefaultBDAddr                       */
/*                                                                          */
/*    Description        : This function is used to Set default BDA         */
/*                            for an ISID                                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetVipIsidDefaultBDAddr (UINT4 u4Isid, tMacAddr DefaultDstBMAC)
{
    tPbbIsidRBtreeNode *pRBCurNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;

    PBB_MEMCPY (pIsidVipInfo->DefBackDestMac,
                DefaultDstBMAC, sizeof (tMacAddr));
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateVip                                   */
/*                                                                          */
/*    Description        : This function is used to validate VIP            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidateVip (INT4 i4ifIndex)
{
    tPbbICompVipRBtreeNode *pSearchRes = NULL;
    tPbbICompVipRBtreeNode key;
    UINT2               u2Vip = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2Vip) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = (UINT4) u2Vip;

    /*Searching Port in RB tree */
    pSearchRes =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);

    if (pSearchRes == NULL)
    {
        /* Element not present in the key */
        CLI_SET_ERR (CLI_PBB_INVALID_VIP);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipPipIfIndex                       */
/*                                                                          */
/*    Description        : This function is used to get pip Value          */
/*                            for an ISID                                   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetVipPipIfIndex (INT4 i4ifIndex, INT4 *pi4PipIfIndex)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT2               u2Pip = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
    if (PbbVcmGetIfIndexFromLocalPort (PBB_CURR_CONTEXT_ID (),
                                       u2Pip, pi4PipIfIndex) != PBB_SUCCESS)
    {
        return PBB_SUCCESS;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipDefaultDstBMAC                          */
/*                                                                          */
/*    Description        : This function is used to get Mac Address         */
/*                           associated with VIP                            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipDefaultDstBMAC (INT4 i4ifIndex, tMacAddr * VipDefaultDstBMAC)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    PBB_MEMCPY (VipDefaultDstBMAC,
                pIsidVipInfo->DefBackDestMac, sizeof (tMacAddr));

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipType                                    */
/*                                                                          */
/*    Description        : This function is used to get VIP Type            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipType (INT4 i4ifIndex, UINT1 *u1VipType)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    *u1VipType = pIsidVipInfo->unPbbCompData.ICompData.u1VipType;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetVipDefaultDstBMAC                          */
/*                                                                          */
/*    Description        : This function is used to Set destination Mac     */
/*                           address                                          */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetVipDefaultDstBMAC (INT4 i4ifIndex, tMacAddr DefaultBackboneDest)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    PBB_MEMCPY (pIsidVipInfo->DefBackDestMac,
                DefaultBackboneDest, sizeof (tMacAddr));
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPbbVipType                                  */
/*                                                                          */
/*    Description        : This function is used to Set VIP Type            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPbbVipType (INT4 i4ifIndex, UINT1 u1VipType)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    pIsidVipInfo->unPbbCompData.ICompData.u1VipType = u1VipType;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPbbVipISid                                 */
/*                                                                          */
/*    Description        : This function is used to Set VIP ISID            */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPbbVipISid (INT4 i4ifIndex, UINT4 u4Isid)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    pIsidVipInfo->u4Isid = u4Isid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbCreateIsidNodeFromVip                         */
/*                                                                          */
/*    Description        : This function Creates a ISID Node From VIP       */
/*                           ISID RB tree                                     */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbCreateIsidNodeFromVip (INT4 i4ifIndex, UINT4 u4Isid)
{
    tPbbIsidRBtreeNode *pRBNode = NULL;
    tPbbICompVipRBtreeNode *pRBNodeVip = NULL;
    tPbbICompVipRBtreeNode key;
    UINT4               u4Result = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;

    /*RB tree node of VIP */
    pRBNodeVip =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);

    if (pRBNodeVip == NULL)
    {
        return PBB_FAILURE;
    }

    /*Allocatig Memory for Isid */
    pRBNode = PbbAllocIsidRBNode ();
    if (pRBNode == NULL)
    {
        return PBB_FAILURE;
    }
    pRBNode->u4Isid = u4Isid;
    pRBNode->pu4PbbIsidVipInfo = pRBNodeVip->pu4PbbIsidVipInfo;

    /*Creating RB tree node of Isid */
    u4Result = RBTreeAdd (PBB_CURR_CONTEXT_PTR ()->IsidTable, pRBNode);
    if (u4Result == RB_FAILURE)
    {
        /* free the memory allocated for RBNode */
        PbbFreeIsidRBTreeNode (pRBNode, 1);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteIsidNodeDll                             */
/*                                                                          */
/*    Description        : This function is used to delete ISID Node from DLL*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteIsidNodeDll (UINT4 u4Isid, INT4 i4ifIndex)
{
    tPbbIsidPipVipNode *pListNode = NULL;
    tPbbIsidPipVipNode *pIsidDllNode = NULL;
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }

    PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.CbpData.IsidList),
                  pListNode, tPbbIsidPipVipNode *)
    {
        pIsidDllNode = (tPbbIsidPipVipNode *) pListNode;
        if (PbbCompareIsidEntry (&u4Isid,
                                 &(pIsidDllNode->unIsidVip.u4Isid)) ==
            PBB_EQUAL)
        {
            PbbDelIsidEntryDll (&(pIsidDllNode->link), u2LocalPort);
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteLocalIsidNodeDll                        */
/*                                                                          */
/*    Description        : This function is used to delete ISID Node from DLL*/
/*                         Based on Local Port                              */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteLocalIsidNodeDll (UINT4 u4Isid, UINT2 u2LocalPort)
{
    tPbbIsidPipVipNode *pListNode = NULL;
    tPbbIsidPipVipNode *pIsidDllNode = NULL;
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }

    PBB_DLL_SCAN (&(pRBCurNode->unPbbPortData.CbpData.IsidList),
                  pListNode, tPbbIsidPipVipNode *)
    {
        pIsidDllNode = (tPbbIsidPipVipNode *) pListNode;
        if (PbbCompareIsidEntry (&u4Isid,
                                 &(pIsidDllNode->unIsidVip.u4Isid)) ==
            PBB_EQUAL)
        {
            PbbDelIsidEntryDll (&(pIsidDllNode->link), u2LocalPort);
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/*********************************************************************
*  Function Name : PbbDelVipEntryDll 
*  Description   : Function to delete an entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/
VOID
PbbDelVipEntryDll (tTMO_DLL_NODE * pNode, UINT2 u2LocalPort)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return;
    }
    PBB_DLL_DEL (&(pRBCurNode->unPbbPortData.PipData.VipList), pNode);
    PBB_RELEASE_BUF (PBB_ISID_PIP_VIP_DLL_BUFF, pNode);

    return;
}

/*********************************************************************
*  Function Name : PbbDelIsidEntryDll 
*  Description   : Function to delete an entry from the list 
*  Parameter(s)  : pNode - Node to be deleted
*  Return Values : None
*********************************************************************/
VOID
PbbDelIsidEntryDll (tTMO_DLL_NODE * pNode, UINT2 u2LocalPort)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return;
    }
    PBB_DLL_DEL (&(pRBCurNode->unPbbPortData.CbpData.IsidList), pNode);
    PBB_RELEASE_BUF (PBB_ISID_PIP_VIP_DLL_BUFF, pNode);

    return;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPortCbpTableRowstatus                     */
/*                                                                          */
/*    Description        : This function is used to Get CBP Table rowstatus */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetPortCbpTableRowstatus (INT4 i4ifIndex, UINT4 *u4Rowstatus)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetPortData (u2LocalPort, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    *u4Rowstatus =
        (UINT1) pRBCurNode->unPbbPortData.CbpData.u1CBPTableRowstatus;
    if (*u4Rowstatus == PBB_INVALID_ROWSTATUS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_ROWSTATUS);
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPortCbpTableRowstatus                      */
/*                                                                          */
/*    Description        : This function is used to Set CBP row status      */
/*                            for a Port                                    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPortCbpTableRowstatus (INT4 i4ifIndex, UINT4 u4RowStatus)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }

    pRBCurNode->unPbbPortData.CbpData.u1CBPTableRowstatus = (UINT1) u4RowStatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipValue                                   */
/*                                                                          */
/*    Description        : This function return the value of VIP assocated  */
/*                          with this ISID                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetVipValue (UINT4 u4ContextId, UINT4 u4Isid, UINT2 *pu2Vip)
{
    tPbbIsidRBtreeNode *pRBNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4ContextId);

    pRBNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBNode == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNode->pu4PbbIsidVipInfo;
    *pu2Vip = pIsidVipInfo->unPbbCompData.ICompData.u2Vip;
    return PBB_SUCCESS;
}

/****************************************************************************
 Function    :  PbbGetValidIsidPortOUI
 Description :  To get Valid OUI for a Isid
 Input       :  Context Id
                Isid
                Interface Index
                
 Output      :  OUI
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT1
PbbGetValidIsidPortOUI (UINT4 u4Isid, UINT2 u2LocalPort)
{
    tPbbBackBoneSerInstEntry *pPbbIsidPortEntry = NULL;

    if (PbbGetIsidPortData (u2LocalPort, u4Isid, &pPbbIsidPortEntry) !=
        PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (pPbbIsidPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    if (pPbbIsidPortEntry->u1OUIRowstatus == PBB_INVALID_ROWSTATUS)
    {
        return PBB_FAILURE;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbGetPipIsidDataWithVip                          */
/*                                                                          */
/*     DESCRIPTION      : This function will be called by VLAN to get       */
/*                         shutdown  status                                 */
/*                                                                          */
/*     INPUT            :                                                   */
/*     OUTPUT           :                                                   */
/*                                                                          */
/*     RETURNS          : PBB_TRUE/PBB_FALSE                                */
/*                                                                          */
/****************************************************************************/
INT4
PbbGetPipIsidDataWithVip (UINT4 u4ContextId,
                          UINT2 u2Vip,
                          UINT4 *pu4PipIndex,
                          UINT4 *pu4Isid, tMacAddr * pBDaAddr)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode *pRBPipNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tMacAddr            tempMacAddr;
    tMacAddr            DefaultBackboneDest;
    tSNMP_OCTET_STRING_TYPE pOUI;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT4               u4SizeofMac = PBB_SIZE_OF_MAC_ADDR;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT2               u2PipPort = PBB_INIT_VAL;
    UINT1               u1ByteIndex = PBB_INIT_VAL;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    pOUI.i4_Length = PBB_OUI_LENGTH;
    pOUI.pu1_OctetList = au1OUI;

    PBB_MEMSET (tempMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (DefaultBackboneDest, PBB_INIT_VAL, sizeof (tMacAddr));
    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }

    if (PbbGetVipData (u2Vip, &pRBCurNode) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    *pu4Isid = (UINT4) pIsidVipInfo->u4Isid;
    u2PipPort = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;

    /* Get the Pip IfIndex from the Port RBTree */
    if (PbbGetPortData (u2PipPort, &pRBPipNode) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }

    *pu4PipIndex = pRBPipNode->u4IfIndex;

    u4Isid = (UINT4) pIsidVipInfo->u4Isid;

    if (PBB_MEMCMP (pIsidVipInfo->DefBackDestMac, tempMacAddr,
                    sizeof (tMacAddr)) == PBB_INIT_VAL)
    {
        if (PbbGetPbbGlbOUI (&pOUI) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        PBB_MEMCPY (DefaultBackboneDest, pOUI.pu1_OctetList, PBB_OUI_LENGTH);
        DefaultBackboneDest[0] = DefaultBackboneDest[0] | 0x01;
        for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
        {
            u1ByteIndex = 0xFF & u4Isid;
            DefaultBackboneDest[u4Index] = u1ByteIndex;
            u4Isid = u4Isid >> 8;
            u1ByteIndex = PBB_INIT_VAL;
        }
        PBB_MEMCPY (pBDaAddr, DefaultBackboneDest, sizeof (tMacAddr));
    }
    else
    {
        PBB_MEMCPY (pBDaAddr,
                    &(pIsidVipInfo->DefBackDestMac), sizeof (tMacAddr));
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbGetCbpPortListForIsid                          */
/*                                                                          */
/*     DESCRIPTION      : This function will be called by VLAN to get       */
/*                         shutdown  status                                 */
/*                                                                          */
/*     INPUT            :                                                   */
/*     OUTPUT           :                                                   */
/*                                                                          */
/*     RETURNS          : PBB_TRUE/PBB_FALSE                                */
/*                                                                          */
/****************************************************************************/
INT4
PbbGetCbpPortListForIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                          UINT4 u4Isid, tLocalPortList OutPortList)
{
    tPbbIsidRBtreeNode *pRBNodeCur = NULL;
    tPbbCbpPortList    *pCurEntry = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    UNUSED_PARAM (u2LocalPort);
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;
    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        CLI_SET_ERR (CLI_PBB_INVALID_CONTEXT);
        return PBB_FAILURE;
    }

    pRBNodeCur =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pRBNodeCur == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNodeCur->pu4PbbIsidVipInfo;
    PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll),
                  pLstNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbCbpPortList *) pLstNode;
        PBB_SET_MEMBER_PORT (OutPortList, pCurEntry->u2CBPPort);
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipISid                                    */
/*                                                                          */
/*    Description        : This function is used to get Isid assciated       */
/*                             with VIP                                    */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipISid (INT4 i4ifIndex, UINT4 *pu4Isid)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    *pu4Isid = pIsidVipInfo->u4Isid;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPortRBtreeStatus                           */
/*                                                                          */
/*    Description        : This function sets the value of PBB port status  */
/*                                                                          */
/*    Input(s)           : u2LocalPort u1PortStatus                         */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPortRBtreeStatus (UINT2 u2LocalPort, UINT1 u1PortStatus)
{
    tPbbPortRBtreeNode *pRBNode = NULL;
    tPbbPortRBtreeNode  key;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbPortRBtreeNode));
    key.u2LocalPort = u2LocalPort;
    pRBNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);
    if (pRBNode == NULL)
    {
        return PBB_FAILURE;
    }
    pRBNode->u1PortStatus = u1PortStatus;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetVipPortStatus                              */
/*                                                                          */
/*    Description     : This function sets the value of PBB VIP port status */
/*                                                                          */
/*    Input(s)           : u2LocalPort u1PortStatus                         */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetVipPortStatus (UINT2 u2LocalPort, UINT1 u1PortStatus)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    pIsidVipInfo->unPbbCompData.ICompData.u1VipPortStatus = u1PortStatus;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateCNPPort
 */
/*                                                                          */
/*    Description        :This function is used to check for pisid port type*/
/*                                                                          */
/*    Input(s)           : u2LocalPort u1PortStatus                         */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidateCNPPort (INT4 i4ifIndex)
{
    tPbbPortRBtreeNode *pRBNode;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = 0;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2LocalPort = u2LocalPort;

    /*Searching Port in RB tree */
    pRBNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    if (pRBNode->u1PortType == PBB_CNP_PORTBASED_PORT ||
        pRBNode->u1PortType == PBB_CNP_STAGGED_PORT ||
        pRBNode->u1PortType == PBB_CNP_CTAGGED_PORT)
    {
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidateCbpPort
 */
/*                                                                          */
/*    Description        :This function is used to validate CBP port type   */
/*                                                                          */
/*    Input(s)           : u2LocalPort u1PortStatus                         */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidateCbpPort (INT4 i4ifIndex)
{
    tPbbPortRBtreeNode *pRBNode;
    tPbbPortRBtreeNode  key;
    UINT2               u2LocalPort = 0;

    if (PbbVcmGetIfMapHlPortId (i4ifIndex, &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2LocalPort = u2LocalPort;

    /*Searching Port in RB tree */
    pRBNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key);

    if (pRBNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    if (pRBNode->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNumPortPerIsidPerContext                   */
/*                                                                          */
/*    Description        : This function return the value for number of port*/
/*                          per Isid per context  */
/*                          with this ISID                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNumPortPerIsidPerContext (UINT4 u4ContextId, UINT4 u4Isid,
                                UINT4 *u4NumofPortPerIsidPerContext)
{
    tPbbIsidRBtreeNode *pRBNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;
    if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    pRBNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBNode == NULL)
    {
        return PBB_SUCCESS;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNode->pu4PbbIsidVipInfo;
    *u4NumofPortPerIsidPerContext =
        pIsidVipInfo->u4NumPortsPerIsidPerContextPresent;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbIncrNumPortPerIsidPerContext                   */
/*                                                                          */
/*    Description        :This function Increment the value for number of port*/
/*                         per Isid per context                            */
/*                          with this ISID                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbIncrNumPortPerIsidPerContext (UINT4 u4Isid)
{
    tPbbIsidRBtreeNode *pRBNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;
    pRBNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBNode == NULL)
    {
        return PBB_SUCCESS;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNode->pu4PbbIsidVipInfo;
    pIsidVipInfo->u4NumPortsPerIsidPerContextPresent++;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDecrNumPortPerIsidPerContext                   */
/*                                                                          */
/*    Description        :This function decrement the value for number of port*/
/*                         per Isid per context                            */
/*                          with this ISID                                  */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDecrNumPortPerIsidPerContext (UINT4 u4Isid)
{
    tPbbIsidRBtreeNode *pRBNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;
    pRBNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBNode == NULL)
    {
        return PBB_SUCCESS;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNode->pu4PbbIsidVipInfo;
    if (pIsidVipInfo->u4NumPortsPerIsidPerContextPresent > PBB_INIT_VAL)
    {
        pIsidVipInfo->u4NumPortsPerIsidPerContextPresent--;
    }
    else
    {
        PBB_TRC_ARG3 (MGMT_TRC | ALL_FAILURE_TRC, "%s :: %s() :: Failed, since"
                      "the value of u4NumPortsPerIsidPerContextPresent value : %d is"
                      "  corrupted \n",
                      __FILE__, PBB_FUNCTION_NAME,
                      pIsidVipInfo->u4NumPortsPerIsidPerContextPresent);
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMaxNumPortPerIsid                          */
/*                                                                          */
/*    Description        :This function return the value for max number of port*/
/*                         per Isid                                         */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetMaxNumPortPerIsid (UINT4 *u4NumOfPortsPerIsidPresent, UINT4 u4Isid,
                         UINT4 u4Contextid)
{
    tPbbIsidRBtreeNode *pRBNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT4               u4NumOfPorts = PBB_INIT_VAL;
    UINT4               u4NumOfPortsPerIsid = PBB_INIT_VAL;

    for (u4NumOfPorts = 0; u4NumOfPorts < PBB_MAX_CONTEXTS; u4NumOfPorts++)
    {
        if (PBB_CONTEXT_PTR (u4NumOfPorts) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4NumOfPorts);
            key.u4Isid = u4Isid;
            pRBNode =
                (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                  IsidTable, &key);
            if (pRBNode == NULL)
            {
                continue;
            }
            pIsidVipInfo =
                (tPbbIsidVipInfo *) (VOID *) pRBNode->pu4PbbIsidVipInfo;
            u4NumOfPortsPerIsid +=
                pIsidVipInfo->u4NumPortsPerIsidPerContextPresent;

        }
        PBB_CURR_CONTEXT_PTR () = NULL;
        pIsidVipInfo = NULL;
        pRBNode = NULL;
    }
    if (PbbSelectContext (u4Contextid) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    *u4NumOfPortsPerIsidPresent = u4NumOfPortsPerIsid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetRelayIsidFromLocalIsid                     */
/*                                                                          */
/*    Description        :This function return the value for Relay Isid     */
/*                        corresponding to Local Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetRelayIsidFromLocalIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                              UINT4 u4LocalIsid, UINT4 *pu4RelayIsid)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry tempRBNextNode;
    PBB_MEMSET (&tempRBNextNode, PBB_INIT_VAL,
                sizeof (tPbbBackBoneSerInstEntry));
    if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                                     IsidPortTable);
    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }
    if (pRBCurNode->u2LocalPort == u2LocalPort)
    {
        if (pRBCurNode->u4LocalIsid == u4LocalIsid)
        {
            *pu4RelayIsid = pRBCurNode->u4Isid;
            return PBB_SUCCESS;
        }
    }

    tempRBNextNode.u2LocalPort = pRBCurNode->u2LocalPort;
    tempRBNextNode.u4Isid = pRBCurNode->u4Isid;
    while (1)
    {

        pRBCurNode =
            (tPbbBackBoneSerInstEntry *)
            RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->IsidPortTable,
                           &tempRBNextNode,
                           (tRBCompareFn) PbbCompareIsidPortRBNodes);

        if (pRBCurNode != NULL)
        {
            if (pRBCurNode->u2LocalPort == u2LocalPort)
            {
                if (pRBCurNode->u4LocalIsid == u4LocalIsid)
                {
                    *pu4RelayIsid = pRBCurNode->u4Isid;
                    return PBB_SUCCESS;
                }
            }

            tempRBNextNode.u2LocalPort = pRBCurNode->u2LocalPort;
            tempRBNextNode.u4Isid = pRBCurNode->u4Isid;
        }
        else
        {
            break;
        }
    }
    UNUSED_PARAM (u2LocalPort);
    *pu4RelayIsid = u4LocalIsid;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteBCompdataFromHw                         */
/*                                                                          */
/*    Description        : This function is used to delete B component data */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteBCompdataFromHw (UINT4 u4ContextId)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry *pRBNextNode = NULL;
    UINT4               u4Isid = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                                     IsidPortTable);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_SUCCESS;
    }
    u4Isid = pRBCurNode->u4Isid;
    u2LocalPort = pRBCurNode->u2LocalPort;
    if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                       u2LocalPort, &i4IfIndex) != PBB_SUCCESS)
    {
        return PBB_SUCCESS;
    }
    if (pRBCurNode->u1CBPRowstatus == PBB_ACTIVE)
    {
        if (PbbDelBCompHwServiceInst (u4ContextId, i4IfIndex, u4Isid) !=
            PBB_SUCCESS)
        {
        }
    }
    while (1)
    {
        pRBNextNode =
            (tPbbBackBoneSerInstEntry *)
            RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->IsidPortTable, pRBCurNode,
                           (tRBCompareFn) PbbCompareIsidPortRBNodes);
        if (pRBNextNode == NULL)
        {
            return PBB_SUCCESS;
        }
        u4Isid = pRBNextNode->u4Isid;
        u2LocalPort = pRBNextNode->u2LocalPort;
        if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                           u2LocalPort,
                                           &i4IfIndex) != PBB_SUCCESS)
        {
        }
        if (pRBNextNode->u1CBPRowstatus == PBB_ACTIVE)
        {
            if (PbbDelBCompHwServiceInst (u4ContextId, i4IfIndex, u4Isid) !=
                PBB_SUCCESS)
            {
            }
        }
        pRBCurNode = pRBNextNode;
        pRBNextNode = NULL;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteICompdataFromHw                         */
/*                                                                          */
/*    Description        : This function is used to delete I component data */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteICompdataFromHw (UINT4 u4ContextId)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode *pRBNextNode = NULL;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2Vip = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    INT4                i4IfIndex = PBB_INIT_VAL;
    UINT2               u2Pip = PBB_INIT_VAL;

    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                                   VipTable);

    if (pRBCurNode == NULL)
    {
        return PBB_SUCCESS;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    u2Vip = pRBCurNode->u2Vip;
    u4Isid = pIsidVipInfo->u4Isid;
    u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
    if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                       u2Vip, &i4IfIndex) != PBB_SUCCESS)
    {
        return PBB_SUCCESS;
    }

    if (pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus == PBB_ACTIVE)
    {
        if (PbbDelICompHwServiceInst (u4ContextId, i4IfIndex) != PBB_SUCCESS)
        {

        }
    }
    if (pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus == PBB_ACTIVE)
    {
        if (PbbHwDeleteVipToPip (u4ContextId,
                                 u4Isid, u2Vip, u2Pip) == PBB_FAILURE)
        {
        }
    }
    while (1)
    {
        pIsidVipInfo = NULL;
        pRBNextNode =
            (tPbbICompVipRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                      VipTable, pRBCurNode,
                                                      (tRBCompareFn)
                                                      PbbCompareVipRBNodes);
        if (pRBNextNode == NULL)
        {
            return PBB_SUCCESS;
        }
        pIsidVipInfo =
            (tPbbIsidVipInfo *) (VOID *) pRBNextNode->pu4PbbIsidVipInfo;
        u2Vip = pRBCurNode->u2Vip;
        u4Isid = pIsidVipInfo->u4Isid;
        u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;

        if (PbbVcmGetIfIndexFromLocalPort (u4ContextId,
                                           u2Vip, &i4IfIndex) != PBB_SUCCESS)
        {
            continue;
        }
        if (pIsidVipInfo->unPbbCompData.ICompData.u1VipRowstatus == PBB_ACTIVE)
        {
            if (PbbDelICompHwServiceInst (u4ContextId, i4IfIndex) !=
                PBB_SUCCESS)
            {

            }
        }
        if (pIsidVipInfo->unPbbCompData.ICompData.u1VipPipRowstatus
            == PBB_ACTIVE)
        {
            if (PbbHwDeleteVipToPip (u4ContextId,
                                     u4Isid, u2Vip, u2Pip) == PBB_FAILURE)
            {
            }
        }
        pRBCurNode = pRBNextNode;
        pRBNextNode = NULL;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteICompdataFromHw                         */
/*                                                                          */
/*    Description        : This function is used to delete I component data */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbDeleteICompPisiddataFromHw (UINT4 u4ContextId)
{
    tPbbPortRBtreeNode *pRBCurNode = NULL;
    tPbbPortRBtreeNode *pRBNextNode = NULL;
    UINT4               u4Pisid = PBB_INIT_VAL;
    UINT4               u4IfIndex = PBB_INIT_VAL;

    /*getting first Port in RB tree */
    pRBCurNode =
        (tPbbPortRBtreeNode *) RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->
                                               PortTable);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_SUCCESS;
    }
    u4IfIndex = pRBCurNode->u4IfIndex;
    if ((pRBCurNode->u1PortType == PBB_CNP_PORTBASED_PORT) ||
        (pRBCurNode->u1PortType == PBB_CNP_STAGGED_PORT) ||
        (pRBCurNode->u1PortType == PBB_CNP_CTAGGED_PORT))
    {
        if (PbbGetPortPisid (u4IfIndex, &u4Pisid) == PBB_SUCCESS)
        {
            if (u4Pisid != PBB_ISID_INVALID_VALUE)
            {
                if (PbbDelHwPortPisid (u4ContextId,
                                       pRBCurNode->u4IfIndex,
                                       u4Pisid) != PBB_SUCCESS)
                {
                    return PBB_FAILURE;
                }
            }
        }
    }
    while (1)
    {
        pRBNextNode =
            (tPbbPortRBtreeNode *) RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->
                                                  PortTable, pRBCurNode,
                                                  (tRBCompareFn)
                                                  PbbComparePortRBNodes);
        if (pRBNextNode == NULL)
        {
            return PBB_SUCCESS;
        }
        u4IfIndex = pRBCurNode->u4IfIndex;
        if ((pRBCurNode->u1PortType == PBB_CNP_PORTBASED_PORT) ||
            (pRBCurNode->u1PortType == PBB_CNP_STAGGED_PORT) ||
            (pRBCurNode->u1PortType == PBB_CNP_CTAGGED_PORT))
        {
            if (PbbGetPortPisid (u4IfIndex, &u4Pisid) == PBB_SUCCESS)
            {
                if (u4Pisid != PBB_ISID_INVALID_VALUE)
                {
                    if (PbbDelHwPortPisid (u4ContextId,
                                           u4IfIndex, u4Pisid) != PBB_SUCCESS)
                    {

                    }
                }
            }
        }
        pRBCurNode = pRBNextNode;
        pRBNextNode = NULL;
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbInitPcpValues                                 */
/*                                                                          */
/*    Description        : This function is used to initialize pcp values   */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbInitPcpValues (tPbbPcpData * pPcpData)
{
    UINT1               u1PcpSelRow = PBB_INIT_VAL;
    UINT1               u1PcpVal = PBB_INIT_VAL;
    UINT1               u1Priority = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;

    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }

    /* Initialise PCP decoding table */
    for (u1PcpSelRow = PBB_INIT_VAL; u1PcpSelRow < PBB_MAX_NUM_PCP_SEL_ROW;
         u1PcpSelRow++)
    {
        for (u1PcpVal = PBB_INIT_VAL; u1PcpVal < PBB_MAX_NUM_PCP; u1PcpVal++)
        {
            pPcpData->au1PcpDecoding[u1PcpSelRow][u1PcpVal].u1Priority
                = gPbbGlobData.au1PcpDecPriority[u1PcpSelRow][u1PcpVal];

            pPcpData->au1PcpDecoding[u1PcpSelRow]
                [u1PcpVal].u1DropEligible
                = gPbbGlobData.au1PcpDecDropEligible[u1PcpSelRow][u1PcpVal];
        }
    }

    /* Initialise PCP encoding table */

    for (u1PcpSelRow = PBB_INIT_VAL; u1PcpSelRow < PBB_MAX_NUM_PCP_SEL_ROW;
         u1PcpSelRow++)
    {
        for (u1Priority = PBB_INIT_VAL; u1Priority < PBB_MAX_NUM_PRIORITY;
             u1Priority++)
        {
            for (u1DropEligible = PBB_INIT_VAL;
                 u1DropEligible < PBB_MAX_NUM_DROP_ELIGIBLE; u1DropEligible++)
            {
                pPcpData->au1PcpEncoding[u1PcpSelRow]
                    [u1Priority]
                    [u1DropEligible]
                    = gPbbGlobData.au1PcpEncValue[u1PcpSelRow]
                    [u1Priority][u1DropEligible];
            }
        }
    }
    pPcpData->u1PcpSelRow = PBB_8P0D_SEL_ROW;
    pPcpData->u1UseDei = PBB_VLAN_SNMP_FALSE;
    pPcpData->u1ReqDropEncoding = PBB_VLAN_SNMP_FALSE;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPcpDecodingPriority                         */
/*                                                                          */
/*    Description        : This function is used to get pcp decoding 
 *                         priority value                                    */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection Row
 *                         Pcp Value
 */
/*    Output(s)          : Pcp Priority                                      */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetPcpDecodingPriority (UINT2 u2LocalPort,
                           UINT1 u1PcpSelRow,
                           UINT1 u1PcpVal, UINT1 *pu1PcpPriority)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    if ((u1PcpSelRow > PBB_MAX_NUM_PCP_SEL_ROW) || (u1PcpSelRow == 0))
    {
        return PBB_FAILURE;

    }

    *pu1PcpPriority = (pPcpData->au1PcpDecoding[u1PcpSelRow - 1]
                       [u1PcpVal]).u1Priority;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPcpDecodingDE                               */
/*                                                                          */
/*    Description        : This function is used to get pcp decoding 
 *                         drop eligible value                               */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection Row
 *                         Pcp Value
 */
/*    Output(s)          : Pcp DE                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetPcpDecodingDE (UINT2 u2LocalPort,
                     UINT1 u1PcpSelRow, UINT1 u1PcpVal, UINT1 *pu1PcpDE)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    if ((u1PcpSelRow > PBB_MAX_NUM_PCP_SEL_ROW) || (u1PcpSelRow == 0))
    {
        return PBB_FAILURE;

    }

    *pu1PcpDE = (pPcpData->au1PcpDecoding[u1PcpSelRow - 1]
                 [u1PcpVal]).u1DropEligible;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPcpDecodingPriority                         */
/*                                                                          */
/*    Description        : This function is used to set pcp decoding 
 *                         priority value                                    */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection Row
 *                         Pcp Value
 *                         Pcp Priority
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPcpDecodingPriority (UINT2 u2LocalPort,
                           UINT1 u1PcpSelRow,
                           UINT1 u1PcpVal, UINT1 u1PcpPriority)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    if ((u1PcpSelRow > PBB_MAX_NUM_PCP_SEL_ROW) || (u1PcpSelRow == 0))
    {
        return PBB_FAILURE;
    }

    (pPcpData->au1PcpDecoding[u1PcpSelRow - 1][u1PcpVal]).u1Priority
        = u1PcpPriority;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPcpDecodingDE                               */
/*                                                                          */
/*    Description        : This function is used to set pcp decoding 
 *                         drop eligible value                               */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection Row
 *                         Pcp Value
 *                         Pcp DE
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPcpDecodingDE (UINT2 u2LocalPort,
                     UINT1 u1PcpSelRow, UINT1 u1PcpVal, UINT1 u1PcpDE)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    if ((u1PcpSelRow > PBB_MAX_NUM_PCP_SEL_ROW) || (u1PcpSelRow == 0))
    {
        return PBB_FAILURE;
    }

    (pPcpData->au1PcpDecoding[u1PcpSelRow - 1][u1PcpVal]).u1DropEligible
        = u1PcpDE;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbValidatePcpDecodingValues                      */
/*                                                                          */
/*    Description        : This function is used to validate pcp decoding 
 *                         values                                            */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection Row
 *                         Pcp Value
 *                         Pcp Priority
 *                         Pcp DE
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbValidatePcpDecodingValues (UINT2 u2LocalPort,
                              UINT1 u1PcpSelRow,
                              UINT1 u1PcpVal,
                              UINT1 u1InPriority, UINT1 u1InDropEligible)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    UINT1               u1Pcp = PBB_INIT_VAL;
    UINT1               u1PrioVal = PBB_INIT_VAL;
    UINT1               u1DropEligible = PBB_INIT_VAL;
    INT1                i1retVal = PBB_INIT_VAL;

    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }

    /* The lower of any two PCP shall not map to a higher priority than 
     * the higher PCP.
     */
    for (u1Pcp = 0; u1Pcp < PBB_MAX_NUM_PCP; u1Pcp++)
    {
        i1retVal = PbbGetPcpDecodingPriority (u2LocalPort,
                                              u1PcpSelRow, u1Pcp, &u1PrioVal);
        if (i1retVal == PBB_FAILURE)
        {
            return PBB_FAILURE;
        }

        if (u1PcpVal < u1Pcp)
        {
            /* Configured PCP is lower */
            if (u1InPriority > u1PrioVal)
            {
                /* Priority is high */
                return PBB_FAILURE;
            }
        }
    }

    if ((u1PcpVal != 0) && (u1PcpVal != 1))
    {
        for (u1Pcp = 2; u1Pcp < PBB_MAX_NUM_PCP; u1Pcp++)
        {
            i1retVal = PbbGetPcpDecodingPriority (u2LocalPort,
                                                  u1PcpSelRow,
                                                  u1Pcp, &u1PrioVal);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            i1retVal = PbbGetPcpDecodingDE (u2LocalPort,
                                            u1PcpSelRow,
                                            u1Pcp, &u1DropEligible);
            if (i1retVal == PBB_FAILURE)
            {
                return PBB_FAILURE;
            }
            if (u1PcpVal > u1Pcp)
            {
                if (u1InPriority == u1PrioVal)
                {

                    if ((u1InDropEligible == PBB_VLAN_SNMP_TRUE)
                        && (u1DropEligible == PBB_DE_FALSE))
                    {
                        /* Lower PCP maps to the same priority with drop eligible 
                         * false. Return failure.
                         */
                        return PBB_FAILURE;
                    }
                }
            }
            if (u1PcpVal < u1Pcp)
            {
                if (u1InPriority == u1PrioVal)
                {
                    if ((u1InDropEligible == PBB_VLAN_SNMP_FALSE)
                        && (u1DropEligible == PBB_DE_TRUE))
                    {
                        /* Higher PCP maps to the same priority with drop eligible 
                         * True. Return failure.
                         */
                        return PBB_FAILURE;
                    }
                }
            }
        }
    }
    return PBB_SUCCESS;

}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextPcpDecodingIndices                      */
/*                                                                          */
/*    Description        : This function is used to get next indices of  
 *                         pcp decoding table                                */
/*                                                                          */
/*    Input(s)           : If Index
 *                         Next If Index 
 *                         Pcp Selection Row
 *                         Next Pcp Selection Row
 *                         Pcp Value
 *                         Next Pcp Value
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetNextPcpDecodingIndices (INT4 i4IfIndex,
                              INT4 *pi4NextIfIndex,
                              UINT1 u1PcpSelRow,
                              UINT1 *pu1NextPcpSelRow,
                              UINT1 u1PcpVal, UINT1 *pu1NextPcpVal)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate that the local port exists for interface index */
    if (i4IfIndex <= 0)
    {
        if (PbbGetFirstPbbPipIfIndex (pi4NextIfIndex) == PBB_SUCCESS)
        {
            *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
            *pu1NextPcpVal = PBB_MIN_PRIORITY;
            return PBB_SUCCESS;
        }
        return PBB_FAILURE;
    }
    if ((INT1) u1PcpSelRow < 0)
    {
        u1PcpSelRow = 0;
    }
    if ((INT1) u1PcpVal < 0)
    {
        u1PcpVal = -1;
    }
    if (PbbGetPipIfIndex (i4IfIndex) == PBB_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        do
        {
            if (*pi4NextIfIndex != i4IfIndex)
            {
                *pu1NextPcpVal = PBB_MIN_PRIORITY;
                *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
                return PBB_SUCCESS;
            }
            if (u1PcpVal < PBB_MAX_PCP_VAL && u1PcpSelRow <= PBB_5P3D_SEL_ROW)
            {
                if (u1PcpSelRow == 0)
                {
                    *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
                    *pu1NextPcpVal = PBB_MIN_PRIORITY;
                }
                else
                {
                    *pu1NextPcpVal = u1PcpVal + 1;
                    *pu1NextPcpSelRow = u1PcpSelRow;
                }
                return PBB_SUCCESS;
            }
            if (u1PcpVal >= PBB_MAX_PCP_VAL && u1PcpSelRow < PBB_5P3D_SEL_ROW)

            {
                *pu1NextPcpSelRow = u1PcpSelRow + 1;
                *pu1NextPcpVal = PBB_MIN_PCP_VAL;
                return PBB_SUCCESS;
            }
            i4IfIndex = *pi4NextIfIndex;
        }
        while (PbbGetNextPbbPipIfIndex (i4IfIndex, pi4NextIfIndex) !=
               PBB_FAILURE);
    }
    else
    {
        if (PbbVcmGetContextInfoFromIfIndex (i4IfIndex,
                                             &u4ContextId,
                                             &u2LocalPort) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (u4ContextId > PBB_CURR_CONTEXT_ID ())
        {
            return PBB_FAILURE;
        }
        if (PbbGetFirstPbbPipIfIndex (pi4NextIfIndex) == PBB_SUCCESS)
        {
            *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
            *pu1NextPcpVal = PBB_MIN_PRIORITY;
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextPcpEncodingIndices                      */
/*                                                                          */
/*    Description        : This function is used to get next indices of  
 *                         pcp encoding table                                */
/*                                                                          */
/*    Input(s)           : If Index
 *                         Next If Index 
 *                         Pcp Selection Row
 *                         Next Pcp Selection Row
 *                         Pcp Priority
 *                         Next Priority Value
 *                         Pcp DE
 *                         Next Pcp DE
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNextPcpEncodingIndices (INT4 i4IfIndex,
                              INT4 *pi4NextIfIndex,
                              UINT1 u1PcpSelRow,
                              UINT1 *pu1NextPcpSelRow,
                              UINT1 u1PcpPriority,
                              UINT1 *pu1NextPcpPriority,
                              UINT1 u1PcpDE, UINT1 *pu1NextPcpDE)
{
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    /* Validate that the local port exists for interface index */
    if (i4IfIndex <= 0)
    {
        if (PbbGetFirstPbbPipIfIndex (pi4NextIfIndex) == PBB_SUCCESS)
        {
            *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
            *pu1NextPcpPriority = PBB_MIN_PRIORITY;
            *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
            return PBB_SUCCESS;
        }
        return PBB_FAILURE;
    }
    if ((INT1) u1PcpSelRow < 0)
    {
        u1PcpSelRow = 0;
    }
    if ((INT1) u1PcpPriority < 0)
    {
        u1PcpPriority = -1;
    }
    if ((INT1) u1PcpDE < 0)
    {
        u1PcpDE = PBB_INVALID_DE;
    }
    if (PbbGetPipIfIndex (i4IfIndex) == PBB_SUCCESS)
    {
        *pi4NextIfIndex = i4IfIndex;
        do
        {
            if (*pi4NextIfIndex != i4IfIndex)
            {
                *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
                *pu1NextPcpPriority = PBB_MIN_PRIORITY;
                *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
                return PBB_SUCCESS;
            }
            if (u1PcpDE == PBB_VLAN_SNMP_TRUE &&
                u1PcpPriority < PBB_MAX_PRIORITY)
            {
                if (u1PcpSelRow == PBB_INVALID_SEL_ROW)
                {
                    *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
                    *pu1NextPcpPriority = PBB_MIN_PRIORITY;
                    *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
                    return PBB_SUCCESS;

                }
                else
                {
                    if ((INT1) u1PcpDE == PBB_INVALID_DE)
                    {
                        *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
                    }
                    else
                    {
                        *pu1NextPcpDE = PBB_VLAN_SNMP_FALSE;
                    }
                    if ((INT1) u1PcpPriority == -1)
                    {
                        *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
                        *pu1NextPcpPriority = PBB_MIN_PRIORITY;
                    }
                    else
                    {
                        *pu1NextPcpPriority = u1PcpPriority;
                    }
                    *pu1NextPcpSelRow = u1PcpSelRow;
                    return PBB_SUCCESS;
                }
            }
            if (u1PcpDE == PBB_VLAN_SNMP_FALSE &&
                u1PcpPriority < PBB_MAX_PRIORITY)
            {
                if (u1PcpSelRow == PBB_INVALID_SEL_ROW)
                {
                    *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
                    *pu1NextPcpPriority = PBB_MIN_PRIORITY;
                    *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;

                }
                else
                {
                    *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
                    *pu1NextPcpPriority = u1PcpPriority + 1;
                    *pu1NextPcpSelRow = u1PcpSelRow;
                }
                return PBB_SUCCESS;
            }
            if (u1PcpPriority == PBB_MAX_PRIORITY &&
                u1PcpSelRow <= PBB_5P3D_SEL_ROW &&
                u1PcpDE == PBB_VLAN_SNMP_TRUE)
            {
                *pu1NextPcpDE = PBB_VLAN_SNMP_FALSE;
                *pu1NextPcpPriority = u1PcpPriority;
                *pu1NextPcpSelRow = u1PcpSelRow;
                return PBB_SUCCESS;
            }
            if (u1PcpPriority >= PBB_MAX_PRIORITY &&
                u1PcpSelRow < PBB_5P3D_SEL_ROW)
            {
                *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
                *pu1NextPcpPriority = PBB_MIN_PRIORITY;
                *pu1NextPcpSelRow = u1PcpSelRow + 1;
                return PBB_SUCCESS;
            }
            if ((INT1) u1PcpPriority == -1 ||
                (INT1) u1PcpDE == PBB_INVALID_DE ||
                u1PcpSelRow == PBB_INVALID_SEL_ROW)
            {
                *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
                *pu1NextPcpPriority = PBB_MIN_PRIORITY;
                *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
                return PBB_SUCCESS;
            }
            i4IfIndex = *pi4NextIfIndex;
        }
        while (PbbGetNextPbbPipIfIndex (i4IfIndex, pi4NextIfIndex) !=
               PBB_FAILURE);
    }
    else
    {
        if (PbbVcmGetContextInfoFromIfIndex (i4IfIndex,
                                             &u4ContextId,
                                             &u2LocalPort) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        if (u4ContextId > PBB_CURR_CONTEXT_ID ())
        {
            return PBB_FAILURE;
        }
        if (PbbGetFirstPbbPipIfIndex (pi4NextIfIndex) == PBB_SUCCESS)
        {
            *pu1NextPcpDE = PBB_VLAN_SNMP_TRUE;
            *pu1NextPcpPriority = PBB_MIN_PRIORITY;
            *pu1NextPcpSelRow = PBB_8P0D_SEL_ROW;
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPcpEncodingPcpVal                              */
/*                                                                          */
/*    Description        : This function is used to get pcp encoding 
 *                         pcp value                                         */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection Row
 *                         Pcp Priority
 *                         Pcp DE
 */
/*    Output(s)          : Pcp value                                         */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetPcpEncodingPcpVal (UINT2 u2LocalPort,
                         UINT1 u1PcpSelRow,
                         UINT1 u1PcpPriority, UINT1 u1PcpDE, UINT1 *pu1PcpVal)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    if ((u1PcpSelRow > PBB_MAX_NUM_PCP_SEL_ROW) || (u1PcpSelRow == 0))
    {
        return PBB_FAILURE;
    }

    *pu1PcpVal =
        pPcpData->au1PcpEncoding[u1PcpSelRow - 1][u1PcpPriority][u1PcpDE];
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPcpEncodingPcpVal                              */
/*                                                                          */
/*    Description        : This function is used to set pcp encoding 
 *                         pcp value                                         */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection Row
 *                         Pcp Priority
 *                         Pcp DE
 *                         Pcp Value
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPcpEncodingPcpVal (UINT2 u2LocalPort,
                         UINT1 u1PcpSelRow,
                         UINT1 u1PcpPriority, UINT1 u1PcpDE, UINT1 u1PcpVal)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    if ((u1PcpSelRow > PBB_MAX_NUM_PCP_SEL_ROW) || (u1PcpSelRow == 0))
    {
        return PBB_FAILURE;
    }

    pPcpData->au1PcpEncoding[u1PcpSelRow - 1][u1PcpPriority][u1PcpDE] =
        u1PcpVal;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPcpSelRow                                   */
/*                                                                          */
/*    Description        : This function is used to get pcp sel 
 *                         row value                                         */
/*                                                                          */
/*    Input(s)           : Local Port
 */
/*    Output(s)          : Pcp selection row                                 */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetPcpSelRow (UINT2 u2LocalPort, UINT1 *pu1PcpSelRow)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    *pu1PcpSelRow = pPcpData->u1PcpSelRow;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPcpSelRow                              */
/*                                                                          */
/*    Description        : This function is used to set pcp selection
 *                         row value                                         */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection row
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPcpSelRow (UINT2 u2LocalPort, UINT1 u1PcpSelRow)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData->u1PcpSelRow = u1PcpSelRow;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPcpUseDei                                   */
/*                                                                          */
/*    Description        : This function is used to get pcp use 
 *                         dei value                                         */
/*                                                                          */
/*    Input(s)           : Local Port
 */
/*    Output(s)          : Pcp use dei value                                 */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetPcpUseDei (UINT2 u2LocalPort, UINT1 *pu1UseDei)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    *pu1UseDei = pPcpData->u1UseDei;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPcpUseDei                                   */
/*                                                                          */
/*    Description        : This function is used to set pcp selection
 *                         row value                                         */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp Selection row
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPcpUseDei (UINT2 u2LocalPort, UINT1 u1UseDei)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData->u1UseDei = u1UseDei;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPcpReqDropEncoding                          */
/*                                                                          */
/*    Description        : This function is used to get pcp drop 
 *                         encoding value                                    */
/*                                                                          */
/*    Input(s)           : Local Port
 */
/*    Output(s)          : Pcp drop encoding value                           */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetPcpReqDropEncoding (UINT2 u2LocalPort, UINT1 *pu1ReqDropEncoding)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    *pu1ReqDropEncoding = pPcpData->u1ReqDropEncoding;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbSetPcpReqDropEncoding                          */
/*                                                                          */
/*    Description        : This function is used to set pcp drop encoding
 *                         value                                             */
/*                                                                          */
/*    Input(s)           : Local Port
 *                         Pcp drop encoding value
 */
/*    Output(s)          : None                                              */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbSetPcpReqDropEncoding (UINT2 u2LocalPort, UINT1 u1ReqDropEncoding)
{
    tPbbPortRBtreeNode *pPortEntry = NULL;
    tPbbPcpData        *pPcpData = NULL;
    PBB_GET_PORT_ENTRY (u2LocalPort, pPortEntry);
    if (pPortEntry == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData = (tPbbPcpData *) pPortEntry->unPbbPortData.PipData.pu4PcpData;
    if (pPcpData == NULL)
    {
        return PBB_FAILURE;
    }
    pPcpData->u1ReqDropEncoding = u1ReqDropEncoding;
    return PBB_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbGetNextActiveBContext                         */
/*                                                                           */
/*    Description         : This function is used to get the next Active     */
/*                          B context present in the system.                   */
/*                                                                           */
/*    Input(s)            : u4CurrContextId - Current Context Id.            */
/*                                                                           */
/*    Output(s)           : pu4NextContextId - Next Context Id.              */
/*                                                                           */
/*    Global Variables Referred : gPbbGlobData.apPbbContextInfo              */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : VLAN_SUCCESS/VLAN_FAILURE                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT1
PbbGetNextActiveBContext (UINT4 u4CurrContextId, UINT4 *pu4NextContextId)
{
    UINT4               u4ContextId = PBB_INIT_VAL;

    for (u4ContextId = u4CurrContextId + 1;
         u4ContextId < PBB_MAX_CONTEXTS; u4ContextId++)
    {
        if (PBB_CONTEXT_PTR (u4ContextId) != NULL)
        {
            PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4ContextId);
            if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
            {
                *pu4NextContextId = u4ContextId;
                return PBB_SUCCESS;
            }
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetNextValidIsidPort                          */
/*                                                                          */
/*    Description        : This function is used to Get Next ISIDPort Entry */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetNextValidIsidPort (UINT4 u4ContextId,
                         UINT4 *pu4NextContextid,
                         UINT4 u4CurrIsid,
                         UINT4 *u4NextIsid,
                         UINT4 u4CurrPort, UINT2 *pu2NextPort)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry *pRBGetFirstNode = NULL;
    tPbbBackBoneSerInstEntry *pRBNextNode = NULL;
    tPbbBackBoneSerInstEntry key;
    UINT4               u4NextContext = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetIfMapHlPortId (u4CurrPort, &u2LocalPort) != PBB_SUCCESS)
    {
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbBackBoneSerInstEntry));
    key.u4Isid = u4CurrIsid;
    key.u2LocalPort = u2LocalPort;

    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);

    if (pRBCurNode == NULL)
    {
        pRBGetFirstNode =
            RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->IsidPortTable);
        if (pRBGetFirstNode == NULL)
        {
            return PBB_FAILURE;
        }
        *u4NextIsid = pRBGetFirstNode->u4Isid;
        *pu2NextPort = pRBGetFirstNode->u2LocalPort;
        return PBB_SUCCESS;

    }
    else
    {
        pRBNextNode =
            (tPbbBackBoneSerInstEntry *)
            RBTreeGetNext (PBB_CURR_CONTEXT_PTR ()->IsidPortTable, pRBCurNode,
                           (tRBCompareFn) PbbCompareIsidPortRBNodes);
        if (pRBNextNode == NULL)
        {
            while (PbbGetNextActiveBContext (u4ContextId, &u4NextContext) ==
                   PBB_SUCCESS)
            {
                PBB_CURR_CONTEXT_PTR () = PBB_CONTEXT_PTR (u4NextContext);
                pRBGetFirstNode =
                    RBTreeGetFirst (PBB_CURR_CONTEXT_PTR ()->IsidPortTable);
                if (pRBGetFirstNode == NULL)
                {
                    PBB_CURR_CONTEXT_PTR () = NULL;
                    u4ContextId = u4NextContext;
                    u4NextContext = PBB_INIT_VAL;
                    continue;
                }
                else
                {
                    *u4NextIsid = pRBGetFirstNode->u4Isid;
                    *pu2NextPort = pRBGetFirstNode->u2LocalPort;
                    *pu4NextContextid = u4NextContext;
                    return PBB_SUCCESS;
                }
            }
        }
        else
        {
            *u4NextIsid = pRBNextNode->u4Isid;
            *pu2NextPort = pRBNextNode->u2LocalPort;
            *pu4NextContextid = u4ContextId;
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteCnpNodeFromCnpDLL                       */
/*                                                                          */
/*    Description        : This function is used to delete Cnp Node from DLL */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDeleteCnpNodeFromCnpDLL (UINT4 u4Isid, UINT4 u4IfIndex)
{
    tTMO_DLL_NODE      *pListNode = NULL;
    tPbbCnpPortList    *pCNPPortDll = NULL;
    tPbbIsidRBtreeNode *pRBCurNode = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT2               u2LocalPort = PBB_INIT_VAL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    if (PbbVcmGetContextInfoFromIfIndex (u4IfIndex,
                                         &u4ContextId,
                                         &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    /*getting first ISID in RB tree */
    pRBCurNode =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);

    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.ICompData.CnpPortTableDll),
                  pListNode, tTMO_DLL_NODE *)
    {
        pCNPPortDll = (tPbbCnpPortList *) pListNode;
        if (PbbCompareCnpPort (&u2LocalPort,
                               &(pCNPPortDll->u2CnpPort)) == PBB_EQUAL)
        {
            PbbDelCnpEntryDll (&(pCNPPortDll->link), u4Isid);
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************
 Function    :  PbbGetIsidInfoDataFromFrame
 Description :  To get Isid tag information from the protocl frame 
 Input       :  Context Id - context id
                 u2LocalPort -  port on which frame received
                pBuf - PDU buffer
                u4IsidTagOffSet - offset where I-tag info present in buf
                
 Output      :  pPbbTag
 Returns     :  PBB_SUCCESS or PBB_FAILURE
****************************************************************************/
INT4
PbbGetIsidInfoDataFromFrame (UINT4 u4ContextId,
                             UINT2 u2LocalPort,
                             tCRU_BUF_CHAIN_DESC * pBuf,
                             tPbbTag * pPbbTag, UINT4 u4IsidTagOffSet)
{
    tSNMP_OCTET_STRING_TYPE OUI;
    tPbbPortRBtreeNode *pPbbPortEntry = NULL;
    UINT4               u4IsidTag = PBB_INIT_VAL;
    UINT4               u4RelayIsid;
    UINT1               u1Pcp;
    UINT1               au1OUI[PBB_OUI_LENGTH];
    tMacAddr            LocalBDA;

    OUI.i4_Length = PBB_OUI_LENGTH;
    OUI.pu1_OctetList = au1OUI;

    /* select the context */
    if (PbbSelectContext (u4ContextId) == PBB_FAILURE)
    {
        return PBB_FAILURE;
    }
    /* module status for the context */
    if (PBB_IS_MODULE_INITIALISED () != PBB_TRUE)
    {
        PbbReleaseContext ();
        return PBB_FAILURE;
    }
    CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4IsidTag,
                               u4IsidTagOffSet + VLAN_TYPE_OR_LEN_SIZE,
                               ISID_TAG_SIZE);
    u4IsidTag = OSIX_NTOHL (u4IsidTag);

    /* Extract the UCA bit from the ISID */
    pPbbTag->InnerIsidTag.u1UcaBitValue = PBB_ISID_IS_UCA_BIT_SET (u4IsidTag);
    pPbbTag->InnerIsidTag.u4Isid = (u4IsidTag & ISID_ID_MASK);
    u1Pcp = (UINT1) (u4IsidTag >> ISID_TAG_PRIORITY_SHIFT);
    pPbbTag->InnerIsidTag.u1Priority = u1Pcp;

    if (PbbGetPortData (u2LocalPort, &pPbbPortEntry) == PBB_FAILURE)
    {
        pPbbTag->InnerIsidTag.u1TagType = VLAN_TAGGED;

        /* get the ISID entry corresponding this isid, 
         * if not present return failure */
        if (PbbValidateIsid (pPbbTag->InnerIsidTag.u4Isid) != PBB_SUCCESS)
        {
            PbbReleaseContext ();
            return PBB_FAILURE;
        }

        PbbReleaseContext ();
        return PBB_SUCCESS;
    }

    /* in caseof CBP port convet Local isid to Relay isid */
    if (pPbbPortEntry->u1PortType == PBB_CUSTOMER_BACKBONE_PORT)
    {
        if (PbbGetRelayIsidFromLocalIsid (u4ContextId, u2LocalPort,
                                          pPbbTag->InnerIsidTag.u4Isid,
                                          &u4RelayIsid) != PBB_SUCCESS)
        {
            PbbReleaseContext ();
            return PBB_FAILURE;
        }
        /* When Local Isid and Relay Isid are same there is no need
         * to do Isid Translation.
         */
        if (pPbbTag->InnerIsidTag.u4Isid != u4RelayIsid)
        {
            /* Use the Global OUI + ISID for going inside PBBN */
            if (PbbGetPbbGlbOUI (&OUI) != PBB_SUCCESS)
            {
                PbbReleaseContext ();
                return PBB_FAILURE;
            }
            PBBGetBDAFromOUI (&OUI, pPbbTag->InnerIsidTag.u4Isid, LocalBDA);
            if (MEMCMP (pPbbTag->BDAMacAddr, LocalBDA, sizeof (tMacAddr)) == 0)
            {
                PBBGetBDAFromOUI (&OUI, u4RelayIsid, pPbbTag->BDAMacAddr);
            }
        }
        pPbbTag->InnerIsidTag.u4Isid = u4RelayIsid;
    }

    pPbbTag->InnerIsidTag.u1TagType = VLAN_TAGGED;

    /* get the ISID entry corresponding this isid, 
     * if not present return failure */
    if (PbbValidateIsid (pPbbTag->InnerIsidTag.u4Isid) != PBB_SUCCESS)
    {
        PbbReleaseContext ();
        return PBB_FAILURE;
    }

    PbbReleaseContext ();
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetPipVipDataWithIsid                         */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetPipVipDataWithIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                          UINT4 u4Isid, UINT2 *pu2Vip, UINT2 *pu2Pip)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;

    UNUSED_PARAM (u2LocalPort);
    if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (PbbGetVipValue (u4ContextId, u4Isid, pu2Vip) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    key.u2Vip = *pu2Vip;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    *pu2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbGetIsidDataForVip                              */
/*                                                                          */
/*     DESCRIPTION      : This function will return ISID associated witrh VIP*/
/*                         shutdown  status                                 */
/*                                                                          */
/*     INPUT            :                                                   */
/*     OUTPUT           :                                                   */
/*                                                                          */
/*     RETURNS          : PBB_TRUE/PBB_FALSE                                */
/*                                                                          */
/****************************************************************************/
INT1
PbbGetIsidDataForVip (UINT4 u4Vip, UINT4 *pu4Isid)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    UINT4               u4ContextId = PBB_INIT_VAL;
    UINT2               u2LocalPort = PBB_INIT_VAL;

    if (PbbVcmGetContextInfoFromIfIndex (u4Vip,
                                         &u4ContextId,
                                         &u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbICompVipRBtreeNode));
    key.u2Vip = u2LocalPort;
    pRBCurNode =
        (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable,
                                              &key);
    if (pRBCurNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
    if (pIsidVipInfo->u4Isid == PBB_ISID_INVALID_VALUE)
    {
        return PBB_FAILURE;
    }
    *pu4Isid = pIsidVipInfo->u4Isid;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetLocalIsidDataFromRelayIsid                 */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbGetLocalIsidDataFromRelayIsid (UINT4 u4ContextId, UINT2 u2LocalPort,
                                  UINT4 u4RelayIsid, UINT4 *pu4LocalIsid)
{
    tPbbBackBoneSerInstEntry *pRBCurNode = NULL;
    tPbbBackBoneSerInstEntry key;

    key.u4Isid = u4RelayIsid;
    key.u2LocalPort = u2LocalPort;

    if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    pRBCurNode =
        (tPbbBackBoneSerInstEntry *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                IsidPortTable, &key);
    if (pRBCurNode == NULL)
    {
        return PBB_FAILURE;
    }

    *pu4LocalIsid = pRBCurNode->u4LocalIsid;

    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbIsIsidMemberPortPresent                       */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbIsIsidMemberPortPresent (UINT4 u4ContextId, UINT4 u4Isid, UINT2 u2LocalPort)
{
    tPbbIsidRBtreeNode *pRBNodeCur = NULL;
    tPbbPortRBtreeNode *pPortRBNode = NULL;
    tPbbPortRBtreeNode  key1;
    tPbbCbpPortList    *pCurEntry = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    UINT2               u2Pip = PBB_INIT_VAL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    pRBNodeCur =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBNodeCur == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNodeCur->pu4PbbIsidVipInfo;

    if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        u2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
        if (u2Pip == u2LocalPort)
        {
            return PBB_SUCCESS;

        }
        else
        {
            key1.u2LocalPort = u2LocalPort;
            pPortRBNode =
                (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                  PortTable, &key1);
            if (pPortRBNode == NULL)
            {
                return PBB_FAILURE;
            }
            if (pPortRBNode->unPbbPortData.CnpData.u4Pisid == u4Isid)
            {
                return PBB_SUCCESS;
            }
            return PBB_FAILURE;
        }
    }
    else if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
    {
        PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbCbpPortList *) pLstNode;
            if (pCurEntry->u2CBPPort == u2LocalPort)
            {
                return PBB_SUCCESS;
            }
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetCnpMemberPortListForIsid                      */
/*                                                                          */
/*    Description        : This function is used to get member ports of ISID*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT4
PbbGetCnpMemberPortListForIsid (UINT4 u4ContextId,
                                UINT4 u4Isid,
                                tSNMP_OCTET_STRING_TYPE * pPortList)
{
    tPbbIsidRBtreeNode *pRBNodeCur = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    BOOL1               IsCnpListSet = PBB_FALSE;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    pRBNodeCur =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBNodeCur == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNodeCur->pu4PbbIsidVipInfo;

    if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        tPbbCnpPortList    *pCurEntry = NULL;
        PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.ICompData.CnpPortTableDll),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbCnpPortList *) pLstNode;
            PBB_SET_MEMBER_PORT (pPortList->pu1_OctetList,
                                 pCurEntry->u2CnpPort);
            IsCnpListSet = PBB_TRUE;
        }
        if (IsCnpListSet == PBB_TRUE)
        {
            return PBB_SUCCESS;
        }
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetVipIsidDataWithPortList                    */
/*                                                                          */
/*    Description        :This function return the value for Local Isid     */
/*                        corresponding to Relay Isid                       */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbGetVipIsidDataWithPortList (UINT4 u4ContextId,
                               UINT2 u2LocalPort,
                               tLocalPortList InPortList,
                               UINT2 *pu2Vip,
                               UINT2 *pu2Pip,
                               UINT4 *pu4Isid, tMacAddr * pBDaAddr)
{
    tPbbICompVipRBtreeNode *pRBCurNode = NULL;
    tPbbICompVipRBtreeNode key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tPbbPortRBtreeNode *pRBNode;
    tPbbPortRBtreeNode  key1;
    tMacAddr            tempMacAddr;
    tMacAddr            DefaultBackboneDest;
    tSNMP_OCTET_STRING_TYPE pOUI;
    UINT2               u2VipPort = PBB_INIT_VAL;
    UINT4               u4Isid = PBB_INIT_VAL;
    UINT4               u4Index = PBB_INIT_VAL;
    UINT4               u4SizeofMac = PBB_SIZE_OF_MAC_ADDR;
    UINT2               u2ByteInd = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT2               u2Port = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;
    UINT1               u1ByteIndex = PBB_INIT_VAL;
    UINT1               au1OUI[PBB_OUI_LENGTH];

    key1.u2LocalPort = u2LocalPort;
    PBB_MEMSET (tempMacAddr, PBB_INIT_VAL, sizeof (tMacAddr));
    PBB_MEMSET (DefaultBackboneDest, PBB_INIT_VAL, sizeof (tMacAddr));
    pOUI.i4_Length = PBB_OUI_LENGTH;
    pOUI.pu1_OctetList = au1OUI;

    if (PbbSelectContext (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    pRBNode =
        (tPbbPortRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->PortTable,
                                          &key1);

    if (pRBNode == NULL)
    {
        /* Element not present in the key */
        return PBB_FAILURE;
    }
    /* u2Port is the local CNP port */
    /* 1. get the pisid corresponding to this port, if it is non default 
     * value get vip corresponding to this return these two 
     */

    /* 2. else its default value then ,
     * from the Input port list get the VIP and return the ISID 
     * corresponding to VIP
     */

    if (pRBNode->unPbbPortData.CnpData.u4Pisid != PBB_ISID_INVALID_VALUE)
    {
        *pu4Isid = pRBNode->unPbbPortData.CnpData.u4Pisid;
        if (PbbGetVipValue (u4ContextId, *pu4Isid, pu2Vip) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        key.u2Vip = *pu2Vip;
        pRBCurNode =
            (tPbbICompVipRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->
                                                  VipTable, &key);
        if (pRBCurNode == NULL)
        {
            return PBB_FAILURE;
        }
        pIsidVipInfo =
            (tPbbIsidVipInfo *) (VOID *) pRBCurNode->pu4PbbIsidVipInfo;
        *pu2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
        u4Isid = *pu4Isid;
        u2VipPort = *pu2Vip;
    }
    else
    {
        for (u2ByteInd = PBB_INIT_VAL; u2ByteInd < PBB_PORT_LIST_SIZE;
             u2ByteInd++)
        {
            if (InPortList[u2ByteInd] == 0)
            {
                continue;
            }
            u1PortFlag = InPortList[u2ByteInd];
            for (u2BitIndex = PBB_INIT_VAL;
                 ((u2BitIndex < PBB_PORTS_PER_BYTE) && (u1PortFlag != 0));
                 u2BitIndex++)
            {
                if ((u1PortFlag & 0x80) != 0)
                {
                    u2Port = (UINT2) ((u2ByteInd * BITS_PER_BYTE) +
                                      u2BitIndex + 1);
                    u2VipPort = u2Port;
                    key.u2Vip = u2VipPort;
                    pRBCurNode =
                        (tPbbICompVipRBtreeNode *)
                        RBTreeGet (PBB_CURR_CONTEXT_PTR ()->VipTable, &key);
                    if (pRBCurNode != NULL)
                    {
                        pIsidVipInfo =
                            (tPbbIsidVipInfo *) (VOID *) pRBCurNode->
                            pu4PbbIsidVipInfo;
                        *pu4Isid = pIsidVipInfo->u4Isid;
                        u4Isid = pIsidVipInfo->u4Isid;
                        *pu2Pip = pIsidVipInfo->unPbbCompData.ICompData.u2Pip;
                        *pu2Vip = u2VipPort;
                        /* end the loop */
                        u1PortFlag = 0;
                        u2ByteInd = PBB_PORT_LIST_SIZE;
                        break;
                    }
                }
                u1PortFlag = (UINT1) (u1PortFlag << 1);
            }
        }
        if (pIsidVipInfo == NULL)
        {
            /*no VIP information  found */
            return PBB_SUCCESS;
        }
    }
    PBB_MEMCPY (DefaultBackboneDest,
                pIsidVipInfo->DefBackDestMac, sizeof (tMacAddr));
    if (PBB_MEMCMP (DefaultBackboneDest, tempMacAddr,
                    sizeof (tMacAddr)) == PBB_INIT_VAL)
    {
        if (PbbGetPbbGlbOUI (&pOUI) != PBB_SUCCESS)
        {
            return PBB_FAILURE;
        }
        PBB_MEMCPY (DefaultBackboneDest, pOUI.pu1_OctetList, PBB_OUI_LENGTH);
        DefaultBackboneDest[0] = DefaultBackboneDest[0] | 0x01;
        for (u4Index = u4SizeofMac; u4Index > PBB_OUI_LENGTH - 1; u4Index--)
        {
            u1ByteIndex = 0xFF & u4Isid;
            DefaultBackboneDest[u4Index] = u1ByteIndex;
            u4Isid = u4Isid >> 8;
            u1ByteIndex = PBB_INIT_VAL;
        }
        PBB_MEMCPY (pBDaAddr, DefaultBackboneDest, sizeof (tMacAddr));
    }
    else
    {
        PBB_MEMCPY (pBDaAddr,
                    &(pIsidVipInfo->DefBackDestMac), sizeof (tMacAddr));
    }
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbGetMemberPortListForIsid                      */
/*                                                                          */
/*    Description        : This function is used to get member ports of ISID*/
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT4
PbbGetMemberPortListForIsid (UINT4 u4ContextId,
                             UINT4 u4Isid, tSNMP_OCTET_STRING_TYPE * pPortList)
{
    tPbbIsidRBtreeNode *pRBNodeCur = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));
    key.u4Isid = u4Isid;

    if (PbbValidateContextId (u4ContextId) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    pRBNodeCur =
        (tPbbIsidRBtreeNode *) RBTreeGet (PBB_CURR_CONTEXT_PTR ()->IsidTable,
                                          &key);
    if (pRBNodeCur == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) pRBNodeCur->pu4PbbIsidVipInfo;

    if (PBB_BRIDGE_MODE () == PBB_ICOMPONENT_BRIDGE_MODE)
    {
        tPbbCnpPortList    *pCurEntry = NULL;
        PBB_SET_MEMBER_PORT (pPortList->pu1_OctetList,
                             pIsidVipInfo->unPbbCompData.ICompData.u2Pip);

        PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.ICompData.CnpPortTableDll),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbCnpPortList *) pLstNode;
            PBB_SET_MEMBER_PORT (pPortList->pu1_OctetList,
                                 pCurEntry->u2CnpPort);
        }
        return PBB_SUCCESS;
    }
    else if (PBB_BRIDGE_MODE () == PBB_BCOMPONENT_BRIDGE_MODE)
    {
        tPbbCbpPortList    *pCurEntry = NULL;
        PBB_DLL_SCAN (&(pIsidVipInfo->unPbbCompData.BCompData.CbpPortTableDll),
                      pLstNode, tTMO_DLL_NODE *)
        {
            pCurEntry = (tPbbCbpPortList *) pLstNode;
            PBB_SET_MEMBER_PORT (pPortList->pu1_OctetList,
                                 pCurEntry->u2CBPPort);
        }
        return PBB_SUCCESS;
    }
    return PBB_FAILURE;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteIsidNodeFromVip                         */
/*                                                                          */
/*    Description        : This function is used to delete the isid node    */
/*                         associated with vip                              */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDeleteIsidNodeFromVip (UINT4 u4Isid)
{

    tPbbIsidRBtreeNode *ptobefreed = NULL;
    tPbbIsidRBtreeNode  key;
    tPbbIsidVipInfo    *pIsidVipInfo = NULL;

    PBB_MEMSET (&key, PBB_INIT_VAL, sizeof (tPbbIsidRBtreeNode));

    key.u4Isid = u4Isid;
    ptobefreed = RBTreeRem (PBB_CURR_CONTEXT_PTR ()->IsidTable, &key);
    if (ptobefreed == NULL)
    {
        return PBB_FAILURE;
    }
    pIsidVipInfo = (tPbbIsidVipInfo *) (VOID *) ptobefreed->pu4PbbIsidVipInfo;
    pIsidVipInfo->u4Isid = PBB_ISID_INVALID_VALUE;
    pIsidVipInfo = NULL;
    ptobefreed->pu4PbbIsidVipInfo = NULL;
    PBB_RELEASE_BUF (PBB_ISID_BUFF, (UINT1 *) (ptobefreed));
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbCreateInstanceNode                            */
/*                                                                          */
/*    Description        : This function Creates an Instance node           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/

INT1
PbbCreateInstanceNode (UINT4 u4InstanceId)
{
    /* allocate instance pointer from memory pool, and store it in the 
       instance pointers array */
    PBB_CURR_INSTANCE_PTR () = (tPbbInstanceInfo *) (VOID *)
        PBB_GET_BUF (PBB_INSTANCE_BUFF, sizeof (tPbbInstanceInfo));

    if (PBB_CURR_INSTANCE_PTR () == NULL)
    {
        PBB_TRC_ARG2 (MGMT_TRC | INIT_SHUT_TRC | BUFFER_TRC | OS_RESOURCE_TRC,
                      "%s :: %s() :: Failed since instance buffer allocation "
                      "failed\n", __FILE__, PBB_FUNCTION_NAME);
        return PBB_FAILURE;
    }
    PBB_MEMSET (PBB_CURR_INSTANCE_PTR (),
                PBB_INIT_VAL, sizeof (tPbbInstanceInfo));
    PBB_INSTANCE_PTR (u4InstanceId) = PBB_CURR_INSTANCE_PTR ();
    PBB_CURR_INSTANCE_PTR ()->u4InstanceId = u4InstanceId;
    PBB_MEMSET (PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
                PBB_INIT_VAL, PBB_INSTANCE_ALIAS_LEN + 1);
    SPRINTF ((CHR1 *) PBB_CURR_INSTANCE_PTR ()->au1InstanceStr,
             "Instance%d", u4InstanceId);
    PbbGetDefaultInstanceMacAddress (u4InstanceId,
                                     PBB_CURR_INSTANCE_PTR ()->InstanceMacAddr);
    PBB_CURR_INSTANCE_PTR ()->u1MacAddressType = PBB_INSTANCE_MAC_DERIVED;
    PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfIcomp = PBB_INIT_VAL;
    PBB_CURR_INSTANCE_PTR ()->u4InstanceNumOfBcomp = PBB_INIT_VAL;
    PBB_DLL_INIT (&(PBB_CURR_INSTANCE_PTR ()->ContextDll));
    PBB_CURR_INSTANCE_PTR ()->u1InstanceRowStatus = PBB_CREATE_AND_WAIT;
    return PBB_SUCCESS;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteInstanceNode                            */
/*                                                                          */
/*    Description        : This function deletes an Instance node           */
/*                                                                          */
/*    Input(s)           : None                                             */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/****************************************************************************/

VOID
PbbDeleteInstanceNode (UINT4 u4InstanceId)
{
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);
    PBB_RELEASE_BUF (PBB_INSTANCE_BUFF, PBB_CURR_INSTANCE_PTR ());
    PBB_INSTANCE_PTR (u4InstanceId) = NULL;
    PBB_CURR_INSTANCE_PTR () = NULL;
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : PbbDeleteContextNodeFromDLL                      */
/*                                                                          */
/*    Description        : This function is used to delete Context Node     */
/*                         from DLL                                         */
/*    Input(s)           : Instance Id                                      */
/*                         Context Id                                       */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : PBB_FAILURE / PBB_SUCCESS                        */
/****************************************************************************/
INT1
PbbDeleteContextNodeFromDLL (UINT4 u4InstanceId, UINT4 u4ContextId)
{
    tTMO_DLL_NODE      *pListNode = NULL;
    tPbbContextNode    *pContextNode = NULL;
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);
    if (PBB_CURR_INSTANCE_PTR () == NULL)
    {
        return PBB_FAILURE;
    }
    PBB_DLL_SCAN (&(PBB_CURR_INSTANCE_PTR ()->ContextDll),
                  pListNode, tTMO_DLL_NODE *)
    {
        pContextNode = (tPbbContextNode *) pListNode;
        if (PbbCompareContext (&u4ContextId,
                               &(pContextNode->u4ContextId)) == PBB_EQUAL)
        {
            PBB_DLL_DEL (&(PBB_CURR_INSTANCE_PTR ()->ContextDll),
                         &(pContextNode->link));
            PBB_RELEASE_BUF (PBB_CONTEXT_DLL_BUFF,
                             (UINT1 *) &(pContextNode->link));
            PBB_CURR_INSTANCE_PTR () = NULL;
            return PBB_SUCCESS;
        }
    }
    PBB_CURR_INSTANCE_PTR () = NULL;
    return PBB_FAILURE;
}

/*********************************************************************
*  Function Name : PbbAddContextEntryDll 
*  Description   : Function to add the Context entry to the list
*  Parameter(s)  : pNode - Node to be added 
*  Return Values : None
*********************************************************************/

VOID
PbbAddContextEntryDll (tTMO_DLL_NODE * pNode, UINT4 u4InstanceId)
{
    tPbbContextNode    *pCurEntry = NULL;
    tPbbContextNode    *pInEntry = NULL;
    tTMO_DLL_NODE      *pLstNode = NULL;
    tTMO_DLL_NODE      *pPrevNode = NULL;

    pInEntry = (tPbbContextNode *) pNode;

    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);
    if (PBB_CURR_INSTANCE_PTR () == NULL)
    {
        return;
    }

    PBB_DLL_SCAN (&(PBB_CURR_INSTANCE_PTR ()->ContextDll),
                  pLstNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbContextNode *) pLstNode;

        if (PbbCompareContext (&(pInEntry->u4ContextId),
                               &(pCurEntry->u4ContextId)) == PBB_LESSER)
        {
            break;
        }
        pPrevNode = pLstNode;
    }
    PBB_DLL_INSERT_NODE (&(PBB_CURR_INSTANCE_PTR ()->ContextDll),
                         pPrevNode, pNode);
    PBB_CURR_INSTANCE_PTR () = NULL;
}

/*********************************************************************
*  Function Name : PbbCreateContextEntryDll 
*  Description   : Function to create and add the Context entry 
                   to the list
*  Parameter(s)  : Instance Id and Context Id
*  Return Values : PBB_SUCCESS / PBB_FAILURE
*********************************************************************/
INT1
PbbCreateContextEntryDll (UINT4 u4Instance, UINT4 u4ContextId)
{
    tPbbContextNode    *pContextNode = NULL;

    pContextNode =
        (tPbbContextNode *) (VOID *) PBB_GET_BUF (PBB_CONTEXT_DLL_BUFF,
                                                  sizeof (tPbbContextNode));
    if (pContextNode == NULL)
    {
        return PBB_FAILURE;
    }

    pContextNode->u4ContextId = u4ContextId;
    PbbAddContextEntryDll (&(pContextNode->link), u4Instance);
    return PBB_SUCCESS;
}

/*********************************************************************
*  Function Name : PbbNumOfBebPortsForInstance 
*  Description   : Function to get the number of BEB ports for the 
                   given instance
*  Input(s)      : Instance Id 
*  Output(s)     : Number of Beb Ports 
*  Return Values : PBB_SUCCESS / PBB_FAILURE
*********************************************************************/

INT1
PbbNumOfBebPortsForInstance (UINT4 u4InstanceId, UINT4 *pu4NumOfBebPorts)
{
    tTMO_DLL_NODE      *pLstNode = NULL;
    tPbbContextNode    *pCurEntry = NULL;
    tCfaIfInfo          IfInfo;
    tPortList          *pIfPortList = NULL;
    UINT4               u4NumOfBeb = PBB_INIT_VAL;
    UINT4               u4Port = PBB_INIT_VAL;
    UINT2               u2ByteIndex = PBB_INIT_VAL;
    UINT2               u2BitIndex = PBB_INIT_VAL;
    UINT1               u1PortFlag = PBB_INIT_VAL;

    PBB_MEMSET (&IfInfo, PBB_INIT_VAL, sizeof (tCfaIfInfo));
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);
    if (PBB_CURR_INSTANCE_PTR () == NULL)
    {
        return PBB_FAILURE;
    }

    pIfPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pIfPortList == NULL)
    {
        PBB_TRC (ALL_FAILURE_TRC, "Error in Allocating memory for bitlist \n");
        return PBB_FAILURE;
    }

    PBB_DLL_SCAN (&(PBB_CURR_INSTANCE_PTR ()->ContextDll),
                  pLstNode, tTMO_DLL_NODE *)
    {
        pCurEntry = (tPbbContextNode *) pLstNode;
        PBB_MEMSET ((*pIfPortList), PBB_INIT_VAL, sizeof (tPortList));
        if (PbbVcmGetContextPortList (pCurEntry->u4ContextId,
                                      *pIfPortList) == PBB_SUCCESS)
        {

            for (u2ByteIndex = 0; u2ByteIndex < PBB_IFPORT_LIST_SIZE;
                 u2ByteIndex++)
            {
                u1PortFlag = (*pIfPortList)[u2ByteIndex];
                for (u2BitIndex = 0; ((u2BitIndex < PBB_PORTS_PER_BYTE)
                                      && (u1PortFlag != 0)); u2BitIndex++)
                {
                    if ((u1PortFlag & PBB_BIT8) != 0)
                    {
                        u4Port = (UINT4) ((u2ByteIndex * PBB_PORTS_PER_BYTE) +
                                          u2BitIndex + 1);
                        if (PbbCfaGetIfInfo (u4Port, &IfInfo) == CFA_FAILURE)
                        {
                            FsUtilReleaseBitList ((UINT1 *) pIfPortList);
                            return PBB_FAILURE;
                        }
                        if ((IfInfo.u1BrgPortType == PBB_PROVIDER_NETWORK_PORT)
                            || (IfInfo.u1BrgPortType == PBB_CNP_PORTBASED_PORT)
                            || (IfInfo.u1BrgPortType == PBB_CNP_STAGGED_PORT)
                            || (IfInfo.u1BrgPortType == PBB_CNP_CTAGGED_PORT)
                            || (IfInfo.u1BrgPortType ==
                                PBB_PROVIDER_INSTANCE_PORT)
                            || (IfInfo.u1BrgPortType ==
                                PBB_CUSTOMER_BACKBONE_PORT))
                        {
                            u4NumOfBeb++;
                        }
                    }
                    u1PortFlag = (UINT1) (u1PortFlag << 1);
                }                /*end of for2 */
            }                    /*end of for1 */

        }

    }
    *pu4NumOfBebPorts = u4NumOfBeb;
    FsUtilReleaseBitList ((UINT1 *) pIfPortList);
    return PBB_SUCCESS;
}

/*********************************************************************
*  Function Name : PbbGetContextListForInstance 
*  Description   : Function to get the context list for the 
                   given instance
*  Input(s)      : Instance Id 
*  Output(s)     : Context List 
*  Return Values : PBB_SUCCESS / PBB_FAILURE
*********************************************************************/
INT1
PbbGetContextListForInstance (UINT4 u4InstanceId,
                              tSNMP_OCTET_STRING_TYPE * pContextList)
{
    tPbbContextNode    *pCurEntry = NULL;
    PBB_CURR_INSTANCE_PTR () = PBB_INSTANCE_PTR (u4InstanceId);
    if (PBB_CURR_INSTANCE_PTR () == NULL)
    {
        return PBB_FAILURE;
    }
    pContextList->i4_Length = PBB_CURR_INSTANCE_PTR ()->ContextDll.u4_Count;

    PBB_DLL_SCAN (&(PBB_CURR_INSTANCE_PTR ()->ContextDll),
                  pCurEntry, tPbbContextNode *)
    {
        PBB_SET_MEMBER_CONTEXT (pContextList->pu1_OctetList,
                                (pCurEntry->u4ContextId + 1));
    }
    return PBB_SUCCESS;
}

/*********************************************************************
*  Function Name : PbbDelCBPMappingRowStatus 
*  Description   : Function to delete the CBP Mapping Row status 
*  Input(s)      : u4ContextId - Context Id 
*                : u4Isid - ISID
*                : u2LocalPort - CBP Local Port 
*  Output(s)     : None
*  Return Values : PBB_SUCCESS / PBB_FAILURE
*********************************************************************/
INT4
PbbDelCBPMappingRowStatus (UINT4 u4ContextId, UINT4 u4Isid,
                           INT4 i4IfIndex, UINT2 u2LocalPort)
{
    tMacAddr            DefaultDstBMAC;
    UINT4               u4NumofPortPerIsidPerContext = PBB_INIT_VAL;

    PBB_MEMSET (DefaultDstBMAC, 0, sizeof (tMacAddr));

    if (PbbDeleteCBPNodeLocalDLL (u4Isid, u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;

    }

    if (PbbSetVipIsidDefaultBDAddr (u4Isid, DefaultDstBMAC) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbDeleteIsidLocalPortRBNodes (u2LocalPort, u4Isid) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }
    if (PbbDelBCompHwServiceInst (u4ContextId, i4IfIndex, u4Isid)
        != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbDecrNumPortPerIsidPerContext (u4Isid) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    if (PbbGetNumPortPerIsidPerContext (u4ContextId, u4Isid,
                                        &u4NumofPortPerIsidPerContext)
        == PBB_SUCCESS)
    {
        if (u4NumofPortPerIsidPerContext == 0)
        {
            if (PbbDeleteIsidRBNodes (u4Isid) != PBB_SUCCESS)
            {
                return PBB_FAILURE;
            }
            PbbL2IwfDeleteIsid (u4ContextId, u4Isid);
        }
    }
    else
    {
        return PBB_FAILURE;
    }

    if (PbbDeleteLocalIsidNodeDll (u4Isid, u2LocalPort) != PBB_SUCCESS)
    {
        return PBB_FAILURE;
    }

    return PBB_SUCCESS;
}
