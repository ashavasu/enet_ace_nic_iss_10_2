/*******************************************************************
 *
 * $Id: pbbglob.h,v 1.8 2013/05/06 11:56:02 siva Exp $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2008                        */
/*                                                                           */
/*  FILE NAME             : pbbglob.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : PBB                                             */
/*  MODULE NAME           : PBB                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 20 Aug 2008                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains constant definitions used in  */
/*                          PBB Module.                                     */
/*                                                                           */
/*****************************************************************************/

#ifndef _PBBGLOB_H
#define _PBBGLOB_H

/*-------------- GLOBAL DECLARATIONS -------------------------------*/
typedef struct tPbbGlobData{
tPbbContextInfo             *apPbbContextInfo [SYS_DEF_MAX_NUM_CONTEXTS];
tPbbInstanceInfo            *apPbbInstanceInfo[SYS_DEF_MAX_NUM_CONTEXTS];
tPbbContextInfo             *pPbbContextInfo;
tPbbInstanceInfo            *pPbbInstanceInfo;
#ifdef L2RED_WANTED
tPbbRedGlobalInfo           PbbRedInfo;
tPbbRedTaskInfo             PbbRedTaskInfo;
#endif
UINT4                       gu4PbbNodeStatus;
UINT4                       u4TraceOption;
UINT1                       au1TraceInput[PBB_TRC_MAX_SIZE];
#if defined VRF_WANTED || defined MI_WANTED
UINT1                       au1PbbStatus[SYS_DEF_MAX_NUM_CONTEXTS];
#else
UINT1                       au1PbbStatus[SYS_DEF_MAX_NUM_CONTEXTS];
UINT1                       au1Padding[3];
#endif
tPbbTaskInfo                PbbTaskInfo;
UINT1                       u1IsPbbInitialised;
UINT1                       au1PbbPortBitMaskMap[PBB_PORTS_PER_BYTE] ;
tMacAddr                    PbbBcastAddress ;
UINT1                       au1PcpDecPriority[PBB_MAX_NUM_PCP_SEL_ROW]
                                             [PBB_MAX_NUM_PCP];

UINT1                       au1PcpDecDropEligible[PBB_MAX_NUM_PCP_SEL_ROW]
                                                 [PBB_MAX_NUM_PCP];
UINT1                       au1PcpEncValue[PBB_MAX_NUM_PCP_SEL_ROW]
                                          [PBB_MAX_NUM_PRIORITY]
                                          [PBB_MAX_NUM_DROP_ELIGIBLE];
UINT1                       au1PbbShutDownStatus;
#ifdef SYSLOG_WANTED
INT4 i4PbbSysLogId;
#endif
#ifdef PBB_PERFORMANCE_WANTED
struct timeval      gCurTime;
struct timeval      gStTime;
struct timeval      gEnTime;
UINT4               gu4TimeTaken;
UINT4               gu4TotalTimeTaken;
UINT4               gu4ISIDCreateTimeTaken;
UINT4               gu4ISIDDeleteTimeTaken;
UINT4               gu4VlanIsidTimeTaken;
UINT4               gu4DelVlanIsidTimeTaken;
UINT4               gu4CreateBVLANTimeTaken;
UINT4               gu4DeleteBVLANTimeTaken;
UINT4               gu4CreateBVLANTunnelTimeTaken;
UINT4               gu4DeleteBVLANTunnelTimeTaken;
#endif

}tPbbGlobData;






#endif
