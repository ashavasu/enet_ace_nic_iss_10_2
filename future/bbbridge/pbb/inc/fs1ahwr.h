#ifndef _FS1AHWR_H
#define _FS1AHWR_H

VOID RegisterFS1AH(VOID);

VOID UnRegisterFS1AH(VOID);
INT4 FsPbbBackboneEdgeBridgeAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbBackboneEdgeBridgeNameGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbNumberOfIComponentsGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbNumberOfBComponentsGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbNumberOfBebPortsGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbNextAvailablePipIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbBackboneEdgeBridgeNameSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbBackboneEdgeBridgeNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbBackboneEdgeBridgeNameDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsPbbVipTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbVipPipIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipISidGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipDefaultDstBMACGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipISidSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipDefaultDstBMACSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipISidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbVipDefaultDstBMACTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbVipTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbVipRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbVipTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPbbISidToVipTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbISidToVipComponentIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbISidToVipPortGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsPbbPipTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbPipBMACAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbPipNameGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbPipIComponentIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbPipRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbPipBMACAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbPipNameSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbPipRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbPipBMACAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbPipNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbPipRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbPipTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPbbVipToPipMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbVipToPipMappingPipIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingPipIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingPipIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbVipToPipMappingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPbbCBPServiceMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbCBPServiceMappingBVidGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingDefaultBackboneDestGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingLocalSidGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingBVidSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingDefaultBackboneDestSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingLocalSidSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingBVidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingDefaultBackboneDestTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingLocalSidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbCBPServiceMappingTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPbbCbpTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbCbpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCbpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbCbpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbCbpTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPbbPipToVipMappingTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbPipToVipMappingStatusGet(tSnmpIndex *, tRetVal *);
#endif /* _FS1AHWR_H */
