/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbparam.h,v 1.5 2013/05/06 11:56:02 siva Exp $
 *
 * Description:  This file contains sizing parameters used in 
 *               PBB Module.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2008                        */
/*                                                                           */
/*  FILE NAME             : pbbparam.h                                      */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : PBB                                             */
/*  MODULE NAME           : PBB                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 20 Aug 2008                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains sizing parameters used in     */
/*                          PBB Module.                                     */
/*                                                                           */
/*****************************************************************************/

#ifndef _PBBPARAM_H
#define _PBBPARAM_H

#ifdef __PBBSYS_C__
tFsModSizingParams gFsPbbSizingParams [] =
{
    {"tPbbQMsg", "PBB_CFG_Q_DEPTH", 
      sizeof (tPbbQMsg), PBB_CFG_Q_DEPTH, 
      PBB_CFG_Q_DEPTH, 0},

    {"tPbbContextInfo", "SYS_DEF_MAX_NUM_CONTEXTS", 
      sizeof (tPbbContextInfo), SYS_DEF_MAX_NUM_CONTEXTS, 
      SYS_DEF_MAX_NUM_CONTEXTS, 0},

    {"tPbbIsidRBtreeNode", 
      "PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID", 
      sizeof (tPbbIsidRBtreeNode), 
      (PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID), 
      (PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID), 0},

    {"tPbbBackBoneSerInstEntry", 
     "PBB_MAX_NUM_OF_BCOMP_ISID", 
      sizeof (tPbbBackBoneSerInstEntry), 
      (PBB_MAX_NUM_OF_BCOMP_ISID), 
      (PBB_MAX_NUM_OF_BCOMP_ISID), 0},

    {"tPbbPortRBtreeNode", "PBB_MAX_PORTS_IN_SYSTEM", 
      sizeof (tPbbPortRBtreeNode), PBB_MAX_PORTS_IN_SYSTEM, 
      PBB_MAX_PORTS_IN_SYSTEM, 0},

    {"tPbbICompVipRBtreeNode", "PBB_MAX_NUM_OF_ICOMP_ISID", 
      sizeof (tPbbICompVipRBtreeNode), PBB_MAX_NUM_OF_ICOMP_ISID, 
      PBB_MAX_NUM_OF_ICOMP_ISID, 0},

    {"tPbbIsidVipInfo", 
      "PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID", 
      sizeof (tPbbIsidVipInfo),
      (PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID), 
      (PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID), 0},

    {"tPbbCbpPortList", "PBB_MAX_PORTS_IN_SYSTEM", 
      sizeof (tPbbCbpPortList), PBB_MAX_PORTS_IN_SYSTEM, 
      PBB_MAX_PORTS_IN_SYSTEM, 0},

    {"tPbbCnpPortList", "PBB_MAX_PORTS_IN_SYSTEM", 
      sizeof (tPbbCnpPortList), PBB_MAX_PORTS_IN_SYSTEM, 
      PBB_MAX_PORTS_IN_SYSTEM, 0},

    {"tPbbIsidPipVipNode", 
      "PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID", 
      sizeof (tPbbIsidPipVipNode), 
      (PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID), 
      (PBB_MAX_NUM_OF_ICOMP_ISID + PBB_MAX_NUM_OF_BCOMP_ISID), 0},

    {"tPbbPcpData", "PBB_MAX_PORTS_IN_SYSTEM", 
      sizeof (tPbbPcpData), PBB_MAX_PORTS_IN_SYSTEM, 
      PBB_MAX_PORTS_IN_SYSTEM, 0},

    {"tPbbInstanceInfo", "SYS_DEF_MAX_NUM_CONTEXTS", 
      sizeof (tPbbInstanceInfo), SYS_DEF_MAX_NUM_CONTEXTS, 
      SYS_DEF_MAX_NUM_CONTEXTS, 0},

    {"tPbbContextNode", "SYS_DEF_MAX_NUM_CONTEXTS", 
      sizeof (tPbbContextNode), SYS_DEF_MAX_NUM_CONTEXTS, 
      SYS_DEF_MAX_NUM_CONTEXTS, 0},

    {"tPbbBufferEntry", "PBB_RED_BUF_ENTRIES", 
      sizeof (tPbbBufferEntry), PBB_RED_BUF_ENTRIES, 
      PBB_RED_BUF_ENTRIES, 0},

    {"0", "0", 0,0,0,0}
};

tFsModSizingInfo gFsPbbSizingInfo;
#else
extern tFsModSizingParams gFsPbbSizingParams[];
extern tFsModSizingInfo gFsPbbSizingInfo;
#endif

#endif
