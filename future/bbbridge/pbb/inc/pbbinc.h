/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbinc.h,v 1.10 2013/05/06 11:56:02 siva Exp $
 *
 * Description:  This file contains header files included in 
 *               PBB Module.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*                          pbbinc.h                                         */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  Date(DD/MM/YYYY)      : 31/07/2008                                       */
/*  Modified by           : PBB  TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/


#ifndef _PBBINC_H
#define _PBBINC_H

#ifdef PBB_PERFORMANCE_WANTED
#include  <sys/time.h>
#endif
#include "lr.h"
#include "pbbcli.h"
#include "cli.h"
#include "vcm.h" 
#include "cfa.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "fsvlan.h"
#include "hwaud.h"
#include "pbb.h"
#include "l2iwf.h"
#include "vlancli.h"
#include "bridge.h"
#ifdef MBSM_WANTED

#include "mbsm.h"
#endif /* End of MBSM Wanted */
#include "pbbcons.h"
#include "pbbport.h"
#include "pbbprot.h"
#include "fssyslog.h"
#include "pbbtdfs.h"
#include "pbbmacs.h"
#include "pbbtrc.h"
#ifdef L2RED_WANTED
#include "pbbred.h"
#endif /* L2RED_WANTED */
#include "pbbglob.h"
#include "pbbextn.h"
#include "fspbblw.h"
#include "fspbbwr.h"
#include "fs1ahwr.h"
#include "fs1ahlw.h"
#ifdef MBSM_WANTED
#include "pbbmbsm.h"
#endif/* End of MBSM Wanted */
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "rmgr.h"
#include "pbbnp.h"
#include "vlnmpbnp.h"
#include "nputil.h"
#ifdef MBSM_WANTED
#include "pbbnpx.h"
#endif/* End of MBSM Wanted */
#endif/* End of NPAPI Wanted */
#ifdef L2RED_WANTED
#include "hwaudmap.h"
#endif /* L2RED_WANTED */
#include "pbbsz.h"
#include "pbbparam.h"

#endif

