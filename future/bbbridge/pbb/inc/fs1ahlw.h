/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs1ahlw.h,v 1.2 2008/12/03 08:26:59 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbBackboneEdgeBridgeAddress ARG_LIST((tMacAddr * ));

INT1
nmhGetFsPbbBackboneEdgeBridgeName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbNumberOfIComponents ARG_LIST((UINT4 *));

INT1
nmhGetFsPbbNumberOfBComponents ARG_LIST((UINT4 *));

INT1
nmhGetFsPbbNumberOfBebPorts ARG_LIST((UINT4 *));

INT1
nmhGetFsPbbNextAvailablePipIfIndex ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbBackboneEdgeBridgeName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbBackboneEdgeBridgeName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbBackboneEdgeBridgeName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbVipTable. */
INT1
nmhValidateIndexInstanceFsPbbVipTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbVipTable  */

INT1
nmhGetFirstIndexFsPbbVipTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbVipTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbVipPipIfIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbbVipISid ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetFsPbbVipDefaultDstBMAC ARG_LIST((INT4  , INT4 ,tMacAddr * ));

INT1
nmhGetFsPbbVipType ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbVipRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbVipISid ARG_LIST((INT4  , INT4  ,UINT4 ));

INT1
nmhSetFsPbbVipDefaultDstBMAC ARG_LIST((INT4  , INT4  ,tMacAddr ));

INT1
nmhSetFsPbbVipType ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPbbVipRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbVipISid ARG_LIST((UINT4 *  ,INT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsPbbVipDefaultDstBMAC ARG_LIST((UINT4 *  ,INT4  , INT4  ,tMacAddr ));

INT1
nmhTestv2FsPbbVipType ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPbbVipRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbVipTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbISidToVipTable. */
INT1
nmhValidateIndexInstanceFsPbbISidToVipTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbISidToVipTable  */

INT1
nmhGetFirstIndexFsPbbISidToVipTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbISidToVipTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbISidToVipComponentId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsPbbISidToVipPort ARG_LIST((UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsPbbPipTable. */
INT1
nmhValidateIndexInstanceFsPbbPipTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbPipTable  */

INT1
nmhGetFirstIndexFsPbbPipTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbPipTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbPipBMACAddress ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsPbbPipName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbPipIComponentId ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbbPipRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbPipBMACAddress ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsPbbPipName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPbbPipRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbPipBMACAddress ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsPbbPipName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPbbPipRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbPipTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbVipToPipMappingTable. */
INT1
nmhValidateIndexInstanceFsPbbVipToPipMappingTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbVipToPipMappingTable  */

INT1
nmhGetFirstIndexFsPbbVipToPipMappingTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbVipToPipMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbVipToPipMappingPipIfIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbbVipToPipMappingStorageType ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbbVipToPipMappingRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbVipToPipMappingPipIfIndex ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPbbVipToPipMappingStorageType ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPbbVipToPipMappingRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbVipToPipMappingPipIfIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPbbVipToPipMappingStorageType ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPbbVipToPipMappingRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbVipToPipMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbCBPServiceMappingTable. */
INT1
nmhValidateIndexInstanceFsPbbCBPServiceMappingTable ARG_LIST((INT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbCBPServiceMappingTable  */

INT1
nmhGetFirstIndexFsPbbCBPServiceMappingTable ARG_LIST((INT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbCBPServiceMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbCBPServiceMappingBVid ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsPbbCBPServiceMappingDefaultBackboneDest ARG_LIST((INT4  , INT4  , UINT4 ,tMacAddr * ));

INT1
nmhGetFsPbbCBPServiceMappingType ARG_LIST((INT4  , INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbCBPServiceMappingLocalSid ARG_LIST((INT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsPbbCBPServiceMappingRowStatus ARG_LIST((INT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbCBPServiceMappingBVid ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsPbbCBPServiceMappingDefaultBackboneDest ARG_LIST((INT4  , INT4  , UINT4  ,tMacAddr ));

INT1
nmhSetFsPbbCBPServiceMappingType ARG_LIST((INT4  , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPbbCBPServiceMappingLocalSid ARG_LIST((INT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsPbbCBPServiceMappingRowStatus ARG_LIST((INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbCBPServiceMappingBVid ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsPbbCBPServiceMappingDefaultBackboneDest ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,tMacAddr ));

INT1
nmhTestv2FsPbbCBPServiceMappingType ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPbbCBPServiceMappingLocalSid ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsPbbCBPServiceMappingRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbCBPServiceMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbCbpTable. */
INT1
nmhValidateIndexInstanceFsPbbCbpTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbCbpTable  */

INT1
nmhGetFirstIndexFsPbbCbpTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbCbpTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbCbpRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbCbpRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbCbpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbCbpTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbPipToVipMappingTable. */
INT1
nmhValidateIndexInstanceFsPbbPipToVipMappingTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbPipToVipMappingTable  */

INT1
nmhGetFirstIndexFsPbbPipToVipMappingTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbPipToVipMappingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbPipToVipMappingStatus ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));
