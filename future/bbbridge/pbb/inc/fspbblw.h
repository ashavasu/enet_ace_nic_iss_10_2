/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbblw.h,v 1.2 2009/01/19 11:28:02 prabuc-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbShutdownStatus ARG_LIST((INT4 *));

INT1
nmhGetFsPbbGlbOUI ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbMaxNoOfISID ARG_LIST((INT4 *));

INT1
nmhGetFsPbbMaxNoOfISIDPerContext ARG_LIST((INT4 *));

INT1
nmhGetFsPbbMaxPortsPerISID ARG_LIST((INT4 *));

INT1
nmhGetFsPbbMaxPortsPerISIDPerContext ARG_LIST((INT4 *));

INT1
nmhGetFsPbbMaxCurrentNoOfISID ARG_LIST((INT4 *));

INT1
nmhGetFsPbbMaxCurrentISIDPerContext ARG_LIST((INT4 *));

INT1
nmhGetFsPbbMaxCurrentPortsPerISID ARG_LIST((INT4 *));

INT1
nmhGetFsPbbMaxCurrPortsPerISIDContext ARG_LIST((INT4 *));

INT1
nmhGetFsPbbTraceInput ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbTraceOption ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbShutdownStatus ARG_LIST((INT4 ));

INT1
nmhSetFsPbbGlbOUI ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPbbMaxNoOfISID ARG_LIST((INT4 ));

INT1
nmhSetFsPbbMaxNoOfISIDPerContext ARG_LIST((INT4 ));

INT1
nmhSetFsPbbMaxPortsPerISID ARG_LIST((INT4 ));

INT1
nmhSetFsPbbMaxPortsPerISIDPerContext ARG_LIST((INT4 ));

INT1
nmhSetFsPbbTraceInput ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbShutdownStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbGlbOUI ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPbbMaxNoOfISID ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbMaxNoOfISIDPerContext ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbMaxPortsPerISID ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbMaxPortsPerISIDPerContext ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbTraceInput ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbShutdownStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbGlbOUI ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbMaxNoOfISID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbMaxNoOfISIDPerContext ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbMaxPortsPerISID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbMaxPortsPerISIDPerContext ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbTraceInput ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbISIDOUITable. */
INT1
nmhValidateIndexInstanceFsPbbISIDOUITable ARG_LIST((INT4  , UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbISIDOUITable  */

INT1
nmhGetFirstIndexFsPbbISIDOUITable ARG_LIST((INT4 * , UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbISIDOUITable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbOUI ARG_LIST((INT4  , UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbOUIRowStatus ARG_LIST((INT4  , UINT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbOUI ARG_LIST((INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPbbOUIRowStatus ARG_LIST((INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbOUI ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPbbOUIRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbISIDOUITable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbPortPisidTable. */
INT1
nmhValidateIndexInstanceFsPbbPortPisidTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbPortPisidTable  */

INT1
nmhGetFirstIndexFsPbbPortPisidTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbPortPisidTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbPortPisid ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbbPIsidRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbPortPisid ARG_LIST((INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPbbPIsidRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbPortPisid ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPbbPIsidRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbPortPisidTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbPortTable. */
INT1
nmhValidateIndexInstanceFsPbbPortTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbPortTable  */

INT1
nmhGetFirstIndexFsPbbPortTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbPortTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbPortPcpSelectionRow ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbbPortUseDei ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbbPortReqDropEncoding ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbPortPcpSelectionRow ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbbPortUseDei ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbbPortReqDropEncoding ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbPortPcpSelectionRow ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbbPortUseDei ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbbPortReqDropEncoding ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbPcpDecodingTable. */
INT1
nmhValidateIndexInstanceFsPbbPcpDecodingTable ARG_LIST((INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbPcpDecodingTable  */

INT1
nmhGetFirstIndexFsPbbPcpDecodingTable ARG_LIST((INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbPcpDecodingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbPcpDecodingPriority ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsPbbPcpDecodingDropEligible ARG_LIST((INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbPcpDecodingPriority ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsPbbPcpDecodingDropEligible ARG_LIST((INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbPcpDecodingPriority ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsPbbPcpDecodingDropEligible ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbPcpDecodingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbPcpEncodingTable. */
INT1
nmhValidateIndexInstanceFsPbbPcpEncodingTable ARG_LIST((INT4  , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbPcpEncodingTable  */

INT1
nmhGetFirstIndexFsPbbPcpEncodingTable ARG_LIST((INT4 * , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbPcpEncodingTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbPcpEncodingPcpValue ARG_LIST((INT4  , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbPcpEncodingPcpValue ARG_LIST((INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbPcpEncodingPcpValue ARG_LIST((UINT4 *  ,INT4  , INT4  , INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbPcpEncodingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbInstanceTable. */
INT1
nmhValidateIndexInstanceFsPbbInstanceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbInstanceTable  */

INT1
nmhGetFirstIndexFsPbbInstanceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbInstanceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbInstanceMacAddr ARG_LIST((INT4 ,tMacAddr * ));

INT1
nmhGetFsPbbInstanceName ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbInstanceIComponents ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbbInstanceBComponents ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbbInstanceBebPorts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbbInstanceRowStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbInstanceMacAddr ARG_LIST((INT4  ,tMacAddr ));

INT1
nmhSetFsPbbInstanceName ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPbbInstanceRowStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbInstanceMacAddr ARG_LIST((UINT4 *  ,INT4  ,tMacAddr ));

INT1
nmhTestv2FsPbbInstanceName ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPbbInstanceRowStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbInstanceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbInstanceMappingTable. */
INT1
nmhValidateIndexInstanceFsPbbInstanceMappingTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbInstanceMappingTable  */

INT1
nmhGetFirstIndexFsPbbInstanceMappingTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbInstanceMappingTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbContextToInstanceId ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbContextToInstanceId ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbContextToInstanceId ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbInstanceMappingTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
