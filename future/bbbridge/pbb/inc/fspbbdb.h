/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fspbbdb.h,v 1.3 2009/01/19 11:28:02 prabuc-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPBBDB_H
#define _FSPBBDB_H

UINT1 FsPbbISIDOUITableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbPortPisidTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbPcpDecodingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbPcpEncodingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbInstanceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbbInstanceMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};

UINT4 fspbb [] ={1,3,6,1,4,1,29601,2,15};
tSNMP_OID_TYPE fspbbOID = {9, fspbb};


UINT4 FsPbbShutdownStatus [ ] ={1,3,6,1,4,1,29601,2,15,1,1};
UINT4 FsPbbGlbOUI [ ] ={1,3,6,1,4,1,29601,2,15,1,2};
UINT4 FsPbbMaxNoOfISID [ ] ={1,3,6,1,4,1,29601,2,15,1,3};
UINT4 FsPbbMaxNoOfISIDPerContext [ ] ={1,3,6,1,4,1,29601,2,15,1,4};
UINT4 FsPbbMaxPortsPerISID [ ] ={1,3,6,1,4,1,29601,2,15,1,5};
UINT4 FsPbbMaxPortsPerISIDPerContext [ ] ={1,3,6,1,4,1,29601,2,15,1,6};
UINT4 FsPbbMaxCurrentNoOfISID [ ] ={1,3,6,1,4,1,29601,2,15,1,7};
UINT4 FsPbbMaxCurrentISIDPerContext [ ] ={1,3,6,1,4,1,29601,2,15,1,8};
UINT4 FsPbbMaxCurrentPortsPerISID [ ] ={1,3,6,1,4,1,29601,2,15,1,9};
UINT4 FsPbbMaxCurrPortsPerISIDContext [ ] ={1,3,6,1,4,1,29601,2,15,1,10};
UINT4 FsPbbTraceInput [ ] ={1,3,6,1,4,1,29601,2,15,1,11};
UINT4 FsPbbTraceOption [ ] ={1,3,6,1,4,1,29601,2,15,1,12};
UINT4 FsPbbContextId [ ] ={1,3,6,1,4,1,29601,2,15,2,1,1,1};
UINT4 FsPbbOUI [ ] ={1,3,6,1,4,1,29601,2,15,2,1,1,2};
UINT4 FsPbbOUIRowStatus [ ] ={1,3,6,1,4,1,29601,2,15,2,1,1,3};
UINT4 FsPbbPortPisid [ ] ={1,3,6,1,4,1,29601,2,15,3,1,1,1};
UINT4 FsPbbPIsidRowStatus [ ] ={1,3,6,1,4,1,29601,2,15,3,1,1,2};
UINT4 FsPbbPortPcpSelectionRow [ ] ={1,3,6,1,4,1,29601,2,15,3,2,1,1};
UINT4 FsPbbPortUseDei [ ] ={1,3,6,1,4,1,29601,2,15,3,2,1,2};
UINT4 FsPbbPortReqDropEncoding [ ] ={1,3,6,1,4,1,29601,2,15,3,2,1,3};
UINT4 FsPbbPcpDecodingPcpSelRow [ ] ={1,3,6,1,4,1,29601,2,15,3,3,1,1};
UINT4 FsPbbPcpDecodingPcpValue [ ] ={1,3,6,1,4,1,29601,2,15,3,3,1,2};
UINT4 FsPbbPcpDecodingPriority [ ] ={1,3,6,1,4,1,29601,2,15,3,3,1,3};
UINT4 FsPbbPcpDecodingDropEligible [ ] ={1,3,6,1,4,1,29601,2,15,3,3,1,4};
UINT4 FsPbbPcpEncodingPcpSelRow [ ] ={1,3,6,1,4,1,29601,2,15,3,4,1,1};
UINT4 FsPbbPcpEncodingPriority [ ] ={1,3,6,1,4,1,29601,2,15,3,4,1,2};
UINT4 FsPbbPcpEncodingDropEligible [ ] ={1,3,6,1,4,1,29601,2,15,3,4,1,3};
UINT4 FsPbbPcpEncodingPcpValue [ ] ={1,3,6,1,4,1,29601,2,15,3,4,1,4};
UINT4 FsPbbInstanceId [ ] ={1,3,6,1,4,1,29601,2,15,4,1,1,1};
UINT4 FsPbbInstanceMacAddr [ ] ={1,3,6,1,4,1,29601,2,15,4,1,1,2};
UINT4 FsPbbInstanceName [ ] ={1,3,6,1,4,1,29601,2,15,4,1,1,3};
UINT4 FsPbbInstanceIComponents [ ] ={1,3,6,1,4,1,29601,2,15,4,1,1,4};
UINT4 FsPbbInstanceBComponents [ ] ={1,3,6,1,4,1,29601,2,15,4,1,1,5};
UINT4 FsPbbInstanceBebPorts [ ] ={1,3,6,1,4,1,29601,2,15,4,1,1,6};
UINT4 FsPbbInstanceRowStatus [ ] ={1,3,6,1,4,1,29601,2,15,4,1,1,7};
UINT4 FsPbbContextToInstanceId [ ] ={1,3,6,1,4,1,29601,2,15,4,2,1,1};


tMbDbEntry fspbbMibEntry[]= {

{{11,FsPbbShutdownStatus}, NULL, FsPbbShutdownStatusGet, FsPbbShutdownStatusSet, FsPbbShutdownStatusTest, FsPbbShutdownStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsPbbGlbOUI}, NULL, FsPbbGlbOUIGet, FsPbbGlbOUISet, FsPbbGlbOUITest, FsPbbGlbOUIDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "00:1E:83"},

{{11,FsPbbMaxNoOfISID}, NULL, FsPbbMaxNoOfISIDGet, FsPbbMaxNoOfISIDSet, FsPbbMaxNoOfISIDTest, FsPbbMaxNoOfISIDDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbMaxNoOfISIDPerContext}, NULL, FsPbbMaxNoOfISIDPerContextGet, FsPbbMaxNoOfISIDPerContextSet, FsPbbMaxNoOfISIDPerContextTest, FsPbbMaxNoOfISIDPerContextDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbMaxPortsPerISID}, NULL, FsPbbMaxPortsPerISIDGet, FsPbbMaxPortsPerISIDSet, FsPbbMaxPortsPerISIDTest, FsPbbMaxPortsPerISIDDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbMaxPortsPerISIDPerContext}, NULL, FsPbbMaxPortsPerISIDPerContextGet, FsPbbMaxPortsPerISIDPerContextSet, FsPbbMaxPortsPerISIDPerContextTest, FsPbbMaxPortsPerISIDPerContextDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbMaxCurrentNoOfISID}, NULL, FsPbbMaxCurrentNoOfISIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPbbMaxCurrentISIDPerContext}, NULL, FsPbbMaxCurrentISIDPerContextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPbbMaxCurrentPortsPerISID}, NULL, FsPbbMaxCurrentPortsPerISIDGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPbbMaxCurrPortsPerISIDContext}, NULL, FsPbbMaxCurrPortsPerISIDContextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsPbbTraceInput}, NULL, FsPbbTraceInputGet, FsPbbTraceInputSet, FsPbbTraceInputTest, FsPbbTraceInputDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "critical"},

{{11,FsPbbTraceOption}, NULL, FsPbbTraceOptionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "256"},

{{13,FsPbbContextId}, GetNextIndexFsPbbISIDOUITable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbbISIDOUITableINDEX, 3, 0, 0, NULL},

{{13,FsPbbOUI}, GetNextIndexFsPbbISIDOUITable, FsPbbOUIGet, FsPbbOUISet, FsPbbOUITest, FsPbbISIDOUITableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPbbISIDOUITableINDEX, 3, 0, 0, NULL},

{{13,FsPbbOUIRowStatus}, GetNextIndexFsPbbISIDOUITable, FsPbbOUIRowStatusGet, FsPbbOUIRowStatusSet, FsPbbOUIRowStatusTest, FsPbbISIDOUITableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbISIDOUITableINDEX, 3, 0, 1, NULL},

{{13,FsPbbPortPisid}, GetNextIndexFsPbbPortPisidTable, FsPbbPortPisidGet, FsPbbPortPisidSet, FsPbbPortPisidTest, FsPbbPortPisidTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPbbPortPisidTableINDEX, 2, 0, 0, NULL},

{{13,FsPbbPIsidRowStatus}, GetNextIndexFsPbbPortPisidTable, FsPbbPIsidRowStatusGet, FsPbbPIsidRowStatusSet, FsPbbPIsidRowStatusTest, FsPbbPortPisidTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbPortPisidTableINDEX, 2, 0, 1, NULL},

{{13,FsPbbPortPcpSelectionRow}, GetNextIndexFsPbbPortTable, FsPbbPortPcpSelectionRowGet, FsPbbPortPcpSelectionRowSet, FsPbbPortPcpSelectionRowTest, FsPbbPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbPortTableINDEX, 1, 0, 0, "1"},

{{13,FsPbbPortUseDei}, GetNextIndexFsPbbPortTable, FsPbbPortUseDeiGet, FsPbbPortUseDeiSet, FsPbbPortUseDeiTest, FsPbbPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbPortTableINDEX, 1, 0, 0, "2"},

{{13,FsPbbPortReqDropEncoding}, GetNextIndexFsPbbPortTable, FsPbbPortReqDropEncodingGet, FsPbbPortReqDropEncodingSet, FsPbbPortReqDropEncodingTest, FsPbbPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbPortTableINDEX, 1, 0, 0, "2"},

{{13,FsPbbPcpDecodingPcpSelRow}, GetNextIndexFsPbbPcpDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbbPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{13,FsPbbPcpDecodingPcpValue}, GetNextIndexFsPbbPcpDecodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbbPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{13,FsPbbPcpDecodingPriority}, GetNextIndexFsPbbPcpDecodingTable, FsPbbPcpDecodingPriorityGet, FsPbbPcpDecodingPrioritySet, FsPbbPcpDecodingPriorityTest, FsPbbPcpDecodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{13,FsPbbPcpDecodingDropEligible}, GetNextIndexFsPbbPcpDecodingTable, FsPbbPcpDecodingDropEligibleGet, FsPbbPcpDecodingDropEligibleSet, FsPbbPcpDecodingDropEligibleTest, FsPbbPcpDecodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbPcpDecodingTableINDEX, 3, 0, 0, NULL},

{{13,FsPbbPcpEncodingPcpSelRow}, GetNextIndexFsPbbPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbbPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{13,FsPbbPcpEncodingPriority}, GetNextIndexFsPbbPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbbPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{13,FsPbbPcpEncodingDropEligible}, GetNextIndexFsPbbPcpEncodingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbbPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{13,FsPbbPcpEncodingPcpValue}, GetNextIndexFsPbbPcpEncodingTable, FsPbbPcpEncodingPcpValueGet, FsPbbPcpEncodingPcpValueSet, FsPbbPcpEncodingPcpValueTest, FsPbbPcpEncodingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbPcpEncodingTableINDEX, 4, 0, 0, NULL},

{{13,FsPbbInstanceId}, GetNextIndexFsPbbInstanceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbbInstanceTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbInstanceMacAddr}, GetNextIndexFsPbbInstanceTable, FsPbbInstanceMacAddrGet, FsPbbInstanceMacAddrSet, FsPbbInstanceMacAddrTest, FsPbbInstanceTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsPbbInstanceTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbInstanceName}, GetNextIndexFsPbbInstanceTable, FsPbbInstanceNameGet, FsPbbInstanceNameSet, FsPbbInstanceNameTest, FsPbbInstanceTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPbbInstanceTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbInstanceIComponents}, GetNextIndexFsPbbInstanceTable, FsPbbInstanceIComponentsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPbbInstanceTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbInstanceBComponents}, GetNextIndexFsPbbInstanceTable, FsPbbInstanceBComponentsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPbbInstanceTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbInstanceBebPorts}, GetNextIndexFsPbbInstanceTable, FsPbbInstanceBebPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPbbInstanceTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbInstanceRowStatus}, GetNextIndexFsPbbInstanceTable, FsPbbInstanceRowStatusGet, FsPbbInstanceRowStatusSet, FsPbbInstanceRowStatusTest, FsPbbInstanceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbInstanceTableINDEX, 1, 0, 1, NULL},

{{13,FsPbbContextToInstanceId}, GetNextIndexFsPbbInstanceMappingTable, FsPbbContextToInstanceIdGet, FsPbbContextToInstanceIdSet, FsPbbContextToInstanceIdTest, FsPbbInstanceMappingTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPbbInstanceMappingTableINDEX, 1, 0, 0, "0"},
};
tMibData fspbbEntry = { 36, fspbbMibEntry };
#endif /* _FSPBBDB_H */

