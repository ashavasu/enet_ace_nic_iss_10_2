/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs1ahdb.h,v 1.2 2008/12/03 08:26:59 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FS1AHDB_H
#define _FS1AHDB_H

UINT1 FsPbbVipTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbISidToVipTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsPbbPipTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbVipToPipMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbCBPServiceMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsPbbCbpTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbPipToVipMappingTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};

UINT4 fs1ah [] ={1,3,6,1,4,1,29601,2,14};
tSNMP_OID_TYPE fs1ahOID = {9, fs1ah};


UINT4 FsPbbBackboneEdgeBridgeAddress [ ] ={1,3,6,1,4,1,29601,2,14,1,1,1,1};
UINT4 FsPbbBackboneEdgeBridgeName [ ] ={1,3,6,1,4,1,29601,2,14,1,1,1,2};
UINT4 FsPbbNumberOfIComponents [ ] ={1,3,6,1,4,1,29601,2,14,1,1,1,3};
UINT4 FsPbbNumberOfBComponents [ ] ={1,3,6,1,4,1,29601,2,14,1,1,1,4};
UINT4 FsPbbNumberOfBebPorts [ ] ={1,3,6,1,4,1,29601,2,14,1,1,1,5};
UINT4 FsPbbNextAvailablePipIfIndex [ ] ={1,3,6,1,4,1,29601,2,14,1,1,1,6};
UINT4 Fsdot1ahContextId [ ] ={1,3,6,1,4,1,29601,2,14,1,1,2,1,1};
UINT4 FsPbbVipPipIfIndex [ ] ={1,3,6,1,4,1,29601,2,14,1,1,2,1,2};
UINT4 FsPbbVipISid [ ] ={1,3,6,1,4,1,29601,2,14,1,1,2,1,3};
UINT4 FsPbbVipDefaultDstBMAC [ ] ={1,3,6,1,4,1,29601,2,14,1,1,2,1,4};
UINT4 FsPbbVipType [ ] ={1,3,6,1,4,1,29601,2,14,1,1,2,1,5};
UINT4 FsPbbVipRowStatus [ ] ={1,3,6,1,4,1,29601,2,14,1,1,2,1,6};
UINT4 FsPbbISidToVipISid [ ] ={1,3,6,1,4,1,29601,2,14,1,1,3,1,1};
UINT4 FsPbbISidToVipComponentId [ ] ={1,3,6,1,4,1,29601,2,14,1,1,3,1,2};
UINT4 FsPbbISidToVipPort [ ] ={1,3,6,1,4,1,29601,2,14,1,1,3,1,3};
UINT4 FsPbbPipIfIndex [ ] ={1,3,6,1,4,1,29601,2,14,1,1,4,1,1};
UINT4 FsPbbPipBMACAddress [ ] ={1,3,6,1,4,1,29601,2,14,1,1,4,1,2};
UINT4 FsPbbPipName [ ] ={1,3,6,1,4,1,29601,2,14,1,1,4,1,3};
UINT4 FsPbbPipIComponentId [ ] ={1,3,6,1,4,1,29601,2,14,1,1,4,1,4};
UINT4 FsPbbPipRowStatus [ ] ={1,3,6,1,4,1,29601,2,14,1,1,4,1,5};
UINT4 FsPbbVipToPipMappingPipIfIndex [ ] ={1,3,6,1,4,1,29601,2,14,1,1,5,1,1};
UINT4 FsPbbVipToPipMappingStorageType [ ] ={1,3,6,1,4,1,29601,2,14,1,1,5,1,2};
UINT4 FsPbbVipToPipMappingRowStatus [ ] ={1,3,6,1,4,1,29601,2,14,1,1,5,1,3};
UINT4 FsPbbCBPServiceMappingBackboneSid [ ] ={1,3,6,1,4,1,29601,2,14,1,1,6,1,1};
UINT4 FsPbbCBPServiceMappingBVid [ ] ={1,3,6,1,4,1,29601,2,14,1,1,6,1,2};
UINT4 FsPbbCBPServiceMappingDefaultBackboneDest [ ] ={1,3,6,1,4,1,29601,2,14,1,1,6,1,3};
UINT4 FsPbbCBPServiceMappingType [ ] ={1,3,6,1,4,1,29601,2,14,1,1,6,1,4};
UINT4 FsPbbCBPServiceMappingLocalSid [ ] ={1,3,6,1,4,1,29601,2,14,1,1,6,1,5};
UINT4 FsPbbCBPServiceMappingRowStatus [ ] ={1,3,6,1,4,1,29601,2,14,1,1,6,1,6};
UINT4 FsPbbCbpRowStatus [ ] ={1,3,6,1,4,1,29601,2,14,1,1,7,1,1};
UINT4 FsPbbPipToVipMappingStatus [ ] ={1,3,6,1,4,1,29601,2,14,1,1,8,1,1};


tMbDbEntry fs1ahMibEntry[]= {

{{13,FsPbbBackboneEdgeBridgeAddress}, NULL, FsPbbBackboneEdgeBridgeAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsPbbBackboneEdgeBridgeName}, NULL, FsPbbBackboneEdgeBridgeNameGet, FsPbbBackboneEdgeBridgeNameSet, FsPbbBackboneEdgeBridgeNameTest, FsPbbBackboneEdgeBridgeNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsPbbNumberOfIComponents}, NULL, FsPbbNumberOfIComponentsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsPbbNumberOfBComponents}, NULL, FsPbbNumberOfBComponentsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsPbbNumberOfBebPorts}, NULL, FsPbbNumberOfBebPortsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsPbbNextAvailablePipIfIndex}, NULL, FsPbbNextAvailablePipIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,Fsdot1ahContextId}, GetNextIndexFsPbbVipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbbVipTableINDEX, 2, 0, 0, NULL},

{{14,FsPbbVipPipIfIndex}, GetNextIndexFsPbbVipTable, FsPbbVipPipIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbbVipTableINDEX, 2, 0, 0, "0"},

{{14,FsPbbVipISid}, GetNextIndexFsPbbVipTable, FsPbbVipISidGet, FsPbbVipISidSet, FsPbbVipISidTest, FsPbbVipTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPbbVipTableINDEX, 2, 0, 0, "1"},

{{14,FsPbbVipDefaultDstBMAC}, GetNextIndexFsPbbVipTable, FsPbbVipDefaultDstBMACGet, FsPbbVipDefaultDstBMACSet, FsPbbVipDefaultDstBMACTest, FsPbbVipTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsPbbVipTableINDEX, 2, 0, 0, "001e83000001"},

{{14,FsPbbVipType}, GetNextIndexFsPbbVipTable, FsPbbVipTypeGet, FsPbbVipTypeSet, FsPbbVipTypeTest, FsPbbVipTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPbbVipTableINDEX, 2, 0, 0, "3"},

{{14,FsPbbVipRowStatus}, GetNextIndexFsPbbVipTable, FsPbbVipRowStatusGet, FsPbbVipRowStatusSet, FsPbbVipRowStatusTest, FsPbbVipTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbVipTableINDEX, 2, 0, 1, NULL},

{{14,FsPbbISidToVipISid}, GetNextIndexFsPbbISidToVipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsPbbISidToVipTableINDEX, 1, 0, 0, NULL},

{{14,FsPbbISidToVipComponentId}, GetNextIndexFsPbbISidToVipTable, FsPbbISidToVipComponentIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPbbISidToVipTableINDEX, 1, 0, 0, NULL},

{{14,FsPbbISidToVipPort}, GetNextIndexFsPbbISidToVipTable, FsPbbISidToVipPortGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbbISidToVipTableINDEX, 1, 0, 0, NULL},

{{14,FsPbbPipIfIndex}, GetNextIndexFsPbbPipTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbbPipTableINDEX, 1, 0, 0, NULL},

{{14,FsPbbPipBMACAddress}, GetNextIndexFsPbbPipTable, FsPbbPipBMACAddressGet, FsPbbPipBMACAddressSet, FsPbbPipBMACAddressTest, FsPbbPipTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsPbbPipTableINDEX, 1, 0, 0, NULL},

{{14,FsPbbPipName}, GetNextIndexFsPbbPipTable, FsPbbPipNameGet, FsPbbPipNameSet, FsPbbPipNameTest, FsPbbPipTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPbbPipTableINDEX, 1, 0, 0, "0"},

{{14,FsPbbPipIComponentId}, GetNextIndexFsPbbPipTable, FsPbbPipIComponentIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPbbPipTableINDEX, 1, 0, 0, NULL},

{{14,FsPbbPipRowStatus}, GetNextIndexFsPbbPipTable, FsPbbPipRowStatusGet, FsPbbPipRowStatusSet, FsPbbPipRowStatusTest, FsPbbPipTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbPipTableINDEX, 1, 0, 1, NULL},

{{14,FsPbbVipToPipMappingPipIfIndex}, GetNextIndexFsPbbVipToPipMappingTable, FsPbbVipToPipMappingPipIfIndexGet, FsPbbVipToPipMappingPipIfIndexSet, FsPbbVipToPipMappingPipIfIndexTest, FsPbbVipToPipMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbVipToPipMappingTableINDEX, 2, 0, 0, NULL},

{{14,FsPbbVipToPipMappingStorageType}, GetNextIndexFsPbbVipToPipMappingTable, FsPbbVipToPipMappingStorageTypeGet, FsPbbVipToPipMappingStorageTypeSet, FsPbbVipToPipMappingStorageTypeTest, FsPbbVipToPipMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbVipToPipMappingTableINDEX, 2, 0, 0, NULL},

{{14,FsPbbVipToPipMappingRowStatus}, GetNextIndexFsPbbVipToPipMappingTable, FsPbbVipToPipMappingRowStatusGet, FsPbbVipToPipMappingRowStatusSet, FsPbbVipToPipMappingRowStatusTest, FsPbbVipToPipMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbVipToPipMappingTableINDEX, 2, 0, 1, NULL},

{{14,FsPbbCBPServiceMappingBackboneSid}, GetNextIndexFsPbbCBPServiceMappingTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsPbbCBPServiceMappingTableINDEX, 3, 0, 0, NULL},

{{14,FsPbbCBPServiceMappingBVid}, GetNextIndexFsPbbCBPServiceMappingTable, FsPbbCBPServiceMappingBVidGet, FsPbbCBPServiceMappingBVidSet, FsPbbCBPServiceMappingBVidTest, FsPbbCBPServiceMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbCBPServiceMappingTableINDEX, 3, 0, 0, NULL},

{{14,FsPbbCBPServiceMappingDefaultBackboneDest}, GetNextIndexFsPbbCBPServiceMappingTable, FsPbbCBPServiceMappingDefaultBackboneDestGet, FsPbbCBPServiceMappingDefaultBackboneDestSet, FsPbbCBPServiceMappingDefaultBackboneDestTest, FsPbbCBPServiceMappingTableDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, FsPbbCBPServiceMappingTableINDEX, 3, 0, 0, NULL},

{{14,FsPbbCBPServiceMappingType}, GetNextIndexFsPbbCBPServiceMappingTable, FsPbbCBPServiceMappingTypeGet, FsPbbCBPServiceMappingTypeSet, FsPbbCBPServiceMappingTypeTest, FsPbbCBPServiceMappingTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPbbCBPServiceMappingTableINDEX, 3, 0, 0, NULL},

{{14,FsPbbCBPServiceMappingLocalSid}, GetNextIndexFsPbbCBPServiceMappingTable, FsPbbCBPServiceMappingLocalSidGet, FsPbbCBPServiceMappingLocalSidSet, FsPbbCBPServiceMappingLocalSidTest, FsPbbCBPServiceMappingTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPbbCBPServiceMappingTableINDEX, 3, 0, 0, "1"},

{{14,FsPbbCBPServiceMappingRowStatus}, GetNextIndexFsPbbCBPServiceMappingTable, FsPbbCBPServiceMappingRowStatusGet, FsPbbCBPServiceMappingRowStatusSet, FsPbbCBPServiceMappingRowStatusTest, FsPbbCBPServiceMappingTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbCBPServiceMappingTableINDEX, 3, 0, 1, NULL},

{{14,FsPbbCbpRowStatus}, GetNextIndexFsPbbCbpTable, FsPbbCbpRowStatusGet, FsPbbCbpRowStatusSet, FsPbbCbpRowStatusTest, FsPbbCbpTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbCbpTableINDEX, 2, 0, 1, NULL},

{{14,FsPbbPipToVipMappingStatus}, GetNextIndexFsPbbPipToVipMappingTable, FsPbbPipToVipMappingStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsPbbPipToVipMappingTableINDEX, 3, 0, 0, NULL},
};
tMibData fs1ahEntry = { 31, fs1ahMibEntry };
#endif /* _FS1AHDB_H */

