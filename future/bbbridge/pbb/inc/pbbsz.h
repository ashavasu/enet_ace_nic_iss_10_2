/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbsz.h,v 1.5 2012/03/21 13:03:47 siva Exp $
 *
 * Description:This file contains prototypes of mempools Creation/
 *             Deletion functions.
 *
 *******************************************************************/
enum {
    MAX_PBB_BACKBONE_SERV_INST_ENTRIES_SIZING_ID,
    MAX_PBB_CBP_PORT_LIST_COUNT_SIZING_ID,
    MAX_PBB_CNP_PORT_LIST_COUNT_SIZING_ID,
    MAX_PBB_CONTEXT_INFO_SIZING_ID,
    MAX_PBB_CONTEXTS_SIZING_ID,
    MAX_PBB_ICOMP_VIP_RBTREE_NODES_SIZING_ID,
    MAX_PBB_INSTANCE_INFO_SIZING_ID,
    MAX_PBB_ISID_PIP_VIP_NODES_SIZING_ID,
    MAX_PBB_ISID_RBTREE_NODES_SIZING_ID,
    MAX_PBB_PCP_DATA_COUNT_SIZING_ID,
    MAX_PBB_PORT_RBTREE_NODES_SIZING_ID,
    MAX_PBB_PORTS_PER_ISID_ENTRIES_SIZING_ID,
    MAX_PBB_Q_MESG_SIZING_ID,
    MAX_PBB_RED_BUFFER_ENTRIES_SIZING_ID,
    PBB_MAX_SIZING_ID
};

#ifdef  _PBBSZ_C
tMemPoolId PBBMemPoolIds[ PBB_MAX_SIZING_ID];
INT4  PbbSizingMemCreateMemPools(VOID);
VOID  PbbSizingMemDeleteMemPools(VOID);
INT4  PbbSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _PBBSZ_C  */
extern tMemPoolId PBBMemPoolIds[ ];
extern INT4  PbbSizingMemCreateMemPools(VOID);
extern VOID  PbbSizingMemDeleteMemPools(VOID);
extern INT4  PbbSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _PBBSZ_C  */


#ifdef  _PBBSZ_C
tFsModSizingParams FsPBBSizingParams [] = {
{ "tPbbBackBoneSerInstEntry", "MAX_PBB_BACKBONE_SERV_INST_ENTRIES", sizeof(tPbbBackBoneSerInstEntry),MAX_PBB_BACKBONE_SERV_INST_ENTRIES, MAX_PBB_BACKBONE_SERV_INST_ENTRIES,0 },
{ "tPbbCbpPortList", "MAX_PBB_CBP_PORT_LIST_COUNT", sizeof(tPbbCbpPortList),MAX_PBB_CBP_PORT_LIST_COUNT, MAX_PBB_CBP_PORT_LIST_COUNT,0 },
{ "tPbbCnpPortList", "MAX_PBB_CNP_PORT_LIST_COUNT", sizeof(tPbbCnpPortList),MAX_PBB_CNP_PORT_LIST_COUNT, MAX_PBB_CNP_PORT_LIST_COUNT,0 },
{ "tPbbContextInfo", "MAX_PBB_CONTEXT_INFO", sizeof(tPbbContextInfo),MAX_PBB_CONTEXT_INFO, MAX_PBB_CONTEXT_INFO,0 },
{ "tPbbContextNode", "MAX_PBB_CONTEXTS", sizeof(tPbbContextNode),MAX_PBB_CONTEXTS, MAX_PBB_CONTEXTS,0 },
{ "tPbbICompVipRBtreeNode", "MAX_PBB_ICOMP_VIP_RBTREE_NODES", sizeof(tPbbICompVipRBtreeNode),MAX_PBB_ICOMP_VIP_RBTREE_NODES, MAX_PBB_ICOMP_VIP_RBTREE_NODES,0 },
{ "tPbbInstanceInfo", "MAX_PBB_INSTANCE_INFO", sizeof(tPbbInstanceInfo),MAX_PBB_INSTANCE_INFO, MAX_PBB_INSTANCE_INFO,0 },
{ "tPbbIsidPipVipNode", "MAX_PBB_ISID_PIP_VIP_NODES", sizeof(tPbbIsidPipVipNode),MAX_PBB_ISID_PIP_VIP_NODES, MAX_PBB_ISID_PIP_VIP_NODES,0 },
{ "tPbbIsidRBtreeNode", "MAX_PBB_ISID_RBTREE_NODES", sizeof(tPbbIsidRBtreeNode),MAX_PBB_ISID_RBTREE_NODES, MAX_PBB_ISID_RBTREE_NODES,0 },
{ "tPbbPcpData", "MAX_PBB_PCP_DATA_COUNT", sizeof(tPbbPcpData),MAX_PBB_PCP_DATA_COUNT, MAX_PBB_PCP_DATA_COUNT,0 },
{ "tPbbPortRBtreeNode", "MAX_PBB_PORT_RBTREE_NODES", sizeof(tPbbPortRBtreeNode),MAX_PBB_PORT_RBTREE_NODES, MAX_PBB_PORT_RBTREE_NODES,0 },
{ "tCbpStatusInfo[PBB_MAX_PORTS_PER_ISID]", "MAX_PBB_PORTS_PER_ISID_ENTRIES", sizeof(tCbpStatusInfo[PBB_MAX_PORTS_PER_ISID]),MAX_PBB_PORTS_PER_ISID_ENTRIES, MAX_PBB_PORTS_PER_ISID_ENTRIES,0 },
{ "tPbbQMsg", "MAX_PBB_Q_MESG", sizeof(tPbbQMsg),MAX_PBB_Q_MESG, MAX_PBB_Q_MESG,0 },
{ "tPbbBufferEntry", "MAX_PBB_RED_BUFFER_ENTRIES", sizeof(tPbbBufferEntry),MAX_PBB_RED_BUFFER_ENTRIES, MAX_PBB_RED_BUFFER_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _PBBSZ_C  */
extern tFsModSizingParams FsPBBSizingParams [];
#endif /*  _PBBSZ_C  */


