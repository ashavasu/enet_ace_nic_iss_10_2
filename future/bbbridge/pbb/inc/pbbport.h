
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : pbbport.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : PBB                                             */
/*  MODULE NAME           : PBB                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains portability related constants */
/*                          and macros used in PBB module.                  */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : PBB TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _PBBPORT_H
#define _PBBPORT_H

#define   PBB_MEMSET(inf1,data,cnt)  MEMSET(inf1,data,cnt)
#define   PBB_MEMCPY(inf1,inf2,cnt)  MEMCPY(inf1,inf2,cnt)
#define   PBB_MEMCMP(inf1,inf2,cnt)  MEMCMP(inf1,inf2,cnt)
#define   PBB_STRCAT(inf1,inf2)      STRCAT(inf1,inf2)
#define   PBB_STRNCAT(inf1,inf2,cnt) STRNCAT(inf1,inf2,cnt)
#define   PBB_STRLEN                 STRLEN
#define   PBB_STRNCPY                STRNCPY
#define   PBB_MALLOC      MEM_MALLOC
#define   PBB_MEMFREE     MEM_FREE  




/* SEM Related Macros */

#define PBB_CREATE_SEM(pu1Sem, u4Count, u4Flags, pSemId) \
        OsixCreateSem ((pu1Sem), (u4Count), (u4Flags), (pSemId))

#define PBB_DELETE_SEM                 OsixSemDel
 
#define PBB_TAKE_SEM(SemId) OsixSemTake(SemId)

#define PBB_RELEASE_SEM(SemId) OsixSemGive(SemId)

#define PBB_RECEIVE_EVENT             OsixEvtRecv

#define PBB_SEND_EVENT                OsixEvtSend

#define PBB_SPAWN_TASK                OsixTskCrt

#define PBB_DELETE_TASK               OsixTskDel 


#define PBB_GET_BUF(u1BufType, u4Size)     PbbGetBuf ((UINT1)(u1BufType), (UINT4)(u4Size))

#define PBB_RELEASE_BUF(u1BufType, pu1Buf) PbbReleaseBuf ((UINT1)(u1BufType), (UINT1 *)(pu1Buf))

/* MEM POOL Related Macros */

#define PBB_CREATE_MEM_POOL(u4Size, u4No, u4Type, pPoolId) \
        MemCreateMemPool ((u4Size), (u4No), (u4Type), (pPoolId))

#define PBB_DELETE_MEM_POOL(PoolId) \
        MemDeleteMemPool ((PoolId))

#define PBB_ALLOC_MEM_BLOCK(PoolId, pBlock) \
    (pBlock = MemAllocMemBlk (PoolId)) 

#define PBB_RELEASE_MEM_BLOCK(PoolId, pu1Block) \
        MemReleaseMemBlock ((PoolId), (pu1Block))
   
   
#define PBB_SEM_NODE_ID      SELF
#define PBB_SEM_NAME                ((const UINT1 *)"PBB1")




/* PBB buffers Id */
#define PBB_ISID_BUFF                1
#define PBB_VIP_BUFF                 2
#define PBB_MESSAGE_BUFFER           3
#define PBB_PORT_BUFF                4
#define PBB_PORT_ISID_BUFF           5
#define PBB_QMSG_BUFF                6
#define PBB_CONTEXT_INFO             7
#define PBB_Q_IN_PKT_BUFF            8
#define PBB_ISID_VIP_INFO            9
#define PBB_ISID_CBP_DLL_BUFF       10
#define PBB_ISID_PIP_VIP_DLL_BUFF   11
#define PBB_ISID_CNP_DLL_BUFF       12
#define PBB_PIP_PCP_BUFF            13
#define PBB_INSTANCE_BUFF           14
#define PBB_CONTEXT_DLL_BUFF        15
#define PBB_GET_BUF_LEN(pBuf) \
        CRU_BUF_Get_ChainValidByteCount(pBuf)  


#define PBB_INIT_COMPLETE(u4Status)	lrInitComplete(u4Status)
#define PBB_SEND_TO_QUEUE               OsixQueSend
#define PBB_RECV_FROM_QUEUE             OsixQueRecv
#define PBB_CREATE_QUEUE                 OsixQueCrt
#define PBB_DELETE_QUEUE                 OsixQueDel
#define PBB_GET_TASK_ID                 OsixGetTaskId 
#define PBB_SELF                        SELF

#define PBB_TASK            ((const UINT1*)"PBB")
    
#define PBB_TASK_QUEUE      ((UINT1*)"PBBAQ")
#define PBB_CFG_QUEUE       ((UINT1*)"PBBCQ")

#define PBB_CFG_Q_DEPTH     PBB_MAX_PORTS * 4

#define PBB_CFG_QUEUE_ID     gPbbGlobData.PbbTaskInfo.PbbCfgQId

#define PBB_CFG_MSG_EVENT      0x04
#define PBB_RED_BULK_UPD_EVENT 0x08
        
/* Transit time handling */


#define PBB_DEF_CONTEXT_ID         L2IWF_DEFAULT_CONTEXT

#define PBB_L2IWF_GETNEXT_VALID_PORT  PbbL2IwfGetNextValidPortForContext

#define PBB_MAX_TRANSLATE_ISID_VAL 16777214
#define PBB_MIN_TRANSLATE_ISID_VAL 256

/* porting Macros to fsap2 for DLL */
#define PBB_DLL_ADD       TMO_DLL_Add 
#define PBB_DLL_DEL       TMO_DLL_Delete 
#define PBB_DLL_INIT      TMO_DLL_Init    
#define PBB_DLL_INIT_NODE TMO_DLL_Init_Node    
#define PBB_DLL_SCAN      TMO_DLL_Scan 
#define PBB_DLL_COUNT     TMO_DLL_Count
#define PBB_DLL_GET       TMO_DLL_Get
#define PBB_DLL_FIRST     TMO_DLL_First
#define PBB_DLL_NEXT      TMO_DLL_Next
#define tPBB_DLL          tTMO_DLL 
#define tPBB_DLL_NODE     tTMO_DLL_NODE 
#define PBB_DLL_INSERT_NODE   TMO_DLL_Insert

#endif
