/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbtrc.h,v 1.5 2011/09/12 06:42:14 siva Exp $
 *
 * Description: This file contains trace and debug related
 *              macros used in PBB module.
 *
 *******************************************************************/
#ifndef _PBBTRC_H
#define _PBBTRC_H


extern VOID
PbbGlobalTrace (UINT4 u4ContextId, UINT4 u4ModTrc, 
   UINT4 u4Flags, const char *fmt, ...);


#define PBB_TRC_MAX_SIZE              288
#define PBB_TRC_MIN_SIZE              1

#define   PBB_INVALID_TRC        0x00000000
#define   PBB_CRITICAL_TRC       0x00000100
#define   PBB_RED_TRC            0x00000200
/* Reserved  */
#define   PBB_RES_TRC            0x00000400

#define PBB_ALL_TRC           (INIT_SHUT_TRC        |\
                               MGMT_TRC             |\
                               DATA_PATH_TRC        |\
                               CONTROL_PLANE_TRC    |\
                               DUMP_TRC             |\
                               OS_RESOURCE_TRC      |\
                               ALL_FAILURE_TRC      |\
                               BUFFER_TRC           |\
                               PBB_CRITICAL_TRC    |\
                               PBB_RED_TRC)


#ifdef TRACE_WANTED

#ifdef __GNUC__
#define PBB_FUNCTION_NAME    __FUNCTION__
#define PBB_FILE_NAME        __FILE__
#else
#define PBB_FUNCTION_NAME    __LINE__
#define PBB_FILE_NAME        __FILE__
#endif


#define  PBB_TRC_FLAG  gPbbGlobData.u4TraceOption
#define   PBB_NAME       ("PBB:")


#define PBB_PKT_DUMP(TraceType, pBuf, Length, Str)                           \
        MOD_PKT_DUMP(PBB_TRC_FLAG, TraceType, PBB_NAME, pBuf, Length, Str)

#define PBB_TRC(TraceType, Str)                                              \
        MOD_TRC(PBB_TRC_FLAG, TraceType, PBB_NAME, Str)

#define PBB_TRC_ARG1(TraceType, Str, Arg1)                                   \
        MOD_TRC_ARG1(PBB_TRC_FLAG, TraceType, PBB_NAME, Str, Arg1)

#define PBB_TRC_ARG2(TraceType, Str, Arg1, Arg2)                             \
        MOD_TRC_ARG2(PBB_TRC_FLAG, TraceType, PBB_NAME, Str, Arg1, Arg2)

#define PBB_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)                       \
        MOD_TRC_ARG3(PBB_TRC_FLAG, TraceType, PBB_NAME, Str, Arg1, Arg2,  \
                     Arg3)

#define PBB_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)                 \
        MOD_TRC_ARG4(PBB_TRC_FLAG, TraceType, PBB_NAME, Str, Arg1, Arg2,  \
                     Arg3,Arg4)

#define PBB_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5)           \
        MOD_TRC_ARG5(PBB_TRC_FLAG, TraceType, PBB_NAME, Str, Arg1, Arg2,  \
                     Arg3, Arg4, Arg5)

#else  /* TRACE_WANTED */
   #define PBB_PKT_DUMP(TraceType, pBuf, Length, Str)
   #define PBB_TRC(TraceType, Str)
   #define PBB_TRC_ARG1(TraceType, Str, Arg1)
   #define PBB_TRC_ARG2(TraceType, Str, Arg1, Arg2)
   #define PBB_TRC_ARG3(TraceType, Str, Arg1, Arg2, Arg3)
   #define PBB_TRC_ARG4(TraceType, Str, Arg1, Arg2, Arg3, Arg4)
   #define PBB_TRC_ARG5(TraceType, Str, Arg1, Arg2, Arg3, Arg4, Arg5)
#endif /* TRACE_WANTED */
#endif /* _PBBTRC_H_ */

