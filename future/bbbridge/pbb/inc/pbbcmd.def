/********************************************************************
* Copyright (C) 2014 Aricent Inc . All Rights Reserved               
*                                                                    
* $Id: pbbcmd.def,v 1.16 2014/11/24 12:13:54 siva Exp $                                                         
*                                                                    
*********************************************************************/
DEFINE GROUP: PBB_GBL_CMDS

 
   COMMAND : service instance  <integer(256-16777214)>
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_ISID_MODE, NULL, $2);
   SYNTAX  : service instance <service-instance(256-16777214)>
   PRVID   : 15
   HELP    : Configures an ISID.
  CXT_HELP : service Configures service instance mode|
             instance Service instance related configuration|
             (256-16777214) Service-instance identifier|
             <CR> Configures an ISID.

   COMMAND : no service instance  <integer(256-16777214)>
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_NO_ISID, NULL, $3);
   SYNTAX  : no service instance <service-instance(256-16777214)>
   PRVID   : 15
   HELP    : Deletes an ISID
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             service Configures service instance mode|
             instance Service instance related configuration|
             (256-16777214) Service-instance identifier|
             <CR> Deletes an ISID.
 
   COMMAND : map backbone instance <string(32)> 
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_MAP_BACKBONE_INSTANCE, NULL, $3);
   SYNTAX  : map backbone instance <name(32)> 
   PRVID   : 15
   HELP    : To map a context to a given provider backbone Instance. 
  CXT_HELP : map Maps a context to a given provider backbone instance|
             backbone Backbone bridge related configuration| 
             instance Backbone instance related configuration|
             <string(32)> Backbone instance name|
             <CR> To map a context to a given provider backbone Instance.
END GROUP

DEFINE GROUP : PBB_IFACE_CMDS

   COMMAND : switchport pisid <integer(256-16777214)>
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_PISID, NULL,$2);
   SYNTAX  : switchport pisid <isid(256-16777214)>
   HELP    : Configures P-ISID on the PBB Ports.
  CXT_HELP : switchport Configures switch port related information|
             pisid Port instance id related configuration|
             (256-16777214) Port instance ID value|
             <CR> Configures P-ISID on the PBB Ports.

   COMMAND : no switchport pisid 
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_NO_PISID, NULL);
   SYNTAX  : no switchport pisid 
   HELP    : Resets P-ISID on the PBB Ports.
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             switchport Switch port related configuration|
             pisid Port instance id related configuration|
             <CR> Resets P-ISID on the PBB Ports.
END GROUP

DEFINE GROUP: PBB_MODE_CMDS

   COMMAND : set service-instance status {enable | disable} [<ifXtype> <ifnum>]
   ACTION  :{
                  UINT4 u4IfIndex = 0;

                  if($5 != NULL)
                  {
                      if (CfaCliGetIfIndex ($5, $6, &u4IfIndex) == CLI_FAILURE)
                      {
                          CliPrintf(CliHandle," \r%% Invalid Interface "
                          "Index\r\n");
                          return CLI_FAILURE;
                      }
                  }

              if ($3 != NULL)
                      cli_process_pbb_cmd (CliHandle, CLI_PBB_ISID_ACTIVE, 
                                           NULL, u4IfIndex);
              else
                      cli_process_pbb_cmd (CliHandle, CLI_PBB_ISID_NO_ACTIVE, 
                                           NULL, u4IfIndex);
            }
   SYNTAX  : set service-instance status {enable | disable} [<ifType> <ifNum>]
   PRVID   : 15
   HELP    : Enable or disable the given ISID. Service instance can be enabled
             or disabled per port(CBP) in B-Component.
  CXT_HELP : set Configures the parameters|
             service-instance Service instance mode related configuration|
             status Service instance status|
             enable Enables the particular ISID in the switch|
             disable Disables the particular ISID in the switch|
             DYNifType|             
             DYNifnum|
             <CR> Enable or disable the given ISID. Service instance can be enabled or disabled per port(CBP) in B-Component.
             

   COMMAND : set service-instance oui <oui> ([<ifXtype> <iface_list>] [<ifXtype> <iface_list>]
                                            [virtual <iface_list>] [<ifXtype> <iface_list>]) 
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_OUI, NULL,$3,$4,$5,$6,$7,$8,$9,$10,$11);
   SYNTAX  : set service-instance oui <aa:bb:cc> ([<interface-type> <0/a, 0/b, 0/c-d,...>] 
              [<interface-type> <0/a, 0/b, 0/c-d,...>] [virtual <iface_list> ] [port-channel <a,b,c-d,...>])
   PRVID   : 15
   HELP    : Configure Organization Unit Identifier (OUI).
  CXT_HELP : set Configures the parameters|
             service-instance Service instance mode related configuration|
             oui Organization Unit Identifier related configuration|
             <aa:bb:cc> Organization unit identifier value| 
             (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
             <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
             (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
             <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
             virtual|
             <iface_list> Interface list|
             port-channel|
             <iface_list> Interface list| 
             <CR>  Configure Organization Unit Identifier (OUI).

   COMMAND : no service-instance oui [<ifXtype> <iface_list>] [<ifXtype> <iface_list>]
                                            [virtual <iface_list>] [<ifXtype> <iface_list>] 
   ACTION  : if($3 == NULL && $5 == NULL && $7 == NULL && $9 == NULL)
                 cli_process_pbb_cmd (CliHandle, CLI_PBB_NO_OUI_ALL,NULL); 
             else
                 cli_process_pbb_cmd (CliHandle, CLI_PBB_NO_OUI, NULL,$3,$4,$5,$6,$7,$8,$9,$10);
   SYNTAX  : no service-instance oui
   PRVID   : 15
   HELP    : Deletes configured Organization Unit Identifier (OUI) per ISID.
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             service-instance Service instance mode related configuration|
             oui Organization Unit Identifier related configuration|
             (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
             <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
             (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
             <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
             virtual|
             <iface_list> Interface list|
             port-channel|
             <iface_list> Interface list|
             <CR>  Deletes configured Organization Unit Identifier (OUI) per ISID.

 
   COMMAND : set destination-mac-address <mac_addr> [<ifXtype> <ifnum>]
   ACTION  : {
                  UINT4 u4IfIndex = 0;
 
                  if($3 != NULL)
                  {
                      if (CfaCliGetIfIndex ($3, $4, &u4IfIndex) == CLI_FAILURE)
                      {
                          CliPrintf(CliHandle," \r%% Invalid Interface "
                          "Index\r\n");
                          return CLI_FAILURE;
                      }
                  }
                  cli_process_pbb_cmd (CliHandle, CLI_PBB_DESTINATION_MAC, 
                                       NULL, u4IfIndex, $2);
             }
   SYNTAX  : set destination-mac-address <aa:aa:aa:aa:aa:aa> [<ifType> <ifNum>]
   HELP    : Configures the destination MAC address for the isid. This can be
             configured per port in B-Component
 CXT_HELP  : set Configures the parameters|
             destination-mac-address Destination mac address related configuration|
             (aa:aa:aa:aa:aa:aa) Destination mac address|
             (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
             DYNifnum|
             <CR> Configures destination MAC address for the isid. This can be configured per port in B-Component.

   COMMAND : no destination-mac-address [<ifXtype> <ifnum>] 
   ACTION  : {
                  UINT4 u4IfIndex = 0;

                  if($2 != NULL)
                  {
                      if (CfaCliGetIfIndex ($2, $3, &u4IfIndex) == CLI_FAILURE)
                      {
                          CliPrintf(CliHandle," \r%% Invalid Interface "
                          "Index\r\n");
                          return CLI_FAILURE;
                      }
                  }
                  cli_process_pbb_cmd (CliHandle, CLI_PBB_NO_DESTINATION_MAC, 
                                       NULL, u4IfIndex);
             }
   SYNTAX  : no destination-mac-address [<ifType> <ifNum>]
   HELP    : Removes the destination MAC address for the isid. This can be
             configured per port in B-Component
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             destination-mac-address Destination mac address related configuration|
             (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
             DYNifnum|
             <CR> Removes destination MAC address for the isid. This can be configured per port in B-Component.

   COMMAND : ports { icomp | bcomp }[virtual <iface_list>]
   [<ifXtype> <iface_list>] [<ifXtype><iface_list>] [<ifXtype><iface_list> ] 
   ACTION  : 
{
        if($1 != NULL)
            cli_process_pbb_cmd (CliHandle, CLI_PBB_ISID_PORTS, NULL,
                    $3, $4, $5,$6,$7,$8,$9,$10,PBB_ICOMPONENT);
        else
            cli_process_pbb_cmd (CliHandle, CLI_PBB_ISID_PORTS, NULL,
                    $3, $4, $5,$6,$7,$8,$9,$10,PBB_BCOMPONENT);
}
   SYNTAX  :  ports   { icomp | bcomp } [virtual <a-b,c,...>]
 [<interface-type> <0/a-b,0/c,...> ] [<interface-type> <0/a-b,0/c,...> ] [port-channel <a-b,c,...> ]  
   PRVID   : 15
   HELP    : Configures port list (CBPs and PIPs) for an ISID
  CXT_HELP : ports Configures port related information|
             icomp Bridge of the type I-Component|
             bcomp Bridge of the type B-Component|
             virtual|
             interface list|
             (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
             <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
             (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
             <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
              port-channel|
              <iface_list> Interface list (port channel ID, port channel ID-port channel ID,..)|
              <CR> Configures port list (CBPs and PIPs) for an ISID.

 COMMAND : translate-isid <integer (256-16777214)> ([<ifXtype> <iface_list>] [<ifXtype> <iface_list>] [<ifXtype> <iface_list>] [<ifXtype> <iface_list>])
ACTION  :   cli_process_pbb_cmd (CliHandle, CLI_PBB_TRANSLATE_ISID, NULL, $1, $2, $3, $4, $5,$6,$7,$8,$9);
 SYNTAX  : translate-isid <isid (256-16777214)> ([<interface-type> <0/a, 0/b, 0/c-d,...>] [<interface-type> <0/a, 0/b, 0/c-d,...>] [port-channel <a,b,c-d,...>] [virtual <iface_list> ])
 PRVID   : 15
 HELP    : This command gives the Local ISID (ISID used outside the bridge) to which the ISID of the mode is to be translated and also gives the 
           list of the ports for which this translation is to be used. 
CXT_HELP : translate-isid Local ISID used outside the bridge for translation|
           (256-16777214) Translate ID list|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
           port-channel|
           <iface_list> Interface list (port channel ID, port channel ID-port channel ID,..)|
           virtual|
           interface list|
           <CR> This command gives the Local ISID (ISID used outside the bridge) to which the ISID of the mode is to be translated and also gives the list of the ports for which this translation is to be used.

 COMMAND : no translate-isid ([<ifXtype> <iface_list>] [<ifXtype> <iface_list>] [<ifXtype> <iface_list>] [<ifXtype> <iface_list>])
 ACTION  :  cli_process_pbb_cmd (CliHandle, CLI_PBB_NO_TRANSLATE_ISID, NULL, $2, $3, $4, $5, $6, $7, $8, $9);
 SYNTAX  : no translate-isid ([<interface-type> <0/a, 0/b, 0/c-d,...>] [<interface-type> <0/a, 0/b, 0/c-d,...>] [port-channel <a,b,c-d,...>] [virtual <iface_list> ])
 PRVID   : 15
 HELP    : This command removes the mapping of the Backbone/Relay ISID and the Local/Translate ISID for the given port.
CXT_HELP :  no Disables the configuration / deletes the entry / resets to default value|
            translate-isid Local ISID used outside the bridge for translation|
            (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
            <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
            <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
            port-channel|
            <iface_list> Interface list (port channel ID, port channel ID-port channel ID,..)|
            virtual|
            interface list|
            <CR>  This command removes the mapping of the Backbone/Relay ISID and the Local/Translate ISID for the given port.
            
 COMMAND : member-ports vlan <integer (1-4094)> [<ifXtype><iface_list>][<ifXtype><iface_list>][<ifXtype> <string> ][<ifXtype><iface_list>]
 ACTION  :  if($4 == NULL && $6 == NULL && $8 == NULL && $10 == NULL)
                CliPrintf (CliHandle, "%% Portlist is empty \r\n");
            else
                cli_process_pbb_cmd (CliHandle, CLI_PBB_ISID_BVLAN_PORT, NULL, $2, $3, $4, $5, $6,$7,$8,$9,$10);
 SYNTAX  : member-ports vlan <bvlan-id (1-4094)> [<interface-type> <0/a, 0/b, 0/c-d,...>][<interface-type> <0/a, 0/b, 0/c-d,...>][port-channel <a,b,c-d,...>][virtual <a,b,c-d,...>]
 PRVID   : 15
 HELP    : Configures CBP member ports for a BVLAN and also maps a BLAN to an isid.
 CXT_HELP: member-ports Member port related configuration|
           vlan Vlan related configuration|
           (1-4094) Vlan ID|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
            port-channel|
            <string> Interface list (port channel ID, port channel ID-port channel ID,..)|
            virtual|
            interface list|
            <CR> Configures CBP member ports for a BVLAN and also maps a BLAN to an isid.


 COMMAND : no member-ports vlan [<ifXtype> <iface_list>][<ifXtype> <iface_list>][<ifXtype> <string>][<ifXtype>]
 ACTION  :if($4 == NULL && $6 == NULL && $8 == NULL && $10 == NULL)
                CliPrintf (CliHandle, "%% Portlist is empty \r\n");
            else
             cli_process_pbb_cmd (CliHandle, CLI_PBB_ISID_NO_BVLAN_PORT, NULL,  $3, $4, $5, $6,$7,$8,$9,$10);
 SYNTAX  : no member-ports vlan [<interface-type> <0/a, 0/b, 0/c-d,...>][<interface-type> <0/a, 0/b, 0/c-d,...>][port-channel <a,b,c-d,...>] [virtual <a,b,c-d,...>] 
 PRVID   : 15
 HELP    : Removes bvlan mapping for an ISID.
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
           member-ports Member port related configuration|
           vlan Vlan related configuration|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
           port-channel|
           <string> Interface list (port channel ID, port channel ID-port channel ID,..)|
           virtual|
           interface type|
           <CR> Removes bvlan mapping for an ISID.



 COMMAND : set service-type {icomp | {bcomp ([<ifXtype><iface_list>][<ifXtype><iface_list>][<ifXtype> <string> ][<ifXtype><iface_list>])}} {ingress | egress | ingress-egress}
 ACTION  : 
{
    if($2 != NULL)
    {
        if ($12 != NULL)
            cli_process_pbb_cmd (CliHandle, CLI_PBB_SERVICE_TYPE, NULL,PBB_ICOMPONENT, PBB_VIP_TYPE_INGRESS,$4,$5,$6,$7,$8,$9,$10,$11);
        else if ($13 != NULL)
            cli_process_pbb_cmd (CliHandle, CLI_PBB_SERVICE_TYPE, NULL,PBB_ICOMPONENT, PBB_VIP_TYPE_EGRESS,$4,$5,$6,$7,$8,$9,$10,$11);
        else
            cli_process_pbb_cmd (CliHandle, CLI_PBB_SERVICE_TYPE, NULL,PBB_ICOMPONENT, PBB_VIP_TYPE_BOTH,$4,$5,$6,$7,$8,$9,$10,$11);
    }
    else
    {
        if ($12 != NULL)
            cli_process_pbb_cmd (CliHandle, CLI_PBB_SERVICE_TYPE, NULL,PBB_BCOMPONENT, PBB_INGRESS, $4,$5,$6,$7,$8,$9,$10,$11);
        if ($13 != NULL)
            cli_process_pbb_cmd (CliHandle, CLI_PBB_SERVICE_TYPE, NULL,PBB_BCOMPONENT, PBB_EGRESS, $4,$5,$6,$7,$8,$9,$10,$11);
        if ($14 != NULL)
            cli_process_pbb_cmd (CliHandle, CLI_PBB_SERVICE_TYPE, NULL,PBB_BCOMPONENT, PBB_EGRESS_INGRESS, $4,$5,$6,$7,$8,$9,$10,$11);
    }
}
SYNTAX  : set service-type {icomp | {bcomp ([<interface-type> <0/a, 0/b, 0/c-d,...>][<interface-type> <0/a, 0/b, 0/c-d,...>][port-channel <a,b,c-d,...>][virtual <a,b,c-d,...>])}} {ingress | egress | ingress-egress}  
 HELP    : Removes the destination MAC address for the isid
CXT_HELP : set Configures the parameters|
           service-type Service type related configuration|
           icomp Bridge of the type I-Component|
           bcomp Bridge of the type B-Component|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           <iface_list> Interface list (slot number/port ID, slot number/port ID-port ID,..../ Interface ID,Interface ID port-Interface ID,.. )|
           port-channel|
           <string> Interface list |
           virtual|
           <iface_list> Interface list|
           ingress Ingress service-type configuration|
           egress  Egress service-type configuration|
           ingress-egress Ingress/Egress service-type related configuration|
           <CR>  Removes the destination MAC address for the isid.


 COMMAND : exit
 ACTION  :
             {
                 CLI_SET_IFINDEX (-1);
                 CliChangePath ("..");
             }
 SYNTAX  : exit
 PRVID   : 15
 HELP    : exit to configure mode.
CXT_HELP : ext Exit to global configuration mode|
           <CR> Exits to configuration mode.
   

END GROUP
/*****************************************************************************/
/*                         PPB  PEXCFG COMMANDS                              */
/*****************************************************************************/

DEFINE GROUP: PBB_PEXCFG_CMDS

COMMAND : debug pbb [{all | [init-shut] [mgmt] [data-path] [ctrl] [pkt-dump] [resource] [all-fail] [buf] [critical] [redundancy] [reserved]}] 
    ACTION  :
            { 
                UINT1 au1TraceInput[PBB_TRC_MAX_SIZE];

                MEMSET (au1TraceInput, 0, sizeof (au1TraceInput));
                
                STRNCPY (au1TraceInput, "enable", STRLEN ("enable"));

                if($2 != NULL)
                {
                    STRNCAT (au1TraceInput, " all", STRLEN (" all"));
                }
                else
                {
                    if($3 != NULL)
                    {
                        STRNCAT (au1TraceInput, " init-shut", STRLEN (" init-shut"));
                    }
                    if($4 != NULL)
                    {
                        STRNCAT (au1TraceInput, " mgmt", STRLEN (" mgmt"));
                    }
                    if($5 != NULL)
                    {
                        STRNCAT (au1TraceInput, " data-path", STRLEN (" data-path"));
                    }
                    if($6 != NULL)
                    {
                        STRNCAT (au1TraceInput, " ctrl", STRLEN (" ctrl"));
                    }
                    if($7 != NULL)
                    {
                        STRNCAT (au1TraceInput, " pkt-dump", STRLEN (" pkt-dump"));
                    }
                    if($8 != NULL)
                    {
                        STRNCAT (au1TraceInput, " resource", STRLEN (" resource"));
                    }
                    if($9 != NULL)
                    {
                        STRNCAT (au1TraceInput, " all-fail", STRLEN (" all-fail"));
                    }
                    if($10 != NULL)
                    {
                        STRNCAT (au1TraceInput, " buf", STRLEN (" buf"));
                    }
                    if($11 != NULL)
                    {
                        STRNCAT (au1TraceInput, " critical", STRLEN (" critical"));
                    }
                    if($12 != NULL)
                    {
                        STRNCAT (au1TraceInput, " redundancy", STRLEN (" redundancy"));
                    }

                }
                if (!STRNCMP (au1TraceInput, "enable", STRLEN (au1TraceInput)))
                {
                    cli_process_pbb_cmd (CliHandle, PBB_CLI_DEBUG_SHOW);
                }
                else
                {
                    cli_process_pbb_cmd (CliHandle, PBB_CLI_DEBUG,
                                          NULL, au1TraceInput);
                }                    
            }
   SYNTAX : debug pbb [{all | [init-shut] [mgmt] [data-path] [ctrl] [pkt-dump] [resource] [all-fail] [buf] [critical] [redundancy] [reserved]}]
   PRVID   : 15
   HELP    : Specify debug level for PBB module.When no arguments are given, 
             displays current debug level.
  CXT_HELP : debug Configures debug traces|
             pbb Provider backbone bridge related configuration|
             all All traces|
             init-shut Init and Shutdown traces|
             mgmt Management traces|
             data-path Data Path traces|
             ctrl Control plane traces|
             pkt-dump Packet dump traces|
             resource Traces related to all resources such as memory, data structure and the like|
             all-fail All Failure traces|
             buf Buffer allocation/release traces|
             critical Critical traces|
             redundancy High availability traces|
             reserved Reserved details|
             <CR> Specify debug level for PBB module.When no arguments are given,displays current debug level.

   COMMAND : no debug pbb [{all | [init-shut] [mgmt] [data-path] [ctrl] [pkt-dump] [resource] [all-fail] [buf] [critical] [redundancy] [reserved]}]
   ACTION  :
    { 
                UINT1 au1TraceInput[PBB_TRC_MAX_SIZE];

                MEMSET (au1TraceInput, 0, sizeof (au1TraceInput));
                
                STRNCPY (au1TraceInput, "disable", STRLEN ("disable"));

                if($3 != NULL)
                {
                    STRNCAT (au1TraceInput, " all", STRLEN (" all"));
                }
                else
                {
                    if($4 != NULL)
                    {
                        STRNCAT (au1TraceInput, " init-shut", STRLEN (" init-shut"));
                    }
                    if($5 != NULL)
                    {
                        STRNCAT (au1TraceInput, " mgmt", STRLEN (" mgmt"));
                    }
                    if($6 != NULL)
                    {
                        STRNCAT (au1TraceInput, " data-path", STRLEN (" data-path"));
                    }
                    if($7 != NULL)
                    {
                        STRNCAT (au1TraceInput, " ctrl", STRLEN (" ctrl"));
                    }
                    if($8 != NULL)
                    {
                        STRNCAT (au1TraceInput, " pkt-dump", STRLEN (" pkt-dump"));
                    }
                    if($9 != NULL)
                    {
                        STRNCAT (au1TraceInput, " resource", STRLEN (" resource"));
                    }
                    if($10 != NULL)
                    {
                        STRNCAT (au1TraceInput, " all-fail", STRLEN (" all-fail"));
                    }
                    if($11 != NULL)
                    {
                        STRNCAT (au1TraceInput, " buf", STRLEN (" buf"));
                    }
                    if($12 != NULL)
                    {
                        STRNCAT (au1TraceInput, " critical", STRLEN (" critical"));
                    }
                    if($13 != NULL)
                    {
                        STRNCAT (au1TraceInput, " redundancy", STRLEN (" redundancy"));
                    }

                }
                if (!STRNCMP (au1TraceInput, "disable", STRLEN (au1TraceInput)))
                {
                    cli_process_pbb_cmd (CliHandle, PBB_CLI_DEBUG_SHOW);
                }
                else
                {
                    cli_process_pbb_cmd (CliHandle, PBB_CLI_DEBUG,
                                          NULL, au1TraceInput);
                }                    
            }
   SYNTAX  :  no debug pbb [{all | [init-shut] [mgmt] [data-path] [ctrl] [pkt-dump] [resource] [all-fail] [buf] [critical] [redundancy] [reserved]}] 
   PRVID   :  15
   HELP    :  Disable debug option for PBB module 
  CXT_HELP :  no Disables the configuration / deletes the entry / resets to default value|
              debug Debug traces|
              pbb Provider backbone bridge related configuration|
              all All traces|
              init-shut Init and Shutdown traces|
              mgmt Management traces|
              data-path Data Path traces|
              ctrl Control plane traces|
              pkt-dump Packet dump traces|
              resource Traces related to all resources such as memory, data structure and the like|
              all-fail All Failure traces|
              buf Buffer allocation/release traces|
              critical Critical traces|
              redundancy High availability traces|
              reserved Reserved details|
              <CR> Disable debug option for PBB module.
             
END GROUP

DEFINE GROUP: PBB_SHOW_CMDS

 COMMAND : show provider-backbone-bridge oui
 ACTION  : cli_process_pbb_show_cmd (CliHandle, CLI_PBB_SHOW_OUI, NULL);
 SYNTAX  : show provider-backbone-bridge oui
 PRVID   : 1
 HELP    : Displays the PBB OUI configured in the bridge
CXT_HELP : show Displays the configuration / statistics / general information|
           provider-backbone-bridge Provider backbone bridge related configuration|
           oui Organization unit identifier related information|
           <CR> Displays the PBB OUI configured in the bridge.


 COMMAND : show pisid [{port <ifXtype> <ifnum> | switch <string(32)>}]
 ACTION  :  {
              UINT4 u4PortId = 0;
              
              if ($3 != NULL)
              {
                if (CfaCliGetIfIndex ($3, $4, &u4PortId) == CLI_FAILURE)
                {
                  CliPrintf (CliHandle, "%% Invalid Interface \r\n");
                  return (CLI_FAILURE);
                }
              }
             cli_process_pbb_show_cmd (CliHandle, CLI_PBB_SHOW_PISID, u4PortId,$6);
            }
 SYNTAX  : show pisid [{port <ifXtype> <ifnum> | switch <string(32)>}]
 PRVID   : 1
 HELP    : Displays the P-ISID configured on a port.
CXT_HELP : show Displays the configuration / statistics / general information|
           pisid Port instance id related configuration|
           port Port related information|
           (gigabitethernet/fastethernet/extreme-ethernet) Interface type|
           DYNifnum|
           switch Virtual context or component|
           <string (32)> Context name|
           <CR> Displays the P-ISID configured on a port.


 COMMAND : show service-instance config [{service-instance <integer(256-16777214)>| switch <string(32)>}]
 ACTION  : cli_process_pbb_show_cmd (CliHandle, CLI_PBB_SHOW_ISID, NULL,$6,$4);
 SYNTAX  : show service-instance config [{service-instance <isid(256-16777214)>| switch <string(32)>}]
 PRVID   : 1
 HELP    : Displays all information related to a given service instance.
CXT_HELP : show Displays the configuration / statistics / general information|
           service-instance Service instance mode configuration related information|
           config Service instance configuration related information|
           service-instance Service instance mode configuration related information|
           (256-16777214) Service instance value|
           switch Virtual context or component|
           <string (32)> Context name|
           <CR>  Displays all information related to a given service instance.


 COMMAND : show backbone instance [<string(32)>]
 ACTION  : cli_process_pbb_show_cmd (CliHandle, CLI_PBB_SHOW_BACKBONE_INSTANCE, NULL, NULL, $3);
 SYNTAX  : show backbone instance [<name(32)>]
 PRVID   : 1
 HELP    : Displays the information of provider backbone instance.
CXT_HELP : show Displays the configuration / statistics / general information|
           backbone Configures backbone bridge related information|
           instance Backbone instance related configuration|
           <string(32)> Backbone instance name|
           <CR> Displays the information of provider backbone instance.

 COMMAND : show backbone instance map [switch <string(32)>]
 ACTION  : cli_process_pbb_show_cmd (CliHandle, CLI_PBB_SHOW_BACKBONE_MAP, NULL, $5);
 SYNTAX  : show backbone instance map [switch <string(32)>]
 PRVID   : 1
 HELP    : Displays the mapping of provider backbone instance and contexts.
CXT_HELP : show Displays the configuration / statistics / general information|
           backbone Configures backbone bridge related information|
           instance Backbone instance related configuration|
           map Maps a context to a given provider backbone instance|
           switch Virtual context or component|
           <string (32)> Context name|
           <CR> Displays the mapping of provider backbone instance and contexts.

 END GROUP

DEFINE GROUP: PBB_CONFIG_CMDS

   COMMAND : shutdown provider-backbone-bridge
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_SHUT, NULL);
   SYNTAX  : shutdown provider-backbone-bridge
   PRVID   : 15
   HELP    : Shuts down provider-backbone-bridge in the switch
  CXT_HELP : shutdown Shuts down the feature|
             provider-backbone-bridge Provider backbone bridge related configuration|
             <CR> Shuts down provider-backbone-bridge in the switch.
   
   COMMAND : no shutdown provider-backbone-bridge
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_NO_SHUT, NULL);
   SYNTAX  : no shutdown provider-backbone-bridge
   PRVID   : 15
   HELP    : Starts and enables provider-backbone-bridge in the switch
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             shutdown Shuts down the feature|
             provider-backbone-bridge Provider backbone bridge related configuration|
             <CR> Starts and enables provider-backbone-bridge in the switch.
             
   COMMAND : set provider-backbone-bridge oui <oui>
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_GLB_OUI, NULL, $3);
   SYNTAX  : set provider-backbone-bridge oui <aa:aa:aa>
   PRVID   : 15
   HELP    : Configures Organization Unit Identifier (OUI).
  CXT_HELP : set Configures the parameters|
             provider-backbone-bridge Provider backbone bridge related configuration|
             oui Organization Unit Identifier related information|
             (aa:aa:aa) Organization Unit Identifier value|
             <CR> Configure Organization Unit Identifier (OUI).

   COMMAND : no provider-backbone-bridge oui
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_GLB_NO_OUI);
   SYNTAX  : no provider-backbone-bridge oui
   PRVID   : 15
   HELP    : Sets Organization Unit Identifier (OUI) to default value (Switch Base
             Mac address).
   CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
             provider-backbone-bridge Provider backbone bridge related configuration|
             oui Organization Unit Identifier related configuration|
             <CR> Sets Organization Unit Identifier (OUI) to default value (Switch Base Mac address).

   COMMAND : backbone instance <string(32)> [mac-address <mac_addr>] 
   ACTION  : if($3 != NULL)
             {
                 cli_process_pbb_cmd (CliHandle, CLI_PBB_BACKBONE_INSTANCE, NULL, $2, $4);
             }
             else
             {
                 cli_process_pbb_cmd (CliHandle, CLI_PBB_BACKBONE_INSTANCE, NULL, $2,NULL);
             }
   SYNTAX  : backbone instance <name(32)> [mac-address <aa:bb:cc:dd:ee:ff>] 
   PRVID   : 15
   HELP    : To create a provider backbone Instance
  CXT_HELP : backbone Configures backbone bridge related information|
             instance Backbone instance related configuration|
             <string(32)> Backbone instance name|
             mac-address MAC address related configuration|
             (<aa:bb:cc:dd:ee:ff) MAC address|
             <CR>  To create a provider backbone Instance.

   COMMAND : no backbone instance <string(32)> 
   ACTION  : cli_process_pbb_cmd (CliHandle, CLI_PBB_NO_BACKBONE_INSTANCE, NULL, $3);
   SYNTAX  : no backbone instance <name(32)> 
   PRVID   : 15
   HELP    : To delete a provider backbone Instance.
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             backbone Backbone bridge related configuration|
             instance Backbone instance related configuration|
             <string (32)> Backbone instance name|
             <CR> To delete a provider backbone Instance.
END GROUP

