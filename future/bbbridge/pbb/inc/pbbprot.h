/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbprot.h,v 1.31 2012/04/16 14:11:52 siva Exp $
 *
 * Description:This file Contains PBB module prototype definitions.
 *
 *******************************************************************/

#ifndef _PBBPROT_H
#define _PBBPROT_H

/*********Protypes for PBB Module *******************/
#include "pbbtdfs.h"
/************************* PBBSYS.C *************************************/



INT4
PbbLock PROTO((VOID));

INT4
PbbUnLock PROTO((VOID));

INT4 
PbbTaskInit PROTO ((VOID));

VOID 
PbbMainDeInit PROTO ((VOID));

INT1
PbbHandleStartModule PROTO ((VOID));

INT1 
PbbInitGlobalInfo PROTO ((VOID));

INT1
PbbDeInitGlobalInfo PROTO ((VOID));

INT1 
PbbInit PROTO ((VOID));

INT1 
PbbInitMemPools PROTO ((VOID));

VOID 
PbbDeInit PROTO ((VOID));

VOID 
PbbProcessCfgEvent PROTO ((tPbbQMsg *pPbbQMsg));

INT1
PbbStartModule PROTO  ((VOID));


INT4
PbbPostPortCreateMessage PROTO  ((UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u4LocalPort,
                           UINT2 u2MsgType));


INT4
PbbPostCfgMessage PROTO  ((UINT2 u2MsgType, INT4 i4IfIndex));



INT4
PbbSelectContext PROTO  ((UINT4 u4ContextId));

INT1
PbbReleaseContext PROTO((VOID));

INT4
PbbHandleCreateContext PROTO ((UINT4 u4ContextId));

INT4
PbbHandleDeleteContext PROTO ((UINT4 u4ContextId));

INT4
PbbHandleUpdateContextName PROTO ((UINT4 u4ContextId));

INT1
PbbHandleCreatePort PROTO ((UINT4 u4ContextId, 
                            INT4 i4IfIndex, 
                            UINT2 u4LocalPort));

INT1
PbbValidateLocalVip PROTO ((UINT2 u2LocalPort));

INT1
PbbHandleDeletePort PROTO((UINT4 u4ContextId,INT4 i4IfIndex, UINT2 u2LocalPort));

VOID
PbbDeInitMemPools PROTO ((VOID));

INT1
PbbTableInit PROTO((VOID));

VOID
PbbTableDeInit PROTO ((VOID));

INT1
PbbHandleUnMapPortInd PROTO ((UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort));

INT1
PbbHandleMapPortInd PROTO ((UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort));

INT1 PbbSetPortStatus PROTO ((UINT4 u4Context, INT4 i4IfIndex,
                              UINT1 u1PortStatus));

INT4
PbbPostMapPortIndication PROTO((UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort,
                           UINT2 u2MsgType));

INT4
PbbPostUnMapPortIndication PROTO((UINT2 u2MsgType, INT4 i4IfIndex));

INT4
PbbPostVlanAuditStatusIndic PROTO((tPbbVlanAuditStatus *pPbbVlanStatus));

VOID PbbDeInitializeContext PROTO ((VOID));

VOID PbbDeInitializeInstance PROTO((VOID));
#ifdef SNMP_2_WANTED
VOID 
PbbRegisterMIB (VOID);

VOID
PbbUnRegisterMIB PROTO((VOID));
#endif

/************************* PBBUTIL.C *************************************/
INT1
PbbCompareContext PROTO((UINT4 *pu4Context1, UINT4 *pu4Context2));
INT1 PbbUpdateInstanceId PROTO((UINT4 u4ContextId, UINT4 u4InstanceId));
VOID PbbIncrInstanceNumOfIcomp PROTO((VOID));
VOID PbbIncrInstanceNumOfBcomp PROTO((VOID));
VOID PbbDecrInstanceNumOfIcomp PROTO((VOID));
VOID PbbDecrInstanceNumOfBcomp PROTO((VOID));

VOID PbbGetDefaultInstanceMacAddress PROTO(( UINT4 u4InstanceId,
            tMacAddr DefInstanceMacAddr));
INT1 PbbIsInstanceMappedToContext PROTO((UINT4 u4InstanceId));

INT1
PbbValidateInstanceId PROTO((UINT4 u4InstanceId));

VOID
PbbUtilSplitStrToTokens PROTO((UINT1 *pu1InputStr, INT4 i4Strlen,
                          UINT1 u1Delimiter, UINT2 u2MaxToken,
                          UINT1 *apu1Token[], UINT1 *pu1TokenCount));

UINT4
PbbUtilGetTraceInputValue PROTO((UINT1 *pu1TraceInput, UINT4 u4TraceOption));
UINT4
PbbUtilGetTraceOptionValue PROTO((UINT1 *pu1TraceInput, INT4 i4Tracelen));
VOID
PbbUtilSetTraceOption PROTO((UINT1 *pu1Token, UINT4 *pu4TraceOption ));


INT1
PbbValidateContextId PROTO((UINT4 u4contextid));

INT1
PbbValidatePort PROTO((INT4 i4ifIndex));

VOID
PbbGetPortListForCurrContext PROTO((tMbsmPortInfo *pPortInfo,
                               tLocalPortList LocalPortList));

INT4
PbbSetVipOperStatus PROTO((INT4 i4ContextId, INT4 i4VipIndex, UINT2 u2Vip,
                           UINT1 u1PipPortStatus));

INT4
PbbSetPipVipListOperStatus (UINT4 u4ContextId, UINT2 u2Pip,
                            UINT1 u1PortStatus);

INT1 PbbHwDeleteLocalVipToPip PROTO((UINT4 u4ContextId, INT4 i4IfIndex, UINT4 u4Isid, UINT2 u2Pip));

INT1
PbbDeleteLocalIsidNodeDll PROTO((UINT4 u4Isid, UINT2 u2LocalPort));

INT1
PbbDeleteCBPNodeLocalDLL  PROTO((UINT4 u4Isid, UINT2 u2LocalPort));
INT1
PbbDeleteIsidLocalPortRBNodes PROTO((UINT2 u2LocalPort,UINT4 u4Isid));

 INT1
PbbValidateLocalPort PROTO((UINT2 u2LocalPort));

VOID
PbbGetAggPortListForCurrContext PROTO((
                                    tLocalPortList LocalPortList));
VOID 
PbbHandleGlbShutdown PROTO ((VOID));

INT1
PbbHandleShutdownStatus (INT4 i4ShutdownStatus);

/************************* PBBDATA.C *************************************/
INT1 PbbNumOfBebPortsForInstance PROTO((UINT4 u4InstanceId,
        UINT4 *pu4NumOfBebPorts));
VOID
PbbAddContextEntryDll PROTO((tTMO_DLL_NODE * pNode, UINT4 u4InstanceId));

INT1
PbbCreateContextEntryDll PROTO((UINT4 u4Instance, UINT4 u4ContextId));

INT1
PbbDeleteContextNodeFromDLL PROTO((UINT4 u4InstanceId, UINT4 u4ContextId));



INT1
PbbCreateInstanceNode PROTO((UINT4 u4InstanceId));

VOID
PbbDeleteInstanceNode PROTO((UINT4 u4InstanceId));

 INT1
PbbGetVipValue PROTO((UINT4 u4ContextId, UINT4 u4Isid, UINT2 *pu2Vip));

INT1 PbbDeleteIsidNodeFromVip PROTO((UINT4 u4Isid));

INT4
PbbGetPipIsidDataWithVip PROTO((UINT4 u4ContextId,
                          UINT2 u2Vip,
                          UINT4 *pu4PipIndex,
                          UINT4 *pu4Isid,
                          tMacAddr * pBDaAddr));
INT4
PbbGetMemberPortListForIsid PROTO((UINT4 u4ContextId,
                             UINT4 u4Isid, 
                             tSNMP_OCTET_STRING_TYPE * pPortList));
INT1
PbbGetVipIsidDataWithPortList PROTO((UINT4 u4ContextId, 
                               UINT2 u2LocalPort,
                               tLocalPortList InPortList, 
                               UINT2 *pu2Vip,
                               UINT2 *pu2Pip, 
                               UINT4 *pu4Isid, 
                               tMacAddr * pBDaAddr));

INT4
PbbGetCnpMemberPortListForIsid PROTO((UINT4 u4ContextId,
                                      UINT4 u4Isid, 
                                      tSNMP_OCTET_STRING_TYPE * pPortList));

INT1
PbbIsIsidMemberPortPresent PROTO((UINT4 u4ContextId, 
                                  UINT4 u4Isid, 
                                  UINT2 u2LocalPort));
INT1
PbbGetLocalIsidDataFromRelayIsid PROTO((UINT4 u4ContextId, 
                                        UINT2 u2LocalPort,
                                        UINT4 u4RelayIsid, 
                                        UINT4 *pu4LocalIsid));
INT1
PbbGetIsidDataForVip PROTO((UINT4 u4Vip, UINT4 *pu4Isid));

INT4
PbbGetIsidInfoDataFromFrame PROTO((UINT4 u4ContextId, 
                             UINT2 u2LocalPort,
                             tCRU_BUF_CHAIN_DESC * pBuf,
                             tPbbTag * pPbbTag,
                             UINT4 u4IsidTagOffSet));

INT1
PbbGetPipVipDataWithIsid PROTO((UINT4 u4ContextId, 
                                UINT2 u2LocalPort,
                                UINT4 u4Isid, 
                                UINT2 *pu2Vip, 
                                UINT2 *pu2Pip));

INT1
PbbDeleteCnpNodeFromCnpDLL PROTO((UINT4 u4Isid, UINT4 u4IfIndex));

    INT1
PbbSetVipPortStatus PROTO((UINT2 u4LocalPort, UINT1 u1PortStatus));


INT1
PbbDeletePortRBNode PROTO((UINT2 u4LocalPort));

INT1
PbbDeletePortInfo PROTO((INT4 i4IfIndex,UINT2 u2LocalPort,UINT4 u4ContextId));


INT4 PbbCompareIsidRBNodes PROTO(( 
                tPbbIsidRBtreeNode   *RBNode1,
                tPbbIsidRBtreeNode   *RBNode2));

INT4 PbbCompareIsidPortRBNodes PROTO(( 
                tPbbBackBoneSerInstEntry   *RBNode1,
                tPbbBackBoneSerInstEntry   *RBNode2));

INT4 PbbComparePortRBNodes PROTO (( 
                tPbbPortRBtreeNode   *RBNode1,
                tPbbPortRBtreeNode   *RBNode2));

INT4 PbbCompareVipRBNodes PROTO (( 
                tPbbICompVipRBtreeNode   *RBNode1,
                tPbbICompVipRBtreeNode   *RBNode2));

INT1 PbbSetVipLocalPipRowStatus PROTO ((UINT4 u4ContextId ,INT4 i4IfIndex, 
                           UINT2 u2Vip, UINT4 i4VipPipRowStatus));


INT1
PbbGetVipIsidonLocal PROTO((UINT2 u2LocalPort, UINT4 *pu4Isid));

INT1
PbbDeleteIsidPortRBTree PROTO ((VOID));

INT1
PbbDeletePortRBTree PROTO((VOID));

INT1
PbbDeleteVipRBTree PROTO((VOID));

INT1
PbbDeleteIsidRBTree PROTO((VOID));

INT1 PbbNpNoShutdown PROTO((VOID));

INT1 PbbValidateLocalIsid PROTO((UINT4 u4LocalIsid, UINT4 u4Isid));


INT1 PbbNpShutdown PROTO ((VOID));

INT1
PbbGetVIPIsidPIsid  PROTO ((INT4 i4ifIndex, UINT4 *u4Pisid));

 INT1
PbbSetVipIsidPIsidRowStatus PROTO ((INT4 i4ifIndex, UINT4 u4RowStatus));

INT1
PbbGetNextVipIfIndex  PROTO ((INT4 i4Vip, INT4 *pNextVip));



 INT1
PbbSetPortPisid PROTO ((INT4 i4ifIndex, UINT4 u4Pisid));

INT1
PbbGetPortPisidRowStatus PROTO ((INT4 i4ifIndex, UINT4 *pu4RowStatus));

INT1
PbbGetPortPisid  PROTO ((INT4 i4ifIndex, UINT4 *pu4Isid));

INT1 PbbDeleteICompdataFromHw  PROTO ((UINT4 u4ContextId));

INT1 PbbDeleteBCompdataFromHw PROTO ((UINT4 u4ContextId));


 INT1
PbbGetNextPort PROTO ((UINT2 u4CurrPort, INT4 *pi4NextifIndex));

INT1
PbbValidateIsidPort  PROTO ((UINT4 u4Isiid, INT4 i4ifIndex));

INT1 PbbDeleteICompPisiddataFromHw  PROTO((UINT4 u4ContextId));

 INT1
PbbValidateCbpPort PROTO((INT4 i4ifIndex));
INT1    
PbbDecrNumPortPerIsidPerContext PROTO((UINT4 u4Isid));

VOID
PbbDelCnpEntryDll PROTO((tTMO_DLL_NODE * pNode, UINT4 u4Isid));
INT1
PbbDeleteCnpNodeDll PROTO((INT4 i4ifIndex));

INT1 PbbMapVcmPort  PROTO((INT4 i4FreeIfIndex , UINT4 u4ContextId));

INT1
PbbValidateIsid PROTO ((UINT4 u4Isid));

 INT1
PbbGetFirstIsid PROTO ((UINT4 *u4Isid));

INT1
PbbGetNextActiveContext PROTO ((UINT4 u4CurrContextId, UINT4 *pu4NextContextId));
 INT1
PbbGetNextActiveIContext  PROTO ((UINT4 u4CurrContextId, UINT4 *pu4NextContextId));


INT1
PbbGetFirstIsidPort  PROTO ((UINT4 *u4Isid, UINT2 *u2Port));

INT1
PbbGetNextIsidPort PROTO ((UINT4 u4CurrIsid, 
                    UINT4 *u4NextIsid, 
                    UINT2 u2LocalPort,
                    UINT2 *pu2NextPort));


 tPbbCnpPortList *
PbbAllocMemCnpDllNode  PROTO ((VOID));

INT1
PbbGetIsidOUI  PROTO ((UINT4 u4ContextId ,
                       UINT4 u4Isid ,
                       INT4 i4IfIndex,
                       tSNMP_OCTET_STRING_TYPE *pOUI));

INT1 PbbGetIsidOUIRowStatus PROTO ((UINT4 u4ContextId ,
                            UINT4 u4Isid ,
                            INT4 i4IfIndex,
                            INT4 *pOUIRowStatus));

INT1 PbbSetIsidOUI PROTO ((UINT4 u4ContextId ,
                           UINT4 u4Isid ,
                           INT4 i4IfIndex,
                           tSNMP_OCTET_STRING_TYPE *pOUI));

INT1 PbbSetIsidOUIRowStatus PROTO ((UINT4 u4ContextId ,
                                    UINT4 u4Isid ,
                                    INT4 i4IfIndex,
                                    INT4 i4OUIRowStatus));


INT1 PbbTestIsidOUI PROTO ((UINT4 *pu4ErrorCode,
                            UINT4 u4ContextId ,
                            UINT4 u4Isid ,
                            INT4 i4IfIndex,
                            tSNMP_OCTET_STRING_TYPE *pOUI));

INT1 PbbTestIsidOUIRowStatus PROTO ((UINT4 u4ContextId ,
                                     UINT4 u4Isid ,
                                     INT4 i4IfIndex, 
                                     INT4 i4OUIRowStatus));

INT4
PbbPostSetVipPortMessage PROTO ((UINT4 u4ContextId, INT4 i4IfIndex, UINT2 u2LocalPort,
                           UINT2 u2MsgType));

INT1 PbbHandleVipPortType PROTO ((UINT4 u4ContextId , INT4 i4Ifindex, UINT2 u2LocalPort));
INT1
PbbGetFirstPort  PROTO ((INT4 *i4ifIndex));


    INT1
PbbGetNextIsid PROTO ((UINT4 u4CurrIsid,UINT4 *u4NextIsid));

 INT1
PbbSetPortCbpTableRowstatus PROTO ((INT4 i4ifIndex, UINT4 u4RowStatus));

INT1
PbbGetPortCbpTableRowstatus PROTO ((INT4 i4ifIndex, UINT4 *u4Rowstatus));

 INT1
PbbDelBCompHwServiceInst PROTO ((UINT4 u4ContextId,  INT4 i4IfIndex,
            UINT4 u4Isid));

INT1
PbbDeleteIsidNodeDll PROTO ((UINT4 u4Isid, INT4 i4ifIndex));

INT1
PbbDeleteCBPNodeDLL PROTO ((UINT4 u4Isid, INT4 i4ifIndex));

INT1
PbbDeleteIsidPortRBNodes PROTO ((INT4 i4ifIndex,UINT4 u4Isid));

 INT1
PbbSetBCompHwServiceInst PROTO ((UINT4 u4ContextId,  UINT4 u4Isid, INT4 i4ifIndex));

 INT1
PbbSetIsidPortCbpRowstatus PROTO ((INT4 i4ifIndex,UINT4 u4Isid,UINT4 u4Rowstatus));

INT1
PbbCreateIsidRBTreeNode PROTO ((UINT4 u4Isid));

INT1
PbbInsertPipVipDll PROTO((UINT2 u2Vip, INT4 u4ifIndex));

INT1
PbbInsertCBPIsidDll PROTO ((UINT4 u4Isid, INT4 i4ifIndex));

INT1
PbbInsertCBPIsidRBTreeNode PROTO ((UINT4 u4Isid, INT4 i4ifIndex));

INT1
PbbSetIsidPortLocalIsid PROTO((INT4 i4ifIndex,UINT4 u4Isid, UINT4 u4LocalIsid));

INT1
PbbSetIsidPortDefBDA PROTO ((INT4 i4ifIndex, UINT4 u4Isid, tMacAddr DefaultDstBMAC));

INT1
PbbSetIsidPortCBPServiceMapType PROTO ((INT4 i4ifIndex,
                                    UINT4 u4Isid, 
                                    tSNMP_OCTET_STRING_TYPE *pServiceMappingType));
INT1
PbbSetIsidPortBVlanId PROTO ((INT4 i4ifIndex, UINT4 u4Isid, UINT4 u4BVlanId));

INT1
PbbGetIsidPortLocalIsid PROTO ((INT4 i4ifIndex,UINT4 u4Isid, UINT4 *u4LocalIsid));

 INT1
PbbDeleteVipLocalNodeDll PROTO ((UINT2 u2Vip, UINT2 u2LocalPort));

INT1
PbbGetIsidPortServiceMapType PROTO ((INT4 i4ifIndex, UINT4 u4Isid,  UINT1 *u1ServiceMapType));


INT1
PbbGetIsidPortBVlanId PROTO ((INT4 i4ifIndex, UINT4 u4Isid, UINT4 *BVlanId));

INT1
PbbGetIsidPortDefBDA PROTO ((UINT2 u2LocalPort, UINT4 u4Isid, 
                             tMacAddr * DAMacAddr));

INT1 PbbTestVipPipRowStatus PROTO ((UINT4 *pu4ErrorCode, 
                                    UINT4 u4CompId , 
                                    UINT2 u2Vip, 
                                    UINT4 i4VipPipRowStatus));



INT1
PbbGetNextValidIsidPort PROTO ((UINT4 u4ContextId,
  UINT4 *pu4NextContextid,
  UINT4 u4CurrIsid,
  UINT4 *u4NextIsid, 
  UINT4 u4CurrPort, 
  UINT2 *pu2NextPort));

INT1
PbbGetNextCnpPort PROTO ((INT4 i4IfMainIndex, INT4 *pi4NextifIndex));

INT1 PbbIsVipPipRowStatusNotActive PROTO ((UINT4 u4CompId, UINT2 u2Vip));

INT1 PbbSetVipPipRowStatus PROTO ((UINT4 u4CompId , UINT2 u2Vip, UINT4 i4VipPipRowStatus));

INT1 PbbSetVipPipStorageType PROTO ((UINT4 u4CompId, UINT2 u2Vip, UINT1 u1PipStorageType));

INT1 PbbSetVipPipInterfaceIndex PROTO ((UINT4 u4CompId, UINT4 u4Isid, UINT2 u2Vip, INT4 i4PipIfIndex));

INT1 PbbGetVipPipRowStatus PROTO ((UINT4 u4CompId, UINT2 u2Vip, UINT4 *pu4PipRowStatus));

INT1 PbbGetVipPipStorageType PROTO ((UINT4 u4CompId, UINT2 u2Vip, UINT4 *pu4PipStorageType));

INT1 PbbGetVipPipInterfaceIndex PROTO ((UINT4 u4CompId, UINT2 u2Vip, INT4 *pi4PipIfIndex));


INT1
PbbGetNextVip PROTO ((UINT4 u4ContextId,
        UINT4 *pu4NextContextid, INT4 i4CurrVip,UINT2 *u2NextVip));
INT1
PbbGetFirstVipIfIndex PROTO ((INT4 *pi4Vip));

INT1
PbbGetOperStatus PROTO((UINT2 u2Port, UINT1 *pu1PortStatus));
INT1
PbbGetNextLocalPort PROTO((UINT2 u2Port, UINT2 *pu2NextPort));
INT1
PbbGetNextLocalVip PROTO((UINT2 u2Vip, UINT2 *pu2NextVip));

INT1 PbbTestPipRowStatus PROTO ((UINT4 *pu4ErrorCode, 
                                 UINT2 u4localPort ,
                                 INT4 i4PipRowStatus));


INT1 PbbIsPipRowStatusNotActive PROTO ((UINT2 u2localPort));

INT1 PbbSetPipRowStatus PROTO ((UINT2 u2localPort , INT4 i4PipRowStatus));


INT1 PbbGetPipRowStatus PROTO ((
              UINT2 u4localPort , INT4 *pi4PipRowStatus));

INT1
PbbGetFirstCnpPort PROTO ((INT4 *pi4ifIndex));


INT1
PbbGetFirstPbbPipIfIndex PROTO ((INT4 *pi4PipIfIndex));

INT1
PbbGetNextPbbPipIfIndex PROTO ((INT4 i4PipIfIndex, INT4 *pi4NextPipIfIndex));

INT4 PbbValidatePipPortExists PROTO ((UINT2 u2localPort));

INT1
PbbGetNextPipIfIndex PROTO ((INT4 i4PipIfIndex, INT4 *pi4NextPipIfIndex));

INT1
PbbGetFirstPipIfIndex PROTO ((INT4 *pi4PipIfIndex));

 INT1
PbbGetVipForIsid PROTO ((UINT4 u4Isid,UINT2 *u2Vip));

INT1
PbbDelICompHwServiceInst PROTO ((UINT4 u4ContextId,  INT4 i4IfMainIndex));

INT1 PbbDelHwPortPisid PROTO ((UINT4 u4ContextId, INT4 i4ifIndex, UINT4 u4Pisid));


 INT1
PbbDelVipInfo PROTO ((UINT2 u2LocalPort));

INT1
PbbSetICompHwServiceInst PROTO ((UINT4 u4ContextId, INT4 i4ifIndex, UINT4 u4Isid));

INT1
PbbSetVipRowstatus PROTO ((INT4 i4ifIndex ,UINT4 u4RowStatus));

INT1
PbbCreateVipRBTreeNode PROTO ((INT4 i4ifIndex));

INT1
PbbSetPbbVipType PROTO ((INT4 i4ifIndex, UINT1 u1VipType));

INT1
PbbSetVipDefaultDstBMAC PROTO ((INT4 i4ifIndex, tMacAddr DefaultBackboneDest));

INT1
PbbCreateIsidNodeFromVip PROTO ((INT4 i4ifIndex, UINT4 u4Isid));

 INT1
PbbSetPbbVipISid PROTO ((INT4 i4ifIndex, UINT4 u4Isid));

 INT1
PbbGetVipRowStatus  PROTO((INT4 i4ifIndex, UINT4 *pu4VipRowStatus));

 INT1
PbbGetVipDefaultDstBMAC  PROTO ((INT4 i4ifIndex, tMacAddr *VipDefaultDstBMAC));


INT1
PbbGetVipPipIfIndex PROTO ((INT4 i4ifIndex, INT4 *pi4Pip));

INT1 PbbGetPipIfIndex PROTO ((INT4 i4FsPbbPipIfIndex));

INT1
PbbValidateVip PROTO ((INT4 i4ifIndex));


INT1
PbbGetIsidPortCbpRowstatus  PROTO ((INT4 i4ifIndex,UINT4 u4Isid, UINT4 *u4Rowstatus));


INT1
PbbGetFirstVip PROTO ((UINT4 *u4Vip));

 INT1
PbbGetVipType PROTO ((INT4 i4ifIndex, UINT1 *u1VipType));


INT1    
PbbGetMaxNumPortPerIsid PROTO ((UINT4 *u4NumOfPortsPerIsidPresent, 
                                UINT4 u4Isid, UINT4 u4ContextId));


INT1
PbbGetPortData PROTO ((UINT2 u2Port,tPbbPortRBtreeNode  **pPortData));

INT1
PbbGetIsidData PROTO((UINT4 u4Isid, tPbbIsidRBtreeNode  **pIsidData));

INT1
PbbGetIsidPortData PROTO((UINT2 u2Port,UINT4 u4Isid,tPbbBackBoneSerInstEntry  **pIsidPortData));

INT1
PbbGetVipData PROTO((UINT2 u2Vip,  tPbbICompVipRBtreeNode  **pVipData));


INT1 PbbRemovePipVipMaps PROTO((tPbbPortRBtreeNode *pPbbPipCurrEntry,
                                UINT4 u4ContextId));


INT1
PbbGetFirstIsidEntry PROTO((tPbbIsidRBtreeNode *pIsidEntry));


INT1
PbbGetNextIsidEntry PROTO((tPbbIsidRBtreeNode IsidEntry, tPbbIsidRBtreeNode 
                                                          *pNextIsidEntry));


INT1
PbbGetNextVipEntry PROTO((tPbbICompVipRBtreeNode VipEntry, tPbbICompVipRBtreeNode 
                                                          *pNextVipEntry));





INT1
PbbHwSetProviderBridgePortType PROTO((UINT4 u4ContextId, INT4 i4IfIndex,
                                 UINT1 u1PortType));

INT1
PbbL2IwfSetPbbShutdownStatus  PROTO(( UINT1 u1ShutdownStatus));

INT1
PbbDelVipData PROTO((INT4 i4ifIndex));


 INT1
PbbGetAllPorts PROTO((UINT4 u4ContextId));

INT1 PbbResetVipInPip PROTO((UINT2 u2Vip,UINT2 u2localPort));

INT1 PbbSetVipInPip PROTO((UINT2 u2Vip,UINT2 u2localPort));

INT1
PbbGetFirstPortEntry PROTO((tPbbPortRBtreeNode *pPortEntry));

INT1
PbbGetNextPortEntry PROTO((tPbbPortRBtreeNode *pPortEntry,
                           tPbbPortRBtreeNode **pNextPortEntry));
INT1
PbbGetFirstPipPortEntry PROTO((tPbbPortRBtreeNode *pPipEntry));

INT1
PbbGetNextPipPortEntry PROTO((tPbbPortRBtreeNode PipEntry, tPbbPortRBtreeNode 
                                                          *pNextPipEntry));

INT1
PbbGetFirstActiveContext PROTO((UINT4 *pu4ContextId));

 INT1
PbbFreeIsidRBTreeNode PROTO((tPbbIsidRBtreeNode * ptobefreed, 
                             UINT4 u4arg));
INT1
PbbFreeIsidPortRBTreeNode PROTO((tPbbBackBoneSerInstEntry * ptobefreed, 
                                 UINT4 u4arg));
INT1
PbbFreePortRBTreeNode PROTO((tPbbPortRBtreeNode * ptobefreed, UINT4 u4arg));


INT1
PbbFreeVipRBTreeNode PROTO((tPbbICompVipRBtreeNode * ptobefreed, UINT4 u4arg));

 INT1
PbbDeleteIsidRBNodes PROTO((UINT4 u4Isid));

INT1
PbbDeleteVipRBNodes PROTO((UINT2 u2Vip));

tPbbIsidRBtreeNode *
PbbAllocIsidRBNode PROTO((VOID));

INT1 PbbCompareCBPPort PROTO(( 
                              UINT2   *pCBPNode1,
                              UINT2   *pCBPNode2));

 tPbbCbpPortList *
PbbAllocMemDllNode PROTO((VOID));
VOID
PbbAddCnpPortEntryDll PROTO((tTMO_DLL_NODE * pNode,UINT4 u4Isid));

VOID
PbbAddCBPPortEntryDll PROTO((tTMO_DLL_NODE * pNode,UINT4 u4Isid));

 tPbbICompVipRBtreeNode *
PbbAllocVipRBNode PROTO((VOID));

INT1 PbbCompareCnpPort PROTO(( 
        UINT2   *pu2CnpPort1,
        UINT2   *pu2CnpPort2));



INT1 PbbCreateCnpEntryDll PROTO((UINT4 u4Isid, INT4 i4IfMainIndex));

tPbbPortRBtreeNode *
PbbAllocPortRBNode PROTO((VOID));


INT1
PbbCreatePortRBTreeNode PROTO((INT4 i4ifIndex, UINT2 u2LocalPort));

tPbbBackBoneSerInstEntry *
PbbAllocIsidPortRBNode PROTO((VOID));

tPbbIsidPipVipNode *PbbAllocMemIsidVipDllNode PROTO((VOID));

 INT1
PbbCreateIsidPortRBTreeNode PROTO((INT4 i4ifIndex,UINT4 u4Isid));

INT1 PbbValidateViptoPip PROTO((INT4 i4VipIfIndex, UINT2 u2localPort));

INT1
PbbGetFreeVipPort PROTO((UINT4 u4ContextId, UINT4 u4Isid, UINT2 *pu2Vip));

INT1 PbbCompareIsidEntry PROTO(( 
                UINT4    *pu4Isid1,
                UINT4    *pu4Isid2));

INT1
PbbCompareVipEntry PROTO((UINT2 *pu2Vip1, UINT2 *pu2Vip2));

VOID
PbbAddVipEntryDll PROTO((tTMO_DLL_NODE * pNode, INT4 i4LocalPort));

VOID
PbbDelCBPEntryDll PROTO((tTMO_DLL_NODE * pNode, UINT4 u4Isid));

VOID
PbbDelVipEntryDll PROTO((tTMO_DLL_NODE * pNode, UINT2 u2LocalPort));

INT1
PbbDeleteVipNodeDll PROTO((UINT2 u2Vip, INT4 i4ifIndex));



VOID
PbbAddIsidEntryDll PROTO((tTMO_DLL_NODE * pNode, INT4 i4LocalPort));


INT1
PbbGetNextActiveBContext PROTO((UINT4 u4CurrContextId, UINT4 *pu4NextContextId));

INT1
PbbGetComponentIdOfIsid PROTO((UINT4 i4Isid, UINT4 *i4ComponentId));


INT1 PbbGetIsidInContext PROTO((UINT4 u4ContextId,UINT4 u4CurrIsid, UINT4 *pu4Nextisid));

INT1
PbbSetVipIsidDefaultBDAddr PROTO((UINT4 u4Isid,tMacAddr DefaultDstBMAC));

VOID
PbbDelIsidEntryDll PROTO((tTMO_DLL_NODE * pNode, UINT2 u2LocalPort));

INT1 PbbSetHwPortPisid PROTO((UINT4 u4ContextId, INT4 i4ifIndex, UINT4 u4Pisid));

INT1
PbbGetFirstVipEntry  PROTO((tPbbICompVipRBtreeNode *pVipEntry));

INT1 PbbGetMaxPortPerIsidPerCtxtPrsnt PROTO((UINT4  u4Isid,
                                                    UINT4 *u4MaxPort));
INT1 PbbGetBackboneEdgeBridgeAddress PROTO((tMacAddr SwitchMac));

INT1 PbbGetBackboneEdgeBridgeName PROTO((tSNMP_OCTET_STRING_TYPE 
                                  *pRetValArPbbBackboneEdgeBridgeName));

INT1 PbbSetBackboneEdgeBridgeName PROTO((tSNMP_OCTET_STRING_TYPE 
                                  *pSetValArPbbBackboneEdgeBridgeName));

INT1 PbbGetNumberOfIComponents PROTO((UINT4 *pu4NumofIComp));

INT1 PbbGetNumberOfBComponents PROTO((UINT4 *pu4NumofBComp));

INT1 PbbGetPbbGlbOUI PROTO((tSNMP_OCTET_STRING_TYPE *pRetValArPbbGlbOUI));

INT1 PbbSetPbbGlbOUI PROTO((tSNMP_OCTET_STRING_TYPE *pRetValArPbbGlbOUI));

INT1 PbbGetShutdownStatus PROTO((INT4 *pi4RetValArPbbShutdownStatus));

INT1 PbbSetShutdownStatus PROTO((INT4 i4SetValArPbbShutdownStatus));

INT1 PbbGetMaxNoOfISID PROTO((INT4 *pi4RetValArPbbMaxNoOfISID));

INT1 PbbGetMaxNoOfISIDPerContext PROTO((INT4 *pi4RetValArPbbMaxNoOfISIDPerContext));

INT1 PbbGetMaxPortsPerISID PROTO((INT4 *pi4RetValArPbbMaxPortsPerISID ));

INT1 PbbGetMaxCurrPortsPerISID PROTO((INT4 *pi4RetValArPbbMaxCurrPortsPerISID ));

INT1 PbbGetMaxPortsPerISIDPerContext PROTO((INT4 *
                                          pi4RetValArPbbMaxPortsPerISIDPerContext ));

INT1 PbbGetMaxCurrPortPerISIDPerCntxt PROTO((INT4 *
                                          pi4RetValArPbbMaxCurrPortsPerISIDPerContext ));


INT1 PbbgetMaxNumofPortForIsid PROTO((UINT4  u4Isid,
        UINT4 *u4MaxPortPerIsid));

   
#ifdef NPAPI_WANTED


INT1
PbbHandleCopyPortPropertiesToHw PROTO((UINT2 u2DstPort, UINT2 u2SrcPort));

INT1
PbbHandleRemovePortPropertiesFromHw PROTO((UINT4 u4DstPort, UINT4 u4SrcPort));
#endif


INT1
PbbGetIsidPortOuiRowstatus PROTO((INT4 i4ifIndex,UINT4 u4Isid, UINT4 *u4Rowstatus));

 INT1
PbbSetIsidPortOuiRowstatus PROTO((INT4 i4ifIndex,UINT4 u4Isid,UINT4 u4Rowstatus));

 INT1
PbbSetPortRBtreeStatus PROTO((UINT2 u2LocalPort, UINT1 u1PortStatus));

 INT1
PbbValidateCNPPort PROTO((INT4 i4ifIndex));

INT1
PbbGetNumPortPerIsidPerContext PROTO((UINT4 u4ContextId, UINT4 u4Isid, UINT4 *u4NumofPortPerIsidPerContext));

INT1    
PbbIncrNumPortPerIsidPerContext PROTO((UINT4 u4Isid));

tPbbPcpData *PbbAllocPipPcpTable PROTO((VOID));

INT1 PbbFreePipPcpTable PROTO((tPbbPcpData * ptobefreed, 
            UINT4 u4arg));

INT1 PbbInitPcpValues PROTO((tPbbPcpData* pPcpData));

INT1 PbbGetPcpDecodingPriority PROTO((
        UINT2  u2LocalPort,
        UINT1  u1PcpSelRow,
        UINT1  u1PcpVal,
        UINT1* pu1PcpPriority));

INT1 PbbGetPcpDecodingDE PROTO((
        UINT2  u2LocalPort,
        UINT1  u1PcpSelRow,
        UINT1  u1PcpVal,
        UINT1* pu1PcpDE));

INT1 PbbSetPcpDecodingPriority PROTO((
        UINT2  u2LocalPort,
        UINT1  u1PcpSelRow,
        UINT1  u1PcpVal,
        UINT1  u1PcpPriority));

INT1 PbbSetPcpDecodingDE PROTO((
        UINT2  u2LocalPort,
        UINT1  u1PcpSelRow,
        UINT1  u1PcpVal,
        UINT1  u1PcpDE));

INT1 PbbValidatePcpDecodingValues PROTO((UINT2 u2LocalPort,
        UINT1  u1PcpSelRow,
        UINT1  u1PcpVal,
        UINT1  u1PcpPriority,
        UINT1  u1PcpDE));

INT1 PbbGetNextPcpDecodingIndices PROTO((INT4  i4IfIndex ,
                             INT4   *pi4NextIfIndex,              
                             UINT1  u1PcpSelRow,
                             UINT1  *pu1NextPcpSelRow,
                             UINT1  u1PcpVal,
                             UINT1  *pu1NextPcpVal));

INT1 PbbGetNextPcpEncodingIndices PROTO((INT4  i4IfIndex ,
                             INT4   *pi4NextIfIndex,              
                             UINT1  u1PcpSelRow,
                             UINT1  *pu1NextPcpSelRow,
                             UINT1  u1PcpPriority ,
                             UINT1  *pu1NextPcpPriority ,
                             UINT1  u1PcpDE,
                             UINT1  *pu1NextPcpDE));

INT1 PbbGetPcpEncodingPcpVal PROTO((
        UINT2  u2LocalPort,
        UINT1  u1PcpSelRow,
        UINT1  u1PcpPriority,
        UINT1  u1PcpDE,
        UINT1* pu1PcpVal));

INT1 PbbSetPcpEncodingPcpVal PROTO((
        UINT2  u2LocalPort,
        UINT1  u1PcpSelRow,
        UINT1  u1PcpPriority,
        UINT1  u1PcpDE,
        UINT1  u1PcpVal));
INT1 PbbGetValidIsidPortOUI PROTO((UINT4 u4Isid ,UINT2 u2LocalPort));

INT1 PbbGetPcpSelRow PROTO((
        UINT2  u2LocalPort,
        UINT1  *pu1PcpSelRow));
INT1 PbbSetPcpSelRow PROTO((
        UINT2  u2LocalPort,
        UINT1  u1PcpSelRow));
INT1 PbbGetPcpUseDei PROTO((
        UINT2  u2LocalPort,
        UINT1  *pu1UseDei));
INT1 PbbSetPcpUseDei PROTO((
        UINT2  u2LocalPort,
        UINT1  u1UseDei));
INT1 PbbGetPcpReqDropEncoding PROTO((
        UINT2  u2LocalPort,
        UINT1  *pu1ReqDropEncoding));
INT1 PbbSetPcpReqDropEncoding PROTO((
        UINT2  u2LocalPort,
        UINT1  u1ReqDropEncoding));

INT4 PbbDelCBPMappingRowStatus PROTO((UINT4 u4ContextId, UINT4 u4Isid,
                                      INT4 i4IfIndex, UINT2 u2LocalPort));

/************************* PBBBUF.C *************************************/

UINT1 *
PbbGetBuf PROTO ((UINT1 u1BufType, 
                   UINT4 u4Size));

VOID  
PbbReleaseBuf PROTO ((UINT1 u1BufType, 
                       UINT1 *pu1Buf));

tCRU_BUF_CHAIN_DESC *
PbbDupBuf PROTO ((tCRU_BUF_CHAIN_DESC *pSrc));


/************************* PBBPORT.C *************************************/
INT1
PbbVcmGetContextPortList PROTO((UINT4 u4ContextId, tPortList PortList));
INT1 PbbCfaGetFreeInterfaceIndex PROTO((
            INT4 *pi4RetValFsPbbNextAvailablePipIfIndex,
            UINT1 u1IfType));

INT1 PbbCfaSetIfMainBrgPortType PROTO ((
            INT4 i4IfMainIndex, 
            INT4 i4SetValIfMainBrgPortType));

INT1
PbbVcmGetIfIndexFromLocalPort PROTO (( UINT4 u4ContextId, 
                                       UINT2 u2LocalPortId,
                                       INT4 *pi4IfIndex));

INT1
PbbVcmGetIfMapHlPortId PROTO ((INT4 i4IfIndex, 
                              UINT2 *pu2localPort));
INT1
PbbVcmGetContextInfoFromIfIndex PROTO((INT4 i4IfIndex, 
                                          UINT4 *pu4ContextId,
                                          UINT2 *pu2LocalPortId));

INT1
PbbVcmGetSystemMode PROTO ((UINT2 u2ProtocolId));

INT1
PbbVcmGetSystemModeExt PROTO ((UINT2 u2ProtocolId));

INT1
PbbVcmGetAliasName PROTO((UINT4 u4ContextId, UINT1 *pu1Alias));

INT4
PbbCfaGetNumberOfBebPorts PROTO((VOID));
INT1
PbbCfaGetIfInfo  PROTO((UINT4 u4IfIndex, tCfaIfInfo * pIfInfo));

INT1
PbbCfaGetIfHwAddr PROTO((INT4 i4IfIndex, tMacAddr *pu1HwAddr));

INT1
PbbCfaSetIfHwAddr PROTO((INT4 i4IfIndex, UINT1 *pu1HwAddr,UINT2 u2Length));

INT1
PbbCfaTestIfHwAddr PROTO((INT4 i4IfIndex, 
                          UINT1 *pu1HwAddr,
                          UINT2 u2Length));

INT1 PbbCfaCreateVip PROTO((INT4 *pi4FreeIfIndex));


INT1
PbbCfaGetIfPipName PROTO(( INT4 i4IfIndex, 
                   tSNMP_OCTET_STRING_TYPE *pPbbPipName));

INT1
PbbCfaSetIfPipName PROTO(( INT4 i4IfIndex, 
                   tSNMP_OCTET_STRING_TYPE *pPbbPipName));

INT1
PbbCfaTestIfPipName PROTO(( INT4 i4IfIndex, 
                   tSNMP_OCTET_STRING_TYPE *pPbbPipName));

INT1
PbbSetDefaultDestMacInServiceMapEntry PROTO ((UINT4 u4ContextId, 
                                              UINT4 u4IfIndex, 
                                              UINT4 u4Isid, 
                                              UINT1 *SetValFsPbbDestinationMacAddr));

INT1 PbbSetPbbGlbOuiInContext PROTO((VOID));

#ifdef L2RED_WANTED
UINT4
PbbRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId, 
                          UINT4 u4DestEntId);

UINT4
PbbRmGetNodeState (VOID);

UINT1
PbbRmGetStandbyNodeCount (VOID);

UINT1
PbbRmGetStaticConfigStatus (VOID);

UINT1
PbbRmHandleProtocolEvent (tRmProtoEvt *pEvt);

UINT4
PbbRmRegisterProtocols (tRmRegParams * pRmReg);

UINT4
PbbRmDeRegisterProtocols (VOID);

UINT4
PbbRmReleaseMemoryForMsg (UINT1 *pu1Block);

INT4
PbbRmSetBulkUpdatesStatus (UINT4 u4AppId);

#endif
/************************* PBBL2WR.C *************************************/

INT4
PbbL2IwfGetPortOperStatus PROTO((INT4 i4ModuleId, UINT2 u2IfIndex,
                            UINT1 *pu1OperStatus));
INT4
PbbL2IwfGetNextValidPortForContext PROTO((UINT4 u4ContextId, UINT2 u2LocalPortId,
                                     UINT2 *pu2NextLocalPort,
                                     UINT4 *pu4NextIfIndex));
INT1
PbbL2IwfIsPortInPortChannel PROTO((INT4 i4IfIndex));

INT1
PbbL2IwfGetPortChannelForPort PROTO((INT4 i4IfIndex, UINT2 *pu2AggId));
INT1
PbbL2IwfGetBridgeMode PROTO((UINT4 u4ContextId, UINT4 *pu4BridgeMode));

INT1
PbbL2IwfSetVipOperStatusFlag (INT4 i4IfIndex, UINT1 u1VipOperStatusFlag);

INT1
PbbL2IwfUpdateVipOperStatus (INT4 i4IfIndex, UINT1 u1VipOperStatus);

/************************* PBBRED.C *************************/
INT4 
PbbRedRegisterWithRM PROTO((VOID));

VOID
PbbRedHandleBulkUpdateEvent PROTO((VOID));

UINT4
PbbRedGetNodeStatus PROTO((VOID));

INT4
PbbRedInitGlobalInfo PROTO((VOID));

INT4
PbbRedDeInitGlobalInfo PROTO((VOID));

INT4
PbbRedInitRedundancyInfo PROTO((VOID));

VOID
PbbRedDeInitRedundancyInfo PROTO((VOID));

VOID
PbbRedDeleteAuditTask PROTO((VOID));

INT4
PbbRedSyncPortOperStatus PROTO((UINT4 u4Context, INT4 i4IfIndex,
                                UINT1 u1Status));

INT4
PbbRedInitBufferPool (VOID);


INT4
PbbRedDeInitBufferPool (VOID);


/************************* PBBNPWR.C *************************************/
INT1 PbbCreateCompBridgeEcfmFilter PROTO((VOID));
VOID PbbDeleteCompBridgeEcfmFilter PROTO((VOID));
INT1 PbbCreateIBCompBridgeEcfmFilter PROTO((VOID));
VOID PbbDeleteIBCompBridgeEcfmFilter PROTO((VOID));
INT1 PbbBrgHwCreateAstControlPktFilter  PROTO((VOID));
INT1 PbbBrgHwDestroyAstControlPktFilter  PROTO((VOID));

INT1
PbbSetBCompHwServiceInstOnDest PROTO((UINT4 u4ContextId,  
                                UINT4 u4Isid, 
                                INT4 i4ifIndex,
                                INT4 i4DestIfIndex));

INT1 PbbHwSetPipPcpAttributesOnDest PROTO((
            UINT4 u4ContextId,
            UINT2 u2SrcPort,
            INT4  i4DestIfIndex));
INT4
PbbHwSetPortUseDei PROTO((UINT4 u4ContextId, UINT4 u4IfIndex, UINT1 u1UseDei));
INT4
PbbHwSetPortReqDropEncoding PROTO((UINT4 u4ContextId, UINT4 u4IfIndex,
                              UINT1 u1ReqDrpEncoding));
INT4
PbbHwSetPortPcpSelection PROTO ((UINT4 u4ContextId, UINT4 u4IfIndex,
                           UINT1 u1PcpSelection));

INT1 PbbHwSetPcpEncodTbl PROTO((UINT4 u4ContextId,
                          UINT4 u4FsPbbPipIfIndex,
                          tHwPbbPcpInfo PcpTblEntry));

INT1 PbbHwSetPcpDecodTbl PROTO((UINT4 u4ContextId,
                          UINT4 u4FsPbbPipIfIndex,
                          tHwPbbPcpInfo PcpTblEntry));

INT1 PbbHwAddVipToPip PROTO ((UINT4 u4ContextId,UINT4 u4Isid,UINT2 u2Vip, UINT2 u2Pip));

INT1 PbbHwDeleteVipToPip PROTO((UINT4 u4ContextId,UINT4 u4Isid,UINT2 u2Vip, UINT2 u2Pip));

INT1
PbbResetNpBackBoneDestAddr PROTO((UINT4 u4ContextId, INT4 i4ifIndex, UINT4 u4Isid));

INT4
PbbGetFsVlanPipVipMap PROTO((UINT4 i4PortId, 
        tSNMP_OCTET_STRING_TYPE *PipVipMap,
        tSNMP_OCTET_STRING_TYPE *PipVipMap1,
        tSNMP_OCTET_STRING_TYPE *PipVipMap2,
        tSNMP_OCTET_STRING_TYPE *PipVipMap3,
        tSNMP_OCTET_STRING_TYPE *PipVipMap4));

INT4 PbbGetVipListForVlanPvid PROTO((UINT4 u4ContextId,
  UINT1 *au1VipList));

INT4
PbbSetIfMainRowStatus PROTO((
        INT4 u4IfIndex,
        INT4 u4RowStatus
        ));
INT4
PbbSetFsVcIfRowStatus PROTO((
        INT4 u4IfIndex,
        INT4 u4RowStatus
        ));
INT4
PbbTestv2IfMainRowStatus PROTO((
        UINT4 *pu4ErrorCode,
        INT4  u4IfIndex,
        INT4 u4RowStatus
        ));
INT4
PbbTestv2FsVcIfRowStatus PROTO((
        UINT4 *pu4ErrorCode,
        INT4 u4IfIndex,
        INT4 u4RowStatus
        ));

INT4 
PBBGetBDAFromOUI(tSNMP_OCTET_STRING_TYPE *pOUI,UINT4 u4Isid,tMacAddr BackboneDest);
INT4
PbbL2IwfGetPortVlanList (UINT4 u4ContextId,
                         tSNMP_OCTET_STRING_TYPE * pVlanIdList,
                         UINT2 u2LocalPort, UINT1 u1VlanPortType);
INT4
PbbL2IwfSetAllToOneBndlStatus (UINT4 u4ContextId, UINT1 u1Status);

INT4 PbbHwSetPortIsidLckStatus PROTO ((UINT4, UINT4,UINT1));
INT4 PbbHwGetPortIsidStats PROTO ((UINT4, UINT4,UINT1, UINT4 *));

VOID
PbbMainAssignMempoolIds PROTO ((VOID));

#endif

