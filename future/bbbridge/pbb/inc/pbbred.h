/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbred.h,v 1.5 2011/09/12 06:42:14 siva Exp $
 *
 * Description:  This file contains constant and typedefs used in 
 *               PBB Redundancy Module.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2008                                               */
/*                                                                           */
/*  FILE NAME             : pbbred.h                                         */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : PBB                                              */
/*  MODULE NAME           : PBB                                              */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 10 Mar 2009                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains constant and typedefs used in */
/*                          PBB Redundanacy Module.                          */
/*                                                                           */
/*****************************************************************************/

#ifndef _PBBRED_H
#define _PBBRED_H

#define PBB_RED_MAX_PBB_DEL_BKTS   100

#define PBB_RED_TYPE_FIELD_SIZE     1
#define PBB_RED_LEN_FIELD_SIZE      2
#define PBB_RED_CONTEXT_FIELD_SIZE  4
#define PBB_RED_PBB_ID_LEN         2
#define PBB_RED_FDB_ID_LEN          4
#define PBB_RED_PORT_NO_LEN         4
#define PBB_RED_PORT_OPER_STATUS_SIZE     1
#define PBB_RED_MAC_ADDR_LEN     6

#define PBB_RED_MAX_MSG_SIZE              1500
#define PBB_RED_DEL_ALL_UPD_SIZE          7
#define PBB_RED_BULK_REQ_MSG_SIZE         3
#define PBB_RED_BULK_UPD_TAIL_MSG_SIZE    3
       
#define PBB_RED_ENTRY_PRESENT_IN_BOTH     1
#define PBB_RED_ENTRY_PRESENT_IN_HW_ONLY  2
#define PBB_RED_ENTRY_PRESENT_IN_SW_ONLY  3
#define PBB_RED_ENTRY_NOT_PRESENT_IN_BOTH 4 

/* gPbbGlobData.PbbRedInfo.u1AuditFlag takes any of the three values.
 *
 * PBB_RED_AUDIT_STOP  -- Mean the audit event should not happen or already
 *                        done.
 * PBB_RED_AUDIT_CLEAN_AND_STOP  -- Means, audit event has to stopped. 
 *                                  This will occur when GO_STANDBY event comes
 *                                  on auditing time
 * PBB_RED_AUDIT_START -- Means, audit event is started and its currently 
 *                        going on.
 */
#define PBB_RED_AUDIT_STOP              0
#define PBB_RED_AUDIT_START             1
#define PBB_RED_AUDIT_CLEAN_AND_STOP    2

#define PBB_AUDIT_TASK_PRIORITY     240 /* should have lower priority */
#define PBB_AUDIT_TASK            ((UINT1*)"PBBU")

#define PBB_RED_AUDIT_START_EVENT         0x00000001
#define PBB_RED_AUDIT_BRG_MODE_CHG_EVENT  0x00000002
#define PBB_RED_AUDIT_TASK_DELETE_EVENT   0x00000004

#define PBB_RED_NO_OF_PORTS_PER_SUB_UPDATE        10
#define PBB_RED_BULK_UPD_REMOVE_PBB               2


/* EventIds for the NPSYNC UP in PBB */
#define    EVTSYNC_FS_MI_PBB_HW_SET_PORT_USE_DEI                 1
#define    EVTSYNC_FS_MI_PBB_HW_SET_PORT_REQ_DROP_ENCODING       2
#define    EVTSYNC_FS_MI_PBB_HW_SET_PORT_PCP_SELECTION           3
#define    EVTSYNC_FS_MI_PBB_HW_SET_PCP_ENCOD_TBL                4
#define    EVTSYNC_FS_MI_PBB_HW_SET_PCP_DECOD_TBL                5
#define    EVTSYNC_FS_MI_PBB_HW_SET_PROVIDER_BRIDGE_PORT_TYPE    6

/* EventId for the Special Loop Events */
#define    EVTSYNC_FS_MI_PBB_DELETE_PORT                    7
#define    EVTSYNC_FS_MI_PBB_SET_GLOBAL_OUI                 9
#define    EVTSYNC_FS_MI_PBB_COPY_PORT_PROPERTIES           10
#define    EVTSYNC_FS_MI_PBB_REMOVE_PORT_PROPERTIES         11

/* Macros */
#define PBB_RED_NUM_STANDBY_NODES() (gPbbGlobData.PbbRedInfo.u1NumStandbyNodes)

#define PBB_RED_AUDIT_FLAG() (gPbbGlobData.PbbRedInfo.u1AuditFlag)

#define PBB_RED_VLAN_AUDIT_FLAG() (gPbbGlobData.PbbRedInfo.u1VlanAuditEnd)

#define PBB_RED_AUD_BRG_MODE_CHG_CONTEXT() \
    (gPbbGlobData.PbbRedInfo.u4BrgModeChgContextId)

#define PBB_RED_BULK_REQ_RECD() (gPbbGlobData.PbbRedInfo.bBulkReqRcvd)

#define PBB_RED_GET_STATIC_CONFIG_STATUS() PbbRmGetStaticConfigStatus()

#define PBB_RED_NUM_STANDBY_NODES() (gPbbGlobData.PbbRedInfo.u1NumStandbyNodes)

#define PBB_AUDIT_TASK_ID() (gPbbGlobData.PbbRedTaskInfo.PbbAuditTaskId)

#define PBB_NPSYNC_BLK() (gPbbGlobData.PbbRedInfo.u1PBBNpSyncBlk)


#define PBB_RED_BUF_SLL()  (gPbbGlobData.PbbRedInfo.PbbRedBufferSll)

/* Macros to write in to RM buffer. */
#define PBB_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define PBB_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define PBB_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define PBB_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define PBB_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define PBB_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define PBB_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define PBB_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)

/* Represents the message types encoded in the update messages */
typedef enum {
    PBB_UPDATE_MESSAGE = 1,
    PBB_PORT_OPER_STATUS_MESSAGE,
    PBB_BULK_REQ_MESSAGE,
    PBB_BULK_UPD_TAIL_MESSAGE
}tPbbRmMessageType;

typedef struct _tPbbRedGlobalInfo {
    tTMO_SLL       PbbRedBufferSll;

    UINT4          u4BulkUpdNextPort;   /* Points to the Next Port No for
                                           which next bulk update has to
                                           be generated.
                                         */
    UINT4          u4BulkUpdNextContext;
    UINT4          u4BrgModeChgContextId;
    BOOL1          bBulkReqRcvd;        /* To check whether bulk request
                                           msg recieved from standby
                                           before RM_STANDBY_UP event.
                                         */  
    UINT1          u1NumStandbyNodes;
    UINT1          u1AuditFlag;
    UINT1          u1PBBNpSyncBlk;
    UINT1          u1VlanAuditEnd; /* This flag is set when the Vlan Audit
                                      is finished. Auditing of Delete_port,
                                      and Delete_Context need to be done
                                      only when this flag is set */
    UINT1          u1Reserved [3];
}tPbbRedGlobalInfo;

typedef struct _tPbbRedTaskInfo {
    tOsixTaskId      PbbAuditTaskId;
}tPbbRedTaskInfo;


VOID
PbbRedProcessUpdateMsg (tPbbQMsg * pPbbQMsg);

VOID
PbbRedCreateAuditTask (VOID);

INT4
PbbRedMakeNodeActiveFromIdle (VOID);

INT4
PbbRedMakeNodeActiveFromStandby (VOID);

VOID
PbbRedHandleStandbyUpEvent (VOID);

VOID
PbbRedSendBulkUpdates (VOID);

INT4
PbbRedMakeNodeStandbyFromActive (VOID);


VOID
PbbRedHandleUpdates (tRmMsg * pMsg, UINT2 u2DataLen);

INT4
PbbRedHandlePortOperStatusUpdate (UINT4 u4Port, UINT1 u1PortStatus);

INT4
PbbRedMakeNodeStandbyFromIdle (VOID);

VOID
PbbRedSendBulkReqMsg (VOID);

INT4
PbbRedSendBulkUpdTailMsg (VOID);

INT4
PbbRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Len);

VOID
PbbRedHandleVlanAudEndEvent (UINT4 u4VlanAudStatus, UINT4 u4ContextId);

VOID
PbbRedAuditMain (INT1 *pi1Param);

#ifdef NPAPI_WANTED
VOID
PbbRedStartAudit (VOID);

VOID
PbbRedAuditBufferEntry (tPbbBufferEntry *pBuf);

VOID
PbbHwAuditCreateOrFlushBuffer (unNpSync  *pNpSync, UINT4 u4NpApiId,
                               UINT4 u4EventId);

VOID
PbbRedAuditControlPktFilter (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditBCompServInst (UINT4 u4ContextId, UINT4 u4CbpIfIndex,
                          UINT4 u4BSid);

VOID
PbbRedAuditPisid (UINT4 u4ContextId, UINT4 u4IfIndex,
                  UINT4 u4Pisid);

VOID
PbbRedAuditDelPisid (tPbbBufferEntry     *pBuf);

VOID
PbbRedAuditInitHw (VOID);

VOID
PbbRedAuditDeInitHw (VOID);

VOID
PbbRedAuditICompServInst (UINT4 u4ContextId, UINT4 u4VipIndex);

VOID
PbbRedAuditVipToPipMap (UINT4 u4ContextId, UINT4 u4VipIfIndex);

VOID
PbbRedAuditForDei (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditForPcpEncTable (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditForPcpDecTable (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditForPortDropEnc (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditForPcpSelection (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditForPipPortType (tPbbBufferEntry *pBuf);

VOID
PbbNpSyncProcessSyncMsg (tRmMsg * pMsg, UINT2 *u3OffSet);

VOID
PbbEventSyncProcessSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet);

VOID
PbbRedAuditDeletePort (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditDeleteContext (UINT4 u4ContextId);

VOID
PbbRedAuditFilters (UINT4 u4ContetxId);

VOID
PbbRedAuditSetGlbOui (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditCopyPortProperties (tPbbBufferEntry *pBuf);

VOID
PbbRedAuditRemPortProperties (tPbbBufferEntry *pBuf);

VOID
PbbSetPortProperties (UINT4 u4ContextId, INT4 i4IfIndex,
                      UINT2 u2LocalPort);
VOID
PbbSetPortPropertiesinHw (UINT4 u4ContextId, INT4 i4IfIndex,
                          UINT2 u2SrcPort);

/* Function Prototypes defined manually for the VLAN NPAPI
   called from PBB */
INT4
NpSyncFsMiPbbHwSetPortUseDei (UINT4 u4ContextId,
                              UINT4 u4IfIndex,
                              UINT1 u1UseDei);

INT4
NpSyncFsMiPbbHwSetPortReqDropEncoding(UINT4 u4ContextId,
                                       UINT4 u4IfIndex,
                                       UINT1 u1ReqDrpEncoding);

INT4
NpSyncFsMiPbbHwSetPortPcpSelection (UINT4 u4ContextId,
                                    UINT4 u4IfIndex,
                                    UINT2 u2PcpSelection);

INT4
NpSyncFsMiPbbHwSetPcpEncodTbl (UINT4            u4ContextId,
                               UINT4            u4IfIndex,
                               tHwVlanPbPcpInfo NpPbVlanPcpInfo);

INT4
NpSyncFsMiPbbHwSetPcpDecodTbl (UINT4            u4ContextId,
                               UINT4            u4IfIndex,
                               tHwVlanPbPcpInfo NpPbVlanPcpInfo);

INT4
NpSyncFsMiPbbHwSetProviderBridgePortType (UINT4 u4ContextId,
                                          UINT4 u4IfIndex,
                                          UINT4 u4PortType);

INT4
EvtSyncFsMiPbbDeletePort (UINT4 u4ContextId,
                          UINT4 u4IfIndex);

INT4
EvtSyncFsMiPbbSetGlbOui (UINT1 *pau1OUI);

INT4
EvtSyncFsMiPbbCopyPortProperty (UINT4 u4ContextId, UINT4 u4IfIndex, 
                                UINT4 u4DstPort);

INT4
EvtSyncFsMiPbbRemPortProperty (UINT4 u4ContextId, UINT4 u4IfIndex, 
                               UINT4 u4DstPort);

VOID
PbbEvtSyncPortIdxSync (tNpSyncPortIdx PortIdx, UINT4 u4AppId,
                       UINT4 u4EventId);

VOID
PbbEvtSyncGlbOuiSync (tNpSyncPbbGlbOui PbbGlbOui, UINT4 u4AppId,
                      UINT4 u4EventId);

VOID
PbbEvtSyncPortPropertySync (tNpSyncPbbPortProperty PortProp, UINT4 u4AppId,
                            UINT4 u4EventId);
#endif

#endif /* _PBBRED_H */
