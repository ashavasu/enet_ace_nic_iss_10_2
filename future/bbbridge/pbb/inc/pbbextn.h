/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/* $Id: pbbextn.h,v 1.2 2009/04/06 12:01:57 prabuc-iss Exp $                                                               */
/*  FILE NAME             : pbbextn.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : PBB                                             */
/*  MODULE NAME           : PBB                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2002                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains extern declarations of        */
/*                          glabal data structures in PBB module.           */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 2.0                                              */
/*  Date(DD/MM/YYYY)      : 15/11/2001                                       */
/*  Modified by           : PBB TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _PBBEXTN_H
#define _PBBEXTN_H
extern tPbbGlobData        gPbbGlobData;            
#ifdef PBB_PERFORMANCE_WANTED
extern struct timeval      gCurTime;
extern struct timeval      gStTime;
extern struct timeval      gEnTime;
extern UINT4               gu4TimeTaken;
extern UINT4               gu4TotalTimeTaken;
extern UINT4               gu4ISIDCreateTimeTaken;
extern UINT4               gu4ISIDDeleteTimeTaken;
extern UINT4               gu4VlanIsidTimeTaken;
extern UINT4               gu4DelVlanIsidTimeTaken;
extern UINT4               gu4CreateBVLANTimeTaken;
extern UINT4               gu4DeleteBVLANTimeTaken;
extern UINT4               gu4CreateBVLANTunnelTimeTaken;
extern UINT4               gu4DeleteBVLANTunnelTimeTaken;
#endif

#endif
