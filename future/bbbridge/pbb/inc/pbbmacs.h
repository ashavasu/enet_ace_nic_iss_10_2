/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/* $Id: pbbmacs.h,v 1.7 2013/05/06 11:56:02 siva Exp $                      */
/*                                                                           */
/*  FILE NAME             : pbbmacs.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : PBB                                              */
/*  MODULE NAME           : PBB                                              */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 26 Mar 2008                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains definitions of macros used    */
/*                          in the PBB module.                               */
/*                                                                           */
/*****************************************************************************/


#ifndef _PBBMACS_H
#define _PBBMACS_H

#define PBB_LOCK()       PbbLock ()
#define PBB_UNLOCK()     PbbUnLock ()
#define PBB_DEFAULT_INSTANCE_NAME     "default"
#define PBB_SIZING_CONTEXT_LIST_COUNT  ((PBB_MAX_INSTANCES + 31)/32 * 4) 

#ifdef PBB_PERFORMANCE_WANTED

#define PBB_PERF_MARK_START_TIME()     gettimeofday(&gStTime, NULL);
#define PBB_PERF_MARK_END_TIME(u4TimeTaken)      \
{\
    long long u4T1, u4T2;\
    gettimeofday(&gEnTime, NULL);\
    u4T1 = (gStTime.tv_sec * 1000000 + gStTime.tv_usec);\
    u4T2 = (gEnTime.tv_sec * 1000000 + gEnTime.tv_usec);\
    gu4TimeTaken = (u4T2 - u4T1);\
    u4TimeTaken += (u4T2 - u4T1);\
}
#define PBB_PERF_PRINT_TIME(u4TimeTaken)      \
{\
    PRINTF("Time taken ->  micro-sec=%u\r\n",u4TimeTaken);\
}

#define PBB_PERF_CURR_TIME()      \
{\
    long long u4T1;\
    gettimeofday(&gCurTime, NULL);\
    u4T1 = (gCurTime.tv_sec * 1000000 + gCurTime.tv_usec);\
    PRINTF("Current Time ->  micro-sec=%llu \r\n",u4T1);\
}

#define PBB_PERF_PRINT_VLANTIME(u4TimeTaken)      \
{\
    PRINTF("VLAN Time taken ->  micro-sec=%u\r\n",u4TimeTaken);\
}
#define PBB_PERF_PRINT_TOTTIME(u4Time1, u4Time2)      \
{\
    PRINTF("Total Time taken ->  micro-sec=%u\r\n",u4Time1 +\
            u4Time2);\
}
#define PBB_PERF_PRINT_DELTOTTIME()      \
{\
    PRINTF("Total Time taken ->  micro-sec=%u\r\n",gu4ISIDDeleteTimeTaken +\
            gu4DelVlanIsidTimeTaken);\
}

#define PBB_PERF_RESET_TIME(u4Time)     u4Time = 0;
#else
#define PBB_PERF_MARK_START_TIME()
#define PBB_PERF_MARK_END_TIME(u4TimeTaken)
#define PBB_PERF_PRINT_TIME(u4TimeTaken)
#define PBB_PERF_CURR_TIME()
#define PBB_PERF_PRINT_VLANTIME(u4TimeTaken)
#define PBB_PERF_PRINT_TOTTIME(u4Time1, u4Time2)
#define PBB_PERF_PRINT_DELTOTTIME()
#define PBB_PERF_RESET_TIME(u4Time)
#endif

/* Get the entries from the Rb trees */
#define PBB_GET_VIP_ENTRY(u4Vip,pPbbVipRBtreeNode) \
    PbbGetVipData(u4Vip,&pPbbVipRBtreeNode)

#define PBB_GET_PORT_ENTRY(u2localPort,pPbbPortRBtreeNode) \
    PbbGetPortData(u2localPort,&pPbbPortRBtreeNode)

#define PBB_GET_ISID_ENTRY(u4Isid, pPbbIsidRBtreeNode) \
    PbbGetIsidData(u4Isid,&pPbbIsidRBtreeNode)

#define PBB_GET_ISID_PORT_ENTRY(u4Isid,u2localPort,pPbbIsidPortRBtreeNode) \
    PbbGetIsidPortData(u4Isid,u2localPort,&pPbbIsidPortRBtreeNode) 
    


#define PBB_VALID_ROW_STATUS(RowStatus)\
    ((RowStatus == PBB_CREATE_AND_WAIT) ||\
    (RowStatus == PBB_CREATE_AND_GO) ||\
    (RowStatus == PBB_NOT_IN_SERVICE) ||\
    (RowStatus == PBB_ACTIVE) ||\
    (RowStatus == PBB_NOT_READY) ||\
    (RowStatus == PBB_DESTROY)?PBB_SUCCESS:PBB_FAILURE)


#define PBB_CONCAT_OUI_ISID(DmacAddr,u4Isid,OUI)\
    PBB_MEMCPY(DmacAddr,OUI,PBB_OUI_LENGTH); \
    PBB_MEMCPY(DmacAddr+PBB_OUI_LENGTH,u4Isid,PBB_OUI_LENGTH);


#define PBB_INVALID_ROWSTATUS 0



#define PBB_UNSET_MEMBER_PORT(au1PortArray, u2Port) \
              {\
                  if(au1PortArray != 0)\
                  {\
                      UINT2 u2PortBytePos = PBB_INIT_VAL;\
                      UINT2 u2PortBitPos = PBB_INIT_VAL;\
                      u2PortBytePos = (UINT2)(u2Port / PBB_PORTS_PER_BYTE);\
                      u2PortBitPos  = (UINT2)(u2Port % PBB_PORTS_PER_BYTE);\
                      if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
                      \
                      au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                              & ~gPbbGlobData.au1PbbPortBitMaskMap[u2PortBitPos]);\
                  }\
              }



/* Validate the Storage type  */
#define PBB_VALID_STORAGE_TYPE(storageType)          \
    ((storageType == PBB_STORAGE_TYPE_VOLATILE) ||   \
    (storageType == PBB_STORAGE_TYPE_NONVOLATILE) || \
    (storageType == PBB_STORAGE_TYPE_PERMANAENT) ||  \
    (storageType == PBB_STORAGE_TYPE_READONLY) ||    \
    (storageType == PBB_STORAGE_TYPE_OTHER)) ? PBB_SUCCESS:PBB_FAILURE
    




#define PBB_MEMORY_TYPE()      \
    ((VcmGetSystemMode (PBBMOD_PROTOCOL_ID) == VCM_SI_MODE) \
     ? MEM_DEFAULT_MEMORY_TYPE : MEM_HEAP_MEMORY_TYPE)


#define PBB_IS_PORT_VALID(u2Port) \
        (((u2Port > PBB_MAX_PORTS) || (u2Port == 0)) ? PBB_FALSE : PBB_TRUE)

#define PBB_IS_CONTEXT_VALID(i4ContextId) \
        (((i4ContextId < 0) || (i4ContextId >= PBB_SIZING_CONTEXT_COUNT)) ? PBB_FALSE : PBB_TRUE)

   
    /* This macro is used to find whether the specified port u2Port is
    * a member of the specified port list.
    */
   /* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.*/
#define PBB_IS_MEMBER_PORT(au1PortArray, u2Port, u1Result) \
        {\
           UINT2 u2PortBytePos;\
           UINT2 u2PortBitPos;\
           u2PortBytePos = (UINT2)(u2Port / PBB_PORTS_PER_BYTE);\
           u2PortBitPos  = (UINT2)(u2Port % PBB_PORTS_PER_BYTE);\
    if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
           if ((au1PortArray[u2PortBytePos] \
                & gPbbGlobData.au1PbbPortBitMaskMap[u2PortBitPos]) != 0) {\
           \
              u1Result = PBB_TRUE;\
           }\
           else {\
           \
              u1Result = PBB_FALSE; \
           } \
        }

    /* This macro adds u2Port as a member port in the list pointed by
     * au1PortArray
     */
#define  PBB_IS_NULL_PORTLIST(au1List) \
         ((MEMCMP (au1List, gPbbNullIfPortList, sizeof (gPbbNullIfPortList)) == 0) \
          ? VLAN_TRUE : VLAN_FALSE)

 /* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.*/
#define PBB_SET_MEMBER_PORT(au1PortArray, u2Port) \
           {\
              UINT2 u2PortBytePos = PBB_INIT_VAL;\
              UINT2 u2PortBitPos = PBB_INIT_VAL;\
              u2PortBytePos = (UINT2)(u2Port / PBB_PORTS_PER_BYTE);\
              u2PortBitPos  = (UINT2)(u2Port % PBB_PORTS_PER_BYTE);\
       if (u2PortBitPos  == 0) {u2PortBytePos -= 1;} \
           \
              au1PortArray[u2PortBytePos] = (UINT1)(au1PortArray[u2PortBytePos] \
                             | gPbbGlobData.au1PbbPortBitMaskMap[u2PortBitPos]);\
           }

 /* Warning!!! - Do not call the macro with u2Context as 0 or Invalid Port.*/
#define PBB_SET_MEMBER_CONTEXT(au1ContextArray, u4Context) \
           {\
              UINT2 u2BytePos = PBB_INIT_VAL;\
              UINT2 u2BitPos = PBB_INIT_VAL;\
              u2BytePos = (UINT2)(u4Context / PBB_CONTEXTS_PER_BYTE);\
              u2BitPos  = (UINT2)(u4Context % PBB_CONTEXTS_PER_BYTE);\
       if (u2BytePos !=0 && u2BitPos  == 0) {u2BytePos -= 1;} \
           \
              au1ContextArray[u2BytePos] = (UINT1)(au1ContextArray[u2BytePos] \
                             | gPbbGlobData.au1PbbPortBitMaskMap[u2BitPos]);\
           }


#define PBB_MODULE_STATUS()   gPbbGlobData.au1PbbStatus[gPbbGlobData.pPbbContextInfo->u4ContextId]

#define PBB_SYSTEM_CONTROL_FOR_CONTEXT(u4CtxId)\
 ((gPbbGlobData.u1IsPbbInitialised == PBB_FALSE)?PBB_TRUE:gPbbGlobData.au1PbbShutDownStatus)

#define PBB_BRIDGE_MODE()               gPbbGlobData.pPbbContextInfo->u4BridgeMode

#define PBB_PROVIDER_BACKBONE_BRIDGE() \
    ((PBB_BRIDGE_MODE () == PBB_CUSTOMER_BRIDGE_MODE) || \
      (PBB_BRIDGE_MODE () == PBB_PROVIDER_BRIDGE_MODE) || \
        (PBB_BRIDGE_MODE () == PBB_PROVIDER_EDGE_BRIDGE_MODE) || \
          (PBB_BRIDGE_MODE () == PBB_PROVIDER_CORE_BRIDGE_MODE) || \
              (PBB_BRIDGE_MODE () == PBB_INVALID_BRIDGE_MODE ) ? PBB_FALSE : PBB_TRUE)

#define PBB_GLOBAL_OUI       gPbbGlobData.PbbTaskInfo.au1OUI
#define PBB_QMSG_TYPE(pMsg) pMsg->u2MsgType


#define PBB_IS_MODULE_INITIALISED() \
        ((gPbbGlobData.u1IsPbbInitialised == PBB_TRUE) ? PBB_TRUE : PBB_FALSE)

#define PBB_CURR_CONTEXT_PTR()    gPbbGlobData.pPbbContextInfo
#define PBB_CURR_CONTEXT_ID()     gPbbGlobData.pPbbContextInfo->u4ContextId
#define PBB_CONTEXT_PTR(CtxId)    gPbbGlobData.apPbbContextInfo[CtxId]
#define PBB_CURR_CONTEXT_STR()    gPbbGlobData.pPbbContextInfo->au1ContextStr

#define PBB_CURR_INSTANCE_PTR()    gPbbGlobData.pPbbInstanceInfo
#define PBB_CURR_INSTANCE_ID()     gPbbGlobData.pPbbInstanceInfo->u4InstanceId
#define PBB_INSTANCE_PTR(InstId)   gPbbGlobData.apPbbInstanceInfo[InstId]
#define PBB_CURR_INSTANCE_STR()    gPbbGlobData.pPbbInstanceInfo->au1InstanceStr

#define PBB_TASK_ID             gPbbGlobData.PbbTaskInfo.PbbTaskId
#define PBB_SEM_ID              gPbbGlobData.PbbTaskInfo.PbbSemId

#define PBB_NODE_STATUS() (gPbbGlobData.gu4PbbNodeStatus)

#define   PBB_IS_NP_PROGRAMMING_ALLOWED() PBB_TRUE

#define PBB_IS_BCASTADDR(pMacAddr) \
        ((PBB_MEMCMP(pMacAddr,gPbbGlobData.PbbBcastAddress,ETHERNET_ADDR_SIZE)) ? \
          PBB_FALSE : PBB_TRUE)


#define PBB_IS_MCASTADDR(pMacAddr) \
        (((pMacAddr[0] & 0x01) != 0) ? \
         PBB_TRUE : PBB_FALSE)


#define PBB_IS_PCP_ROW_VALID(selectedrow) \
        (((selectedrow < PBB_8P0D_SEL_ROW ) || (selectedrow > \
          PBB_5P3D_SEL_ROW)) ? PBB_FALSE : PBB_TRUE)

#define PBB_IS_PCP_VALUE_VALID(pcpval) \
        (((pcpval < PBB_MIN_PCP_VAL) || (pcpval > PBB_MAX_PCP_VAL)) \
         ? PBB_FALSE : PBB_TRUE)
#define PBB_IS_PRIORITY_VALID(priority) \
        (((priority < PBB_MIN_PRIORITY) || (priority > PBB_MAX_PRIORITY)) \
         ? PBB_FALSE : PBB_TRUE)
#define PBB_IS_PCP_DE_VALID(DE) \
        (((DE != PBB_VLAN_SNMP_TRUE) && (DE != PBB_VLAN_SNMP_FALSE)) ? PBB_FALSE : \
        PBB_TRUE)
#define PBB_IS_PCP_USE_DEI_VALID(useDEI) \
        (((useDEI != PBB_SNMP_TRUE) && (useDEI != PBB_SNMP_FALSE)) ? PBB_FALSE : \
        PBB_TRUE)

#define PBB_IS_PCP_REQ_DROP_ENC_VALID(dropEnc) \
        (((dropEnc != PBB_SNMP_TRUE) && (dropEnc != PBB_SNMP_FALSE)) ? PBB_FALSE : \
        PBB_TRUE)

#define PBB_SET_DEFAULT_GLB_OUI(au1OUI) \
{\
   au1OUI[0] = 0;\
   au1OUI[1] = 30;\
   au1OUI[2] = 131;\
}
#endif /* _PBBMACS_H */
