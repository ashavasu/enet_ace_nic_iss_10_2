/*****************************************************************************/
/* Copyright PROTO((C)) 2006 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*                          pbbmbsm.h                                         */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  DatePROTO((DD/MM/YYYY))      : 31/07/2008                                       */
/*  Modified by           : PBB  TEAM                                        */
/*  Description of change : Created Original                                 */
/*****************************************************************************/


#ifndef _PBBMBSM_H
#define _PBBMBSM_H

#ifdef MBSM_WANTED
#define MBSM_PBB    "MbsmPbb"

INT1
PbbMbsmUpdateOnCardInsertion   PROTO((tMbsmPortInfo * pPortInfo,
                               tMbsmSlotInfo * pSlotInfo));

INT1
PbbMbsmSetPortPropertiesToHw   PROTO((tMbsmSlotInfo * pSlotInfo,
                                tLocalPortList LocalPorts));
INT1
PbbMbsmUpdateOnCardRemoval   PROTO((tMbsmPortInfo * pPortInfo,
                               tMbsmSlotInfo * pSlotInfo));

INT1
PbbMbsmUpdtForLoadSharing   PROTO((UINT1 u1Flag));

/************************* PBB MBSM NP WR**********************************/
INT1 PbbMbsmHwInit PROTO((tMbsmSlotInfo * pSlotInfo));

INT4
PbbMbsmHwSetProviderBridgePortType (UINT4 u4ContextId, INT4 i4IfIndex,
                                UINT1 u1PortType,
                                tMbsmSlotInfo * pSlotInfo);
INT4
PbbMbsmHwSetPortUseDei PROTO((
            UINT4 u4ContextId, 
            UINT4 u4IfIndex, 
            UINT1 u1UseDei,
            tMbsmSlotInfo * pSlotInfo));
INT4
PbbMbsmHwSetPortReqDropEncoding PROTO((
            UINT4 u4ContextId, 
            UINT4 u4IfIndex,
            UINT1 u1ReqDrpEncoding,
            tMbsmSlotInfo * pSlotInfo));

INT4
PbbMbsmHwSetPortPcpSelection PROTO((UINT4 u4ContextId, UINT4 u4IfIndex,
                           UINT1 u1PcpSelection,
                           tMbsmSlotInfo * pSlotInfo));

INT1 PbbMbsmHwSetPcpEncodTbl PROTO((UINT4 u4ContextId,
                          UINT4 u4IfIndex,
                          tHwPbbPcpInfo PcpTblEntry,
                          tMbsmSlotInfo * pSlotInfo));

INT1 PbbMbsmHwSetPcpDecodTbl PROTO((UINT4 u4ContextId,
                          UINT4 u4IfIndex,
                          tHwPbbPcpInfo PcpTblEntry,
                          tMbsmSlotInfo * pSlotInfo));

INT1 PbbMbsmHwSetPipPcpAttributes PROTO((
            UINT4 u4ContextId,
            UINT2 u2SrcPort,
            tMbsmSlotInfo * pSlotInfo));

INT1
pbbMbsmSetICompHwServiceInst PROTO((UINT4 u4ContextId, 
        INT4 i4ifIndex, 
        UINT4 u4Isid,
        tMbsmSlotInfo * pSlotInfo));

    INT1
pbbMbsmSetBCompHwServiceInst PROTO((UINT4 u4ContextId,  
                              UINT4 u4Isid, 
                              INT4 i4ifIndex,
                              tMbsmSlotInfo * pSlotInfo));



INT1 PbbMbsmHwSetPortInfo PROTO((UINT4 u4ContextId,
                          INT4 i4IfIndex,
                          tMbsmSlotInfo * pSlotInfo));

INT1 PbbMbsmCreateCompBridgeEcfmFilter PROTO( 
                           (UINT4 u4ContextId,
                            tMbsmSlotInfo * pSlotInfo));

INT1 PbbMbsmCreateIBCompBridgeEcfmFilter PROTO (
                            (UINT4 u4ContextId,
                            tMbsmSlotInfo *pSlotInfo));
INT1 PbbBrgMbsmHwCreateAstControlPktFilter PROTO (
                            (UINT4 u4ContextId, 
                            tMbsmSlotInfo *pSlotInfo));

#endif /* MBSM_WANTED */

#endif
