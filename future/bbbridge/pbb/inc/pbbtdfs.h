/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbtdfs.h,v 1.22 2011/12/30 12:06:10 siva Exp $
 *
 * Description:  This file contains type definitions of the data 
 *               structures used in PBB Module.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                                  */
/* Licensee Aricent Inc., 2001-2002                        */
/*                                                                           */
/*  FILE NAME             : pbbtdfs.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                  */
/*  SUBSYSTEM NAME        : PBB                                             */
/*  MODULE NAME           : PBB                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 10 Aug 2008                                      */
/*  AUTHOR                : Aricent Inc.                                  */
/*  DESCRIPTION           : This file contains type definitions of the data  */
/*                          structures used in PBB Module.                  */
/**************************************************************************/
#ifndef _PBBTDFS_H
#define _PBBTDFS_H

typedef tCbpStatus tCbpStatusInfo;

/***************************************************************************/
/* PBB Pool Ids */
/***************************************************************************/
typedef struct _PbbPoolIds {
   tMemPoolId PbbPortPoolId;  /* For Port pool table entries */
   tMemPoolId PbbIsidPoolId;  /* For per ISID entries */
   tMemPoolId PbbIsidPortPoolId;  /* For per ISID and port entries */
   tMemPoolId PbbQMsgPoolId;  /* For Config Msg */
   tMemPoolId PbbVipPoolId;  /* For PBB Port entry table */
   tMemPoolId PbbContextPoolId;  /* For Pbb Context entry table */
   tMemPoolId PbbInstancePoolId;  /* For Pbb Instance entry table */
   tMemPoolId PbbIsidVipInfoPoolId;  /* For ISID VIP data base */
   tMemPoolId PbbCBPDllPoolId;  /* For CBP DLL data base */
   tMemPoolId PbbCnpDllPoolId;  /* For CBP DLL data base */
   tMemPoolId PbbPipVipMapPoolId; /* For Pip Vip Map DLL data base */
   tMemPoolId PbbPipPcpPoolId; /* For Pip Pcp encoding/decoding tables */
   tMemPoolId PbbContextDllPoolId;  /* For Context DLL data base */
   tMemPoolId PbbRedBuffMemPoolId; /* MemPool for Red Buffer Entries */
   tMemPoolId PbbCbpStatusMemPoolId; /* MemPool for CbpStatus Entries */
}tPbbPoolInfo;

/***************************************************************************/
/* PBB Task Info*/  
/***************************************************************************/
typedef struct _tPbbTaskInfo {
 tOsixSemId                PbbSemId;
 tPbbPoolInfo              PbbPoolIds;
 tOsixTaskId               PbbTaskId;
 tOsixQId                  PbbCfgQId;

    /* Scalability parameters start */
 UINT4                     u4MaxIsid;
 UINT4                     u4MaxNumIsidPerContext;
 UINT4                     u4MaxPortperIsid;/*Maximum number of port per isid */
 UINT4                     u4MaxPortperIsidPerContext;/*Maximum number of port per isid per context */

 UINT4                     u4NumberofIcomp;
 UINT4                     u4NumberofBcomp;
    UINT4                     u4NumOfIsidInstance;/*Number of Isid Instance currently configured in system*/    
 tMacAddr                  BridgeMacAddr;
 UINT1                     au1OUI[PBB_OUI_LENGTH];/*GLobal OUI*/
 UINT1                     au1BridgeName[PBB_BRIDGE_NAME_LEN + 1];/*Bridge Name*/
    UINT1                     au1Reserved[2];
} tPbbTaskInfo;

#ifdef L2RED_WANTED 
/* tPbbRmData - Used to enqueue message to RM */
typedef struct _tPbbRmData {
    UINT4         u4Events;
    /* VOID should be made as tPeerId */
    VOID         *PeerId;
    tRmMsg       *pRmMsg;
    UINT2         u2DataLen;
    UINT1         au1Reserved[2];
}tPbbRmData;
#endif /* L2RED_WANTED */


/***************************************************************************/
/* PBB Q Message */  
/***************************************************************************/
typedef struct _tPbbQMsg {
   UINT4         u4DstPort;
   UINT4         u4ContextId; /*context */    
   INT4          i4IfIndex;
   UINT2         u2MsgType;
   UINT2         u2LocalPort; /* this is used when create port is called */
   UINT1         u1PortStatus;
   UINT1         u1CompType; /* this is used when create port is called */
   UINT1         au1Reserved[2];
   UINT4         u4VlanAudStatus; 
#ifdef MBSM_WANTED
   struct MbsmIndication {
          tMbsmProtoMsg *pMbsmProtoMsg;
          }MbsmCardUpdate;
#endif  /* End of MBSM flag */
#ifdef L2RED_WANTED
   tPbbRmData           RmData;
#endif /* L2RED_WANTED */
}tPbbQMsg;

/* PCP decoding table entry */
typedef struct _tPbbPcpDecVal
{
   UINT1   u1Priority;      /* Priority associated with the 
                               incoming frame */
   UINT1   u1DropEligible;  /* Drop eligible value associated with the 
                               incoming frame. VLAN_DE_TRUE/VLAN_DE_FALSE  */
   UINT1   au1Reserved[2]; 
}tPbbPcpDecVal;

/* PCP entry */
typedef struct _tPbbPcpEntry
{
    /* Decoding Table */ /* 64 */
    tPbbPcpDecVal                   au1PcpDecoding[PBB_MAX_NUM_PCP_SEL_ROW]
                                                  [PBB_MAX_NUM_PCP];
    /* Encoding Table */ /* 64 */
    UINT1                           au1PcpEncoding[PBB_MAX_NUM_PCP_SEL_ROW]
                                                  [PBB_MAX_NUM_PRIORITY]
                                                  [PBB_MAX_NUM_DROP_ELIGIBLE];
    UINT1                           u1PcpSelRow;
    UINT1                           u1UseDei; /* PBB_SNMP_TRUE/PBB_SNMP_FALSE*/
    UINT1                           u1ReqDropEncoding;/* PBB_SNMP_TRUE/PBB_SNMP_FALSE*/
    UINT1                           au1Reserved[1];
}tPbbPcpData; /* 132*/

/*PIP data*/
typedef struct tPbbPipData{

    tPBB_DLL        VipList; /* DLL of the Vip's associated with port */ /*16*/
    UINT4           *pu4PcpData; /* Pcp tables */
    UINT1           u1PipRowStatus;
    UINT1           au1Reserved[3];
} tPbbPipData ; /*24*/

/*CNP data*/
typedef struct tPbbCnpData{
    UINT4                          u4Pisid;/*PISID associated with port*/ /*4*/
    UINT1                          u1PisidRowStatus;/*PISID associated with port*/ /*1*/
    UINT1                          au1Reserved[3];
} tPbbCnpData ; /* 8 */


/*CBP data*/
typedef struct tPbbCbpData{
    tPBB_DLL                       IsidList; /* DLL of the Isid's associated with port */ /*16*/
    UINT1                          u1CBPTableRowstatus; /*1*/
    UINT1                          au1Reserved[3];

}tPbbCbpData; /*20*/

/*I Comp data of vip_isid info*/
typedef struct tPbbICompData{
    tPBB_DLL                       CnpPortTableDll;/* DLL list to maintain CNP Port List information */ /*16*/
    UINT2                          u2Vip;/*Value of VIP*/ /*4*/
    UINT2                          u2Pip;/* PIP port associated per Isid*/ /*2*/
    UINT1                          u1VipPipRowstatus; /*1*/
    UINT1                          u1VipType;/*VIP Type*/ /*1*/
    UINT1                          u1PipStorageType; /*1*/
    UINT1                          u1VipPortStatus;
    UINT1                          u1VipRowstatus; /*1*/
    UINT1                          au1Reserved[3];

}tPbbICompData; /*12*/

/*B Comp data of vip_isid info*/
typedef struct tPbbBCompData{
    tPBB_DLL                       CbpPortTableDll;/* DLL list to maintain CBP Port List information */ /*16*/
}tPbbBCompData; /*16*/

/*CNP List*/
typedef struct{
 tPBB_DLL_NODE                  link;
    UINT2                          u2CnpPort;/*Value of Isid*/
    UINT1                          au1Reserved[2];
}tPbbCnpPortList; /* 8 */


/*CBP List*/
typedef struct{
 tPBB_DLL_NODE                  link;
    UINT2                          u2CBPPort;/*Value of CBP Port*/
    UINT1                          au1Reserved[2];
}tPbbCbpPortList; /* 8 */

/* Isid or Pip Vip Map Node */
typedef struct{
 tPBB_DLL_NODE                  link;
    union
    {
        UINT4                          u4Isid;/*Value of Isid or Vip */
        UINT2                          u2Vip;
    }unIsidVip;
}tPbbIsidPipVipNode;/* 8 */



/***************************************************************************/
/*ISID VIP related data structure*/
/***************************************************************************/
typedef struct {
    /* I and B Comp data */
    UINT4                          u4Isid;/*Value of IComponent per ISID*/ /*4*/
    UINT4                          u4NumPortsPerIsidPerContextPresent;
    tMacAddr                       DefBackDestMac;/* Default Destination Mac 
                                                   * addrees associated with
                                                   * ISID*/ /*6*/
    UINT1                          au1Reserved[2];
    union {
        tPbbICompData                       ICompData; /* data associatd with an I Component */ /*12*/
        tPbbBCompData                       BCompData; /* data associatd with a B Component */ /*16*/
    }unPbbCompData;
}tPbbIsidVipInfo;



/***************************************************************************/
/* VIP RB tree structure Index is vip*/
/* Applicable only for I component */
/***************************************************************************/
typedef struct{
 tRBNodeEmbd                  RBNode;/*Root of RB tree node*/
 UINT4                          *pu4PbbIsidVipInfo;/*Pointer to VIP/ISID buffer*/
    UINT2                          u2Vip;/*Value of VIP*/
    UINT1                          au1Reserved[2];
}tPbbICompVipRBtreeNode;/* 24 */

/***************************************************************************/
/* Isid RB tree structure Index is Isid*/
/* Applicable for both I and B component */
/***************************************************************************/
typedef struct{
 tRBNodeEmbd                  RBNode;/*Root of RB tree node*/
    UINT4                          u4Isid;/*Value of ISID*/
 UINT4                          *pu4PbbIsidVipInfo;/*Pointer to VIP/ISIDbuffer*/
}tPbbIsidRBtreeNode; /* 24 */


/***************************************************************************/
/*Port RB tree structure Index is Port*/
/***************************************************************************/
typedef struct tPbbPortRBtreeNode{
    tRBNodeEmbd                    RBNode;/*Root of RB tree node*/
    UINT4                          u4IfIndex;  /* Port Number*/
    UINT2                          u2LocalPort; /*local port*/
    UINT1                          u1PortType; /* Port type*/
    UINT1                          u1PortStatus; /* Port type*/

    union {
        /* PIP data */
        tPbbPipData                   PipData; 
        /* CBP data */
        tPbbCbpData                   CbpData; 
        tPbbCnpData                   CnpData; 
    }unPbbPortData;
}tPbbPortRBtreeNode; /* 48 */


/***************************************************************************/
/* ISID + Port RB tree structure Index is ISID+Port*/
/* Applicable only for B Component */
/***************************************************************************/
typedef struct{
 tRBNodeEmbd                  RBNode;
 UINT4                          u4Isid;  /* ISID */
 UINT4                          u4LocalIsid;/* Translate Isid / Local Isid*/
    tMacAddr                  DefBackDestMac;/* Default Destination Mac addr */
 tVlanId                        u2BVlanId;/*Bvlan ID*/
    UINT2                          u2LocalPort;/* Port number*/
    UINT1                          u1OUIRowstatus;
    UINT1                          u1CBPRowstatus;
    UINT1                     au1IsidPortOUI[PBB_OUI_LENGTH];/*OUI value associated with a ISID*/
 UINT1                          u1CbpMappingType;
}tPbbBackBoneSerInstEntry; /* 40 */


/***************************************************************************/
/* PBB Context Info */
/***************************************************************************/
typedef struct PbbContextInfo{
 tRBTree                   IsidTable;/*RB tree for ISID  */
 tRBTree                   IsidPortTable;/*RB tree for ISID and port*/
 tRBTree                   PortTable;/*RB tree for port table*/
 tRBTree                   VipTable;/*RB tree for port table*/
 UINT4                     u4ContextId;/*Context id */
    UINT4                     u4InstanceId;/* Associated Instance */
 UINT4                     u4NumIsidPerContextPresent;
    UINT4                     u4PbbDbg;
    UINT4                     u4BridgeMode;
    UINT2                     u2InternalCbpCount;
    UINT2                     u2PisidCount;
    UINT1                     au1ContextStr[PBB_CONTEXT_ALIAS_LEN + 1];
    UINT1                     au1Reserved[3];
}tPbbContextInfo;

/*Context Node*/
typedef struct{
 tPBB_DLL_NODE                  link;
    UINT4                          u4ContextId;/*Value of Context*/
}tPbbContextNode; /* 8 */


/***************************************************************************/
/* PBB Instance Info */
/***************************************************************************/
typedef struct PbbInstanceInfo{
    tPBB_DLL                  ContextDll;/* DLL maintain Contexts that are */
                                         /*mapped to this instance */ /*16*/
 UINT4                     u4InstanceId; /* Instance Id */
 UINT4                     u4InstanceNumOfIcomp;
 UINT4                     u4InstanceNumOfBcomp;
    UINT1                     au1InstanceStr[PBB_INSTANCE_ALIAS_LEN + 1]; 
    tMacAddr                  InstanceMacAddr; /* Instance Mac Address */ 
    UINT1                     u1InstanceRowStatus; /* Instance table row status */
    UINT1                     u1MacAddressType; /* Mac address type is Derived
                                                   from NVRAM or user configured */
    UINT1                     au1Reserved[3];
}tPbbInstanceInfo;

typedef tNpSyncBufferEntry tPbbBufferEntry;

/* Sizing Params enum */
enum
{
   PBB_CFG_Q_MSG_SIZING_ID,
   PBB_CONTEXT_INFO_SIZING_ID,
   PBB_ISID_INFO_SIZING_ID,
   PBB_BACKBONE_SERV_INFO_SIZING_ID,
   PBB_PORT_INFO_SIZING_ID,
   PBB_ICOMP_VIP_NODE_SIZING_ID,
   PBB_ISID_VIP_INFO_SIZING_ID,
   PBB_CBP_PORT_INFO_SIZING_ID,
   PBB_CNP_PORT_INFO_SIZING_ID,
   PBB_ISID_PIP_VIP_NODE_SIZING_ID,
   PBB_PIP_PCP_INFO_SIZING_ID,
   PBB_INSTANCE_INFO_SIZING_ID,
   PBB_CONTEXT_NODE_SIZING_ID,
   PBB_RED_BUF_ENTRIES_SIZING_ID, 
   PBB_NO_SIZING_ID
};
#endif
