/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbbcons.h,v 1.11 2013/05/06 11:56:02 siva Exp $
 *
 * Description: This file contains constant definitions used in 
 *              PBB Module.
 *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2008 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2002                                          */
/*                                                                           */
/*  FILE NAME             : pbbcons.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : PBB                                              */
/*  MODULE NAME           : PBB                                              */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 30 Jul 2008                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains constant definitions used in  */
/*                          PBB Module.                                      */
/*                                                                           */
/*****************************************************************************/
/*                                                                           */
/*  Change History                                                           */
/*  Version               : 1.0                                              */
/*  Date(DD/MM/YYYY)      : 30/07/2008                                       */
/*  Modified by           : PBB TEAM                                         */
/*  Description of change : Created Original                                 */
/*****************************************************************************/

#ifndef _PBBCONS_H
#define _PBBCONS_H

#define PBB_INIT_VAL                  0
#define PBB_SIZE_OF_MAC_ADDR          5

#define PBB_PORT_CREATE_MSG              1
#define PBB_PORT_DELETE_MSG              2

/* MI related messages */
#define PBB_CREATE_CONTEXT_MSG           3
#define PBB_DELETE_CONTEXT_MSG           4
#define PBB_UPDATE_CONTEXT_NAME          5
#define PBB_SET_COMP_TYPE_MSG            6
#define PBB_COPY_PORT_INFO_MSG           7
#define PBB_REMOVE_PORT_INFO_MSG         8
#define PBB_UPDATE_PORT_STATUS           9
#define PBB_MAP_PORT_INDICATION          10
#define PBB_UNMAP_PORT_INDICATION        11
#define PBB_RM_MSG                       12
#define PBB_VLAN_AUDIT_STATUS_IND        13

#define PBB_MAX_CONTEXTS               FsPBBSizingParams[MAX_PBB_CONTEXT_INFO_SIZING_ID].u4PreAllocatedUnits
#define PBB_MAX_INSTANCES              FsPBBSizingParams[MAX_PBB_CONTEXTS_SIZING_ID].u4PreAllocatedUnits

#define PBB_INVALID_INSTANCE             (PBB_MAX_INSTANCES+ 1)
#define PBB_EMPTY_STRING                 ""
#define PBB_NO_ERROR CLI_PBB_MAX_ERR

#define PBB_IFPORT_LIST_SIZE  BRG_PORT_LIST_SIZE
#define PBB_PORT_LIST_SIZE    CONTEXT_PORT_LIST_SIZE
#define PBB_PORTS_PER_BYTE         8
#define PBB_CONTEXTS_PER_BYTE         8
#define PBB_CLI_MAX_MAC_STRING_SIZE        21

#define PBB_MAX_PORTS              L2IWF_MAX_PORTS_PER_CONTEXT

/*L2RED Wanted*/
#define PBB_RED_BUF_ENTRIES        55
#define PBB_RED_BUF_MEMPOOL()  (gPbbGlobData.PbbTaskInfo.PbbPoolIds.\
                                PbbRedBuffMemPoolId)
/* PBB bit set/reset */
#define PBB_BIT_RESET 0
#define PBB_BIT_SET   1

#define PBB_TRUE            1
#define PBB_FALSE           0

#define PBB_MIN_ISID_VALUE  255
#define PBB_MAX_ISID_VALUE  16777214

#define PBB_MIN_ISID  256

/* PBB 8 bit */
#define PBB_BIT8 0x80

/* Default Port Value */
#define PBB_DEFAULT_PORT_VAL 0


#define PBB_ISID_INVALID_VALUE       1

#define PBB_CONTEXT_ALIAS_LEN  L2IWF_CONTEXT_ALIAS_LEN

#define PBB_SWITCH_ALIAS_LEN      VCM_ALIAS_MAX_LEN
/*PBB enable or disable variable*/
#define PBB_ENABLED                   1
#define PBB_DISABLED                  2


/* PBB storage types */
#define PBB_STORAGE_TYPE_OTHER         1
#define PBB_STORAGE_TYPE_VOLATILE      2
#define PBB_STORAGE_TYPE_NONVOLATILE   3
#define PBB_STORAGE_TYPE_PERMANAENT    4
#define PBB_STORAGE_TYPE_READONLY      5
#define PBB_INVALID_STORAGE_TYPE       0


/*Bridge Name length*/
#define  PBB_BRIDGE_NAME_LEN 32

#define PBB_MAX_BRIDGE_NAME          32
#define PBB_MAX_BVLAN_VALUE          4094
 

#define PBB_GREATER        1            
#define PBB_EQUAL          0            
#define PBB_LESSER        -1

#define PBB_VALID_SERVICE_MAP_LENGTH 1
#define PBB_MAC_ADDR_LEN   6

#ifdef NPAPI_WANTED

#define PBB_COPY_TO_CPU             3
#define PBB_SWITCH                  2
#define PBB_DROP                    1

#define PBB_ECFM_LLC_CFM_TYPE               0x8902          
#define PBB_ECFM_LLC_CFM_TYPE_OFFSET_1      12
#define PBB_ECFM_LLC_CFM_TYPE_OFFSET_2      16
#define PBB_ECFM_LLC_CFM_TYPE_OFFSET_3      30
#define PBB_ECFM_LLC_CFM_TYPE_OFFSET_4      34
#define PBB_ECFM_LLC_CFM_TYPE_OFFSET_5      38
#define PBB_AST_ICOMP_OFFSET                18
#define PBB_AST_BCOMP_OFFSET                22


#endif

#define PBB_P2P_FORCETRUE              0
#define PBB_P2P_FORCEFALSE             1
#define PBB_P2P_AUTO                   2

#define PBB_DEF_UNICAST_LEARNING_LIMIT \
        (VLAN_DEV_MAX_L2_TABLE_SIZE - VLAN_DEV_MAX_ST_UCAST_ENTRIES)

#endif
