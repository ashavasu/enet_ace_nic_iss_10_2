/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pbtprot.h,v 1.8 2011/09/12 06:42:20 siva Exp $
*
* Description: <File description>
*
*******************************************************************/
/* pbttesid.c */
INT4 
PbbTeTeSidEspEntryGet PROTO ((UINT4 u4TeSid ,UINT4 u4EspIndex, 
         tEspInfo *pEspInfo));

INT4 
PbbTeTeSidEspEntryGetNext PROTO ((UINT4 u4TeSid ,UINT4 u4EspIndex, 
      tEspInfo *pEspInfo));
INT4 
PbbTeTeSidEspEntryCreate PROTO ((UINT4 u4TeSid ,UINT4 * pu4EspIndex));

INT4 
PbbTeTeSidEspEntryDel PROTO ((UINT4 u4TeSid ,UINT4 u4EspIndex));

INT4
PbbTeTeSidEspEntryDelAllInCtxt PROTO ((UINT4 u4ContextId));

INT4
PbbTeTeSidRowStatusSet PROTO ((UINT4 u4TeSid ,UINT4 u4EspIndex, 
          UINT1 u1RowStatus));

INT4
PbbTeTeSidStorageTypeSet PROTO ((UINT4 u4TeSid ,UINT4 u4EspIndex, 
     UINT1 u1StorageType));

INT4
PbbTeTeSidEspSet PROTO ((UINT4 u4TeSid ,UINT4 u4EspIndex,tMacAddr DestMac, 
    tMacAddr SrcMac, tVlanId VlanId));
INT4
PbbTeTeSidContextIdSet PROTO ((UINT4 u4TeSid ,UINT4 u4EspIndex, 
          UINT4 u4ContextId));
INT4
PbbTeTeSidAnyTeSidEntryInEspVlan PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4
PbbTeTeSidGetESPByTeSid PROTO ((UINT4 u4TeSid, tMacAddr DestMac, 
    tMacAddr SrcMac, tVlanId EspVid,
    tEspInfo *pEspInfo,
                                tEspDMacVIDInfo * pEspDMacVIDInfo));
INT4
PbbTeTeSidGetESPInfoByTeSid PROTO ((tEspInfo * pEspInfo));

INT4
PbbTeDMacVIDConfigEspStatus PROTO ((UINT4 u4ContextId, tMacAddr DestAddr,
        tVlanId VlanId, UINT1 u1EspStatus));
INT4
PbbTeDMacVIDGetEspInfo PROTO ((UINT4 u4ContextId, tMacAddr DestAddr,
                               tVlanId VlanId, 
                               tEspDMacVIDInfo * pRetEspDMacVIDInfo));

INT4
PbbTeTeSidUtlGetFreeEspIndex PROTO ((UINT4 u4TeSidId, UINT4 * pu4EspIndex));

VOID
PbbTeTeSidUtlEspEntryCpy PROTO ((tTeSidEspEntry *pTeSidEspEntry, 
     tEspInfo *pEspInfo));

tEspDMacVIDInfo *
PbbTeTeSidUtlDMacVIDInfoMove PROTO ((UINT4 u4ContextId, tEspDMacVIDInfo  
         *pEspDMacVIDInfo));

tEspDMacVIDInfo *
PbbTeTeSidUtlDMacVIDInfoAdd PROTO ((UINT4 u4ContextId, tMacAddr DestAddr, 
        tVlanId VlanId));

INT4
PbbTeTeSidUtlDMacVIDInfoDel PROTO ((UINT4 u4ContextId, 
        tEspDMacVIDInfo  *pEspDMacVIDInfo));

/* pbtutil.c */

INT4
PbbTeLock PROTO ((VOID));

INT4
PbbTeUnLock PROTO ((VOID));
   
VOID
PbbTeUtlIsNonMonitoredEspPresent PROTO ((UINT4 u4ContextId, tMacAddr DestAddr, 
                                         tVlanId VlanId, 
                                         UINT1 * pu1NonMonitoredEspPresent));
UINT4
PbbTeUtlGetHashIndex PROTO ((UINT4 u4ContextId,tMacAddr DestAddr, 
        tVlanId VlanId));

INT4
PbbTeUtlConfigureEsp PROTO ((tEspPathInfo * pEspPathInfo, UINT1 u1Action));
/* pbtsys.c */
INT4
PbbTeSysModuleStartInCtxt PROTO ((UINT4 u4ContextId));

INT4
PbbTeSysModuleShutInCtxt PROTO ((UINT4 u4ContextId));


INT4
PbbTeSysCreateEsp PROTO ((tEspPathInfo *pEspPathInfo));

INT4
PbbTeSysDeleteEsp PROTO ((tEspPathInfo *pEspPathInfo));

INT4
PbbTeSysDeleteAllEspVlans PROTO ((UINT4 u4ContextId));

INT4
PbbTeSysConfigMcastEntry PROTO ((UINT4 u4ContextId, tMacAddr DestMac, 
     tVlanId EspVlanId, 
     tPortList EgressIfIndexList,
     UINT1 u1Action));
INT4
PbbTeSysConfigUcastEntry PROTO ((UINT4 u4ContextId, tMacAddr DestMac, 
     tVlanId EspVlanId, UINT4 u4EgressIfIndex,
     UINT1 u1Action));

VOID
PbbTeProcessCfgEvent PROTO ((tPbbTeQMsg * pPbbTeQMsg));

INT4
PbbTeDisable PROTO ((UINT4 u4ContextId));

INT4
PbbTeTaskInit PROTO ((VOID));

INT4
PbbTeHandleStartModule PROTO ((VOID));

VOID
PbbTeDeInit PROTO ((VOID));
/* pbtespvl.c */
INT4 
PbbTeEspVlanConfig PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
      eConfigEspVidStatus EspVidStatus));
INT4
PbbTeEspVlanGetNext PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
       tVlanId * pNextVlanId));
VOID
PbbTeEspVlanListGet PROTO ((UINT4 u4Context, UINT1 * au1VlanList));

VOID
PbbTeEspVlanListReset PROTO ((UINT4 u4Context));

/* pbtport.c */

INT4
PbbTeVlanConfigStaticUcastEntry PROTO ((tConfigStUcastInfo 
     *pStaticUnicastInfo, 
     UINT1 u1Action,
     UINT1 * pu1ErrorCode));

INT4
PbbTeVlanConfigStaticMcastEntry PROTO ((tConfigStMcastInfo 
     *pStaticMulticastInfo, 
     UINT1 u1Action,
     UINT1 *pu1ErrorCode));
INT4
PbbTeVlanDelStFiltEntStatusOther PROTO ((UINT4 u4ContextId, tVlanId VlanId,
      UINT1 *pu1ErrorCode));

INT4
PbbTeVlanGetStaticMulticastEntry PROTO ((tConfigStMcastInfo
      *pStaticMulticastInfo,
      UINT1 *pu1ErrorCode));

INT4
PbbTeVlanGetStaticUnicastEntry PROTO ((tConfigStUcastInfo
           *pStaticUnicastInfo,
           UINT1 *pu1ErrorCode));
INT4
PbbTeVlanCfgLearningFlooding PROTO ((UINT4 u4ContextId, tVlanId VlanId,
         UINT1 u1Action, UINT1 *pu1ErrorCode));
INT4
PbbTeMstpMapVidToMstId PROTO ((UINT4 u4ContextId, tVlanId VlanId, 
          UINT2 u2MstId));
BOOL1
PbbTeIsVlanActive PROTO ((UINT4 u4ContextId, tVlanId VlanId));

INT4
PbbTeVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum);

INT4
PbbTeRmSetBulkUpdatesStatus (UINT4 u4AppId);

UINT4
PbbTeRmReleaseMemoryForMsg (UINT1 *pu1Block);

UINT4
PbbTeRmDeRegisterProtocols (VOID);

UINT4
PbbTeRmRegisterProtocols (tRmRegParams * pRmReg);

UINT1
PbbTeRmHandleProtocolEvent (tRmProtoEvt * pEvt);

UINT1
PbbTeRmGetStaticConfigStatus (VOID);

UINT1
PbbTeRmGetStandbyNodeCount (VOID);

UINT4
PbbTeRmGetNodeState (VOID);

UINT4
PbbTeRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                           UINT4 u4DestEntId);

/* pbtutil.c */
INT4
PbbTeTeSidTableCmp PROTO ((tRBElem * pRBElem1, tRBElem * pRBElem2));

INT4
PbbTeUnlock PROTO ((VOID));

/* pbtapi.c */


INT4
PbbTeVcmGetAliasName PROTO ((UINT4 u4ContextId, UINT1 *pu1Alias));
INT4
PbbTeCfaCliGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));

/* pbtred.c or pbtrdstb.c */
VOID 
PbbTeRedInitGlobalInfo (VOID);

INT4
PbbTeRedRegisterWithRm (VOID);

VOID
PbbTeRedHandleBulkUpdateEvent (VOID);

INT4
PbbTeRedSyncDynamicMonEsp PROTO ((tEspPathInfo *pEspPathInfo, UINT1 u1Action));

INT4
PbbTeRedSyncDynamicNonMonEsp PROTO ((UINT4 u4ContextId, tMacAddr DestMac, tVlanId EspVlan, 
                               UINT1 u1Action));

INT4
PbbTeMrpPropVlanDelFromEsp PROTO ((UINT4 u4ContextId, tVlanId VlanId));
INT4
PbbTeMrpRemVlanAddedInEsp PROTO ((UINT4 u4ContextId, tVlanId VlanId));


VOID
PbbTeAssignMemPoolsId (VOID);
