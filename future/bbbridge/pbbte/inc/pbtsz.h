/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbtsz.h,v 1.3 2012/03/21 13:03:51 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/

enum {
    MAX_PBBTE_CONFIG_Q_MESG_SIZING_ID,
    MAX_PBBTE_CONTEXT_INFO_SIZING_ID,
    MAX_PBBTE_ESP_DMAC_VID_INFO_SIZING_ID,
    MAX_PBBTE_RED_BUFER_ENTRIES_SIZING_ID,
    MAX_PBBTE_SERVICE_ID_ESP_ENTRIES_SIZING_ID,
    PBBTE_MAX_SIZING_ID
};

#ifdef  _PBBTESZ_C
tMemPoolId PBBTEMemPoolIds[ PBBTE_MAX_SIZING_ID];
INT4  PbbteSizingMemCreateMemPools(VOID);
VOID  PbbteSizingMemDeleteMemPools(VOID);
INT4  PbbteSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _PBBTESZ_C  */
extern tMemPoolId PBBTEMemPoolIds[ ];
extern INT4  PbbteSizingMemCreateMemPools(VOID);
extern VOID  PbbteSizingMemDeleteMemPools(VOID);
#endif /*  _PBBTESZ_C  */


#ifdef  _PBBTESZ_C
tFsModSizingParams FsPBBTESizingParams [] = {
{ "tPbbTeQMsg", "MAX_PBBTE_CONFIG_Q_MESG", sizeof(tPbbTeQMsg),MAX_PBBTE_CONFIG_Q_MESG, MAX_PBBTE_CONFIG_Q_MESG,0 },
{ "tPbbTeContextInfo", "MAX_PBBTE_CONTEXT_INFO", sizeof(tPbbTeContextInfo),MAX_PBBTE_CONTEXT_INFO, MAX_PBBTE_CONTEXT_INFO,0 },
{ "tEspDMacVIDInfo", "MAX_PBBTE_ESP_DMAC_VID_INFO", sizeof(tEspDMacVIDInfo),MAX_PBBTE_ESP_DMAC_VID_INFO, MAX_PBBTE_ESP_DMAC_VID_INFO,0 },
{ "tPbbTeBufferEntry", "MAX_PBBTE_RED_BUFER_ENTRIES", sizeof(tPbbTeBufferEntry),MAX_PBBTE_RED_BUFER_ENTRIES, MAX_PBBTE_RED_BUFER_ENTRIES,0 },
{ "tTeSidEspEntry", "MAX_PBBTE_SERVICE_ID_ESP_ENTRIES", sizeof(tTeSidEspEntry),MAX_PBBTE_SERVICE_ID_ESP_ENTRIES, MAX_PBBTE_SERVICE_ID_ESP_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _PBBTESZ_C  */
extern tFsModSizingParams FsPBBTESizingParams [];
#endif /*  _PBBTESZ_C  */


