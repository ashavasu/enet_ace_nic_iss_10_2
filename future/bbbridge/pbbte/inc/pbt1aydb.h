/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pbt1aydb.h,v 1.1.1.1 2008/10/29 09:46:06 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FS1AYDB_H
#define _FS1AYDB_H

UINT1 Ieee8021PbbTeProtectionGroupListTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021PbbTeMASharedGroupTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021PbbTeTeSidTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021PbbTeProtectionGroupConfigTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021PbbTeProtectionGroupISidTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fs1ay [] ={1,3,6,1,4,1,29601,2,12};
tSNMP_OID_TYPE fs1ayOID = {9, fs1ay};


UINT4 Ieee8021PbbTeProtectionGroupListGroupId [ ] ={1,3,6,1,4,1,29601,2,12,1,1,1,1};
UINT4 Ieee8021PbbTeProtectionGroupListMode [ ] ={1,3,6,1,4,1,29601,2,12,1,1,1,2};
UINT4 Ieee8021PbbTeProtectionGroupListWorkingMA [ ] ={1,3,6,1,4,1,29601,2,12,1,1,1,3};
UINT4 Ieee8021PbbTeProtectionGroupListProtectionMA [ ] ={1,3,6,1,4,1,29601,2,12,1,1,1,4};
UINT4 Ieee8021PbbTeProtectionGroupListStorageType [ ] ={1,3,6,1,4,1,29601,2,12,1,1,1,5};
UINT4 Ieee8021PbbTeProtectionGroupListRowStatus [ ] ={1,3,6,1,4,1,29601,2,12,1,1,1,6};
UINT4 Ieee8021PbbTeMASharedGroupSubIndex [ ] ={1,3,6,1,4,1,29601,2,12,1,2,1,1};
UINT4 Ieee8021PbbTeMASharedGroupId [ ] ={1,3,6,1,4,1,29601,2,12,1,2,1,2};
UINT4 Ieee8021PbbTeTeSidId [ ] ={1,3,6,1,4,1,29601,2,12,1,3,1,1};
UINT4 Ieee8021PbbTeTeSidEspIndex [ ] ={1,3,6,1,4,1,29601,2,12,1,3,1,2};
UINT4 Ieee8021PbbTeTeSidEsp [ ] ={1,3,6,1,4,1,29601,2,12,1,3,1,3};
UINT4 Ieee8021PbbTeTeSidStorageType [ ] ={1,3,6,1,4,1,29601,2,12,1,3,1,4};
UINT4 Ieee8021PbbTeTeSidRowStatus [ ] ={1,3,6,1,4,1,29601,2,12,1,3,1,5};
UINT4 Ieee8021PbbTeProtectionGroupConfigState [ ] ={1,3,6,1,4,1,29601,2,12,1,4,1,1};
UINT4 Ieee8021PbbTeProtectionGroupConfigCommandStatus [ ] ={1,3,6,1,4,1,29601,2,12,1,4,1,2};
UINT4 Ieee8021PbbTeProtectionGroupConfigCommandAdmin [ ] ={1,3,6,1,4,1,29601,2,12,1,4,1,3};
UINT4 Ieee8021PbbTeProtectionGroupConfigWTR [ ] ={1,3,6,1,4,1,29601,2,12,1,4,1,4};
UINT4 Ieee8021PbbTeProtectionGroupConfigHoldOff [ ] ={1,3,6,1,4,1,29601,2,12,1,4,1,5};
UINT4 Ieee8021PbbTeProtectionGroupConfigStorageType [ ] ={1,3,6,1,4,1,29601,2,12,1,4,1,6};
UINT4 Ieee8021PbbTeProtectionGroupISidIndex [ ] ={1,3,6,1,4,1,29601,2,12,1,5,1,1};
UINT4 Ieee8021PbbTeProtectionGroupISidGroupId [ ] ={1,3,6,1,4,1,29601,2,12,1,5,1,2};
UINT4 Ieee8021PbbTeProtectionGroupISidStorageType [ ] ={1,3,6,1,4,1,29601,2,12,1,5,1,3};
UINT4 Ieee8021PbbTeProtectionGroupISidRowStatus [ ] ={1,3,6,1,4,1,29601,2,12,1,5,1,4};
UINT4 Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex [ ] ={1,3,6,1,4,1,29601,2,12,1,6,1,1};
UINT4 Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts [ ] ={1,3,6,1,4,1,29601,2,12,1,6,1,2};
UINT4 Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts [ ] ={1,3,6,1,4,1,29601,2,12,1,6,1,3};
UINT4 Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageType [ ] ={1,3,6,1,4,1,29601,2,12,1,6,1,4};
UINT4 Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus [ ] ={1,3,6,1,4,1,29601,2,12,1,6,1,5};


tMbDbEntry fs1ayMibEntry[]= {

{{13,Ieee8021PbbTeProtectionGroupListGroupId}, GetNextIndexIeee8021PbbTeProtectionGroupListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021PbbTeProtectionGroupListTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupListMode}, GetNextIndexIeee8021PbbTeProtectionGroupListTable, Ieee8021PbbTeProtectionGroupListModeGet, Ieee8021PbbTeProtectionGroupListModeSet, Ieee8021PbbTeProtectionGroupListModeTest, Ieee8021PbbTeProtectionGroupListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupListTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupListWorkingMA}, GetNextIndexIeee8021PbbTeProtectionGroupListTable, Ieee8021PbbTeProtectionGroupListWorkingMAGet, Ieee8021PbbTeProtectionGroupListWorkingMASet, Ieee8021PbbTeProtectionGroupListWorkingMATest, Ieee8021PbbTeProtectionGroupListTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupListTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupListProtectionMA}, GetNextIndexIeee8021PbbTeProtectionGroupListTable, Ieee8021PbbTeProtectionGroupListProtectionMAGet, Ieee8021PbbTeProtectionGroupListProtectionMASet, Ieee8021PbbTeProtectionGroupListProtectionMATest, Ieee8021PbbTeProtectionGroupListTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupListTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupListStorageType}, GetNextIndexIeee8021PbbTeProtectionGroupListTable, Ieee8021PbbTeProtectionGroupListStorageTypeGet, Ieee8021PbbTeProtectionGroupListStorageTypeSet, Ieee8021PbbTeProtectionGroupListStorageTypeTest, Ieee8021PbbTeProtectionGroupListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupListTableINDEX, 2, 0, 0, "3"},

{{13,Ieee8021PbbTeProtectionGroupListRowStatus}, GetNextIndexIeee8021PbbTeProtectionGroupListTable, Ieee8021PbbTeProtectionGroupListRowStatusGet, Ieee8021PbbTeProtectionGroupListRowStatusSet, Ieee8021PbbTeProtectionGroupListRowStatusTest, Ieee8021PbbTeProtectionGroupListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupListTableINDEX, 2, 0, 1, NULL},

{{13,Ieee8021PbbTeMASharedGroupSubIndex}, GetNextIndexIeee8021PbbTeMASharedGroupTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021PbbTeMASharedGroupTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeMASharedGroupId}, GetNextIndexIeee8021PbbTeMASharedGroupTable, Ieee8021PbbTeMASharedGroupIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Ieee8021PbbTeMASharedGroupTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeTeSidId}, GetNextIndexIeee8021PbbTeTeSidTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021PbbTeTeSidTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeTeSidEspIndex}, GetNextIndexIeee8021PbbTeTeSidTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021PbbTeTeSidTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeTeSidEsp}, GetNextIndexIeee8021PbbTeTeSidTable, Ieee8021PbbTeTeSidEspGet, Ieee8021PbbTeTeSidEspSet, Ieee8021PbbTeTeSidEspTest, Ieee8021PbbTeTeSidTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021PbbTeTeSidTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeTeSidStorageType}, GetNextIndexIeee8021PbbTeTeSidTable, Ieee8021PbbTeTeSidStorageTypeGet, Ieee8021PbbTeTeSidStorageTypeSet, Ieee8021PbbTeTeSidStorageTypeTest, Ieee8021PbbTeTeSidTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeTeSidTableINDEX, 2, 0, 0, "3"},

{{13,Ieee8021PbbTeTeSidRowStatus}, GetNextIndexIeee8021PbbTeTeSidTable, Ieee8021PbbTeTeSidRowStatusGet, Ieee8021PbbTeTeSidRowStatusSet, Ieee8021PbbTeTeSidRowStatusTest, Ieee8021PbbTeTeSidTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeTeSidTableINDEX, 2, 0, 1, NULL},

{{13,Ieee8021PbbTeProtectionGroupConfigState}, GetNextIndexIeee8021PbbTeProtectionGroupConfigTable, Ieee8021PbbTeProtectionGroupConfigStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021PbbTeProtectionGroupConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupConfigCommandStatus}, GetNextIndexIeee8021PbbTeProtectionGroupConfigTable, Ieee8021PbbTeProtectionGroupConfigCommandStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Ieee8021PbbTeProtectionGroupConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupConfigCommandAdmin}, GetNextIndexIeee8021PbbTeProtectionGroupConfigTable, Ieee8021PbbTeProtectionGroupConfigCommandAdminGet, Ieee8021PbbTeProtectionGroupConfigCommandAdminSet, Ieee8021PbbTeProtectionGroupConfigCommandAdminTest, Ieee8021PbbTeProtectionGroupConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupConfigWTR}, GetNextIndexIeee8021PbbTeProtectionGroupConfigTable, Ieee8021PbbTeProtectionGroupConfigWTRGet, Ieee8021PbbTeProtectionGroupConfigWTRSet, Ieee8021PbbTeProtectionGroupConfigWTRTest, Ieee8021PbbTeProtectionGroupConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupConfigTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupConfigHoldOff}, GetNextIndexIeee8021PbbTeProtectionGroupConfigTable, Ieee8021PbbTeProtectionGroupConfigHoldOffGet, Ieee8021PbbTeProtectionGroupConfigHoldOffSet, Ieee8021PbbTeProtectionGroupConfigHoldOffTest, Ieee8021PbbTeProtectionGroupConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupConfigTableINDEX, 2, 0, 0, "0"},

{{13,Ieee8021PbbTeProtectionGroupConfigStorageType}, GetNextIndexIeee8021PbbTeProtectionGroupConfigTable, Ieee8021PbbTeProtectionGroupConfigStorageTypeGet, Ieee8021PbbTeProtectionGroupConfigStorageTypeSet, Ieee8021PbbTeProtectionGroupConfigStorageTypeTest, Ieee8021PbbTeProtectionGroupConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupConfigTableINDEX, 2, 0, 0, "3"},

{{13,Ieee8021PbbTeProtectionGroupISidIndex}, GetNextIndexIeee8021PbbTeProtectionGroupISidTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021PbbTeProtectionGroupISidTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupISidGroupId}, GetNextIndexIeee8021PbbTeProtectionGroupISidTable, Ieee8021PbbTeProtectionGroupISidGroupIdGet, Ieee8021PbbTeProtectionGroupISidGroupIdSet, Ieee8021PbbTeProtectionGroupISidGroupIdTest, Ieee8021PbbTeProtectionGroupISidTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupISidTableINDEX, 1, 0, 0, NULL},

{{13,Ieee8021PbbTeProtectionGroupISidStorageType}, GetNextIndexIeee8021PbbTeProtectionGroupISidTable, Ieee8021PbbTeProtectionGroupISidStorageTypeGet, Ieee8021PbbTeProtectionGroupISidStorageTypeSet, Ieee8021PbbTeProtectionGroupISidStorageTypeTest, Ieee8021PbbTeProtectionGroupISidTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupISidTableINDEX, 1, 0, 0, "3"},

{{13,Ieee8021PbbTeProtectionGroupISidRowStatus}, GetNextIndexIeee8021PbbTeProtectionGroupISidTable, Ieee8021PbbTeProtectionGroupISidRowStatusGet, Ieee8021PbbTeProtectionGroupISidRowStatusSet, Ieee8021PbbTeProtectionGroupISidRowStatusTest, Ieee8021PbbTeProtectionGroupISidTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeProtectionGroupISidTableINDEX, 1, 0, 1, NULL},

{{13,Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex}, GetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts}, GetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable, Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPortsGet, Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPortsSet, Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPortsTest, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts}, GetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable, Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPortsGet, Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPortsSet, Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPortsTest, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableINDEX, 2, 0, 0, NULL},

{{13,Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageType}, GetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable, Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageTypeGet, Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageTypeSet, Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageTypeTest, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableINDEX, 2, 0, 0, "3"},

{{13,Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus}, GetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable, Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatusGet, Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatusSet, Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatusTest, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableINDEX, 2, 0, 1, NULL},
};
tMibData fs1ayEntry = { 28, fs1ayMibEntry };
#endif /* _FS1AYDB_H */

