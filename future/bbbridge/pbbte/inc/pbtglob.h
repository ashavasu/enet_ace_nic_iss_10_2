#ifndef _PBTGLOB_H
#define _PBTGLOB_H
#ifdef  _PBTSYS_C

#ifdef PBBTE_PERFORM_TRACE_WANTED
#include <sys/time.h>
#endif

tPbbTeGlobalInfo    gPbbTeGlobalInfo;
tEspPathInfo        gPbbTeEspPathInfo;
tMacAddr            gPbbTeBcastAddress = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
#ifdef NPAPI_WANTED
UINT1               gPbbTeNpSyncBlk = PBBTE_FALSE;
#endif

#ifdef PBBTE_PERFORM_TRACE_WANTED
FILE                *gPbbTePerformanceFp;
struct timeval      gStartTime;
struct timeval      gEndTime;
UINT4               gu4TimeTaken;
UINT4               gu4EspCnt;
UINT1               gau1PbbTePerfMonBuf[80];
#endif /* PBBTE_PERFORM_TRACE_WANTED End*/

UINT1               gu1IsPbbTeInitialised = PBBTE_FALSE;
UINT1               gau1PbbTeVlanBitMaskMap[PBBTE_PORTS_PER_BYTE] =
    { 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02};
#else /* PBTSYS_C */
extern tPbbTeGlobalInfo    gPbbTeGlobalInfo;
extern tEspPathInfo        gPbbTeEspPathInfo;
extern tMacAddr            gPbbTeBcastAddress;
#ifdef NPAPI_WANTED
extern UINT1               gPbbTeNpSyncBlk;
#endif

#ifdef PBBTE_PERFORM_TRACE_WANTED
extern FILE                *gPbbTePerformanceFp;
extern struct timeval      gStartTime;
extern struct timeval      gEndTime;
extern UINT4               gu4TimeTaken;
extern UINT4               gu4EspCnt;
extern UINT1               gau1PbbTePerfMonBuf[80];
#endif /* PBBTE_PERFORM_TRACE_WANTED End*/

extern UINT1       gu1IsPbbTeInitialised;
extern UINT1       gau1PbbTeVlanBitMaskMap[PBBTE_PORTS_PER_BYTE];
#endif
#endif
