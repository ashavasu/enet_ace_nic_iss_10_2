/********************************************************************
  * Copyright (C) Aricent Inc . All Rights Reserved
  *
  * $Id: pbttrc.h,v 1.6 2013/12/18 12:50:14 siva Exp $
  *
  * Description: This file contains data structures defined for
  *               PBBTE module.
  **********************************************************************/

#define  PBBTE_TRC_FLAG(CtxId)  gPbbTeGlobalInfo.au4ContextTraceOption[CtxId] 
#define  PBBTE_GBL_TRC_FLAG     gPbbTeGlobalInfo.u4TraceOption

#define  PBBTE_MOD_NAME            ((const char *)"PBBTE")

enum {
    PBBTE_TRC_MEMPOOL_CTX = 1,
    PBBTE_TRC_MEMPOOL_TESID,
    PBBTE_TRC_MEMPOOL_DMACVID,
    PBBTE_TRC_MEM_FAIL_CTX,
    PBBTE_TRC_MEM_FAIL_TESID,
    PBBTE_TRC_MEM_FAIL_DMACVIDNODE,
    PBBTE_TRC_MOD_START,
    PBBTE_TRC_MOD_ALREADY_START,
    PBBTE_TRC_MOD_START_FAIL,
    PBBTE_TRC_MOD_SHUT,
    PBBTE_TRC_MOD_ALREADY_SHUT,
    PBBTE_TRC_MOD_NOT_STARTED,
    PBBTE_TRC_ESP_VLAN,
    PBBTE_TRC_ESP_VLAN_RESET,
    PBBTE_TRC_ESP_VLAN_FAIL,
    PBBTE_TRC_ESP_VLAN_RECONFIG,
    PBBTE_TRC_RESET_ESP_VLAN_RECONFIG,
    PBBTE_TRC_ESP_VLAN_DEL_FAIL,
    PBBTE_TRC_GET_FREE_ESP_FAIL,
    PBTTE_TRC_GET_FREE_ESP,
    PBBTE_TRC_ST_MCAST_MODIFIED,
    PBBTE_TRC_ST_MCAST_MODIFIED_FAIL,
    PBBTE_TRC_ST_MCAST_CREATE,
    PBBTE_TRC_ST_MCAST_CREATE_FAIL,
    PBBTE_TRC_ST_UCAST_MODIFIED,
    PBBTE_TRC_ST_UCAST_MODIFIED_FAIL,
    PBBTE_TRC_ST_UCAST_CREATE,
    PBBTE_TRC_ST_UCAST_CREATE_FAIL,
    PBBTE_TRC_ST_UCAST_DELETE,
    PBBTE_TRC_ST_MCAST_DELETE,
    PBBTE_TRC_NON_MONITORED_CONFIG,
    PBBTE_TRC_NON_MONITORED_CONFIG_TESID_ESP,
    PBBTE_TRC_NON_MONITORED_RESET,
    PBBTE_TRC_ESP_VLAN_DEL_ALL,
    PBBTE_TRC_TESID_DEL_ALL,
    PBBTE_TRC_INVALID_VLAN,
    PBBTE_TRC_INVALID_CONTEXT,
    PBBTE_TRC_VLAN_NOTACTIVE,
    PBBTE_TRC_TESID_CRT,
    PBBTE_TRC_TESID_ESP_CRT,
    PBBTE_TRC_TESID_ESP_SET_FAIL,
    PBBTE_TRC_TESID_DEL,
    PBBTE_TRC_TESID_ESP_SET,
    PBBTE_TRC_TESID_ESP_DEL,
    PBBTE_TRC_TESID_CRT_FAIL,
    PBBTE_TRC_TESID_DEL_FAIL,
    PBBTE_TRC_TESID_DEL_API_FAIL,
    PBBTE_TRC_API_INVALID_SRC_MAC,
    PBBTE_TRC_API_INVALID_PORT,
    PBBTE_TRC_TESID_GET_FAIL,
    PBBTE_TRC_TESID_GET,
    PBBTE_TRC_TESID_GET_ESP_BY_TESID,
    PBBTE_TRC_TESID_STORAGETYPE_GET_FAIL,
    PBBTE_TRC_TESID_STORAGETYPE_SET_FAIL,
    PBBTE_TRC_TESID_CONTEXTID_SET_FAIL,
    PBBTE_TRC_TESID_SET,
    PBBTE_TRC_TESID_WRONG_ROWSTATUS,
    PBBTE_TRC_TESID_WRONG_ROWSTATUS_2,
    PBBTE_TRC_TESID_SAME_ROWSTATUS,
    PBBTE_TRC_TESID_ALREADY_MAPPED,
    PBBTE_TRC_TESID_NOT_FOUND,
    PBBTE_TRC_TESID_RBTREE_ADD_FAIL,
    PBBTE_TRC_TESID_RBTREE_GET_FAIL,
    PBBTE_TRC_TESID_RBTREE_DEL_FAIL,
    PBBTE_TRC_TESID_ROWSTATUS_CTX_FAIL,
    PBBTE_TRC_TESID_ROWSTATUS_VLAN_FAIL,
    PBBTE_TRC_DMAC_HASH_REM,
    PBBTE_TRC_HW_ESP_SET_FAIL,
    PBBTE_TRC_HW_ESP_RESET_FAIL,
    PBBTE_TRC_VLAN_FLOODING_STATUS_FAIL,
    PBBTE_TRC_TASK_FAIL,
    PBBTE_TRC_SEM_FAIL,
    PBBTE_TRC_Q_FAIL,
    PBBTE_TRC_Q_MEM_FAIL,
    PBBTE_TRC_RM_REG_FAIL,
    PBBTE_TRC_RM_BUF_NULL,
    PBBTE_TRC_Q_BUF_MEM_FAIL,
    PBBTE_TRC_Q_SEND_TO_FAIL,
    PBBTE_TRC_SEND_EVENT_FAIL,
    PBBTE_TRC_RED_ALR_ACTIVE,
    PBBTE_TRC_RED_IDLE_TO_ACTIVE,
    PBBTE_TRC_RED_IGN_STNDBY,
    PBBTE_TRC_RED_STANDBY_TO_ACT,
    PBBTE_TRC_RED_ALR_STNDBY,
    PBBTE_TRC_RED_ACT_TO_STNDBY,
    PBBTE_TRC_RED_INIT_HW_AUD,
    PBBTE_TRC_RED_STNDBY_DOWN,
    PBBTE_TRC_RED_IDLE_TO_STNDBY,
    PBBTE_TRC_RED_INIT_BULK_REQ,
    PBBTE_TRC_RED_ENB_FAIL,
    PBBTE_TRC_RED_DISABLE_FAIL,
    PBBTE_TRC_SPWN_TSK_FAIL,
    PBBTE_TRC_RED_BULK_TAIL_RECVD,
    PBBTE_TRC_RED_MSG_AT_ACTIVE,
    PBBTE_TRC_RED_DYN_UPD_INC_LEN_FAIL,
    PBBTE_TRC_RED_DYN_MSG_FAIL,
    PBBTE_TRC_RED_NO_STNDBY,
    PBBTE_TRC_RED_RM_MEM_FAIL,
    PBBTE_TRC_RED_HW_AUDIT_FAIL,
    PBBTE_TRC_RED_SEND_BULK_REQ,
    PBBTE_TRC_RED_BULK_UPD_TAIL,
    PBBTE_TRC_MOD_NOT_INIT,
    PBBTE_TRC_RED_AUD_FOR_CTX,
    PBBTE_TRC_RED_AUD_FOR_ESP,
    PBBTE_TRC_RED_AUD_BUF_NOT_FND,
    PBBTE_TRC_RED_AUD_BUF_FND,
    PBBTE_TRC_RED_AUD_ACC_BUF, 
    PBBTE_TRC_MEM_FAIL_BITLIST
};

#ifdef  _PBTSYS_C
CONST CHR1 *gaPbbTeTrcString [] = {
    NULL,
    "Memory Pool creation failure for ctx mempool\r\n",
    "Memory Pool creation failure for TE-SID table mempool\r\n",
    "Memory Pool creation failure for DestMacVIDInfo table mempool\r\n",
    "Memory allocation fail for ContextEntry %d\n",
    "Memory allocation failed while creting entry for TeSid %u EspIndex %u\n",
    "Memory allocation failed while allocating node for DestMAC-VID node\n",
    "PBB-TE Module successfully started in context %d\n",
    "PBB-TE is already started in context %d\n",
    "PBBTE Module start failure in the context %d\n",
    "PBB-TE Module successfully shutdown in context %d\n",
    "PBB-TE is already shutdown in context %d\n",
    "PBB-TE Module not started in context %d\n",
    "Vlan : %d is configured as ESP-VID\n",
    "Vlan : %d is reset as ESP-VID\n",
    "Vlan : %d is not configured as ESP-VID\n",
    "ESP VLAN is already configured\n",
    "ESP VLAN reset is reconfigured\n",
    "ESP Vlan can't be deleted, Some TeSid entries in ESP Vlan %d is configured\n",
    "Getting free ESP Index failed in TeSid %u\n",
    "Next free ESP index for TE-SID entry %u, has been identified as :%u\n",    
    "Static Mcast Entry in Vlan %d is modified \n",
    "Static Mcast Entry in Vlan %d modification failed \n",
    "Static Mcast Entry in Vlan %d is created \n",
    "Static Mcast Entry in Vlan %d creation failed \n",
    "Static Ucast Entry in Vlan %d is modified \n",
    "Static Ucast Entry in Vlan %d modification failed \n",
    "Static Ucast Entry in Vlan %d is created \n",
    "Static Ucast Entry in Vlan %d creation failed \n",
    "Static unicast Entry added for TE-SID %u is deleted\n",
    "Static multicast Entry added for TE-SID %u is deleted\n",
    "NonMonitoredEspPresent bit is set while configuring TE-SID %u entry\n",
    "Non-Monitored ESP bit is set while configuring TESID entry for TeSid: %u, & EspIndex: %u \n",
    "NonMonitoredEspPresent bit is reset \n",
    "All the ESP-VLANs in context is successfuly deleted\n",
    "All the TE-SID entries in this context has been successfully deleted\n",
    "Invalid VLAN id is given \n",
    "Invalid ContextId is given \n",
    "Vlan is not active \r\n",
    "TE-SID Entry for TE-SID %u created \n",
    "TE-SID entry for TeSid %u - EspIndex %u has been successfully created\n",
    "PbbTeTeSidEspSet failed on TE-SID %u & EspIndex %u.- Entry Not found\n",
    "TE-SID entry %u is successfully deleted  \n",
    "PbbTeTeSidEspSet on TE-SID %u & EspIndex %u. \n",
    "TE-SID entry for TeSid %u - EspIndex %u has been successfully deleted\n",
    "TeSidEspEntryCreation failed for TeSid: %u& ESPIndex : %u \n",
    "TeSidEspEntrydeletion failed for TeSid: %u & ESPIndex : %u \n",
    "TeSid entry %u deletion thru API failed - TE-SID Configured statically\n",
    "Source MAC address mentioned while configuring TeSid entry %u is not valid\n",
    "Egress port mentioned while configuring TeSid entry %u is invalid\n",
    "TeSidEspGet failed for TeSid : %u & ESPIndex : %u \n",
    "PbbTeTeSidGetESPByTeSid successful on TE-SID %u\n",
    "PbbTeTeSidGetESPInfoByTeSid successful on TE-SID %u\n",
    "TeSidStorageTypeGet failed for TeSid : %u & ESPIndex : %u\n",
    "TeSidStorageTypeSet failed for TeSid : %u & ESPIndex :%u \n",
    "TeSidContextId set failed for TeSid : %u & ESPIndex : %u \n",
    "TeSidEspSet successful for TeSid : %u & ESPIndex : %u \n",
    "TeSidEntry with TeSid : %u & ESPIndex : %u is not existing,but RowStatus value provided is other than CREATE_AND_WAIT or CREATE_AND_GO \n",
    "TeSidEntry is already existing, so RowStatus value of either CREATE_AND_WAIT or CREATE_AND_GO won't be allowed\n",
    "Same RowStatus value is configured in TeSidTable entry,so just return\n",
    "TE-SID Entry %u & EspIndex %u has already been mapped to a context \n",
    "TE-SID entry %u is not found for deletion \n",
    "RB Tree add failed for TeSid %u EspIndex %u\n",
    "RB Tree get failed for TeSid %u EspIndex %u - Entry not existing\n",
    "RB Tree del failed for TeSid %u EspIndex %u\n",
    "RowStatus ACTIVE config for TE-SID entry %u & ESP Id %u failed, -- ContextId is not given\n",
    "RowStatus ACTIVE config for TE-SID entry %u & ESP Id %u failed, -- VLAN %d configured in ESP is not an ESP-VLAN\n",
    "ESP Entry for TE-SID %u and ESP-Index %u, configured on Global/Ctx Hash has been unreferred\n",
    "Configuration of Vlan %d as ESP-VLAN in Hardware failed\n",
    "Reset of Vlan %d as ESP-VLAN in Hardware failed\n",
    "Vlan flooding status set failed for Context %d Vlan %d\n",
    "Get Task Id Failed \n",
    "Failed to Create Semaphore for PBB-TE \n",
    "Failed to Create Queue for PBB-TE Configuration \n",
    "Memory Pool Creation Failed for Queue of PBB-TE Configuration \n",
    "PBB-TE Registration with RM Failed \n",
    "Buffer From RM to PBB-TE is NULL, hence RM message is ignored \n",
    "No memory avaiable for PBB-TE Queue message \n",
    "Message Enqueue to PBB-TE failed \n",
    "Send Event to PBB-TE failed \n",
    "PBB-TE Node Already in Active State \n",
    "PBB-TE Node Transition from Idle to Active State \n",
    "PBB-TE Node Transition from Standby to Active State \n",
    "PBB-TE Node Already in Standby State \n",
    "GO_STANDBY event received in Idle PBB-TE node is ignored \n",
    "PBB-TE Node Transition from Active to Standby State \n",
    "Hardware Audit for Active PBB-TE node is initialized \n",
    "Active PBB-TE Node receives STANDBY_DOWN event \n",
    "PBB-TE Node Transition from Idle to Standby State \n",
    "Standby PBB-TE Node initiates Dynamic Bulk Request \n",
    "PBB-TE Module Enable Failed \n",
    "PBB-TE Module Disable Failed \n",
    "PBB-TE Spawn Task for Hardware audit failed \n",
    "Bulk Update Tail Message Received \n",
    "Sync-up Messages Received at Idle or Active node hence ignored \n",
    "Incorrect Message Length in the Dynamic Update message \n",
    "Dynamic Update Failed at Standby for ESPs \n",
    "No Standby present to send the dynamic update \n",
    "RM Memory allocation for Message failed \n",
    "Red Hardware Audit Failed for PBB-TE \n",
    "Sending Bulk Request Message ..\n",
    "Sending Bulk Update Tail Message ..\n",
    "PBB-TE Module not yet Initialized \n",
    "Hardware Audit performed for Special Event - Context Deletion\n",
    "Hardware Audit performed for ESP Vlan \n",
    "Buffer Entry found, hence deleting it \n",
    "Buffer Entry not found, hence creating it \n",
    "Hardware Audit Buffer Entry access at the Standby node \n",
    "Error in Allocating memory for bitlist \n"
};
UINT1 gau1CtxName[26];
#else
extern CONST CHR1 *gaPbbTeTrcString[];
extern UINT1 gau1CtxName[26];
#endif

#ifdef TRACE_WANTED
#define  PBBTE_TRC(CtxId, TraceType, TrcNum) \
         {\
            SPRINTF((CHR1 *)gau1CtxName, "PBBTE[Switch: %d]", CtxId);\
            UtlTrcLog(PBBTE_TRC_FLAG(CtxId),TraceType,(const char *)gau1CtxName,\
        gaPbbTeTrcString[TrcNum]);\
         }
#define  PBBTE_TRC_ARG1(CtxId, TraceType, TrcNum, arg1) \
         {\
            SPRINTF((CHR1 *)gau1CtxName, "PBBTE[Switch: %d]", CtxId);\
            UtlTrcLog(PBBTE_TRC_FLAG(CtxId),TraceType,(const char *)gau1CtxName,\
        gaPbbTeTrcString[TrcNum], arg1);\
         }
#define  PBBTE_TRC_ARG2(CtxId, TraceType, TrcNum, arg1, arg2) \
         {\
            SPRINTF((CHR1 *)gau1CtxName, "PBBTE[Switch: %d]", CtxId);\
            UtlTrcLog(PBBTE_TRC_FLAG(CtxId),TraceType,(const char *)gau1CtxName,\
        gaPbbTeTrcString[TrcNum], arg1, arg2);\
         }
#define  PBBTE_GBL_TRC(TraceType, TrcNum) \
         UtlTrcLog(PBBTE_GBL_TRC_FLAG, TraceType, PBBTE_MOD_NAME, \
                   gaPbbTeTrcString[TrcNum])
#define  PBBTE_GBL_TRC_ARG1(TraceType, TrcNum, arg1) \
         UtlTrcLog(PBBTE_GBL_TRC_FLAG, TraceType, PBBTE_MOD_NAME, \
                   gaPbbTeTrcString[TrcNum], arg1)
#define  PBBTE_GBL_TRC_ARG2(TraceType, TrcNum ,arg1 ,arg2) \
         UtlTrcLog(PBBTE_GBL_TRC_FLAG, TraceType, PBBTE_MOD_NAME,\
                   gaPbbTeTrcString[TrcNum], arg1, arg2)
#define  PBBTE_GBL_TRC_ARG3(TraceType, TrcNum ,arg1 ,arg2, arg3) \
         UtlTrcLog(PBBTE_GBL_TRC_FLAG, TraceType, PBBTE_MOD_NAME,\
                   gaPbbTeTrcString[TrcNum], arg1, arg2, arg3)
#else
#define  PBBTE_TRC(CtxId, TraceType, TrcNum) 
#define  PBBTE_TRC_ARG1(CtxId, TraceType, TrcNum, arg1) 
#define  PBBTE_TRC_ARG2(CtxId, TraceType, TrcNum, arg1, arg2) 
#define  PBBTE_GBL_TRC(TraceType, TrcNum) 
#define  PBBTE_GBL_TRC_ARG1(TraceType, TrcNum, arg1) 
#define  PBBTE_GBL_TRC_ARG2(TraceType, TrcNum, arg1, arg2) 
#define  PBBTE_GBL_TRC_ARG3(TraceType, TrcNum, arg1, arg2, arg3) 
#endif

/* PBB-TE Redundancy module traces */
#define  PBBTE_RED_TRC           0x00040000 

#define  PBBTE_INIT_SHUT_TRC     INIT_SHUT_TRC
#define  PBBTE_MGMT_TRC          MGMT_TRC
#define  PBBTE_DATA_PATH_TRC     DATA_PATH_TRC
#define  PBBTE_CONTROL_PATH_TRC  CONTROL_PLANE_TRC
#define  PBBTE_DUMP_TRC          DUMP_TRC
#define  PBBTE_OS_RESOURCE_TRC   OS_RESOURCE_TRC
#define  PBBTE_ALL_FAILURE_TRC   ALL_FAILURE_TRC
#define  PBBTE_BUFFER_TRC        BUFFER_TRC
#define  PBBTE_TRC_ALL           INIT_SHUT_TRC | MGMT_TRC\
                          DATA_PATH_TRC | CONTROL_PLANE_TRC\
     DUMP_TRC | OS_RESOURCE_TRC\
     ALL_FAILURE_TRC | BUFFER_TRC | PBBTE_RED_TRC
     
     
