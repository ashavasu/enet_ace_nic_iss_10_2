#ifndef _FSPBTWR_H
#define _FSPBTWR_H

VOID RegisterFSPBT(VOID);

VOID UnRegisterFSPBT(VOID);
INT4 FsPbbTeGlobalTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeGlobalTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeGlobalTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeGlobalTraceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsPbbTeContextTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbTeContextSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextNoOfActiveEspsGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextNoOfCreatedEspsGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextNoOfDeletedEspsGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeContextTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPbbTeEspVidTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbTeEspVidRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeEspVidRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeEspVidRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeEspVidTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPbbTeTeSidExtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPbbTeTeSidExtContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTeSidExtContextIdSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTeSidExtContextIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTeSidExtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsPbbTeTestApiContextIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiTeSidGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiDestMacAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiSourceMacAddrGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEspVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEgressPortGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEgressPortListGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiActionGet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiContextIdSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiTeSidSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiDestMacAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiSourceMacAddrSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEspVlanIdSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEgressPortSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEgressPortListSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiActionSet(tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiContextIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiTeSidTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiDestMacAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiSourceMacAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEspVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEgressPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiEgressPortListTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiActionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPbbTeTestApiContextIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbbTeTestApiTeSidDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbbTeTestApiDestMacAddrDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbbTeTestApiSourceMacAddrDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbbTeTestApiEspVlanIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbbTeTestApiEgressPortDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbbTeTestApiEgressPortListDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsPbbTeTestApiActionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSPBTWR_H */
