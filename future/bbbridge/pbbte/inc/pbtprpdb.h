/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pbtprpdb.h,v 1.1.1.1 2008/10/29 09:46:06 premap-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSPBTDB_H
#define _FSPBTDB_H

UINT1 FsPbbTeContextTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsPbbTeEspVidTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER};
UINT1 FsPbbTeTeSidExtTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fspbt [] ={1,3,6,1,4,1,29601,2,11};
tSNMP_OID_TYPE fspbtOID = {9, fspbt};


UINT4 FsPbbTeGlobalTraceOption [ ] ={1,3,6,1,4,1,29601,2,11,1,1};
UINT4 FsPbbTeContextId [ ] ={1,3,6,1,4,1,29601,2,11,2,1,1,1};
UINT4 FsPbbTeContextSystemControl [ ] ={1,3,6,1,4,1,29601,2,11,2,1,1,2};
UINT4 FsPbbTeContextTraceOption [ ] ={1,3,6,1,4,1,29601,2,11,2,1,1,3};
UINT4 FsPbbTeContextNoOfActiveEsps [ ] ={1,3,6,1,4,1,29601,2,11,2,1,1,4};
UINT4 FsPbbTeContextNoOfCreatedEsps [ ] ={1,3,6,1,4,1,29601,2,11,2,1,1,5};
UINT4 FsPbbTeContextNoOfDeletedEsps [ ] ={1,3,6,1,4,1,29601,2,11,2,1,1,6};
UINT4 FsPbbTeEspVid [ ] ={1,3,6,1,4,1,29601,2,11,3,1,1,1};
UINT4 FsPbbTeEspVidRowStatus [ ] ={1,3,6,1,4,1,29601,2,11,3,1,1,2};
UINT4 FsPbbTeTeSidExtContextId [ ] ={1,3,6,1,4,1,29601,2,11,4,1,1,1};
UINT4 FsPbbTeTestApiContextId [ ] ={1,3,6,1,4,1,29601,2,11,5,1};
UINT4 FsPbbTeTestApiTeSid [ ] ={1,3,6,1,4,1,29601,2,11,5,2};
UINT4 FsPbbTeTestApiDestMacAddr [ ] ={1,3,6,1,4,1,29601,2,11,5,3};
UINT4 FsPbbTeTestApiSourceMacAddr [ ] ={1,3,6,1,4,1,29601,2,11,5,4};
UINT4 FsPbbTeTestApiEspVlanId [ ] ={1,3,6,1,4,1,29601,2,11,5,5};
UINT4 FsPbbTeTestApiEgressPort [ ] ={1,3,6,1,4,1,29601,2,11,5,6};
UINT4 FsPbbTeTestApiEgressPortList [ ] ={1,3,6,1,4,1,29601,2,11,5,7};
UINT4 FsPbbTeTestApiAction [ ] ={1,3,6,1,4,1,29601,2,11,5,8};


tMbDbEntry fspbtMibEntry[]= {

{{11,FsPbbTeGlobalTraceOption}, NULL, FsPbbTeGlobalTraceOptionGet, FsPbbTeGlobalTraceOptionSet, FsPbbTeGlobalTraceOptionTest, FsPbbTeGlobalTraceOptionDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{13,FsPbbTeContextId}, GetNextIndexFsPbbTeContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsPbbTeContextTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbTeContextSystemControl}, GetNextIndexFsPbbTeContextTable, FsPbbTeContextSystemControlGet, FsPbbTeContextSystemControlSet, FsPbbTeContextSystemControlTest, FsPbbTeContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbTeContextTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbTeContextTraceOption}, GetNextIndexFsPbbTeContextTable, FsPbbTeContextTraceOptionGet, FsPbbTeContextTraceOptionSet, FsPbbTeContextTraceOptionTest, FsPbbTeContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPbbTeContextTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbTeContextNoOfActiveEsps}, GetNextIndexFsPbbTeContextTable, FsPbbTeContextNoOfActiveEspsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbbTeContextTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbTeContextNoOfCreatedEsps}, GetNextIndexFsPbbTeContextTable, FsPbbTeContextNoOfCreatedEspsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbbTeContextTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbTeContextNoOfDeletedEsps}, GetNextIndexFsPbbTeContextTable, FsPbbTeContextNoOfDeletedEspsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsPbbTeContextTableINDEX, 1, 0, 0, NULL},

{{13,FsPbbTeEspVid}, GetNextIndexFsPbbTeEspVidTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsPbbTeEspVidTableINDEX, 2, 0, 0, NULL},

{{13,FsPbbTeEspVidRowStatus}, GetNextIndexFsPbbTeEspVidTable, FsPbbTeEspVidRowStatusGet, FsPbbTeEspVidRowStatusSet, FsPbbTeEspVidRowStatusTest, FsPbbTeEspVidTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsPbbTeEspVidTableINDEX, 2, 0, 1, NULL},

{{13,FsPbbTeTeSidExtContextId}, GetNextIndexFsPbbTeTeSidExtTable, FsPbbTeTeSidExtContextIdGet, FsPbbTeTeSidExtContextIdSet, FsPbbTeTeSidExtContextIdTest, FsPbbTeTeSidExtTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsPbbTeTeSidExtTableINDEX, 2, 0, 0, "-1"},

{{11,FsPbbTeTestApiContextId}, NULL, FsPbbTeTestApiContextIdGet, FsPbbTeTestApiContextIdSet, FsPbbTeTestApiContextIdTest, FsPbbTeTestApiContextIdDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbTeTestApiTeSid}, NULL, FsPbbTeTestApiTeSidGet, FsPbbTeTestApiTeSidSet, FsPbbTeTestApiTeSidTest, FsPbbTeTestApiTeSidDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbTeTestApiDestMacAddr}, NULL, FsPbbTeTestApiDestMacAddrGet, FsPbbTeTestApiDestMacAddrSet, FsPbbTeTestApiDestMacAddrTest, FsPbbTeTestApiDestMacAddrDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbTeTestApiSourceMacAddr}, NULL, FsPbbTeTestApiSourceMacAddrGet, FsPbbTeTestApiSourceMacAddrSet, FsPbbTeTestApiSourceMacAddrTest, FsPbbTeTestApiSourceMacAddrDep, SNMP_DATA_TYPE_MAC_ADDRESS, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbTeTestApiEspVlanId}, NULL, FsPbbTeTestApiEspVlanIdGet, FsPbbTeTestApiEspVlanIdSet, FsPbbTeTestApiEspVlanIdTest, FsPbbTeTestApiEspVlanIdDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbTeTestApiEgressPort}, NULL, FsPbbTeTestApiEgressPortGet, FsPbbTeTestApiEgressPortSet, FsPbbTeTestApiEgressPortTest, FsPbbTeTestApiEgressPortDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbTeTestApiEgressPortList}, NULL, FsPbbTeTestApiEgressPortListGet, FsPbbTeTestApiEgressPortListSet, FsPbbTeTestApiEgressPortListTest, FsPbbTeTestApiEgressPortListDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsPbbTeTestApiAction}, NULL, FsPbbTeTestApiActionGet, FsPbbTeTestApiActionSet, FsPbbTeTestApiActionTest, FsPbbTeTestApiActionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fspbtEntry = { 18, fspbtMibEntry };
#endif /* _FSPBTDB_H */

