#ifndef _PBTHDRS_H
#define _PBTHDRS_H

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                      */
/* $Id: pbthdrs.h,v 1.8 2011/09/12 06:42:20 siva Exp $                     */
/* Licensee Aricent Inc., 2001-2002                                          */
/*****************************************************************************/
/*    FILE  NAME            : pbthdrs.h                                      */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : Provider Backbone Bridge Traffic Engineering   */
/*    MODULE NAME           : PBB-TE                                         */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE : 30 September 2008                              */
/*    AUTHOR                : PBB-TE Team                                    */
/*    DESCRIPTION           : This file contains all include files in the    */
/*                            PBB-TE module.                                 */
/*---------------------------------------------------------------------------*/

#include "lr.h"
#include "cfa.h"
#include "iss.h"
#include "vcm.h"
#include "size.h"
#include "bridge.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "snmputil.h"
#include "trace.h"
#include "fsvlan.h"
#include "mstp.h"
#include "l2iwf.h"
#include "mrp.h"
#include "redblack.h"
#include "pbbte.h"
#include "npapi.h"
#include "pbtnp.h"
#include "mrp.h"
#include "hwaud.h"
#include "pbttdfs.h"
#include "pbtprot.h"
#include "pbt1aylw.h"
#include "pbtprplw.h"
#include "pbtmacs.h"
#include "pbtglob.h"
#include "pbttrc.h"
#include "pbt1aywr.h"
#include "pbtprpwr.h"
#include "pbtcli.h"
#include "pbtsz.h"
#ifdef SYSLOG_WANTED
#include "fssyslog.h"
#endif

#ifdef MBSM_WANTED
#include "mbsm.h"
#include "pbtmbsm.h"
#endif

#ifdef L2RED_WANTED
#include "pbtred.h"
#ifdef NPAPI_WANTED
#include "hwaudmap.h"
#endif
#endif

#ifdef  PBBTE_PERFORM_TRACE_WANTED
#include  <sys/time.h>
#endif

#endif  
