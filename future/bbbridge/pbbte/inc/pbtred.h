/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pbtred.h,v 1.4 2010/11/15 14:36:17 prabuc Exp $
*
* Description: <File description>
*****************************************************************************/
/*    FILE  NAME            : pbtred.h                                       */
/*    PRINCIPAL AUTHOR      : Aricent Inc.                                   */
/*    SUBSYSTEM NAME        : PBBTE Redundancy                               */
/*    MODULE NAME           : PBBTE                                          */
/*                                                                           */
/*    LANGUAGE              : C                                              */
/*    TARGET ENVIRONMENT    : Any                                            */
/*    DATE OF FIRST RELEASE :                                                */
/*    AUTHOR                : L2RED TEAM                                     */
/*    DESCRIPTION           : This file contains all constants and typedefs  */
/*                            for PBBTE redundancy module.                   */
/*---------------------------------------------------------------------------*/
#ifndef _PBBTERED_H
#define _PBBTERED_H

#define PBBTE_RED_TYPE_FIELD_SIZE     1
#define PBBTE_RED_LEN_FIELD_SIZE      2
#define PBBTE_RED_ACTION_FIELD_SIZE   1
#define PBBTE_RED_MON_ESP_FIELD_SIZE \
    (sizeof (UINT4) + sizeof (UINT4) + sizeof (tMacAddr) + sizeof (tMacAddr)\
     + sizeof (tVlanId) + sizeof (UINT1) + sizeof (UINT1))
#define PBBTE_RED_NON_MON_ESP_FIELD_SIZE \
    (sizeof (UINT4) + sizeof (tMacAddr) + sizeof (tVlanId))

#define PBBTE_RED_MAX_MSG_SIZE              1500
#define PBBTE_RED_DEL_ALL_UPD_SIZE          7
#define PBBTE_RED_BULK_REQ_MSG_SIZE         3
#define PBBTE_RED_BULK_UPD_TAIL_MSG_SIZE    3
       
#define PBBTE_RED_ENTRY_PRESENT_IN_BOTH     1
#define PBBTE_RED_ENTRY_PRESENT_IN_HW_ONLY  2
#define PBBTE_RED_ENTRY_PRESENT_IN_SW_ONLY  3
#define PBBTE_RED_ENTRY_NOT_PRESENT_IN_BOTH 4 

#define PBBTE_RED_NO_OF_MON_ESPS_PER_SUB_UPDATE     30

#define PBBTE_RED_AUDIT_STARTED             1
#define PBBTE_RED_AUDIT_COMPLETED           2

#define PBBTE_AUDIT_TASK_PRIORITY     240 /* should have lower priority */
#define PBBTE_AUDIT_TASK            ((UINT1*)"PBTAUD")

#define PBBTE_RED_NO_OF_VLANS_PER_SUB_UPDATE         10
#define PBBTE_RED_NO_OF_PORTS_PER_SUB_UPDATE         10
    
/* Macros to write in to RM buffer. */
#define PBBTE_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define PBBTE_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define PBBTE_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define PBBTE_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define PBBTE_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define PBBTE_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define PBBTE_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define PBBTE_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)

/* Macros */

#define PBBTE_RED_GET_STATIC_CONFIG_STATUS() PbbTeRmGetStaticConfigStatus()

#define PBBTE_RED_BULK_REQ_RECD()            PBBTE_RED_GLOBAL_INFO.bBulkReqRecvd

#define PBBTE_RED_BULK_UPD_NEXT_ESP \
    PBBTE_RED_GLOBAL_INFO.BulkUpdNextEspInfo

#define PBBTE_RED_NUM_STANDBY_NODES() \
    PBBTE_RED_GLOBAL_INFO.u1NumStandbyNodes

#define PBBTE_RED_BULK_UPD_NEXT_ESP_INFO \
    PBBTE_RED_GLOBAL_INFO.BulkUpdNextEspInfo

#define PBBTE_RED_BULK_UPD_NEXT_CTXT \
    PBBTE_RED_BULK_UPD_NEXT_ESP_INFO.u4ContextId

#define PBBTE_RED_AUDIT_TASK_ID PBBTE_RED_GLOBAL_INFO.HwAuditTaskId 

#ifdef NPAPI_WANTED
#define PBBTE_RED_AUD_BUF_TABLE      PBBTE_RED_GLOBAL_INFO.AuditBufferTable 
#define PBBTE_NPSYNC_BLK()  gPbbTeNpSyncBlk

#define  PBBTE_RED_AUD_BUF_ENTRY_ALLOC_MEMBLK(pu1Msg) \
   (pu1Msg = (tPbbTeBufferEntry *)MemAllocMemBlk(PBBTE_RED_AUD_BUF_MEMPOOL_ID))

#define  PBBTE_RED_AUD_BUF_ENTRY_FREE_MEMBLK(pu1Msg) \
{\
         MemReleaseMemBlock(PBBTE_RED_AUD_BUF_MEMPOOL_ID, (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}


#endif

/* Represents the message types encoded in the update messages */
typedef enum {
    PBBTE_BULK_REQ_MESSAGE = 1,
    PBBTE_BULK_UPD_TAIL_MESSAGE,
    PBBTE_DYN_MON_ESP_INFO_MSG,
    PBBTE_DYN_NON_MON_ESP_INFO_MSG
}tPbbTeRmMessageType;

#define PBBTE_CONTEXT_DELETE_SYNCUP 10
/* Prototypes for file pbtred.c */
INT4
PbbTeRedHandleUpdateEvents PROTO ((UINT1 u1Event, tRmMsg * pData, 
                                   UINT2 u2DataLen));
VOID
PbbTeRedProcessUpdateMsg PROTO ((tPbbTeQMsg * pPbbTeQMsg));

VOID
PbbTeRedHandleStandbyUpEvent PROTO ((VOID));

INT4
PbbTeRedHandleIdleToActive PROTO ((VOID));

INT4
PbbTeRedHandleStandbyToActive PROTO ((VOID));

INT4
PbbTeRedHandleIdleToStandby PROTO ((VOID));

INT4
PbbTeRedHandleActiveToStandby PROTO ((VOID));

VOID
PbbTeRedInitHardwareAudit PROTO ((VOID));

VOID
PbbTeRedAuditTaskMain PROTO ((INT1 *pi1Param));

VOID
PbbTeRedHandleUpdates PROTO ((tRmMsg *pMsg, UINT2 u2DataLen));


VOID
PbbTeRedSendBulkUpdates PROTO ((VOID));

VOID
PbbTeRedSendBulkReqMsg PROTO((VOID));

INT4
PbbTeRedSendMsgToRm PROTO ((tRmMsg * pMsg, UINT2 u2Len));

INT4
PbbTeRedSendBulkUpdTailMsg PROTO ((VOID));

VOID
PbbTeRedSendEspBulkUpdates PROTO ((VOID));

VOID
PbbTePerformAudit PROTO ((VOID));

#ifdef NPAPI_WANTED
VOID
PbbTeCreateAuditBufferTable PROTO ((VOID));

VOID
PbbTeDoEspVlanHwAudit PROTO ((UINT4 u4ContextId, tVlanId EspVlan));

VOID
PbbTeDoContextHwAudit PROTO ((UINT4 u4ContextId));

VOID
PbbteHwAuditCreateOrFlushBuffer PROTO ((unNpSync  *pNpSync, UINT4 u4NpApiId, 
                                        UINT4 u4EventId));
VOID
PbbTeNpSyncProcessSyncMsg PROTO ((tRmMsg * pMsg, UINT2 *pu2OffSet));

PUBLIC VOID 
NpSyncSendDeleteContextEvent PROTO ((UINT4 u4ContextId));
#endif
#endif /* _PBBTERED_H */
