/********************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: pbtmacs.h,v 1.10 2014/02/07 13:28:09 siva Exp $
 *
 * Description: This file contains macro definitions used in
 *              PBB-TE module
 **********************************************************************/

#ifndef _PBBTEMACS_H_
#define _PBBTEMACS_H_

#define PBBTE_ACTIVE           1
#define PBBTE_NOT_IN_SERVICE   2
#define PBBTE_NOT_READY        3
#define PBBTE_CREATE_AND_GO    4
#define PBBTE_CREATE_AND_WAIT  5
#define PBBTE_DESTROY          6

#define PBBTE_MAX_CONTEXTS           FsPBBTESizingParams[MAX_PBBTE_CONTEXT_INFO_SIZING_ID].u4PreAllocatedUnits

#define PBBTE_CTX_VLAN_HASH_TBL(Ctx, Vlan)\
            (tTMO_HASH_TABLE *)(PBBTE_CONTEXT_PTR (Ctx)->aHashTableList[PBBTE_CONTEXT_PTR(Ctx)->aEspVlanHashTblNum[Vlan]])

#define PBBTE_MAX_PORTS              SYS_MAX_INTERFACES

#define PBBTE_CREATE_SEM  OsixSemCrt
#define PBBTE_DELETE_SEM  OsixSemDel
#define PBBTE_TAKE_SEM    OsixSemTake
#define PBBTE_GIVE_SEM    OsixSemGive
#define PBBTE_SEM_NAME    ((UINT1 *) "PBBTE")


#define PBBTE_SWITCH_ALIAS_LEN     VCM_ALIAS_MAX_LEN

#define PBBTE_MEM_SUCCESS   MEM_SUCCESS
#define PBBTE_MEM_FAILURE   MEM_FAILURE
#define PBBTE_MEM_CPY       MEMCPY
#define PBBTE_MEM_CMP       MEMCMP
#define PBBTE_MEM_SET       MEMSET

#define PBBTE_32BIT_MAX 4294967295U
#define PBBTE_INIT_VAL  0

#define PBBTE_ESP_LENGTH      14
#define PBBTE_MAC_ADDR_SIZE   6
#define PBBTE_VLAN_ID_SIZE    2
#define PBBTE_ESP_DA_OFFSET   0
#define PBBTE_ESP_SA_OFFSET   6
#define PBBTE_ESP_VLAN_OFFSET 12

#define PBBTE_PORTS_PER_BYTE  8
#define PBBTE_MAX_VLANS       4094

/* Since the PrintMacAddress adds 3 spaces after MAC address
 * in the output parameter string below macro size should not
 * be less than 21 */
#define PBBTE_CLI_MAX_MAC_STRING_SIZE  21
#define PBBTE_MAX_RED_AUDIT_BUFFER_ENTRIES  50

#define PBBTE_GET_DMAC_VID_INFO_CNT(u4Val) ((u4Val) & (0x7FFFFFFF))
#define PBBTE_GET_DMAC_VID_INFO_STATUS(u4Val) (((u4Val) & (0x80000000))>>31)

#define PBBTE_SET_DMAC_VID_INFO_CNT(u4Val, u4SetVal) \
    (u4Val = ((u4Val & 0x80000000) | (u4SetVal & 0x7FFFFFFF)))
#define PBBTE_SET_DMAC_VID_INFO_NM_STATUS(u4Val, u4SetVal) \
    (u4Val = ((u4Val & 0x7FFFFFFF) | ((u4SetVal<<31) & 0x80000000)))
#define PBBTE_INCR_DMAC_VID_INFO_CNT(u4Val) \
    {\
        UINT4 u4Temp = 0;\
        u4Temp = ++(u4Val);\
        u4Val = ((u4Val & 0x80000000) | ((u4Temp) & 0x7FFFFFFF));\
    }
#define PBBTE_DECR_DMAC_VID_INFO_CNT(u4Val) \
    {\
        UINT4 u4Temp = 0;\
        u4Temp = --(u4Val);\
        u4Val = ((u4Val & 0x80000000) | ((u4Temp) & 0x7FFFFFFF));\
    }

#define PBBTE_RED_GLOBAL_INFO                gPbbTeGlobalInfo.PbbTeRedGlobInfo
#define PBBTE_RED_AUD_BUF_MEMPOOL_ID           PBBTE_RED_GLOBAL_INFO.AuditBufferPoolId
#define   PBBTE_DELETE_MEM_POOL(PoolId)  \
          if (PoolId != 0) (VOID) MemDeleteMemPool(PoolId)

#define  PBBTE_CONTEXT_MEMPOOL_ID      gPbbTeGlobalInfo.ContextTablePoolId
#define  PBBTE_CONTEXT_ENTRY_ALLOC_MEMBLK(pu1Msg) \
         (pu1Msg = (tPbbTeContextInfo *)MemAllocMemBlk(PBBTE_CONTEXT_MEMPOOL_ID))
#define  PBBTE_CONTEXT_ENTRY_FREE_MEMBLK(pu1Msg) \
{\
         MemReleaseMemBlock(PBBTE_CONTEXT_MEMPOOL_ID, (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  PBBTE_TESID_MEMPOOL_ID      gPbbTeGlobalInfo.TeSidTablePoolId
#define  PBBTE_TESID_ENTRY_ALLOC_MEMBLK(pu1Msg) \
         (pu1Msg = (tTeSidEspEntry *)MemAllocMemBlk(PBBTE_TESID_MEMPOOL_ID))
#define  PBBTE_TESID_ENTRY_FREE_MEMBLK(pu1Msg) \
{\
         MemReleaseMemBlock(PBBTE_TESID_MEMPOOL_ID, (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  PBBTE_ESP_DESTMAC_MEMPOOL_ID      gPbbTeGlobalInfo.EspDestMacPoolId
#define  PBBTE_ESP_DESTMAC_ENTRY_ALLOC_MEMBLK(pu1Msg) \
         (pu1Msg = (tEspDMacVIDInfo *)MemAllocMemBlk(PBBTE_ESP_DESTMAC_MEMPOOL_ID))
#define  PBBTE_ESP_DESTMAC_ENTRY_FREE_MEMBLK(pu1Msg) \
{\
         MemReleaseMemBlock(PBBTE_ESP_DESTMAC_MEMPOOL_ID, (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define  PBBTE_SCAN_ALL_CONTEXT(u4Context)\
    for (u4ContextId = 0; u4ContextId <= PBBTE_MAX_CONTEXTS; u4ContextId++)\

#define PBBTE_IS_VC_VALID(i4ContextId) \
    (((i4ContextId  < 0) || ((UINT4)i4ContextId >= PBBTE_MAX_CONTEXTS)) ? \
     PBBTE_FALSE : PBBTE_TRUE)
    
#define PBBTE_IS_SYSTEM_STARTED(u4ContextId) \
    (((u4ContextId > 0) || ((UINT4)u4ContextId <= PBBTE_MAX_CONTEXTS))? \
 (((gPbbTeGlobalInfo.apPbbTeContextInfo[u4ContextId] != NULL)) \
        ? PBBTE_START : PBBTE_SHUTDOWN) : PBBTE_SHUTDOWN)

#define PBBTE_CONTEXT_PTR(CtxId)  (gPbbTeGlobalInfo.apPbbTeContextInfo[CtxId])
#define PBBTE_INVALID_CONTEXT          (PBBTE_MAX_CONTEXTS + 1)

#define PBBTE_GET_GBL_TRC_OPTION(u4TrcOpt) \
    {\
        u4TrcOpt = gPbbTeGlobalInfo.u4TraceOption;\
    }
#define PBBTE_SET_GBL_TRC_OPTION(u4TrcOpt) \
    {\
        gPbbTeGlobalInfo.u4TraceOption = u4TrcOpt;\
    }

#define PBBTE_IS_ESP_VLAN(u4ContextId, u2Vlan, u1Result) \
        {\
           PBBTE_LOCK();\
           u1Result = (((PBBTE_CONTEXT_PTR(u4ContextId)->aEspVlanHashTblNum[u2Vlan]) != 0) ? PBBTE_TRUE : PBBTE_FALSE);\
           PBBTE_UNLOCK();\
        }        

#define PBBTE_SET_MEMBER_PORT(au1PortArray, u2Port) \
           {\
              OSIX_BITLIST_SET_BIT(au1PortArray, u2Port, PBBTE_PORTS_PER_BYTE)\
           }

#define PBBTE_GET_CTX_TRC_OPTION(CtxId, u4TrcOpt) \
    {\
        PBBTE_LOCK();\
        u4TrcOpt = gPbbTeGlobalInfo.au4ContextTraceOption[CtxId];\
        PBBTE_UNLOCK();\
    }
#define PBBTE_SET_CTX_TRC_OPTION(CtxId, u4TrcOpt) \
    {\
 PBBTE_LOCK();\
        gPbbTeGlobalInfo.au4ContextTraceOption[CtxId] = u4TrcOpt;\
 PBBTE_UNLOCK();\
    }
#define PBBTE_GET_NO_OF_CREATED_ESP(CtxId, u4Cnt) \
    {\
 PBBTE_LOCK();\
        u4Cnt = gPbbTeGlobalInfo.apPbbTeContextInfo[CtxId]->u4NoOfCreatedEsps;\
 PBBTE_UNLOCK();\
    }
#define PBBTE_GET_NO_OF_DELETED_ESP(CtxId, u4Cnt) \
    {\
 PBBTE_LOCK();\
        u4Cnt = gPbbTeGlobalInfo.apPbbTeContextInfo[CtxId]->u4NoOfDeletedEsps;\
 PBBTE_UNLOCK();\
    }
#define PBBTE_GET_NO_OF_ACTIVE_ESP(CtxId, u4Cnt) \
    {\
 PBBTE_LOCK();\
        u4Cnt = gPbbTeGlobalInfo.apPbbTeContextInfo[CtxId]->u4NoOfActiveEsps;\
 PBBTE_UNLOCK();\
    }
#define PBBTE_INCR_NO_OF_CREATED_ESP(CtxId) \
    {\
        gPbbTeGlobalInfo.apPbbTeContextInfo[CtxId]->u4NoOfCreatedEsps++;\
    }
#define PBBTE_INCR_NO_OF_DELETED_ESP(CtxId) \
    {\
        gPbbTeGlobalInfo.apPbbTeContextInfo[CtxId]->u4NoOfDeletedEsps++;\
    }
#define PBBTE_INCR_NO_OF_ACTIVE_ESP(CtxId) \
    {\
        gPbbTeGlobalInfo.apPbbTeContextInfo[CtxId]->u4NoOfActiveEsps++;\
    }
#define PBBTE_DECR_NO_OF_ACTIVE_ESP(CtxId) \
    {\
        gPbbTeGlobalInfo.apPbbTeContextInfo[CtxId]->u4NoOfActiveEsps--;\
    }

#define PBBTE_CPY_MAC_ADDR(pMacAddr1, pMacAddr2) \
         MEMCPY(pMacAddr1,pMacAddr2,ETHERNET_ADDR_SIZE) 

#define PBBTE_ARE_MAC_ADDR_EQUAL(pMacAddr1, pMacAddr2) \
        ((PBBTE_MEM_CMP(pMacAddr1,pMacAddr2,ETHERNET_ADDR_SIZE)) ? \
          PBBTE_FALSE : PBBTE_TRUE)

#define PBBTE_IS_MCASTADDR(pMacAddr) \
        (((pMacAddr[0] & 0x01) != 0) ? PBBTE_TRUE : PBBTE_FALSE)

#define PBBTE_IS_BCASTADDR(pMacAddr) \
        ((PBBTE_MEM_CMP(pMacAddr, gPbbTeBcastAddress,\
   ETHERNET_ADDR_SIZE) != 0) ? PBBTE_FALSE : PBBTE_TRUE)

#define PBBTE_INIT_COMPLETE(u4Status)    lrInitComplete(u4Status)

#define PBBTE_Q_MEMPOOL_ID gPbbTeGlobalInfo.QMsgPoolId

#define  PBBTE_Q_MSG_ENTRY_ALLOC_MEMBLK(pu1Msg) \
         (pu1Msg = (tPbbTeQMsg *)MemAllocMemBlk(PBBTE_Q_MEMPOOL_ID))

#define  PBBTE_Q_MSG_ENTRY_FREE_MEMBLK(pu1Msg) \
{\
         MemReleaseMemBlock(PBBTE_Q_MEMPOOL_ID, (UINT1 *)pu1Msg);\
         pu1Msg = NULL;\
}

#define PBBTE_IS_MODULE_INITIALISED() \
            ((gu1IsPbbTeInitialised == PBBTE_TRUE) ? PBBTE_TRUE : PBBTE_FALSE)

#define PBBTE_TASK_ID           gPbbTeGlobalInfo.TaskId
#define PBBTE_SELF              SELF
#define PBBTE_TASK              ((UINT1 *) "PBBTE")
#define PBBTE_GET_TASK_ID       OsixGetTaskId 

#define PBBTE_SEND_TO_QUEUE     OsixQueSend
#define PBBTE_RECV_FROM_QUEUE   OsixQueRecv
#define PBBTE_CREATE_QUEUE      OsixQueCrt
#define PBBTE_DELETE_QUEUE      OsixQueDel

#define PBBTE_CFG_QUEUE_ID      gPbbTeGlobalInfo.CfgQId
#define PBBTE_DEF_MSG_LEN       OSIX_DEF_MSG_LEN
#define PBBTE_MAX_MSG_LEN       OSIX_MAX_Q_MSG_LEN

#define PBBTE_CFG_QUEUE         ((UINT1 *)"PBBTEQ")

/* PBB-TE Queue had to handle RM, MBSM and Context Delete Messages 
 * Reason: Configuations/ dynamic API invocations happens one at a time. So we
 * can have a mininum queue depth here.
 */
#define PBBTE_CFG_Q_DEPTH       20

#define PBBTE_CFG_MSG_EVENT      0x01
#define PBBTE_RED_BULK_UPD_EVENT 0x02

#define PBBTE_SPAWN_TASK         OsixTskCrt
#define PBBTE_DELETE_TASK        OsixTskDel
#define PBBTE_SEND_EVENT         OsixEvtSend
#define PBBTE_RECEIVE_EVENT      OsixEvtRecv

#define PBBTE_Q_MSG_TYPE(pPbbTeQMsg) pPbbTeQMsg->u4MsgType

/* Message Types handled by PBB-Te */
enum {
    PBBTE_CONTEXT_DELETE_MSG = 1, 
    PBBTE_RM_MSG
};

#define PBBTE_NODE_STATE                     gPbbTeGlobalInfo.NodeState

#ifdef PBBTE_PERFORM_TRACE_WANTED

#define PBBTE_PERF_INIT() \
    gu4EspCnt = 0;\
    gPbbTePerformanceFp = FOPEN("PBBTE_Performance_Monitor.txt", "w");
#define PBBTE_PERF_MARK_START_TIME()     gettimeofday(&gStartTime, NULL);
#define PBBTE_PERF_MARK_END_TIME()      \
{\
    long long u4T1, u4T2;\
    gettimeofday(&gEndTime, NULL);\
    u4T1 = (gStartTime.tv_sec * 1000000 + gStartTime.tv_usec);\
    u4T2 = (gEndTime.tv_sec * 1000000 + gEndTime.tv_usec);\
    gu4TimeTaken += (u4T2 - u4T1);\
}

/* For MSR Performance Testing, pass u1Reset as 0, so that it will
 * count the cumulative time for restoring all the ESPs */
#define PBBTE_PERF_CALC_ADD_TIMETAKEN(u1Reset) \
{\
        UINT2 u2Val;\
        PBBTE_MEM_SET(gau1PbbTePerfMonBuf, 0, sizeof(gau1PbbTePerfMonBuf));\
        gu4EspCnt++;\
        u2Val = SPRINTF((CHR1 *)gau1PbbTePerfMonBuf,\
   "Time for %u ESP creation is %u uSec\n",gu4EspCnt,\
   gu4TimeTaken);\
        FWRITE(gau1PbbTePerfMonBuf, 1, u2Val, gPbbTePerformanceFp);\
        fflush(gPbbTePerformanceFp);\
        if(u1Reset)\
        {\
           gu4TimeTaken = 0;\
        }\
}

#define PBBTE_PERF_CALC_DEL_TIMETAKEN() \
{\
        UINT2 u2Val;\
        PBBTE_MEM_SET(gau1PbbTePerfMonBuf, 0, sizeof(gau1PbbTePerfMonBuf));\
        u2Val = SPRINTF((CHR1 *)gau1PbbTePerfMonBuf,\
   "Time for a ESP del when total ESPcnt %u is %u uSec\n",\
   gu4EspCnt, gu4TimeTaken);\
        FWRITE(gau1PbbTePerfMonBuf, 1, u2Val, gPbbTePerformanceFp);\
        fflush(gPbbTePerformanceFp);\
        gu4EspCnt--;\
        gu4TimeTaken = 0;\
}


#else

#define PBBTE_PERF_INIT() 
#define PBBTE_PERF_MARK_START_TIME()
#define PBBTE_PERF_MARK_END_TIME()  
#define PBBTE_PERF_CALC_ADD_TIMETAKEN(u1Reset) 
#define PBBTE_PERF_CALC_DEL_TIMETAKEN() 

#endif

   /* Macros for accessing the port bit array */

   /* 
    * This macro is used to find whether the specified port u2Port is
    * a member of the specified port list.
    */
   /* Warning!!! - Do not call the macro with u2Port as 0 or Invalid Port.*/

#define PBBTE_IS_VLAN_SET(au1VlanArray, Vlan, u1Result) \
        {\
           UINT2 VlanBytePos;\
           UINT2 VlanBitPos;\
           VlanBytePos = (UINT2)(Vlan / PBBTE_PORTS_PER_BYTE);\
           VlanBitPos  = (UINT2)(Vlan % PBBTE_PORTS_PER_BYTE);\
    if (VlanBitPos  == 0) {VlanBytePos -= 1;} \
           \
           if ((au1VlanArray[VlanBytePos] \
                & gau1PbbTeVlanBitMaskMap[VlanBitPos]) != 0) {\
           \
              u1Result = PBBTE_TRUE;\
           }\
           else {\
           \
              u1Result = PBBTE_FALSE; \
           } \
        }

#endif
