#ifdef MBSM_WANTED

#define MBSM_PBBTE    "PbbTeMbsm"

INT4
PbbTeMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
				tMbsmSlotInfo * pSlotInfo);
INT4
PbbTeMbsmUpdateOnCardRemoval (tMbsmPortInfo * pPortInfo,
				tMbsmSlotInfo * pSlotInfo);
#endif
