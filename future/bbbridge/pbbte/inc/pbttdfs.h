/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbttdfs.h,v 1.7 2013/05/06 11:56:06 siva Exp $
 *
 * Description: This file contains data structures defined for
 *              PBB-TE module.
 *********************************************************************/
#ifndef _PBBTETDFS_H
#define _PBBTETDFS_H

typedef tMemPoolId     tPbbTeMemPoolId;

/* This node will be stored in the TE-SID table entries.
 * ESPs having the same Destination MAC and VlanId will be referring to the
 * same EspDMacVIDInfo node. Count in this node denotes, number of TE-SID 
 * entries referring this node */
typedef struct EspDMacVIDInfo
{
    tTMO_HASH_NODE   NextNode; /* Pointer to the Next DMacVIDInfo Hash node */
    tMacAddr         DestAddr; /* Destination MAC Address in the TE-SID entry */
    tVlanId          EspVid; /* ESP Vlan ID in the TE-SID entry */
    UINT4            u4RefCount; /* Denotes number of TE-SID entries referring
        this node */
}tEspDMacVIDInfo;

/* This datastructure corresponds to TE-SID table, that will be stored in the 
 * database */
typedef struct TeSidEspEntry
{
    tRBNodeEmbd       RBNode; /* Embedded RB Node */
    UINT4             u4TeSid;
    UINT4             u4TeSidEspIndex;
    UINT4             u4ContextId;
    tEspDMacVIDInfo   *pEspDMacVIDInfo; /* Pointer to the DMacVIDInfo node */
    tMacAddr          SourceAddr;
    UINT1             u1RowStatus;
    UINT1             u1StorageType;
}tTeSidEspEntry;

/* Context specific information node */
typedef struct PbbTeContextInfo
{
    tTMO_HASH_TABLE   *aHashTableList[PBBTE_MAX_HASH_TABLE_PER_CONTEXT + 1];
    UINT4             u4NoOfCreatedEsps; /* Count of totally created ESPs */
    UINT4             u4NoOfDeletedEsps; /* Count of totally deleted ESPs */
    UINT4             u4NoOfActiveEsps; /* Count of currently existing ESPs */
    UINT4             u4HashCnt;
    UINT2             aEspVlanHashTblNum[PBBTE_MAX_VLAN_ID + 1];/* ESP Vlan list */
    UINT1             au1Pad[2];
}tPbbTeContextInfo;

/* PBB-TE HA Module Global Structure */
typedef struct PbbTeRedGlobInfo
{
    tEspInfo      BulkUpdNextEspInfo;          /* Next EspPathInfo to be sent for 
                                                  Dynmaic bulk update. */
    tTMO_SLL          AuditBufferTable;
    tPbbTeMemPoolId   AuditBufferPoolId; 
    tOsixTaskId       HwAuditTaskId;           /* PBB-TE Hardware Audit TaskId */
    BOOL1             bBulkReqRecvd;              /* Flag to indicate Bulk Request 
                                                  Reception */
    UINT1             u1NumStandbyNodes;       /* Number of Hot-Standby nodes */
    UINT1             au1Reserved[2];          /* Reserved bytes for Alignment */
}tPbbTeRedGlobInfo;

#ifdef L2RED_WANTED
/* PBB-TE HA Message Data */
typedef struct PbbTeRedData
{
    UINT4    u4Events;       /* Events handled by PBB-TE */
    VOID     *PeerId;        /* Peer Node's Id */
    tRmMsg   *pRmMsg;        /* RM Message Buffer pointer */
    UINT2    u2DataLen;      /* Number of bytes in the RM Message */
    UINT1    u1Reserved[2];  /* Reserved bytes for Alignment */
}tPbbTeRmData;

#endif

/* Global PBB-TE Module Information */
typedef struct PbbTeGlobalInfo
{
    tPbbTeContextInfo *apPbbTeContextInfo[SYS_DEF_MAX_NUM_CONTEXTS]; /* All context
          information */
    tRBTree           TeSidTable; /* TE-SID table - RB Tree */
    tTMO_HASH_TABLE   *pGlblEspDMacVIDInfo; /*Global DMacVIDInfo HASH, contains,
          DMacVIDInof of TE-SID entries that
          are not mapped to any of the 
          context */
    tPbbTeMemPoolId   ContextTablePoolId; /* Contexts PoolId */
    tPbbTeMemPoolId   TeSidTablePoolId;   /* TE-SID table entries PoolId */
    tPbbTeMemPoolId   EspDestMacPoolId;   /* DMacVIDInfo nodes PoolId */
    tPbbTeMemPoolId   QMsgPoolId;         /* PBB-TE Message Queue PoolId */
    tOsixSemId        DataSemId;
    tOsixTaskId       TaskId;             /* PBB-TE Task Id */
    tOsixQId          CfgQId;             /* PBB-TE Configuration Queue Id */
    tPbbTeRedGlobInfo PbbTeRedGlobInfo;   /* PBB-TE HA Global Structure */
    UINT4             au4ContextTraceOption[SYS_DEF_MAX_NUM_CONTEXTS]; /* Context
                                                                    specific 
            trace option
            */
    UINT4             u4TraceOption; /* Global trace option, for events
                                        that are not 
     specific to any contexts */
    tPbbTeNodeState   NodeState;               /* RED Node State */
}tPbbTeGlobalInfo;


/* PBB-TE Queue Data */
typedef struct PbbTeQMsg
{
    UINT4   u4MsgType;                /* Message Types handled by PBB-TE */
    UINT4   u4ContextId;              /* Context Id from VCM */
#ifdef MBSM_WANTED
    struct MbsmIndication {
        tMbsmProtoMsg *pMbsmProtoMsg; /* Message from MBSM */
    }MbsmCardUpdate;
#endif
#ifdef L2RED_WANTED
    tPbbTeRmData RmData;            /* Message from RM */
#endif
}tPbbTeQMsg;

/*PBB-TE HA Hardware Audit Buffer Structure */
typedef tNpSyncBufferEntry tPbbTeBufferEntry;
#endif
