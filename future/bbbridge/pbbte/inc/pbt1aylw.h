/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pbt1aylw.h,v 1.1.1.1 2008/10/29 09:46:06 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Ieee8021PbbTeProtectionGroupListTable. */
INT1
nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupListTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbbTeProtectionGroupListTable  */

INT1
nmhGetFirstIndexIeee8021PbbTeProtectionGroupListTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbbTeProtectionGroupListTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbbTeProtectionGroupListMode ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupListWorkingMA ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupListProtectionMA ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupListStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupListRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbbTeProtectionGroupListMode ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupListWorkingMA ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupListProtectionMA ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupListStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupListRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListMode ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListWorkingMA ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListProtectionMA ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbbTeProtectionGroupListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbbTeMASharedGroupTable. */
INT1
nmhValidateIndexInstanceIeee8021PbbTeMASharedGroupTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbbTeMASharedGroupTable  */

INT1
nmhGetFirstIndexIeee8021PbbTeMASharedGroupTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbbTeMASharedGroupTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbbTeMASharedGroupId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Proto Validate Index Instance for Ieee8021PbbTeTeSidTable. */
INT1
nmhValidateIndexInstanceIeee8021PbbTeTeSidTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbbTeTeSidTable  */

INT1
nmhGetFirstIndexIeee8021PbbTeTeSidTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbbTeTeSidTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbbTeTeSidEsp ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021PbbTeTeSidStorageType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbbTeTeSidRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbbTeTeSidEsp ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021PbbTeTeSidStorageType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021PbbTeTeSidRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbbTeTeSidEsp ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021PbbTeTeSidStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbbTeTeSidRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbbTeTeSidTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbbTeProtectionGroupConfigTable. */
INT1
nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbbTeProtectionGroupConfigTable  */

INT1
nmhGetFirstIndexIeee8021PbbTeProtectionGroupConfigTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbbTeProtectionGroupConfigTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbbTeProtectionGroupConfigState ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupConfigCommandStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupConfigCommandAdmin ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupConfigWTR ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupConfigHoldOff ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupConfigStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbbTeProtectionGroupConfigCommandAdmin ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupConfigWTR ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupConfigHoldOff ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupConfigStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupConfigCommandAdmin ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupConfigWTR ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupConfigHoldOff ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupConfigStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbbTeProtectionGroupConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbbTeProtectionGroupISidTable. */
INT1
nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupISidTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbbTeProtectionGroupISidTable  */

INT1
nmhGetFirstIndexIeee8021PbbTeProtectionGroupISidTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbbTeProtectionGroupISidTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbbTeProtectionGroupISidGroupId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupISidStorageType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbbTeProtectionGroupISidRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbbTeProtectionGroupISidGroupId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupISidStorageType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetIeee8021PbbTeProtectionGroupISidRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupISidGroupId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupISidStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbbTeProtectionGroupISidRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbbTeProtectionGroupISidTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Ieee8021PbbTeBridgeStaticForwardAnyUnicastTable. */
INT1
nmhValidateIndexInstanceIeee8021PbbTeBridgeStaticForwardAnyUnicastTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Ieee8021PbbTeBridgeStaticForwardAnyUnicastTable  */

INT1
nmhGetFirstIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts ARG_LIST((INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts ARG_LIST((UINT4 *  ,INT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
