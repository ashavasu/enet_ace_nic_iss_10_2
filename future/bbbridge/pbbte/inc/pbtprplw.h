/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pbtprplw.h,v 1.1.1.1 2008/10/29 09:46:06 premap-iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbTeGlobalTraceOption ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbTeGlobalTraceOption ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbTeGlobalTraceOption ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbTeGlobalTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbTeContextTable. */
INT1
nmhValidateIndexInstanceFsPbbTeContextTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbTeContextTable  */

INT1
nmhGetFirstIndexFsPbbTeContextTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbTeContextTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbTeContextSystemControl ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsPbbTeContextTraceOption ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbbTeContextNoOfActiveEsps ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbbTeContextNoOfCreatedEsps ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsPbbTeContextNoOfDeletedEsps ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbTeContextSystemControl ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsPbbTeContextTraceOption ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbTeContextSystemControl ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsPbbTeContextTraceOption ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbTeContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbTeEspVidTable. */
INT1
nmhValidateIndexInstanceFsPbbTeEspVidTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbTeEspVidTable  */

INT1
nmhGetFirstIndexFsPbbTeEspVidTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbTeEspVidTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbTeEspVidRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbTeEspVidRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbTeEspVidRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbTeEspVidTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsPbbTeTeSidExtTable. */
INT1
nmhValidateIndexInstanceFsPbbTeTeSidExtTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsPbbTeTeSidExtTable  */

INT1
nmhGetFirstIndexFsPbbTeTeSidExtTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsPbbTeTeSidExtTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbTeTeSidExtContextId ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbTeTeSidExtContextId ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbTeTeSidExtContextId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbTeTeSidExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsPbbTeTestApiContextId ARG_LIST((INT4 *));

INT1
nmhGetFsPbbTeTestApiTeSid ARG_LIST((INT4 *));

INT1
nmhGetFsPbbTeTestApiDestMacAddr ARG_LIST((tMacAddr * ));

INT1
nmhGetFsPbbTeTestApiSourceMacAddr ARG_LIST((tMacAddr * ));

INT1
nmhGetFsPbbTeTestApiEspVlanId ARG_LIST((INT4 *));

INT1
nmhGetFsPbbTeTestApiEgressPort ARG_LIST((INT4 *));

INT1
nmhGetFsPbbTeTestApiEgressPortList ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsPbbTeTestApiAction ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsPbbTeTestApiContextId ARG_LIST((INT4 ));

INT1
nmhSetFsPbbTeTestApiTeSid ARG_LIST((INT4 ));

INT1
nmhSetFsPbbTeTestApiDestMacAddr ARG_LIST((tMacAddr ));

INT1
nmhSetFsPbbTeTestApiSourceMacAddr ARG_LIST((tMacAddr ));

INT1
nmhSetFsPbbTeTestApiEspVlanId ARG_LIST((INT4 ));

INT1
nmhSetFsPbbTeTestApiEgressPort ARG_LIST((INT4 ));

INT1
nmhSetFsPbbTeTestApiEgressPortList ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsPbbTeTestApiAction ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsPbbTeTestApiContextId ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbTeTestApiTeSid ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbTeTestApiDestMacAddr ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsPbbTeTestApiSourceMacAddr ARG_LIST((UINT4 *  ,tMacAddr ));

INT1
nmhTestv2FsPbbTeTestApiEspVlanId ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbTeTestApiEgressPort ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsPbbTeTestApiEgressPortList ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsPbbTeTestApiAction ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsPbbTeTestApiContextId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbTeTestApiTeSid ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbTeTestApiDestMacAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbTeTestApiSourceMacAddr ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbTeTestApiEspVlanId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbTeTestApiEgressPort ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbTeTestApiEgressPortList ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsPbbTeTestApiAction ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
