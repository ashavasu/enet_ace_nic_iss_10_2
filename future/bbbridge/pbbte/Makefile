#####################################################################
####                                                             ####
####                                                             ####
#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved        ####
#####################################################################
####  MAKEFILE HEADER ::                                         ####
##|###############################################################|##
##| $Id: Makefile,v 1.2 2010/11/09 14:03:09 prabuc Exp            |##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.                   |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  PBB-TE                         |##
##|                                                               |##
##|    MODULE NAME             ::  TOP most level Makefile        |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  UNIX                           |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  30 September 2008              |##
##|                                                               |##
##|    DESCRIPTION             ::  MAKEFILE for Arcient           |##
##|                                PBB-TE Final Object            |##
##|                                                               |##
##|###############################################################|##

.PHONY  : clean
include ../../LR/make.h
include ../../LR/make.rule
include ./make.h

C_FLAGS           = ${CC_FLAGS} ${TOTAL_OPNS} ${INCLUDES}
PBBTE_FINAL_OBJ     = ${PBBTE_OBJ_DIR}/FuturePBBTE.o

#object module list

PBBTE_OBJS = \
        ${PBBTE_OBJ_DIR}/pbt1aylw.o \
        ${PBBTE_OBJ_DIR}/pbtapi.o \
        ${PBBTE_OBJ_DIR}/pbtsys.o \
        ${PBBTE_OBJ_DIR}/pbtport.o \
        ${PBBTE_OBJ_DIR}/pbtprplw.o \
        ${PBBTE_OBJ_DIR}/pbttesid.o \
        ${PBBTE_OBJ_DIR}/pbtespvl.o \
        ${PBBTE_OBJ_DIR}/pbtutil.o \
        ${PBBTE_OBJ_DIR}/pbtsz.o 

ifeq (${SNMP_2}, YES)
PBBTE_OBJS += \
     	  ${PBBTE_OBJ_DIR}/pbtprpwr.o \
     	  ${PBBTE_OBJ_DIR}/pbt1aywr.o
endif

ifeq (${CLI}, YES)
PBBTE_OBJS += \
       ${PBBTE_OBJ_DIR}/pbtcli.o 
endif

ifeq (${MBSM}, YES)
PBBTE_OBJS += \
     	${PBBTE_OBJ_DIR}/pbtmbsm.o
endif


ifeq (${L2RED}, YES)
PBBTE_OBJS += \
        ${PBBTE_OBJ_DIR}/pbtred.o
ifeq (${NPAPI}, YES)
PBBTE_OBJS += \
        ${PBBTE_OBJ_DIR}/pbtaudsync.o
PBBTE_OBJS += \
        ${PBBTE_OBJ_DIR}/pbtaudget.o
endif
else
PBBTE_OBJS += \
        ${PBBTE_OBJ_DIR}/pbtrdstb.o
endif


ifeq (${MBSM}, YES)
INTERNAL_DEPENDENCIES += ${PBBTE_INC_DIR}/pbtmbsm.h
endif

ifeq (${L2RED}, YES)
INTERNAL_DEPENDENCIES += ${PBBTE_INC_DIR}/pbtred.h
endif

#object module dependency
${PBBTE_FINAL_OBJ} : obj ${PBBTE_OBJS}
	${LD} ${LD_FLAGS} ${CC_COMMON_OPTIONS} -o ${PBBTE_FINAL_OBJ} ${PBBTE_OBJS}

obj:
	$(MKDIR) $(MKDIR_FLAGS) ${PBBTE_OBJ_DIR} 

EXTERNAL_DEPENDENCIES =              \
        ${COMN_INCL_DIR}/lr.h        \
        ${COMN_INCL_DIR}/pbbte.h     \
        ${COMN_INCL_DIR}/size.h      \
        ${COMN_INCL_DIR}/rmgr.h      \
        ${COMN_INCL_DIR}/params.h    

ifeq (${MBSM}, YES)
EXTERNAL_DEPENDENCIES +=${COMN_INCL_DIR}/mbsm.h
endif

INTERNAL_DEPENDENCIES = \
        ${PBBTE_BASE_DIR}/make.h \
        ${PBBTE_BASE_DIR}/Makefile \
        ${COMN_INCL_DIR}/pbbte.h \
        ${COMN_INCL_DIR}/cli/pbtcli.h \
        ${PBBTE_INC_DIR}/pbthdrs.h \
        ${PBBTE_INC_DIR}/pbt1aylw.h \
        ${PBBTE_INC_DIR}/pbtprplw.h \
        ${PBBTE_INC_DIR}/pbtprot.h \
        ${PBBTE_INC_DIR}/pbtglob.h \
        ${PBBTE_INC_DIR}/pbtmacs.h \
        ${PBBTE_INC_DIR}/pbttdfs.h \
        ${PBBTE_INC_DIR}/pbttrc.h \
        ${PBBTE_INC_DIR}/pbt1aywr.h \
        ${PBBTE_INC_DIR}/pbtprpwr.h \
        ${PBBTE_INC_DIR}/pbtsz.h
 
DEPENDENCIES =  \
    ${EXTERNAL_DEPENDENCIES} \
	 ${INTERNAL_DEPENDENCIES} \
	 ${COMMON_DEPENDENCIES}

$(PBBTE_OBJ_DIR)/pbtcli.o : \
   $(PBBTE_SRC_DIR)/pbtcli.c  \
	${DEPENDENCIES}
	$(CC) $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtcli.c -o $@

$(PBBTE_OBJ_DIR)/pbt1aylw.o : \
   $(PBBTE_SRC_DIR)/pbt1aylw.c \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbt1aylw.c -o $@

$(PBBTE_OBJ_DIR)/pbtapi.o : \
   $(PBBTE_SRC_DIR)/pbtapi.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtapi.c -o $@

$(PBBTE_OBJ_DIR)/pbtsys.o : \
   $(PBBTE_SRC_DIR)/pbtsys.c  \
	${DEPENDENCIES}
	$(CC) ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtsys.c -o $@

$(PBBTE_OBJ_DIR)/pbtport.o : \
   $(PBBTE_SRC_DIR)/pbtport.c \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtport.c -o $@

$(PBBTE_OBJ_DIR)/pbtprplw.o : \
	$(PBBTE_SRC_DIR)/pbtprplw.c \
   $(PBBTE_NPAPI_DEPEND) \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtprplw.c -o $@

$(PBBTE_OBJ_DIR)/pbttesid.o : \
   $(PBBTE_SRC_DIR)/pbttesid.c  \
	${DEPENDENCIES}
	$(CC) ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbttesid.c -o $@

$(PBBTE_OBJ_DIR)/pbtespvl.o : \
   $(PBBTE_SRC_DIR)/pbtespvl.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtespvl.c -o $@


$(PBBTE_OBJ_DIR)/pbtutil.o : \
   $(PBBTE_SRC_DIR)/pbtutil.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtutil.c -o $@

$(PBBTE_OBJ_DIR)/pbt1aywr.o : \
   $(PBBTE_SRC_DIR)/pbt1aywr.c  \
	${DEPENDENCIES}
	$(CC) ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbt1aywr.c -o $@

$(PBBTE_OBJ_DIR)/pbtprpwr.o : \
   $(PBBTE_SRC_DIR)/pbtprpwr.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtprpwr.c -o $@

$(PBBTE_OBJ_DIR)/pbtmbsm.o : \
   $(PBBTE_SRC_DIR)/pbtmbsm.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtmbsm.c -o $@

$(PBBTE_OBJ_DIR)/pbtred.o : \
   $(PBBTE_SRC_DIR)/pbtred.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtred.c -o $@

$(PBBTE_OBJ_DIR)/pbtaudsync.o : \
   $(PBBTE_SRC_DIR)/pbtaudsync.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtaudsync.c -o $@

$(PBBTE_OBJ_DIR)/pbtaudget.o : \
   $(PBBTE_SRC_DIR)/pbtaudget.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtaudget.c -o $@

$(PBBTE_OBJ_DIR)/pbtrdstb.o : \
   $(PBBTE_SRC_DIR)/pbtrdstb.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtrdstb.c -o $@
	
$(PBBTE_OBJ_DIR)/pbtsz.o : \
   $(PBBTE_SRC_DIR)/pbtsz.c  \
	${DEPENDENCIES}
	$(CC)  ${COMP_OPNS} $(C_FLAGS) $(PBBTE_SRC_DIR)/pbtsz.c -o $@
clean:
	$(RM) $(RM_FLAGS) $(PBBTE_OBJ_DIR)/*.o

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../../;\
        tar cvzf $(ISS_PKG_CREATE_DIR)/pbbte.tgz  -T ${BASE_DIR}/bbbridge/pbbte/FILES.NEW;\
        cd ${CUR_PWD};

