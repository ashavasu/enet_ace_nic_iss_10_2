#include "pbthdrs.h"
/* -------------------------------------------------------------
 *
 * Function: PbbTeNpSyncProcessSyncMsg
 *
 * -------------------------------------------------------------
 */
PUBLIC VOID
PbbTeNpSyncProcessSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    INT4                NpApiId = 0;
    unNpSync            NpSync;

    MEMSET (&NpSync, 0, sizeof (unNpSync));

    NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet, NpApiId);

    switch (NpApiId)
    {

        case NPSYNC_FS_MI_PBB_TE_HW_SET_ESP_VID:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbTeHwSetEspVid.u4ContextId);
            NPSYNC_RM_GET_2_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbTeHwSetEspVid.EspVlanId);
            PbbteHwAuditCreateOrFlushBuffer (&NpSync,
                                             NPSYNC_FS_MI_PBB_TE_HW_SET_ESP_VID,
                                             0);
            break;

        case NPSYNC_FS_MI_PBB_TE_HW_RESET_ESP_VID:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbTeHwResetEspVid.u4ContextId);
            NPSYNC_RM_GET_2_BYTE (pMsg, pu2OffSet,
                                  NpSync.FsMiPbbTeHwResetEspVid.EspVlanId);
            PbbteHwAuditCreateOrFlushBuffer (&NpSync,
                                             NPSYNC_FS_MI_PBB_TE_HW_RESET_ESP_VID,
                                             0);
            break;

        case PBBTE_CONTEXT_DELETE_SYNCUP:
            NPSYNC_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                  NpSync.ContextIdx.u4ContextId);
            PbbteHwAuditCreateOrFlushBuffer (&NpSync,
                                             0, PBBTE_CONTEXT_DELETE_SYNCUP);

            break;

        default:
            break;

    }                            /* switch */
    return;
}
