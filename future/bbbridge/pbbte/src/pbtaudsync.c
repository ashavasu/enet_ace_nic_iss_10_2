#include "pbthdrs.h"

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbTeHwSetEspVid
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbTeHwSetEspVid (UINT4 u4ContextId, tVlanId EspVlanId)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsMiPbbTeHwSetEspVid.u4ContextId = u4ContextId;
    NpSync.FsMiPbbTeHwSetEspVid.EspVlanId = EspVlanId;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBBTE_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsMiPbbTeHwSetEspVidSync (NpSync.FsMiPbbTeHwSetEspVid,
                                                RM_PBBTE_APP_ID,
                                                NPSYNC_FS_MI_PBB_TE_HW_SET_ESP_VID);
            }
        }
#undef FsMiPbbTeHwSetEspVid
        i4RetVal = FsMiPbbTeHwSetEspVid (u4ContextId, EspVlanId);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBBTE_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsMiPbbTeHwSetEspVidSync (NpSync.FsMiPbbTeHwSetEspVid,
                                                    RM_PBBTE_APP_ID,
                                                    NPSYNC_FS_MI_PBB_TE_HW_SET_ESP_VID);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBBTE_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbteHwAuditCreateOrFlushBuffer (&NpSync,
                                             NPSYNC_FS_MI_PBB_TE_HW_SET_ESP_VID,
                                             0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbTeHwResetEspVid
 *
 * -------------------------------------------------------------
 */
PUBLIC INT4
NpSyncFsMiPbbTeHwResetEspVid (UINT4 u4ContextId, tVlanId EspVlanId)
{
    unNpSync            NpSync;
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4NodeState;

    MEMSET (&NpSync, 0, sizeof (NpSync));
    NpSync.FsMiPbbTeHwResetEspVid.u4ContextId = u4ContextId;
    NpSync.FsMiPbbTeHwResetEspVid.EspVlanId = EspVlanId;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBBTE_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncFsMiPbbTeHwResetEspVidSync (NpSync.FsMiPbbTeHwResetEspVid,
                                                  RM_PBBTE_APP_ID,
                                                  NPSYNC_FS_MI_PBB_TE_HW_RESET_ESP_VID);
            }
        }
#undef FsMiPbbTeHwResetEspVid
        i4RetVal = FsMiPbbTeHwResetEspVid (u4ContextId, EspVlanId);
        if (i4RetVal == FNP_FAILURE)
        {
            if (RmGetStandbyNodeCount () != 0)
            {
                if (PBBTE_NPSYNC_BLK () == OSIX_FALSE)
                {
                    NpSyncFsMiPbbTeHwResetEspVidSync (NpSync.
                                                      FsMiPbbTeHwResetEspVid,
                                                      RM_PBBTE_APP_ID,
                                                      NPSYNC_FS_MI_PBB_TE_HW_RESET_ESP_VID);
                }
            }
        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBBTE_NPSYNC_BLK () == OSIX_FALSE)
        {
            PbbteHwAuditCreateOrFlushBuffer (&NpSync,
                                             NPSYNC_FS_MI_PBB_TE_HW_RESET_ESP_VID,
                                             0);
        }
    }
    return (i4RetVal);
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbTeHwSetEspVidSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsMiPbbTeHwSetEspVidSync (tNpSyncFsMiPbbTeHwSetEspVid
                                PbbTeHwSetEspVid, UINT4 u4AppId,
                                UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (tVlanId);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PbbTeHwSetEspVid.u4ContextId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, PbbTeHwSetEspVid.EspVlanId);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncFsMiPbbTeHwResetEspVidSync
 *
 * -------------------------------------------------------------
 */
VOID
NpSyncFsMiPbbTeHwResetEspVidSync (tNpSyncFsMiPbbTeHwResetEspVid
                                  PbbTeHwResetEspVid, UINT4 u4AppId,
                                  UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg;
    UINT2               u2OffSet;
    UINT2               u2MsgSize;

    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (tVlanId);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        NPSYNC_RM_PUT_1_BYTE (pMsg, &u2OffSet, NPSYNC_MESSAGE);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        NPSYNC_RM_PUT_4_BYTE (pMsg, &u2OffSet, PbbTeHwResetEspVid.u4ContextId);
        NPSYNC_RM_PUT_2_BYTE (pMsg, &u2OffSet, PbbTeHwResetEspVid.EspVlanId);
        if (RmEnqMsgToRmFromAppl (pMsg, u2OffSet, u4AppId, u4AppId) ==
            RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            RmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    return;
}

/* -------------------------------------------------------------
 *
 * Function: NpSyncSendDeleteContextEvent 
 *
 * -------------------------------------------------------------
 */

VOID
NpSyncSendDeleteContextEvent (UINT4 u4ContextId)
{
    tNpSyncContextIdx   ContextIdx;
    unNpSync            NpSync;
    UINT4               u4NodeState;

    MEMSET (&ContextIdx, 0, sizeof (tNpSyncContextIdx));
    ContextIdx.u4ContextId = u4ContextId;
    u4NodeState = RmGetNodeState ();
    if (u4NodeState == RM_ACTIVE)
    {
        if (RmGetStandbyNodeCount () != 0)
        {
            if (PBBTE_NPSYNC_BLK () == OSIX_FALSE)
            {
                NpSyncContextIdxSync (ContextIdx,
                                      RM_PBBTE_APP_ID,
                                      PBBTE_CONTEXT_DELETE_SYNCUP);
            }

        }
    }
    else if (u4NodeState == RM_STANDBY)
    {
        if (PBBTE_NPSYNC_BLK () == OSIX_FALSE)
        {
            MEMSET (&NpSync, 0, sizeof (NpSync));
            NpSync.ContextIdx.u4ContextId = u4ContextId;
            PbbteHwAuditCreateOrFlushBuffer (&NpSync, 0,
                                             PBBTE_CONTEXT_DELETE_SYNCUP);
        }
    }

    return;
}
