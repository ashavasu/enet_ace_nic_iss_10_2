/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbtsz.c,v 1.3 2013/11/29 11:04:11 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _PBBTESZ_C
#include "pbthdrs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
PbbteSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < PBBTE_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsPBBTESizingParams[i4SizingId].u4StructSize,
                              FsPBBTESizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(PBBTEMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            PbbteSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
PbbteSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsPBBTESizingParams);
    IssSzRegisterModulePoolId (pu1ModName, PBBTEMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
PbbteSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < PBBTE_MAX_SIZING_ID; i4SizingId++)
    {
        if (PBBTEMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (PBBTEMemPoolIds[i4SizingId]);
            PBBTEMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
