/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: pbtmbsm.c,v 1.6 2013/06/06 12:03:47 siva Exp $
 *
 *******************************************************************/
#ifdef MBSM_WANTED

#include "pbthdrs.h"

/***************************************************************************/
/* Function Name    : PbbTeMbsmPostMessage                                 */
/*                                                                           */
/* Description      : This function is an API for mbsm module to enqueue   */
/*                    packets when a new LC is inserted or removed         */
/*                                                                           */
/* Input(s)         : pProtoMsg - Queue message.                           */
/*                                                                           */
/*                          i4Event   - Event type.                          */
/*                                                                           */
/* Output(s)        : None.                                                */
/*                                                                           */
/* Exceptions or OS                                                        */
/* Error Handling   : None.                                                */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/* Returns          : MBSM_SUCCESS on success,                             */
/*                    MBSM_FAILURE otherwise.                              */
/***************************************************************************/
INT4
PbbTeMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tPbbTeQMsg         *pPbbTeQMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    if (PBBTE_IS_MODULE_INITIALISED () == PBBTE_FALSE)
    {
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    PBBTE_Q_MSG_ENTRY_ALLOC_MEMBLK (pPbbTeQMsg);

    if (NULL == pPbbTeQMsg)
    {
        PBBTE_GBL_TRC (OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_Q_BUF_MEM_FAIL);
        return MBSM_FAILURE;
    }

    PBBTE_MEM_SET (pPbbTeQMsg, 0, sizeof (tPbbTeQMsg));

    pPbbTeQMsg->u4MsgType = (UINT2) i4Event;

    if (pProtoMsg != NULL)
    {
        if (!(pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg =
              MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)))
        {
            PBBTE_Q_MSG_ENTRY_FREE_MEMBLK (pPbbTeQMsg);
            return MBSM_FAILURE;
        }

        MEMCPY (pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg, pProtoMsg,
                sizeof (tMbsmProtoMsg));
    }

    if (PBBTE_SEND_TO_QUEUE (PBBTE_CFG_QUEUE_ID,
                             (UINT1 *) &pPbbTeQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PBBTE_GBL_TRC (ALL_FAILURE_TRC | PBBTE_OS_RESOURCE_TRC,
                       PBBTE_TRC_Q_SEND_TO_FAIL);

        if (pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg != NULL)
        {
            MEM_FREE (pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg);
        }

        PBBTE_Q_MSG_ENTRY_FREE_MEMBLK (pPbbTeQMsg);

        return MBSM_FAILURE;
    }

    if (PBBTE_SEND_EVENT (PBBTE_TASK_ID, PBBTE_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        PBBTE_GBL_TRC (ALL_FAILURE_TRC, PBBTE_TRC_SEND_EVENT_FAIL);

        /* No Need to Free Message Sice it is already Queued */
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbTeMbsmUpdateOnCardInsertion                   */
/*                                                                           */
/*    Description         : This function walks all the tables in PBBTE and  */
/*                          programs the harware on line card insertion.     */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeMbsmUpdateOnCardInsertion (tMbsmPortInfo * pPortInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    tSNMP_OCTET_STRING_TYPE VlanListAll;
    UINT4               u4ByteIndex;
    UINT4               u4BitIndex;
    UINT4               u4ContextId;
    UINT2               u2VlanFlag;
    tVlanId             VlanId;
    UINT1               au1TmpAll[PBBTE_VLAN_LIST_SIZE];

    UNUSED_PARAM (pPortInfo);

    if (OSIX_FALSE != MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        return MBSM_SUCCESS;
    }

    for (u4ContextId = 0; u4ContextId < PBBTE_MAX_CONTEXTS; u4ContextId++)
    {
        if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_SHUTDOWN)
        {
            continue;
        }

        PbbTeEspVlanListGet (u4ContextId, au1TmpAll);

        VlanListAll.pu1_OctetList = &au1TmpAll[0];
        VlanListAll.i4_Length = PBBTE_VLAN_LIST_SIZE;

        for (u4ByteIndex = 0; u4ByteIndex < PBBTE_VLAN_LIST_SIZE; u4ByteIndex++)
        {
            if (VlanListAll.pu1_OctetList[u4ByteIndex] != 0)
            {
                u2VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];

                for (u4BitIndex = 0;
                     ((u4BitIndex < PBBTE_PORTS_PER_BYTE) && (u2VlanFlag != 0));
                     u4BitIndex++)
                {
                    if ((u2VlanFlag & VLAN_BIT8) != 0)
                    {
                        VlanId =
                            (UINT2) ((u4ByteIndex * PBBTE_PORTS_PER_BYTE) +
                                     u4BitIndex + 1);

                        if (FsMiPbbTeMbsmHwSetEspVid (u4ContextId, VlanId,
                                                      pSlotInfo) == FNP_FAILURE)
                        {
                            MBSM_DBG (MBSM_PROTO_TRC, MBSM_PBBTE,
                                      "Card Insertion: FsMiPbbTeMbsmHwSetEspVid "
                                      "Failed \n");

                            return MBSM_FAILURE;
                        }
                    }
                    u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                }
            }
        }
    }
    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbTeMbsmUpdateOnCardRemoval                     */
/*                                                                           */
/*    Description         : This function walks all the tables in PBBTE and  */
/*                          programs the harware on line card removal  .     */
/*                                                                           */
/*    Input(s)            : pPortInfo - Inserted port list                   */
/*                          pSlotInfo - Information about inserted slot      */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : MBSM_SUCCESS on success                           */
/*                         MBSM_FAILURE on failure                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeMbsmUpdateOnCardRemoval (tMbsmPortInfo * pPortInfo,
                              tMbsmSlotInfo * pSlotInfo)
{
    UNUSED_PARAM (pPortInfo);
    UNUSED_PARAM (pSlotInfo);
    return MBSM_SUCCESS;
}

#endif
