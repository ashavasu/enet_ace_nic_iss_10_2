#ifndef __PBTCLI_C__
#define __PBTCLI_C__

/* SOURCE FILE HEADER :
 *$Id: pbtcli.c,v 1.17 2014/03/19 13:36:12 siva Exp $
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : pbtcli.c                                        |
 * |                                                                          |
 * |  PRINCIPAL AUTHOR      : PBBTE TEAM                                      |
 * |                                                                          |
 * |  SUBSYSTEM NAME        : Pbbte                                           |
 * |                                                                          |
 * |  MODULE NAME           : Pbbte configuration                             |
 * |                                                                          |
 * |  LANGUAGE              : C                                               |
 * |                                                                          |
 * |  TARGET ENVIRONMENT    :                                                 |
 * |                                                                          |
 * |  DATE OF FIRST RELEASE :                                                 |
 * |                                                                          |
 * |  DESCRIPTION           : Action routines for set/get objects in          |
 * |                         fspbt.mib                                        |
 * |                                                                          |
 *  ---------------------------------------------------------------------------
 *
 */
#include "pbthdrs.h"

INT4
cli_process_pbbte_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[PBBTE_MAX_ARGS];
    UINT1              *pu1Inst = NULL;
    INT1                i1argno = 0;
    INT4                i4RetStatus = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4TeSid = 0;
    tMacAddr            SrcMac;
    tMacAddr            DstMac;
    UINT1              *pu1EspVlanList = NULL;

    va_start (ap, u4Command);

    /* third arguement is always interface name/index */
    pu1Inst = va_arg (ap, UINT1 *);
    UNUSED_PARAM (pu1Inst);
    /* Walk through the rest of the arguements and store in args array. 
     * Store 20 arguements at the max. This is because pbbte commands do not
     * take more than twenty inputs from the command line. Another reason to
     * store is in some cases first input may be optional but user may give 
     * second input. In that case first arg will be null and second arg only 
     * has value */
    while (1)
    {
        args[i1argno++] = va_arg (ap, UINT4 *);
        if (i1argno == PBBTE_MAX_ARGS)
            break;
    }
    va_end (ap);

    switch (u4Command)
    {

        case CLI_PBBTE_SHUT:

            u4ContextId = CLI_GET_CXT_ID ();
            i4RetStatus = PbbTeCliSetSystemControl (CliHandle,
                                                    u4ContextId,
                                                    PBBTE_SHUTDOWN);
            break;

        case CLI_PBBTE_NO_SHUT:

            u4ContextId = CLI_GET_CXT_ID ();
            i4RetStatus = PbbTeCliSetSystemControl (CliHandle,
                                                    u4ContextId, PBBTE_START);
            break;

        case CLI_PBBTE_MAP_ESP_VLAN:

            u4ContextId = CLI_GET_CXT_ID ();

            pu1EspVlanList = UtlShMemAllocVlanList ();

            if (pu1EspVlanList == NULL)
            {
                CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                           " failed.\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1EspVlanList, 0, PBBTE_VLAN_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) (args[0]), pu1EspVlanList,
                                  PBBTE_VLAN_LIST_SIZE, CFA_L2VLAN)
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                UtlShMemFreeVlanList (pu1EspVlanList);
                return CLI_FAILURE;
            }

            i4RetStatus = PbbTeCliConfigEspVlans (CliHandle,
                                                  u4ContextId,
                                                  pu1EspVlanList, PBBTE_CREATE);
            UtlShMemFreeVlanList (pu1EspVlanList);
            break;

        case CLI_PBBTE_UNMAP_ESP_VLAN:

            u4ContextId = CLI_GET_CXT_ID ();

            pu1EspVlanList = UtlShMemAllocVlanList ();

            if (pu1EspVlanList == NULL)
            {
                CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                           " failed.\r\n");
                return CLI_FAILURE;
            }

            CLI_MEMSET (pu1EspVlanList, 0, PBBTE_VLAN_LIST_SIZE);

            if (CliStrToPortList ((UINT1 *) (args[0]), pu1EspVlanList,
                                  PBBTE_VLAN_LIST_SIZE, CFA_L2VLAN)
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid Vlan List\r\n");
                UtlShMemFreeVlanList (pu1EspVlanList);
                return CLI_FAILURE;
            }

            i4RetStatus = PbbTeCliConfigEspVlans (CliHandle,
                                                  u4ContextId,
                                                  pu1EspVlanList, PBBTE_DELETE);
            UtlShMemFreeVlanList (pu1EspVlanList);
            break;

        case CLI_PBBTE_TE_SID:

            u4ContextId = CLI_GET_CXT_ID ();

            if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_SHUTDOWN)
            {
                CliPrintf (CliHandle, "%% PBBTE Module not started\r\n");
                return CLI_FAILURE;
            }
            i4RetStatus = PbbTeCliConfigTesid (CliHandle, *((UINT4 *) args[0]));
            break;

        case CLI_PBBTE_NO_TE_SID:

            i4RetStatus = PbbTeCliDeleteTesiEntry (CliHandle,
                                                   *((UINT4 *) args[0]));
            break;

        case CLI_PBBTE_MAP_ESP_TESID:

            u4TeSid = CLI_GET_TESID ();
            u4ContextId = CLI_GET_CXT_ID ();

            StrToMac ((UINT1 *) (args[1]), SrcMac);
            StrToMac ((UINT1 *) (args[2]), DstMac);

            i4RetStatus =
                PbbTeCliConfigTeSidEspEntry (CliHandle, u4ContextId,
                                             u4TeSid,
                                             *((UINT4 *) (args[0])),
                                             SrcMac, DstMac,
                                             (UINT2) *((UINT4 *) (args[3])));
            break;

        case CLI_PBBTE_UNMAP_ESP_TESID:

            u4TeSid = CLI_GET_IFINDEX ();

            i4RetStatus =
                PbbTeCliDeleteTeSidEspEntry (CliHandle,
                                             u4TeSid, *((UINT4 *) args[0]));

            break;
#ifdef TRACE_WANTED

        case CLI_PBBTE_DEBUGS:
            /* args[0] - Context Name */
            /* args[1] - Debug Type */

            if (args[0] != NULL)
            {
                if (PbbTeVcmIsSwitchExist ((UINT1 *) (args[0]), &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = VLAN_DEF_CONTEXT_ID;
            }

            i4RetStatus = PbbTeCliSetDebugs (CliHandle, u4ContextId,
                                             CLI_PTR_TO_U4 (args[1]),
                                             PBBTE_SET_CMD);
            break;

        case CLI_PBBTE_NO_DEBUGS:
            /* args[0] - Context Name */
            /* args[1] - Debug Type */

            if (args[0] != NULL)
            {
                if (PbbTeVcmIsSwitchExist ((UINT1 *) (args[0]), &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = VLAN_DEF_CONTEXT_ID;
            }

            i4RetStatus = PbbTeCliSetDebugs (CliHandle, u4ContextId,
                                             CLI_PTR_TO_U4 (args[1]),
                                             PBBTE_NO_CMD);
            break;

        case CLI_PBBTE_GLOBAL_DEBUGS:

            i4RetStatus = PbbTeCliSetGlobalDebugs (CliHandle,
                                                   PBBTE_TRACE_ENABLED);
            break;

        case CLI_PBBTE_NO_GLOBAL_DEBUGS:

            i4RetStatus = PbbTeCliSetGlobalDebugs (CliHandle,
                                                   PBBTE_TRACE_DISABLED);
            break;
#else
        case CLI_PBBTE_DEBUGS:
        case CLI_PBBTE_NO_DEBUGS:
        case CLI_PBBTE_GLOBAL_DEBUGS:
        case CLI_PBBTE_NO_GLOBAL_DEBUGS:

            CliPrintf (CliHandle, "\r%% Debug not supported \r\n");
            i4RetStatus = CLI_FAILURE;
            break;
#endif /*TRACE_WANTED */

        case CLI_PBBTE_SHOW_TE_STATUS:

            if (args[0] != NULL)
            {
                if (PbbTeVcmIsSwitchExist ((UINT1 *) (args[0]), &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                i4RetStatus =
                    PbbTeCliShowModuleStatusInCtxt (CliHandle, u4ContextId);
            }
            else
            {
                i4RetStatus = PbbTeCliShowModuleGlobalStatus (CliHandle);
            }
            break;

        case CLI_PBBTE_SHOW_ESP_VLANS:

            if (args[0] != NULL)
            {
                if (PbbTeVcmIsSwitchExist ((UINT1 *) (args[0]), &u4ContextId) !=
                    VCM_TRUE)
                {
                    CliPrintf (CliHandle,
                               "Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                i4RetStatus =
                    PbbTeCliShowEspVlanIDsInCtxt (CliHandle, u4ContextId);
            }
            else
            {
                i4RetStatus = PbbTeCliShowEspIDsGlobal (CliHandle);
            }
            break;
        case CLI_PBBTE_SHOW_TESID_ESPID:

            i4RetStatus = PbbTeCliShowTesidEspID (CliHandle,
                                                  *((UINT4 *) args[0]),
                                                  *((UINT4 *) args[1]));
            break;
        case CLI_PBBTE_SHOW_TESID_ESPID_DETAIL:

            i4RetStatus =
                PbbTeCliShowTesidEspIdDetail (CliHandle,
                                              *((UINT4 *) args[0]),
                                              *((UINT4 *) args[1]));
            break;
        case CLI_PBBTE_SHOW_TESID_DETAIL:

            i4RetStatus = PbbTeCliShowTesidDetail (CliHandle,
                                                   *((UINT4 *) args[0]));
            break;
        case CLI_PBBTE_SHOW_TESID:

            i4RetStatus = PbbTeCliShowTeSid (CliHandle, *((UINT4 *) args[0]));
            break;
        case CLI_PBBTE_SHOW_TESI_ENTRY_DETAIL:

            i4RetStatus = PbbTeCliShowTeSiTableDetail (CliHandle);
            break;
        case CLI_PBBTE_SHOW_TESI_ENTRY:

            i4RetStatus = PbbTeCliShowTeSiTable (CliHandle);
            break;
        default:
            i4RetStatus = CLI_FAILURE;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < PBBTE_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", PbbTeCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    return CLI_SUCCESS;

}

/*************************************************************************/
/*  FUNCTION NAME    : PbbteCliSetSystemControl                          */
/*                                                                       */
/*  DESCRIPTION      : This function will start/shut pbbte module        */
/*                                                                       */
/*  INPUT            : u4ContextId - Virtual context in which            */
/*                                   pbbte is going to start             */
/*                   i4Status - Pbbte Module status                    */
/*                                                                       */
/*  OUTPUT           : CliHandle - Contains error messages               */
/*                                                                       */
/*  RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                       */
/*************************************************************************/
INT4
PbbTeCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                          INT4 i4Status)
{

    UINT4               u4ErrorCode = PBBTE_INIT_VAL;

    if (nmhTestv2FsPbbTeContextSystemControl (&u4ErrorCode, u4ContextId,
                                              i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPbbTeContextSystemControl (u4ContextId,
                                           i4Status) == SNMP_FAILURE)
        /* if i4Status == PBBT_START, then the PBBTE module will
         * be started */
    {
        CLI_FATAL_ERROR (CliHandle);
        return (CLI_FAILURE);
    }

    return (CLI_SUCCESS);
}

/**************************************************************************/
/*     FUNCTION NAME    : PbbTeConfigEspVlans                             */
/*                                                                        */
/*     DESCRIPTION      : This function will configure / delete a set of 
 *                        vlan as ESP VLANS.                              */
/*                                                                        */
/*     INPUT            : u4ContextId - Virutal context in which vlans    */
/*                      are going to group as a ESP vlans               */
/*               pau1VlanList - Pointer to the list of vlans 
 *               u1Action - Create / Delete action to be 
 *               performed                                    
 *                                                               */
/*     OUTPUT           : CliHandle - Contains error messages             */
/*                                                                        */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                         */
/*                                                                        */
/**************************************************************************/
INT4
PbbTeCliConfigEspVlans (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT1 *pau1VlanList, UINT1 u1Action)
{
    tSNMP_OCTET_STRING_TYPE VlanListAll;
    UINT4               u4ErrorCode = 0;
    UINT4               u4ByteIndex = 0;
    UINT4               u4BitIndex = 0;
    UINT2               u2VlanFlag = 0;
    tVlanId             VlanId = 0;
    UINT1              *pu1TmpAll = NULL;
    UINT1               u1ErrCode = 0;
    INT4                i4RetStatus = 0;
    INT4                i4RowStatus = 0;
    BOOL1               bEspNotActive = 0;

    if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_SHUTDOWN)
    {
        CLI_SET_ERR (PBBTE_MODULE_SHUTDOWN);
        return CLI_FAILURE;
    }

    pu1TmpAll = UtlShMemAllocVlanList ();
    if (pu1TmpAll == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    MEMSET (pu1TmpAll, 0, PBBTE_VLAN_LIST_SIZE);

    VlanListAll.pu1_OctetList = pu1TmpAll;
    VlanListAll.i4_Length = PBBTE_VLAN_LIST_SIZE;

    CLI_MEMCPY (VlanListAll.pu1_OctetList, pau1VlanList, VlanListAll.i4_Length);

    for (u4ByteIndex = 0; u4ByteIndex < PBBTE_VLAN_LIST_SIZE; u4ByteIndex++)
    {
        if (VlanListAll.pu1_OctetList[u4ByteIndex] != 0)
        {
            u2VlanFlag = VlanListAll.pu1_OctetList[u4ByteIndex];

            for (u4BitIndex = 0;
                 ((u4BitIndex < PBBTE_PORTS_PER_BYTE) && (u2VlanFlag != 0));
                 u4BitIndex++)
            {
                if ((u2VlanFlag & VLAN_BIT8) != 0)
                {
                    VlanId =
                        (UINT2) ((u4ByteIndex * PBBTE_PORTS_PER_BYTE) +
                                 u4BitIndex + 1);
                    switch (u1Action)
                    {
                        case PBBTE_CREATE:

                            if (nmhGetFsPbbTeEspVidRowStatus (u4ContextId,
                                                              VlanId,
                                                              &i4RowStatus)
                                == SNMP_SUCCESS)
                            {
                                if (i4RowStatus == PBBTE_ACTIVE)
                                {
                                    u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                                    continue;
                                }
                            }

                            if (nmhTestv2FsPbbTeEspVidRowStatus
                                (&u4ErrorCode, u4ContextId, VlanId,
                                 PBBTE_CREATE_AND_GO) == SNMP_FAILURE)
                            {
                                if (bEspNotActive == 0)
                                {
                                    CliPrintf (CliHandle, "\r%% Some vlans in "
                                               "this list are not active "
                                               "in this switch.\r\n");
                                    bEspNotActive = 1;
                                }
                                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                                continue;
                            }

                            if (nmhSetFsPbbTeEspVidRowStatus
                                (u4ContextId,
                                 VlanId, PBBTE_CREATE_AND_GO) == SNMP_FAILURE)
                            {
                                UtlShMemFreeVlanList (pu1TmpAll);
                                CLI_FATAL_ERROR (CliHandle);
                                return CLI_FAILURE;
                            }

                            i4RetStatus = PbbTeVlanCfgLearningFlooding
                                (u4ContextId, VlanId, VLAN_DISABLED,
                                 &u1ErrCode);

                            if (i4RetStatus != PBBTE_SUCCESS)
                            {
                                nmhSetFsPbbTeEspVidRowStatus (u4ContextId,
                                                              VlanId,
                                                              PBBTE_DESTROY);

                                PbbTeCliVlanFailurePrintMesg (CliHandle,
                                                              u1ErrCode);
                                UtlShMemFreeVlanList (pu1TmpAll);
                                return CLI_FAILURE;

                            }

                            i4RetStatus = PbbTeMstpMapVidToMstId
                                (u4ContextId, VlanId, PBBTE_MSTID);

                            if (i4RetStatus != PBBTE_SUCCESS)
                            {
                                nmhSetFsPbbTeEspVidRowStatus (u4ContextId,
                                                              VlanId,
                                                              PBBTE_DESTROY);
                                PbbTeVlanCfgLearningFlooding
                                    (u4ContextId, VlanId, VLAN_ENABLED,
                                     &u1ErrCode);

                                PbbTeCliVlanFailurePrintMesg (CliHandle,
                                                              u1ErrCode);
                                UtlShMemFreeVlanList (pu1TmpAll);
                                return CLI_FAILURE;

                            }

                            PbbTeMrpRemVlanAddedInEsp (u4ContextId, VlanId);
                            break;

                        case PBBTE_DELETE:

                            if (nmhGetFsPbbTeEspVidRowStatus (u4ContextId,
                                                              VlanId,
                                                              &i4RowStatus)
                                == SNMP_FAILURE)
                            {
                                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                                continue;
                            }

                            if (nmhTestv2FsPbbTeEspVidRowStatus
                                (&u4ErrorCode, u4ContextId, VlanId,
                                 PBBTE_DESTROY) == SNMP_FAILURE)
                            {
                                if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)

                                {
                                    CliPrintf (CliHandle, "\r%% ESP Vlan can't"
                                               " be deleted - Some TeSid entries"
                                               " are configured in this Vlan\r\n");
                                }
                                else
                                {
                                    CliPrintf (CliHandle, "\r%% Some vlans in "
                                               "this list are Invalid\r\n");
                                }
                                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
                                continue;
                            }
                            if (nmhSetFsPbbTeEspVidRowStatus (u4ContextId,
                                                              VlanId,
                                                              PBBTE_DESTROY)
                                == SNMP_FAILURE)
                            {
                                UtlShMemFreeVlanList (pu1TmpAll);
                                CLI_FATAL_ERROR (CliHandle);
                                return CLI_FAILURE;
                            }
                            PbbTeVlanCfgLearningFlooding
                                (u4ContextId, VlanId, VLAN_ENABLED, &u1ErrCode);
                            PbbTeCliVlanFailurePrintMesg (CliHandle, u1ErrCode);

                            PbbTeVlanDelStFiltEntStatusOther
                                (u4ContextId, VlanId, &u1ErrCode);
                            PbbTeCliVlanFailurePrintMesg (CliHandle, u1ErrCode);

                            PbbTeMstpMapVidToMstId (u4ContextId, VlanId,
                                                    PBBTE_CISTID);
                            /* Invoke MRP to propagate the VLANs, since the Vlan
                             * is removed 4m ESP vlan list
                             * */
                            PbbTeMrpPropVlanDelFromEsp (u4ContextId, VlanId);
                            break;

                        default:
                            UtlShMemFreeVlanList (pu1TmpAll);
                            return CLI_FAILURE;
                    }

                }
                u2VlanFlag = (UINT2) (u2VlanFlag << 1);
            }
        }
    }
    UtlShMemFreeVlanList (pu1TmpAll);
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbTeCliConfigTesid                               */
/*                                                                          */
/*     DESCRIPTION      : This function will change the cli mode to TESI    */
/*                        mode.                                             */
/*                                                                          */
/*     INPUT            : u4ContextId - Virutal context in which TESID      */
/*                                      entry is will be  created.          */
/*               u4TeSid     - Traffic-Engineering Service-Instance*/
/*                             Identifier.                         */
/*                                                                          */
/*     OUTPUT           : CliHandle   - Contains error messages             */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbTeCliConfigTesid (tCliHandle CliHandle, UINT4 u4TeSidId)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    UNUSED_PARAM (CliHandle);

    /* ENTER TESI Mode */
    SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_PBBTE_TESI_MODE, u4TeSidId);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "Unable to enter into TESI "
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbTeCliDeleteTesiEntry                           */
/*                                                                          */
/*     DESCRIPTION      : This function will delete the given TESI entry    */
/*                                                                          */
/*     INPUT            :  u4TeSid     - Traffic-Engineering                */
/*                                       Service-Instance Identifier.       */
/*                                                                          */
/*     OUTPUT           : CliHandle   - Contains error messages             */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbTeCliDeleteTesiEntry (tCliHandle CliHandle, UINT4 u4TeSidId)
{
    UINT4               u4NextTeSidId;
    UINT4               u4NextEspId;
    UINT4               u4EspId;
    UINT4               u4ErrorCode;
    INT4                i4EntryFound = PBBTE_FALSE;

    UNUSED_PARAM (CliHandle);

    if (nmhGetNextIndexFsPbbTeTeSidExtTable (u4TeSidId,
                                             &u4NextTeSidId,
                                             0, &u4NextEspId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%% TeSid entry with TESID %u is not present\r\n",
                   u4TeSidId);
        return CLI_FAILURE;
    }

    do
    {
        if (u4TeSidId != u4NextTeSidId)
        {
            break;
        }
        if (nmhTestv2Ieee8021PbbTeTeSidRowStatus (&u4ErrorCode,
                                                  u4NextTeSidId,
                                                  u4NextEspId,
                                                  PBBTE_DESTROY)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021PbbTeTeSidRowStatus (u4NextTeSidId,
                                               u4NextEspId,
                                               PBBTE_DESTROY) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        u4TeSidId = u4NextTeSidId;
        u4EspId = u4NextEspId;
        i4EntryFound = PBBTE_TRUE;

    }
    while (nmhGetNextIndexFsPbbTeTeSidExtTable (u4TeSidId,
                                                &u4NextTeSidId,
                                                u4EspId,
                                                &u4NextEspId) != SNMP_FAILURE);
    if (i4EntryFound == PBBTE_FALSE)
    {
        CliPrintf (CliHandle, "%% TeSid entry with TESID %u is not present\r\n",
                   u4TeSidId);
    }
    return CLI_SUCCESS;
}

/****************************************************************************/
/*     FUNCTION NAME    : PbbTeCliConfigTeSidEspEntry                       */
/*                                                                          */
/*     DESCRIPTION      : This function will create the given TESI entry    */
/*                        with ESP entry                                    */
/*                                                                          */
/*     INPUT            : u4ContextId - Virutal context in which TESID      */
/*                                      entry is will be  created.          */
/*               u4TeSid     - Traffic-Engineering Service-Instance         */
/*                             Identifier.                                  */
/*               u4EspId     - Esp Identifier.                              */
/*               SA          - Source CBP mac address.                      */
/*               VID         - ESP vlan Id                                  */
/*                                                                          */
/*                                                                          */
/*     OUTPUT           : CliHandle   - Contains error messages             */
/*                                                                          */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                           */
/*                                                                          */
/****************************************************************************/
INT4
PbbTeCliConfigTeSidEspEntry (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4TeSidId, UINT4 u4EspId, tMacAddr SrcAddr,
                             tMacAddr DstAddr, UINT2 u2EspVid)
{
    tSNMP_OCTET_STRING_TYPE PbbTeSidEsp;
    tSNMP_OCTET_STRING_TYPE OldPbbTeSidEsp;
    UINT1               au1PbbTeSidEsp[PBBTE_ESP_LENGTH];
    UINT1               au1OldPbbTeSidEsp[PBBTE_ESP_LENGTH];
    UINT1               u1ModfiyFlag = PBBTE_FALSE;
    UINT4               u4ErrorCode = 0;
    UINT2               u2EspId = 0;
    INT4                i4RowStatus = 0;

    MEMSET (au1PbbTeSidEsp, PBBTE_INIT_VAL, sizeof (au1PbbTeSidEsp));
    PbbTeSidEsp.i4_Length = PBBTE_ESP_LENGTH;

    PbbTeSidEsp.pu1_OctetList = au1PbbTeSidEsp;

    MEMSET (au1OldPbbTeSidEsp, PBBTE_INIT_VAL, sizeof (au1OldPbbTeSidEsp));
    OldPbbTeSidEsp.i4_Length = PBBTE_ESP_LENGTH;

    OldPbbTeSidEsp.pu1_OctetList = au1OldPbbTeSidEsp;

    MEMCPY (PbbTeSidEsp.pu1_OctetList, SrcAddr, 6);
    MEMCPY (PbbTeSidEsp.pu1_OctetList + 6, DstAddr, 6);

    u2EspId = OSIX_HTONS (u2EspVid);
    MEMCPY (PbbTeSidEsp.pu1_OctetList + PBBTE_ESP_VLAN_OFFSET, &u2EspId,
            PBBTE_VLAN_ID_SIZE);

    if (nmhGetIeee8021PbbTeTeSidRowStatus (u4TeSidId, u4EspId, &i4RowStatus)
        == SNMP_SUCCESS)
    {
        nmhGetIeee8021PbbTeTeSidEsp (u4TeSidId, u4EspId, &OldPbbTeSidEsp);
        u1ModfiyFlag = PBBTE_TRUE;
    }
    /* Entry is not present. Create New entry */
    if (u1ModfiyFlag == PBBTE_FALSE)
    {
        if (nmhTestv2Ieee8021PbbTeTeSidRowStatus (&u4ErrorCode,
                                                  u4TeSidId,
                                                  u4EspId,
                                                  PBBTE_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId,
                                               u4EspId,
                                               PBBTE_CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsPbbTeTeSidExtContextId (&u4ErrorCode, u4TeSidId, u4EspId,
                                               u4ContextId) == SNMP_FAILURE)
        {
            nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId, u4EspId,
                                               PBBTE_DESTROY);

            return CLI_FAILURE;
        }

        if (nmhSetFsPbbTeTeSidExtContextId (u4TeSidId, u4EspId, u4ContextId)
            == SNMP_FAILURE)
        {
            nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId, u4EspId,
                                               PBBTE_DESTROY);

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2Ieee8021PbbTeTeSidEsp (&u4ErrorCode, u4TeSidId,
                                            u4EspId,
                                            &PbbTeSidEsp) == SNMP_FAILURE)
        {
            nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId, u4EspId,
                                               PBBTE_DESTROY);

            return CLI_FAILURE;
        }

        if (nmhSetIeee8021PbbTeTeSidEsp (u4TeSidId,
                                         u4EspId, &PbbTeSidEsp) == SNMP_FAILURE)
        {
            nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId, u4EspId,
                                               PBBTE_DESTROY);

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId,
                                               u4EspId,
                                               PBBTE_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId, u4EspId,
                                               PBBTE_DESTROY);

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else                        /* Entry is Present. So modify the Previous entry with given entry */
    {
        if (nmhTestv2FsPbbTeTeSidExtContextId (&u4ErrorCode, u4TeSidId, u4EspId,
                                               u4ContextId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* Here no need to call nmhSetFsPbbTeTeSidExtContextId, because 
         * already this Tesid Entry is created and mapped to some other context,
         * so mapping of this TeSid entry to some other context is not allowed. 
         * This check is done inside the nmhTestv2FsPbbTeTeSidExtContextId 
         * routine itself. */
        if (nmhTestv2Ieee8021PbbTeTeSidEsp (&u4ErrorCode, u4TeSidId,
                                            u4EspId,
                                            &PbbTeSidEsp) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetIeee8021PbbTeTeSidEsp (u4TeSidId,
                                         u4EspId, &PbbTeSidEsp) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId,
                                               u4EspId,
                                               PBBTE_ACTIVE) == SNMP_FAILURE)
        {
            nmhSetIeee8021PbbTeTeSidEsp (u4TeSidId, u4EspId, &OldPbbTeSidEsp);

            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*     FUNCTION NAME    : PbbTeCliDeleteTeSidEspEntry                     */
/*                                                                        */
/*     DESCRIPTION      : This function will delete the given TESI entry  */
/*                        with ESP entry                                  */
/*                                                                        */
/*     INPUT            : u4ContextId - Virutal context in which TESID    */
/*                                      entry is will be  created.        */
/*               u4TeSid     - Traffic-Engineering Service-Instance       */
/*                             Identifier.                                */
/*               u4EspId     - Esp Identifier.                            */
/*                                                                        */
/*                                                                        */
/*     OUTPUT           : CliHandle   - Contains error messages           */
/*                                                                        */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                         */
/*                                                                        */
/**************************************************************************/
INT4
PbbTeCliDeleteTeSidEspEntry (tCliHandle CliHandle, UINT4 u4TeSidId,
                             UINT4 u4EspId)
{
    UINT4               u4ErrorCode;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Ieee8021PbbTeTeSidRowStatus (&u4ErrorCode,
                                              u4TeSidId,
                                              u4EspId,
                                              PBBTE_DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetIeee8021PbbTeTeSidRowStatus (u4TeSidId,
                                           u4EspId,
                                           PBBTE_DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

#ifdef TRACE_WANTED
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliSetDebugs                                  */
/*                                                                           */
/*     DESCRIPTION      : This function configures/deconfigures debug level  */
/*                                                                           */
/*     INPUT            : u4DbgMod   - Submodule for which Trace is modified */
/*                        u4DbgValue - Type of the Trace that is modified    */
/*                        u1Action   - Set/Re-set action                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliSetDebugs (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4DbgValue,
                   UINT1 u1Action)
{
    UINT4               u4CurDbgValue;
    UINT4               u4NewDbgValue = 0;
    UINT4               u4NewDbgType;
    UINT4               u4ErrCode;

    if (nmhGetFsPbbTeContextTraceOption (u4ContextId, &u4CurDbgValue)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u1Action == PBBTE_SET_CMD)
    {
        u4NewDbgValue = u4DbgValue | u4CurDbgValue;
    }
    else
    {
        /* reset the trace value bits */
        u4NewDbgType = u4CurDbgValue & 0x0000FFFF;
        u4NewDbgType = ~u4DbgValue & u4NewDbgType;

        u4NewDbgType = 0xFFFF0000 | u4NewDbgType;
        u4NewDbgValue = u4CurDbgValue & u4NewDbgType;

    }

    if (nmhTestv2FsPbbTeContextTraceOption (&u4ErrCode, u4ContextId,
                                            (INT4) u4NewDbgValue)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsPbbTeContextTraceOption (u4ContextId, (INT4) u4NewDbgValue)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliSetGlobalDebugs                            */
/*                                                                           */
/*     DESCRIPTION      : This function configures/deconfigures debug level 
                          globally irrespective of  the contexts             */
/*                                                                           */
/*     INPUT            : u1Action   - Set/Re-set action                     */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliSetGlobalDebugs (tCliHandle CliHandle, UINT1 u1Action)
{
    UINT4               u4ErrCode;

    UNUSED_PARAM (CliHandle);

    if (nmhTestv2FsPbbTeGlobalTraceOption (&u4ErrCode, u1Action)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhSetFsPbbTeGlobalTraceOption (u1Action);

    return CLI_SUCCESS;
}
#endif /* TRACE_WANTED */
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowModuleStatusInCtxt                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays the PBBTE status 
                          (start/shutdown) in the given virtual context.     */
/*                                                                           */
/*     INPUT            : u4ContextId  - Context Id, to which context the    */
/*                                         status of the PBBTE module will be*/
/*                                         shown.                            */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowModuleStatusInCtxt (tCliHandle CliHandle, UINT4 u4ContextId)
{
    UINT1               au1ContextName[PBBTE_SWITCH_ALIAS_LEN];
    UINT4               u4NumOfEspsCreated = 0;
    UINT4               u4NumOfEspsActive = 0;
    UINT4               u4NumOfEspsDeleted = 0;

    PBBTE_MEM_SET (au1ContextName, PBBTE_INIT_VAL, PBBTE_SWITCH_ALIAS_LEN);

    if (PbbTeVcmGetAliasName (u4ContextId, au1ContextName) == VCM_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nSwitch  %-12s\r\n\r\n", au1ContextName);

        if (PbbTeVcmIsSwitchExist (au1ContextName, &u4ContextId) != VCM_TRUE)
        {
            return CLI_FAILURE;
        }

        if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_START)
        {
            CliPrintf (CliHandle, "Module status                ");
            CliPrintf (CliHandle, ": Started\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Module status                ");
            CliPrintf (CliHandle, ": ShutDown\r\n");
        }

        nmhGetFsPbbTeContextNoOfCreatedEsps (u4ContextId, &u4NumOfEspsCreated);
        nmhGetFsPbbTeContextNoOfActiveEsps (u4ContextId, &u4NumOfEspsActive);
        nmhGetFsPbbTeContextNoOfDeletedEsps (u4ContextId, &u4NumOfEspsDeleted);

        CliPrintf (CliHandle, "Total Number of ESPs "
                   "Created : %d\r\n", u4NumOfEspsCreated);
        CliPrintf (CliHandle, "Total Number of ESPs "
                   "Active  : %d\r\n", u4NumOfEspsActive);
        CliPrintf (CliHandle, "Total Number of ESPs "
                   "Deleted : %d\r\n", u4NumOfEspsDeleted);
        CliPrintf (CliHandle, "\r\n");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowModuleGlobalStatus                     */
/*                                                                           */
/*     DESCRIPTION      : This function displays the PBBTE status 
                          (start/shutdown) global irrespective of context.   */
/*                                                                           */
/*     INPUT            :                              */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowModuleGlobalStatus (tCliHandle CliHandle)
{
    UINT4               u4ContextId;

    PBBTE_SCAN_ALL_CONTEXT (u4ContextId)
    {
        if (PBBTE_IS_VC_VALID ((INT4) u4ContextId) == PBBTE_TRUE)
        {
            PbbTeCliShowModuleStatusInCtxt (CliHandle, u4ContextId);
        }
    }

    return CLI_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowEspVlanIDsInCtxt                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the ESP-VIDs that has been 
 *                        grouped in the given virtual context.              */
/*                                                                           */
/*     INPUT            : u4ContextId  - To display the ESP-VIDs that has 
 *                                       been grouped for this particular 
 *                                       context that specified here.        */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowEspVlanIDsInCtxt (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE VlanListAll;
    INT4                i4NextContextId;
    INT4                i4NextEspVid;
    UINT2               u2EspVid = 0;
    UINT1               au1ContextName[PBBTE_SWITCH_ALIAS_LEN];
    UINT1              *pu1EspVlanList = NULL;
    INT4                i4CommaCount = 0;
    BOOL1               bSeprtr = PBBTE_TRUE;

    pu1EspVlanList = UtlShMemAllocVlanList ();
    if (pu1EspVlanList == NULL)
    {
        CliPrintf (CliHandle,
                   "\r%% pu1EspVlanList:Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    PBBTE_MEM_SET (pu1EspVlanList, PBBTE_INIT_VAL, PBBTE_VLAN_LIST_SIZE);

    VlanListAll.pu1_OctetList = pu1EspVlanList;
    VlanListAll.i4_Length = PBBTE_VLAN_LIST_SIZE;

    bSeprtr = PBBTE_FALSE;

    if (nmhGetNextIndexFsPbbTeEspVidTable ((INT4) u4ContextId,
                                           &i4NextContextId,
                                           0, &i4NextEspVid) == SNMP_FAILURE)
    {
        UtlShMemFreeVlanList (pu1EspVlanList);
        return CLI_FAILURE;
    }

    PBBTE_MEM_SET (au1ContextName, PBBTE_INIT_VAL, PBBTE_SWITCH_ALIAS_LEN);

    PbbTeVcmGetAliasName (u4ContextId, au1ContextName);

    CliPrintf (CliHandle, "\r\nSwitch %-8s\r\n\r\n", au1ContextName);
    CliPrintf (CliHandle, "   ESP Vlans : ");

    do
    {
        if (u4ContextId != (UINT4) i4NextContextId)
        {
            break;
        }

        OSIX_BITLIST_SET_BIT (VlanListAll.pu1_OctetList, (UINT2) i4NextEspVid,
                              PBBTE_VLAN_LIST_SIZE);

        u2EspVid = (UINT2) i4NextEspVid;
    }
    while (nmhGetNextIndexFsPbbTeEspVidTable ((INT4) u4ContextId,
                                              &i4NextContextId,
                                              (INT4) u2EspVid,
                                              &i4NextEspVid) == SNMP_SUCCESS);

    PbbTeCliDisplayOctetToVlan (CliHandle, VlanListAll.pu1_OctetList,
                                VlanListAll.i4_Length, bSeprtr, &i4CommaCount);
    CliPrintf (CliHandle, "\r\n\r\n");
    UtlShMemFreeVlanList (pu1EspVlanList);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowEspIDsGlobal                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the ESP-VIDs that are       */
/*                        grouped in all the context.                        */
/*                                                                           */
/*     INPUT            :                                                    */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowEspIDsGlobal (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE VlanListAll;
    INT4                i4NextContextId = 0;
    INT4                i4NextEspVid = 0;
    INT4                i4EspVid = 0;
    UINT1              *pu1EspVlanList = NULL;
    UINT1               au1ContextName[PBBTE_SWITCH_ALIAS_LEN];
    INT4                i4CommaCount = 0;
    UINT4               u4ContextId = 0;
    BOOL1               bEspVlanPresent = 0;
    BOOL1               bSeprtr = PBBTE_TRUE;

    pu1EspVlanList = UtlShMemAllocVlanList ();

    if (pu1EspVlanList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    PBBTE_MEM_SET (pu1EspVlanList, PBBTE_INIT_VAL, PBBTE_VLAN_LIST_SIZE);

    VlanListAll.pu1_OctetList = pu1EspVlanList;
    VlanListAll.i4_Length = PBBTE_VLAN_LIST_SIZE;

    if (nmhGetNextIndexFsPbbTeEspVidTable (0,
                                           &i4NextContextId,
                                           0, &i4NextEspVid) == SNMP_FAILURE)
    {
        UtlShMemFreeVlanList (pu1EspVlanList);
        return CLI_SUCCESS;
    }

    do
    {
        /* If the loop goes to search the next context, it should 
         * print the ESP-VLANs of the prev context before printing 
         * the next context */

        if ((i4NextContextId != (INT4) u4ContextId) && (bEspVlanPresent == 1))
        {
            PBBTE_MEM_SET (au1ContextName, PBBTE_INIT_VAL,
                           PBBTE_SWITCH_ALIAS_LEN);

            PbbTeVcmGetAliasName (u4ContextId, au1ContextName);

            CliPrintf (CliHandle, "\r\nSwitch %-8s\r\n\r\n", au1ContextName);
            CliPrintf (CliHandle, "   ESP Vlans : ");

            bSeprtr = PBBTE_FALSE;
            i4CommaCount = 0;

            PbbTeCliDisplayOctetToVlan (CliHandle,
                                        VlanListAll.pu1_OctetList,
                                        VlanListAll.i4_Length, bSeprtr,
                                        &i4CommaCount);
            CliPrintf (CliHandle, "\r\n");

            PBBTE_MEM_SET (pu1EspVlanList, PBBTE_INIT_VAL,
                           PBBTE_VLAN_LIST_SIZE);

        }
        i4EspVid = i4NextEspVid;
        u4ContextId = i4NextContextId;

        OSIX_BITLIST_SET_BIT (VlanListAll.pu1_OctetList, (UINT2) i4EspVid,
                              PBBTE_VLAN_LIST_SIZE);
        bEspVlanPresent = 1;
    }
    while (nmhGetNextIndexFsPbbTeEspVidTable ((INT4) u4ContextId,
                                              &i4NextContextId,
                                              i4EspVid,
                                              &i4NextEspVid) == SNMP_SUCCESS);

    /* It will print the ESP-VLANs for the last contexr */
    PBBTE_MEM_SET (au1ContextName, PBBTE_INIT_VAL, PBBTE_SWITCH_ALIAS_LEN);

    PbbTeVcmGetAliasName (u4ContextId, au1ContextName);

    CliPrintf (CliHandle, "\r\nSwitch %-8s\r\n\r\n", au1ContextName);
    CliPrintf (CliHandle, "   ESP Vlans : ");

    bSeprtr = PBBTE_FALSE;
    i4CommaCount = 0;
    PbbTeCliDisplayOctetToVlan (CliHandle, VlanListAll.pu1_OctetList,
                                VlanListAll.i4_Length, bSeprtr, &i4CommaCount);
    CliPrintf (CliHandle, "\r\n\r\n");
    UtlShMemFreeVlanList (pu1EspVlanList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowTesidEspIdDetail                       */
/*                                                                           */
/*     DESCRIPTION      : This function displays the TESID ESP entry for     */
/*                        the given TeSid, EspId Combination with the ports  */
/*                        that configured in data path.                      */
/*                                                                           */
/*     INPUT            : u4TeSid - Traffic-enginnering Service-instance     */
/*                                 Identifier.                               */
/*                        u4EspId - Ethernet-switched path Identifier.       */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowTesidEspIdDetail (tCliHandle CliHandle, UINT4 u4TeSid,
                              UINT4 u4EspId)
{
    INT4                i4TeSidRowStatus;

    nmhGetIeee8021PbbTeTeSidRowStatus (u4TeSid, u4EspId, &i4TeSidRowStatus);

    if (i4TeSidRowStatus == PBBTE_ACTIVE)
    {
        CliPrintf (CliHandle, "\r\nTESID %u", u4TeSid);
        CliPrintf (CliHandle, "\r\n---------------\r\n");

        PbbTeCliPrintTeSidEntInDetail (CliHandle, u4TeSid, u4EspId);
        CliPrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowTesidEspID                             */
/*                                                                           */
/*     DESCRIPTION      : This function displays the TESID ESP entry for     */
/*                        the given TeSid, EspId Combination without the     */
/*                        ports that has configured in data path.            */
/*                                                                           */
/*     INPUT            :   u4TeSid - Traffic-enginnering Service-instance   */
/*                                   Identifier.                             */
/*                          u4EspId - Ethernet-switched path Identifier.     */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowTesidEspID (tCliHandle CliHandle, UINT4 u4TeSid, UINT4 u4EspId)
{
    INT4                i4TeSidRowStatus;

    CliPrintf (CliHandle,
               "\r\n V - Volatile  NV - Non-Volatile  P - Permenant ");
    CliPrintf (CliHandle, " RO - Read-Only  O - Other\r\n");

    CliPrintf (CliHandle, "\r\nTeSid        EspIndex     Destination Mac    "
               "Source Mac         Vlan  Status\r\n");
    CliPrintf (CliHandle, "-----------  -----------  -----------------  "
               "-----------------  ----  ------\r\n");
    nmhGetIeee8021PbbTeTeSidRowStatus (u4TeSid, u4EspId, &i4TeSidRowStatus);

    if (i4TeSidRowStatus == PBBTE_ACTIVE)
    {
        PbbTeCliDisplayTeSidEntry (CliHandle, u4TeSid, u4EspId);
        CliPrintf (CliHandle, "\r\n\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowTesidDetail                            */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  ESP entry's for the    */
/*                        given TeSid, i.e. it scans all the ESPs that are   */
/*                        configured for this TESID with the port(s) for     */
/*                        each ESP.                                          */
/*                                                                           */
/*     INPUT            : u4TeSid - Traffic-enginnering Service-instance     */
/*                                 Identifier.                               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowTesidDetail (tCliHandle CliHandle, UINT4 u4TeSid)
{
    UINT4               u4NextTeSid;
    UINT4               u4NextEspId;
    UINT4               u4EspId;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4TeSidRowStatus;
    INT4                i4ContinueShow = CLI_SUCCESS;
    INT4                i4EspCount = 0;

    if (nmhGetNextIndexIeee8021PbbTeTeSidTable (u4TeSid,
                                                &u4NextTeSid,
                                                0,
                                                &u4NextEspId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\nTESID %u", u4TeSid);
    CliPrintf (CliHandle, "\r\n---------------\r\n");

    do
    {
        if (u4TeSid != u4NextTeSid)
        {
            CliPrintf (CliHandle, "-----------------------------------");
            CliPrintf (CliHandle, "-------------------------\r\n");
            CliPrintf (CliHandle,
                       "\r\n  Number of ESPs in TESID %u : %u\r\n\r\n", u4TeSid,
                       i4EspCount);
            break;
        }

        nmhGetIeee8021PbbTeTeSidRowStatus (u4TeSid, u4NextEspId,
                                           &i4TeSidRowStatus);

        if (i4TeSidRowStatus == PBBTE_ACTIVE)
        {
            i4EspCount++;
            PbbTeCliPrintTeSidEntInDetail (CliHandle, u4TeSid, u4NextEspId);
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        }

        i4ContinueShow = u4PagingStatus;

        u4EspId = u4NextEspId;
        if (nmhGetNextIndexIeee8021PbbTeTeSidTable (u4TeSid,
                                                    &u4NextTeSid,
                                                    u4EspId,
                                                    &u4NextEspId) ==
            SNMP_FAILURE)
        {
            i4ContinueShow = CLI_FAILURE;
        }
    }
    while (i4ContinueShow == CLI_SUCCESS);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowTesid                                  */
/*                                                                           */
/*     DESCRIPTION      : This function displays the  ESP entry's for the    */
/*                        given TeSid, i.e. it scans all the ESPs that are   */
/*                        configured for this TESID without the port(s).     */
/*                                                                           */
/*     INPUT            : u4TeSid - Traffic-enginnering Service-instance     */
/*                                 Identifier.                               */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowTeSid (tCliHandle CliHandle, UINT4 u4TeSid)
{
    UINT4               u4NextTeSid;
    UINT4               u4NextEspId;
    UINT4               u4EspId;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4TeSidRowStatus;
    INT4                i4ContinueShow = CLI_SUCCESS;

    CliPrintf (CliHandle,
               "\r\n V - Volatile  NV - Non-Volatile  P - Permenant ");
    CliPrintf (CliHandle, " RO - Read-Only  O - Other\r\n");

    CliPrintf (CliHandle, "\r\nTeSid        EspIndex     Destination Mac    "
               "Source Mac         Vlan  Status\n\r");
    CliPrintf (CliHandle, "-----------  -----------  -----------------  "
               "-----------------  ----  ------\n\r");

    if (nmhGetNextIndexIeee8021PbbTeTeSidTable (u4TeSid,
                                                &u4NextTeSid,
                                                0,
                                                &u4NextEspId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u4TeSid != u4NextTeSid)
        {
            break;
        }
        nmhGetIeee8021PbbTeTeSidRowStatus (u4TeSid, u4NextEspId,
                                           &i4TeSidRowStatus);

        if (i4TeSidRowStatus == PBBTE_ACTIVE)
        {
            PbbTeCliDisplayTeSidEntry (CliHandle, u4TeSid, u4NextEspId);
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");

        }

        i4ContinueShow = u4PagingStatus;
        u4EspId = u4NextEspId;

        if (nmhGetNextIndexIeee8021PbbTeTeSidTable (u4TeSid,
                                                    &u4NextTeSid,
                                                    u4EspId,
                                                    &u4NextEspId)
            == SNMP_FAILURE)
        {
            i4ContinueShow = CLI_FAILURE;
        }
    }
    while (i4ContinueShow == CLI_SUCCESS);

    CliPrintf (CliHandle, "\r\n");
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowTeSiTableDetail                        */
/*                                                                           */
/*     DESCRIPTION      : This function displays the full  TESI table along  */
/*                        with the port(s) that configured in the data path. */
/*                                                                           */
/*     INPUT            :                                                    */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowTeSiTableDetail (tCliHandle CliHandle)
{
    UINT4               u4NextTeSid = 0;
    UINT4               u4NextEspId = 0;
    UINT4               u4EspId = 0;
    UINT4               u4TeSid = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4ContinueShow = CLI_SUCCESS;
    INT4                i4TeSidRowStatus = 0;
    INT4                i4EspCount = 0;

    if (nmhGetNextIndexIeee8021PbbTeTeSidTable (0,
                                                &u4NextTeSid,
                                                0,
                                                &u4NextEspId) != SNMP_SUCCESS)
    {
        return CLI_SUCCESS;
    }

    do
    {
        if (u4TeSid != u4NextTeSid)
        {
            if (u4TeSid != 0)
            {
                CliPrintf (CliHandle, "-----------------------------------");
                CliPrintf (CliHandle, "-------------------------\r\n");
                CliPrintf (CliHandle, "\r\n  Number of ESPs in ");
                CliPrintf (CliHandle, "TESID %u : %u\r\n\r\n", u4TeSid,
                           i4EspCount);
                i4EspCount = 0;
            }
            CliPrintf (CliHandle, "\r\nTESID %u", u4NextTeSid);
            CliPrintf (CliHandle, "\r\n---------------\r\n");
        }
        nmhGetIeee8021PbbTeTeSidRowStatus (u4NextTeSid, u4NextEspId,
                                           &i4TeSidRowStatus);

        if (i4TeSidRowStatus == PBBTE_ACTIVE)
        {
            i4EspCount++;
            PbbTeCliPrintTeSidEntInDetail (CliHandle, u4NextTeSid, u4NextEspId);
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        }

        i4ContinueShow = u4PagingStatus;

        u4TeSid = u4NextTeSid;
        u4EspId = u4NextEspId;

        if (nmhGetNextIndexIeee8021PbbTeTeSidTable (u4TeSid,
                                                    &u4NextTeSid,
                                                    u4EspId,
                                                    &u4NextEspId) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "-----------------------------------");
            CliPrintf (CliHandle, "-------------------------\r\n");
            CliPrintf (CliHandle,
                       "\r\n  Number of ESPs in TESID %u : %u\r\n\r\n", u4TeSid,
                       i4EspCount);
            i4ContinueShow = CLI_FAILURE;
        }

    }
    while (i4ContinueShow == CLI_SUCCESS);
    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliShowTeSiTable                              */
/*                                                                           */
/*     DESCRIPTION      : This function displays the full  TESI table along  */
/*                        with out the port(s) that configured in the data   */
/*                        path.                                              */
/*                                                                           */
/*     INPUT            :                                                    */
/*                                                                           */
/*                                                                           */
/*     OUTPUT           : CliHandle - Contains error messages                */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliShowTeSiTable (tCliHandle CliHandle)
{
    UINT4               u4NextTeSid;
    UINT4               u4NextEspId;
    UINT4               u4EspId;
    UINT4               u4TeSid;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4ContinueShow = CLI_SUCCESS;
    INT4                i4TeSidRowStatus;

    if (nmhGetNextIndexIeee8021PbbTeTeSidTable (0,
                                                &u4NextTeSid,
                                                0,
                                                &u4NextEspId) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle,
               "\r\n V - Volatile  NV - Non-Volatile  P - Permenant ");
    CliPrintf (CliHandle, " RO - Read-Only  O - Other\r\n");

    CliPrintf (CliHandle, "\r\nTeSid        EspIndex     Destination Mac    "
               "Source Mac         Vlan  Status\n\r");
    CliPrintf (CliHandle, "-----------  -----------  -----------------  "
               "-----------------  ----  ------\n\r");
    do
    {
        nmhGetIeee8021PbbTeTeSidRowStatus (u4NextTeSid, u4NextEspId,
                                           &i4TeSidRowStatus);

        if (i4TeSidRowStatus == PBBTE_ACTIVE)
        {
            PbbTeCliDisplayTeSidEntry (CliHandle, u4NextTeSid, u4NextEspId);
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
        }
        UNUSED_PARAM (u4PagingStatus);
        u4TeSid = u4NextTeSid;
        u4EspId = u4NextEspId;

        if (nmhGetNextIndexIeee8021PbbTeTeSidTable (u4TeSid,
                                                    &u4NextTeSid,
                                                    u4EspId,
                                                    &u4NextEspId)
            == SNMP_FAILURE)
        {
            i4ContinueShow = CLI_FAILURE;
        }
    }
    while (i4ContinueShow == CLI_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliPrintEspVlanList                           */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Esp vlan list           */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of vlan in vlan list       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : Success/Failure                                    */
/*                                                                           */
/*****************************************************************************/
VOID
PbbTeCliPrintEspVlanList (tCliHandle CliHandle, INT4 i4CommaCount, UINT2 VlanId)
{

    INT4                i4Times;
    UINT2               u2Loop = 0;
    INT4                i4Count = 1;

    i4Times = 4094;

    if (i4CommaCount == i4Count)
    {
        CliPrintf (CliHandle, "%d", VlanId);
        return;
    }
    for (u2Loop = 1; u2Loop < i4Times; u2Loop++)
    {
        /* Only 4 commas per line will be displayed */
        if (i4CommaCount <= (8 * u2Loop))
        {
            if (i4CommaCount == (8 * (u2Loop - 1)) + 1)
            {
                CliPrintf (CliHandle, ",\r\n                ");
                CliPrintf (CliHandle, "%d", VlanId);
            }
            else
            {
                CliPrintf (CliHandle, ",%d", VlanId);
            }
            break;
        }
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliDisplayOctetToVlan                         */
/*                                                                           */
/*     DESCRIPTION      : This function converts the octet list containing   */
/*                        ESP vlans and displays it in suitable form .       */
/*                                                                           */
/*     INPUT            : CliHandle  - Handle to the cli context             */
/*                        pOctet - Vlan Octet list                           */
/*                        OctetLen - octet list length                       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          :                                                    */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeCliDisplayOctetToVlan (tCliHandle CliHandle, UINT1 *pOctet,
                            UINT4 u4OctetLen, INT4 bSeprtr, INT4 *pi4CommaCount)
{
    BOOL1               bOutCome = PBBTE_FALSE;
    BOOL1               bFlag = PBBTE_TRUE;
    UINT1              *pVlanList = NULL;
    UINT2               u2VlanId = PBBTE_INIT_VAL;
    UINT2               u2Temp = 0;
    UINT2               u2LastPrint = 0;

    pVlanList = UtlShMemAllocVlanList ();
    if (pVlanList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    PBBTE_MEM_SET (pVlanList, 0, PBBTE_VLAN_LIST_SIZE);

    PBBTE_MEM_CPY (pVlanList, pOctet, u4OctetLen);

    for (u2VlanId = 1; u2VlanId <= PBBTE_MAX_VLANS; u2VlanId++)
    {
        OSIX_BITLIST_IS_BIT_SET (pVlanList, u2VlanId, PBBTE_VLAN_LIST_SIZE,
                                 bOutCome);
        if (bOutCome == PBBTE_TRUE)
        {
            if (bFlag != PBBTE_TRUE)
            {
                if ((u2VlanId - u2Temp) != 1)
                {
                    if (u2LastPrint != u2Temp)
                    {
                        CliPrintf (CliHandle, "-");
                        CliPrintf (CliHandle, "%d", u2Temp);
                    }
                    (*pi4CommaCount)++;
                    PbbTeCliPrintEspVlanList (CliHandle, *pi4CommaCount,
                                              u2VlanId);
                    u2LastPrint = u2VlanId;
                }
            }
            else
            {
                if (bSeprtr == PBBTE_TRUE)
                {
                    (*pi4CommaCount)++;
                    PbbTeCliPrintEspVlanList (CliHandle, *pi4CommaCount,
                                              u2VlanId);
                }
                else
                {
                    (*pi4CommaCount)++;
                    CliPrintf (CliHandle, "%d", u2VlanId);
                }
                u2LastPrint = u2VlanId;
                bFlag = PBBTE_FALSE;
            }
            bSeprtr = PBBTE_TRUE;
            u2Temp = u2VlanId;
        }
    }

    OSIX_BITLIST_IS_BIT_SET (pVlanList, u2Temp, PBBTE_VLAN_LIST_SIZE, bOutCome);
    if (bOutCome == PBBTE_TRUE)
    {
        if (u2LastPrint != u2Temp)
        {
            CliPrintf (CliHandle, "-");
            CliPrintf (CliHandle, "%d\r\n", u2Temp);
        }
    }
    UtlShMemFreeVlanList (pVlanList);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : PbbTeCliHandleVlanFailures                         */
/*                                                                           */
/*     DESCRIPTION      : This function handles the return values from       */
/*                        the vlan APIs and intimates to the user through    */
/*                        Trace messages or CliPrintf's.                     */
/*                                                                           */
/*     INPUT            : i4RetValue - The value returned by VLAN API.       */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          :                                                    */
/*                                                                           */
/*****************************************************************************/
VOID
PbbTeCliVlanFailurePrintMesg (tCliHandle CliHandle, INT4 i4ReturnValue)
{
    UNUSED_PARAM (CliHandle);
    switch (i4ReturnValue)
    {
        case VLAN_API_ERR_HW_FAILURE:
            CLI_SET_ERR (PBBTE_HW_FAIL);
            break;
        case VLAN_API_ERR_INVALID_PORTS:
            CLI_SET_ERR (PBBTE_INVALID_PORTS);
            break;
        case VLAN_API_ERR_ENTRY_NOT_PRESENT:
            CLI_SET_ERR (PBBTE_VLAN_NOT_PRESENT);
            break;
        case VLAN_API_ERR_CONTEXT_INVALID:
            CLI_SET_ERR (PBBTE_CONTEXT_INVALID);
            break;
        case VLAN_API_ERR_VLAN_NOT_ACTIVE:
            CLI_SET_ERR (PBBTE_VLAN_NOT_ACTIVE);
            break;
        case MST_FAILURE:
            CLI_SET_ERR (PBBTE_MST_INST_MAPPING_FAILURE);
            break;
        default:
            break;
    }
}

/**************************************************************************/
/*                                                                        */
/*     FUNCTION NAME    : PbbTeGetVcmPbbTeCfgPrompt                       */
/*                                                                        */
/*     DESCRIPTION      : This function Checks for pbbte validity and 
 *                        returns the prompt to be displayed.             */
/*                                                                        */
/*     INPUT            : pi1ModeName - Mode to be configured.            */
/*                                                                        */
/*     OUTPUT           : pi1DispStr  - Prompt to be displayed.           */
/*                                                                        */
/*     RETURNS          : TRUE or FALSE                                   */
/*                                                                        */
/**************************************************************************/
INT1
PbbTeGetVcmPbbTeCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4TeSidIndex;
    UINT4               u4Len = STRLEN (CLI_PBBTE_TESI_MODE);

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, CLI_PBBTE_TESI_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4TeSidIndex = CLI_ATOI (pi1ModeName);

    /* 
     * No need to take lock here, since it is taken by
     * Cli in cli_process_vlan_cmd.
     */
    CLI_SET_VLANID (u4TeSidIndex);

    STRCPY (pi1DispStr, "(config-switch-tesi)#");

    return TRUE;
}

/**********************************************************************/
/*                                                                    */
/*     FUNCTION NAME    : PbbTeCliPrintTeSidEntInDetail               */
/*                                                                    */
/*     DESCRIPTION      : This function is used to display the tesid  */
/*                        table in detail.                            */
/*                                                                    */
/*     INPUT            : u4TeSid - Traffic-engineering               */
/*                                  Service-Insatnce Identifier.      */
/*                        u4EspId - Ethernet Switched path Identifer. */
/*                                                                    */
/*     RETURNS          :                                             */
/*                                                                    */
/**********************************************************************/
VOID
PbbTeCliPrintTeSidEntInDetail (tCliHandle CliHandle, UINT4 u4TeSid,
                               UINT4 u4EspId)
{
    tSNMP_OCTET_STRING_TYPE PbbTeSidEsp;
    tSNMP_OCTET_STRING_TYPE SnmpEgressPorts;
    tConfigStMcastInfo  StaticMultiCastEntry;
    tConfigStUcastInfo  StaticUnicastEntry;
    tMacAddr            SrcMac;
    tMacAddr            DstMac;
    UINT4               u4PagingStatus;
    INT4                i4TeSidContextId;
    UINT2               u2EspVid = 0;
    UINT2               u2PrintEspVid = 0;
    INT4                i4TeSidStorageType;
    UINT1               au1PbbTeSidEsp[PBBTE_ESP_LENGTH];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ContextName[PBBTE_SWITCH_ALIAS_LEN];
    UINT1               au1DstMacString[PBBTE_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1SrcMacString[PBBTE_CLI_MAX_MAC_STRING_SIZE];
    UINT1               u1ErrCode = 0;

    PBBTE_MEM_SET (au1PbbTeSidEsp, PBBTE_INIT_VAL, sizeof (au1PbbTeSidEsp));
    PbbTeSidEsp.i4_Length = PBBTE_ESP_LENGTH;
    PbbTeSidEsp.pu1_OctetList = au1PbbTeSidEsp;

    PBBTE_MEM_SET (SrcMac, PBBTE_INIT_VAL, sizeof (tMacAddr));
    PBBTE_MEM_SET (DstMac, PBBTE_INIT_VAL, sizeof (tMacAddr));
    PBBTE_MEM_SET (&StaticMultiCastEntry, PBBTE_INIT_VAL,
                   sizeof (tConfigStMcastInfo));
    PBBTE_MEM_SET (&StaticUnicastEntry, PBBTE_INIT_VAL,
                   sizeof (tConfigStUcastInfo));

    nmhGetIeee8021PbbTeTeSidEsp (u4TeSid, u4EspId, &PbbTeSidEsp);
    nmhGetIeee8021PbbTeTeSidStorageType (u4TeSid, u4EspId, &i4TeSidStorageType);
    nmhGetFsPbbTeTeSidExtContextId (u4TeSid, u4EspId, &i4TeSidContextId);

    PBBTE_MEM_SET (au1ContextName, PBBTE_INIT_VAL, PBBTE_SWITCH_ALIAS_LEN);

    PbbTeVcmGetAliasName ((UINT4) i4TeSidContextId, au1ContextName);

    MEMCPY (DstMac, PbbTeSidEsp.pu1_OctetList, sizeof (tMacAddr));
    MEMCPY (SrcMac, PbbTeSidEsp.pu1_OctetList + sizeof (tMacAddr),
            sizeof (tMacAddr));
    MEMCPY (&u2EspVid, PbbTeSidEsp.pu1_OctetList + PBBTE_ESP_VLAN_OFFSET,
            PBBTE_VLAN_ID_SIZE);

    u2PrintEspVid = OSIX_NTOHS (u2EspVid);

    CliPrintf (CliHandle, "  ESP-ID            : ");
    CliPrintf (CliHandle, "%u/%u\r\n", u4TeSid, u4EspId);

    PrintMacAddress (DstMac, au1DstMacString);
    CliPrintf (CliHandle, "  Destination Mac   : ");
    CliPrintf (CliHandle, "%s\r\n", au1DstMacString);

    PrintMacAddress (SrcMac, au1SrcMacString);
    CliPrintf (CliHandle, "  Source Mac        : ");
    CliPrintf (CliHandle, "%s\r\n", au1SrcMacString);

    CliPrintf (CliHandle, "  VlanID            : ");
    CliPrintf (CliHandle, "%d\r\n", u2PrintEspVid);

    CliPrintf (CliHandle, "  Virtual-Switch    : ");
    CliPrintf (CliHandle, "switch %s\r\n", au1ContextName);

    CliPrintf (CliHandle, "  Status            : ");

    switch (i4TeSidStorageType)
    {
        case PBBTE_STORAGE_VOLATILE:

            CliPrintf (CliHandle, "Volatile\r\n");
            break;

        case PBBTE_STORAGE_PERMANENT:

            CliPrintf (CliHandle, "Permanent\r\n");
            break;

        case PBBTE_STORAGE_OTHER:

            CliPrintf (CliHandle, "Other\r\n");
            break;

        case PBBTE_STORAGE_READONLY:

            CliPrintf (CliHandle, "Read-Only\r\n");
            break;

        default:
            CliPrintf (CliHandle, "Non-Volatile\r\n");
    }

    CliPrintf (CliHandle, "  Egress Port(s)    : ");

    if (PBBTE_IS_MCASTADDR (DstMac) == PBBTE_TRUE)
    {

        StaticMultiCastEntry.pEgressIfIndexList =
            (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (StaticMultiCastEntry.pEgressIfIndexList == NULL)
        {
            CliPrintf (CliHandle,
                       "\r%% Error in Allocating memory for bitlist \r\n");
            return;
        }

        PBBTE_MEM_SET ((*StaticMultiCastEntry.pEgressIfIndexList),
                       PBBTE_INIT_VAL, sizeof (tPortList));

        /* Fill the static multicast entry with the context Id and 
         * Vlan id, By using this we can get the egress portlist 
         * from the vlan Module */
        StaticMultiCastEntry.u4ContextId = (UINT4) i4TeSidContextId;
        StaticMultiCastEntry.VlanId = OSIX_NTOHS (u2EspVid);
        PBBTE_MEM_CPY (StaticMultiCastEntry.DestMac, DstMac, sizeof (tMacAddr));

        /* Vlan API to get the egress portlist from the VLAN module */
        PbbTeVlanGetStaticMulticastEntry (&StaticMultiCastEntry, &u1ErrCode);

        SnmpEgressPorts.pu1_OctetList =
            *(StaticMultiCastEntry.pEgressIfIndexList);
        SnmpEgressPorts.i4_Length = sizeof (tPortList);

        if (FsUtilBitListIsAllZeros (SnmpEgressPorts.pu1_OctetList,
                                     sizeof (tPortList)) == OSIX_FALSE)
        {
            CliOctetToIfName (CliHandle,
                              NULL,
                              &SnmpEgressPorts,
                              BRG_MAX_PHY_PLUS_LOG_PORTS,
                              BRG_PORT_LIST_SIZE, 45, &u4PagingStatus, 3);
        }

        FsUtilReleaseBitList (*(StaticMultiCastEntry.pEgressIfIndexList));
    }
    else
    {
        /* Fill the static unicast entry with the context Id and 
         * Vlan id, By using this we can get the egress port 
         * from the vlan Module */
        StaticUnicastEntry.u4ContextId = (UINT4) i4TeSidContextId;
        StaticUnicastEntry.VlanId = OSIX_NTOHS (u2EspVid);
        PBBTE_MEM_CPY (StaticUnicastEntry.DestMac, DstMac, sizeof (tMacAddr));

        PbbTeVlanGetStaticUnicastEntry (&StaticUnicastEntry, &u1ErrCode);

        MEMSET (au1NameStr, 0, CFA_MAX_PORT_NAME_LENGTH);

        if (StaticUnicastEntry.u4EgressIfIndex != 0)
        {
            PbbTeCfaCliGetIfName (StaticUnicastEntry.u4EgressIfIndex,
                                  (INT1 *) au1NameStr);

            CliPrintf (CliHandle, "%s", au1NameStr);
        }
    }
    CliPrintf (CliHandle, "\r\n");
}

/**********************************************************************/
/*                                                                    */
/*     FUNCTION NAME    : PbbTeCliDisplayTeSidEntry                   */
/*                                                                    */
/*     DESCRIPTION      : This function is used to display the tesid  */
/*                        table in brief ie without the forwarding    */
/*                        path.                                       */
/*                                                                    */
/*     INPUT            : u4TeSid - Traffic-engineering               */
/*                                  Service-Insatnce Identifier.      */
/*                        u4EspId - Ethernet Switched path Identifer. */
/*                                                                    */
/*     RETURNS          :                                             */
/*                                                                    */
/**********************************************************************/
VOID
PbbTeCliDisplayTeSidEntry (tCliHandle CliHandle, UINT4 u4TeSid, UINT4 u4EspId)
{
    tSNMP_OCTET_STRING_TYPE PbbTeSidEsp;
    tMacAddr            SrcMac;
    tMacAddr            DstMac;
    UINT2               u2EspVid = 0;
    UINT2               u2PrintEspVid = 0;
    INT4                i4TeSidStorageType;
    UINT1               au1PbbTeSidEsp[PBBTE_ESP_LENGTH];
    UINT1               au1DstMacString[PBBTE_CLI_MAX_MAC_STRING_SIZE];
    UINT1               au1SrcMacString[PBBTE_CLI_MAX_MAC_STRING_SIZE];

    PBBTE_MEM_SET (au1PbbTeSidEsp, PBBTE_INIT_VAL, sizeof (au1PbbTeSidEsp));

    PbbTeSidEsp.i4_Length = PBBTE_ESP_LENGTH;

    PbbTeSidEsp.pu1_OctetList = &au1PbbTeSidEsp[0];

    PBBTE_MEM_SET (SrcMac, PBBTE_INIT_VAL, sizeof (tMacAddr));
    PBBTE_MEM_SET (DstMac, PBBTE_INIT_VAL, sizeof (tMacAddr));

    nmhGetIeee8021PbbTeTeSidEsp (u4TeSid, u4EspId, &PbbTeSidEsp);
    nmhGetIeee8021PbbTeTeSidStorageType (u4TeSid, u4EspId, &i4TeSidStorageType);

    MEMCPY (DstMac, PbbTeSidEsp.pu1_OctetList, sizeof (tMacAddr));
    MEMCPY (SrcMac, PbbTeSidEsp.pu1_OctetList + sizeof (tMacAddr),
            sizeof (tMacAddr));
    MEMCPY (&u2EspVid, PbbTeSidEsp.pu1_OctetList + PBBTE_ESP_VLAN_OFFSET,
            PBBTE_VLAN_ID_SIZE);

    u2PrintEspVid = OSIX_NTOHS (u2EspVid);

    CliPrintf (CliHandle, "%-13u", u4TeSid);

    CliPrintf (CliHandle, "%-13u", u4EspId);

    PbbTeCliMacToStr (DstMac, au1DstMacString);
    CliPrintf (CliHandle, "%s ", au1DstMacString);

    PbbTeCliMacToStr (SrcMac, au1SrcMacString);
    CliPrintf (CliHandle, "%s ", au1SrcMacString);

    CliPrintf (CliHandle, "%-5d", u2PrintEspVid);

    switch (i4TeSidStorageType)
    {
        case PBBTE_STORAGE_VOLATILE:

            CliPrintf (CliHandle, "   V");
            break;

        case PBBTE_STORAGE_PERMANENT:

            CliPrintf (CliHandle, "   P");
            break;

        case PBBTE_STORAGE_OTHER:

            CliPrintf (CliHandle, "   O");
            break;

        case PBBTE_STORAGE_READONLY:

            CliPrintf (CliHandle, "  RO");
            break;

        default:
            CliPrintf (CliHandle, "  NV");
    }
}

/***************************************************************************
 *                                                                         *
 *     Function Name : PbbTeCliMacToStr                                    *
 *                                                                         *
 *     Description   : This function converts the given mac address        * 
 *                     in to a string of form aa:aa:aa:aa:aa:aa            *
 *                                                                         *
 *     Input(s)      : pMacAdrr   : Pointer to the Mac address value array *
 *                     pu1Temp    : Pointer to the converted mac address   *
 *                                  string.(The string must be of length   *
 *                                  21 bytes minimum)                      *
 *                                                                         *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NULL                                                *
 *                                                                         *
 ***************************************************************************/

VOID
PbbTeCliMacToStr (UINT1 *pMacAddr, UINT1 *pu1Temp)
{

    UINT1               u1Byte;

    if ((!(pMacAddr)) || (!(pu1Temp)))
        return;

    for (u1Byte = 0; u1Byte < MAC_LEN; u1Byte++)
    {
        pu1Temp += SPRINTF ((CHR1 *) pu1Temp, "%02x:", *(pMacAddr + u1Byte));
    }
    SPRINTF ((CHR1 *) (pu1Temp - 1), " ");

}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssPbbTeShowDebugging                              */
/*                                                                           */
/*     DESCRIPTION      : This function used to dsplay the debug levels      */
/*                        for backbone module                                */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IssPbbTeShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4GblTraceVal = 0;
    UINT4               u4TraceVal = 0;

    nmhGetFsPbbTeContextTraceOption (VLAN_DEF_CONTEXT_ID, &u4TraceVal);

    u4GblTraceVal = gPbbTeGlobalInfo.u4TraceOption;

    if ((u4TraceVal == 0) && (u4GblTraceVal != PBBTE_GBL_TRC_FLAG))
    {
        return;
    }

    CliPrintf (CliHandle, "\rBACKBONE :");

    if (u4GblTraceVal == PBBTE_GBL_TRC_FLAG)
    {
        CliPrintf (CliHandle, "\r\n  BACKBONE global debugging is on");
    }
    if ((u4TraceVal & PBBTE_INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  BACKBONE initialization and shutdown debugging is on");
    }
    if ((u4TraceVal & PBBTE_MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BACKBONE management debugging is on");
    }
    if ((u4TraceVal & PBBTE_DATA_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BACKBONE data path debugging is on");
    }
    if ((u4TraceVal & PBBTE_CONTROL_PATH_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BACKBONE control debugging is on");
    }
    if ((u4TraceVal & PBBTE_DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BACKBONE packet dump debugging is on");
    }
    if ((u4TraceVal & PBBTE_OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BACKBONE resources debugging is on");
    }
    if ((u4TraceVal & PBBTE_ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BACKBONE all failure debugging is on");
    }
    if ((u4TraceVal & PBBTE_BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  BACKBONE buffer debugging is on");
    }

    CliPrintf (CliHandle, "\r\n");
    return;

}

#endif /*__PBTCLI_C_*/
