#include "pbthdrs.h"

/*****************************************************************************/
/*    Function Name       : PbbTeFdbTableCmp                                 */
/*    Description         : User compare function for VLAN FDB table         */
/*    Input(s)            : Two FDB entries to be compared                   */
/*    Output(s)           : None                                             */
/*    Returns             : 1  - pRBElem1 > pRBElem2                         */
/*                          -1 - pRBElem1 < pRBElem2                         */
/*                          0  = both are equal                              */
/*****************************************************************************/
INT4
PbbTeTeSidTableCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4TeSidId1 = ((tTeSidEspEntry *) pRBElem1)->u4TeSid;
    UINT4               u4TeSidId2 = ((tTeSidEspEntry *) pRBElem2)->u4TeSid;
    UINT4               u4TeSidEspIndex1 =
        ((tTeSidEspEntry *) pRBElem1)->u4TeSidEspIndex;
    UINT4               u4TeSidEspIndex2 =
        ((tTeSidEspEntry *) pRBElem2)->u4TeSidEspIndex;

    if (u4TeSidId1 < u4TeSidId2)
    {
        return -1;
    }
    if (u4TeSidId1 > u4TeSidId2)
    {
        return 1;
    }
    if (u4TeSidEspIndex1 < u4TeSidEspIndex2)
    {
        return -1;
    }
    if (u4TeSidEspIndex1 > u4TeSidEspIndex2)
    {
        return 1;
    }
    return 0;
}

/*****************************************************************************/
/* Function Name      : PbbTeLock                                            */
/*                                                                           */
/* Description        : This function is used to take the PBBTE mutual       */
/*                      exclusion semaphore to avoid simultaneous access     */
/*                      of it's data structures by more than one thread.     */
/*                      This lock is a data structure lock that has a very   */
/*                      small critical section to protect only the access/   */
/*                      modification of the following data structures .      */
/*                      Context table (tPbbTeContextInfo), TESID table       */
/*                      (tTeSidEspEntry), ESP {DA, VID}                      */
/*                      Hash (tEspDMacVIDInfo).                              */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeLock (VOID)
{
    if (PBBTE_TAKE_SEM (gPbbTeGlobalInfo.DataSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeUnLock                                          */
/*                                                                           */
/* Description        : This function is used to release the PBB-TE          */
/*                      mutual exclusion semaphore.                          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeUnLock (VOID)
{
    PBBTE_GIVE_SEM (gPbbTeGlobalInfo.DataSemId);
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeUtlIsNonMonitoredEspPresent                     */
/*                                                                           */
/* Description        : This function is used to find whether a non          */
/*                      monitored ESP is configured in the particular        */
/*                      Destination address and Vlan Id.                     */
/*                                                                           */
/* Input(s)           : DestAddr     - Destination Address                   */
/*                      VlanId       - Vlan Identifier                       */
/*                      u4ContextId  - Context Identifier                    */
/*                      pu1NonMonitoredEspPresent - Set if Non monitored ESP */
/*                                                  is present               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
VOID
PbbTeUtlIsNonMonitoredEspPresent (UINT4 u4ContextId, tMacAddr DestAddr,
                                  tVlanId VlanId,
                                  UINT1 *pu1NonMonitoredEspPresent)
{
    tConfigStUcastInfo  StaticUnicastInfo;
    tConfigStMcastInfo  StaticMulticastInfo;
    tEspDMacVIDInfo    *pEspDMacVIDInfo = NULL;
    UINT4               u4HIndex = 0;
    UINT1               u1ErrorCode = PBBTE_INIT_VAL;

    *pu1NonMonitoredEspPresent = PBBTE_FALSE;

    if (PBBTE_CTX_VLAN_HASH_TBL (u4ContextId, VlanId) == NULL)
    {
        /* No creation of Hash Table itself, Its not an ESP-VLAN itself */
        return;
    }

    u4HIndex = PbbTeUtlGetHashIndex (u4ContextId, DestAddr, VlanId);

    /* If there is any node existing with the same Destination MAC & VID
     * information, then no need to identification of Monitored ESP present 
     * or not. */
    TMO_HASH_Scan_Bucket (PBBTE_CTX_VLAN_HASH_TBL (u4ContextId, VlanId),
                          u4HIndex, pEspDMacVIDInfo, tEspDMacVIDInfo *)
    {
        if ((PBBTE_ARE_MAC_ADDR_EQUAL (DestAddr, pEspDMacVIDInfo->DestAddr)
             == PBBTE_TRUE) && (VlanId == pEspDMacVIDInfo->EspVid))
        {
            /* DMacVIDInfo node is existing for the DestMAC address */
            return;
        }
    }

    /* No DMacVIDInfo node is present.. */
    if (PBBTE_IS_MCASTADDR (DestAddr) == PBBTE_TRUE)
    {
        PBBTE_MEM_SET (&StaticMulticastInfo, PBBTE_INIT_VAL,
                       sizeof (tConfigStMcastInfo));

        StaticMulticastInfo.pEgressIfIndexList =
            (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

        if (StaticMulticastInfo.pEgressIfIndexList == NULL)
        {
            PBBTE_GBL_TRC (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC |
                           PBBTE_BUFFER_TRC, PBBTE_TRC_MEM_FAIL_BITLIST);
            return;
        }
        PBBTE_MEM_SET (StaticMulticastInfo.pEgressIfIndexList,
                       PBBTE_INIT_VAL, sizeof (tPortList));

        StaticMulticastInfo.u4ContextId = u4ContextId;
        StaticMulticastInfo.VlanId = VlanId;
        PBBTE_CPY_MAC_ADDR (StaticMulticastInfo.DestMac, DestAddr);

        if (PbbTeVlanGetStaticMulticastEntry (&StaticMulticastInfo,
                                              &u1ErrorCode) == PBBTE_SUCCESS)
        {
            if (StaticMulticastInfo.u4Status == VLAN_OTHER)
            {
                *pu1NonMonitoredEspPresent = PBBTE_TRUE;
            }
        }
        FsUtilReleaseBitList (*(StaticMulticastInfo.pEgressIfIndexList));
    }
    else
    {
        StaticUnicastInfo.u4ContextId = u4ContextId;
        StaticUnicastInfo.VlanId = VlanId;
        PBBTE_CPY_MAC_ADDR (StaticUnicastInfo.DestMac, DestAddr);

        if (PbbTeVlanGetStaticUnicastEntry (&StaticUnicastInfo, &u1ErrorCode)
            == PBBTE_SUCCESS)
        {
            if (StaticUnicastInfo.u4Status == VLAN_OTHER)
            {
                *pu1NonMonitoredEspPresent = PBBTE_TRUE;
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : PbbTeUtlGetHashIndex                                 */
/*                                                                           */
/* Description        : This function is used to find Hash index for         */
/*                      the given Mac Address & VLAN Id.                     */
/*                                                                           */
/* Input(s)           : DestAddr     - Destination Address                   */
/*                      VlanId       - Vlan Identifier                       */
/*                      u4ContextId  - Context Identifier                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : u4HashIndex                                          */
/*****************************************************************************/
UINT4
PbbTeUtlGetHashIndex (UINT4 u4ContextId, tMacAddr DestAddr, tVlanId VlanId)
{
    UINT4               u4HashIndex = 0;

    UNUSED_PARAM (u4ContextId);

    u4HashIndex = (DestAddr[0] + DestAddr[1] + DestAddr[2] + DestAddr[3] +
                   DestAddr[4] + DestAddr[5] + VlanId) % PBBTE_MAX_BUCKETS;

    return u4HashIndex;
}

/*****************************************************************************/
/* Function Name      : PbbTeUtlConfigureEsp                                 */
/*                                                                           */
/* Description        : This function is used to create or delete ethernet   */
/*                      switched path. This utility routine is called by     */
/*                      PBB-TE API -- PbbTeUtlConfigureEsp and PBB-TE RED    */
/*                      Dynamic Update message handling                      */
/*                                                                           */
/* Input(s)           : pEspPathInfo - ESP Path Information                  */
/*                      u1Action - PBBTE_CREATE/PBBTE_DELETE                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
INT4
PbbTeUtlConfigureEsp (tEspPathInfo * pEspPathInfo, UINT1 u1Action)
{
    if (u1Action == PBBTE_CREATE)
    {
        return (PbbTeSysCreateEsp (pEspPathInfo));
    }
    else if (u1Action == PBBTE_DELETE)
    {
        return (PbbTeSysDeleteEsp (pEspPathInfo));
    }

    return PBBTE_FAILURE;
}
