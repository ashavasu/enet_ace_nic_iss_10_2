/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: pbtsys.c,v 1.10 2012/08/29 10:32:13 siva Exp $                 *
 *                                                                           *
 * Description: This file contains routines related to the                   *
 *              PBB-TE module init/deinit                                    *
 *                                                                           *
 *****************************************************************************/
#ifndef _PBTSYS_C
#define _PBTSYS_C

#include "pbthdrs.h"

/*****************************************************************************/
/* Function Name      : PbbTeSysModuleStartInCtxt                            */
/*                                                                           */
/* Description        : This function is called to start the PBB-TE module   */
/*                      in the particular context. While starting, context   */
/*                      pointer will be initialized.                         */
/*                                                                           */
/* Input(s)           : u4ContextId - An integer value that represents the   */
/*                                    virtual context.                       */
/*                                                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeSysModuleStartInCtxt (UINT4 u4ContextId)
{
    tPbbTeContextInfo  *pPbbTeContextInfo = NULL;

    if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_START)
    {
        PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_MOD_ALREADY_START,
                            u4ContextId);
        return PBBTE_SUCCESS;
    }
    if (PBBTE_CONTEXT_ENTRY_ALLOC_MEMBLK (pPbbTeContextInfo) == NULL)
    {
        PBBTE_GBL_TRC_ARG1 (PBBTE_ALL_FAILURE_TRC | PBBTE_OS_RESOURCE_TRC,
                            PBBTE_TRC_MEM_FAIL_CTX, u4ContextId);
        return PBBTE_FAILURE;
    }

    PBBTE_MEM_SET (pPbbTeContextInfo, PBBTE_INIT_VAL,
                   sizeof (tPbbTeContextInfo));

    PBBTE_LOCK ();
    PBBTE_CONTEXT_PTR (u4ContextId) = pPbbTeContextInfo;
    PBBTE_CONTEXT_PTR (u4ContextId)->u4HashCnt = 1;
    PBBTE_UNLOCK ();

    PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_MOD_START, u4ContextId);
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeSysModuleShutInCtxt                             */
/*                                                                           */
/* Description        : This function is called to shutdown the TE module in */
/*                      the particular context. During shutdown, the         */
/*                      following operation will be performed.               */
/*                         1. Delete static unicast/multicast entries in VLAN*/
/*                            module that have been configured internally    */
/*                            by the external control plane.                 */
/*                         2. Remove all the Vlans mapped to ESP-VID range   */
/*                            and assigns them to the spanning tree CIST     */
/*                            context.                                       */
/*                         3. Delete all the TESID ESP entries mapped to the */
/*                            context.                                       */
/*                         4. Releases the memory allocated for the context. */
/*                                                                           */
/* Input(s)           : u4ContextId - An integer value that represents the   */
/*                                    virtual context.                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeSysModuleShutInCtxt (UINT4 u4ContextId)
{
    UINT2               u2HashTableCnt = 0;

    if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_SHUTDOWN)
    {
        PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_MOD_ALREADY_SHUT,
                            u4ContextId);
        return PBBTE_SUCCESS;
    }

#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    /* Send Event Sync-up message to Standby for Hardware Audit 
     * Also Block the NPAPI Sync-ups in this module till this function
     * is over.
     */
    NpSyncSendDeleteContextEvent (u4ContextId);
    PBBTE_NPSYNC_BLK () = OSIX_TRUE;
#endif
#endif
    PbbTeTeSidEspEntryDelAllInCtxt (u4ContextId);

    PbbTeSysDeleteAllEspVlans (u4ContextId);

    PBBTE_LOCK ();

    /* Delete all the Hash tables created in the context */
    for (; u2HashTableCnt <= PBBTE_MAX_HASH_TABLE_PER_CONTEXT; u2HashTableCnt++)
    {
        if (PBBTE_CONTEXT_PTR (u4ContextId)->aHashTableList[u2HashTableCnt]
            != NULL)
        {
            TMO_HASH_Delete_Table (PBBTE_CONTEXT_PTR (u4ContextId)->
                                   aHashTableList[u2HashTableCnt], NULL);
        }
    }

    PBBTE_CONTEXT_ENTRY_FREE_MEMBLK (PBBTE_CONTEXT_PTR (u4ContextId));
    PBBTE_CONTEXT_PTR (u4ContextId) = NULL;
    PBBTE_UNLOCK ();

#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    PBBTE_NPSYNC_BLK () = OSIX_FALSE;
#endif
#endif

    PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_MOD_SHUT, u4ContextId);
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeSysCreateEsp                                    */
/*                                                                           */
/* Description        : This API is used to support any external control     */
/*                      plane that dynamically identies and provisions       */
/*                      traffice engineered paths across the backbone network*/
/*                      It is called to set up the vlan static unicast or    */
/*                      multicast table and optionally the TESID table from  */
/*                      the external control plane.                          */
/*                                                                           */
/* Input(s)           : pEspPathInfo - Pointer to the EspPath Information    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeSysCreateEsp (tEspPathInfo * pEspPathInfo)
{
    tEspInfo            EspInfo;
    tEspDMacVIDInfo     DMacVIDInfo;
    UINT4               u4EspIndex = PBBTE_INIT_VAL;
    UINT1               u1Result = PBBTE_FALSE;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    if (!pEspPathInfo)
    {
        /* Nothing passed to this function */
        return PBBTE_FAILURE;
    }

    if (PBBTE_IS_SYSTEM_STARTED (pEspPathInfo->u4ContextId) == PBBTE_SHUTDOWN)
    {
        /* PBB-TE is not started in this context */
        PBBTE_GBL_TRC_ARG1 (PBBTE_ALL_FAILURE_TRC | PBBTE_MGMT_TRC,
                            PBBTE_TRC_MOD_NOT_STARTED,
                            pEspPathInfo->u4ContextId);
        return PBBTE_FAILURE;
    }

    PBBTE_IS_ESP_VLAN (pEspPathInfo->u4ContextId, pEspPathInfo->EspVlanId,
                       u1Result);

    if (u1Result == PBBTE_FALSE)
    {
        /* VLAN is not configured as ESP-VLAN */
        PBBTE_TRC_ARG1 (pEspPathInfo->u4ContextId, PBBTE_ALL_FAILURE_TRC |
                        PBBTE_MGMT_TRC, PBBTE_TRC_ESP_VLAN_FAIL,
                        pEspPathInfo->EspVlanId);
        return PBBTE_FAILURE;
    }

    /* Configure the TE-SID table, if provided */
    if (pEspPathInfo->u4TeSid)
    {
        /* Source MAC address should be unicast */
        if ((PBBTE_IS_BCASTADDR (pEspPathInfo->SrcMac)) ||
            (PBBTE_IS_MCASTADDR (pEspPathInfo->SrcMac)))
        {
            PBBTE_TRC_ARG1 (pEspPathInfo->u4ContextId, PBBTE_MGMT_TRC |
                            PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_API_INVALID_SRC_MAC,
                            pEspPathInfo->u4TeSid);
            return PBBTE_FAILURE;
        }
        /* TE-SID is provided - check if an entry with the same TESID,DA_SA_VID
         * parameters already exists. If so - creation of entry is not 
         * needed. Ports may be reconfigured, so proceed for MAC address table
         * configurations. */

        if (PbbTeTeSidGetESPByTeSid
            (pEspPathInfo->u4TeSid, pEspPathInfo->DestMac, pEspPathInfo->SrcMac,
             pEspPathInfo->EspVlanId, &EspInfo, &DMacVIDInfo) == PBBTE_FAILURE)
        {

            if (PbbTeTeSidEspEntryCreate (pEspPathInfo->u4TeSid, &u4EspIndex)
                == PBBTE_FAILURE)
            {
                PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                    PBBTE_TRC_TESID_CRT, pEspPathInfo->u4TeSid);
                return PBBTE_FAILURE;
            }

            PbbTeTeSidContextIdSet (pEspPathInfo->u4TeSid, u4EspIndex,
                                    pEspPathInfo->u4ContextId);

            /* Non-monitored ESP Present bit will be set inside, if there is
             * any Non-monitored ESP using the same DstMAC address is present */

            PbbTeTeSidEspSet (pEspPathInfo->u4TeSid, u4EspIndex,
                              pEspPathInfo->DestMac, pEspPathInfo->SrcMac,
                              pEspPathInfo->EspVlanId);

            PbbTeTeSidStorageTypeSet (pEspPathInfo->u4TeSid, u4EspIndex,
                                      PBBTE_STORAGE_VOLATILE);

            PbbTeTeSidRowStatusSet (pEspPathInfo->u4TeSid, u4EspIndex,
                                    PBBTE_ACTIVE);
        }
        else
        {
            if (EspInfo.u4ContextId != pEspPathInfo->u4ContextId)
            {
                PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC,
                                    PBBTE_TRC_TESID_ALREADY_MAPPED,
                                    EspInfo.u4TeSid, EspInfo.u4EspIndex);
                return PBBTE_FAILURE;
            }
        }
    }
    else
    {
        /* No TESID is provided - this is a non-monitored ESP.
         * If there is any other ESP using the same DMAC & VID then,
         * NonMonitoredESPPresent bit has to be set in the DMacVIDInfo node */

        if (PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE)
        {
            if (PbbTeDMacVIDConfigEspStatus (pEspPathInfo->u4ContextId,
                                             pEspPathInfo->DestMac,
                                             pEspPathInfo->EspVlanId,
                                             PBBTE_TRUE) == PBBTE_SUCCESS)
            {
                PBBTE_TRC_ARG1 (pEspPathInfo->u4ContextId, PBBTE_MGMT_TRC,
                                PBBTE_TRC_NON_MONITORED_CONFIG,
                                pEspPathInfo->u4TeSid);
            }
        }
    }

    if (PBBTE_NODE_STATE != PBBTE_NODE_ACTIVE)
    {
        return PBBTE_SUCCESS;
    }

    if (PBBTE_IS_MCASTADDR (pEspPathInfo->DestMac) == PBBTE_TRUE)
    {
        /* EgressIfIndex also will be set as Multicast address' portlist,
         * if provided */
        if (pEspPathInfo->u4EgressIfIndex)
        {
            PBBTE_SET_MEMBER_PORT (pEspPathInfo->EgressIfIndexList,
                                   pEspPathInfo->u4EgressIfIndex);
        }
        /* If it is Multicast Address, we need to configure the
         * Multicast Table.*/
        if (PbbTeSysConfigMcastEntry (pEspPathInfo->u4ContextId,
                                      pEspPathInfo->DestMac,
                                      pEspPathInfo->EspVlanId,
                                      pEspPathInfo->EgressIfIndexList,
                                      VLAN_ADD) == PBBTE_FAILURE)
        {
            if (pEspPathInfo->u4TeSid)
            {
                PbbTeTeSidEspEntryDel (pEspPathInfo->u4TeSid, u4EspIndex);
            }
            return PBBTE_FAILURE;
        }
    }
    else
    {
        /* Check the EgressPort mentioned, it should not be zero */
        if (pEspPathInfo->u4EgressIfIndex == PBBTE_INIT_VAL)
        {
            PBBTE_TRC_ARG1 (pEspPathInfo->u4ContextId, PBBTE_MGMT_TRC |
                            PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_API_INVALID_PORT, pEspPathInfo->u4TeSid);
            if (pEspPathInfo->u4TeSid)
            {
                PbbTeTeSidEspEntryDel (pEspPathInfo->u4TeSid, u4EspIndex);
            }
            return PBBTE_FAILURE;
        }

        /* If it is Unicast Address, we need to configure the
         * Unicast Table. So fill the UnicastInfo field */
        if (PbbTeSysConfigUcastEntry (pEspPathInfo->u4ContextId,
                                      pEspPathInfo->DestMac,
                                      pEspPathInfo->EspVlanId,
                                      pEspPathInfo->u4EgressIfIndex, VLAN_ADD)
            == PBBTE_FAILURE)
        {
            if (pEspPathInfo->u4TeSid)
            {
                PbbTeTeSidEspEntryDel (pEspPathInfo->u4TeSid, u4EspIndex);
            }
            return PBBTE_FAILURE;
        }
    }

    /* Send Dynamic Sync-up message for Creation of ESP */
    PbbTeRedSyncDynamicMonEsp (pEspPathInfo, PBBTE_CREATE);
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeSysDeleteEsp                                    */
/*                                                                           */
/* Description        : This API is used to support any external control     */
/*                      plane that dynamically identies and provisions       */
/*                      traffice engineered paths across the backbone network*/
/*                      It is called to dlete  the vlan static unicast or    */
/*                      multicast table and optionally the TESID table from  */
/*                      the external control plane.                          */
/*                                                                           */
/* Input(s)           : pEspPathInfo - Pointer to the EspPath Information    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS/PBBTE_FAILURE                          */
/*****************************************************************************/
INT4
PbbTeSysDeleteEsp (tEspPathInfo * pEspPathInfo)
{
    tEspInfo            EspInfo;
    tEspDMacVIDInfo     DMacVIDInfo;
    UINT1               u1Result = PBBTE_FALSE;
    UINT1               u1DelFdbEntry = PBBTE_TRUE;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    if (!pEspPathInfo)
    {
        /* Nothing passed to this function */
        return PBBTE_FAILURE;
    }

    if (PBBTE_IS_SYSTEM_STARTED (pEspPathInfo->u4ContextId) == PBBTE_SHUTDOWN)
    {
        /* PBB-TE is not started in this context */
        PBBTE_GBL_TRC_ARG1 (PBBTE_ALL_FAILURE_TRC | PBBTE_MGMT_TRC,
                            PBBTE_TRC_MOD_NOT_STARTED,
                            pEspPathInfo->u4ContextId);
        return PBBTE_FAILURE;
    }

    PBBTE_IS_ESP_VLAN (pEspPathInfo->u4ContextId, pEspPathInfo->EspVlanId,
                       u1Result);

    if (u1Result == PBBTE_FALSE)
    {
        /* VLAN is not configured as ESP-VLAN */
        PBBTE_TRC_ARG1 (pEspPathInfo->u4ContextId, PBBTE_ALL_FAILURE_TRC |
                        PBBTE_MGMT_TRC, PBBTE_TRC_ESP_VLAN_FAIL,
                        pEspPathInfo->EspVlanId);
        return PBBTE_FAILURE;
    }

    /* Now we need to identify, whether the Static Unicast/Multicast entry
     * configured in the ESP, can be deleted or not */

    if (pEspPathInfo->u4TeSid)
    {
        /* In case of TE-SID provided */

        if (PbbTeTeSidGetESPByTeSid
            (pEspPathInfo->u4TeSid, pEspPathInfo->DestMac, pEspPathInfo->SrcMac,
             pEspPathInfo->EspVlanId, &EspInfo, &DMacVIDInfo) == PBBTE_SUCCESS)
        {
            /* Check for the storage Type, 
             * If it is any value other than VOLATILE
             * then the entry can't be deleted dynamically */
            if (EspInfo.u1StorageType != PBBTE_STORAGE_VOLATILE)
            {
                PBBTE_TRC_ARG1 (pEspPathInfo->u4ContextId,
                                PBBTE_ALL_FAILURE_TRC | PBBTE_MGMT_TRC,
                                PBBTE_TRC_TESID_DEL_API_FAIL,
                                pEspPathInfo->u4TeSid);

                return PBBTE_FAILURE;
            }
        }
        else
        {
            PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_TESID_NOT_FOUND,
                                pEspPathInfo->u4TeSid);
            /* TE-SID entry itself is not there, then what to delete? */
            return PBBTE_FAILURE;
        }
        u1DelFdbEntry = PBBTE_FALSE;
    }
    else
    {
        /* No TESID is provided - we are deleting a non-monitored ESP.
         * If other ESPs are using the same DA-VID path
         * then don't delete */
        if (PbbTeDMacVIDGetEspInfo (pEspPathInfo->u4ContextId,
                                    pEspPathInfo->DestMac,
                                    pEspPathInfo->EspVlanId,
                                    &DMacVIDInfo) == PBBTE_SUCCESS)
        {
            u1DelFdbEntry = PBBTE_FALSE;
        }
    }

    if (u1DelFdbEntry == PBBTE_TRUE && PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE)
    {
        /* Its the time to delete static Unicast/Multicast entry in VLAN */
        if (PBBTE_IS_MCASTADDR (pEspPathInfo->DestMac) == PBBTE_TRUE)
        {
            if (PbbTeSysConfigMcastEntry (pEspPathInfo->u4ContextId,
                                          pEspPathInfo->DestMac,
                                          pEspPathInfo->EspVlanId,
                                          pEspPathInfo->EgressIfIndexList,
                                          VLAN_DELETE) == PBBTE_FAILURE)
            {
                return PBBTE_FAILURE;
            }
            PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_ST_MCAST_DELETE,
                                pEspPathInfo->u4TeSid);
        }
        else
        {
            if (PbbTeSysConfigUcastEntry (pEspPathInfo->u4ContextId,
                                          pEspPathInfo->DestMac,
                                          pEspPathInfo->EspVlanId,
                                          pEspPathInfo->u4EgressIfIndex,
                                          VLAN_DELETE) == PBBTE_FAILURE)
            {
                return PBBTE_FAILURE;
            }
            PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_ST_UCAST_DELETE,
                                pEspPathInfo->u4TeSid);
        }
    }
    if (pEspPathInfo->u4TeSid)
    {
        /* Its the time to delete TE-SID entry */
        PbbTeTeSidEspEntryDel (pEspPathInfo->u4TeSid, EspInfo.u4EspIndex);
        PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_TESID_DEL,
                            pEspPathInfo->u4TeSid);
    }
    else
    {
        /* Non-Monitored ESP is deleted, its the time to reset NonMonitoredESP
         * bit */
        if (PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE)
        {
            if (PbbTeDMacVIDConfigEspStatus (pEspPathInfo->u4ContextId,
                                             pEspPathInfo->DestMac,
                                             pEspPathInfo->EspVlanId,
                                             PBBTE_FALSE) == PBBTE_SUCCESS)
            {
                PBBTE_TRC (pEspPathInfo->u4ContextId, PBBTE_MGMT_TRC,
                           PBBTE_TRC_NON_MONITORED_RESET);

            }
        }
    }

    if (PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE)
    {
        /* Send Dynamic Sync-up message for Deletion of ESP */
        PbbTeRedSyncDynamicMonEsp (pEspPathInfo, PBBTE_DELETE);
    }
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeSysDeleteAllEspVlans                            */
/*                                                                           */
/* Description        : This function is used to reset all the Vlans         */
/*                      configured as ESP Vlan in the particular context.    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeSysDeleteAllEspVlans (UINT4 u4ContextId)
{
    UINT1              *pu1EspVlanList = NULL;
    UINT2               u2VlanCnt = 1;
    UINT1               u1Result = PBBTE_FALSE;
    UINT1               u1ErrorCode = PBBTE_INIT_VAL;

    pu1EspVlanList = UtlShMemAllocVlanList ();
    if (pu1EspVlanList == NULL)
    {
        return PBBTE_FAILURE;
    }
    MEMSET (pu1EspVlanList, 0, PBBTE_VLAN_LIST_SIZE);

    /* Get the List of ESP Vlans */
    PbbTeEspVlanListGet (u4ContextId, pu1EspVlanList);

    for (; u2VlanCnt <= PBBTE_MAX_VLAN_ID; u2VlanCnt++)
    {
        OSIX_BITLIST_IS_BIT_SET (pu1EspVlanList, u2VlanCnt,
                                 PBBTE_VLAN_LIST_SIZE, u1Result);

        if (u1Result == OSIX_TRUE)
        {
#ifdef NPAPI_WANTED
            FsMiPbbTeHwResetEspVid (u4ContextId, u2VlanCnt);
#endif
            /* Indicate VLAN & STP about the ESP-VLAN */
            PbbTeVlanCfgLearningFlooding (u4ContextId, u2VlanCnt,
                                          VLAN_ENABLED, &u1ErrorCode);
            PbbTeVlanDelStFiltEntStatusOther (u4ContextId, u2VlanCnt,
                                              &u1ErrorCode);
            PbbTeMstpMapVidToMstId (u4ContextId, u2VlanCnt, PBBTE_CISTID);
        }
    }

    /* Clear the ESP-VLAN List in the context */
    PbbTeEspVlanListReset (u4ContextId);

    PBBTE_TRC (u4ContextId, PBBTE_MGMT_TRC, PBBTE_TRC_ESP_VLAN_DEL_ALL);
    UtlShMemFreeVlanList (pu1EspVlanList);

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeSysConfigMcastEntry                             */
/*                                                                           */
/* Description        : This function is used to reset all the Vlans         */
/*                      configured as ESP Vlan in the particular context.    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeSysConfigMcastEntry (UINT4 u4ContextId, tMacAddr DestMac,
                          tVlanId EspVlanId, tPortList EgressIfIndexList,
                          UINT1 u1Action)
{
    tConfigStMcastInfo  StaticMulticastInfo;
    UINT1               u1ErrorCode = PBBTE_INIT_VAL;
    INT4                i4RetVal = PBBTE_FAILURE;

    PBBTE_MEM_SET (&StaticMulticastInfo, PBBTE_INIT_VAL,
                   sizeof (tConfigStMcastInfo));

    StaticMulticastInfo.pEgressIfIndexList =
        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (StaticMulticastInfo.pEgressIfIndexList == NULL)
    {
        PBBTE_GBL_TRC (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC |
                       PBBTE_BUFFER_TRC, PBBTE_TRC_MEM_FAIL_BITLIST);
        return PBBTE_FAILURE;
    }
    PBBTE_MEM_SET (StaticMulticastInfo.pEgressIfIndexList, PBBTE_INIT_VAL,
                   sizeof (tPortList));

    StaticMulticastInfo.u4ContextId = u4ContextId;
    StaticMulticastInfo.VlanId = EspVlanId;
    PBBTE_CPY_MAC_ADDR (StaticMulticastInfo.DestMac, DestMac);

    /* Get from VLAN, whether Multicast Info is existing or not */
    i4RetVal = PbbTeVlanGetStaticMulticastEntry (&StaticMulticastInfo,
                                                 &u1ErrorCode);
    if (u1Action == VLAN_ADD)
    {
        if (i4RetVal == PBBTE_SUCCESS)
        {
            /* Entry is already existing, check whether the old port info
             * and new info are same. If differ, we have to configure */
            if (PBBTE_MEM_CMP (StaticMulticastInfo.pEgressIfIndexList,
                               EgressIfIndexList, sizeof (tPortList))
                != PBBTE_INIT_VAL)
            {
                PBBTE_MEM_CPY (StaticMulticastInfo.pEgressIfIndexList,
                               EgressIfIndexList, sizeof (tPortList));

                if (PbbTeVlanConfigStaticMcastEntry (&StaticMulticastInfo,
                                                     u1Action, &u1ErrorCode)
                    == PBBTE_FAILURE)
                {
                    PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC |
                                    PBBTE_ALL_FAILURE_TRC,
                                    PBBTE_TRC_ST_MCAST_MODIFIED_FAIL,
                                    EspVlanId);
                    FsUtilReleaseBitList
                        (*(StaticMulticastInfo.pEgressIfIndexList));
                    return PBBTE_FAILURE;
                }
                PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC,
                                PBBTE_TRC_ST_MCAST_MODIFIED, EspVlanId);
            }
        }
        else
        {
            /* Vlan static filtering entry not existing already, so create 
             * fresh entry */
            PBBTE_MEM_CPY (StaticMulticastInfo.pEgressIfIndexList,
                           EgressIfIndexList, sizeof (tPortList));
            StaticMulticastInfo.u4Status = VLAN_OTHER;

            if (PbbTeVlanConfigStaticMcastEntry (&StaticMulticastInfo, u1Action,
                                                 &u1ErrorCode) == PBBTE_FAILURE)
            {
                PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC |
                                PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_ST_MCAST_CREATE_FAIL, EspVlanId);
                FsUtilReleaseBitList
                    (*(StaticMulticastInfo.pEgressIfIndexList));
                return PBBTE_FAILURE;
            }
        }
        PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC, PBBTE_TRC_ST_MCAST_CREATE,
                        EspVlanId);
    }
    else
    {
        if (i4RetVal == PBBTE_SUCCESS)
        {
            if (StaticMulticastInfo.u4Status == VLAN_OTHER)
            {
                /* Static entry will be deleted, onl if its status is 
                 * VLAN_OTHER */
                if (PbbTeVlanConfigStaticMcastEntry (&StaticMulticastInfo,
                                                     u1Action, &u1ErrorCode)
                    == PBBTE_FAILURE)
                {
                    FsUtilReleaseBitList
                        (*(StaticMulticastInfo.pEgressIfIndexList));
                    return PBBTE_FAILURE;
                }
            }
        }
    }
    FsUtilReleaseBitList (*(StaticMulticastInfo.pEgressIfIndexList));
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeSysConfigUcastEntry                             */
/*                                                                           */
/* Description        : This function is used to reset all the Vlans         */
/*                      configured as ESP Vlan in the particular context.    */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeSysConfigUcastEntry (UINT4 u4ContextId, tMacAddr DestMac,
                          tVlanId EspVlanId, UINT4 u4EgressIfIndex,
                          UINT1 u1Action)
{
    tConfigStUcastInfo  StaticUnicastInfo;
    UINT1               u1ErrorCode = PBBTE_INIT_VAL;
    INT4                i4RetVal = PBBTE_FAILURE;

    PBBTE_MEM_SET (&StaticUnicastInfo, PBBTE_INIT_VAL,
                   sizeof (tConfigStUcastInfo));

    StaticUnicastInfo.u4ContextId = u4ContextId;
    StaticUnicastInfo.VlanId = EspVlanId;
    PBBTE_CPY_MAC_ADDR (StaticUnicastInfo.DestMac, DestMac);

    i4RetVal =
        PbbTeVlanGetStaticUnicastEntry (&StaticUnicastInfo, &u1ErrorCode);

    if (u1Action == VLAN_ADD)
    {
        if (i4RetVal == PBBTE_SUCCESS)
        {
            if (StaticUnicastInfo.u4EgressIfIndex != u4EgressIfIndex)
            {
                StaticUnicastInfo.u4EgressIfIndex = u4EgressIfIndex;

                if (PbbTeVlanConfigStaticUcastEntry (&StaticUnicastInfo,
                                                     u1Action, &u1ErrorCode)
                    == PBBTE_FAILURE)
                {
                    PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC |
                                    PBBTE_ALL_FAILURE_TRC,
                                    PBBTE_TRC_ST_UCAST_MODIFIED_FAIL,
                                    EspVlanId);
                    return PBBTE_FAILURE;
                }
                PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC,
                                PBBTE_TRC_ST_UCAST_MODIFIED, EspVlanId);
            }
        }
        else
        {
            StaticUnicastInfo.u4Status = VLAN_OTHER;
            StaticUnicastInfo.u4EgressIfIndex = u4EgressIfIndex;

            if (PbbTeVlanConfigStaticUcastEntry (&StaticUnicastInfo, u1Action,
                                                 &u1ErrorCode) == PBBTE_FAILURE)
            {
                PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC |
                                PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_ST_UCAST_CREATE_FAIL, EspVlanId);
                return PBBTE_FAILURE;
            }
            PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC,
                            PBBTE_TRC_ST_UCAST_CREATE, EspVlanId);
        }
    }
    else
    {
        if (i4RetVal == PBBTE_SUCCESS)
        {
            if (StaticUnicastInfo.u4Status == VLAN_OTHER)
            {
                /* Static entry will be deleted, only if its status is 
                 * VLAN_OTHER */
                if (PbbTeVlanConfigStaticUcastEntry (&StaticUnicastInfo,
                                                     u1Action, &u1ErrorCode)
                    == PBBTE_FAILURE)
                {
                    return PBBTE_FAILURE;
                }
            }
        }
    }
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbTeProcessCfgEvent                             */
/*                                                                           */
/*    Description         : Function Process the received configuration event*/
/*                                                                           */
/*    Input(s)            : pPbbTeQMsg - Pointer to the Q Message            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
PbbTeProcessCfgEvent (tPbbTeQMsg * pPbbTeQMsg)
{
#ifdef MBSM_WANTED
    INT4                i4RetVal;
    INT4                i4ProtoId = 0;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmPortInfo      *pPortInfo = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;
#endif /* MBSM_WANTED */

    switch (PBBTE_Q_MSG_TYPE (pPbbTeQMsg))
    {
        case PBBTE_CONTEXT_DELETE_MSG:

            PbbTeSysModuleShutInCtxt (pPbbTeQMsg->u4ContextId);
            L2MI_SYNC_GIVE_SEM ();

            break;

#ifdef MBSM_WANTED
        case MBSM_MSG_CARD_INSERT:

            i4ProtoId = pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg->i4ProtoCookie;
            pSlotInfo =
                &(pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmSlotInfo);
            pPortInfo =
                &(pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmPortInfo);

            i4RetVal = PbbTeMbsmUpdateOnCardInsertion (pPortInfo, pSlotInfo);

            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
            MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
            MbsmSendAckFromProto (&MbsmProtoAckMsg);
            MEM_FREE (pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg);

            break;

        case MBSM_MSG_CARD_REMOVE:

            i4ProtoId = pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg->i4ProtoCookie;
            pSlotInfo =
                &(pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmSlotInfo);
            pPortInfo =
                &(pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg->MbsmPortInfo);

            i4RetVal = PbbTeMbsmUpdateOnCardRemoval (pPortInfo, pSlotInfo);

            MbsmProtoAckMsg.i4ProtoCookie = i4ProtoId;
            MbsmProtoAckMsg.i4SlotId = MBSM_SLOT_INFO_SLOT_ID (pSlotInfo);
            MbsmProtoAckMsg.i4RetStatus = i4RetVal;
            MbsmSendAckFromProto (&MbsmProtoAckMsg);
            MEM_FREE (pPbbTeQMsg->MbsmCardUpdate.pMbsmProtoMsg);

            break;
#endif

#ifdef L2RED_WANTED
        case PBBTE_RM_MSG:

            PbbTeRedProcessUpdateMsg (pPbbTeQMsg);
            break;
#endif

        default:
            break;
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : PbbTeDisable                                     */
/*                                                                           */
/*    Description         : All the dynamic entries are identifed and deleted*/
/*                                                                           */
/*    Input(s)            : u4ContextId - Context Id                         */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None                                       */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeDisable (UINT4 u4ContextId)
{
    tEspInfo            EspInfo;
    UINT4               u4TeSid = 0;
    UINT4               u4EspIndex = 0;
    UINT4               u4RetVal = PBBTE_FAILURE;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    if (PbbTeTeSidEspEntryGetNext (u4TeSid, u4EspIndex, &EspInfo) ==
        PBBTE_SUCCESS)
    {
        do
        {
            if (EspInfo.u4ContextId != u4ContextId)
            {
                continue;
            }

            if (EspInfo.u1StorageType == PBBTE_STORAGE_VOLATILE)
            {
                /* Delete this dynamic entry */
                PbbTeTeSidEspEntryDel (EspInfo.u4TeSid, EspInfo.u4EspIndex);
            }
        }
        while ((u4RetVal = (PbbTeTeSidEspEntryGetNext ((EspInfo.u4TeSid),
                                                       (EspInfo.u4EspIndex),
                                                       &EspInfo)) !=
                PBBTE_FAILURE));
    }

    return PBBTE_SUCCESS;
}

/************************************************************************/
/* Function Name    : PbbTeTaskInit                                     */
/*                                                                      */
/* Description      : Creates PBB-TE Config Queue and Sema4             */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : PBBTE_SUCCESS / PBBTE_FAILURE                     */
/************************************************************************/
INT4
PbbTeTaskInit ()
{

    if (PBBTE_CREATE_SEM (PBBTE_SEM_NAME, &(gPbbTeGlobalInfo.DataSemId))
        != OSIX_SUCCESS)
    {
        PBBTE_GBL_TRC (PBBTE_INIT_SHUT_TRC | PBBTE_OS_RESOURCE_TRC |
                       PBBTE_ALL_FAILURE_TRC, PBBTE_TRC_SEM_FAIL);
        return PBBTE_FAILURE;
    }

    PBBTE_UNLOCK ();

    if (PBBTE_CREATE_QUEUE (PBBTE_CFG_QUEUE, PBBTE_MAX_MSG_LEN,
                            PBBTE_CFG_Q_DEPTH,
                            &(PBBTE_CFG_QUEUE_ID)) != OSIX_SUCCESS)
    {
        PBBTE_GBL_TRC (PBBTE_INIT_SHUT_TRC | PBBTE_OS_RESOURCE_TRC |
                       PBBTE_ALL_FAILURE_TRC, PBBTE_TRC_Q_FAIL);
        PBBTE_DELETE_SEM (gPbbTeGlobalInfo.DataSemId);
        return PBBTE_FAILURE;
    }
    return PBBTE_SUCCESS;

}

/************************************************************************/
/* Function Name    : PbbTeDeInit                                       */
/*                                                                      */
/* Description      : Deletes PBB-TE Config Queue, Sema4,               */
/*                    Memory pools and global informations              */
/*                                                                      */
/* Input(s)         : None                                              */
/*                                                                      */
/* Output(s)        : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Referred         : None                                              */
/*                                                                      */
/* Global Variables                                                     */
/* Modified         : None                                              */
/*                                                                      */
/* Exceptions or OS                                                     */
/* Error Handling   : None                                              */
/*                                                                      */
/* Use of Recursion : None                                              */
/*                                                                      */
/* Returns          : None.                                             */
/************************************************************************/
VOID
PbbTeDeInit ()
{
    gu1IsPbbTeInitialised = PBBTE_FALSE;

    if (gPbbTeGlobalInfo.pGlblEspDMacVIDInfo != NULL)
    {
        TMO_HASH_Delete_Table ((gPbbTeGlobalInfo.pGlblEspDMacVIDInfo), NULL);
    }

    if (gPbbTeGlobalInfo.TeSidTable != NULL)
    {
        RBTreeDestroy (gPbbTeGlobalInfo.TeSidTable, NULL, 0);
    }

    PbbteSizingMemDeleteMemPools ();

    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeHandleStartModule                               */
/*                                                                           */
/* Description        : This function will start PBB-TE module               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE.                       */
/*****************************************************************************/
INT4
PbbTeHandleStartModule ()
{
    UINT4               u4RetVal = PBBTE_FAILURE;

    /* Creation the Global ESP DMAC VID Info hash */
    gPbbTeGlobalInfo.pGlblEspDMacVIDInfo =
        TMO_HASH_Create_Table (PBBTE_MAX_BUCKETS, NULL, FALSE);

    gPbbTeGlobalInfo.u4TraceOption = PBBTE_ALL_FAILURE_TRC;

    gPbbTeGlobalInfo.TeSidTable =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tTeSidEspEntry, RBNode),
                              PbbTeTeSidTableCmp);

    PBBTE_MEM_SET (&gPbbTeEspPathInfo, PBBTE_INIT_VAL, sizeof (tEspPathInfo));

#ifdef SNMP_2_WANTED
    /* MIB Registrations */
    RegisterFS1AY ();
    RegisterFSPBT ();
#endif

    PBBTE_PERF_INIT ();

    gu1IsPbbTeInitialised = PBBTE_TRUE;

    PbbTeRedInitGlobalInfo ();

    if (PbbteSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        PBBTE_GBL_TRC (PBBTE_INIT_SHUT_TRC | PBBTE_OS_RESOURCE_TRC |
                       PBBTE_ALL_FAILURE_TRC, PBBTE_TRC_Q_MEM_FAIL);
        return PBBTE_FAILURE;
    }
    PbbTeAssignMemPoolsId ();
    /* RM Registration */
    u4RetVal = (UINT4) PbbTeRedRegisterWithRm ();

    if (u4RetVal == (UINT4) PBBTE_FAILURE)
    {
        PBBTE_GBL_TRC (PBBTE_INIT_SHUT_TRC | PBBTE_OS_RESOURCE_TRC |
                       PBBTE_ALL_FAILURE_TRC, PBBTE_TRC_RM_REG_FAIL);
        return PBBTE_FAILURE;
    }

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeAssignMemPoolsId                                */
/*                                                                           */
/* Description        : This function will Assign the mempool Id's for       */
/*                      all mempools created for PBB-TE module               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbTeAssignMemPoolsId (VOID)
{
    PBBTE_Q_MEMPOOL_ID = PBBTEMemPoolIds[MAX_PBBTE_CONFIG_Q_MESG_SIZING_ID];
    PBBTE_CONTEXT_MEMPOOL_ID =
        PBBTEMemPoolIds[MAX_PBBTE_CONTEXT_INFO_SIZING_ID];
    PBBTE_TESID_MEMPOOL_ID =
        PBBTEMemPoolIds[MAX_PBBTE_SERVICE_ID_ESP_ENTRIES_SIZING_ID];
    PBBTE_ESP_DESTMAC_MEMPOOL_ID =
        PBBTEMemPoolIds[MAX_PBBTE_ESP_DMAC_VID_INFO_SIZING_ID];
    PBBTE_RED_AUD_BUF_MEMPOOL_ID =
        PBBTEMemPoolIds[MAX_PBBTE_RED_BUFER_ENTRIES_SIZING_ID];

    return;
}

#endif
