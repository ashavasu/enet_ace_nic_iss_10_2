/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: pbtapi.c,v 1.4 2009/09/24 14:30:08 prabuc Exp $                  *
 *                                                                           *
 * Description: This file contains API routine exporte by the                *
 *              PBB-TE module                                                *
 *                                                                           *
 *****************************************************************************/
#include "pbthdrs.h"
/*****************************************************************************/
/* Function Name      : PbbTeInit                                            */
/*                                                                           */
/* Description        : This is the initialization function for the PBB-TE   */
/*                      module, called at bootup time from. This function    */
/*                      performs MemPool initializations and MIB registration*/
/*                      with the Aricent SNMP module.                        */
/*                                                                           */
/* Input(s)           : pi1Param                                             */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
VOID
PbbTeInit (INT1 *pi1Param)
{
    tPbbTeQMsg         *pPbbTeQMsg = NULL;
    INT4                i4RetVal = PBBTE_FAILURE;
    UINT4               u4Events;

    UNUSED_PARAM (pi1Param);

    i4RetVal = PbbTeTaskInit ();

    if (i4RetVal == PBBTE_FAILURE)
    {
        PBBTE_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    i4RetVal = PbbTeHandleStartModule ();

    if (i4RetVal == PBBTE_FAILURE)
    {
        PbbTeRmDeRegisterProtocols ();
        PbbTeDeInit ();
        PBBTE_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (PBBTE_GET_TASK_ID (PBBTE_SELF, PBBTE_TASK, &(PBBTE_TASK_ID)) !=
        OSIX_SUCCESS)
    {
        PbbTeRmDeRegisterProtocols ();
        PbbTeDeInit ();
        PBBTE_GBL_TRC (PBBTE_INIT_SHUT_TRC | PBBTE_OS_RESOURCE_TRC |
                       PBBTE_ALL_FAILURE_TRC, PBBTE_TRC_TASK_FAIL);
        PBBTE_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    PBBTE_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if (PBBTE_RECEIVE_EVENT (PBBTE_TASK_ID, (PBBTE_CFG_MSG_EVENT |
                                                 PBBTE_RED_BULK_UPD_EVENT),
                                 OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            if (u4Events & PBBTE_CFG_MSG_EVENT)
            {
                while (PBBTE_RECV_FROM_QUEUE (PBBTE_CFG_QUEUE_ID,
                                              (UINT1 *) &pPbbTeQMsg,
                                              PBBTE_DEF_MSG_LEN,
                                              OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    PbbTeProcessCfgEvent (pPbbTeQMsg);
                    PBBTE_Q_MSG_ENTRY_FREE_MEMBLK (pPbbTeQMsg);
                }
            }

            if (u4Events & PBBTE_RED_BULK_UPD_EVENT)
            {
                PbbTeRedHandleBulkUpdateEvent ();
            }
        }
    }
}

/*****************************************************************************/
/* Function Name      : PbbTeVidIsEspVid                                     */
/*                                                                           */
/* Description        : This API is exported to other modules to check       */
/*                      whether a particular VlanId is an ESP Vlan in the    */
/*                      particular context.                                  */
/*                                                                           */
/* Input(s)           : u4ContextId - ContextIdentifer                       */
/*                      EspVlanId   - An integer that represents the ESP Vlan*/
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE / OSIX_FALSE                               */
/*****************************************************************************/
INT4
PbbTeVidIsEspVid (UINT4 u4ContextId, tVlanId VlanId)
{
    UINT1               u1Result = OSIX_FALSE;

    if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_SHUTDOWN)
    {
        return OSIX_FALSE;
    }

    PBBTE_IS_ESP_VLAN (u4ContextId, VlanId, u1Result);

    return u1Result;
}

/*****************************************************************************/
/* Function Name      : PbbTeConfigureEsp                                    */
/*                                                                           */
/* Description        : This API is used to support any external control     */
/*                      plane that dynamically create or deletet the         */
/*                      traffice engineered paths across the backbone network*/
/*                      It is called to set up the vlan static unicast or    */
/*                      multicast table and optionally the TESID table from  */
/*                      the external control plane.                          */
/*                                                                           */
/* Input(s)           : pEspPathInfo - Pointer to the EspPath Information    */
/*                      u1Action     - Indicates create or delete the ESP    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_TRUE / PBBTE_FALSE                             */
/*****************************************************************************/
INT4
PbbTeConfigureEsp (tEspPathInfo * pEspPathInfo, UINT1 u1Action)
{
    if (PBBTE_NODE_STATE != PBBTE_NODE_IDLE)
    {
        return (PbbTeUtlConfigureEsp (pEspPathInfo, u1Action));
    }

    return PBBTE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PbbTeDeleteContext                                   */
/*                                                                           */
/* Description        : This API is called when a virtual context is deleted.*/
/*                      Cleans up the PBB-TE informations in the context.    */
/*                                                                           */
/* Input(s)           : u4ContextId - ContextId                              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeDeleteContext (UINT4 u4ContextId)
{
    /* post message to PBB-TE */
    tPbbTeQMsg         *pPbbTeQMsg = NULL;

    if (gu1IsPbbTeInitialised == PBBTE_FALSE)
    {
        /* PBB-TE Task is not initialised  */
        return PBBTE_SUCCESS;
    }

    PBBTE_Q_MSG_ENTRY_ALLOC_MEMBLK (pPbbTeQMsg);

    if (NULL == pPbbTeQMsg)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_Q_BUF_MEM_FAIL);
        return PBBTE_FAILURE;
    }

    PBBTE_MEM_SET (pPbbTeQMsg, 0, sizeof (tPbbTeQMsg));

    pPbbTeQMsg->u4MsgType = PBBTE_CONTEXT_DELETE_MSG;
    pPbbTeQMsg->u4ContextId = u4ContextId;

    if (PBBTE_SEND_TO_QUEUE (PBBTE_CFG_QUEUE_ID,
                             (UINT1 *) &pPbbTeQMsg,
                             OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC | PBBTE_OS_RESOURCE_TRC,
                       PBBTE_TRC_Q_SEND_TO_FAIL);

        PBBTE_Q_MSG_ENTRY_FREE_MEMBLK (pPbbTeQMsg);

        return PBBTE_FAILURE;
    }

    if (PBBTE_SEND_EVENT (PBBTE_TASK_ID, PBBTE_CFG_MSG_EVENT) == OSIX_FAILURE)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_SEND_EVENT_FAIL);

        /* No Need to Relase the buffer since Msg is already posted */
        return PBBTE_FAILURE;
    }

    L2MI_SYNC_TAKE_SEM ();
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeGetEspInfo                                      */
/*                                                                           */
/* Description        : This API is used get the EspInformation from the     */
/*                      TE-SID table using ContextID and source MAC address  */
/*                                                                           */
/* Input(s)           : pEspInfo->u4ContextId - ContextId                    */
/*                      pEspInfo->u4TeSid     -  TE-SID of the ESP.          */
/*                      pEspInfo->SourceAddr  - Source MAC Address in ESP    */
/*                                                                           */
/* Output(s)          : pEspInfo->DestAddr    - Destination Address in ESP   */
/*                      pEspInfo->EspVlan     - VLAN Id of the ESP.          */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeGetEspInfo (tEspInfo * pEspInfo)
{
    if (PbbTeTeSidGetESPInfoByTeSid (pEspInfo) == PBBTE_SUCCESS)
    {
        return ((pEspInfo->u1RowStatus == PBBTE_ACTIVE) ? PBBTE_SUCCESS
                : PBBTE_FAILURE);
    }
    return PBBTE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PbbTeStartModule                                     */
/*                                                                           */
/* Description        : This function is an API to start the PBB-TE module   */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE.                       */
/*****************************************************************************/
INT4
PbbTeStartModule (VOID)
{
    PBBTE_LOCK ();

    if (PbbTeHandleStartModule () == PBBTE_FAILURE)
    {
        PBBTE_UNLOCK ();
        return PBBTE_FAILURE;
    }

    PBBTE_UNLOCK ();
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeShutdownModule                                  */
/*                                                                           */
/* Description        : This function will shutdown the PBB-TE module        */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE.                       */
/*****************************************************************************/
INT4
PbbTeShutdownModule (VOID)
{
    tPbbTeQMsg         *pPbbTeQMsg = NULL;
    UINT4               u4ContextId;

    /* PBB-TE initialised status should be made false for avoiding the
     * Message posting, configurations when mempools, timers etc.,
     * are deleted here*/

    PBBTE_LOCK ();

    gu1IsPbbTeInitialised = PBBTE_FALSE;
    /* HW Audit task should be deleted if running,
     * before flushing out the data structures otherwise it will access
     * unavailable entries.
     */
#ifdef L2RED_WANTED
    if (PBBTE_RED_AUDIT_TASK_ID != 0)
    {
        PBBTE_DELETE_TASK (PBBTE_RED_AUDIT_TASK_ID);
        PBBTE_RED_AUDIT_TASK_ID = 0;
    }
#endif

    while (PBBTE_RECV_FROM_QUEUE (PBBTE_CFG_QUEUE_ID,
                                  (UINT1 *) &pPbbTeQMsg,
                                  OSIX_DEF_MSG_LEN,
                                  OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        PBBTE_Q_MSG_ENTRY_FREE_MEMBLK (pPbbTeQMsg);
    }

    for (u4ContextId = 0; u4ContextId < PBBTE_MAX_CONTEXTS; u4ContextId++)
    {
        if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_SHUTDOWN)
        {
            PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_MOD_ALREADY_SHUT,
                                u4ContextId);
            continue;
        }

        PBBTE_UNLOCK ();
        PbbTeSysModuleShutInCtxt (u4ContextId);
        PBBTE_LOCK ();
    }

    PbbTeRmDeRegisterProtocols ();

    PbbTeDeInit ();

    PBBTE_UNLOCK ();
    return PBBTE_SUCCESS;
}
