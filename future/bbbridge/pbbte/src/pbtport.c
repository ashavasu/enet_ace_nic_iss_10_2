
#include "pbthdrs.h"

/*****************************************************************************/
/* Function Name      : PbbTeVlanConfigStaticUcastEntry                      */
/*                                                                           */
/* Description        : This function calls the VLAN module to configure     */
/*                      (Add or delete) the static unicast entry.            */
/*                                                                           */
/* Input(s)           : pStaticUnicastInfo - Pointer to the structure        */
/*                                           containing the static unicast   */
/*                                           address info.                   */
/*                      u1Action - Denotes, whether the entry needs to be    */
/*                                 deleted or added                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeVlanConfigStaticUcastEntry (tConfigStUcastInfo * pStaticUnicastInfo,
                                 UINT1 u1Action, UINT1 *pu1ErrorCode)
{

    return ((VlanConfigStaticUcastEntry (pStaticUnicastInfo, u1Action,
                                         pu1ErrorCode)
             == VLAN_FAILURE) ? PBBTE_FAILURE : PBBTE_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PbbTeVlanConfigStaticMcastEntry                      */
/*                                                                           */
/* Description        : This function calls the VLAN module to configure     */
/*                      (Add or delete) the static multicast entry.          */
/*                                                                           */
/* Input(s)           : pStaticMulticastInfo - Pointer to the structure      */
/*                      containing the static multicast address info.        */
/*                                                                           */
/*                      u1Action - Denotes, whether the entry needs to be    */
/*                                 deleted or added                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeVlanConfigStaticMcastEntry (tConfigStMcastInfo
                                 * pStaticMulticastInfo, UINT1 u1Action,
                                 UINT1 *pu1ErrorCode)
{
    return ((VlanConfigStaticMcastEntry (pStaticMulticastInfo, u1Action,
                                         pu1ErrorCode)
             == VLAN_FAILURE) ? PBBTE_FAILURE : PBBTE_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PbbTeVlanDelStFiltEntStatusOther                     */
/*                                                                           */
/* Description        : This function deletes all the static unicast  or     */
/*                      multicast entries configured on the particular vlan. */
/*                                                                           */
/* Input(s)           : u4ContextId - Denotes in which context, the config   */
/*                                    has to be applied.                     */
/*                      VlanId      - Identifies the VLAN                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeVlanDelStFiltEntStatusOther (UINT4 u4ContextId, tVlanId VlanId,
                                  UINT1 *pu1ErrCode)
{
    return ((VlanDelStFiltEntStatusOther (u4ContextId, VlanId, pu1ErrCode)
             == VLAN_FAILURE) ? PBBTE_FAILURE : PBBTE_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PbbTeVlanGetStaticMulticastEntry                     */
/*                                                                           */
/* Description        : This function calls the VLAN module to Get the       */
/*                      information from the static multicast entry.         */
/*                                                                           */
/* Input(s)           : pStaticMulticastInfo - Pointer to the structure      */
/*                      containing the static multicast address info.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeVlanGetStaticMulticastEntry (tConfigStMcastInfo *
                                  pStaticMulticastInfo, UINT1 *pu1ErrCode)
{
    return ((VlanGetStaticMulticastEntry (pStaticMulticastInfo, pu1ErrCode)
             == VLAN_FAILURE) ? PBBTE_FAILURE : PBBTE_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PbbTeVlanGetStaticUnicastEntry                       */
/*                                                                           */
/* Description        : This function calls the VLAN module to get the       */
/*                      information from the static unicast entry.           */
/*                                                                           */
/* Input(s)           : pStaticUnicastInfo - Pointer to the structure        */
/*                                           containing the static unicast   */
/*                                           address info.                   */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeVlanGetStaticUnicastEntry (tConfigStUcastInfo * pStaticUnicastInfo,
                                UINT1 *pu1ErrCode)
{
    return ((VlanGetStaticUnicastEntry (pStaticUnicastInfo, pu1ErrCode)
             == VLAN_FAILURE) ? PBBTE_FAILURE : PBBTE_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PbbTeVlanCfgLearningFlooding                         */
/*                                                                           */
/* Description        : This function calls the VLAN module to configure     */
/*                      Learning and flooding in the particular VLAN.        */
/*                                                                           */
/* Input(s)           : u4ContextId - Denotes in which context, the config   */
/*                                    has to be applied.                     */
/*                      VlanId      - Identifies the VLAN                    */
/*                      u1Action    - Denotes, whether Enabling or disabling */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*+***************************************************************************/
INT4
PbbTeVlanCfgLearningFlooding (UINT4 u4ContextId, tVlanId VlanId, UINT1 u1Action,
                              UINT1 *pu1ErrCode)
{
    return ((VlanCfgLearningFlooding (u4ContextId, VlanId, u1Action, pu1ErrCode)
             == VLAN_FAILURE) ? PBBTE_FAILURE : PBBTE_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PbbTeMstpMapVidToMstId                               */
/*                                                                           */
/* Description        : This function calls the  STP module to map the       */
/*                      VLAN to the MSTP Instance.                           */
/*                                                                           */
/* Input(s)           : u4ContextId - Denotes in which context, the config   */
/*                                    has to be applied.                     */
/*                      VlanId      - Identifies the VLAN                    */
/*                      u2MstId     - Denotes the instance to which the      */
/*                                    particular VLAN needs to be mapped     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeMstpMapVidToMstId (UINT4 u4ContextId, tVlanId VlanId, UINT2 u2MstId)
{
    return ((MstpMapVidToMstId (u4ContextId, VlanId, u2MstId)
             == MST_FAILURE) ? PBBTE_FAILURE : PBBTE_SUCCESS);
}

/*****************************************************************************/
/* Function Name      : PbbTeIsVlanActive                                    */
/*                                                                           */
/* Description        : This routine is called to check whether the given    */
/*                      Vlan is active.                                      */
/*                                                                           */
/* Input(s)           : ContextId, VlanId                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_TRUE/ OSIX_FALSE                                */
/*****************************************************************************/
BOOL1
PbbTeIsVlanActive (UINT4 u4ContextId, tVlanId VlanId)
{
    return (L2IwfMiIsVlanActive (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : PbbTeVcmIsSwitchExist                                */
/*                                                                           */
/* Description        : This function calls the VCM Module to check whether  */
/*                      the context exist or not.                            */
/*                                                                           */
/* Input(s)           : pu1Alias - Alias Name                                */
/*                                                                           */
/* Output(s)          : pu4VcNum - Context Identifier                        */
/*                                                                           */
/* Return Value(s)    : VCM_FALSE / VCM_TRUE                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    return (VcmIsSwitchExist (pu1Alias, pu4VcNum));
}

/*****************************************************************************/
/* Function Name      : PbbTeVcmGetAliasName                                 */
/*                                                                           */
/* Description        : This function calls the VCM Module to get the        */
/*                      Alias name of the given context.                     */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier.                    */
/*                                                                           */
/* Output(s)          : pu1Alias    - Alias Name                             */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    return (VcmGetAliasName (u4ContextId, pu1Alias));
}

/*****************************************************************************
 *
 *    Function Name       : PbbTeCfaCliGetIfName
 *
 *    Description         : This function is used to get the Interface name 
 *                          for given interface index.
 *
 *    Input(s)            : Interface index
 *
 *
 *    Output(s)           : pi1IfName - Interface name of the given 
 *                          interface index.
 *
 *    Returns             : CLI_SUCCESS or CLI_FAILURE.
 *****************************************************************************/
INT4
PbbTeCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
#ifdef CLI_WANTED
    return (CfaCliGetIfName (u4IfIndex, pi1IfName));
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pi1IfName);
    return CLI_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmEnqMsgToRmFromAppl                            */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbTeRmEnqMsgToRmFromAppl (tRmMsg * pRmMsg, UINT2 u2DataLen, UINT4 u4SrcEntId,
                           UINT4 u4DestEntId)
{
#ifdef L2RED_WANTED
    return (RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen, u4SrcEntId, u4DestEntId));
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmGetNodeState                                  */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbTeRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmGetStandbyNodeCount                           */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
PbbTeRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmGetStaticConfigStatus                         */
/*                                                                           */
/* Description        : This function calls the RM module to get the         */
/*                      status of static configuration restoration.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
PbbTeRmGetStaticConfigStatus (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStaticConfigStatus ());
#else
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmHandleProtocolEvent                           */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT1
PbbTeRmHandleProtocolEvent (tRmProtoEvt * pEvt)
{
#ifdef L2RED_WANTED
    return (RmApiHandleProtocolEvent (pEvt));
#else
    UNUSED_PARAM (pEvt);
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmRegisterProtocols                             */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbTeRmRegisterProtocols (tRmRegParams * pRmReg)
{
#ifdef L2RED_WANTED
    return (RmRegisterProtocols (pRmReg));
#else
    UNUSED_PARAM (pRmReg);
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmDeRegisterProtocols                           */
/*                                                                           */
/* Description        : This function deregisters PBB-TE with RM.            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbTeRmDeRegisterProtocols (VOID)
{
#ifdef L2RED_WANTED
    return (RmDeRegisterProtocols (RM_PBBTE_APP_ID));
#else
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmReleaseMemoryForMsg                           */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
UINT4
PbbTeRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
#ifdef L2RED_WANTED
    return (RmReleaseMemoryForMsg (pu1Block));
#else
    UNUSED_PARAM (pu1Block);
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeRmSetBulkUpdatesStatus                          */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
#ifdef L2RED_WANTED
    return (RmSetBulkUpdatesStatus (u4AppId));
#else
    UNUSED_PARAM (u4AppId);
    return RM_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : PbbTeMrpPropVlanDelFromEsp                           */
/*                                                                           */
/* Description        : This function calls the MRP module to propagate the  */
/*                      VLAN which is removed from ESP List.                 */
/*                                                                           */
/* Input(s)           : u4ContextId - Denotes in which context, the config   */
/*                                    has to be applied.                     */
/*                      VlanId      - Identifies the VLAN                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeMrpPropVlanDelFromEsp (UINT4 u4ContextId, tVlanId VlanId)
{
    return (MrpApiPropVlanDelFromEsp (u4ContextId, VlanId));
}

/*****************************************************************************/
/* Function Name      : PbbTeMrpRemVlanAddedInEsp                           */
/*                                                                           */
/* Description        : This function calls the MRP module to remove the     */
/*                      VLAN which is added to ESP now.                      */
/*                                                                           */
/* Input(s)           : u4ContextId - Denotes in which context, the config   */
/*                                    has to be applied.                     */
/*                      VlanId      - Identifies the VLAN                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS / PBBTE_FAILURE                        */
/*                                                                           */
/*****************************************************************************/
INT4
PbbTeMrpRemVlanAddedInEsp (UINT4 u4ContextId, tVlanId VlanId)
{
    return (MrpApiRemVlanAddedInEsp (u4ContextId, VlanId));
}
