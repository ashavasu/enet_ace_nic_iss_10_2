/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: pbtrdstb.c,v                                                         *
 *                                                                           *
 * Description: This file contains RM Stub routine used by the               *
 *              PBB-TE module                                                *
 *                                                                           *
 *****************************************************************************/
#include "pbthdrs.h"

/*****************************************************************************/
/* Function Name      : PbbTeRedInitGlobalInfo                                */
/*                                                                           */
/* Description        : Initializes redundancy global variables.             */
/*                      This function will get invoked during BOOTUP.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PbbTeRedInitGlobalInfo (VOID)
{
    PBBTE_NODE_STATE = PBBTE_NODE_ACTIVE;
    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedRegisterWithRm                               */
/*                                                                           */
/* Description        : Registers PBBTE module with RM to send and receive   */
/*                      update messages.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then PBBTE_SUCCESS        */
/*                      Otherwise PBBTE_FAILURE                              */
/*****************************************************************************/
INT4
PbbTeRedRegisterWithRm (VOID)
{
    return PBBTE_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : PbbTeRedHandleBulkUpdateEvent                           */
/*                                                                            */
/* Description      : It Handles the bulk update event. This event is used    */
/*                    to start the next sub bulk update. So                   */
/*                    PbbTeRedSendBulkUpdates is triggered.                   */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbTeRedHandleBulkUpdateEvent (VOID)
{
    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedSyncDynamicNonMonEsp                         */
/*                                                                           */
/* Description        : This routine forms the Dynamic Update for the        */
/*                      Non-Monitored ESPs                                   */
/*                      created through API by external module. It sends the */
/*                      packet to Active RM which sends it to Peer node      */
/*                                                                           */
/* Input(s)           : pEspPathInfo - Pointer ESP Structure to synchronized */
/*                      u1Action    - Creation/deletion                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS/PBBTE_FAILURE                          */
/*****************************************************************************/
INT4
PbbTeRedSyncDynamicNonMonEsp (UINT4 u4ContextId, tMacAddr DestMac,
                              tVlanId EspVlanId, UINT1 u1Action)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (DestMac);
    UNUSED_PARAM (EspVlanId);
    UNUSED_PARAM (u1Action);
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedSyncDynamicMonEsp                            */
/*                                                                           */
/* Description        : This routine forms the Dynamic Update for the        */
/*                      Monitored ESPs                                       */
/*                      created through API by external module. It sends the */
/*                      packet to Active RM which sends it to Peer node      */
/*                                                                           */
/* Input(s)           : pEspPathInfo - Pointer ESP Structure to synchronized */
/*                      u1Action    - Creation/deletion                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS/PBBTE_FAILURE                          */
/*****************************************************************************/
INT4
PbbTeRedSyncDynamicMonEsp (tEspPathInfo * pEspPathInfo, UINT1 u1Action)
{
    UNUSED_PARAM (pEspPathInfo);
    UNUSED_PARAM (u1Action);
    return PBBTE_SUCCESS;
}
