# include  "lr.h"
# include  "fssnmp.h"
# include  "pbt1aylw.h"
# include  "pbt1aywr.h"
# include  "pbt1aydb.h"

INT4
GetNextIndexIeee8021PbbTeProtectionGroupListTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021PbbTeProtectionGroupListTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021PbbTeProtectionGroupListTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFS1AY ()
{
    SNMPRegisterMib (&fs1ayOID, &fs1ayEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fs1ayOID, (const UINT1 *) "fs1ay");
}

VOID
UnRegisterFS1AY ()
{
    SNMPUnRegisterMib (&fs1ayOID, &fs1ayEntry);
    SNMPDelSysorEntry (&fs1ayOID, (const UINT1 *) "fs1ay");
}

INT4
Ieee8021PbbTeProtectionGroupListModeGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupListMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupListWorkingMAGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupListWorkingMA
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupListProtectionMAGet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupListProtectionMA
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupListStorageTypeGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupListStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupListRowStatusGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupListTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupListRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupListModeSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupListMode
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListWorkingMASet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupListWorkingMA
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListProtectionMASet (tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupListProtectionMA
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListStorageTypeSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupListStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListRowStatusSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupListRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListModeTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupListMode (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListWorkingMATest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupListWorkingMA (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListProtectionMATest (UINT4 *pu4Error,
                                                  tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupListProtectionMA (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiIndex->
                                                                   pIndex[1].
                                                                   u4_ULongValue,
                                                                   pMultiData->
                                                                   u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListStorageTypeTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupListStorageType (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  i4_SLongValue,
                                                                  pMultiIndex->
                                                                  pIndex[1].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListRowStatusTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupListRowStatus (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupListTableDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021PbbTeProtectionGroupListTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021PbbTeMASharedGroupTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021PbbTeMASharedGroupTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021PbbTeMASharedGroupTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021PbbTeMASharedGroupIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeMASharedGroupTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeMASharedGroupId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexIeee8021PbbTeTeSidTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021PbbTeTeSidTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021PbbTeTeSidTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021PbbTeTeSidEspGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeTeSidTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeTeSidEsp (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeTeSidStorageTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeTeSidTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeTeSidStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeTeSidRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeTeSidTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeTeSidRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeTeSidEspSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeTeSidEsp (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeTeSidStorageTypeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeTeSidStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeTeSidRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeTeSidRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeTeSidEspTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeTeSidEsp (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeTeSidStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeTeSidStorageType (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeTeSidRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeTeSidRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeTeSidTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021PbbTeTeSidTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021PbbTeProtectionGroupConfigTable (tSnmpIndex *
                                                     pFirstMultiIndex,
                                                     tSnmpIndex *
                                                     pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021PbbTeProtectionGroupConfigTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021PbbTeProtectionGroupConfigTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021PbbTeProtectionGroupConfigStateGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupConfigState
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupConfigCommandStatusGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupConfigCommandStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupConfigCommandAdminGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupConfigCommandAdmin
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupConfigWTRGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupConfigWTR
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupConfigHoldOffGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupConfigHoldOff
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupConfigStorageTypeGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupConfigStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupConfigCommandAdminSet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupConfigCommandAdmin
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupConfigWTRSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupConfigWTR
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupConfigHoldOffSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupConfigHoldOff
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupConfigStorageTypeSet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupConfigStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupConfigCommandAdminTest (UINT4 *pu4Error,
                                                    tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupConfigCommandAdmin (pu4Error,
                                                                     pMultiIndex->
                                                                     pIndex[0].
                                                                     i4_SLongValue,
                                                                     pMultiIndex->
                                                                     pIndex[1].
                                                                     u4_ULongValue,
                                                                     pMultiData->
                                                                     i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupConfigWTRTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupConfigWTR (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            i4_SLongValue,
                                                            pMultiIndex->
                                                            pIndex[1].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupConfigHoldOffTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupConfigHoldOff (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                i4_SLongValue,
                                                                pMultiIndex->
                                                                pIndex[1].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupConfigStorageTypeTest (UINT4 *pu4Error,
                                                   tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupConfigStorageType (pu4Error,
                                                                    pMultiIndex->
                                                                    pIndex[0].
                                                                    i4_SLongValue,
                                                                    pMultiIndex->
                                                                    pIndex[1].
                                                                    u4_ULongValue,
                                                                    pMultiData->
                                                                    i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupConfigTableDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021PbbTeProtectionGroupConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021PbbTeProtectionGroupISidTable (tSnmpIndex *
                                                   pFirstMultiIndex,
                                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021PbbTeProtectionGroupISidTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021PbbTeProtectionGroupISidTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021PbbTeProtectionGroupISidGroupIdGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupISidTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupISidGroupId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupISidStorageTypeGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupISidTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupISidStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupISidRowStatusGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupISidTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeProtectionGroupISidRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeProtectionGroupISidGroupIdSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupISidGroupId
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupISidStorageTypeSet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupISidStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupISidRowStatusSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeProtectionGroupISidRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupISidGroupIdTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupISidGroupId (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              u4_ULongValue));

}

INT4
Ieee8021PbbTeProtectionGroupISidStorageTypeTest (UINT4 *pu4Error,
                                                 tSnmpIndex * pMultiIndex,
                                                 tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupISidStorageType (pu4Error,
                                                                  pMultiIndex->
                                                                  pIndex[0].
                                                                  u4_ULongValue,
                                                                  pMultiData->
                                                                  i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupISidRowStatusTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeProtectionGroupISidRowStatus (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                i4_SLongValue));

}

INT4
Ieee8021PbbTeProtectionGroupISidTableDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021PbbTeProtectionGroupISidTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable (tSnmpIndex *
                                                             pFirstMultiIndex,
                                                             tSnmpIndex *
                                                             pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPortsGet (tSnmpIndex *
                                                          pMultiIndex,
                                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPortsGet (tSnmpIndex *
                                                             pMultiIndex,
                                                             tRetVal *
                                                             pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageTypeGet (tSnmpIndex *
                                                          pMultiIndex,
                                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatusGet (tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPortsSet (tSnmpIndex *
                                                          pMultiIndex,
                                                          tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPortsSet (tSnmpIndex *
                                                             pMultiIndex,
                                                             tRetVal *
                                                             pMultiData)
{
    return (nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageTypeSet (tSnmpIndex *
                                                          pMultiIndex,
                                                          tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatusSet (tSnmpIndex *
                                                        pMultiIndex,
                                                        tRetVal * pMultiData)
{
    return (nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPortsTest (UINT4 *pu4Error,
                                                           tSnmpIndex *
                                                           pMultiIndex,
                                                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPortsTest (UINT4 *pu4Error,
                                                              tSnmpIndex *
                                                              pMultiIndex,
                                                              tRetVal *
                                                              pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageTypeTest (UINT4 *pu4Error,
                                                           tSnmpIndex *
                                                           pMultiIndex,
                                                           tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatusTest (UINT4 *pu4Error,
                                                         tSnmpIndex *
                                                         pMultiIndex,
                                                         tRetVal * pMultiData)
{
    return (nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
            (pu4Error, pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
Ieee8021PbbTeBridgeStaticForwardAnyUnicastTableDep (UINT4 *pu4Error,
                                                    tSnmpIndexList *
                                                    pSnmpIndexList,
                                                    tSNMP_VAR_BIND *
                                                    pSnmpvarbinds)
{
    return (nmhDepv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
