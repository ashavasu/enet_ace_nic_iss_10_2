# include  "lr.h"
# include  "fssnmp.h"
# include  "pbtprplw.h"
# include  "pbtprpwr.h"
# include  "pbtprpdb.h"

VOID
RegisterFSPBT ()
{
    SNMPRegisterMib (&fspbtOID, &fspbtEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fspbtOID, (const UINT1 *) "fspbt");
}

VOID
UnRegisterFSPBT ()
{
    SNMPUnRegisterMib (&fspbtOID, &fspbtEntry);
    SNMPDelSysorEntry (&fspbtOID, (const UINT1 *) "fspbt");
}

INT4
FsPbbTeGlobalTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeGlobalTraceOption (&(pMultiData->u4_ULongValue)));
}

INT4
FsPbbTeGlobalTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeGlobalTraceOption (pMultiData->u4_ULongValue));
}

INT4
FsPbbTeGlobalTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeGlobalTraceOption
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsPbbTeGlobalTraceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeGlobalTraceOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbTeContextTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbTeContextTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbTeContextTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbTeContextSystemControlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbTeContextTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbTeContextSystemControl
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbTeContextTraceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbTeContextTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbTeContextTraceOption
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPbbTeContextNoOfActiveEspsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbTeContextTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbTeContextNoOfActiveEsps
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPbbTeContextNoOfCreatedEspsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbTeContextTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbTeContextNoOfCreatedEsps
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPbbTeContextNoOfDeletedEspsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbTeContextTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbTeContextNoOfDeletedEsps
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsPbbTeContextSystemControlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbTeContextSystemControl
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsPbbTeContextTraceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbTeContextTraceOption
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->u4_ULongValue));

}

INT4
FsPbbTeContextSystemControlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbTeContextSystemControl (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
FsPbbTeContextTraceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbTeContextTraceOption (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
FsPbbTeContextTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeContextTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbTeEspVidTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbTeEspVidTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbTeEspVidTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].i4_SLongValue,
             &(pNextMultiIndex->pIndex[1].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbTeEspVidRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbTeEspVidTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbTeEspVidRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbTeEspVidRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbTeEspVidRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].i4_SLongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsPbbTeEspVidRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbTeEspVidRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsPbbTeEspVidTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeEspVidTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsPbbTeTeSidExtTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPbbTeTeSidExtTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPbbTeTeSidExtTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsPbbTeTeSidExtContextIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsPbbTeTeSidExtTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsPbbTeTeSidExtContextId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsPbbTeTeSidExtContextIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsPbbTeTeSidExtContextId
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsPbbTeTeSidExtContextIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsPbbTeTeSidExtContextId (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsPbbTeTeSidExtTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTeSidExtTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTeTestApiContextIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeTestApiContextId (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbTeTestApiTeSidGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeTestApiTeSid (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbTeTestApiDestMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    pMultiData->pOctetStrValue->i4_Length = 6;
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeTestApiDestMacAddr
            ((tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));
}

INT4
FsPbbTeTestApiSourceMacAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    pMultiData->pOctetStrValue->i4_Length = 6;
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeTestApiSourceMacAddr
            ((tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList));
}

INT4
FsPbbTeTestApiEspVlanIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeTestApiEspVlanId (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbTeTestApiEgressPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeTestApiEgressPort (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbTeTestApiEgressPortListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeTestApiEgressPortList (pMultiData->pOctetStrValue));
}

INT4
FsPbbTeTestApiActionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsPbbTeTestApiAction (&(pMultiData->i4_SLongValue)));
}

INT4
FsPbbTeTestApiContextIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeTestApiContextId (pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiTeSidSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeTestApiTeSid (pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiDestMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeTestApiDestMacAddr
            ((*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));
}

INT4
FsPbbTeTestApiSourceMacAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeTestApiSourceMacAddr
            ((*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));
}

INT4
FsPbbTeTestApiEspVlanIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeTestApiEspVlanId (pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiEgressPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeTestApiEgressPort (pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiEgressPortListSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeTestApiEgressPortList (pMultiData->pOctetStrValue));
}

INT4
FsPbbTeTestApiActionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsPbbTeTestApiAction (pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiContextIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeTestApiContextId
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiTeSidTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeTestApiTeSid (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiDestMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeTestApiDestMacAddr
            (pu4Error,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));
}

INT4
FsPbbTeTestApiSourceMacAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    if (pMultiData->pOctetStrValue->i4_Length != MAC_ADDR_LEN)
    {
        *pu4Error = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeTestApiSourceMacAddr
            (pu4Error,
             (*(tMacAddr *) pMultiData->pOctetStrValue->pu1_OctetList)));
}

INT4
FsPbbTeTestApiEspVlanIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeTestApiEspVlanId
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiEgressPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeTestApiEgressPort
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiEgressPortListTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeTestApiEgressPortList
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsPbbTeTestApiActionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsPbbTeTestApiAction
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsPbbTeTestApiContextIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTestApiContextId
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTeTestApiTeSidDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTestApiTeSid
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTeTestApiDestMacAddrDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTestApiDestMacAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTeTestApiSourceMacAddrDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTestApiSourceMacAddr
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTeTestApiEspVlanIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTestApiEspVlanId
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTeTestApiEgressPortDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTestApiEgressPort
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTeTestApiEgressPortListDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTestApiEgressPortList
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPbbTeTestApiActionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPbbTeTestApiAction
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
