/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pbtprplw.c,v 1.18 2014/02/07 13:28:10 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "pbthdrs.h"

extern UINT4        FsPbbTeContextSystemControl[13];
extern UINT4        FsPbbTeContextTraceOption[13];
extern UINT4        FsPbbTeGlobalTraceOption[13];
extern UINT4        FsPbbTeEspVidRowStatus[13];
extern UINT4        FsPbbTeTeSidExtContextId[13];
extern UINT4        fspbt[9];
extern UINT4        fs1ay[9];
VOID                PbbteNotifyProtocolShutdownStatus (INT4);
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbbTeGlobalTraceOption
 Input       :  The Indices

                The Object 
                retValFsPbbTeGlobalTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeGlobalTraceOption (UINT4 *pu4RetValFsPbbTeGlobalTraceOption)
{
    PBBTE_GET_GBL_TRC_OPTION (*pu4RetValFsPbbTeGlobalTraceOption);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbbTeGlobalTraceOption
 Input       :  The Indices

                The Object 
                setValFsPbbTeGlobalTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeGlobalTraceOption (UINT4 u4SetValFsPbbTeGlobalTraceOption)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    PBBTE_MEM_SET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    PBBTE_SET_GBL_TRC_OPTION (u4SetValFsPbbTeGlobalTraceOption);

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsPbbTeGlobalTraceOption,
                          u4SeqNum, FALSE, NULL, NULL, 0, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", u4SetValFsPbbTeGlobalTraceOption));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeGlobalTraceOption
 Input       :  The Indices

                The Object 
                testValFsPbbTeGlobalTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeGlobalTraceOption (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsPbbTeGlobalTraceOption)
{
    if ((u4TestValFsPbbTeGlobalTraceOption < PBBTE_INIT_SHUT_TRC) ||
        (u4TestValFsPbbTeGlobalTraceOption > PBBTE_BUFFER_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeGlobalTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeGlobalTraceOption (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbTeContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbbTeContextTable
 Input       :  The Indices
                FsPbbTeContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbTeContextTable (INT4 i4FsPbbTeContextId)
{
    if (PBBTE_IS_VC_VALID (i4FsPbbTeContextId) != PBBTE_TRUE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbbTeContextTable
 Input       :  The Indices
                FsPbbTeContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbTeContextTable (INT4 *pi4FsPbbTeContextId)
{
    UINT4               u4Context = PBBTE_INIT_VAL;

    *pi4FsPbbTeContextId = PBBTE_INIT_VAL;

    for (; u4Context < PBBTE_MAX_CONTEXTS; u4Context++)
    {
        if (PBBTE_IS_SYSTEM_STARTED (u4Context) == PBBTE_START)
        {
            *pi4FsPbbTeContextId = (INT4) u4Context;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbbTeContextTable
 Input       :  The Indices
                FsPbbTeContextId
                nextFsPbbTeContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbTeContextTable (INT4 i4FsPbbTeContextId,
                                    INT4 *pi4NextFsPbbTeContextId)
{
    UINT4               u4Context = (UINT4) i4FsPbbTeContextId + 1;

    *pi4NextFsPbbTeContextId = PBBTE_INIT_VAL;

    for (; u4Context < PBBTE_MAX_CONTEXTS; u4Context++)
    {
        if (PBBTE_IS_SYSTEM_STARTED (u4Context) == PBBTE_START)
        {
            *pi4NextFsPbbTeContextId = (INT4) u4Context;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbbTeContextSystemControl
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                retValFsPbbTeContextSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeContextSystemControl (INT4 i4FsPbbTeContextId,
                                   INT4 *pi4RetValFsPbbTeContextSystemControl)
{
    *pi4RetValFsPbbTeContextSystemControl =
        PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeContextTraceOption
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                retValFsPbbTeContextTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeContextTraceOption (INT4 i4FsPbbTeContextId,
                                 UINT4 *pu4RetValFsPbbTeContextTraceOption)
{
    UINT4               u4TrcOpt = PBBTE_INIT_VAL;

    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) == PBBTE_SHUTDOWN)
    {
        return SNMP_FAILURE;
    }

    PBBTE_GET_CTX_TRC_OPTION (i4FsPbbTeContextId, u4TrcOpt);

    *pu4RetValFsPbbTeContextTraceOption = u4TrcOpt;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeContextNoOfActiveEsps
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                retValFsPbbTeContextNoOfActiveEsps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeContextNoOfActiveEsps (INT4 i4FsPbbTeContextId,
                                    UINT4
                                    *pu4RetValFsPbbTeContextNoOfActiveEsps)
{
    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) == PBBTE_SHUTDOWN)
    {
        return SNMP_FAILURE;
    }

    PBBTE_GET_NO_OF_ACTIVE_ESP (i4FsPbbTeContextId,
                                *pu4RetValFsPbbTeContextNoOfActiveEsps);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeContextNoOfCreatedEsps
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                retValFsPbbTeContextNoOfCreatedEsps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeContextNoOfCreatedEsps (INT4 i4FsPbbTeContextId,
                                     UINT4
                                     *pu4RetValFsPbbTeContextNoOfCreatedEsps)
{
    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) == PBBTE_SHUTDOWN)
    {
        return SNMP_FAILURE;
    }

    PBBTE_GET_NO_OF_CREATED_ESP (i4FsPbbTeContextId,
                                 *pu4RetValFsPbbTeContextNoOfCreatedEsps);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeContextNoOfDeletedEsps
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                retValFsPbbTeContextNoOfDeletedEsps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeContextNoOfDeletedEsps (INT4 i4FsPbbTeContextId,
                                     UINT4
                                     *pu4RetValFsPbbTeContextNoOfDeletedEsps)
{
    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) == PBBTE_SHUTDOWN)
    {
        return SNMP_FAILURE;
    }

    PBBTE_GET_NO_OF_DELETED_ESP (i4FsPbbTeContextId,
                                 *pu4RetValFsPbbTeContextNoOfDeletedEsps);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbbTeContextSystemControl
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                setValFsPbbTeContextSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeContextSystemControl (INT4 i4FsPbbTeContextId,
                                   INT4 i4SetValFsPbbTeContextSystemControl)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    PBBTE_MEM_SET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (i4SetValFsPbbTeContextSystemControl == PBBTE_START)
    {
        if (PbbTeSysModuleStartInCtxt (i4FsPbbTeContextId) == PBBTE_FAILURE)
        {
            PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_MOD_START_FAIL, i4FsPbbTeContextId);
            return SNMP_FAILURE;
        }
    }
    else if (i4SetValFsPbbTeContextSystemControl == PBBTE_SHUTDOWN)
    {
        PbbTeSysModuleShutInCtxt (i4FsPbbTeContextId);
        PbbteNotifyProtocolShutdownStatus (i4FsPbbTeContextId);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsPbbTeContextSystemControl,
                          u4SeqNum, FALSE, NULL, NULL, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FsPbbTeContextId,
                      i4SetValFsPbbTeContextSystemControl));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbTeContextTraceOption
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                setValFsPbbTeContextTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeContextTraceOption (INT4 i4FsPbbTeContextId,
                                 UINT4 u4SetValFsPbbTeContextTraceOption)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    PBBTE_MEM_SET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) == PBBTE_SHUTDOWN)
    {
        return SNMP_FAILURE;
    }

    PBBTE_SET_CTX_TRC_OPTION ((UINT4) i4FsPbbTeContextId,
                              u4SetValFsPbbTeContextTraceOption);

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsPbbTeContextTraceOption,
                          u4SeqNum, FALSE, NULL, NULL, 1, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4FsPbbTeContextId,
                      u4SetValFsPbbTeContextTraceOption));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeContextSystemControl
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                testValFsPbbTeContextSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeContextSystemControl (UINT4 *pu4ErrorCode,
                                      INT4 i4FsPbbTeContextId,
                                      INT4 i4TestValFsPbbTeContextSystemControl)
{
    if (PBBTE_IS_VC_VALID (i4FsPbbTeContextId) != PBBTE_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsPbbTeContextSystemControl != PBBTE_START) &&
        (i4TestValFsPbbTeContextSystemControl != PBBTE_SHUTDOWN))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeContextTraceOption
 Input       :  The Indices
                FsPbbTeContextId

                The Object 
                testValFsPbbTeContextTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeContextTraceOption (UINT4 *pu4ErrorCode,
                                    INT4 i4FsPbbTeContextId,
                                    UINT4 u4TestValFsPbbTeContextTraceOption)
{
    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) != PBBTE_START)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (PBBTE_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    if ((u4TestValFsPbbTeContextTraceOption < PBBTE_INIT_SHUT_TRC) ||
        (u4TestValFsPbbTeContextTraceOption > PBBTE_BUFFER_TRC))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeContextTable
 Input       :  The Indices
                FsPbbTeContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeContextTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbTeEspVidTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbbTeEspVidTable
 Input       :  The Indices
                FsPbbTeContextId
                FsPbbTeEspVid
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbTeEspVidTable (INT4 i4FsPbbTeContextId,
                                            INT4 i4FsPbbTeEspVid)
{
    if (PBBTE_IS_VC_VALID (i4FsPbbTeContextId) != PBBTE_TRUE)
    {
        return SNMP_FAILURE;
    }
    if ((i4FsPbbTeEspVid < 1) || (i4FsPbbTeEspVid > PBBTE_MAX_VLAN_ID))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbbTeEspVidTable
 Input       :  The Indices
                FsPbbTeContextId
                FsPbbTeEspVid
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbTeEspVidTable (INT4 *pi4FsPbbTeContextId,
                                    INT4 *pi4FsPbbTeEspVid)
{
    return (nmhGetNextIndexFsPbbTeEspVidTable (0, pi4FsPbbTeContextId,
                                               0, pi4FsPbbTeEspVid));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbbTeEspVidTable
 Input       :  The Indices
                FsPbbTeContextId
                nextFsPbbTeContextId
                FsPbbTeEspVid
                nextFsPbbTeEspVid
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbTeEspVidTable (INT4 i4FsPbbTeContextId,
                                   INT4 *pi4NextFsPbbTeContextId,
                                   INT4 i4FsPbbTeEspVid,
                                   INT4 *pi4NextFsPbbTeEspVid)
{
    UINT4               u4Context = i4FsPbbTeContextId;
    tVlanId             VlanId = (UINT2) i4FsPbbTeEspVid;
    tVlanId             NextVlanId = PBBTE_INIT_VAL;

    *pi4NextFsPbbTeContextId = PBBTE_INIT_VAL;
    *pi4NextFsPbbTeEspVid = PBBTE_INIT_VAL;

    for (; u4Context < PBBTE_MAX_CONTEXTS; u4Context++)
    {
        if (PBBTE_IS_SYSTEM_STARTED (u4Context) == PBBTE_SHUTDOWN)
        {
            /* PBB-TE is not started in this context, Proceed to the next */
            continue;
        }
        if (PbbTeEspVlanGetNext (u4Context, VlanId, &NextVlanId) ==
            PBBTE_SUCCESS)
        {
            /* Gotcha !! Next Vlan!! */
            *pi4NextFsPbbTeContextId = (INT4) u4Context;
            *pi4NextFsPbbTeEspVid = (INT4) NextVlanId;

            return SNMP_SUCCESS;
        }
        VlanId = 0;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbbTeEspVidRowStatus
 Input       :  The Indices
                FsPbbTeContextId
                FsPbbTeEspVid

                The Object 
                retValFsPbbTeEspVidRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeEspVidRowStatus (INT4 i4FsPbbTeContextId, INT4 i4FsPbbTeEspVid,
                              INT4 *pi4RetValFsPbbTeEspVidRowStatus)
{
    UINT1               u1Result = PBBTE_FALSE;

    *pi4RetValFsPbbTeEspVidRowStatus = PBBTE_DESTROY;

    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) == PBBTE_SHUTDOWN)
    {
        PBBTE_GBL_TRC_ARG1 (PBBTE_ALL_FAILURE_TRC | PBBTE_MGMT_TRC,
                            PBBTE_TRC_MOD_NOT_STARTED, i4FsPbbTeContextId);
        return SNMP_FAILURE;
    }

    if ((i4FsPbbTeEspVid > 0) || (i4FsPbbTeEspVid < PBBTE_MAX_VLAN_ID))
    {
        PBBTE_IS_ESP_VLAN ((UINT4) i4FsPbbTeContextId, (UINT2) i4FsPbbTeEspVid,
                           u1Result);
    }
    if (u1Result == PBBTE_TRUE)
    {
        /* Only one value(ACTIVE) of RowStatus can be get in the ESP-VID
         * table. If the entry exists, then RowStatus will be ACTIVE */
        *pi4RetValFsPbbTeEspVidRowStatus = PBBTE_ACTIVE;
    }
    else
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbbTeEspVidRowStatus
 Input       :  The Indices
                FsPbbTeContextId
                FsPbbTeEspVid

                The Object 
                setValFsPbbTeEspVidRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeEspVidRowStatus (INT4 i4FsPbbTeContextId, INT4 i4FsPbbTeEspVid,
                              INT4 i4SetValFsPbbTeEspVidRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    UINT1               u1Result = PBBTE_FALSE;

    PBBTE_MEM_SET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) == PBBTE_SHUTDOWN)
    {
        PBBTE_GBL_TRC_ARG1 (PBBTE_ALL_FAILURE_TRC | PBBTE_MGMT_TRC,
                            PBBTE_TRC_MOD_NOT_STARTED, i4FsPbbTeContextId);
        return SNMP_FAILURE;
    }

    PBBTE_IS_ESP_VLAN ((UINT4) i4FsPbbTeContextId, (UINT2) i4FsPbbTeEspVid,
                       u1Result);

    if (u1Result == PBBTE_TRUE)
    {
        if (i4SetValFsPbbTeEspVidRowStatus == PBBTE_CREATE_AND_GO)
        {
            PBBTE_TRC (i4FsPbbTeContextId, PBBTE_MGMT_TRC,
                       PBBTE_TRC_ESP_VLAN_RECONFIG);

            return SNMP_SUCCESS;
        }
    }
    switch (i4SetValFsPbbTeEspVidRowStatus)
    {
        case PBBTE_CREATE_AND_WAIT:    /*Intentional fallthrough for MSR */
        case PBBTE_CREATE_AND_GO:
#ifdef NPAPI_WANTED
            /* Set the Vlan ID as ESP-VID in hardware. This should disable
             * flooding of unknown unicast/ multicast and broadcast packets
             * for this Vlan in hardware. */
            if (FsMiPbbTeHwSetEspVid ((UINT4) i4FsPbbTeContextId,
                                      (tVlanId) i4FsPbbTeEspVid) == FNP_FAILURE)
            {
                PBBTE_TRC_ARG1 (i4FsPbbTeContextId,
                                PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_HW_ESP_SET_FAIL, i4FsPbbTeEspVid);
                return SNMP_FAILURE;
            }
#endif
            /* Set the flooding disabled flag in Vlan.
             * This should ensure that while forwarding packets in software
             * for this Vlan, the packet is dropped if the DA is not present
             * in the static or learnt entry tables.
             * Since all unicast and multicast learning is disabled for
             * ESP Vlans this effectively means that the packet will be dropped 
             * if a static unicast or mulicast entry is NOT found for it's DA. */
            if (VlanSetVlanFloodingStatus
                (i4FsPbbTeContextId, (tVlanId) i4FsPbbTeEspVid,
                 VLAN_DISABLED) == VLAN_FAILURE)
            {
                PBBTE_TRC_ARG2 (i4FsPbbTeContextId,
                                PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_VLAN_FLOODING_STATUS_FAIL,
                                i4FsPbbTeContextId, i4FsPbbTeEspVid);

#ifdef NPAPI_WANTED
                FsMiPbbTeHwResetEspVid ((UINT4) i4FsPbbTeContextId,
                                        (tVlanId) i4FsPbbTeEspVid);
#endif

                return SNMP_FAILURE;
            }

            PbbTeEspVlanConfig (i4FsPbbTeContextId, (tVlanId) i4FsPbbTeEspVid,
                                PBBTE_SET);
            break;

        case PBBTE_DESTROY:
#ifdef NPAPI_WANTED
            if (FsMiPbbTeHwResetEspVid ((UINT4) i4FsPbbTeContextId,
                                        (tVlanId) i4FsPbbTeEspVid) ==
                FNP_FAILURE)
            {
                PBBTE_TRC_ARG1 (i4FsPbbTeContextId,
                                PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_HW_ESP_RESET_FAIL,
                                i4FsPbbTeContextId);
                return SNMP_FAILURE;
            }
#endif
            if (VlanSetVlanFloodingStatus
                (i4FsPbbTeContextId, (tVlanId) i4FsPbbTeEspVid,
                 VLAN_ENABLED) == VLAN_FAILURE)
            {
                PBBTE_TRC_ARG2 (i4FsPbbTeContextId,
                                PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_VLAN_FLOODING_STATUS_FAIL,
                                i4FsPbbTeContextId, i4FsPbbTeEspVid);

#ifdef NPAPI_WANTED
                FsMiPbbTeHwSetEspVid ((UINT4) i4FsPbbTeContextId,
                                      (tVlanId) i4FsPbbTeEspVid);
#endif

                return SNMP_FAILURE;
            }

            PbbTeEspVlanConfig (i4FsPbbTeContextId, (tVlanId) i4FsPbbTeEspVid,
                                PBBTE_RESET);
            break;

        case PBBTE_ACTIVE:
            break;

        default:
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsPbbTeEspVidRowStatus,
                          u4SeqNum, TRUE, NULL, NULL, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i %i", i4FsPbbTeContextId,
                      i4FsPbbTeEspVid, i4SetValFsPbbTeEspVidRowStatus));
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeEspVidRowStatus
 Input       :  The Indices
                FsPbbTeContextId
                FsPbbTeEspVid

                The Object 
                testValFsPbbTeEspVidRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeEspVidRowStatus (UINT4 *pu4ErrorCode, INT4 i4FsPbbTeContextId,
                                 INT4 i4FsPbbTeEspVid,
                                 INT4 i4TestValFsPbbTeEspVidRowStatus)
{
    UINT1               u1Result = 0;

    if (PBBTE_IS_SYSTEM_STARTED (i4FsPbbTeContextId) == PBBTE_SHUTDOWN)
    {
        PBBTE_GBL_TRC_ARG1 (PBBTE_ALL_FAILURE_TRC | PBBTE_MGMT_TRC,
                            PBBTE_TRC_MOD_NOT_STARTED, i4FsPbbTeContextId);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if ((i4FsPbbTeEspVid < 1) || (i4FsPbbTeEspVid > PBBTE_MAX_VLAN_ID))
    {
        PBBTE_TRC (i4FsPbbTeContextId, PBBTE_ALL_FAILURE_TRC | PBBTE_MGMT_TRC,
                   PBBTE_TRC_INVALID_VLAN);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsPbbTeEspVidRowStatus < PBBTE_ACTIVE) ||
        (i4TestValFsPbbTeEspVidRowStatus > PBBTE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((i4TestValFsPbbTeEspVidRowStatus != PBBTE_CREATE_AND_GO) &&
        (i4TestValFsPbbTeEspVidRowStatus != PBBTE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    PBBTE_IS_ESP_VLAN ((UINT4) i4FsPbbTeContextId, (UINT2) i4FsPbbTeEspVid,
                       u1Result);

    if (i4TestValFsPbbTeEspVidRowStatus == PBBTE_DESTROY)
    {
        if (u1Result == PBBTE_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            PBBTE_TRC (i4FsPbbTeContextId, PBBTE_MGMT_TRC,
                       PBBTE_TRC_RESET_ESP_VLAN_RECONFIG);
            return SNMP_FAILURE;
        }
        else
        {
            /* Check if any TE-SID entries configured for the ESP-VLAN
             * If any thing configured, don't allow ESP-VLAN deletion */

            if (PbbTeTeSidAnyTeSidEntryInEspVlan (i4FsPbbTeContextId,
                                                  i4FsPbbTeEspVid) ==
                PBBTE_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                PBBTE_TRC_ARG1 (i4FsPbbTeContextId, PBBTE_MGMT_TRC,
                                PBBTE_TRC_ESP_VLAN_DEL_FAIL, i4FsPbbTeEspVid);
                return SNMP_FAILURE;
            }
        }
    }
    else if ((i4TestValFsPbbTeEspVidRowStatus == PBBTE_CREATE_AND_GO) &&
             (u1Result == PBBTE_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        PBBTE_TRC (i4FsPbbTeContextId, PBBTE_MGMT_TRC,
                   PBBTE_TRC_ESP_VLAN_RECONFIG);
        return SNMP_FAILURE;
    }

    if (i4TestValFsPbbTeEspVidRowStatus == PBBTE_CREATE_AND_GO)
    {
        if (PbbTeIsVlanActive (i4FsPbbTeContextId, (tVlanId) i4FsPbbTeEspVid)
            == OSIX_FALSE)
        {
            PBBTE_TRC (i4FsPbbTeContextId,
                       PBBTE_ALL_FAILURE_TRC | PBBTE_MGMT_TRC,
                       PBBTE_TRC_VLAN_NOTACTIVE);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeEspVidTable
 Input       :  The Indices
                FsPbbTeContextId
                FsPbbTeEspVid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeEspVidTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsPbbTeTeSidExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsPbbTeTeSidExtTable
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsPbbTeTeSidExtTable (UINT4 u4Ieee8021PbbTeTeSidId,
                                              UINT4
                                              u4Ieee8021PbbTeTeSidEspIndex)
{
    tEspInfo            EspInfo;

    if (PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                               u4Ieee8021PbbTeTeSidEspIndex, &EspInfo)
        == PBBTE_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsPbbTeTeSidExtTable
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsPbbTeTeSidExtTable (UINT4 *pu4Ieee8021PbbTeTeSidId,
                                      UINT4 *pu4Ieee8021PbbTeTeSidEspIndex)
{
    return (nmhGetNextIndexFsPbbTeTeSidExtTable (1,
                                                 pu4Ieee8021PbbTeTeSidId,
                                                 0,
                                                 pu4Ieee8021PbbTeTeSidEspIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsPbbTeTeSidExtTable
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                nextIeee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex
                nextIeee8021PbbTeTeSidEspIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsPbbTeTeSidExtTable (UINT4 u4Ieee8021PbbTeTeSidId,
                                     UINT4 *pu4NextIeee8021PbbTeTeSidId,
                                     UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                     UINT4 *pu4NextIeee8021PbbTeTeSidEspIndex)
{
    /* GetNext of the TeSidTable */
    return (nmhGetNextIndexIeee8021PbbTeTeSidTable (u4Ieee8021PbbTeTeSidId,
                                                    pu4NextIeee8021PbbTeTeSidId,
                                                    u4Ieee8021PbbTeTeSidEspIndex,
                                                    pu4NextIeee8021PbbTeTeSidEspIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbbTeTeSidExtContextId
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                retValFsPbbTeTeSidExtContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTeSidExtContextId (UINT4 u4Ieee8021PbbTeTeSidId,
                                UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                INT4 *pi4RetValFsPbbTeTeSidExtContextId)
{
    tEspInfo            EspInfo;

    *pi4RetValFsPbbTeTeSidExtContextId = PBBTE_INIT_VAL;

    if (PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                               u4Ieee8021PbbTeTeSidEspIndex,
                               &EspInfo) == PBBTE_FAILURE)
    {
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_GET_FAIL, u4Ieee8021PbbTeTeSidId,
                            u4Ieee8021PbbTeTeSidEspIndex);
        return SNMP_FAILURE;
    }

    *pi4RetValFsPbbTeTeSidExtContextId = EspInfo.u4ContextId;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbbTeTeSidExtContextId
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                setValFsPbbTeTeSidExtContextId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTeSidExtContextId (UINT4 u4Ieee8021PbbTeTeSidId,
                                UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                INT4 i4SetValFsPbbTeTeSidExtContextId)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    PBBTE_PERF_MARK_START_TIME ();

    PBBTE_MEM_SET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (PBBTE_IS_SYSTEM_STARTED (i4SetValFsPbbTeTeSidExtContextId) ==
        PBBTE_SHUTDOWN)
    {
        return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    if (PbbTeTeSidContextIdSet (u4Ieee8021PbbTeTeSidId,
                                u4Ieee8021PbbTeTeSidEspIndex,
                                i4SetValFsPbbTeTeSidExtContextId) ==
        PBBTE_FAILURE)
    {
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_CONTEXTID_SET_FAIL,
                            u4Ieee8021PbbTeTeSidId,
                            u4Ieee8021PbbTeTeSidEspIndex);
        return SNMP_FAILURE;
    }

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, FsPbbTeTeSidExtContextId,
                          u4SeqNum, FALSE, NULL, NULL, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", u4Ieee8021PbbTeTeSidId,
                      u4Ieee8021PbbTeTeSidEspIndex,
                      i4SetValFsPbbTeTeSidExtContextId));

    PBBTE_PERF_MARK_END_TIME ();

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTeSidExtContextId
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                testValFsPbbTeTeSidExtContextId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTeSidExtContextId (UINT4 *pu4ErrorCode,
                                   UINT4 u4Ieee8021PbbTeTeSidId,
                                   UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                   INT4 i4TestValFsPbbTeTeSidExtContextId)
{
    tEspInfo            EspInfo;
    UINT1               u1Result = PBBTE_FALSE;

    if ((i4TestValFsPbbTeTeSidExtContextId < 0) ||
        ((UINT4) i4TestValFsPbbTeTeSidExtContextId >= PBBTE_MAX_CONTEXTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (PBBTE_MODULE_SHUTDOWN);
        return SNMP_FAILURE;
    }
    if (PBBTE_IS_SYSTEM_STARTED (i4TestValFsPbbTeTeSidExtContextId) ==
        PBBTE_SHUTDOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                               u4Ieee8021PbbTeTeSidEspIndex, &EspInfo)
        == PBBTE_SUCCESS)
    {
        if (EspInfo.u4ContextId != PBBTE_INVALID_CONTEXT)
        {
            if (EspInfo.u4ContextId !=
                (UINT4) i4TestValFsPbbTeTeSidExtContextId)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (PBBTE_TESID_ALREADY_PRESENT_ERR);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (PBBTE_TESID_NOT_FOUND_ERR);
        return SNMP_FAILURE;
    }

    if (EspInfo.EspVlan != 0 && EspInfo.EspVlan <= PBBTE_MAX_VLAN_ID)
    {

        /* An VLAN ID has already been configured for the ESP.
         * Check if that VLAN is configured as an ESP-VLAN in this context */
        PBBTE_IS_ESP_VLAN (i4TestValFsPbbTeTeSidExtContextId, EspInfo.EspVlan,
                           u1Result);

        if (u1Result == PBBTE_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (PBBTE_VLAN_NOT_AN_ESP_VLAN);
            PBBTE_GBL_TRC_ARG3 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_TESID_ROWSTATUS_VLAN_FAIL,
                                u4Ieee8021PbbTeTeSidId,
                                u4Ieee8021PbbTeTeSidEspIndex, EspInfo.EspVlan);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTeSidExtTable
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTeSidExtTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsPbbTeTestApiTeSid
 Input       :  The Indices

                The Object 
                retValFsPbbTeTestApiTeSid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTestApiTeSid (INT4 *pi4RetValFsPbbTeTestApiTeSid)
{
    *pi4RetValFsPbbTeTestApiTeSid = gPbbTeEspPathInfo.u4TeSid;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeTestApiDestMacAddr
 Input       :  The Indices

                The Object 
                retValFsPbbTeTestApiDestMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTestApiDestMacAddr (tMacAddr * pRetValFsPbbTeTestApiDestMacAddr)
{
    PBBTE_MEM_CPY (pRetValFsPbbTeTestApiDestMacAddr, gPbbTeEspPathInfo.DestMac,
                   sizeof (tMacAddr));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeTestApiSourceMacAddr
 Input       :  The Indices

                The Object 
                retValFsPbbTeTestApiSourceMacAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTestApiSourceMacAddr (tMacAddr *
                                   pRetValFsPbbTeTestApiSourceMacAddr)
{
    PBBTE_MEM_CPY (pRetValFsPbbTeTestApiSourceMacAddr,
                   gPbbTeEspPathInfo.SrcMac, sizeof (tMacAddr));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeTestApiEgressPort
 Input       :  The Indices

                The Object 
                retValFsPbbTeTestApiEgressPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTestApiEgressPort (INT4 *pi4RetValFsPbbTeTestApiEgressPort)
{
    *pi4RetValFsPbbTeTestApiEgressPort = gPbbTeEspPathInfo.u4EgressIfIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeTestApiEgressPortList
 Input       :  The Indices

                The Object 
                retValFsPbbTeTestApiEgressPortList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTestApiEgressPortList (tSNMP_OCTET_STRING_TYPE *
                                    pRetValFsPbbTeTestApiEgressPortList)
{
    /* As the snmp and msr supports only MAX_OCTET_STRING_LEN
     * for the Octet String, the Egress portlist returned is filled
     * only for MAX_OCTET_STRING_LEN length so as to avoid invalid 
     * memory access */

    PBBTE_MEM_CPY (pRetValFsPbbTeTestApiEgressPortList->pu1_OctetList,
                   gPbbTeEspPathInfo.EgressIfIndexList, sizeof (tPortList));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeTestApiEspVlanId
 Input       :  The Indices

                The Object 
                retValFsPbbTeTestApiEspVlanId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTestApiEspVlanId (INT4 *pi4RetValFsPbbTeTestApiEspVlanId)
{
    *pi4RetValFsPbbTeTestApiEspVlanId = gPbbTeEspPathInfo.EspVlanId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeTestApiAction
 Input       :  The Indices

                The Object 
                retValFsPbbTeTestApiAction
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTestApiAction (INT4 *pi4RetValFsPbbTeTestApiAction)
{
    *pi4RetValFsPbbTeTestApiAction = 1;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsPbbTeTestApiTeSid
 Input       :  The Indices

                The Object 
                setValFsPbbTeTestApiTeSid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTestApiTeSid (INT4 i4SetValFsPbbTeTestApiTeSid)
{
    gPbbTeEspPathInfo.u4TeSid = i4SetValFsPbbTeTestApiTeSid;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbTeTestApiDestMacAddr
 Input       :  The Indices

                The Object 
                setValFsPbbTeTestApiDestMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTestApiDestMacAddr (tMacAddr SetValFsPbbTeTestApiDestMacAddr)
{
    PBBTE_MEM_CPY (gPbbTeEspPathInfo.DestMac,
                   SetValFsPbbTeTestApiDestMacAddr, sizeof (tMacAddr));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbTeTestApiSourceMacAddr
 Input       :  The Indices

                The Object 
                setValFsPbbTeTestApiSourceMacAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTestApiSourceMacAddr (tMacAddr SetValFsPbbTeTestApiSourceMacAddr)
{
    PBBTE_MEM_CPY (gPbbTeEspPathInfo.SrcMac,
                   SetValFsPbbTeTestApiSourceMacAddr, sizeof (tMacAddr));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbTeTestApiEgressPort
 Input       :  The Indices

                The Object 
                setValFsPbbTeTestApiEgressPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTestApiEgressPort (INT4 i4SetValFsPbbTeTestApiEgressPort)
{
    gPbbTeEspPathInfo.u4EgressIfIndex = i4SetValFsPbbTeTestApiEgressPort;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbTeTestApiEgressPortList
 Input       :  The Indices

                The Object 
                setValFsPbbTeTestApiEgressPortList
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTestApiEgressPortList (tSNMP_OCTET_STRING_TYPE *
                                    pSetValFsPbbTeTestApiEgressPortList)
{
    /* As the snmp and msr supports only MAX_OCTET_STRING_LEN
     * for the Octet String, the Egress portlist is copied
     * only for MAX_OCTET_STRING_LEN length so as to avoid invalid 
     * memory access */

    PBBTE_MEM_CPY (gPbbTeEspPathInfo.EgressIfIndexList,
                   pSetValFsPbbTeTestApiEgressPortList->pu1_OctetList,
                   sizeof (tPortList));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbTeTestApiEspVlanId
 Input       :  The Indices

                The Object 
                setValFsPbbTeTestApiEspVlanId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTestApiEspVlanId (INT4 i4SetValFsPbbTeTestApiEspVlanId)
{
    gPbbTeEspPathInfo.EspVlanId = (UINT2) i4SetValFsPbbTeTestApiEspVlanId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbTeTestApiAction
 Input       :  The Indices

                The Object 
                setValFsPbbTeTestApiAction
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTestApiAction (INT4 i4SetValFsPbbTeTestApiAction)
{
    INT4                i4RetVal = PBBTE_SUCCESS;

    if (i4SetValFsPbbTeTestApiAction == PBBTE_CREATE)
    {
        PBBTE_PERF_MARK_START_TIME ();
        i4RetVal = PbbTeConfigureEsp (&gPbbTeEspPathInfo, PBBTE_CREATE);
        PBBTE_PERF_MARK_END_TIME ();
        PBBTE_PERF_CALC_ADD_TIMETAKEN (1);
    }
    else
    {
        PBBTE_PERF_MARK_START_TIME ();
        i4RetVal = PbbTeConfigureEsp (&gPbbTeEspPathInfo, PBBTE_DELETE);
        PBBTE_PERF_MARK_END_TIME ();
        PBBTE_PERF_CALC_DEL_TIMETAKEN ();
    }
    PBBTE_MEM_SET (&gPbbTeEspPathInfo, PBBTE_INIT_VAL, sizeof (tEspPathInfo));
    return ((i4RetVal == PBBTE_SUCCESS) ? SNMP_SUCCESS : SNMP_FAILURE);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTestApiTeSid
 Input       :  The Indices

                The Object 
                testValFsPbbTeTestApiTeSid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTestApiTeSid (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsPbbTeTestApiTeSid)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsPbbTeTestApiTeSid);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTestApiDestMacAddr
 Input       :  The Indices

                The Object 
                testValFsPbbTeTestApiDestMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTestApiDestMacAddr (UINT4 *pu4ErrorCode,
                                    tMacAddr TestValFsPbbTeTestApiDestMacAddr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (TestValFsPbbTeTestApiDestMacAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTestApiSourceMacAddr
 Input       :  The Indices

                The Object 
                testValFsPbbTeTestApiSourceMacAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTestApiSourceMacAddr (UINT4 *pu4ErrorCode,
                                      tMacAddr
                                      TestValFsPbbTeTestApiSourceMacAddr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (TestValFsPbbTeTestApiSourceMacAddr);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTestApiEgressPort
 Input       :  The Indices

                The Object 
                testValFsPbbTeTestApiEgressPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTestApiEgressPort (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsPbbTeTestApiEgressPort)
{
    if ((i4TestValFsPbbTeTestApiEgressPort < 0) ||
        (i4TestValFsPbbTeTestApiEgressPort > PBBTE_MAX_PORTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTestApiEgressPortList
 Input       :  The Indices

                The Object 
                testValFsPbbTeTestApiEgressPortList
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTestApiEgressPortList (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pTestValFsPbbTeTestApiEgressPortList)
{
    /* As the snmp and msr supports only MAX_OCTET_STRING_LEN
     * for the Octet String, the Egress portlist is copied
     * only for MAX_OCTET_STRING_LEN length so as to avoid invalid 
     * memory access */

    if (pTestValFsPbbTeTestApiEgressPortList->i4_Length > MAX_OCTET_STRING_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTestApiEspVlanId
 Input       :  The Indices

                The Object 
                testValFsPbbTeTestApiEspVlanId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTestApiEspVlanId (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsPbbTeTestApiEspVlanId)
{
    if ((i4TestValFsPbbTeTestApiEspVlanId < 1) ||
        (i4TestValFsPbbTeTestApiEspVlanId > PBBTE_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTestApiAction
 Input       :  The Indices

                The Object 
                testValFsPbbTeTestApiAction
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTestApiAction (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsPbbTeTestApiAction)
{
    if ((i4TestValFsPbbTeTestApiAction < PBBTE_CREATE) ||
        (i4TestValFsPbbTeTestApiAction > PBBTE_DELETE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTestApiTeSid
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTestApiTeSid (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTestApiDestMacAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTestApiDestMacAddr (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTestApiSourceMacAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTestApiSourceMacAddr (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTestApiEgressPort
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTestApiEgressPort (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTestApiEgressPortList
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTestApiEgressPortList (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTestApiEspVlanId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTestApiEspVlanId (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTestApiAction
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTestApiAction (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsPbbTeTestApiContextId
 Input       :  The Indices

                The Object 
                retValFsPbbTeTestApiContextId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsPbbTeTestApiContextId (INT4 *pi4RetValFsPbbTeTestApiContextId)
{
    *pi4RetValFsPbbTeTestApiContextId = gPbbTeEspPathInfo.u4ContextId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsPbbTeTestApiContextId
 Input       :  The Indices

                The Object 
                setValFsPbbTeTestApiContextId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsPbbTeTestApiContextId (INT4 i4SetValFsPbbTeTestApiContextId)
{
    gPbbTeEspPathInfo.u4ContextId = i4SetValFsPbbTeTestApiContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsPbbTeTestApiContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsPbbTeTestApiContextId (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsPbbTeTestApiContextId
 Input       :  The Indices

                The Object 
                testValFsPbbTeTestApiContextId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsPbbTeTestApiContextId (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsPbbTeTestApiContextId)
{
    if ((i4TestValFsPbbTeTestApiContextId < 0) ||
        ((UINT4) i4TestValFsPbbTeTestApiContextId >= PBBTE_MAX_CONTEXTS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  PbbteNotifyProtocolShutdownStatus
 Input       :  None
 Output      :  None
 Returns     :  None
****************************************************************************/

VOID
PbbteNotifyProtocolShutdownStatus (INT4 i4ContextId)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[3][SNMP_MAX_OID_LENGTH];
    UINT2               u2Objects = 3;

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fspbt, (sizeof (fspbt) / sizeof (UINT4)),
                      au1ObjectOid[0]);
    SNMPGetOidString (fs1ay, (sizeof (fs1ay) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    SNMPGetOidString
        (FsPbbTeContextSystemControl,
         (sizeof (FsPbbTeContextSystemControl) / sizeof (UINT4)),
         au1ObjectOid[u2Objects - 1]);
    /* Send a notification to MSR to process the vlan shutdown, with 
     * vlan oids and its system control object */
    MsrNotifyProtocolShutdown (au1ObjectOid, u2Objects, i4ContextId);
#else
    UNUSED_PARAM (i4ContextId);
#endif
    return;
}
