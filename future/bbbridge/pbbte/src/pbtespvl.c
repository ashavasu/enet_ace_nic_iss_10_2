/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: pbtespvl.c,v 1.3 2008/12/08 09:33:47 premap-iss Exp $                *
 *                                                                           *
 * Description: This file contains routines related to the                   *
 *              PBB-TE module's ESP Vlan                                     *
 *                                                                           *
 *****************************************************************************/
#include "pbthdrs.h"

/*****************************************************************************/
/* Function Name      : PbbTeEspVlanConfig                                   */
/*                                                                           */
/* Description        : This function is used to set or reset the given      */
/*                      VlanID as an ESP Vlan ID in the particula context.   */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId      - Vlan to be configured as an ESP-VLAN   */
/*                      EspVidStatus- Indicates whether the particular Vlan  */
/*                                    has to be Set or Reset as ESP-Vlan     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeEspVlanConfig (UINT4 u4ContextId, tVlanId VlanId,
                    eConfigEspVidStatus EspVidStatus)
{
    UINT4               u4HashTblNum;
    UINT1               u1Result = PBBTE_FALSE;

    PBBTE_IS_ESP_VLAN (u4ContextId, VlanId, u1Result);

    if (((u1Result == PBBTE_TRUE) && (EspVidStatus == PBBTE_SET))
        || ((u1Result == PBBTE_FALSE) && (EspVidStatus == PBBTE_RESET)))
    {
        return PBBTE_SUCCESS;
    }

    if (EspVidStatus == PBBTE_SET)
    {
        PBBTE_LOCK ();

        /* HashCnt variable will be rotating from 1 to 
         * PBBTE_MAX_HASH_TABLE_PER_CONTEXT. This variable denotes the
         * hash table number, to which the ESP-VLAN will be assigned */

        /* Find, the Hash Table, to which the particular VLAN is to be
         * configured */

        u4HashTblNum = PBBTE_CONTEXT_PTR (u4ContextId)->u4HashCnt;

        /* Find, whether the corresponding Hash table for the particular VLAN 
         * is already allocated or not. If Hash table is not created already,
         * create the hash table */

        if (PBBTE_CONTEXT_PTR (u4ContextId)->aHashTableList[u4HashTblNum]
            == NULL)
        {
            PBBTE_CONTEXT_PTR (u4ContextId)->aHashTableList[u4HashTblNum] =
                TMO_HASH_Create_Table (PBBTE_MAX_BUCKETS, NULL, FALSE);
        }

        /* Store the Hash table number used by the VLAN */

        PBBTE_CONTEXT_PTR (u4ContextId)->aEspVlanHashTblNum[VlanId]
            = u4HashTblNum;

        /* Increment the HashTable count(u4HashCnt) for this context by 1
         * (HashTblNum of 0 should not be used, so + 1) */

        u4HashTblNum = (u4HashTblNum % PBBTE_MAX_HASH_TABLE_PER_CONTEXT) + 1;

        PBBTE_CONTEXT_PTR (u4ContextId)->u4HashCnt = u4HashTblNum;

        PBBTE_UNLOCK ();

        PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC, PBBTE_TRC_ESP_VLAN,
                        VlanId);
    }
    else
    {
        PBBTE_LOCK ();

        PBBTE_CONTEXT_PTR (u4ContextId)->aEspVlanHashTblNum[VlanId] = 0;

        PBBTE_UNLOCK ();

        PBBTE_TRC_ARG1 (u4ContextId, PBBTE_MGMT_TRC, PBBTE_TRC_ESP_VLAN_RESET,
                        VlanId);
    }
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeEspVlanGetNext                                  */
/*                                                                           */
/* Description        : This function is used to get next ESP vlan from      */
/*                      the given VLAN in the particular context.            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId      - Vlan Identifier from which Next has to */
/*                                    be scanned.                            */
/*                      pNextVlanId - Pointer to the next ESP-VID            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeEspVlanGetNext (UINT4 u4Context, tVlanId VlanId, tVlanId * pNextVlanId)
{
    tVlanId             VlanCnt = (tVlanId) (VlanId + 1);    /* Incremented, so 
                                                               that to get 
                                                               the next */
    UINT1               u1Result = PBBTE_FALSE;

    PBBTE_LOCK ();
    for (; VlanCnt <= PBBTE_MAX_VLAN_ID; VlanCnt++)
    {
        /* A Vlan will be identified as an ESP Vlan if the
           Hash Table for the particular VLAN is configured */

        u1Result = (((PBBTE_CONTEXT_PTR (u4Context)->
                      aEspVlanHashTblNum[VlanCnt]) != 0)
                    ? PBBTE_TRUE : PBBTE_FALSE);
        if (u1Result == PBBTE_TRUE)
        {
            *pNextVlanId = VlanCnt;

            PBBTE_UNLOCK ();
            return PBBTE_SUCCESS;
        }
    }
    PBBTE_UNLOCK ();

    return PBBTE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PbbTeEspVlanListGet                                  */
/*                                                                           */
/* Description        : This function is used to get the list of ESP VLANs   */
/*                      a context                                            */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                      VlanId      - Vlan Identifier from which Next has to */
/*                                    be scanned.                            */
/*                      pNextVlanId - Pointer to the next ESP-VID            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PbbTeEspVlanListGet (UINT4 u4Context, UINT1 *au1VlanList)
{
    UINT2               u2VlanCnt = 1;

    PBBTE_MEM_SET (au1VlanList, PBBTE_INIT_VAL, PBBTE_VLAN_LIST_SIZE);

    PBBTE_LOCK ();

    for (; u2VlanCnt <= PBBTE_MAX_VLAN_ID; u2VlanCnt++)
    {
        /* A Vlan will be identified as an ESP Vlan if the
           Hash Table for the particular VLAN is configured */
        if ((PBBTE_CONTEXT_PTR (u4Context)->aEspVlanHashTblNum[u2VlanCnt]) != 0)
        {
            OSIX_BITLIST_SET_BIT (au1VlanList, u2VlanCnt, PBBTE_VLAN_LIST_SIZE);
        }
    }

    PBBTE_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeEspVlanListReset                                */
/*                                                                           */
/* Description        : This function is used to reset the ESP VLAN List     */
/*                      in a context                                         */
/*                                                                           */
/* Input(s)           : u4ContextId - Context Identifier                     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PbbTeEspVlanListReset (UINT4 u4ContextId)
{
    PBBTE_LOCK ();

    PBBTE_MEM_SET (PBBTE_CONTEXT_PTR (u4ContextId)->aEspVlanHashTblNum,
                   PBBTE_INIT_VAL, (PBBTE_MAX_VLAN_ID + 1) * sizeof (UINT2));

    PBBTE_UNLOCK ();

    return;
}
