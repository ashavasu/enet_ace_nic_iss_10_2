/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: pbt1aylw.c,v 1.8 2014/01/20 12:16:54 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "pbthdrs.h"

extern UINT4        Ieee8021PbbTeTeSidEsp[13];
extern UINT4        Ieee8021PbbTeTeSidStorageType[13];
extern UINT4        Ieee8021PbbTeTeSidRowStatus[13];
/* LOW LEVEL Routines for Table : Ieee8021PbbTeProtectionGroupListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupListTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupListTable (INT4
                                                               i4FsDot1qVlanContextId,
                                                               UINT4
                                                               u4Ieee8021PbbTeProtectionGroupListGroupId)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbbTeProtectionGroupListTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbbTeProtectionGroupListTable (INT4
                                                       *pi4FsDot1qVlanContextId,
                                                       UINT4
                                                       *pu4Ieee8021PbbTeProtectionGroupListGroupId)
{
    UNUSED_PARAM (pi4FsDot1qVlanContextId);
    UNUSED_PARAM (pu4Ieee8021PbbTeProtectionGroupListGroupId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbbTeProtectionGroupListTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId
                nextIeee8021PbbTeProtectionGroupListGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbbTeProtectionGroupListTable (INT4
                                                      i4FsDot1qVlanContextId,
                                                      INT4
                                                      *pi4NextFsDot1qVlanContextId,
                                                      UINT4
                                                      u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                      UINT4
                                                      *pu4NextIeee8021PbbTeProtectionGroupListGroupId)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (pi4NextFsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pu4NextIeee8021PbbTeProtectionGroupListGroupId);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupListMode
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupListMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupListMode (INT4 i4FsDot1qVlanContextId,
                                            UINT4
                                            u4Ieee8021PbbTeProtectionGroupListGroupId,
                                            INT4
                                            *pi4RetValIeee8021PbbTeProtectionGroupListMode)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupListMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupListWorkingMA
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupListWorkingMA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupListWorkingMA (INT4 i4FsDot1qVlanContextId,
                                                 UINT4
                                                 u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                 UINT4
                                                 *pu4RetValIeee8021PbbTeProtectionGroupListWorkingMA)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pu4RetValIeee8021PbbTeProtectionGroupListWorkingMA);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupListProtectionMA
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupListProtectionMA
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupListProtectionMA (INT4 i4FsDot1qVlanContextId,
                                                    UINT4
                                                    u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                    UINT4
                                                    *pu4RetValIeee8021PbbTeProtectionGroupListProtectionMA)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pu4RetValIeee8021PbbTeProtectionGroupListProtectionMA);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupListStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupListStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupListStorageType (INT4 i4FsDot1qVlanContextId,
                                                   UINT4
                                                   u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                   INT4
                                                   *pi4RetValIeee8021PbbTeProtectionGroupListStorageType)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupListStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupListRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupListRowStatus (INT4 i4FsDot1qVlanContextId,
                                                 UINT4
                                                 u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                 INT4
                                                 *pi4RetValIeee8021PbbTeProtectionGroupListRowStatus)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupListRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupListMode
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupListMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupListMode (INT4 i4FsDot1qVlanContextId,
                                            UINT4
                                            u4Ieee8021PbbTeProtectionGroupListGroupId,
                                            INT4
                                            i4SetValIeee8021PbbTeProtectionGroupListMode)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4SetValIeee8021PbbTeProtectionGroupListMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupListWorkingMA
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupListWorkingMA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupListWorkingMA (INT4 i4FsDot1qVlanContextId,
                                                 UINT4
                                                 u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                 UINT4
                                                 u4SetValIeee8021PbbTeProtectionGroupListWorkingMA)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4SetValIeee8021PbbTeProtectionGroupListWorkingMA);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupListProtectionMA
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupListProtectionMA
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupListProtectionMA (INT4
                                                    i4FsDot1qVlanContextId,
                                                    UINT4
                                                    u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                    UINT4
                                                    u4SetValIeee8021PbbTeProtectionGroupListProtectionMA)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4SetValIeee8021PbbTeProtectionGroupListProtectionMA);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupListStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupListStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupListStorageType (INT4
                                                   i4FsDot1qVlanContextId,
                                                   UINT4
                                                   u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                   INT4
                                                   i4SetValIeee8021PbbTeProtectionGroupListStorageType)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4SetValIeee8021PbbTeProtectionGroupListStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupListRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupListRowStatus (INT4 i4FsDot1qVlanContextId,
                                                 UINT4
                                                 u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                 INT4
                                                 i4SetValIeee8021PbbTeProtectionGroupListRowStatus)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4SetValIeee8021PbbTeProtectionGroupListRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupListMode
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupListMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListMode (UINT4 *pu4ErrorCode,
                                               INT4 i4FsDot1qVlanContextId,
                                               UINT4
                                               u4Ieee8021PbbTeProtectionGroupListGroupId,
                                               INT4
                                               i4TestValIeee8021PbbTeProtectionGroupListMode)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4TestValIeee8021PbbTeProtectionGroupListMode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupListWorkingMA
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupListWorkingMA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListWorkingMA (UINT4 *pu4ErrorCode,
                                                    INT4 i4FsDot1qVlanContextId,
                                                    UINT4
                                                    u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                    UINT4
                                                    u4TestValIeee8021PbbTeProtectionGroupListWorkingMA)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4TestValIeee8021PbbTeProtectionGroupListWorkingMA);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupListProtectionMA
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupListProtectionMA
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListProtectionMA (UINT4 *pu4ErrorCode,
                                                       INT4
                                                       i4FsDot1qVlanContextId,
                                                       UINT4
                                                       u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                       UINT4
                                                       u4TestValIeee8021PbbTeProtectionGroupListProtectionMA)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4TestValIeee8021PbbTeProtectionGroupListProtectionMA);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupListStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupListStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListStorageType (UINT4 *pu4ErrorCode,
                                                      INT4
                                                      i4FsDot1qVlanContextId,
                                                      UINT4
                                                      u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                      INT4
                                                      i4TestValIeee8021PbbTeProtectionGroupListStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4TestValIeee8021PbbTeProtectionGroupListStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupListRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupListRowStatus (UINT4 *pu4ErrorCode,
                                                    INT4 i4FsDot1qVlanContextId,
                                                    UINT4
                                                    u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                    INT4
                                                    i4TestValIeee8021PbbTeProtectionGroupListRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4TestValIeee8021PbbTeProtectionGroupListRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbbTeProtectionGroupListTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbbTeProtectionGroupListTable (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbbTeMASharedGroupTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbbTeMASharedGroupTable
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupListGroupId
                Ieee8021PbbTeMASharedGroupSubIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbbTeMASharedGroupTable (UINT4
                                                         u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                         UINT4
                                                         u4Ieee8021PbbTeMASharedGroupSubIndex)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4Ieee8021PbbTeMASharedGroupSubIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbbTeMASharedGroupTable
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupListGroupId
                Ieee8021PbbTeMASharedGroupSubIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbbTeMASharedGroupTable (UINT4
                                                 *pu4Ieee8021PbbTeProtectionGroupListGroupId,
                                                 UINT4
                                                 *pu4Ieee8021PbbTeMASharedGroupSubIndex)
{
    UNUSED_PARAM (pu4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pu4Ieee8021PbbTeMASharedGroupSubIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbbTeMASharedGroupTable
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupListGroupId
                nextIeee8021PbbTeProtectionGroupListGroupId
                Ieee8021PbbTeMASharedGroupSubIndex
                nextIeee8021PbbTeMASharedGroupSubIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbbTeMASharedGroupTable (UINT4
                                                u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                UINT4
                                                *pu4NextIeee8021PbbTeProtectionGroupListGroupId,
                                                UINT4
                                                u4Ieee8021PbbTeMASharedGroupSubIndex,
                                                UINT4
                                                *pu4NextIeee8021PbbTeMASharedGroupSubIndex)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pu4NextIeee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4Ieee8021PbbTeMASharedGroupSubIndex);
    UNUSED_PARAM (pu4NextIeee8021PbbTeMASharedGroupSubIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeMASharedGroupId
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupListGroupId
                Ieee8021PbbTeMASharedGroupSubIndex

                The Object 
                retValIeee8021PbbTeMASharedGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeMASharedGroupId (UINT4
                                    u4Ieee8021PbbTeProtectionGroupListGroupId,
                                    UINT4 u4Ieee8021PbbTeMASharedGroupSubIndex,
                                    UINT4
                                    *pu4RetValIeee8021PbbTeMASharedGroupId)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4Ieee8021PbbTeMASharedGroupSubIndex);
    UNUSED_PARAM (pu4RetValIeee8021PbbTeMASharedGroupId);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbbTeTeSidTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbbTeTeSidTable
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbbTeTeSidTable (UINT4 u4Ieee8021PbbTeTeSidId,
                                                 UINT4
                                                 u4Ieee8021PbbTeTeSidEspIndex)
{
    if ((u4Ieee8021PbbTeTeSidId == 0) || (u4Ieee8021PbbTeTeSidEspIndex == 0))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbbTeTeSidTable
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbbTeTeSidTable (UINT4 *pu4Ieee8021PbbTeTeSidId,
                                         UINT4 *pu4Ieee8021PbbTeTeSidEspIndex)
{
    return (nmhGetNextIndexIeee8021PbbTeTeSidTable (1,
                                                    pu4Ieee8021PbbTeTeSidId,
                                                    0,
                                                    pu4Ieee8021PbbTeTeSidEspIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbbTeTeSidTable
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                nextIeee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex
                nextIeee8021PbbTeTeSidEspIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbbTeTeSidTable (UINT4 u4Ieee8021PbbTeTeSidId,
                                        UINT4 *pu4NextIeee8021PbbTeTeSidId,
                                        UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                        UINT4
                                        *pu4NextIeee8021PbbTeTeSidEspIndex)
{
    tEspInfo            EspInfo;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    *pu4NextIeee8021PbbTeTeSidId = PBBTE_INIT_VAL;
    *pu4NextIeee8021PbbTeTeSidEspIndex = PBBTE_INIT_VAL;

    if (PbbTeTeSidEspEntryGetNext (u4Ieee8021PbbTeTeSidId,
                                   u4Ieee8021PbbTeTeSidEspIndex, &EspInfo)
        == PBBTE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4NextIeee8021PbbTeTeSidId = EspInfo.u4TeSid;
    *pu4NextIeee8021PbbTeTeSidEspIndex = EspInfo.u4EspIndex;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeTeSidEsp
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                retValIeee8021PbbTeTeSidEsp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeTeSidEsp (UINT4 u4Ieee8021PbbTeTeSidId,
                             UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValIeee8021PbbTeTeSidEsp)
{
    tEspInfo            EspInfo;
    tVlanId             EspVlan;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    if (PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                               u4Ieee8021PbbTeTeSidEspIndex,
                               &EspInfo) == PBBTE_SUCCESS)
    {
        MEMCPY (pRetValIeee8021PbbTeTeSidEsp->pu1_OctetList,
                EspInfo.DestAddr, PBBTE_MAC_ADDR_SIZE);
        MEMCPY (pRetValIeee8021PbbTeTeSidEsp->pu1_OctetList +
                PBBTE_ESP_SA_OFFSET, EspInfo.SourceAddr, PBBTE_MAC_ADDR_SIZE);
        EspVlan = OSIX_HTONS (EspInfo.EspVlan);
        MEMCPY (pRetValIeee8021PbbTeTeSidEsp->pu1_OctetList +
                PBBTE_ESP_VLAN_OFFSET, &EspVlan, PBBTE_VLAN_ID_SIZE);
        pRetValIeee8021PbbTeTeSidEsp->i4_Length = PBBTE_ESP_LENGTH;

        return SNMP_SUCCESS;
    }
    PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                        PBBTE_TRC_TESID_GET_FAIL,
                        u4Ieee8021PbbTeTeSidId, u4Ieee8021PbbTeTeSidEspIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeTeSidStorageType
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                retValIeee8021PbbTeTeSidStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeTeSidStorageType (UINT4 u4Ieee8021PbbTeTeSidId,
                                     UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                     INT4
                                     *pi4RetValIeee8021PbbTeTeSidStorageType)
{
    tEspInfo            EspInfo;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    *pi4RetValIeee8021PbbTeTeSidStorageType = PBBTE_INIT_VAL;

    if (PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                               u4Ieee8021PbbTeTeSidEspIndex,
                               &EspInfo) == PBBTE_SUCCESS)
    {
        *pi4RetValIeee8021PbbTeTeSidStorageType = EspInfo.u1StorageType;
        return SNMP_SUCCESS;
    }
    PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                        PBBTE_TRC_TESID_STORAGETYPE_GET_FAIL,
                        u4Ieee8021PbbTeTeSidId, u4Ieee8021PbbTeTeSidEspIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeTeSidRowStatus
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                retValIeee8021PbbTeTeSidRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeTeSidRowStatus (UINT4 u4Ieee8021PbbTeTeSidId,
                                   UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                   INT4 *pi4RetValIeee8021PbbTeTeSidRowStatus)
{
    tEspInfo            EspInfo;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    *pi4RetValIeee8021PbbTeTeSidRowStatus = PBBTE_INIT_VAL;

    if (PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                               u4Ieee8021PbbTeTeSidEspIndex,
                               &EspInfo) == PBBTE_SUCCESS)
    {
        *pi4RetValIeee8021PbbTeTeSidRowStatus = EspInfo.u1RowStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeTeSidEsp
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                setValIeee8021PbbTeTeSidEsp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeTeSidEsp (UINT4 u4Ieee8021PbbTeTeSidId,
                             UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValIeee8021PbbTeTeSidEsp)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tMacAddr            DestMac;
    tMacAddr            SrcMac;
    UINT4               u4SeqNum = 0;
    UINT2               u2Vlan = PBBTE_INIT_VAL;

    PBBTE_PERF_MARK_START_TIME ();

    PBBTE_MEM_SET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    MEMCPY (DestMac, pSetValIeee8021PbbTeTeSidEsp->pu1_OctetList,
            PBBTE_MAC_ADDR_SIZE);
    MEMCPY (SrcMac, pSetValIeee8021PbbTeTeSidEsp->pu1_OctetList
            + PBBTE_ESP_SA_OFFSET, PBBTE_MAC_ADDR_SIZE);
    MEMCPY (&u2Vlan, pSetValIeee8021PbbTeTeSidEsp->pu1_OctetList
            + PBBTE_ESP_VLAN_OFFSET, PBBTE_VLAN_ID_SIZE);

    u2Vlan = OSIX_NTOHS (u2Vlan);

    RM_GET_SEQ_NUM (&u4SeqNum);
    if (PbbTeTeSidEspSet (u4Ieee8021PbbTeTeSidId, u4Ieee8021PbbTeTeSidEspIndex,
                          DestMac, SrcMac, u2Vlan) == PBBTE_FAILURE)
    {
        return SNMP_FAILURE;
    }
    PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC, PBBTE_TRC_TESID_SET,
                        u4Ieee8021PbbTeTeSidId, u4Ieee8021PbbTeTeSidEspIndex);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Ieee8021PbbTeTeSidEsp,
                          u4SeqNum, FALSE, NULL, NULL, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %s", u4Ieee8021PbbTeTeSidId,
                      u4Ieee8021PbbTeTeSidEspIndex,
                      pSetValIeee8021PbbTeTeSidEsp));

    PBBTE_PERF_MARK_END_TIME ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeTeSidStorageType
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                setValIeee8021PbbTeTeSidStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeTeSidStorageType (UINT4 u4Ieee8021PbbTeTeSidId,
                                     UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                     INT4 i4SetValIeee8021PbbTeTeSidStorageType)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    PBBTE_MEM_SET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    if (PbbTeTeSidStorageTypeSet (u4Ieee8021PbbTeTeSidId,
                                  u4Ieee8021PbbTeTeSidEspIndex,
                                  (UINT1) i4SetValIeee8021PbbTeTeSidStorageType)
        == PBBTE_FAILURE)
    {
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_STORAGETYPE_SET_FAIL,
                            u4Ieee8021PbbTeTeSidId,
                            u4Ieee8021PbbTeTeSidEspIndex);

        return SNMP_FAILURE;
    }

    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Ieee8021PbbTeTeSidStorageType,
                          u4SeqNum, FALSE, NULL, NULL, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", u4Ieee8021PbbTeTeSidId,
                      u4Ieee8021PbbTeTeSidEspIndex,
                      i4SetValIeee8021PbbTeTeSidStorageType));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeTeSidRowStatus
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                setValIeee8021PbbTeTeSidRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeTeSidRowStatus (UINT4 u4Ieee8021PbbTeTeSidId,
                                   UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                   INT4 i4SetValIeee8021PbbTeTeSidRowStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    tEspInfo            EspInfo;
    UINT4               u4SeqNum = 0;

    PBBTE_PERF_MARK_START_TIME ();

    PBBTE_MEM_SET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    if (PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                               u4Ieee8021PbbTeTeSidEspIndex,
                               &EspInfo) == PBBTE_FAILURE)
    {
        /* Entry is not existing, it has to be a new entry, no value other
         * than CREATE_AND_WAIT or CREATE_AND_GO are allowed */
        if ((i4SetValIeee8021PbbTeTeSidRowStatus != PBBTE_CREATE_AND_WAIT) &&
            (i4SetValIeee8021PbbTeTeSidRowStatus != PBBTE_CREATE_AND_GO))
        {
            PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_TESID_WRONG_ROWSTATUS,
                                u4Ieee8021PbbTeTeSidId,
                                u4Ieee8021PbbTeTeSidEspIndex);

            return SNMP_FAILURE;
        }
    }
    else
    {
        if (EspInfo.u1RowStatus == (UINT1) i4SetValIeee8021PbbTeTeSidRowStatus)
        {
            PBBTE_GBL_TRC (PBBTE_MGMT_TRC, PBBTE_TRC_TESID_SAME_ROWSTATUS);
            return SNMP_SUCCESS;
        }
        if ((i4SetValIeee8021PbbTeTeSidRowStatus == PBBTE_CREATE_AND_WAIT) ||
            (i4SetValIeee8021PbbTeTeSidRowStatus == PBBTE_CREATE_AND_GO))
        {
            PBBTE_GBL_TRC (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                           PBBTE_TRC_TESID_WRONG_ROWSTATUS_2);

            return SNMP_FAILURE;
        }
    }
    switch (i4SetValIeee8021PbbTeTeSidRowStatus)
    {
        case PBBTE_CREATE_AND_GO:
        case PBBTE_CREATE_AND_WAIT:

            if (PbbTeTeSidEspEntryCreate (u4Ieee8021PbbTeTeSidId,
                                          &u4Ieee8021PbbTeTeSidEspIndex)
                == PBBTE_FAILURE)
            {
                PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                    PBBTE_TRC_TESID_CRT_FAIL,
                                    u4Ieee8021PbbTeTeSidId,
                                    u4Ieee8021PbbTeTeSidEspIndex);
                return SNMP_FAILURE;
            }
            if (PbbTeTeSidRowStatusSet (u4Ieee8021PbbTeTeSidId,
                                        u4Ieee8021PbbTeTeSidEspIndex,
                                        (UINT1)
                                        PBBTE_NOT_IN_SERVICE) == PBBTE_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case PBBTE_ACTIVE:

            if (PbbTeTeSidRowStatusSet (u4Ieee8021PbbTeTeSidId,
                                        u4Ieee8021PbbTeTeSidEspIndex,
                                        (UINT1)
                                        i4SetValIeee8021PbbTeTeSidRowStatus) ==
                PBBTE_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        case PBBTE_DESTROY:

            if (PbbTeTeSidEspEntryDel (u4Ieee8021PbbTeTeSidId,
                                       u4Ieee8021PbbTeTeSidEspIndex)
                == PBBTE_FAILURE)
            {
                PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                    PBBTE_TRC_TESID_DEL_FAIL,
                                    u4Ieee8021PbbTeTeSidId,
                                    u4Ieee8021PbbTeTeSidEspIndex);
                return SNMP_FAILURE;
            }
            break;

        case PBBTE_NOT_IN_SERVICE:
            if (PbbTeTeSidRowStatusSet (u4Ieee8021PbbTeTeSidId,
                                        u4Ieee8021PbbTeTeSidEspIndex,
                                        (UINT1)
                                        i4SetValIeee8021PbbTeTeSidRowStatus) ==
                PBBTE_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;

        default:
            return SNMP_FAILURE;
    }
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Ieee8021PbbTeTeSidRowStatus,
                          u4SeqNum, TRUE, NULL, NULL, 2, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i", u4Ieee8021PbbTeTeSidId,
                      u4Ieee8021PbbTeTeSidEspIndex,
                      i4SetValIeee8021PbbTeTeSidRowStatus));

    PBBTE_PERF_MARK_END_TIME ();

    if (i4SetValIeee8021PbbTeTeSidRowStatus == PBBTE_ACTIVE)
    {
        PBBTE_PERF_CALC_ADD_TIMETAKEN (1);
    }
    else if (i4SetValIeee8021PbbTeTeSidRowStatus == PBBTE_DESTROY)
    {
        PBBTE_PERF_CALC_DEL_TIMETAKEN ();
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeTeSidEsp
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                testValIeee8021PbbTeTeSidEsp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeTeSidEsp (UINT4 *pu4ErrorCode,
                                UINT4 u4Ieee8021PbbTeTeSidId,
                                UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValIeee8021PbbTeTeSidEsp)
{
    tMacAddr            DestMac;
    tMacAddr            SrcMac;
    tMacAddr            NullMac;
    tMacAddr            BroadCastMac;
    tEspInfo            EspInfo;
    UINT2               u2Vlan = 0;
    UINT1               u1Result = PBBTE_FALSE;

    PBBTE_PERF_MARK_START_TIME ();

    if (PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                               u4Ieee8021PbbTeTeSidEspIndex,
                               &EspInfo) == PBBTE_FAILURE)
    {
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_GET_FAIL,
                            u4Ieee8021PbbTeTeSidId,
                            u4Ieee8021PbbTeTeSidEspIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    MEMCPY (DestMac, pTestValIeee8021PbbTeTeSidEsp->pu1_OctetList,
            PBBTE_MAC_ADDR_SIZE);
    MEMCPY (SrcMac, pTestValIeee8021PbbTeTeSidEsp->pu1_OctetList
            + PBBTE_ESP_SA_OFFSET, PBBTE_MAC_ADDR_SIZE);
    MEMCPY (&u2Vlan, pTestValIeee8021PbbTeTeSidEsp->pu1_OctetList
            + PBBTE_ESP_VLAN_OFFSET, PBBTE_VLAN_ID_SIZE);

    u2Vlan = OSIX_NTOHS (u2Vlan);

    if (pTestValIeee8021PbbTeTeSidEsp->i4_Length != PBBTE_ESP_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Source MAC & Destination MAC configured can't be NULL MAC */

    PBBTE_MEM_SET (&NullMac, PBBTE_INIT_VAL, sizeof (tMacAddr));

    if (PBBTE_MEM_CMP (DestMac, NullMac, sizeof (tMacAddr)) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (PBBTE_NULL_MAC_ADDR);
        return SNMP_FAILURE;
    }
    if (PBBTE_MEM_CMP (SrcMac, NullMac, sizeof (tMacAddr)) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (PBBTE_NULL_MAC_ADDR);
        return SNMP_FAILURE;
    }

    /* Source MAC & Destination MAC configured can't be Bcast MAC */

    PBBTE_MEM_SET (&BroadCastMac, 1, sizeof (tMacAddr));
    if (PBBTE_MEM_CMP (DestMac, BroadCastMac, sizeof (tMacAddr)) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (PBBTE_BCAST_MAC_ADDR);
        return SNMP_FAILURE;
    }
    if (PBBTE_MEM_CMP (SrcMac, BroadCastMac, sizeof (tMacAddr)) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (PBBTE_BCAST_MAC_ADDR);
        return SNMP_FAILURE;
    }

    /* Source MAC configured can't be Multicast MAC */

    if (PBBTE_IS_MCASTADDR (SrcMac) == PBBTE_TRUE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (PBBTE_SRC_MCAST_ADDR_ERR);
        return SNMP_FAILURE;
    }
    if (EspInfo.u4ContextId != PBBTE_INVALID_CONTEXT)
    {
        /* VLAN Configured in the ESP, will be verified as ESP Vlan or not, 
         * before RowStatus is made ACTIVE */

        PBBTE_IS_ESP_VLAN (EspInfo.u4ContextId, u2Vlan, u1Result);

        if (u1Result == PBBTE_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (PBBTE_VLAN_NOT_AN_ESP_VLAN);
            PBBTE_GBL_TRC_ARG3 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_TESID_ROWSTATUS_VLAN_FAIL,
                                u4Ieee8021PbbTeTeSidId,
                                u4Ieee8021PbbTeTeSidEspIndex, u2Vlan);
            return SNMP_FAILURE;
        }
    }
    if ((u2Vlan < 1) || (u2Vlan > PBBTE_MAX_VLAN_ID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (PBBTE_INVALID_VLAN_ID);
        return SNMP_FAILURE;
    }

    PBBTE_PERF_MARK_END_TIME ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeTeSidStorageType
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                testValIeee8021PbbTeTeSidStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeTeSidStorageType (UINT4 *pu4ErrorCode,
                                        UINT4 u4Ieee8021PbbTeTeSidId,
                                        UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                        INT4
                                        i4TestValIeee8021PbbTeTeSidStorageType)
{
    UNUSED_PARAM (u4Ieee8021PbbTeTeSidId);
    UNUSED_PARAM (u4Ieee8021PbbTeTeSidEspIndex);

    if ((i4TestValIeee8021PbbTeTeSidStorageType < PBBTE_STORAGE_OTHER) ||
        (i4TestValIeee8021PbbTeTeSidStorageType > PBBTE_STORAGE_READONLY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeTeSidRowStatus
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex

                The Object 
                testValIeee8021PbbTeTeSidRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeTeSidRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4Ieee8021PbbTeTeSidId,
                                      UINT4 u4Ieee8021PbbTeTeSidEspIndex,
                                      INT4 i4TestValIeee8021PbbTeTeSidRowStatus)
{
    tEspInfo            EspInfo;
    UINT1               u1Result = PBBTE_FALSE;
    INT4                i4RetVal = PBBTE_FAILURE;

    PBBTE_PERF_MARK_START_TIME ();

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    i4RetVal = PbbTeTeSidEspEntryGet (u4Ieee8021PbbTeTeSidId,
                                      u4Ieee8021PbbTeTeSidEspIndex, &EspInfo);

    if ((i4RetVal == PBBTE_FAILURE) &&
        (i4TestValIeee8021PbbTeTeSidRowStatus != PBBTE_CREATE_AND_WAIT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (PBBTE_TESID_NOT_FOUND_ERR);
        return SNMP_FAILURE;
    }
    else if ((i4RetVal == PBBTE_SUCCESS) &&
             (i4TestValIeee8021PbbTeTeSidRowStatus == PBBTE_CREATE_AND_WAIT))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (PBBTE_TESID_NOT_FOUND_ERR);
        return SNMP_FAILURE;
    }

    switch (i4TestValIeee8021PbbTeTeSidRowStatus)
    {
        case PBBTE_CREATE_AND_WAIT:
            if (MemGetFreeUnits (PBBTE_TESID_MEMPOOL_ID) == 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC |
                                    PBBTE_BUFFER_TRC, PBBTE_TRC_MEM_FAIL_TESID,
                                    u4Ieee8021PbbTeTeSidId,
                                    u4Ieee8021PbbTeTeSidEspIndex);
                return SNMP_FAILURE;
            }
            break;

        case PBBTE_DESTROY:
        case PBBTE_NOT_IN_SERVICE:
            break;

        case PBBTE_ACTIVE:
            /* Valid context ID need to be given, before making the
             * RowStatus as ACTIVE */
            if (EspInfo.u4ContextId == PBBTE_INVALID_CONTEXT)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (PBBTE_CONTEXT_INVALID);
                PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                    PBBTE_TRC_TESID_ROWSTATUS_CTX_FAIL,
                                    u4Ieee8021PbbTeTeSidId,
                                    u4Ieee8021PbbTeTeSidEspIndex);
                CLI_SET_ERR (PBBTE_CONTEXT_INVALID);
                return SNMP_FAILURE;
            }

            /* VLAN Configured in the ESP, will be verified as ESP Vlan or not, 
             * before RowStatus is made ACTIVE */

            PBBTE_IS_ESP_VLAN (EspInfo.u4ContextId, EspInfo.EspVlan, u1Result);

            if (u1Result == PBBTE_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (PBBTE_VLAN_NOT_AN_ESP_VLAN);
                PBBTE_GBL_TRC_ARG3 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                    PBBTE_TRC_TESID_ROWSTATUS_VLAN_FAIL,
                                    u4Ieee8021PbbTeTeSidId,
                                    u4Ieee8021PbbTeTeSidEspIndex,
                                    EspInfo.EspVlan);
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
            break;
    }

    PBBTE_PERF_MARK_END_TIME ();

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbbTeTeSidTable
 Input       :  The Indices
                Ieee8021PbbTeTeSidId
                Ieee8021PbbTeTeSidEspIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbbTeTeSidTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbbTeProtectionGroupConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupConfigTable (INT4
                                                                 i4FsDot1qVlanContextId,
                                                                 UINT4
                                                                 u4Ieee8021PbbTeProtectionGroupListGroupId)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbbTeProtectionGroupConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbbTeProtectionGroupConfigTable (INT4
                                                         *pi4FsDot1qVlanContextId,
                                                         UINT4
                                                         *pu4Ieee8021PbbTeProtectionGroupListGroupId)
{
    UNUSED_PARAM (pi4FsDot1qVlanContextId);
    UNUSED_PARAM (pu4Ieee8021PbbTeProtectionGroupListGroupId);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbbTeProtectionGroupConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId
                nextIeee8021PbbTeProtectionGroupListGroupId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbbTeProtectionGroupConfigTable (INT4
                                                        i4FsDot1qVlanContextId,
                                                        INT4
                                                        *pi4NextFsDot1qVlanContextId,
                                                        UINT4
                                                        u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                        UINT4
                                                        *pu4NextIeee8021PbbTeProtectionGroupListGroupId)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (pi4NextFsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pu4NextIeee8021PbbTeProtectionGroupListGroupId);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupConfigState
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupConfigState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupConfigState (INT4 i4FsDot1qVlanContextId,
                                               UINT4
                                               u4Ieee8021PbbTeProtectionGroupListGroupId,
                                               INT4
                                               *pi4RetValIeee8021PbbTeProtectionGroupConfigState)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupConfigState);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupConfigCommandStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupConfigCommandStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupConfigCommandStatus (INT4
                                                       i4FsDot1qVlanContextId,
                                                       UINT4
                                                       u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                       INT4
                                                       *pi4RetValIeee8021PbbTeProtectionGroupConfigCommandStatus)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupConfigCommandStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupConfigCommandAdmin
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupConfigCommandAdmin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupConfigCommandAdmin (INT4
                                                      i4FsDot1qVlanContextId,
                                                      UINT4
                                                      u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                      INT4
                                                      *pi4RetValIeee8021PbbTeProtectionGroupConfigCommandAdmin)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupConfigCommandAdmin);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupConfigWTR
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupConfigWTR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupConfigWTR (INT4 i4FsDot1qVlanContextId,
                                             UINT4
                                             u4Ieee8021PbbTeProtectionGroupListGroupId,
                                             UINT4
                                             *pu4RetValIeee8021PbbTeProtectionGroupConfigWTR)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pu4RetValIeee8021PbbTeProtectionGroupConfigWTR);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupConfigHoldOff
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupConfigHoldOff
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupConfigHoldOff (INT4 i4FsDot1qVlanContextId,
                                                 UINT4
                                                 u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                 UINT4
                                                 *pu4RetValIeee8021PbbTeProtectionGroupConfigHoldOff)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pu4RetValIeee8021PbbTeProtectionGroupConfigHoldOff);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupConfigStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                retValIeee8021PbbTeProtectionGroupConfigStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupConfigStorageType (INT4
                                                     i4FsDot1qVlanContextId,
                                                     UINT4
                                                     u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                     INT4
                                                     *pi4RetValIeee8021PbbTeProtectionGroupConfigStorageType)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupConfigStorageType);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupConfigCommandAdmin
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupConfigCommandAdmin
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupConfigCommandAdmin (INT4
                                                      i4FsDot1qVlanContextId,
                                                      UINT4
                                                      u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                      INT4
                                                      i4SetValIeee8021PbbTeProtectionGroupConfigCommandAdmin)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4SetValIeee8021PbbTeProtectionGroupConfigCommandAdmin);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupConfigWTR
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupConfigWTR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupConfigWTR (INT4 i4FsDot1qVlanContextId,
                                             UINT4
                                             u4Ieee8021PbbTeProtectionGroupListGroupId,
                                             UINT4
                                             u4SetValIeee8021PbbTeProtectionGroupConfigWTR)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4SetValIeee8021PbbTeProtectionGroupConfigWTR);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupConfigHoldOff
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupConfigHoldOff
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupConfigHoldOff (INT4 i4FsDot1qVlanContextId,
                                                 UINT4
                                                 u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                 UINT4
                                                 u4SetValIeee8021PbbTeProtectionGroupConfigHoldOff)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4SetValIeee8021PbbTeProtectionGroupConfigHoldOff);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupConfigStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                setValIeee8021PbbTeProtectionGroupConfigStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupConfigStorageType (INT4
                                                     i4FsDot1qVlanContextId,
                                                     UINT4
                                                     u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                     INT4
                                                     i4SetValIeee8021PbbTeProtectionGroupConfigStorageType)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4SetValIeee8021PbbTeProtectionGroupConfigStorageType);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupConfigCommandAdmin
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupConfigCommandAdmin
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupConfigCommandAdmin (UINT4 *pu4ErrorCode,
                                                         INT4
                                                         i4FsDot1qVlanContextId,
                                                         UINT4
                                                         u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                         INT4
                                                         i4TestValIeee8021PbbTeProtectionGroupConfigCommandAdmin)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4TestValIeee8021PbbTeProtectionGroupConfigCommandAdmin);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupConfigWTR
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupConfigWTR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupConfigWTR (UINT4 *pu4ErrorCode,
                                                INT4 i4FsDot1qVlanContextId,
                                                UINT4
                                                u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                UINT4
                                                u4TestValIeee8021PbbTeProtectionGroupConfigWTR)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4TestValIeee8021PbbTeProtectionGroupConfigWTR);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupConfigHoldOff
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupConfigHoldOff
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupConfigHoldOff (UINT4 *pu4ErrorCode,
                                                    INT4 i4FsDot1qVlanContextId,
                                                    UINT4
                                                    u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                    UINT4
                                                    u4TestValIeee8021PbbTeProtectionGroupConfigHoldOff)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (u4TestValIeee8021PbbTeProtectionGroupConfigHoldOff);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupConfigStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId

                The Object 
                testValIeee8021PbbTeProtectionGroupConfigStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupConfigStorageType (UINT4 *pu4ErrorCode,
                                                        INT4
                                                        i4FsDot1qVlanContextId,
                                                        UINT4
                                                        u4Ieee8021PbbTeProtectionGroupListGroupId,
                                                        INT4
                                                        i4TestValIeee8021PbbTeProtectionGroupConfigStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupListGroupId);
    UNUSED_PARAM (i4TestValIeee8021PbbTeProtectionGroupConfigStorageType);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbbTeProtectionGroupConfigTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeProtectionGroupListGroupId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbbTeProtectionGroupConfigTable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbbTeProtectionGroupISidTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupISidTable
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbbTeProtectionGroupISidTable (UINT4
                                                               u4Ieee8021PbbTeProtectionGroupISidIndex)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbbTeProtectionGroupISidTable
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbbTeProtectionGroupISidTable (UINT4
                                                       *pu4Ieee8021PbbTeProtectionGroupISidIndex)
{
    UNUSED_PARAM (pu4Ieee8021PbbTeProtectionGroupISidIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbbTeProtectionGroupISidTable
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex
                nextIeee8021PbbTeProtectionGroupISidIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbbTeProtectionGroupISidTable (UINT4
                                                      u4Ieee8021PbbTeProtectionGroupISidIndex,
                                                      UINT4
                                                      *pu4NextIeee8021PbbTeProtectionGroupISidIndex)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (pu4NextIeee8021PbbTeProtectionGroupISidIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupISidGroupId
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                retValIeee8021PbbTeProtectionGroupISidGroupId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupISidGroupId (UINT4
                                               u4Ieee8021PbbTeProtectionGroupISidIndex,
                                               UINT4
                                               *pu4RetValIeee8021PbbTeProtectionGroupISidGroupId)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (pu4RetValIeee8021PbbTeProtectionGroupISidGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupISidStorageType
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                retValIeee8021PbbTeProtectionGroupISidStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupISidStorageType (UINT4
                                                   u4Ieee8021PbbTeProtectionGroupISidIndex,
                                                   INT4
                                                   *pi4RetValIeee8021PbbTeProtectionGroupISidStorageType)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupISidStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeProtectionGroupISidRowStatus
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                retValIeee8021PbbTeProtectionGroupISidRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeProtectionGroupISidRowStatus (UINT4
                                                 u4Ieee8021PbbTeProtectionGroupISidIndex,
                                                 INT4
                                                 *pi4RetValIeee8021PbbTeProtectionGroupISidRowStatus)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeProtectionGroupISidRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupISidGroupId
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                setValIeee8021PbbTeProtectionGroupISidGroupId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupISidGroupId (UINT4
                                               u4Ieee8021PbbTeProtectionGroupISidIndex,
                                               UINT4
                                               u4SetValIeee8021PbbTeProtectionGroupISidGroupId)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (u4SetValIeee8021PbbTeProtectionGroupISidGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupISidStorageType
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                setValIeee8021PbbTeProtectionGroupISidStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupISidStorageType (UINT4
                                                   u4Ieee8021PbbTeProtectionGroupISidIndex,
                                                   INT4
                                                   i4SetValIeee8021PbbTeProtectionGroupISidStorageType)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (i4SetValIeee8021PbbTeProtectionGroupISidStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeProtectionGroupISidRowStatus
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                setValIeee8021PbbTeProtectionGroupISidRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeProtectionGroupISidRowStatus (UINT4
                                                 u4Ieee8021PbbTeProtectionGroupISidIndex,
                                                 INT4
                                                 i4SetValIeee8021PbbTeProtectionGroupISidRowStatus)
{
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (i4SetValIeee8021PbbTeProtectionGroupISidRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupISidGroupId
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                testValIeee8021PbbTeProtectionGroupISidGroupId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupISidGroupId (UINT4 *pu4ErrorCode,
                                                  UINT4
                                                  u4Ieee8021PbbTeProtectionGroupISidIndex,
                                                  UINT4
                                                  u4TestValIeee8021PbbTeProtectionGroupISidGroupId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (u4TestValIeee8021PbbTeProtectionGroupISidGroupId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupISidStorageType
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                testValIeee8021PbbTeProtectionGroupISidStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupISidStorageType (UINT4 *pu4ErrorCode,
                                                      UINT4
                                                      u4Ieee8021PbbTeProtectionGroupISidIndex,
                                                      INT4
                                                      i4TestValIeee8021PbbTeProtectionGroupISidStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (i4TestValIeee8021PbbTeProtectionGroupISidStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeProtectionGroupISidRowStatus
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex

                The Object 
                testValIeee8021PbbTeProtectionGroupISidRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeProtectionGroupISidRowStatus (UINT4 *pu4ErrorCode,
                                                    UINT4
                                                    u4Ieee8021PbbTeProtectionGroupISidIndex,
                                                    INT4
                                                    i4TestValIeee8021PbbTeProtectionGroupISidRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4Ieee8021PbbTeProtectionGroupISidIndex);
    UNUSED_PARAM (i4TestValIeee8021PbbTeProtectionGroupISidRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbbTeProtectionGroupISidTable
 Input       :  The Indices
                Ieee8021PbbTeProtectionGroupISidIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbbTeProtectionGroupISidTable (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ieee8021PbbTeBridgeStaticForwardAnyUnicastTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceIeee8021PbbTeBridgeStaticForwardAnyUnicastTable (INT4
                                                                         i4FsDot1qVlanContextId,
                                                                         UINT4
                                                                         u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable (INT4
                                                                 *pi4FsDot1qVlanContextId,
                                                                 UINT4
                                                                 *pu4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex)
{
    UNUSED_PARAM (pi4FsDot1qVlanContextId);
    UNUSED_PARAM (pu4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                nextFsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex
                nextIeee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexIeee8021PbbTeBridgeStaticForwardAnyUnicastTable (INT4
                                                                i4FsDot1qVlanContextId,
                                                                INT4
                                                                *pi4NextFsDot1qVlanContextId,
                                                                UINT4
                                                                u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                                UINT4
                                                                *pu4NextIeee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (pi4NextFsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM (pu4NextIeee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                retValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts (INT4
                                                             i4FsDot1qVlanContextId,
                                                             UINT4
                                                             u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                             tSNMP_OCTET_STRING_TYPE
                                                             *
                                                             pRetValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM (pRetValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                retValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts (INT4
                                                                i4FsDot1qVlanContextId,
                                                                UINT4
                                                                u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                                tSNMP_OCTET_STRING_TYPE
                                                                *
                                                                pRetValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM
        (pRetValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                retValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType (INT4
                                                             i4FsDot1qVlanContextId,
                                                             UINT4
                                                             u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                             INT4
                                                             *pi4RetValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM
        (pi4RetValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                retValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus (INT4
                                                           i4FsDot1qVlanContextId,
                                                           UINT4
                                                           u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                           INT4
                                                           *pi4RetValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM (pi4RetValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                setValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts (INT4
                                                             i4FsDot1qVlanContextId,
                                                             UINT4
                                                             u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                             tSNMP_OCTET_STRING_TYPE
                                                             *
                                                             pSetValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM (pSetValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                setValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts (INT4
                                                                i4FsDot1qVlanContextId,
                                                                UINT4
                                                                u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                                tSNMP_OCTET_STRING_TYPE
                                                                *
                                                                pSetValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM
        (pSetValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                setValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType (INT4
                                                             i4FsDot1qVlanContextId,
                                                             UINT4
                                                             u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                             INT4
                                                             i4SetValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM
        (i4SetValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                setValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus (INT4
                                                           i4FsDot1qVlanContextId,
                                                           UINT4
                                                           u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                           INT4
                                                           i4SetValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus)
{
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM (i4SetValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                testValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts (UINT4
                                                                *pu4ErrorCode,
                                                                INT4
                                                                i4FsDot1qVlanContextId,
                                                                UINT4
                                                                u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                                tSNMP_OCTET_STRING_TYPE
                                                                *
                                                                pTestValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM
        (pTestValIeee8021PbbTeBridgeStaticForwardAnyUnicastEgressPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                testValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts (UINT4
                                                                   *pu4ErrorCode,
                                                                   INT4
                                                                   i4FsDot1qVlanContextId,
                                                                   UINT4
                                                                   u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                                   tSNMP_OCTET_STRING_TYPE
                                                                   *
                                                                   pTestValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM
        (pTestValIeee8021PbbTeBridgeStaticForwardAnyUnicastForbiddenPorts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                testValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastStorageType (UINT4
                                                                *pu4ErrorCode,
                                                                INT4
                                                                i4FsDot1qVlanContextId,
                                                                UINT4
                                                                u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                                INT4
                                                                i4TestValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM
        (i4TestValIeee8021PbbTeBridgeStaticForwardAnyUnicastStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex

                The Object 
                testValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus (UINT4
                                                              *pu4ErrorCode,
                                                              INT4
                                                              i4FsDot1qVlanContextId,
                                                              UINT4
                                                              u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex,
                                                              INT4
                                                              i4TestValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsDot1qVlanContextId);
    UNUSED_PARAM (u4Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex);
    UNUSED_PARAM (i4TestValIeee8021PbbTeBridgeStaticForwardAnyUnicastRowStatus);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastTable
 Input       :  The Indices
                FsDot1qVlanContextId
                Ieee8021PbbTeBridgeStaticForwardAnyUnicastVlanIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Ieee8021PbbTeBridgeStaticForwardAnyUnicastTable (UINT4 *pu4ErrorCode,
                                                         tSnmpIndexList *
                                                         pSnmpIndexList,
                                                         tSNMP_VAR_BIND *
                                                         pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
