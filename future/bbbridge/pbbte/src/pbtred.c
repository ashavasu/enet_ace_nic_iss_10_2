/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: pbtred.c,v 1.6 2014/03/19 13:36:12 siva Exp $
 *                                                                           *
 * Description: This file contains RED routine used by the                   *
 *              PBB-TE HA module                                             *
 *                                                                           *
 *****************************************************************************/
#ifdef L2RED_WANTED
#include "pbthdrs.h"

/*****************************************************************************/
/* Function Name      : PbbTeRedInitGlobalInfo                               */
/*                                                                           */
/* Description        : Initializes redundancy global variables.             */
/*                      This function will get invoked during BOOTUP.        */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
PbbTeRedInitGlobalInfo (VOID)
{
    PBBTE_MEM_SET (&PBBTE_RED_GLOBAL_INFO, PBBTE_INIT_VAL,
                   sizeof (tPbbTeRedGlobInfo));
    PBBTE_NODE_STATE = PBBTE_NODE_IDLE;
    PBBTE_RED_BULK_REQ_RECD () = PBBTE_FALSE;
    PBBTE_RED_NUM_STANDBY_NODES () = PbbTeRmGetStandbyNodeCount ();
#ifdef NPAPI_WANTED
    /* The NpSync-ups are sent on calling NPAPIs for H/W Audit, these sync-ups 
     * are blocked by this flag. It is set to false, hence wont be blocked by
     * default 
     */
    PBBTE_NPSYNC_BLK () = OSIX_FALSE;

#endif
    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedRegisterWithRm                               */
/*                                                                           */
/* Description        : Registers PBBTE module with RM to send and receive   */
/*                      update messages.                                     */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then PBBTE_SUCCESS        */
/*                      Otherwise PBBTE_FAILURE                              */
/*****************************************************************************/
INT4
PbbTeRedRegisterWithRm (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_PBBTE_APP_ID;
    RmRegParams.pFnRcvPkt = PbbTeRedHandleUpdateEvents;

    if (PbbTeRmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        PBBTE_GBL_TRC (PBBTE_INIT_SHUT_TRC | PBBTE_RED_TRC |
                       PBBTE_ALL_FAILURE_TRC, PBBTE_TRC_RM_REG_FAIL);
        return PBBTE_FAILURE;
    }

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedHandleUpdateEvents                           */
/*                                                                           */
/* Description        : This function will be invoked by the RM module to    */
/*                      pass events like GO_ACTIVE, GO_STANDBY etc. to PBB-TE*/
/*                      module.                                              */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module.                  */
/*                      pData   - Msg to be enqueued, valid if u1Event is    */
/*                                VALID_UPDATE_MESSAGE.                      */
/*                      u2DataLen - Size of the update message.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS - If the message is handled successfully  */
/*                      RM_FAILURE otherwise                                 */
/*****************************************************************************/
INT4
PbbTeRedHandleUpdateEvents (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tPbbTeQMsg         *pPbbTeQMsg = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to PBB-TE task. */
        PBBTE_GBL_TRC (PBBTE_BUFFER_TRC | PBBTE_ALL_FAILURE_TRC |
                       PBBTE_RED_TRC, PBBTE_TRC_RM_BUF_NULL);

        return RM_FAILURE;
    }

    if (PBBTE_IS_MODULE_INITIALISED () != PBBTE_TRUE)
    {
        /* PBB-TE module is not yet initiailised. Ignoring the
         * message from RM */
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PbbTeRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        PBBTE_GBL_TRC (ALL_FAILURE_TRC, PBBTE_TRC_MOD_NOT_INIT);
        return RM_FAILURE;
    }

    PBBTE_Q_MSG_ENTRY_ALLOC_MEMBLK (pPbbTeQMsg);

    if (pPbbTeQMsg == NULL)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_Q_BUF_MEM_FAIL);

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PbbTeRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return RM_FAILURE;
    }

    PBBTE_MEM_SET (pPbbTeQMsg, 0, sizeof (tPbbTeQMsg));

    pPbbTeQMsg->u4MsgType = PBBTE_RM_MSG;

    pPbbTeQMsg->RmData.u4Events = (UINT4) u1Event;
    pPbbTeQMsg->RmData.pRmMsg = pData;
    pPbbTeQMsg->RmData.u2DataLen = u2DataLen;

    if (PBBTE_SEND_TO_QUEUE (PBBTE_CFG_QUEUE_ID,
                             (UINT1 *) &pPbbTeQMsg,
                             OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC | PBBTE_OS_RESOURCE_TRC,
                       PBBTE_TRC_Q_SEND_TO_FAIL);

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            PbbTeRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        PBBTE_Q_MSG_ENTRY_FREE_MEMBLK (pPbbTeQMsg);

        return RM_FAILURE;
    }

    if (PBBTE_SEND_EVENT (PBBTE_TASK_ID, PBBTE_CFG_MSG_EVENT) != OSIX_SUCCESS)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_SEND_EVENT_FAIL);

        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedProcessUpdateMsg                             */
/*                                                                           */
/* Description        : This function handles the received update messages   */
/*                      from the RM module. This function invokes appropriate*/
/*                      functions to handle the update message.              */
/*                                                                           */
/* Input(s)           : pPbbTeQMsg - Pointer to the PBB-TE Queue message.    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbTeRedProcessUpdateMsg (tPbbTeQMsg * pPbbTeQMsg)
{
    tRmNodeInfo        *pData = NULL;
    tRmProtoEvt         ProtoEvt;
    INT4                i4RetVal = PBBTE_SUCCESS;
    tPbbTeNodeState     PbbTePrevNodeState = PBBTE_NODE_IDLE;
    tRmProtoAck         ProtoAck;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_PBBTE_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pPbbTeQMsg->RmData.u4Events)
    {
        case GO_ACTIVE:

            if (PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE)
            {
                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_ALR_ACTIVE);
                break;
            }

            PbbTePrevNodeState = PBBTE_NODE_STATE;

            if (PbbTePrevNodeState == PBBTE_NODE_IDLE)
            {
                /* On boot-up, Idle to Active, Node state is updated,
                 * the hardware programming is done for the static 
                 * configurations that were configured when node state was Idle
                 */
                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_IDLE_TO_ACTIVE);
                i4RetVal = PbbTeRedHandleIdleToActive ();
                ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
            }
            else if (PbbTePrevNodeState == PBBTE_NODE_STANDBY)
            {
                /* On switchover, Standby to Active, Node state is updated,
                 * other than that nothing is done as the PBB-TE does not 
                 * have any timers, state machines.
                 */
                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_STANDBY_TO_ACT);
                i4RetVal = PbbTeRedHandleStandbyToActive ();
                ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
            }

            /* Before the arrival of GO_ACTIVE event, Bulk Request has 
             * arrived from Peer PBB-TE in the Standby node. Hence send the 
             * Bulk updates Now.
             */
            if (PBBTE_RED_BULK_REQ_RECD () == PBBTE_TRUE)
            {
                PBBTE_RED_BULK_REQ_RECD () = PBBTE_FALSE;
                PBBTE_MEM_SET (&PBBTE_RED_BULK_UPD_NEXT_ESP, PBBTE_INIT_VAL,
                               sizeof (tEspInfo));

                PbbTeRedHandleStandbyUpEvent ();
            }

            PbbTeRmHandleProtocolEvent (&ProtoEvt);

            break;

        case GO_STANDBY:

            if (PBBTE_NODE_STATE == PBBTE_NODE_STANDBY)
            {
                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_ALR_STNDBY);
                break;
            }

            /* Flag not to be used in the standby node */
            PBBTE_RED_BULK_REQ_RECD () = PBBTE_FALSE;

#ifdef NPAPI_WANTED
            PbbTeCreateAuditBufferTable ();
#endif

            if (PBBTE_NODE_STATE == PBBTE_NODE_IDLE)
            {
                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_IGN_STNDBY);

#ifdef NPAPI_WANTED
                /* Between GO_STANDBY event and CONFIG_RESTORE_COMPLETE event 
                 * Static Bulk update will happen. In this phase the Audit 
                 * buffer table should not be accessed hence the block flag is
                 * set 
                 */
                PBBTE_NPSYNC_BLK () = OSIX_TRUE;
#endif
            }
            else if (PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE)
            {
                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_ACT_TO_STNDBY);
                i4RetVal = PbbTeRedHandleActiveToStandby ();
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                PbbTeRmHandleProtocolEvent (&ProtoEvt);
            }

            break;

        case RM_INIT_HW_AUDIT:

            if (PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE)
            {
                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_INIT_HW_AUD);
                /* Hardware Audit for PBB-TE is performed here
                 */
                PbbTeRedInitHardwareAudit ();
            }
            break;

        case RM_STANDBY_UP:

            pData = (tRmNodeInfo *) pPbbTeQMsg->RmData.pRmMsg;

            PBBTE_RED_NUM_STANDBY_NODES () = pData->u1NumStandby;

            PbbTeRmReleaseMemoryForMsg ((UINT1 *) pData);

            if (PBBTE_RED_BULK_REQ_RECD () == PBBTE_TRUE)
            {
                PBBTE_RED_BULK_REQ_RECD () = PBBTE_FALSE;

                /* Bulk request msg is recieved before RM_STANDBY_UP
                 * event.So we are sending bulk updates now.
                 */
                PBBTE_RED_BULK_REQ_RECD () = PBBTE_FALSE;
                PBBTE_MEM_SET (&PBBTE_RED_BULK_UPD_NEXT_ESP, PBBTE_INIT_VAL,
                               sizeof (tEspInfo));
                PbbTeRedHandleStandbyUpEvent ();
            }
            break;

        case RM_STANDBY_DOWN:

            PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                           PBBTE_TRC_RED_STNDBY_DOWN);
            pData = (tRmNodeInfo *) pPbbTeQMsg->RmData.pRmMsg;
            PBBTE_RED_NUM_STANDBY_NODES () = pData->u1NumStandby;
            PbbTeRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pPbbTeQMsg->RmData.pRmMsg, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pPbbTeQMsg->RmData.pRmMsg,
                                 pPbbTeQMsg->RmData.u2DataLen);

            ProtoAck.u4AppId = RM_PBBTE_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            PbbTeRedHandleUpdates (pPbbTeQMsg->RmData.pRmMsg,
                                   pPbbTeQMsg->RmData.u2DataLen);

            RM_FREE (pPbbTeQMsg->RmData.pRmMsg);

            /* Sending ACK to RM */
            RmApiSendProtoAckToRM (&ProtoAck);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:

            if (PBBTE_NODE_STATE == PBBTE_NODE_IDLE)
            {
                if (PbbTeRmGetNodeState () == RM_STANDBY)
                {
                    PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                                   PBBTE_TRC_RED_IDLE_TO_STNDBY);
                    i4RetVal = PbbTeRedHandleIdleToStandby ();
#ifdef NPAPI_WANTED
                    PBBTE_NPSYNC_BLK () = OSIX_FALSE;
#endif
                }

                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                PbbTeRmHandleProtocolEvent (&ProtoEvt);
            }
            break;

        case L2_INITIATE_BULK_UPDATES:

            PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                           PBBTE_TRC_RED_INIT_BULK_REQ);
            PbbTeRedSendBulkReqMsg ();
            break;

        default:
            break;
    }
    UNUSED_PARAM (i4RetVal);
    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedHandleStandbyUpEvent                         */
/*                                                                           */
/* Description        : This function handles the standby UP event from the  */
/*                      RM module. This function sends the bulk updates      */
/*                      containing the dynamic ESPs created by external      */
/*                      module through API.                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbTeRedHandleStandbyUpEvent (VOID)
{
    if (PBBTE_NODE_STATE != PBBTE_ACTIVE)
    {
        PBBTE_RED_BULK_REQ_RECD () = PBBTE_TRUE;
        return;
    }

    PbbTeRedSendBulkUpdates ();

    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedHandleIdleToActive                           */
/*                                                                           */
/* Description        : This function makes the node active from the idle    */
/*                      state. This function is called when the node becomes */
/*                      active and there are no other nodes.                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT4
PbbTeRedHandleIdleToActive (VOID)
{
    PBBTE_NODE_STATE = PBBTE_NODE_ACTIVE;

    PBBTE_RED_NUM_STANDBY_NODES () = PbbTeRmGetStandbyNodeCount ();

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedHandleStandbyToActive                        */
/*                                                                           */
/* Description        : This function makes the node active from standby     */
/*                      state. This function is called during switchover     */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS/PBBTE_FAILURE                          */
/*****************************************************************************/
INT4
PbbTeRedHandleStandbyToActive (VOID)
{
    PBBTE_NODE_STATE = PBBTE_NODE_ACTIVE;

    PBBTE_RED_NUM_STANDBY_NODES () = PbbTeRmGetStandbyNodeCount ();

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedHandleIdleToStandby                          */
/*                                                                           */
/* Description        : This function makes the node standby from the idle   */
/*                      state. This function is called when an active node is*/
/*                      present in the system.                               */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS/PBBTE_FAILURE                          */
/*****************************************************************************/
INT4
PbbTeRedHandleIdleToStandby (VOID)
{
    PBBTE_NODE_STATE = PBBTE_NODE_STANDBY;

    PBBTE_RED_NUM_STANDBY_NODES () = 0;

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedHandleActiveToStandby                        */
/*                                                                           */
/* Description        : This function makes the node active from the standby */
/*                      state. This function is called during Switchover.    */
/*                      All the dynamic entries in the system are deleted.   */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS/PBBTE_FAILURE                          */
/*****************************************************************************/
INT4
PbbTeRedHandleActiveToStandby (VOID)
{
    UINT4               u4ContextId = 0;

    /* Set NUM Peers to zero */
    PBBTE_RED_NUM_STANDBY_NODES () = 0;

    PBBTE_NODE_STATE = PBBTE_NODE_STANDBY;

    /*
     * Stop auditing if it is running. This condition should not arise.
     * Ideally RM should prevent this frequent switchover between active
     * to standby.
     */
    if (PBBTE_RED_AUDIT_TASK_ID != 0)
    {
        PBBTE_DELETE_TASK (PBBTE_RED_AUDIT_TASK_ID);
        PBBTE_RED_AUDIT_TASK_ID = 0;
    }

    for (u4ContextId = 0; u4ContextId < PBBTE_MAX_CONTEXTS; u4ContextId++)
    {
        if (PBBTE_IS_SYSTEM_STARTED (u4ContextId) == PBBTE_SHUTDOWN)
        {
            PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_MOD_ALREADY_SHUT,
                                u4ContextId);
            continue;
        }

        /* This Disable functionality will delete all the dynamic TESID entries
         * in the PBB-TE database. The Dynamic Bulk update will be initiated 
         * for the standby which will sync-up the dynamic TESID entries.
         */
        if (PbbTeDisable (u4ContextId) == PBBTE_FAILURE)
        {
            PBBTE_GBL_TRC (PBBTE_ALL_FAILURE_TRC | PBBTE_RED_TRC,
                           PBBTE_TRC_RED_DISABLE_FAIL);
            continue;
        }
    }

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedInitHardwareAudit                            */
/*                                                                           */
/* Description        : This function spawns a low priority task for the     */
/*                      hardware audit.                                      */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbTeRedInitHardwareAudit (VOID)
{
    INT4                i4RetVal;

    if (PBBTE_RED_AUDIT_TASK_ID == PBBTE_INIT_VAL)
    {
        /* Doing audit for the first time */
        i4RetVal = PBBTE_SPAWN_TASK (PBBTE_AUDIT_TASK,
                                     PBBTE_AUDIT_TASK_PRIORITY,
                                     OSIX_DEFAULT_STACK_SIZE,
                                     (OsixTskEntry) PbbTePerformAudit,
                                     0, &PBBTE_RED_AUDIT_TASK_ID);

        if (i4RetVal != OSIX_SUCCESS)
        {
            PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC |
                           PBBTE_OS_RESOURCE_TRC, PBBTE_TRC_SPWN_TSK_FAIL);
            return;
        }

    }

    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedHandleUpdates                                */
/*                                                                           */
/* Description        : This function handles messages from the PBB-TE module*/
/*                      in the peer node. The messages that are handled here */
/*                      RM_BULK_UPDT_REQ_MSG and RM_BULK_UPDT_TAIL_MSG and   */
/*                      other dynamic sync-up and dynamic bulk messages of   */
/*                      PBB-TE                                               */
/*                                                                           */
/* Input(s)           : pMsg     - RM Data buffer holding the messages       */
/*                      u2DataLen  - Length of the data in the buffer.       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbTeRedHandleUpdates (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    tEspInfo            EspInfo;
    UINT2               u2OffSet = 0;
    UINT2               u2Length;
    UINT2               u2RemMsgLen;
    UINT2               u2MinLen;
    UINT1               u1MsgType;
    UINT1               u1Action;
    UINT1               u1NonMonEspFlag;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));
    PBBTE_MEM_SET (&gPbbTeEspPathInfo, PBBTE_INIT_VAL, sizeof (tEspPathInfo));

    ProtoEvt.u4AppId = RM_PBBTE_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MinLen = PBBTE_RED_TYPE_FIELD_SIZE + PBBTE_RED_LEN_FIELD_SIZE;

    PBBTE_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

    if (u1MsgType == PBBTE_BULK_UPD_TAIL_MESSAGE)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                       PBBTE_TRC_RED_BULK_TAIL_RECVD);
        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
        PbbTeRmHandleProtocolEvent (&ProtoEvt);
        return;
    }

    /* Only bulk req msg shud be received */
    if (u1MsgType == PBBTE_BULK_REQ_MESSAGE)
    {
        /* Process this Bulk Request Message by sending the bulk updates */
        if (PBBTE_RED_NUM_STANDBY_NODES () == 0)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            PBBTE_RED_BULK_REQ_RECD () = PBBTE_TRUE;
            return;
        }

        PBBTE_RED_BULK_REQ_RECD () = PBBTE_FALSE;

        /* On recieving PBBTE_BULK_REQ_MESSAGE, Bulk updation process
         * should be restarted.
         */
        PBBTE_MEM_SET (&PBBTE_RED_BULK_UPD_NEXT_ESP, PBBTE_INIT_VAL,
                       sizeof (tEspInfo));
        PbbTeRedHandleStandbyUpEvent ();
        return;
    }

    u2OffSet = 0;

    if (PBBTE_NODE_STATE != PBBTE_NODE_STANDBY)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                       PBBTE_TRC_RED_MSG_AT_ACTIVE);
        return;
    }

    while ((u2OffSet + u2MinLen) < u2DataLen)
    {
        PBBTE_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

        PBBTE_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum 
             * number of bytes, which is MessageType + Length. 
             */
            u2OffSet += u2Length;
            continue;
        }

        u2RemMsgLen = u2Length - u2MinLen;

        switch (u1MsgType)
        {
            case PBBTE_DYN_MON_ESP_INFO_MSG:

                if (u2RemMsgLen != (PBBTE_RED_MON_ESP_FIELD_SIZE))
                {
                    /* The Length field in the packet is incorrect, hence 
                     * skipping the message.
                     */
                    PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                                   PBBTE_TRC_RED_DYN_UPD_INC_LEN_FAIL);
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    PbbTeRmHandleProtocolEvent (&ProtoEvt);
                    u2OffSet += u2RemMsgLen;
                    break;
                }

                if ((u2OffSet + u2RemMsgLen) <= u2DataLen)
                {
                    /* room exists completely for this update... */
                    PBBTE_RM_GET_4_BYTE (pMsg, &u2OffSet, EspInfo.u4ContextId);
                    PBBTE_RM_GET_4_BYTE (pMsg, &u2OffSet, EspInfo.u4TeSid);
                    PBBTE_RM_GET_2_BYTE (pMsg, &u2OffSet, EspInfo.EspVlan);
                    PBBTE_RM_GET_N_BYTE (pMsg, EspInfo.DestAddr, &u2OffSet,
                                         sizeof (tMacAddr));
                    PBBTE_RM_GET_N_BYTE (pMsg, EspInfo.SourceAddr, &u2OffSet,
                                         sizeof (tMacAddr));

                    PBBTE_RM_GET_1_BYTE (pMsg, &u2OffSet, u1Action);
                    PBBTE_RM_GET_1_BYTE (pMsg, &u2OffSet, u1NonMonEspFlag);

                    /* Copy the EspInfo to gPbbTeEspPathInfo */
                    gPbbTeEspPathInfo.u4ContextId = EspInfo.u4ContextId;
                    gPbbTeEspPathInfo.u4TeSid = EspInfo.u4TeSid;
                    gPbbTeEspPathInfo.EspVlanId = EspInfo.EspVlan;
                    PBBTE_MEM_CPY (gPbbTeEspPathInfo.DestMac, EspInfo.DestAddr,
                                   sizeof (tMacAddr));
                    PBBTE_MEM_CPY (gPbbTeEspPathInfo.SrcMac, EspInfo.SourceAddr,
                                   sizeof (tMacAddr));

                    /* Apply the Monitored Dynamic ESP by calling the utility 
                     * routine which the API also calls.
                     */
                    if (PbbTeUtlConfigureEsp (&gPbbTeEspPathInfo, u1Action) ==
                        PBBTE_FAILURE)
                    {
                        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                                       PBBTE_TRC_RED_DYN_MSG_FAIL);
                        ProtoEvt.u4Error = RM_PROCESS_FAIL;
                        PbbTeRmHandleProtocolEvent (&ProtoEvt);
                    }

                    if (u1Action == PBBTE_CREATE)
                    {
                        if (PbbTeDMacVIDConfigEspStatus
                            (gPbbTeEspPathInfo.u4ContextId,
                             gPbbTeEspPathInfo.DestMac,
                             gPbbTeEspPathInfo.EspVlanId,
                             u1NonMonEspFlag) == PBBTE_FAILURE)
                        {
                            PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                                           PBBTE_TRC_RED_DYN_MSG_FAIL);
                            ProtoEvt.u4Error = RM_PROCESS_FAIL;
                            PbbTeRmHandleProtocolEvent (&ProtoEvt);
                        }
                    }
                }
                else
                {
                    u2OffSet = u2DataLen;
                }

                break;

            case PBBTE_DYN_NON_MON_ESP_INFO_MSG:

                if (u2RemMsgLen != (PBBTE_RED_NON_MON_ESP_FIELD_SIZE +
                                    PBBTE_RED_ACTION_FIELD_SIZE))
                {
                    /* The Length field in the packet is incorrect, hence 
                     * skipping the message.
                     */
                    PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                                   PBBTE_TRC_RED_DYN_UPD_INC_LEN_FAIL);
                    ProtoEvt.u4Error = RM_PROCESS_FAIL;
                    PbbTeRmHandleProtocolEvent (&ProtoEvt);
                    u2OffSet += u2RemMsgLen;
                    break;
                }

                if ((u2OffSet + u2RemMsgLen) <= u2DataLen)
                {
                    /* room exists completely for this update... */
                    PBBTE_RM_GET_4_BYTE (pMsg, &u2OffSet,
                                         (gPbbTeEspPathInfo.u4ContextId));
                    PBBTE_RM_GET_N_BYTE (pMsg, (gPbbTeEspPathInfo.DestMac),
                                         &u2OffSet, sizeof (tMacAddr));
                    PBBTE_RM_GET_2_BYTE (pMsg, &u2OffSet,
                                         (gPbbTeEspPathInfo.EspVlanId));
                    PBBTE_RM_GET_1_BYTE (pMsg, &u2OffSet, u1Action);

                    /* Apply the Dynamic Non-Monitored ESP by this utility 
                     * routine. 
                     */
                    if (PbbTeDMacVIDConfigEspStatus
                        (gPbbTeEspPathInfo.u4ContextId,
                         gPbbTeEspPathInfo.DestMac, gPbbTeEspPathInfo.EspVlanId,
                         u1Action) == PBBTE_FAILURE)
                    {
                        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                                       PBBTE_TRC_RED_DYN_MSG_FAIL);
                        ProtoEvt.u4Error = RM_PROCESS_FAIL;
                        PbbTeRmHandleProtocolEvent (&ProtoEvt);
                    }
                }
                else
                {
                    u2OffSet = u2DataLen;
                }

                break;

#ifdef NPAPI_WANTED
            case NPSYNC_MESSAGE:

                PbbTeNpSyncProcessSyncMsg (pMsg, &u2OffSet);

                break;
#endif

            default:
                u2OffSet += u2Length;    /* Skip the attribute */
                break;
        }
    }                            /* The End of while loop */

    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedSyncDynamicMonEsp                            */
/*                                                                           */
/* Description        : This routine forms the Dynamic Update for the        */
/*                      Monitored ESPs                                       */
/*                      created through API by external module. It sends the */
/*                      packet to Active RM which sends it to Peer node      */
/*                                                                           */
/* Input(s)           : pEspPathInfo - Pointer ESP Structure to synchronized */
/*                      u1Action    - Creation/deletion                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS/PBBTE_FAILURE                          */
/*****************************************************************************/
INT4
PbbTeRedSyncDynamicMonEsp (tEspPathInfo * pEspPathInfo, UINT1 u1Action)
{
    tEspDMacVIDInfo     EspDMacVIDInfo;
    tRmMsg             *pMsg;
    UINT2               u2Offset = 0;
    UINT2               u2MsgLen = 0;

    if (PBBTE_NODE_STATE != PBBTE_ACTIVE)
    {
        /* Only Active node should send the dynamic sync-up messages */
        return PBBTE_SUCCESS;
    }

    if (PBBTE_RED_NUM_STANDBY_NODES () == 0)
    {
        /* Standby node is not present so no need to send dynamic 
         * sync-up message 
         */
        PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                       PBBTE_TRC_RED_NO_STNDBY);
        return PBBTE_SUCCESS;
    }

    PBBTE_MEM_SET (&EspDMacVIDInfo, PBBTE_INIT_VAL, sizeof (tEspDMacVIDInfo));

    if (!(pEspPathInfo->u4TeSid))
    {
        /* No need to Synchronize anything -- Monitored ESP without TeSid 
         * is not possible -- Nothing to be done for Non-Monitored ESP 
         */
        return PBBTE_SUCCESS;
    }

    PBBTE_MEM_SET (&EspDMacVIDInfo, PBBTE_INIT_VAL, sizeof (tEspDMacVIDInfo));

    /* Dynamic Monitored ESP Update Message Content 
     * 
     * <- 1 byte ->|<- 2 bytes->|<- n bytes  -> |<- 1 byte->|
     * --------------------------------------------------------
     * | Msg. Type |   Length    | ESP Path Info| Action    |
     * |     (1)   |1 + 2 + 1 +  | sizeof       |   (1)     |
     * |           |sizeof(      |(tEspPathInfo)|           |
     * |           |tEspPathInfo)|              |           | 
     * |----------------------------------------------------|
     *
     * The RM Header will be appended by RM.
     *
     */

    u2MsgLen = PBBTE_RED_TYPE_FIELD_SIZE + PBBTE_RED_LEN_FIELD_SIZE +
        PBBTE_RED_MON_ESP_FIELD_SIZE;

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_RED_RM_MEM_FAIL);
        return PBBTE_FAILURE;
    }

    u2Offset = 0;

    PBBTE_RM_PUT_1_BYTE (pMsg, &u2Offset, PBBTE_DYN_MON_ESP_INFO_MSG);
    PBBTE_RM_PUT_2_BYTE (pMsg, &u2Offset, u2MsgLen);
    PBBTE_RM_PUT_4_BYTE (pMsg, &u2Offset, pEspPathInfo->u4ContextId);
    PBBTE_RM_PUT_4_BYTE (pMsg, &u2Offset, pEspPathInfo->u4TeSid);
    PBBTE_RM_PUT_2_BYTE (pMsg, &u2Offset, pEspPathInfo->EspVlanId);
    PBBTE_RM_PUT_N_BYTE (pMsg, pEspPathInfo->DestMac, &u2Offset,
                         sizeof (tMacAddr));
    PBBTE_RM_PUT_N_BYTE (pMsg, pEspPathInfo->SrcMac, &u2Offset,
                         sizeof (tMacAddr));
    PBBTE_RM_PUT_1_BYTE (pMsg, &u2Offset, u1Action);

    /* The Dynamic Update for Non-Monitored ESP is also sent here.
     * */
    PbbTeDMacVIDGetEspInfo (pEspPathInfo->u4ContextId,
                            pEspPathInfo->DestMac,
                            pEspPathInfo->EspVlanId, &EspDMacVIDInfo);

    PBBTE_RM_PUT_1_BYTE (pMsg, &u2Offset,
                         PBBTE_GET_DMAC_VID_INFO_STATUS
                         (EspDMacVIDInfo.u4RefCount));

    PbbTeRedSendMsgToRm (pMsg, u2Offset);

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedSyncDynamicNonMonEsp                         */
/*                                                                           */
/* Description        : This routine forms the Dynamic Update for the        */
/*                      Non-Monitored ESPs                                   */
/*                      created through API by external module. It sends the */
/*                      packet to Active RM which sends it to Peer node      */
/*                                                                           */
/* Input(s)           : pEspPathInfo - Pointer ESP Structure to synchronized */
/*                      u1Action    - Creation/deletion                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS/PBBTE_FAILURE                          */
/*****************************************************************************/
INT4
PbbTeRedSyncDynamicNonMonEsp (UINT4 u4ContextId, tMacAddr DestMac,
                              tVlanId EspVlanId, UINT1 u1Action)
{
    tRmMsg             *pMsg;
    UINT2               u2Offset = 0;
    UINT2               u2MsgLen = 0;

    if (PBBTE_NODE_STATE != PBBTE_ACTIVE)
    {
        /* Only Active node should send the dynamic sync-up messages */
        return PBBTE_SUCCESS;
    }

    if (PBBTE_RED_NUM_STANDBY_NODES () == 0)
    {
        /* Standby node is not present so no need to send dynamic 
         * sync-up message 
         */
        PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                       PBBTE_TRC_RED_NO_STNDBY);
        return PBBTE_SUCCESS;
    }

    /* Synchronize the Non-monitored ESP Bit information */

    /* Dynamic Non-Monitored ESP Update Message Content 
     * 
     * <- 1 byte ->|<- 2 bytes->|<- 8 bytes  -> |<- 1 byte->|
     * --------------------------------------------------------
     * | Msg. Type |   Length    | ESP Path Info| Action    |
     * |     (1)   |1 + 2 + 1 +  |Context + DMac|   (1)     |
     * |           |(4+6+2)      |+ Vid (4+6+2) |           |
     * |----------------------------------------------------|
     *
     * The RM Header will be appended by RM.
     *
     */

    u2MsgLen = PBBTE_RED_TYPE_FIELD_SIZE + PBBTE_RED_LEN_FIELD_SIZE +
        PBBTE_RED_NON_MON_ESP_FIELD_SIZE + PBBTE_RED_ACTION_FIELD_SIZE;

    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgLen)) == NULL)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_RED_RM_MEM_FAIL);
        return PBBTE_FAILURE;
    }

    u2Offset = 0;

    PBBTE_RM_PUT_1_BYTE (pMsg, &u2Offset, PBBTE_DYN_NON_MON_ESP_INFO_MSG);
    PBBTE_RM_PUT_2_BYTE (pMsg, &u2Offset, u2MsgLen);
    PBBTE_RM_PUT_4_BYTE (pMsg, &u2Offset, u4ContextId);
    PBBTE_RM_PUT_N_BYTE (pMsg, (DestMac), &u2Offset, sizeof (tMacAddr));
    PBBTE_RM_PUT_2_BYTE (pMsg, &u2Offset, (EspVlanId));
    PBBTE_RM_PUT_1_BYTE (pMsg, &u2Offset, u1Action);

    PbbTeRedSendMsgToRm (pMsg, u2Offset);

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedSendBulkUpdates                              */
/*                                                                           */
/* Description        : This function sends the Bulk update messages to the  */
/*                      Active RM which will send to Standby RM. Hence the   */
/*                      Bulk update reach the peer PBB-TE.                   */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbTeRedSendBulkUpdates (VOID)
{
    if (PBBTE_IS_MODULE_INITIALISED () != PBBTE_TRUE)
    {
        /* PBB-TE Module is not initialized hence indicate completion of Bulk 
         * update process to RM
         */
        PBBTE_GBL_TRC (ALL_FAILURE_TRC, PBBTE_TRC_MOD_NOT_INIT);

        PbbTeRmSetBulkUpdatesStatus (RM_PBBTE_APP_ID);

        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        PbbTeRedSendBulkUpdTailMsg ();

        return;
    }

    /* Sub Bulk Update for Monitored ESPs */
    PbbTeRedSendEspBulkUpdates ();

    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedSendEspBulkUpdates                           */
/*                                                                           */
/* Description        : This function sends the Bulk update messages to the  */
/*                      Active RM which will send to Standby RM. Hence the   */
/*                      Bulk update reach the peer PBB-TE. Here only the     */
/*                      dynamic bulk update msgs for Monitored ESPs are sent */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbTeRedSendEspBulkUpdates (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tEspInfo            EspInfo;
    tEspDMacVIDInfo     EspDMacVIDInfo;
    tRmMsg             *pMsg = NULL;
    UINT4               u4BulkUpdMonEspCount;
    UINT4               u4TeSid = 0;
    UINT4               u4EspIndex = 0;
    INT4                i4RetVal = PBBTE_FAILURE;
    UINT2               u2BufSize;
    UINT2               u2Offset = 0;
    UINT2               u2EspUpdLen = 0;

    ProtoEvt.u4AppId = RM_PBBTE_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    PBBTE_MEM_SET (&EspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));
    PBBTE_MEM_SET (&EspDMacVIDInfo, PBBTE_INIT_VAL, sizeof (tEspDMacVIDInfo));

    u4BulkUpdMonEspCount = PBBTE_RED_NO_OF_MON_ESPS_PER_SUB_UPDATE;

    u4TeSid = PBBTE_RED_BULK_UPD_NEXT_ESP_INFO.u4TeSid;
    u4EspIndex = PBBTE_RED_BULK_UPD_NEXT_ESP_INFO.u4EspIndex;

    if (PbbTeTeSidEspEntryGetNext (u4TeSid, u4EspIndex, &EspInfo) ==
        PBBTE_SUCCESS)
    {
        do
        {
            if (EspInfo.u1StorageType != PBBTE_STORAGE_VOLATILE)
            {
                /* No need to Sync-up the Static TE-SID entries */
                continue;
            }

            u2EspUpdLen = PBBTE_RED_TYPE_FIELD_SIZE +
                PBBTE_RED_LEN_FIELD_SIZE + PBBTE_RED_MON_ESP_FIELD_SIZE;

            u2BufSize = PBBTE_RED_MAX_MSG_SIZE;

            if (pMsg == NULL)
            {
                if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
                {
                    PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                                   PBBTE_TRC_RED_RM_MEM_FAIL);
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    PbbTeRmHandleProtocolEvent (&ProtoEvt);
                    return;
                }
            }
            /* Dynamic Monitored ESP Update Message Content
             *
             * <- 1 byte ->|<- 2 bytes->|<- n bytes  -> |<- 1 byte->|
             * --------------------------------------------------------
             * | Msg. Type |   Length    | ESP Path Info| Action    |
             * |     (1)   |1 + 2 + 1 +  | sizeof       |   (1)     |
             * |           |sizeof(      |(tEspPathInfo)|           |
             * |           |tEspPathInfo)|              |           |
             * |----------------------------------------------------|
             *
             * The RM Header will be appended by RM.
             *
             */

            if ((u2BufSize - u2Offset) < u2EspUpdLen)
            {
                /* no room for the current message */
                PbbTeRedSendMsgToRm (pMsg, u2Offset);

                pMsg = RM_ALLOC_TX_BUF (u2BufSize);

                if (pMsg == NULL)
                {
                    PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                                   PBBTE_TRC_RED_RM_MEM_FAIL);
                    ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
                    PbbTeRmHandleProtocolEvent (&ProtoEvt);
                    return;
                }

                u2Offset = 0;
            }

            if (PbbTeDMacVIDGetEspInfo (EspInfo.u4ContextId,
                                        EspInfo.DestAddr,
                                        EspInfo.EspVlan,
                                        &EspDMacVIDInfo) == PBBTE_FAILURE)
            {
                continue;
            }

            /* Fill the Packet */
            PBBTE_RM_PUT_1_BYTE (pMsg, &u2Offset, PBBTE_DYN_MON_ESP_INFO_MSG);
            PBBTE_RM_PUT_2_BYTE (pMsg, &u2Offset, u2EspUpdLen);
            PBBTE_RM_PUT_4_BYTE (pMsg, &u2Offset, EspInfo.u4ContextId);
            PBBTE_RM_PUT_4_BYTE (pMsg, &u2Offset, EspInfo.u4TeSid);
            PBBTE_RM_PUT_2_BYTE (pMsg, &u2Offset, EspInfo.EspVlan);
            PBBTE_RM_PUT_N_BYTE (pMsg, EspInfo.DestAddr, &u2Offset,
                                 sizeof (tMacAddr));
            PBBTE_RM_PUT_N_BYTE (pMsg, EspInfo.SourceAddr, &u2Offset,
                                 sizeof (tMacAddr));
            PBBTE_RM_PUT_1_BYTE (pMsg, &u2Offset, PBBTE_CREATE);
            PBBTE_RM_PUT_1_BYTE (pMsg, &u2Offset, PBBTE_GET_DMAC_VID_INFO_STATUS
                                 (EspDMacVIDInfo.u4RefCount));

            u4BulkUpdMonEspCount--;
        }
        while (u4BulkUpdMonEspCount > 0 && ((i4RetVal =
                                             (PbbTeTeSidEspEntryGetNext
                                              ((EspInfo.u4TeSid),
                                               (EspInfo.u4EspIndex),
                                               &EspInfo))) != PBBTE_FAILURE));

        if (u2Offset != 0)
        {
            /* Send the packet to RM */
            if (PbbTeRedSendMsgToRm (pMsg, u2Offset) != PBBTE_SUCCESS)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                PbbTeRmHandleProtocolEvent (&ProtoEvt);
            }
        }
        else if (pMsg)
        {
            /* Empty Buffer created without any update messages filled */
            RM_FREE (pMsg);
        }

        if (i4RetVal != PBBTE_FAILURE && u4BulkUpdMonEspCount == 0)
        {
            PBBTE_RED_BULK_UPD_NEXT_ESP_INFO.u4TeSid = EspInfo.u4TeSid;
            PBBTE_RED_BULK_UPD_NEXT_ESP_INFO.u4EspIndex = EspInfo.u4EspIndex;

            /* Send an event to start the next sun bulk update */
            if (PBBTE_SEND_EVENT (PBBTE_TASK_ID, PBBTE_RED_BULK_UPD_EVENT)
                == OSIX_FAILURE)
            {
                PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                               PBBTE_TRC_SEND_EVENT_FAIL);
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                PbbTeRmHandleProtocolEvent (&ProtoEvt);
            }

            return;
        }
        else if (i4RetVal == PBBTE_FAILURE)
        {
            PBBTE_MEM_SET (&PBBTE_RED_BULK_UPD_NEXT_ESP, PBBTE_INIT_VAL,
                           sizeof (tEspInfo));

        }
    }

    /* PBB-TE completes its Sync-up during standby boot-up, inform RM */
    PbbTeRmSetBulkUpdatesStatus (RM_PBBTE_APP_ID);

    /* Send the tail msg to indicate the completion of Bulk
     * update process.
     */
    PbbTeRedSendBulkUpdTailMsg ();

    return;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedSendBulkUpdTailMsg                           */
/*                                                                           */
/* Description        : This function will send the tail msg to the standy   */
/*                      node, which indicates the completion of Bulk update  */
/*                      process.                                             */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE.                       */
/*****************************************************************************/
INT4
PbbTeRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet;

    ProtoEvt.u4AppId = RM_PBBTE_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><--2 Byte-->
     ***************************************************
     *        *                            *           *
     * RM Hdr * PBBTE_BULK_UPD_TAIL_MESSAGE*Msg Length *
     *        *                            *           *
     ***************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (PBBTE_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_RED_RM_MEM_FAIL);
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        PbbTeRmHandleProtocolEvent (&ProtoEvt);
        return PBBTE_FAILURE;
    }

    u2OffSet = 0;

    PBBTE_RM_PUT_1_BYTE (pMsg, &u2OffSet, PBBTE_BULK_UPD_TAIL_MESSAGE);
    PBBTE_RM_PUT_2_BYTE (pMsg, &u2OffSet, PBBTE_RED_BULK_UPD_TAIL_MSG_SIZE);

    if (PbbTeRedSendMsgToRm (pMsg, u2OffSet) == PBBTE_FAILURE)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        PbbTeRmHandleProtocolEvent (&ProtoEvt);
    }

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeRedSendBulkReqMsg                               */
/*                                                                           */
/* Description        : This function sends bulk request message to the      */
/*                      active node.                                         */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
PbbTeRedSendBulkReqMsg (VOID)
{
    tRmMsg             *pMsg;
    UINT2               u2OffSet;

    if (PBBTE_NODE_STATE != PBBTE_NODE_STANDBY)
    {
        /* Node not in Standby State, so dont send 
         * Bulk request message
         */
        return;
    }

    /*
     *    PBB-TE Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     (7)   |   3        |
     *    |-------------------------
     *
     *
     */

    if ((pMsg = RM_ALLOC_TX_BUF (PBBTE_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        PBBTE_GBL_TRC (PBBTE_RED_TRC | ALL_FAILURE_TRC,
                       PBBTE_TRC_RED_RM_MEM_FAIL);
        return;
    }

    PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                   PBBTE_TRC_RED_SEND_BULK_REQ);

    u2OffSet = 0;

    PBBTE_RM_PUT_1_BYTE (pMsg, &u2OffSet, PBBTE_BULK_REQ_MESSAGE);

    PBBTE_RM_PUT_2_BYTE (pMsg, &u2OffSet, PBBTE_RED_BULK_REQ_MSG_SIZE);

    PbbTeRedSendMsgToRm (pMsg, u2OffSet);

    return;
}

/******************************************************************************/
/*  Function Name   : PbbTeRedSendMsgToRm                                     */
/*                                                                            */
/*  Description     : This function enqueues the update messages to the RM    */
/*                    module.                                                 */
/*                                                                            */
/*  Input(s)        : pMsg  - Message to be sent to RM module                 */
/*                    u2Len - Length of the Message                           */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : PBBTE_SUCESS/PBBTE_FAILURE                              */
/******************************************************************************/
INT4
PbbTeRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Len)
{
    UINT4               u4RetVal;

    u4RetVal = PbbTeRmEnqMsgToRmFromAppl (pMsg, u2Len, RM_PBBTE_APP_ID,
                                          RM_PBBTE_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return PBBTE_FAILURE;
    }

    return PBBTE_SUCCESS;
}

/******************************************************************************/
/*  Function Name   : PbbTeRedHandleBulkUpdateEvent                           */
/*                                                                            */
/* Description      : It Handles the bulk update event. This event is used    */
/*                    to start the next sub bulk update. So                   */
/*                    PbbTeRedSendBulkUpdates is triggered.                   */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbTeRedHandleBulkUpdateEvent (VOID)
{
    if (PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE)
    {
        PbbTeRedSendBulkUpdates ();
    }

    return;
}

#ifdef NPAPI_WANTED
/******************************************************************************/
/*  Function Name   : PbbteHwAuditCreateOrFlushBuffer                         */

/*  Description     : This function performs cehcks whether a buffer entry    */
/*                    is already present or not. If not present then create it*/

/*  Input(s)        : pBuf - Buffer Entry                                     */

/*  Output(s)       : None                                                    */

/*  Global Variables Referred :None                                           */

/*  Global variables Modified :None                                           */

/*  Exceptions or Operating System Error Handling : None                      */

/*  Use of Recursion : None                                                   */

/*  Returns         : None                                                    */

/******************************************************************************/
VOID
PbbteHwAuditCreateOrFlushBuffer (unNpSync * pNpSync, UINT4 u4NpApiId,
                                 UINT4 u4EventId)
{
    tPbbTeBufferEntry  *pBuf = NULL;
    tPbbTeBufferEntry  *pTempBuf = NULL;
    tPbbTeBufferEntry  *pPrevNode = NULL;
    UINT1               u1Match = PBBTE_FALSE;

    /* Scan the Buffer entries and delete it */

    PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                   PBBTE_TRC_RED_AUD_ACC_BUF);

    TMO_DYN_SLL_Scan (&PBBTE_RED_AUD_BUF_TABLE, pBuf, pTempBuf,
                      tPbbTeBufferEntry *)
    {
        if (((u4NpApiId != PBBTE_INIT_VAL) && (u4NpApiId == pBuf->u4NpApiId)) ||
            ((u4EventId != PBBTE_INIT_VAL) && (u4EventId == pBuf->u4EventId)))
        {
            if (MEMCMP (&(pBuf->unNpData), pNpSync, sizeof (unNpSync)) == 0)
            {
                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_AUD_BUF_FND);
                TMO_SLL_Delete (&PBBTE_RED_AUD_BUF_TABLE, &(pBuf->Node));
                PBBTE_RED_AUD_BUF_ENTRY_FREE_MEMBLK (pBuf);
                u1Match = PBBTE_TRUE;
                break;
            }
        }

        pPrevNode = pBuf;
    }

    if (u1Match == PBBTE_FALSE)
    {
        pTempBuf = NULL;

        PBBTE_RED_AUD_BUF_ENTRY_ALLOC_MEMBLK (pTempBuf);
        if (pTempBuf == NULL)
        {
            return;
        }

        PBBTE_MEM_SET (pTempBuf, PBBTE_INIT_VAL, sizeof (tPbbTeBufferEntry));

        PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                       PBBTE_TRC_RED_AUD_BUF_NOT_FND);

        TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pTempBuf);

        MEMCPY (&pTempBuf->unNpData, pNpSync, sizeof (unNpSync));

        pTempBuf->u4NpApiId = u4NpApiId;

        pTempBuf->u4EventId = u4EventId;

        if (pPrevNode == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&PBBTE_RED_AUD_BUF_TABLE, NULL,
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
        else
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&PBBTE_RED_AUD_BUF_TABLE,
                            (tTMO_SLL_NODE *) & (pPrevNode->Node),
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
    }
    return;
}

/******************************************************************************/
/*  Function Name   : PbbTeDoEspVlanHwAudit                                   */
/*                                                                            */
/* Description      : It Handles the Esp Vlan Hardware Audit. It is based on  */
/*                    Deleting ESP Vlan in H/w if not present in S/w and vice */
/*                                                                            */
/*  Input(s)        : u4ContextId, EspVlan -- Context and Esp Vlan            */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbTeDoEspVlanHwAudit (UINT4 u4ContextId, tVlanId EspVlan)
{
    UINT1               u1Result = PBBTE_FALSE;

    PBBTE_IS_ESP_VLAN (u4ContextId, EspVlan, u1Result);

    if (u1Result == PBBTE_FALSE)
    {
        if (FsMiPbbTeHwResetEspVid (u4ContextId, EspVlan) == FNP_FAILURE)
        {
            PBBTE_TRC_ARG1 (u4ContextId,
                            PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_HW_ESP_RESET_FAIL, u4ContextId);
            return;
        }
    }
    else
    {
        if (FsMiPbbTeHwSetEspVid (u4ContextId, EspVlan) == FNP_FAILURE)
        {
            PBBTE_TRC_ARG1 (u4ContextId,
                            PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_HW_ESP_SET_FAIL, u4ContextId);
            return;
        }
    }
}

/******************************************************************************/
/*  Function Name   : PbbTeCreateAuditBufferTable                             */
/*                                                                            */
/* Description      : It handles the Creation of Audit Buffer Table when the  */
/*                    Node becomes Standby                                    */
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/

VOID
PbbTeCreateAuditBufferTable ()
{
    tPbbTeBufferEntry  *pBuf = NULL;
    tPbbTeBufferEntry  *pTempBuf = NULL;

    /* If the SLL is not empty then delete all the buffer entries. 
     */
    if (TMO_SLL_Count (&PBBTE_RED_AUD_BUF_TABLE) != PBBTE_INIT_VAL)
    {
        TMO_DYN_SLL_Scan (&PBBTE_RED_AUD_BUF_TABLE, pBuf, pTempBuf,
                          tPbbTeBufferEntry *)
        {
            TMO_SLL_Delete (&PBBTE_RED_AUD_BUF_TABLE, &(pBuf->Node));
            PBBTE_RED_AUD_BUF_ENTRY_FREE_MEMBLK (pBuf);
            pBuf = pTempBuf;
        }
    }

    TMO_SLL_Init (&(PBBTE_RED_AUD_BUF_TABLE));
    return;
}

/******************************************************************************/
/*  Function Name   : PbbTeDoContextHwAudit                                   */
/*                                                                            */
/* Description      : It Handles the Hardware Audit for a Context.            */
/*                    Deleting ESP Vlan in H/w if not present in S/w and vice */
/*                                                                            */
/*  Input(s)        : u4ContextId -- ContextIdentifier                        */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/
VOID
PbbTeDoContextHwAudit (UINT4 u4ContextId)
{
    tVlanId             EspVlan;

    for (EspVlan = 1; EspVlan <= PBBTE_MAX_VLAN_ID; EspVlan++)
    {
        PbbTeDoEspVlanHwAudit (u4ContextId, EspVlan);
    }

    return;
}

#endif

/******************************************************************************/
/*  Function Name   : PbbTePerformAudit                                       */
/*                                                                            */
/* Description      : Here in this function scan of Buffer entries is done to */
/*                    to do the H/w Audit for them. The Nodes are deleted then*/
/*                                                                            */
/*  Input(s)        : None                                                    */
/*                                                                            */
/*  Output(s)       : None                                                    */
/*                                                                            */
/*  Global Variables Referred :None                                           */
/*                                                                            */
/*  Global variables Modified :None                                           */
/*                                                                            */
/*  Exceptions or Operating System Error Handling : None                      */
/*                                                                            */
/*  Use of Recursion : None                                                   */
/*                                                                            */
/*  Returns         : None                                                    */
/******************************************************************************/

VOID
PbbTePerformAudit ()
{
#ifdef NPAPI_WANTED
    tPbbTeBufferEntry  *pBuf = NULL;
    tPbbTeBufferEntry  *pTempBuf = NULL;

    TMO_DYN_SLL_Scan (&PBBTE_RED_AUD_BUF_TABLE, pBuf, pTempBuf,
                      tPbbTeBufferEntry *)
    {
        switch (pBuf->u4NpApiId)
        {
            case NPSYNC_FS_MI_PBB_TE_HW_SET_ESP_VID:

                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_AUD_FOR_ESP);
                PbbTeDoEspVlanHwAudit (pBuf->unNpData.FsMiPbbTeHwSetEspVid.
                                       u4ContextId,
                                       pBuf->unNpData.FsMiPbbTeHwSetEspVid.
                                       EspVlanId);

                TMO_SLL_Delete (&PBBTE_RED_AUD_BUF_TABLE, &(pBuf->Node));
                PBBTE_RED_AUD_BUF_ENTRY_FREE_MEMBLK (pBuf);
                pBuf = pTempBuf;
                break;

            case NPSYNC_FS_MI_PBB_TE_HW_RESET_ESP_VID:

                PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                               PBBTE_TRC_RED_AUD_FOR_ESP);
                PbbTeDoEspVlanHwAudit (pBuf->unNpData.FsMiPbbTeHwResetEspVid.
                                       u4ContextId,
                                       pBuf->unNpData.FsMiPbbTeHwResetEspVid.
                                       EspVlanId);

                TMO_SLL_Delete (&PBBTE_RED_AUD_BUF_TABLE, &(pBuf->Node));
                PBBTE_RED_AUD_BUF_ENTRY_FREE_MEMBLK (pBuf);
                pBuf = pTempBuf;
                break;

            default:
            {
                switch (pBuf->u4EventId)
                {
                    case PBBTE_CONTEXT_DELETE_SYNCUP:

                        PBBTE_GBL_TRC (PBBTE_RED_TRC | PBBTE_CONTROL_PATH_TRC,
                                       PBBTE_TRC_RED_AUD_FOR_CTX);
                        PbbTeDoContextHwAudit (pBuf->unNpData.ContextIdx.
                                               u4ContextId);
                        break;

                    default:
                        break;
                }
                break;
            }
        }

    }
#endif
    PBBTE_RED_AUDIT_TASK_ID = PBBTE_INIT_VAL;
    return;
}

#endif
