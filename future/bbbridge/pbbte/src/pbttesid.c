/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: pbttesid.c,v 1.11 2014/02/07 13:28:10 siva Exp $                *
 *                                                                           *
 * Description: This file contains routines related to the                   *
 *              TE-SID table in PBB-TE module                                *
 *                                                                           *
 *****************************************************************************/

#include "pbthdrs.h"
extern UINT4        Ieee8021PbbTeTeSidRowStatus[13];

/*****************************************************************************/
/* Function Name      : PbbTeTeSidEspEntryCreate                             */
/*                                                                           */
/* Description        : This function is used to create and initialize a     */
/*                      node in the TESID table using the TESID and ESP index*/
/*                      It will also allocate a unused ESPID optionally.     */
/*                                                                           */
/* Input(s)           : u4TeSid      - TE-SID identifier                     */
/*                      *pu4EspIndex - ESP Index, if 0 then a unused ESPID is*/
/*                                     allocated and used. Computed ESP Id   */
/*                                     will be filled in the pu4EspIndex     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidEspEntryCreate (UINT4 u4TeSid, UINT4 *pu4EspIndex)
{
    tTeSidEspEntry     *pTeSidEspEntry = NULL;

    if (PBBTE_TESID_ENTRY_ALLOC_MEMBLK (pTeSidEspEntry) == NULL)
    {
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC |
                            PBBTE_BUFFER_TRC, PBBTE_TRC_MEM_FAIL_TESID,
                            u4TeSid, *pu4EspIndex);
        return PBBTE_FAILURE;
    }

    PBBTE_MEM_SET (pTeSidEspEntry, PBBTE_INIT_VAL, sizeof (tTeSidEspEntry));

    PBBTE_LOCK ();

    /* If the EspIndex is not provided, we do need to compute the ESP-Index */
    if (*pu4EspIndex == 0)
    {
        if (PbbTeTeSidUtlGetFreeEspIndex (u4TeSid, pu4EspIndex) ==
            PBBTE_FAILURE)
        {
            PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                                PBBTE_TRC_GET_FREE_ESP_FAIL, u4TeSid);
            PBBTE_TESID_ENTRY_FREE_MEMBLK (pTeSidEspEntry);
            PBBTE_UNLOCK ();
            return PBBTE_FAILURE;
        }
    }

    pTeSidEspEntry->u4TeSid = u4TeSid;
    pTeSidEspEntry->u4TeSidEspIndex = *pu4EspIndex;
    pTeSidEspEntry->u4ContextId = PBBTE_INVALID_CONTEXT;
    pTeSidEspEntry->u1StorageType = PBBTE_STORAGE_NONVOLATILE;

    if (RBTreeAdd (gPbbTeGlobalInfo.TeSidTable, pTeSidEspEntry) == RB_FAILURE)
    {
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_RBTREE_ADD_FAIL, u4TeSid,
                            *pu4EspIndex);
        PBBTE_TESID_ENTRY_FREE_MEMBLK (pTeSidEspEntry);
        PBBTE_UNLOCK ();
        return PBBTE_FAILURE;

    }
    PBBTE_UNLOCK ();

    PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC, PBBTE_TRC_TESID_ESP_CRT, u4TeSid,
                        *pu4EspIndex);
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidEspEntryDel                                */
/*                                                                           */
/* Description        : This function is used to delete a node in the        */
/*                      TESID table using the TESID and ESP index. All the   */
/*                      information in the TE-SID entry will be cleaned up   */
/*                      properly and memory will be released.                */
/*                                                                           */
/* Input(s)           : u4TeSid    - TE-SID identifier                       */
/*                      u4EspIndex - ESP Index                               */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidEspEntryDel (UINT4 u4TeSid, UINT4 u4EspIndex)
{
    tTeSidEspEntry      TeSidEspEntry;
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;

    TeSidEspEntry.u4TeSid = u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = u4EspIndex;

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGet (gPbbTeGlobalInfo.TeSidTable,
                                      &TeSidEspEntry);

    if (pRbTreeTeSidEspEntry == NULL)
    {
        PBBTE_UNLOCK ();
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_RBTREE_GET_FAIL, u4TeSid,
                            u4EspIndex);
        return PBBTE_FAILURE;
    }

    /* If ESP(DA,SA,VID) is configured,then unrefer from the DMacVIDInfo node */
    if (pRbTreeTeSidEspEntry->pEspDMacVIDInfo != NULL)
    {
        if (pRbTreeTeSidEspEntry->u4ContextId != PBBTE_INVALID_CONTEXT)
        {
            PBBTE_INCR_NO_OF_DELETED_ESP (pRbTreeTeSidEspEntry->u4ContextId);
            PBBTE_DECR_NO_OF_ACTIVE_ESP (pRbTreeTeSidEspEntry->u4ContextId);
        }
        PbbTeTeSidUtlDMacVIDInfoDel (pRbTreeTeSidEspEntry->u4ContextId,
                                     pRbTreeTeSidEspEntry->pEspDMacVIDInfo);
    }
    if (RBTreeRemove (gPbbTeGlobalInfo.TeSidTable, pRbTreeTeSidEspEntry)
        == RB_FAILURE)
    {
        PBBTE_UNLOCK ();
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_RBTREE_DEL_FAIL,
                            u4TeSid, u4EspIndex);
        return PBBTE_FAILURE;
    }
    PBBTE_TESID_ENTRY_FREE_MEMBLK (pRbTreeTeSidEspEntry);

    PBBTE_UNLOCK ();

    PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC, PBBTE_TRC_TESID_ESP_DEL,
                        u4TeSid, u4EspIndex);

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidEspEntryDelAllInCtxt                       */
/*                                                                           */
/* Description        : This function is used to delete all the TE-SID table */
/*                      entries mapped to a particular context. All the      */
/*                      information in the TE-SID entry will be cleaned up   */
/*                      properly and memory will be released.                */
/*                                                                           */
/* Input(s)           : u4ContextId - Context identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidEspEntryDelAllInCtxt (UINT4 u4ContextId)
{
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;
    tTeSidEspEntry     *pRbTreeNextTeSidEspEntry = NULL;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGetFirst (gPbbTeGlobalInfo.TeSidTable);

    while (pRbTreeTeSidEspEntry != NULL)
    {
        pRbTreeNextTeSidEspEntry =
            (tTeSidEspEntry *) RBTreeGetNext (gPbbTeGlobalInfo.TeSidTable,
                                              pRbTreeTeSidEspEntry, NULL);
        if (pRbTreeTeSidEspEntry->u4ContextId == u4ContextId)
        {
            if (pRbTreeTeSidEspEntry->pEspDMacVIDInfo)
            {
                PbbTeTeSidUtlDMacVIDInfoDel (u4ContextId,
                                             pRbTreeTeSidEspEntry->
                                             pEspDMacVIDInfo);
            }
            RBTreeRemove (gPbbTeGlobalInfo.TeSidTable, pRbTreeTeSidEspEntry);

            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Ieee8021PbbTeTeSidRowStatus,
                                  0, TRUE, NULL, NULL, 2, SNMP_SUCCESS);

            SNMP_MSR_NOTIFY_CFG ((SnmpNotifyInfo, "%u %u %i",
                                  pRbTreeTeSidEspEntry->u4TeSid,
                                  pRbTreeTeSidEspEntry->u4TeSidEspIndex,
                                  PBBTE_DESTROY));
            PBBTE_TESID_ENTRY_FREE_MEMBLK (pRbTreeTeSidEspEntry);
        }
        pRbTreeTeSidEspEntry = pRbTreeNextTeSidEspEntry;
    }
    PBBTE_UNLOCK ();
    PBBTE_TRC (u4ContextId, PBBTE_MGMT_TRC, PBBTE_TRC_TESID_DEL_ALL);
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidEspEntryGet                                */
/*                                                                           */
/* Description        : This function is used to get the TE-SID table's ESP  */
/*                      entry using TeSid & EspIndex. Return ESP entry will  */
/*                      be filled in the EspInfo structure                   */
/*                                                                           */
/* Input(s)           : u4TeSid    - TE-SID identifier                       */
/*                      u4EspIndex - ESP Index                               */
/*                      pEspInfo   - Pointer to the EspInfo structure,       */
/*                                   containing all the TE-SID ESP entry     */
/*                                   information.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidEspEntryGet (UINT4 u4TeSid, UINT4 u4EspIndex, tEspInfo * pEspInfo)
{
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;
    tTeSidEspEntry      TeSidEspEntry;

    TeSidEspEntry.u4TeSid = u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = u4EspIndex;

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGet (gPbbTeGlobalInfo.TeSidTable,
                                      &TeSidEspEntry);
    if (pRbTreeTeSidEspEntry == NULL)
    {
        PBBTE_UNLOCK ();
        return PBBTE_FAILURE;
    }

    /* Copy the information from the TeSidEspEntry structure to EspInfo 
     * structure */
    PbbTeTeSidUtlEspEntryCpy (pRbTreeTeSidEspEntry, pEspInfo);

    PBBTE_UNLOCK ();

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidEspEntryGetNext                            */
/*                                                                           */
/* Description        : This function is used to get the next TE-SID table's */
/*                      ESP entry using TeSid & EspIndex. Return ESP entry'll*/
/*                      be filled in the EspInfo structure                   */
/*                                                                           */
/* Input(s)           : u4TeSid    - TE-SID identifier                       */
/*                      u4EspIndex - ESP Index                               */
/*                      pEspInfo   - Pointer to the EspInfo structure,       */
/*                                   containing all the TE-SID ESP entry     */
/*                                   information.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidEspEntryGetNext (UINT4 u4TeSid, UINT4 u4EspIndex, tEspInfo * pEspInfo)
{
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;
    tTeSidEspEntry      TeSidEspEntry;

    TeSidEspEntry.u4TeSid = u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = u4EspIndex;

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGetNext (gPbbTeGlobalInfo.TeSidTable,
                                          &TeSidEspEntry, NULL);
    if (pRbTreeTeSidEspEntry == NULL)
    {
        PBBTE_UNLOCK ();
        return PBBTE_FAILURE;
    }

    PbbTeTeSidUtlEspEntryCpy (pRbTreeTeSidEspEntry, pEspInfo);

    PBBTE_UNLOCK ();

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidGetESPByTeSid                              */
/*                                                                           */
/* Description        : This function is used to get the TE-SID table's ESP  */
/*                      entry using TeSid, and ESP informations (DA,SA,VID)  */
/*                      Return ESP entry will be filled in the EspInfo       */
/*                                                                           */
/* Input(s)           : u4TeSid    - TE-SID identifier                       */
/*                      u4EspIndex - ESP Index                               */
/*                      pEspInfo   - Pointer to the EspInfo structure,       */
/*                                   containing all the TE-SID ESP entry     */
/*                                   information.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidGetESPByTeSid (UINT4 u4TeSid, tMacAddr DestMac, tMacAddr SrcMac,
                         tVlanId EspVid, tEspInfo * pEspInfo,
                         tEspDMacVIDInfo * pEspDMacVIDInfo)
{
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;
    tTeSidEspEntry      TeSidEspEntry;

    TeSidEspEntry.u4TeSid = u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = 0;    /*Starting from the first ESP index */

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGetNext (gPbbTeGlobalInfo.TeSidTable,
                                          &TeSidEspEntry, NULL);

    while ((pRbTreeTeSidEspEntry != NULL) &&
           (pRbTreeTeSidEspEntry->u4TeSid == u4TeSid))
    {
        if (pRbTreeTeSidEspEntry->pEspDMacVIDInfo)
        {
            if (PBBTE_ARE_MAC_ADDR_EQUAL (SrcMac,
                                          pRbTreeTeSidEspEntry->SourceAddr)
                == PBBTE_TRUE)
            {
                /* Source address is equal, then find DestMac & VlanId in
                 * EspDMacVIDInfo node are also equal */
                if ((PBBTE_ARE_MAC_ADDR_EQUAL (pRbTreeTeSidEspEntry->
                                               pEspDMacVIDInfo->DestAddr,
                                               DestMac) == PBBTE_TRUE)
                    && (pRbTreeTeSidEspEntry->pEspDMacVIDInfo->EspVid ==
                        EspVid))
                {
                    /* All are equal, copy the EspInfo & DMacVIDInfo nodes */
                    PbbTeTeSidUtlEspEntryCpy (pRbTreeTeSidEspEntry, pEspInfo);
                    PBBTE_MEM_CPY (pEspDMacVIDInfo,
                                   pRbTreeTeSidEspEntry->pEspDMacVIDInfo,
                                   sizeof (tEspDMacVIDInfo));
                    PBBTE_UNLOCK ();
                    PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC, PBBTE_TRC_TESID_GET,
                                        u4TeSid);
                    return PBBTE_SUCCESS;
                }
            }
        }
        TeSidEspEntry.u4TeSid = pRbTreeTeSidEspEntry->u4TeSid;
        TeSidEspEntry.u4TeSidEspIndex = pRbTreeTeSidEspEntry->u4TeSidEspIndex;

        pRbTreeTeSidEspEntry =
            (tTeSidEspEntry *) RBTreeGetNext (gPbbTeGlobalInfo.TeSidTable,
                                              &TeSidEspEntry, NULL);
    }
    PBBTE_UNLOCK ();
    return PBBTE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidGetESPInfoByTeSid                          */
/*                                                                           */
/* Description        : This function is used to get the TE-SID table's ESP  */
/*                      entry using TeSid, and SA in ESP info. This is used  */
/*                      to get the Destination address & ESP-VLAN Id.        */
/*                      Return ESP entry will be filled in the EspInfo       */
/*                                                                           */
/* Input(s)           : pEspInfo   - Pointer to the EspInfo structure,       */
/*                                   containing all the TE-SID ESP entry     */
/*                                   information.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidGetESPInfoByTeSid (tEspInfo * pEspInfo)
{
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;
    tTeSidEspEntry      TeSidEspEntry;

    TeSidEspEntry.u4TeSid = pEspInfo->u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = 0;    /* Starting from the first ESP index */

    PBBTE_LOCK ();
    do
    {
        pRbTreeTeSidEspEntry =
            (tTeSidEspEntry *) RBTreeGetNext (gPbbTeGlobalInfo.TeSidTable,
                                              &TeSidEspEntry, NULL);
        if (pRbTreeTeSidEspEntry == NULL)
        {
            break;
        }
        if (PBBTE_ARE_MAC_ADDR_EQUAL (pEspInfo->SourceAddr,
                                      pRbTreeTeSidEspEntry->SourceAddr)
            == PBBTE_TRUE)
        {
            /* Fill the DestMac & VlanId in  EspInfo structure */
            PbbTeTeSidUtlEspEntryCpy (pRbTreeTeSidEspEntry, pEspInfo);
            PBBTE_UNLOCK ();
            PBBTE_GBL_TRC_ARG1 (PBBTE_MGMT_TRC,
                                PBBTE_TRC_TESID_GET_ESP_BY_TESID,
                                pEspInfo->u4TeSid);
            return PBBTE_SUCCESS;
        }
        TeSidEspEntry.u4TeSid = pRbTreeTeSidEspEntry->u4TeSid;
        TeSidEspEntry.u4TeSidEspIndex = pRbTreeTeSidEspEntry->u4TeSidEspIndex;
    }
    while (pRbTreeTeSidEspEntry->u4TeSid == pEspInfo->u4TeSid);

    PBBTE_UNLOCK ();
    return PBBTE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidRowStatusSet                               */
/*                                                                           */
/* Description        : This function is used to set the RowStatus field     */
/*                      in the TE-SID table entry.                           */
/*                                                                           */
/* Input(s)           : u4TeSid    - TE-SID identifier                       */
/*                      u4EspIndex - ESP Index                               */
/*                      u1RowStatus- RowStatus value to set.                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidRowStatusSet (UINT4 u4TeSid, UINT4 u4EspIndex, UINT1 u1RowStatus)
{
    tTeSidEspEntry      TeSidEspEntry;
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;

    TeSidEspEntry.u4TeSid = u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = u4EspIndex;

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGet (gPbbTeGlobalInfo.TeSidTable,
                                      &TeSidEspEntry);

    if (pRbTreeTeSidEspEntry == NULL)
    {
        PBBTE_UNLOCK ();
        return PBBTE_FAILURE;
    }

    pRbTreeTeSidEspEntry->u1RowStatus = u1RowStatus;

    /* Updating the ESP Counters */
    if (u1RowStatus == PBBTE_ACTIVE)
    {
        PBBTE_INCR_NO_OF_CREATED_ESP (pRbTreeTeSidEspEntry->u4ContextId);
        PBBTE_INCR_NO_OF_ACTIVE_ESP (pRbTreeTeSidEspEntry->u4ContextId);
    }

    PBBTE_UNLOCK ();

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidStorageTypeSet                             */
/*                                                                           */
/* Description        : This function is used to set the StorageType field   */
/*                      in the TE-SID table entry.                           */
/*                                                                           */
/* Input(s)           : u4TeSid       - TE-SID identifier                    */
/*                      u4EspIndex    - ESP Index                            */
/*                      u1StorageType - Storage type value to be set.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidStorageTypeSet (UINT4 u4TeSid, UINT4 u4EspIndex, UINT1 u1StorageType)
{
    tTeSidEspEntry      TeSidEspEntry;
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;

    TeSidEspEntry.u4TeSid = u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = u4EspIndex;

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGet (gPbbTeGlobalInfo.TeSidTable,
                                      &TeSidEspEntry);

    if (pRbTreeTeSidEspEntry == NULL)
    {
        PBBTE_UNLOCK ();
        return PBBTE_FAILURE;
    }

    pRbTreeTeSidEspEntry->u1StorageType = u1StorageType;
    PBBTE_UNLOCK ();

    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidEspSet                                     */
/*                                                                           */
/* Description        : This function is used to set the ESP (DA, SA, VID)   */
/*                      field  in the TE-SID table entry.                    */
/*                                                                           */
/* Input(s)           : u4TeSid       - TE-SID identifier                    */
/*                      u4EspIndex    - ESP Index                            */
/*                      DestMac       - Destination MAC address of the ESP   */
/*                      SrcMac        - Source MAC address of the ESP        */
/*                      VlanId        - Vlan identifier of the ESP           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidEspSet (UINT4 u4TeSid, UINT4 u4EspIndex, tMacAddr DestMac,
                  tMacAddr SrcMac, tVlanId VlanId)
{
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;
    tTeSidEspEntry      TeSidEspEntry;
    UINT1               u1NonMonitoredEspPresent = PBBTE_FALSE;

    TeSidEspEntry.u4TeSid = u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = u4EspIndex;

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGet (gPbbTeGlobalInfo.TeSidTable,
                                      &TeSidEspEntry);
    if (pRbTreeTeSidEspEntry == NULL)
    {
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_ESP_SET_FAIL, u4TeSid, u4EspIndex);
        PBBTE_UNLOCK ();
        return PBBTE_FAILURE;
    }

    /* Directly copy the source MAC address */
    PBBTE_CPY_MAC_ADDR (pRbTreeTeSidEspEntry->SourceAddr, SrcMac);

    /* Copy the Destination MAC address & Vlan Id in the EspDMacVIDInfo node */
    if (pRbTreeTeSidEspEntry->u4ContextId != PBBTE_INVALID_CONTEXT)
    {
        /* Verify with VLAN module if the same path (DA, VID) has already been
         * set up for any NonMonitoredESP.
         * If present, NonMonitoredEspPresent bit has to be set
         * in the DMacVIDInfo node */
        PbbTeUtlIsNonMonitoredEspPresent (pRbTreeTeSidEspEntry->u4ContextId,
                                          DestMac, VlanId,
                                          &u1NonMonitoredEspPresent);
    }

    if (pRbTreeTeSidEspEntry->pEspDMacVIDInfo != NULL)
    {
        /* Some other DestMac & VID info is already configured - unrefer from 
         * that DMacVID info, and add in the new DMacVIDInfo node */
        PbbTeTeSidUtlDMacVIDInfoDel (pRbTreeTeSidEspEntry->u4ContextId,
                                     pRbTreeTeSidEspEntry->pEspDMacVIDInfo);
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC, PBBTE_TRC_DMAC_HASH_REM,
                            u4TeSid, u4EspIndex);
    }
    pRbTreeTeSidEspEntry->pEspDMacVIDInfo =
        PbbTeTeSidUtlDMacVIDInfoAdd (pRbTreeTeSidEspEntry->u4ContextId,
                                     DestMac, VlanId);

    if (pRbTreeTeSidEspEntry->pEspDMacVIDInfo == NULL)
    {
        PBBTE_MEM_SET (pRbTreeTeSidEspEntry->SourceAddr, PBBTE_INIT_VAL,
                       ETHERNET_ADDR_SIZE);
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC,
                            PBBTE_TRC_TESID_ESP_SET_FAIL, u4TeSid, u4EspIndex);
        return PBBTE_FAILURE;
    }

    if (u1NonMonitoredEspPresent == PBBTE_TRUE)
    {
        PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC,
                            PBBTE_TRC_NON_MONITORED_CONFIG_TESID_ESP,
                            u4TeSid, u4EspIndex);
        PBBTE_SET_DMAC_VID_INFO_NM_STATUS (pRbTreeTeSidEspEntry->
                                           pEspDMacVIDInfo->u4RefCount,
                                           PBBTE_TRUE);
    }

    PBBTE_UNLOCK ();

    PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC, PBBTE_TRC_TESID_ESP_SET,
                        u4TeSid, u4EspIndex);
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidContextIdSet                               */
/*                                                                           */
/* Description        : This function is used to configure the ContextId     */
/*                      in the TE-SID table entry.                           */
/*                      * If some other contextId is already configured,     */
/*                        configurations will fail.                          */
/*                      * If ESP is configured before setting these field,   */
/*                        DMacVID info will be in GlblEspDMacVIDInfo Hash,   */
/*                        this will be moved to context specific             */
/*                        EspDMacVIDInfo hash.                               */
/*                                                                           */
/* Input(s)           : u4TeSid       - TE-SID identifier                    */
/*                      u4EspIndex    - ESP Index                            */
/*                      u1Status      - Status value to be set.              */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidContextIdSet (UINT4 u4TeSid, UINT4 u4EspIndex, UINT4 u4ContextId)
{
    tTeSidEspEntry      TeSidEspEntry;
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;
    UINT1               u1NonMonitoredEspPresent = PBBTE_FALSE;

    TeSidEspEntry.u4TeSid = u4TeSid;
    TeSidEspEntry.u4TeSidEspIndex = u4EspIndex;

    PBBTE_LOCK ();

    pRbTreeTeSidEspEntry =
        (tTeSidEspEntry *) RBTreeGet (gPbbTeGlobalInfo.TeSidTable,
                                      &TeSidEspEntry);
    if (pRbTreeTeSidEspEntry == NULL)
    {
        PBBTE_UNLOCK ();
        return PBBTE_FAILURE;
    }
    if (pRbTreeTeSidEspEntry->u4ContextId == u4ContextId)
    {
        PBBTE_UNLOCK ();
        return PBBTE_SUCCESS;
    }

    pRbTreeTeSidEspEntry->u4ContextId = u4ContextId;

    /* Until the configuration of ContextID, configured ESP's Destination MAC &
     * VlanID information will be stored in the GlobalDMacVID Hash, now this has
     * to be moved to the newly configured Context's Hash */
    if (pRbTreeTeSidEspEntry->pEspDMacVIDInfo)
    {
        /* Verify with VLAN module if the same path (DA, VID) has already been
         * set up for any NonMonitoredESP.
         * If present, NonMonitoredEspPresent bit has to be set
         * in the DMacVIDInfo node */
        PbbTeUtlIsNonMonitoredEspPresent (u4ContextId,
                                          pRbTreeTeSidEspEntry->
                                          pEspDMacVIDInfo->DestAddr,
                                          pRbTreeTeSidEspEntry->
                                          pEspDMacVIDInfo->EspVid,
                                          &u1NonMonitoredEspPresent);

        pRbTreeTeSidEspEntry->pEspDMacVIDInfo =
            PbbTeTeSidUtlDMacVIDInfoMove (u4ContextId,
                                          pRbTreeTeSidEspEntry->
                                          pEspDMacVIDInfo);
        if (pRbTreeTeSidEspEntry->pEspDMacVIDInfo == NULL)
        {
            PBBTE_UNLOCK ();
            return PBBTE_FAILURE;
        }

        if (u1NonMonitoredEspPresent == PBBTE_TRUE)
        {
            PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC,
                                PBBTE_TRC_NON_MONITORED_CONFIG_TESID_ESP,
                                u4TeSid, u4EspIndex);
            PBBTE_SET_DMAC_VID_INFO_NM_STATUS (pRbTreeTeSidEspEntry->
                                               pEspDMacVIDInfo->u4RefCount,
                                               PBBTE_TRUE);
        }
    }
    PBBTE_UNLOCK ();
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidAnyTeSidEntryInEspVlan                     */
/*                                                                           */
/* Description        : This function is used to find whether any TE-SID     */
/*                      entry is configured in particular ESP-VLAN           */
/*                                                                           */
/* Input(s)           : u4ContextId   - Context Identifier                   */
/*                      VlanId        - ESP Vlan ID                          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_TRUE / PBBTE_FALSE                             */
/*****************************************************************************/
INT4
PbbTeTeSidAnyTeSidEntryInEspVlan (UINT4 u4ContextId, tVlanId VlanId)
{
    tEspDMacVIDInfo    *pEspDMacVIDInfo = NULL;
    UINT4               u4HashIndex = 0;

    PBBTE_LOCK ();

    TMO_HASH_Scan_Table ((PBBTE_CTX_VLAN_HASH_TBL (u4ContextId, VlanId)),
                         u4HashIndex)
    {
        TMO_HASH_Scan_Bucket ((PBBTE_CTX_VLAN_HASH_TBL (u4ContextId, VlanId)),
                              u4HashIndex, pEspDMacVIDInfo, tEspDMacVIDInfo *)
        {
            if (pEspDMacVIDInfo->EspVid == VlanId)
            {
                PBBTE_UNLOCK ();
                return PBBTE_TRUE;
            }
        }
    }

    PBBTE_UNLOCK ();
    return PBBTE_FALSE;
}

/*****************************************************************************/
/* Function Name      : PbbTeDMacVIDConfigEspStatus                          */
/*                                                                           */
/* Description        : This function is used to configure the DMacVID info  */
/*                      status bit. If status bit set, indicates that some   */
/*                      Non monitored ESPs with the same DA & VID are        */
/*                      existing.                                            */
/*                                                                           */
/* Input(s)           : u4ContexID   - Context Identifier                    */
/*                      DestAddr     - Destination Address                   */
/*                      VlanId       - Vlan Identifier                       */
/*                      u1EspStatus  - Status bit, If TRUE indicates that,   */
/*                                     some NonMonitoredESPs with same DA,VID*/
/*                                     is existing in the switch.            */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeDMacVIDConfigEspStatus (UINT4 u4ContextId, tMacAddr DestAddr,
                             tVlanId VlanId, UINT1 u1EspStatus)
{
    tEspDMacVIDInfo    *pEspDMacVIDInfo = NULL;
    UINT4               u4HIndex = 0;

    u4HIndex = PbbTeUtlGetHashIndex (u4ContextId, DestAddr, VlanId);

    PBBTE_LOCK ();

    TMO_HASH_Scan_Bucket ((PBBTE_CTX_VLAN_HASH_TBL (u4ContextId, VlanId)),
                          u4HIndex, pEspDMacVIDInfo, tEspDMacVIDInfo *)
    {
        if ((PBBTE_ARE_MAC_ADDR_EQUAL (DestAddr, pEspDMacVIDInfo->DestAddr)
             == PBBTE_TRUE) && (VlanId == pEspDMacVIDInfo->EspVid))
        {
            PBBTE_SET_DMAC_VID_INFO_NM_STATUS (pEspDMacVIDInfo->u4RefCount,
                                               u1EspStatus);
            PbbTeRedSyncDynamicNonMonEsp (u4ContextId, DestAddr, VlanId,
                                          u1EspStatus);

            PBBTE_UNLOCK ();
            return PBBTE_SUCCESS;
        }
    }

    PBBTE_UNLOCK ();
    return PBBTE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PbbTeDMacVIDGetEspInfo                               */
/*                                                                           */
/* Description        : This function is used to get the DMacVID info        */
/*                      status bit. If status bit set, indicates that some   */
/*                      Non monitored ESPs with the same DA & VID are        */
/*                      existing.                                            */
/*                                                                           */
/* Input(s)           : u4ContexID   - Context Identifier                    */
/*                      DestAddr     - Destination Address                   */
/*                      VlanId       - Vlan Identifier                       */
/*                                                                           */
/* Output(s)          : pRetEspDMacVIDInfo - Pointer to the tEspDMacVIDInfo  */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeDMacVIDGetEspInfo (UINT4 u4ContextId, tMacAddr DestAddr,
                        tVlanId VlanId, tEspDMacVIDInfo * pRetEspDMacVIDInfo)
{
    tEspDMacVIDInfo    *pEspDMacVIDInfo = NULL;
    UINT4               u4HIndex = 0;

    u4HIndex = PbbTeUtlGetHashIndex (u4ContextId, DestAddr, VlanId);

    PBBTE_LOCK ();

    TMO_HASH_Scan_Bucket (PBBTE_CTX_VLAN_HASH_TBL (u4ContextId, VlanId),
                          u4HIndex, pEspDMacVIDInfo, tEspDMacVIDInfo *)
    {
        if ((PBBTE_ARE_MAC_ADDR_EQUAL (DestAddr, pEspDMacVIDInfo->DestAddr)
             == PBBTE_TRUE) && (VlanId == pEspDMacVIDInfo->EspVid))
        {
            PBBTE_MEM_CPY (pRetEspDMacVIDInfo, pEspDMacVIDInfo,
                           sizeof (tEspDMacVIDInfo));
            PBBTE_UNLOCK ();
            return PBBTE_SUCCESS;
        }
    }

    PBBTE_UNLOCK ();

    return PBBTE_FAILURE;
}

/* The below functions are utility functions called from within this file
 * and will not take the PBBTE_LOCK */

/*****************************************************************************/
/* Function Name      : PbbTeTeSidUtlGetFreeEspIndex                         */
/*                                                                           */
/* Description        : This function is used to get the free ESP index,     */
/*                      that's not been used by any other in the particular  */
/*                      TE-SID identifier.                                   */
/*                                                                           */
/* Input(s)           : u4TeSid       - TE-SID identifier                    */
/*                                                                           */
/* Output(s)          : pu4EspIndex   - Pointer to the EspIndex              */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidUtlGetFreeEspIndex (UINT4 u4TeSidId, UINT4 *pu4EspIndex)
{
    tTeSidEspEntry     *pRbTreeTeSidEspEntry = NULL;
    tTeSidEspEntry      TeSidEspEntry;
    UINT4               u4Index = 1;

    TeSidEspEntry.u4TeSid = u4TeSidId;

    /* Searching for the First Free ESP Index in a TE-SID entry */
    for (; u4Index < PBBTE_32BIT_MAX; u4Index++)
    {
        TeSidEspEntry.u4TeSidEspIndex = u4Index;

        pRbTreeTeSidEspEntry =
            (tTeSidEspEntry *) RBTreeGet (gPbbTeGlobalInfo.TeSidTable,
                                          &TeSidEspEntry);
        if (pRbTreeTeSidEspEntry == NULL)
        {
            *pu4EspIndex = u4Index;
            PBBTE_UNLOCK ();
            PBBTE_GBL_TRC_ARG2 (PBBTE_MGMT_TRC, PBTTE_TRC_GET_FREE_ESP,
                                u4TeSidId, u4Index);
            return PBBTE_SUCCESS;
        }
    }
    /* Is all the 2^32 ESP Index are used? :-( */
    return PBBTE_FAILURE;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidUtlEspEntryCpy                             */
/*                                                                           */
/* Description        : This function is used to copy the information in the */
/*                      TE-SID entry to the EspInfo structure.               */
/*                                                                           */
/* Input(s)           : pTeSidEspEntry - Pointer to the TE-SID identifier    */
/*                      pEspInfo       - Pointer to the ESP info structure.  */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
VOID
PbbTeTeSidUtlEspEntryCpy (tTeSidEspEntry * pTeSidEspEntry, tEspInfo * pEspInfo)
{
    PBBTE_MEM_SET (pEspInfo, PBBTE_INIT_VAL, sizeof (tEspInfo));

    pEspInfo->u4TeSid = pTeSidEspEntry->u4TeSid;
    pEspInfo->u4EspIndex = pTeSidEspEntry->u4TeSidEspIndex;
    pEspInfo->u4ContextId = pTeSidEspEntry->u4ContextId;
    pEspInfo->u1StorageType = pTeSidEspEntry->u1StorageType;

    PBBTE_CPY_MAC_ADDR (pEspInfo->SourceAddr, pTeSidEspEntry->SourceAddr);

    if (pTeSidEspEntry->pEspDMacVIDInfo)
    {
        PBBTE_CPY_MAC_ADDR (pEspInfo->DestAddr,
                            pTeSidEspEntry->pEspDMacVIDInfo->DestAddr);
        pEspInfo->EspVlan = pTeSidEspEntry->pEspDMacVIDInfo->EspVid;
    }

    pEspInfo->u1RowStatus = pTeSidEspEntry->u1RowStatus;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidUtlDMacVIDInfoAdd                          */
/*                                                                           */
/* Description        : This function is used to add the DestMacVID info     */
/*                      in the given DMacVID Hash(Global or context specific)*/
/*                      * If the given info is already exists in the Hash,   */
/*                        then count alone will be incremented.              */
/*                      * Ifthe given info not exists in the Hash, then new  */
/*                        node will be created.                              */
/*                                                                           */
/* Input(s)           : pDMacVIDTable -Pointer to the DMacVID Hash (Global or*/
/*                                     or context specific) to which the     */
/*                                     particular info has to be added.      */
/*                      DestAddr     - Destination Address to be added.      */
/*                      VlanId       - Vlan Identifier to be added.          */
/*                      pRetEspDMacVIDInfo - Pointer to the DMacVIDInfo node */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : tEspDMacVIDInfo * - Pointer to the EspDMacVIDInfo    */
/*****************************************************************************/
tEspDMacVIDInfo    *
PbbTeTeSidUtlDMacVIDInfoAdd (UINT4 u4ContextId, tMacAddr DestAddr,
                             tVlanId VlanId)
{
    tEspDMacVIDInfo    *pEspDMacVIDInfo = NULL;
    tTMO_HASH_TABLE    *pDMacVIDTable = NULL;
    UINT4               u4HIndex = 0;

    if (u4ContextId == PBBTE_INVALID_CONTEXT)
    {
        pDMacVIDTable = gPbbTeGlobalInfo.pGlblEspDMacVIDInfo;
    }
    else
    {
        pDMacVIDTable = PBBTE_CTX_VLAN_HASH_TBL (u4ContextId, VlanId);
    }

    u4HIndex = PbbTeUtlGetHashIndex (u4ContextId, DestAddr, VlanId);

    TMO_HASH_Scan_Bucket (pDMacVIDTable, u4HIndex, pEspDMacVIDInfo,
                          tEspDMacVIDInfo *)
    {
        if ((PBBTE_ARE_MAC_ADDR_EQUAL (DestAddr, pEspDMacVIDInfo->DestAddr)
             == PBBTE_TRUE) && (VlanId == pEspDMacVIDInfo->EspVid))
        {
            PBBTE_INCR_DMAC_VID_INFO_CNT (pEspDMacVIDInfo->u4RefCount);
            return pEspDMacVIDInfo;
        }
    }

    if (PBBTE_ESP_DESTMAC_ENTRY_ALLOC_MEMBLK (pEspDMacVIDInfo) == NULL)
    {
        PBBTE_GBL_TRC (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC |
                       PBBTE_BUFFER_TRC, PBBTE_TRC_MEM_FAIL_DMACVIDNODE);
        return NULL;
    }

    PBBTE_MEM_SET (pEspDMacVIDInfo, PBBTE_INIT_VAL, sizeof (tEspDMacVIDInfo));

    PBBTE_CPY_MAC_ADDR (pEspDMacVIDInfo->DestAddr, DestAddr);
    pEspDMacVIDInfo->EspVid = VlanId;
    PBBTE_SET_DMAC_VID_INFO_CNT (pEspDMacVIDInfo->u4RefCount, 1);
    TMO_HASH_Insert_Bucket (pDMacVIDTable, u4HIndex, NULL,
                            &(pEspDMacVIDInfo->NextNode));

    return pEspDMacVIDInfo;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidUtlDMacVIDInfoDel                          */
/*                                                                           */
/* Description        : This function is used to delete the DestMacVID info  */
/*                      in the given DMacVID Hash(Global or context specific)*/
/*                      * If the given info is already exists in the Hash,    */
/*                        then count alone will be decremented.              */
/*                      * If the count in the Info node reaches below 1, then*/
/*                        node will be deleated.                             */
/*                                                                           */
/* Input(s)           : pDMacVIDTable -Pointer to the DMacVID Hash (Global or*/
/*                                     or context specific) to which the     */
/*                                     particular info has to be deleted.    */
/*                      pEspDMacVIDInfo - Pointer to Destination Address &   */
/*                                        Vlan ID information to be deleted. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : PBBTE_SUCCESS / PBBTE_FAILURE                        */
/*****************************************************************************/
INT4
PbbTeTeSidUtlDMacVIDInfoDel (UINT4 u4ContextId,
                             tEspDMacVIDInfo * pEspDMacVIDInfo)
{
    tTMO_HASH_TABLE    *pDMacVIDTable = NULL;
    tPortList          *pEgressIfIndexList = NULL;
    UINT4               u4HIndex = 0;

    if (pEspDMacVIDInfo == NULL)
    {
        return PBBTE_FAILURE;
    }
    if (PBBTE_GET_DMAC_VID_INFO_CNT (pEspDMacVIDInfo->u4RefCount) > 1)
    {
        /* Still more than one TE-SID entries are referring to this DMacVID
         * node, Just decrement the count */
        PBBTE_DECR_DMAC_VID_INFO_CNT (pEspDMacVIDInfo->u4RefCount);
    }
    else
    {
        /* No more ESPs are using this DA, VID. So Delete */

        /* Have to find, where this DMacVIDInfo node will be existing, in global
         * Hash or context specific Hash */

        if (u4ContextId == PBBTE_INVALID_CONTEXT)
        {
            pDMacVIDTable = gPbbTeGlobalInfo.pGlblEspDMacVIDInfo;
        }
        else
        {
            /* Identify the Hash Table for the particular VLAN */
            pDMacVIDTable = PBBTE_CTX_VLAN_HASH_TBL (u4ContextId,
                                                     pEspDMacVIDInfo->EspVid);
        }

        /* Check the Non-Monitored ESP Bit Status before deleting the entry 
         * and dont delete the Vlan entry if so ...
         */

        if ((PBBTE_GET_DMAC_VID_INFO_STATUS (pEspDMacVIDInfo->u4RefCount) ==
             PBBTE_FALSE) && (PBBTE_NODE_STATE == PBBTE_NODE_ACTIVE))
        {
            /* Its the time to delete static Unicast/Multicast entry in VLAN */
            if (PBBTE_IS_MCASTADDR (pEspDMacVIDInfo->DestAddr) == PBBTE_TRUE)
            {
                pEgressIfIndexList =
                    (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

                if (pEgressIfIndexList == NULL)
                {
                    PBBTE_GBL_TRC (PBBTE_MGMT_TRC | PBBTE_ALL_FAILURE_TRC |
                                   PBBTE_BUFFER_TRC,
                                   PBBTE_TRC_MEM_FAIL_BITLIST);
                    return PBBTE_FAILURE;
                }

                PBBTE_MEM_SET (pEgressIfIndexList, PBBTE_INIT_VAL,
                               sizeof (tPortList));

                if (PbbTeSysConfigMcastEntry (u4ContextId,
                                              pEspDMacVIDInfo->DestAddr,
                                              pEspDMacVIDInfo->EspVid,
                                              *pEgressIfIndexList,
                                              VLAN_DELETE) == PBBTE_FAILURE)
                {
                    FsUtilReleaseBitList ((UINT1 *) pEgressIfIndexList);
                    return PBBTE_FAILURE;
                }
                FsUtilReleaseBitList ((UINT1 *) pEgressIfIndexList);
            }
            else
            {
                if (PbbTeSysConfigUcastEntry (u4ContextId,
                                              pEspDMacVIDInfo->DestAddr,
                                              pEspDMacVIDInfo->EspVid,
                                              PBBTE_INIT_VAL,
                                              VLAN_DELETE) == PBBTE_FAILURE)
                {
                    return PBBTE_FAILURE;
                }
            }
        }

        /* Get the Hash Index of the DA, VID Info */
        u4HIndex = PbbTeUtlGetHashIndex (u4ContextId, pEspDMacVIDInfo->DestAddr,
                                         pEspDMacVIDInfo->EspVid);

        TMO_HASH_Delete_Node (pDMacVIDTable, &(pEspDMacVIDInfo->NextNode),
                              u4HIndex);

        PBBTE_ESP_DESTMAC_ENTRY_FREE_MEMBLK (pEspDMacVIDInfo);
    }
    return PBBTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : PbbTeTeSidUtlDMacVIDInfoMove                         */
/*                                                                           */
/* Description        : This function is used to move the DestMacVID info    */
/*                      from the global DMacVID Hash to the context specific */
/*                      Hash.                                                */
/*                                                                           */
/* Input(s)           : u4ContexID   - Context Identifier, to which the      */
/*                                     particular DMacVID info has to be     */
/*                                     moved                                 */
/*                      DestAddr     - Destination Address to be deleted.    */
/*                      VlanId       - Vlan Identifier to be deleted.        */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : tEspDMacVIDInfo * - Pointer to the EspDMacVIDInfo    */
/*****************************************************************************/
tEspDMacVIDInfo    *
PbbTeTeSidUtlDMacVIDInfoMove (UINT4 u4ContextId,
                              tEspDMacVIDInfo * pEspDMacVIDInfo)
{
    tMacAddr            DestAddr;
    tVlanId             VlanId;

    PBBTE_CPY_MAC_ADDR (DestAddr, pEspDMacVIDInfo->DestAddr);
    VlanId = pEspDMacVIDInfo->EspVid;

    /* Delete from Global Hash */

    PbbTeTeSidUtlDMacVIDInfoDel (PBBTE_INVALID_CONTEXT, pEspDMacVIDInfo);

    /* Add newly to the Context specific Hash */

    pEspDMacVIDInfo = PbbTeTeSidUtlDMacVIDInfoAdd (u4ContextId, DestAddr,
                                                   VlanId);

    return pEspDMacVIDInfo;
}
