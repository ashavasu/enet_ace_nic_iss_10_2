/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544trc.h,v 1.3 2017/02/27 13:52:08 siva Exp $
 *
 * Description:   This file contains procedures and definitions
 *                used for debugging in rfc2544 module. 
 ********************************************************************/

#ifndef __R2544TRC_H__
#define __R2544TRC_H__


#define  R2544_MODULE_TRACE(u4ContextId)     \
                  gR2544GlobalInfo.apContextInfo[u4ContextId]->u4TraceOption



#ifdef TRACE_WANTED

#define R2544_MODULE_NAME        ((const char *)"[RFC2544]")

#define   RFC2544_TRC(Flag, CxtId, Fmt ) \
          UtlTrcLog((UINT4) R2544_MODULE_TRACE(CxtId), Flag, R2544_MODULE_NAME, Fmt)

#define   RFC2544_TRC1(Flag, CxtId, Fmt, Arg) \
          UtlTrcLog((UINT4) R2544_MODULE_TRACE(CxtId), Flag, R2544_MODULE_NAME, Fmt, Arg)

#define   RFC2544_TRC2(Flag, CxtId, Fmt, Arg1, Arg2) \
          UtlTrcLog((UINT4) R2544_MODULE_TRACE(CxtId), Flag, R2544_MODULE_NAME, Fmt, Arg1, Arg2)

#define   RFC2544_TRC3(Flag, CxtId, Fmt, Arg1, Arg2, Arg3) \
          UtlTrcLog((UINT4) R2544_MODULE_TRACE(CxtId), Flag, R2544_MODULE_NAME, Fmt, Arg1, Arg2, Arg3)

#define   RFC2544_TRC4(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4) \
          UtlTrcLog((UINT4) R2544_MODULE_TRACE(CxtId), Flag, R2544_MODULE_NAME, Fmt, Arg1, Arg2, Arg3, Arg4)

#define   RFC2544_TRC5(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
          UtlTrcLog((UINT4) R2544_MODULE_TRACE(CxtId), Flag, R2544_MODULE_NAME, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)

#define   RFC2544_TRC6(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
          UtlTrcLog((UINT4) R2544_MODULE_TRACE(CxtId), Flag, R2544_MODULE_NAME, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)


#define   RFC2544_GLOBAL_TRC(Fmt) \
         UtlTrcLog (RFC2544_CRITICAL_TRC, RFC2544_CRITICAL_TRC, R2544_MODULE_NAME, Fmt)

#else

#define   RFC2544_TRC(Flag, CxtId, Fmt)
#define   RFC2544_TRC1(Flag, CxtId, Fmt, Arg)
#define   RFC2544_TRC2(Flag, CxtId, Fmt, Arg1, Arg2)
#define   RFC2544_TRC3(Flag, CxtId, Fmt, Arg1, Arg2, Arg3)
#define   RFC2544_TRC4(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4)
#define   RFC2544_TRC5(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define   RFC2544_TRC6(Flag, CxtId, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define  RFC2544_GLOBAL_TRC(args) do{ }while(0);

#endif /*TRACE_WANTED*/

#endif /*__R2544TRC_H__*/
