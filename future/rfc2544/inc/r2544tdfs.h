/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544tdfs.h,v 1.11 2016/06/14 12:29:05 siva Exp $
 *
 * Description: This file contains type definitions and enumerated data 
 *               structures for rfc2544 module.
 *******************************************************************/

#ifndef __R2544TDFS_H__
#define __R2544TDFS_H__


/* Structure for Context Information */
typedef struct __R2544ContextInfo{
    UINT4                  u4ContextId;
                              /* This variable contains the context Id. */
    UINT4                  u4TraceOption;
                              /* This variable will maintain a bitmap for trace
                               * options. If bit is set, this will represent
                               * option is enabled and if bit is not set then
                               * that particular trace option is disabled. */
    UINT4                  u4NumOfTestRunning;
                              /* This variable indicates the number of test 
                               * currently running in the system.*/
    UINT1                  au1ContextName[VCM_ALIAS_MAX_LEN];
                              /* Context alias name */
    UINT1                  u1ModuleStatus;
                              /* This variable maintains RFC2544 module status of
                               * the context. */
    UINT1                  u1TrapStatus;
                              /* This variable maintains Trap enable/disable
                               * status of the RFC2544 in the context. */
    UINT1                  au1Reserved[2];
}tR2544ContextInfo;


/* Structure for Global Information */

typedef struct __R2544GlobalInfo {
    tOsixTaskId            TaskId; 
                              /* RFC2544 Main Task ID */
    tOsixQId               MsgQId; 
                              /* RFC2544 Message Queue ID */
    tOsixSemId             SemId; 
                              /* RFC2544 Semaphore ID */
    tTimerListId           TmrListId; 
                              /* RFC2544 Timer List to maintain timers */
    tTmrDesc               TmrDesc[RFC2544_MAX_TMR_TYPES]; 
                              /* RFC2544 Timer Description to hold the 
                               * information on timers*/
    tMemPoolId             QMsgPoolId; 
                              /* MemPool Id for RFC2544 message queues */
    tMemPoolId             ContextPoolId;
                              /* MemPool Id for RFC2544 Context Info */          
    tMemPoolId             SlaPoolId; 
                              /* MemPool Id for RFC2544 SLA data structure */
    tMemPoolId             TrafProfPoolId; 
                              /* MemPool Id for RFC2544 Traffic Profile data 
                               * structure */
    tMemPoolId             SacPoolId; 
                              /* MemPool Id for RFC2544 SAC data structure */
    tMemPoolId             SlaReportStatsPoolId; 
                              /* MemPool Id for RFC2544 Report Statistics 
                               * data structure  */
    tMemPoolId             ThroughputStatsPoolId; 
                              /* MemPool Id for RFC2544 throughput test 
                               * statistis data structure  */
    tMemPoolId             LatencyStatsPoolId; 
                              /* MemPool Id for RFC2544 latency test 
                               * statistics data structure  */
    tMemPoolId             FramelossStatsPoolId; 
                              /* MemPool Id for RFC2544 frameloss test 
                               * statistics data structure  */
    tMemPoolId             BackToBackStatsPoolId; 
                              /* MemPool Id for RFC2544 back-to-back test 
                               * statistics data structure  */
    tMemPoolId             R2544ExtInParamsPoolId; 
                              /* MemPool Id for RFC2544 input params
                               * data structure for external interaction */
    tMemPoolId             R2544ExtOutParamsPoolId; 
                              /* MemPool Id for RFC2544 output params
                               * data structure for external interaction */
    tR2544ContextInfo     *apContextInfo[SYS_DEF_MAX_NUM_CONTEXTS];
                              /* This is an array of pointers of context
                               * information . When context create
                               * indication is received by RFC2544 module,
                               * memory will be allocated for the context
                               * and context information will get initialized
                               * with valid info such as NodeId.
                               * When the context delete is received by the
                               * RFC2544 module, this context memory will be
                               * released. */
    tRBTree                SlaTable; 
                              /* RBTree for maintaining SLA information */
    tRBTree                TrafficProfileTable; 
                              /* RBTree for maintaining packet information */
    tRBTree                SacTable; 
                              /* RBTree for maintaining SAC information */
    tRBTree                SlaReportStatsTable; 
                              /* RBTree for maintaining Report Statistics 
                               * information */
    tRBTree                ThroughputStatsTable; 
                              /* RBTree for maintaining throughput test 
                               * statistis information  */
    tRBTree                LatencyStatsTable; 
                              /* RBTree for maintaining latency test 
                               * statistics information */
    tRBTree                FramelossStatsTable; 
                              /* RBtree for maintaining frameloss test 
                               * statistics data structure  */
    tRBTree                BackToBackStatsTable; 
                              /* RBtree for maintaining back-to-back test 
                               * statistics data structure  */
    UINT4                  u4MemFailCount;
                              /* This variable indicates the total number 
                               * of memory failures */
    UINT4                  u4SyslogId;
}tR2544GlobalInfo;



/* Structure for RFC2544 Service Level Agreement */
typedef struct  __R2544SlaTable
{
    tRBNodeEmbd            SlaNode; 
                              /* This contains the RBtree Node pointer 
                               * for SLA table indexed with SLA ID */
    tTmrBlk                SlaTimer;
    tTmrBlk                DwellTimeTimer;
    tR2544SysTime          R2544TestStartTime; 
                              /* This variable  indicates the start time of 
                               * the benchmark test */                                  
    tR2544SysTime          R2544TestEndTime; 
                              /* This variable indicates the end time of 
                               * the benchmark test */                                  
    tMacAddr                TxSrcMacAddr;
                               /* This variable indicates the source mac 
                                * address that is send in the test frames */
    tMacAddr                TxDstMacAddr;
                               /* This variable indicates the destination mac 
                                * address that is send in the test frames */
    UINT4                  u4SlaId; 
                              /* This variable contains RFC2544 Service 
                               * Level Agreement Identifier */
    UINT4                  u4ContextId; 
                              /* This variable contains the context in which 
                               * the SLA is present*/
    UINT4                  u4SlaMeg; 
                              /* This variable contains the MEG Id
                                 mapped to the SLA*/
    UINT4                  u4SlaMe; 
                              /* This variable contains the ME Id 
                               * mapped to the SLA*/
    UINT4                  u4SlaMep; 
                              /* This variable contains the MEP Id
                               * mapped to the SLA*/
    UINT4                  u4SlaTrafProfId; 
                              /* This variable contains the traffic profile Id
                               * mapped to the SLA */
    UINT4                  u4SlaSacId; 
                              /* This variable contains the SAC Id  mapped to 
                               * the SLA*/
    UINT4                  u4IfIndex; 
                              /* This variable indicates the interface on which 
                               * the test frames are to be sent*/
    UINT4                  u4TagType; 
                              /* This variable contains the tag type that is to 
                               * be sent in the test frames */
    UINT4                  u4PortSpeed; 
                              /* This variable contains the port speed.
                               * Test frames are send at this rate */
    UINT4                  u4IfMtu; 
                              /* This variable contains the interface MTU .
                               * Test frames will not be send if the frame size 
                               * is greater than MTU*/
    UINT4                  u4PrevFrameSize; 
                              /* This variable indicates the interface on which 
                               * the test frames are to be sent*/
    UINT4                  u4PrevTrialCount; 
                              /* This variable contains the tag type that is to 
                               * be sent in the test frames */
    UINT4                  u4PrevRate; 
                              /* This variable contains the tag type that is to 
                               * be sent in the test frames */
    tVlanId                OutVlanId;
                              /* This variable contains the vlan ID*
                               * that is send in the test frames */

    UINT1                  u1PrevSubTest; 
                              /* This variable contains the port speed.
                               * Test frames are send at this rate */
    UINT1                  u1SlaTestStatus;
                              /* This varaible indicates the  status (Start/Stop) 
                               * of the SLA test*/
    UINT1                  u1SlaCurrentTestState;
                              /* This varaible indicates the  current state 
                               * (not initiated/inprogress/completed/aborted) of 
                               * the SLA test */
    UINT1                  u1RowStatus; 
                              /* This variable  creats/deletes an entry in the 
                               * SLA table*/
    UINT1                  au1Reserved[2];    
}tSlaEntry;

/* Structure for RFC2544 Traffic Profile  */
typedef struct  __R2544TrafficProfileTable
{
    tRBNodeEmbd            TrafficProfileNode; 
                              /* This contains the RBtree Node pointer 
                               * for traffic profile table indexed with 
                               * traffic profile ID */
    UINT4                  u4TrafficProfileId; 
                              /* This variable contains RFC2544 
                               * Traffic Profile Identifier */
    UINT4                  u4ContextId; 
                              /* This variable contains the context in which 
                               * the traffic profile is present*/
    UINT4                  u4TrafProfDwellTime; 
                              /* This variable contains the number of seconds 
                               * to wait after each trail  for the system to 
                               * settle before reading the statistics from 
                               * hardware */
    UINT4                  u4TrafProfPCP; 
                              /* This variable contains the Priority Code 
                               * Point*/
    UINT4                  u4TrafProfThTrialDuration;
                              /* This variable contains duration of each trails 
                               * in seconds for the Throughput test*/
    UINT4                  u4TrafProfThMinRate;
                              /* This variable contains the maximum rate for 
                               * the Throughput test specified in % of the 
                               * link's bandwidth*/
    UINT4                  u4TrafProfThMaxRate;
                              /* This variable contains the minimum rate for 
                               * the Throughput test specified in % of the 
                               * link's bandwidth*/
    UINT4                  u4TrafProfThRateStep; 
                             /* This variable contains the granularity between 
                              * the minimum and  maximum rates defined for 
                              * Throughput Test */ 
    UINT4                  u4TrafProfThCurrRate;
                              /* This variable contains the current rate  
                               * for the Throughput test*/
    UINT4                  u4TrafProfThCurrTrial;
                              /* This variable contains the current trial 
                               * count for the throughput test*/ 
    UINT4                  u4TrafProfThMaxTrial;
                              /* This variable contains the Maximun trial
                               * count for the throughput test*/
    UINT4                  u4TrafProfLaTrialDuration;
                              /* This variable contains trial duration for
                               * Latency(Delay Measurement)Test in seconds */ 
    UINT4                  u4TrafProfLaDelayMeasureInterval;
                              /* This variable contains the number of seconds 
                               * between each delay measurement */ 
    UINT4                  u4TrafProfLacurrRate;
                              /* This variable contains the curren  rate  
                               * for the Latency test*/
    UINT4                  u4TrafProfLaCurrTrial;
                              /* This variable contains the current trial 
                               * count for the  test*/ 
    UINT4                  u4TrafProfLaCurrRate;
                            
    UINT4                  u4TrafProfFlTrialDuration;
                              /* This variable contains the duration of the 
                               * trail for  Frame Loss Test in seconds */
    UINT4                  u4TrafProfFlMaxRate;
                              /* This variable contains maximum rate for the 
                               * Frame Loss specified in % of the link's 
                               * bandwidth */   
    UINT4                  u4TrafProfFlMinRate;
                              /* This variable contains minimum rate for the 
                               * Frame Loss specified in % of the link's 
                               * bandwidth */   
    UINT4                  u4TrafProfFlRateStep;
                              /* This variable contains the granularity between 
                               * the minimum and  maximum rates defined for 
                               * Frame Loss Test */
    UINT4                  u4TrafProfFlMaxTrial;
                              /* This variable contains the granularity between 
                               * the minimum and  maximum rates defined for 
                               * Frame Loss Test*/
    UINT4                  u4TrafProfFlCurrRate;
                              /* This variable contains the current rate  
                               * for Frame Loss Test */
    UINT4                  u4TrafProfFlCurrTrial;
                              /* This variable contains the current Trial  
                               * count for the Frame Loss Test */
    UINT4                  u4TrafProfBbTrialDuration; 
                              /* This variable contains the trail duration for 
                               * Back-To-Back test  in seconds */
    UINT4                  u4TrafProfBbTrialCount;
                              /* This variable contains number of times the 
                               * Back-To-Back test is to be executed */
    UINT4                  u4TrafProfBbCurrRate;
                              /* This variable contains the current rate  
                               * for Back To Back Test */
    UINT4                  u4TrafProfBbCurrTrial;
                              /* This variable contains the current Trial  
                               * count for the Back To Back  Test */
    UINT4                  u4FlPassCount;
    UINT4                  u4FlPassFrameloss;
    UINT4                  u4FlFailFrameloss;
    UINT4                  au4TrafProfFrameSize[RFC2544_MAX_FRAMESIZE][RFC2544_MIN_FRAMESIZE];
                              /* This array contains the frame sizes used for  
                               * the bench mark test */
    UINT4                  au4TrafProfPrevFrameSize[RFC2544_MAX_FRAMESIZE];
    UINT1                  au1ThroughputCount[RFC2544_TEN]; 
                              /* This is a Binary Array for throughputput test. 
                               * The value in this array is initially set to max trial count 
                               * After each trail it is reset .
                               * This is to avoid posting messages to Y1731 muliple times
                               * for a particular trial*/
    UINT1                  au1LatencyCount[RFC2544_LATENCY_MAX_TRIAL];
                              /* This is a Binary Array for Latency test. 
                               * The value in this array is initially set to max trial count 
                               * After each trail it is reset .
                               * This is to avoid posting messages to Y1731 muliple times
                               * for a particular trial*/
    UINT1                  au1FrameLossCount[RFC2544_TEN]; 
                              /* This is a Binary Array for Frameloss test. 
                               * The value in this array is initially set to max trial count 
                               * After each trail it is reset .
                               * This is to avoid posting messages to Y1731 muliple times
                               * for a particular trial*/
    UINT1                  au1BackToBackCount[RFC2544_HUNDRED];
                              /* This is a Binary Array for Back To Back Test. 
                               * The value in this array is initially set to max trial count 
                               * After each trail it is reset .
                               * This is to avoid posting messages to Y1731 muliple times
                               * for a particular trial*/
    UINT1                  au1TrafProfName[RFC2544_PROFILE_NAME_MAX_LEN]; 
                              /* This variable contains the traffic profile
                                 name*/
    UINT1                  au1TrafProfFrameSize[RFC2544_STRING_MAX_LENGTH]; 
                              /* This variable contains the frame size used for 
                               * each benchmark test */
    UINT1                  u1TrafProfSeqNoCheck; 
                              /* This variable specifies to enable or disable 
                               * the validation of sequence number in received 
                               * frames */
    UINT1                  u1TrafProfThTestStatus;
                              /* This varaible contains throughput test status 
                               * (enable/disable)*/
    UINT1                  u1TrafProfFlTestStatus;
                              /* This varaible contains frameloss test status 
                               * (enable/disable)*/
    UINT1                  u1TrafProfLaTestStatus; 
                              /* This varaible contains latency test status 
                               * (enable/disable)*/
    UINT1                  u1TrafProfBbTestStatus; 
                              /* This varaible contains back-to-back test status 
                               * (enable/disable)*/
    UINT1                  u1RowStatus;
                              /* This variable  creats/deletes an entry in the
                               * traffic profile*/
    UINT1                  au1Reserved[2];
}tTrafficProfile;


/* Structure for RFC2544 Service Acceptance Criteria  */
typedef struct  __R2544SacTable
{
    tRBNodeEmbd            SacNode; 
                              /* This contains the RBtree Node pointer 
                               * for Sac indexed with Context ID and 
                               * SAC ID  */
    UINT4                  u4SacId; 
                              /* This variable contains RFC2544 
                               * SLA Identifier */
    UINT4                  u4ContextId; 
                              /* This variable contains the context in which 
                               * the SAC is present */
    UINT4                  u4SacThAllowedFrameLoss; 
                              /* This variable contains the allowable
                               * frameloss for throughput test */
    UINT4                  u4SacLaAllowedFrameLoss; 
                              /* This variable contains the allowable 
                               * frameloss for latency test*/
    UINT4                  u4SacFlAllowedFrameLoss; 
                              /* This variable contains the allowable  
                               * frameloss for frameloss test */
    UINT1                  u1RowStatus;
                              /* This variable  creats/deletes an entry in the
                                 SAC table*/
    UINT1                  au1Reserved[3];
}tSacEntry;

/* Structure for RFC2544 Throughput test Stats  */
typedef struct  __R2544ThroughputStatsTable
{
    tRBNodeEmbd            ThroughputStatsNode; 
                              /* This contains the RBtree Node pointer 
                               * for ThroughputStats indexed  with Context ID 
                               * SLA ID  and Frame Size*/
    UINT4                  u4ContextId; 
                              /* This variable contains the context in which 
                               * the SLA is present */
    UINT4                  u4SlaId; 
                              /* This variable contains RFC2544 
                               * SLA Identifier */
    UINT4                  u4FrameSize; 
                              /* This variable contains the frame size
                               * with which the test is performed */
    UINT4                  u4AllowedFrameLoss; 
                              /* This variable contains the allowable 
                               * frameloss for throughput test*/
    UINT4                  u4FrameLoss; 
                              /* This variable contains the actual  
                               * frameloss for throughput test */
    UINT4                  u4VerifiedThroughput;
                              /* This variable  contains the verified
                                 Througput rate*/
    UINT4                  u4TrialCount;
    UINT4                  u4TxCount;
    UINT4                  u4RxCount;

}tThroughputStats;


/* Structure for RFC2544 Latency test Stats  */
typedef struct  __R2544LatencyStatsTable
{
    tRBNodeEmbd            LatencyStatsNode; 
                              /* This contains the RBtree Node pointer 
                               * for Latency Stats indexed  with Context ID 
                               * SLA ID and Frame Size */
    UINT4                  u4ContextId; 
                              /* This variable contains the context in which 
                               * the SLA is present */
    UINT4                  u4SlaId; 
                              /* This variable contains RFC2544 
                               * SLA Identifier */
    UINT4                  u4FrameSize; 
                              /* This variable contains the frame size
                               * with which the test is performed */
    UINT4                  u4AllowedFrameLoss; 
                              /* This variable contains the allowable 
                               * frameloss for latency test*/
    UINT4                  u4LatencyMin; 
    UINT4                  u4LatencyMean; 
    UINT4                  u4LatencyMax; 
                              /* This variable contains the actual  
                               * frameloss for latency test */
    UINT4                  u4TrialCount;
    UINT4                  u4TxCount;
    UINT4                  u4RxCount;
}tLatencyStats;

/* Structure for RFC2544 Frame Loss test Stats  */
typedef struct  __R2544FrameLossStatsTable
{
    tRBNodeEmbd            FrameLossStatsNode; 
                              /* This contains the RBtree Node pointer 
                               * for ThroughputStats indexed  with Context ID 
                               * SLA ID and Frame Size */
    UINT4                  u4ContextId; 
                              /* This variable contains the context in which 
                               * the SLA is present */
    UINT4                  u4SlaId; 
                              /* This variable contains RFC2544 
                               * SLA Identifier */
    UINT4                  u4FrameSize; 
                              /* This variable contains the frame size
                               * with which the test is performed */
    UINT4                  u4AllowedFrameLoss; 
                              /* This variable contains the allowable 
                               * frameloss for frame loss test*/
    UINT4                  u4FrameLoss; 
                              /* This variable contains the actual  
                               * frameloss for frame loss test */
    UINT4                  u4TrialCount;
    UINT4                  u4TxCount;
    UINT4                  u4RxCount;

}tFrameLossStats;

/* Structure for RFC2544 Frame Loss test Stats  */
typedef struct  __R2544BackToBackStatsTable
{
    tRBNodeEmbd            BackToBackStatsNode; 
                              /* This contains the RBtree Node pointer 
                               * for ThroughputStats indexed  with Context ID 
                               * SLA ID and Frame Size */
    UINT4                  u4ContextId; 
                              /* This variable contains the context in which 
                               * the SLA is present */
    UINT4                  u4SlaId; 
                              /* This variable contains RFC2544 
                               * SLA Identifier */
    UINT4                  u4FrameSize; 
                              /* This variable contains the frame size
                               * with which the test is performed */
    UINT4                  u4TrialCount;
    UINT4                  u4BurstSize;
}tBackToBackStats;



/* Structure for RFC2544 Report Statistics  */
typedef struct  __R2544ReportStatisticsTable
{
    tRBNodeEmbd            ReportStatisticsNode; 
                              /* This contains the RBtree Node pointer 
                               * for report statistics indexed with 
                               * Context Id, SLA Id and Framesize  */
    UINT4                  u4ReportStatsSlaId; 
                              /* This variable contains RFC2544 
                               * SLA Identifier */
    UINT4                  u4ReportStatsContextId; 
                              /* This variable contains the context in which 
                               * the SLA is present */
    UINT4                  u4ReportStatsFrameSize; 
                              /* This variable contains the frame size
                               * with which the test is performed */
    UINT4                  u4ReportStatsThVerifiedBps; 
                              /* This variable contains the verified 
                               * throughput value */
    UINT4                  u4ReportStatsLatencyMin; 
                              /* This variable contains the maximum delay 
                               * measured */
    UINT4                  u4ReportStatsLatencyMax; 
                              /* This variable contains the minimum delay
                               * measured */
    UINT4                  u4ReportStatsLatencyMean; 
                              /* This variable contains the average delay 
                               * measured */
    UINT4                  u4ReportStatsLatencyFailCount;
                              /* This varaible contains the number of failures 
                               * against each frame */
    UINT4                  u4ReportStatsLaIterationCalculated;
                              /* This varaible contains the number of iterations 
                               * used for calulating the latency */
    UINT4                  u4ReportStatsFrameLossRate; 
                              /* This variable contains the Frame Loss Rate observed 
                               * during the test */
    UINT4                  u4ReportStatsBacktoBackBurstSize;
                              /* This varaible indicates burst size for back-toback 
                               * test*/
    UINT1                  u1ReportStatsThResult; 
                              /* This variable indicates the throughput test 
                               * result*/
    UINT1                  u1ReportStatsLatencyResult; 
                              /* This variable indicates the latency test
                               * result */
    UINT1                  u1ReportStatsFrameLossResult;
                              /* This varaible indicates the frameloss test 
                               * result test*/
    UINT1                  u1ReportStatsBbResult;
                              /* This varaible indicates the Back To Back  test 
                               * result test*/
}tReportStatistics;


/* This structure is the output for the Exit Funtion of RFC2544 */
typedef union __R2544ExtOutParams
{
    UINT4              u4ContextId;
    UINT1              au1ContextName[VCM_ALIAS_MAX_LEN];
    tEcfmRespParams    EcfmRespParams;   
}tR2544ExtOutParams;


/* This Structure is the  input for the Exit Function
 * (Function to interact with the external modules) */
typedef struct __R2544ExtInParams
{
    tR2544ExtReqType       eExtReqType;
                              /* This variable denotes the type of the external
                               * request */
    UINT1             au1Alias[VCM_ALIAS_MAX_LEN];
    UINT4             u4ContextId;
    tEcfmReqParams    EcfmReqParams;
}tR2544ExtInParams;


#endif /* end of __R2544TDFS_H__ */ 

