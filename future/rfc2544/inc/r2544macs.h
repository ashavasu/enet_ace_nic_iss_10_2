/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544macs.h,v 1.7 2015/12/21 13:27:21 siva Exp $
 *
 * Description: This file contains the macro definition for the RFC2544
 *              Module.
 *********************************************************************/
#ifndef __R2544MACS_H__
#define __R2544MACS_H__


/* Macro to get context information */
#define R2544_GET_CONTEXT_INFO(u4ContextId)\
            gR2544GlobalInfo.apContextInfo[u4ContextId]

/* Macro for storing Syslog ID for RFC2544 */
#define RFC2544_SYSLOG_ID         gR2544GlobalInfo.u4SyslogId


#define RFC2544_GLB_TRC()                gR2544GlobalInfo.R2544ContextInfo[[u4ContextId]->u4TraceOption
#define RFC2544_TASK_ID()                gR2544GlobalInfo.TaskId
#define RFC2544_SEM_ID()                 gR2544GlobalInfo.SemId
#define RFC2544_QUEUE_ID()               gR2544GlobalInfo.MsgQId
#define RFC2544_TIMER_LIST()             gR2544GlobalInfo.TmrListId
#define RFC2544_TIMER_DESC()             gR2544GlobalInfo.TmrDesc

#define RFC2544_SLA_TABLE()              gR2544GlobalInfo.SlaTable
#define RFC2544_TRAFFIC_PROFILE_TABLE()  gR2544GlobalInfo.TrafficProfileTable
#define RFC2544_SAC_TABLE()              gR2544GlobalInfo.SacTable
#define RFC2544_REPORT_STATS_TABLE()     gR2544GlobalInfo.SlaReportStatsTable
#define RFC2544_TH_STATS_TABLE()         gR2544GlobalInfo.ThroughputStatsTable
#define RFC2544_LA_STATS_TABLE()         gR2544GlobalInfo.LatencyStatsTable
#define RFC2544_FL_STATS_TABLE()         gR2544GlobalInfo.FramelossStatsTable
#define RFC2544_BB_STATS_TABLE()         gR2544GlobalInfo.BackToBackStatsTable

#define RFC2544_QMSG_POOL()              gR2544GlobalInfo.QMsgPoolId
#define RFC2544_CONTEXT_POOL()           gR2544GlobalInfo.ContextPoolId
#define RFC2544_SLA_POOL()               gR2544GlobalInfo.SlaPoolId
#define RFC2544_TRAFFIC_PROFILE_POOL()   gR2544GlobalInfo.TrafProfPoolId
#define RFC2544_SAC_POOL()               gR2544GlobalInfo.SacPoolId
#define RFC2544_REPORT_STATS_POOL()      gR2544GlobalInfo.SlaReportStatsPoolId
#define RFC2544_TH_STATS_POOL()          gR2544GlobalInfo.ThroughputStatsPoolId
#define RFC2544_LA_STATS_POOL()          gR2544GlobalInfo.LatencyStatsPoolId
#define RFC2544_FL_STATS_POOL()          gR2544GlobalInfo.FramelossStatsPoolId
#define RFC2544_BB_STATS_POOL()          gR2544GlobalInfo.BackToBackStatsPoolId
#define RFC2544_EXTINPARAMS_POOL()       gR2544GlobalInfo.R2544ExtInParamsPoolId
#define RFC2544_EXTOUTPARAMS_POOL()      gR2544GlobalInfo.R2544ExtOutParamsPoolId

#define RFC2544_TRAP_STATUS(u4ContextId) \
                       gR2544GlobalInfo.apContextInfo[u4ContextId]->u1TrapStatus

#define RFC2544_CONTEXT_NAME(u4ContextId) \
                       gR2544GlobalInfo.apContextInfo[u4ContextId]->au1ContextName

#define RFC2544_INITIALISED()  gu1R2544Initialised

/* Definitions of Library functions used*/
#define RFC2544_MEMCPY              MEMCPY
#define RFC2544_MEMCMP              MEMCMP
#define RFC2544_MEMSET              MEMSET
#define RFC2544_STRCMP              STRCMP
#define RFC2544_STRCPY              STRCPY
#define RFC2544_STRTOK              STRTOK
#define RFC2544_STRLEN              STRLEN
#define RFC2544_MALLOC              MEM_MALLOC
#define RFC2544_MEM_FREE            MEM_FREE


/************* FILE Operations Macros ********************/
#define RFC2544_FILE_SAVE(u4SlaId, au1VcAlias, pu1UserCmd) \
        if (u4SlaId != 0)                                                                                                    \
        {                                                                                                                    \
            SPRINTF((CHR1 *)pu1UserCmd, "end;show rfc2544 report sla %d switch %s;", u4SlaId, au1VcAlias);                        \
        }                                                                                                                    \
        else                                                                                                                 \
        {                                                                                                                    \
            SPRINTF((CHR1 *)pu1UserCmd, "end;show rfc2544 report sla switch %s;", au1VcAlias);                        \
        }
#define RFC2544_FILE_TFTP(pu1OutputFile, pu1TftpPath, pu1UserCmd) \
    SPRINTF( (CHR1 *)pu1UserCmd, "end;copy flash:%s %s;", pu1OutputFile, pu1TftpPath);

#define RFC2544_FILE_ERASE(pu1OutputFile, pu1UserCmd) \
    SPRINTF( (CHR1 *)pu1UserCmd, "end;erase flash:%s;", pu1OutputFile);


#endif /* end of __R2544MACS_H__ */
