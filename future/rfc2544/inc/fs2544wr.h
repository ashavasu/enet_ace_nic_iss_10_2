/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs2544wr.h,v 1.3 2016/05/20 10:18:07 siva Exp $
*
* Description: Wrapper functions for Low Level  Routines
*********************************************************************/

#ifndef _FS2544WR_H
#define _FS2544WR_H
INT4 GetNextIndexFs2544ContextTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFS2544(VOID);

VOID UnRegisterFS2544(VOID);
INT4 Fs2544ContextNameGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextTraceOptionGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextNumOfTestRunningGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextTrapStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextTraceOptionSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextTrapStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextTraceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextTrapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544ContextTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFs2544SlaTable(tSnmpIndex *, tSnmpIndex *);
INT4 Fs2544SlaMEGGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaMEGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaMEPGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTrafficProfileIdGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaSacIdGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTestStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaCurrentTestStateGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTestStartTimeGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTestEndTimeGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaMEGSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaMESet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaMEPSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTrafficProfileIdSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaSacIdSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTestStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaMEGTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaMETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaMEPTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTrafficProfileIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaSacIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTestStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SlaTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFs2544TrafficProfileTable(tSnmpIndex *, tSnmpIndex *);
INT4 Fs2544TrafficProfileNameGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileSeqNoCheckGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileDwellTimeGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFrameSizeGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfilePCPGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThTestStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlTestStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaTestStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTestStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThTrialDurationGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThMaxRateGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThMinRateGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaTrialDurationGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaDelayMeasureIntervalGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlTrialDurationGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlMaxRateGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlMinRateGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlRateStepGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTrialDurationGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTrialCountGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThRateStepGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileNameSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileSeqNoCheckSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileDwellTimeSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFrameSizeSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfilePCPSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThTestStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlTestStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaTestStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTestStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThTrialDurationSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThMaxRateSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThMinRateSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaTrialDurationSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaDelayMeasureIntervalSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlTrialDurationSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlMaxRateSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlMinRateSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlRateStepSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTrialDurationSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTrialCountSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThRateStepSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileSeqNoCheckTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileDwellTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFrameSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfilePCPTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThTestStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlTestStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaTestStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTestStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThTrialDurationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThMaxRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThMinRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaTrialDurationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileLaDelayMeasureIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlTrialDurationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlMaxRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlMinRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileFlRateStepTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTrialDurationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileBbTrialCountTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileThRateStepTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544TrafficProfileTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFs2544SacTable(tSnmpIndex *, tSnmpIndex *);
INT4 Fs2544SacThAllowedFrameLossGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SacLaAllowedFrameLossGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SacFlAllowedFrameLossGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SacRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SacThAllowedFrameLossSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SacLaAllowedFrameLossSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SacFlAllowedFrameLossSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SacRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 Fs2544SacThAllowedFrameLossTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SacLaAllowedFrameLossTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SacFlAllowedFrameLossTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SacRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 Fs2544SacTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFs2544ReportStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 Fs2544ReportStatsThVerifiedBpsGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsThResultGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsLatencyMinGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsLatencyMaxGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsLatencyMeanGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsLatencyFailCountGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsLaIterationCalculatedGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsLatencyResultGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsFLossRateGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsFLResultGet(tSnmpIndex *, tRetVal *);
INT4 Fs2544ReportStatsBacktoBackBurstSizeGet(tSnmpIndex *, tRetVal *);
#endif /* _FS2544WR_H */
