
/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544extn.h,v 1.4 2015/11/21 12:13:25 siva Exp $
 *
 * Description: This file contains global variables and global
 *              structures used in  rfc2544  module.
 *******************************************************************/

#ifndef __R2544EXTN_H__
#define __R2544EXTN_H__

extern tR2544GlobalInfo    gR2544GlobalInfo;
extern UINT1               gau1R2544SystemControl[SYS_DEF_MAX_NUM_CONTEXTS]; 
extern UINT1               gu1R2544Initialised;
                    /* This variable maintains ERPS module system status of 
                     * the context */

INT4 CliGetTftpParams (INT1 *pi1TftpStr, INT1 *pi1TftpFileName,
                       tIPvXAddr *pIpAddress, UINT1 *pu1HostName);

INT4 IssCopyFileGeneric (tCliHandle, UINT1 *,
                         UINT4, UINT1 *, UINT1 *, tIPvXAddr, UINT1 *,
                         UINT1 *, UINT4, UINT1 *, UINT1 *, tIPvXAddr, UINT1 *);

INT4 CliSetIssErase (tCliHandle ,UINT4, UINT1 *);

INT2 cli_file_operation ( CONST CHR1 *, CONST CHR1 * );
#endif /* end of __R2544GLOB__ */

