
/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544glob.h,v 1.2 2016/01/21 10:28:12 siva Exp $
 *
 * Description: This file contains global variables and global
 *              structures used in  rfc2544  module.
 *******************************************************************/

#ifndef __R2544GLOB_H__
#define __R2544GLOB_H__

tR2544GlobalInfo    gR2544GlobalInfo;
UINT1               gau1R2544SystemControl[SYS_DEF_MAX_NUM_CONTEXTS]; 
UINT1               gu1R2544Initialised = RFC2544_FALSE;

#endif /* end of __R2544GLOB__ */

