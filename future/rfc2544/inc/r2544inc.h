/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544inc.h,v 1.1 2015/10/16 07:12:53 siva Exp $
 *
 * Description: This file contains the super set  of all header files
 *              rfc2544  module.
 *******************************************************************/
#ifndef __R2544INC_H__
#define __R2544INC_H__

#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "ip.h"
#include "l2iwf.h"
#include "iss.h"
#include "cli.h"
#include "fm.h"
#include "fsvlan.h"
#include "vcm.h"
#include "npapi.h"

#include "dbutil.h"
#include "fssyslog.h"

#include "r2544const.h"
#include "r2544.h"
#include "ecfm.h"
#include "r2544tdfs.h"
#include "r2544macs.h"
#include "r2544sz.h"

#include "r2544prot.h"
#ifdef _R2544MAIN_C_
#include "r2544glob.h"
#else
#include "r2544extn.h"
#endif

#include "fs2544wr.h"
#include "fs2544lw.h"
#include "r2544trc.h"
#include "r2544cli.h"

#endif /*__R2544INC_H__*/


