/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544prot.h,v 1.11 2016/06/14 12:29:05 siva Exp $

 *
 * Description: This file contains function prototypes used in
 *              rfc2544  module.
 *******************************************************************/

#ifndef __R2544PROT_H__
#define __R2544PROT_H__

/************************r2544cxt.c*********************************/
PUBLIC BOOL1 
R2544CxtIsContextExist PROTO ((UINT4 ));

PUBLIC tR2544ContextInfo * 
R2544CxtGetContextEntry PROTO ((UINT4));

PUBLIC tR2544ContextInfo * 
R2544CxtValidateContextInfo PROTO ((UINT4, UINT4 *));

PUBLIC INT4
R2544CxtIsR2544Started PROTO ((UINT4));

PUBLIC INT4
R2544CxtModuleStart PROTO ((UINT4));

PUBLIC INT4
R2544CxtModuleShutdown PROTO ((UINT4));

PUBLIC INT4
R2544CxtModuleEnable PROTO ((tR2544ContextInfo * ));

PUBLIC INT4
R2544CxtModuleDisable PROTO ((tR2544ContextInfo * ));

PUBLIC INT4
R2544CxtCreateContext PROTO ((UINT4));

PUBLIC INT4
R2544CxtDeleteContext PROTO ((UINT4));

PUBLIC INT4
R2544CxtVcmIsSwitchExist PROTO ((UINT1 *, UINT4 *));

/***********************r2544util.c *********************************/
PUBLIC INT4
R2544CreateSlaTable PROTO ((VOID));

PUBLIC INT4
R2544CreateTrafficProfileTable PROTO ((VOID));

PUBLIC INT4
R2544CreateSacTable PROTO ((VOID));

PUBLIC INT4
R2544CreateReportStatsTable PROTO ((VOID));

PUBLIC INT4
R2544CreateThroughputStatsTable PROTO ((VOID));

PUBLIC INT4
R2544CreateLatencyStatsTable PROTO ((VOID));

PUBLIC INT4
R2544CreateFrameLossStatsTable PROTO ((VOID));

PUBLIC INT4
R2544CreateBackToBackStatsTable PROTO ((VOID));

PUBLIC INT4
R2544SlaCmpFn PROTO ((tRBElem * , tRBElem *));

PUBLIC INT4
R2544TrafProfCmpFn PROTO ((tRBElem * , tRBElem *));

PUBLIC INT4
R2544SacCmpFn PROTO ((tRBElem * , tRBElem *));

PUBLIC INT4
R2544ReportStatsCmpFn PROTO ((tRBElem * , tRBElem *));

PUBLIC INT4
R2544ThStatsCmpFn PROTO ((tRBElem * , tRBElem *));

PUBLIC INT4
R2544LaStatsCmpFn PROTO ((tRBElem * , tRBElem *));

PUBLIC INT4
R2544FlStatsCmpFn PROTO ((tRBElem * , tRBElem *));

PUBLIC INT4
R2544BbStatsCmpFn PROTO ((tRBElem * , tRBElem *));

PUBLIC VOID
R2544DeleteSlaTable PROTO ((VOID));

PUBLIC VOID
R2544DeleteTrafficProfileTable PROTO ((VOID));

PUBLIC VOID
R2544DeleteSacTable PROTO ((VOID));

PUBLIC VOID
R2544DeleteReportStatsTable PROTO ((VOID));

PUBLIC VOID
R2544DeleteThroughputStatsTable PROTO ((VOID));

PUBLIC VOID
R2544DeleteLatencyStatsTable PROTO ((VOID));

PUBLIC VOID
R2544DeleteFrameLossStatsTable PROTO ((VOID));

PUBLIC VOID
R2544DeleteBackToBackStatsTable PROTO ((VOID));

PUBLIC VOID
R2544DelAllSlaEntries PROTO ((UINT4));

PUBLIC VOID
R2544DelAllTrafficProfileEntries PROTO ((UINT4));

PUBLIC VOID
R2544DelAllSacEntries PROTO ((UINT4));

PUBLIC VOID
R2544DelAllReportStatsEntries PROTO ((UINT4));

PUBLIC VOID
R2544DelAllThroughputStatsEntries PROTO ((UINT4));

PUBLIC VOID
R2544DelAllLatencyStatsEntries PROTO ((UINT4));

PUBLIC VOID
R2544DelAllFrameLossStatsEntries PROTO ((UINT4));

PUBLIC VOID
R2544DelAllBackToBackStatsEntries PROTO ((UINT4));

PUBLIC INT4
R2544UtilAddSlaEntry PROTO ((UINT4 ,UINT4));

tSlaEntry    *
R2544UtilGetSlaEntry PROTO ((UINT4, UINT4));

PUBLIC INT4
R2544UtilDelSlaEntry PROTO ((tSlaEntry * ));

PUBLIC INT1
R2544UtilValidateSla PROTO ((UINT4 ,UINT4 ,UINT4 *));

PUBLIC INT4
R2544UtilAddTrafProfEntry PROTO ((UINT4 ,UINT4));

PUBLIC INT4
R2544UtilAddReportStatsEntry PROTO ((UINT4 ,UINT4, UINT4 ));


tTrafficProfile    *
R2544UtilGetTrafProfEntry PROTO ((UINT4 ,UINT4));

PUBLIC INT4
R2544UtilDelTrafProfEntry PROTO ((tTrafficProfile * ));

PUBLIC INT1
R2544UtilValidateTrafficProfile PROTO ((UINT4 ,UINT4 ,UINT4 *));

PUBLIC INT4
R2544UtilAddSacEntry PROTO ((UINT4 ,UINT4 ));

tSacEntry    *
R2544UtilGetSacEntry PROTO ((UINT4 ,UINT4));

PUBLIC INT4
R2544UtilDelSacEntry PROTO ((tSacEntry * ));

tReportStatistics   *
R2544UtilGetReportStatsEntry PROTO ((UINT4 ,UINT4 ,UINT4));

PUBLIC INT4
R2544UtilDelReportStats PROTO ((tReportStatistics * ));

PUBLIC INT4 
R2544UtilDelThroughputStats PROTO ((tThroughputStats *));

PUBLIC INT4
R2544UtilDelLatencyStats PROTO ((tLatencyStats *));

PUBLIC INT4
R2544UtilDelFrameLossStats PROTO ((tFrameLossStats *));

PUBLIC INT4
R2544UtilDelBackToBackStats PROTO ((tBackToBackStats *));


PUBLIC INT1
R2544UtilValidateSac PROTO ((UINT4 ,UINT4 ,UINT4 *));

PUBLIC INT1
R2544UtilValidateSlaInformation PROTO ((tSlaEntry * ));

PUBLIC INT1
R2544UtilValidateMepInfo PROTO ((tSlaEntry * ));

PUBLIC INT4
R2544UtilParseValidateFrameSize PROTO ((UINT4 ,UINT4 ,
                  tSNMP_OCTET_STRING_TYPE *));

PUBLIC INT4
R2544UtilAddThroughputStats PROTO ((tR2544ReqParams *));
PUBLIC INT4
R2544UtilAddFrameLossStats PROTO ((tR2544ReqParams *));

PUBLIC INT4
R2544UtilAddLatencyStats PROTO ((tR2544ReqParams *));

PUBLIC INT4
R2544UtilAddBackToBackStats PROTO ((tR2544ReqParams *));

tThroughputStats    *
R2544UtilGetThroughputStats PROTO ((UINT4 , UINT4,
                          UINT4, UINT4));
tLatencyStats    *
R2544UtilGetLatencyStats PROTO ((UINT4, UINT4,
                      UINT4 ,UINT4));

tFrameLossStats    *
R2544UtilGetFrameLossStats PROTO ((UINT4, UINT4,
                          UINT4, UINT4));

tBackToBackStats    *
R2544UtilGetBackToBackStats PROTO ((UINT4, UINT4,
                          UINT4, UINT4));

PUBLIC INT4
R2544UtilParseFrameSize PROTO ((UINT4 ,UINT4, 
                tSNMP_OCTET_STRING_TYPE *));

PUBLIC VOID
R2544UtilUnmapTrafSacFromSla PROTO ((UINT4));

PUBLIC INT4
R2544UtilUpdateTestParams PROTO ((tR2544ReqParams *));

tSlaEntry *
R2544UtilIsTrafProfMappedToSla PROTO ((UINT4, UINT4));

tSlaEntry *
R2544UtilIsSacMappedToSla PROTO ((UINT4, UINT4));

PUBLIC INT1
R2544UtilGetSlaTestParams PROTO ((tSlaEntry *));

PUBLIC INT4
R2544UtilHandleSignalMsg PROTO ((tR2544ReqParams *));

PUBLIC
INT4 R2544UtilValidateSlaParams PROTO ((tSlaEntry *));

PUBLIC
INT4 R2544UtilValidateRate PROTO ((UINT4, UINT4));
/***********************r2544core.c**********************************/
PUBLIC  INT4
R2544CoreStartSlaTest PROTO ((tSlaEntry *));

PUBLIC  INT4
R2544CoreStopSlaTest PROTO ((tSlaEntry *));

PUBLIC  INT4
R2544CoreGenerateThResult PROTO ((UINT4, UINT4, UINT4, UINT4));

PUBLIC  INT4
R2544CoreGenerateLaResult PROTO ((UINT4, UINT4, UINT4));

PUBLIC  INT4
R2544CoreGenerateFlResult PROTO ((UINT4, UINT4, UINT4, UINT4));

PUBLIC  INT4
R2544CoreGenerateBbResult PROTO ((UINT4, UINT4, UINT4));

/**********************r2544tmr.c***********************************/
VOID
R2544TmrExpHandler PROTO ((VOID));

VOID
R2544TmrSlaTimerExp PROTO ((VOID *));

INT4
R2544TmrStartTimer PROTO ((tSlaEntry *, UINT1, UINT4));

INT4
R2544TmrStopTimer PROTO ((tSlaEntry *, UINT1));

VOID 
R2544TmrDwellTimeTimerExp PROTO ((VOID *)); 
  
VOID 
R2544CoreTest PROTO ((tSlaEntry *)); 

VOID
R2544CoreReportRequest PROTO ((VOID *));

/**********************r2544queue.c*********************************/

PUBLIC VOID
R2544MsgQueueHandler PROTO ((VOID));

PUBLIC INT4
R2544QueEnqMsg PROTO ((tR2544ReqParams * ));



/***********************r2544port.c**********************************/
PUBLIC INT4
R2544PortHandleExtInteraction PROTO ((tR2544ExtInParams * ,
                                tR2544ExtOutParams *));
PUBLIC VOID
R2544PortNotifyProtocolShutStatus PROTO ((INT4 ));

PUBLIC VOID 
R2544PortExternalEventNotify PROTO ((tEcfmEventNotification * ));

PUBLIC INT4
R2544PortMepRegAndGetFltState PROTO ((tSlaEntry * ));

PUBLIC INT4
R2544PortMepDeRegister PROTO ((tSlaEntry * ));


/***********************r2544trap.c********************************/
PUBLIC VOID
R2544SendTrapNotifications PROTO ((tSlaEntry  * , UINT1 ));

PUBLIC VOID
R2544PortFmNotifyFaults PROTO ((tSNMP_OID_TYPE *, UINT4 ,
                                UINT4 , tSNMP_VAR_BIND *,
                                UINT1 *, tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT4
R2544MainModuleShutDown PROTO ((VOID));

#endif /* end of __R2544PROT_H__ */
