/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544const.h,v 1.14 2016/05/20 10:18:07 siva Exp $
 *
 * Description: This file contains constants used in  
 *              rfc2544  module.
 *******************************************************************/
#ifndef __R2544CONS_H__
#define __R2544CONS_H__

#define RFC2544_START                   1
#define RFC2544_SHUTDOWN                2

#define RFC2544_PASS                   1
#define RFC2544_FAIL                   2
#define RFC2544_NOTEXECUTED            3

#define RFC2544_START_TEST              1
#define RFC2544_STOP_TEST               2

#define RFC2544_TRUE                    TRUE
#define RFC2544_FALSE                   FALSE

#define RFC2544_ENABLED                 1 
#define RFC2544_DISABLED                2

#define RFC2544_MAX_CONTEXTS            FsRFC2544SizingParams[MAX_RFC2544_CONTEXT_ENTRIES_SIZING_ID].u4PreAllocatedUnits
#define RFC2544_INVALID_CONTEXT_ID (RFC2544_MAX_CONTEXTS + 1)
#define RFC2544_SWITCH_ALIAS_LEN        L2IWF_CONTEXT_ALIAS_LEN

#define RFC2544_DEFAULT_CONTEXT_ID      L2IWF_DEFAULT_CONTEXT
#define RFC2544_INIT_VAL                0   /* Initialization value */
#define RFC2544_ONE                     1   /* Initialization value */
#define RFC2544_TWO                     2
#define RFC2544_THREE                   3
#define RFC2544_FOUR                    4
#define RFC2544_FIVE                    5
#define RFC2544_SIX                     6
#define RFC2544_NINE                    9
#define RFC2544_THIRTEEN                13
#define RFC2544_STRING_MAX_LENGTH       40 

#define RFC2544_MAX_CMD_LEN            200
#define RFC2544_PROFILE_NAME_MAX_LEN    32
#define RFC2544_TASK_NAME              ((UINT1 *)"R2544") 
#define RFC2544_TASK_QUEUE_NAME        ((UINT1 *)"R2544")
#define RFC2544_PROTO_SEM              ((UINT1 *)"R2544")


/* Events of ERPS Main task */
#define RFC2544_TMR_EXPIRY_EVENT             0x01 /* Bit 1 */
#define RFC2544_QMSG_EVENT                   0x02 /* Bit 2 */
#define RFC2544_ALL_EVENTS                   (RFC2544_TMR_EXPIRY_EVENT | \
                                                   RFC2544_QMSG_EVENT)

 /* Timer Types */ 
enum { 
   RFC2544_SLA_TIMER, 
   RFC2544_DWELL_TIME_TIMER, 
   RFC2544_MAX_TMR_TYPES 
}; 



/* SLA Index validation constants */
#define RFC2544_MIN_SLA_ID              1
#define RFC2544_MAX_SLA_ID              10

/* TRAFFIC Profile  Index validation constants */
#define RFC2544_MIN_TRAF_PROF_ID        1 
#define RFC2544_MAX_TRAF_PROF_ID        10

/* SAC index validation constants */
#define RFC2544_MIN_SAC_ID              1 
#define RFC2544_MAX_SAC_ID              10 

/* MEG Index Validation constants*/
#define RFC2544_MEG_MIN_INDEX              1
#define RFC2544_MEG_MAX_INDEX              4294967295
/* ME Index Validation constants*/
#define RFC2544_ME_MIN_INDEX               1
#define RFC2544_ME_MAX_INDEX               4294967295
/* MEP ID Validation constants*/
#define RFC2544_MEP_MIN_INDEX              1
#define RFC2544_MEP_MAX_INDEX              8191

/* Trap Events */
enum
{
    RFC2544_TRANSACTION_FAIL = 1,
    RFC2544_TRAP_BUF_ALLOC_FAIL
};




#define RFC2544_TRAFPROF_MIN_PCP        1
#define RFC2544_TRAFPROF_MAX_PCP        4 
#define RFC2544_TRAFPROF_MIN_DWELL_TIME 5 
#define RFC2544_TRAFPROF_MAX_DWELL_TIME 60
#define RFC2544_TWENTY                  20  
#define RFC2544_NINTY                   90 
#define RFC2544_TH_MIN_TRIALDURATION    10
#define RFC2544_TH_MAX_TRIALDURATION    1800
#define RFC2544_LA_MIN_TRIALDURATION    10
#define RFC2544_LA_MAX_TRIALDURATION    1800
#define RFC2544_LA_MIN_DELAY_INTERVAL   1 
#define RFC2544_LA_MAX_DELAY_INTERVAL   60
#define RFC2544_FL_MIN_TRIALDURATION    10
#define RFC2544_FL_MAX_TRIALDURATION    1800
#define RFC2544_BB_MIN_TRIALCOUNT       1
#define RFC2544_BB_MAX_TRIALCOUNT       100
#define RFC2544_BB_MIN_TRIALDURATION    100
#define RFC2544_BB_MAX_TRIALDURATION    10000
#define RFC2544_MIN_TH_ALLOWED_FRAMELOSS 0
#define RFC2544_MAX_TH_ALLOWED_FRAMELOSS 100
#define RFC2544_MIN_LA_ALLOWED_FRAMELOSS 0
#define RFC2544_MAX_LA_ALLOWED_FRAMELOSS 10
#define RFC2544_MIN_FL_ALLOWED_FRAMELOSS 0
#define RFC2544_MAX_FL_ALLOWED_FRAMELOSS 100



#define RFC2544_MAX_FRAMESIZE    10
#define RFC2544_MIN_FRAMESIZE    5
#define RFC2544_FRAME_64         64
#define RFC2544_FRAME_128        128  
#define RFC2544_FRAME_256        256
#define RFC2544_FRAME_512        512
#define RFC2544_FRAME_768        768
#define RFC2544_FRAME_1024       1024
#define RFC2544_FRAME_1280       1280
#define RFC2544_FRAME_1518       1518 
#define RFC2544_FRAME_2000       2000 
#define RFC2544_FRAME_9600       9600 

/* Default values for traffic profile table */

#define RFC2544_SEQUENCE_NUM_CHECK     RFC2544_DISABLED
#define RFC2544_DWELL_TIME             5
#define RFC2544_PCP                    1
#define RFC2544_THROUGHPUT_STATUS      RFC2544_ENABLED
#define RFC2544_LATENCY_STATUS         RFC2544_ENABLED
#define RFC2544_FRAMELOSS_STATUS       RFC2544_DISABLED
#define RFC2544_BACKTOBACK_STATUS      RFC2544_DISABLED
#define RFC2544_TH_TRIALDURATION       60
#define RFC2544_LA_TRAILDURATION       120
#define RFC2544_LA_DELAY_MEA_INTERVAL  10
#define RFC2544_BB_TRIALDURATION       2000 
#define RFC2544_BB_TRIALCOUNT          50
#define RFC2544_TH_RATESTEP            10
#define RFC2544_FL_RATESTEP            10
#define RFC2544_FL_TRIALDURATION       60
#define RFC2544_MAX_RATE               100
#define RFC2544_MIN_RATE               80
#define RFC2544_DEFAULT_FRAMESIZE     {"64,128,256,512,1024,1280,1518"}

#define RFC2544_CMP_GREATER            1
#define RFC2544_CMP_LESS              -1
#define RFC2544_CMP_EQUAL              0

#define RFC2544_THROUGHPUT_TEST         1
#define RFC2544_LATENCY_TEST            2
#define RFC2544_FRAMELOSS_TEST          3
#define RFC2544_BACKTOBACK_TEST         4

#define RFC2544_SET                     1
#define RFC2544_RESET                   0  

#define RFC2544_LAKH                   100000 
#define RFC2544_THOUSAND               1000
#define RFC2544_HUNDRED                100
#define RFC2544_EIGHTY                 0.8
#define RFC2544_NINTY_PERCENT          0.9
#define RFC2544_BITS_PER_BYTE          8  


#define RFC2544_TEN                    10
#define RFC2544_FIVE                    5
#define RFC2544_INVALID_VALUE          -1

#define RFC2544_LATENCY_MAX_TRIAL      20    

#define RFC2544_OBJECT_NAME_LEN        256
#define RFC2544_OBJECT_VALUE_LEN       128
#define RFC2544_MAX_TRAP_STR_LEN       256
#define RFC2544_TRAP_FAILURE           1

/* To check profile name has numbers within the range  - 
 * 0(ascii 48) to 9(ascii 57) */
#define RFC2544_NUM_CHAR_START 48
#define RFC2544_NUM_CHAR_END   57

/* To check traffic profile name  has upper case 
 * alphabet within it - A(ascii 65) to Z(ascii 90) */
#define RFC2544_UPPER_CHAR_START  65
#define RFC2544_UPPER_CHAR_END    90

/* To check traffic profile name  has lower case 
 * alphabet within it - a(ascii 97) to z(ascii 122) */
#define RFC2544_LOWER_CHAR_START  97
#define RFC2544_LOWER_CHAR_END    122

#endif


