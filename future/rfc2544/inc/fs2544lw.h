/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs2544lw.h,v 1.4 2016/05/20 10:18:07 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for Fs2544ContextTable. */
INT1
nmhValidateIndexInstanceFs2544ContextTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fs2544ContextTable  */

INT1
nmhGetFirstIndexFs2544ContextTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFs2544ContextTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFs2544ContextName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFs2544ContextSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFs2544ContextModuleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFs2544ContextTraceOption ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFs2544ContextNumOfTestRunning ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFs2544ContextTrapStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFs2544ContextSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFs2544ContextModuleStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFs2544ContextTraceOption ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFs2544ContextTrapStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fs2544ContextSystemControl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544ContextModuleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544ContextTraceOption ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544ContextTrapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fs2544ContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fs2544SlaTable. */
INT1
nmhValidateIndexInstanceFs2544SlaTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fs2544SlaTable  */

INT1
nmhGetFirstIndexFs2544SlaTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFs2544SlaTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFs2544SlaMEG ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SlaME ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SlaMEP ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SlaTrafficProfileId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SlaSacId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SlaTestStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544SlaCurrentTestState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544SlaTestStartTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SlaTestEndTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SlaRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFs2544SlaMEG ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544SlaME ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544SlaMEP ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544SlaTrafficProfileId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544SlaSacId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544SlaTestStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFs2544SlaRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fs2544SlaMEG ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544SlaME ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544SlaMEP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544SlaTrafficProfileId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544SlaSacId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544SlaTestStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544SlaRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fs2544SlaTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fs2544TrafficProfileTable. */
INT1
nmhValidateIndexInstanceFs2544TrafficProfileTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fs2544TrafficProfileTable  */

INT1
nmhGetFirstIndexFs2544TrafficProfileTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFs2544TrafficProfileTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFs2544TrafficProfileName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFs2544TrafficProfileSeqNoCheck ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544TrafficProfileDwellTime ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544TrafficProfileFrameSize ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFs2544TrafficProfilePCP ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544TrafficProfileThTestStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544TrafficProfileFlTestStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544TrafficProfileLaTestStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544TrafficProfileBbTestStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544TrafficProfileThTrialDuration ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileThMaxRate ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileThMinRate ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileLaTrialDuration ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileLaDelayMeasureInterval ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileFlTrialDuration ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileFlMaxRate ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileFlMinRate ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileFlRateStep ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileBbTrialDuration ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544TrafficProfileBbTrialCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1 
nmhGetFs2544TrafficProfileThRateStep ARG_LIST((UINT4  , UINT4 ,UINT4 *)); 

INT1
nmhGetFs2544TrafficProfileRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFs2544TrafficProfileName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFs2544TrafficProfileSeqNoCheck ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFs2544TrafficProfileDwellTime ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFs2544TrafficProfileFrameSize ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFs2544TrafficProfilePCP ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFs2544TrafficProfileThTestStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFs2544TrafficProfileFlTestStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFs2544TrafficProfileLaTestStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFs2544TrafficProfileBbTestStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFs2544TrafficProfileThTrialDuration ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileThMaxRate ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileThMinRate ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileLaTrialDuration ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileLaDelayMeasureInterval ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileFlTrialDuration ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileFlMaxRate ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileFlMinRate ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileFlRateStep ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileBbTrialDuration ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544TrafficProfileBbTrialCount ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1 
nmhSetFs2544TrafficProfileThRateStep ARG_LIST((UINT4  , UINT4  ,UINT4 )); 

INT1
nmhSetFs2544TrafficProfileRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fs2544TrafficProfileName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fs2544TrafficProfileSeqNoCheck ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544TrafficProfileDwellTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544TrafficProfileFrameSize ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2Fs2544TrafficProfilePCP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544TrafficProfileThTestStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544TrafficProfileFlTestStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544TrafficProfileLaTestStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544TrafficProfileBbTestStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2Fs2544TrafficProfileThTrialDuration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileThMaxRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileThMinRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileLaTrialDuration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileLaDelayMeasureInterval ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileFlTrialDuration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileFlMaxRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileFlMinRate ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileFlRateStep ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileBbTrialDuration ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544TrafficProfileBbTrialCount ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1 
nmhTestv2Fs2544TrafficProfileThRateStep ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 )); 

INT1
nmhTestv2Fs2544TrafficProfileRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fs2544TrafficProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fs2544SacTable. */
INT1
nmhValidateIndexInstanceFs2544SacTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fs2544SacTable  */

INT1
nmhGetFirstIndexFs2544SacTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFs2544SacTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFs2544SacThAllowedFrameLoss ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SacLaAllowedFrameLoss ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SacFlAllowedFrameLoss ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544SacRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFs2544SacThAllowedFrameLoss ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544SacLaAllowedFrameLoss ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544SacFlAllowedFrameLoss ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFs2544SacRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fs2544SacThAllowedFrameLoss ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544SacLaAllowedFrameLoss ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544SacFlAllowedFrameLoss ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2Fs2544SacRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fs2544SacTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fs2544ReportStatsTable. */
INT1
nmhValidateIndexInstanceFs2544ReportStatsTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for Fs2544ReportStatsTable  */

INT1
nmhGetFirstIndexFs2544ReportStatsTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFs2544ReportStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFs2544ReportStatsThVerifiedBps ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544ReportStatsThResult ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544ReportStatsLatencyMin ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544ReportStatsLatencyMax ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544ReportStatsLatencyMean ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFs2544ReportStatsLatencyFailCount ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544ReportStatsLaIterationCalculated ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544ReportStatsLatencyResult ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544ReportStatsFLossRate ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544ReportStatsFLResult ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFs2544ReportStatsBacktoBackBurstSize ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));
