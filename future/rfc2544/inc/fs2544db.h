/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs2544db.h,v 1.5 2016/05/20 10:18:07 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FS2544DB_H
#define _FS2544DB_H

UINT1 Fs2544ContextTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs2544SlaTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs2544TrafficProfileTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs2544SacTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 Fs2544ReportStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fs2544 [] ={1,3,6,1,4,1,29601,2,105};
tSNMP_OID_TYPE fs2544OID = {9, fs2544};


UINT4 Fs2544ContextId [ ] ={1,3,6,1,4,1,29601,2,105,1,1,1,1};
UINT4 Fs2544ContextName [ ] ={1,3,6,1,4,1,29601,2,105,1,1,1,2};
UINT4 Fs2544ContextSystemControl [ ] ={1,3,6,1,4,1,29601,2,105,1,1,1,3};
UINT4 Fs2544ContextModuleStatus [ ] ={1,3,6,1,4,1,29601,2,105,1,1,1,4};
UINT4 Fs2544ContextTraceOption [ ] ={1,3,6,1,4,1,29601,2,105,1,1,1,5};
UINT4 Fs2544ContextNumOfTestRunning [ ] ={1,3,6,1,4,1,29601,2,105,1,1,1,6};
UINT4 Fs2544ContextTrapStatus [ ] ={1,3,6,1,4,1,29601,2,105,1,1,1,7};
UINT4 Fs2544SlaId [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,1};
UINT4 Fs2544SlaMEG [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,2};
UINT4 Fs2544SlaME [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,3};
UINT4 Fs2544SlaMEP [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,4};
UINT4 Fs2544SlaTrafficProfileId [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,5};
UINT4 Fs2544SlaSacId [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,6};
UINT4 Fs2544SlaTestStatus [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,7};
UINT4 Fs2544SlaCurrentTestState [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,8};
UINT4 Fs2544SlaTestStartTime [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,9};
UINT4 Fs2544SlaTestEndTime [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,10};
UINT4 Fs2544SlaRowStatus [ ] ={1,3,6,1,4,1,29601,2,105,2,1,1,11};
UINT4 Fs2544TrafficProfileId [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,1};
UINT4 Fs2544TrafficProfileName [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,2};
UINT4 Fs2544TrafficProfileSeqNoCheck [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,3};
UINT4 Fs2544TrafficProfileDwellTime [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,4};
UINT4 Fs2544TrafficProfileFrameSize [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,5};
UINT4 Fs2544TrafficProfilePCP [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,6};
UINT4 Fs2544TrafficProfileThTestStatus [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,7};
UINT4 Fs2544TrafficProfileFlTestStatus [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,8};
UINT4 Fs2544TrafficProfileLaTestStatus [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,9};
UINT4 Fs2544TrafficProfileBbTestStatus [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,10};
UINT4 Fs2544TrafficProfileThTrialDuration [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,11};
UINT4 Fs2544TrafficProfileThMaxRate [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,12};
UINT4 Fs2544TrafficProfileThMinRate [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,13};
UINT4 Fs2544TrafficProfileLaTrialDuration [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,14};
UINT4 Fs2544TrafficProfileLaDelayMeasureInterval [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,15};
UINT4 Fs2544TrafficProfileFlTrialDuration [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,16};
UINT4 Fs2544TrafficProfileFlMaxRate [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,17};
UINT4 Fs2544TrafficProfileFlMinRate [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,18};
UINT4 Fs2544TrafficProfileFlRateStep [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,19};
UINT4 Fs2544TrafficProfileBbTrialDuration [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,20};
UINT4 Fs2544TrafficProfileBbTrialCount [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,21};
UINT4 Fs2544TrafficProfileThRateStep [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,22};
UINT4 Fs2544TrafficProfileRowStatus [ ] ={1,3,6,1,4,1,29601,2,105,3,1,1,23};
UINT4 Fs2544SacId [ ] ={1,3,6,1,4,1,29601,2,105,4,1,1,1};
UINT4 Fs2544SacThAllowedFrameLoss [ ] ={1,3,6,1,4,1,29601,2,105,4,1,1,2};
UINT4 Fs2544SacLaAllowedFrameLoss [ ] ={1,3,6,1,4,1,29601,2,105,4,1,1,3};
UINT4 Fs2544SacFlAllowedFrameLoss [ ] ={1,3,6,1,4,1,29601,2,105,4,1,1,4};
UINT4 Fs2544SacRowStatus [ ] ={1,3,6,1,4,1,29601,2,105,4,1,1,5};
UINT4 Fs2544ReportStatsFrameSize [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,1};
UINT4 Fs2544ReportStatsThVerifiedBps [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,2};
UINT4 Fs2544ReportStatsThResult [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,3};
UINT4 Fs2544ReportStatsLatencyMin [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,4};
UINT4 Fs2544ReportStatsLatencyMax [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,5};
UINT4 Fs2544ReportStatsLatencyMean [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,6};
UINT4 Fs2544ReportStatsLatencyFailCount [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,7};
UINT4 Fs2544ReportStatsLaIterationCalculated [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,8};
UINT4 Fs2544ReportStatsLatencyResult [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,9};
UINT4 Fs2544ReportStatsFLossRate [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,10};
UINT4 Fs2544ReportStatsFLResult [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,11};
UINT4 Fs2544ReportStatsBacktoBackBurstSize [ ] ={1,3,6,1,4,1,29601,2,105,5,1,1,12};
UINT4 Fs2544TrapSlaId [ ] ={1,3,6,1,4,1,29601,2,105,6,1,2};
UINT4 Fs2544TypeOfFailure [ ] ={1,3,6,1,4,1,29601,2,105,6,1,3};




tMbDbEntry fs2544MibEntry[]= {

{{13,Fs2544ContextId}, GetNextIndexFs2544ContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs2544ContextTableINDEX, 1, 0, 0, NULL},

{{13,Fs2544ContextName}, GetNextIndexFs2544ContextTable, Fs2544ContextNameGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fs2544ContextTableINDEX, 1, 0, 0, NULL},

{{13,Fs2544ContextSystemControl}, GetNextIndexFs2544ContextTable, Fs2544ContextSystemControlGet, Fs2544ContextSystemControlSet, Fs2544ContextSystemControlTest, Fs2544ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544ContextTableINDEX, 1, 0, 0, "2"},

{{13,Fs2544ContextModuleStatus}, GetNextIndexFs2544ContextTable, Fs2544ContextModuleStatusGet, Fs2544ContextModuleStatusSet, Fs2544ContextModuleStatusTest, Fs2544ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544ContextTableINDEX, 1, 0, 0, "2"},

{{13,Fs2544ContextTraceOption}, GetNextIndexFs2544ContextTable, Fs2544ContextTraceOptionGet, Fs2544ContextTraceOptionSet, Fs2544ContextTraceOptionTest, Fs2544ContextTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544ContextTableINDEX, 1, 0, 0, "32"},

{{13,Fs2544ContextNumOfTestRunning}, GetNextIndexFs2544ContextTable, Fs2544ContextNumOfTestRunningGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs2544ContextTableINDEX, 1, 0, 0, NULL},

{{13,Fs2544ContextTrapStatus}, GetNextIndexFs2544ContextTable, Fs2544ContextTrapStatusGet, Fs2544ContextTrapStatusSet, Fs2544ContextTrapStatusTest, Fs2544ContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544ContextTableINDEX, 1, 0, 0, NULL},

{{13,Fs2544SlaId}, GetNextIndexFs2544SlaTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs2544SlaTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SlaMEG}, GetNextIndexFs2544SlaTable, Fs2544SlaMEGGet, Fs2544SlaMEGSet, Fs2544SlaMEGTest, Fs2544SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544SlaTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SlaME}, GetNextIndexFs2544SlaTable, Fs2544SlaMEGet, Fs2544SlaMESet, Fs2544SlaMETest, Fs2544SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544SlaTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SlaMEP}, GetNextIndexFs2544SlaTable, Fs2544SlaMEPGet, Fs2544SlaMEPSet, Fs2544SlaMEPTest, Fs2544SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544SlaTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SlaTrafficProfileId}, GetNextIndexFs2544SlaTable, Fs2544SlaTrafficProfileIdGet, Fs2544SlaTrafficProfileIdSet, Fs2544SlaTrafficProfileIdTest, Fs2544SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544SlaTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SlaSacId}, GetNextIndexFs2544SlaTable, Fs2544SlaSacIdGet, Fs2544SlaSacIdSet, Fs2544SlaSacIdTest, Fs2544SlaTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544SlaTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SlaTestStatus}, GetNextIndexFs2544SlaTable, Fs2544SlaTestStatusGet, Fs2544SlaTestStatusSet, Fs2544SlaTestStatusTest, Fs2544SlaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544SlaTableINDEX, 2, 0, 0, "2"},

{{13,Fs2544SlaCurrentTestState}, GetNextIndexFs2544SlaTable, Fs2544SlaCurrentTestStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs2544SlaTableINDEX, 2, 0, 0, "1"},

{{13,Fs2544SlaTestStartTime}, GetNextIndexFs2544SlaTable, Fs2544SlaTestStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Fs2544SlaTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SlaTestEndTime}, GetNextIndexFs2544SlaTable, Fs2544SlaTestEndTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, Fs2544SlaTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SlaRowStatus}, GetNextIndexFs2544SlaTable, Fs2544SlaRowStatusGet, Fs2544SlaRowStatusSet, Fs2544SlaRowStatusTest, Fs2544SlaTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544SlaTableINDEX, 2, 0, 1, NULL},

{{13,Fs2544TrafficProfileId}, GetNextIndexFs2544TrafficProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs2544TrafficProfileTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544TrafficProfileName}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileNameGet, Fs2544TrafficProfileNameSet, Fs2544TrafficProfileNameTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544TrafficProfileSeqNoCheck}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileSeqNoCheckGet, Fs2544TrafficProfileSeqNoCheckSet, Fs2544TrafficProfileSeqNoCheckTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "2"},

{{13,Fs2544TrafficProfileDwellTime}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileDwellTimeGet, Fs2544TrafficProfileDwellTimeSet, Fs2544TrafficProfileDwellTimeTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "5"},

{{13,Fs2544TrafficProfileFrameSize}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileFrameSizeGet, Fs2544TrafficProfileFrameSizeSet, Fs2544TrafficProfileFrameSizeTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544TrafficProfilePCP}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfilePCPGet, Fs2544TrafficProfilePCPSet, Fs2544TrafficProfilePCPTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "1"},

{{13,Fs2544TrafficProfileThTestStatus}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileThTestStatusGet, Fs2544TrafficProfileThTestStatusSet, Fs2544TrafficProfileThTestStatusTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "1"},

{{13,Fs2544TrafficProfileFlTestStatus}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileFlTestStatusGet, Fs2544TrafficProfileFlTestStatusSet, Fs2544TrafficProfileFlTestStatusTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "2"},

{{13,Fs2544TrafficProfileLaTestStatus}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileLaTestStatusGet, Fs2544TrafficProfileLaTestStatusSet, Fs2544TrafficProfileLaTestStatusTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "1"},

{{13,Fs2544TrafficProfileBbTestStatus}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileBbTestStatusGet, Fs2544TrafficProfileBbTestStatusSet, Fs2544TrafficProfileBbTestStatusTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "2"},

{{13,Fs2544TrafficProfileThTrialDuration}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileThTrialDurationGet, Fs2544TrafficProfileThTrialDurationSet, Fs2544TrafficProfileThTrialDurationTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "60"},

{{13,Fs2544TrafficProfileThMaxRate}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileThMaxRateGet, Fs2544TrafficProfileThMaxRateSet, Fs2544TrafficProfileThMaxRateTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "100"},

{{13,Fs2544TrafficProfileThMinRate}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileThMinRateGet, Fs2544TrafficProfileThMinRateSet, Fs2544TrafficProfileThMinRateTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "80"},

{{13,Fs2544TrafficProfileLaTrialDuration}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileLaTrialDurationGet, Fs2544TrafficProfileLaTrialDurationSet, Fs2544TrafficProfileLaTrialDurationTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "120"},

{{13,Fs2544TrafficProfileLaDelayMeasureInterval}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileLaDelayMeasureIntervalGet, Fs2544TrafficProfileLaDelayMeasureIntervalSet, Fs2544TrafficProfileLaDelayMeasureIntervalTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "10"},

{{13,Fs2544TrafficProfileFlTrialDuration}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileFlTrialDurationGet, Fs2544TrafficProfileFlTrialDurationSet, Fs2544TrafficProfileFlTrialDurationTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "60"},

{{13,Fs2544TrafficProfileFlMaxRate}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileFlMaxRateGet, Fs2544TrafficProfileFlMaxRateSet, Fs2544TrafficProfileFlMaxRateTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "100"},

{{13,Fs2544TrafficProfileFlMinRate}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileFlMinRateGet, Fs2544TrafficProfileFlMinRateSet, Fs2544TrafficProfileFlMinRateTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "80"},

{{13,Fs2544TrafficProfileFlRateStep}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileFlRateStepGet, Fs2544TrafficProfileFlRateStepSet, Fs2544TrafficProfileFlRateStepTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "10"},

{{13,Fs2544TrafficProfileBbTrialDuration}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileBbTrialDurationGet, Fs2544TrafficProfileBbTrialDurationSet, Fs2544TrafficProfileBbTrialDurationTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "2000"},

{{13,Fs2544TrafficProfileBbTrialCount}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileBbTrialCountGet, Fs2544TrafficProfileBbTrialCountSet, Fs2544TrafficProfileBbTrialCountTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "50"},

{{13,Fs2544TrafficProfileThRateStep}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileThRateStepGet, Fs2544TrafficProfileThRateStepSet, Fs2544TrafficProfileThRateStepTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 0, "10"},

{{13,Fs2544TrafficProfileRowStatus}, GetNextIndexFs2544TrafficProfileTable, Fs2544TrafficProfileRowStatusGet, Fs2544TrafficProfileRowStatusSet, Fs2544TrafficProfileRowStatusTest, Fs2544TrafficProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544TrafficProfileTableINDEX, 2, 0, 1, NULL},

{{13,Fs2544SacId}, GetNextIndexFs2544SacTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs2544SacTableINDEX, 2, 0, 0, NULL},

{{13,Fs2544SacThAllowedFrameLoss}, GetNextIndexFs2544SacTable, Fs2544SacThAllowedFrameLossGet, Fs2544SacThAllowedFrameLossSet, Fs2544SacThAllowedFrameLossTest, Fs2544SacTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544SacTableINDEX, 2, 0, 0, "0"},

{{13,Fs2544SacLaAllowedFrameLoss}, GetNextIndexFs2544SacTable, Fs2544SacLaAllowedFrameLossGet, Fs2544SacLaAllowedFrameLossSet, Fs2544SacLaAllowedFrameLossTest, Fs2544SacTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544SacTableINDEX, 2, 0, 0, "0"},

{{13,Fs2544SacFlAllowedFrameLoss}, GetNextIndexFs2544SacTable, Fs2544SacFlAllowedFrameLossGet, Fs2544SacFlAllowedFrameLossSet, Fs2544SacFlAllowedFrameLossTest, Fs2544SacTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, Fs2544SacTableINDEX, 2, 0, 0, "0"},

{{13,Fs2544SacRowStatus}, GetNextIndexFs2544SacTable, Fs2544SacRowStatusGet, Fs2544SacRowStatusSet, Fs2544SacRowStatusTest, Fs2544SacTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fs2544SacTableINDEX, 2, 0, 1, NULL},

{{13,Fs2544ReportStatsFrameSize}, GetNextIndexFs2544ReportStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsThVerifiedBps}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsThVerifiedBpsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsThResult}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsThResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsLatencyMin}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsLatencyMinGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsLatencyMax}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsLatencyMaxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsLatencyMean}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsLatencyMeanGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsLatencyFailCount}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsLatencyFailCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsLaIterationCalculated}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsLaIterationCalculatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsLatencyResult}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsLatencyResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsFLossRate}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsFLossRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsFLResult}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsFLResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{13,Fs2544ReportStatsBacktoBackBurstSize}, GetNextIndexFs2544ReportStatsTable, Fs2544ReportStatsBacktoBackBurstSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fs2544ReportStatsTableINDEX, 3, 0, 0, NULL},

{{12,Fs2544TrapSlaId}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{12,Fs2544TypeOfFailure}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},
};
tMibData fs2544Entry = { 59, fs2544MibEntry };

#endif /* _FS2544DB_H */

