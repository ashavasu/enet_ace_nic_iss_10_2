
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)
############################################################################
#                         Directories                                      #
############################################################################

RFC2544_BASE_DIR = ${BASE_DIR}/rfc2544
RFC2544_INC_DIR  = ${RFC2544_BASE_DIR}/inc
RFC2544_SRC_DIR  = ${RFC2544_BASE_DIR}/src
RFC2544_OBJ_DIR  = ${RFC2544_BASE_DIR}/obj

RFC2544_TEST_BASE_DIR  = ${BASE_DIR}/rfc2544/test
RFC2544_TEST_OBJ_DIR   = ${RFC2544_TEST_BASE_DIR}/obj



############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${RFC2544_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################

