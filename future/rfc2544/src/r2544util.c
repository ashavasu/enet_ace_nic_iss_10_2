/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544util.c,v 1.22 2016/06/14 12:29:05 siva Exp $
 *
 * Description: This file contains utility function for RFC2544 module
 *
 *******************************************************************/
#include "r2544inc.h"
/*************************************************************************
*                                                                        *
* Function Name       : R2544CreateSlaTable                              *
*                                                                        *
* Description         : This function will create RBTree for SlaTable.   *
*                                                                        *
* Input(s)            : None                                             *
*                                                                        *
* Output(s)           : None                                             *
*                                                                        *
* Returns             : R2544_SUCCESS - When this function successfully  *
*                                      created Sla Table.                *
*                       R2544_FAILURE - When this function failed to     *
*                                      create Sla Table.                 *
*************************************************************************/
PUBLIC INT4
R2544CreateSlaTable (VOID)
{
    UINT4               u4RBNodeOffset = RFC2544_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tSlaEntry, SlaNode);

    if ((RFC2544_SLA_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, R2544SlaCmpFn)) == NULL)
    {
        RFC2544_GLOBAL_TRC
            ("R2544CreateSlaTable: Sla Table creation failed\r\n");

        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/*************************************************************************
*                                                                        *
* Function Name       : R2544CreateTrafficProfileTable                   *
*                                                                        *
* Description         : This function will create RBTree for traffic     *
*                       profile Table.                                   *
*                                                                        *
* Input(s)            : None                                             *
*                                                                        *
* Output(s)           : None                                             *
*                                                                        *
* Returns             : R2544_SUCCESS - When this function successfully  *
*                                      created Traffic profile Table.    *
*                       R2544_FAILURE - When this function failed to     *
*                                      create Traffic profile Table.     *
**************************************************************************/
PUBLIC INT4
R2544CreateTrafficProfileTable (VOID)
{
    UINT4               u4RBNodeOffset = RFC2544_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tTrafficProfile, TrafficProfileNode);

    if ((RFC2544_TRAFFIC_PROFILE_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, R2544TrafProfCmpFn)) == NULL)
    {
        RFC2544_GLOBAL_TRC ("R2544CreateTrafficProfileTable:"
                            "Traffic Profile Table creation failed\r\n");

        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/**************************************************************************
*                                                                        *
* Function Name       : R2544CreateSacTable                              *
*                                                                        *
* Description         : This function will create RBTree for SacTable.   *
*                                                                        *
* Input(s)            : None                                             *
*                                                                        *
* Output(s)           : None                                             *
*                                                                        *
* Returns             : R2544_SUCCESS - When this function successfully  *
*                                      created SacTable.                 *
*                       R2544_FAILURE - When this function failed to     *
*                                      create SacTable.                  *
**************************************************************************/
PUBLIC INT4
R2544CreateSacTable (VOID)
{
    UINT4               u4RBNodeOffset = RFC2544_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tSacEntry, SacNode);

    if ((RFC2544_SAC_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, R2544SacCmpFn)) == NULL)
    {
        RFC2544_GLOBAL_TRC
            ("R2544CreateSacTable: Sac Table creation failed\r\n");

        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/*************************************************************************
*                                                                        *
* Function Name       : R2544CreateReportStatsTable                      *
*                                                                        *
* Description         : This function will create RBTree for             *
*                       ReportStatsTable.                                *
*                                                                        *
* Input(s)            : None                                             *
*                                                                        *
* Output(s)           : None                                             *
*                                                                        *
* Returns             : R2544_SUCCESS - When this function successfully  *
*                                      created report Stats Table.       *
*                       R2544_FAILURE - When this function failed to     *
*                                      create report Stats Table.        *
**************************************************************************/
PUBLIC INT4
R2544CreateReportStatsTable (VOID)
{
    UINT4               u4RBNodeOffset = RFC2544_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tReportStatistics, ReportStatisticsNode);

    if ((RFC2544_REPORT_STATS_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, R2544ReportStatsCmpFn)) == NULL)
    {
        RFC2544_GLOBAL_TRC ("R2544CreateReportStatsTable: "
                            "Report Stats Table creation failed\r\n");

        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/*************************************************************************
*                                                                        *
* Function Name       : R2544CreateThroughputStatsTable                  *
*                                                                        *
* Description         : This function will create RBTree for             *
*                       ThroughputStats.                                 *
*                                                                        *
* Input(s)            : None                                             *
*                                                                        *
* Output(s)           : None                                             *
*                                                                        *
* Returns             : R2544_SUCCESS - When this function successfully  *
*                                      created ThroughputStatsTable.     *
*                       R2544_FAILURE - When this function failed to     *
*                                      create ThroughputStatsTable.      *
*************************************************************************/
PUBLIC INT4
R2544CreateThroughputStatsTable (VOID)
{
    UINT4               u4RBNodeOffset = RFC2544_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tThroughputStats, ThroughputStatsNode);

    if ((RFC2544_TH_STATS_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, R2544ThStatsCmpFn)) == NULL)
    {
        RFC2544_GLOBAL_TRC ("R2544CreateThroughputStatsTable: "
                            "Throughput Stats Table creation failed\r\n");

        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/*************************************************************************
*                                                                        *
* Function Name       : R2544CreateLatencyStatsTable                     *
*                                                                        *
* Description         : This function will create RBTree for             *
*                       LatencyStats.                                    *
*                                                                        *
* Input(s)            : None                                             *
*                                                                        *
* Output(s)           : None                                             *
*                                                                        *
* Returns             : R2544_SUCCESS - When this function successfully  *
*                                      created LatencyStatsTable .       *
*                       R2544_FAILURE - When this function failed to     *
*                                      create LatencyStatsTable .        *
*************************************************************************/
PUBLIC INT4
R2544CreateLatencyStatsTable (VOID)
{
    UINT4               u4RBNodeOffset = RFC2544_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tLatencyStats, LatencyStatsNode);

    if ((RFC2544_LA_STATS_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, R2544LaStatsCmpFn)) == NULL)
    {
        RFC2544_GLOBAL_TRC ("R2544CreateLatencyStatsTable: "
                            "Latency Stats Table creation failed\r\n");

        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/*************************************************************************
*                                                                        *
* Function Name       : R2544CreateFrameLossStatsTable                   *
*                                                                        *
* Description         : This function will create RBTree for             *
*                       FrameLossStats.                                  *
*                                                                        *
* Input(s)            : None                                             *
*                                                                        *
* Output(s)           : None                                             *
*                                                                        *
* Returns             : R2544_SUCCESS - When this function successfully  *
*                                      created FrameLossStatsTable.      *
*                       R2544_FAILURE - When this function failed to     *
*                                      create FrameLossStatsTable.       *
**************************************************************************/
PUBLIC INT4
R2544CreateFrameLossStatsTable (VOID)
{
    UINT4               u4RBNodeOffset = RFC2544_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tFrameLossStats, FrameLossStatsNode);

    if ((RFC2544_FL_STATS_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, R2544FlStatsCmpFn)) == NULL)
    {
        RFC2544_GLOBAL_TRC ("R2544CreateFrameLossStatsTable: "
                            "FrameLoss Stats Table creation failed\r\n");

        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/*************************************************************************
*                                                                        *
* Function Name       : R2544CreateBackToBackStatsTable                  *
*                                                                        *
* Description         : This function will create RBTree for             *
*                       BackToBackStats.                                 *
*                                                                        *
* Input(s)            : None                                             *
*                                                                        *
* Output(s)           : None                                             *
*                                                                        *
* Returns             : R2544_SUCCESS - When this function successfully  *
*                                      created BackToBackStats .         *
*                       R2544_FAILURE - When this function failed to     *
*                                      create BackToBackStats.           *
*************************************************************************/
PUBLIC INT4
R2544CreateBackToBackStatsTable (VOID)
{
    UINT4               u4RBNodeOffset = RFC2544_ZERO;

    u4RBNodeOffset = FSAP_OFFSETOF (tBackToBackStats, BackToBackStatsNode);

    if ((RFC2544_BB_STATS_TABLE () =
         RBTreeCreateEmbedded (u4RBNodeOffset, R2544BbStatsCmpFn)) == NULL)
    {
        RFC2544_GLOBAL_TRC ("R2544CreateBackToBackStatsTable: "
                            "BackToBack Stats Table creation failed\r\n");

        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544SlaCmpFn                                    *
*                                                                           *
*    Description         : This is compare function for SlaTable RBTree.    *
*                                                                           *
*    Input(s)            : pRBElem1 - First RBtree node pointer.            *
*                          pRBElem2 - Second RBtree node Pointer.           *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : 1 - if first entry is greater than second entry. *
*                         -1 - if first entry is smaller than second entry. *
*                          0 - if both first and second entry are same.     *
****************************************************************************/
PUBLIC INT4
R2544SlaCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tSlaEntry          *pSlaEntry1 = (tSlaEntry *) pRBElem1;
    tSlaEntry          *pSlaEntry2 = (tSlaEntry *) pRBElem2;

    if (pSlaEntry1->u4ContextId > pSlaEntry2->u4ContextId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pSlaEntry1->u4ContextId < pSlaEntry2->u4ContextId)
    {
        return RFC2544_CMP_LESS;
    }

    if (pSlaEntry1->u4SlaId > pSlaEntry2->u4SlaId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pSlaEntry1->u4SlaId < pSlaEntry2->u4SlaId)
    {
        return RFC2544_CMP_LESS;
    }

    return RFC2544_CMP_EQUAL;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544TrafProfCmpFn                               *
*                                                                           *
*    Description         : This is compare function for Traffic Profile     *
*                            Table RBTree.                                  *
*                                                                           *
*    Input(s)            : pRBElem1 - First RBtree node pointer.            *
*                          pRBElem2 - Second RBtree node Pointer.           *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : 1 - if first entry is greater than second entry. *
*                         -1 - if first entry is smaller than second entry. *
*                          0 - if both first and second entry are same.     *
*****************************************************************************/
PUBLIC INT4
R2544TrafProfCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tTrafficProfile    *pTrafficProfile1 = (tTrafficProfile *) pRBElem1;
    tTrafficProfile    *pTrafficProfile2 = (tTrafficProfile *) pRBElem2;

    if (pTrafficProfile1->u4ContextId > pTrafficProfile2->u4ContextId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pTrafficProfile1->u4ContextId < pTrafficProfile2->u4ContextId)
    {
        return RFC2544_CMP_LESS;
    }

    if (pTrafficProfile1->u4TrafficProfileId
        > pTrafficProfile2->u4TrafficProfileId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pTrafficProfile1->u4TrafficProfileId
        < pTrafficProfile2->u4TrafficProfileId)
    {
        return RFC2544_CMP_LESS;
    }

    return RFC2544_CMP_EQUAL;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544SacCmpFn                                    *
*                                                                           *
*    Description         : This is compare function for SacTable RBTree.    *
*                                                                           *
*    Input(s)            : pRBElem1 - First RBtree node pointer.            *
*                          pRBElem2 - Second RBtree node Pointer.           *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : 1 - if first entry is greater than second entry. *
*                         -1 - if first entry is smaller than second entry. *
*                          0 - if both first and second entry are same.     *
*****************************************************************************/
PUBLIC INT4
R2544SacCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tSacEntry          *pSacEntry1 = (tSacEntry *) pRBElem1;
    tSacEntry          *pSacEntry2 = (tSacEntry *) pRBElem2;

    if (pSacEntry1->u4ContextId > pSacEntry2->u4ContextId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pSacEntry1->u4ContextId < pSacEntry2->u4ContextId)
    {
        return RFC2544_CMP_LESS;
    }

    if (pSacEntry1->u4SacId > pSacEntry2->u4SacId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pSacEntry1->u4SacId < pSacEntry2->u4SacId)
    {
        return RFC2544_CMP_LESS;
    }

    return RFC2544_CMP_EQUAL;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544ReportStatsCmpFn                            *
*                                                                           *
*    Description         : This is compare function for SacTable RBTree.    *
*                                                                           *
*    Input(s)            : pRBElem1 - First RBtree node pointer.            *
*                          pRBElem2 - Second RBtree node Pointer.           *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : 1 - if first entry is greater than second entry. *
*                         -1 - if first entry is smaller than second entry. *
*                          0 - if both first and second entry are same.     *
*****************************************************************************/
PUBLIC INT4
R2544ReportStatsCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tReportStatistics  *pReportStatistics1 = (tReportStatistics *) pRBElem1;
    tReportStatistics  *pReportStatistics2 = (tReportStatistics *) pRBElem2;

    if (pReportStatistics1->u4ReportStatsContextId >
        pReportStatistics2->u4ReportStatsContextId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pReportStatistics1->u4ReportStatsContextId <
        pReportStatistics2->u4ReportStatsContextId)
    {
        return RFC2544_CMP_LESS;
    }

    if (pReportStatistics1->u4ReportStatsSlaId >
        pReportStatistics2->u4ReportStatsSlaId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pReportStatistics1->u4ReportStatsSlaId <
        pReportStatistics2->u4ReportStatsSlaId)
    {
        return RFC2544_CMP_LESS;
    }
    if (pReportStatistics1->u4ReportStatsFrameSize >
        pReportStatistics2->u4ReportStatsFrameSize)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pReportStatistics1->u4ReportStatsFrameSize <
        pReportStatistics2->u4ReportStatsFrameSize)
    {
        return RFC2544_CMP_LESS;
    }

    return RFC2544_CMP_EQUAL;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544ThStatsCmpFn                                *
*                                                                           *
*    Description         : This is compare function for Through put stats   *
*                            Table RBTree.                                  *
*                                                                           *
*    Input(s)            : pRBElem1 - First RBtree node pointer.            *
*                          pRBElem2 - Second RBtree node Pointer.           *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : 1 - if first entry is greater than second entry. *
*                         -1 - if first entry is smaller than second entry. *
*                          0 - if both first and second entry are same.     *
*****************************************************************************/
PUBLIC INT4
R2544ThStatsCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tThroughputStats   *pThroughputStats1 = (tThroughputStats *) pRBElem1;
    tThroughputStats   *pThroughputStats2 = (tThroughputStats *) pRBElem2;

    if (pThroughputStats1->u4ContextId > pThroughputStats2->u4ContextId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pThroughputStats1->u4ContextId < pThroughputStats2->u4ContextId)
    {
        return RFC2544_CMP_LESS;
    }

    if (pThroughputStats1->u4SlaId > pThroughputStats2->u4SlaId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pThroughputStats1->u4SlaId < pThroughputStats2->u4SlaId)
    {
        return RFC2544_CMP_LESS;
    }
    if (pThroughputStats1->u4FrameSize > pThroughputStats2->u4FrameSize)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pThroughputStats1->u4FrameSize < pThroughputStats2->u4FrameSize)
    {
        return RFC2544_CMP_LESS;
    }
    if (pThroughputStats1->u4TrialCount > pThroughputStats2->u4TrialCount)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pThroughputStats1->u4TrialCount < pThroughputStats2->u4TrialCount)
    {
        return RFC2544_CMP_LESS;
    }
    return RFC2544_CMP_EQUAL;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544LaStatsCmpFn                                *
*                                                                           *
*    Description         : This is compare function for Latency stats       *
*                            Table RBTree.                                  *
*                                                                           *
*    Input(s)            : pRBElem1 - First RBtree node pointer.            *
*                          pRBElem2 - Second RBtree node Pointer.           *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : 1 - if first entry is greater than second entry. *
*                         -1 - if first entry is smaller than second entry. *
*                          0 - if both first and second entry are same.     *
*****************************************************************************/
PUBLIC INT4
R2544LaStatsCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tLatencyStats      *pLatencyStats1 = (tLatencyStats *) pRBElem1;
    tLatencyStats      *pLatencyStats2 = (tLatencyStats *) pRBElem2;

    if (pLatencyStats1->u4ContextId > pLatencyStats2->u4ContextId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pLatencyStats1->u4ContextId < pLatencyStats2->u4ContextId)
    {
        return RFC2544_CMP_LESS;
    }

    if (pLatencyStats1->u4SlaId > pLatencyStats2->u4SlaId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pLatencyStats1->u4SlaId < pLatencyStats2->u4SlaId)
    {
        return RFC2544_CMP_LESS;
    }
    if (pLatencyStats1->u4FrameSize > pLatencyStats2->u4FrameSize)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pLatencyStats1->u4FrameSize < pLatencyStats2->u4FrameSize)
    {
        return RFC2544_CMP_LESS;
    }

    return RFC2544_CMP_EQUAL;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544FlStatsCmpFn                                *
*                                                                           *
*    Description         : This is compare function for Frame Loss stats    *
*                            Table RBTree.                                  *
*                                                                           *
*    Input(s)            : pRBElem1 - First RBtree node pointer.            *
*                          pRBElem2 - Second RBtree node Pointer.           *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : 1 - if first entry is greater than second entry. *
*                         -1 - if first entry is smaller than second entry. *
*                          0 - if both first and second entry are same.     *
*****************************************************************************/
PUBLIC INT4
R2544FlStatsCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tFrameLossStats    *pFrameLossStats1 = (tFrameLossStats *) pRBElem1;
    tFrameLossStats    *pFrameLossStats2 = (tFrameLossStats *) pRBElem2;

    if (pFrameLossStats1->u4ContextId > pFrameLossStats2->u4ContextId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pFrameLossStats1->u4ContextId < pFrameLossStats2->u4ContextId)
    {
        return RFC2544_CMP_LESS;
    }

    if (pFrameLossStats1->u4SlaId > pFrameLossStats2->u4SlaId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pFrameLossStats1->u4SlaId < pFrameLossStats2->u4SlaId)
    {
        return RFC2544_CMP_LESS;
    }
    if (pFrameLossStats1->u4FrameSize > pFrameLossStats2->u4FrameSize)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pFrameLossStats1->u4FrameSize < pFrameLossStats2->u4FrameSize)
    {
        return RFC2544_CMP_LESS;
    }

    return RFC2544_CMP_EQUAL;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544BbStatsCmpFn                                *
*                                                                           *
*    Description         : This is compare function for Back To back stats  *
*                            Table RBTree.                                  *
*                                                                           *
*    Input(s)            : pRBElem1 - First RBtree node pointer.            *
*                          pRBElem2 - Second RBtree node Pointer.           *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : 1 - if first entry is greater than second entry. *
*                         -1 - if first entry is smaller than second entry. *
*                          0 - if both first and second entry are same.     *
*****************************************************************************/
PUBLIC INT4
R2544BbStatsCmpFn (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tBackToBackStats   *pBackToBackStats1 = (tBackToBackStats *) pRBElem1;
    tBackToBackStats   *pBackToBackStats2 = (tBackToBackStats *) pRBElem2;

    if (pBackToBackStats1->u4ContextId > pBackToBackStats2->u4ContextId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pBackToBackStats1->u4ContextId < pBackToBackStats2->u4ContextId)
    {
        return RFC2544_CMP_LESS;
    }

    if (pBackToBackStats1->u4SlaId > pBackToBackStats2->u4SlaId)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pBackToBackStats1->u4SlaId < pBackToBackStats2->u4SlaId)
    {
        return RFC2544_CMP_LESS;
    }
    if (pBackToBackStats1->u4FrameSize > pBackToBackStats2->u4FrameSize)
    {
        return RFC2544_CMP_GREATER;
    }

    if (pBackToBackStats1->u4FrameSize < pBackToBackStats2->u4FrameSize)
    {
        return RFC2544_CMP_LESS;
    }

    return RFC2544_CMP_EQUAL;
}

/****************************************************************************
* Function Name      : R2544DeleteSlaTable                                  *
*                                                                           *
* Description        : This routine deletes Sla Table                       *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
VOID
R2544DeleteSlaTable (VOID)
{
    UINT4               u4ContextId = RFC2544_ZERO;

    /* RBTree deletion for SLA Table. */
    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544DelAllSlaEntries (u4ContextId);
        }
    }
    if (RFC2544_SLA_TABLE () != NULL)
    {
        RBTreeDestroy (RFC2544_SLA_TABLE (), NULL, RFC2544_ZERO);
        RFC2544_SLA_TABLE () = NULL;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DeleteTrafficProfileTable                       *
*                                                                           *
* Description        : This routine deletesTrafficProfileTable              *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
VOID
R2544DeleteTrafficProfileTable (VOID)
{
    UINT4               u4ContextId = RFC2544_ZERO;

    /* RBTree deletion for Traffic profile Table. */
    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544DelAllTrafficProfileEntries (u4ContextId);
        }
    }

    if (RFC2544_TRAFFIC_PROFILE_TABLE () != NULL)
    {
        RBTreeDestroy (RFC2544_TRAFFIC_PROFILE_TABLE (), NULL, RFC2544_ZERO);
        RFC2544_TRAFFIC_PROFILE_TABLE () = NULL;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DeleteSacTable                                  *
*                                                                           *
* Description        : This routine deletes Sac  Table                      *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
VOID
R2544DeleteSacTable (VOID)
{
    UINT4               u4ContextId = RFC2544_ZERO;

    /* RBTree deletion for SAC Table. */
    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544DelAllSacEntries (u4ContextId);
        }
    }

    if (RFC2544_SAC_TABLE () != NULL)
    {
        RBTreeDestroy (RFC2544_SAC_TABLE (), NULL, RFC2544_ZERO);
        RFC2544_SAC_TABLE () = NULL;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DeleteReportStatsTable                          *
*                                                                           *
* Description        : This routine deletes Report Stats Table              *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
VOID
R2544DeleteReportStatsTable (VOID)
{
    UINT4               u4ContextId = RFC2544_ZERO;

    /* RBTree deletion for Report Stats Table. */
    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544DelAllReportStatsEntries (u4ContextId);
        }
    }

    if (RFC2544_REPORT_STATS_TABLE () != NULL)
    {
        RBTreeDestroy (RFC2544_REPORT_STATS_TABLE (), NULL, RFC2544_ZERO);
        RFC2544_REPORT_STATS_TABLE () = NULL;
    }
    return;
}

/*****************************************************************************
* Function Name      : R2544DeleteThroughputStatsTable                      *
*                                                                           *
* Description        : This routine deletes Throughput Stats  Table         *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
VOID
R2544DeleteThroughputStatsTable (VOID)
{
    UINT4               u4ContextId = RFC2544_ZERO;

    /* RBTree deletion for Throughput Stats Table. */
    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544DelAllThroughputStatsEntries (u4ContextId);
        }
    }

    if (RFC2544_TH_STATS_TABLE () != NULL)
    {
        RBTreeDestroy (RFC2544_TH_STATS_TABLE (), NULL, RFC2544_ZERO);
        RFC2544_TH_STATS_TABLE () = NULL;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DeleteLatencyStatsTable                         *
*                                                                           *
* Description        : This routine deletes latency stats Table             *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
VOID
R2544DeleteLatencyStatsTable (VOID)
{
    UINT4               u4ContextId = RFC2544_ZERO;
    /* RBTree deletion for Latency Stats Table. */
    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544DelAllLatencyStatsEntries (u4ContextId);
        }
    }

    if (RFC2544_LA_STATS_TABLE () != NULL)
    {
        RBTreeDestroy (RFC2544_LA_STATS_TABLE (), NULL, RFC2544_ZERO);
        RFC2544_LA_STATS_TABLE () = NULL;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DeleteFramelossStatsTable                       *
*                                                                           *
* Description        : This routine deletes frame loss stats Table          *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
VOID
R2544DeleteFrameLossStatsTable (VOID)
{
    UINT4               u4ContextId = RFC2544_ZERO;

    /* RBTree deletion for Frame Loss Stats  Table. */
    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544DelAllFrameLossStatsEntries (u4ContextId);
        }
    }

    if (RFC2544_FL_STATS_TABLE () != NULL)
    {
        RBTreeDestroy (RFC2544_FL_STATS_TABLE (), NULL, RFC2544_ZERO);
        RFC2544_FL_STATS_TABLE () = NULL;
    }
    return;
}

/*****************************************************************************
* Function Name      : R2544DeleteBackToBackStatsTable                      *
*                                                                           *
* Description        : This routine deletes  BackToBack Stats Table         *
*                                                                           *
* Input(s)           : None                                                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
VOID
R2544DeleteBackToBackStatsTable (VOID)
{
    UINT4               u4ContextId = RFC2544_ZERO;

    /* RBTree deletion for BackToBack Stats  Table. */
    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544DelAllBackToBackStatsEntries (u4ContextId);
        }
    }
    if (RFC2544_BB_STATS_TABLE () != NULL)
    {
        RBTreeDestroy (RFC2544_BB_STATS_TABLE (), NULL, RFC2544_ZERO);
        RFC2544_BB_STATS_TABLE () = NULL;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DelAllSlaEntries                                *
*                                                                           *
* Description        : This function deletes all the SlaEntry from the      *
*                      SLA Table                                            *
*                                                                           *
* Input(s)           : u4ContextId - Context identifier                     *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/
VOID
R2544DelAllSlaEntries (UINT4 u4ContextId)
{
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry          *pNextSlaEntry = NULL;

    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());

    while (pSlaEntry != NULL)
    {
        /* Delete all the nodes */
        pNextSlaEntry = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                     (tRBElem *) pSlaEntry,
                                                     NULL);
        if (pSlaEntry->u4ContextId == u4ContextId)
        {
            R2544UtilDelSlaEntry (pSlaEntry);
        }
        pSlaEntry = pNextSlaEntry;
    }
    return;
}

/*****************************************************************************
* Function Name      : R2544DelAllTrafficProfileEntries                     *
*                                                                           *
* Description        : This function deletes all the Traffic Profile from   *
*                      Traffic Profile  Table                               *
*                                                                           *
* Input(s)           : NONE                                                 *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/
VOID
R2544DelAllTrafficProfileEntries (UINT4 u4ContextId)
{
    tTrafficProfile    *pTrafficProfile = NULL;
    tTrafficProfile    *pNextTrafficProfile = NULL;

    pTrafficProfile = (tTrafficProfile *) RBTreeGetFirst
        (RFC2544_TRAFFIC_PROFILE_TABLE ());

    while (pTrafficProfile != NULL)
    {
        /* Delete all the nodes */
        pNextTrafficProfile = (tTrafficProfile *) RBTreeGetNext
            (RFC2544_TRAFFIC_PROFILE_TABLE (), (tRBElem *) pTrafficProfile,
             NULL);
        if (pTrafficProfile->u4ContextId == u4ContextId)
        {
            R2544UtilDelTrafProfEntry (pTrafficProfile);
        }
        pTrafficProfile = pNextTrafficProfile;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DelAllSacEntries                                *
*                                                                           *
* Description        : This function deletes all the SacEntry from the      *
*                      SLA Table                                            *
*                                                                           *
* Input(s)           : NONE                                                 *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/
VOID
R2544DelAllSacEntries (UINT4 u4ContextId)
{
    tSacEntry          *pSacEntry = NULL;
    tSacEntry          *pNextSacEntry = NULL;

    pSacEntry = (tSacEntry *) RBTreeGetFirst (RFC2544_SAC_TABLE ());

    while (pSacEntry != NULL)
    {
        /* Delete all the nodes */
        pNextSacEntry = (tSacEntry *) RBTreeGetNext (RFC2544_SAC_TABLE (),
                                                     (tRBElem *) pSacEntry,
                                                     NULL);
        if (pSacEntry->u4ContextId == u4ContextId)
        {
            R2544UtilDelSacEntry (pSacEntry);
        }
        pSacEntry = pNextSacEntry;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DelAllReportStatsEntries                        *
*                                                                           *
* Description        : This function deletes all the Report Stats from the  *
*                      Report Stats Table                                   *
*                                                                           *
* Input(s)           : NONE                                                 *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/
VOID
R2544DelAllReportStatsEntries (UINT4 u4ContextId)
{
    tReportStatistics  *pReportStatistics = NULL;
    tReportStatistics  *pNextReportStatistics = NULL;

    pReportStatistics =
        (tReportStatistics *) RBTreeGetFirst (RFC2544_REPORT_STATS_TABLE ());

    while (pReportStatistics != NULL)
    {
        /* Delete all the nodes */
        pNextReportStatistics =
            (tReportStatistics *) RBTreeGetNext (RFC2544_REPORT_STATS_TABLE (),
                                                 (tRBElem *) pReportStatistics,
                                                 NULL);
        if (pReportStatistics->u4ReportStatsContextId == u4ContextId)
        {

            R2544UtilDelReportStats (pReportStatistics);
        }

        pReportStatistics = pNextReportStatistics;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DelAllThroughputStatsEntries                    *
*                                                                           *
* Description        : This function deletes all the Throughput Stats from  *
*                      the Throughput Stats Table                           *
*                                                                           *
* Input(s)           : NONE                                                 *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/
VOID
R2544DelAllThroughputStatsEntries (UINT4 u4ContextId)
{
    tThroughputStats   *pThroughputStats = NULL;
    tThroughputStats   *pNextThroughputStats = NULL;

    pThroughputStats =
        (tThroughputStats *) RBTreeGetFirst (RFC2544_TH_STATS_TABLE ());

    while (pThroughputStats != NULL)
    {
        /* Delete all the nodes */
        pNextThroughputStats =
            (tThroughputStats *) RBTreeGetNext (RFC2544_TH_STATS_TABLE (),
                                                (tRBElem *) pThroughputStats,
                                                NULL);
        if (pThroughputStats->u4ContextId == u4ContextId)
        {
            R2544UtilDelThroughputStats (pThroughputStats);
        }

        pThroughputStats = pNextThroughputStats;
    }
    return;
}

/*****************************************************************************
* Function Name      : R2544DelAllLatencyStatsEntries                       *
*                                                                           *
* Description        : This function deletes all the Latency Stats from     *
*                      the Latency Stats Table                              *
*                                                                           *
* Input(s)           : NONE                                                 *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/
VOID
R2544DelAllLatencyStatsEntries (UINT4 u4ContextId)
{
    tLatencyStats      *pLatencyStats = NULL;
    tLatencyStats      *pNextLatencyStats = NULL;

    pLatencyStats =
        (tLatencyStats *) RBTreeGetFirst (RFC2544_LA_STATS_TABLE ());

    while (pLatencyStats != NULL)
    {
        /* Delete all the nodes */
        pNextLatencyStats =
            (tLatencyStats *) RBTreeGetNext (RFC2544_LA_STATS_TABLE (),
                                             (tRBElem *) pLatencyStats, NULL);
        if (pLatencyStats->u4ContextId == u4ContextId)
        {
            R2544UtilDelLatencyStats (pLatencyStats);
        }

        pLatencyStats = pNextLatencyStats;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544DelAllFrameLossStatsEntries                     *
*                                                                           *
* Description        : This function deletes all the FrameLoss Stats from   *
*                      the FrameLoss Stats Table                            *
*                                                                           *
* Input(s)           : NONE                                                 *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/
VOID
R2544DelAllFrameLossStatsEntries (UINT4 u4ContextId)
{
    tFrameLossStats    *pFrameLossStats = NULL;
    tFrameLossStats    *pNextFrameLossStats = NULL;

    pFrameLossStats =
        (tFrameLossStats *) RBTreeGetFirst (RFC2544_FL_STATS_TABLE ());

    while (pFrameLossStats != NULL)
    {
        /* Delete all the nodes */
        pNextFrameLossStats =
            (tFrameLossStats *) RBTreeGetNext (RFC2544_FL_STATS_TABLE (),
                                               (tRBElem *) pFrameLossStats,
                                               NULL);
        if (pFrameLossStats->u4ContextId == u4ContextId)
        {
            R2544UtilDelFrameLossStats (pFrameLossStats);
        }

        pFrameLossStats = pNextFrameLossStats;
    }
    return;
}

/*****************************************************************************
* Function Name      : R2544DelAllBackToBackStatsEntries                    *
*                                                                           *
* Description        : This function deletes all the BackToBack Stats from  *
*                      the BackToBack Stats Table                           *
*                                                                           *
* Input(s)           : NONE                                                 *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/
VOID
R2544DelAllBackToBackStatsEntries (UINT4 u4ContextId)
{
    tBackToBackStats   *pBackToBackStats = NULL;
    tBackToBackStats   *pNextBackToBackStats = NULL;

    pBackToBackStats =
        (tBackToBackStats *) RBTreeGetFirst (RFC2544_BB_STATS_TABLE ());

    while (pBackToBackStats != NULL)
    {
        /* Delete all the nodes */
        pNextBackToBackStats =
            (tBackToBackStats *) RBTreeGetNext (RFC2544_BB_STATS_TABLE (),
                                                (tRBElem *) pBackToBackStats,
                                                NULL);
        if (pBackToBackStats->u4ContextId == u4ContextId)
        {
            R2544UtilDelBackToBackStats (pBackToBackStats);
        }
        pBackToBackStats = pNextBackToBackStats;
    }
    return;
}

/*****************************************************************************/
/*              SERVICE LEVEL AGREEMENT UTILITIES                            */
/*****************************************************************************/

/****************************************************************************
* Function Name      : R2544UtilAddSlaEntry                                 *
*                                                                           *
* Description        : This routine adds a Sla Entry to  Sla Table          *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA ID                                     *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilAddSlaEntry (UINT4 u4ContextId, UINT4 u4SlaId)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);

    if (pSlaEntry != NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4ContextId,
                      "Sla entry exists for Sla ID :%d \r\n", u4SlaId);
        return RFC2544_SUCCESS;
    }
    pSlaEntry = (tSlaEntry *) MemAllocMemBlk (RFC2544_SLA_POOL ());
    if (pSlaEntry == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                     "Memory allocation for SLA Entry failed\r\n");
        return RFC2544_FAILURE;
    }

    RFC2544_MEMSET (pSlaEntry, RFC2544_ZERO, sizeof (tSlaEntry));

    pSlaEntry->u4ContextId = u4ContextId;
    pSlaEntry->u4SlaId = u4SlaId;
    pSlaEntry->u1SlaCurrentTestState = RFC2544_NOT_INITIATED;

    pSlaEntry->u1SlaTestStatus = RFC2544_STOP_TEST;

    if (RBTreeAdd (RFC2544_SLA_TABLE (), (tRBElem *) pSlaEntry) == RB_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_SLA_POOL (), (UINT1 *) pSlaEntry);

        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, u4ContextId,
                      "R2544UtilAddSlaEntry: Sla Table Addition Failed for "
                      "u4SlaId %d \r\n", u4SlaId);
        return RFC2544_FAILURE;
    }

    RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4ContextId,
                  "R2544UtilAddSlaEntry: Sla Entry is added to "
                  "SLA Table with Id %d \r\n", u4SlaId);
    return RFC2544_SUCCESS;

}

/*****************************************************************************
* Function Name      : R2544UtilGetSlaEntry                                 *
*                                                                           *
* Description        : This routine gets a SLA Entry                        *
*                      corresponding to the                                 *
*                                                                           *
* Input(s)           : u4ContextId - Context Id                             *
*                      u4SlaId - Sla Id                                     *
*                                                                           *
* Output(s)          : pSlaEntry - Sla Entry                                *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
tSlaEntry          *
R2544UtilGetSlaEntry (UINT4 u4ContextId, UINT4 u4SlaId)
{
    tSlaEntry          *pSlaEntry = NULL;
    tR2544ContextInfo  *pContextInfo = NULL;
    tSlaEntry           SlaEntry;

    RFC2544_MEMSET (&SlaEntry, RFC2544_ZERO, sizeof (tSlaEntry));
    pContextInfo = R2544CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");

        return NULL;
    }

    SlaEntry.u4ContextId = u4ContextId;
    SlaEntry.u4SlaId = u4SlaId;

    pSlaEntry = (tSlaEntry *) RBTreeGet
        (RFC2544_SLA_TABLE (), (tRBElem *) & SlaEntry);

    return pSlaEntry;
}

/****************************************************************************
* Function Name      : R2544UtilDelSlaEntry                                 *
*                                                                           *
* Description        : This routine delete a Entry from Trffic Profile Table*
*                                                                           *
* Input(s)           : pSlaEntry - node to be deleted                       *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
INT4
R2544UtilDelSlaEntry (tSlaEntry * pSlaEntry)
{

    tReportStatistics  *pReportStatistics = NULL;
    tReportStatistics  *pNextReportStatistics = NULL;
    tSlaEntry          *pSlaNode = NULL;

    if (pSlaEntry == NULL)
    {
        return RFC2544_SUCCESS;
    }
    if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)
    {

        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                      "R2544UtilDelSlaEntry: "
                      "Test is in-progress for SLA entry %d\r\n",
                      pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_RFC2544_SLA_CONF_ERR);
        return RFC2544_FAILURE;
    }

    pReportStatistics =
        (tReportStatistics *) RBTreeGetFirst (RFC2544_REPORT_STATS_TABLE ());

    while (pReportStatistics != NULL)
    {
        /* Delete all the nodes */
        pNextReportStatistics = (tReportStatistics *) RBTreeGetNext
            (RFC2544_REPORT_STATS_TABLE (), (tRBElem *) pReportStatistics,
             NULL);
        if ((pReportStatistics->u4ReportStatsContextId ==
             pSlaEntry->u4ContextId) &&
            (pReportStatistics->u4ReportStatsSlaId == pSlaEntry->u4SlaId))
        {

            R2544UtilDelReportStats (pReportStatistics);
        }
        pReportStatistics = pNextReportStatistics;
    }

    pSlaNode =
        R2544UtilGetSlaEntry (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId);

    if (pSlaNode == NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pSlaEntry->u4ContextId,
                      "Sla entry not exists for Sla ID :%d \r\n",
                      pSlaEntry->u4SlaId);
        return RFC2544_FAILURE;
    }

    if (RBTreeRemove ((RFC2544_SLA_TABLE ()),
                      (tRBElem *) pSlaEntry) == RB_FAILURE)
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, pSlaEntry->u4ContextId,
                     "R2544UtilDelSlaEntry: RBTree remove failed \r\n");
        return RFC2544_FAILURE;
    }

    RFC2544_TRC1 (RFC2544_MGMT_TRC, pSlaEntry->u4ContextId,
                  "R2544UtilDelSlaEntry: Sla Entry is deleted from"
                  "SLA Table with Id %d \r\n", pSlaEntry->u4SlaId);

    MemReleaseMemBlock (RFC2544_SLA_POOL (), (UINT1 *) (pSlaEntry));
    pSlaEntry = NULL;
    return RFC2544_SUCCESS;
}

/*******************************************************************************
 * FUNCTION NAME    : R2544UtilValidateSla                                     *
 *                                                                             *
 * DESCRIPTION      : This function will check whether the context is created  *
 *                    the system control of the context and the                *
 *                                                                             *
 *                    This function will be called by the nmhGet/nmhSet        *
 *                    routines and this function will take care of filling the *
 *                    the SNMP error codes, CLI set error and trace messages.  *
 *                                                                             *
 * INPUT            : u4ContextId - Context Identifier                         *
 *                    u4SlaId - SLA Id                                         *
 *                                                                             *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.                *
 *                                                                             *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                                *
 *                                                                             *
 ******************************************************************************/
PUBLIC INT1
R2544UtilValidateSla (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 *pu4ErrorCode)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtValidateContextInfo (u4ContextId, pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return RFC2544_FAILURE;
    }
    if ((u4SlaId < RFC2544_MIN_SLA_ID) || (u4SlaId > RFC2544_MAX_SLA_ID))
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                     "Invalid SLA ID \r\n");
        CLI_SET_ERR (CLI_RFC2544_INVALID_SLA_ID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME    : R2544UtilValidateSlaInformation                        *
 *                                                                           *
 * DESCRIPTION      : This function will check whether the configured MEP is *
 *                    existing in ECFM?Y1731                                 *
 *                                                                           *
 * INPUT            :                                                        *
 *                    u4SlaId - SLA Id                                       *
 *                                                                           *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.              *
 *                                                                           *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                              *
 *                                                                           *
 *****************************************************************************/ 
PUBLIC INT1                                                                  
R2544UtilValidateSlaInformation (tSlaEntry * pSlaEntry)                      
{                                                                            
                                                                             
    if ((pSlaEntry->u4SlaMeg == RFC2544_ZERO)
        || (pSlaEntry->u4SlaMe == RFC2544_ZERO)
        || (pSlaEntry->u4SlaMep == RFC2544_ZERO))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                      "R2544UtilValidateSlaInformation: MEP/ME/MEG is not "
                      "configured for Sla %d \r\n", pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_RFC2544_SERVICE_NOT_CONFIGURED);

        return RFC2544_FAILURE;
    }
    if (pSlaEntry->u4SlaTrafProfId == RFC2544_ZERO)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                      "R2544UtilValidateSlaInformation: Traffic Profile not "
                      "configured for Sla %d \r\n", pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_RFC2544_SLA_TRAFPROF_NOT_CONFIGURED);

        return RFC2544_FAILURE;
    }

    if (pSlaEntry->u4SlaSacId == RFC2544_ZERO)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                      "R2544UtilValidateSlaInformation: Sac not configured "
                      "for Sla %d \r\n", pSlaEntry->u4SlaId);
        CLI_SET_ERR (CLI_RFC2544_SLA_SAC_NOT_CONFIGURED);

        return RFC2544_FAILURE;
    }

    return RFC2544_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R2544UtilGetSlaTestParams                            *
 *                                                                         *
 * DESCRIPTION      : This function will fetch the test params             *
 *                                                                         *
 * INPUT            : pSlaEntry - Sla Entry                                *
 *                                                                         *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.            *
 *                                                                         *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                            *
 *                                                                         *
 ***************************************************************************/
PUBLIC INT1                                                                
R2544UtilGetSlaTestParams (tSlaEntry * pSlaEntry)                          
{
    tEcfmReqParams     *pEcfmReqParams = NULL;                             
                                                                           
    if (pSlaEntry == NULL)                                                 
    {
        RFC2544_GLOBAL_TRC
            ("R2544UtilGetSlaTestParams: Sla Entry is not present\r\n ");
        return RFC2544_FAILURE;
    }

    pEcfmReqParams = EcfmSlaGetPerfTestParams (pSlaEntry->u4ContextId,
                                               pSlaEntry->u4SlaId,
                                               pSlaEntry->u4SlaMeg,
                                               pSlaEntry->u4SlaMe,
                                               pSlaEntry->u4SlaMep);
    
    RFC2544_TRC2 (RFC2544_Y1731_INTF_TRC,
                  pSlaEntry->u4ContextId,
                  "R2544UtilGetSlaTestParams: Interfaces with the"
                  " Y1731 to fetch the test params for Context ID: %d,"
                  " Sla ID: %d\r\n",
                  pSlaEntry->u4ContextId,pSlaEntry->u4SlaId);
    
    if (pEcfmReqParams == NULL)
    {

        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_Y1731_INTF_TRC, pSlaEntry->u4ContextId,
                      "R2544UtilGetSlaTestParams: Failed to update Sla test "
                      "parameters for Sla %d \r\n", pSlaEntry->u4SlaId);
        return RFC2544_FAILURE;

    }

    pSlaEntry->u4IfIndex = pEcfmReqParams->u4IfIndex;
    pSlaEntry->u4TagType = pEcfmReqParams->u4TagType;
    pSlaEntry->u4PortSpeed = pEcfmReqParams->u4PortSpeed;
    pSlaEntry->OutVlanId = pEcfmReqParams->OutVlanId;
    pSlaEntry->u4IfMtu = pEcfmReqParams->u4IfMtu;

    RFC2544_MEMCPY (pSlaEntry->TxSrcMacAddr, pEcfmReqParams->TxSrcMacAddr,
                    sizeof (tMacAddr));

    RFC2544_MEMCPY (pSlaEntry->TxDstMacAddr, pEcfmReqParams->TxDstMacAddr,
                    sizeof (tMacAddr));

    RFC2544_TRC1 (RFC2544_Y1731_INTF_TRC, pSlaEntry->u4ContextId,
                  "R2544UtilGetSlaTestParams: Test Parameters are updated"
                  " successfully for SLA %d \r\n", pSlaEntry->u4SlaId);
    return RFC2544_SUCCESS;
}

/*****************************************************************************/
/*                    TRAFFIC PROFILE TABLE UTILITIES                        */
/*****************************************************************************/

/*****************************************************************************
* Function Name      : R2544UtilAddTrafProfEntry                            *
*                                                                           *
* Description        : This routine adds a Traffic Profile Entry to         *
*                      Traffic Profile Table                                *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4TrafficProfileId - Traffic profile ID              *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilAddTrafProfEntry (UINT4 u4ContextId, UINT4 u4TrafficProfileId)
{
    tTrafficProfile    *pTrafficProfile = NULL;
    UINT1               au1DefaultFrameSize[RFC2544_STRING_MAX_LENGTH] =
        RFC2544_DEFAULT_FRAMESIZE;
    UINT1               au1ProfileName[RFC2544_PROFILE_NAME_MAX_LEN] = { "Profile" };
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4ContextId, u4TrafficProfileId);

    if (pTrafficProfile != NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4ContextId,
                      "Traffic Profile entry exists for Traffic Profile "
                      "ID :%d \r\n", u4TrafficProfileId);
        return RFC2544_SUCCESS;
    }
    pTrafficProfile = (tTrafficProfile *) MemAllocMemBlk
        (RFC2544_TRAFFIC_PROFILE_POOL ());
    if (pTrafficProfile == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                     "Memory allocation for Traffic profile Entry failed\r\n");
        return RFC2544_FAILURE;
    }

    RFC2544_MEMSET (pTrafficProfile, RFC2544_ZERO, sizeof (tTrafficProfile));
    RFC2544_MEMSET (au1ProfileName, RFC2544_ZERO, RFC2544_PROFILE_NAME_MAX_LEN);

    pTrafficProfile->u4ContextId = u4ContextId;
    pTrafficProfile->u4TrafficProfileId = u4TrafficProfileId;

    SPRINTF ((char *) au1ProfileName, "Profile%d",
             pTrafficProfile->u4TrafficProfileId);

    RFC2544_MEMCPY (pTrafficProfile->au1TrafProfName, au1ProfileName,
                    STRLEN (au1ProfileName));
    pTrafficProfile->u1TrafProfSeqNoCheck = RFC2544_SEQUENCE_NUM_CHECK;
    pTrafficProfile->u4TrafProfDwellTime = RFC2544_DWELL_TIME;
    pTrafficProfile->u4TrafProfPCP = RFC2544_PCP;

    RFC2544_MEMCPY (pTrafficProfile->au1TrafProfFrameSize, au1DefaultFrameSize,
                    RFC2544_STRLEN (au1DefaultFrameSize));

    pTrafficProfile->au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_ZERO] =
        RFC2544_FRAME_64;
    pTrafficProfile->au4TrafProfFrameSize[RFC2544_ONE][RFC2544_ZERO] =
        RFC2544_FRAME_128;
    pTrafficProfile->au4TrafProfFrameSize[RFC2544_TWO][RFC2544_ZERO] =
        RFC2544_FRAME_256;
    pTrafficProfile->au4TrafProfFrameSize[RFC2544_THREE][RFC2544_ZERO] =
        RFC2544_FRAME_512;
    pTrafficProfile->au4TrafProfFrameSize[RFC2544_FOUR][RFC2544_ZERO] =
        RFC2544_FRAME_1024;
    pTrafficProfile->au4TrafProfFrameSize[RFC2544_FIVE][RFC2544_ZERO] =
        RFC2544_FRAME_1280;
    pTrafficProfile->au4TrafProfFrameSize[RFC2544_SIX][RFC2544_ZERO] =
        RFC2544_FRAME_1518;
    pTrafficProfile->u1TrafProfThTestStatus = RFC2544_THROUGHPUT_STATUS;
    pTrafficProfile->u1TrafProfLaTestStatus = RFC2544_LATENCY_STATUS;
    pTrafficProfile->u1TrafProfFlTestStatus = RFC2544_FRAMELOSS_STATUS;
    pTrafficProfile->u4TrafProfThRateStep = RFC2544_TH_RATESTEP; 
    pTrafficProfile->u4TrafProfFlRateStep = RFC2544_FL_RATESTEP;
    pTrafficProfile->u4TrafProfThMinRate = RFC2544_MIN_RATE;
    pTrafficProfile->u4TrafProfThMaxRate = RFC2544_MAX_RATE;
    pTrafficProfile->u4TrafProfFlMinRate = RFC2544_MIN_RATE;
    pTrafficProfile->u4TrafProfFlMaxRate = RFC2544_MAX_RATE;
    pTrafficProfile->u4TrafProfFlTrialDuration = RFC2544_FL_TRIALDURATION;
    pTrafficProfile->u1TrafProfBbTestStatus = RFC2544_BACKTOBACK_STATUS;
    pTrafficProfile->u4TrafProfThTrialDuration = RFC2544_TH_TRIALDURATION;
    pTrafficProfile->u4TrafProfLaTrialDuration = RFC2544_LA_TRAILDURATION;
    pTrafficProfile->u4TrafProfLaDelayMeasureInterval =
        RFC2544_LA_DELAY_MEA_INTERVAL;

    pTrafficProfile->u4TrafProfBbTrialDuration = RFC2544_BB_TRIALDURATION;
    pTrafficProfile->u4TrafProfBbTrialCount = RFC2544_BB_TRIALCOUNT;
    pTrafficProfile->u1RowStatus = ACTIVE;

    if (RBTreeAdd (RFC2544_TRAFFIC_PROFILE_TABLE (), (tRBElem *)
                   pTrafficProfile) == RB_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_TRAFFIC_PROFILE_POOL (),
                            (UINT1 *) pTrafficProfile);

        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, u4ContextId,
                      "R2544UtilAddTrafProfEntry: Traffic Profile Table Addition"
                      " Failed for u4TrafProfId %d \r\n", u4TrafficProfileId);
        return RFC2544_FAILURE;
    }

    RFC2544_TRC1 (RFC2544_MGMT_TRC, u4ContextId,
                  "R2544UtilAddTrafProfEntry: Traffic Profile Entry "
                  "is added to Traffic profile Table with Id %d \r\n",
                  u4TrafficProfileId);
    return RFC2544_SUCCESS;

}

/****************************************************************************
* Function Name      : R2544UtilGetTrafProfEntry                            *
*                                                                           *
* Description        : This routine gets a Traffic profile Entry            *
*                      corresponding to the  from Traffic Profile Table     *
*                                                                           *
* Input(s)           : u4ContextId - Context Id                             *
*                      u4TrafficProfileId - Traffic Profile Id              *
*                                                                           *
* Output(s)          : pTrafficProfile - Traffic profile Entry              *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
tTrafficProfile    *
R2544UtilGetTrafProfEntry (UINT4 u4ContextId, UINT4 u4TrafficProfileId)
{
    tTrafficProfile    *pTrafficProfile = NULL;
    tR2544ContextInfo  *pContextInfo = NULL;
    tTrafficProfile     TrafficProfile;

    RFC2544_MEMSET (&TrafficProfile, RFC2544_ZERO, sizeof (tTrafficProfile));
    pContextInfo = R2544CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "Context entry %d does not exist\r\n", u4ContextId);
        return NULL;
    }

    TrafficProfile.u4ContextId = u4ContextId;
    TrafficProfile.u4TrafficProfileId = u4TrafficProfileId;

    pTrafficProfile = (tTrafficProfile *) RBTreeGet
        (RFC2544_TRAFFIC_PROFILE_TABLE (), (tRBElem *) & TrafficProfile);

    return pTrafficProfile;
}

/****************************************************************************
* Function Name      : R2544UtilDelTrafProfEntry                            *
*                                                                           *
* Description        : This routine delete a Entry from Trffic Profile Table*
*                                                                           *
* Input(s)           : pTrafProfEntry - node to be deleted                  *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
INT4
R2544UtilDelTrafProfEntry (tTrafficProfile * pTrafficProfile)
{
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry          *pNextSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfileNode = NULL;
    if (pTrafficProfile == NULL)
    {
        return RFC2544_SUCCESS;
    }

    pTrafficProfileNode =
        R2544UtilGetTrafProfEntry (pTrafficProfile->u4ContextId,
                                   pTrafficProfile->u4TrafficProfileId);

    if (pTrafficProfileNode == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_INIT_SHUT_TRC, pTrafficProfile->u4ContextId,
                      "Traffic Profile entry not exists for Sla ID :%d \r\n",
                      pTrafficProfile->u4TrafficProfileId);
        return RFC2544_FAILURE;
    }

    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());

    while (pSlaEntry != NULL)
    {
        pNextSlaEntry = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                     (tRBElem *) pSlaEntry,
                                                     NULL);

        /* Check whether traffic profile is mapped to any SLA */
        if ((pSlaEntry->u4SlaTrafProfId == pTrafficProfile->u4TrafficProfileId)
            && (pSlaEntry->u4ContextId == pTrafficProfile->u4ContextId))
        {
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, pTrafficProfile->u4ContextId,
                         "R2544UtilDelTrafProfEntry: traffic profile is mapped "
                         "to SLA\r\n");
            CLI_SET_ERR (CLI_RFC2544_ERR_TRAF_PROF_MAPPED);
            return RFC2544_FAILURE;
        }

        pSlaEntry = pNextSlaEntry;
    }

    if (RBTreeRemove ((RFC2544_TRAFFIC_PROFILE_TABLE ()),
                      (tRBElem *) pTrafficProfile) == RB_FAILURE)
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, pTrafficProfile->u4ContextId,
                     "R2544UtilDelTrafProfEntry: RBTree remove failed \r\n");
        return RFC2544_FAILURE;
    }

    RFC2544_TRC1 (RFC2544_MGMT_TRC, pTrafficProfile->u4ContextId,
                  "R2544UtilDelTrafProfEntry:Traffic Profile Entry is deleted "
                  "from Traffic profile Table with Id %d \r\n",
                  pTrafficProfile->u4TrafficProfileId);

    MemReleaseMemBlock (RFC2544_TRAFFIC_PROFILE_POOL (),
                        (UINT1 *) (pTrafficProfile));
    pTrafficProfile = NULL;
    return RFC2544_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : R2544UtilValidateTrafficProfile                       *
 *                                                                          *
 * DESCRIPTION      : This function will validate the Traffic Profile       *
 *                                                                          *
 *                                                                          *
 * INPUT            : u4ContextId - Context Identifier                      *
 *                    u4TrafficProfileId - Traffic Profile                  *
 *                                                                          *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.             *
 *                                                                          *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                             *
 *                                                                          *
 ***************************************************************************/
PUBLIC INT1
R2544UtilValidateTrafficProfile (UINT4 u4ContextId,
                                 UINT4 u4TrafficProfileId, UINT4 *pu4ErrorCode)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtValidateContextInfo (u4ContextId, pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return RFC2544_FAILURE;
    }
    if ((u4TrafficProfileId < RFC2544_MIN_TRAF_PROF_ID) ||
        (u4TrafficProfileId > RFC2544_MAX_TRAF_PROF_ID))
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                     "Invalid Traffic Profile ID \r\n");
        CLI_SET_ERR (CLI_RFC2544_INVALID_TRAF_PROF_ID);

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/*****************************************************************************/
/*              SERVICE ACCEPTANCE CRITERIA UTILITIES                        */
/*****************************************************************************/

/****************************************************************************
* Function Name      : R2544UtilAddSacEntry                                 *
*                                                                           *
* Description        : This routine adds a Sac Entry to  Sac Table          *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SacId - SAC ID                                     *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
INT4
R2544UtilAddSacEntry (UINT4 u4ContextId, UINT4 u4SacId)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = R2544UtilGetSacEntry (u4ContextId, u4SacId);

    if (pSacEntry != NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4ContextId,
                      "Sac entry exists for Sac ID :%d \r\n", u4SacId);
        return RFC2544_SUCCESS;
    }
    pSacEntry = (tSacEntry *) MemAllocMemBlk (RFC2544_SAC_POOL ());
    if (pSacEntry == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                     "Memory allocation for SAC Entry failed\r\n");
        return RFC2544_FAILURE;
    }

    RFC2544_MEMSET (pSacEntry, RFC2544_ZERO, sizeof (tSacEntry));

    pSacEntry->u4ContextId = u4ContextId;
    pSacEntry->u4SacId = u4SacId;

    if (RBTreeAdd (RFC2544_SAC_TABLE (), (tRBElem *) pSacEntry) == RB_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_SAC_POOL (), (UINT1 *) pSacEntry);

        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, u4ContextId,
                      "R2544UtilAddSacEntry: Sac Table Addition Failed for "
                      "u4SacId %d \r\n", u4SacId);
        return RFC2544_FAILURE;
    }

    RFC2544_TRC1 (RFC2544_MGMT_TRC, u4ContextId,
                  "R2544UtilAddSacEntry: Sac Entry is added to "
                  "SAC Table with Id %d \r\n", u4SacId);
    return RFC2544_SUCCESS;

}

/*****************************************************************************
* Function Name      : R2544UtilGetSacEntry                                 *
*                                                                           *
* Description        : This routine gets a SAC Entry                        *
*                      corresponding to the                                 *
*                                                                           *
* Input(s)           : u4ContextId - Context Id                             *
*                      u4SacId - Sac Id                                     *
*                                                                           *
* Output(s)          : pSacEntry - Sac Entry                                *
*                                                                           *
* Return Value(s)    : None                                                 *
*****************************************************************************/
tSacEntry          *
R2544UtilGetSacEntry (UINT4 u4ContextId, UINT4 u4SacId)
{
    tSacEntry          *pSacEntry = NULL;
    tR2544ContextInfo  *pContextInfo = NULL;
    tSacEntry           SacEntry;

    RFC2544_MEMSET (&SacEntry, RFC2544_ZERO, sizeof (tSacEntry));
    pContextInfo = R2544CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "Context entry %d does not exist\r\n", u4ContextId);
        return NULL;
    }

    SacEntry.u4ContextId = u4ContextId;
    SacEntry.u4SacId = u4SacId;

    pSacEntry = (tSacEntry *) RBTreeGet
        (RFC2544_SAC_TABLE (), (tRBElem *) & SacEntry);

    return pSacEntry;
}

/****************************************************************************
* Function Name      : R2544UtilDelSacEntry                                 *
*                                                                           *
* Description        : This routine delete a Entry from Trffic Profile Table*
*                                                                           *
* Input(s)           : pSacEntry - node to be deleted                       *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
INT4
R2544UtilDelSacEntry (tSacEntry * pSacEntry)
{
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry          *pNextSlaEntry = NULL;
    tSacEntry          *pSacNode = NULL;

    if (pSacEntry == NULL)
    {
        return RFC2544_SUCCESS;
    }

    pSacNode =
        R2544UtilGetSacEntry (pSacEntry->u4ContextId, pSacEntry->u4SacId);

    if (pSacNode == NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pSacEntry->u4ContextId,
                      "Sac entry not exists for Sac ID :%d \r\n",
                      pSacEntry->u4SacId);
        return RFC2544_FAILURE;
    }

    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());

    while (pSlaEntry != NULL)
    {
        pNextSlaEntry = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                     (tRBElem *) pSlaEntry,
                                                     NULL);

        /* Check whether SAC is mapped to any SLA */
        if ((pSlaEntry->u4SlaSacId == pSacEntry->u4SacId) &&
            (pSlaEntry->u4ContextId == pSacEntry->u4ContextId))
        {
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, pSacEntry->u4ContextId,
                         "R2544UtilDelSacEntry: sac is mapped to " "SLA\r\n");
            CLI_SET_ERR (CLI_RFC2544_ERR_SAC_MAPPED);
            return RFC2544_FAILURE;
        }

        pSlaEntry = pNextSlaEntry;
    }

    if (RBTreeRemove ((RFC2544_SAC_TABLE ()),
                      (tRBElem *) pSacEntry) == RB_FAILURE)
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, pSacEntry->u4ContextId,
                     "R2544UtilDelSacEntry: RBTree remove failed \r\n");
        return RFC2544_FAILURE;
    }

    RFC2544_TRC1 (RFC2544_MGMT_TRC, pSacEntry->u4ContextId,
                  "R2544UtilDelSacEntry:Sac Entry is deleted from"
                  "SAC Table with Id %d \r\n", pSacEntry->u4SacId);

    MemReleaseMemBlock (RFC2544_SAC_POOL (), (UINT1 *) (pSacEntry));
    pSacEntry = NULL;
    return RFC2544_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : R2544UtilValidateSac                                    *
 *                                                                            *
 * DESCRIPTION      : This function will check whether the context is created *
 *                    the system control of the context and the               *
 *                                                                            *
 *                    This function will be called by the nmhGet/nmhSet       *
 *                    routines and this function will take care of filling the*
 *                    the SNMP error codes, CLI set error and trace messages. *
 *                                                                            *
 * INPUT            : u4ContextId - Context Identifier                        *
 *                    u4SacId - SAC Id                                        *
 *                                                                            *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.               *
 *                                                                            *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                               *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT1
R2544UtilValidateSac (UINT4 u4ContextId, UINT4 u4SacId, UINT4 *pu4ErrorCode)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtValidateContextInfo (u4ContextId, pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return RFC2544_FAILURE;
    }
    if ((u4SacId < RFC2544_MIN_SAC_ID) || (u4SacId > RFC2544_MAX_SAC_ID))
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                     "Invalid SAC ID \r\n");
        CLI_SET_ERR (CLI_RFC2544_INVALID_SAC_ID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return RFC2544_FAILURE;
    }
    return RFC2544_SUCCESS;
}

/****************************************************************************
* Function Name      : R2544UtilDelReportStats                              *
*                                                                           *
* Description        : This routine delete a Entry from Report Stats Table  *
*                                                                           *
* Input(s)           : pReportStatistics - node to be deleted               *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilDelReportStats (tReportStatistics * pReportStatistics)
{

    if (pReportStatistics == NULL)
    {
        return RFC2544_FAILURE;
    }

    if (RBTreeRemove ((RFC2544_REPORT_STATS_TABLE ()),
                      (tRBElem *) pReportStatistics) == RB_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC,
                      pReportStatistics->u4ReportStatsContextId,
                      "R2544UtilDelReportStats: RBTree remove failed \r\n",
                      pReportStatistics->u4ReportStatsSlaId);
        return RFC2544_FAILURE;
    }

    RFC2544_TRC1 (RFC2544_MGMT_TRC, pReportStatistics->u4ReportStatsContextId,
                  "R2544UtilDelReportStats: Report Statistics Entry is deleted \r\n",
                  pReportStatistics->u4ReportStatsSlaId);

    MemReleaseMemBlock (RFC2544_REPORT_STATS_POOL (),
                        (UINT1 *) (pReportStatistics));
    pReportStatistics = NULL;
    return RFC2544_SUCCESS;
}

/*****************************************************************************
* Function Name      : R2544UtilDelThroughputStats                          *
*                                                                           *
* Description        : This routine deletes Entry from ThroughputStats Table*
*                                                                           *
* Input(s)           : pThroughputStats - node to be deleted                *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilDelThroughputStats (tThroughputStats * pThroughputStats)
{
    tThroughputStats   *pThroughputNode = NULL;

    if (pThroughputStats == NULL)
    {
        return RFC2544_FAILURE;
    }

    pThroughputNode =
        R2544UtilGetThroughputStats (pThroughputStats->u4ContextId,
                                     pThroughputStats->u4SlaId,
                                     pThroughputStats->u4FrameSize,
                                     pThroughputStats->u4TrialCount);

    if (pThroughputNode == NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pThroughputStats->u4ContextId,
                      "Sla entry not exists for Sla ID :%d \r\n",
                      pThroughputStats->u4SlaId);
        return RFC2544_FAILURE;
    }

    if (RBTreeRemove ((RFC2544_TH_STATS_TABLE ()),
                      (tRBElem *) pThroughputStats) == RB_FAILURE)
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, pThroughputStats->u4ContextId,
                     "R2544UtilDelThroughputStats: RBTree remove failed \r\n");
        return RFC2544_FAILURE;
    }
    RFC2544_TRC1 (RFC2544_MGMT_TRC, pThroughputStats->u4ContextId,
                  "R2544UtilDelThroughputStats:Throughput Stats Entry is "
                  "deleted", pThroughputStats->u4SlaId);

    MemReleaseMemBlock (RFC2544_TH_STATS_POOL (), (UINT1 *) (pThroughputStats));
    pThroughputStats = NULL;
    return RFC2544_SUCCESS;
}

/****************************************************************************
* Function Name      : R2544UtilDelLatencyStats                             *
*                                                                           *
* Description        : This routine deletes Entry from LatencyStats Table   *
*                                                                           *
* Input(s)           : pLatencyStats - node to be deleted                   *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilDelLatencyStats (tLatencyStats * pLatencyStats)
{

    tLatencyStats      *pLatencyNode = NULL;

    if (pLatencyStats == NULL)
    {
        return RFC2544_FAILURE;
    }

    pLatencyNode =
        R2544UtilGetLatencyStats (pLatencyStats->u4ContextId,
                                  pLatencyStats->u4SlaId,
                                  pLatencyStats->u4FrameSize,
                                  pLatencyStats->u4TrialCount);

    if (pLatencyNode == NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pLatencyStats->u4ContextId,
                      "Sla entry not exists for Sla ID :%d \r\n",
                      pLatencyStats->u4SlaId);
        return RFC2544_FAILURE;
    }

    if (RBTreeRemove ((RFC2544_LA_STATS_TABLE ()),
                      (tRBElem *) pLatencyStats) == RB_FAILURE)
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, 
                     pLatencyStats->u4ContextId,
                     "R2544UtilDelLatencyStats: RBTree remove failed \r\n");
        return RFC2544_FAILURE;
    }
    RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pLatencyStats->u4ContextId,
                  "R2544UtilDelLatencyStats:Latency Stats Entry is deleted",
                  pLatencyStats->u4SlaId);

    MemReleaseMemBlock (RFC2544_LA_STATS_POOL (), (UINT1 *) (pLatencyStats));
    pLatencyStats = NULL;
    return RFC2544_SUCCESS;
}

/*****************************************************************************
* Function Name      : R2544UtilDelFrameLossStats                           *
*                                                                           *
* Description        : This routine deletes Entry from FrameLossStats Table *
*                                                                           *
* Input(s)           : pFrameLossStats - node to be deleted                 *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
INT4
R2544UtilDelFrameLossStats (tFrameLossStats * pFrameLossStats)
{

    tFrameLossStats    *pFrameLossNode = NULL;

    if (pFrameLossStats == NULL)
    {
        return RFC2544_FAILURE;
    }

    pFrameLossNode =
        R2544UtilGetFrameLossStats (pFrameLossStats->u4ContextId,
                                    pFrameLossStats->u4SlaId,
                                    pFrameLossStats->u4FrameSize,
                                    pFrameLossStats->u4TrialCount);

    if (pFrameLossNode == NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pFrameLossStats->u4ContextId,
                      "Sla entry not exists for Sla ID :%d \r\n",
                      pFrameLossStats->u4SlaId);
        return RFC2544_FAILURE;
    }

    if (RBTreeRemove ((RFC2544_FL_STATS_TABLE ()),
                      (tRBElem *) pFrameLossStats) == RB_FAILURE)
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, pFrameLossStats->u4ContextId,
                     "R2544UtilDelFrameLossStats: RBTree remove failed \r\n");
        return RFC2544_FAILURE;
    }
    RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pFrameLossStats->u4ContextId,
                  "R2544UtilDelFrameLossStats:Throughput Stats Entry is deleted",
                  pFrameLossStats->u4SlaId);

    MemReleaseMemBlock (RFC2544_FL_STATS_POOL (), (UINT1 *) (pFrameLossStats));
    pFrameLossStats = NULL;
    return RFC2544_SUCCESS;
}

/****************************************************************************
* Function Name      : R2544UtilDelBackToBackStats                          *
*                                                                           *
* Description        : This routine deletes Entry from BackToBackStats Table*
*                                                                           *
* Input(s)           : pBackToBackStats - node to be deleted                *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
INT4
R2544UtilDelBackToBackStats (tBackToBackStats * pBackToBackStats)
{

    tBackToBackStats   *pBackToBackNode = NULL;

    if (pBackToBackStats == NULL)
    {
        return RFC2544_FAILURE;
    }

    pBackToBackNode =
        R2544UtilGetBackToBackStats (pBackToBackStats->u4ContextId,
                                     pBackToBackStats->u4SlaId,
                                     pBackToBackStats->u4FrameSize,
                                     pBackToBackStats->u4TrialCount);

    if (pBackToBackNode == NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pBackToBackStats->u4ContextId,
                      "Sla entry not exists for Sla ID :%d \r\n",
                      pBackToBackStats->u4SlaId);
        return RFC2544_FAILURE;
    }

    if (RBTreeRemove ((RFC2544_BB_STATS_TABLE ()),
                      (tRBElem *) pBackToBackStats) == RB_FAILURE)
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, pBackToBackStats->u4ContextId,
                     "R2544UtilDelBackToBackStats: RBTree remove failed \r\n");
        return RFC2544_FAILURE;
    }
    RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pBackToBackStats->u4ContextId,
                  "R2544UtilDelBackToBackStats:Throughput Stats Entry is "
                  "deleted", pBackToBackStats->u4SlaId);

    MemReleaseMemBlock (RFC2544_BB_STATS_POOL (), (UINT1 *) (pBackToBackStats));
    pBackToBackStats = NULL;
    return RFC2544_SUCCESS;
}

/*****************************************************************************
* Function Name      : R2544UtilParseValidateFrameSize                      *
*                                                                           *
* Description        : This routine parses the frame size , stores in       *
*                      an array and validates the frame size                *
*                                                                           *
* Input(s)           : u4ContextId - Context Id                             *
*                      u4TrafficProfile ID - Traffic Profile ID             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    :                                                      *
*****************************************************************************/

PUBLIC INT4
R2544UtilParseFrameSize (UINT4 u4ContextId, UINT4 u4TrafficProfileId,
                         tSNMP_OCTET_STRING_TYPE * pTrafficProfileFrameSize)
{

    tTrafficProfile    *pTrafficProfile = NULL;
    UINT4               au4FrameSize[RFC2544_MAX_FRAMESIZE];
    UINT4               u4FrameCount = RFC2544_ZERO;
    UINT4               u4Count = RFC2544_ZERO;
    UINT4               u4FrameCheckCount = RFC2544_ZERO;
    UINT4               u4Frame = RFC2544_ZERO;
    UINT1               au1FrameSize[RFC2544_STRING_MAX_LENGTH];
    UINT1              *au1IndividualFrameSize;

    RFC2544_MEMSET (au4FrameSize, RFC2544_ZERO, sizeof (au4FrameSize));

    RFC2544_MEMCPY (au1FrameSize, pTrafficProfileFrameSize->pu1_OctetList,
                    pTrafficProfileFrameSize->i4_Length);

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4ContextId, u4TrafficProfileId);

    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544UtilValidateFrameSize: "
                      "Traffic profile Entry not present for %d\r\n",
                      u4TrafficProfileId);

        return RFC2544_FAILURE;
    }

    au1IndividualFrameSize = (UINT1 *) RFC2544_STRTOK (au1FrameSize, ",");

    while (au1IndividualFrameSize != NULL)
    {
        u4Frame = (UINT4) (ATOI (au1IndividualFrameSize));
        for (u4FrameCheckCount = 0; u4FrameCheckCount < u4FrameCount;
             u4FrameCheckCount++)
        {
            if (u4Frame == au4FrameSize[u4FrameCheckCount])
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                              "R2544UtilParseFrameSize: "
                              " User should not Repeat the frame size for "
                              "Trafficprofile %d\r\n", u4TrafficProfileId);
                CLI_SET_ERR (CLI_RFC2544_INVALID_REPEAT_FRAME_SIZE);

                return RFC2544_FAILURE;
            }
        }
        au4FrameSize[u4FrameCount] = u4Frame;
        if ((au4FrameSize[u4FrameCount] == RFC2544_FRAME_64) ||
            (au4FrameSize[u4FrameCount] == RFC2544_FRAME_128) ||
            (au4FrameSize[u4FrameCount] == RFC2544_FRAME_256) ||
            (au4FrameSize[u4FrameCount] == RFC2544_FRAME_512) ||
            (au4FrameSize[u4FrameCount] == RFC2544_FRAME_1024) ||
            (au4FrameSize[u4FrameCount] == RFC2544_FRAME_1280) ||
            (au4FrameSize[u4FrameCount] == RFC2544_FRAME_1518) ||
            (au4FrameSize[u4FrameCount] == RFC2544_FRAME_2000) ||
            (au4FrameSize[u4FrameCount] == RFC2544_FRAME_9600))

        {
            u4FrameCount++;
        }
        else
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                          "R2544UtilParseFrameSize: "
                          " User should specify valid frame size format for "
                          "Trafficprofile %d\r\n", u4TrafficProfileId);
            CLI_SET_ERR (CLI_RFC2544_INVALID_FRAME_SIZE);
            return RFC2544_FAILURE;

        }
        au1IndividualFrameSize = (UINT1 *) RFC2544_STRTOK (NULL, ",");
    }
    if (u4FrameCount < RFC2544_MIN_FRAMESIZE)
    {

        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544UtilParseFrameSize: "
                      " User should provide Atleast five frame sizes%d\r\n",
                      u4TrafficProfileId);
        CLI_SET_ERR (CLI_RFC2544_INVALID_MIN_FRAME_SIZE);
        return RFC2544_FAILURE;

    }
    for (u4Count = 0; u4Count < RFC2544_MAX_FRAMESIZE; u4Count++)
    {
        pTrafficProfile->au4TrafProfFrameSize[u4Count][RFC2544_ZERO] =
            RFC2544_ZERO;
    }
    for (u4Count = 0; u4Count <= u4FrameCount; u4Count++)
    {
        pTrafficProfile->au4TrafProfFrameSize[u4Count][RFC2544_ZERO] =
            au4FrameSize[u4Count];
    }
    return RFC2544_SUCCESS;

}

/****************************************************************************
* Function Name      : R2544UtilGetReportEntry                              *
*                                                                           *
* Description        : This routine gets a SLA Entry                        *
*                      corresponding to the                                 *
*                                                                           *
* Input(s)           : u4ContextId - Context Id                             *
*                      u4SlaId - Sla Id                                     *
*                                                                           *
* Output(s)          : pSlaEntry - Sla Entry                                *
*                                                                           *
* Return Value(s)    : pReportStatistics                                    *
*****************************************************************************/
tReportStatistics  *
R2544UtilGetReportStatsEntry (UINT4 u4ContextId, UINT4 u4SlaId,
                              UINT4 u4FrameSize)
{
    tReportStatistics  *pReportStatistics = NULL;
    tReportStatistics   ReportStatistics;

    RFC2544_MEMSET (&ReportStatistics, RFC2544_ZERO,
                    sizeof (tReportStatistics));

    ReportStatistics.u4ReportStatsContextId = u4ContextId;
    ReportStatistics.u4ReportStatsSlaId = u4SlaId;
    ReportStatistics.u4ReportStatsFrameSize = u4FrameSize;

    pReportStatistics = (tReportStatistics *) RBTreeGet
        (RFC2544_REPORT_STATS_TABLE (), (tRBElem *) & ReportStatistics);

    return pReportStatistics;
}

/*****************************************************************************
* Function Name      : R2544UtilAddReportStatsEntry                         *
*                                                                           *
* Description        : This routine adds a Report Stats Entry to            *
*                      Report Stats Table                                   *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId      - Sla Id                                *
*                      u4FrameSize  - Frame Size                            *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilAddReportStatsEntry (UINT4 u4ContextId,
                              UINT4 u4SlaId, UINT4 u4FrameSize)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4ContextId, u4SlaId, u4FrameSize);

    if (pReportStatistics != NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4ContextId,
                      "Report stats  entry exists for SLA ID :%d \r\n",
                      u4SlaId);
        return RFC2544_SUCCESS;
    }
    pReportStatistics = (tReportStatistics *) MemAllocMemBlk
        (RFC2544_REPORT_STATS_POOL ());
    if (pReportStatistics == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                     "Memory allocation for Report Stats entry Addition "
                     "failed\r\n");
        return RFC2544_FAILURE;
    }

    RFC2544_MEMSET (pReportStatistics, RFC2544_ZERO,
                    sizeof (tReportStatistics));

    pReportStatistics->u4ReportStatsSlaId = u4SlaId;
    pReportStatistics->u4ReportStatsContextId = u4ContextId;
    pReportStatistics->u4ReportStatsFrameSize = u4FrameSize;
    pReportStatistics->u1ReportStatsThResult = RFC2544_NOTEXECUTED;
    pReportStatistics->u1ReportStatsLatencyResult = RFC2544_NOTEXECUTED;
    pReportStatistics->u1ReportStatsFrameLossResult = RFC2544_NOTEXECUTED;
    pReportStatistics->u1ReportStatsBbResult = RFC2544_NOTEXECUTED;

    if (RBTreeAdd (RFC2544_REPORT_STATS_TABLE (), (tRBElem *)
                   pReportStatistics) == RB_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_REPORT_STATS_POOL (),
                            (UINT1 *) pReportStatistics);

        RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, u4ContextId,
                      "R2544UtilAddReportStatsEntry: Report Stats Addition "
                      "Failed for sla Id  %d  Frame Size %d\r\n", 
                      u4SlaId, u4FrameSize);
        return RFC2544_FAILURE;
    }

    if(u4FrameSize != RFC2544_ZERO)
    {
        RFC2544_TRC2 (RFC2544_MGMT_TRC, u4ContextId,
                      "R2544UtilAddReportStatsEntry: Report Stats is added for "
                      "Sla Id %d Frame Size %d \r\n", u4SlaId, u4FrameSize);
    }
    return RFC2544_SUCCESS;

}

/****************************************************************************
* Function Name      : R2544UtilAddThroughputStats                          *
*                                                                           *
* Description        : This routine adds a Throughput Stats to the          *
*                      Throughput stats table                               *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA Id                                     *
*                      u4FrameSize - Frame Size                             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilAddThroughputStats (tR2544ReqParams * pR2544ReqParams)
{
    tThroughputStats   *pThroughputStats = NULL;

    if (pR2544ReqParams == NULL)
    {
        RFC2544_GLOBAL_TRC
            ("R2544UtilAddThroughputStats:"
             " Throughput Stats Addition Failed\r\n ");
        return RFC2544_FAILURE;
    }

    pThroughputStats =
        R2544UtilGetThroughputStats (pR2544ReqParams->u4ContextId,
                                     pR2544ReqParams->SlaInfo.u4SlaId,
                                     pR2544ReqParams->u4FrameSize,
                                     pR2544ReqParams->unReqParams.
                                     ThroughputResults.u4TrialCount);

    if (pThroughputStats == NULL)
    {
        pThroughputStats = (tThroughputStats *) MemAllocMemBlk
            (RFC2544_TH_STATS_POOL ());
        if (pThroughputStats == NULL)
        {
            gR2544GlobalInfo.u4MemFailCount++;
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                "Memory allocation for Throughput Stats Entry failed\r\n");
            return RFC2544_FAILURE;
        }

        RFC2544_MEMSET (pThroughputStats, RFC2544_ZERO,
                        sizeof (tThroughputStats));

        pThroughputStats->u4ContextId = pR2544ReqParams->u4ContextId;
        pThroughputStats->u4SlaId = pR2544ReqParams->SlaInfo.u4SlaId;
        pThroughputStats->u4FrameSize = pR2544ReqParams->u4FrameSize;
        pThroughputStats->u4TrialCount =
            pR2544ReqParams->unReqParams.ThroughputResults.u4TrialCount;

        if (RBTreeAdd (RFC2544_TH_STATS_TABLE (), (tRBElem *)
                       pThroughputStats) == RB_FAILURE)
        {
            MemReleaseMemBlock (RFC2544_TH_STATS_POOL (),
                                (UINT1 *) pThroughputStats);

            RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, 
                          pR2544ReqParams->u4ContextId,
                          "R2544UtilAddThroughputStats: Throughput Stats "
                          "Addition Failed for u4SlaId %d FrameSize %d \r\n",
                          pR2544ReqParams->SlaInfo.u4SlaId,
                          pR2544ReqParams->u4FrameSize);
            return RFC2544_FAILURE;
        }

    }
    pThroughputStats->u4TxCount =
        pR2544ReqParams->unReqParams.ThroughputResults.u4TxCount;
    pThroughputStats->u4RxCount =
        pR2544ReqParams->unReqParams.ThroughputResults.u4RxCount;

    pThroughputStats->u4VerifiedThroughput =
        pR2544ReqParams->unReqParams.ThroughputResults.u4Rate;

    R2544CoreGenerateThResult (pThroughputStats->u4ContextId,
                               pThroughputStats->u4SlaId,
                               pThroughputStats->u4FrameSize,
                               pThroughputStats->u4TrialCount);

    RFC2544_TRC2 (RFC2544_INIT_SHUT_TRC | RFC2544_MGMT_TRC, pR2544ReqParams->u4ContextId,
                  "R2544UtilAddThroughputStats: Throughput Stats is added to "
                  "the SLA %d FrameSize %d\r\n", 
                  pR2544ReqParams->SlaInfo.u4SlaId,
                  pR2544ReqParams->u4FrameSize);
    return RFC2544_SUCCESS;

}

/*****************************************************************************
* Function Name      : R2544UtilGetThroughputStats                          *
*                                                                           *
* Description        : This routine gets  Throughput Stats from the         *
*                      Throughput stats table                               *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA Id                                     *
*                      u4FrameSize - Frame Size                             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pThroughputStats                                     *
*****************************************************************************/

tThroughputStats   *
R2544UtilGetThroughputStats (UINT4 u4ContextId, UINT4 u4SlaId,
                             UINT4 u4FrameSize, UINT4 u4TrialCount)
{
    tThroughputStats   *pThroughputStats = NULL;
    tThroughputStats    ThroughputStats;

    RFC2544_MEMSET (&ThroughputStats, RFC2544_ZERO, sizeof (tThroughputStats));

    ThroughputStats.u4ContextId = u4ContextId;
    ThroughputStats.u4SlaId = u4SlaId;
    ThroughputStats.u4FrameSize = u4FrameSize;
    ThroughputStats.u4TrialCount = u4TrialCount;

    pThroughputStats = (tThroughputStats *) RBTreeGet
        (RFC2544_TH_STATS_TABLE (), (tRBElem *) & ThroughputStats);

    return pThroughputStats;
}

/****************************************************************************
* Function Name      : R2544UtilAddLatencyStats                             *
*                                                                           *
* Description        : This routine adds a Latency Stats to the             *
*                      Latency stats table                                  *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA Id                                     *
*                      u4FrameSize - Frame Size                             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilAddLatencyStats (tR2544ReqParams * pR2544ReqParams)
{
    tLatencyStats      *pLatencyStats = NULL;
    UINT4               u4TrafficProfileId = 0;
    UINT4               u4TrialCount = 0;

    if (pR2544ReqParams == NULL)
    {
        RFC2544_GLOBAL_TRC
            ("R2544UtilAddLatencyStats: Latency Stats Addition Failed\r\n ");
        return RFC2544_FAILURE;
    }

    if (pR2544ReqParams->unReqParams.LatencyResults.u4TrialCount >=
        RFC2544_LATENCY_MAX_TRIAL)
    {
        u4TrialCount =
            pR2544ReqParams->unReqParams.LatencyResults.u4TrialCount %
            RFC2544_LATENCY_MAX_TRIAL;
    }
    else
    {
        u4TrialCount = pR2544ReqParams->unReqParams.LatencyResults.u4TrialCount;
    }

    pLatencyStats =
        R2544UtilGetLatencyStats (pR2544ReqParams->u4ContextId,
                                  pR2544ReqParams->SlaInfo.u4SlaId,
                                  pR2544ReqParams->u4FrameSize, u4TrialCount);

    if (pLatencyStats == NULL)
    {
        pLatencyStats = (tLatencyStats *) MemAllocMemBlk
            (RFC2544_LA_STATS_POOL ());
        if (pLatencyStats == NULL)
        {
            gR2544GlobalInfo.u4MemFailCount++;
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                        "Memory allocation for Latency Stats Entry failed\r\n");
            return RFC2544_FAILURE;
        }

        RFC2544_MEMSET (pLatencyStats, RFC2544_ZERO, sizeof (tLatencyStats));

        pLatencyStats->u4ContextId = pR2544ReqParams->u4ContextId;
        pLatencyStats->u4SlaId = pR2544ReqParams->SlaInfo.u4SlaId;
        pLatencyStats->u4FrameSize = pR2544ReqParams->u4FrameSize;
        pLatencyStats->u4TrialCount = u4TrialCount;

        if (RBTreeAdd (RFC2544_LA_STATS_TABLE (), (tRBElem *)
                       pLatencyStats) == RB_FAILURE)
        {
            MemReleaseMemBlock (RFC2544_LA_STATS_POOL (),
                                (UINT1 *) pLatencyStats);

            RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, pR2544ReqParams->u4ContextId,
                          "R2544UtilAddLatencyStats: Latency Stats "
                          "Addition Failed for u4SlaId %d FrameSize %d \r\n",
                          pR2544ReqParams->SlaInfo.u4SlaId,
                          pR2544ReqParams->u4FrameSize);
            return RFC2544_FAILURE;
        }

    }
    pLatencyStats->u4LatencyMin =
        pR2544ReqParams->unReqParams.LatencyResults.u4LatencyMin;
    pLatencyStats->u4LatencyMean =
        pR2544ReqParams->unReqParams.LatencyResults.u4LatencyMean;
    pLatencyStats->u4LatencyMax =
        pR2544ReqParams->unReqParams.LatencyResults.u4LatencyMax;
    pLatencyStats->u4TxCount =
        pR2544ReqParams->unReqParams.LatencyResults.u4TxCount;
    pLatencyStats->u4RxCount =
        pR2544ReqParams->unReqParams.LatencyResults.u4RxCount;

    if (nmhGetFs2544SlaTrafficProfileId (pLatencyStats->u4ContextId,
                                         pLatencyStats->u4SlaId,
                                         &u4TrafficProfileId) == SNMP_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                      "R2544UtilAddLatencyStats: Traffic profile does not "
                      "exist for SLA %d FrameSize %d\r\n",
                      pR2544ReqParams->SlaInfo.u4SlaId);

    }

    if (RFC2544_LATENCY_MAX_TRIAL == u4TrialCount + RFC2544_ONE)
    {
        R2544CoreGenerateLaResult (pLatencyStats->u4ContextId,
                                   pLatencyStats->u4SlaId,
                                   pLatencyStats->u4FrameSize);
    }

    RFC2544_TRC2 (RFC2544_INIT_SHUT_TRC | RFC2544_MGMT_TRC, 
                  pR2544ReqParams->u4ContextId,
                  "R2544UtilAddLatencyStats: Latency Stats is added to the"
                  " SLA %d FrameSize %d\r\n", pR2544ReqParams->SlaInfo.u4SlaId,
                  pR2544ReqParams->u4FrameSize);
    return RFC2544_SUCCESS;

}

/*****************************************************************************
* Function Name      : R2544UtilGetLatencyStats                             *
*                                                                           *
* Description        : This routine gets  Latency Stats from the            *
*                      Latency stats table                                  *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA Id                                     *
*                      u4FrameSize - Frame Size                             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pLatencyStats                                        *
*****************************************************************************/

tLatencyStats      *
R2544UtilGetLatencyStats (UINT4 u4ContextId, UINT4 u4SlaId,
                          UINT4 u4FrameSize, UINT4 u4TrialCount)
{
    tLatencyStats      *pLatencyStats = NULL;
    tLatencyStats       LatencyStats;

    RFC2544_MEMSET (&LatencyStats, RFC2544_ZERO, sizeof (tLatencyStats));

    LatencyStats.u4ContextId = u4ContextId;
    LatencyStats.u4SlaId = u4SlaId;
    LatencyStats.u4FrameSize = u4FrameSize;
    LatencyStats.u4TrialCount = u4TrialCount;

    pLatencyStats = (tLatencyStats *) RBTreeGet
        (RFC2544_LA_STATS_TABLE (), (tRBElem *) & LatencyStats);

    return pLatencyStats;
}

/****************************************************************************
* Function Name      : R2544UtilAddFrameLossStats                           *
*                                                                           *
* Description        : This routine adds a FrameLoss Stats to the           *
*                      FrameLoss stats table                                *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA Id                                     *
*                      u4FrameSize - Frame Size                             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilAddFrameLossStats (tR2544ReqParams * pR2544ReqParams)
{
    tFrameLossStats    *pFrameLossStats = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    UINT4               u4TrafficProfileId;

    if (pR2544ReqParams == NULL)
    {
        RFC2544_GLOBAL_TRC
            ("R2544UtilAddFrameLossStats: FrameLoss Stats Addition Failed\r\n ");
        return RFC2544_FAILURE;
    }

    pFrameLossStats =
        R2544UtilGetFrameLossStats (pR2544ReqParams->u4ContextId,
                                    pR2544ReqParams->SlaInfo.u4SlaId,
                                    pR2544ReqParams->u4FrameSize,
                                    pR2544ReqParams->unReqParams.
                                    FrameLossResults.u4TrialCount);

    if (pFrameLossStats == NULL)
    {
        pFrameLossStats = (tFrameLossStats *) MemAllocMemBlk
            (RFC2544_FL_STATS_POOL ());
        if (pFrameLossStats == NULL)
        {
            gR2544GlobalInfo.u4MemFailCount++;
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                         "Memory allocation for FrameLoss "
                         "Stats Entry failed\r\n");
            return RFC2544_FAILURE;
        }

        RFC2544_MEMSET (pFrameLossStats, RFC2544_ZERO,
                        sizeof (tFrameLossStats));

        pFrameLossStats->u4ContextId = pR2544ReqParams->u4ContextId;
        pFrameLossStats->u4SlaId = pR2544ReqParams->SlaInfo.u4SlaId;
        pFrameLossStats->u4FrameSize = pR2544ReqParams->u4FrameSize;
        pFrameLossStats->u4TrialCount =
            pR2544ReqParams->unReqParams.FrameLossResults.u4TrialCount;

        if (RBTreeAdd (RFC2544_FL_STATS_TABLE (), (tRBElem *)
                       pFrameLossStats) == RB_FAILURE)
        {
            MemReleaseMemBlock (RFC2544_FL_STATS_POOL (),
                                (UINT1 *) pFrameLossStats);

            RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, 
                          pR2544ReqParams->u4ContextId,
                          "R2544UtilAddFrameLossStats: FrameLoss Stats "
                          "Addition Failed for u4SlaId %d FrameSize %d \r\n",
                          pR2544ReqParams->SlaInfo.u4SlaId,
                          pR2544ReqParams->u4FrameSize);
            return RFC2544_FAILURE;
        }

    }
    pFrameLossStats->u4TxCount =
        pR2544ReqParams->unReqParams.FrameLossResults.u4TxCount;
    pFrameLossStats->u4RxCount =
        pR2544ReqParams->unReqParams.FrameLossResults.u4RxCount;

    if (nmhGetFs2544SlaTrafficProfileId (pFrameLossStats->u4ContextId,
                                         pFrameLossStats->u4SlaId,
                                         &u4TrafficProfileId) == SNMP_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                      "R2544UtilAddFrameLossStats: Traffic profile does not "
                      "exist for SLA %d FrameSize %d\r\n",
                      pR2544ReqParams->SlaInfo.u4SlaId);

    }
    pTrafficProfile = R2544UtilGetTrafProfEntry (pFrameLossStats->u4ContextId,
                                                 u4TrafficProfileId);

    if (pTrafficProfile == NULL)
    {
          RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pFrameLossStats->u4ContextId,
                      "R2544UtilAddFrameLossStats: "
                      "Traffic profile entry not present for SLA %d\r\n",
                      u4TrafficProfileId);
        return RFC2544_FAILURE;

    }
      /*  if (pTrafficProfile->u4TrafProfFlMaxTrial ==
            pR2544ReqParams->unReqParams.FrameLossResults.u4TrialCount +
            RFC2544_ONE)*/
        
    R2544CoreGenerateFlResult (pFrameLossStats->u4ContextId,
                               pFrameLossStats->u4SlaId,
                               pFrameLossStats->u4FrameSize,
                               pR2544ReqParams->unReqParams.FrameLossResults.u4TrialCount);
        
    

    RFC2544_TRC2 (RFC2544_INIT_SHUT_TRC | RFC2544_MGMT_TRC, pR2544ReqParams->u4ContextId,
                  "R2544UtilAddFrameLossStats: FrameLoss Stats is added to the"
                  " SLA %d FrameSize %d\r\n", pR2544ReqParams->SlaInfo.u4SlaId,
                  pR2544ReqParams->u4FrameSize);
    return RFC2544_SUCCESS;

}

/*****************************************************************************
* Function Name      : R2544UtilGetFrameLossStats                           *
*                                                                           *
* Description        : This routine gets  FrameLoss Stats from the          *
*                      FrameLoss stats table                                *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA Id                                     *
*                      u4FrameSize - Frame Size                             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pFrameLossStats                                      *
*****************************************************************************/

tFrameLossStats    *
R2544UtilGetFrameLossStats (UINT4 u4ContextId, UINT4 u4SlaId,
                            UINT4 u4FrameSize, UINT4 u4TrialCount)
{
    tFrameLossStats    *pFrameLossStats = NULL;
    tFrameLossStats     FrameLossStats;

    RFC2544_MEMSET (&FrameLossStats, RFC2544_ZERO, sizeof (tFrameLossStats));

    FrameLossStats.u4ContextId = u4ContextId;
    FrameLossStats.u4SlaId = u4SlaId;
    FrameLossStats.u4FrameSize = u4FrameSize;
    FrameLossStats.u4TrialCount = u4TrialCount;

    pFrameLossStats = (tFrameLossStats *) RBTreeGet
        (RFC2544_FL_STATS_TABLE (), (tRBElem *) & FrameLossStats);

    return pFrameLossStats;
}

/****************************************************************************
* Function Name      : R2544UtilAddBackToBackStats                          *
*                                                                           *
* Description        : This routine adds a BackToBack Stats to the          *
*                      BackToBack stats table                               *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA Id                                     *
*                      u4FrameSize - Frame Size                             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilAddBackToBackStats (tR2544ReqParams * pR2544ReqParams)
{
    tBackToBackStats   *pBackToBackStats = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    UINT4               u4TrafficProfileId;

    if (pR2544ReqParams == NULL)
    {
        RFC2544_GLOBAL_TRC
            ("R2544UtilAddBackToBackStats: BackToBack Stats "
             "Addition Failed\r\n ");
        return RFC2544_FAILURE;
    }

    pBackToBackStats =
        R2544UtilGetBackToBackStats (pR2544ReqParams->u4ContextId,
                                     pR2544ReqParams->SlaInfo.u4SlaId,
                                     pR2544ReqParams->u4FrameSize,
                                     pR2544ReqParams->unReqParams.
                                     BackToBackResults.u4TrialCount);

    if (pBackToBackStats == NULL)
    {
        pBackToBackStats = (tBackToBackStats *) MemAllocMemBlk
            (RFC2544_BB_STATS_POOL ());
        if (pBackToBackStats == NULL)
        {
            gR2544GlobalInfo.u4MemFailCount++;
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                         "Memory allocation for BackToBack Stats "
                         "Entry failed\r\n");
            return RFC2544_FAILURE;
        }

        RFC2544_MEMSET (pBackToBackStats, RFC2544_ZERO,
                        sizeof (tBackToBackStats));

        pBackToBackStats->u4ContextId = pR2544ReqParams->u4ContextId;
        pBackToBackStats->u4SlaId = pR2544ReqParams->SlaInfo.u4SlaId;
        pBackToBackStats->u4FrameSize = pR2544ReqParams->u4FrameSize;
        pBackToBackStats->u4TrialCount =
            pR2544ReqParams->unReqParams.BackToBackResults.u4TrialCount;

        if (RBTreeAdd (RFC2544_BB_STATS_TABLE (), (tRBElem *)
                       pBackToBackStats) == RB_FAILURE)
        {
            MemReleaseMemBlock (RFC2544_BB_STATS_POOL (),
                                (UINT1 *) pBackToBackStats);

            RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, 
                          pR2544ReqParams->u4ContextId,
                          "R2544UtilAddBackToBackStats: BackToBack Stats "
                          "Addition Failed for u4SlaId %d FrameSize %d \r\n",
                          pR2544ReqParams->SlaInfo.u4SlaId,
                          pR2544ReqParams->u4FrameSize);
            return RFC2544_FAILURE;
        }

    }
    /* For HW populate with burst Size */
    /* pBackToBackStats->u4BurstSize = 
     * pR2544ReqParams->unReqParams.BackToBackResults.u4BurstSize; */

    /* Since test is done in npsim and packets cannot be sent in 
     * Burst Tx count is taken */
    if (pR2544ReqParams->unReqParams.BackToBackResults.u4TxCount ==
        pR2544ReqParams->unReqParams.BackToBackResults.u4RxCount)
    {
        pBackToBackStats->u4BurstSize =
            pR2544ReqParams->unReqParams.BackToBackResults.u4RxCount;
    }
    if (nmhGetFs2544SlaTrafficProfileId (pBackToBackStats->u4ContextId,
                                         pBackToBackStats->u4SlaId,
                                         &u4TrafficProfileId) == SNMP_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                      "R2544UtilAddBackToBackStats: Traffic profile does not "
                      "exist for "
                      "SLA %d FrameSize %d\r\n",
                      pR2544ReqParams->SlaInfo.u4SlaId);

    }
    pTrafficProfile = R2544UtilGetTrafProfEntry (pBackToBackStats->u4ContextId,
                                                 u4TrafficProfileId);

    if (pTrafficProfile != NULL)
    {
        if (pTrafficProfile->u4TrafProfBbTrialCount ==
            pR2544ReqParams->unReqParams.BackToBackResults.u4TrialCount +
            RFC2544_ONE)
        {
            R2544CoreGenerateBbResult (pBackToBackStats->u4ContextId,
                                       pBackToBackStats->u4SlaId,
                                       pBackToBackStats->u4FrameSize);
        }
    }

    RFC2544_TRC2 (RFC2544_INIT_SHUT_TRC | RFC2544_MGMT_TRC, 
                  pR2544ReqParams->u4ContextId,
                  "R2544UtilAddBackToBackStats: BackToBack Stats is added to the"
                  " SLA %d FrameSize %d\r\n", pR2544ReqParams->SlaInfo.u4SlaId,
                  pR2544ReqParams->u4FrameSize);
    return RFC2544_SUCCESS;

}

/****************************************************************************
* Function Name      : R2544UtilGetBackToBackStats                          *
*                                                                           *
* Description        : This routine gets  BackToBack Stats from the         *
*                      BackToBack stats table                               *
*                                                                           *
* Input(s)           : u4ContextId  - Context Id                            *
*                      u4SlaId - SLA Id                                     *
*                      u4FrameSize - Frame Size                             *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : pBackToBackStats                                     *
*****************************************************************************/

tBackToBackStats   *
R2544UtilGetBackToBackStats (UINT4 u4ContextId, UINT4 u4SlaId,
                             UINT4 u4FrameSize, UINT4 u4TrialCount)
{
    tBackToBackStats   *pBackToBackStats = NULL;
    tBackToBackStats    BackToBackStats;

    RFC2544_MEMSET (&BackToBackStats, RFC2544_ZERO, sizeof (tBackToBackStats));

    BackToBackStats.u4ContextId = u4ContextId;
    BackToBackStats.u4SlaId = u4SlaId;
    BackToBackStats.u4FrameSize = u4FrameSize;
    BackToBackStats.u4TrialCount = u4TrialCount;

    pBackToBackStats = (tBackToBackStats *) RBTreeGet
        (RFC2544_BB_STATS_TABLE (), (tRBElem *) & BackToBackStats);

    return pBackToBackStats;
}

/*****************************************************************************
* Function Name      : R2544UtilUnmapTrafSacFromSla                         *
*                                                                           *
* Description        : This function Unmaps the Traffic profile and Sac     *
*                      entries from the SLA table                           *
*                                                                           *
* Input(s)           : u4ContextId - Context identifier                     *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : NONE                                                 *
*****************************************************************************/

PUBLIC VOID
R2544UtilUnmapTrafSacFromSla (UINT4 u4ContextId)
{
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry          *pNextSlaEntry = NULL;

    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());

    while (pSlaEntry != NULL)
    {
        /* Delete all the nodes */
        pNextSlaEntry = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                     (tRBElem *) pSlaEntry,
                                                     NULL);
        if (pSlaEntry->u4ContextId == u4ContextId)
        {
            pSlaEntry->u4SlaSacId = RFC2544_ZERO;
            pSlaEntry->u4SlaTrafProfId = RFC2544_ZERO;
        }
        pSlaEntry = pNextSlaEntry;
    }
    return;
}

/****************************************************************************
* Function Name      : R2544UtilUpdateTestParams                            *
*                                                                           *
* Description        : This routine updates the test parameters             *
*                                                                           *
* Input(s)           : pR2544ReqParams - pointer to the request params      *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilUpdateTestParams (tR2544ReqParams * pR2544ReqParams)
{
    tSlaEntry          *pSlaEntry = NULL;

    if (pR2544ReqParams == NULL)
    {
        RFC2544_GLOBAL_TRC
            ("R2544UtilUpdateTestParams: Test Parameters updation failed\r\n ");
        return RFC2544_FAILURE;
    }

    pSlaEntry =
        R2544UtilGetSlaEntry (pR2544ReqParams->u4ContextId,
                              pR2544ReqParams->SlaInfo.u4SlaId);

    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                      "SLA Entry does not exit for Sla %d \r\n",
                      pR2544ReqParams->SlaInfo.u4SlaId);
        return RFC2544_FAILURE;
    }

    pSlaEntry->u4IfIndex = pR2544ReqParams->TestParams.u4IfIndex;
    pSlaEntry->u4TagType = pR2544ReqParams->TestParams.u4TagType;
    pSlaEntry->u4PortSpeed = pR2544ReqParams->TestParams.u4PortSpeed;
    pSlaEntry->OutVlanId = pR2544ReqParams->TestParams.OutVlanId;
    RFC2544_MEMCPY (pSlaEntry->TxSrcMacAddr,
                    pR2544ReqParams->TestParams.TxSrcMacAddr,
                    sizeof (tMacAddr));

    RFC2544_MEMCPY (pSlaEntry->TxDstMacAddr,
                    pR2544ReqParams->TestParams.TxDstMacAddr,
                    sizeof (tMacAddr));

    RFC2544_TRC1 (RFC2544_Y1731_INTF_TRC | RFC2544_MGMT_TRC, 
                  pR2544ReqParams->u4ContextId,
                  "R2544UtilUpdateTestParams: Test Parameters are updated "
                  "successfully for SLA %d \r\n", pR2544ReqParams->SlaInfo.u4SlaId);
    return RFC2544_SUCCESS;

}

/*****************************************************************************
* Function Name      : R2544UtilIsSacMappedToSla                            *
*                                                                           *
* Description        : This function Checks whether the Sac is mapped to    *
*                      Any SLA                                              *
*                                                                           *
* Input(s)           : u4ContextId - Context identifier                     *
*                      u4SacId  - Sac Id                                    *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/

tSlaEntry          *
R2544UtilIsSacMappedToSla (UINT4 u4ContextId, UINT4 u4SacId)
{
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry          *pNextSlaEntry = NULL;

    UNUSED_PARAM (u4ContextId);

    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());

    while (pSlaEntry != NULL)
    {
        pNextSlaEntry = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                     (tRBElem *) pSlaEntry,
                                                     NULL);

        /* Check whether SAC is mapped to any SLA */
        if ((pSlaEntry->u4SlaSacId == u4SacId) &&
            (pSlaEntry->u4ContextId == u4ContextId))
        {
            return pSlaEntry;
        }

        pSlaEntry = pNextSlaEntry;
    }
    return NULL;
}

/****************************************************************************
* Function Name      : R2544UtilIsTrafProfMappedToSla                       *
*                                                                           *
* Description        : This function Checks whether the traffic Profile     *
*                       is mapped to Any SLA                                *
*                                                                           *
* Input(s)           : u4ContextId - Context identifier                     *
*                      u4TrafficProfileId  - Traffic profile Id             *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/

tSlaEntry          *
R2544UtilIsTrafProfMappedToSla (UINT4 u4ContextId, UINT4 u4TrafficProfileId)
{
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry          *pNextSlaEntry = NULL;
    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());
    while (pSlaEntry != NULL)
    {
        pNextSlaEntry = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                     (tRBElem *) pSlaEntry,
                                                     NULL);

        /* Check whether traffic profile is mapped to any SLA */
        if ((pSlaEntry->u4SlaTrafProfId == u4TrafficProfileId) &&
            (pSlaEntry->u4ContextId == u4ContextId))
        {
            return pSlaEntry;
        }

        pSlaEntry = pNextSlaEntry;
    }
    return NULL;
}

/*****************************************************************************
* Function Name      : R2544UtilIsTrafProfMappedToSla                       *
*                                                                           *
* Description        : This function handles the CFM signal status for a    *
*                      MEP                                                  *
*                                                                           *
* Input(s)           : pR2544ReqParams - pointer to the request params      *
*                                                                           *
* Output(s)          : NONE                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilHandleSignalMsg (tR2544ReqParams * pR2544ReqParams)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (pR2544ReqParams->u4ContextId,
                                      pR2544ReqParams->SlaInfo.u4SlaId);

    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pR2544ReqParams->u4ContextId,
                      "SLA Entry does not exit for Sla %d \r\n",
                      pR2544ReqParams->SlaInfo.u4SlaId);
        return RFC2544_FAILURE;
    }

    if ((pR2544ReqParams->u4ReqType == ECFM_DEFECT_CONDITION_ENCOUNTERED)
        || (pR2544ReqParams->u4ReqType == ECFM_RDI_CONDITION_ENCOUNTERED))
    {

        if ((pSlaEntry->u4SlaMep == pR2544ReqParams->SlaInfo.u4MEP) &&
            (pSlaEntry->u4SlaMeg == pR2544ReqParams->SlaInfo.u4MEG) &&
            (pSlaEntry->u4SlaMe == pR2544ReqParams->SlaInfo.u4ME))
        {
            if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)
            {
                pSlaEntry->u1SlaCurrentTestState = RFC2544_ABORTED;

                if (R2544CoreStopSlaTest (pSlaEntry) != RFC2544_SUCCESS)
                {
                    RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC,
                                  pR2544ReqParams->u4ContextId,
                                  "Failed to stop Test for Sla %d \r\n",
                                  pR2544ReqParams->SlaInfo.u4SlaId);
                    return RFC2544_FAILURE;
                }
                R2544SendTrapNotifications (pSlaEntry,
                                            RFC2544_TRANSACTION_FAIL);
            }

        }
    }
    return RFC2544_SUCCESS;

}

/****************************************************************************
* Function Name      : R2544UtilValidateSlaParams                           *
*                                                                           *
* Description        : This routine checks whether MEP/ME/MEG is already    *
*                      mapped to any SLA                                    *
*                                                                           *
* Input(s)           : pSlaEntry - Sla node pointer                         *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilValidateSlaParams (tSlaEntry * pSlaEntry)
{

    tSlaEntry          *pSlaInfo = NULL;
    tSlaEntry          *pNextSlaInfo = NULL;

    if ((pSlaEntry->u4SlaMep == RFC2544_ZERO)
        || (pSlaEntry->u4SlaMeg == RFC2544_ZERO)
        || (pSlaEntry->u4SlaMe == RFC2544_ZERO))
    {
        return RFC2544_SUCCESS;

    }
    else
    {
        pSlaInfo = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());
        while (pSlaInfo != NULL)
        {
            pNextSlaInfo = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                        (tRBElem *) pSlaInfo,
                                                        NULL);
            if (((pSlaInfo->u4SlaId != pSlaEntry->u4SlaId)
                 && (pSlaInfo->u4ContextId != pSlaEntry->u4ContextId))
                || ((pSlaInfo->u4SlaId == pSlaEntry->u4SlaId)
                    && (pSlaInfo->u4ContextId != pSlaEntry->u4ContextId))
                || ((pSlaInfo->u4SlaId != pSlaEntry->u4SlaId)
                    && (pSlaInfo->u4ContextId == pSlaEntry->u4ContextId)))
            {
                if ((pSlaEntry->u4SlaMep == pSlaInfo->u4SlaMep)
                    && (pSlaEntry->u4SlaMeg == pSlaInfo->u4SlaMeg)
                    && (pSlaEntry->u4SlaMe == pSlaInfo->u4SlaMe))

                {
                    nmhSetFs2544SlaMEG (pSlaEntry->u4ContextId,
                                        pSlaEntry->u4SlaId, RFC2544_ZERO);
                    nmhSetFs2544SlaME (pSlaEntry->u4ContextId,
                                       pSlaEntry->u4SlaId, RFC2544_ZERO);
                    nmhSetFs2544SlaMEP (pSlaEntry->u4ContextId,
                                        pSlaEntry->u4SlaId, RFC2544_ZERO);
                    return RFC2544_FAILURE;

                }
            }
            pSlaInfo = pNextSlaInfo;

        }

    }

    return RFC2544_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : R2544UtilValidateMepInfo                              *
 *                                                                          *
 * DESCRIPTION      : This function will check whether the configured MEP is* 
 *                    existing in ECFM?Y1731                                *
 *                                                                          *
 * INPUT            :                                                       *
 *                    u4SlaId - SLA Id                                      *
 *                                                                          *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.             *
 *                                                                          *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                             *
 *                                                                          *
 ***************************************************************************/
PUBLIC INT1                                                                 
R2544UtilValidateMepInfo (tSlaEntry * pSlaEntry)                            
{                                                                           
    tR2544ExtInParams  *pR2544ExtInParams = NULL;                           
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;

    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC ("Memory allocation" "failed \r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_GLOBAL_TRC ("Memory allocation failed \r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));

    if ((pSlaEntry->u4SlaMeg != RFC2544_ZERO)
        && (pSlaEntry->u4SlaMe != RFC2544_ZERO)
        && (pSlaEntry->u4SlaMep != RFC2544_ZERO))
    {
        pR2544ExtInParams->eExtReqType = R2544_REQ_CFM_MEP_INDEX_VALIDATION;
        pR2544ExtInParams->EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
        pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId =
            pSlaEntry->u4SlaId;
        pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEGId =
            pSlaEntry->u4SlaMeg;
        pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEId =
            pSlaEntry->u4SlaMe;
        pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId =
            pSlaEntry->u4SlaMep;

        if (R2544PortHandleExtInteraction
            (pR2544ExtInParams, pR2544ExtOutParams) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                                (UINT1 *) pR2544ExtInParams);
            MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                                (UINT1 *) pR2544ExtOutParams);
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                          "R2544UtilValidateMepInfo: MEP/ME/MEG configured "
                          "for Sla %d  does not exist \r\n",
                          pSlaEntry->u4SlaId);
            CLI_SET_ERR (CLI_RFC2544_SERVICE_NOT_PRESENT);

            return RFC2544_FAILURE;
        }
    }
    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);

    return RFC2544_SUCCESS;
}

/*****************************************************************************
* Function Name      : R2544UtilValidateSlaRate                             *
*                                                                           *
* Description        : This routine validates the MinRate/MaxRate/RateStep  *
*                      for Throughput/FrameLoss test                        *
*                                                                           *
* Input(s)           : u4ContextId - Context Identifier                     *
*                      u4TrafficProfileId - Traffic Profile INdentifier     *
*                                                                           *
* Output(s)          : None                                                 *
*                                                                           *
* Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                      *
*****************************************************************************/
PUBLIC INT4
R2544UtilValidateRate (UINT4 u4ContextId, UINT4 u4TrafficProfileId)
{

    tTrafficProfile    *pTrafficProfile = NULL;
    UINT4               u4TrialCount = RFC2544_ZERO;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4ContextId, u4TrafficProfileId);

    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544UtilValidateRate: "
                      "Traffic profile entry not present for SLA %d\r\n",
                      u4TrafficProfileId);
        return RFC2544_FAILURE;

    }
    if (pTrafficProfile->u1TrafProfThTestStatus == RFC2544_ENABLED)
    {

        if (pTrafficProfile->u4TrafProfThMinRate >
            pTrafficProfile->u4TrafProfThMaxRate)
        {

            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                          "R2544UtilValidateRate: "
                          "Min rate Should not be greater than the max rate for"
                          " Throughput test in Traffic profile %d\r\n",
                          u4TrafficProfileId);
            CLI_SET_ERR (CLI_RFC2544_ERR_TH_MIN_RATE_GREATER_THAN_MAX_RATE);
            pTrafficProfile->u4TrafProfThMinRate = RFC2544_MIN_RATE;
            pTrafficProfile->u4TrafProfThMaxRate = RFC2544_MAX_RATE;
            pTrafficProfile->u4TrafProfThRateStep = RFC2544_TH_RATESTEP;
            return RFC2544_FAILURE;

        }
        else if (pTrafficProfile->u4TrafProfThMinRate ==
                 pTrafficProfile->u4TrafProfThMaxRate)
        {
            if(RFC2544_ZERO != pTrafficProfile->u4TrafProfThRateStep)
            {
                 RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                 "R2544UtilValidateRate: "
                 "When Min rate equal to max rate then rate step should "
                 "be ZERO for Throughput test in Traffic profile %d\r\n",
                 u4TrafficProfileId);
                 CLI_SET_ERR (CLI_RFC2544_NON_ZERO_RATE_STEP_ERR); 
                 pTrafficProfile->u4TrafProfThMinRate = RFC2544_MIN_RATE; 
                 pTrafficProfile->u4TrafProfThMaxRate = RFC2544_MAX_RATE; 
                 pTrafficProfile->u4TrafProfThRateStep = RFC2544_TH_RATESTEP; 
                 return RFC2544_FAILURE; 
            }
        }
        else  
        { 
                if(RFC2544_ZERO == pTrafficProfile->u4TrafProfThRateStep) 
                {        
                        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId, 
                          "R2544UtilValidateRate: " 
                          "When Min rate is less than  max rate then rate step should " 
                          "not be ZERO for Throughput test in Traffic profile %d\r\n", 
                          u4TrafficProfileId); 
                        CLI_SET_ERR (CLI_RFC2544_ZERO_RATE_STEP_ERR);
                        pTrafficProfile->u4TrafProfThMinRate = RFC2544_MIN_RATE;
                        pTrafficProfile->u4TrafProfThMaxRate = RFC2544_MAX_RATE;
                        pTrafficProfile->u4TrafProfThRateStep = RFC2544_TH_RATESTEP;
                        return RFC2544_FAILURE;
               }
        }
    }
    u4TrialCount = 0;
    if (pTrafficProfile->u1TrafProfFlTestStatus == RFC2544_ENABLED)
    {
        if (pTrafficProfile->u4TrafProfFlMinRate >=
            pTrafficProfile->u4TrafProfFlMaxRate)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                          "R2544UtilValidateRate: "
                          "Min rate Should not be greater  than the max rate "
                          "for FrameLoss test in Traffic profile Id %d\r\n",
                          u4TrafficProfileId);
            CLI_SET_ERR (CLI_RFC2544_INVALID_MIN_RATE_ERR_GREATER);
            pTrafficProfile->u4TrafProfFlRateStep = RFC2544_FL_RATESTEP;
            pTrafficProfile->u4TrafProfFlMinRate = RFC2544_MIN_RATE;
            pTrafficProfile->u4TrafProfFlMaxRate = RFC2544_MAX_RATE;
            return RFC2544_FAILURE;

        }

        u4TrialCount =
            ((pTrafficProfile->u4TrafProfFlMaxRate -
              pTrafficProfile->u4TrafProfFlMinRate) /
             pTrafficProfile->u4TrafProfFlRateStep) + 1;

        if (u4TrialCount < RFC2544_TWO)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                          "R2544UtilValidateRate: Rate Step should be "
                          "configured such that there is  atleast two  trials "
                          "for Frameloss Test in  Traffic Profile Id%d\r\n",
                          u4TrafficProfileId);
            CLI_SET_ERR (CLI_RFC2544_INVALID_MIN_RATE_ERR);
            pTrafficProfile->u4TrafProfFlRateStep = RFC2544_FL_RATESTEP;
            pTrafficProfile->u4TrafProfFlMinRate = RFC2544_MIN_RATE;
            pTrafficProfile->u4TrafProfFlMaxRate = RFC2544_MAX_RATE;
            return RFC2544_FAILURE;
        }
    }
    return RFC2544_SUCCESS;
}
