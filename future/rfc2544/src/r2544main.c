/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: r2544main.c,v 1.3 2016/05/04 12:21:01 siva Exp $
*
* Description: This file contains the init and de-init routines for 
*              RFC2544 mPool and Timers.
* 
*****************************************************************************/

#ifndef _R2544MAIN_C_
#define _R2544MAIN_C_

#include "r2544inc.h"
/****************************************************************************
 *                           r2544main.c prototypes                         *
 ****************************************************************************/

PRIVATE INT4        R2544TaskInit (VOID);
PRIVATE VOID        R2544TaskDeInit (VOID);
PRIVATE INT4        R2544TmrInit (VOID);
PRIVATE VOID        R2544TmrDeInit (VOID);
PRIVATE VOID        R2544AssignMemPools (VOID);
PRIVATE INT4        R2544MainModuleStart (VOID);

/****************************************************************************
*                                                                           *
*    Function Name       : R2544Task                                        *
*                                                                           *
*    Description         : This function will perform following task in     *
*                          RFc2544 Module:                                  *
*                          o  Initialized the R2544 task (Sem, queue,       *
*                             mempool creation).                            *
*                          o  Register R2544 Mib with SNMP module           *
*                          o  Wait for external event and call the          *
*                             corresponding even handler routines           *
*                             on receiving the events.                      *
*                                                                           *
*    Input(s)            : pi1Arg -  Pointer to input arguments             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : None                                             *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
R2544Task (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;

#ifdef SYSLOG_WANTED
    INT4                i4SyslogId = 0;
#endif

    UNUSED_PARAM (pi1Arg);
    if (R2544TaskInit () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544Task failed to initialize\r\n");

        /* Deinitialize RFC2544  task */
        R2544TaskDeInit ();

        lrInitComplete (OSIX_FAILURE);
        return;
    }

#ifdef SYSLOG_WANTED
    /* Register with SysLog Server to log messages. */
    i4SyslogId = SYS_LOG_REGISTER ((CONST UINT1 *) RFC2544_TASK_NAME,
                                   SYSLOG_ALERT_LEVEL);

    if (i4SyslogId < 0)
    {
        RFC2544_GLOBAL_TRC
            ("RFC2544 registration with Syslog server failed\r\n");

        /* Deinitialize ERPS task */
        R2544TaskDeInit ();

        lrInitComplete (OSIX_FAILURE);
        return;
    }
    RFC2544_SYSLOG_ID = (UINT4) i4SyslogId;

    /* De-registration is not called now as there are no further registrations
     * done below which could fail. Should there be registrations below syslog
     * reg added later, the de-reg should be called appropriately.
     */
#endif

#ifdef SNMP_2_WANTED
    RegisterFS2544 ();
#endif

    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gR2544GlobalInfo.TaskId, RFC2544_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            RFC2544_LOCK ();

            if (u4Events & RFC2544_QMSG_EVENT)
            {
                R2544MsgQueueHandler ();

            }

            if (u4Events & RFC2544_TMR_EXPIRY_EVENT)
            {
                R2544TmrExpHandler ();
            }

            RFC2544_UNLOCK ();
        }
    }

    return;
}

/*****************************************************************************
*                                                                           *
*    Function Name       : R2544TaskInit                                    *
*                                                                           *
*    Description         : This function will perform following task in     *
*                          R2544 Module:                                    *
*                          o  Initializes global variables                  *
*                          o  Create queues, semaphore, mempools for ERPS   *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         allocated RFC2544 Module resource.*
*                          OSIX_FAILURE - When this function failed to      *
*                                         allocate RFC2544 Module resources.*
*****************************************************************************/
PRIVATE INT4
R2544TaskInit (VOID)
{
    MEMSET (&gR2544GlobalInfo, 0, sizeof (tR2544GlobalInfo));
    MEMSET (&gau1R2544SystemControl, RFC2544_SHUTDOWN,
            sizeof (UINT1) * SYS_DEF_MAX_NUM_CONTEXTS);

    if (OsixGetTaskId (SELF, RFC2544_TASK_NAME, &(RFC2544_TASK_ID ()))
        == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544TaskInit: Get Task Id FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    if (OsixSemCrt (RFC2544_PROTO_SEM, &(RFC2544_SEM_ID ())) == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544TaskInit Sem Creation failed\r\n");
        return OSIX_FAILURE;
    }

    OsixSemGive (RFC2544_SEM_ID ());

    if (OsixQueCrt ((UINT1 *) RFC2544_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    FsRFC2544SizingParams[MAX_RFC2544_Q_MESG_SIZING_ID].
                    u4PreAllocatedUnits,
                    &(gR2544GlobalInfo.MsgQId)) == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544TaskInit Queue Creation failed\r\n");
        return OSIX_FAILURE;
    }

    if (R2544MainModuleStart () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC (("R2544MainTaskInit: RFC2544 Module Start "
                             "FAILED !!!\r\n"));
        return (OSIX_FAILURE);
    }
    /* set Y1564 Initialization as True */
    RFC2544_INITIALISED () = RFC2544_TRUE;

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544TaskDeInit                                  *
*                                                                           *
*    Description         : This function will perform deinitialization of   *
*                          R2544 Task                                       *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : None                                             *
*                                                                           *
*****************************************************************************/
PRIVATE VOID
R2544TaskDeInit (VOID)
{

    /* Delete the MemPools */
    R2544MainModuleShutDown ();
    if (RFC2544_QUEUE_ID () != RFC2544_ZERO)
    {
        OsixQueDel (RFC2544_QUEUE_ID ());
        RFC2544_QUEUE_ID () = RFC2544_ZERO;

    }
    if (RFC2544_SEM_ID () != 0)
    {
        OsixSemDel (RFC2544_SEM_ID ());
        RFC2544_SEM_ID () = RFC2544_ZERO;
    }
    RFC2544_INITIALISED () = RFC2544_FALSE;
    return;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544AssignMemPools                              *
*                                                                           *
*    Description         : This function will Assign the mempool Id's       *
*                          created for R2544 Module:                        *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : None.                                            *
*                                                                           *
*****************************************************************************/
VOID
R2544AssignMemPools (VOID)
{
    /* Assigning the pool information */
    RFC2544_QMSG_POOL () = RFC2544MemPoolIds[MAX_RFC2544_Q_MESG_SIZING_ID];
    RFC2544_CONTEXT_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_CONTEXT_ENTRIES_SIZING_ID];
    RFC2544_SLA_POOL () = RFC2544MemPoolIds[MAX_RFC2544_SLA_ENTRIES_SIZING_ID];
    RFC2544_TRAFFIC_PROFILE_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_TRAF_PROF_ENTRIES_SIZING_ID];
    RFC2544_SAC_POOL () = RFC2544MemPoolIds[MAX_RFC2544_SAC_ENTRIES_SIZING_ID];
    RFC2544_REPORT_STATS_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_STATS_ENTRIES_SIZING_ID];
    RFC2544_TH_STATS_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_TH_TEST_STATS_SIZING_ID];
    RFC2544_FL_STATS_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_FL_TEST_STATS_SIZING_ID];
    RFC2544_LA_STATS_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_LA_TEST_STATS_SIZING_ID];
    RFC2544_BB_STATS_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_BB_TEST_STATS_SIZING_ID];
    RFC2544_EXTINPARAMS_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_EXTINPARAMS_SIZING_ID];
    RFC2544_EXTOUTPARAMS_POOL () =
        RFC2544MemPoolIds[MAX_RFC2544_EXTOUTPARAMS_SIZING_ID];
    return;
}

/*****************************************************************************
*                                                                           *
*    Function Name       : R2544MainModuleStart                             *
*                                                                           *
*    Description         : This function allocates memory for all tables    *
*                          created  R2544 Module.                           *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : None.                                            *
*                                                                           *
*****************************************************************************/
PRIVATE INT4
R2544MainModuleStart (VOID)
{

    /* Initialize Database Mempools */
    if (Rfc2544SizingMemCreateMemPools () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC (("R2544MainModuleStart: Memory Initialization "
                             "FAILED !!!\r\n"));
        Rfc2544SizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
    /*Assigning the respective mempools */
    R2544AssignMemPools ();

    /* Timer Initialization */
    if (R2544TmrInit () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Timer Initialization FAILED !!!\r\n");
        Rfc2544SizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (R2544CreateSlaTable () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Sla Table creation failed\r\n");
        return OSIX_FAILURE;
    }

    if (R2544CreateTrafficProfileTable () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Traffic Profile table creation failed\r\n");
        return OSIX_FAILURE;
    }

    if (R2544CreateSacTable () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Sac table creation failed\r\n");
        return OSIX_FAILURE;
    }
    if (R2544CreateReportStatsTable () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Report Stats table creation failed\r\n");
        return OSIX_FAILURE;
    }
    if (R2544CreateThroughputStatsTable () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Throughput  Stats Table creation failed\r\n");
        return OSIX_FAILURE;
    }
    if (R2544CreateFrameLossStatsTable () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Frameloss Stats Table creation failed\r\n");
        return OSIX_FAILURE;
    }
    if (R2544CreateLatencyStatsTable () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Latency Stats Table creation failed\r\n");
        return OSIX_FAILURE;
    }
    if (R2544CreateBackToBackStatsTable () == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Report Stats Tablecreation failed\r\n");
        return OSIX_FAILURE;
    }
    if (R2544CxtCreateContext (RFC2544_DEFAULT_CONTEXT_ID) == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Default context creation failed\r\n");
        R2544TmrDeInit ();

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
* FUNCTION NAME    : R2544MainModuleShutDown                                 *
*                                                                            *
* DESCRIPTION      : This function will be called by RM module to shutdown   *
*                    the R2544 module                                        *
*                                                                            *
* INPUT            : None                                                    *
*                                                                            *
* OUTPUT           : None                                                    *
*                                                                            *
* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                               *
*                                                                            *
******************************************************************************/

PUBLIC INT4
R2544MainModuleShutDown (VOID)
{
#if 0
    tR2544ReqParams        *pMsg = NULL;
#endif 
    UINT4               u4ContextId = 0;

    if (RFC2544_INITIALISED () == RFC2544_FALSE)
    {
        return OSIX_FAILURE;
    }
    RFC2544_INITIALISED () = RFC2544_FALSE;

    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            R2544CxtDeleteContext (u4ContextId);
        }
    }
#if 0
    while (OsixQueRecv (gR2544GlobalInfo.MsgQId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)

    {
        if (MemReleaseMemBlock (gR2544GlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            == MEM_FAILURE)
        {
            RFC2544_GLOBAL_TRC
                ("R2544MainModuleShutDown:Free MemBlock QMsg FAILED\n");
        }

    }
#endif 
     R2544DeleteReportStatsTable ();
     R2544DeleteTrafficProfileTable ();
     R2544DeleteSacTable ();
     R2544DeleteSlaTable ();
     R2544DeleteThroughputStatsTable ();
     R2544DeleteFrameLossStatsTable ();
     R2544DeleteLatencyStatsTable ();
     R2544DeleteBackToBackStatsTable ();
    
    /* Delete the Timers */
    R2544TmrDeInit ();

    /* Delete the MemPools */

    Rfc2544SizingMemDeleteMemPools ();

    MEMSET (&gau1R2544SystemControl, RFC2544_SHUTDOWN,
            sizeof (UINT1) * SYS_DEF_MAX_NUM_CONTEXTS);

    return OSIX_SUCCESS;

}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544TmrInit                                     *
*                                                                           *
*    Description         : This function will create timer list for R2544   *
*                          Module:                                          *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         created timer list.               *
*                          OSIX_FAILURE - When this function failed to      *
*                                         create timer list.                *
*****************************************************************************/
INT4
R2544TmrInit (VOID)
{
    if (TmrCreateTimerList (RFC2544_TASK_NAME, RFC2544_TMR_EXPIRY_EVENT,
                            NULL, &(RFC2544_TIMER_LIST ())) == TMR_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544TmrInit Timer list creation failed\r\n");

        return OSIX_FAILURE;
    }

     gR2544GlobalInfo.TmrDesc[RFC2544_SLA_TIMER].i2Offset =  
         (INT2) FSAP_OFFSETOF (tSlaEntry, SlaTimer); 
     gR2544GlobalInfo.TmrDesc[RFC2544_SLA_TIMER].TmrExpFn =  
         R2544TmrSlaTimerExp; 
   
     gR2544GlobalInfo.TmrDesc[RFC2544_DWELL_TIME_TIMER].i2Offset =  
         (INT2) FSAP_OFFSETOF (tSlaEntry, DwellTimeTimer); 
     gR2544GlobalInfo.TmrDesc[RFC2544_DWELL_TIME_TIMER].TmrExpFn =  
         R2544TmrDwellTimeTimerExp; 

    return OSIX_SUCCESS;
}

/*****************************************************************************
*                                                                           *
*    Function Name       : R2544TmrDeInit                                   *
*                                                                           *
*    Description         : If timer list was created, this function will    *
*                          delete the timer List.                           *
*                                                                           *
*    Input(s)            : None                                             *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : None.                                            *
*                                                                           *
*****************************************************************************/
VOID
R2544TmrDeInit (VOID)
{
    if (TmrDeleteTimerList (RFC2544_TIMER_LIST ()) == TMR_FAILURE)
    {
        RFC2544_GLOBAL_TRC ("R2544TmrDeInit Timer list deletion failed\r\n");
        return;
    }

    MEMSET (&(RFC2544_TIMER_DESC ()), RFC2544_ZERO,
               sizeof (tTmrDesc) * RFC2544_MAX_TMR_TYPES); 

    return;
}

#endif /* end of __R2544MAIN_C__ */
