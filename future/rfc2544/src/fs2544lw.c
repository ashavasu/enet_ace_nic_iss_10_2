/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fs2544lw.c,v 1.17 2016/05/20 10:18:07 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "r2544inc.h"

/* LOW LEVEL Routines for Table : Fs2544ContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs2544ContextTable
 Input       :  The Indices
                Fs2544ContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs2544ContextTable (UINT4 u4Fs2544ContextId)
{
    if (R2544CxtIsContextExist (u4Fs2544ContextId) != RFC2544_TRUE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs2544ContextTable
 Input       :  The Indices
                Fs2544ContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs2544ContextTable (UINT4 *pu4Fs2544ContextId)
{
    UINT4               u4ContextId = 0;

    *pu4Fs2544ContextId = 0;

    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544_GET_CONTEXT_INFO (u4ContextId) != NULL)
        {
            *pu4Fs2544ContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, *pu4Fs2544ContextId,
                 "Failed to fetch the first index\r\n");

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs2544ContextTable
 Input       :  The Indices
                Fs2544ContextId
                nextFs2544ContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs2544ContextTable (UINT4 u4Fs2544ContextId,
                                   UINT4 *pu4NextFs2544ContextId)
{
    UINT4               u4ContextId = u4Fs2544ContextId + 1;

    *pu4NextFs2544ContextId = RFC2544_ZERO;

    for (; u4ContextId < RFC2544_MAX_CONTEXTS; u4ContextId++)
    {
        if (R2544CxtGetContextEntry (u4ContextId) != NULL)
        {
            *pu4NextFs2544ContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs2544ContextName
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                retValFs2544ContextName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ContextName (UINT4 u4Fs2544ContextId,
                         tSNMP_OCTET_STRING_TYPE * pRetValFs2544ContextName)
{
    tR2544ContextInfo  *pContextInfo = NULL;
    tR2544ExtInParams  *pR2544ExtInParams = NULL;
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;

    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;

        RFC2544_TRC (RFC2544_RESOURCE_TRC | RFC2544_ALL_FAILURE_TRC, 
                     u4Fs2544ContextId,
                     "nmhGetFs2544ContextName :Memory allocation"
                     "failed for ExtInParams\r\n");
        return SNMP_FAILURE;
    }
    RFC2544_MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_TRC (RFC2544_RESOURCE_TRC | RFC2544_ALL_FAILURE_TRC, 
                     u4Fs2544ContextId,
                     "nmhGetFs2544ContextName :Memory allocation"
                     "failed for ExtOutParams \r\n");
        return SNMP_FAILURE;
    }
    RFC2544_MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC
            ("nmhGetFs2544ContextName : Context does not exist\r\n");
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                            (UINT1 *) pR2544ExtOutParams);

        return SNMP_FAILURE;
    }
    pR2544ExtInParams->u4ContextId = u4Fs2544ContextId;
    pR2544ExtInParams->eExtReqType = R2544_REQ_VCM_GET_CONTEXT_NAME;
    if (R2544PortHandleExtInteraction (pR2544ExtInParams, pR2544ExtOutParams) ==
        OSIX_FAILURE)
    {
         RFC2544_TRC (RFC2544_RESOURCE_TRC | RFC2544_ALL_FAILURE_TRC,
                      u4Fs2544ContextId,"nmhGetFs2544ContextName : Cannot"
                      " retrieve the context ID\r\n");
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                            (UINT1 *) pR2544ExtOutParams);
        return SNMP_FAILURE;
    }

    RFC2544_MEMCPY (pRetValFs2544ContextName->pu1_OctetList,
                    pR2544ExtOutParams->au1ContextName,
                    RFC2544_STRLEN (pR2544ExtOutParams->au1ContextName));
    pRetValFs2544ContextName->i4_Length =
        (INT4) RFC2544_STRLEN (pR2544ExtOutParams->au1ContextName);

    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544ContextSystemControl
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                retValFs2544ContextSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ContextSystemControl (UINT4 u4Fs2544ContextId,
                                  INT4 *pi4RetValFs2544ContextSystemControl)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544ContextSystemControl =
        gau1R2544SystemControl[u4Fs2544ContextId];
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ContextModuleStatus
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                retValFs2544ContextModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ContextModuleStatus (UINT4 u4Fs2544ContextId,
                                 INT4 *pi4RetValFs2544ContextModuleStatus)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");

        return SNMP_FAILURE;
    }

    *pi4RetValFs2544ContextModuleStatus = pContextInfo->u1ModuleStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544ContextTraceOption
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                retValFs2544ContextTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ContextTraceOption (UINT4 u4Fs2544ContextId,
                                UINT4 *pu4RetValFs2544ContextTraceOption)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");

        return SNMP_FAILURE;
    }

    *pu4RetValFs2544ContextTraceOption = pContextInfo->u4TraceOption;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ContextNumOfTestRunning
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                retValFs2544ContextNumOfTestRunning
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ContextNumOfTestRunning (UINT4 u4Fs2544ContextId,
                                     UINT4
                                     *pu4RetValFs2544ContextNumOfTestRunning)
{
    tR2544ContextInfo  *pContextInfo = NULL;
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry          *pNextSlaEntry = NULL;
    UINT4               u4NumOfTestRunning = RFC2544_ZERO;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");

        return SNMP_FAILURE;
    }

    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());

    while (pSlaEntry != NULL)
    {
        /* Delete all the nodes */
        pNextSlaEntry = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                     (tRBElem *) pSlaEntry,
                                                     NULL);
        if (pSlaEntry->u4ContextId == u4Fs2544ContextId)
        {
            if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)
            {
                u4NumOfTestRunning++;
            }
        }
        pSlaEntry = pNextSlaEntry;
    }
    pContextInfo->u4NumOfTestRunning = u4NumOfTestRunning;
    *pu4RetValFs2544ContextNumOfTestRunning = pContextInfo->u4NumOfTestRunning;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ContextTrapStatus
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                retValFs2544ContextTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ContextTrapStatus (UINT4 u4Fs2544ContextId,
                               INT4 *pi4RetValFs2544ContextTrapStatus)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");
        return SNMP_FAILURE;
    }

    *pi4RetValFs2544ContextTrapStatus = pContextInfo->u1TrapStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs2544ContextSystemControl
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                setValFs2544ContextSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544ContextSystemControl (UINT4 u4Fs2544ContextId,
                                  INT4 i4SetValFs2544ContextSystemControl)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");
        return SNMP_FAILURE;
    }


    if (gau1R2544SystemControl[u4Fs2544ContextId] ==
        (UINT1) i4SetValFs2544ContextSystemControl)
    {
        /* If the system control Status present in the context is
         * same as new system control Status then return success */
        return SNMP_SUCCESS;

    }

    if (i4SetValFs2544ContextSystemControl == RFC2544_START)
    {
        /* Start RFC2544 */
        gau1R2544SystemControl[u4Fs2544ContextId] = RFC2544_START;
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4Fs2544ContextId,
                      "RFC2544 Module is started %d\r\n", u4Fs2544ContextId);
    }

    if (i4SetValFs2544ContextSystemControl == RFC2544_SHUTDOWN)
    {

        if (R2544CxtModuleShutdown (u4Fs2544ContextId) == RFC2544_SUCCESS)
        {
            RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4Fs2544ContextId,
                          "RFC2544 Module is shutdown %d\r\n",
                          u4Fs2544ContextId);
        }
        R2544PortNotifyProtocolShutStatus ((INT4)(u4Fs2544ContextId));
    }
    gau1R2544SystemControl[u4Fs2544ContextId] =
        (UINT1) i4SetValFs2544ContextSystemControl;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544ContextModuleStatus
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                setValFs2544ContextModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544ContextModuleStatus (UINT4 u4Fs2544ContextId,
                                 INT4 i4SetValFs2544ContextModuleStatus)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pContextInfo->u1ModuleStatus == i4SetValFs2544ContextModuleStatus)
    {
        /* If module is already enabled in that context then return
         * success */
        return SNMP_SUCCESS;
    }

    pContextInfo->u1ModuleStatus = (UINT1) i4SetValFs2544ContextModuleStatus;

    if (i4SetValFs2544ContextModuleStatus == RFC2544_ENABLED)
    {
        /* Enable RFC2544 */
        if (R2544CxtModuleEnable (pContextInfo) == RFC2544_FAILURE)
        {
            RFC2544_TRC (RFC2544_INIT_SHUT_TRC | RFC2544_ALL_FAILURE_TRC, 
                         u4Fs2544ContextId, 
                         "Failed to enable RFC2544 Module \r\n");
            /* Call module shutdown to deallocate the memory. */
            R2544CxtModuleDisable (pContextInfo);
            return SNMP_FAILURE;
        }
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4Fs2544ContextId,
                      "RFC2544 Module is enabled %d\r\n", u4Fs2544ContextId);
    }
    else if (i4SetValFs2544ContextModuleStatus == RFC2544_DISABLED)
    {
        R2544CxtModuleDisable (pContextInfo);
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4Fs2544ContextId,
                      "RFC2544 Module is disbaled %d\r\n", u4Fs2544ContextId);
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544ContextTraceOption
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                setValFs2544ContextTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544ContextTraceOption (UINT4 u4Fs2544ContextId,
                                UINT4 u4SetValFs2544ContextTraceOption)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");
        return SNMP_FAILURE;
    }
    pContextInfo->u4TraceOption = u4SetValFs2544ContextTraceOption;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs2544ContextTrapStatus
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                setValFs2544ContextTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544ContextTrapStatus (UINT4 u4Fs2544ContextId,
                               INT4 i4SetValFs2544ContextTrapStatus)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4Fs2544ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");
        return SNMP_FAILURE;
    }
    pContextInfo->u1TrapStatus = (UINT1) i4SetValFs2544ContextTrapStatus;

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs2544ContextSystemControl
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                testValFs2544ContextSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544ContextSystemControl (UINT4 *pu4ErrorCode,
                                     UINT4 u4Fs2544ContextId,
                                     INT4 i4TestValFs2544ContextSystemControl)
{

    /* Check whether the System control value is in allowed limit */
    if ((i4TestValFs2544ContextSystemControl != RFC2544_START) &&
        (i4TestValFs2544ContextSystemControl != RFC2544_SHUTDOWN))
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                     "Invalid System Control \r\n");
        CLI_SET_ERR (CLI_RFC2544_INVALID_SYSTEM_CONTROL);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Validate the maximum allowed Contexts */
    if (u4Fs2544ContextId >= RFC2544_MAX_CONTEXTS)
    {
        RFC2544_GLOBAL_TRC ("Invalid Context Identifier \r\n");

        CLI_SET_ERR (CLI_RFC2544_INVALID_CONTEXT_ID);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544ContextModuleStatus
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                testValFs2544ContextModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544ContextModuleStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fs2544ContextId,
                                    INT4 i4TestValFs2544ContextModuleStatus)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    if ((i4TestValFs2544ContextModuleStatus != RFC2544_ENABLED) &&
        (i4TestValFs2544ContextModuleStatus != RFC2544_DISABLED))
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                     "Invalid Module Status . \r\n");
        CLI_SET_ERR (CLI_RFC2544_INVALID_MODULE_STATUS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = R2544CxtValidateContextInfo (u4Fs2544ContextId,
                                                pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544ContextTraceOption
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                testValFs2544ContextTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544ContextTraceOption (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                                   UINT4 u4TestValFs2544ContextTraceOption)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    /*  The min value for trace option is zero 
     *  Comparison of unsigned expression < 0 is always false 
     *  hence only max comparision is done */
    if (u4TestValFs2544ContextTraceOption > RFC2544_ALL_TRC)
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                     "Invalid Trace Option \r\n");
        CLI_SET_ERR (CLI_RFC2544_INVALID_SYSTEM_CONTROL);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = R2544CxtValidateContextInfo (u4Fs2544ContextId,
                                                pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544ContextTrapStatus
 Input       :  The Indices
                Fs2544ContextId

                The Object 
                testValFs2544ContextTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544ContextTrapStatus (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                                  INT4 i4TestValFs2544ContextTrapStatus)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    if ((i4TestValFs2544ContextTrapStatus != RFC2544_ENABLED) &&
        (i4TestValFs2544ContextTrapStatus != RFC2544_DISABLED))
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                     "Invalid Trap status \r\n");
        CLI_SET_ERR (CLI_RFC2544_INVALID_SYSTEM_CONTROL);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = R2544CxtValidateContextInfo (u4Fs2544ContextId,
                                                pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs2544ContextTable
 Input       :  The Indices
                Fs2544ContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs2544ContextTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs2544SlaTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs2544SlaTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs2544SlaTable (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544SlaId)
{
    if (R2544CxtIsContextExist (u4Fs2544ContextId) != RFC2544_TRUE)
    {
        RFC2544_GLOBAL_TRC ("nmhValidateIndexInstanceFs2544SlaTable: "
                            "Invalid Context  \r\n");
        return SNMP_FAILURE;
    }
    if ((u4Fs2544SlaId < RFC2544_MIN_SLA_ID) ||
        (u4Fs2544SlaId > RFC2544_MAX_SLA_ID))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhValidateIndexInstanceFs2544SlaTable: "
                      "Invalid  SLA Identifier %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs2544SlaTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs2544SlaTable (UINT4 *pu4Fs2544ContextId,
                                UINT4 *pu4Fs2544SlaId)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());
    if (pSlaEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4Fs2544ContextId = pSlaEntry->u4ContextId;
    *pu4Fs2544SlaId = pSlaEntry->u4SlaId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs2544SlaTable
 Input       :  The Indices
                Fs2544ContextId
                nextFs2544ContextId
                Fs2544SlaId
                nextFs2544SlaId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs2544SlaTable (UINT4 u4Fs2544ContextId,
                               UINT4 *pu4NextFs2544ContextId,
                               UINT4 u4Fs2544SlaId, UINT4 *pu4NextFs2544SlaId)
{
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry           SlaEntry;

    MEMSET (&SlaEntry, RFC2544_ZERO, sizeof (tSlaEntry));

    SlaEntry.u4ContextId = u4Fs2544ContextId;
    SlaEntry.u4SlaId = u4Fs2544SlaId;

    pSlaEntry = (tSlaEntry *) RBTreeGetNext
        (RFC2544_SLA_TABLE (), (tRBElem *) & SlaEntry, NULL);
    if (pSlaEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFs2544ContextId = pSlaEntry->u4ContextId;
    *pu4NextFs2544SlaId = pSlaEntry->u4SlaId;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs2544SlaMEG
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaMEG
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaMEG (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                    UINT4 *pu4RetValFs2544SlaMEG)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaMEG: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SlaMEG = pSlaEntry->u4SlaMeg;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544SlaME
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaME
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaME (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                   UINT4 *pu4RetValFs2544SlaME)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaME: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SlaME = pSlaEntry->u4SlaMe;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544SlaMEP
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaMEP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaMEP (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                    UINT4 *pu4RetValFs2544SlaMEP)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaMEP: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SlaMEP = pSlaEntry->u4SlaMep;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544SlaTrafficProfileId
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaTrafficProfileId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaTrafficProfileId (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                                 UINT4 *pu4RetValFs2544SlaTrafficProfileId)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaTrafficProfileId: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SlaTrafficProfileId = pSlaEntry->u4SlaTrafProfId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544SlaSacId
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaSacId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaSacId (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                      UINT4 *pu4RetValFs2544SlaSacId)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaSacId: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SlaSacId = pSlaEntry->u4SlaSacId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544SlaTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaTestStatus (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                           INT4 *pi4RetValFs2544SlaTestStatus)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaTestStatus: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544SlaTestStatus = pSlaEntry->u1SlaTestStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544SlaCurrentTestState
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaCurrentTestState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaCurrentTestState (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                                 INT4 *pi4RetValFs2544SlaCurrentTestState)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaCurrentTestState: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544SlaCurrentTestState = pSlaEntry->u1SlaCurrentTestState;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544SlaTestStartTime
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaTestStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaTestStartTime (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                              UINT4 *pu4RetValFs2544SlaTestStartTime)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaTestStartTime: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SlaTestStartTime = pSlaEntry->R2544TestStartTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544SlaTestEndTime
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaTestEndTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaTestEndTime (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                            UINT4 *pu4RetValFs2544SlaTestEndTime)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaTestEndTime: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SlaTestEndTime = pSlaEntry->R2544TestEndTime;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544SlaRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                retValFs2544SlaRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SlaRowStatus (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                          INT4 *pi4RetValFs2544SlaRowStatus)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SlaRowStatus: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544SlaRowStatus = pSlaEntry->u1RowStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs2544SlaMEG
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                setValFs2544SlaMEG
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SlaMEG (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                    UINT4 u4SetValFs2544SlaMEG)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544SlaMEG: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    pSlaEntry->u4SlaMeg = u4SetValFs2544SlaMEG;

    /*This  validates whether all the mandatory parameters 
     *are mapped to the SLA .If all are mapped then makes the 
     *SLA row status as active.
     *SLA should be active to initite the bench mark test */
    if (R2544UtilValidateSlaParams (pSlaEntry) == RFC2544_FAILURE)
    {
        CLI_SET_ERR (CLI_RFC2544_MEG_MEP_ME_MAPPED);
        return SNMP_FAILURE;
    }
    if ((pSlaEntry->u4SlaMeg != RFC2544_ZERO)
        && (pSlaEntry->u4SlaMe != RFC2544_ZERO)
        && (pSlaEntry->u4SlaMep != RFC2544_ZERO))
    {
        if (R2544UtilValidateMepInfo (pSlaEntry) == RFC2544_SUCCESS)
        {
            if (R2544PortMepRegAndGetFltState (pSlaEntry) != RFC2544_SUCCESS)
            {
                RFC2544_TRC1 (RFC2544_MGMT_TRC, pSlaEntry->u4ContextId,
                              " MEP Registeration "
                              "for MEP %d with ECFM/Y.1731 "
                              "failed \r\n", pSlaEntry->u4SlaMep);
                return SNMP_FAILURE;
            }
        }
        else
        {

            nmhSetFs2544SlaMEG (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                RFC2544_ZERO);
            nmhSetFs2544SlaME (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                               RFC2544_ZERO);
            nmhSetFs2544SlaMEP (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                RFC2544_ZERO);
            CLI_SET_ERR (CLI_RFC2544_SERVICE_NOT_PRESENT);
            return SNMP_FAILURE;
        }
    }

    nmhSetFs2544SlaRowStatus (u4Fs2544ContextId, u4Fs2544SlaId, ACTIVE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs2544SlaME
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                setValFs2544SlaME
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SlaME (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                   UINT4 u4SetValFs2544SlaME)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544SlaME: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    pSlaEntry->u4SlaMe = u4SetValFs2544SlaME;
    /*This  validates whether all the mandatory parameters 
     *are mapped to the SLA .If all are mapped then makes the 
     *SLA row status as active.
     *SLA should be active to initite the bench mark test */
    if (R2544UtilValidateSlaParams (pSlaEntry) == RFC2544_FAILURE)
    {
        CLI_SET_ERR (CLI_RFC2544_MEG_MEP_ME_MAPPED);
        return SNMP_FAILURE;
    }
    if ((pSlaEntry->u4SlaMeg != RFC2544_ZERO)
        && (pSlaEntry->u4SlaMe != RFC2544_ZERO)
        && (pSlaEntry->u4SlaMep != RFC2544_ZERO))
    {
        if (R2544UtilValidateMepInfo (pSlaEntry) == RFC2544_SUCCESS)
        {
            if (R2544PortMepRegAndGetFltState (pSlaEntry) != RFC2544_SUCCESS)
            {
                RFC2544_TRC1 (RFC2544_MGMT_TRC, pSlaEntry->u4ContextId,
                              " MEP Registeration "
                              "for MEP %d with ECFM/Y.1731 "
                              "failed \r\n", pSlaEntry->u4SlaMep);
                return SNMP_FAILURE;
            }
        }
        else
        {

            nmhSetFs2544SlaMEG (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                RFC2544_ZERO);
            nmhSetFs2544SlaME (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                               RFC2544_ZERO);
            nmhSetFs2544SlaMEP (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                RFC2544_ZERO);
            CLI_SET_ERR (CLI_RFC2544_SERVICE_NOT_PRESENT);
            return SNMP_FAILURE;
        }
    }

    nmhSetFs2544SlaRowStatus (u4Fs2544ContextId, u4Fs2544SlaId, ACTIVE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs2544SlaMEP
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                setValFs2544SlaMEP
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SlaMEP (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                    UINT4 u4SetValFs2544SlaMEP)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544SlaMEP: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    pSlaEntry->u4SlaMep = u4SetValFs2544SlaMEP;
    /*This  validates whether all the mandatory parameters 
     *are mapped to the SLA .If all are mapped then makes the 
     *SLA row status as active.
     *SLA should be active to initite the bench mark test */
    if (R2544UtilValidateSlaParams (pSlaEntry) == RFC2544_FAILURE)
    {
        CLI_SET_ERR (CLI_RFC2544_MEG_MEP_ME_MAPPED);
        return SNMP_FAILURE;
    }
    if ((pSlaEntry->u4SlaMeg != RFC2544_ZERO)
        && (pSlaEntry->u4SlaMe != RFC2544_ZERO)
        && (pSlaEntry->u4SlaMep != RFC2544_ZERO))
    {
        if (R2544UtilValidateMepInfo (pSlaEntry) == RFC2544_SUCCESS)
        {
            if (R2544PortMepRegAndGetFltState (pSlaEntry) != RFC2544_SUCCESS)
            {
                RFC2544_TRC1 (RFC2544_MGMT_TRC, pSlaEntry->u4ContextId,
                              " MEP Registeration "
                              "for MEP %d with ECFM/Y.1731 "
                              "failed \r\n", pSlaEntry->u4SlaMep);
                return SNMP_FAILURE;
            }
        }
        else
        {

            nmhSetFs2544SlaMEG (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                RFC2544_ZERO);
            nmhSetFs2544SlaME (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                               RFC2544_ZERO);
            nmhSetFs2544SlaMEP (pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                                RFC2544_ZERO);
            CLI_SET_ERR (CLI_RFC2544_SERVICE_NOT_PRESENT);
            return SNMP_FAILURE;
        }
    }

    nmhSetFs2544SlaRowStatus (u4Fs2544ContextId, u4Fs2544SlaId, ACTIVE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544SlaTrafficProfileId
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                setValFs2544SlaTrafficProfileId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SlaTrafficProfileId (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                                 UINT4 u4SetValFs2544SlaTrafficProfileId)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544SlaTrafficProfileId: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }

    pSlaEntry->u4SlaTrafProfId = u4SetValFs2544SlaTrafficProfileId;

    /*This  validates whether all the mandatory parameters 
     *are mapped to the SLA .If all are mapped then makes the 
     *SLA row status as active.
     *SLA should be active to initite the bench mark test */
    nmhSetFs2544SlaRowStatus (u4Fs2544ContextId, u4Fs2544SlaId, ACTIVE);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544SlaSacId
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                setValFs2544SlaSacId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SlaSacId (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                      UINT4 u4SetValFs2544SlaSacId)
{
    tSlaEntry          *pSlaEntry = NULL;

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544SlaSacId: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }

    pSlaEntry->u4SlaSacId = u4SetValFs2544SlaSacId;
    /*This  validates whether all the mandatory parameters 
     *are mapped to the SLA .If all are mapped then makes the 
     *SLA row status as active.
     *SLA should be active to initite the bench mark test */

    nmhSetFs2544SlaRowStatus (u4Fs2544ContextId, u4Fs2544SlaId, ACTIVE);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs2544SlaTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                setValFs2544SlaTestStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SlaTestStatus (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                           INT4 i4SetValFs2544SlaTestStatus)
{
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    tMacAddr            DstMacAddr;

    RFC2544_MEMSET (DstMacAddr, 0, sizeof (tMacAddr));
    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544SlaTestStatus: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                   pSlaEntry->u4SlaTrafProfId);

    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4SlaTrafProfId,
                      "nmhGetFs2544TrafficProfileSeqNoCheck: "
                      "Entry is not present for Traffic profile Id: %d\r\n",
                      pSlaEntry->u4SlaTrafProfId);
        return SNMP_FAILURE;
    }

    if (i4SetValFs2544SlaTestStatus == RFC2544_START_TEST)
    {
        if (pSlaEntry->u1SlaCurrentTestState != RFC2544_INPROGRESS)
        {
            if (R2544UtilGetSlaTestParams (pSlaEntry) == RFC2544_SUCCESS)
            {
                if (RFC2544_MEMCMP (pSlaEntry->TxDstMacAddr, DstMacAddr,
                                    sizeof (tMacAddr)) != RFC2544_ZERO)
                {

                    if ((pTrafficProfile->u1TrafProfThTestStatus ==
                         RFC2544_DISABLED)
                        && (pTrafficProfile->u1TrafProfLaTestStatus ==
                            RFC2544_DISABLED)
                        && (pTrafficProfile->u1TrafProfFlTestStatus ==
                            RFC2544_DISABLED)
                        && (pTrafficProfile->u1TrafProfBbTestStatus ==
                            RFC2544_DISABLED))
                    {
                        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC,
                                      u4Fs2544ContextId,
                                      "nmhSetFs2544SlaTestStatus: "
                                      "All subtests are disabled. "
                                      "Hence test cannot be " 
                                      "initiated for SLA %d\r\n",
                                      u4Fs2544SlaId);
                        CLI_SET_ERR (CLI_RFC2544_SUBTEST_NOT_ENABLED);
                        return SNMP_FAILURE;
                    }

                    if (R2544CoreStartSlaTest (pSlaEntry) != RFC2544_SUCCESS)
                    {
                        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC,
                                      u4Fs2544ContextId,
                                      "nmhSetFs2544SlaTestStatus: "
                                      "Failed to initiate the test for SLA %d\r\n",
                                      u4Fs2544SlaId);
                        CLI_SET_ERR (CLI_RFC2544_REMOTE_MEP_NOT_PRESENT);
                        return SNMP_FAILURE;
                    }
                    RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4Fs2544ContextId,
                                  "nmhSetFs2544SlaTestStatus: "
                                  "Test has been initiated successfully for SLA %d\r\n",
                                  u4Fs2544SlaId);
                }
                else
                {
                    RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                                  "nmhSetFs2544SlaTestStatus: "
                                  "Remote MEP is not present " 
                                  "hence test cannot be "
                                  "initiated for SLA %d\r\n", u4Fs2544SlaId);
                    CLI_SET_ERR (CLI_RFC2544_REMOTE_MEP_NOT_PRESENT);
                    return SNMP_FAILURE;

                }
            }
            else
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhSetFs2544SlaTestStatus: "
                              "Failed to fetch the test parameters " 
                              "hence test cannot be  "
                              "initiated for SLA %d\r\n", u4Fs2544SlaId);
                CLI_SET_ERR (CLI_RFC2544_TEST_PARAMETERS_NOT_PRESENT);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        pSlaEntry->u1SlaCurrentTestState = RFC2544_ABORTED;
        if (R2544CoreStopSlaTest (pSlaEntry) != RFC2544_SUCCESS)
        {
            RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4Fs2544ContextId,
                          "nmhSetFs2544SlaTestStatus: "
                          "Failed to stop the test for SLA %d\r\n", u4Fs2544SlaId);
            return SNMP_FAILURE;
        }
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544SlaTestStatus: "
                      "Test has been stopped Successfully for SLA %d\r\n", u4Fs2544SlaId);

    }

    pSlaEntry->u1SlaTestStatus = (UINT1) i4SetValFs2544SlaTestStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544SlaRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                setValFs2544SlaRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SlaRowStatus (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                          INT4 i4SetValFs2544SlaRowStatus)
{
    tSlaEntry          *pSlaEntry = NULL;

    if ((i4SetValFs2544SlaRowStatus != CREATE_AND_WAIT) &&
        (i4SetValFs2544SlaRowStatus != CREATE_AND_GO))
    {
        pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
        if ((pSlaEntry == NULL) && (i4SetValFs2544SlaRowStatus == DESTROY))
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhSetFs2544SlaRowStatus: "
                          "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
            CLI_SET_ERR (CLI_RFC2544_SLA_NOT_PRESENT);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFs2544SlaRowStatus)
    {
        case CREATE_AND_WAIT:

            /* Memblock creation and initializing the default values
             * to the context are handled inside the below function and
             * rowstatus is initialized here */
            if (R2544UtilAddSlaEntry (u4Fs2544ContextId,
                                      u4Fs2544SlaId) == RFC2544_FAILURE)
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhSetFs2544SlaRowStatus: "
                              "Failed to create Sla"
                              "Entry for Sla %d\r\n", u4Fs2544SlaId);
                return SNMP_FAILURE;
            }
            /* The Row Status is set to NOT READY since mandatory 
             * objects are filled during creation*/

            break;
        case DESTROY:

            /* The Entry is not present */

            if (R2544UtilDelSlaEntry (pSlaEntry) == RFC2544_FAILURE)
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhSetFs2544SlaRowStatus: "
                              "Failed to delete Sla"
                              "Entry for ID %d\r\n", u4Fs2544SlaId);
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }

            break;

        case NOT_IN_SERVICE:
        case NOT_READY:
            if (NULL != pSlaEntry)
            {
                pSlaEntry->u1RowStatus = (UINT1) i4SetValFs2544SlaRowStatus;
            }
            break;
        case ACTIVE:
            if (pSlaEntry == NULL)
            {
                return SNMP_FAILURE;
            }

            if ((pSlaEntry->u4SlaMeg != RFC2544_ZERO)
                && (pSlaEntry->u4SlaMe != RFC2544_ZERO)
                && (pSlaEntry->u4SlaMep != RFC2544_ZERO)
                && (pSlaEntry->u4SlaTrafProfId != RFC2544_ZERO)
                && (pSlaEntry->u4SlaSacId != RFC2544_ZERO))
            {

                pSlaEntry->u1RowStatus = (UINT1) i4SetValFs2544SlaRowStatus;
            }
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs2544SlaMEG
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                testValFs2544SlaMEG
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SlaMEG (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                       UINT4 u4Fs2544SlaId, UINT4 u4TestValFs2544SlaMEG)
{

    tSlaEntry          *pSlaEntry = NULL;

    if (R2544UtilValidateSla (u4Fs2544ContextId,
                              u4Fs2544SlaId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }

    /* 
     * Test validation is not done for the below two reasons
     * Minimum Value  -> comparison of unsigned expression < 0 is always false 
     * Maximum Value  -> decimal constant is unsigned only in ISO C90 
     * */

    UNUSED_PARAM (u4TestValFs2544SlaMEG);
    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaMEG: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (RFC2544_INPROGRESS == pSlaEntry->u1SlaCurrentTestState)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaMEG: "
                      "We cannot modify Sla MEG because test is "
                      "already started for SLA %d\r\n",
                      u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RFC2544_ME_MEG_MEP_CONF_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SlaME
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                testValFs2544SlaME
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SlaME (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                      UINT4 u4Fs2544SlaId, UINT4 u4TestValFs2544SlaME)
{

    tSlaEntry          *pSlaEntry = NULL;
    if (R2544UtilValidateSla (u4Fs2544ContextId,
                              u4Fs2544SlaId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }

    /* 
     * Test validation is not done for the below two reasons
     * Minimum Value  -> comparison of unsigned expression < 0 is always false 
     * Maximum Value  -> decimal constant is unsigned only in ISO C90 
     * */

    UNUSED_PARAM (u4TestValFs2544SlaME);
    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaME: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (RFC2544_INPROGRESS == pSlaEntry->u1SlaCurrentTestState)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaME: "
                      "Test is in progress hence MEP/ME/MEG cannot be "
                      "modified for SLA %d\r\n",
                      u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RFC2544_ME_MEG_MEP_CONF_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SlaMEP
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                testValFs2544SlaMEP
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SlaMEP (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                       UINT4 u4Fs2544SlaId, UINT4 u4TestValFs2544SlaMEP)
{

    tSlaEntry          *pSlaEntry = NULL;
    if (R2544UtilValidateSla (u4Fs2544ContextId,
                              u4Fs2544SlaId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }

    /* 
     * Test validation is not done for the below reasons
     * Minimum Value  -> comparison of unsigned expression < 0 is always false 
     * */

    if (u4TestValFs2544SlaMEP > RFC2544_MEP_MAX_INDEX)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaMEP: "
                      "Invalid MEP value for SLA%d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_SLA_MEP);
        return SNMP_FAILURE;
    }

    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaMEP: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (RFC2544_INPROGRESS == pSlaEntry->u1SlaCurrentTestState)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaMEP: "
                      "Test is in progress hence MEP/ME/MEG cannot "
                      "be modified for SLA %d\r\n",
                      u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_ME_MEG_MEP_CONF_ERR);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SlaTrafficProfileId
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                testValFs2544SlaTrafficProfileId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SlaTrafficProfileId (UINT4 *pu4ErrorCode,
                                    UINT4 u4Fs2544ContextId,
                                    UINT4 u4Fs2544SlaId,
                                    UINT4 u4TestValFs2544SlaTrafficProfileId)
{

    tSlaEntry          *pSlaEntry = NULL;
    INT4                i4TrafficProfileRowStatus = RFC2544_ZERO;
    if (R2544UtilValidateSla (u4Fs2544ContextId,
                              u4Fs2544SlaId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if (u4TestValFs2544SlaTrafficProfileId > RFC2544_MAX_TRAF_PROF_ID)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaTrafficprofileId: "
                      "Invalid traffic profile ID for SLA %d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_TRAF_PROF_ID);
        return SNMP_FAILURE;
    }
    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaTrafficprofileId: "
                      "Entry is not present for SLA Id: %d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaTrafficprofileId: "
                      "Test is in-progress for SLA entry %d\r\n",
                      u4Fs2544SlaId);
        CLI_SET_ERR (CLI_RFC2544_TRAF_PROF_CONF_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pSlaEntry->u4SlaTrafProfId == RFC2544_ZERO)
    {
        if (u4TestValFs2544SlaTrafficProfileId == RFC2544_ZERO)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SlaTrafficProfileId: "
                          "Traffic profile Entry is not mapped "
                          "for SLA %d\r\n ",
                          u4TestValFs2544SlaTrafficProfileId);
            CLI_SET_ERR (CLI_RFC2544_SLA_TRAF_PROF_NOT_MAPPED);
            return SNMP_FAILURE;

        }
        else
        {
            if (NULL ==
                R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                           u4TestValFs2544SlaTrafficProfileId))
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhTestv2Fs2544SlaTrafficProfileId: "
                              "Entry is not present for Traffic Profile Id: %d "
                              "Hence Traffic Profile cannot be mapped to "
                              "SLA\r\n",
                              u4TestValFs2544SlaTrafficProfileId);
                CLI_SET_ERR (CLI_RFC2544_SLA_TRAFPROF_NOT_PRESENT);
                return SNMP_FAILURE;
            }
        }

    }
    else
    {
        if (u4TestValFs2544SlaTrafficProfileId != RFC2544_ZERO)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SlaTrafficProfileId: "
                          "Traffic profile is already mapped."
                          "Unmap the existing traffic profile Id "
                          "and then map the new Traffic Profile %d\r\n",
                          u4Fs2544SlaId);
            CLI_SET_ERR (CLI_RFC2544_ERR_SLA_TRAF_PROF_MAP);
            return SNMP_FAILURE;
        }
    }
    nmhGetFs2544TrafficProfileRowStatus (u4Fs2544ContextId,
                                         u4TestValFs2544SlaTrafficProfileId,
                                         &i4TrafficProfileRowStatus);
    if (u4TestValFs2544SlaTrafficProfileId != RFC2544_ZERO)
    {
        if (i4TrafficProfileRowStatus != ACTIVE)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SlaTrafficProfileId: "
                          "Traffic profile parameters configured does not "
                          "sync. Verify MaxRate/MinRate/RateStep in "
                          "Throughput/FrameLoss Test"
                          " %d\r\n", u4Fs2544SlaId);
            /*CLI_SET_ERR (CLI_RFC2544_ERR_SLA_TRAF_PROF_MAP); */
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SlaSacId
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                testValFs2544SlaSacId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SlaSacId (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                         UINT4 u4Fs2544SlaId, UINT4 u4TestValFs2544SlaSacId)
{

    tSlaEntry          *pSlaEntry = NULL;
    if (R2544UtilValidateSla (u4Fs2544ContextId,
                              u4Fs2544SlaId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if (u4TestValFs2544SlaSacId > RFC2544_MAX_SAC_ID)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaSacId: "
                      "Invalid SAC id %d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_SAC_ID);
        return SNMP_FAILURE;
    }
    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaSacId: "
                      "Entry is not present for Sla Id: %d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaSacId: "
                      "Test is in-progress for SLA entry %d\r\n",
                      u4Fs2544SlaId);
        CLI_SET_ERR (CLI_RFC2544_SAC_CONF_ERR);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pSlaEntry->u4SlaSacId == RFC2544_ZERO)
    {
        if (u4TestValFs2544SlaSacId == RFC2544_ZERO)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SlaSacId: "
                          "SAC Entry is not mapped for SLA %d\r\n ",
                          u4Fs2544SlaId);
            CLI_SET_ERR (CLI_RFC2544_SLA_SAC_NOT_MAPPED);
            return SNMP_FAILURE;
        }
        else
        {
            if (NULL ==
                R2544UtilGetSacEntry (u4Fs2544ContextId,
                                      u4TestValFs2544SlaSacId))
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhTestv2Fs2544SlaSacId: "
                              "Entry is not present for Sac Id: %d "
                              "Hence SAC cannot be mapped to SLA\r\n",
                              u4TestValFs2544SlaSacId);
                CLI_SET_ERR (CLI_RFC2544_SLA_SAC_NOT_PRESENT);
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        if (u4TestValFs2544SlaSacId != RFC2544_ZERO)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SlaSacId: "
                          "Traffic profile is already mapped."
                          "Unmap the existing SAC Id "
                          "and then map the new sac %d\r\n", u4Fs2544SlaId);
            CLI_SET_ERR (CLI_RFC2544_ERR_SLA_SAC_MAP);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SlaTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                testValFs2544SlaTestStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SlaTestStatus (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                              UINT4 u4Fs2544SlaId,
                              INT4 i4TestValFs2544SlaTestStatus)
{

    tR2544ContextInfo  *pContextInfo = NULL;
    tSlaEntry          *pSlaEntry = NULL;
    INT4                i4ModuleStatus = RFC2544_ZERO;

    pContextInfo = R2544CxtValidateContextInfo (u4Fs2544ContextId,
                                                pu4ErrorCode);
    if (pContextInfo == NULL)
    {
        /* As the function itself will take care of filling the error code,
         *  CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }
    nmhGetFs2544ContextModuleStatus (u4Fs2544ContextId, &i4ModuleStatus);
    if (i4ModuleStatus == RFC2544_DISABLED)
    {
        if (i4TestValFs2544SlaTestStatus == RFC2544_START_TEST)
        {
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                         "nmhTestv2Fs2544SlaTestStatus: "
                         "RFC2544 Module is Disbaled."
                         "Enable Rf2544 Module to start the test.\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RFC2544_ERR_TEST_START);
            return SNMP_FAILURE;
        }
        else
        {
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                         "nmhTestv2Fs2544SlaTestStatus: "
                         "RFC2544 Module is Disbaled."
                         "Test will not be running when the module is "
                         "disabled.\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RFC2544_ERR_TEST_STOP);
            return SNMP_FAILURE;

        }

    }

    if ((u4Fs2544SlaId < RFC2544_MIN_SLA_ID) ||
        (u4Fs2544SlaId > RFC2544_MAX_SLA_ID))
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                     "Invalid SLA ID \r\n");
        CLI_SET_ERR (CLI_RFC2544_INVALID_SLA_ID);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFs2544SlaTestStatus != RFC2544_START_TEST) &&
        (i4TestValFs2544SlaTestStatus != RFC2544_STOP_TEST))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaTestStatus: "
                      "Invslid Test status for Sla %d\r\n", u4Fs2544SlaId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_TEST_STATUS);
        return SNMP_FAILURE;
    }
    pSlaEntry = R2544UtilGetSlaEntry (u4Fs2544ContextId, u4Fs2544SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SlaTestStatus: "
                      "Entry is not present for Sla Id: %d\r\n", u4Fs2544SlaId);
        CLI_SET_ERR (CLI_RFC2544_SLA_NOT_PRESENT);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (i4TestValFs2544SlaTestStatus == RFC2544_START_TEST)
    {

        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SlaTestStatus: "
                          "Test is in already in progress for Sla %d. Hence"
                          "test cannot be initaited again\r\n",
                          pSlaEntry->u4SlaId);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RFC2544_ERR_TEST_IN_PROGRESS);
            return SNMP_FAILURE;
        }

    }
    if (i4TestValFs2544SlaTestStatus == RFC2544_STOP_TEST)
    {
        if (pSlaEntry->u1SlaCurrentTestState != RFC2544_INPROGRESS)
        {
            RFC2544_TRC1 (RFC2544_MGMT_TRC | RFC2544_ALL_FAILURE_TRC, 
                          u4Fs2544ContextId,
                          "nmhTestv2Fs2544SlaTestStatus: "
                          "Test is in not in progress for Sla %d\r\n",
                          pSlaEntry->u4SlaId);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RFC2544_ERR_TEST_COMPLETED);
            return SNMP_FAILURE;

        }

    }

    /* To start benchmark test on a SLA row status of 
       the corresponding SLA entry should be active */

    /*Validate MEG, ME, MEP, traffic profile ID and  Sac ID .
     *Set the row status as active once the above all are  validated 

     1)Traffic profile ID should have an entry in the 
     traffic profile table
     2)SAC ID should have an entry in the SAC table
     3)MEG, ME, MEP should have an valid entry in ECFM/Y1731 

     Test will not be initiated if the row status is not active*/
    if (i4TestValFs2544SlaTestStatus == RFC2544_START_TEST)
    {
        if (R2544UtilValidateSlaInformation (pSlaEntry) == RFC2544_FAILURE)
        {
            /* Traces and CLI_SET_ERROR a are handles in the above function */
            CLI_SET_ERR (CLI_RFC2544_ERR_SLA_VALIDATION);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SlaRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId

                The Object 
                testValFs2544SlaRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SlaRowStatus (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                             UINT4 u4Fs2544SlaId,
                             INT4 i4TestValFs2544SlaRowStatus)
{
    if (R2544UtilValidateSla (u4Fs2544ContextId,
                              u4Fs2544SlaId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544SlaRowStatus < ACTIVE) ||
        (i4TestValFs2544SlaRowStatus > DESTROY))
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                     "Invalid Row Status \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs2544SlaTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs2544SlaTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs2544TrafficProfileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs2544TrafficProfileTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs2544TrafficProfileTable (UINT4 u4Fs2544ContextId,
                                                   UINT4
                                                   u4Fs2544TrafficProfileId)
{
    if (R2544CxtIsContextExist (u4Fs2544ContextId) != RFC2544_TRUE)
    {
        RFC2544_GLOBAL_TRC
            ("nmhValidateIndexInstanceFs2544TrafficProfileTable: "
             "Invalid Context  %d\r\n");
        return SNMP_FAILURE;
    }
    if ((u4Fs2544TrafficProfileId < RFC2544_MIN_TRAF_PROF_ID) ||
        (u4Fs2544TrafficProfileId > RFC2544_MAX_TRAF_PROF_ID))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhValidateIndexInstanceFs2544TrafficProfileTable: "
                      "Invalid Traffic profile Identifier %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs2544TrafficProfileTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs2544TrafficProfileTable (UINT4 *pu4Fs2544ContextId,
                                           UINT4 *pu4Fs2544TrafficProfileId)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = (tTrafficProfile *) RBTreeGetFirst
        (RFC2544_TRAFFIC_PROFILE_TABLE ());
    if (pTrafficProfile == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4Fs2544ContextId = pTrafficProfile->u4ContextId;
    *pu4Fs2544TrafficProfileId = pTrafficProfile->u4TrafficProfileId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFs2544TrafficProfileTable
 Input       :  The Indices
                Fs2544ContextId
                nextFs2544ContextId
                Fs2544TrafficProfileId
                nextFs2544TrafficProfileId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs2544TrafficProfileTable (UINT4 u4Fs2544ContextId,
                                          UINT4 *pu4NextFs2544ContextId,
                                          UINT4 u4Fs2544TrafficProfileId,
                                          UINT4 *pu4NextFs2544TrafficProfileId)
{
    tTrafficProfile    *pTrafficProfile = NULL;
    tTrafficProfile     TrafficProfile;

    MEMSET (&TrafficProfile, RFC2544_ZERO, sizeof (tTrafficProfile));

    TrafficProfile.u4ContextId = u4Fs2544ContextId;
    TrafficProfile.u4TrafficProfileId = u4Fs2544TrafficProfileId;

    pTrafficProfile = (tTrafficProfile *) RBTreeGetNext
        (RFC2544_TRAFFIC_PROFILE_TABLE (), (tRBElem *) & TrafficProfile, NULL);
    if (pTrafficProfile == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFs2544ContextId = pTrafficProfile->u4ContextId;
    *pu4NextFs2544TrafficProfileId = pTrafficProfile->u4TrafficProfileId;
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileName
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileName (UINT4 u4Fs2544ContextId,
                                UINT4 u4Fs2544TrafficProfileId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFs2544TrafficProfileName)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileName: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    RFC2544_MEMCPY (pRetValFs2544TrafficProfileName->pu1_OctetList,
                    pTrafficProfile->au1TrafProfName,
                    RFC2544_STRLEN (pTrafficProfile->au1TrafProfName));
    pRetValFs2544TrafficProfileName->i4_Length =
        (INT4) RFC2544_STRLEN (pTrafficProfile->au1TrafProfName);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileSeqNoCheck
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileSeqNoCheck
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileSeqNoCheck (UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544TrafficProfileId,
                                      INT4
                                      *pi4RetValFs2544TrafficProfileSeqNoCheck)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileSeqNoCheck: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pi4RetValFs2544TrafficProfileSeqNoCheck =
        pTrafficProfile->u1TrafProfSeqNoCheck;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileDwellTime
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileDwellTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileDwellTime (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     INT4
                                     *pi4RetValFs2544TrafficProfileDwellTime)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileDwellTime: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544TrafficProfileDwellTime =
        (INT4) pTrafficProfile->u4TrafProfDwellTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileFrameSize
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileFrameSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileFrameSize (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFs2544TrafficProfileFrameSize)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileFrameSize: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    RFC2544_MEMCPY (pRetValFs2544TrafficProfileFrameSize->pu1_OctetList,
                    pTrafficProfile->au1TrafProfFrameSize,
                    RFC2544_STRLEN (pTrafficProfile->au1TrafProfFrameSize));
    pRetValFs2544TrafficProfileFrameSize->i4_Length =
        (INT4) RFC2544_STRLEN (pTrafficProfile->au1TrafProfFrameSize);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfilePCP
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfilePCP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfilePCP (UINT4 u4Fs2544ContextId,
                               UINT4 u4Fs2544TrafficProfileId,
                               INT4 *pi4RetValFs2544TrafficProfilePCP)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfilePCP: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544TrafficProfilePCP = (INT4) pTrafficProfile->u4TrafProfPCP;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileThTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileThTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileThTestStatus (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                     *pi4RetValFs2544TrafficProfileThTestStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileThTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544TrafficProfileThTestStatus =
        pTrafficProfile->u1TrafProfThTestStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileFlTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileFlTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileFlTestStatus (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                     *pi4RetValFs2544TrafficProfileFlTestStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileFlTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544TrafficProfileFlTestStatus =
        pTrafficProfile->u1TrafProfFlTestStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileLaTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileLaTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileLaTestStatus (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                     *pi4RetValFs2544TrafficProfileLaTestStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileLaTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544TrafficProfileLaTestStatus =
        pTrafficProfile->u1TrafProfLaTestStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileBbTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileBbTestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileBbTestStatus (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                  *pi4RetValFs2544TrafficProfileBbTestStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileBbTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544TrafficProfileBbTestStatus =
        pTrafficProfile->u1TrafProfBbTestStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileThTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileThTrialDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileThTrialDuration (UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                               *pu4RetValFs2544TrafficProfileThTrialDuration)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileThTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544TrafficProfileThTrialDuration =
        pTrafficProfile->u4TrafProfThTrialDuration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileThMaxRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileThMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileThMaxRate (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     UINT4
                                *pu4RetValFs2544TrafficProfileThMaxRate)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileThMaxRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pu4RetValFs2544TrafficProfileThMaxRate =
        pTrafficProfile->u4TrafProfThMaxRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileThMinRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileThMinRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileThMinRate (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     UINT4
                                     *pu4RetValFs2544TrafficProfileThMinRate)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileThMinRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pu4RetValFs2544TrafficProfileThMinRate =
        pTrafficProfile->u4TrafProfThMinRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileLaTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileLaTrialDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileLaTrialDuration (UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                                *pu4RetValFs2544TrafficProfileLaTrialDuration)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileLaTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544TrafficProfileLaTrialDuration =
        pTrafficProfile->u4TrafProfLaTrialDuration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileLaDelayMeasureInterval
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileLaDelayMeasureInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileLaDelayMeasureInterval (UINT4 u4Fs2544ContextId,
                                                  UINT4
                                                  u4Fs2544TrafficProfileId,
                                                  UINT4
                    *pu4RetValFs2544TrafficProfileLaDelayMeasureInterval)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileLaDelayMeasureInterval: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pu4RetValFs2544TrafficProfileLaDelayMeasureInterval =
        pTrafficProfile->u4TrafProfLaDelayMeasureInterval;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileFlTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileFlTrialDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileFlTrialDuration (UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                      *pu4RetValFs2544TrafficProfileFlTrialDuration)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileFlTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pu4RetValFs2544TrafficProfileFlTrialDuration =
        pTrafficProfile->u4TrafProfFlTrialDuration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileFlMaxRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileFlMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileFlMaxRate (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     UINT4
                                     *pu4RetValFs2544TrafficProfileFlMaxRate)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileFlMaxRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pu4RetValFs2544TrafficProfileFlMaxRate =
        pTrafficProfile->u4TrafProfFlMaxRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileFlMinRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileFlMinRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileFlMinRate (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     UINT4
                                     *pu4RetValFs2544TrafficProfileFlMinRate)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileFlMinRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pu4RetValFs2544TrafficProfileFlMinRate =
        pTrafficProfile->u4TrafProfFlMinRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileFlRateStep
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileFlRateStep
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileFlRateStep (UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544TrafficProfileId,
                                      UINT4
                                      *pu4RetValFs2544TrafficProfileFlRateStep)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileFlRateStep: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544TrafficProfileFlRateStep =
        pTrafficProfile->u4TrafProfFlRateStep;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileBbTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileBbTrialDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileBbTrialDuration (UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                         *pu4RetValFs2544TrafficProfileBbTrialDuration)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileBbTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pu4RetValFs2544TrafficProfileBbTrialDuration =
        pTrafficProfile->u4TrafProfBbTrialDuration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileBbTrialCount
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileBbTrialCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileBbTrialCount (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        UINT4
                          *pu4RetValFs2544TrafficProfileBbTrialCount)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileBbTrialCount: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pu4RetValFs2544TrafficProfileBbTrialCount =
        pTrafficProfile->u4TrafProfBbTrialCount;
    return SNMP_SUCCESS;

}

/**************************************************************************** 
 Function    :  nmhGetFs2544TrafficProfileThRateStep 
 Input       :  The Indices 
                Fs2544ContextId 
                Fs2544TrafficProfileId 
  
                The Object 
                retValFs2544TrafficProfileThRateStep 
 Output      :  The Get Low Lev Routine Take the Indices & 
                store the Value requested in the Return val. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/ 
INT1 nmhGetFs2544TrafficProfileThRateStep(UINT4 u4Fs2544ContextId ,  
                                          UINT4 u4Fs2544TrafficProfileId ,  
                                          UINT4 *pu4RetValFs2544TrafficProfileThRateStep) 
{ 
    tTrafficProfile    *pTrafficProfile = NULL; 
  
    pTrafficProfile = 
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId); 
    if (pTrafficProfile == NULL) 
    { 
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                      "nmhGetFs2544TrafficProfileThRateStep: " 
                      "Traffic profile Entry not present for %d\r\n", 
                      u4Fs2544TrafficProfileId); 
        return SNMP_FAILURE; 
    } 
    *pu4RetValFs2544TrafficProfileThRateStep = 
        pTrafficProfile->u4TrafProfThRateStep; 
    return SNMP_SUCCESS; 
}

/****************************************************************************
 Function    :  nmhGetFs2544TrafficProfileRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                retValFs2544TrafficProfileRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544TrafficProfileRowStatus (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     INT4
                                     *pi4RetValFs2544TrafficProfileRowStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileRowStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    *pi4RetValFs2544TrafficProfileRowStatus = pTrafficProfile->u1RowStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileName
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileName (UINT4 u4Fs2544ContextId,
                                UINT4 u4Fs2544TrafficProfileId,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFs2544TrafficProfileName)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileName: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    RFC2544_MEMSET (pTrafficProfile->au1TrafProfName, RFC2544_ZERO,
                    RFC2544_PROFILE_NAME_MAX_LEN);
    RFC2544_MEMCPY (pTrafficProfile->au1TrafProfName,
                    pSetValFs2544TrafficProfileName->pu1_OctetList,
                    pSetValFs2544TrafficProfileName->i4_Length);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileSeqNoCheck
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileSeqNoCheck
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileSeqNoCheck (UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544TrafficProfileId,
                                      INT4
                                      i4SetValFs2544TrafficProfileSeqNoCheck)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileSeqNoCheck: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u1TrafProfSeqNoCheck =
        (UINT1) i4SetValFs2544TrafficProfileSeqNoCheck;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileDwellTime
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileDwellTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileDwellTime (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     INT4 i4SetValFs2544TrafficProfileDwellTime)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileDwellTime: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    pTrafficProfile->u4TrafProfDwellTime =
        (UINT4) i4SetValFs2544TrafficProfileDwellTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileFrameSize
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileFrameSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileFrameSize (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pSetValFs2544TrafficProfileFrameSize)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileFrameSize: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    if (R2544UtilParseFrameSize (u4Fs2544ContextId, u4Fs2544TrafficProfileId,
                                 pSetValFs2544TrafficProfileFrameSize) ==
        RFC2544_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileFrameSize: "
                      "Frames Size not set for Traffic profile Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;

    }
    RFC2544_MEMSET (pTrafficProfile->au1TrafProfFrameSize, RFC2544_ZERO,
                    RFC2544_STRING_MAX_LENGTH);
    RFC2544_MEMCPY (pTrafficProfile->au1TrafProfFrameSize,
                    pSetValFs2544TrafficProfileFrameSize->pu1_OctetList,
                    pSetValFs2544TrafficProfileFrameSize->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfilePCP
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfilePCP
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfilePCP (UINT4 u4Fs2544ContextId,
                               UINT4 u4Fs2544TrafficProfileId,
                               INT4 i4SetValFs2544TrafficProfilePCP)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfilePCP: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    pTrafficProfile->u4TrafProfPCP = (UINT4) i4SetValFs2544TrafficProfilePCP;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileThTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileThTestStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileThTestStatus (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                        i4SetValFs2544TrafficProfileThTestStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileThTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    if (i4SetValFs2544TrafficProfileThTestStatus == RFC2544_DISABLED)
    {
        if (pTrafficProfile->u1TrafProfLaTestStatus == RFC2544_ENABLED)
        {
            RFC2544_TRC1 (RFC2544_MGMT_TRC, u4Fs2544ContextId,
                          "nmhSetFs2544TrafficProfileThTestStatus: "
                          "Latency test is disabled as throughput "
                          "is disabled for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            pTrafficProfile->u1TrafProfLaTestStatus = RFC2544_DISABLED;
        }
    }
    pTrafficProfile->u1TrafProfThTestStatus =
        (UINT1) i4SetValFs2544TrafficProfileThTestStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileFlTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileFlTestStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileFlTestStatus (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                        i4SetValFs2544TrafficProfileFlTestStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileFlTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    pTrafficProfile->u1TrafProfFlTestStatus =
        (UINT1) i4SetValFs2544TrafficProfileFlTestStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileLaTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileLaTestStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileLaTestStatus (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                        i4SetValFs2544TrafficProfileLaTestStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileLaTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u1TrafProfLaTestStatus =
        (UINT1) i4SetValFs2544TrafficProfileLaTestStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileBbTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileBbTestStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileBbTestStatus (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                        i4SetValFs2544TrafficProfileBbTestStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileBbTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    pTrafficProfile->u1TrafProfBbTestStatus =
        (UINT1) i4SetValFs2544TrafficProfileBbTestStatus;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileThTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileThTrialDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileThTrialDuration (UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                                   u4SetValFs2544TrafficProfileThTrialDuration)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileThTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    pTrafficProfile->u4TrafProfThTrialDuration =
        u4SetValFs2544TrafficProfileThTrialDuration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileThMaxRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileThMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileThMaxRate (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     UINT4
                                     u4SetValFs2544TrafficProfileThMaxRate)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileThMaxRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u4TrafProfThMaxRate =
        u4SetValFs2544TrafficProfileThMaxRate;
    nmhSetFs2544TrafficProfileRowStatus (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         NOT_IN_SERVICE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileThMinRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileThMinRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileThMinRate (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     UINT4
                                     u4SetValFs2544TrafficProfileThMinRate)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileThMinRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u4TrafProfThMinRate
        = u4SetValFs2544TrafficProfileThMinRate;

    nmhSetFs2544TrafficProfileRowStatus (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         NOT_IN_SERVICE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileLaTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileLaTrialDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileLaTrialDuration (UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                            u4SetValFs2544TrafficProfileLaTrialDuration)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileLaTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    pTrafficProfile->u4TrafProfLaTrialDuration =
        u4SetValFs2544TrafficProfileLaTrialDuration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileLaDelayMeasureInterval
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileLaDelayMeasureInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileLaDelayMeasureInterval (UINT4 u4Fs2544ContextId,
                                                  UINT4
                                                  u4Fs2544TrafficProfileId,
                                                 UINT4
                         u4SetValFs2544TrafficProfileLaDelayMeasureInterval)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileLaDelayMeasureInterval: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u4TrafProfLaDelayMeasureInterval =
        u4SetValFs2544TrafficProfileLaDelayMeasureInterval;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileFlTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileFlTrialDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileFlTrialDuration (UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                            u4SetValFs2544TrafficProfileFlTrialDuration)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileFlTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u4TrafProfFlTrialDuration =
        u4SetValFs2544TrafficProfileFlTrialDuration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileFlMaxRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileFlMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileFlMaxRate (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     UINT4
                                     u4SetValFs2544TrafficProfileFlMaxRate)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile = R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                                 u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileFlMaxRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u4TrafProfFlMaxRate =
        u4SetValFs2544TrafficProfileFlMaxRate;

    nmhSetFs2544TrafficProfileRowStatus (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         NOT_IN_SERVICE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileFlMinRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileFlMinRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileFlMinRate (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     UINT4
                                     u4SetValFs2544TrafficProfileFlMinRate)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544TrafficProfileFlMinRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u4TrafProfFlMinRate =
        u4SetValFs2544TrafficProfileFlMinRate;

    nmhSetFs2544TrafficProfileRowStatus (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         NOT_IN_SERVICE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileFlRateStep
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileFlRateStep
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileFlRateStep (UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544TrafficProfileId,
                                      UINT4
                                      u4SetValFs2544TrafficProfileFlRateStep)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileFlRateStep: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }
    pTrafficProfile->u4TrafProfFlRateStep =
        u4SetValFs2544TrafficProfileFlRateStep;

    nmhSetFs2544TrafficProfileRowStatus (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         NOT_IN_SERVICE);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileBbTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileBbTrialDuration
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileBbTrialDuration (UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                           u4SetValFs2544TrafficProfileBbTrialDuration)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileBbTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u4TrafProfBbTrialDuration =
        u4SetValFs2544TrafficProfileBbTrialDuration;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileBbTrialCount
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileBbTrialCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileBbTrialCount (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        UINT4
                              u4SetValFs2544TrafficProfileBbTrialCount)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhSetFs2544TrafficProfileBbTrialCount: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        return SNMP_FAILURE;
    }

    pTrafficProfile->u4TrafProfBbTrialCount =
        u4SetValFs2544TrafficProfileBbTrialCount;
    return SNMP_SUCCESS;

}
/**************************************************************************** 
 Function    :  nmhSetFs2544TrafficProfileThRateStep 
 Input       :  The Indices 
                Fs2544ContextId 
                Fs2544TrafficProfileId 
  
                The Object 
                setValFs2544TrafficProfileThRateStep 
 Output      :  The Set Low Lev Routine Take the Indices & 
                Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/ 
INT1 nmhSetFs2544TrafficProfileThRateStep(UINT4 u4Fs2544ContextId ,  
                                          UINT4 u4Fs2544TrafficProfileId ,  
                                          UINT4 u4SetValFs2544TrafficProfileThRateStep) 
{ 
    tTrafficProfile    *pTrafficProfile = NULL; 
  
    pTrafficProfile = 
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId); 
    if (pTrafficProfile == NULL) 
    { 
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                      "nmhSetFs2544TrafficProfileThRateStep: " 
                      "Traffic profile Entry not present for %d\r\n", 
                      u4Fs2544TrafficProfileId); 
        return SNMP_FAILURE; 
    } 
    pTrafficProfile->u4TrafProfThRateStep = 
        u4SetValFs2544TrafficProfileThRateStep; 
  
    return SNMP_SUCCESS; 
} 


/****************************************************************************
 Function    :  nmhSetFs2544TrafficProfileRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                setValFs2544TrafficProfileRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544TrafficProfileRowStatus (UINT4 u4Fs2544ContextId,
                                     UINT4 u4Fs2544TrafficProfileId,
                                     INT4 i4SetValFs2544TrafficProfileRowStatus)
{
    tTrafficProfile    *pTrafficProfile = NULL;

    if ((i4SetValFs2544TrafficProfileRowStatus != CREATE_AND_WAIT) &&
        (i4SetValFs2544TrafficProfileRowStatus != CREATE_AND_GO))
    {
        pTrafficProfile =
            R2544UtilGetTrafProfEntry (u4Fs2544ContextId,
                                       u4Fs2544TrafficProfileId);
        if ((pTrafficProfile == NULL)
            && (i4SetValFs2544TrafficProfileRowStatus == DESTROY))
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhSetFs2544TrafficProfileRowStatus: "
                          "Entry is not present for Traffic Profile Id: %d\r\n",
                          u4Fs2544TrafficProfileId);
            CLI_SET_ERR (CLI_RFC2544_TRAF_PROF_NOT_PRESENT);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFs2544TrafficProfileRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:

            /* Memblock creation and initializing the default values
             * to the context are handled inside the below function and
             * rowstatus is initialized here */
            if (R2544UtilAddTrafProfEntry (u4Fs2544ContextId,
                                           u4Fs2544TrafficProfileId) ==
                RFC2544_FAILURE)
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhSetFs2544TrafficProfileRowStatus: "
                              "Failed to create Traffic profile"
                              "Entry for ID %d\r\n", u4Fs2544TrafficProfileId);
                return SNMP_FAILURE;
            }

            break;
        case DESTROY:

            /* The Entry is not present */

            if (R2544UtilDelTrafProfEntry (pTrafficProfile) == RFC2544_FAILURE)
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhSetFs2544TrafficProfileRowStatus: "
                              "Failed to delete Traffic profile"
                              "Entry for ID %d\r\n", u4Fs2544TrafficProfileId);
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }

            break;

        case NOT_IN_SERVICE:
        case NOT_READY:
        case ACTIVE:
            /* All Testing will be handled in Test routine */
            if (NULL != pTrafficProfile)
            {
                pTrafficProfile->u1RowStatus =
                    (UINT1) i4SetValFs2544TrafficProfileRowStatus;
            }
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileName
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileName (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                                   UINT4 u4Fs2544TrafficProfileId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFs2544TrafficProfileName)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    INT4                i4ProfileNamelength = RFC2544_ZERO;
    INT4                i4Index = RFC2544_ZERO;

    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    i4ProfileNamelength =
        (INT4)(RFC2544_STRLEN (pTestValFs2544TrafficProfileName->pu1_OctetList));

    if (i4ProfileNamelength > RFC2544_STRING_MAX_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    for (i4Index = 0; i4Index < i4ProfileNamelength; i4Index++)
    {
        if (!(((pTestValFs2544TrafficProfileName->pu1_OctetList[i4Index] >=
                RFC2544_UPPER_CHAR_START) &&
               (pTestValFs2544TrafficProfileName->pu1_OctetList[i4Index] <=
                RFC2544_UPPER_CHAR_END)) ||
              ((pTestValFs2544TrafficProfileName->pu1_OctetList[i4Index] >=
                RFC2544_LOWER_CHAR_START) &&
               (pTestValFs2544TrafficProfileName->pu1_OctetList[i4Index] <=
                RFC2544_LOWER_CHAR_END)) ||
              ((pTestValFs2544TrafficProfileName->pu1_OctetList[i4Index] >=
                RFC2544_NUM_CHAR_START) &&
               (pTestValFs2544TrafficProfileName->pu1_OctetList[i4Index] <=
                RFC2544_NUM_CHAR_END))))
        {
            RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                         "nmhTestv2Fs2544TrafficProfileName: "
                         "Only characters  (A-Z, a-z) and numeric "
                         "values (0-9) are allowed to configure "
                         "traffic profile name \r\n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TRAF_PROF_NAME_ERR);
            return SNMP_FAILURE;

        }

    }

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileName: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileName: "
                          "Test is in progress hence Traffic profile name "
                          "cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TRAF_PROF_NAME_CONF_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileSeqNoCheck
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileSeqNoCheck
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileSeqNoCheck (UINT4 *pu4ErrorCode,
                                         UINT4 u4Fs2544ContextId,
                                         UINT4 u4Fs2544TrafficProfileId,
                                         INT4
                                         i4TestValFs2544TrafficProfileSeqNoCheck)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544TrafficProfileSeqNoCheck != RFC2544_ENABLED) &&
        (i4TestValFs2544TrafficProfileSeqNoCheck != RFC2544_DISABLED))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileSeqNoCheck: "
                      "Invalid SeqNoCheck for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_TRAF_PROF_SEQ_NO_CHECK);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileSeqNoCheck: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileSeqNoCheck: "
                          " Test is in progress hence Sequence no cannot be "
                          "modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TRAF_PROF_SEQ_NO_CHECK_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileDwellTime
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileDwellTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileDwellTime (UINT4 *pu4ErrorCode,
                                        UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                        i4TestValFs2544TrafficProfileDwellTime)
{
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544TrafficProfileDwellTime <
         RFC2544_TRAFPROF_MIN_DWELL_TIME) ||
        (i4TestValFs2544TrafficProfileDwellTime >
         RFC2544_TRAFPROF_MAX_DWELL_TIME))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileDwellTime: "
                      "Invalid Dwell time for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_TRAF_PROF_DWELL_TIME);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileDwellTime: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileDwellTime: "
                          "Test is in progress hence Dwell Time cannot be "
                          "modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TRAF_PROF_DWELL_TIME_CONF_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileFrameSize
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileFrameSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileFrameSize (UINT4 *pu4ErrorCode,
                                        UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pTestValFs2544TrafficProfileFrameSize)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if (RFC2544_STRLEN (pTestValFs2544TrafficProfileFrameSize->pu1_OctetList) >
        RFC2544_STRING_MAX_LENGTH)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFrameSize: "
                      "Invalid framesize for Traffic Profile %d\r\n", u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_TRAF_PROF_FRAME_SIZE);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFrameSize: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileFrameSize: "
                          "Test is in progress hence Frame size cannot be "
                          "modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TRAF_PROF_FRAME_SIZE_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfilePCP
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfilePCP
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfilePCP (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                                  UINT4 u4Fs2544TrafficProfileId,
                                  INT4 i4TestValFs2544TrafficProfilePCP)
{
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfilePCP: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfilePCP: "
                          "Test is in progress hence PCP cannot be "
                          "modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TRAF_PROF_PCP_CONF_ERR);

            return SNMP_FAILURE;
        }
    }
    if ((i4TestValFs2544TrafficProfilePCP < RFC2544_TRAFPROF_MIN_PCP) ||
        (i4TestValFs2544TrafficProfilePCP > RFC2544_TRAFPROF_MAX_PCP))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfilePCP: "
                      "Invalid PCP for %d\r\n", u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_TRAF_PROF_PCP);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileThTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileThTestStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileThTestStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           INT4
                                           i4TestValFs2544TrafficProfileThTestStatus)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544TrafficProfileThTestStatus != RFC2544_ENABLED) &&
        (i4TestValFs2544TrafficProfileThTestStatus != RFC2544_DISABLED))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileThTestStatus: "
                      "Invalid Throughput test Status for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_TH_TEST_STATUS);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileThTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileThTestStatus: "
                          "Test is in progress hence Throughput Test Status "
                          "cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TH_TEST_STATUS_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileFlTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileFlTestStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileFlTestStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           INT4
                                           i4TestValFs2544TrafficProfileFlTestStatus)
{
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544TrafficProfileFlTestStatus != RFC2544_ENABLED) &&
        (i4TestValFs2544TrafficProfileFlTestStatus != RFC2544_DISABLED))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlTestStatus: "
                      "Invalid Frameloss test Status for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_FL_TEST_STATUS);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileFlTestStatus: "
                          "Test is in progress hence Frame Loss Test Status "
                          "cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_FL_TEST_STATUS_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileLaTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileLaTestStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileLaTestStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           INT4
                                           i4TestValFs2544TrafficProfileLaTestStatus)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544TrafficProfileLaTestStatus != RFC2544_ENABLED) &&
        (i4TestValFs2544TrafficProfileLaTestStatus != RFC2544_DISABLED))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileLaTestStatus: "
                      "Invalid Latency test Status for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_LA_TEST_STATUS);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileLaTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4TestValFs2544TrafficProfileLaTestStatus == RFC2544_ENABLED)
    {
        if (pTrafficProfile->u1TrafProfThTestStatus != RFC2544_ENABLED)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileLaTestStatus: "
                          "Latency cannot be enabled when throughput "
                          "is disabled %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RFC2544_INVALID_LA_TEST_STATUS);
            return SNMP_FAILURE;
        }

    }
    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileLaTestStatus: "
                          "Test is in progress hence Latency Test Status "
                          "cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_LA_TEST_STATUS_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileBbTestStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileBbTestStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileBbTestStatus (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           INT4
                                           i4TestValFs2544TrafficProfileBbTestStatus)
{
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544TrafficProfileBbTestStatus != RFC2544_ENABLED) &&
        (i4TestValFs2544TrafficProfileBbTestStatus != RFC2544_DISABLED))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileBbTestStatus: "
                      "Invalid Back-to-Back test Status for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_BB_TEST_STATUS);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileBbTestStatus: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileBbTestStatus: "
                          "Test is in progress hence Back to back Test Status "
                          "cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_BB_TEST_STATUS_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileThTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileThTrialDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileThTrialDuration (UINT4 *pu4ErrorCode,
                                              UINT4 u4Fs2544ContextId,
                                              UINT4 u4Fs2544TrafficProfileId,
                                              UINT4
                                              u4TestValFs2544TrafficProfileThTrialDuration)
{
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((u4TestValFs2544TrafficProfileThTrialDuration <
         RFC2544_TH_MIN_TRIALDURATION) ||
        (u4TestValFs2544TrafficProfileThTrialDuration >
         RFC2544_TH_MAX_TRIALDURATION))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileThTrialDuration: "
                      "Invalid Throughput Trial Duration for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_TH_TRIAL_DURATION);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileThTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileThTrialDuration: "
                          "Test is in progress hence Throughput Test "
                          "Trail Duration cannot be modified "
                          "for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TH_TRIAL_DURATION_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileThMaxRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileThMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileThMaxRate (UINT4 *pu4ErrorCode,
                                        UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        UINT4
                                        u4TestValFs2544TrafficProfileThMaxRate)
{
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    /*  The min value for Throughput min rate  is zero 
     *  Comparison of unsigned expression < 0 is always false 
     *  hence only max comparision is done */

    if ((u4TestValFs2544TrafficProfileThMaxRate > RFC2544_HUNDRED) ||  
         (u4TestValFs2544TrafficProfileThMaxRate < RFC2544_TEN)) 
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileThMaxRate: "
                      "Invalid Max rate %d\r\n", u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RFC2544_INVALID_MAX_RATE);
        return SNMP_FAILURE;
    }
    if (RFC2544_ZERO != (u4TestValFs2544TrafficProfileThMaxRate %  RFC2544_TEN)) 
    { 
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                      "nmhTestv2Fs2544TrafficProfileThMaxRate: " 
                      "Max rate should be in multiples of 10 for traffic profile Id %d\r\n", u4Fs2544TrafficProfileId); 
        *pu4ErrorCode = SNMP_ERR_NO_CREATION; 
        CLI_SET_ERR (CLI_RFC2544_MAX_RATE_ERR); 
        return SNMP_FAILURE; 
    } 

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileThMaxRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileThMaxRate: "
                          "Test is in progress hence Throughput Test max rate "
                          "cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TH_MAX_RATE_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileThMinRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileThMinRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileThMinRate (UINT4 *pu4ErrorCode,
                                        UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        UINT4
                                        u4TestValFs2544TrafficProfileThMinRate)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    /*  The min value for Throughput min rate  is zero
     *  Comparison of unsigned expression < 0 is always false
     *  hence only max comparision is done */

     if ((u4TestValFs2544TrafficProfileThMinRate > RFC2544_HUNDRED) ||  
         (u4TestValFs2544TrafficProfileThMinRate < RFC2544_TEN)) 
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileThMinRate: "
                      "Invalid Min rate %d\r\n", u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RFC2544_INVALID_MIN_RATE);
        return SNMP_FAILURE;
    }
    
    if (RFC2544_ZERO != (u4TestValFs2544TrafficProfileThMinRate %  RFC2544_TEN)) 
    { 
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                      "nmhTestv2Fs2544TrafficProfileThMinRate: " 
                      "Min rate should be in multiples of 10 for traffic profile Id %d\r\n", u4Fs2544TrafficProfileId); 
        *pu4ErrorCode = SNMP_ERR_NO_CREATION; 
        CLI_SET_ERR (CLI_RFC2544_MAX_RATE_ERR); 
        return SNMP_FAILURE;
    } 

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileThMinRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileThMinRate: "
                          "Test is in progress hence Throughput Test min rate "
                          "cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TH_MIN_RATE_CONF_ERR);

            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileLaTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileLaTrialDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileLaTrialDuration (UINT4 *pu4ErrorCode,
                                              UINT4 u4Fs2544ContextId,
                                              UINT4 u4Fs2544TrafficProfileId,
                                              UINT4
                                              u4TestValFs2544TrafficProfileLaTrialDuration)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((u4TestValFs2544TrafficProfileLaTrialDuration <
         RFC2544_LA_MIN_TRIALDURATION) ||
        (u4TestValFs2544TrafficProfileLaTrialDuration >
         RFC2544_LA_MAX_TRIALDURATION))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileLaTrialDuration: "
                      "Invalid Latency Trial Duration for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_LA_TRIAL_DURATION);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileLaTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileLaTrialDuration: "
                          " Test is in progress hence Latency Test Trail "
                          "Duration cannot be modified  for Traffic "
                          "Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_LA_TRIAL_DURATION_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileLaDelayMeasureInterval
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileLaDelayMeasureInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileLaDelayMeasureInterval (UINT4 *pu4ErrorCode,
                                                     UINT4 u4Fs2544ContextId,
                                                     UINT4
                                                     u4Fs2544TrafficProfileId,
                                                     UINT4
                                                     u4TestValFs2544TrafficProfileLaDelayMeasureInterval)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((u4TestValFs2544TrafficProfileLaDelayMeasureInterval <
         RFC2544_LA_MIN_DELAY_INTERVAL) ||
        (u4TestValFs2544TrafficProfileLaDelayMeasureInterval >
         RFC2544_LA_MAX_DELAY_INTERVAL))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileLaDelayMeasureInterval:"
                      "Invalid Latency Delay Measure interval for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_LA_DELAY_MEASURE_INTERVAL);

        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileLaDelayMeasureInterval: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileLaDelayMeasureInterval: "
                          "Test is in progress hence Latency Test Delay "
                          "Measurement Interval cannot be modified for "
                          "Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_LA_DELAY_MEASURE_INTERVAL_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileFlTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileFlTrialDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileFlTrialDuration (UINT4 *pu4ErrorCode,
                                              UINT4 u4Fs2544ContextId,
                                              UINT4 u4Fs2544TrafficProfileId,
                                              UINT4
                                              u4TestValFs2544TrafficProfileFlTrialDuration)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((u4TestValFs2544TrafficProfileFlTrialDuration <
         RFC2544_FL_MIN_TRIALDURATION) ||
        (u4TestValFs2544TrafficProfileFlTrialDuration >
         RFC2544_FL_MAX_TRIALDURATION))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlTrialDuration:"
                      "Invalid Frameloss Trialduration for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_FL_TRIAL_DURATION);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileFlTrialDuration: "
                          "Test is in progress hence Frameloss Test Trail "
                          "Duration cannot be modified for traffic Profile "
                          "%d\r\n", u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_FL_TRIAL_DURATION_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileFlMaxRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileFlMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileFlMaxRate (UINT4 *pu4ErrorCode,
                                        UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        UINT4
                                        u4TestValFs2544TrafficProfileFlMaxRate)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }

    if ((u4TestValFs2544TrafficProfileFlMaxRate > RFC2544_HUNDRED)  
            || (u4TestValFs2544TrafficProfileFlMaxRate < RFC2544_TWENTY)) 
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlMaxRate: "
                      "Invalid Max rate for Traffic profile entry %d\r\n", 
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RFC2544_INVALID_MAX_RATE);
        return SNMP_FAILURE;
    }

    if (RFC2544_ZERO != (u4TestValFs2544TrafficProfileFlMaxRate %  RFC2544_TEN)) 
    { 
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                      "nmhTestv2Fs2544TrafficProfileFlMaxRate: " 
                      "Max rate should be in multiples of 10 for " 
                      "Traffic profile entry %d\r\n", u4Fs2544TrafficProfileId); 
        *pu4ErrorCode = SNMP_ERR_NO_CREATION; 
        CLI_SET_ERR (CLI_RFC2544_MAX_RATE_ERR); 
        return SNMP_FAILURE; 
    }

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlMaxRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileFlMaxRate: "
                          "Test is in progress hence Frameloss Test Trail max "
                          " rate cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_FL_MAX_RATE_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileFlMinRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileFlMinRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileFlMinRate (UINT4 *pu4ErrorCode,
                                        UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        UINT4
                                        u4TestValFs2544TrafficProfileFlMinRate)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }

    if ((u4TestValFs2544TrafficProfileFlMinRate > RFC2544_NINTY)  
            || (u4TestValFs2544TrafficProfileFlMinRate < RFC2544_TEN)) 
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlMinRate: "
                      "Invalid Min rate for Traffic profile entry %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RFC2544_INVALID_MIN_RATE);
        return SNMP_FAILURE;
    }

    if (RFC2544_ZERO != (u4TestValFs2544TrafficProfileFlMinRate %  RFC2544_TEN)) 
    { 
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                      "nmhTestv2Fs2544TrafficProfileFlMinRate: " 
                      "Min rate should be in multiples of 10 for " 
                      "Traffic profile entry %d\r\n", u4Fs2544TrafficProfileId); 
        *pu4ErrorCode = SNMP_ERR_NO_CREATION; 
        CLI_SET_ERR (CLI_RFC2544_MIN_RATE_ERR); 
        return SNMP_FAILURE; 
    } 

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlMinRate: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileFlMinRate: "
                          "Test is in progress hence Frameloss Test Trail min "
                          "rate cannot be modified  for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_FL_MIN_RATE_CONF_ERR);

            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileFlRateStep
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileFlRateStep
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileFlRateStep (UINT4 *pu4ErrorCode,
                                         UINT4 u4Fs2544ContextId,
                                         UINT4 u4Fs2544TrafficProfileId,
                                         UINT4
                                         u4TestValFs2544TrafficProfileFlRateStep)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    /*  The min value for frame loss rate step is zero
     *  Comparison of unsigned expression < 0 is always false
     *  hence only max comparision is done */
   
    if ((u4TestValFs2544TrafficProfileFlRateStep != RFC2544_TEN) && 
             (u4TestValFs2544TrafficProfileFlRateStep != RFC2544_FIVE)) 
    {
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                     "nmhTestv2Fs2544TrafficProfileFlRateStep: "
                     "Invalid Rate Step\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RFC2544_INVALID_RATE_STEP);
        return SNMP_FAILURE;
    }

    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileFlRateStep: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileFlRateStep: "
                          "Test is in progress hence Frameloss Test Trail "
                          "rate step cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_FL_RATE_STEP_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileBbTrialDuration
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileBbTrialDuration
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileBbTrialDuration (UINT4 *pu4ErrorCode,
                                              UINT4 u4Fs2544ContextId,
                                              UINT4 u4Fs2544TrafficProfileId,
                                              UINT4
                                              u4TestValFs2544TrafficProfileBbTrialDuration)
{
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;

    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((u4TestValFs2544TrafficProfileBbTrialDuration <
         RFC2544_BB_MIN_TRIALDURATION) ||
        (u4TestValFs2544TrafficProfileBbTrialDuration >
         RFC2544_BB_MAX_TRIALDURATION))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileBbTrialDuration:"
                      "Invalid Back-to-Back test Trialduration for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_BB_TRIAL_DURATION);

        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileBbTrialDuration: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileBbTrialDuration: "
                          "Test is in progress hence Back to back Test Trail "
                          "Duration cannot be modified for traffic Profile "
                          "%d\r\n", u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_BB_TRIAL_DURATION_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileBbTrialCount
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileBbTrialCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileBbTrialCount (UINT4 *pu4ErrorCode,
                                           UINT4 u4Fs2544ContextId,
                                           UINT4 u4Fs2544TrafficProfileId,
                                           UINT4
                                           u4TestValFs2544TrafficProfileBbTrialCount)
{
    tSlaEntry          *pSlaEntry = NULL;

    tTrafficProfile    *pTrafficProfile = NULL;
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((u4TestValFs2544TrafficProfileBbTrialCount <
         RFC2544_BB_MIN_TRIALCOUNT) ||
        (u4TestValFs2544TrafficProfileBbTrialCount > RFC2544_BB_MAX_TRIALCOUNT))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileBbTrialCount: "
                      "Invalid Back-to-Back test Trial count for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RFC2544_INVALID_BB_TRIAL_COUNT);
        return SNMP_FAILURE;
    }
    pTrafficProfile =
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId);
    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileBbTrialCount: "
                      "Entry is not present for Traffic Profile Id: %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pSlaEntry =
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId,
                                        u4Fs2544TrafficProfileId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileBbTrialCount: "
                          "Test is in progress hence Back to back Test Trail "
                          "Count cannot be modified for Traffic Profile %d\r\n",
                          u4Fs2544TrafficProfileId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_BB_TRIAL_COUNT_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/**************************************************************************** 
 Function    :  nmhTestv2Fs2544TrafficProfileThRateStep 
 Input       :  The Indices 
                Fs2544ContextId 
                Fs2544TrafficProfileId 
  
                The Object 
                testValFs2544TrafficProfileThRateStep 
 Output      :  The Test Low Lev Routine Take the Indices & 
                Test whether that Value is Valid Input for Set. 
                Stores the value of error code in the Return val 
 Error Codes :  The following error codes are to be returned 
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905) 
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905) 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE 
****************************************************************************/ 
INT1 nmhTestv2Fs2544TrafficProfileThRateStep(UINT4 *pu4ErrorCode , UINT4 u4Fs2544ContextId , 
                                             UINT4 u4Fs2544TrafficProfileId ,  
                                             UINT4 u4TestValFs2544TrafficProfileThRateStep) 
{ 
    tSlaEntry          *pSlaEntry = NULL; 
  
    tTrafficProfile    *pTrafficProfile = NULL; 
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId, 
                                         u4Fs2544TrafficProfileId, 
                                         pu4ErrorCode) == RFC2544_FAILURE) 
    { 
        /* The function  will take care of filling the error code, 
           CLI set error and trace messages. */ 
        return SNMP_FAILURE; 
    } 
    /*  The value for Throughput rate step are zero, five and ten*/ 
  
    if ((u4TestValFs2544TrafficProfileThRateStep != RFC2544_TEN) &&  
        (u4TestValFs2544TrafficProfileThRateStep != RFC2544_ZERO) &&  
        (u4TestValFs2544TrafficProfileThRateStep != RFC2544_FIVE)) 
    { 
        RFC2544_TRC (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                     "nmhTestv2Fs2544TrafficProfileThRateStep: " 
                     "Invalid Rate Step\r\n"); 
        *pu4ErrorCode = SNMP_ERR_NO_CREATION; 
        CLI_SET_ERR (CLI_RFC2544_INVALID_RATE_STEP); 
        return SNMP_FAILURE; 
    } 
  
    pTrafficProfile = 
        R2544UtilGetTrafProfEntry (u4Fs2544ContextId, u4Fs2544TrafficProfileId); 
    if (pTrafficProfile == NULL) 
    { 
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                      "nmhTestv2Fs2544TrafficProfileThRateStep: " 
                      "Traffic profile Entry not present for %d\r\n", 
                      u4Fs2544TrafficProfileId); 
        *pu4ErrorCode = SNMP_ERR_NO_CREATION; 
        return SNMP_FAILURE; 
    } 
  
    pSlaEntry = 
        R2544UtilIsTrafProfMappedToSla (u4Fs2544ContextId, 
                                        u4Fs2544TrafficProfileId); 
    if (pSlaEntry != NULL) 
    { 
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS) 
  
        { 
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId, 
                          "nmhTestv2Fs2544TrafficProfileThRateStep: " 
                          "Test is in progress hence Throughput Test Trail " 
                          "rate step cannot be modified for traffic Profile %d\r\n", 
                          u4Fs2544TrafficProfileId); 
            *pu4ErrorCode = SNMP_ERR_NO_CREATION; 
            CLI_SET_ERR (CLI_RFC2544_FL_RATE_STEP_CONF_ERR); 
  
            return SNMP_FAILURE; 
        } 
    } 
  
    return SNMP_SUCCESS; 
  
} 
  


/****************************************************************************
 Function    :  nmhTestv2Fs2544TrafficProfileRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId

                The Object 
                testValFs2544TrafficProfileRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544TrafficProfileRowStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544TrafficProfileId,
                                        INT4
                                        i4TestValFs2544TrafficProfileRowStatus)
{
    if (R2544UtilValidateTrafficProfile (u4Fs2544ContextId,
                                         u4Fs2544TrafficProfileId,
                                         pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544TrafficProfileRowStatus < ACTIVE) ||
        (i4TestValFs2544TrafficProfileRowStatus > DESTROY))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544TrafficProfileRowStatus "
                      "Invalid TrafficProfile RowStatus for Traffic Profile %d\r\n",
                      u4Fs2544TrafficProfileId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFs2544TrafficProfileRowStatus == ACTIVE)
    {
        if (R2544UtilValidateRate (u4Fs2544ContextId, u4Fs2544TrafficProfileId)
            == RFC2544_FAILURE)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544TrafficProfileRowStatus: "
                          "Rate Validation failed for Traffic profile"
                          " ID %d\r\n", u4Fs2544TrafficProfileId);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs2544TrafficProfileTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544TrafficProfileId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs2544TrafficProfileTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs2544SacTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs2544SacTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs2544SacTable (UINT4 u4Fs2544ContextId,
                                        UINT4 u4Fs2544SacId)
{
    if (R2544CxtIsContextExist (u4Fs2544ContextId) != RFC2544_TRUE)
    {
        RFC2544_GLOBAL_TRC ("nmhValidateIndexInstanceFs2544SacTable: "
                            "Invalid Context  \r\n");

        return SNMP_FAILURE;
    }
    if ((u4Fs2544SacId < RFC2544_MIN_SAC_ID) ||
        (u4Fs2544SacId > RFC2544_MAX_SAC_ID))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhValidateIndexInstanceFs2544SacTable: "
                      "Invalid SAC Identifier %d\r\n", u4Fs2544SacId);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs2544SacTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs2544SacTable (UINT4 *pu4Fs2544ContextId,
                                UINT4 *pu4Fs2544SacId)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = (tSacEntry *) RBTreeGetFirst (RFC2544_SAC_TABLE ());
    if (pSacEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4Fs2544ContextId = pSacEntry->u4ContextId;
    *pu4Fs2544SacId = pSacEntry->u4SacId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFs2544SacTable
 Input       :  The Indices
                Fs2544ContextId
                nextFs2544ContextId
                Fs2544SacId
                nextFs2544SacId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs2544SacTable (UINT4 u4Fs2544ContextId,
                               UINT4 *pu4NextFs2544ContextId,
                               UINT4 u4Fs2544SacId, UINT4 *pu4NextFs2544SacId)
{
    tSacEntry          *pSacEntry = NULL;
    tSacEntry           SacEntry;

    MEMSET (&SacEntry, RFC2544_ZERO, sizeof (tSacEntry));

    SacEntry.u4ContextId = u4Fs2544ContextId;
    SacEntry.u4SacId = u4Fs2544SacId;

    pSacEntry = (tSacEntry *) RBTreeGetNext
        (RFC2544_SAC_TABLE (), (tRBElem *) & SacEntry, NULL);
    if (pSacEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4NextFs2544ContextId = pSacEntry->u4ContextId;
    *pu4NextFs2544SacId = pSacEntry->u4SacId;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs2544SacThAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                retValFs2544SacThAllowedFrameLoss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SacThAllowedFrameLoss (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SacId,
                                   UINT4 *pu4RetValFs2544SacThAllowedFrameLoss)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SacThAllowedFrameLoss: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SacThAllowedFrameLoss = pSacEntry->u4SacThAllowedFrameLoss;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544SacLaAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                retValFs2544SacLaAllowedFrameLoss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SacLaAllowedFrameLoss (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SacId,
                                   UINT4 *pu4RetValFs2544SacLaAllowedFrameLoss)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SacLaAllowedFrameLoss: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SacLaAllowedFrameLoss = pSacEntry->u4SacLaAllowedFrameLoss;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544SacFlAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                retValFs2544SacFlAllowedFrameLoss
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SacFlAllowedFrameLoss (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SacId,
                                   UINT4 *pu4RetValFs2544SacFlAllowedFrameLoss)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SacFlAllowedFrameLoss: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544SacFlAllowedFrameLoss = pSacEntry->u4SacFlAllowedFrameLoss;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544SacRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                retValFs2544SacRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544SacRowStatus (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SacId,
                          INT4 *pi4RetValFs2544SacRowStatus)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544SacRowStatus: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544SacRowStatus = pSacEntry->u1RowStatus;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFs2544SacThAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                setValFs2544SacThAllowedFrameLoss
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SacThAllowedFrameLoss (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SacId,
                                   UINT4 u4SetValFs2544SacThAllowedFrameLoss)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhsetFs2544SacThAllowedFrameLoss: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        return SNMP_FAILURE;
    }
    pSacEntry->u4SacThAllowedFrameLoss = u4SetValFs2544SacThAllowedFrameLoss;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFs2544SacLaAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                setValFs2544SacLaAllowedFrameLoss
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SacLaAllowedFrameLoss (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SacId,
                                   UINT4 u4SetValFs2544SacLaAllowedFrameLoss)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhsetFs2544SacLaAllowedFrameLoss: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        return SNMP_FAILURE;
    }
    pSacEntry->u4SacLaAllowedFrameLoss = u4SetValFs2544SacLaAllowedFrameLoss;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs2544SacFlAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                setValFs2544SacFlAllowedFrameLoss
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SacFlAllowedFrameLoss (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SacId,
                                   UINT4 u4SetValFs2544SacFlAllowedFrameLoss)
{
    tSacEntry          *pSacEntry = NULL;

    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhsetFs2544SacFlAllowedFrameLoss: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        return SNMP_FAILURE;
    }
    pSacEntry->u4SacFlAllowedFrameLoss = u4SetValFs2544SacFlAllowedFrameLoss;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFs2544SacRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                setValFs2544SacRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFs2544SacRowStatus (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SacId,
                          INT4 i4SetValFs2544SacRowStatus)
{
    tSacEntry          *pSacEntry = NULL;

    if ((i4SetValFs2544SacRowStatus != CREATE_AND_WAIT) &&
        (i4SetValFs2544SacRowStatus != CREATE_AND_GO))
    {
        pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
        if ((pSacEntry == NULL) && (i4SetValFs2544SacRowStatus == DESTROY))
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhSetFs2544SacRowStatus: "
                          "Sac Entry not present for %d\r\n", u4Fs2544SacId);
            CLI_SET_ERR (CLI_RFC2544_SAC_NOT_PRESENT);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValFs2544SacRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:

            /* Memblock creation and initializing the default values
             * to the context are handled inside the below function and
             * rowstatus is initialized here */
            if (R2544UtilAddSacEntry (u4Fs2544ContextId,
                                      u4Fs2544SacId) == RFC2544_FAILURE)
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhSetFs2544SacRowStatus: "
                              "Failed to create Sac"
                              "Entry for ID %d\r\n", u4Fs2544SacId);
                return SNMP_FAILURE;
            }

            break;
        case DESTROY:

            /* The Entry is not present */

            if (R2544UtilDelSacEntry (pSacEntry) == RFC2544_FAILURE)
            {
                RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                              "nmhSetFs2544SacRowStatus: "
                              "Failed to delete Sac"
                              "Entry for ID %d\r\n", u4Fs2544SacId);
                return SNMP_FAILURE;
            }
            else
            {
                return SNMP_SUCCESS;
            }

            break;

        case NOT_IN_SERVICE:
        case NOT_READY:
        case ACTIVE:
            /* All Testing will be handled in Test routine */
            if (NULL != pSacEntry)
            {
                pSacEntry->u1RowStatus = (UINT1) i4SetValFs2544SacRowStatus;
            }
            break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fs2544SacThAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                testValFs2544SacThAllowedFrameLoss
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SacThAllowedFrameLoss (UINT4 *pu4ErrorCode,
                                      UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544SacId,
                                      UINT4
                                      u4TestValFs2544SacThAllowedFrameLoss)
{
    tSlaEntry          *pSlaEntry = NULL;

    tSacEntry          *pSacEntry = NULL;
    if (R2544UtilValidateSac (u4Fs2544ContextId,
                              u4Fs2544SacId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    /*  The min value for allowedd frmae loss is zero 
     *  Comparison of unsigned expression < 0 is always false 
     *  hence only max comparision is done */

    if ((u4TestValFs2544SacThAllowedFrameLoss >
         RFC2544_MAX_TH_ALLOWED_FRAMELOSS))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SacThAllowedFrameLoss: "
                      "Invalid Sac Throughput AllowedFrameLoss for Sac %d\r\n",
                      u4Fs2544SacId);
        CLI_SET_ERR (CLI_RFC2544_INVALID_TH_FRAMELOSS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SacThAllowedFrameLoss: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pSlaEntry = R2544UtilIsSacMappedToSla (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SacThAllowedFrameLoss: "
                          "Test is in progress hence Throughput Test Allowed "
                          "Frameloss cannot be modified for Sac %d\r\n",
                          u4Fs2544SacId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_TH_FRAMELOSS_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SacLaAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                testValFs2544SacLaAllowedFrameLoss
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SacLaAllowedFrameLoss (UINT4 *pu4ErrorCode,
                                      UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544SacId,
                                      UINT4
                                      u4TestValFs2544SacLaAllowedFrameLoss)
{
    tSlaEntry          *pSlaEntry = NULL;

    tSacEntry          *pSacEntry = NULL;
    if (R2544UtilValidateSac (u4Fs2544ContextId,
                              u4Fs2544SacId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    /*  The min value for allowedd frmae loss is zero 
     *  Comparison of unsigned expression < 0 is always false 
     *  hence only max comparision is done */

    if ((u4TestValFs2544SacLaAllowedFrameLoss >
         RFC2544_MAX_LA_ALLOWED_FRAMELOSS))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SacLaAllowedFrameLoss: "
                      "Invalid Sac latency test AllowedFrameLoss for Sac %d\r\n",
                      u4Fs2544SacId);
        CLI_SET_ERR (CLI_RFC2544_INVALID_LA_FRAMELOSS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SacLaAllowedFrameLoss: "
                      "Entry not present for Sac Id: %d\r\n", u4Fs2544SacId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pSlaEntry = R2544UtilIsSacMappedToSla (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SacLaAllowedFrameLoss: "
                          "Test is in progress hence Latency Test Allowed "
                          "Frameloss cannot be modified for Sac %d\r\n",
                          u4Fs2544SacId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_LA_FRAMELOSS_CONF_ERR);

            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SacFlAllowedFrameLoss
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                testValFs2544SacFlAllowedFrameLoss
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SacFlAllowedFrameLoss (UINT4 *pu4ErrorCode,
                                      UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544SacId,
                                      UINT4
                                      u4TestValFs2544SacFlAllowedFrameLoss)
{
    tSlaEntry          *pSlaEntry = NULL;

    tSacEntry          *pSacEntry = NULL;
    if (R2544UtilValidateSac (u4Fs2544ContextId,
                              u4Fs2544SacId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    /*  The min value for allowedd frmae loss is zero 
     *  Comparison of unsigned expression < 0 is always false 
     *  hence only max comparision is done */

    if ((u4TestValFs2544SacFlAllowedFrameLoss >
         RFC2544_MAX_FL_ALLOWED_FRAMELOSS))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SacFlAllowedFrameLoss: "
                      "Invalid Sac frameloss test AllowedFrameLoss for Sac %d\r\n",
                      u4Fs2544SacId);
        CLI_SET_ERR (CLI_RFC2544_INVALID_FL_FRAMELOSS);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    pSacEntry = R2544UtilGetSacEntry (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SacFlAllowedFrameLoss: "
                      "Entry is not present for Sac Id: %d\r\n", u4Fs2544SacId);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    pSlaEntry = R2544UtilIsSacMappedToSla (u4Fs2544ContextId, u4Fs2544SacId);
    if (pSlaEntry != NULL)
    {
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)

        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                          "nmhTestv2Fs2544SacFlAllowedFrameLoss: "
                          "Test is in progress hence Frameloss Test Allowed"
                          "Frameloss cannot be modified for Sac %d\r\n",
                          u4Fs2544SacId);
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CLI_SET_ERR (CLI_RFC2544_FL_FRAMELOSS_CONF_ERR);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fs2544SacRowStatus
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId

                The Object 
                testValFs2544SacRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fs2544SacRowStatus (UINT4 *pu4ErrorCode, UINT4 u4Fs2544ContextId,
                             UINT4 u4Fs2544SacId,
                             INT4 i4TestValFs2544SacRowStatus)
{
    if (R2544UtilValidateSac (u4Fs2544ContextId,
                              u4Fs2544SacId, pu4ErrorCode) == RFC2544_FAILURE)
    {
        /* The function  will take care of filling the error code,
           CLI set error and trace messages. */
        return SNMP_FAILURE;
    }
    if ((i4TestValFs2544SacRowStatus < ACTIVE) ||
        (i4TestValFs2544SacRowStatus > DESTROY))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhTestv2Fs2544SacRowStatus "
                      "Invalid SAC Row status for Sac %d\r\n", u4Fs2544SacId);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fs2544SacTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SacId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fs2544SacTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fs2544ReportStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFs2544ReportStatsTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFs2544ReportStatsTable (UINT4 u4Fs2544ContextId,
                                                UINT4 u4Fs2544SlaId,
                                                UINT4
                                                u4Fs2544ReportStatsFrameSize)
{
    if (R2544CxtIsContextExist (u4Fs2544ContextId) != RFC2544_TRUE)
    {
        RFC2544_GLOBAL_TRC ("nmhValidateIndexInstanceFs2544SacTable: "
                            "Invalid Context  \r\n");
        return SNMP_FAILURE;
    }
    if ((u4Fs2544SlaId < RFC2544_MIN_SLA_ID) ||
        (u4Fs2544SlaId > RFC2544_MAX_SLA_ID))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhValidateIndexInstanceFs2544ReportStatsTable: "
                      "Invalid Sla %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    if ((u4Fs2544ReportStatsFrameSize != RFC2544_FRAME_64) &&
        (u4Fs2544ReportStatsFrameSize != RFC2544_FRAME_128) &&
        (u4Fs2544ReportStatsFrameSize != RFC2544_FRAME_256) &&
        (u4Fs2544ReportStatsFrameSize != RFC2544_FRAME_512) &&
        (u4Fs2544ReportStatsFrameSize != RFC2544_FRAME_768) &&
        (u4Fs2544ReportStatsFrameSize != RFC2544_FRAME_1024) &&
        (u4Fs2544ReportStatsFrameSize != RFC2544_FRAME_1280) &&
        (u4Fs2544ReportStatsFrameSize != RFC2544_FRAME_1518))
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhValidateIndexInstanceFs2544ReportStatsTable: "
                      "Invalid Frame Size %d\r\n",
                      u4Fs2544ReportStatsFrameSize);
        return SNMP_FAILURE;

    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFs2544ReportStatsTable
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFs2544ReportStatsTable (UINT4 *pu4Fs2544ContextId,
                                        UINT4 *pu4Fs2544SlaId,
                                        UINT4 *pu4Fs2544ReportStatsFrameSize)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        (tReportStatistics *) RBTreeGetFirst (RFC2544_REPORT_STATS_TABLE ());
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, *pu4Fs2544ContextId,
                      "nmhGetFirstIndexFs2544ReportStatsTable: "
                      "Report is not present for SLA %d\r\n", *pu4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4Fs2544ContextId = pReportStatistics->u4ReportStatsContextId;
    *pu4Fs2544SlaId = pReportStatistics->u4ReportStatsSlaId;
    *pu4Fs2544ReportStatsFrameSize = pReportStatistics->u4ReportStatsFrameSize;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFs2544ReportStatsTable
 Input       :  The Indices
                Fs2544ContextId
                nextFs2544ContextId
                Fs2544SlaId
                nextFs2544SlaId
                Fs2544ReportStatsFrameSize
                nextFs2544ReportStatsFrameSize
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFs2544ReportStatsTable (UINT4 u4Fs2544ContextId,
                                       UINT4 *pu4NextFs2544ContextId,
                                       UINT4 u4Fs2544SlaId,
                                       UINT4 *pu4NextFs2544SlaId,
                                       UINT4 u4Fs2544ReportStatsFrameSize,
                                       UINT4 *pu4NextFs2544ReportStatsFrameSize)
{
    tReportStatistics  *pReportStatistics = NULL;
    tReportStatistics   ReportStatistics;

    MEMSET (&ReportStatistics, RFC2544_ZERO, sizeof (tReportStatistics));

    ReportStatistics.u4ReportStatsContextId = u4Fs2544ContextId;
    ReportStatistics.u4ReportStatsSlaId = u4Fs2544SlaId;
    ReportStatistics.u4ReportStatsFrameSize = u4Fs2544ReportStatsFrameSize;

    pReportStatistics = (tReportStatistics *) RBTreeGetNext
        (RFC2544_REPORT_STATS_TABLE (), (tRBElem *) & ReportStatistics, NULL);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetNextIndexFs2544ReportStatsTable: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }

    *pu4NextFs2544ContextId = pReportStatistics->u4ReportStatsContextId;
    *pu4NextFs2544SlaId = pReportStatistics->u4ReportStatsSlaId;
    *pu4NextFs2544ReportStatsFrameSize =
        pReportStatistics->u4ReportStatsFrameSize;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsThVerifiedBps
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsThVerifiedBps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsThVerifiedBps (UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544SlaId,
                                      UINT4 u4Fs2544ReportStatsFrameSize,
                                      UINT4
                                      *pu4RetValFs2544ReportStatsThVerifiedBps)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsThVerifiedBps: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544ReportStatsThVerifiedBps =
        pReportStatistics->u4ReportStatsThVerifiedBps;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsThResult
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsThResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsThResult (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                                 UINT4 u4Fs2544ReportStatsFrameSize,
                                 INT4 *pi4RetValFs2544ReportStatsThResult)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsThResult: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544ReportStatsThResult =
        pReportStatistics->u1ReportStatsThResult;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsLatencyMin
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsLatencyMin
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsLatencyMin (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                                   UINT4 u4Fs2544ReportStatsFrameSize,
                                   UINT4 *pu4RetValFs2544ReportStatsLatencyMin)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsLatencyMin: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544ReportStatsLatencyMin =
        pReportStatistics->u4ReportStatsLatencyMin;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsLatencyMax
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsLatencyMax
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsLatencyMax (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                                   UINT4 u4Fs2544ReportStatsFrameSize,
                                   UINT4 *pu4RetValFs2544ReportStatsLatencyMax)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsLatencyMax: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544ReportStatsLatencyMax =
        pReportStatistics->u4ReportStatsLatencyMax;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsLatencyMean
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsLatencyMean
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsLatencyMean (UINT4 u4Fs2544ContextId,
                                    UINT4 u4Fs2544SlaId,
                                    UINT4 u4Fs2544ReportStatsFrameSize,
                                    UINT4
                                    *pu4RetValFs2544ReportStatsLatencyMean)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsLatencyMean: "
                      "Report not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pu4RetValFs2544ReportStatsLatencyMean =
        pReportStatistics->u4ReportStatsLatencyMean;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsLatencyFailCount
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsLatencyFailCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsLatencyFailCount (UINT4 u4Fs2544ContextId,
                                         UINT4 u4Fs2544SlaId,
                                         UINT4 u4Fs2544ReportStatsFrameSize,
                                         INT4
                                         *pi4RetValFs2544ReportStatsLatencyFailCount)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsLatencyFailCount: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544ReportStatsLatencyFailCount =
        (INT4) pReportStatistics->u4ReportStatsLatencyFailCount;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsLaIterationCalculated
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsLaIterationCalculated
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsLaIterationCalculated (UINT4 u4Fs2544ContextId,
                                              UINT4 u4Fs2544SlaId,
                                              UINT4
                                              u4Fs2544ReportStatsFrameSize,
                                              INT4
                                              *pi4RetValFs2544ReportStatsLaIterationCalculated)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsLaIterationCalculated: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544ReportStatsLaIterationCalculated =
        (INT4) pReportStatistics->u4ReportStatsLaIterationCalculated;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsLatencyResult
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsLatencyResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsLatencyResult (UINT4 u4Fs2544ContextId,
                                      UINT4 u4Fs2544SlaId,
                                      UINT4 u4Fs2544ReportStatsFrameSize,
                                      INT4
                                      *pi4RetValFs2544ReportStatsLatencyResult)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsLatencyResult: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544ReportStatsLatencyResult =
        pReportStatistics->u1ReportStatsLatencyResult;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsFLossRate
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsFLossRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsFLossRate (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                                  UINT4 u4Fs2544ReportStatsFrameSize,
                                  INT4 *pi4RetValFs2544ReportStatsFLossRate)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsFLossRate: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544ReportStatsFLossRate =
        (INT4) pReportStatistics->u4ReportStatsFrameLossRate;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsFLResult
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsFLResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsFLResult (UINT4 u4Fs2544ContextId, UINT4 u4Fs2544SlaId,
                                 UINT4 u4Fs2544ReportStatsFrameSize,
                                 INT4 *pi4RetValFs2544ReportStatsFLResult)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsFLResult: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544ReportStatsFLResult =
        pReportStatistics->u1ReportStatsFrameLossResult;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFs2544ReportStatsBacktoBackBurstSize
 Input       :  The Indices
                Fs2544ContextId
                Fs2544SlaId
                Fs2544ReportStatsFrameSize

                The Object 
                retValFs2544ReportStatsBacktoBackBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFs2544ReportStatsBacktoBackBurstSize (UINT4 u4Fs2544ContextId,
                                            UINT4 u4Fs2544SlaId,
                                            UINT4 u4Fs2544ReportStatsFrameSize,
                                            INT4
                                            *pi4RetValFs2544ReportStatsBacktoBackBurstSize)
{
    tReportStatistics  *pReportStatistics = NULL;

    pReportStatistics =
        R2544UtilGetReportStatsEntry (u4Fs2544ContextId, u4Fs2544SlaId,
                                      u4Fs2544ReportStatsFrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4Fs2544ContextId,
                      "nmhGetFs2544ReportStatsBacktoBackBurstSize: "
                      "Report is not present for SLA %d\r\n", u4Fs2544SlaId);
        return SNMP_FAILURE;
    }
    *pi4RetValFs2544ReportStatsBacktoBackBurstSize =
        (INT4) pReportStatistics->u4ReportStatsBacktoBackBurstSize;
    return SNMP_SUCCESS;

}
