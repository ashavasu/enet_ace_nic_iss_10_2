/********************************************************************
* Copyright (C) 2015 Aricent Inc . All Rights Reserved
*
* $Id: r2544cli.c,v 1.26 2016/06/14 12:29:05 siva Exp $
*
* Description: This file contains RFC2544 CLI related routines 
*********************************************************************/

#ifndef _R2544CLI_C_
#define _R2544CLI_C_

#include "r2544inc.h"
#include "fs2544cli.h"

/****************************************************************************
 * Function    :  cli_process_rfc2544_cmd                                   *
 *                                                                          *
 * Description :  This function is exported to CLI module to handle the     *
 *                   RFC2544 cli commands to take the corresponding action. *
 *                                                                          *
 * Input       :  Variable arguments                                        *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 *****************************************************************************/
INT4                                                                        
cli_process_r2544_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tSlaEntry          *pSlaEntry = NULL;
    UINT4               u4ContextId = CLI_GET_CXT_ID ();
    UINT4               u4ErrCode = RFC2544_ZERO;
    UINT4               u4SlaId = RFC2544_ZERO;
    UINT4               u4SlaCount = RFC2544_ZERO;
    UINT4               u4IfIndex = RFC2544_ZERO;
    UINT4              *args[RFC2544_CLI_MAX_ARGS];
    INT4                i4ModeId = RFC2544_ZERO;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT1                i1Argno = RFC2544_ZERO;

    CliRegisterLock (CliHandle, R2544ApiLock, R2544ApiUnLock);

    RFC2544_LOCK ();

    /* Check if the command is a switch mode command */
    if (u4ContextId == RFC2544_CLI_INVALID_CONTEXT)
    {
        u4ContextId = RFC2544_DEFAULT_CONTEXT_ID;
    }
    va_start (ap, u4Command);

    MEMSET (args, 0, sizeof (args));

    /* Third argument is always interface name/index */

    u4IfIndex = va_arg (ap, UINT4);
    UNUSED_PARAM (u4IfIndex);
    /* Walk through the rest of the arguements and store in args array.
     * Store RFC2544_CLI_MAX_ARGS arguements at the max */

    while (1)
    {
        args[i1Argno++] = va_arg (ap, UINT4 *);
        if (i1Argno == RFC2544_CLI_MAX_ARGS)
        {
            break;
        }
    }

    i4ModeId = CLI_GET_MODE_ID ();

    va_end (ap);

    switch (u4Command)
    {
        case CLI_RFC2544_SHUTDOWN:

            i4RetStatus = R2544CliSetSystemControl (CliHandle, u4ContextId,
                                                    RFC2544_SHUTDOWN);
            break;

        case CLI_RFC2544_START:

            i4RetStatus = R2544CliSetSystemControl (CliHandle, u4ContextId,
                                                    RFC2544_START);
            break;

        case CLI_RFC2544_ENABLE:

            i4RetStatus = R2544CliSetModuleStatus (CliHandle, u4ContextId,
                                                   RFC2544_ENABLED);
            break;

        case CLI_RFC2544_DISABLE:

            i4RetStatus = R2544CliSetModuleStatus (CliHandle, u4ContextId,
                                                   RFC2544_DISABLED);
            break;

        case CLI_RFC2544_SLA_CREATE:

            i4RetStatus = R2544CliSetSlaCreate (CliHandle, u4ContextId,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_SLA_DELETE:

            i4RetStatus = R2544CliSetSlaDelete (CliHandle, u4ContextId,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_TRAF_PROF_CREATE:

            i4RetStatus = R2544CliSetTrafProfCreate (CliHandle, u4ContextId,
                                                     CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_TRAF_PROF_DELETE:

            i4RetStatus = R2544CliSetTrafProfDelete (CliHandle, u4ContextId,
                                                     CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_SAC_CREATE:

            i4RetStatus = R2544CliSetSacCreate (CliHandle, u4ContextId,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_SAC_DELETE:

            i4RetStatus = R2544CliSetSacDelete (CliHandle, u4ContextId,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_NOTIFY_ENABLE:

            i4RetStatus = R2544CliSetTrapNotification (CliHandle, u4ContextId,
                                                       RFC2544_ENABLED);
            break;

        case CLI_RFC2544_NOTIFY_DISABLE:

            i4RetStatus = R2544CliSetTrapNotification (CliHandle, u4ContextId,
                                                       RFC2544_DISABLED);
            break;

        case CLI_RFC2544_MAP_TRAF_PROFILE:

            i4RetStatus =
                R2544CliSetSlaMapTrafficProfile (CliHandle, u4ContextId,
                                                 (UINT4) i4ModeId,
                                                 CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_UNMAP_TRAF_PROFILE:

            i4RetStatus =
                R2544CliSetSlaUnMapTrafficProfile (CliHandle, u4ContextId,
                                                   (UINT4) i4ModeId);
            break;

        case CLI_RFC2544_MAP_SAC:

            i4RetStatus = R2544CliSetSlaMapSac (CliHandle, u4ContextId,
                                                (UINT4) i4ModeId,
                                                CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_UNMAP_SAC:

            i4RetStatus = R2544CliSetSlaUnMapSac (CliHandle, u4ContextId,
                                                  (UINT4) i4ModeId);
            break;

        case CLI_RFC2544_MAP_CFM_CFG:

            i4RetStatus = R2544CliSetSlaMapCfmEntries (CliHandle, u4ContextId,
                                                       (UINT4) i4ModeId,
                                                       CLI_PTR_TO_U4 (args[0]),
                                                       CLI_PTR_TO_U4 (args[1]),
                                                       CLI_PTR_TO_U4 (args[2]));

            break;

        case CLI_RFC2544_UNMAP_CFM_CFG:

            i4RetStatus = R2544CliSetSlaUnMapCfmEntries (CliHandle, u4ContextId,
                                                         (UINT4) i4ModeId);
            break;

        case CLI_RFC2544_TRAF_PROF_NAME:

            i4RetStatus = R2544CliSetTrafProfName (CliHandle, u4ContextId,
                                                   (UINT4) i4ModeId,
                                                   (UINT1 *) args[0]);

            break;

        case CLI_RFC2544_THROUGHPUT_ENABLE:

            i4RetStatus = R2544CliSetThroughputParams (CliHandle, u4ContextId,
                                                       (UINT4) i4ModeId,
                                                       RFC2544_ENABLED,
                                                       CLI_PTR_TO_U4 (args[0]),
                                                       CLI_PTR_TO_U4 (args[1]),
                                                       CLI_PTR_TO_U4 (args[2]),
                                                       CLI_PTR_TO_U4 (args[3]));
            break;

        case CLI_RFC2544_THROUGHPUT_DISABLE:

            i4RetStatus = R2544CliSetThroughputParams (CliHandle, u4ContextId,
                                                       (UINT4) i4ModeId,
                                                       RFC2544_DISABLED,
                                                       CLI_PTR_TO_U4 (args[0]),
                                                       CLI_PTR_TO_U4 (args[1]),
                                                       CLI_PTR_TO_U4 (args[2]),
                                                       CLI_PTR_TO_U4 (args[3]));
            break;

        case CLI_RFC2544_LATENCY_ENABLE:

            i4RetStatus = R2544CliSetLatencyParams (CliHandle, u4ContextId,
                                                    (UINT4) i4ModeId,
                                                    RFC2544_ENABLED,
                                                    CLI_PTR_TO_U4 (args[0]),
                                                    CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_RFC2544_LATENCY_DISABLE:

            i4RetStatus = R2544CliSetLatencyParams (CliHandle, u4ContextId,
                                                    (UINT4) i4ModeId,
                                                    RFC2544_DISABLED,
                                                    CLI_PTR_TO_U4 (args[0]),
                                                    CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_RFC2544_FRAMELOSS_ENABLE:

            i4RetStatus = R2544CliSetFrameLossParams (CliHandle, u4ContextId,
                                                      (UINT4) i4ModeId,
                                                      RFC2544_ENABLED,
                                                      CLI_PTR_TO_U4 (args[0]),
                                                      CLI_PTR_TO_U4 (args[1]),
                                                      CLI_PTR_TO_U4 (args[2]),
                                                      CLI_PTR_TO_U4 (args[3]));
            break;

        case CLI_RFC2544_FRAMELOSS_DISABLE:

            i4RetStatus = R2544CliSetFrameLossParams (CliHandle, u4ContextId,
                                                      (UINT4) i4ModeId,
                                                      RFC2544_DISABLED,
                                                      CLI_PTR_TO_U4 (args[0]),
                                                      CLI_PTR_TO_U4 (args[1]),
                                                      CLI_PTR_TO_U4 (args[2]),
                                                      CLI_PTR_TO_U4 (args[3]));
            break;

        case CLI_RFC2544_BACKTOBACK_ENABLE:

            i4RetStatus = R2544CliSetBackToBackParams (CliHandle, u4ContextId,
                                                       (UINT4) i4ModeId,
                                                       RFC2544_ENABLED,
                                                       CLI_PTR_TO_U4 (args[0]),
                                                       CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_RFC2544_BACKTOBACK_DISABLE:

            i4RetStatus = R2544CliSetBackToBackParams (CliHandle, u4ContextId,
                                                       (UINT4) i4ModeId,
                                                       RFC2544_DISABLED,
                                                       CLI_PTR_TO_U4 (args[0]),
                                                       CLI_PTR_TO_U4 (args[1]));
            break;

        case CLI_RFC2544_TRAF_PROF_FRAMESIZE:

            i4RetStatus = R2544CliSetFrameSize (CliHandle, u4ContextId,
                                                (UINT4) i4ModeId,
                                                (UINT1 *) args[0]);

            break;

        case CLI_RFC2544_TRAF_PROF_SEQCHK_ENABLE:

            i4RetStatus = R2544CliSetSeqNumCheck (CliHandle, u4ContextId,
                                                  (UINT4) i4ModeId,
                                                  RFC2544_ENABLED);
            break;

        case CLI_RFC2544_TRAF_PROF_PCP:

            i4RetStatus = R2544CliSetPCP (CliHandle, u4ContextId,
                                          (UINT4) i4ModeId,
                                          CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_RFC2544_TRAF_PROF_DWELL_TIME:

            i4RetStatus = R2544CliSetDwellTime (CliHandle, u4ContextId,
                                                (UINT4) i4ModeId,
                                                CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_RFC2544_TRAF_PROF_SEQCHK_DISABLE:

            i4RetStatus = R2544CliSetSeqNumCheck (CliHandle, u4ContextId,
                                                  (UINT4) i4ModeId,
                                                  RFC2544_DISABLED);
            break;

        case CLI_RFC2544_SAC_THROUGHPUT_PARAMS:

            i4RetStatus = R2544CliSetThAllowedFrameLoss (CliHandle, u4ContextId,
                                                         (UINT4) i4ModeId,
                                                 CLI_PTR_TO_U4 (args[0]));

            break;

        case CLI_RFC2544_SAC_LATENCY_PARAMS:

            i4RetStatus = R2544CliSetLaAllowedFrameLoss (CliHandle, u4ContextId,
                                                         (UINT4) i4ModeId,
                                                  CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_SAC_FRAMELOSS_PARAMS:

            i4RetStatus = R2544CliSetFlAllowedFrameLoss (CliHandle, u4ContextId,
                                                         (UINT4) i4ModeId,
                                                  CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RFC2544_DEBUG:

            u4ContextId = RFC2544_DEFAULT_CONTEXT_ID;
            if (args[0] != NULL)
            {
                if (R2544CxtVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    == RFC2544_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            i4RetStatus = R2544CliSetTraceLevel (CliHandle, u4ContextId,
                                                 CLI_PTR_TO_U4 (args[1]),
                                                 RFC2544_ENABLED);
            break;

        case CLI_RFC2544_NO_DEBUG:

            u4ContextId = RFC2544_DEFAULT_CONTEXT_ID;
            if (args[0] != NULL)
            {
                if (R2544CxtVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    == RFC2544_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            i4RetStatus = R2544CliSetTraceLevel (CliHandle, u4ContextId,
                                                 CLI_PTR_TO_U4 (args[1]),
                                                 RFC2544_DISABLED);

            break;

        case CLI_RFC2544_SLA_TEST:

            u4ContextId = RFC2544_DEFAULT_CONTEXT_ID;
            if (args[0] != NULL)
            {
                if (R2544CxtVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    == RFC2544_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

            }

            i4RetStatus = R2544CliSetTestStatus (CliHandle, u4ContextId,
                                                 CLI_PTR_TO_U4 (args[1]),
                                                 CLI_PTR_TO_I4 (args[2]));

            break;

        case CLI_RFC2544_SAVE_SLA_REPORT:
        case CLI_RFC2544_TFTP_SLA_REPORT:
            /*  args [0] - ContextId
             *  args [1] - Report Name or TFTP Path
             *  args [2] - SLA Id */

            if (args[0] != NULL)
            {
                if (R2544CxtVcmIsSwitchExist ((UINT1 *) args[0], &u4ContextId)
                    == RFC2544_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Switch %s Does not "
                               "exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = RFC2544_DEFAULT_CONTEXT_ID;
            }

            if (args[2] != NULL)
            {
                u4SlaId = CLI_PTR_TO_U4 (args[2]);

                i4RetStatus =
                    R2544CliSaveSlaReport (CliHandle, u4ContextId, u4Command,
                                           (CHR1 *) args[1], u4SlaId);

            }
            else
            {
                for (u4SlaCount = RFC2544_ONE; u4SlaCount < RFC2544_TEN;
                     u4SlaCount++)
                {

                    pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaCount);

                    if (pSlaEntry == NULL)
                    {
                        continue;
                    }
                    if (pSlaEntry->u1SlaCurrentTestState ==
                        RFC2544_NOT_INITIATED)
                    {
                        CliPrintf (CliHandle,
                                   "\rSLA Test is not initiated for SLA Id "
                                   ": %d\r\n",
                                   u4SlaCount);
                        continue;

                    }
                    i4RetStatus =
                        R2544CliSaveSlaReport (CliHandle, u4ContextId,
                                               u4Command, (CHR1 *) args[1],
                                               u4SlaCount);

                }
            }

            break;

        case CLI_RFC2544_DELETE_SLA_REPORT:

            /* args [0] - Report Name */

            i4RetStatus = R2544CliDeleteReport (CliHandle, (CHR1 *) args[0]);

            break;

        default:
            i4RetStatus = CLI_FAILURE;
            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > RFC2544_ZERO) && (u4ErrCode < CLI_RFC2544_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", gaR2544CliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    RFC2544_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;

}

/*****************************************************************************
 * Function    :  cli_process_rfc2544_show_cmd                               *
 *                                                                           *
 * Description :  This function is exported to CLI module to handle the      *
 *                RFC2544 show commands.The display is taken care in this    *
 *                function                                                   *
 *                                                                           *
 * Input       :  Variable arguments                                         *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 *****************************************************************************/
INT4
cli_process_r2544_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4               u4ContextId = RFC2544_ZERO;
    UINT4               u4CurrContextId = 0xffffffff;
    UINT4               u4ErrCode = RFC2544_ZERO;
    UINT4               u4SacId = RFC2544_ZERO;
    UINT4               u4SlaId = RFC2544_ZERO;
    UINT4               u4TrafficProfileId = RFC2544_ZERO;
    UINT4               u4SwFlag = RFC2544_TRUE;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4Inst = 0;
    UINT4              *args[RFC2544_CLI_MAX_ARGS];
    UINT1              *pu1ContextName = NULL;
    INT1                i1ArgNo = 0;

    CliRegisterLock (CliHandle, R2544ApiLock, R2544ApiUnLock);

    RFC2544_LOCK ();

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);
    UNUSED_PARAM (i4Inst);

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing
     * IfIndex as the first argument in variable argument list. Like that
     * as the second argument we have to pass context-name*/

    pu1ContextName = va_arg (ap, UINT1 *);

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == RFC2544_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    /* Display required info for the appropriate context id (If it is
     * given pu1ContextName by user) or display info for all contexts
     * available (if context id is not given) */
    while (R2544CliGetContextInfoForShowCmd (CliHandle, pu1ContextName,
                                             u4CurrContextId,
                                             &u4ContextId) == CLI_SUCCESS)
    {

        switch (u4Command)
        {

            case CLI_RFC2544_SHOW_GLOBAL_INFO:

                i4RetStatus = R2544CliShowGlobalInfo (CliHandle, u4ContextId);
                break;

            case CLI_RFC2544_SHOW_SAC:

                if (args[0] != NULL)
                {
                    u4SacId = CLI_PTR_TO_U4 (args[0]);
                }
                i4RetStatus = R2544CliShowSac (CliHandle, u4ContextId, u4SacId, u4SwFlag);
                break;

            case CLI_RFC2544_SHOW_TRAF_PROF:

                if (args[0] != NULL)
                {
                    u4TrafficProfileId = CLI_PTR_TO_U4 (args[0]);
                }

                i4RetStatus = R2544CliShowTrafficProfile (CliHandle,
                                                          u4ContextId,
                                                          u4TrafficProfileId, u4SwFlag);

                break;

            case CLI_RFC2544_SHOW_SLA:

                i4RetStatus = R2544CliShowSla (CliHandle, u4ContextId,
                                               CLI_PTR_TO_U4 (args[0]),
                                               CLI_PTR_TO_U4 (args[1]));
                break;

            case CLI_RFC2544_SHOW_SLA_REPORT:

                if (args[0] != NULL)
                {
                    u4SlaId = CLI_PTR_TO_U4 (args[0]);
                }
                i4RetStatus = R2544CliShowSlaReport (CliHandle, u4ContextId,
                                                     u4SlaId);
                break;

            default:
                i4RetStatus = CLI_FAILURE;
                break;
        }
        /* If context name is given by user , break from loop as display
         * is over */
        if (pu1ContextName != NULL)
        {
            break;
        }

        u4CurrContextId = u4ContextId;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > RFC2544_ZERO) && (u4ErrCode < CLI_RFC2544_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", gaR2544CliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS ((UINT4) i4RetStatus);

    RFC2544_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return i4RetStatus;
}

/*************************************************************************
 * Function    :  R2544CliSetSystemControl                               *
 *                                                                       *
 * Description :  This function starts/shuts down RFC2544 module         *
 *                                                                       *
 * Input       :   u4ContextId  - Context Identifier                     *
 *                i4SystemControl - start/shudown RFC2544                *
 *                                                                       *
 * Output      :  None                                                   *
 *                                                                       *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                *
 *************************************************************************/
INT4
R2544CliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId,
                          INT4 i4SystemControl)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544ContextSystemControl (&u4ErrorCode,
                                             u4ContextId,
                                             i4SystemControl) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544ContextSystemControl (u4ContextId, i4SystemControl)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*************************************************************************
 * Function    :  R2544CliSetModuleStatus                                *
 *                                                                       *
 * Description :  This function enables/disables RFC2544 module          *
 *                                                                       *
 * Input       :  u4ContextId  - Context Identifier                      *
 *                i4ModuleStatus - enables/disbales RFC2544              *
 *                                                                       *
 * Output      :  None                                                   *
 *                                                                       *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                *
 *************************************************************************/
INT4
R2544CliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                         INT4 i4ModuleStatus)
{

    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544ContextModuleStatus (&u4ErrorCode,
                                            u4ContextId,
                                            i4ModuleStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544ContextModuleStatus (u4ContextId, i4ModuleStatus)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  R2544CliSetSlaCreate                                      *
 *                                                                          *
 * Description :  This function creates a sla if the sla                    *
 *                   is not already present                                 *
 *                                                                          *
 * Input       :  CliHandle - Cli Handle                                    *
 *                u4ContextId - Context Identifier                          *
 *                u4SlaId - SLA Identifier                                  *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
R2544CliSetSlaCreate (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SlaRowStatus (&u4ErrorCode, u4ContextId,
                                     u4SlaId, CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaRowStatus (u4ContextId, u4SlaId,
                                  CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CliChangePath ("sla-2544") == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to enter into sla "
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }

    /* If the mode is changed successfully then set the sla id */
    CLI_SET_RFC2544_SLA_ID ((INT4) u4SlaId);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  R2544CliSetSlaDelete                                       *
 *                                                                           *
 * Description :  This function deletes a sla Entry if it is present         *
 *                                                                           *
 * Input       :  CliHandle - Cli Handle                                     *
 *                u4ContextId - Context Identifier                           *
 *                u4SlaId - SLA Identifier                                   *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 *****************************************************************************/
INT4
R2544CliSetSlaDelete (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SlaRowStatus (&u4ErrorCode, u4ContextId,
                                     u4SlaId, DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaRowStatus (u4ContextId, u4SlaId,
                                  DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  R2544CliSetTrafProfCreate                                 *
 *                                                                          *
 * Description :  This function creates a traffic profile if the            *
 *                   traffic profile is not already present                 *
 *                                                                          *
 * Input       :  CliHandle - Cli Handle                                    *
 *                u4ContextId - Context Identifier                          *
 *                u4TrafficProfileId - SLA Identifier                       *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
R2544CliSetTrafProfCreate (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4TrafficProfileId)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544TrafficProfileRowStatus (&u4ErrorCode, u4ContextId,
                                                u4TrafficProfileId,
                                                CREATE_AND_WAIT) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544TrafficProfileRowStatus (u4ContextId, u4TrafficProfileId,
                                             CREATE_AND_WAIT) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CliChangePath ("traffic-profile-2544") == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to enter into traffic profile "
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }

    /* If the mode is changed successfully then set the sla id */
    CLI_SET_RFC2544_TRAFPROF_ID ((INT4) u4TrafficProfileId);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  R2544CliSetTrafProfDelete                                  *
 *                                                                           *
 * Description :  This function deletes a traffic profile  entry if it is    *
 *                           present                                         *
 *                                                                           *
 * Input       :  u4ContextId - Context Identifier                           *
 *                u4TrafficProfileId - SLA Identifier                        *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 *****************************************************************************/
INT4
R2544CliSetTrafProfDelete (tCliHandle CliHandle, UINT4 u4ContextId,
                           UINT4 u4TrafficProfileId)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544TrafficProfileRowStatus (&u4ErrorCode, u4ContextId,
                                                u4TrafficProfileId,
                                                DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544TrafficProfileRowStatus (u4ContextId, u4TrafficProfileId,
                                             DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  R2544CliSetSacCreate                                      *
 *                                                                          *
 * Description :  This function creates a sac if the sac                    *
 *                   is not already present                                 *
 *                                                                          *
 * Input       :  CliHandle - Cli Handle                                    *
 *                u4ContextId - Context Identifier                          *
 *                u4SacId - SAC Identifier                                  *
 *                                                                          *
 * Output      :  None                                                      *
 *                                                                          *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                   *
 ****************************************************************************/
INT4
R2544CliSetSacCreate (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SacRowStatus (&u4ErrorCode, u4ContextId,
                                     u4SacId, CREATE_AND_GO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SacRowStatus (u4ContextId, u4SacId,
                                  CREATE_AND_GO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (CliChangePath ("sac-2544") == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "%% Unable to enter into sac "
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }

    /* If the mode is changed successfully then set the sac id */
    CLI_SET_RFC2544_SAC_ID ((INT4) u4SacId);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  R2544CliSetSacDelete                                       *
 *                                                                           *
 * Description :  This function deletes a sac Entry if it is present         *
 *                                                                           *
 * Input       :  u4ContextId - Context Identifier                           *
 *                u4SacId - SAC Identifier                                   *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 *****************************************************************************/
INT4
R2544CliSetSacDelete (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SacRowStatus (&u4ErrorCode, u4ContextId,
                                     u4SacId, DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SacRowStatus (u4ContextId, u4SacId,
                                  DESTROY) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*************************************************************************
 * Function    :  R2544CliSetTrapNotification                            *
 *                                                                       *
 * Description :  This function enables/disables traps for RFC2544       *
 *                                                                       *
 * Input       :  u4ContextId  - Context Identifier                      *
 *                i4TrapNotification - enables/disbles RFC2544 trap      *
 *                                                                       *
 * Output      :  None                                                   *
 *                                                                       *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                *
 *************************************************************************/
INT4
R2544CliSetTrapNotification (tCliHandle CliHandle, UINT4 u4ContextId,
                             INT4 i4TrapNotification)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544ContextTrapStatus (&u4ErrorCode,
                                          u4ContextId,
                                          i4TrapNotification) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544ContextTrapStatus (u4ContextId, i4TrapNotification)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/***************************************************************************
 * Function    :  R2544CliSetSlaMapTrafficProfile                          *
 *                                                                         *
 * Description :  This function maps a traffic profile to a SLA            *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4SlaId - SLA Identifier                                 *
 *                u4TrafProfId - Traffic profile Identifier                *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetSlaMapTrafficProfile (tCliHandle CliHandle, UINT4 u4ContextId,
                                 UINT4 u4SlaId, UINT4 u4TrafProfId)
{
    tSlaEntry          *pSlaEntry = NULL;
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CliSetSlaMapTrafficProfile: "
                      "Sla Entry not present for %d\r\n", u4SlaId);
        return CLI_FAILURE;
    }

   /* checking if the traffic-profile id is already mapped to the 
    * particular SLA */
    /* if it is not mapped, then trying to map traffic-profile id 
     * to particular SLA */
    if (u4TrafProfId != pSlaEntry->u4SlaTrafProfId)
    {
        if (nmhTestv2Fs2544SlaTrafficProfileId (&u4ErrorCode, u4ContextId,
                                                u4SlaId,
                                                u4TrafProfId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544SlaTrafficProfileId (u4ContextId, u4SlaId,
                                             u4TrafProfId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        RFC2544_TRC1 (RFC2544_MGMT_TRC, u4ContextId,
                      "R2544CliSetSlaMapTrafficProfile: "
                      "This traffic-profile is already mapped to Sla %d\r\n",
                      u4SlaId);
        CliPrintf (CliHandle,
                   "\r%%This traffic-profile is already mapped to Sla %d\r\n",
                   u4SlaId);
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetSlaUnMapTrafficProfile                        *
 *                                                                         *
 * Description :  This function maps a traffic profile to a SLA            *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4SlaId - SLA Identifier                                 *
 *                u4TrafProfId - Traffic profile Identifier                *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetSlaUnMapTrafficProfile (tCliHandle CliHandle, UINT4 u4ContextId,
                                   UINT4 u4SlaId)
{

    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SlaTrafficProfileId (&u4ErrorCode, u4ContextId,
                                            u4SlaId,
                                            (INT4) RFC2544_ZERO) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs2544SlaTrafficProfileId (u4ContextId, u4SlaId,
                                         (INT4) RFC2544_ZERO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetSlaMapSac                                     *
 *                                                                         *
 * Description :  This function maps SAC  to a SLA                         *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4SlaId - SLA Identifier                                 *
 *                u4SacId - SAC Identifier                                 *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetSlaMapSac (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId,
                      UINT4 u4SacId)
{
    tSlaEntry          *pSlaEntry = NULL;

    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CliSetSlaMapSac: "
                      "Sla Entry not present for %d\r\n", u4SlaId);
        return CLI_FAILURE;
    }

    /* checking if the SAC id is already mapped to the particular SLA */
    /* if it is not mapped, then trying to map SAC id to particular SLA */
    if (u4SacId != pSlaEntry->u4SlaSacId)
    {

        if (nmhTestv2Fs2544SlaSacId (&u4ErrorCode, u4ContextId,
                                     u4SlaId, u4SacId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544SlaSacId (u4ContextId, u4SlaId,
                                  u4SacId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
        RFC2544_TRC1 (RFC2544_MGMT_TRC, u4ContextId,
                      "R2544CliSetSlaMapSac: "
                      "This Sac is already mapped to Sla %d\r\n", u4SlaId);

        CliPrintf (CliHandle, "\r%%This SAC is already mapped to Sla %d\r\n",
                   u4SlaId);
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetSlaUnMapSac                                   *
 *                                                                         *
 * Description :  This function maps a traffic profile to a SLA            *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4SlaId - SLA Identifier                                 *
 *                u4SacId - SAC Identifier                                 *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetSlaUnMapSac (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SlaSacId (&u4ErrorCode, u4ContextId,
                                 u4SlaId, (INT4) RFC2544_ZERO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaSacId (u4ContextId, u4SlaId,
                              (INT4) RFC2544_ZERO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function    :  R2544CliSetSlaMapCfmEntries                             *
 *                                                                        *
 * Description :  This function maps the CFM entries to the SLA           *
 *                                                                        *
 * Input       :  u4ContextId  - Context ID                               *
 *                u4SlaId - Sla Id                                        *
 *                u4SlaMeg - Meg Index                                    * 
 *                u4SlaMe - Me Index                                      * 
 *                u4SlaMep - Mep Index                                    * 
 *                                                                        *
 * Output      :  None                                                    *
 *                                                                        *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                 *
 ***************************************************************************/
INT4
R2544CliSetSlaMapCfmEntries (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4SlaId, UINT4 u4SlaMeg, UINT4 u4SlaMe,
                             UINT4 u4SlaMep)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (u4SlaMeg == RFC2544_ZERO)
    {
        CLI_SET_ERR (CLI_RFC2544_INVALID_SLA_MEG);
        return CLI_FAILURE;
    }
    if (u4SlaMe == RFC2544_ZERO)
    {
        CLI_SET_ERR (CLI_RFC2544_INVALID_SLA_ME);
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs2544SlaMEG (&u4ErrorCode, u4ContextId, u4SlaId,
                               u4SlaMeg) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaMEG (u4ContextId, u4SlaId, u4SlaMeg) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs2544SlaME (&u4ErrorCode, u4ContextId,
                              u4SlaId, u4SlaMe) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaME (u4ContextId, u4SlaId, u4SlaMe) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs2544SlaMEP (&u4ErrorCode, u4ContextId,
                               u4SlaId, u4SlaMep) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaMEP (u4ContextId, u4SlaId, u4SlaMep) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function    :  R2544CliSetSlaUnMapCfmEntries                           *
 *                                                                        *
 * Description :  This function maps the CFM entries to the SLA           *
 *                                                                        *
 * Input       :  u4ContextId  - Context ID                               *
 *                u4SlaId - Sla Id                                        *
 *                u4SlaMeg - Meg Index                                    * 
 *                u4SlaMe - Me Index                                      * 
 *                u4SlaMep - Mep Index                                    * 
 *                                                                        *
 * Output      :  None                                                    *
 *                                                                        *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                 *
 ***************************************************************************/
INT4
R2544CliSetSlaUnMapCfmEntries (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4SlaId)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SlaMEG (&u4ErrorCode, u4ContextId, u4SlaId,
                               (INT4) RFC2544_ZERO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaMEG (u4ContextId, u4SlaId, (INT4) RFC2544_ZERO)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs2544SlaME (&u4ErrorCode, u4ContextId,
                              u4SlaId, (INT4) RFC2544_ZERO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaME (u4ContextId, u4SlaId, (INT4) RFC2544_ZERO)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhTestv2Fs2544SlaMEP (&u4ErrorCode, u4ContextId,
                               u4SlaId, (INT4) RFC2544_ZERO) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544SlaMEP (u4ContextId, u4SlaId, (INT4) RFC2544_ZERO)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function    :  R2544CliSetThroughputParams                             *
 *                                                                        *
 * Description :  This function sets throughput Parameters                *
 *                                                                        *
 * Input       :  u4ContextId  - Context ID                               *
 *                u4TrafficProfileID - Traffic Profile ID                 *
 *                i4ThroughputStatus - Throughput test status             *
 *                u4ThTrialDuration - Throughput Trial Duration           * 
 *                u4ThMinRate - Throughput Min Rate                       * 
 *                u4ThMaxRate - Throughput Max Rate                       * 
 *                                                                        *
 * Output      :  None                                                    *
 *                                                                        *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                 *
 ***************************************************************************/
INT4
R2544CliSetThroughputParams (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4TrafficProfileId, INT4 i4ThroughputStatus,
                             UINT4 u4ThTrialDuration, UINT4 u4ThMinRate,
                             UINT4 u4ThMaxRate, UINT4 u4ThRateStep)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544TrafficProfileThTestStatus (&u4ErrorCode, u4ContextId,
                                                   u4TrafficProfileId,
                                                   i4ThroughputStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    else
    {
     if (nmhSetFs2544TrafficProfileThTestStatus (u4ContextId, u4TrafficProfileId,
                                                i4ThroughputStatus) !=
        SNMP_SUCCESS)
     {
        return CLI_FAILURE;
     }

    if (RFC2544_INVALID_VALUE != (INT4) u4ThTrialDuration)
    {
        if (nmhTestv2Fs2544TrafficProfileThTrialDuration
            (&u4ErrorCode, u4ContextId, u4TrafficProfileId,
             u4ThTrialDuration) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544TrafficProfileThTrialDuration
            (u4ContextId, u4TrafficProfileId,
             u4ThTrialDuration) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (RFC2544_INVALID_VALUE != (INT4) u4ThMaxRate)
    {
        if (nmhTestv2Fs2544TrafficProfileThMaxRate (&u4ErrorCode, u4ContextId,
                                                    u4TrafficProfileId,
                                                    u4ThMaxRate) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs2544TrafficProfileThMaxRate
            (u4ContextId, u4TrafficProfileId, u4ThMaxRate) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    if (RFC2544_INVALID_VALUE != (INT4) u4ThMinRate)
    {
        if (nmhTestv2Fs2544TrafficProfileThMinRate (&u4ErrorCode, u4ContextId,
                                                    u4TrafficProfileId,
                                                    u4ThMinRate) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544TrafficProfileThMinRate
            (u4ContextId, u4TrafficProfileId, u4ThMinRate) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    if (RFC2544_INVALID_VALUE != (INT4) u4ThRateStep) 
    { 
        if (nmhTestv2Fs2544TrafficProfileThRateStep (&u4ErrorCode, 
                                                     u4ContextId, 
                                                     u4TrafficProfileId, 
                                                     u4ThRateStep) != 
            SNMP_SUCCESS) 
        { 
            return CLI_FAILURE; 
        } 
        if (nmhSetFs2544TrafficProfileThRateStep 
            (u4ContextId, u4TrafficProfileId, u4ThRateStep) != SNMP_SUCCESS) 
        { 
            return CLI_FAILURE; 
        } 
    } 

    if (nmhTestv2Fs2544TrafficProfileRowStatus (&u4ErrorCode, u4ContextId,
                                                u4TrafficProfileId,
                                                ACTIVE) == SNMP_SUCCESS)
    {
        if(nmhSetFs2544TrafficProfileRowStatus (u4ContextId, u4TrafficProfileId,
                                             ACTIVE) != SNMP_SUCCESS)
        {
	   return CLI_FAILURE;
	}
    }
    else
    {
        return CLI_FAILURE;
    }
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function    :  R2544CliSetLatencyParams                                *
 *                                                                        *
 * Description :  This function sets latency test  Parameters             *
 *                                                                        *
 * Input       :  u4ContextId  - Context ID                               *
 *                u4TrafficProfileID - Traffic Profile ID                 *
 *                i4LatencyStatus - Latency test status                   *
 *                u4LaTrialDuration - Latency Trial Duration              * 
 *                u4LaDelayInterval - Latency Delay Measure Interval      * 
 *                                                                        *
 * Output      :  None                                                    *
 *                                                                        *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                 *
 ***************************************************************************/
INT4
R2544CliSetLatencyParams (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4TrafficProfileId, INT4 i4LatencyStatus,
                          UINT4 u4LaTrialDuration, UINT4 u4LaDelayInterval)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544TrafficProfileLaTestStatus (&u4ErrorCode, u4ContextId,
                                                   u4TrafficProfileId,
                                                   i4LatencyStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544TrafficProfileLaTestStatus (u4ContextId,
                                                u4TrafficProfileId,
                                                i4LatencyStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RFC2544_INVALID_VALUE != (INT4) u4LaTrialDuration)
    {
        if (nmhTestv2Fs2544TrafficProfileLaTrialDuration
            (&u4ErrorCode, u4ContextId, u4TrafficProfileId,
             u4LaTrialDuration) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544TrafficProfileLaTrialDuration (u4ContextId,
                                                       u4TrafficProfileId,
                                                       u4LaTrialDuration) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (RFC2544_INVALID_VALUE != (INT4) u4LaDelayInterval)
    {
        if (nmhTestv2Fs2544TrafficProfileLaDelayMeasureInterval (&u4ErrorCode,
                                                    u4ContextId,
                                                    u4TrafficProfileId,
                                                    u4LaDelayInterval)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544TrafficProfileLaDelayMeasureInterval (u4ContextId,
                                                            u4TrafficProfileId,
                                                            u4LaDelayInterval)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function    :  R2544CliSetFrameLossParams                              *
 *                                                                        *
 * Description :  This function sets Frame Loss Test Parameters           *
 *                                                                        *
 * Input       :  u4ContextId  - Context ID                               *
 *                u4TrafficProfileID - Traffic Profile ID                 *
 *                i4FrameLossStatus - Frameloss  test status              *
 *                u4FlTrialDuration - Frameloss Trial Duration            * 
 *                u4FlMinRate - Frameloss Min Rate                        * 
 *                u4FlMaxRate - Frameloss Max Rate                        * 
 *                u4FlRateStep - Frameloss Rate Step                      * 
 *                                                                        *
 * Output      :  None                                                    *
 *                                                                        *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                 *
 ***************************************************************************/
INT4
R2544CliSetFrameLossParams (tCliHandle CliHandle, UINT4 u4ContextId,
                            UINT4 u4TrafficProfileId, INT4 i4FrameLossStatus,
                            UINT4 u4FlTrialDuration, UINT4 u4FlMinRate,
                            UINT4 u4FlMaxRate, UINT4 u4FlRateStep)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544TrafficProfileFlTestStatus (&u4ErrorCode, u4ContextId,
                                                   u4TrafficProfileId,
                                                   i4FrameLossStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    else
    {
    if (nmhSetFs2544TrafficProfileFlTestStatus (u4ContextId, 
                                                u4TrafficProfileId,
                                                i4FrameLossStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RFC2544_INVALID_VALUE != (INT4) u4FlTrialDuration)
    {
        if (nmhTestv2Fs2544TrafficProfileFlTrialDuration
            (&u4ErrorCode, u4ContextId, u4TrafficProfileId,
             u4FlTrialDuration) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544TrafficProfileFlTrialDuration
            (u4ContextId, u4TrafficProfileId,
             u4FlTrialDuration) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (RFC2544_INVALID_VALUE != (INT4) u4FlMaxRate)
    {
        if (nmhTestv2Fs2544TrafficProfileFlMaxRate (&u4ErrorCode, u4ContextId,
                                                    u4TrafficProfileId,
                                                    u4FlMaxRate) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs2544TrafficProfileFlMaxRate
            (u4ContextId, u4TrafficProfileId, u4FlMaxRate) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    if (RFC2544_INVALID_VALUE != (INT4) u4FlMinRate)
    {
        if (nmhTestv2Fs2544TrafficProfileFlMinRate (&u4ErrorCode, u4ContextId,
                                                    u4TrafficProfileId,
                                                    u4FlMinRate) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544TrafficProfileFlMinRate
            (u4ContextId, u4TrafficProfileId, u4FlMinRate) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (RFC2544_INVALID_VALUE != (INT4) u4FlRateStep)
    {
        if (nmhTestv2Fs2544TrafficProfileFlRateStep (&u4ErrorCode, 
                                                     u4ContextId,
                                                     u4TrafficProfileId,
                                                     u4FlRateStep) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFs2544TrafficProfileFlRateStep
            (u4ContextId, u4TrafficProfileId, u4FlRateStep) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhTestv2Fs2544TrafficProfileRowStatus (&u4ErrorCode, u4ContextId,
                                                u4TrafficProfileId,
                                                ACTIVE) == SNMP_SUCCESS)
    {
        if (nmhSetFs2544TrafficProfileRowStatus (u4ContextId, u4TrafficProfileId,
                                             ACTIVE) != SNMP_SUCCESS)
       {
          return CLI_FAILURE;
       }

    }
    else
    {
        return CLI_FAILURE;
    }
    }
    return CLI_SUCCESS;
}

/**************************************************************************
 * Function    :  R2544CliSetBackToBackParams                             *
 *                                                                        *
 * Description :  This function sets Back to Back  test Parameters        *
 *                                                                        *
 * Input       :  u4ContextId  - Context ID                               *
 *                u4TrafficProfileID - Traffic Profile ID                 *
 *                i4LatencyStatus - Latency test status                   *
 *                u4LaTrialDuration - Latency Trial Duration              * 
 *                u4LaDelayInterval - Latency Delay Measure Interval      * 
 *                                                                        *
 * Output      :  None                                                    *
 *                                                                        *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                 *
 ***************************************************************************/
INT4
R2544CliSetBackToBackParams (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4TrafficProfileId, INT4 i4BackToBackStatus,
                             UINT4 u4BbTrialDuration, UINT4 u4BbTrialCount)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544TrafficProfileBbTestStatus (&u4ErrorCode, u4ContextId,
                                                   u4TrafficProfileId,
                                                   i4BackToBackStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544TrafficProfileBbTestStatus (u4ContextId, u4TrafficProfileId,
                                                i4BackToBackStatus) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (RFC2544_INVALID_VALUE != (INT4) u4BbTrialDuration)
    {
        if (nmhTestv2Fs2544TrafficProfileBbTrialDuration
            (&u4ErrorCode, u4ContextId, u4TrafficProfileId,
             u4BbTrialDuration) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544TrafficProfileBbTrialDuration (u4ContextId,
                                                       u4TrafficProfileId,
                                                       u4BbTrialDuration) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (RFC2544_INVALID_VALUE != (INT4) u4BbTrialCount)
    {
        if (nmhTestv2Fs2544TrafficProfileBbTrialCount (&u4ErrorCode,
                                                       u4ContextId,
                                                       u4TrafficProfileId,
                                                       u4BbTrialCount) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFs2544TrafficProfileBbTrialCount (u4ContextId,
                                                    u4TrafficProfileId,
                                                    u4BbTrialCount) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  R2544CliSetTrafProfName                                    *
 *                                                                           *
 * Description :  This function sets the Name for the traffic profile        *
 *                                                                           *
 * Input       :  u4ContextId - Context Id                                   *
 *                u4TrafficProfileId - Traffic profile Identifier            *
 *                pu1TrafProfName       - traffic Profie Name                *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 *****************************************************************************/
INT4
R2544CliSetTrafProfName (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4TrafficProfileId, UINT1 *pu1TrafProfName)
{
    tSNMP_OCTET_STRING_TYPE TrafProfName;
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UINT1               au1ProfileName[VCM_ALIAS_MAX_LEN];
    UNUSED_PARAM (CliHandle);

    RFC2544_MEMSET (&TrafProfName, RFC2544_ZERO,
                    sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Convert the string to OCTET_STRING */
    if (pu1TrafProfName == NULL)
    {
        SPRINTF ((char *) au1ProfileName, "Profile%d", u4TrafficProfileId);
        TrafProfName.pu1_OctetList = au1ProfileName;
        TrafProfName.i4_Length = (INT4) STRLEN (au1ProfileName);

    }
    else
    {
        TrafProfName.pu1_OctetList = pu1TrafProfName;
        TrafProfName.i4_Length = (INT4) STRLEN (pu1TrafProfName);
    }

    if (nmhTestv2Fs2544TrafficProfileName (&u4ErrorCode, u4ContextId,
                                           u4TrafficProfileId,
                                           &TrafProfName) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544TrafficProfileName (u4ContextId, u4TrafficProfileId,
                                        &TrafProfName) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  R2544CliSetFrameSize                                       *
 *                                                                           *
 * Description :  This function sets the frame size for the traffic profile  *
 *                                                                           *
 * Input       :  u4ContextId - Context Id                                   * 
 *                u4TrafficProfileId - Traffic profile Identifier            *
 *                pu1FrameSize       - Frame Size                            *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 *****************************************************************************/
INT4
R2544CliSetFrameSize (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT4 u4TrafficProfileId, UINT1 *pu1FrameSize)
{
    tSNMP_OCTET_STRING_TYPE FrameSize;
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    RFC2544_MEMSET (&FrameSize, RFC2544_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));

    /* Convert the string to OCTET_STRING */
    FrameSize.pu1_OctetList = pu1FrameSize;
    FrameSize.i4_Length = (INT4) STRLEN (pu1FrameSize);

    if (nmhTestv2Fs2544TrafficProfileFrameSize (&u4ErrorCode, u4ContextId,
                                                u4TrafficProfileId,
                                                &FrameSize) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFs2544TrafficProfileFrameSize (u4ContextId, u4TrafficProfileId,
                                             &FrameSize) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetSeqNumCheck                                   *
 *                                                                         *
 * Description :  This function enables/disbales the sequence number check *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4TrafficProfileId - Traffic Profile Identifier          *
 *                i4SeqNumCheck    - Sequence Number check status          *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetSeqNumCheck (tCliHandle CliHandle, UINT4 u4ContextId,
                        UINT4 u4TrafficProfileId, INT4 i4SeqNumCheck)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544TrafficProfileSeqNoCheck (&u4ErrorCode, u4ContextId,
                                                 u4TrafficProfileId,
                                                i4SeqNumCheck) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs2544TrafficProfileSeqNoCheck (u4ContextId, u4TrafficProfileId,
                                              i4SeqNumCheck) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetPCP                                           *
 *                                                                         *
 * Description :  This function sets the PCP Value                         *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4TrafficProfileId - Traffic Profile Identifier          *
 *                i4Pcp    - Priority code point value                     *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetPCP (tCliHandle CliHandle, UINT4 u4ContextId,
                UINT4 u4TrafficProfileId, INT4 i4Pcp)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    /* By default PCP is set to Zero. 
     * But the configurable values are (1-4) .
     * Hence during reset test routine is skipped*/
    if (i4Pcp != RFC2544_ZERO)
    {
        if (nmhTestv2Fs2544TrafficProfilePCP (&u4ErrorCode, u4ContextId,
                                              u4TrafficProfileId,
                                              i4Pcp) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }
    if (nmhSetFs2544TrafficProfilePCP (u4ContextId, u4TrafficProfileId,
                                       i4Pcp) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetDwellTime                                     *
 *                                                                         *
 * Description :  This function sets the Dwell Time                        *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4TrafficProfileId - Traffic Profile Identifier          *
 *                i4DwellTime - Dwell Time                                 *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetDwellTime (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT4 u4TrafficProfileId, INT4 i4DwellTime)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544TrafficProfileDwellTime (&u4ErrorCode, u4ContextId,
                                                u4TrafficProfileId,
                                                i4DwellTime) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs2544TrafficProfileDwellTime (u4ContextId, u4TrafficProfileId,
                                             i4DwellTime) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetThAllowedFrameLoss                            *
 *                                                                         *
 * Description :  This function sets the Allowed Frame Loss for throughput *
 *                  test                                                   *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4SacId - SAC Identifier                                 *
 *                u4ThAllowedFrameLoss - Allowable frame loss              *                                                                               
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetThAllowedFrameLoss (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4SacId, UINT4 u4ThAllowedFrameLoss)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SacThAllowedFrameLoss (&u4ErrorCode, u4ContextId,
                                              u4SacId,
                                              u4ThAllowedFrameLoss) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs2544SacThAllowedFrameLoss (u4ContextId, u4SacId,
                                           u4ThAllowedFrameLoss) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetLaAllowedFrameLoss                            *
 *                                                                         *
 * Description :  This function sets the Allowed Frame Loss for latency    *
 *                test                                                     *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4SacId - Sac Identifier                                 *
 *                u4LaAllowedFrameLoss - Allowable frame loss              *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetLaAllowedFrameLoss (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4SacId, UINT4 u4LaAllowedFrameLoss)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SacLaAllowedFrameLoss (&u4ErrorCode, u4ContextId,
                                              u4SacId,
                                              u4LaAllowedFrameLoss) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs2544SacLaAllowedFrameLoss (u4ContextId, u4SacId,
                                           u4LaAllowedFrameLoss) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetFlAllowedFrameLoss                            *
 *                                                                         *
 * Description :  This function sets the Allowed frameloss for frame loss  *
 *                test                                                     *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4SacId - Sac Identifier                                 *
 *                u4FlAllowedFrameLoss  - Allowable frame loss             *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetFlAllowedFrameLoss (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4SacId, UINT4 u4FlAllowedFrameLoss)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SacFlAllowedFrameLoss (&u4ErrorCode, u4ContextId,
                                              u4SacId,
                                              u4FlAllowedFrameLoss) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs2544SacFlAllowedFrameLoss (u4ContextId, u4SacId,
                                           u4FlAllowedFrameLoss) !=
        SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetTraceLevel                                    *
 *                                                                         *
 * Description :  This function sets the trace level for RFC2544           *
 *                                                                         *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4TraceInput - Trace Input                               *
 *                u1TraceStatus - Trace Status                             *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetTraceLevel (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4TraceInput, UINT1 u1TraceStatus)
{

    UINT4               u4ErrorCode = RFC2544_ZERO;
    UINT4               u4TraceLevel = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544ContextTraceOption (&u4ErrorCode, u4ContextId,
                                           u4TraceInput) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFs2544ContextTraceOption (u4ContextId, &u4TraceLevel)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    switch (u1TraceStatus)
    {
        case RFC2544_ENABLED:

            if (u4TraceLevel == u4TraceInput)
            {
                return CLI_SUCCESS;
            }
            u4TraceLevel = u4TraceLevel | u4TraceInput;
            break;

        case RFC2544_DISABLED:

            u4TraceLevel = u4TraceLevel & (~u4TraceInput);
            break;

        default:
            return CLI_FAILURE;
            break;
    }

    if (nmhSetFs2544ContextTraceOption (u4ContextId, u4TraceLevel)
        != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliSetTestStatus                                    *
 *                                                                         *
 * Description :  This function starts/stops the test                      *
 *                                                                         *
 * Input       :  u4ContextId  - Context Identifier                        *
 *                u4SlaId -  Sla Identifier                                *
 *                u4TestStatus  - Test status                              *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliSetTestStatus (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId,
                       INT4 i4TestStatus)
{
    UINT4               u4ErrorCode = RFC2544_ZERO;
    UNUSED_PARAM (CliHandle);

    if (nmhTestv2Fs2544SlaTestStatus (&u4ErrorCode, u4ContextId,
                                      u4SlaId, i4TestStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFs2544SlaTestStatus (u4ContextId, u4SlaId,
                                   i4TestStatus) != SNMP_SUCCESS)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : R2544CliGetContextInfoForShowCmd                           *
 *                                                                          *
 * Description : This Routine validates the string input for the switch     *
 *                    name or loop through for all the context. It also     *
 *                    checks if Rfc2544 is started or not                   *
 *                                                                          *
 * Input       : u4CurrContextId - Context Identifier                       *
 *                pu1ContextName - Entered Context Name                     *
 *                u4Command      - Command Identifier                       *
 *                                                                          *
 * Output      : pu4ContextId - Next Context Id or                          *
 *                                   Context Id of the entered Context name *
 *                                                                          *
 * Returns     : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                          *
 ****************************************************************************/
INT4
R2544CliGetContextInfoForShowCmd (tCliHandle CliHandle, UINT1 *pu1ContextName,
                                  UINT4 u4CurrContextId, UINT4 *pu4ContextId)
{
    /* If Switch-name is given then get the Context-Id from it */
    if (pu1ContextName != NULL)
    {
        if (R2544CxtVcmIsSwitchExist (pu1ContextName, pu4ContextId) ==
            RFC2544_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not "
                       "exist.\r\n", pu1ContextName);
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhGetNextIndexFs2544ContextTable (u4CurrContextId,
                                               pu4ContextId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * Function    :  R2544CliShowGlobalInfo                                   *
 *                                                                         *
 * Description :  This function displays RFC2544 Global info               *
 *                test                                                     *
 *                                                                         *
 * Input       :  Cli Handle - Cli Context                                 *
 *                u4ContextId - Context Identifier                         *
 *                                                                         *
 * Output      :  None                                                     *
 *                                                                         *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                  *
 ***************************************************************************/
INT4
R2544CliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE ContextName;
    UINT4               u4TraceOption;
    UINT4               u4NumOfTestRunning;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    INT4                i4ModuleStatus = RFC2544_DISABLED;
    INT4                i4SystemControl = RFC2544_SHUTDOWN;
    INT4                i4TrapStatus = RFC2544_DISABLED;

    RFC2544_MEMSET (au1ContextName, RFC2544_ZERO, VCM_ALIAS_MAX_LEN);
    ContextName.pu1_OctetList = au1ContextName;

    nmhGetFs2544ContextName (u4ContextId, &ContextName);
    nmhGetFs2544ContextSystemControl (u4ContextId, &i4SystemControl);
    CliPrintf (CliHandle, "\r\n\rSwitch %s \r\n", au1ContextName);

    if (i4SystemControl == RFC2544_SHUTDOWN)
    {
        CliPrintf (CliHandle, "\r\n %%Rfc2544 is ShutDown\r\n");
        return CLI_SUCCESS;
    }

    nmhGetFs2544ContextModuleStatus (u4ContextId, &i4ModuleStatus);
    nmhGetFs2544ContextTraceOption (u4ContextId, &u4TraceOption);
    nmhGetFs2544ContextNumOfTestRunning (u4ContextId, &u4NumOfTestRunning);
    nmhGetFs2544ContextTrapStatus (u4ContextId, &i4TrapStatus);

    CliPrintf (CliHandle, "\r\n RFC2544 Global Info \r\n");

    CliPrintf (CliHandle,
               "---------------------------------------------------------\r\n");
    if (i4SystemControl == RFC2544_START)
    {
        CliPrintf (CliHandle, "%-33s: Start \r\n", "System Control");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s: Shutdown \r\n", "System Control");
    }
    if (i4ModuleStatus == RFC2544_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s: Enabled\r\n", "Module Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s: Disabled\r\n", "Module Status");
    }
    if (i4TrapStatus == RFC2544_TRUE)
    {
        CliPrintf (CliHandle, "%-33s: Enabled\r\n", "Trap Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s: Disabled\r\n", "Trap Status");
    }

    CliPrintf (CliHandle, "%-33s: %d\r\n", "Number of Test Running",
               u4NumOfTestRunning);

    return CLI_SUCCESS;

}

/**************************************************************************
 * Function Name : R2544CliShowSac                                        *
 *                                                                        *
 * Description   : This routine shows the contents present in the         *
 *                 SAC Table.                                             *
 *                                                                        *
 * Input         : CliHandle - Handle to CLI                              *
 *                 u4ContextId - Context Identifer                        *
 *                 u4SacId  -  SAC Identifier                             *
 *                                                                        *
 * Output        : NONE                                                   *
 *                                                                        *
 * Return        : CLI_FAILURE/CLI_SUCCESS                                *
 **************************************************************************/
INT4
R2544CliShowSac (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SacId, UINT4 u4SwFlag)
{
    tSacEntry          *pSacEntry = NULL;
    UINT4               u4PrevContextId = RFC2544_ZERO;
    UINT4               u4PrevSacId = RFC2544_ZERO;
    UINT4               u4ThAllowedFrameLoss = RFC2544_ZERO;
    UINT4               u4LaAllowedFrameLoss = RFC2544_ZERO;
    UINT4               u4FlAllowedFrameLoss = RFC2544_ZERO;
    UINT4               u4DisplayAllFlag = RFC2544_RESET;
    INT4                i4SystemControl = RFC2544_SHUTDOWN;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1ContextName, 0, sizeof (au1ContextName));
    VcmGetAliasName (u4ContextId, au1ContextName);

    if(u4SwFlag == RFC2544_TRUE)
    {
        CliPrintf (CliHandle, "\r\n\rSwitch %s  \r\n", au1ContextName);
    }

    nmhGetFs2544ContextSystemControl (u4ContextId, &i4SystemControl);
    if (i4SystemControl == RFC2544_SHUTDOWN)
    {
        CliPrintf (CliHandle, "\r\n%% RFC2544 is ShutDown \r\n",
                   au1ContextName);
        return CLI_SUCCESS;
    }

    /* If sac Id is specified show information required for specific SAC Entry 
     * else show informattion for all the sac entries */
    if (u4SacId == RFC2544_ZERO)
    {
        if (nmhGetNextIndexFs2544SacTable
            (u4ContextId, &u4PrevContextId, u4SacId,
             &u4PrevSacId) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        u4DisplayAllFlag = RFC2544_SET;
    }
    else
    {
        pSacEntry = R2544UtilGetSacEntry (u4ContextId, u4SacId);

        if (pSacEntry == NULL)
        {
            RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4ContextId,
                          "Sac entry does not exist for SAC ID :%d \r\n",
                          u4SacId);
            return CLI_FAILURE;
        }

        u4PrevContextId = u4ContextId;
        u4PrevSacId = u4SacId;
    }

    do
    {
        if (u4PrevContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4SacId = u4PrevSacId;

        nmhGetFs2544SacThAllowedFrameLoss (u4ContextId, u4SacId,
                                           &u4ThAllowedFrameLoss);
        nmhGetFs2544SacLaAllowedFrameLoss (u4ContextId, u4SacId,
                                           &u4LaAllowedFrameLoss);
        nmhGetFs2544SacFlAllowedFrameLoss (u4ContextId, u4SacId,
                                           &u4FlAllowedFrameLoss);

        CliPrintf (CliHandle, "\r\nSac Id : %u\r\n", u4SacId);
        CliPrintf (CliHandle, "--------------------------------------\r\n");

        /* Throughput Allowed Frame Loss */
        CliPrintf (CliHandle, "Throughput Allowed Frame Loss : %u\r\n",
                   u4ThAllowedFrameLoss);

        /* Throughput Allowed Frame Loss */
        CliPrintf (CliHandle, "Latency Allowed Frame Loss    : %u\r\n",
                   u4LaAllowedFrameLoss);

        /* Throughput Allowed Frame Loss */
        CliPrintf (CliHandle, "Frame Loss Allowed Frame Loss : %u\r\n",
                   u4FlAllowedFrameLoss);


        /* Get the next SAC Entry */
        if (u4DisplayAllFlag != RFC2544_SET)
        {
            break;
        }

    }
    while (nmhGetNextIndexFs2544SacTable (u4ContextId, &u4PrevContextId,
                                          u4SacId,
                                          &u4PrevSacId) == SNMP_SUCCESS);
    return CLI_SUCCESS;

}

/**************************************************************************
 * Function Name : R2544CliShowTrafficProfile                             *
 *                                                                        *
 * Description   : This routine shows the contents present in the         *
 *                 Traffic Profile table                                  *
 *                                                                        *
 * Input         : CliHandle - Handle to CLI                              *
 *                 u4ContextId - Context Identifer                        *
 *                 u4TrafficProfileID - Traffic Profile Identifier        *
 *                                                                        *
 * Output        : NONE                                                   *
 *                                                                        *
 * Return        : CLI_FAILURE/CLI_SUCCESS                                *
 **************************************************************************/
INT4
R2544CliShowTrafficProfile (tCliHandle CliHandle, UINT4 u4ContextId, UINT4
                            u4TrafficProfileId, UINT4 u4SwFlag)
{
    tSNMP_OCTET_STRING_TYPE TrafProfName;
    tSNMP_OCTET_STRING_TYPE TrafProfFrameSize;
    tTrafficProfile    *pTrafficProfile = NULL;
    UINT4               u4PrevContextId = RFC2544_ZERO;
    UINT4               u4PrevTrafficProfileId = RFC2544_ZERO;
    INT4                i4TrafProfDwellTime = RFC2544_ZERO;
    INT4                i4TrafProfPCP = RFC2544_ZERO;
    UINT4               u4TrafProfThTrialDuration = RFC2544_ZERO;
    UINT4               u4TrafProfThMinRate = RFC2544_ZERO;
    UINT4               u4TrafProfThMaxRate = RFC2544_ZERO;
    UINT4               u4TrafProfThRateStep = RFC2544_ZERO;
    UINT4               u4TrafProfLaTrialDuration = RFC2544_ZERO;
    UINT4               u4TrafProfLaDelayMeasureInterval = RFC2544_ZERO;
    UINT4               u4TrafProfFlTrialDuration = RFC2544_ZERO;
    UINT4               u4TrafProfFlMaxRate = RFC2544_ZERO;
    UINT4               u4TrafProfFlMinRate = RFC2544_ZERO;
    UINT4               u4TrafProfFlRateStep = RFC2544_ZERO;
    UINT4               u4TrafProfBbTrialDuration = RFC2544_ZERO;
    UINT4               u4TrafProfBbTrialCount = RFC2544_ZERO;
    UINT4               u4DisplayAllFlag = RFC2544_RESET;
    INT4                i4SystemControl = RFC2544_SHUTDOWN;
    UINT1               au1TrafProfName[VCM_ALIAS_MAX_LEN];
    UINT1               au1TrafProfFrameSize[RFC2544_STRING_MAX_LENGTH];
    INT4                i4TrafProfSeqNoCheck = RFC2544_DISABLED;
    INT4                i4TrafProfThTestStatus = RFC2544_DISABLED;
    INT4                i4TrafProfFlTestStatus = RFC2544_DISABLED;
    INT4                i4TrafProfLaTestStatus = RFC2544_DISABLED;
    INT4                i4TrafProfBbTestStatus = RFC2544_DISABLED;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1ContextName, 0, sizeof (au1ContextName));
    VcmGetAliasName (u4ContextId, au1ContextName);

    if(u4SwFlag == RFC2544_TRUE)
    {
        CliPrintf (CliHandle, "\r\n\rSwitch %s  \r\n", au1ContextName);
    }

    nmhGetFs2544ContextSystemControl (u4ContextId, &i4SystemControl);
    if (i4SystemControl == RFC2544_SHUTDOWN)
    {
        CliPrintf (CliHandle, "\r\n %%RFC2544 is ShutDown \r\n");
        return CLI_SUCCESS;
    }

    /* If Traffic Profile Id is specified show information required 
     * for specific Traffic profile else show information for all 
     * the traffic profile */
    if (u4TrafficProfileId == RFC2544_ZERO)
    {
        if (nmhGetNextIndexFs2544TrafficProfileTable (u4ContextId,
                                                      &u4PrevContextId,
                                                      u4TrafficProfileId,
                                                      &u4PrevTrafficProfileId)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        u4DisplayAllFlag = RFC2544_SET;
    }
    else
    {

        pTrafficProfile =
            R2544UtilGetTrafProfEntry (u4ContextId, u4TrafficProfileId);

        if (pTrafficProfile == NULL)
        {
            RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4ContextId,
                          "Traffic Profile entry does not exist "
                          "for Traffic Profile ID :%d \r\n",
                          u4TrafficProfileId);

            return CLI_FAILURE;
        }

        u4PrevContextId = u4ContextId;
        u4PrevTrafficProfileId = u4TrafficProfileId;

    }

    do
    {
        if (u4PrevContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4TrafficProfileId = u4PrevTrafficProfileId;

        RFC2544_MEMSET (au1TrafProfName, RFC2544_ZERO, VCM_ALIAS_MAX_LEN);
        RFC2544_MEMSET (au1TrafProfFrameSize, RFC2544_ZERO, RFC2544_STRING_MAX_LENGTH);
        TrafProfName.pu1_OctetList = au1TrafProfName;
        TrafProfFrameSize.pu1_OctetList = au1TrafProfFrameSize;

        nmhGetFs2544TrafficProfileName (u4ContextId, u4TrafficProfileId,
                                        &TrafProfName);
        nmhGetFs2544TrafficProfileSeqNoCheck (u4ContextId, u4TrafficProfileId,
                                              &i4TrafProfSeqNoCheck);
        nmhGetFs2544TrafficProfileDwellTime (u4ContextId, u4TrafficProfileId,
                                             &i4TrafProfDwellTime);
        nmhGetFs2544TrafficProfileFrameSize (u4ContextId, u4TrafficProfileId,
                                             &TrafProfFrameSize);
        nmhGetFs2544TrafficProfilePCP (u4ContextId, u4TrafficProfileId,
                                       &i4TrafProfPCP);
        nmhGetFs2544TrafficProfileThTestStatus (u4ContextId, u4TrafficProfileId,
                                                &i4TrafProfThTestStatus);
        nmhGetFs2544TrafficProfileFlTestStatus (u4ContextId, u4TrafficProfileId,
                                                &i4TrafProfFlTestStatus);
        nmhGetFs2544TrafficProfileLaTestStatus (u4ContextId, u4TrafficProfileId,
                                                &i4TrafProfLaTestStatus);
        nmhGetFs2544TrafficProfileBbTestStatus (u4ContextId, u4TrafficProfileId,
                                                &i4TrafProfBbTestStatus);
        nmhGetFs2544TrafficProfileThTrialDuration (u4ContextId,
                                                   u4TrafficProfileId,
                                                   &u4TrafProfThTrialDuration);
        nmhGetFs2544TrafficProfileThMaxRate (u4ContextId, u4TrafficProfileId,
                                             &u4TrafProfThMaxRate);
        nmhGetFs2544TrafficProfileThMinRate (u4ContextId, u4TrafficProfileId,
                                             &u4TrafProfThMinRate);
        nmhGetFs2544TrafficProfileLaTrialDuration (u4ContextId,
                                                   u4TrafficProfileId,
                                                   &u4TrafProfLaTrialDuration);
        nmhGetFs2544TrafficProfileLaDelayMeasureInterval (u4ContextId,
                                                          u4TrafficProfileId,
                                        &u4TrafProfLaDelayMeasureInterval);
        nmhGetFs2544TrafficProfileFlTrialDuration (u4ContextId,
                                                   u4TrafficProfileId,
                                                   &u4TrafProfFlTrialDuration);
        nmhGetFs2544TrafficProfileFlMaxRate (u4ContextId, u4TrafficProfileId,
                                             &u4TrafProfFlMaxRate);
        nmhGetFs2544TrafficProfileFlMinRate (u4ContextId, u4TrafficProfileId,
                                             &u4TrafProfFlMinRate);
        nmhGetFs2544TrafficProfileFlRateStep (u4ContextId, u4TrafficProfileId,
                                              &u4TrafProfFlRateStep);
        nmhGetFs2544TrafficProfileBbTrialDuration (u4ContextId,
                                                   u4TrafficProfileId,
                                                   &u4TrafProfBbTrialDuration);
        nmhGetFs2544TrafficProfileBbTrialCount (u4ContextId, u4TrafficProfileId,
                                                &u4TrafProfBbTrialCount);
        nmhGetFs2544TrafficProfileThRateStep (u4ContextId, u4TrafficProfileId, 
                                                &u4TrafProfThRateStep); 

        CliPrintf (CliHandle, "\r\nTrafficProfile Id :     %u\r\n",
                   u4TrafficProfileId);
        CliPrintf (CliHandle, "-----------------------------------\r\n");

        CliPrintf (CliHandle, " Name                        : %s\r\n",
                   TrafProfName.pu1_OctetList);
        if (i4TrafProfSeqNoCheck == RFC2544_ENABLED)
        {
            CliPrintf (CliHandle, " Sequence Number Check       : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Sequence Number Check       : Disabled\r\n");
        }
        CliPrintf (CliHandle, " Dwell Time                  : %u\r\n",
                   i4TrafProfDwellTime);
        CliPrintf (CliHandle, " Frame Size                  : %s\r\n",
                   TrafProfFrameSize.pu1_OctetList);
        CliPrintf (CliHandle, " PCP                         : %u\r\n",
                   i4TrafProfPCP);
        if (i4TrafProfThTestStatus == RFC2544_ENABLED)
        {
            CliPrintf (CliHandle, " Throughput Test Status      : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Throughput Test Status      : Disabled\r\n");
        }
        CliPrintf (CliHandle, "     Trial Duration          : %u\r\n",
                   u4TrafProfThTrialDuration);
        CliPrintf (CliHandle, "     Maximum Rate            : %u\r\n",
                   u4TrafProfThMaxRate);
        CliPrintf (CliHandle, "     Minimum Rate            : %u\r\n",
                   u4TrafProfThMinRate);
        CliPrintf (CliHandle, "     Rate Step               : %u\r\n", 
                   u4TrafProfThRateStep); 

        if (i4TrafProfFlTestStatus == RFC2544_ENABLED)
        {
            CliPrintf (CliHandle, " Frame Loss Test Status      : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Frame Loss Test Status      : Disabled\r\n");
        }
        CliPrintf (CliHandle, "     Trial Duration          : %u\r\n",
                   u4TrafProfFlTrialDuration);
        CliPrintf (CliHandle, "     Maximum Rate            : %u\r\n",
                   u4TrafProfFlMaxRate);
        CliPrintf (CliHandle, "     Minimum Rate            : %u\r\n",
                   u4TrafProfFlMinRate);
        CliPrintf (CliHandle, "     Rate Step               : %u\r\n",
                   u4TrafProfFlRateStep);
        if (i4TrafProfLaTestStatus == RFC2544_ENABLED)
        {
            CliPrintf (CliHandle, " Latency Test Status         : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Latency Test Status         : Disabled\r\n");
        }

        CliPrintf (CliHandle, "     Trial Duration          : %u\r\n",
                   u4TrafProfLaTrialDuration);
        CliPrintf (CliHandle, "     Delay Measure Interval  : %u\r\n",
                   u4TrafProfLaDelayMeasureInterval);
        if (i4TrafProfBbTestStatus == RFC2544_ENABLED)
        {
            CliPrintf (CliHandle, " Back to back Test Status    : Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle,
                       " Back to back Test Status    : Disabled\r\n");
        }
        CliPrintf (CliHandle, "     Trial Duration          : %u\r\n",
                   u4TrafProfBbTrialDuration);
        CliPrintf (CliHandle, "     Trial Count             : %u\r\n",
                   u4TrafProfBbTrialCount);

        if (u4DisplayAllFlag != RFC2544_SET)
        {
            break;
        }

    }                            /* Get the next SAC Entry */
    while (nmhGetNextIndexFs2544TrafficProfileTable
           (u4ContextId, &u4PrevContextId, u4TrafficProfileId,
            &u4PrevTrafficProfileId) == SNMP_SUCCESS);

    return CLI_SUCCESS;

}

/**************************************************************************
 * Function Name : R2544CliShowSla                                        *
 *                                                                        *
 * Description   : This routine shows the contents present in the         *
 *                 SAC Table.                                             *
 *                                                                        *
 * Input         : CliHandle - Handle to CLI                              *
 *                 u4ContextId - Context Identifer                        *
 *                 u4SlaId  -  SAC Identifier                             *
 *                                                                        *
 * Output        : NONE                                                   *
 *                                                                        *
 * Return        : CLI_FAILURE/CLI_SUCCESS                                *
 **************************************************************************/
INT4
R2544CliShowSla (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId,
                 UINT4 u4DisplayType)
{
    tSlaEntry          *pSlaEntry = NULL;
    UINT4               u4NextContextId = RFC2544_ZERO;
    UINT4               u4NextSlaId = RFC2544_ZERO;
    UINT4               u4SlaMeg = RFC2544_ZERO;
    UINT4               u4SlaMe = RFC2544_ZERO;
    UINT4               u4SlaMep = RFC2544_ZERO;
    UINT4               u4SlaTrafProfId = RFC2544_ZERO;
    UINT4               u4SlaSacId = RFC2544_ZERO;
    UINT4               u4DisplayAllFlag = RFC2544_RESET;
    UINT4               u4SwFlag = RFC2544_TRUE;
    INT4                i4SystemControl = RFC2544_SHUTDOWN;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1ContextName, 0, sizeof (au1ContextName));
    VcmGetAliasName (u4ContextId, au1ContextName);

    CliPrintf (CliHandle, "\r\n\rSwitch %s  \r\n", au1ContextName);
    nmhGetFs2544ContextSystemControl (u4ContextId, &i4SystemControl);
    if (i4SystemControl == RFC2544_SHUTDOWN)
    {
        CliPrintf (CliHandle, "\r\n%% RFC2544 is ShutDown \r\n",
                   au1ContextName);
        return CLI_SUCCESS;
    }

    /* If Sla Id is specified show information required for specific SLA Entry 
     * else show informattion for all the sla entries */
    if (u4SlaId == RFC2544_ZERO)
    {
        if (nmhGetNextIndexFs2544SlaTable (u4ContextId, &u4NextContextId,
                                           u4SlaId, &u4NextSlaId)
            != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        u4DisplayAllFlag = RFC2544_SET;
    }
    else
    {
        pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);

        if (pSlaEntry == NULL)
        {
            RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, u4ContextId,
                          "Sla entry doesn't  exists for Sla ID :%d \r\n",
                          u4SlaId);
            return RFC2544_SUCCESS;
        }
        u4NextContextId = u4ContextId;
        u4NextSlaId = u4SlaId;
    }

    do
    {
        if (u4NextContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4SlaId = u4NextSlaId;

        nmhGetFs2544SlaMEG (u4ContextId, u4SlaId, &u4SlaMeg);
        nmhGetFs2544SlaME (u4ContextId, u4SlaId, &u4SlaMe);
        nmhGetFs2544SlaMEP (u4ContextId, u4SlaId, &u4SlaMep);
        nmhGetFs2544SlaTrafficProfileId (u4ContextId, u4SlaId,
                                         &u4SlaTrafProfId);
        nmhGetFs2544SlaSacId (u4ContextId, u4SlaId, &u4SlaSacId);

        CliPrintf (CliHandle, "\r\nSla Id : %u\r\n", u4SlaId);
        CliPrintf (CliHandle, "-------------------------------\r\n");

        /* SLA MEG */
        CliPrintf (CliHandle, "MEG                 : %u\r\n", u4SlaMeg);

        /* SLA ME */
        CliPrintf (CliHandle, "ME                  : %u\r\n", u4SlaMe);

        /* SLA MEP */
        CliPrintf (CliHandle, "MEP                 : %u\r\n", u4SlaMep);

        if (u4DisplayType == CLI_RFC2544_SHOW_SLA_DETAIL)

        {
            u4SwFlag = RFC2544_FALSE;
            if (u4SlaTrafProfId != RFC2544_ZERO)
            {
                R2544CliShowTrafficProfile (CliHandle, u4ContextId,
                                            u4SlaTrafProfId, u4SwFlag);
            }
            if (u4SlaSacId != RFC2544_ZERO)
            {
                R2544CliShowSac (CliHandle, u4ContextId, u4SlaSacId, u4SwFlag);
            }
        }
        else
        {
            /* SLA Mapped Traffic Profile ID  */
            CliPrintf (CliHandle, "Traffic Profile ID  : %u\r\n",
                       u4SlaTrafProfId);

            /* SLA Mapped Sac Id  */
            CliPrintf (CliHandle, "Sac Id              : %u\r\n", u4SlaSacId);
        }


        if (u4DisplayAllFlag != RFC2544_SET)
        {
            break;
        }

    }                            /* Get the next SAC Entry */
    while (nmhGetNextIndexFs2544SlaTable (u4ContextId, &u4NextContextId,
                                          u4SlaId,
                                          &u4NextSlaId) == SNMP_SUCCESS);
    return CLI_SUCCESS;

}

/**************************************************************************
 * Function Name : R2544CliShowSlaReport                                  *
 *                                                                        *
 * Description   : This routine shows the contents present in the         *
 *                 SAC Table.                                             *
 *                                                                        *
 * Input         : CliHandle - Handle to CLI                              *
 *                 u4ContextId - Context Identifer                        *
 *                 u4SlaId  -  SAC Identifier                             *
 *                                                                        *
 * Output        : NONE                                                   *
 *                                                                        *
 * Return        : CLI_FAILURE/CLI_SUCCESS                                *
 **************************************************************************/
INT4
R2544CliShowSlaReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4SlaId)
{

    tSlaEntry          *pSlaEntry = NULL;
    tReportStatistics  *pReportStatistics = NULL;
    time_t              TestTime;
    FLT4                f4BurstSize = RFC2544_ZERO;
    UINT4               u4NextContextId = RFC2544_ZERO;
    UINT4               u4NextSlaId = RFC2544_ZERO;
    UINT4               u4PrevSlaId = RFC2544_ZERO;
    UINT4               u4FlagSlaId = RFC2544_ONE;
    UINT4               u4NextFrameSize = RFC2544_ZERO;
    UINT4               u4FrameSize = RFC2544_ZERO;
    UINT4               u4ThVerifiedBps = RFC2544_ZERO;
    UINT4               u4LatencyMin = RFC2544_ZERO;
    UINT4               u4LatencyMax = RFC2544_ZERO;
    UINT4               u4LatencyMean = RFC2544_ZERO;
    UINT4               u4DisplayAllFlag = RFC2544_RESET;
    INT4                i4LatencyFailCount = RFC2544_ZERO;
    INT4                i4LaIterationCalculated = RFC2544_ZERO;
    INT4                i4FrameLossRate = RFC2544_ZERO;
    INT4                i4BacktoBackBurstSize = RFC2544_ZERO;
    INT4                i4ThResult = RFC2544_ZERO;
    INT4                i4LatencyResult = RFC2544_ZERO;
    INT4                i4FrameLossResult = RFC2544_ZERO;
    INT4                i4SystemControl = RFC2544_ZERO;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1ContextName, 0, sizeof (au1ContextName));
    VcmGetAliasName (u4ContextId, au1ContextName);

    CliPrintf (CliHandle, "\r\n\rSwitch %s  \r\n", au1ContextName);
    nmhGetFs2544ContextSystemControl (u4ContextId, &i4SystemControl);
    if (i4SystemControl == RFC2544_SHUTDOWN)
    {
        CliPrintf (CliHandle, "\r\n%% RFC2544 is ShutDown \r\n",
                   au1ContextName);
        return CLI_SUCCESS;
    }
    /* If Sla Id is specified show information required for specific SLA Entry 
     * else show informattion for all the sla entries */
    if (u4SlaId == RFC2544_ZERO)
    {
        if (nmhGetNextIndexFs2544ReportStatsTable
            (u4ContextId, &u4NextContextId, u4SlaId, &u4NextSlaId, u4FrameSize,
             &u4NextFrameSize) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        u4SlaId = u4NextSlaId;
        u4ContextId = u4NextContextId;
        u4FrameSize = u4NextFrameSize;
        u4DisplayAllFlag = RFC2544_SET;

    }
    else
    {

        pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);

        if (pSlaEntry == NULL)
        {

            CliPrintf (CliHandle, "\r\n%% Sla %d does not exist\r\n", u4SlaId);
            return CLI_FAILURE;
        }

        pReportStatistics = (tReportStatistics *) RBTreeGetFirst
            (RFC2544_REPORT_STATS_TABLE ());

        while (pReportStatistics != NULL)
        {
            if ((pReportStatistics->u4ReportStatsContextId == u4ContextId) &&
                (pReportStatistics->u4ReportStatsSlaId == u4SlaId))
            {
                u4FrameSize = pReportStatistics->u4ReportStatsFrameSize;
                break;
            }
       
                pReportStatistics = RBTreeGetNext (RFC2544_REPORT_STATS_TABLE (),
                               (tRBElem *) pReportStatistics, NULL);


        }

        u4NextContextId = u4ContextId;
        u4NextSlaId = u4SlaId;
        u4NextFrameSize = u4FrameSize;

    }

    /* show the basic configuration */

    do
    {

        if ((u4NextSlaId != u4PrevSlaId) && (u4FlagSlaId == RFC2544_ZERO))
        {
            if (u4DisplayAllFlag != RFC2544_SET)
            {
                return CLI_SUCCESS;
            }
        }
        if (u4NextContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }
        u4PrevSlaId = u4SlaId;
        u4NextContextId = u4ContextId;
        u4SlaId = u4NextSlaId;
        u4FrameSize = u4NextFrameSize;

        if ((u4NextSlaId != u4PrevSlaId) || (u4FlagSlaId == RFC2544_ONE))
        {
            if (u4FlagSlaId == RFC2544_ONE)
            {
                u4FlagSlaId = RFC2544_ZERO;
            }

            pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);

            if (pSlaEntry == NULL)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "\r\nSLA Number %u:\r\n", pSlaEntry->u4SlaId);
            if (pSlaEntry != NULL)
            {
                if (pSlaEntry->u1SlaCurrentTestState == RFC2544_NOT_INITIATED)
                {
                    CliPrintf (CliHandle, "\rSLA Test is not initiated.\r\n");
                    return CLI_SUCCESS;

                }
                TestTime = (INT4) pSlaEntry->R2544TestStartTime;
                CliPrintf (CliHandle, "\rSLA Test Start Time : %s\r\n",
                           ctime (&TestTime));

                if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)
                {
                    CliPrintf (CliHandle, "\rSLA Test is In-Progress...\r\n");
                }
                else if (pSlaEntry->u1SlaCurrentTestState == RFC2544_COMPLETED)
                {
                    CliPrintf (CliHandle, "\rSLA Test is Completed.\r\n");
                    TestTime = (INT4) pSlaEntry->R2544TestEndTime;
                    CliPrintf (CliHandle, "\rSLA Test End Time : %s\r\n",
                               ctime (&TestTime));

                }
                else if (pSlaEntry->u1SlaCurrentTestState == RFC2544_ABORTED)
                {
                    CliPrintf (CliHandle, "\rSLA Test is Aborted.\r\n");
                    TestTime = (INT4) pSlaEntry->R2544TestEndTime;
                    CliPrintf (CliHandle, "\rSLA Test End Time : %s\r\n",
                               ctime (&TestTime));

                }
                CliPrintf (CliHandle,
                           "|------------------------------------------------");
                CliPrintf (CliHandle, "----------------------------|\r\n");

                CliPrintf (CliHandle,
                           "| FL : Frame loss, BB : Back-To-Back, Iter. : ");
                CliPrintf (CliHandle,
                           "Iterations, Cal. : Calculated, |\r\n");
                CliPrintf (CliHandle, 
                           "|                        K : Thousand, L : Lakh");
                CliPrintf (CliHandle,"                              |\r\n");
                CliPrintf (CliHandle,
                           "|-----|--------------|---------------------------");
                CliPrintf (CliHandle, "--------|-------------|-----|\r\n");

                CliPrintf (CliHandle,
                           "|Frame|  Throughput  |              Latency       ");
                CliPrintf (CliHandle, "       |  Frameloss  | BB  |\r\n");

                CliPrintf (CliHandle,
                           "|size |     Test     |               Test         ");
                CliPrintf (CliHandle,
                           "       |    Test     |Test |\r\n");

                CliPrintf (CliHandle,
                           "|-----|--------|-----|-----|-----|-----|-----|--");
                CliPrintf (CliHandle, "---|-----|-------|-----|-----|\r\n");
                CliPrintf (CliHandle,
                           "|     |Verified|Pass/| Min | Max | Mean|Fail ");
                CliPrintf (CliHandle, "|Iter.|Pass/|FL Rate|Pass/|Burst|\r\n");
                CliPrintf (CliHandle,
                           "|     |  Kbps  |Fail |     |     |     |Count| ");
                CliPrintf (CliHandle,
                           "cal.|Fail | in (%%)|Fail | Size|\r\n");
                CliPrintf (CliHandle,
                           "|-----|--------|-----|-----|-----|-----|-----|---");
                CliPrintf (CliHandle,
                "--|-----|-------|-----|-----|\r\n");
            }

        }

        if (u4FrameSize != RFC2544_ZERO)
        {

            pReportStatistics =
                R2544UtilGetReportStatsEntry (u4ContextId, u4SlaId,
                                              u4FrameSize);

            if (pReportStatistics == NULL)
            {
                return CLI_SUCCESS;
            }

            nmhGetFs2544ReportStatsThVerifiedBps (u4ContextId, u4SlaId,
                                                  u4FrameSize,
                                                  &u4ThVerifiedBps);
            nmhGetFs2544ReportStatsThResult (u4ContextId, u4SlaId, u4FrameSize,
                                             &i4ThResult);
            nmhGetFs2544ReportStatsLatencyMin (u4ContextId, u4SlaId,
                                               u4FrameSize, &u4LatencyMin);
            nmhGetFs2544ReportStatsLatencyMax (u4ContextId, u4SlaId,
                                               u4FrameSize, &u4LatencyMax);
            nmhGetFs2544ReportStatsLatencyMean (u4ContextId, u4SlaId,
                                                u4FrameSize, &u4LatencyMean);
            nmhGetFs2544ReportStatsLatencyFailCount (u4ContextId, u4SlaId,
                                                     u4FrameSize,
                                                     &i4LatencyFailCount);
            nmhGetFs2544ReportStatsLaIterationCalculated (u4ContextId, u4SlaId,
                                                          u4FrameSize,
                                                    &i4LaIterationCalculated);
            nmhGetFs2544ReportStatsLatencyResult (u4ContextId, u4SlaId,
                                                  u4FrameSize,
                                                  &i4LatencyResult);
            nmhGetFs2544ReportStatsFLossRate (u4ContextId, u4SlaId, u4FrameSize,
                                              &i4FrameLossRate);
            nmhGetFs2544ReportStatsFLResult (u4ContextId, u4SlaId, u4FrameSize,
                                             &i4FrameLossResult);
            nmhGetFs2544ReportStatsBacktoBackBurstSize (u4ContextId, u4SlaId,
                                                        u4FrameSize,
                                                        &i4BacktoBackBurstSize);

            CliPrintf (CliHandle, "| %4d|", u4FrameSize);

            if (u4FrameSize > pSlaEntry->u4IfMtu)
            {
                CliPrintf (CliHandle,
                           "            Framesize %4d is greater than "
                           "MTU %4d                   |\r\n",
                           u4FrameSize, pSlaEntry->u4IfMtu);
            }
            else
            {
                if (pReportStatistics->u1ReportStatsThResult ==
                    RFC2544_NOTEXECUTED)
                {
                    CliPrintf (CliHandle, "%7s |%4s |", "-", "-");
                }
                else
                {
                    CliPrintf (CliHandle, "%8d| %4s|", u4ThVerifiedBps,
                               ((i4ThResult ==
                                 RFC2544_PASS) ? "PASS" : (i4ThResult ==
                                                           RFC2544_FAIL) ?
                                "FAIL" : "-"));
                }

                if (pReportStatistics->u1ReportStatsLatencyResult ==
                    RFC2544_NOTEXECUTED)
                {
                    CliPrintf (CliHandle, "%4s |%4s |%4s |  %2s |  %2s |%4s |",
                               "-", "-", "-", "-", "-", "-");
                }
                else
                {
                    CliPrintf (CliHandle, " %4d| %4d| %4d|   %2d|   %2d| %4s|",
                               u4LatencyMin, u4LatencyMax, u4LatencyMean,
                               i4LatencyFailCount, i4LaIterationCalculated,
                               ((i4LatencyResult ==
                                 RFC2544_PASS) ? "PASS" : (i4LatencyResult ==
                                                           RFC2544_FAIL) ?
                                "FAIL" : "-"));

                }
                if (pReportStatistics->u1ReportStatsFrameLossResult ==
                    RFC2544_NOTEXECUTED)
                {

                    CliPrintf (CliHandle, "   %3s |%4s |", "-", "-");
                }
                else
                {
                    CliPrintf (CliHandle, "    %3d| %4s|", i4FrameLossRate,
                               ((i4FrameLossResult == RFC2544_PASS) ? "PASS" :
                                (i4FrameLossResult ==
                                 RFC2544_FAIL) ? "FAIL" : "-"));

                }
                if (pReportStatistics->u1ReportStatsBbResult ==
                    RFC2544_NOTEXECUTED)
                {

                    CliPrintf (CliHandle, "%4s |\r\n", "-");
                }
                else
                {
                    if(i4BacktoBackBurstSize < RFC2544_THOUSAND)
                    {     
                        CliPrintf (CliHandle, "%5d|\r\n", i4BacktoBackBurstSize);
                    }
                    else if (i4BacktoBackBurstSize < RFC2544_LAKH)
                    {
                        f4BurstSize = (FLT4) i4BacktoBackBurstSize; 
                        f4BurstSize = (FLT4) (f4BurstSize / RFC2544_THOUSAND); 
                        CliPrintf (CliHandle, "%4.1fK|\r\n", f4BurstSize); 
                    }
                    else 
                    { 
                        f4BurstSize = (FLT4) i4BacktoBackBurstSize; 
                        f4BurstSize = (FLT4) (f4BurstSize / RFC2544_LAKH); 
                        CliPrintf (CliHandle, "%4.1fL|\r\n", f4BurstSize); 
                    } 

                }

            }
            CliPrintf (CliHandle,
                       "|-----|--------|-----|-----|-----|-----|-----|-----");
            CliPrintf (CliHandle,
                       "|-----|-------|-----|-----|\r\n");
        }
    }                            /* Get the next Report Stats Entry */
    while (nmhGetNextIndexFs2544ReportStatsTable (u4ContextId, &u4NextContextId,
                                                  u4SlaId, &u4NextSlaId,
                                                  u4FrameSize,
                                                  &u4NextFrameSize) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;

}

/****************************************************************************
 * Function         : R2544ShowRunningConfig                                *
 *                                                                          *
 * Description      : This function used in show running config of Rfc2544  *
 *                                                                          *
 * Input            : CliHandle   - Handle to the CLI                       *
 *                                                                          *
 * Output           : None.                                                 *
 *                                                                          *
 * Returns          : CLI_SUCCESS / CLI_FAILURE                             *
 *                                                                          *
 ***************************************************************************/
INT4
R2544ShowRunningConfig (tCliHandle CliHandle)
{

    if (CLI_SUCCESS != R2544ShowRunningConfigCxtCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (CLI_SUCCESS != R2544ShowRunningConfigTrafProfCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (CLI_SUCCESS != R2544ShowRunningConfigSacCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (CLI_SUCCESS != R2544ShowRunningConfigSlaCmds (CliHandle))
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/****************************************************************************
 * Function         : R2544ShowRunningConfigCxtCmds                         *
 *                                                                          *
 * Description      : This function used in show running config of Rfc2544  *
 *                    Context Commands                                      *
 *                                                                          *
 * Input            : CliHandle   - Handle to the CLI                       *
 *                                                                          *
 * Output           : None.                                                 *
 *                                                                          *
 * Returns          : CLI_SUCCESS / CLI_FAILURE                             *
 *                                                                          *
 ****************************************************************************/
INT4
R2544ShowRunningConfigCxtCmds (tCliHandle CliHandle)
{

    UINT4               u4ContextId;
    UINT4               u4NextContextId;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    INT4                i4ModuleStatus = RFC2544_DISABLED;
    INT4                i4SystemControl = RFC2544_SHUTDOWN;
    INT4                i4TrapStatus = RFC2544_DISABLED;

    /* Code to get rfc2544 context entries */
    if (nmhGetFirstIndexFs2544ContextTable (&u4ContextId) != SNMP_FAILURE)
    {
        u4NextContextId = u4ContextId;

        do
        {
            u4ContextId = u4NextContextId;
            VcmGetAliasName (u4ContextId, au1ContextName);

            CliPrintf (CliHandle, "\rswitch %s\n", au1ContextName);

            /* Code to verify the execution of command : no shutdown rfc2544 */
            nmhGetFs2544ContextSystemControl (u4ContextId, &i4SystemControl);
            if (i4SystemControl == RFC2544_START)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "no shutdown rfc2544");
            }

            /* Code to verify the execution of command : rfc2544 enable */
            nmhGetFs2544ContextModuleStatus (u4ContextId, &i4ModuleStatus);
            if (i4ModuleStatus == RFC2544_ENABLED)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 enable");
            }

            /* Code to verify the execution of command : 
             * rfc2544 notification enable */
            nmhGetFs2544ContextTrapStatus (u4ContextId, &i4TrapStatus);
            if (i4TrapStatus == RFC2544_TRUE)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 notification enable");
            }
            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n");
        }
        while (nmhGetNextIndexFs2544ContextTable
               (u4ContextId, &u4NextContextId) == SNMP_SUCCESS);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : R2544ShowRunningConfigSlaCmds                         *
 *                                                                          *
 * Description      : This function used in show running config of Rfc2544  *
 *                    Sla Commands                                          *
 *                                                                          *
 * Input            : CliHandle   - Handle to the CLI                       *
 *                                                                          *
 * Output           : None.                                                 *
 *                                                                          *
 * Returns          : CLI_SUCCESS / CLI_FAILURE                             *
 *                                                                          *
 ***************************************************************************/

INT4
R2544ShowRunningConfigSlaCmds (tCliHandle CliHandle)
{
    UINT4               u4ContextId = RFC2544_ZERO;
    UINT4               u4SlaId = RFC2544_ZERO;
    UINT4               u4NextContextId = RFC2544_ZERO;
    UINT4               u4NextSlaId = RFC2544_ZERO;
    UINT4               u4SlaMeg = RFC2544_ZERO;
    UINT4               u4SlaMe = RFC2544_ZERO;
    UINT4               u4SlaMep = RFC2544_ZERO;
    UINT4               u4SlaTrafProfId = RFC2544_ZERO;
    UINT4               u4SlaSacId = RFC2544_ZERO;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    if (nmhGetFirstIndexFs2544SlaTable (&u4ContextId, &u4SlaId) != SNMP_FAILURE)
    {
        u4NextContextId = u4ContextId;
        u4NextSlaId = u4SlaId;

        do
        {
            u4ContextId = u4NextContextId;
            u4SlaId = u4NextSlaId;

            VcmGetAliasName (u4ContextId, au1ContextName);

            CliPrintf (CliHandle, "\rswitch %s\n", au1ContextName);

            if (u4SlaId != RFC2544_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 sla %u", u4SlaId);
            }

            nmhGetFs2544SlaMEG (u4ContextId, u4SlaId, &u4SlaMeg);
            nmhGetFs2544SlaME (u4ContextId, u4SlaId, &u4SlaMe);
            nmhGetFs2544SlaMEP (u4ContextId, u4SlaId, &u4SlaMep);
            if ((u4SlaMeg != RFC2544_ZERO) && (u4SlaMe != RFC2544_ZERO)
                && (u4SlaMep != RFC2544_ZERO))
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "map meg %u me %u mep %u",
                           u4SlaMeg, u4SlaMe, u4SlaMep);
            }

            nmhGetFs2544SlaTrafficProfileId (u4ContextId, u4SlaId,
                                             &u4SlaTrafProfId);
            if (u4SlaTrafProfId != RFC2544_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "map traffic-profile %u",
                           u4SlaTrafProfId);
            }

            nmhGetFs2544SlaSacId (u4ContextId, u4SlaId, &u4SlaSacId);
            if (u4SlaSacId != RFC2544_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "map sac %u", u4SlaSacId);
            }
            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n");
        }
        while (nmhGetNextIndexFs2544SlaTable
               (u4ContextId, &u4NextContextId,
                u4SlaId, &u4NextSlaId) == SNMP_SUCCESS);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : R2544ShowRunningConfigSacCmds                         *
 *                                                                          *
 * Description      : This function used in show running config of Rfc2544  *
 *                    Sac Commands                                          *
 *                                                                          *
 * Input            : CliHandle   - Handle to the CLI                       *
 *                                                                          *
 * Output           : None.                                                 *
 *                                                                          *
 * Returns          : CLI_SUCCESS / CLI_FAILURE                             *
 *                                                                          *
 ***************************************************************************/

INT4
R2544ShowRunningConfigSacCmds (tCliHandle CliHandle)
{
    UINT4               u4ContextId = RFC2544_ZERO;
    UINT4               u4SacId = RFC2544_ZERO;
    UINT4               u4NextContextId = RFC2544_ZERO;
    UINT4               u4NextSacId = RFC2544_ZERO;
    UINT4               u4ThAllowedFrameLoss = RFC2544_ZERO;
    UINT4               u4LaAllowedFrameLoss = RFC2544_ZERO;
    UINT4               u4FlAllowedFrameLoss = RFC2544_ZERO;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    if (nmhGetFirstIndexFs2544SacTable (&u4ContextId, &u4SacId) != SNMP_FAILURE)
    {
        u4NextContextId = u4ContextId;
        u4NextSacId = u4SacId;

        do
        {
            u4ContextId = u4NextContextId;
            u4SacId = u4NextSacId;

            VcmGetAliasName (u4ContextId, au1ContextName);
            CliPrintf (CliHandle, "switch %s", au1ContextName);

            if (u4SacId != RFC2544_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 sac %u", u4SacId);
            }
            nmhGetFs2544SacThAllowedFrameLoss (u4ContextId, u4SacId,
                                               &u4ThAllowedFrameLoss);

            nmhGetFs2544SacLaAllowedFrameLoss (u4ContextId, u4SacId,
                                               &u4LaAllowedFrameLoss);
            nmhGetFs2544SacFlAllowedFrameLoss (u4ContextId, u4SacId,
                                               &u4FlAllowedFrameLoss);
            if (u4ThAllowedFrameLoss != RFC2544_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle,
                           "rfc2544 sac throughput-test allowed-frame-loss %u",
                           u4ThAllowedFrameLoss);
            }

            if (u4LaAllowedFrameLoss != RFC2544_ZERO)
            {

                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle,
                           "rfc2544 sac latency-test  allowed-frame-loss %u",
                           u4LaAllowedFrameLoss);
            }

            if (u4FlAllowedFrameLoss != RFC2544_ZERO)
            {

                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle,
                           "rfc2544 sac frameloss-test allowed-frame-loss %u",
                           u4FlAllowedFrameLoss);
            }
            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n");
        }
        while (nmhGetNextIndexFs2544SacTable
               (u4ContextId, &u4NextContextId, u4SacId,
                &u4NextSacId) == SNMP_SUCCESS);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function         : R2544ShowRunningConfigTrafProfCmds                    *
 *                                                                          *
 * Description      : This function used in show running config of Rfc2544  *
 *                    Traffic Profile commands                              *
 *                                                                          *
 * Input            : CliHandle   - Handle to the CLI                       *
 *                                                                          *
 * Output           : None.                                                 *
 *                                                                          *
 * Returns          : CLI_SUCCESS / CLI_FAILURE                             *
 *                                                                          *
 ***************************************************************************/

INT4
R2544ShowRunningConfigTrafProfCmds (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE TrafProfName;
    tSNMP_OCTET_STRING_TYPE TrafProfFrameSize;
    UINT4               u4ContextId = RFC2544_ZERO;
    UINT4               u4TrafficProfileId = RFC2544_ZERO;
    UINT4               u4NextContextId = RFC2544_ZERO;
    UINT4               u4NextTrafficProfileId = RFC2544_ZERO;
    UINT4               u4TrafProfThTrialDuration = RFC2544_ZERO;
    UINT4               u4TrafProfThMinRate = RFC2544_ZERO;
    UINT4               u4TrafProfThMaxRate = RFC2544_ZERO;
    UINT4               u4TrafProfThRateStep = RFC2544_ZERO;
    UINT4               u4TrafProfLaTrialDuration = RFC2544_ZERO;
    UINT4               u4TrafProfLaDelayMeasureInterval = RFC2544_ZERO;
    UINT4               u4TrafProfFlTrialDuration = RFC2544_ZERO;
    UINT4               u4TrafProfFlMaxRate = RFC2544_ZERO;
    UINT4               u4TrafProfFlMinRate = RFC2544_ZERO;
    UINT4               u4TrafProfFlRateStep = RFC2544_ZERO;
    UINT4               u4TrafProfBbTrialDuration = RFC2544_ZERO;
    INT4                i4TrafProfDwellTime = RFC2544_ZERO;
    INT4                i4TrafProfPCP = RFC2544_ZERO;
    INT4                i4TrafProfSeqNoCheck = RFC2544_DISABLED;
    INT4                i4TrafProfThTestStatus = RFC2544_DISABLED;
    INT4                i4TrafProfFlTestStatus = RFC2544_DISABLED;
    INT4                i4TrafProfLaTestStatus = RFC2544_DISABLED;
    INT4                i4TrafProfBbTestStatus = RFC2544_DISABLED;
    UINT4               u4TrafProfBbTrialCount = RFC2544_ZERO;
    UINT1               au1TrafProfName[VCM_ALIAS_MAX_LEN];
    UINT1               au1ProfileName[VCM_ALIAS_MAX_LEN] = { "Profile" };
    UINT1               au1DefaultFrameSize[RFC2544_STRING_MAX_LENGTH]
        = CLI_RFC2544_DEFAULT_FRAMESIZE;
    UINT1               au1TrafProfFrameSize[RFC2544_STRING_MAX_LENGTH];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    if (nmhGetFirstIndexFs2544TrafficProfileTable (&u4ContextId,
                                                   &u4TrafficProfileId) !=
        SNMP_FAILURE)
    {
        u4NextContextId = u4ContextId;
        u4NextTrafficProfileId = u4TrafficProfileId;

        do
        {
            u4ContextId = u4NextContextId;
            u4TrafficProfileId = u4NextTrafficProfileId;

            VcmGetAliasName (u4ContextId, au1ContextName);
            CliPrintf (CliHandle, "switch %s", au1ContextName);

            RFC2544_MEMSET (au1TrafProfName, RFC2544_ZERO, VCM_ALIAS_MAX_LEN);
            RFC2544_MEMSET (au1TrafProfFrameSize, RFC2544_ZERO,
                            RFC2544_STRING_MAX_LENGTH);
            TrafProfName.pu1_OctetList = au1TrafProfName;
            TrafProfFrameSize.pu1_OctetList = au1TrafProfFrameSize;
            SPRINTF ((char *) au1ProfileName, "Profile%d", u4TrafficProfileId);

            nmhGetFs2544TrafficProfileName (u4ContextId, u4TrafficProfileId,
                                            &TrafProfName);
            nmhGetFs2544TrafficProfileSeqNoCheck (u4ContextId,
                                                  u4TrafficProfileId,
                                                  &i4TrafProfSeqNoCheck);
            nmhGetFs2544TrafficProfileDwellTime (u4ContextId,
                                                 u4TrafficProfileId,
                                                 &i4TrafProfDwellTime);
            nmhGetFs2544TrafficProfileFrameSize (u4ContextId,
                                                 u4TrafficProfileId,
                                                 &TrafProfFrameSize);
            nmhGetFs2544TrafficProfilePCP (u4ContextId, u4TrafficProfileId,
                                           &i4TrafProfPCP);
            nmhGetFs2544TrafficProfileThTestStatus (u4ContextId,
                                                    u4TrafficProfileId,
                                                    &i4TrafProfThTestStatus);
            nmhGetFs2544TrafficProfileFlTestStatus (u4ContextId,
                                                    u4TrafficProfileId,
                                                    &i4TrafProfFlTestStatus);
            nmhGetFs2544TrafficProfileLaTestStatus (u4ContextId,
                                                    u4TrafficProfileId,
                                                    &i4TrafProfLaTestStatus);
            nmhGetFs2544TrafficProfileBbTestStatus (u4ContextId,
                                                    u4TrafficProfileId,
                                                    &i4TrafProfBbTestStatus);
            nmhGetFs2544TrafficProfileThTrialDuration (u4ContextId,
                                                       u4TrafficProfileId,
                                                &u4TrafProfThTrialDuration);
            nmhGetFs2544TrafficProfileThMaxRate (u4ContextId,
                                                 u4TrafficProfileId,
                                                 &u4TrafProfThMaxRate);
            nmhGetFs2544TrafficProfileThMinRate (u4ContextId,
                                                 u4TrafficProfileId,
                                                 &u4TrafProfThMinRate);
            nmhGetFs2544TrafficProfileThRateStep (u4ContextId, 
                                                  u4TrafficProfileId, 
                                                  &u4TrafProfThRateStep); 

            nmhGetFs2544TrafficProfileLaTrialDuration (u4ContextId,
                                                       u4TrafficProfileId,
                                                   &u4TrafProfLaTrialDuration);
            nmhGetFs2544TrafficProfileLaDelayMeasureInterval (u4ContextId,
                                                             u4TrafficProfileId,
                                                &u4TrafProfLaDelayMeasureInterval);
            nmhGetFs2544TrafficProfileFlTrialDuration (u4ContextId,
                                                       u4TrafficProfileId,
                                                    &u4TrafProfFlTrialDuration);
            nmhGetFs2544TrafficProfileFlMaxRate (u4ContextId,
                                                 u4TrafficProfileId,
                                                 &u4TrafProfFlMaxRate);
            nmhGetFs2544TrafficProfileFlMinRate (u4ContextId,
                                                 u4TrafficProfileId,
                                                 &u4TrafProfFlMinRate);
            nmhGetFs2544TrafficProfileFlRateStep (u4ContextId,
                                                  u4TrafficProfileId,
                                                  &u4TrafProfFlRateStep);
            nmhGetFs2544TrafficProfileBbTrialDuration (u4ContextId,
                                                       u4TrafficProfileId,
                                                    &u4TrafProfBbTrialDuration);
            nmhGetFs2544TrafficProfileBbTrialCount (u4ContextId,
                                                    u4TrafficProfileId,
                                                    &u4TrafProfBbTrialCount);

            if (u4TrafficProfileId != RFC2544_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 traffic-profile %u",
                           u4TrafficProfileId);
            }
            if (RFC2544_STRCMP (au1TrafProfName, au1ProfileName) !=
                RFC2544_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 traffic-profile profile-name %s",
                           au1TrafProfName);
            }

            if (i4TrafProfDwellTime != RFC2544_DWELL_TIME)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 traffic-profile dwell-time %d",
                           i4TrafProfDwellTime);
            }
            if (i4TrafProfSeqNoCheck != RFC2544_DISABLED)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 traffic-profile seq-no-check");
            }
            if (i4TrafProfPCP != RFC2544_PCP)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 traffic-profile pcp %d",
                           i4TrafProfPCP);
            }
            if (RFC2544_STRCMP (au1TrafProfFrameSize, au1DefaultFrameSize) !=
                RFC2544_ZERO)
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 traffic-profile framesize %s",
                           au1TrafProfFrameSize);
            }
            if ((u4TrafProfThTrialDuration == CLI_RFC2544_DEF_TH_TRIALDURATION)
                && (u4TrafProfThMaxRate == RFC2544_MAX_RATE)
                && (u4TrafProfThMinRate == RFC2544_MIN_RATE)
                && (u4TrafProfThRateStep == RFC2544_TH_RATESTEP)
                && (i4TrafProfThTestStatus == RFC2544_DISABLED))
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "no rfc2544 latency-test ");
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "no rfc2544 throughput-test ");
            }
            else if ((u4TrafProfThTrialDuration !=
                      CLI_RFC2544_DEF_TH_TRIALDURATION)
                     || (u4TrafProfThMaxRate != RFC2544_MAX_RATE)
                     || (u4TrafProfThMinRate != RFC2544_MIN_RATE)
                     || (u4TrafProfThRateStep != RFC2544_TH_RATESTEP))
            {

                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 throughput-test ");
                if (u4TrafProfThTrialDuration !=
                    CLI_RFC2544_DEF_TH_TRIALDURATION)
                {
                    CliPrintf (CliHandle, "trial-duration %u ",
                               u4TrafProfThTrialDuration);
                }
                if (u4TrafProfThMaxRate != RFC2544_MAX_RATE)
                {
                    CliPrintf (CliHandle, "max-rate %u ", u4TrafProfThMaxRate);
                }
                if(u4TrafProfThMinRate != RFC2544_MIN_RATE)
                {
                    CliPrintf (CliHandle, "min-rate %u ", u4TrafProfThMinRate);
                }
                if (u4TrafProfThRateStep != RFC2544_TH_RATESTEP) 
                { 
                    CliPrintf (CliHandle, "rate-step %u ", u4TrafProfThRateStep); 
                }  

            }

            if (i4TrafProfLaTestStatus == RFC2544_ENABLED)
            {
                if ((u4TrafProfLaTrialDuration !=
                     CLI_RFC2544_DEF_LA_TRIALDURATION)
                    || (u4TrafProfLaDelayMeasureInterval !=
                        CLI_RFC2544_DEF_LA_DELAY_INTERVAL))
                {
                    CliPrintf (CliHandle, "\r\n");
                    CliPrintf (CliHandle, "rfc2544 latency-test ");
                }

                if (u4TrafProfLaTrialDuration !=
                    CLI_RFC2544_DEF_LA_TRIALDURATION)
                {
                    CliPrintf (CliHandle, "trial-duration %u ",
                               u4TrafProfLaTrialDuration);
                }

                if (u4TrafProfLaDelayMeasureInterval !=
                    CLI_RFC2544_DEF_LA_DELAY_INTERVAL)
                {
                    CliPrintf (CliHandle, "delay-measurement-interval %u ",
                               u4TrafProfLaDelayMeasureInterval);
                }
            }
            else
            {
                if (i4TrafProfThTestStatus == RFC2544_ENABLED)
                {
                     CliPrintf (CliHandle, "\r\n");
                     CliPrintf (CliHandle, "no rfc2544 latency-test ");
                }

            }
            if ((u4TrafProfFlTrialDuration != CLI_RFC2544_DEF_FL_TRIALDURATION)
                || (u4TrafProfFlMaxRate != RFC2544_MAX_RATE)
                || (u4TrafProfFlMinRate != RFC2544_MIN_RATE)
                || (u4TrafProfFlRateStep != RFC2544_FL_RATESTEP))
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 frame-loss-test ");
            }

            if (u4TrafProfFlTrialDuration != CLI_RFC2544_DEF_FL_TRIALDURATION)
            {
                CliPrintf (CliHandle, "trial-duration %u ",
                           u4TrafProfFlTrialDuration);
            }
            if ((u4TrafProfFlMaxRate != RFC2544_MAX_RATE)
                || (u4TrafProfFlMinRate != RFC2544_MIN_RATE)
                || (u4TrafProfFlRateStep != RFC2544_FL_RATESTEP))
            {
                CliPrintf (CliHandle, "max-rate %u ", u4TrafProfFlMaxRate);
                CliPrintf (CliHandle, "min-rate %u ", u4TrafProfFlMinRate);
                CliPrintf (CliHandle, "rate-step %u ", u4TrafProfFlRateStep);
            }
            if ((u4TrafProfBbTrialDuration != CLI_RFC2544_DEF_BB_TRIALDURATION)
                || (u4TrafProfBbTrialCount != CLI_RFC2544_DEF_BB_TRIALCOUNT))
            {
                CliPrintf (CliHandle, "\r\n");
                CliPrintf (CliHandle, "rfc2544 back-to-back-test ");
            }
            if (u4TrafProfBbTrialCount != CLI_RFC2544_DEF_BB_TRIALCOUNT)
            {
                CliPrintf (CliHandle, "trial-count %u ",
                           u4TrafProfBbTrialCount);
            }

            if (u4TrafProfBbTrialDuration != CLI_RFC2544_DEF_BB_TRIALDURATION)
            {
                CliPrintf (CliHandle, "trial-duration %u ",
                           u4TrafProfBbTrialDuration);
            }

            /* TP parameters */
            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n!");
            CliPrintf (CliHandle, "\r\n");
        }
        while (nmhGetNextIndexFs2544TrafficProfileTable
               (u4ContextId, &u4NextContextId,
                u4TrafficProfileId, &u4NextTrafficProfileId) == SNMP_SUCCESS);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * FUNCTION NAME    : R2544GetSlaCfgPrompt                                  *
 *                                                                          *
 * DESCRIPTION      : This function returns the prompt to be displayed      *
 *                    for SLA. It is exported to CLI module.                *
 *                    This function will be invoked when the System is      *
 *                    running in SI mode.                                   *
 *                                                                          *
 * INPUT            : None                                                  *
 *                                                                          *
 *                                                                          *
 * OUTPUT           : pi1ModeName - Mode String                             *
 *                    pi1DispStr - Display string                           *
 *                                                                          *
 *                                                                          *
 * RETURNS          : TRUE/FALSE                                            *
 *                                                                          *
 ***************************************************************************/
INT1
R2544GetSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("sla-2544");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "sla-2544", u4Len) != RFC2544_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_r2544_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-sla-2544)#");

    return TRUE;
}

/****************************************************************************
 * FUNCTION NAME    : R2544GetTrafficProfileCfgPrompt                       *
 *                                                                          *
 * DESCRIPTION      : This function returns the prompt to be displayed      *
 *                    for Traffic Profile. It is exported to CLI module.    *
 *                    This function will be invoked when the System is      *
 *                    running in SI mode.                                   *
 *                                                                          *
 * INPUT            : None                                                  *
 *                                                                          *
 *                                                                          *
 * OUTPUT           : pi1ModeName - Mode String                             *
 *                    pi1DispStr - Display string                           *
 *                                                                          *
 *                                                                          *
 * RETURNS          : TRUE/FALSE                                            *
 *                                                                          *
 ***************************************************************************/
INT1
R2544GetTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("traffic-profile-2544");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "traffic-profile-2544", u4Len) != RFC2544_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_r2544_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-traffic-profile-2544)#");

    return TRUE;
}

/**************************************************************************
 * FUNCTION NAME    : R2544GetSacCfgPrompt                                *
 *                                                                        *
 * DESCRIPTION      : This function returns the prompt to be displayed    *
 *                    for SLA. It is exported to CLI module.              *
 *                    This function will be invoked when the System is    *
 *                    running in SI mode.                                 *
 *                                                                        *
 * INPUT            : None                                                *
 *                                                                        *
 *                                                                        *
 * OUTPUT           : pi1ModeName - Mode String                           *
 *                    pi1DispStr - Display string                         *
 *                                                                        *
 *                                                                        *
 * RETURNS          : TRUE/FALSE                                          *
 *                                                                        *
 **************************************************************************/
INT1
R2544GetSacCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("sac-2544");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "sac-2544", u4Len) != RFC2544_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_r2544_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-sac-2544)#");

    return TRUE;
}

/**************************************************************************
 * FUNCTION NAME    : R2544GetVcmSlaCfgPrompt                             *
 *                                                                        *
 * DESCRIPTION      : This function returns the prompt to be displayed    *
 *                    for SLA. It is exported to CLI module.              *
 *                    This function will be invoked when the System is    *
 *                    running in MI mode.                                 *
 *                                                                        *
 * INPUT            : None                                                *
 *                                                                        *
 *                                                                        *
 * OUTPUT           : pi1ModeName - Mode String                           *
 *                    pi1DispStr - Display string                         *
 *                                                                        *
 *                                                                        *
 * RETURNS          : TRUE/FALSE                                          *
 *                                                                        *
 **************************************************************************/
INT1
R2544GetVcmSlaCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("sla-2544");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "sla-2544", u4Len) != RFC2544_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_r2544_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-switch-sla-2544)#");

    return TRUE;
}

/**************************************************************************
 * FUNCTION NAME    : R2544GetVcmTrafficProfileCfgPrompt                  *
 *                                                                        *
 * DESCRIPTION      : This function returns the prompt to be displayed    *
 *                    for Traffic Profile. It is exported to CLI module.  *
 *                    This function will be invoked when the System is    *
 *                    running in MI mode.                                 *
 *                                                                        *
 * INPUT            : None                                                *
 *                                                                        *
 *                                                                        *
 * OUTPUT           : pi1ModeName - Mode String                           *
 *                    pi1DispStr - Display string                         *
 *                                                                        *
 *                                                                        *
 * RETURNS          : TRUE/FALSE                                          *
 *                                                                        *
 **************************************************************************/
INT1
R2544GetVcmTrafficProfileCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("traffic-profile-2544");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "traffic-profile-2544", u4Len) != RFC2544_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_rfc2544_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-switch-traffic-profile-2544)#");

    return TRUE;
}

/**************************************************************************
 * FUNCTION NAME    : R2544GetVcmSacCfgPrompt                             *
 *                                                                        *
 * DESCRIPTION      : This function returns the prompt to be displayed    *
 *                    for SLA. It is exported to CLI module.              *
 *                    This function will be invoked when the System is    *
 *                    running in MI mode.                                 *
 *                                                                        *
 * INPUT            : None                                                *
 *                                                                        *
 *                                                                        *
 * OUTPUT           : pi1ModeName - Mode String                           *
 *                    pi1DispStr - Display string                         *
 *                                                                        *
 *                                                                        *
 * RETURNS          : TRUE/FALSE                                          *
 *                                                                        *
 ***************************************************************************/
INT1
R2544GetVcmSacCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN ("sac-2544");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "sac-2544", u4Len) != RFC2544_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    /*
     * No need to take lock here, since it is taken by
     * Cli in cli_process_r2544_cmd.
     */
    UNUSED_PARAM (pi1ModeName);
    STRCPY (pi1DispStr, "(config-switch-sac-2544)#");

    return TRUE;
}

/***************************************************************************
 *     Function Name : R2544CliShowDebug                                   *
 *                                                                         *
 *     Description   : This function prints the debug level                *
 *                                                                         *
 *     Input(s)      : CliHandle - Cli Handle                              * 
                                                                           *
 *     Output(s)     : NULL                                                *
 *                                                                         *
 *     Returns       : NONE                                                *
                                                                           *
 ***************************************************************************/
VOID
R2544CliShowDebug (tCliHandle CliHandle)
{

    UINT4               u4DbgLevel = RFC2544_ZERO;
    UINT4               u4ContextId = RFC2544_DEFAULT_CONTEXT_ID;

    if (R2544CxtIsR2544Started (u4ContextId) == RFC2544_FALSE)
    {
        return;
    }
 
    nmhGetFs2544ContextTraceOption (u4ContextId, &u4DbgLevel);

    if (u4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rRFC2544: ");

    if ((u4DbgLevel & RFC2544_INIT_SHUT_TRC) == RFC2544_INIT_SHUT_TRC)
    {
        CliPrintf (CliHandle,
                   "\r\n  RFC2544 init and shutdown debugging is on");
    }
    if ((u4DbgLevel & RFC2544_MGMT_TRC) == RFC2544_MGMT_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 management debugging is on");
    }
    if ((u4DbgLevel & RFC2544_Y1731_INTF_TRC) == RFC2544_Y1731_INTF_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 interface debugging is on");
    }
    if ((u4DbgLevel & RFC2544_RESOURCE_TRC) == RFC2544_RESOURCE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 resource debugging is on");
    }
    if ((u4DbgLevel & RFC2544_TIMER_TRC) == RFC2544_TIMER_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 timer  debugging is on");
    }
    if ((u4DbgLevel & RFC2544_CRITICAL_TRC) == RFC2544_CRITICAL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 critical debugging is on");
    }
    if ((u4DbgLevel & RFC2544_BENCHMARK_TEST_TRC) == RFC2544_BENCHMARK_TEST_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 benchmark debugging is on");
    }
    if ((u4DbgLevel & RFC2544_SESSION_RECORD_TRC) == RFC2544_SESSION_RECORD_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 session record debugging is on");
    }
    if ((u4DbgLevel & RFC2544_ALL_FAILURE_TRC) == RFC2544_ALL_FAILURE_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 all failure debugging is on");
    }
    if ((u4DbgLevel & RFC2544_ALL_TRC) == RFC2544_ALL_TRC)
    {
        CliPrintf (CliHandle, "\r\n  RFC2544 global debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");
    return;
}

/*****************************************************************************
 * Function    :  R2544CliSaveSlaReport                                      *
 *                                                                           *
 * Description :  This function saves the Sla test Report                    *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                i4DebugType  - Debug Type                                  *
 *                i4Action     - Enabled / Disabled Trace                    *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 ****************************************************************************/
INT4
R2544CliSaveSlaReport (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4Command,
                       CHR1 * pu1FileName, UINT4 u4SlaId)
{
    tSlaEntry                *pSlaEntry = NULL;
    tIPvXAddr                DstIpAddress;
    tIp6Addr                 Ip6Addr;
    tIPvXAddr                IpAddress;
    UINT4                    u4DstIpAddress = 0;
    UINT4                    u4Val = 0;
    UINT1                    au1UserCmd[RFC2544_MAX_CMD_LEN + 4];
    UINT1                    au1TempCmd[RFC2544_MAX_CMD_LEN + 4];
    UINT1                    au1FileName[MAX_FLASH_FILENAME_LEN];
    UINT1                    au1TempFileName[ISS_CONFIG_FILE_NAME_LEN + 1];
    UINT1                    au1DstHostName[DNS_MAX_QUERY_LEN];
    UINT1                    au1VcAlias[VCM_ALIAS_MAX_LEN];
    CHR1                     *pu1TempFileName = "temp.txt";

    MEMSET (au1TempFileName, 0, sizeof (au1TempFileName));
    MEMSET (&au1DstHostName, 0, DNS_MAX_QUERY_LEN);
    MEMSET (&DstIpAddress, 0, sizeof (tIPvXAddr));
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&IpAddress, 0, sizeof (tIPvXAddr));

    /* Get the SLA Entry */
    if (u4SlaId != RFC2544_ZERO)
    {
        pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);

        if (pSlaEntry == NULL)
        {
            CliPrintf (CliHandle, "\r%% Invalid Sla Entry \r\n ");
            return CLI_FAILURE;
        }
        if (pSlaEntry->u1SlaCurrentTestState == RFC2544_NOT_INITIATED)
        {
            CliPrintf (CliHandle,
                       "\rSLA Test is not initiated for SLA Id : %d\r\n",
                       u4SlaId);
            return CLI_SUCCESS;

        }

    }

    RFC2544_UNLOCK ();

    MEMSET (au1VcAlias, RFC2544_ZERO, VCM_ALIAS_MAX_LEN);

    if (VcmGetAliasName (u4ContextId, au1VcAlias) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to get Alias Name for context Id  %d\r\n",
                   u4ContextId);
        return CLI_FAILURE;
    }

    MEMSET (au1UserCmd, RFC2544_ZERO, sizeof (au1UserCmd));

    RFC2544_FILE_SAVE (u4SlaId, au1VcAlias, au1UserCmd);

    if (u4Command == CLI_RFC2544_SAVE_SLA_REPORT)
    {
        MEMSET (au1FileName, RFC2544_ZERO, sizeof (au1FileName));
        MEMCPY (au1FileName, pu1FileName, STRLEN (pu1FileName));

        MEMSET (au1TempCmd, RFC2544_ZERO, sizeof (au1TempCmd));
        RFC2544_FILE_ERASE (pu1FileName, au1TempCmd);
        cli_file_operation ((CONST CHR1 *) au1TempCmd,
                            (CONST CHR1 *) au1FileName);

        cli_file_operation ((CONST CHR1 *) au1UserCmd,
                            (CONST CHR1 *) au1FileName);
    }
    else if (u4Command == CLI_RFC2544_TFTP_SLA_REPORT)
    {
        MEMSET (au1TempCmd, RFC2544_ZERO, sizeof (au1TempCmd));
        MEMCPY (au1TempCmd, pu1FileName, STRLEN (pu1FileName));

        cli_file_operation ((CONST CHR1 *) au1UserCmd,
                            (CONST CHR1 *) pu1TempFileName);

        MEMSET (au1UserCmd, RFC2544_ZERO, sizeof (au1UserCmd));

        if (u4Command == CLI_RFC2544_TFTP_SLA_REPORT)
        {
            if (CliGetTftpParams ((INT1 *) au1TempCmd, (INT1 *) au1TempFileName,
                                  &DstIpAddress,
                                  (UINT1 *) au1DstHostName) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid TFTP Parameters \r\n");
                RFC2544_LOCK ();
                return CLI_FAILURE;
            }
            if (DstIpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4DstIpAddress, DstIpAddress.au1Addr);
                /* Check for self IP address */
                if (CfaIpIfIsOurAddress (u4DstIpAddress) == CFA_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% This TFTP operation is not permitted for "
                               "self IP  \r\n");
                    RFC2544_LOCK ();
                    return CLI_FAILURE;
                }
            }
#ifdef IP6_WANTED
            else
            {
                MEMCPY (&Ip6Addr, DstIpAddress.au1Addr, sizeof (tIp6Addr));
                if (NetIpv6IsOurAddress (&Ip6Addr, &u4Val) == NETIPV6_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% This SFTP operation is not permitted for "
                               "self IP  \r\n");
                    RFC2544_LOCK ();
                    return CLI_FAILURE;
                }
            }
#else
            UNUSED_PARAM (u4Val);
#endif
        }

        IssCopyFileGeneric (CliHandle, (UINT1 *) pu1TempFileName,
                            RFC2544_FOUR,
                            NULL, NULL,
                            IpAddress,
                            NULL,
                            au1TempFileName,
                            RFC2544_ONE,
                            NULL, NULL, DstIpAddress, au1DstHostName);

        CliSetIssErase (CliHandle, RFC2544_FOUR, (UINT1 *) pu1TempFileName);

    }
    RFC2544_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("configure terminal");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function    :  R2544CliDeleteReport                                       *
 *                                                                           *
 * Description :  This function delete test Report                           *
 *                                                                           *
 * Input       :  CliHandle    - Cli Handler                                 *
 *                i4DebugType  - Debug Type                                  *
 *                i4Action     - Enabled / Disabled Trace                    *
 *                                                                           *
 * Output      :  None                                                       *
 *                                                                           *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE                                    *
 ****************************************************************************/

INT4
R2544CliDeleteReport (tCliHandle CliHandle, CHR1 * pu1FileName)
{
    UINT1                    au1UserCmd[RFC2544_MAX_CMD_LEN + 4];
    

    MEMSET (au1UserCmd, RFC2544_ZERO, sizeof (au1UserCmd));

    RFC2544_UNLOCK ();

    MEMSET (au1UserCmd, RFC2544_ZERO, sizeof (au1UserCmd));
    RFC2544_FILE_ERASE (pu1FileName, au1UserCmd);
    cli_file_operation ((CONST CHR1 *) au1UserCmd, (CONST CHR1 *) pu1FileName);

    RFC2544_LOCK ();

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

#endif /* _R2544CLI_C_ */
