/***************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                    *
 *                                                                         *
 * $Id: r2544tmr.c,v 1.15 2016/05/20 10:18:08 siva Exp $                    *
 *                                                                         *
 * Description: This file contains the RFC2544 timer utility and expiry    *
 *              functions.                                                 *
 ***************************************************************************/

#ifndef __R2544TMR_C__
#define __R2544TMR_C__

#include "r2544inc.h"

/****************************************************************************
*                                                                           *
*    Function Name       : R2544TmrExpHandler                               *
*                                                                           *
*    Description         : This function will invoke timer expiry function  *
*                                                                           *
*    Input(s)            : None.                                            *
*                                                                           *
*    Output(s)           : None.                                            *
*                                                                           *
*    Returns             : None.                                            *
*****************************************************************************/
VOID
R2544TmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimer = NULL;
    INT2                i2Offset = RFC2544_ZERO;
    UINT1               u1TimerType = 0;

    while ((pExpiredTimer =
            TmrGetNextExpiredTimer (RFC2544_TIMER_LIST ())) != NULL)
    {
        u1TimerType = ((tTmrBlk *) pExpiredTimer)->u1TimerId;

        /* Get the timer information from the timer block */
        i2Offset = gR2544GlobalInfo.TmrDesc[u1TimerType].i2Offset;

        /* Call the Timer expiry handler */
        (*(gR2544GlobalInfo.TmrDesc[u1TimerType].TmrExpFn)) ((UINT1 *) pExpiredTimer - i2Offset);
    }

    return;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544TmrStartTimer                               *
*                                                                           *
*    Description         : This function will start the timer for given     *
*                          SLA for the test duration                        *
*                                                                           *
*    Input(s)            : pSlaEntry - Pointer to SlaEntry                  *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         started timer of given timer type.*
*                          OSIX_FAILURE - When this function failed to      *
*                                         start the timer.                  *
*****************************************************************************/
INT4
R2544TmrStartTimer (tSlaEntry * pSlaEntry, UINT1 u1TimerType, 
                    UINT4 u4Duration)
{
    tTmrBlk            *pTimer = NULL;
    UINT4               u4MilliSec = RFC2544_ZERO;

    switch (u1TimerType) 
    { 
         case RFC2544_SLA_TIMER: 
   
             pTimer = &(pSlaEntry->SlaTimer); 
             break; 
   
         case RFC2544_DWELL_TIME_TIMER: 
   
             pTimer = &(pSlaEntry->DwellTimeTimer); 
             break; 
   
         default: 
   
             RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId, 
                       "R2544TmrStartTimer: Invalid timer for SLA " 
                       "- %d\r\n", 
                       pSlaEntry->u4SlaId); 
           
             return OSIX_FAILURE; 
    } 
    if (TmrStart
        (RFC2544_TIMER_LIST (), pTimer, u1TimerType,     
        u4Duration, u4MilliSec) == TMR_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_TIMER_TRC, pSlaEntry->u4ContextId,
                      "R2544TmrStartTimer: Timer start failed for "
                      "Sla Id %d\r\n",
                      pSlaEntry->u4SlaId);
        return OSIX_FAILURE;
    }

    if( u1TimerType == RFC2544_SLA_TIMER) 
    { 
        RFC2544_TRC1 (RFC2544_MGMT_TRC | RFC2544_TIMER_TRC, pSlaEntry->u4ContextId,
                  "R2544TmrStartTimer: Timer started for SLA"
                  "- %d \r\n", pSlaEntry->u4SlaId);
    }
    else if(u1TimerType == RFC2544_DWELL_TIME_TIMER) 
    { 
       RFC2544_TRC1 (RFC2544_TIMER_TRC, pSlaEntry->u4ContextId, 
                   "R2544TmrStartTimer: Dwell time Timer started for SLA" 
                   "- %d \r\n", pSlaEntry->u4SlaId);  
    } 

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544TmrStopTimer                                *
*                                                                           *
*    Description         : This function will stop the timer for given SLA  *
*                                                                           *
*    Input(s)            : pSlaEntry - Pointer to SlaEntry                  *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : OSIX_SUCCESS - When this function successfully   *
*                                         started timer of given timer type.*
*                          OSIX_FAILURE - When this function failed to      *
*                                         start the timer.                  *
*****************************************************************************/
INT4
R2544TmrStopTimer (tSlaEntry * pSlaEntry, UINT1 u1TimerType)
{


   tTmrBlk            *pTimer = NULL; 
   
   switch (u1TimerType) 
   { 
       case RFC2544_SLA_TIMER: 
 
           pTimer = &(pSlaEntry->SlaTimer); 
           break; 
 
       case RFC2544_DWELL_TIME_TIMER: 
 
           pTimer = &(pSlaEntry->DwellTimeTimer); 
           break; 
 
       default: 
 
           RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId, 
                     "R2544TmrStopTimer: Invalid timer  for SLA " 
                     "- %d\r\n", 
                     pSlaEntry->u4SlaId); 
 
       return OSIX_FAILURE; 
   } 
   




    if (TmrStop (RFC2544_TIMER_LIST (), pTimer) == TMR_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                      "R2544TmrStopTimer: Timer stop failed for SLA "
                      " %d\r\n",
                      pSlaEntry->u4SlaId);
        return OSIX_FAILURE;
    }

   if(u1TimerType == RFC2544_SLA_TIMER ) 
   { 
       RFC2544_TRC1 (RFC2544_TIMER_TRC, pSlaEntry->u4ContextId,
                  "R2544TmrStopTimer: Timer stopped for SLA"
                  " %d \r\n", pSlaEntry->u4SlaId);
   }
   else if(u1TimerType == RFC2544_DWELL_TIME_TIMER ) 
   { 
            RFC2544_TRC1 (RFC2544_TIMER_TRC, pSlaEntry->u4ContextId, 
                  "R2544TmrStopTimer: Dwell time Timer stopped for SLA" 
                  "- %d \r\n", pSlaEntry->u4SlaId); 
   } 
 
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544TmrSlaTimerExp                              *
*                                                                           *
*    Description         : This function will handle ho                     *
*                          event.                                           *
*                                                                           *
*    Input(s)            : pArg - Pointer to ring node.                     *
*                                                                           *
*    Output(s)           : None                                             *
*                                                                           *
*    Returns             : None.                                            *
*****************************************************************************/
VOID
R2544TmrSlaTimerExp (VOID *pArg)
{
   tSlaEntry           * pSlaEntry;
   tTrafficProfile     * pTrafficProfile = NULL;
   
   pSlaEntry = (tSlaEntry *) pArg;

   if(pSlaEntry == NULL) 
   { 
     return; 
   } 

   pTrafficProfile = R2544UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                     pSlaEntry->u4SlaTrafProfId);

   if(pTrafficProfile == NULL)
   {
      RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                    "R2544TmrSlaTimerExp: "
                    "Traffic profile entry not present for SLA %d\r\n",
                    pSlaEntry->u4SlaTrafProfId);
      return;
   }
   R2544TmrStopTimer(pSlaEntry, RFC2544_SLA_TIMER);
   R2544TmrStartTimer (pSlaEntry, RFC2544_DWELL_TIME_TIMER, pTrafficProfile->u4TrafProfDwellTime);
   return;
}


  
/**************************************************************************** 
*                                                                           * 
*    Function Name       : R2544TmrDwellTImeTimerExp                        * 
*                                                                           * 
*    Description         : This function will handle ho                     * 
*                          event.                                           * 
*                                                                           * 
*    Input(s)            : pArg - Pointer to ring node.                     * 
*                                                                           * 
*    Output(s)           : None                                             * 
*                                                                           * 
*    Returns             : None.                                            * 
*****************************************************************************/ 
VOID 
R2544TmrDwellTimeTimerExp (VOID *pArg) 
{ 
   tSlaEntry * pSlaEntry; 
  
   pSlaEntry = (tSlaEntry *) pArg; 
  
   if(pSlaEntry == NULL) 
   { 
       return; 
   } 
   R2544TmrStopTimer (pSlaEntry,RFC2544_DWELL_TIME_TIMER);  
   R2544CoreReportRequest(pArg);
  
   return; 
} 
  


#endif
