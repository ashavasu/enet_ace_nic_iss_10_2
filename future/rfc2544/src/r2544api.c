/*****************************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544api.c,v 1.4 2016/01/21 10:28:19 siva Exp $
 *
 * Description: This file contains the R2544 Api functions
 *
 *****************************************************************************/
#ifndef _R2544API_C_
#define _R2544API_C_

#include "r2544inc.h"

/*****************************************************************************
 *                                                                           *
 * Function     : R2544ApiHandleExtRequest                                   *
 *                                                                           *
 * Description  : This function is the common entry point for all the        *
 *                external module.                                           *
 *                                                                           *
 * Input        : pR2544ReqParams - Pointer to the Request params.           *
 *                                                                           *
 * Output       : pR2544RespParams - Pointer to the response params          *
 *                                                                           *
 * Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
 *                                                                           *
 *****************************************************************************/
INT4
R2544ApiHandleExtRequest (tR2544ReqParams * pR2544ReqParams,
                          tR2544RespParams * pR2544RespParams)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tR2544ReqParams    *pMsg = NULL;
    UNUSED_PARAM (pR2544RespParams);
    /* External Requests Handling */

    if (pR2544ReqParams == NULL)
    {
        return OSIX_FAILURE;
    }

    /* External Requests Handling */
    switch (pR2544ReqParams->u4ReqType)
    {
        case R2544_CREATE_CONTEXT_MSG:
            /* This API is called by the VCM module.
             * This indicates the creation of context
             *
             * Input : u4ContextId-Context id
             *
             **/
        case R2544_DELETE_CONTEXT_MSG:
            /* This API is called by the VCM module.
             * This indicates the deletion of context
             *
             * Input : u4ContextId-Context id
             *
             **/
        case R2544_THROUGHPUT_TEST_RESULT:
            /* This API is called by Y.1731 module.
             * This gives the throughput test result
             *
             * Input : 
             **/
        case R2544_LATENCY_TEST_RESULT:
            /* This API is called by Y.1731 module.
             * This gives the throughput test result
             *
             * Input : 
             **/
        case R2544_FRAMELOSS_TEST_RESULT:
            /* This API is called by Y.1731 module.
             * This gives the throughput test result
             *
             * Input : 
             **/
        case R2544_BACKTOBACK_TEST_RESULT:
            /* This API is called by Y.1731 module.
             * This gives the throughput test result
             *
             * Input : 
             **/
        case R2544_PERF_TEST_PARAMS:
        case ECFM_DEFECT_CONDITION_ENCOUNTERED:
        case ECFM_RDI_CONDITION_ENCOUNTERED:
        case ECFM_DEFECT_CONDITION_CLEARED:
        case ECFM_RDI_CONDITION_CLEARED:
            /* This is called by ECFM/Y1731 module to indicate the
             * defect condition and defect clearance status
             * Input : CFM entries,
             *         Context Id,
             *         SLA Id */

            /* Allocate memory for Queue Message */
            if ((pMsg = (tR2544ReqParams *)
                 MemAllocMemBlk (RFC2544_QMSG_POOL ())) == NULL)
            {
                gR2544GlobalInfo.u4MemFailCount++;
                return OSIX_FAILURE;
            }

            /* Form the message */
            MEMCPY (pMsg, pR2544ReqParams, sizeof (tR2544ReqParams));
            i4RetVal = (R2544QueEnqMsg (pMsg));
            break;
        default:
            i4RetVal = OSIX_FAILURE;
            break;
    }                            /* end of switch */
    return i4RetVal;
}

/*************************************************************************
 * FUNCTION NAME    : R2544ApiLock                                       *
 *                                                                       *
 * DESCRIPTION      : Function to take the mutual exclusion protocol     *
 *                    semaphore                                          *
 *                                                                       *
 * INPUT            : NONE                                               *
 *                                                                       *
 * OUTPUT           : NONE                                               *
 *                                                                       *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                          *
 *                                                                       *
 *************************************************************************/
PUBLIC INT4
R2544ApiLock (VOID)
{
    if (OsixSemTake (RFC2544_SEM_ID ()) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/**************************************************************************
 * FUNCTION NAME    : R2544ApiUnLock                                      *
 *                                                                        *
 * DESCRIPTION      : Function to release the mutual exclusion protocol   *
 *                    semaphore                                           *
 *                                                                        *
 * INPUT            : NONE                                                *
 *                                                                        *
 * OUTPUT           : NONE                                                *
 *                                                                        *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE                           *
 *                                                                        *
 **************************************************************************/
PUBLIC INT4
R2544ApiUnLock (VOID)
{
    if (OsixSemGive (RFC2544_SEM_ID ()) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
 * Function     : Y1564ApiCreateContext                                      *
 *                                                                           *
 * Description  : This function is called to by VCM Module to indicate the   *
 *                context creation.                                          *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
R2544ApiCreateContext (UINT4 u4ContextId)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tR2544ReqParams     R2544ReqParam;
    tR2544RespParams    R2544RespParams;
    tR2544ReqParams    *pR2544ReqParams = NULL;

    if (RFC2544_INITIALISED () != RFC2544_TRUE)
    {
        return OSIX_FAILURE;
    }
    pR2544ReqParams = &R2544ReqParam;
    MEMSET (pR2544ReqParams, 0, sizeof (tR2544ReqParams));

    MEMSET (&R2544RespParams, 0, sizeof (tR2544RespParams));

    pR2544ReqParams->u4ReqType = R2544_CREATE_CONTEXT_MSG;
    pR2544ReqParams->u4ContextId = u4ContextId;
    i4RetVal = R2544ApiHandleExtRequest (pR2544ReqParams, &R2544RespParams);

    UNUSED_PARAM (u4ContextId);
    return i4RetVal;
}

/*****************************************************************************
 * Function     : R2544ApiDeleteContext                                      *
 *                                                                           *
 * Description  : This function is called to by VCM Module to indicate the   *
 *                context deletion.                                          *
 *                                                                           *
 * Input        : u4ContextId - Context Id                                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
R2544ApiDeleteContext (UINT4 u4ContextId)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tR2544ReqParams     R2544ReqParam;
    tR2544RespParams    R2544RespParams;
    tR2544ReqParams    *pR2544ReqParams = NULL;

    pR2544ReqParams = &R2544ReqParam;
    MEMSET (&R2544ReqParam, 0, sizeof (tR2544ReqParams));

    MEMSET (&R2544RespParams, 0, sizeof (tR2544RespParams));

    R2544ReqParam.u4ReqType = R2544_DELETE_CONTEXT_MSG;
    R2544ReqParam.u4ContextId = u4ContextId;

    i4RetVal = R2544ApiHandleExtRequest (pR2544ReqParams, &R2544RespParams);

    return i4RetVal;
}

/****************************************************************************
 * FUNCTION NAME    : R2544ApiModuleShutDown                                *
 *                                                                          *
 * DESCRIPTION      : This API completely shuts the module down. RM uses    *
 *                    this API to restart the module to handle Communication* 
 *                    lost and restored scenario.                           *
 *                                                                          *
 * INPUT            : None                                                  *
 *                                                                          *
 * OUTPUT           : None                                                  *
 *                                                                          *
 * RETURNS          : VOID                                                  *
 *                                                                          *
 ****************************************************************************/
PUBLIC VOID
R2544ApiModuleShutDown (VOID)
{
    /* RFC2544_LOCK and RFC2544_UNLOCK is done inside this function itself */
    R2544MainModuleShutDown ();
    return;
}

#endif
