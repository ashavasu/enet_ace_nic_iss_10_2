/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved]
 *
 * $Id: r2544core.c,v 1.19.10.1 2018/05/09 13:33:29 siva Exp $
 *
 * Description: This file contains RFC2544 core function
 *                       
 *******************************************************************/

#include "r2544inc.h"

/****************************************************************************
*                                                                           *
*     FUNCTION NAME    : R2544CoreStartSlaTest                              *
*                                                                           *
*     DESCRIPTION      : This function will  initiate ECFM/Y1731 to initiate*
*                        the test                                           *
*                                                                           *
*     INPUT            : pSlaEntry - Sla Entry Information                  *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                    *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
R2544CoreStartSlaTest (tSlaEntry * pSlaEntry)
{

    tUtlSysPreciseTime  SysPreciseTime;
    tTrafficProfile    *pTrafficProfile = NULL;
    tR2544ExtInParams  *pR2544ExtInParams = NULL;
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;
    tReportStatistics  *pReportStatistics = NULL;
    tReportStatistics   ReportStatistics;

    UINT4               au4FrameSize[RFC2544_MAX_FRAMESIZE] =
        { RFC2544_FRAME_64, RFC2544_FRAME_128,
        RFC2544_FRAME_256, RFC2544_FRAME_512, RFC2544_FRAME_768,
        RFC2544_FRAME_1024, RFC2544_FRAME_1280, RFC2544_FRAME_1518
    };
    UINT4               u4Count = RFC2544_ZERO;
    UINT4               u4Duration = RFC2544_ZERO;

    RFC2544_MEMSET (au4FrameSize, RFC2544_ZERO, sizeof (au4FrameSize));
    RFC2544_MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    RFC2544_MEMSET (&ReportStatistics, 0, sizeof (tReportStatistics));

    /* Fetch the traffic profile Mapped to the SLA */
    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC ("R2544CoreStartSlaTest: Memory allocation"
                            "failed r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_GLOBAL_TRC ("R2544CoreStartSlaTest: Memory allocatio"
                            "failed r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));

    pTrafficProfile = R2544UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                 pSlaEntry->u4SlaTrafProfId);

    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                      "Traffic Profile entry does not exists for Traffic"
                      " Profile ID :%d \r\n", pSlaEntry->u4SlaTrafProfId);

        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                            (UINT1 *) pR2544ExtOutParams);
        return RFC2544_FAILURE;
    }

    /* 
     *  |-------|-------|-------|-------|-------|
     *  | Frame |Throug |Latency|Frame  |Back To|  
     *  |  Size | hput  |       |Loss   |Back   |       
     *  |-------|-------|-------|-------|-------|
     *  |  64   |  1    |   1   |  1    |  0    |
     *  |-------|-------|-------|-------|-------|
     *  |  128  |  1    |   1   |  1    |  0    |
     *  |-------|-------|-------|-------|-------|
     *  |  256  |  1    |   1   |  1    |  0    |
     *  |-------|-------|-------|-------|-------|
     *  |  512  |  1    |   1   |  1    |  0    |
     *  |-------|-------|-------|-------|-------|
     *  |  1024 |  1    |   1   |  1    |  0    |
     *  |-------|-------|-------|-------|-------|
     *  |   0   |  0    |   0   |  0    |  0    |   
     *  |-------|-------|-------|-------|-------|
     *  |   0   |  0    |   0   |  0    |  0    | 
     *  |-------|-------|-------|-------|-------|
     *
     */

    for (u4Count = 0; u4Count < RFC2544_MAX_FRAMESIZE; u4Count++)
    {
        if (pTrafficProfile->au4TrafProfFrameSize[u4Count][RFC2544_ZERO] ==
            RFC2544_ZERO)
        {
            break;
        }
        if (pTrafficProfile->au4TrafProfFrameSize[u4Count][RFC2544_ZERO] <=
            pSlaEntry->u4IfMtu)
        {
            if (pTrafficProfile->u1TrafProfThTestStatus == RFC2544_ENABLED)
            {
                pTrafficProfile->
                    au4TrafProfFrameSize[u4Count][RFC2544_THROUGHPUT_TEST] =
                    RFC2544_SET;
            }
            if (pTrafficProfile->u1TrafProfLaTestStatus == RFC2544_ENABLED)
            {
                pTrafficProfile->
                    au4TrafProfFrameSize[u4Count][RFC2544_LATENCY_TEST] =
                    RFC2544_SET;
            }
            if (pTrafficProfile->u1TrafProfFlTestStatus == RFC2544_ENABLED)
            {
                pTrafficProfile->
                    au4TrafProfFrameSize[u4Count][RFC2544_FRAMELOSS_TEST] =
                    RFC2544_SET;
            }
            if (pTrafficProfile->u1TrafProfBbTestStatus == RFC2544_ENABLED)
            {
                pTrafficProfile->
                    au4TrafProfFrameSize[u4Count][RFC2544_BACKTOBACK_TEST] =
                    RFC2544_SET;
            }
        }
        else
        {
            RFC2544_TRC2 (RFC2544_CRITICAL_TRC, pSlaEntry->u4ContextId,
                          "Framesize is greater than MTU %d. "
                          "So test is not performed for %d framesize\r\n",
                          pSlaEntry->u4IfMtu,
                          pTrafficProfile->
                          au4TrafProfFrameSize[u4Count][RFC2544_ZERO]);

        }

    }
    pTrafficProfile->u4TrafProfThCurrTrial = RFC2544_ONE;
    pTrafficProfile->u4TrafProfLaCurrTrial = RFC2544_ONE;
    pTrafficProfile->u4TrafProfFlCurrTrial = RFC2544_ONE;
    pTrafficProfile->u4TrafProfBbCurrTrial = RFC2544_ONE;

    /*Calculate the max trial Count for Throughput */
    if (pTrafficProfile->u4TrafProfThMaxRate != RFC2544_ZERO)
    {
        if (pTrafficProfile->u4TrafProfThRateStep != RFC2544_ZERO)
        {
            pTrafficProfile->u4TrafProfThMaxTrial =
                (((pTrafficProfile->u4TrafProfThMaxRate) -
                  (pTrafficProfile->u4TrafProfThMinRate)) /
                 pTrafficProfile->u4TrafProfThRateStep) + RFC2544_ONE;
        }
        else
        {
            pTrafficProfile->u4TrafProfThMaxTrial = RFC2544_ONE;
        }
    }
    /* Calculate the max trial Count for Frameloss */

    if (pTrafficProfile->u4TrafProfFlMaxRate != RFC2544_ZERO)
    {

        pTrafficProfile->u4TrafProfFlMaxTrial =
            (pTrafficProfile->u4TrafProfFlMaxRate -
             pTrafficProfile->u4TrafProfFlMinRate) /
            pTrafficProfile->u4TrafProfFlRateStep + 1;
    }

    if (pTrafficProfile->u1TrafProfThTestStatus == RFC2544_ENABLED)
    {
        for (u4Count = 0; u4Count < pTrafficProfile->u4TrafProfThMaxTrial;
             u4Count++)
        {
            pTrafficProfile->au1ThroughputCount[u4Count] = RFC2544_SET;
        }
    }
    if (pTrafficProfile->u1TrafProfLaTestStatus == RFC2544_ENABLED)
    {
        for (u4Count = 0; u4Count < RFC2544_LATENCY_MAX_TRIAL; u4Count++)
        {
            pTrafficProfile->au1LatencyCount[u4Count] = RFC2544_SET;
        }
    }
    if (pTrafficProfile->u1TrafProfFlTestStatus == RFC2544_ENABLED)
    {
        for (u4Count = 0; u4Count < pTrafficProfile->u4TrafProfFlMaxTrial;
             u4Count++)
        {
            pTrafficProfile->au1FrameLossCount[u4Count] = RFC2544_SET;
        }
    }
    if (pTrafficProfile->u1TrafProfBbTestStatus == RFC2544_ENABLED)
    {
        for (u4Count = 0; u4Count < pTrafficProfile->u4TrafProfBbTrialCount;
             u4Count++)
        {
            pTrafficProfile->au1BackToBackCount[u4Count] = RFC2544_SET;
        }
    }
    pTrafficProfile->u4FlPassCount = RFC2544_ZERO;
    UtlGetPreciseSysTime (&SysPreciseTime);
    pSlaEntry->R2544TestStartTime = SysPreciseTime.u4Sec;

    pR2544ExtInParams->EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMeg;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMe;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMep;
    pR2544ExtInParams->EcfmReqParams.u4IfIndex = pSlaEntry->u4IfIndex;
    pR2544ExtInParams->EcfmReqParams.u4TagType = pSlaEntry->u4TagType;
    pR2544ExtInParams->EcfmReqParams.u4PortSpeed = pSlaEntry->u4PortSpeed;
    pR2544ExtInParams->EcfmReqParams.OutVlanId = pSlaEntry->OutVlanId;
    RFC2544_MEMCPY (pR2544ExtInParams->EcfmReqParams.TxSrcMacAddr,
                    pSlaEntry->TxSrcMacAddr, sizeof (tMacAddr));

    RFC2544_MEMCPY (pR2544ExtInParams->EcfmReqParams.TxDstMacAddr,
                    pSlaEntry->TxDstMacAddr, sizeof (tMacAddr));

    pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u2FrameSize =
        (UINT2) pTrafficProfile->
        au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_ZERO];
    pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1TrafProfPCP =
        (UINT1) pTrafficProfile->u4TrafProfPCP;
    pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u4TrafProfDwellTime =
        (UINT1) pTrafficProfile->u4TrafProfDwellTime;
    pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1TrafProfSeqNoCheck =
        (UINT1) pTrafficProfile->u1TrafProfSeqNoCheck;
    if (pTrafficProfile->
        au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_THROUGHPUT_TEST] ==
        RFC2544_SET)
    {
        pTrafficProfile->u4TrafProfThCurrRate =
            pTrafficProfile->u4TrafProfThMaxRate;
        pR2544ExtInParams->eExtReqType = R2544_REQ_START_THROUGHPUT_TEST;
        pR2544ExtInParams->EcfmReqParams.u4MsgType =
            ECFM_R2544_REQ_START_THROUGHPUT_TEST;

        pR2544ExtInParams->EcfmReqParams.ThroughputParam.u4TrialDuration =
            (pTrafficProfile->u4TrafProfThTrialDuration);
        pR2544ExtInParams->EcfmReqParams.ThroughputParam.u4TrialCount =
            RFC2544_ONE;
        pR2544ExtInParams->EcfmReqParams.ThroughputParam.u4Rate =
            (pTrafficProfile->u4TrafProfThCurrRate * (pSlaEntry->u4PortSpeed /
                                                      RFC2544_HUNDRED));
        pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1R2544SubTest =
            RFC2544_THROUGHPUT_TEST;
        u4Duration = pTrafficProfile->u4TrafProfThTrialDuration;
        pTrafficProfile->au1ThroughputCount[RFC2544_ZERO] = RFC2544_SET;
        RFC2544_TRC3 (RFC2544_BENCHMARK_TEST_TRC, pSlaEntry->u4ContextId,
                      "R2544CoreStartSlaTest: Throughput Test Started for"
                      " Context Id: %d, "
                      "Sla Id: %d,  Frame Size: %d \r\n",
                      pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                      pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                      u2FrameSize);

        RFC2544_TRC5 (RFC2544_SESSION_RECORD_TRC, pSlaEntry->u4ContextId,
                      "R2544CoreStartSlaTest: Throughput Test Started for"
                      " Context Id: %d, "
                      "Sla Id: %d, Frame Size: %d, Trial Count: %d, "
                      "Rate: %d%% of Portspeed \r\n",
                      pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                      pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                      u2FrameSize,
                      pR2544ExtInParams->EcfmReqParams.ThroughputParam.
                      u4TrialCount, pTrafficProfile->u4TrafProfThCurrRate);

    }
    else
    {
        if (pTrafficProfile->
            au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_FRAMELOSS_TEST] ==
            RFC2544_SET)
        {
            pTrafficProfile->u4TrafProfFlCurrRate =
                pTrafficProfile->u4TrafProfFlMaxRate;
            pR2544ExtInParams->eExtReqType = R2544_REQ_START_FRAMELOSS_TEST;
            pR2544ExtInParams->EcfmReqParams.u4MsgType =
                ECFM_R2544_REQ_START_FRAMELOSS_TEST;
            pR2544ExtInParams->EcfmReqParams.FrameLossParam.u4TrialDuration =
                (pTrafficProfile->u4TrafProfFlTrialDuration);
            pR2544ExtInParams->EcfmReqParams.FrameLossParam.u4TrialCount =
                RFC2544_ZERO;
            pR2544ExtInParams->EcfmReqParams.FrameLossParam.u4Rate =
                (pTrafficProfile->u4TrafProfFlCurrRate *
                 (pSlaEntry->u4PortSpeed / RFC2544_HUNDRED));
            pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                u1R2544SubTest = RFC2544_FRAMELOSS_TEST;
            u4Duration = pTrafficProfile->u4TrafProfFlTrialDuration;
            pTrafficProfile->au1FrameLossCount[RFC2544_ZERO] = RFC2544_SET;
            RFC2544_TRC3 (RFC2544_BENCHMARK_TEST_TRC, pSlaEntry->u4ContextId,
                          "R2544CoreStartSlaTest: FrameLoss Test Started for"
                          " Context Id: %d, "
                          "Sla Id: %d, Frame Size: %d \r\n",
                          pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                          u2FrameSize);

            RFC2544_TRC5 (RFC2544_SESSION_RECORD_TRC, pSlaEntry->u4ContextId,
                          "R2544CoreStartSlaTest: FrameLoss Test Started for"
                          " Context Id : %d, "
                          "Sla Id: %d, Frame Size: %d, Trial Count: %d, "
                          "Rate: %d%% of Portspeed \r\n",
                          pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                          u2FrameSize,
                          pR2544ExtInParams->EcfmReqParams.FrameLossParam.
                          u4TrialCount, pTrafficProfile->u4TrafProfFlCurrRate);

        }
        else
        {
            if (pTrafficProfile->
                au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_BACKTOBACK_TEST] ==
                RFC2544_SET)
            {
                pR2544ExtInParams->eExtReqType =
                    R2544_REQ_START_BACKTOBACK_TEST;
                pR2544ExtInParams->EcfmReqParams.u4MsgType =
                    ECFM_R2544_REQ_START_BACKTOBACK_TEST;
                pR2544ExtInParams->EcfmReqParams.BackToBackParam.u4TrialCount =
                    RFC2544_ZERO;
                pR2544ExtInParams->EcfmReqParams.BackToBackParam.u4Rate =
                    pSlaEntry->u4PortSpeed;
                pR2544ExtInParams->EcfmReqParams.BackToBackParam.
                    u4TrialDuration =
                    (pTrafficProfile->u4TrafProfBbTrialDuration /
                     RFC2544_THOUSAND);
                pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                    u1R2544SubTest = RFC2544_BACKTOBACK_TEST;
                u4Duration =
                    pTrafficProfile->u4TrafProfBbTrialDuration /
                    RFC2544_THOUSAND;
                pTrafficProfile->au1BackToBackCount[RFC2544_ZERO] = RFC2544_SET;
                RFC2544_TRC3 (RFC2544_BENCHMARK_TEST_TRC,
                              pSlaEntry->u4ContextId,
                              "R2544CoreStartSlaTest: Back-To-Back Test "
                              "Started for Context Id: %d, "
                              "Sla Id: %d, Frame Size: %d \r\n",
                              pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                              pR2544ExtInParams->EcfmReqParams.
                              TrafficProfileParam.u2FrameSize);

                RFC2544_TRC5 (RFC2544_SESSION_RECORD_TRC,
                              pSlaEntry->u4ContextId,
                              "R2544CoreStartSlaTest: Back-To-Back Test "
                              "Started for Context Id: %d, "
                              "Sla Id: %d, Frame Size: %d, Trial Count: %d,"
                              " Rate: %d%% of Portspeed \r\n",
                              pSlaEntry->u4ContextId, pSlaEntry->u4SlaId,
                              pR2544ExtInParams->EcfmReqParams.
                              TrafficProfileParam.u2FrameSize,
                              pR2544ExtInParams->EcfmReqParams.BackToBackParam.
                              u4TrialCount, RFC2544_HUNDRED);

            }
        }
    }

    if (R2544PortHandleExtInteraction (pR2544ExtInParams, pR2544ExtOutParams) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                            (UINT1 *) pR2544ExtOutParams);
        return RFC2544_FAILURE;
    }

    if (pTrafficProfile->
        au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_THROUGHPUT_TEST] ==
        RFC2544_SET)
    {
        pTrafficProfile->au1ThroughputCount[RFC2544_ZERO] = RFC2544_RESET;
        pTrafficProfile->u4TrafProfThCurrTrial++;
        pSlaEntry->u1PrevSubTest = RFC2544_THROUGHPUT_TEST;
        pSlaEntry->u4PrevRate =
            (pTrafficProfile->u4TrafProfThCurrRate * (pSlaEntry->u4PortSpeed /
                                                      RFC2544_HUNDRED));
    }
    else
    {
        if (pTrafficProfile->
            au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_FRAMELOSS_TEST] ==
            RFC2544_SET)
        {
            pTrafficProfile->au1FrameLossCount[RFC2544_ZERO] = RFC2544_RESET;
            pTrafficProfile->u4TrafProfFlCurrTrial++;
            pSlaEntry->u1PrevSubTest = RFC2544_FRAMELOSS_TEST;
            pSlaEntry->u4PrevRate =
                (pTrafficProfile->u4TrafProfFlCurrRate *
                 (pSlaEntry->u4PortSpeed / RFC2544_HUNDRED));
        }
        else if (pTrafficProfile->
                 au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_BACKTOBACK_TEST] ==
                 RFC2544_SET)
        {
            pTrafficProfile->au1BackToBackCount[RFC2544_ZERO] = RFC2544_RESET;
            pTrafficProfile->u4TrafProfBbCurrTrial++;
            pSlaEntry->u1PrevSubTest = RFC2544_BACKTOBACK_TEST;
        }
    }

    R2544TmrStartTimer (pSlaEntry, RFC2544_SLA_TIMER, u4Duration);

    pSlaEntry->u1SlaCurrentTestState = RFC2544_INPROGRESS;

    pSlaEntry->u4PrevFrameSize =
        pTrafficProfile->au4TrafProfFrameSize[RFC2544_ZERO][RFC2544_ZERO];
    pSlaEntry->u4PrevTrialCount = RFC2544_ZERO;

    /* Add report Stats entry for each frame size */
    for (u4Count = 0; u4Count < RFC2544_MAX_FRAMESIZE; u4Count++)
    {
        if (pTrafficProfile->au4TrafProfPrevFrameSize[u4Count] != RFC2544_ZERO)
        {
            pReportStatistics =
                R2544UtilGetReportStatsEntry (pSlaEntry->u4ContextId,
                                              pSlaEntry->u4SlaId,
                                              pTrafficProfile->
                                              au4TrafProfPrevFrameSize
                                              [u4Count]);
            if (pReportStatistics != NULL)
            {
                R2544UtilDelReportStats (pReportStatistics);
            }
        }
    }

    for (u4Count = 0; u4Count < RFC2544_MAX_FRAMESIZE; u4Count++)
    {
        pReportStatistics =
            R2544UtilGetReportStatsEntry (pSlaEntry->u4ContextId,
                                          pSlaEntry->u4SlaId,
                                          pTrafficProfile->
                                          au4TrafProfFrameSize[u4Count]
                                          [RFC2544_ZERO]);

        if (pReportStatistics == NULL)
        {
            if ((R2544UtilAddReportStatsEntry (pSlaEntry->u4ContextId,
                                               pSlaEntry->u4SlaId,
                                               pTrafficProfile->
                                               au4TrafProfFrameSize[u4Count]
                                               [RFC2544_ZERO]) ==
                 RFC2544_FAILURE))
            {
                RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC, pSlaEntry->u4ContextId,
                              "R2544CoreStartSlaTest: Report Stats"
                              " Addition Failed for "
                              "Sla Id %d Frame Size %d\r\n",
                              pSlaEntry->u4SlaId,
                              pTrafficProfile->
                              au4TrafProfFrameSize[u4Count][RFC2544_ZERO]);
                return RFC2544_FAILURE;
            }
        }
        else
        {
            pReportStatistics->u4ReportStatsThVerifiedBps = RFC2544_ZERO;
            pReportStatistics->u4ReportStatsLatencyMin = RFC2544_ZERO;
            pReportStatistics->u4ReportStatsLatencyMax = RFC2544_ZERO;
            pReportStatistics->u4ReportStatsLatencyMean = RFC2544_ZERO;
            pReportStatistics->u4ReportStatsLatencyFailCount = RFC2544_ZERO;
            pReportStatistics->u4ReportStatsLaIterationCalculated =
                RFC2544_ZERO;
            pReportStatistics->u4ReportStatsFrameLossRate = RFC2544_ZERO;
            pReportStatistics->u4ReportStatsBacktoBackBurstSize = RFC2544_ZERO;
            pReportStatistics->u1ReportStatsThResult = RFC2544_NOTEXECUTED;
            pReportStatistics->u1ReportStatsLatencyResult = RFC2544_NOTEXECUTED;
            pReportStatistics->u1ReportStatsFrameLossResult =
                RFC2544_NOTEXECUTED;
            pReportStatistics->u1ReportStatsBbResult = RFC2544_NOTEXECUTED;
        }

    }
    for (u4Count = 0; u4Count < RFC2544_MAX_FRAMESIZE; u4Count++)
    {

        pTrafficProfile->au4TrafProfPrevFrameSize[u4Count] =
            pTrafficProfile->au4TrafProfFrameSize[u4Count][RFC2544_ZERO];

    }

    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);

    return RFC2544_SUCCESS;
}

/****************************************************************************
*                                                                           *
*     FUNCTION NAME    : R2544CoreStopSlaTest                               *
*                                                                           *
*     DESCRIPTION      : This function will  initiate ECFM/Y1731 to stop    *
*                        the test                                           *
*                                                                           *
*     INPUT            : pSlaEntry - Sla Entry Information                  *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                    *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
R2544CoreStopSlaTest (tSlaEntry * pSlaEntry)
{
    tUtlSysPreciseTime  SysPreciseTime;
    tR2544ExtInParams  *pR2544ExtInParams = NULL;
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;

    RFC2544_MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC ("R2544CoreStopSlaTest :Memory allocation "
                            " failed \r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_GLOBAL_TRC ("R2544CoreStopSlaTest :Memory allocation "
                            " failed \r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));

    /* Stop the Timer */
    R2544TmrStopTimer (pSlaEntry, RFC2544_SLA_TIMER);
    R2544TmrStopTimer (pSlaEntry, RFC2544_DWELL_TIME_TIMER);

    UtlGetPreciseSysTime (&SysPreciseTime);
    pSlaEntry->R2544TestEndTime = SysPreciseTime.u4Sec;

    if (pSlaEntry->u1SlaCurrentTestState == RFC2544_INPROGRESS)
    {
        pSlaEntry->u1SlaCurrentTestState = RFC2544_COMPLETED;
    }
    /* Post Message to ECFM/Y1731 to stop the test */
    pR2544ExtInParams->eExtReqType = R2544_REQ_STOP_BENCHMARK_TEST;
    pR2544ExtInParams->EcfmReqParams.u4MsgType =
        ECFM_R2544_REQ_STOP_BENCHMARK_TEST;
    pR2544ExtInParams->u4ContextId = pSlaEntry->u4ContextId;
    pR2544ExtInParams->EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMeg;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMe;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMep;

    if (R2544PortHandleExtInteraction (pR2544ExtInParams, pR2544ExtOutParams) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                            (UINT1 *) pR2544ExtOutParams);
        return RFC2544_FAILURE;
    }

    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);

    return RFC2544_SUCCESS;
}

/****************************************************************************
*                                                                           *
*     FUNCTION NAME    : R2544CoreGenerateThResult                          *
*                                                                           *
*     DESCRIPTION      : This function will generate the throughput result  *
*                        and update the throughput result in the result     *
*                        stats parameter                                    *
*                                                                           *
*     INPUTS           : u4ContextId  - Context Id                          *
*                        u4SlaId - SLA Id                                   *
*                        u4FrameSize - Frame Size                           *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                    *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
R2544CoreGenerateThResult (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4FrameSize,
                           UINT4 u4TrialCount)
{

    tReportStatistics  *pReportStatistics = NULL;
    tThroughputStats   *pThroughputStats = NULL;
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficprofile = NULL;
    tSacEntry          *pSacEntry = NULL;
    FLT4                f4FrameLoss = RFC2544_ZERO;
    UINT4               u4SetCount = RFC2544_ZERO;
    INT4                i4Count = RFC2544_ZERO;

    pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateThResult: "
                      "Sla entry not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;

    }

    pTrafficprofile = R2544UtilGetTrafProfEntry (u4ContextId,
                                                 pSlaEntry->u4SlaTrafProfId);

    if (pTrafficprofile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateThResult: "
                      "Traffic profile entry not present for SLA %d\r\n",
                      u4SlaId);
        return RFC2544_FAILURE;

    }
    pSacEntry = R2544UtilGetSacEntry (u4ContextId, pSlaEntry->u4SlaSacId);
    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateThResult: "
                      "Sac entry not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;

    }

    pThroughputStats =
        R2544UtilGetThroughputStats (u4ContextId, u4SlaId, u4FrameSize,
                                     u4TrialCount);
    if (pThroughputStats == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC,
                      u4ContextId,
                      "R2544CoreGenerateThResult: "
                      "Throughput Stats not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;
    }
    if (pThroughputStats->u4TxCount != RFC2544_ZERO)
    {

        f4FrameLoss =
            ((((FLT4) pThroughputStats->u4TxCount -
               (FLT4) pThroughputStats->u4RxCount) * RFC2544_HUNDRED) /
             (FLT4) pThroughputStats->u4TxCount);

        RFC2544_TRC6 (RFC2544_BENCHMARK_TEST_TRC, u4ContextId,
                      "R2544CoreGenerateThResult: "
                      "Throughput Stats for Context Id: %d, Sla Id: %d, "
                      "Frame size: %d, Trial Count: %d are\r\n"
                      "\t\t\t\t      No.of packets Transmitted: %d "
                      "and Received: %d\r\n", u4ContextId, u4SlaId,
                      u4FrameSize, u4TrialCount + RFC2544_ONE,
                      pThroughputStats->u4TxCount, pThroughputStats->u4RxCount);
        RFC2544_TRC5 (RFC2544_BENCHMARK_TEST_TRC, u4ContextId,
                      "R2544CoreGenerateThResult: "
                      "Calculated Frameloss ratio is %.2f%% for Context Id: %d, Sla Id: %d, "
                      "Frame size: %d, Trial Count: %d in Throughput test\r\n",
                      f4FrameLoss, u4ContextId, u4SlaId,
                      u4FrameSize, u4TrialCount + RFC2544_ONE);

    }
    pReportStatistics = R2544UtilGetReportStatsEntry (u4ContextId,
                                                      u4SlaId, u4FrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC,
                      u4ContextId,
                      "R2544CoreGenerateThResult: "
                      "Throughput report not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;
    }
    if (f4FrameLoss <= (FLT4) pSacEntry->u4SacThAllowedFrameLoss)
    {

        pReportStatistics->u4ReportStatsThVerifiedBps =
            (((pThroughputStats->u4RxCount /
               pTrafficprofile->u4TrafProfThTrialDuration) * u4FrameSize *
              RFC2544_BITS_PER_BYTE) / RFC2544_THOUSAND);

        if (pReportStatistics->u4ReportStatsThVerifiedBps != RFC2544_ZERO)
        {
            pReportStatistics->u1ReportStatsThResult = RFC2544_PASS;

            for (i4Count = 0; i4Count < RFC2544_MAX_FRAMESIZE; i4Count++)
            {
                if (pTrafficprofile->
                    au4TrafProfFrameSize[i4Count][RFC2544_ZERO] == u4FrameSize)
                {
                    break;
                }
            }
            pTrafficprofile->
                au4TrafProfFrameSize[i4Count][RFC2544_THROUGHPUT_TEST] =
                RFC2544_RESET;
            pTrafficprofile->u4TrafProfThCurrRate =
                pTrafficprofile->u4TrafProfThMaxRate;
            for (u4SetCount = 0;
                 u4SetCount < pTrafficprofile->u4TrafProfThMaxTrial;
                 u4SetCount++)
            {
                pTrafficprofile->au1ThroughputCount[u4SetCount] = RFC2544_SET;
            }
            pTrafficprofile->u4TrafProfThCurrTrial = RFC2544_ONE;
        }
        else
        {
            pReportStatistics->u1ReportStatsThResult = RFC2544_FAIL;
        }
    }
    else
    {
        pReportStatistics->u1ReportStatsThResult = RFC2544_FAIL;
    }
    R2544CoreTest (pSlaEntry);

    return RFC2544_SUCCESS;
}

/****************************************************************************
*                                                                           *
*     FUNCTION NAME    : R2544CoreGenerateLaResult                          *
*                                                                           *
*     DESCRIPTION      : This function will generate the latency result     *
*                        and update the latency result in the result        *
*                        stats parameter                                    *
*                                                                           *
*     INPUTS           : u4ContextId  - Context Id                          *
*                        u4SlaId - SLA Id                                   *
*                        u4FrameSize - Frame Size                           *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                    *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
R2544CoreGenerateLaResult (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4FrameSize)
{

    tReportStatistics  *pReportStatistics = NULL;
    tLatencyStats      *pLatencyStats = NULL;
    UINT4               u4LatencyMin = RFC2544_ZERO;
    UINT4               u4LatencyTotal = RFC2544_ZERO;
    UINT4               u4LatencyMax = RFC2544_ZERO;
    UINT4               u4TrialCount = RFC2544_ONE;
    UINT4               u4PassCount = RFC2544_ZERO;
    UINT4               u4FailCount = RFC2544_ZERO;
    UINT4               u4FrameLoss = RFC2544_ZERO;
    UINT4               u4SacId = RFC2544_ZERO;
    UINT4               u4LaAllowedFrameLoss = RFC2544_ZERO;

    if (nmhGetFs2544SlaSacId (u4ContextId, u4SlaId, &u4SacId) == SNMP_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateLaResult: "
                      "Failed to get SAC for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;

    }
    if (nmhGetFs2544SacLaAllowedFrameLoss (u4ContextId, u4SacId,
                                           &u4LaAllowedFrameLoss) ==
        SNMP_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateLaResult: "
                      "Failed to get Allowed frameloss from Sac for SLA %d\r\n",
                      u4SlaId);
        return RFC2544_FAILURE;

    }
    do
    {
        pLatencyStats =
            R2544UtilGetLatencyStats (u4ContextId, u4SlaId, u4FrameSize,
                                      u4TrialCount);

        if (pLatencyStats == NULL)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC,
                          u4ContextId,
                          "R2544CoreGenerateLaResult: "
                          "Latency Stats not present for SLA %d\r\n", u4SlaId);
            return RFC2544_FAILURE;
        }
        if (pLatencyStats->u4TxCount != 0)
        {
            u4FrameLoss =
                (((pLatencyStats->u4TxCount -
                   pLatencyStats->u4RxCount) * RFC2544_HUNDRED) /
                 pLatencyStats->u4TxCount);
            RFC2544_TRC6 (RFC2544_BENCHMARK_TEST_TRC, u4ContextId,
                          "R2544CoreGenerateLaResult: "
                          "Latency Stats for Context Id: %d, Sla Id: %d, "
                          "Frame size: %d, Trial Count: %d are\r\n"
                          "\t\t\t\t      No.of packets Transmitted: %d "
                          "and Received: %d\r\n", u4ContextId, u4SlaId,
                          u4FrameSize, u4TrialCount, pLatencyStats->u4TxCount,
                          pLatencyStats->u4RxCount);
            RFC2544_TRC5 (RFC2544_BENCHMARK_TEST_TRC, u4ContextId,
                          "R2544CoreGenerateLaResult: "
                          "Calculated Frameloss ratio is %d%% for Context Id: %d, Sla Id: %d, "
                          "Frame size: %d, Trial Count: %d in Latency test\r\n",
                          u4FrameLoss, u4ContextId, u4SlaId, u4FrameSize,
                          u4TrialCount + RFC2544_ONE);

        }

        if (u4FrameLoss <= u4LaAllowedFrameLoss)
        {
            if (u4TrialCount == RFC2544_ONE)
            {
                u4LatencyMin = pLatencyStats->u4LatencyMin;
                u4LatencyMax = pLatencyStats->u4LatencyMax;
                u4LatencyTotal = pLatencyStats->u4LatencyMean;
            }
            else
            {

                if ((pLatencyStats->u4LatencyMin < u4LatencyMin)
                    && (pLatencyStats->u4LatencyMin != RFC2544_ZERO))
                {
                    u4LatencyMin = pLatencyStats->u4LatencyMin;
                }
                if ((pLatencyStats->u4LatencyMax > u4LatencyMax)
                    && (pLatencyStats->u4LatencyMax != RFC2544_ZERO))
                {
                    u4LatencyMax = pLatencyStats->u4LatencyMax;
                }
                if (pLatencyStats->u4LatencyMean != RFC2544_ZERO)
                {
                    u4LatencyTotal += pLatencyStats->u4LatencyMean;
                }
            }
            u4PassCount++;
        }
        else
        {
            u4FailCount++;
        }
        u4TrialCount++;

    }
    while (u4TrialCount <= RFC2544_LATENCY_MAX_TRIAL);

    pReportStatistics = R2544UtilGetReportStatsEntry (u4ContextId,
                                                      u4SlaId, u4FrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC,
                      u4ContextId,
                      "R2544CoreGenerateLaResult: "
                      "Latency report not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;
    }

    if (u4TrialCount == RFC2544_LATENCY_MAX_TRIAL + RFC2544_ONE)
    {
        pReportStatistics->u4ReportStatsLatencyMin =
            pLatencyStats->u4LatencyMin;
        pReportStatistics->u4ReportStatsLatencyMax =
            pLatencyStats->u4LatencyMax;

        pReportStatistics->u4ReportStatsLaIterationCalculated = u4PassCount;
        pReportStatistics->u4ReportStatsLatencyFailCount = u4FailCount;
        if (u4PassCount > RFC2544_ZERO)
        {
            pReportStatistics->u4ReportStatsLatencyMean =
                (u4LatencyTotal / u4PassCount);
        }
        if (u4PassCount >= u4FailCount)
        {
            pReportStatistics->u1ReportStatsLatencyResult = RFC2544_PASS;
        }
        else
        {
            pReportStatistics->u1ReportStatsLatencyResult = RFC2544_FAIL;

        }
    }
    return RFC2544_SUCCESS;
}

/*****************************************************************************
*                                                                           *
*     FUNCTION NAME    : R2544CoreGenerateFlResult                          *
*                                                                           *
*     DESCRIPTION      : This function will generate the Frame Loss result  *
*                        and update the latency result in the result        *
*                        stats parameter                                    *
*                                                                           *
*     INPUTS           : u4ContextId  - Context Id                          *
*                        u4SlaId - SLA Id                                   *
*                        u4FrameSize - Frame Size                           *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                    *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
R2544CoreGenerateFlResult (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4FrameSize,
                           UINT4 u4TrialCount)
{

    tReportStatistics  *pReportStatistics = NULL;
    tFrameLossStats    *pFrameLossStats = NULL;
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficprofile = NULL;
    tSacEntry          *pSacEntry = NULL;
    UINT4               u4SetCount = RFC2544_ZERO;
    INT4                i4Count = RFC2544_ZERO;
    FLT4                f4FrameLoss = RFC2544_ZERO;

    pSlaEntry = R2544UtilGetSlaEntry (u4ContextId, u4SlaId);
    if (pSlaEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateFlResult: "
                      "Sla entry not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;
    }

    pTrafficprofile = R2544UtilGetTrafProfEntry (u4ContextId,
                                                 pSlaEntry->u4SlaTrafProfId);

    if (pTrafficprofile == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateFlResult: "
                      "Traffic profile entry not present for SLA %d\r\n",
                      u4SlaId);
        return RFC2544_FAILURE;

    }

    pSacEntry = R2544UtilGetSacEntry (u4ContextId, pSlaEntry->u4SlaSacId);

    if (pSacEntry == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateFlResult: "
                      "Sac entry not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;

    }

    pFrameLossStats =
        R2544UtilGetFrameLossStats (u4ContextId, u4SlaId, u4FrameSize,
                                    u4TrialCount);

    if (pFrameLossStats == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC |
                      RFC2544_BENCHMARK_TEST_TRC, u4ContextId,
                      "R2544CoreGenerateFlResult: "
                      "FrameLoss Stats not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;
    }
    if (pFrameLossStats->u4TxCount != RFC2544_ZERO)
    {
        f4FrameLoss =
            ((((FLT4) pFrameLossStats->u4TxCount -
               (FLT4) pFrameLossStats->u4RxCount) *
              RFC2544_HUNDRED) / (FLT4) pFrameLossStats->u4TxCount);

        RFC2544_TRC6 (RFC2544_BENCHMARK_TEST_TRC, u4ContextId,
                      "R2544CoreGenerateFlResult: "
                      "Frameloss Stats for Context Id: %d, Sla Id: %d, "
                      "Frame size: %d, Trial Count: %d are\r\n"
                      "\t\t\t\t      No.of packets Transmitted: %d "
                      "and Received: %d\r\n", u4ContextId, u4SlaId,
                      u4FrameSize, u4TrialCount + RFC2544_ONE,
                      pFrameLossStats->u4TxCount, pFrameLossStats->u4RxCount);

        RFC2544_TRC5 (RFC2544_BENCHMARK_TEST_TRC, u4ContextId,
                      "R2544CoreGenerateFlResult: "
                      "Calculated Frameloss ratio is %.2f%% for Context Id: %d, Sla Id: %d, "
                      "Frame size: %d, Trial Count: %d in Frameloss test\r\n",
                      f4FrameLoss, u4ContextId, u4SlaId,
                      u4FrameSize, u4TrialCount + RFC2544_ONE);

        if (f4FrameLoss <= (FLT4) pSacEntry->u4SacFlAllowedFrameLoss)
        {
            pTrafficprofile->u4FlPassCount++;

            if ((pTrafficprofile->u4FlPassFrameloss = RFC2544_ZERO)
                || (pTrafficprofile->u4FlPassFrameloss < (UINT4) f4FrameLoss))
            {
                pTrafficprofile->u4FlPassFrameloss = (UINT4) f4FrameLoss;
            }
        }
        else
        {
            if (pTrafficprofile->u4FlPassCount > RFC2544_ZERO)
            {
                pTrafficprofile->u4FlPassCount--;
            }
            else
            {
                pTrafficprofile->u4FlPassCount = RFC2544_ZERO;
            }

            if ((pTrafficprofile->u4FlFailFrameloss == RFC2544_ZERO)
                || (pTrafficprofile->u4FlFailFrameloss < (UINT4) f4FrameLoss))
            {
                pTrafficprofile->u4FlFailFrameloss = (UINT4) f4FrameLoss;
            }
        }
    }
    if ((pTrafficprofile->u4FlPassCount == RFC2544_TWO)
        || (u4TrialCount ==
            pTrafficprofile->u4TrafProfFlMaxTrial - RFC2544_ONE))
    {
        pReportStatistics = R2544UtilGetReportStatsEntry (u4ContextId,
                                                          u4SlaId, u4FrameSize);
        if (pReportStatistics == NULL)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC,
                          u4ContextId, "R2544CoreGenerateFlResult: "
                          "FrameLoss report not present for SLA %d\r\n",
                          u4SlaId);
            return RFC2544_FAILURE;
        }

        if (pFrameLossStats->u4TxCount == RFC2544_ZERO)
        {
            pReportStatistics->u1ReportStatsFrameLossResult = RFC2544_FAIL;
            pReportStatistics->u4ReportStatsFrameLossRate =
                (INT4) pTrafficprofile->u4FlFailFrameloss;

            pTrafficprofile->u4FlFailFrameloss = RFC2544_ZERO;
            pTrafficprofile->u4FlPassFrameloss = RFC2544_ZERO;
            pTrafficprofile->u4FlPassCount = RFC2544_ZERO;
        }

        else if (pTrafficprofile->u4FlPassCount == RFC2544_TWO)
        {
            pReportStatistics->u1ReportStatsFrameLossResult = RFC2544_PASS;
            pReportStatistics->u4ReportStatsFrameLossRate =
                (INT4) pTrafficprofile->u4FlPassFrameloss;

            for (i4Count = 0; i4Count < RFC2544_MAX_FRAMESIZE; i4Count++)
            {
                if (pTrafficprofile->
                    au4TrafProfFrameSize[i4Count][RFC2544_ZERO] == u4FrameSize)
                {
                    break;
                }
            }

            pTrafficprofile->
                au4TrafProfFrameSize[i4Count][RFC2544_FRAMELOSS_TEST] =
                RFC2544_RESET;
            pTrafficprofile->u4TrafProfFlCurrRate =
                pTrafficprofile->u4TrafProfThMaxRate;

            for (u4SetCount = 0;
                 u4SetCount < pTrafficprofile->u4TrafProfFlMaxTrial;
                 u4SetCount++)
            {
                pTrafficprofile->au1FrameLossCount[u4SetCount] = RFC2544_SET;
            }

            pTrafficprofile->u4FlFailFrameloss = RFC2544_ZERO;
            pTrafficprofile->u4FlPassFrameloss = RFC2544_ZERO;
            pTrafficprofile->u4FlPassCount = RFC2544_ZERO;
            pTrafficprofile->u4TrafProfFlCurrTrial = RFC2544_ONE;

        }

        else
        {
            pReportStatistics->u1ReportStatsFrameLossResult = RFC2544_FAIL;
            pReportStatistics->u4ReportStatsFrameLossRate =
                (INT4) pTrafficprofile->u4FlFailFrameloss;

            pTrafficprofile->u4FlFailFrameloss = RFC2544_ZERO;
            pTrafficprofile->u4FlPassFrameloss = RFC2544_ZERO;
            pTrafficprofile->u4FlPassCount = RFC2544_ZERO;
        }
    }
    R2544CoreTest (pSlaEntry);
    return RFC2544_SUCCESS;
}

/****************************************************************************
*                                                                           *
*     FUNCTION NAME    : R2544CoreGenerateBbResult                          *
*                                                                           *
*     DESCRIPTION      : This function will generate the Back to Back result*
*                        and update the latency result in the result        *
*                        stats parameter                                    *
*                                                                           *
*     INPUTS           : u4ContextId  - Context Id                          *
*                        u4SlaId - SLA Id                                   *
*                        u4FrameSize - Frame Size                           *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                    *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
R2544CoreGenerateBbResult (UINT4 u4ContextId, UINT4 u4SlaId, UINT4 u4FrameSize)
{

    tReportStatistics  *pReportStatistics = NULL;
    tBackToBackStats   *pBackToBackStats = NULL;
    UINT4               u4TrialCount = RFC2544_ONE;
    UINT4               u4TrafficProfileBbTrialCount = RFC2544_ZERO;
    UINT4               u4BurstTotal = RFC2544_ZERO;
    UINT4               u4BurstSize = RFC2544_ZERO;
    UINT4               u4TrafficProfileId = RFC2544_ZERO;

    if (nmhGetFs2544SlaTrafficProfileId (u4ContextId, u4SlaId,
                                         &u4TrafficProfileId) == SNMP_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateBbResult: "
                      "Traffic profile id  not present for SLA %d\r\n",
                      u4SlaId);
        return RFC2544_FAILURE;

    }
    if (nmhGetFs2544TrafficProfileBbTrialCount (u4ContextId, u4TrafficProfileId,
                                                &u4TrafficProfileBbTrialCount)
        == SNMP_FAILURE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC, u4ContextId,
                      "R2544CoreGenerateBbResult: "
                      "Failed to get BackToBack TrialCount from Traffic profile"
                      " for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;
    }

    do
    {
        pBackToBackStats =
            R2544UtilGetBackToBackStats (u4ContextId, u4SlaId, u4FrameSize,
                                         u4TrialCount);

        if (pBackToBackStats == NULL)
        {
            RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC,
                          u4ContextId,
                          "R2544CoreGenerateBbResult: "
                          "BackTOBack Stats not present for SLA %d\r\n",
                          u4SlaId);
            return RFC2544_FAILURE;
        }
        u4BurstTotal += pBackToBackStats->u4BurstSize;

        RFC2544_TRC5 (RFC2544_BENCHMARK_TEST_TRC, u4ContextId,
                      "R2544CoreGenerateBbResult: "
                      "Back-to-Back Stats for Context Id: %d, Sla Id: %d, "
                      "Frame size: %d, Trial Count: %d are\r\n"
                      "\t\t\t\t      Burst size: %d\r\n", u4ContextId, u4SlaId,
                      u4FrameSize, u4TrialCount, pBackToBackStats->u4BurstSize);

        u4TrialCount++;

    }
    while (u4TrialCount <= u4TrafficProfileBbTrialCount);

    u4BurstSize = u4BurstTotal / u4TrafficProfileBbTrialCount;

    pReportStatistics = R2544UtilGetReportStatsEntry (u4ContextId,
                                                      u4SlaId, u4FrameSize);
    if (pReportStatistics == NULL)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC,
                      u4ContextId,
                      "R2544CoreGenerateBbResult: "
                      "BackToBack report not present for SLA %d\r\n", u4SlaId);
        return RFC2544_FAILURE;
    }
    pReportStatistics->u4ReportStatsBacktoBackBurstSize = u4BurstSize;
    pReportStatistics->u1ReportStatsBbResult = RFC2544_PASS;

    return RFC2544_SUCCESS;
}

/***************************************************************************** 
 *                                                                           * 
 *    Function Name       : R2544CoreTest                                    * 
 *                                                                           * 
 *    Description         : This function will handle holdoff timer expiry   * 
 *                          event.                                           * 
 *                                                                           * 
 *    Input(s)            : pSlaEntry - Sla pointer                          * 
 *                                                                           * 
 *    Output(s)           : None                                             * 
 *                                                                           * 
 *    Returns             : None.                                            * 
 *****************************************************************************/

VOID
R2544CoreTest (tSlaEntry * pSlaEntry)
{
    /*Start the through  test for next frmae size */
    tR2544ExtInParams  *pR2544ExtInParams = NULL;
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;
    UINT4               u4Duration = RFC2544_ZERO;
    UINT4               u4SlaStopFlag = RFC2544_RESET;
    UINT4               u4SetCount = RFC2544_ZERO;
    UINT4               u4TempTrialCount = RFC2544_ZERO;
    UINT4               u4TempTest = RFC2544_ZERO;
    UINT4               u4TempRate = RFC2544_ZERO;
    INT4                i4SubTest = RFC2544_ZERO;
    INT4                i4MaxTrialCount = RFC2544_ZERO;
    INT4                i4Count = RFC2544_ZERO;
    INT4                i4SubTestTrialCount = RFC2544_ZERO;
    INT4                i4TmrStartFlag = RFC2544_ZERO;

    pTrafficProfile = R2544UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                 pSlaEntry->u4SlaTrafProfId);

    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pSlaEntry->u4ContextId,
                      "Traffic Profile entry does not exists for Traffic"
                      " Profile ID :%d \r\n", pSlaEntry->u4SlaTrafProfId);
        return;
    }

    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC ("R2544CoreTest :Memory allocation" "failed r\n");
        return;
    }
    MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_GLOBAL_TRC ("R2544CoreTest :Memory allocatio" "failed r\n");
        return;
    }
    MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));

    /*For loop for starting each subtest */
    for (i4SubTest = RFC2544_THROUGHPUT_TEST;
         i4SubTest <= RFC2544_BACKTOBACK_TEST; i4SubTest++)
    {
        /* Parameters related to particular subtest are assigned If  
         * subtest is enabled */
        if (i4SubTest == RFC2544_THROUGHPUT_TEST)
        {
            u4Duration = pTrafficProfile->u4TrafProfThTrialDuration;

            i4MaxTrialCount = (INT4) pTrafficProfile->u4TrafProfThMaxTrial;
            pR2544ExtInParams->eExtReqType = R2544_REQ_START_THROUGHPUT_TEST;
            pR2544ExtInParams->EcfmReqParams.u4MsgType =
                ECFM_R2544_REQ_START_THROUGHPUT_TEST;
            pR2544ExtInParams->EcfmReqParams.ThroughputParam.u4TrialDuration =
                (pTrafficProfile->u4TrafProfThTrialDuration);
            pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                u1R2544SubTest = RFC2544_THROUGHPUT_TEST;
        }
        else if (i4SubTest == RFC2544_LATENCY_TEST)
        {

            u4Duration = pTrafficProfile->u4TrafProfLaTrialDuration;
            i4MaxTrialCount = RFC2544_LATENCY_MAX_TRIAL;
            pR2544ExtInParams->eExtReqType = R2544_REQ_START_LATENCY_TEST;
            pR2544ExtInParams->EcfmReqParams.u4MsgType =
                ECFM_R2544_REQ_START_LATENCY_TEST;
            pR2544ExtInParams->EcfmReqParams.LatencyParam.u4DelayInterval =
                pTrafficProfile->u4TrafProfLaDelayMeasureInterval;
            pR2544ExtInParams->EcfmReqParams.LatencyParam.u4TrialDuration =
                (pTrafficProfile->u4TrafProfLaTrialDuration);
            pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                u1R2544SubTest = RFC2544_LATENCY_TEST;

        }
        else if (i4SubTest == RFC2544_FRAMELOSS_TEST)
        {

            u4Duration = pTrafficProfile->u4TrafProfFlTrialDuration;
            i4MaxTrialCount = (INT4) pTrafficProfile->u4TrafProfFlMaxTrial;

            pR2544ExtInParams->eExtReqType = R2544_REQ_START_FRAMELOSS_TEST;
            pR2544ExtInParams->EcfmReqParams.u4MsgType =
                ECFM_R2544_REQ_START_FRAMELOSS_TEST;
            pR2544ExtInParams->EcfmReqParams.FrameLossParam.u4TrialDuration =
                (pTrafficProfile->u4TrafProfFlTrialDuration);
            pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                u1R2544SubTest = RFC2544_FRAMELOSS_TEST;
        }
        else if (i4SubTest == RFC2544_BACKTOBACK_TEST)
        {

            u4Duration =
                pTrafficProfile->u4TrafProfBbTrialDuration / RFC2544_THOUSAND;
            i4MaxTrialCount = (INT4) pTrafficProfile->u4TrafProfBbTrialCount;
            pR2544ExtInParams->eExtReqType = R2544_REQ_START_BACKTOBACK_TEST;
            pR2544ExtInParams->EcfmReqParams.u4MsgType =
                ECFM_R2544_REQ_START_BACKTOBACK_TEST;
            pR2544ExtInParams->EcfmReqParams.BackToBackParam.u4TrialDuration =
                (pTrafficProfile->u4TrafProfBbTrialDuration / RFC2544_THOUSAND);
            pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                u1R2544SubTest = RFC2544_BACKTOBACK_TEST;

        }
        /*For loop to perform each subtest for each framesize */
        for (i4Count = 0; i4Count < RFC2544_MAX_FRAMESIZE; i4Count++)
        {
            /*Loop break after performing the subtest to all the framesizes */
            if (pTrafficProfile->au4TrafProfFrameSize[i4Count][RFC2544_ZERO] ==
                RFC2544_ZERO)
            {
                break;
            }

            /*Check particular subtest is set or reset */
            if (pTrafficProfile->au4TrafProfFrameSize[i4Count][i4SubTest] ==
                RFC2544_SET)
            {

                /* For loop to perform number of trials for particular  
                 * framesize  */
                for (i4SubTestTrialCount = 0;
                     i4SubTestTrialCount < i4MaxTrialCount;
                     i4SubTestTrialCount++)
                {

                    if (i4SubTest == RFC2544_THROUGHPUT_TEST)
                    {
                        /* Throughput Current rate Calculation 
                         * Current Rate = Current Rate * ((100 - RateStep)/ 100) 
                         */
                        if (pTrafficProfile->u4TrafProfThCurrTrial ==
                            RFC2544_ONE)
                        {
                            pTrafficProfile->u4TrafProfThCurrRate =
                                pTrafficProfile->u4TrafProfThMaxRate;
                        }
                        else
                        {
                            pTrafficProfile->u4TrafProfThCurrRate =
                                ((pTrafficProfile->u4TrafProfThMaxRate) -
                                 ((pTrafficProfile->u4TrafProfThCurrTrial -
                                   RFC2544_ONE) *
                                  pTrafficProfile->u4TrafProfThRateStep));
                        }
                        pR2544ExtInParams->EcfmReqParams.ThroughputParam.
                            u4Rate =
                            (pTrafficProfile->u4TrafProfThCurrRate *
                             (pSlaEntry->u4PortSpeed / RFC2544_HUNDRED));
                        pR2544ExtInParams->EcfmReqParams.ThroughputParam.
                            u4TrialCount =
                            pTrafficProfile->u4TrafProfThCurrTrial;
                        u4TempTrialCount = pR2544ExtInParams->EcfmReqParams.ThroughputParam.u4TrialCount;    /* For debug traces */
                        u4TempRate = pTrafficProfile->u4TrafProfThCurrRate;
                    }
                    else if (i4SubTest == RFC2544_LATENCY_TEST)
                    {

                        nmhGetFs2544ReportStatsThVerifiedBps (pSlaEntry->
                                                              u4ContextId,
                                                              pSlaEntry->
                                                              u4SlaId,
                                                              pTrafficProfile->
                                                              au4TrafProfFrameSize
                                                              [i4Count]
                                                              [RFC2544_ZERO],
                                                              &pTrafficProfile->
                                                              u4TrafProfLaCurrRate);
                        pR2544ExtInParams->EcfmReqParams.LatencyParam.
                            u4TrialCount =
                            pTrafficProfile->u4TrafProfLaCurrTrial;
                        pR2544ExtInParams->EcfmReqParams.LatencyParam.u4Rate =
                            pTrafficProfile->u4TrafProfLaCurrRate *
                            RFC2544_THOUSAND;
                        if (pTrafficProfile->u4TrafProfLaCurrRate ==
                            RFC2544_ZERO)
                        {
                            pTrafficProfile->
                                au1LatencyCount[i4SubTestTrialCount] =
                                RFC2544_RESET;
                        }

                        u4TempTrialCount =
                            pTrafficProfile->u4TrafProfLaCurrTrial;
                        /* For debug traces */
                        u4TempRate = RFC2544_HUNDRED;

                    }
                    else if (i4SubTest == RFC2544_FRAMELOSS_TEST)
                    {
                        if ((pTrafficProfile->u4TrafProfFlCurrTrial ==
                             RFC2544_ONE))
                        {
                            pTrafficProfile->u4TrafProfFlCurrRate =
                                pTrafficProfile->u4TrafProfFlMaxRate;
                        }
                        else
                        {
                            pTrafficProfile->u4TrafProfFlCurrRate =
                                ((pTrafficProfile->u4TrafProfFlMaxRate) -
                                 ((pTrafficProfile->u4TrafProfFlCurrTrial -
                                   RFC2544_ONE) *
                                  pTrafficProfile->u4TrafProfFlRateStep));

                        }
                        pR2544ExtInParams->EcfmReqParams.FrameLossParam.
                            u4TrialCount =
                            pTrafficProfile->u4TrafProfFlCurrTrial;

                        u4TempTrialCount =
                            pTrafficProfile->u4TrafProfFlCurrTrial;
                        /* For debug traces */

                        pR2544ExtInParams->EcfmReqParams.FrameLossParam.u4Rate =
                            (pTrafficProfile->u4TrafProfFlCurrRate *
                             (pSlaEntry->u4PortSpeed / RFC2544_HUNDRED));
                        u4TempRate = pTrafficProfile->u4TrafProfFlCurrRate;
                    }
                    else if (i4SubTest == RFC2544_BACKTOBACK_TEST)
                    {

                        pR2544ExtInParams->EcfmReqParams.BackToBackParam.
                            u4TrialCount =
                            pTrafficProfile->u4TrafProfBbCurrTrial;
                        /* For debug traces */
                        u4TempTrialCount =
                            pTrafficProfile->u4TrafProfBbCurrTrial;

                        pR2544ExtInParams->EcfmReqParams.BackToBackParam.
                            u4Rate = pSlaEntry->u4PortSpeed;
                        u4TempRate = RFC2544_HUNDRED;

                    }

                    if (((pR2544ExtInParams->eExtReqType ==
                          R2544_REQ_START_THROUGHPUT_TEST)
                         && (pTrafficProfile->
                             au1ThroughputCount[i4SubTestTrialCount] ==
                             RFC2544_SET))
                        ||
                        ((pR2544ExtInParams->eExtReqType ==
                          R2544_REQ_START_LATENCY_TEST)
                         && (pTrafficProfile->
                             au1LatencyCount[i4SubTestTrialCount] ==
                             RFC2544_SET))
                        ||
                        ((pR2544ExtInParams->eExtReqType ==
                          R2544_REQ_START_FRAMELOSS_TEST)
                         && (pTrafficProfile->
                             au1FrameLossCount[i4SubTestTrialCount] ==
                             RFC2544_SET))
                        ||
                        ((pR2544ExtInParams->eExtReqType ==
                          R2544_REQ_START_BACKTOBACK_TEST)
                         && (pTrafficProfile->
                             au1BackToBackCount[i4SubTestTrialCount] ==
                             RFC2544_SET)))
                    {

                        pR2544ExtInParams->EcfmReqParams.u4ContextId =
                            pSlaEntry->u4ContextId;
                        pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId =
                            pSlaEntry->u4SlaId;
                        pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEGId =
                            pSlaEntry->u4SlaMeg;
                        pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEId =
                            pSlaEntry->u4SlaMe;
                        pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId =
                            pSlaEntry->u4SlaMep;

                        pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                            u2FrameSize =
                            (UINT2) pTrafficProfile->
                            au4TrafProfFrameSize[i4Count][RFC2544_ZERO];
                        pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                            u1TrafProfPCP =
                            (UINT1) pTrafficProfile->u4TrafProfPCP;
                        pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                            u4TrafProfDwellTime =
                            (UINT4) pTrafficProfile->u4TrafProfDwellTime;
                        pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
                            u1TrafProfSeqNoCheck =
                            (UINT4) pTrafficProfile->u1TrafProfSeqNoCheck;
                        pR2544ExtInParams->EcfmReqParams.u4IfIndex =
                            pSlaEntry->u4IfIndex;
                        pR2544ExtInParams->EcfmReqParams.u4TagType =
                            pSlaEntry->u4TagType;
                        pR2544ExtInParams->EcfmReqParams.u4PortSpeed =
                            pSlaEntry->u4PortSpeed;
                        pR2544ExtInParams->EcfmReqParams.OutVlanId =
                            pSlaEntry->OutVlanId;

                        RFC2544_MEMCPY (pR2544ExtInParams->EcfmReqParams.
                                        TxSrcMacAddr, pSlaEntry->TxSrcMacAddr,
                                        sizeof (tMacAddr));

                        RFC2544_MEMCPY (pR2544ExtInParams->EcfmReqParams.
                                        TxDstMacAddr, pSlaEntry->TxDstMacAddr,
                                        sizeof (tMacAddr));

                        u4TempTest =
                            pR2544ExtInParams->EcfmReqParams.
                            TrafficProfileParam.u1R2544SubTest;
                        /*Interacting with y1731 to start the test */
                        RFC2544_TRC4 (RFC2544_BENCHMARK_TEST_TRC,
                                      pSlaEntry->u4ContextId,
                                      "R2544CoreTest: %s Test Started "
                                      "for  Context Id : %d "
                                      "Sla Id : %d  Frame Size : %d \r\n",
                                      ((u4TempTest ==
                                        RFC2544_THROUGHPUT_TEST) ? "Throughput"
                                       : (u4TempTest ==
                                          RFC2544_LATENCY_TEST) ? "Latency"
                                       : (u4TempTest ==
                                          RFC2544_FRAMELOSS_TEST) ? "FrameLoss"
                                       : (u4TempTest ==
                                          RFC2544_BACKTOBACK_TEST) ?
                                       "Back-To-Back" : ""),
                                      pSlaEntry->u4ContextId,
                                      pSlaEntry->u4SlaId,
                                      pR2544ExtInParams->EcfmReqParams.
                                      TrafficProfileParam.u2FrameSize);
                        if (u4TempTest != RFC2544_LATENCY_TEST)
                        {
                            RFC2544_TRC6 (RFC2544_SESSION_RECORD_TRC,
                                          pSlaEntry->u4ContextId,
                                          "R2544CoreTest: %s Test Started "
                                          "for  Context Id : %d "
                                          "Sla Id : %d  Frame Size : %d Trial "
                                          "Count : %u Rate : %u%% of Portspeed\r\n",
                                          ((u4TempTest ==
                                            RFC2544_THROUGHPUT_TEST) ?
                                           "Throughput" : (u4TempTest ==
                                                           RFC2544_FRAMELOSS_TEST)
                                           ? "FrameLoss" : (u4TempTest ==
                                                            RFC2544_BACKTOBACK_TEST)
                                           ? "Back-To-Back" : ""),
                                          pSlaEntry->u4ContextId,
                                          pSlaEntry->u4SlaId,
                                          pR2544ExtInParams->EcfmReqParams.
                                          TrafficProfileParam.u2FrameSize,
                                          u4TempTrialCount, u4TempRate);
                        }
                        else
                        {
                            RFC2544_TRC6 (RFC2544_SESSION_RECORD_TRC,
                                          pSlaEntry->u4ContextId,
                                          "R2544CoreTest: %s Test Started "
                                          "for  Context Id : %d "
                                          "Sla Id : %d  Frame Size : %d Trial "
                                          "Count : %u Rate : %u%% of Throughput Rate\r\n",
                                          "Latency", pSlaEntry->u4ContextId,
                                          pSlaEntry->u4SlaId,
                                          pR2544ExtInParams->EcfmReqParams.
                                          TrafficProfileParam.u2FrameSize,
                                          u4TempTrialCount, u4TempRate);

                        }
                        if (R2544PortHandleExtInteraction
                            (pR2544ExtInParams,
                             pR2544ExtOutParams) == OSIX_FAILURE)
                        {
                            MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                                                (UINT1 *) pR2544ExtInParams);
                            MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                                                (UINT1 *) pR2544ExtOutParams);
                            return;
                        }
                        if (pR2544ExtInParams->eExtReqType ==
                            R2544_REQ_START_THROUGHPUT_TEST)
                        {
                            pTrafficProfile->
                                au1ThroughputCount[i4SubTestTrialCount] =
                                RFC2544_RESET;
                            pTrafficProfile->u4TrafProfThCurrTrial++;
                        }
                        if (pR2544ExtInParams->eExtReqType ==
                            R2544_REQ_START_LATENCY_TEST)
                        {
                            pTrafficProfile->
                                au1LatencyCount[i4SubTestTrialCount] =
                                RFC2544_RESET;
                            pSlaEntry->u4PrevTrialCount =
                                pTrafficProfile->u4TrafProfLaCurrTrial;

                            pTrafficProfile->u4TrafProfLaCurrTrial++;
                        }
                        if (pR2544ExtInParams->eExtReqType ==
                            R2544_REQ_START_FRAMELOSS_TEST)
                        {
                            pTrafficProfile->
                                au1FrameLossCount[i4SubTestTrialCount] =
                                RFC2544_RESET;
                            pTrafficProfile->u4TrafProfFlCurrTrial++;
                        }
                        if (pR2544ExtInParams->eExtReqType ==
                            R2544_REQ_START_BACKTOBACK_TEST)
                        {
                            pTrafficProfile->
                                au1BackToBackCount[i4SubTestTrialCount] =
                                RFC2544_RESET;
                            pTrafficProfile->u4TrafProfBbCurrTrial++;
                        }
                        /* Update Data Structure to fetch the previous results */

                        R2544TmrStopTimer (pSlaEntry, RFC2544_SLA_TIMER);
                        R2544TmrStartTimer (pSlaEntry, RFC2544_SLA_TIMER,
                                            u4Duration);
                        i4TmrStartFlag = RFC2544_SET;

                        pSlaEntry->u4PrevFrameSize =
                            pTrafficProfile->
                            au4TrafProfFrameSize[i4Count][RFC2544_ZERO];
                        if (i4SubTest != RFC2544_LATENCY_TEST)
                        {

                            pSlaEntry->u4PrevTrialCount =
                                (UINT4) i4SubTestTrialCount;

                        }
                        pSlaEntry->u1PrevSubTest = (UINT1) i4SubTest;
                        pSlaEntry->u4PrevRate =
                            (pTrafficProfile->u4TrafProfThCurrRate *
                             (pSlaEntry->u4PortSpeed / RFC2544_HUNDRED));

                        /*Resetting the subtest flags */
                    }
                    if (i4TmrStartFlag == RFC2544_SET)
                    {
                        break;
                    }

                }
                if (i4SubTestTrialCount == (i4MaxTrialCount))
                {

                    pTrafficProfile->au4TrafProfFrameSize[i4Count][i4SubTest] =
                        RFC2544_RESET;
                    if (i4SubTest == RFC2544_THROUGHPUT_TEST)
                    {
                        pTrafficProfile->u4TrafProfThCurrRate =
                            pTrafficProfile->u4TrafProfThMaxRate;
                        for (u4SetCount = 0;
                             u4SetCount < pTrafficProfile->u4TrafProfThMaxTrial;
                             u4SetCount++)
                        {
                            pTrafficProfile->au1ThroughputCount[u4SetCount] =
                                RFC2544_SET;
                        }
                        pTrafficProfile->u4TrafProfThCurrTrial = RFC2544_ONE;
                    }
                    else if (i4SubTest == RFC2544_LATENCY_TEST)
                    {
                        for (u4SetCount = 0;
                             u4SetCount < RFC2544_LATENCY_MAX_TRIAL;
                             u4SetCount++)
                        {
                            pTrafficProfile->au1LatencyCount[u4SetCount] =
                                RFC2544_SET;
                        }
                    }
                    else if (i4SubTest == RFC2544_FRAMELOSS_TEST)
                    {
                        pTrafficProfile->u4TrafProfFlCurrRate =
                            pTrafficProfile->u4TrafProfFlMaxRate;
                        for (u4SetCount = 0;
                             u4SetCount < pTrafficProfile->u4TrafProfFlMaxTrial;
                             u4SetCount++)
                        {
                            pTrafficProfile->au1FrameLossCount[u4SetCount] =
                                RFC2544_SET;
                        }
                        pTrafficProfile->u4TrafProfFlCurrTrial = RFC2544_ONE;
                    }
                    else if (i4SubTest == RFC2544_BACKTOBACK_TEST)
                    {
                        for (u4SetCount = 0;
                             u4SetCount <
                             pTrafficProfile->u4TrafProfBbTrialCount;
                             u4SetCount++)
                        {
                            pTrafficProfile->au1BackToBackCount[u4SetCount] =
                                RFC2544_SET;
                        }
                        pTrafficProfile->u4TrafProfBbCurrTrial = RFC2544_ONE;
                    }

                }
                if (i4TmrStartFlag == RFC2544_SET)
                {
                    break;
                }
            }
            else
            {
                continue;
            }
        }
        if (i4TmrStartFlag == RFC2544_SET)
        {
            break;
        }
        else
        {
            continue;
        }
    }

    for (i4Count = 0; i4Count < RFC2544_MAX_FRAMESIZE; i4Count++)
    {
        if (pTrafficProfile->au4TrafProfFrameSize[i4Count][RFC2544_ZERO] ==
            RFC2544_ZERO)
        {
            break;
        }
        for (i4SubTest = RFC2544_THROUGHPUT_TEST;
             i4SubTest <= RFC2544_BACKTOBACK_TEST; i4SubTest++)
        {
            if (pTrafficProfile->au4TrafProfFrameSize[i4Count][i4SubTest] ==
                RFC2544_ONE)
            {
                u4SlaStopFlag = RFC2544_SET;
            }
        }

    }

    if (u4SlaStopFlag == RFC2544_RESET)
    {
        R2544CoreStopSlaTest (pSlaEntry);
        pSlaEntry->u1SlaCurrentTestState = RFC2544_COMPLETED;
    }

    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);
    UNUSED_PARAM (u4TempRate);
    UNUSED_PARAM (u4TempTrialCount);
    return;
}

/****************************************************************************
*                                                                           *
*     FUNCTION NAME    : R2544CoreReportRequest                             *
*                                                                           *
*     DESCRIPTION      : This function will send request to ecfm for Report *
*                        statistics                                         *
*                                                                           *
*     INPUTS           : pArg - Pointer to ring node.                       *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                    *
*                                                                           *
*****************************************************************************/

VOID
R2544CoreReportRequest (VOID *pArg)
{
    /*Start the through  test for next frmae size */
    tR2544ExtInParams  *pR2544ExtInParams = NULL;
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;
    tSlaEntry          *pSlaEntry = NULL;
    tTrafficProfile    *pTrafficProfile = NULL;

    pSlaEntry = (tSlaEntry *) pArg;

    pTrafficProfile = R2544UtilGetTrafProfEntry (pSlaEntry->u4ContextId,
                                                 pSlaEntry->u4SlaTrafProfId);

    if (pTrafficProfile == NULL)
    {
        RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pSlaEntry->u4ContextId,
                      "Traffic Profile entry does not exists for Traffic"
                      " Profile ID :%d \r\n", pSlaEntry->u4SlaTrafProfId);
        return;
    }
    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC ("R2544CoreTest :Memory allocation" "failed r\n");
        return;
    }
    MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_GLOBAL_TRC ("R2544CoreTest :Memory allocatio" "failed r\n");
        return;
    }
    MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));

    pR2544ExtInParams->EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMeg;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMe;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMep;

    pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u2FrameSize =
        (UINT2) pSlaEntry->u4PrevFrameSize;
    pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u4TrialCount =
        pSlaEntry->u4PrevTrialCount;

    pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1R2544SubTest =
        pSlaEntry->u1PrevSubTest;
    pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u4Rate =
        pSlaEntry->u4PrevRate;
    pR2544ExtInParams->EcfmReqParams.u4MsgType = R2544_REQ_SLA_TEST_RESULT;
    pR2544ExtInParams->EcfmReqParams.u2EvcId = pSlaEntry->OutVlanId;
    pR2544ExtInParams->EcfmReqParams.OutVlanId = pSlaEntry->OutVlanId;
    pR2544ExtInParams->EcfmReqParams.u4IfIndex = pSlaEntry->u4IfIndex;
    pR2544ExtInParams->eExtReqType = R2544_REQ_SLA_TEST_RESULT;
    pR2544ExtInParams->EcfmReqParams.u4MsgType = ECFM_R2544_REQ_SLA_TEST_RESULT;

    if (R2544PortHandleExtInteraction (pR2544ExtInParams, pR2544ExtOutParams) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                            (UINT1 *) pR2544ExtOutParams);
        return;
    }

    if ((pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1R2544SubTest !=
         RFC2544_THROUGHPUT_TEST)
        && (pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.
            u1R2544SubTest != RFC2544_FRAMELOSS_TEST))
    {
        R2544CoreTest (pSlaEntry);
    }

    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);

    return;
}
