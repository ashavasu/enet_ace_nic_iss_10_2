/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved]
 *
 * $Id: r2544cxt.c,v 1.9 2016/05/04 12:21:01 siva Exp $
 *
 * Description: This file contains RFC2544 context related functions
 *                       
 *******************************************************************/
#include "r2544inc.h"
/****************************************************************************
 * FUNCTION NAME    : R2544CxtCreateContext                                 *
 *                                                                          *
 * DESCRIPTION      : This function Creates the context in the R2544 module.*
 *                                                                          *
 * INPUT            : None                                                  *
 *                                                                          *
 * OUTPUT           : None                                                  *
 *                                                                          *
 * RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                       *
 *                                                                          *
 ***************************************************************************/
PUBLIC INT4
R2544CxtCreateContext (UINT4 u4ContextId)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    if ((pContextInfo = (tR2544ContextInfo *)
         MemAllocMemBlk (RFC2544_CONTEXT_POOL ())) == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC (("R2544CxtCreateContext:"
                             "Memory allocation for Contex failed!!!\r\n"));
        return RFC2544_FAILURE;
    }

    MEMSET (pContextInfo, 0, sizeof (tR2544ContextInfo));

    gR2544GlobalInfo.apContextInfo[u4ContextId] = pContextInfo;

    pContextInfo->u4ContextId = u4ContextId;
    VcmGetAliasName (u4ContextId, pContextInfo->au1ContextName);
    pContextInfo->u1ModuleStatus = RFC2544_DISABLED;
    pContextInfo->u4TraceOption = RFC2544_CRITICAL_TRC;
    pContextInfo->u1TrapStatus = RFC2544_DISABLED;
    return RFC2544_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : R2544CxtDeleteContext                                *
 *                                                                         *
 * DESCRIPTION      : This function deletes the context from the RFC2544   *
 *                     module.                                             *
 *                                                                         *
 * INPUT            : None                                                 *
 *                                                                         *
 * OUTPUT           : None                                                 *
 *                                                                         *
 * RETURNS          : RFC2544_SUCCESS/RFC2544_FAILURE                      *
 *                                                                         *
 **************************************************************************/
PUBLIC INT4
R2544CxtDeleteContext (UINT4 u4ContextId)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC (("R2544CxtDeleteContext Context %d does "
                             "not exist\r\n"));
        return RFC2544_FAILURE;
    }

    /* If RFC2544 is started, then first shutdown the RFC2544 in that 
     * context */
    if (gau1R2544SystemControl[u4ContextId] == RFC2544_START)
    {
        if (R2544CxtModuleShutdown (u4ContextId) == RFC2544_FAILURE)
        {
            RFC2544_GLOBAL_TRC ("R2544CxtDeleteContext: "
                                "R2544CxtDeleteContext Failed \r\n");
            return RFC2544_SUCCESS;
        }
    }

    MemReleaseMemBlock (RFC2544_CONTEXT_POOL (), (UINT1 *) pContextInfo);

    gau1R2544SystemControl[u4ContextId] = RFC2544_SHUTDOWN;

    gR2544GlobalInfo.apContextInfo[u4ContextId] = NULL;

    return RFC2544_SUCCESS;
}

/****************************************************************************
*                                                                           *
*     FUNCTION NAME    : R2544CxtIsContextExist                             *
*                                                                           *
*     DESCRIPTION      : This function will return whether the given        *
*                        context exist or not.                              *
*                                                                           *
*     INPUT            : u4ContextId - Context-Id                           *
*                                                                           *
*     OUTPUT           : None                                               *
*                                                                           *
*     RETURNS          : R2544_TRUE/R2544_FALSE                             *
*                                                                           *
*****************************************************************************/
PUBLIC              BOOL1
R2544CxtIsContextExist (UINT4 u4ContextId)
{
    if (u4ContextId >= RFC2544_MAX_CONTEXTS)
    {
        return RFC2544_FALSE;
    }
    if (R2544_GET_CONTEXT_INFO (u4ContextId) != NULL)
    {
        return RFC2544_TRUE;
    }
    return RFC2544_FALSE;
}

/****************************************************************************
*                                                                           *
*    Function Name       : R2544CxtGetContextEntry                          *
*                                                                           *
*    Description         : This function used to get the pointer to the     *
*                          context info entry.                              *
*                                                                           *
*    Input(s)            : u4ContextId - Context Index                      *
*                                                                           *
*    Output(s)           : None.                                            *
*                                                                           *
*    Returns             : pointer to tR2544ContextInfo                     *
*                                                                           *
*****************************************************************************/
PUBLIC tR2544ContextInfo *
R2544CxtGetContextEntry (UINT4 u4ContextId)
{
    if (u4ContextId >= RFC2544_MAX_CONTEXTS)
    {
        return NULL;
    }
    return gR2544GlobalInfo.apContextInfo[u4ContextId];
}

/*******************************************************************************
 * FUNCTION NAME    : R2544CxtValidateContextInfo                              *
 *                                                                             *
 * DESCRIPTION      : This function will check whether the context is created  *
 *                    and also the system control of the context.              *
 *                                                                             *
 *                    This function will be called by the nmhGet/nmhSet        *
 *                    routines and this function will take care of filling     *
 *                    the SNMP error codes, CLI set error and trace messages.  *
 *                                                                             *
 * INPUT            : u4ContextId - Context Identifier                         *
 *                                                                             *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.                *
 *                                                                             *
 * RETURNS          : Pointer to the tR2544ContextInfo structure.              *
 *                                                                             *
 ******************************************************************************/
PUBLIC tR2544ContextInfo *
R2544CxtValidateContextInfo (UINT4 u4ContextId, UINT4 *pu4ErrorCode)
{
    tR2544ContextInfo  *pContextInfo = NULL;

    pContextInfo = R2544CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {
        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RFC2544_CONTEXT_NOT_PRESENT);
        return NULL;
    }
    if (R2544CxtIsR2544Started (u4ContextId) == RFC2544_FALSE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_INIT_SHUT_TRC,
                      u4ContextId,
                     "RFC2544 Module is shutdown in Context %d\r\n",
                     u4ContextId);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RFC2544_ERR_MODULE_SHUTDOWN);
        return NULL;
    }
    return pContextInfo;
}

/****************************************************************************
 * FUNCTION NAME    : R2544CxtIsR2544Started                                *
 *                                                                          *
 * DESCRIPTION      : This function will check whether R2544 is started in  *
 *                    context. If started, this function will return        *
 *                    RFC2544_TRUE,  RFC2544_FALSE otherwise.               *
 *                                                                          *
 * INPUT            : u4ContextId - Context Identifier                      *
 *                                                                          *
 * OUTPUT           : NONE                                                  *
 *                                                                          *
 * RETURNS          : RFC2544_TRUE/RFC2544_FALSE                            *
 *                                                                          *
 ***************************************************************************/
PUBLIC INT4                                                                 
R2544CxtIsR2544Started (UINT4 u4ContextId)                                  
{                                                                           
    if (u4ContextId >= RFC2544_MAX_CONTEXTS)
    {
        return RFC2544_FALSE;
    }
    if (gR2544GlobalInfo.apContextInfo[u4ContextId] == NULL)
    {
        /* RFC2544 is not initialized in this context */
        return RFC2544_FALSE;
    }
    return ((gau1R2544SystemControl[u4ContextId]
             == RFC2544_START) ? RFC2544_TRUE : RFC2544_FALSE);
}

/*****************************************************************************
 * FUNCTION NAME    : R2544CxtModuleStart                                    *
 *                                                                           *
 * DESCRIPTION      : This function will check whether the context is created*
 *                    and also the system control of the context.            *
 *                                                                           *
 * INPUT            : u4ContextId - Context Identifier                       *
 *                                                                           *
 * OUTPUT           :                                                        *
 *                                                                           *
 * RETURNS          : RFC2544_TRUE/RFC2544_FALSE                             *
 *                                                                           *
 ****************************************************************************/
PUBLIC INT4                                                                  
R2544CxtModuleStart (UINT4 u4ContextId)                                      
{                                                                            
    if (R2544CxtCreateContext (u4ContextId) == RFC2544_FAILURE)              
    {
        RFC2544_GLOBAL_TRC ("R2544MainModuleStart:"
                            "Report Stats Tablecreation failed\r\n");

        return RFC2544_FAILURE;
    }

    gau1R2544SystemControl[u4ContextId] = RFC2544_START;
    return RFC2544_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME    : R2544CxtModuleShutdown                                 *
 *                                                                           *
 * DESCRIPTION      : This function will check whether the context is created*
 *                    and also the system control of the context.            *
 *                                                                           *
 * INPUT            : u4ContextId - Context Identifier                       *
 *                                                                           *
 * OUTPUT           :                                                        *
 *                                                                           *
 * RETURNS          : RFC2544_TRUE/RFC2544_FALSE                             *
 *                                                                           *
 ****************************************************************************/
PUBLIC INT4                                                                  
R2544CxtModuleShutdown (UINT4 u4ContextId)                                   
{                                                                            
    tR2544ContextInfo  *pContextInfo = NULL;                                 

    pContextInfo = R2544CxtGetContextEntry (u4ContextId);

    if (pContextInfo == NULL)
    {

        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");
        CLI_SET_ERR (CLI_RFC2544_CONTEXT_NOT_PRESENT);
        return RFC2544_FAILURE;
    }

    if (R2544CxtIsR2544Started (u4ContextId) == RFC2544_TRUE)
    {
        if (R2544CxtModuleDisable (pContextInfo) == RFC2544_FAILURE)
        {
            RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC | RFC2544_ALL_FAILURE_TRC
                          , u4ContextId,
                          "Module Shutdown failed for Context %d", u4ContextId);
            return RFC2544_FAILURE;

        }
        pContextInfo->u1ModuleStatus = RFC2544_DISABLED;
        pContextInfo->u4TraceOption = RFC2544_CRITICAL_TRC;
        pContextInfo->u1TrapStatus = RFC2544_DISABLED;
        R2544DelAllFrameLossStatsEntries (u4ContextId);
        R2544DelAllLatencyStatsEntries (u4ContextId);
        R2544DelAllBackToBackStatsEntries (u4ContextId);
        R2544DelAllThroughputStatsEntries (u4ContextId);
        R2544DelAllReportStatsEntries (u4ContextId);
        R2544UtilUnmapTrafSacFromSla (u4ContextId);
        R2544DelAllSacEntries (u4ContextId);
        R2544DelAllTrafficProfileEntries (u4ContextId);
        R2544DelAllSlaEntries (u4ContextId);
        /*Notify  Protocol */

    }

    return RFC2544_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : R2544CxtModuleEnable                                    *
 *                                                                            *
 * DESCRIPTION      : This function will check whether the context is created *
 *                    and also the system control of the context.             *
 *                                                                            *
 * INPUT            : u4ContextId - Context Identifier                        *
 *                                                                            *
 * OUTPUT           :                                                         *
 *                                                                            *
 * RETURNS          : RFC2544_TRUE/RFC2544_FALSE                              *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4                                                                   
R2544CxtModuleEnable (tR2544ContextInfo * pContextInfo)                       
{                                                                             
                                                                              
    if (pContextInfo == NULL)
    {

        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");

        CLI_SET_ERR (CLI_RFC2544_CONTEXT_NOT_PRESENT);
        return RFC2544_FAILURE;
    }

    if (R2544CxtIsR2544Started (pContextInfo->u4ContextId) == RFC2544_FALSE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_INIT_SHUT_TRC, 
                      pContextInfo->u4ContextId,
                      "RFC2544 Module is shutdown in context %d\r\n",
                      pContextInfo->u4ContextId);
        CLI_SET_ERR (CLI_RFC2544_ERR_MODULE_SHUTDOWN);
        return RFC2544_FAILURE;
    }
    pContextInfo->u1ModuleStatus = RFC2544_ENABLED;
    /*TODO module enable -make sla's active */

    return RFC2544_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : R2544CxtModuleDisable                                   *
 *                                                                            *
 * DESCRIPTION      : This function will check whether the context is created *
 *                    and also the system control of the context.             *
 *                                                                            *
 * INPUT            : u4ContextId - Context Identifier                        *
 *                                                                            *
 * OUTPUT           :                                                         *
 *                                                                            *
 * RETURNS          : RFC2544_TRUE/RFC2544_FALSE                              *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4                                                                   
R2544CxtModuleDisable (tR2544ContextInfo * pContextInfo)                      
{                                                                             
                                                                              
    tSlaEntry          *pSlaEntry = NULL;
    tSlaEntry          *pNextSlaEntry = NULL;

    if (pContextInfo == NULL)
    {

        RFC2544_GLOBAL_TRC ("Context does not exist\r\n");
        CLI_SET_ERR (CLI_RFC2544_CONTEXT_NOT_PRESENT);
        return RFC2544_FAILURE;
    }

    if (R2544CxtIsR2544Started (pContextInfo->u4ContextId) == RFC2544_FALSE)
    {
        RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_INIT_SHUT_TRC, 
                      pContextInfo->u4ContextId,
                      "RFC2544 Module is shutdown in context %d\r\n",
                      pContextInfo->u4ContextId);
        CLI_SET_ERR (CLI_RFC2544_ERR_MODULE_SHUTDOWN);
        return RFC2544_FAILURE;
    }
    pContextInfo->u1ModuleStatus = RFC2544_DISABLED;
    /* Stop all running test  */

    pSlaEntry = (tSlaEntry *) RBTreeGetFirst (RFC2544_SLA_TABLE ());

    while (pSlaEntry != NULL)
    {
        /* Delete all the nodes */
        pNextSlaEntry = (tSlaEntry *) RBTreeGetNext (RFC2544_SLA_TABLE (),
                                                     (tRBElem *) pSlaEntry,
                                                     NULL);
        if (pSlaEntry->u4ContextId == pContextInfo->u4ContextId)
        {
            /*pSlaEntry->u1SlaCurrentTestState = RFC2544_ABORTED; */
            if (R2544CoreStopSlaTest (pSlaEntry) != RFC2544_SUCCESS)
            {
                RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pContextInfo->u4ContextId,
                              "nmhSetFs2544SlaTestStatus: "
                              "Test Stop failed for Sla %d\r\n",
                              pSlaEntry->u4SlaId);
            }
            RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pContextInfo->u4ContextId,
                          "nmhSetFs2544SlaTestStatus: "
                          "Test Stop Succedded for Sla %d\r\n",
                          pSlaEntry->u4SlaId);

            if ((pSlaEntry->u4SlaMeg != RFC2544_ZERO) &&
                (pSlaEntry->u4SlaMe != RFC2544_ZERO) &&
                (pSlaEntry->u4SlaMep != RFC2544_ZERO))
            {
                if (R2544PortMepDeRegister (pSlaEntry) != OSIX_SUCCESS)
                {
                    RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pContextInfo->u4ContextId,
                                  "Mep De-Register failed for Sla %d\r\n",
                                  pSlaEntry->u4SlaId);
                    return RFC2544_FAILURE;
                }
                RFC2544_TRC1 (RFC2544_INIT_SHUT_TRC, pContextInfo->u4ContextId,
                              "Mep De-Register Succedded for Sla %d\r\n",
                              pSlaEntry->u4SlaId);
            }
        }
        pSlaEntry = pNextSlaEntry;
    }

    return RFC2544_SUCCESS;
}

/******************************************************************************
 * FUNCTION NAME    : R2544VcmIsSwitchExist                                   *
 *                                                                            *
 * DESCRIPTION      : Routine used to get if the context exists for the input *
 *                    Switch Alias name Information                           *
 *                                                                            *
 * INPUT            : pu1Alias - Context Name                                 *
 *                                                                            *
 * OUTPUT           : pu4ContextId - Context Identifier                       *
 *                                                                            *
 * RETURNS          : RFC2544_TRUE / RFC2544_FALSE                            *
 *                                                                            *
 *****************************************************************************/
PUBLIC INT4
R2544CxtVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId)
{
    tR2544ExtInParams  *pR2544ExtInParams = NULL;
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;
    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC ("Memory allocation" "failed \r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_GLOBAL_TRC ("Memory allocation failed \r\n");
        return CLI_FAILURE;
    }
    MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));
    MEMSET (pR2544ExtInParams->au1Alias, RFC2544_ZERO, STRLEN (pu1Alias));

    pR2544ExtInParams->eExtReqType = R2544_REQ_VCM_IS_CONTEXT_VALID;
    MEMCPY (pR2544ExtInParams->au1Alias, pu1Alias, STRLEN (pu1Alias));

    if (R2544PortHandleExtInteraction (pR2544ExtInParams, pR2544ExtOutParams) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                            (UINT1 *) pR2544ExtOutParams);
        return RFC2544_FAILURE;
    }
    *pu4ContextId = pR2544ExtOutParams->u4ContextId;
    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);

    return RFC2544_SUCCESS;

}
