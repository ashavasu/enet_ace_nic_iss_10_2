/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544sz.c,v 1.1.1.1 2015/10/16 07:19:05 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _RFC2544SZ_C
#include "r2544inc.h"
extern INT4  IssSzRegisterModuleSizingParams( CHR1 *pu1ModName, tFsModSizingParams *pModSizingParams); 
extern INT4  IssSzRegisterModulePoolId( CHR1 *pu1ModName, tMemPoolId *pModPoolId); 
INT4  Rfc2544SizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < RFC2544_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = (INT4)MemCreateMemPool( 
                          FsRFC2544SizingParams[i4SizingId].u4StructSize,
                          FsRFC2544SizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(RFC2544MemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            Rfc2544SizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   Rfc2544SzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsRFC2544SizingParams); 
      IssSzRegisterModulePoolId( pu1ModName, RFC2544MemPoolIds); 
      return OSIX_SUCCESS; 
}


VOID  Rfc2544SizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < RFC2544_MAX_SIZING_ID; i4SizingId++) {
        if(RFC2544MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( RFC2544MemPoolIds[ i4SizingId] );
            RFC2544MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
