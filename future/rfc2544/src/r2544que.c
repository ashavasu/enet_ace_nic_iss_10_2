/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: r2544que.c,v 1.6 2016/05/04 12:21:01 siva Exp $
 *
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/

#include "r2544inc.h"

/***************************************************************************
* FUNCTION NAME    : R2544QueEnqMsg                                        *
*                                                                          *
* DESCRIPTION      : Function is used to post queue message to R2544 task. *
*                                                                          *
* INPUT            : pMsg - Pointer to the message to be posted.           *
*                                                                          *
* OUTPUT           : NONE                                                  *
*                                                                          *
* RETURNS          : OSIX_SUCCESS / OSIX_FAILURE                           *
*                                                                          *
****************************************************************************/
PUBLIC INT4
R2544QueEnqMsg (tR2544ReqParams * pR2544QueMsg)
{
    /*Queue the message */
    if (OsixQueSend (RFC2544_QUEUE_ID (), (UINT1 *) &pR2544QueMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        /* Release the queue memory */
        MemReleaseMemBlock (RFC2544_QMSG_POOL (), (UINT1 *) pR2544QueMsg);        
        return OSIX_FAILURE;
    }
    /*post event to BFD Task */
    if (OsixEvtSend (RFC2544_TASK_ID(), RFC2544_QMSG_EVENT) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : R2544MsgQueueHandler                                       *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
R2544MsgQueueHandler ()
{
    tR2544ReqParams      *pR2544QueMsg = NULL;

    while (OsixQueRecv (RFC2544_QUEUE_ID(),
                        (UINT1 *) &pR2544QueMsg, OSIX_DEF_MSG_LEN,
                        0) == OSIX_SUCCESS)
    {
        switch (pR2544QueMsg->u4ReqType)
        {
            case R2544_CREATE_CONTEXT_MSG:
           
                if (R2544CxtCreateContext (pR2544QueMsg->u4ContextId) 
                      == RFC2544_FAILURE)
                {
                    RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, 
                            pR2544QueMsg->u4ContextId,
                        "Context creation failed for R2544MsgQueueHandler for"  
                         "context :%d \r\n",pR2544QueMsg->u4ContextId);

                }
                break;

            case R2544_DELETE_CONTEXT_MSG:

                if (R2544CxtDeleteContext (pR2544QueMsg->u4ContextId)
                      == RFC2544_FAILURE)
                {
                    RFC2544_TRC1 (RFC2544_ALL_FAILURE_TRC | RFC2544_RESOURCE_TRC, 
                            pR2544QueMsg->u4ContextId,
                            "Context Deletion failed for R2544MsgQueueHandler"
                            "for context :%d \r\n",pR2544QueMsg->u4ContextId);

                }
                break;

            case R2544_THROUGHPUT_TEST_RESULT:
                 if (R2544UtilAddThroughputStats(pR2544QueMsg) == 
                         RFC2544_FAILURE)
                 {
                        RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC , 
                                pR2544QueMsg->u4ContextId,
                            "R2544MsgQueueHandler: Result Generation for "
                            "throughput Test is Failed sla Id  %d Context "
                            "Id %d\r\n", pR2544QueMsg->SlaInfo.u4SlaId, 
                            pR2544QueMsg->u4ContextId);

                 } 
                break;

            case R2544_LATENCY_TEST_RESULT:
                 if (R2544UtilAddLatencyStats(pR2544QueMsg) ==  
                         RFC2544_FAILURE)
                 {
                        RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC, 
                                pR2544QueMsg->u4ContextId,
                            "R2544MsgQueueHandler: Result Generation for "
                            "Latency Test is Failed "
                            "sla Id  %d Context Id %d\r\n", 
                            pR2544QueMsg->SlaInfo.u4SlaId, 
                            pR2544QueMsg->u4ContextId);

                 } 
                break;

            case R2544_FRAMELOSS_TEST_RESULT:
                 if (R2544UtilAddFrameLossStats(pR2544QueMsg) ==  
                         RFC2544_FAILURE)
                 {
                        RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC, 
                                pR2544QueMsg->u4ContextId,
                            "R2544MsgQueueHandler: Result Generation for "
                            "frame loss Test is Failed "
                            "sla Id  %d Context Id %d\r\n", 
                            pR2544QueMsg->SlaInfo.u4SlaId, 
                            pR2544QueMsg->u4ContextId);

                 } 
                break;

            case R2544_BACKTOBACK_TEST_RESULT:
                 if (R2544UtilAddBackToBackStats(pR2544QueMsg) ==  
                         RFC2544_FAILURE)
                 {
                        RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_BENCHMARK_TEST_TRC, 
                                pR2544QueMsg->u4ContextId,
                            "R2544MsgQueueHandler: Result Generation for"
                            "BackToBack Test is Failed "
                            "sla Id  %d Context Id %d\r\n", 
                            pR2544QueMsg->SlaInfo.u4SlaId, 
                            pR2544QueMsg->u4ContextId);

                 } 
                break;
            case R2544_PERF_TEST_PARAMS:
                 if (R2544UtilUpdateTestParams(pR2544QueMsg) == RFC2544_FAILURE)
                 {
                        RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC  | RFC2544_Y1731_INTF_TRC, 
                                pR2544QueMsg->u4ContextId,
                            "R2544MsgQueueHandler: Test parameters Updation "
                            "Failed sla Id  %d Context Id %d\r\n", 
                            pR2544QueMsg->SlaInfo.u4SlaId,
                            pR2544QueMsg->u4ContextId);
                 }
                 break;   
            case ECFM_DEFECT_CONDITION_ENCOUNTERED:
            case ECFM_DEFECT_CONDITION_CLEARED:
            case ECFM_RDI_CONDITION_ENCOUNTERED:
            case ECFM_RDI_CONDITION_CLEARED:

                if (R2544UtilHandleSignalMsg (pR2544QueMsg) == RFC2544_FAILURE)
                 {
                        RFC2544_TRC2 (RFC2544_ALL_FAILURE_TRC | RFC2544_Y1731_INTF_TRC, 
                                pR2544QueMsg->u4ContextId,
                            "R2544MsgQueueHandler: Handling Signal Messages "
                            "from ECFM failed sla Id  %d Context Id %d\r\n", 
                            pR2544QueMsg->SlaInfo.u4SlaId,
                            pR2544QueMsg->u4ContextId);
                 } 

               break;

            default:
                break;
        }


        /* Release the buffer to pool */
        if ((MemReleaseMemBlock (RFC2544_QMSG_POOL (),
                                 (UINT1 *) pR2544QueMsg)) == MEM_FAILURE)
        {
            return;
        }
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  r2544que.c                      */
/*-----------------------------------------------------------------------*/
