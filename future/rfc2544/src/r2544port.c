/********************************************************************
 * Copyright (C) 2015 Aricent Inc . All Rights Reserved]
 *
 * $Id: r2544port.c,v 1.10 2016/05/04 12:21:01 siva Exp $
 *
 * Description: This file contains the External module routines used 
 *              in RFC2544
 *******************************************************************/
#ifndef __R2544PORT_C__
#define __R2544PORT_C__

#include "r2544inc.h"
#include "snmputil.h"

extern UINT4        fs2544[RFC2544_NINE];
extern UINT4        Fs2544ContextSystemControl[RFC2544_THIRTEEN];
/****************************************************************************
*                                                                           *
* Function     : R2544PortHandleExtInteraction                              *
*                                                                           *
* Description  : This function is the common exit  point for interacting    *
*                with all external module.                                  *
*                                                                           *
* Input        :                                                            *
*                pR2544ExtReqInParams - Pointer to the Request params.      *
*                                                                           *
* Output       : pR2544ExtReqOutParams - Pointer to the response params     *
*                                                                           *
* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               *
*                                                                           *
*****************************************************************************/
INT4
R2544PortHandleExtInteraction (tR2544ExtInParams * pR2544ExtInParams,
                               tR2544ExtOutParams * pR2544ExtOutParams)
{
    INT4                i4RetVal = OSIX_SUCCESS;
    tEcfmRegParams      R2544RegParams;
    tEcfmMepInfoParams  MepInfo;
    tEcfmEventNotification R2544MepFltStatus;

    RFC2544_MEMSET (&MepInfo, RFC2544_ZERO, sizeof (tEcfmMepInfoParams));
    RFC2544_MEMSET (&R2544RegParams, RFC2544_ZERO, sizeof (tEcfmRegParams));
    RFC2544_MEMSET (&R2544MepFltStatus, RFC2544_ZERO,
                    sizeof (tEcfmEventNotification));
    /* External requests handling */
    switch (pR2544ExtInParams->eExtReqType)
    {

        case R2544_REQ_VCM_GET_CONTEXT_NAME:
            /* This function is called by RFC 2544 module to fetch the context
             * name for the given context id.
             * Input:
             *      u4ContextId- Context Id
             * Output:
             *      pu1Alias - Alias Name
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE.
             * */
            i4RetVal = VcmGetAliasName (pR2544ExtInParams->u4ContextId,
                                        pR2544ExtOutParams->au1ContextName);
            
            RFC2544_TRC1 (RFC2544_MGMT_TRC, pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Fetching the context "
                          "name for Context entry %d \r\n",
                          pR2544ExtInParams->EcfmReqParams.u4ContextId);
 
            if (i4RetVal == VCM_SUCCESS)
            {
                i4RetVal = OSIX_SUCCESS;
            }
            else
            {
                i4RetVal = OSIX_FAILURE;
            }
            break;

        case R2544_REQ_VCM_IS_CONTEXT_VALID:
            /* This function is called by RFC2544 module to check whether the
             * given Context name is present or not.
             * 
             * Input:
             *      pu1Alias - Alias name
             * Output:
             *      u4ContextId - Context Identifier.
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal = VcmIsSwitchExist (pR2544ExtInParams->au1Alias,
                                         &pR2544ExtOutParams->u4ContextId);

            RFC2544_TRC1 (RFC2544_MGMT_TRC, pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Verifing the creation"
                          " for Context entry %d \r\n",
                          pR2544ExtInParams->EcfmReqParams.u4ContextId);
            if (i4RetVal == VCM_TRUE)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;

        case R2544_REQ_START_THROUGHPUT_TEST:
            /* This function is called by RFC2544 module to start the 
             * through put test  
             * Input:
             *      SlaInfo         
             *      ThroughputParams
             * Output:
             *      None
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal =
                Y1731ApiHandleExtRequest (&pR2544ExtInParams->EcfmReqParams,
                                          &pR2544ExtOutParams->EcfmRespParams);
            RFC2544_TRC3 (RFC2544_Y1731_INTF_TRC | RFC2544_BENCHMARK_TEST_TRC,
                          pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Interfaces with the "
                          "Y1731 to intiate the Throughput test for Context "
                          "ID: %d, Sla ID: %d, Frame Size: %d\r\n", 
                          pR2544ExtInParams->EcfmReqParams.u4ContextId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u2FrameSize);

            if (i4RetVal == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;

        case R2544_REQ_START_LATENCY_TEST:
            /* This function is called by RFC2544 module to start the 
             * latency test  
             * Input:
             *      SlaInfo         
             *      Latency Params
             * Output:
             *      None
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal =
                Y1731ApiHandleExtRequest (&pR2544ExtInParams->EcfmReqParams,
                                          &pR2544ExtOutParams->EcfmRespParams);
            RFC2544_TRC3 (RFC2544_Y1731_INTF_TRC | RFC2544_BENCHMARK_TEST_TRC,
                          pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Interfaces with the"
                          " Y1731 to intiate the latency test for Context "
                          "ID: %d, Sla ID: %d, Frame Size: %d\r\n",
                          pR2544ExtInParams->EcfmReqParams.u4ContextId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u2FrameSize);
            
            if (i4RetVal == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }

            break;

        case R2544_REQ_START_FRAMELOSS_TEST:
            /* This function is called by RFC2544 module to start the 
             * frame loss test  
             * Input:
             *      SlaInfo         
             *      FrameLoss Params
             * Output:
             *      None
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal =
                Y1731ApiHandleExtRequest (&pR2544ExtInParams->EcfmReqParams,
                                          &pR2544ExtOutParams->EcfmRespParams);
            RFC2544_TRC3 (RFC2544_Y1731_INTF_TRC | RFC2544_BENCHMARK_TEST_TRC,
                          pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Interfaces with the"
                          " Y1731 to intiate the frame-loss test for Context "
                          "ID: %d, Sla ID: %d, Frame Size: %d\r\n",
                          pR2544ExtInParams->EcfmReqParams.u4ContextId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u2FrameSize);
            if (i4RetVal == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }
            break;

        case R2544_REQ_START_BACKTOBACK_TEST:
            /* This function is called by RFC2544 module to start the 
             * back to back test  
             * Input:
             *      SlaInfo         
             *      BackToBack Params         
             * Output:
             *      None
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal =
                Y1731ApiHandleExtRequest (&pR2544ExtInParams->EcfmReqParams,
                                          &pR2544ExtOutParams->EcfmRespParams);
            RFC2544_TRC3 (RFC2544_Y1731_INTF_TRC,
                          pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Interfaces with the"
                          " Y1731 to intiate the back-to-back test for Context "
                          "ID: %d, Sla ID: %d, Frame Size: %d\r\n",
                          pR2544ExtInParams->EcfmReqParams.u4ContextId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u2FrameSize);
            if (i4RetVal == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }

            break;

        case R2544_REQ_STOP_BENCHMARK_TEST:
            /* This function is called by RFC2544 module to stop the 
             * benchmark test
             * Input:
             *      SlaInfo         
             * Output:
             *      None
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/
            i4RetVal =
                Y1731ApiHandleExtRequest (&pR2544ExtInParams->EcfmReqParams,
                                          &pR2544ExtOutParams->EcfmRespParams);

                RFC2544_TRC2 (RFC2544_Y1731_INTF_TRC | RFC2544_BENCHMARK_TEST_TRC |RFC2544_MGMT_TRC,
                              pR2544ExtInParams->u4ContextId,
                              "R2544PortHandleExtInteraction: Interfaces with the"
                              " Y1731 to stop the benchmark test for Context "
                              "ID: %d, Sla ID: %d \r\n",
                              pR2544ExtInParams->EcfmReqParams.u4ContextId,
                              pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId);

            if (i4RetVal == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }

            break;

        case R2544_REQ_SLA_TEST_RESULT:
            /* This function is called by RFC2544 module to fetch the results
             *  
             * Input:
             *      SlaInfo         
             * Output:
             *      None
             * Returns:
             *      OSIX_SUCCESS or OSIX_FAILURE
             **/

            i4RetVal =
                Y1731ApiHandleExtRequest (&pR2544ExtInParams->EcfmReqParams,
                                          &pR2544ExtOutParams->EcfmRespParams);
            RFC2544_TRC4 (RFC2544_Y1731_INTF_TRC | RFC2544_MGMT_TRC | RFC2544_BENCHMARK_TEST_TRC,
                          pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Interfaces with the"
                          " Y1731 to fetch the %s test results for benchmark test for Context "
                          "ID: %d, Sla ID: %d, Frame Size: %d\r\n",
                          ((pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1R2544SubTest ==
                            RFC2544_THROUGHPUT_TEST) ? "Throughput"
                            : (pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1R2544SubTest ==
                            RFC2544_LATENCY_TEST) ? "Latency"
                            : (pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1R2544SubTest ==
                            RFC2544_FRAMELOSS_TEST) ? "FrameLoss"
                            : (pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u1R2544SubTest ==
                            RFC2544_BACKTOBACK_TEST) ?
                            "Back-To-Back" : ""),
                          pR2544ExtInParams->EcfmReqParams.u4ContextId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.TrafficProfileParam.u2FrameSize);

            if (i4RetVal == OSIX_SUCCESS)
            {
                return OSIX_SUCCESS;
            }
            else
            {
                return OSIX_FAILURE;
            }

            break;
        case R2544_REQ_CFM_REG_MEP_AND_FLT_NOTIFY:
            /* This function is called to register
             * MEP information with ECFM/Y1731 to get
             * Fault notification
             * Input : MEPInfo structure
             *         SlaId
             * Output: None
             * Returns : OSIX_SUCCES / OSIX_FAILURE*/

            MEMSET (&R2544RegParams, 0, sizeof (tEcfmRegParams));

            R2544RegParams.u4ModId = ECFM_R2544_APP_ID;

            R2544RegParams.u4EventsId = (ECFM_DEFECT_CONDITION_CLEARED |
                                         ECFM_DEFECT_CONDITION_ENCOUNTERED |
                                         ECFM_RDI_CONDITION_ENCOUNTERED |
                                         ECFM_RDI_CONDITION_CLEARED);

            R2544RegParams.u4VlanInd = ECFM_INDICATION_FOR_ALL_VLANS;

            R2544RegParams.pFnRcvPkt = R2544PortExternalEventNotify;

            R2544MepFltStatus.u4ContextId = pR2544ExtInParams->EcfmReqParams.
                u4ContextId;
            R2544MepFltStatus.u4MdIndex = pR2544ExtInParams->EcfmReqParams.
                EcfmMepInfo.u4MEGId;
            R2544MepFltStatus.u4MaIndex = pR2544ExtInParams->EcfmReqParams.
                EcfmMepInfo.u4MEId;
            R2544MepFltStatus.u2MepId =
                (UINT2) pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId;
            R2544MepFltStatus.u4SlaId =
                pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId;

            i4RetVal = EcfmMepRegisterAndGetFltStatus (&R2544RegParams,
                                                       &R2544MepFltStatus);
            RFC2544_TRC3 (RFC2544_Y1731_INTF_TRC | RFC2544_MGMT_TRC,
                          pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Interfaces with the"
                          " Y1731 to register MEP information with ECFM/Y1731"
                          " to get fault notification for Context "
                          "ID: %d, Sla ID: %d, MEP ID: %d\r\n",
                          pR2544ExtInParams->EcfmReqParams.u4ContextId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId);

            i4RetVal =
                (((i4RetVal == ECFM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
            break;

        case R2544_REQ_CFM_DEREGISTER_MEP:
            /* This function is called to De-register
             * MEP information with ECFM/Y1731.
             * Input : MEPInfo structure
             *         SlaId
             * Output: None
             * Returns : OSIX_SUCCES / OSIX_FAILURE*/

            MEMSET (&R2544RegParams, 0, sizeof (tEcfmRegParams));

            R2544RegParams.u4ModId = ECFM_R2544_APP_ID;

            R2544RegParams.pAppInfo = &pR2544ExtInParams->EcfmReqParams.
                u4ContextId;

            R2544MepFltStatus.u4ContextId = pR2544ExtInParams->EcfmReqParams.
                u4ContextId;
            R2544MepFltStatus.u4MdIndex = pR2544ExtInParams->EcfmReqParams.
                EcfmMepInfo.u4MEGId;
            R2544MepFltStatus.u4MaIndex = pR2544ExtInParams->EcfmReqParams.
                EcfmMepInfo.u4MEId;
            R2544MepFltStatus.u2MepId =
                (UINT2) pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId;
            R2544MepFltStatus.u4SlaId =
                pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId;

            i4RetVal = EcfmMepDeRegister (&R2544RegParams, &R2544MepFltStatus);

            RFC2544_TRC3 (RFC2544_Y1731_INTF_TRC | RFC2544_MGMT_TRC,
                          pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Interfaces with the"
                          " Y1731 to De-register MEP information with ECFM/Y1731"
                          " for Context ID: %d, Sla ID: %d, MEP ID: %d\r\n",
                          pR2544ExtInParams->EcfmReqParams.u4ContextId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId);

            i4RetVal =
                (((i4RetVal == ECFM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
            break;

        case R2544_REQ_CFM_MEP_INDEX_VALIDATION:
            MepInfo.u4ContextId = pR2544ExtInParams->EcfmReqParams.u4ContextId;
            MepInfo.u4MdIndex = pR2544ExtInParams->EcfmReqParams.
                EcfmMepInfo.u4MEGId;
            MepInfo.u4MaIndex = pR2544ExtInParams->EcfmReqParams.
                EcfmMepInfo.u4MEId;
            MepInfo.u2MepId = (UINT2) pR2544ExtInParams->EcfmReqParams.
                EcfmMepInfo.u4MEPId;
            i4RetVal = EcfmApiValMepIndex (&MepInfo);
            RFC2544_TRC3 (RFC2544_Y1731_INTF_TRC | RFC2544_MGMT_TRC,
                          pR2544ExtInParams->u4ContextId,
                          "R2544PortHandleExtInteraction: Interfaces with the"
                          " Y1731 to MEP index validation for Context "
                          "ID: %d, Sla ID: %d, MEP ID: %d\r\n",
                          pR2544ExtInParams->EcfmReqParams.u4ContextId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId,
                          pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId);

            i4RetVal =
                (((i4RetVal == ECFM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
            break;
        default:
            i4RetVal = OSIX_FAILURE;
            break;
    }                            /* End of switch */
    return i4RetVal;
}

#ifdef SNMP_3_WANTED
/******************************************************************************
 * FUNCTION NAME    : R2544PortFmNotifyFaults                                 *
 * DESCRIPTION      : This function Sends the trap message to the Fault       *
 *                    Manager.                                                *
 *                                                                            *
 * INPUT            : pEnterpriseOid - OID                                    *
 *                    u4GenTrapType - Trap Type                               *
 *                                                                            *
 * OUTPUT           : None                                                    *
 *                                                                            *
 * RETURNS          : None                                                    *
 *                                                                            *
 ******************************************************************************/
PUBLIC VOID
R2544PortFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                         UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg,
                         UINT1 *pu1Msg, tSNMP_OCTET_STRING_TYPE * pContextName)
{

    UNUSED_PARAM (pEnterpriseOid);
    UNUSED_PARAM (pContextName);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = pu1Msg;
    FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_RFC2544;

    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        RFC2544_GLOBAL_TRC
            ("R2544PortFmNotifyFaults: Sending Notification to FM failed.\r\n");
        if (pTrapMsg != NULL)
        {
            SNMP_AGT_FreeVarBindList (pTrapMsg);
        }

    }
#else
    if (pTrapMsg != NULL)
    {
        SNMP_AGT_FreeVarBindList (pTrapMsg);
    }
    UNUSED_PARAM (pu1Msg);
#endif
    return;
}
#endif
/******************************************************************************
 * Function Name      : R2544PortNotifyProtocolShutStatus                     *
 *                                                                            *
 * Description        : This function sends indication to MSR on module       *
 *                      shutdown in a context                                 *
 *                                                                            *
 * Input(s)           : apu1Token      - Poiner to token array                *
 *                      pu4TraceOption - Pointer to trace option              *
 *                                                                            *
 * Output(s)          : pu4TraceOption - Pointer to trace option              *
 *                                                                            *
 * Return Value(s)    : VOID                                                  *
 ******************************************************************************/
PUBLIC VOID
R2544PortNotifyProtocolShutStatus (INT4 i4ContextId)
{

#ifdef ISS_WANTED
    UINT1               au1ObjectOid[RFC2544_TWO][SNMP_MAX_OID_LENGTH];
    UINT2               u2Objects = RFC2544_TWO;

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fs2544, (sizeof (fs2544) / sizeof (UINT4)),
                      au1ObjectOid[0]);

    SNMPGetOidString (Fs2544ContextSystemControl,
                      (sizeof (Fs2544ContextSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the rfc2544 shutdown, with 
     * rfc2544 oids and its system control object
     */
    MsrNotifyProtocolShutdown (au1ObjectOid, u2Objects, i4ContextId);
#endif
    UNUSED_PARAM (i4ContextId);
#endif
    return;
}

/*****************************************************************************
 * Function Name      : R2544PortExternalEventNotify                         *
 *                                                                           *
 * Description        : This routine populates the tY1564ReqParams           *
 *                      structure from the tEcfmEventNotification structure. *
 *                                                                           *
 * Input(s)           : pEvent - pointer to tEcfmEventNotification structure *
 *                                                                           *
 * Output(s)          : None                                                 *
 *                                                                           *
 * Return Value(s)    : None                                                 *
 *****************************************************************************/
PUBLIC VOID
R2544PortExternalEventNotify (tEcfmEventNotification * pEvent)
{
    tR2544ReqParams     R2544ReqParam;
    tR2544RespParams    RFC25444RespParam;

    RFC2544_MEMSET (&R2544ReqParam, RFC2544_ZERO, sizeof (tR2544ReqParams));
    RFC2544_MEMSET (&RFC25444RespParam, RFC2544_ZERO,
                    sizeof (tR2544RespParams));

    R2544ReqParam.u4ReqType = pEvent->u4Event;
    R2544ReqParam.u4ContextId = pEvent->u4ContextId;
    R2544ReqParam.SlaInfo.u4SlaId = pEvent->u4SlaId;
    R2544ReqParam.SlaInfo.u4MEG = pEvent->u4MdIndex;
    R2544ReqParam.SlaInfo.u4ME = pEvent->u4MaIndex;
    R2544ReqParam.SlaInfo.u4MEP = pEvent->u2MepId;

    /* Fill the result info structure here */

    if (R2544ApiHandleExtRequest (&R2544ReqParam, &RFC25444RespParam) !=
        OSIX_SUCCESS)
    {
        return;
    }
    return;
}

/******************************************************************************
 * Function Name      : R2544PortMepRegAndGetFltState                         *
 *                                                                            *
 * Description        : This function calls the CFM module to register MEP    *
 *                      information                                           *
 * Input(s)           : tSlaInfo - Sla Information structure                  *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                       *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
R2544PortMepRegAndGetFltState (tSlaEntry * pSlaEntry)
{
    tR2544ExtInParams  *pR2544ExtInParams = NULL;
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;

    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC ("Memory allocation"
                                  "failed \r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_GLOBAL_TRC ("Memory allocation failed \r\n");
        return CLI_FAILURE;
    }
    MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));

    pR2544ExtInParams->eExtReqType = R2544_REQ_CFM_REG_MEP_AND_FLT_NOTIFY;

    /* MEP Information */
    pR2544ExtInParams->EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMeg;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMe;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMep;

    if (R2544PortHandleExtInteraction (pR2544ExtInParams, pR2544ExtOutParams) ==
        OSIX_FAILURE)
    {
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                            (UINT1 *) pR2544ExtOutParams);
        return RFC2544_FAILURE;
    }

    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);

    return RFC2544_SUCCESS;
}

/******************************************************************************
 * Function Name      : R2544PortMepDeRegister                                *
 *                                                                            *
 * Description        : This function calls the CFM module to De-register MEP *
 *                      information                                           *
 * Input(s)           : tSlaInfo - Sla Information structure                  *
 *                                                                            *
 * Output(s)          : None                                                  *
 *                                                                            *
 * Return Value(s)    : RFC2544_SUCCESS/RFC2544_FAILURE                       *
 *                                                                            *
 ******************************************************************************/
PUBLIC INT4
R2544PortMepDeRegister (tSlaEntry * pSlaEntry)
{
    INT4                i4RetVal = RFC2544_SUCCESS;
    tR2544ExtInParams  *pR2544ExtInParams = NULL;
    tR2544ExtOutParams *pR2544ExtOutParams = NULL;

    pR2544ExtInParams = (tR2544ExtInParams *)
        MemAllocMemBlk (RFC2544_EXTINPARAMS_POOL ());
    if (pR2544ExtInParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        RFC2544_GLOBAL_TRC ("Memory allocation"
                                  "failed \r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtInParams, 0, sizeof (tR2544ExtInParams));

    pR2544ExtOutParams = (tR2544ExtOutParams *)
        MemAllocMemBlk (RFC2544_EXTOUTPARAMS_POOL ());
    if (pR2544ExtOutParams == NULL)
    {
        gR2544GlobalInfo.u4MemFailCount++;
        MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                            (UINT1 *) pR2544ExtInParams);
        RFC2544_GLOBAL_TRC ("Memory allocation failed \r\n");
        return RFC2544_FAILURE;
    }
    MEMSET (pR2544ExtOutParams, 0, sizeof (tR2544ExtOutParams));

    pR2544ExtInParams->eExtReqType = R2544_REQ_CFM_DEREGISTER_MEP;

    /* MEP Information */
    pR2544ExtInParams->EcfmReqParams.u4ContextId = pSlaEntry->u4ContextId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4SlaId = pSlaEntry->u4SlaId;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEGId = pSlaEntry->u4SlaMeg;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEId = pSlaEntry->u4SlaMe;
    pR2544ExtInParams->EcfmReqParams.EcfmMepInfo.u4MEPId = pSlaEntry->u4SlaMep;

    if (R2544PortHandleExtInteraction (pR2544ExtInParams, pR2544ExtOutParams) ==
        OSIX_FAILURE)
    {
        i4RetVal = RFC2544_FAILURE;
    }

    MemReleaseMemBlock (RFC2544_EXTINPARAMS_POOL (),
                        (UINT1 *) pR2544ExtInParams);
    MemReleaseMemBlock (RFC2544_EXTOUTPARAMS_POOL (),
                        (UINT1 *) pR2544ExtOutParams);

    return i4RetVal;
}

