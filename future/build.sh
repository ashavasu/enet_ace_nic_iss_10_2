#!/bin/bash
#=============================================================
# Main build script
# Parmeters:
# $1 - target (what to make) May be "clean", "exe", "iss"...
# $2 - make command-line options (if any).
#=============================================================


# ================================================= #
# Aricent' build settings #
# ================================================= #

export BASE_DIR=$(pwd) 
#export BUILD_TYPE=ISS_BIN
export BUILD_TYPE=ISS
export TARGET_ASIC=ENET_ADAPTOR
export NPAPI=YES
export ENET_DEBUG=YES
#export MEA_ENV_S1_K7=yes
#export MEA_OS_EPC_ZYNQ7045="yes"

export ISS_LIB_BUILD=YES
export ISS_LIB_BUILD_FROM_SRC=YES
export ENET_WITH_THIRD_PARTY=yes

if [[ $1 == "clean" ]] 
then
export BUILD_TYPE=ISS
elif [[ $1 != "exenic" && $1 != "clean" && $1 != "exezync" && $1 != "exebl" ]] 
then
    echo "Wrong target $1 !!! Legal targets are: \"clean\" or \"exenic\" or \"exezync\" or \"exebl\" "
    exit 1
elif [[ $1 == "exenic" ]] 
then
	export NIC_PLATFORM=yes
elif [[ $1 == "exezync" ]]
then
    export NIC_PLATFORM=no
elif [[ $1 == "exebl" ]]
then
    export BL_PLATFORM=yes
else
	echo "Illegal input. Legal targets are: \"clean\" or \"exenic\" or \"exezync\" or \"exebl\""
fi




echo "   BASE_DIR=\"$BASE_DIR\" "
echo "   BUILD_TYPE=\"$BUILD_TYPE\" "
echo "   TARGET_ASIC=\"$TARGET_ASIC\" "
echo "   NPAPI=\"$NPAPI\" "
echo "   ENET_DEBUG=\"$ENET_DEBUG\" "

echo "   ISS_LIB_BUILD=\"$ISS_LIB_BUILD\" "
echo "   ISS_LIB_BUILD_FROM_SRC=\"$ISS_LIB_BUILD_FROM_SRC\" "

# Do the build!
cd LR

if [[ $1 == "exenic" || $1 == "exezync" || $1 == "exebl" ]] 
then
	make  --no-print-directory "exe"
elif [[ $1 == "clean" ]]; then
	make  --no-print-directory $1
fi

#echo "   OBJCOPY=\"$OBJCOPY\" "
#/opt/petalinux/linux-i386/arm-xilinx-linux-gnueabi/bin/arm-xilinx-linux-gnueabi-objcopy --strip-debug --strip-unneeded $BASE_DIR/LR/ISS.exe  $BASE_DIR/LR/ISS_strip.exe
