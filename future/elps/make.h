# $Id: make.h,v 1.2 2010/10/22 14:11:26 prabuc Exp $ #
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                                  |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : ANY                                           |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)
############################################################################
#                         Directories                                      #
############################################################################

ELPS_BASE_DIR = ${BASE_DIR}/elps
ELPS_INC_DIR  = ${ELPS_BASE_DIR}/inc
ELPS_SRC_DIR  = ${ELPS_BASE_DIR}/src
ELPS_OBJ_DIR  = ${ELPS_BASE_DIR}/obj
MPLS_UTIL_DIR = ${BASE_DIR}/util/mpls

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${ELPS_INC_DIR} -I${MPLS_UTIL_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################

