/*****************************************************************************
 * Copyright (C) 2007 Aricent Inc . All Rights Reserved
 *
 * $Id: elpssem.h,v 1.8 2014/03/10 12:51:25 siva Exp $
 *
 * Description: This file contains the global variables and data structures
 * required for ELPS state event machine implementation.
 * Note: This file is best viewed in 110(cols) X 42 (rows) window. 
 *
 *****************************************************************************/
#ifndef _ELPSSEM_H_
#define _ELPSSEM_H_

/* Convention used here :
 * _1T1BR_  means 1:1 bidirectional revertive
 * _1T1BNR_ means 1:1 bidirectional non-revertive
 * _1P1BR_  means 1+1 bidirectional revertive
 * _1P1BNR_ means 1+1 bidirectional non-revertive
 */

enum
{
            /* Local Requests (_LR_) */
    /* E0 */ELPS_EV_LR_LO = 0,/* Lockout */
    /* E1 */ELPS_EV_LR_FS,/* Force Switch */
    /* E2 */ELPS_EV_LR_WRK_SF,/* SF On Working */
    /* E3 */ELPS_EV_LR_WRK_RECOV_SF,/* Working Recovers From SF */
    /* E4 */ELPS_EV_LR_PROT_SF,/* SF On Protection */
    /* E5 */ELPS_EV_LR_PROT_RECOV_SF,/* Protection Recovers From SF */
    /* E6 */ELPS_EV_LR_MS,  /* Manual Switch */
    /* E7 */ELPS_EV_LR_MS_W,/* Manual Switch to working
                               used for non-revertive mode only*/
    /* E8 */ELPS_EV_LR_CLEAR,/* Clear */
    /* E9 */ELPS_EV_LR_EXER,/* Exercise */
    /* E10 */ELPS_EV_LR_WTR_EXP,/* WTR Timer Expires
                                * used for revertive mode only */
            /* Far end Request (_FR_) */
    /* E11 */ELPS_EV_FR_LO,     /* LO [r/b=null] */
    /* E12 */ELPS_EV_FR_SF_P,   /* SF-P [r/b=null] */
    /* E13 */ELPS_EV_FR_FS,     /* FS [r/b=normal] */
    /* E14 */ELPS_EV_FR_SF,     /* SF [r/b=normal] */
    /* E15 */ELPS_EV_FR_MS,     /* MS [r/b=normal] */
    /* E16 */ELPS_EV_FR_MS_W,   /* MS [r/b=null]
                                 * used for non-revertive mode only */
    
    /* E17 */ELPS_EV_FR_WTR,    /* WTR [r/b=normal] */
    /* E18 */ELPS_EV_FR_EXER_W, /* EXER [r/b=null] */
    /* E19 */ELPS_EV_FR_EXER_P, /* EXER [r/b=normal] 
                                   used for non-revertive mode only */
    /* E20 */ELPS_EV_FR_RR_W,     /* RR [r/b=null] */
    /* E21 */ELPS_EV_FR_RR_P,     /* RR [r/b=normal] */
    /* E22 */ELPS_EV_FR_NR_W,   /* NR [r/b=null] */
    /* E23 */ELPS_EV_FR_NR_P,   /* NR [r/b=normal] */
    /* E24 */ELPS_EV_FR_DNR,    /* DNR [r/b=normal] 
                                   used for non-revertive mode only */
    ELPS_SEM_MAX_EVENTS,    /* Maximum Events */
};


/* If the Events mentioned above are changed then the global array of strings
 * for CLI needs to be updated as well. Update the value of ELPS_MAX_REQUESTS
 * also
 */

enum
{
 /* S0  */ ELPS_STATE_NR_W   = 0,/* No Request [working/active,protection/standby] */
 /* S1  */ ELPS_STATE_NR_P   = 1,/* No Request [working/standby,protection/active] */
 /* S2  */ ELPS_STATE_LO_W   = 2,/* Lockout [working/active,protection/standby] */
 /* S3  */ ELPS_STATE_FS_P   = 3,/* ForceSwitch [working/standby,protection/active]*/
 /* S4  */ ELPS_STATE_SF_W   = 4,/* SF(W) [working/standby,protection/active] */
 /* S5  */ ELPS_STATE_SF_P   = 5,/* SF(P) [working/active,protection/standby] */
 /* S6  */ ELPS_STATE_MS_P   = 6,/* ManualSwitch [working/standby,protection/active] */
 /* S7  */ ELPS_STATE_MS_W   = 7,/* ManualSwitch [working/active,protection/standby] */
 /* S8  */ ELPS_STATE_WTR_P  = 8,/* WaitToRestore [working/standby,protection/active] */
 /* S9  */ ELPS_STATE_DNR_P  = 9,/* DoNotRevert [working/standby,protection/active] */
 /* S10 */ ELPS_STATE_EXER_W = 10,/* Exercise [working/active,protection/standby] */
 /* S11 */ ELPS_STATE_EXER_P = 11,/* Exercise [working/standby,protection/active] */
 /* S12 */ ELPS_STATE_RR_W   = 12,/* Reverse Request [working/active, protection/standby] */
 /* S13 */ ELPS_STATE_RR_P   = 13,/* Reverse Request [working/standby, protection/active] */
    ELPS_SEM_MAX_STATES = 14,
};


enum
{
    A0 = 0,
    A1,
    A2,
    A3,
    A4,
    A5,
    A6,
    A7,
    A8,
    A9,
    A10,
    A11,
    A12,
    A13,
    A14,
    A15,
    A16,
    A17,
    ELPS_SEM_MAX_ACTIONS,
};
enum
{
    ELPS_ZERO
};
enum
{
    /* invalid */ ELPS_COMMAND_MIN   = 0,
    /* A14 */ ELPS_COMMAND_OVERRULED = 14,
    /* A15 */ ELPS_COMMAND_NA        = 15,
    /* A16 */ ELPS_MULTIPLE_ACTION   = 16,
    /* A17 */ ELPS_INVALID_ACTION    = 17
};


/* Define the global objects only once */
#ifdef _ELPSSEM_C_

const tElpsSemState gaElpsSemStateInfo[ELPS_SEM_MAX_STATES] =
{
    /*_______________________________________________________________________________________________________*/
    /*                                   STATE ACTION INFORMATION                                            */
    /*_______________________________________________________________________________________________________*/
    /*      | Wrk status   | Prot Status | Request Or State       | Requested Signal  |  Bridged Signal      */
    /*State-|--------------|-------------|------------------------|-------------------|----------------------*/
    /* S0   |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_APS_REQ_STATE_NR,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S1   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_APS_REQ_STATE_NR,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S2   |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_APS_REQ_STATE_LO,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S3   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_APS_REQ_STATE_FS,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S4   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_APS_REQ_STATE_SF,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S5   |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_APS_REQ_STATE_SF_P, ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S6   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_APS_REQ_STATE_MS,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S7   |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_APS_REQ_STATE_MS_W, ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S8   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_APS_REQ_STATE_WTR,  ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S9   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_APS_REQ_STATE_DNR,  ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S10  |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_APS_REQ_STATE_EXER, ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S11  |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_APS_REQ_STATE_EXER, ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S12  |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_APS_REQ_STATE_RR,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S13  |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_APS_REQ_STATE_RR,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /*______|________________________________________________________________________________________________*/
};

const tElpsPscSemState gaElpsPscSemStateInfo[ELPS_SEM_MAX_STATES] =
{
    /*_______________________________________________________________________________________________________*/
    /*                                   STATE ACTION INFORMATION                                            */
    /*_______________________________________________________________________________________________________*/
    /*      | Wrk status   | Prot Status | Request Or State       |        F-Path  |    Path         */
    /*State-|--------------|-------------|------------------------|-------------------|----------------------*/
    /* S0   |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_PSC_REQ_STATE_NR,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S1   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_PSC_REQ_STATE_NR,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NORMAL},
    /* S2   |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_PSC_REQ_STATE_LO,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S3   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_PSC_REQ_STATE_FS,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S4   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_PSC_REQ_STATE_SF,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S5   |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_PSC_REQ_STATE_SF,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S6   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_PSC_REQ_STATE_MS,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S7   |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_PSC_REQ_STATE_MS,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S8   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_PSC_REQ_STATE_WTR,  ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NORMAL},
    /* S9   |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_PSC_REQ_STATE_DNR,  ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NORMAL},
    /* S10  |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_PSC_REQ_STATE_EXER, ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S11  |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_PSC_REQ_STATE_EXER, ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /* S12  |*/ {ELPS_ACTIVE,  ELPS_STANDBY, ELPS_PSC_REQ_STATE_RR,   ELPS_SIGNAL_NULL,   ELPS_SIGNAL_NULL},
    /* S13  |*/ {ELPS_STANDBY, ELPS_ACTIVE,  ELPS_PSC_REQ_STATE_RR,   ELPS_SIGNAL_NORMAL, ELPS_SIGNAL_NORMAL},
    /*______|________________________________________________________________________________________________*/
};

const UINT4 gau4ActionInfo[ELPS_SEM_MAX_ACTIONS]=
{
 /* A0  */ ELPS_STATE_NR_W,   /* S0  */
 /* A1  */ ELPS_STATE_NR_P,   /* S1  */
 /* A2  */ ELPS_STATE_LO_W,   /* S2  */
 /* A3  */ ELPS_STATE_FS_P,   /* S3  */
 /* A4  */ ELPS_STATE_SF_W,   /* S4  */
 /* A5  */ ELPS_STATE_SF_P,   /* S5  */
 /* A6  */ ELPS_STATE_MS_P,   /* S6  */
 /* A7  */ ELPS_STATE_MS_W,   /* S7  */
 /* A8  */ ELPS_STATE_WTR_P,  /* S8  */
 /* A9  */ ELPS_STATE_DNR_P,  /* S9  */
 /* A10 */ ELPS_STATE_EXER_W, /* S10  */
 /* A11 */ ELPS_STATE_EXER_P, /* S11 */
 /* A12 */ ELPS_STATE_RR_W,   /* S12 */
 /* A13 */ ELPS_STATE_RR_P,   /* S13 */
    /* A14 */ ELPS_COMMAND_OVERRULED,
    /* A15 */ ELPS_COMMAND_NA, /* Not Applicable */
    /* A16 */ ELPS_MULTIPLE_ACTION,/* Special Case */
    /* A17 */ ELPS_INVALID_ACTION,
};

/* NOTE: The below state machine is as defined in the G.8031 Specification 
 * Amendment 1. The only exception is the state transition defined for 
 * state F and event f which has been modified from A0 to A15 as per the 
 * newer emerging version of the G.8031 (2009). */

const UINT4 gaau4Elps1T1BRSem[ELPS_SEM_MAX_EVENTS][ELPS_SEM_MAX_STATES] =
{
    /*-------------------------------------------------------------------------------------------------------------*/
    /*                           1:1 Bidirectional Revertive State Event Machine                                   */
    /*_____________________________________________________________________________________________________________*/
    /*                                                 STATES                                                      */
    /*_____________________________________________________________________________________________________________*/
    /*                 |     S0  | S1  | S2  | S3  | S4  | S5  | S6  | S7  | S8  | S9  | S10  | S11  | S12  | S13 |*/
    /* EVENTS          |     (A) | (B) | (C) | (D) | (E) | (F) | (G) | NA  | (I) | NA  | (K)  | NA   | (M)  | NA  |*/
    /*_________________|_________|_____|_____|_____|_____|_____|_____|_____|_____|_____|______|______|_____ |_____|*/
    /*-----------------|-----------------------------Near End Requests---------------------------------------------*/
    /* E0 (a)          |*/{  A2,   A2,   A14,  A2,   A2,   A2,   A2,   A17,  A2,   A17,  A2,   A17,   A2,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E1 (b)          |*/{  A3,   A3,   A14,  A14,  A3,   A14,  A3,   A17,  A3,   A17,  A3,   A17,   A3,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E2 (c)          |*/{  A4,   A4,   A14,  A14,  A15,  A14,  A4,   A17,  A4,   A17,  A4,   A17,   A4,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E3 (d)          |*/{  A15,  A14,  A14,  A14,  A8,   A14,  A15,  A17,  A15,  A17,  A15,  A17,   A15,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E4 (e)          |*/{  A5,   A5,   A14,  A5,   A5,   A15,  A5,   A17,  A5,   A17,  A5,   A17,   A5,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E5 (f)          |*/{  A15,  A15,  A14,  A15,  A15,  A16,  A15,  A17,  A15,  A17,  A15,  A17,   A15,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E6 (g)          |*/{  A6,   A6,   A14,  A14,  A14,  A14,  A14,  A17,  A6,   A17,  A6,   A17,   A6,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E7 (unused)     |*/{  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,   A17,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E8 (h)          |*/{  A15,  A15,  A16,  A16,  A15,  A15,  A0,   A17,  A0,   A17,  A0,   A17,   A15,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E9 (i)          |*/{  A10,  A14,  A14,  A14,  A14,  A14,  A14,  A17,  A14,  A17,  A14,  A17,   A10,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E10 (j)         |*/{  A15,  A15,  A15,  A15,  A15,  A15,  A15,  A17,  A0,   A17,  A15,  A17,   A15,   A17 },
    /*-----------------|-----------------------------Far End Requests---------------------------------------------*/
    /* E11 (k)         |*/{  A0,   A0,   A2,   A0,   A0,   A0,   A0,   A17,  A0,   A17,  A0,   A17,   A0,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E12 (l)         |*/{  A0,   A0,   A14,  A0,   A0,   A5,   A0,   A17,  A0,   A17,  A0,   A17,   A0,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E13 (m)         |*/{  A1,   A1,   A14,  A3,   A1,   A14,  A1,   A17,  A1,   A17,  A1,   A17,   A1,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E14 (n)         |*/{  A1,   A1,   A14,  A14,  A4,   A14,  A1,   A17,  A1,   A17,  A1,   A17,   A1,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E15 (o)         |*/{  A1,   A1,   A14,  A14,  A14,  A14,  A6,   A17,  A1,   A17,  A1,   A17,   A1,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E16 (unused)    |*/{  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,   A17,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E17 (p)         |*/{  A1,   A16,   A14, A14,  A14,  A14,  A14,  A17,  A8,   A17,  A15,  A17,   A15,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E18 (q)         |*/{  A12,  A15,  A14,  A14,  A14,  A14,  A14,  A17,  A14,  A17,  A10,   A17,  A12,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E19 (unused)    |*/{  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,   A17,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E20 (r)         |*/{  A0,   A15,  A14,  A14,  A14,  A14,  A14,  A17,  A14,  A17,  A10,   A17,  A0,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E21 (unused)    |*/{  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,   A17,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E22 (s)         |*/{  A16,  A16,  A14,  A14,  A14,  A14,  A14,  A17,  A16,  A17,  A14,  A17,   A0,    A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E23 (t)         |*/{  A0,   A16,  A14,  A14,  A14,  A14,  A14,  A17,  A16,  A17,  A15,  A17,   A15,   A17 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E24 (unused)    |*/{  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,   A17,   A17 },
    /*_________________|_________|_____|_____|_____|_____|_____|_____|_____|_____|_____|______|______|_____|_____|*/
    /*                 |     S0  | S1  | S2  | S3  | S4  | S5  | S6  | S7  | S8  | S9  | S10  | S11  | S12 | S13 |*/
    /*_________________|_________|_____|_____|_____|_____|_____|_____|_____|_____|_____|______|______|_____|_____|*/
};

/* NOTE: The below state machine is as defined in the G.8031 Specification 
 * Amendment 1. The only exception is the state transition defined for 
 * state F and event f which has been modified from A0 to A16 as per the 
 * newer emerging version of the G.8031 (2009). */

const UINT4 gaau4Elps1T1BNRSem[ELPS_SEM_MAX_EVENTS][ELPS_SEM_MAX_STATES] =
{
    /*------------------------------------------------------------------------------------------------------------*/
    /*                            1:1 Bidirectional Non-Revertive State Event Machine                             */
    /*____________________________________________________________________________________________________________*/
    /*                                                 STATES                                                     */
    /*___________________________________________________________________________________________________________ */
    /* EVENTS          |     S0  | S1  | S2  | S3  | S4  | S5  | S6  | S7  | S8  | S9  | S10  | S11  | S12 | S13 |*/
    /*                 |     (A) | (B) | (C) | (D) | (E) | (F) | (G) | (H) | NA  | (J) | (K)  | (L)  | (M) | (N) |*/
    /*_________________|_________|_____|_____|_____|_____|_____|_____|_____|_____|_____|______|______|_____|_____|*/
    /*-----------------|-----------------------------Near End Requests--------------------------------------------*/
    /* E0 (a)          |*/{  A2,   A2,   A14,  A2,   A2,   A2,   A2,   A2,   A17,  A2,   A2,    A2,    A2  ,  A2 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E1 (b)          |*/{  A3,   A3,   A14,  A14,  A3,   A14,  A3,   A3,   A17,  A3,   A3,    A3,    A3  ,  A3 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E2 (c)          |*/{  A4,   A4,   A14,  A14,  A15,  A14,  A4,   A4,   A17,  A4,   A4,    A4,    A4  ,  A4 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E3 (d)          |*/{  A15,  A14,  A14,  A14,  A9,   A14,  A15,  A15,  A17,  A15,  A15,   A15,   A15 ,  A15},
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E4 (e)          |*/{  A5,   A5,   A14,  A5,   A5,   A15,  A5,   A5,   A17,  A5,   A5,    A5,    A5  ,  A5 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E5 (f)          |*/{  A15,  A15,  A14,  A15,  A15,  A16,  A15,  A15,  A17,  A15,  A15,   A15,   A15 ,  A15 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E6 (g)          |*/{  A6,   A6,   A14,  A14,  A14,  A14,  A14,  A6,   A17,  A6,   A6,    A6,    A6  ,  A6 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E7 (h)          |*/{  A7,   A14,  A14,  A14,  A14,  A14,  A14,  A14,  A17,  A7,   A7,    A7,    A7  ,  A7 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E8 (i)          |*/{  A15,  A15,  A16,  A16,  A15,  A15,  A16,  A0,   A17,  A15,  A0,    A9,    A15 ,  A15 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E9 (j)          |*/{  A10,  A14,  A14,  A14,  A14,  A14,  A14,  A14,  A17,  A11,  A14,   A14,   A10 ,  A11 },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E10(unused)     |*/{  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,  A17,   A17,   A17 ,  A17 },
    /*-----------------|-----------------------------Far End Requests---------------------------------------------*/
    /* E11 (k)         |*/{  A0,   A0,   A2,   A0,   A0,   A0,   A0,   A0,   A17,  A0,   A0,    A0,    A0,    A0   },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E12 (l)         |*/{  A0,   A0,   A14,  A0,   A0,   A5,   A0,   A0,   A17,  A0,   A0,    A0,    A0,    A0   },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E13 (m)         |*/{  A1,   A1,   A14,  A3,   A1,   A14,  A1,   A1,   A17,  A1,   A1,    A1,    A1,    A1   },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E14 (n)         |*/{  A1,   A1,   A14,  A14,  A4,   A14,  A1,   A1,   A17,  A1,   A1,    A1,    A1,    A1   },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E15 (o)         |*/{  A1,   A1,   A14,  A14,  A14,  A14,  A6,   A1,   A17,  A1,   A1,    A1,    A1,    A1   },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E16 (p)         |*/{  A0,   A15,  A14,  A14,  A14,  A14,  A14,  A7,   A17,  A0,   A0,    A0,    A0,    A0   },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E17 (q)         |*/{  A1,   A1,   A14,  A14,  A14,  A14,  A14,  A14,  A17,  A1,   A1,    A1,    A1,    A1   },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E18 (r)         |*/{  A12,  A15,  A14,  A14,  A14,  A14,  A14,  A14,  A17,  A15,  A10,   A15,   A12,   A15  },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E19 (s)         |*/{  A15,  A15,  A14,  A14,  A14,  A14,  A14,  A14,  A17,  A13,  A15,   A11,   A15,   A13  },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E20 (t)         |*/{  A0,   A15,  A14,  A14,  A14,  A14,  A14,  A14,  A17,  A15,  A10,   A15,   A0,    A15  },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E21 (u)         |*/{  A15,  A15,  A14,  A14,  A14,  A14,  A14,  A14,  A17,  A9,   A15,   A11,   A15,   A9   },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E22 (v)         |*/{  A16,  A16,  A14,  A14,  A14,  A14,  A14,  A14,  A17,  A14,  A14,   A15,   A0,    A15  },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E23 (w)         |*/{  A0,   A9,   A14,  A14,  A14,  A14,  A14,  A14,  A17,  A14,  A15,   A14,   A15,   A15  },
    /*-----------------|------------------------------------------------------------------------------------------*/
    /* E24 (x)         |*/{  A15,  A9,   A14,  A14,  A14,  A14,  A14,  A14,  A17,  A9,   A15,   A14,   A15,   A9   },    
    /*_________________|_________|_____|_____|_____|_____|_____|_____|_____|_____|_____|______|______|_____|_____|*/
    /* EVENTS          |     S0  | S1  | S2  | S3  | S4  | S5  | S6  | S7  | S8  | S9  | S10  | S11  | S12 |  S13 */
    /*_________________|_________|_____|_____|_____|_____|_____|_____|_____|_____|_____|______|______|_____|______*/
};

const UINT1 gau1ElpsLocalCondToEvMap[ELPS_MAX_LOCAL_CONDITION] =
{
    /* UNUSED EVENT               */ ELPS_SEM_MAX_EVENTS,
    /* ELPS_LOCAL_COND_SF_W       */ ELPS_EV_LR_WRK_SF,
    /* ELPS_LOCAL_COND_WRK_REC_SF */ ELPS_EV_LR_WRK_RECOV_SF,
    /* ELPS_LOCAL_COND_SF_P       */ ELPS_EV_LR_PROT_SF,
    /* ELPS_LOCAL_COND_PROT_REC_SF*/ ELPS_EV_LR_PROT_RECOV_SF,
    /* ELPS_LOCAL_COND_WTR_EXP    */ ELPS_EV_LR_WTR_EXP
};
const UINT1 gau1ElpsLocalCmdToEvMap[ELPS_MAX_LOCAL_COMMAND] =
{
    /* UNUSED EVENT      */      ELPS_SEM_MAX_EVENTS,
    /* ELPS_EXT_CMD_LOP  */      ELPS_EV_LR_LO,
    /* ELPS_EXT_CMD_FS   */      ELPS_EV_LR_FS,
    /* ELPS_EXT_CMD_MS   */      ELPS_EV_LR_MS,
    /* ELPS_EXT_CMD_EXER */      ELPS_EV_LR_EXER,
    /* ELPS_EXT_CMD_CLR  */      ELPS_EV_LR_CLEAR,
    /* ELPS_EXT_CMD_FREEZE */    ELPS_ZERO,
    /* ELPS_EXT_CMD_CLR_FREEZE*/ ELPS_ZERO,
    /* ELPS_EXT_CMD_MS_W */      ELPS_EV_LR_MS_W,
};
const UINT1 gau1ElpsFarEndReqToEvMap[ELPS_MAX_FAR_REQUEST] =
{
    /* UNUSED EVENT       */ ELPS_SEM_MAX_EVENTS,
    /* ELPS_FAR_REQ_LO    */ ELPS_EV_FR_LO,
    /* ELPS_FAR_REQ_SF_P  */ ELPS_EV_FR_SF_P,
    /* ELPS_FAR_REQ_FS    */ ELPS_EV_FR_FS,
    /* ELPS_FAR_REQ_SF_W  */ ELPS_EV_FR_SF,
    /* ELPS_FAR_REQ_MS    */ ELPS_EV_FR_MS,
    /* ELPS_FAR_REQ_WTR   */ ELPS_EV_FR_WTR,
    /* ELPS_FAR_REQ_EXER_W*/ ELPS_EV_FR_EXER_W,
    /* ELPS_FAR_REQ_EXER_P*/ ELPS_EV_FR_EXER_P,
    /* ELPS_FAR_REQ_RR_W  */ ELPS_EV_FR_RR_W,
    /* ELPS_FAR_REQ_RR_P  */ ELPS_EV_FR_RR_P,
    /* ELPS_FAR_REQ_NR_W  */ ELPS_EV_FR_NR_W,
    /* ELPS_FAR_REQ_NR_P  */ ELPS_EV_FR_NR_P,
    /* ELPS_FAR_REQ_DNR   */ ELPS_EV_FR_DNR,
    /* ELPS_FAR_REQ_MS_W  */ ELPS_EV_FR_MS_W
};

const INT1 gai1ReqMapPriority[ELPS_SEM_MAX_EVENTS]=
{
    /* Local Requests (_LR_) */
       ELPS_PKT_REQ_STATE_LO,      /* ELPS_EV_LR_LO */
       ELPS_PKT_REQ_STATE_FS,      /* ELPS_EV_LR_FS */
       ELPS_PKT_REQ_STATE_SF,      /* ELPS_EV_LR_WRK_SF */
       ELPS_NA,                    /* ELPS_EV_LR_WRK_RECOV_SF */
       ELPS_PKT_REQ_STATE_SF_P,    /* ELPS_EV_LR_PROT_SF */
       ELPS_NA,                    /* ELPS_EV_LR_PROT_RECOV_SF */
       ELPS_PKT_REQ_STATE_MS,      /* ELPS_EV_LR_MS */
       ELPS_PKT_REQ_STATE_MS_W,    /* ELPS_EV_LR_MS_W */
       ELPS_NA,                    /* ELPS_EV_LR_CLEAR */
       ELPS_PKT_REQ_STATE_EXER,    /* ELPS_EV_LR_EXER */
       ELPS_NA,                    /* ELPS_EV_LR_WTR_EXP */

    /* Far end Request (_FR_) */
       ELPS_PKT_REQ_STATE_LO,      /* ELPS_EV_FR_LO - LO [r/b=null] */
       ELPS_PKT_REQ_STATE_SF_P,    /* ELPS_EV_FR_SF_P - SF-P [r/b=null] */
       ELPS_PKT_REQ_STATE_FS,      /* ELPS_EV_FR_FS -  FS [r/b=normal] */
       ELPS_PKT_REQ_STATE_SF,      /* ELPS_EV_FR_SF - SF [r/b=normal] */
       ELPS_PKT_REQ_STATE_MS,      /* ELPS_EV_FR_MS -  MS [r/b=normal] */
       ELPS_PKT_REQ_STATE_MS_W,    /* ELPS_EV_FR_MS_W - MS [r/b=null]*/
       ELPS_PKT_REQ_STATE_WTR,     /* ELPS_EV_FR_WTR - WTR [r/b=normal] */
       ELPS_PKT_REQ_STATE_EXER,    /* ELPS_EV_FR_EXER_W - [r/b=null] */
       ELPS_PKT_REQ_STATE_EXER,    /* ELPS_EV_FR_EXER_P - EXER [r/b=normal]*/
       ELPS_PKT_REQ_STATE_RR,      /* ELPS_EV_FR_RR_W - RR [r/b=null] */
       ELPS_PKT_REQ_STATE_RR,      /* ELPS_EV_FR_RR_P - RR [r/b=normal] */
       ELPS_PKT_REQ_STATE_NR,      /* ELPS_EV_FR_NR_W - NR [r/b=null] */
       ELPS_PKT_REQ_STATE_NR,      /* ELPS_EV_FR_NR_P - NR [r/b=normal] */
       ELPS_PKT_REQ_STATE_DNR     /* ELPS_EV_FR_DNR - DNR [r/b=normal]*/

};

const INT1 gai1ReqMapPriorityPSCVersion[ELPS_SEM_MAX_EVENTS]=
{
    /* Local Requests (_LR_) */
       ELPS_PKT_REQ_STATE_LO,      /* ELPS_EV_LR_LO */
       ELPS_PSC_VER_PKT_REQ_STATE_FS,      /* ELPS_EV_LR_FS */
       ELPS_PKT_REQ_STATE_SF,      /* ELPS_EV_LR_WRK_SF */
       ELPS_NA,                    /* ELPS_EV_LR_WRK_RECOV_SF */
       ELPS_PSC_VER_PKT_REQ_STATE_SF_P,    /* ELPS_EV_LR_PROT_SF */
       ELPS_NA,                    /* ELPS_EV_LR_PROT_RECOV_SF */
       ELPS_PKT_REQ_STATE_MS,      /* ELPS_EV_LR_MS */
       ELPS_PKT_REQ_STATE_MS_W,    /* ELPS_EV_LR_MS_W */
       ELPS_REQ_CLEAR,             /* ELPS_EV_LR_CLEAR */
       ELPS_PKT_REQ_STATE_EXER,    /* ELPS_EV_LR_EXER */
       ELPS_NA,                    /* ELPS_EV_LR_WTR_EXP */

    /* Far end Request (_FR_) */
       ELPS_PKT_REQ_STATE_LO,      /* ELPS_EV_FR_LO - LO [r/b=null] */
       ELPS_PSC_VER_PKT_REQ_STATE_SF_P,    /* ELPS_EV_FR_SF_P - SF-P [r/b=null] */
       ELPS_PSC_VER_PKT_REQ_STATE_FS,      /* ELPS_EV_FR_FS -  FS [r/b=normal] */
       ELPS_PKT_REQ_STATE_SF,      /* ELPS_EV_FR_SF - SF [r/b=normal] */
       ELPS_PKT_REQ_STATE_MS,      /* ELPS_EV_FR_MS -  MS [r/b=normal] */
       ELPS_PKT_REQ_STATE_MS_W,    /* ELPS_EV_FR_MS_W - MS [r/b=null]*/
       ELPS_PKT_REQ_STATE_WTR,     /* ELPS_EV_FR_WTR - WTR [r/b=normal] */
       ELPS_PKT_REQ_STATE_EXER,    /* ELPS_EV_FR_EXER_W - [r/b=null] */
       ELPS_PKT_REQ_STATE_EXER,    /* ELPS_EV_FR_EXER_P - EXER [r/b=normal]*/
       ELPS_PKT_REQ_STATE_RR,      /* ELPS_EV_FR_RR_W - RR [r/b=null] */
       ELPS_PKT_REQ_STATE_RR,      /* ELPS_EV_FR_RR_P - RR [r/b=normal] */
       ELPS_PKT_REQ_STATE_NR,      /* ELPS_EV_FR_NR_W - NR [r/b=null] */
       ELPS_PKT_REQ_STATE_NR,      /* ELPS_EV_FR_NR_P - NR [r/b=normal] */
       ELPS_PKT_REQ_STATE_DNR     /* ELPS_EV_FR_DNR - DNR [r/b=normal]*/

};

const INT1 gai1StateMapPriority[ELPS_SEM_MAX_STATES]=
{
 ELPS_PKT_REQ_STATE_NR,  /* ELPS_STATE_NR_W   -  S0  */
 ELPS_PKT_REQ_STATE_NR,  /* ELPS_STATE_NR_P   -  S1  */
 ELPS_PKT_REQ_STATE_LO,  /* ELPS_STATE_LO_W   -  S2  */
 ELPS_PKT_REQ_STATE_FS,  /* ELPS_STATE_FS_P   -  S3  */
 ELPS_PKT_REQ_STATE_SF,  /* ELPS_STATE_SF_W   -  S4  */
 ELPS_PKT_REQ_STATE_SF_P,/* ELPS_STATE_SF_P   -  S5  */
 ELPS_PKT_REQ_STATE_MS,  /* ELPS_STATE_MS_P   -  S6  */
 ELPS_PKT_REQ_STATE_MS_W,/* ELPS_STATE_MS_W   -  S7  */
 ELPS_PKT_REQ_STATE_WTR, /* ELPS_STATE_WTR_P  -  S8  */
 ELPS_PKT_REQ_STATE_DNR, /* ELPS_STATE_DNR_P  -  S9  */
 ELPS_PKT_REQ_STATE_EXER,/* ELPS_STATE_EXER_W -  S10 */
 ELPS_PKT_REQ_STATE_EXER,/* ELPS_STATE_EXER_P -  S11 */
 ELPS_PKT_REQ_STATE_RR,  /* ELPS_STATE_RR_W   -  S12 */
 ELPS_PKT_REQ_STATE_RR   /* ELPS_STATE_RR_P   -  S13 */
};

const INT1 gai1StateMapPriorityPSCVersion[ELPS_SEM_MAX_STATES]=
{
 ELPS_PKT_REQ_STATE_NR,  /* ELPS_STATE_NR_W   -  S0  */
 ELPS_PKT_REQ_STATE_NR,  /* ELPS_STATE_NR_P   -  S1  */
 ELPS_PKT_REQ_STATE_LO,  /* ELPS_STATE_LO_W   -  S2  */
 ELPS_PSC_VER_PKT_REQ_STATE_FS,  /* ELPS_STATE_FS_P   -  S3  */
 ELPS_PKT_REQ_STATE_SF,  /* ELPS_STATE_SF_W   -  S4  */
 ELPS_PSC_VER_PKT_REQ_STATE_SF_P,/* ELPS_STATE_SF_P   -  S5  */
 ELPS_PKT_REQ_STATE_MS,  /* ELPS_STATE_MS_P   -  S6  */
 ELPS_PKT_REQ_STATE_MS_W,/* ELPS_STATE_MS_W   -  S7  */
 ELPS_PKT_REQ_STATE_WTR, /* ELPS_STATE_WTR_P  -  S8  */
 ELPS_PKT_REQ_STATE_DNR, /* ELPS_STATE_DNR_P  -  S9  */
 ELPS_PKT_REQ_STATE_EXER,/* ELPS_STATE_EXER_W -  S10 */
 ELPS_PKT_REQ_STATE_EXER,/* ELPS_STATE_EXER_P -  S11 */
 ELPS_PKT_REQ_STATE_RR,  /* ELPS_STATE_RR_W   -  S12 */
 ELPS_PKT_REQ_STATE_RR   /* ELPS_STATE_RR_P   -  S13 */
};

#else
extern const tElpsSemState gaElpsSemStateInfo[ELPS_SEM_MAX_STATES];
extern const tElpsPscSemState gaElpsPscSemStateInfo[ELPS_SEM_MAX_STATES];
extern const UINT4 gau4ActionInfo[ELPS_SEM_MAX_ACTIONS];
extern const UINT4 gaau4Elps1T1BRSem[ELPS_SEM_MAX_EVENTS][ELPS_SEM_MAX_STATES];
extern const UINT4 gaau4Elps1T1BNRSem[ELPS_SEM_MAX_EVENTS][ELPS_SEM_MAX_STATES];
extern const UINT1 gau1ElpsLocalCondToEvMap[ELPS_MAX_LOCAL_CONDITION];
extern const UINT1 gau1ElpsLocalCmdToEvMap[ELPS_MAX_LOCAL_COMMAND];
extern const UINT1 gau1ElpsFarEndReqToEvMap[ELPS_MAX_FAR_REQUEST];
extern const INT1 gai1ReqMapPriority[ELPS_SEM_MAX_EVENTS];
extern const INT1 gai1StateMapPriority[ELPS_SEM_MAX_STATES];
extern const INT1 gai1StateMapPriorityPSCVersion[ELPS_SEM_MAX_STATES];
extern const INT1 gai1ReqMapPriorityPSCVersion[ELPS_SEM_MAX_EVENTS];
#endif /* _ELPSSEM_C_ */



#endif /* _ELPSSEM_H_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpssem.h                        */
/*-----------------------------------------------------------------------*/
