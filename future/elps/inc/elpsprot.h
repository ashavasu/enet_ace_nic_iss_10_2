/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsprot.h,v 1.21 2015/06/10 05:39:55 siva Exp $
 *
 * Description: This file contains prototypes for functions defined in ELPS.
 *****************************************************************************/
#ifndef _ELPSPROT_H
#define _ELPSPROT_H
#include "mplsapi.h"
/*--------------------------------------------------------------------------*/
/*                       elpsmain.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsMainModuleStart PROTO ((VOID));
PUBLIC VOID ElpsMainModuleShutDown PROTO ((VOID));
PUBLIC VOID ElpsMainProcessEvent PROTO ((UINT4));
PUBLIC INT4 ElpsMainTaskInit PROTO ((VOID));
PUBLIC VOID ElpsMainTaskDeInit PROTO ((VOID));
PUBLIC VOID ElpsMainReleaseQMemory PROTO ((tElpsQMsg *pMsg));
PUBLIC VOID ElpsMainAssignMempoolIds PROTO ((VOID));
/*--------------------------------------------------------------------------*/
/*                       elpstmr.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsTmrInit PROTO ((VOID));
PUBLIC INT4 ElpsTmrDeInit PROTO ((VOID));
PUBLIC VOID ElpsTmrExpHandler PROTO ((VOID));
PUBLIC INT4 ElpsTmrStartTimer PROTO ((tElpsPgInfo *, UINT4, UINT1));
PUBLIC INT4 ElpsTmrStopTimer PROTO ((tElpsPgInfo *, UINT1));

/*--------------------------------------------------------------------------*/
/*                       elpsutil.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsUtilApsTxDbLock PROTO ((VOID));
PUBLIC INT4 ElpsUtilApsTxDbUnLock PROTO ((VOID));
PUBLIC INT4 ElpsUtilIsElpsStarted PROTO ((UINT4));
PUBLIC tElpsContextInfo * ElpsUtilValidateContextInfo PROTO ((UINT4, UINT4 *));
PUBLIC tElpsPgInfo * ElpsUtilValidateCxtAndPgInfo PROTO ((UINT4, UINT4, 
                                                          UINT4 *));
PUBLIC INT4 ElpsUtilValidateContext PROTO ((UINT4, UINT4 *));
PUBLIC tElpsPgInfo * ElpsUtilValContextAndProtGroup PROTO ((UINT4, UINT4, 
                                                            UINT4 *));
PUBLIC VOID 
ElpsUtilPgCfgNotReadyToNotInServ PROTO ((tElpsPgInfo *pPgConfigInfo));
PUBLIC VOID
ElpsUtilPgCfmNotReadyToNotInServ PROTO ((tElpsPgInfo *pPgConfigInfo));
PUBLIC INT4
ElpsUtilGetTraceInputValue PROTO ((UINT1 *pu1TraceInput, UINT4 u4TraceOption));
PUBLIC INT4
ElpsUtilGetTraceOptionValue PROTO ((UINT1 *pu1TraceInput, INT4 i4TraceLen));
PUBLIC VOID
ElpsUtilSetTraceOption PROTO ((UINT1 *pu1Token, UINT4 *pu4TraceOption));
PUBLIC VOID
ElpsUtilSplitStrToTokens PROTO ((UINT1 *pu1InputStr, INT4 i4Strlen,
                          UINT1 u1Delimiter, UINT2 u2MaxToken,
                          UINT1 *apu1Token[], UINT1 *pu1TokenCount));
PUBLIC INT4 ElpsUtilGetBrgPortType PROTO ((UINT4, UINT1 *));
PUBLIC VOID
ElpsUtilGetPhyPortFromSispPort PROTO ((UINT4 *));
PUBLIC UINT4
ElpsUtilGetPgIdFromPgName PROTO ((UINT4, UINT1 *));
PUBLIC UINT1
ElpsUtilIsHoldOffOrWtrRunning PROTO ((tElpsPgInfo *, UINT1 * ,
                                      UINT4 *,UINT4 *,UINT4 *));
PUBLIC INT4 ElpsUtilUpdateMplsDb PROTO ((tElpsPgInfo *,  UINT4));

PUBLIC INT4 ElpsUtilFillHwSwitchInfo (tElpsContextInfo *,
                        tElpsHwPgSwitchInfo *, unServiceParam *,
                        unServiceParam *, unServiceParam *,
                        unServiceParam *, UINT1);
PUBLIC UINT4
ElpsGetPktCountForAllContexts(INT1, INT1 (*pCallBk)(UINT4,UINT4,UINT4 *));
/* HITLESS RESTART */
PUBLIC INT4
ElpsUtilGetPeriodicTime PROTO ((UINT4 u4IfIndex,
                         UINT4 *pu4RetValElpsApsPeriodicTime));

PUBLIC INT4
ElpsUtilSetVlanGroupIdForVlan (UINT4 u4ContextId, tVlanId VlanId,
                               UINT2 u2VlanGroupId);
PUBLIC INT4
ElpsUtilUpdateL2iwfOnPgDisable(tElpsPgInfo *pPgInfo);

/* performance measurement */
VOID
ElpsUtilMeasureTime PROTO ((tElpsPgInfo * pPgInfo, UINT1 u1Flag));
VOID
ElpsGetSysTime PROTO ((tOsixSysTime * pSysTime));

/*--------------------------------------------------------------------------*/
/*                       elpscxt.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsCxtCreateContext PROTO ((UINT4));
PUBLIC INT4 ElpsCxtDeleteContext PROTO ((UINT4));
PUBLIC INT4 ElpsCxtStart PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsCxtShutdown PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsCxtEnable PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsCxtDisable PROTO ((tElpsContextInfo *));
PUBLIC tElpsContextInfo * ElpsCxtGetNode PROTO ((UINT4));

/*--------------------------------------------------------------------------*/
/*                       elpstx.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC VOID ElpsTxInitApsTxInfoSLL PROTO ((VOID));
PUBLIC VOID ElpsTxEventHandler PROTO ((VOID));
PUBLIC VOID ElpsTxFormAndSendAPSPdu PROTO ((tElpsPgInfo *));
PUBLIC VOID ElpsTxHandleSemStateChange PROTO ((tElpsPgInfo *));
PUBLIC tCRU_BUF_CHAIN_HEADER *
ElpsTxFormMplsPkt PROTO ((tElpsPgInfo *, UINT4 *, UINT1 *));
PUBLIC INT4
ElpsTxSendPscPduToMpls PROTO ((tElpsApsTxInfo *));
PUBLIC INT4
ElpsTxFillLspInfo PROTO ((tElpsPgInfo *, tCRU_BUF_CHAIN_HEADER *,
                          UINT4 *, UINT1 *));
PUBLIC INT4
ElpsTxFillPwInfo PROTO ((tElpsPgInfo *, tCRU_BUF_CHAIN_HEADER *,
                         UINT4 *, UINT1 *));
PUBLIC UINT1 ElpsTxGetPscStateForPkt PROTO ((tElpsPgInfo * pPg, UINT1));
PUBLIC UINT1 ElpsTxGetPscStateForPscVersion1 PROTO ((tElpsPgInfo * pPg, UINT1));

/*--------------------------------------------------------------------------*/
/*                       elpsrx.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsRxProcessAPSPdu PROTO ((tElpsContextInfo *, tElpsQMsg *));
PUBLIC INT4 ElpsRxProcessPSCPdu PROTO ((tElpsContextInfo *, tElpsQMsg *));
PUBLIC INT4 ElpsRxValidatePscPdu PROTO ((tElpsPgInfo *, tElpsCfmConfigInfo *,
                                          UINT1 *));
PUBLIC UINT1 ElpsRxGetStateFromPscPkt PROTO ((UINT1, UINT1, UINT1));
PUBLIC UINT1 ElpsRxGetStateFromPscPktForPSCVersion1 PROTO ((UINT1, UINT1, UINT1));
/*--------------------------------------------------------------------------*/
/*                       elpspg.c                                           */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsPgCreateTable PROTO ((tElpsContextInfo *));
PUBLIC VOID ElpsPgDeleteTable PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsPgRBCmp PROTO ((tRBElem *, tRBElem *));
PUBLIC INT4 ElpsPgRBFree PROTO ((tRBElem *, UINT4));
PUBLIC tElpsPgInfo * 
ElpsPgCreateNode PROTO ((VOID));
PUBLIC INT4 ElpsPgAddNodeToPgTable PROTO ((tElpsContextInfo *, tElpsPgInfo *));
PUBLIC VOID ElpsPgDelNode PROTO ((tElpsPgInfo *));
PUBLIC tElpsPgInfo *
ElpsPgGetFirstNodeInContext PROTO ((tElpsContextInfo *));
PUBLIC tElpsPgInfo * 
ElpsPgGetNextNodeInContext PROTO ((tElpsContextInfo *, tElpsPgInfo *));
PUBLIC tElpsPgInfo * 
ElpsPgGetNode PROTO ((tElpsContextInfo *, UINT4));
PUBLIC INT4 ElpsPgActivate PROTO ((tElpsContextInfo *, tElpsPgInfo *));
PUBLIC INT4 ElpsPgDeActivate PROTO ((tElpsContextInfo *, tElpsPgInfo *));
PUBLIC VOID ElpsPgClearStatistics PROTO ((tElpsPgInfo *));
PUBLIC INT4 ElpsPgHandleProtPortConfig PROTO ((tElpsContextInfo *,
                                               tElpsPgInfo *, UINT4));
PUBLIC INT4 ElpsPgEnableAllPgEntries PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsPgDisableAllPgEntries PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsPgSwitchDataPath PROTO ((tElpsContextInfo *, tElpsPgInfo *, 
                                         UINT4));
PUBLIC tElpsPgInfo * 
ElpsPgGetFirstNode PROTO ((UINT4 *, UINT4 *));
PUBLIC tElpsPgInfo *
ElpsPgGetNextNode PROTO ((UINT4, UINT4, UINT4 *, UINT4 *));

/*--------------------------------------------------------------------------*/
/*                       elpscfm.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsCfmCreateTable PROTO ((tElpsContextInfo *));
PUBLIC VOID ElpsCfmDeleteTable PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsCfmRBCmp PROTO ((tRBElem *, tRBElem *));
PUBLIC INT4 ElpsCfmRBFree PROTO ((tRBElem *, UINT4));
PUBLIC tElpsCfmConfigInfo * 
ElpsCfmCreateNode PROTO ((VOID));
PUBLIC INT4 ElpsCfmAddNodeToCfmTable PROTO ((tElpsContextInfo *,
                                             tElpsPgInfo *,
                                             tElpsCfmConfigInfo *));
PUBLIC INT4 ElpsCfmDelNode PROTO ((tElpsContextInfo *,
                                   tElpsCfmConfigInfo *));
PUBLIC tElpsCfmConfigInfo *
ElpsCfmGetNode PROTO ((tElpsContextInfo *, tElpsCfmConfigInfo *));
PUBLIC tElpsPgInfo * 
ElpsCfmGetPgInfo PROTO ((tElpsContextInfo *, tElpsCfmConfigInfo *));
PUBLIC VOID
ElpsCfmProcessReceivedSignal PROTO ((tElpsCfmConfigInfo *, UINT4));
PUBLIC INT4 ElpsCfmDeActivate PROTO ((tElpsContextInfo *, tElpsPgInfo *));
PUBLIC INT4 ElpsCfmActivate PROTO ((tElpsContextInfo *, tElpsPgInfo *));
PUBLIC VOID ElpsCfmApplyReceivedSignalToSEM PROTO ((tElpsPgInfo *, UINT1));
PUBLIC INT4 ElpsCfmUpdateWithMonitorType PROTO ((tElpsPgInfo *, UINT1));
PUBLIC INT4 ElpsCfmUpdateWithServiceType PROTO ((tElpsPgInfo *, UINT1));

/*--------------------------------------------------------------------------*/
/*                       elpssrvl.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsSrvListCreateTable PROTO ((tElpsContextInfo *));
PUBLIC VOID ElpsSrvListDeleteTable PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsSrvListRBCmp PROTO ((tRBElem *, tRBElem *));
PUBLIC INT4 ElpsSrvListRBFree PROTO ((tRBElem *, UINT4 ));
PUBLIC tElpsServiceListInfo * ElpsSrvListCreateNode PROTO ((VOID));
PUBLIC INT4 ElpsSrvListAddNodeToSrvListTable PROTO ((tElpsContextInfo *,
                                                     tElpsPgInfo *,
                                                     tElpsServiceListInfo *));
PUBLIC INT4 ElpsSrvListRmvNodeInSrvListTable PROTO ((tElpsContextInfo *,
                                                     tElpsPgInfo *,
                                                     tElpsServiceListInfo *));
PUBLIC VOID ElpsSrvListDelNode PROTO ((tElpsContextInfo *,
                                       tElpsPgInfo *,
                                       tElpsServiceListInfo *));
PUBLIC VOID ElpsSrvListDelAllNodesForPg PROTO ((tElpsContextInfo *,
                                                tElpsPgInfo *));
PUBLIC tElpsServiceListInfo * 
ElpsSrvListGetFirstNode PROTO ((tElpsContextInfo *));
PUBLIC tElpsServiceListInfo * 
ElpsSrvListGetNextNode PROTO ((tElpsContextInfo *, 
                               tElpsServiceListInfo *));
PUBLIC tElpsServiceListInfo * 
ElpsSrvListGetNextNodeForPg PROTO ((tElpsContextInfo *, tElpsPgInfo *, 
                                    tElpsServiceListInfo *));
PUBLIC tElpsServiceListInfo *
ElpsSrvListGetNode PROTO ((tElpsContextInfo *, UINT4, UINT4 ));


PUBLIC INT4 ElpsSrvListPointerCreateTable PROTO ((tElpsContextInfo *));
PUBLIC VOID ElpsSrvListPointerDeleteTable PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsSrvListPointerRBCmp PROTO ((tRBElem *, tRBElem *));
PUBLIC INT4 ElpsSrvListPointerRBFree PROTO ((tRBElem *, UINT4 ));
PUBLIC tElpsServiceListPointerInfo * ElpsSrvListPointerCreateNode PROTO ((VOID));

PUBLIC INT4 ElpsSrvListAddNodeToSrvListPtrTable PROTO ((tElpsContextInfo *,
                                                     tElpsPgInfo *,
                                                     tElpsServiceListPointerInfo *));
PUBLIC VOID ElpsSrvListPtrDelNode PROTO ((tElpsContextInfo *,
                                       tElpsPgInfo *,
                                       tElpsServiceListPointerInfo *));
PUBLIC VOID ElpsSrvListPtrDelAllNodesForPg PROTO ((tElpsContextInfo *,
                                                tElpsPgInfo *));
PUBLIC tElpsServiceListPointerInfo * 
ElpsSrvListPtrGetFirstNode PROTO ((tElpsContextInfo *));
PUBLIC tElpsServiceListPointerInfo * 
ElpsSrvListPtrGetNextNode PROTO ((tElpsContextInfo *, 
                               tElpsServiceListPointerInfo *));
PUBLIC tElpsServiceListPointerInfo * 
ElpsSrvListPtrGetNextNodeForPg PROTO ((tElpsContextInfo *, tElpsPgInfo *, 
                                    tElpsServiceListPointerInfo *));
PUBLIC INT4 ElpsSrvListRmvNodeInSrvListPtrTable PROTO ((tElpsContextInfo *,
                                                     tElpsPgInfo *,
                                                     tElpsServiceListPointerInfo *));
PUBLIC tElpsServiceListPointerInfo *
ElpsSrvListPtrGetNode PROTO ((tElpsContextInfo *, UINT4, UINT4 ));



/*--------------------------------------------------------------------------*/
/*                       elpsshrl.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsPgShareCreateTable PROTO ((tElpsContextInfo *));
PUBLIC VOID ElpsPgShareDeleteTable PROTO ((tElpsContextInfo *));
PUBLIC INT4 ElpsPgShareRBCmp PROTO ((tRBElem *, tRBElem *));
PUBLIC INT4 ElpsPgShareAddNodeToPgShareTable PROTO ((tElpsContextInfo *, 
                                                     tElpsPgInfo *));
PUBLIC VOID ElpsPgShareDelNode PROTO ((tElpsContextInfo *,
                                       tElpsPgInfo *));
PUBLIC tElpsPgInfo *
ElpsPgShareGetFirstNode PROTO ((tElpsContextInfo *));
PUBLIC tElpsPgInfo * 
ElpsPgShareGetNextNode PROTO ((tElpsContextInfo *,
                               tElpsPgInfo *));
PUBLIC tElpsPgInfo *
ElpsPgShareGetNode PROTO ((tElpsContextInfo *, UINT4, UINT4));
    
/*--------------------------------------------------------------------------*/
/*                       elpssem.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC UINT1 ElpsSemApplyEvent PROTO ((tElpsPgInfo *, UINT1, UINT1));
PUBLIC VOID ElpsSemFreeze PROTO ((tElpsPgInfo *));
PUBLIC VOID ElpsSemClearFreeze PROTO ((tElpsPgInfo *));
PUBLIC UINT1 ElpsSemGetEventIdForReq PROTO ((UINT1, UINT1));
PUBLIC INT4 ElpsSemGetStateInfo PROTO ((UINT1, tElpsSemState *));
PUBLIC INT4 ElpsSemPscGetStateInfo PROTO ((UINT1, tElpsPscSemState *));
PUBLIC INT4 ElpsSemCheckForActiveRequest PROTO ((UINT4 , UINT4 , INT4));
PUBLIC VOID ElpsSemResetSemVariables PROTO ((tElpsPgInfo *));
PUBLIC UINT1 ElpsSemApplyGlobalPriorityLogic PROTO((tElpsPgInfo * ,UINT1 *));
PUBLIC UINT1 ElpsSemValidateLocalRequest PROTO((tElpsPgInfo * ,UINT1 ));
PUBLIC VOID ElpsSemModifyLocalCommandStatus PROTO((tElpsPgInfo * ,UINT1));
PUBLIC VOID ElpsSemChangePgStatus PROTO((UINT4, UINT4 , tElpsPgInfo *));

/*--------------------------------------------------------------------------*/
/*                       elpspw.c                                           */
/*--------------------------------------------------------------------------*/

PUBLIC INT4 ElpsPwUpdatePwTableBaseOid PROTO ((VOID));
PUBLIC VOID ElpsPwGetPwServicePointer PROTO ((tElpsPwInfo * pPwServiceInfo,
                       tSNMP_OID_TYPE * pPgConfigServicePointer));

PUBLIC INT4 ElpsPwSetPwServiceInfo PROTO ((tElpsPwInfo * pPwServiceInfo,
                       tSNMP_OID_TYPE *));

PUBLIC INT4 ElpsPwGetMegInfo PROTO((tMplsEventNotif * pMplsEvent,
                       tElpsCfmMonitorInfo * pMonitorInfo));

PUBLIC INT4 ElpsPwGetPwIndex PROTO((UINT4, UINT4, UINT4,
                       UINT4 *));

/*--------------------------------------------------------------------------*/
/*                       elpstnl.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsTnlUpdateTnlTableBaseOid PROTO ((VOID));

PUBLIC VOID ElpsTnlGetTnlServicePointer PROTO ((tElpsLspInfo * pLspServiceInfo,
                         tSNMP_OID_TYPE * pPgConfigServicePointer));

PUBLIC INT4 ElpsTnlGetMegInfo PROTO ((tMplsEventNotif * pMplsEvent,
                         tElpsCfmMonitorInfo * pMonitorInfo));

PUBLIC INT4 ElpsTnlGetTunnelAddrType PROTO ((tSNMP_OID_TYPE Service,
                                     tMplsApiInInfo *, tMplsApiOutInfo *));


/*--------------------------------------------------------------------------*/
/*                       elpsport.c                                         */
/*--------------------------------------------------------------------------*/

PUBLIC INT4 ElpsPortEcfmRegisterPerMep PROTO ((tElpsPgInfo *pPgInfo));
PUBLIC VOID ElpsPortEcfmDeRegisterPerMep PROTO ((tElpsPgInfo *pPgInfo));

PUBLIC INT4
ElpsPortMplsApiHandleExtRequest PROTO ((UINT4 , tMplsApiInInfo *,
                                        tMplsApiOutInfo *));

PUBLIC INT4
ElpsPortEcfmInitiateExPdu PROTO ((tEcfmMepInfoParams *, UINT1,  UINT1, 
                                  UINT1 *, UINT4, tMacAddr, UINT1));
PUBLIC INT4
ElpsPortVcmGetCxtInfoFromIfIndex PROTO ((UINT4 u4IfIndex, 
                                             UINT4 * pu4ContextId,
                                      UINT2 * pu2LocalPortId));
PUBLIC INT4
ElpsPortVcmIsSwitchExist PROTO ((UINT1 * pu1Alias, UINT4 * pu4ContextId));

PUBLIC INT4
ElpsPortVcmGetAliasName PROTO ((UINT4 u4ContextId, UINT1 * pu1Alias));
PUBLIC INT4
ElpsPortCfaGetIfInfo PROTO ((UINT4, tCfaIfInfo *));
PUBLIC INT4
ElpsPortCfaCliGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));
PUBLIC INT4 ElpsPortHwPgSwitchDataPath PROTO ((UINT4, UINT4 , 
                                               tElpsHwPgSwitchInfo *));
PUBLIC INT4
ElpsPortCfaCliConfGetIfName PROTO ((UINT4 u4IfIndex, INT1 *pi1IfName));
PUBLIC VOID
ElpsPortNotifyProtocolShutStatus PROTO ((INT4 i4ContextId));
PUBLIC VOID
ElpsPortFmNotifyFaults PROTO ((tSNMP_OID_TYPE *, UINT4, UINT4, 
                               tSNMP_VAR_BIND *, UINT1 *, 
                               tSNMP_OCTET_STRING_TYPE *));
PUBLIC INT4 
ElpsPortVcmSispGetPhysicalPortOfSispPort PROTO ((UINT4 , UINT4 *));
PUBLIC INT4
ElpsPortSNMPCheckForNVTChars PROTO ((UINT1 *, INT4));
PUBLIC UINT4
ElpsPortRmEnqMsgToRm PROTO ((tRmMsg * , UINT2 ,
                                     UINT4 , UINT4 ));
PUBLIC UINT4
ElpsPortRmGetNodeState PROTO ((VOID));
PUBLIC UINT1
ElpsPortRmGetStandbyNodeCount PROTO ((VOID));
PUBLIC UINT1
ElpsPortRmApiHandleProtocolEvent PROTO ((tRmProtoEvt * ));
PUBLIC UINT4
ElpsPortRmRegisterProtocols PROTO ((tRmRegParams * ));
PUBLIC INT4
ElpsPortRmReleaseMemoryForMsg PROTO ((UINT1 *));
PUBLIC INT4
ElpsPortRmSetBulkUpdatesStatus PROTO ((UINT4 ));
PUBLIC INT4
ElpsPortRmApiSendProtoAckToRM PROTO ((tRmProtoAck * ));
PUBLIC UINT4
ElpsPortRmDeRegisterProtocols PROTO ((VOID));
PUBLIC INT4
ElpsPortSendChannelCode (UINT4 u4ChannelCode);
/* HITLESS RESTART */
UINT1
ElpsPortRmGetHRFlag (VOID);
INT1
ElpsRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER *pBuf, UINT4 u4PktLen,
        UINT2 u2Port, UINT4 u4TimeOut);

/*--------------------------------------------------------------------------*/
/*                       elpsque.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 ElpsQueEnqMsg PROTO ((tElpsQMsg *));
PUBLIC VOID ElpsQuePostEventToAPSTxTask PROTO ((VOID));
PUBLIC VOID ElpsQueMsgHandler PROTO ((VOID));
    
/*--------------------------------------------------------------------------*/
/*                       elpstrc.c                                          */
/*--------------------------------------------------------------------------*/
VOID
ElpsTrcTrace (tElpsContextInfo *pContextInfo, 
         UINT4 u4Flags, const char *fmt, ...);
VOID
ElpsTrcApsPktDumpTrc (tElpsContextInfo *, UINT1 *pu1Buf, UINT4 u4Length);
VOID
ElpsTrcPscPktDumpTrc (tElpsContextInfo *, tCRU_BUF_CHAIN_HEADER *pu1Buf,
                      UINT1  u1Offset);

/*--------------------------------------------------------------------------*/
/*                       elpstrap.c                                        */
/*--------------------------------------------------------------------------*/
PUBLIC VOID
ElpsTrapSendTrapNotifications (tElpsNotifyInfo *pNotifyInfo, UINT1 u1TrapType);

#ifdef MBSM_WANTED
/*--------------------------------------------------------------------------*/
/*                       elpsmbsm.c                                        */
/*--------------------------------------------------------------------------*/
PUBLIC VOID ElpsMbsmHandleLCEvent PROTO ((tMbsmProtoMsg *, UINT4));

#endif /* MBSM_WANTED */
#ifdef L2RED_WANTED
/*--------------------------------------------------------------------------*/
/*                       elpsred.c                                          */
/*--------------------------------------------------------------------------*/
PUBLIC INT4 
ElpsRedInitGlobalInfo PROTO ((VOID));
PUBLIC VOID
ElpsRedDeInitGlobalInfo PROTO ((VOID));
PUBLIC INT4
ElpsRedRmCallBack PROTO ((UINT1, tRmMsg *, UINT2));
PUBLIC VOID
ElpsRedHandleRmEvents PROTO ((tElpsQMsg *));
PUBLIC VOID
ElpsRedHandleGoActive PROTO ((VOID));
PUBLIC VOID
ElpsRedHandleGoStandby PROTO ((VOID));
PUBLIC VOID
ElpsRedHandleStandbyToActive PROTO ((VOID));
PUBLIC VOID
ElpsRedHandleActiveToStandby PROTO ((VOID));
PUBLIC VOID
ElpsRedHandleIdleToActive PROTO ((VOID));
PUBLIC VOID
ElpsRedHandleIdleToStandby PROTO ((VOID));
PUBLIC VOID
ElpsRedProcessPeerMsgAtActive PROTO ((tRmMsg * , UINT2 ));
PUBLIC VOID
ElpsRedProcessPeerMsgAtStandby PROTO ((tRmMsg * , UINT2 ));
PUBLIC VOID
ElpsRedProcessBulkTailMsg PROTO ((tRmMsg * , UINT2 *));
PUBLIC VOID
ElpsRedRelinquishSystemTime PROTO ((VOID));
PUBLIC VOID
ElpsRedSendDynamicBulkMsg PROTO ((VOID));
PUBLIC VOID
ElpsRedSendPgDynamicBulkMsg PROTO ((VOID));
PUBLIC tRmMsg *
ElpsRedGetMsgBuffer PROTO ((UINT2 ));
PUBLIC INT4
ElpsRedApplySysTimeRelinquish PROTO ((tOsixSysTime *, tRmMsg **, UINT2 * ));
PUBLIC VOID
ElpsRedSendBulkUpdTailMsg PROTO ((VOID));
PUBLIC VOID
ElpsRedSendBulkReqMsg PROTO ((VOID));
PUBLIC VOID
ElpsRedSendDynamicSyncMsg PROTO ((tElpsPgInfo *, UINT1 , UINT1 ));
PUBLIC VOID
ElpsRedFormSemInfoSyncMsg PROTO ((tElpsPgInfo *, tRmMsg *, UINT2 *,
                                   UINT1 ));
PUBLIC VOID
ElpsRedFormRemTimeSyncMsg PROTO ((tElpsPgInfo *, tRmMsg *, UINT2 *,
                            UINT1 , UINT4 ));
PUBLIC VOID
ElpsRedProcessDynSyncSemInfoMsg PROTO ((tRmMsg *, UINT2 * , UINT2));
PUBLIC VOID
ElpsRedProcessDynSyncRemTimeMsg PROTO ((tRmMsg *, UINT2 *, UINT2));
PUBLIC INT4
ElpsRedSendMsgToRm PROTO ((tRmMsg *, UINT2 ));
PUBLIC VOID 
ElpsRedInitHardwareAudit PROTO ((VOID));
PUBLIC VOID
ElpsRedHwAuditTask PROTO ((VOID));
PUBLIC VOID
ElpsRedHwAudCrtNpSyncBufferTable PROTO ((VOID));
PUBLIC VOID
ElpsRedHwAudUpdateNpSyncBuffer PROTO ((unNpSync * , UINT4 ,
                                       UINT4));
PUBLIC VOID
ElpsRedHwAuditHandlePgAudit PROTO ((UINT4 , UINT4 , UINT4 ));
PUBLIC VOID
ElpsRedHwAudHandleContextAudit PROTO ((UINT4 , UINT4 ));
PUBLIC VOID
ElpsRedHwAuditFindAndUpdNpSync PROTO ((tElpsPgInfo *, UINT1 ));
PUBLIC VOID
ElpsRedHwAuditIncBlkCounter PROTO ((VOID));
PUBLIC VOID
ElpsRedHwAuditDecBlkCounter PROTO ((VOID));
PUBLIC UINT1
ElpsRedGetNodeState PROTO ((VOID)); 

/* HITLESS RESTART */
VOID ElpsRedHRProcStdyStPktReq  (VOID);
INT1 ElpsRedHRSendStdyStTailMsg (VOID);

/*--------------------------------------------------------------------------*/
/*                       elpshwad.c                                         */
/*--------------------------------------------------------------------------*/
PUBLIC VOID
ElpsHwAdSendPgNpSync PROTO ((UINT4 , UINT4 , UINT4 ));
PUBLIC VOID
ElpsHwAdSendCxtSync PROTO ((UINT4 , UINT4 ));
PUBLIC VOID
ElpsHwAdFormAndSendPgNpSync PROTO ((tElpsNpSyncPgEntry, UINT4 , UINT4 ));
PUBLIC VOID
ElpsHwAdFormAndSendCxtNpSync PROTO ((tElpsNpSyncCxtEntry ,UINT4 , UINT4));
PUBLIC VOID
ElpsHwAdProcessNpSyncMsg PROTO ((tRmMsg * , UINT2 *));
#else
/* These functions are called from functions defined outside elpsred.c file. 
 * These macros are defined so that neither L2RED_WANTED switch is used nor 
 * stubs are defined for these functions.
 */
/*--------------------------------------------------------------------------*/
/*                       elpsred.c                                          */
/*--------------------------------------------------------------------------*/
#define ElpsRedInitGlobalInfo() OSIX_SUCCESS
#define ElpsRedDeInitGlobalInfo()
#define ElpsRedHwAuditIncBlkCounter()
#define ElpsRedHwAuditDecBlkCounter()
#define ElpsHwAdSendCxtSync(u4ContextId, u4ModStatus)
#define ElpsHwAdSendPgNpSync(u4ContextId, u4PgId, u4PgStatus)
#define ElpsRedGetNodeState() RM_ACTIVE
#define ElpsRedSendDynamicSyncMsg(pPg, u1TimerType, u1TmrEvent)
#define ElpsRedHandleRmEvents(pMsg)
#define ElpsRedHRProcStdyStPktReq()
#define ElpsRedHRSendStdyStTailMsg() 
#endif
#endif /* _ELPSPROT_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file elpsprot.h                      */
/*-----------------------------------------------------------------------*/
