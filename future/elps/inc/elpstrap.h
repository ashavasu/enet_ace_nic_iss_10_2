/********************************************************************
 * Copyright (C) Aricent Inc . All Rights Reserved
 *
 * $Id: elpstrap.h,v 1.3 2010/10/22 14:11:31 prabuc Exp $
 *
 * Description: This file contains trap data structures defined for
 *              ELPS module.
 *********************************************************************/

#ifndef _ELPS_TRAP_H
#define _ELPS_TRAP_H

#define ELPS_OBJECT_NAME_LEN 256

#define ELPS_SNMPV2_TRAP_OID_LEN 10


enum
{
    ELPS_AUTO_PROT_SWITCH_TRAP = 1,
    ELPS_FORCED_SWITCH_TRAP,
    ELPS_MANUAL_SWITCH_TRAP,
    ELPS_PROT_TYPE_MISMATCH_TRAP,
    ELPS_CONFIG_MISMATCH_TRAP,
    ELPS_LACK_OF_RESPONSE_TRAP
};

typedef struct ElpsNotifyInfo
{
    UINT4 u4ContextId;
    UINT4 u4PgId;
    UINT1 u1PgStatus;
    UINT1 au1Pad[3];
}tElpsNotifyInfo;

VOID 
ElpsSendNotifications PROTO ((tElpsNotifyInfo *NotifyInfo, UINT1 u1TrapType));

tSNMP_OID_TYPE     *
ElpsMakeObjIdFrmString PROTO ((INT1 *pi1TextStr, UINT1 *pu1TableName));

INT4
ElpsParseSubIdNew PROTO ((UINT1 **ppu1TempPtr, UINT4 *pu4Value));

INT4
ElpsSendPgStatusNotifications PROTO ((tSNMP_VAR_BIND  *pVbList, tElpsNotifyInfo
                               *pNotifyInfo, UINT1 u1TrapType));
#endif

