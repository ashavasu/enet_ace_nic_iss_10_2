/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsextn.h,v 1.4 2010/10/22 14:11:31 prabuc Exp $
 *
 * Description: This file contains the exten variables refered across ELPS.
 *****************************************************************************/
#ifndef _ELPSEXTN_H
#define _ELPSEXTN_H

extern tElpsGlobalInfo         gElpsGlobalInfo;
extern tElpsGlobalApsTxInfo    gElpsGlobalApsTxInfo;
extern tMacAddr                gNullMacAddr;
extern UINT1                   gau1ApsPdu[ELPS_APS_PDU_LEN];
extern UINT1 gaau1EventString[ELPS_MAX_REQUESTS][ELPS_MAX_STR_LEN];

extern UINT4 gau4TnlTableOid[ELPS_MAX_OID_LEN];
extern UINT4 gu4TnlTableBaseOidLen;
extern UINT4 gau4PwTableOid[ELPS_MAX_OID_LEN];
extern UINT4 gu4PwTableBaseOidLen;

extern UINT1 gaau1StateString[ELPS_MAX_STATES][ELPS_MAX_STR_LEN];
extern UINT4 gau4ElpsRedPgStatusChgToActMap[ELPS_PG_MAX_STATUS]
[ELPS_PG_MAX_STATUS];

/* Sizing Params Structure */
extern tFsModSizingParams gFsElpsSizingParams []; 
extern tFsModSizingInfo gFsElpsSizingInfo;
#endif /* _ELPSEXTN_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file elpsextn.h                      */
/*-----------------------------------------------------------------------*/
