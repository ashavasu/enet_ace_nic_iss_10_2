/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsglob.h,v 1.6 2010/10/23 10:53:35 prabuc Exp $
 *
 * Description: This file contains the global variables refered across ELPS.
 *****************************************************************************/
#ifndef _ELPSGLOB_H
#define _ELPSGLOB_H

tElpsGlobalInfo         gElpsGlobalInfo;
tElpsGlobalApsTxInfo    gElpsGlobalApsTxInfo;
tMacAddr                gNullMacAddr;
UINT1                   gau1ApsPdu[ELPS_APS_PDU_LEN];
/* Variable use to form the Row pointer of the MPLS-TP LSP/PW path */
UINT4 gau4TnlTableOid[ELPS_MAX_OID_LEN]; 
UINT4 gu4TnlTableBaseOidLen = 0; 
UINT4 gau4PwTableOid[ELPS_MAX_OID_LEN]; 
UINT4 gu4PwTableBaseOidLen = 0; 

UINT1 gaau1EventString[ELPS_MAX_REQUESTS][ELPS_MAX_STR_LEN] = {
    "[Local Request] Lockout",
    "[Local Request] Forced Switch",
    "[Local Request] SF on Working",
    "[Local Request] Working recovers from SF",
    "[Local Request] SF on Protection",
    "[Local Request] Protection recovers from SF",
    "[Local Request] Manual Switch to Protection",
    "[Local Request] Manual Switch to Working",
    "[Local Request] Clear",
    "[Local Request] Exercise",
    "[Local Request] Wait To Restore timer Expires",
    "[Far End Request] Lockout",
    "[Far End Request] SF on Protection",
    "[Far End Request] Force Switch",
    "[Far End Request] SF on Working",
    "[Far End Request] Manual Switch to Protection",
    "[Far End Request] Manual Switch to Working",
    "[Far End Request] Wait To Restore",
    "[Far End Request] Exercise on Working",
    "[Far End Request] Exercise on Protection",
    "[Far End Request] Reverse Request on Working",
    "[Far End Request] Reverse Request on Protection",
    "[Far End Request] No Request on Working",
    "[Far End Request] No Request on Protection",
    "[Far End Request] Do Not Revert"
};
UINT1 gaau1StateString[ELPS_MAX_STATES][ELPS_MAX_STR_LEN] = {
    "No Request [working/active,protection/standby]",
    "No Request [working/standby,protection/active]",
    "Lockout [working/active,protection/standby]",
    "ForceSwitch [working/standby,protection/active]",
    "Signal Fail(Working) [working/standby,protection/active]",
    "Signal Fail(Protection) [working/active,protection/standby]",
    "ManualSwitch [working/standby,protection/active]",
    "ManualSwitch [working/active,protection/standby]",
    "WaitToRestore [working/standby,protection/active]",
    "DoNotRevert [working/standby,protection/active]",
    "Exercise [working/active,protection/standby]",
    "Exercise [working/standby,protection/active]",
    "Reverse Request [working/active, protection/standby]",
    "Reverse Request [working/standby, protection/active]"
};
#ifdef L2RED_WANTED
/* Mapping between Pg Status change and NPAPI Action */
UINT4 gau4ElpsRedPgStatusChgToActMap[ELPS_PG_MAX_STATUS][ELPS_PG_MAX_STATUS] =
{
    /* Old Software Pg Status - ELPS_PG_STATUS_DISABLE */
    /* When Pg Status is disabled, the protection group is not in service or 
     * ELPS module is disabled in the context.
     */
    {
        ELPS_PG_STATUS_INVALID, /* New Pg Status - ELPS_PG_STATUS_DISABLE, 
                                   No NP Call made, hence the Action is 
                                   ELPS_PG_STATUS_INVALID */

        ELPS_PG_CREATE,         /* New Pg Status - ELPS_PG_STATUS_WORK_PATH_ACTIVE,
                                   NP call made with the Action ELPS_PG_CREATE */

        ELPS_PG_STATUS_INVALID, /* New Pg Status - ELPS_PG_STATUS_PROT_PATH_ACTIVE, 
                                   this transition is not possible as the default
                                   path when Pg made active is Working path. */ 

        ELPS_PG_STATUS_INVALID, /* New Pg Status - ELPS_PG_STATUS_WTR_STATE,
                                   this transition is not possible as the default
                                   path when Pg made active is Working path. */

        ELPS_PG_STATUS_INVALID, /* New Pg Status - ELPS_PG_STATUS_HOLD_OFF_STATE,
                                   this transition is not possible as the default
                                   path when Pg made active is Working path. */

        ELPS_PG_STATUS_INVALID  /* New Pg Status - ELPS_PG_STATUS_SWITCHING_FAILED,
                                   As the NP Call failed, no need to identify the 
                                   action here. */
    },

    /* Old Software Pg Status - ELPS_PG_STATUS_WORK_PATH_ACTIVE */
    {
        ELPS_PG_DELETE,               /* New Pg Status - ELPS_PG_STATUS_DISABLE, 
                                         Pg Disabled in software, hence NP call made 
                                         with action ELPS_PG_DELETE. */ 

        ELPS_PG_STATUS_INVALID,       /* New Pg Status - ELPS_PG_STATUS_WORK_PATH_ACTIVE, 
                                         Transition is from Working path to Working path, 
                                         hence No NP Call made. */

        ELPS_PG_SWITCH_TO_PROTECTION, /* New Pg Status - ELPS_PG_STATUS_PROT_PATH_ACTIVE,
                                         Transition is from Working path to Protection path,
                                         hence NP Call made with Action 
                                         ELPS_PG_SWITCH_TO_PROTECTION */

        ELPS_PG_STATUS_INVALID,       /* New Pg Status - ELPS_PG_STATUS_WTR_STATE, 
                                         Transition is from Working path to Working path, 
                                         hence No NP Call made. */

        ELPS_PG_STATUS_INVALID,       /* New Pg Status - ELPS_PG_STATUS_HOLD_OFF_STATE,
                                         Transition is from Working path to Working path, 
                                         hence No NP Call made. */

        ELPS_PG_STATUS_INVALID        /* New Pg Status - ELPS_PG_STATUS_SWITCHING_FAILED, 
                                         As the NP Call failed, no need to identify the 
                                         action here. */
    }, 

    /* Old Software Pg Status - ELPS_PG_STATUS_PROT_PATH_ACTIVE */
    {
        ELPS_PG_DELETE,             /* New Pg Status - ELPS_PG_STATUS_DISABLE,
                                       Pg Disabled in software, hence NP call made
                                       with action ELPS_PG_DELETE. */

        ELPS_PG_SWITCH_TO_WORKING,  /* New Pg Status - ELPS_PG_STATUS_WORK_PATH_ACTIVE,
                                       Transition is from Protection path to Working path,
                                       hence NP Call made with Action 
                                       ELPS_PG_SWITCH_TO_WORKING. */

        ELPS_PG_STATUS_INVALID,     /* New Pg Status - ELPS_PG_STATUS_PROT_PATH_ACTIVE,
                                       Transition is from Protection path to Protection 
                                       path, hence No NP Call made. */

        ELPS_PG_STATUS_INVALID,     /* New Pg Status - ELPS_PG_STATUS_WTR_STATE,
                                       Transition is from Protection path to Protection
                                       path, hence No NP Call made. */

        ELPS_PG_STATUS_INVALID,     /* New Pg Status - ELPS_PG_STATUS_HOLD_OFF_STATE,
                                       This Transition is not possible in software,
                                       hence no NP Call made. */

        ELPS_PG_STATUS_INVALID      /* New Pg Status - ELPS_PG_STATUS_SWITCHING_FAILED,
                                       As the NP Call failed, no need to identify the
                                       action here. */
    },

    /* Old Software Pg Status - ELPS_PG_STATUS_WTR_STATE */
    {
        ELPS_PG_DELETE,             /* New Pg Status - ELPS_PG_STATUS_DISABLE,
                                       Pg Disabled in software, hence NP call made
                                       with action ELPS_PG_DELETE. */

        ELPS_PG_SWITCH_TO_WORKING,  /* New Pg Status - ELPS_PG_STATUS_WORK_PATH_ACTIVE,
                                       Transition is from Protection path to Working path,
                                       hence NP Call made with Action
                                       ELPS_PG_SWITCH_TO_WORKING. */

        ELPS_PG_STATUS_INVALID,     /* New Pg Status - ELPS_PG_STATUS_PROT_PATH_ACTIVE,
                                       Transition is from Protection path to Protection
                                       path, hence No NP Call made. */

        ELPS_PG_STATUS_INVALID,     /* New Pg Status - ELPS_PG_STATUS_WTR_STATE,
                                       Transition is from Protection path to Protection
                                       path, hence No NP Call made. */

        ELPS_PG_STATUS_INVALID,     /* New Pg Status - ELPS_PG_STATUS_HOLD_OFF_STATE,
                                       This Transition is not possible in software,
                                       hence no NP Call made. */

        ELPS_PG_STATUS_INVALID      /* New Pg Status - ELPS_PG_STATUS_SWITCHING_FAILED,
                                       As the NP Call failed, no need to identify the
                                       action here. */
    },

    /* Old Software Pg Status - ELPS_PG_STATUS_HOLD_OFF_STATE */
    {
        ELPS_PG_DELETE,               /* New Pg Status - ELPS_PG_STATUS_DISABLE,
                                         Pg Disabled in software, hence NP call made
                                         with action ELPS_PG_DELETE. */

        ELPS_PG_STATUS_INVALID,       /* New Pg Status - ELPS_PG_STATUS_WORK_PATH_ACTIVE,
                                         Transition is from Working path to Working path,
                                         hence No NP Call made. */

        ELPS_PG_SWITCH_TO_PROTECTION, /* New Pg Status - ELPS_PG_STATUS_PROT_PATH_ACTIVE,
                                         Transition is from Working path to 
                                         Protection path, hence NP Call made 
                                         with Action 
                                         ELPS_PG_SWITCH_TO_PROTECTION */

        ELPS_PG_STATUS_INVALID,       /* New Pg Status - ELPS_PG_STATUS_WTR_STATE,
                                         This Transition is not possible in software,
                                         hence no NP Call made. */

        ELPS_PG_STATUS_INVALID,       /* New Pg Status - ELPS_PG_STATUS_HOLD_OFF_STATE,
                                         Transition is from Working path to Working path,
                                         hence No NP Call made. */

        ELPS_PG_STATUS_INVALID        /* New Pg Status - ELPS_PG_STATUS_SWITCHING_FAILED,
                                         As the NP Call failed, no need to identify the
                                         action here. */
    },

    /* Old Software Pg Status - ELPS_PG_STATUS_SWITCHING_FAILED */
    {
        ELPS_PG_DELETE,               /* New Pg Status - ELPS_PG_STATUS_DISABLE,
                                         Pg Disabled in software, hence NP call made
                                         with action ELPS_PG_DELETE. */ 

        ELPS_PG_SWITCH_TO_WORKING,    /* New Pg Status - ELPS_PG_STATUS_WORK_PATH_ACTIVE,
                                         As the hardware call failed, the current path 
                                         in the software will be set in hardware, so 
                                         NP Call made with action 
                                         ELPS_PG_SWITCH_TO_WORKING. */

        ELPS_PG_SWITCH_TO_PROTECTION, /* New Pg Status - ELPS_PG_STATUS_PROT_PATH_ACTIVE,
                                         As the hardware call failed, the current path
                                         in the software will be set in hardware, so
                                         NP Call made with action
                                         ELPS_PG_SWITCH_TO_PROTECTION. */

        ELPS_PG_SWITCH_TO_PROTECTION, /* New Pg Status - ELPS_PG_STATUS_WTR_STATE,
                                         As the hardware call failed, the current path
                                         in the software will be set in hardware, so
                                         NP Call made with action
                                         ELPS_PG_SWITCH_TO_PROTECTION. */

        ELPS_PG_STATUS_INVALID,       /* New Pg Status - ELPS_PG_STATUS_HOLD_OFF_STATE,
                                         In Hold-Off state, NP call will not be made, 
                                         hence ELPS_PG_STATUS_INVALID. */

        ELPS_PG_STATUS_INVALID        /* New Pg Status - ELPS_PG_STATUS_SWITCHING_FAILED,
                                         As the NP Call failed, no need to identify the
                                         action here. */
    }
};
#endif


#endif /* _ELPSGLOB_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file elpsglob.h                      */
/*-----------------------------------------------------------------------*/
