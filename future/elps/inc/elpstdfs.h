/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstdfs.h,v 1.15 2013/04/29 13:16:57 siva Exp $
 *
 * Description: This file contains data structures defined for
 *              ELPS module.
 *********************************************************************/
#ifndef _ELPS_TDFS_H
#define _ELPS_TDFS_H

/*------------------------------------------------------------------------
 *           Common Enumerations
 *-----------------------------------------------------------------------*/
enum 
{
    ELPS_HOLD_OFF_TMR = 0,
    ELPS_WTR_TMR = 1,
    ELPS_APS_PERIODIC_TMR = 2,
    ELPS_LOR_TMR = 3,
    ELPS_HOLD_OFF_TMR_P = 4,
    ELPS_MAX_TMR_TYPES = 5
};

/* Used for WorkingStatus or ProtectionStatus */
enum
{
        ELPS_SIGNAL_NULL   = 0,
        ELPS_SIGNAL_NORMAL = 1
};

typedef struct _tElpsPgInfo tElpsPgInfo;

/*------------------------------------------------------------------------
 *           CFM Monitor Identification information
 *-----------------------------------------------------------------------*/
typedef struct ElpsCfmMonitorInfo
{
    UINT4    u4MegId; /* Maintainence Entity Group Id*/
    UINT4    u4MeId;  /* Maintainence Entity Id*/
    UINT4    u4MepId; /* Maintainence End Point Id */ 
}tElpsCfmMonitorInfo;

/* Data structures used for Messaging/Interface with RM  */
#ifdef L2RED_WANTED
typedef struct _ElpsRmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tElpsRmCtrlMsg;
#endif /* L2RED_WANTED */

/* Data Structure for NP-Sync Buffer Table */
typedef tNpSyncBufferEntry tElpsRedNpSyncEntry;

/*------------------------------------------------------------------------
 *           Message Q Structures
 *-----------------------------------------------------------------------*/
/* Following structure is used to send the received APS pdu to the ELPS 
 * queue.*/
typedef struct ElpsQApsPduMsg
{
    tElpsCfmMonitorInfo  MonitorInfo;
                            /* MegId,MeId and MepId */
    tCRU_BUF_CHAIN_DESC *pApsPdu; 
                            /* Pointer to the APS PDU buffer */
    UINT1                u1TlvOffset;
    UINT1                au1Pad[3];
}tElpsQApsPduMsg;

/* Following structure is used to send the monitor status indication
 * to ELPS queue */
typedef struct ElpsQMonitorStatus
{
    tElpsCfmMonitorInfo  MonitorInfo;
                            /* MegId,MeId and MepId */
    UINT4                u4Status;
                            /* Cfm monitor status information*/
}tElpsQMonitorStatus;

/* This is the main ELPS queue message structure */
typedef struct ElpsQMsg
{
    UINT4    u4MsgType;  /* Message type */
    UINT4    u4ContextId;/* Virtual context id maintained by VCM module */
    union
    {
        tElpsQApsPduMsg        ApsPduMsg;
        tElpsQMonitorStatus    MonitorStatus;
#ifdef MBSM_WANTED
        tMbsmProtoMsg         *pMbsmProtoMsg; /* MbsmProtoMsg received from 
                                                 MBSM */
#endif
#ifdef L2RED_WANTED
        tElpsRmCtrlMsg         RmCtrlMsg; /* Control message from RM */
#endif
    }unMsgParam;
    UINT1                u1ServiceType;
    UINT1                u1ModId;
    UINT1                au1Pad[2];
}tElpsQMsg;

/*------------------------------------------------------------------------
 *           Context Info Structures
 * This structure is used to maintain the per context basis ELPS 
 * information.
 *----------------------------------------------------------------------- */
typedef struct ElpsContextInfo
{
    tRBTree     PgInfoTbl;
                    /* RBTree head for tElpsPgInfo structure.
                     * PG configuration information are stored in this table.*/
    tRBTree     CfmConfigTbl; 
                    /* RBTree head for tElpsCfmConfigInfo structure.
                     * CFM Monitoring configuration and its mapping with a
                     * PG information are stored in this table. */
    tRBTree     PgServiceListTbl;
                    /* RBTree head for tElpsServiceListInfo structure.
                     * This table contains the information  about those 
                     * PG which contains list of working vlan entities. */
    tRBTree     PgServiceListPointerTbl;
                    /* RBTree head for tElpsServiceListPointerInfo structure.
                     * This table contains the information  about those 
                     * PG which contains list of working/protection forward & reverse
                     * LSP entities. */
    tRBTree     PgShareTbl; 
                    /* RBTree head for the tElpsPgShareInfo structure.
                     * This table contains the information about those
                     * protectection group nodes status, which are 
                     * sharing the same port as their protection port.*/
    UINT4       u4ContextId;
                    /* Virtual Context ID maintained by the VCM module */
    UINT4       u4TraceInput;
                    /* Maintain the trace/debug input flag */
    INT4        i4VlanGroupManager;
                    /* Specifies if mstp or elps module manages the grouping of vlans*/
    UINT1       u1ModuleStatus; 
                    /* Module Enable or Disable status */
    UINT1       u1TrapStatusFlag; 
                    /* Trap Enable or Disable status */
    UINT1       au1Pad[2];
}tElpsContextInfo;

/*------------------------------------------------------------------------
 *           Protection Group Information
 * This section contains the datastructures used to maintained the 
 * protection group related information.
 *-----------------------------------------------------------------------*/

/* This structure contains the APS State Event Machine related information 
 * for a protection group */
typedef struct ElpsSemState
{
    UINT4    u4WorkingStatus;
    UINT4    u4ProtectionStatus;
    UINT4    u4RequestOrState; /* APS information */
    UINT4    u4RequestedSignal;/* APS information */
    UINT4    u4BridgedSignal;  /* APS information */
}tElpsSemState;

/* This structure contains the PSC State Event Machine related information 
 * for a protection group */
typedef struct ElpsPscSemState
{
    UINT4    u4WorkingStatus;
    UINT4    u4ProtectionStatus;
    UINT4    u4Request; /* PSC information */
    UINT4    u4FaultPath;/* PSC information */
    UINT4    u4Path;  /* PSC information */
}tElpsPscSemState;

/* This structure contains the APS specific information that is used in the
 * APS PDU information */
typedef struct ElpsApsInfo
{
    UINT1    u1ProtectionType;
               /* A,B,D,R bit as required in APS PDU.
                * A bit - with or without APS
                * B bit - 1+1 or 1:1 bridging
                * D bit - Uni directional or Bi-directional switching
                * R bit - non-revertive or revertive operation(Operation Type)*/
    UINT1    u1ApsSemStateId;
               /* APS State Event Machine current state Id. 
                * RequestOrStat, RequestedSignal, BridgedSignal are the
                * information required here as APS info, which is maintained by
                * the APS state event machine. By using this sem id those
                * information can be retrived from the APS sem. */
    UINT1    au1Pad[2];
}tElpsApsInfo;

typedef struct ElpsLspInfo
{
   UINT4      u4TnlIndex;
   UINT4      u4TnlInstance;
   UINT4      u4TnlIngressLsrId;
   UINT4      u4TnlEgressLsrId;
   UINT4      u4WorkingHwTnlEgrIf;
   UINT4      u4ProtectionHwTnlEgrIf;
}tElpsLspInfo;

typedef struct ElpsPwInfo
{
    UINT4  u4PeerAddr; /* Peer Address */
    UINT4  u4VcId;      /* Virtual Circuit Identifier */
    UINT4  u4PwIndex;   /* Pseudowire Index */
}tElpsPwInfo;

typedef union ServiceParams
{
    UINT4      u4ServiceId;
    /* working service Id or protection service id  
     * of VLAN */
    tElpsLspInfo   LspServiceInfo;
    /* working service Id or protection service id  
     * of mplstp-LSP*/
    tElpsPwInfo    PwServiceInfo;
    /* working service Id or protection service id  
     * of mplstp-PW*/
}unServiceParam;


/*------------------------------------------------------------------------
 *           Cfm Configuration Information
 * This structure is used to store the CFM related configuration.
 *----------------------------------------------------------------------- */
typedef struct ElpsCfmConfigInfo
{
    tRBNodeEmbd         CfmConfigRBNode; 
                            /* Embedded RBTree Node which is
                             * associated with the RBTree head CfmConfigTbl. */
    tElpsPgInfo        *pPgInfo;
                            /* points to the associated protection group 
                             * information */
    tElpsCfmMonitorInfo MonitorInfo;
                            /* Index of the table CfmConfigTbl. */
    UINT1               u1EntityType; 
                            /* Entity type: Working or Protection*/
    UINT1               u1ServiceType; 
                            /* Entity type: Service Type*/
    UINT1               u1ModId; 
                            /* Entity type: Module Id*/
    UINT1               u1Direction;
                            /* Forward or Reverse or Both */
    UINT1               u1Status;
                            /* Status of the MEG- UP/DOWN */
    UINT1               au1Pad[3];
}tElpsCfmConfigInfo;

/*------------------------------------------------------------------------
 *           Service List Information
 * If for a single protection group, multiple working vlan is configured
 * then those vlan list information is stored in this table.
 *----------------------------------------------------------------------- */
typedef struct ElpsServiceListInfo
{
    tRBNodeEmbd  PgServiceListRBNode;
                    /* Embedded RBTree Node which is associated with the 
                     * RBTree head PgServiceListTbl */
    UINT4        u4PgId; 
                    /* First Index of this table. */
    UINT4        u4ServiceId;
                    /* Second Index of this table. 
                     * Service Id(normaly VlanId/mplstp-PW)*/
    UINT1        u1RowStatus;
                    /* Row status object for Service List info */
    UINT1        au1Pad[3];
}tElpsServiceListInfo;

typedef struct ElpsServiceListPointerInfo
{
    tRBNodeEmbd  PgServiceListPointerRBNode;
                    /* Embedded RBTree Node which is associated with the 
                     * RBTree head PgServiceListPointerTbl */
    unServiceParam WorkingInfo;
                     /* Working Service value */
    unServiceParam WorkingRvrInfo;
                     /* Working Reverse Service */
    unServiceParam ProtectionInfo;
                     /* Protection Service value */
    unServiceParam ProtectionRvrInfo;
                     /* Protection Reverse Service value */
    UINT4            u4PgId; 
                    /* First Index of this table. */
    UINT4            u4PgServiceListId; 
                    /* Second Index of this table. */
    UINT1            u1RowStatus;
                    /* Row status object for Service List info */
    UINT1            au1Pad[3];
}tElpsServiceListPointerInfo;

/* This structure is used to store the request and its status */
typedef struct ElpsRequest
{
    UINT1     u1Request;
                /* Received Request */
    UINT1     u1RequestStatus;
                /* Accepted/Overruled/NotApplicable */
    UINT1     au1Pad[2];
}tElpsRequest;



/* This structure is used to maintain the entity information. */
typedef struct ElpsEntity
{
    tElpsCfmConfigInfo  *pCfmConfig;
                /* CFM configuration information associated with this 
                 * entity (working or protection entity)*/
    unServiceParam ServiceParam;
                 /* Service Value Info */
    UINT4      u4PortId;
                /* working or protection id. */
}tElpsEntity;



/* This structure is used to maintain the statistics information for a PG */
typedef struct ElpsStats
{
    UINT4     u4AutoSwitchCount;
                /* Number of automation switching happened in this protection
                 * group */
    UINT4     u4ForceSwitchCount;
                /* Number of force switch executed in this protection group*/
    UINT4     u4ManualSwitchCount;
                /* Number of manual switch executed in this protection group*/
    UINT4     u4TotalApsTxCount;
                /* Total Number of APS/PSC packets attempted to be transmitted 
                 * by this protection group. */
    UINT4     u4TotalApsRxCount;
               /* Total Number of APS/PSC Packets received for this protection 
                * group. */
    UINT4     u4TotalApsDiscardCount;
               /* Total Number of APS/PSC Packets discarded for this protection 
                * group. */
}tElpsStats;

/* This structure is used to maintain the performance measurement for a PG */
typedef struct ElpsPerf
{
    UINT4     u4LRSFRxTime;
                 /* Time (T0) when receiving Local Request - signal fail message.
                  * This is detected in local node and it is not got from pkt.*/
    UINT4     u4LRSFTxTime;
                 /* Time (T1)  when sending Local Request - signal fail pkt.*/
    UINT4     u4FRSFRxTime;
                /* Time (T2) when receiving Far end Request - signal fail pkt.*/
    UINT4     u4StateChgTime;
                /* Time (T3) when the protection path becomes active after processing 
                 * the signal fail pkt that is received from the peer.*/
}tElpsPerf;

/* This is the main structure that maintains the protection group information*/
struct _tElpsPgInfo
{
    tRBNodeEmbd   PgInfoRBNode;
                    /* Embedded RBTree Node, which creates the PgInfoTbl */
    tRBNodeEmbd   PgShareRBNode;
                    /* Embedded RBTree Node which is associated with the
                     * RBTree head PgShareTbl. This is used for implementing
                     * the PG Share info table */
    tElpsEntity   WorkingEntity;
                    /* contains the working port, working vlan/PW/LSP and cfm info */
    tElpsEntity   WorkingReverseEntity;
                    /* contains the working port, working reverse LSP/PW and cfm info */
    tElpsEntity   ProtectionEntity;
                    /* contains the protection port, protection vlan/PW/LSP and
                     * cfm info */
    tElpsEntity   ProtectionReverseEntity;
                    /* contains the protection port, protection reverse LSP/PW and
                     * cfm info */
    tTmrBlk       HoldOffTimer;
                    /* Hold Off Timer block for working entity */
    tTmrBlk       ProtectionHoldOffTimer;
                    /* Hold Off Timer block for protection entity */
    tTmrBlk       WTRTimer;
                    /* Wait to restore timer block */
    tTmrBlk       PeriodicTimer;
                    /* Periodic timer block */
    tTmrBlk       LORTimer;
                    /* Lack Of Response Timer block */
    tElpsRequest  LastLocalCmd;
                    /* Last received near end external command given by the 
                     * management modules(CLI/SNMP/WEB)and the command status.
                     * Following commands are part of this - 
                     * lockout of protection, force switch, manual switch, 
                     * exercise, clear, freeze, clear freeze */
    tElpsRequest  LastLocalCond;
                    /* Last received local condition (signal indication)
                     * and its status */
    tElpsRequest  LastFarEndReq;
                    /* Last received far end command and its status */
    tElpsStats    Stats;
                    /* protection group statistics information */
    tElpsPerf     Perf;
                    /* performance measurement information */
    tElpsApsInfo  ApsInfo;
                    /* Maintains the APS Specific Information that needs to 
                     * be transmitted through the APS PDU */
    union
    {
        tElpsServiceListInfo     *pWorkingServiceList;
                    /* Points to the first service list entry 
                     * (in PgServiceListTbl) associated with this 
                     * protection group id */
        tElpsServiceListPointerInfo   *pWorkingServiceListPointer;
                    /* Points to the first service list entry 
                     * (in PgServiceListPointerTbl) associated with this 
                     * protection group id */
    }unServiceList;

    tElpsContextInfo *pContextInfo;
                    /* link to the context info structure */
    UINT4         u4PgId; 
                    /* Index of the table PgInfoTbl.*/
    UINT4         u4IngressPortId;
                    /* Ingress port Id */
    UINT4         u4WorkingInstanceId;
                    /* InstanceId used by the List of vlans on working path */
    UINT4         u4ProtectionInstanceId;
                    /* InstanceId used by the List of vlans on protection path */
    UINT2         u2HoldOffTime; 
                    /* Hold-Off timer interval in millisecond value */
    UINT2         u2WtrTime; 
                    /* Wait-to-restore timer interval in millisecond value */
    UINT2         u2PeriodicTime; 
                    /* APS Periodic timer interval in second value */
    UINT1         au1PgName[ELPS_PG_MAX_NAME_LEN + 1];
                    /* Protection Group Name */
    UINT1         u1PgConfigType; 
                    /* Protection group type: list/individual */
    UINT1         u1ServiceType;
                    /* protection service type. */
    UINT1         u1MonitorType; 
                    /* Monitoring type:  CFM (Y.1731)/MPLS-OAM */
    UINT1         u1FreezeFlag; 
                    /* This flag is used to freeze the APS state event machine.
                     * If this flag is set additional near end command are
                     * rejected, condition changes and received APS information
                     * are ignored. When this flag is reset, the state of the 
                     * protection group is recomputed based on the condition
                     * and received APS information.*/
    UINT1         u1LastActiveReq; 
                    /* This indicates the last active request which is accepted
                     * by the state event machine. If the last request is
                     * Overruled/NA/Invalid - by the SEM then this variable
                     * will not be updated. */
    UINT1         u1LastRecvdReqType;
                    /* This variable stores the last received request type */
    UINT1         u1PgStatus;
                    /* Status of the protection group.
                     * - protectionDisabled
                     * - workingPathActive
                     * - protectionPathActive
                     * - waitToRestoreState
                     * - holdOffState
                     * - switchingFailed
                     */
    UINT1         u1PgRowStatus;
                    /* Row status object for protection group info */
    UINT1         u1CfmRowStatus;
                    /* Row status object for CFM config info */
    UINT1         u1WorkingCfmStatus;
                    /* This variable is used to store the state of the 
                     * working CFM indication. This variable is used to know
                     * if SF is Reasserted in the following scenarios
                     * 1. hold-off timer is running or 
                     * 2. PG is in Lock Out state
                     * 3. PG is in Freeze state 
                     */
    UINT1         u1ProtectionCfmStatus;
                    /* This variable is used to store the state of the 
                     * Protection CFM indication. This variable is used to know
                     * if SF-P is Reasserted in the following scenarios
                     * 1. hold-off timer is running or 
                     * 2. PG is in Lock Out state
                     * 3. PG is in Freeze state 
                     */
   UINT1         u1PreviousStateId;    
                    /* Denotes previous state of the state machine */
   UINT1         u1InterMediateStateId;
                    /* Denotes the intermadiate state of the state machine 
                     * in case of 2-pass*/
   UINT1         u1PscVersion;
                    /* Denotes the PSC PDU version 
                     * 0 - Draft version,
                     * 1 - RFC Version. */
   UINT1         u1PgHwStatus; /*Hardware status of PG entry*/
   UINT1         au1Pad[2]; /* Padding */
};

/*------------------------------------------------------------------------
 *           APS PDU Transmission Information       
 * The information required to transmit APS PDU on the SEM state change 
 * of a protection group.
 *----------------------------------------------------------------------- */
typedef struct ElpsApsTxInfo
{
    tTMO_SLL_NODE        SllNode;
    tElpsCfmMonitorInfo  MonitorInfo;
                            /* MegId,MeId and MepId */
    UINT4                u4ContextId;
                           /* Virtual context id. maintained by VCM module */
    UINT4                u4PgId;
                           /* Protection Group Id */
    UINT4                u4Index;
                           /* Interface Index through which it is to be 
                            * transmitted*/
    union
    {
     UINT1                *pu1ApsPdu;
                      /* APS specific information in an APS PDU */
        tCRU_BUF_CHAIN_HEADER *pPscPdu;
                      /* PSC PDU with MPLS label*/
    }unBufferParam;

    UINT1                au1DstMac[CFA_ENET_ADDR_LEN];
                           /* Destination Mac address */
    UINT1                u1Count;
                           /* Counter variable that will be refered for sending 
                            * trap message when there is no APS PDU 
                            * transmitted (on state change). */
    UINT1                u1PgStatus;
                           /* Protection Group Status */
    UINT1                u1PgServiceType;
                           /* Protection Service Type */
    UINT1                au1Pad[3];
}tElpsApsTxInfo;

/* ------------------------------------------------------------------
 *                Global Redundancy Information
 * This structure contains all the Global Data required for ELPS
 * Redundancy (High Availability) Operation .
 * ----------------------------------------------------------------- */

typedef struct _ElpsRedGlobalInfo
{
    tOsixTaskId HwAuditTaskId;
                  /* Hardware Audit Task Id */
    tMemPoolId  HwAuditTablePoolId;
                  /* Hardware Audit Table Pool Id - Np Sync Buffer Table 
     * Pool Id. */
    tTMO_SLL    HwAuditTable;
                  /* SLL header of Hardware Audit Np-Sync Buffer Table */
    UINT1       u1NodeStatus;
                  /* Node Status - Idle/Active/Standby. 
     * Idle - Node Status on boot-up.
     * Active - Node where SEM runs and hardware programming 
     *          is done.
     * Standby - Node which is in sync with active of all the 
     *           information and becomes active on switchover
     */
    UINT1       u1NumOfPeerNodesUp;
                  /* Number of Standby nodes which are booted up. If this
     * value is 0, then dynamic messages are not sent by 
     * active node. */
    BOOL1       bBulkReqRecvd;
                  /* Bulk Request Received Flag - OSIX_TRUE/OSIX_FALSE. This 
     * flag is set when the dynamic bulk request message reaches
     * the active node before the STANDBY_UP event. Bulk updates
     * are sent only after STANDBY_UP event is reached.
     */
    UINT1       u1NpSyncBlockCount;
                  /* At Active Node - NP-Syncups are not sent when the value is 
                   * not 0.
                   * At Standby Node - NP-Syncup Buffer Table is not accessed 
                   * when the value is greater than 1.
                   * Block Counter is incremented when there is a need to block
                   * the Np Sync-ups. When there is a special event (Module 
                   * shutdown, Module Enable and Module disable per context) 
                   * are those events. And also during static, dynamic bulk 
                   * updates and Module Shutdown (for all contexts to handle 
                   * communication lost and restored scenario), the Block 
                   * counter is incremented. After the completion of these 
                   * special events the counter is decremented. The reason for 
                   * having a counter instead of flag is that during static 
                   * bulk updates, the Module enable will also be called. After
                   * the completion of Module enable, the block flag will be 
                   * reset. To avoid this the counter is used instead of flag.
                   */
    /* HITLESS RESTART */ 
    /* To check whether steady state request received from RM*/ 
    UINT1       u1StdyStReqRcvd; 
    UINT1       au1Reserved[3]; 

}tElpsRedGlobalInfo;


/* ------------------------------------------------------------------
 *                Global Information
 * This structure contains all the Global Data required for ELPS
 * Operation .
 * ----------------------------------------------------------------- */
typedef struct ElpsGlobalInfo
{
    tOsixTaskId     MainTaskId;
                        /* Main Task ID */
    tOsixQId        TaskQId;
                        /* ELPS Task Queue ID */
    tOsixSemId      SemId;
                        /* ELPS Task Semaphore ID */
    tMemPoolId      QMsgPoolId; 
                        /* Mempool Id for the Task Queue */
    tMemPoolId      ContextTablePoolId;
                        /* Mempool Id for tElpsContextInfo */
    tMemPoolId      PgTablePoolId; 
                        /* Mempool Id for tElpsPgInfo */
    tMemPoolId      ServiceListTablePoolId; 
                        /* Mempool Id for tElpsServiceListInfo */
    tMemPoolId      ServiceListPointerTablePoolId; 
                        /* Mempool Id for tElpsServiceListPointerInfo */
    tMemPoolId      CfmConfigTablePoolId; 
                        /* Mempool Id for tElpsCfmConfigInfo */
    tMemPoolId      MplsInApiPoolId; 
                        /* Mempool Id for tMplsApiInInfo */
    tMemPoolId      MplsOutApiPoolId; 
                        /* Mempool Id for tMplsApiOutInfo */
    tMemPoolId      MplsPathInfoPoolId;
                        /* Mempool Id for tMplsPathId */
    tTimerListId    TmrListId; 
                        /* Timer List Id */
    tTmrDesc        aTmrDesc[ELPS_MAX_TMR_TYPES]; 
                        /* description of all timers are stored in this 
                         * array with TimerId as the array index. */
    tElpsRedGlobalInfo  RedGlobalInfo;
                         /* Global Redundancy information. */
    tElpsContextInfo *apContextInfo[SYS_DEF_MAX_NUM_CONTEXTS];
                        /* Per vertual context specific information pointers
                         * are store in this array with context id as the
                         * array index*/
    UINT4            u4RapidTxTime;
                         /* Time interval for rapid transmission of
                          * first three APS/PSC messages upon a protection 
                          * state change in a protection group.*/
    UINT4            u4PscChannelCode;
                        /* G-ACh channel number to be used for transmitting 
     * the Protection State Coordination (PSC) control packet through
     * the generic associated channel (G-ACh) for LSP/pseudowire.*/
    UINT1            au1SystemCtrl [SYS_DEF_MAX_NUM_CONTEXTS];
                        /* Start or Shutdown status of ELPS per context */
    UINT1            u1GlobalTrcOption;
                        /* Trace Option across all context */
    UINT1            u1IsElpsInitialized;
                        /* Indicates whether ELPS module is initialised. This 
                         * is to avoid posting messages to ELPS module before 
                         * it is initialised. */
    UINT1            au1Pad[2];
}tElpsGlobalInfo;

/* ------------------------------------------------------------------
 *                Global APS Tx Information
 * This structure contains all the Global Data required for ELPS
 * Tx operation. This SLLs, BOOLEAN variables present in this structure 
 * is protected by a seperate semaphore (ApsTxInfoSemId).
 * ----------------------------------------------------------------- */
typedef struct ElpsGlobalApsTxInfo
{
    tOsixTaskId     ApsTxTaskId;
                        /* APS Transmission Task ID */
    tOsixSemId      ApsTxInfoSemId;
                        /* Semaphore ID used for protecting the 
                         * tElpsApsTxInfo and their corresponding SLLs. */
    tMemPoolId      ApsTxInfoTablePoolId; 
                        /* Mempool Id for tElpsApsTxInfo */
    tMemPoolId      ApsTxPduPoolId; 
                        /* Mempool Id for TxPDU which is stored in the 
                         * tElpsApsTxInfo */
    tTMO_SLL         ApsTxInfoList[ELPS_MAX_APS_PDU_ON_STATE_CHANGE];
                        /* SLLs that are used to achieve the transmission of 
                         * APS PDU in a time gap of 3.3 milliseconds on 
                         * state change of a PG entry. */
    tTMO_SLL        *pApsTxSll[ELPS_MAX_APS_PDU_ON_STATE_CHANGE];
                        /* Pointer to the SLLs, which is used to acheive the 
                         * faster movement of SLL entries across SLLs. */
    BOOL1            b1InApsTxEventHandler;
                        /* Flag to indicate whether the APS Task is in the   
                         * Event Handler thread. If this is True, then no 
                         * need to post an event to APS Task while adding 
                         * new APS Tx node to the SLL. */
    UINT1            au1Pad[3];
}tElpsGlobalApsTxInfo;

#endif /* _ELPS_TDFS_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpstdfs.h                     */
/*-----------------------------------------------------------------------*/
