/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsred.h,v 1.7 2011/08/11 09:38:07 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *              and prototypes to support High Availability for ELPS module.   
 *
 ******************************************************************/
#ifndef _ELPSRED_H_
#define _ELPSRED_H_

/****************************************************************************/
/* Sizable parameters                                                       */
/****************************************************************************/
/* Number of Buffer entries cannot exceed 1 at any point of time */
#define ELPS_RED_MAX_HW_AUD_NPSYNC_BUF_ENTRIES 1


#define ELPS_RED_MAX_MSG_SIZE        1500
#define ELPS_RED_TYPE_FIELD_SIZE     1
#define ELPS_RED_LEN_FIELD_SIZE      2

/* Macros to write in to RM buffer. */
#define ELPS_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define ELPS_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define ELPS_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ELPS_RM_PUT_N_BYTE(pdest, psrc, pu4Offset, u4Size) \
do { \
    RM_COPY_TO_OFFSET(pdest, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) +=u4Size; \
}while (0)

#define ELPS_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define ELPS_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define ELPS_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define ELPS_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)

/* Represents the message types encoded in the update messages */
typedef enum {
    ELPS_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    ELPS_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    ELPS_RED_DYN_SEM_INFO_MSG,
    ELPS_RED_DYN_REM_TIME_INFO_MSG,
    ELPS_RED_NP_SYNC_INFO_MSG
}eElpsRedRmMsgType;

#define ELPS_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define ELPS_RED_BULK_REQ_MSG_SIZE            3
#define ELPS_RED_DYN_SEM_INFO_VALUE_SIZE      21
#define ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE 13

/* The Threshold time for which the system Time can be used by Dynamic 
 * bulk updates. The unit of this value is in STUPS. 
 * 1 STUPS = 10 ms.
 */
#define ELPS_RED_DYN_BULK_THRESHOLD_SYS_TIME  1

#define ELPS_HW_AUDIT_TASK_PRIORITY     240 /* should have lower priority */
#define ELPS_HW_AUDIT_TASK            ((UINT1*)"ELAU")

#define ELPS_RED_HW_AUD_PG_NP_SYNC             1
#define ELPS_RED_HW_AUD_CXT_NP_SYNC            2

#endif
