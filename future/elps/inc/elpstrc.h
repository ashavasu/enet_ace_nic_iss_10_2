/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstrc.h,v 1.4 2013/12/07 10:52:34 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/
#ifndef _ELPSTRC_H_
#define _ELPSTRC_H_

#define  ELPS_TRC_FLAG  /*Global Variable for traces*/ 
#define  ELPS_MOD_NAME      "ELPS"               

#define ELPS_CLR_TRC_OPT       0

#define ELPS_ALL_TRC (INIT_SHUT_TRC | MGMT_TRC | \
                      CONTROL_PLANE_TRC | DUMP_TRC | OS_RESOURCE_TRC | \
                      ALL_FAILURE_TRC | BUFFER_TRC | ELPS_CRITICAL_TRC)

#define ELPS_INVALID_TRC   0x00000000
#define ELPS_CRITICAL_TRC  0x00000100

#define ELPS_MIN_TRC_VALUE INIT_SHUT_TRC
#define ELPS_MAX_TRC_VALUE ELPS_TRC_CRITICAL 
#define ELPS_GLB_TRC       ELPS_INVALID_CONTEXT_ID

#define ELPS_TRC_BUF_SIZE    2000

extern UINT4 gu4ElpsTrcLvl;
#define ELPS_TRC_LVL        gu4ElpsTrcLvl

#ifdef TRACE_WANTED

#define  ELPS_TRC(x)       ElpsTrcTrace x

#else /* TRACE_WANTED */

#define  ELPS_TRC(x) 

#endif /* TRACE_WANTED */


#endif /* _ELPSTRC_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpstrc.h                      */
/*-----------------------------------------------------------------------*/
