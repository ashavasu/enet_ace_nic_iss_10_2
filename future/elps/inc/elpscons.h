/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpscons.h,v 1.15 2014/03/10 12:51:26 siva Exp $
 *
 * Description: This file contains macros definitions for constants 
 *              used in ELPS module. 
 *****************************************************************************/
#ifndef _ELPSCONS_H
#define _ELPSCONS_H

#define ELPS_MAX_CONTEXTS   FsELPSSizingParams[MAX_ELPS_CONTEXTS_SIZING_ID].u4PreAllocatedUnits
#define ELPS_INVALID_CONTEXT_ID       (ELPS_MAX_CONTEXTS + 1)
#define ELPS_MAX_PORTS_PER_CONTEXT    L2IWF_MAX_PORTS_PER_CONTEXT
#define ELPS_DEFAULT_CONTEXT_ID       L2IWF_DEFAULT_CONTEXT

/****************************************************************************/
/* Sizable parameters                                                       */
/****************************************************************************/
#define ELPS_MAX_Q_DEPTH              (2 * ELPS_MAX_PORTS_PER_CONTEXT)  
#define ELPS_MAX_SIMULTANEOUS_CFM_SIGNAL  1024 /* Maximum number of CFM signals
                                                  handled simultaneously */
#define ELPS_MAX_MPLS_IN_API_COUNT    2
#define ELPS_MAX_MPLS_OUT_API_COUNT   2

/****************************************************************************/
/* General                                                                  */
/****************************************************************************/
#define ELPS_TASK_NAME                ((UINT1 *)"ELPT")
#define ELPS_APS_TX_TASK_NAME         ((UINT1 *)"APST")
#define ELPS_TASK_QUEUE_NAME          ((UINT1 *)"ELPQ")
#define ELPS_PROTO_SEM                ((UINT1 *)"ELPS")
#define ELPS_APS_TX_DB_SEM            ((UINT1 *)"APSS")

#define ELPS_PSC_DEFAULT_CHANNEL_CODE(ver,val)  \
      if(ver==1) \
           val=0x0024;\
      else \
           val=(UINT2) gElpsGlobalInfo.u4PscChannelCode;
#define ELPS_ONE 1
#define ELPS_TWO 2
#define ELPS_THREE 3
#define ELPS_FOUR 4
#define ELPS_SIX 6
#define ELPS_SEVEN 7
#define ELPS_NINE 9
#define ELPS_THIRTEEN 13

/* Events that will be received by ELPS Task */
#define ELPS_TMR_EXPIRY_EVENT         0x01 /* Bit 1 */
#define ELPS_QMSG_EVENT               0x02 /* Bit 2 */
#define ELPS_ALL_EVENTS               (ELPS_TMR_EXPIRY_EVENT | ELPS_QMSG_EVENT)

/* Events that will be received by ELPS APS Tx Task */
#define ELPS_APS_TX_EVENT             0x01

#define ELPS_QMSG_MEMBLK_COUNT        ELPS_MAX_Q_DEPTH

/* Message Types */
#define ELPS_CREATE_CONTEXT_MSG       1
#define ELPS_DELETE_CONTEXT_MSG       2
#define ELPS_APS_PDU_MSG              3
#define ELPS_CFM_SIGNAL_MSG           4
#define ELPS_RM_MSG                   5
#define ELPS_PSC_PDU_MSG              6

#define ELPS_APS_PDU_OFFSET           4

/* Others */
#define ELPS_APS_OFFSET_OF_VERSION    0
#define ELPS_APS_OFFSET_OF_OPCODE     1
#define ELPS_APS_OFFSET_OF_FLAGS      2
#define ELPS_APS_OFFSET_OF_TLV_OFFSET 3
#define ELPS_APS_OFFSET_OF_REQ_OFFSET 4
#define ELPS_APS_OFFSET_OF_END_TLV    8

#define ELPS_APS_PDU_VERSION          0
#define ELPS_APS_PDU_FLAGS            0
#define ELPS_APS_PDU_OPCODE           0x27
#define ELPS_APS_PDU_TLV_OFFSET       4
#define ELPS_APS_PDU_END_TLV          0
#define ELPS_APS_PDU_HDR_LEN          4
#define ELPS_APS_PDU_APS_INFO_LEN     4
#define ELPS_APS_PDU_LEN              12 /* MEL, Version(1) + Opcode(1) 
                                            + Flags(1) + TLV Offset(1)
                                            APS Specific Info(4) + End TLV(1) 
                                            + Padding (3) */
#define ELPS_APS_PDU_PADDING         3

/* Psc related macros */
#define ELPS_PSC_PDU_VERSION(ver,val)     if(ver==0)\
                                             val = 0x00;\
                                          else\
                                             val = 0x01; 
#define ELPS_PSC_PDU_ACH_VERSION      0x00
#define ELPS_PSC_PDU_LEN              4  /* Ver, Req, PT (1 byte) +
                                       Revertive type and reserved (1 byte) +
                                       Fault Path (1 byte) +
                                       Path (1 byte) */

#define ELPS_PSC_ACH_TLV_HDR_LEN      4  /* Length (2 bytes) + 
         reserved (2 bytes) */

#define ELPS_PSC_GACH_HDR_LEN         4  /* Version (1 byte) +
                                            Reserved (1 byte)
         PSC channel code (2 bytes) */
#define ELPS_PSC_PDU_MAX_TLV_LEN      0

#define ELPS_MAX_MPLS_PKT_LEN         (MPLS_MAX_L2HEADER_LEN +\
                                       (MPLS_MAX_LABEL_STACK * 4)+\
                                       MPLS_MAX_GAL_GACH_HEADER_LEN +\
                                       ELPS_PSC_PDU_LEN)

#define ELPS_PSC_GAL_HDR_LEN       4

#define ELPS_MPLS_PKT_PSC_OFF(Ver,Offset)   { \
                                         if (Ver == 0) \
                                            Offset = (MPLS_MAX_L2HEADER_LEN +\
                                            (MPLS_MAX_LABEL_STACK  * 4)+\
                                              MPLS_MAX_GAL_GACH_HEADER_LEN);\
                                         else if (Ver == 1)\
                                            Offset = (MPLS_MAX_L2HEADER_LEN +\
                                              (MPLS_MAX_LABEL_STACK  * 4)+\
                                              ELPS_PSC_GACH_HDR_LEN +\
                                              ELPS_PSC_GAL_HDR_LEN);\
                                            }

#define ELPS_PERIODIC_APS_PDU_INTERVAL    5  /* seconds */
#define ELPS_LOR_INTERVAL                 50 /* milli seconds */
#define ELPS_MAX_APS_PDU_ON_STATE_CHANGE  3
#define ELPS_STATE_CHANGE_TX_INTERVAL     3300 /* 3.3 mili Second */

#define ELPS_MIN_RAPID_TX_TIME           1000
#define ELPS_MAX_RAPID_TX_TIME           3300
#define ELPS_MIN_PSC_CHANNEL_CODE        0x9
#define ELPS_MAX_PSC_CHANNEL_CODE        0x20

#define ELPS_MAX_UINT4_VALUE            0xffffffff
#define ELPS_MIN_UINT4_VALUE            0x00000000
#define ELPS_MIN_MCAST_ADDR_VALUE       0xe0000000
#define ELPS_MAX_MCAST_ADDR_VALUE       0xe00000ff

#define ELPS_MAX_OID_LEN                20
#define ELPS_ERR_OID_LEN                2
/* Tunnel Table related macros */
#define ELPS_TNL_TABLE_DEF_OFFSET       gu4TnlTableBaseOidLen
#define ELPS_TNL_INDEX_START_OFFSET     ELPS_TNL_TABLE_DEF_OFFSET+0
#define ELPS_TNL_INST_START_OFFSET      ELPS_TNL_TABLE_DEF_OFFSET+1
#define ELPS_TNL_ING_LSRID_START_OFFSET ELPS_TNL_TABLE_DEF_OFFSET+2
#define ELPS_TNL_EGG_LSRID_START_OFFSET ELPS_TNL_TABLE_DEF_OFFSET+3
#define ELPS_TNL_TABLE_OID_LEN          gu4TnlTableBaseOidLen + 4

/* Psuedo wire Table related macros */
#define ELPS_PW_TABLE_DEF_OFFSET       gu4PwTableBaseOidLen
#define ELPS_PW_INDEX_OFFSET           gu4PwTableBaseOidLen

#define ELPS_SNMP_TRUE                 1
#define ELPS_SNMP_FALSE                2

#define ELPS_MIN_MEP_ID                1
#define ELPS_MAX_MEP_ID                8191

#define ELPS_PORTS_PER_BYTE            8
#define ELPS_BIT8                      0x80
#define ELPS_SWITCH_ALIAS_LEN          VCM_ALIAS_MAX_LEN
#define ELPS_INIT_VAL                  0


/* SEM related Constants */
#define ELPS_ACTIVE                   1
#define ELPS_STANDBY                  0

#define ELPS_CFM_SIGNAL_FAIL          1
#define ELPS_CFM_SIGNAL_OK            2
#define ELPS_CFM_SIGNAL_FAIL_HOLD_OFF 3
#define ELPS_CFM_SIGNAL_AIS           4
#define ELPS_CFM_SIGNAL_AIS_CLEAR     5

#define ELPS_ENTITY_TYPE_WORKING      1
#define ELPS_ENTITY_TYPE_PROTECTION   2

#define  ELPS_SERVICE_DIRECTION_FWD   1
#define  ELPS_SERVICE_DIRECTION_RVR   2

/* RBTree constants */
#define ELPS_RB_GREATER               1
#define ELPS_RB_LESS                  -1
#define ELPS_RB_EQUAL                 0

/* Local Admin Command */
#define ELPS_MIN_LOCAL_COMMAND        0
#define ELPS_EXT_CMD_LOP              1
#define ELPS_EXT_CMD_FS               2
#define ELPS_EXT_CMD_MS               3
#define ELPS_EXT_CMD_EXER             4
#define ELPS_EXT_CMD_CLR              5
#define ELPS_EXT_CMD_FREEZE           6
#define ELPS_EXT_CMD_CLR_FREEZE       7
#define ELPS_EXT_CMD_MS_W             8
#define ELPS_MAX_LOCAL_COMMAND        9

/* Local Condition */
#define ELPS_LOCAL_COND_SF_W          1
#define ELPS_LOCAL_COND_WRK_REC_SF    2
#define ELPS_LOCAL_COND_SF_P          3
#define ELPS_LOCAL_COND_PROT_REC_SF   4
#define ELPS_LOCAL_COND_WTR_EXP       5
#define ELPS_MAX_LOCAL_CONDITION      6

/* Far End Request */
#define ELPS_FAR_REQ_LO               1
#define ELPS_FAR_REQ_SF_P             2
#define ELPS_FAR_REQ_FS               3
#define ELPS_FAR_REQ_SF_W             4
#define ELPS_FAR_REQ_MS               5
#define ELPS_FAR_REQ_WTR              6
#define ELPS_FAR_REQ_EXER_W           7
#define ELPS_FAR_REQ_EXER_P           8
#define ELPS_FAR_REQ_RR_W             9
#define ELPS_FAR_REQ_RR_P             10
#define ELPS_FAR_REQ_NR_W             11
#define ELPS_FAR_REQ_NR_P             12
#define ELPS_FAR_REQ_DNR              13
#define ELPS_FAR_REQ_MS_W             14
#define ELPS_MAX_FAR_REQUEST          15

/* Command Status */
#define ELPS_CMD_ACCEPTED             1
#define ELPS_CMD_OVERRULED            2
#define ELPS_CMD_NA                   3
#define ELPS_CMD_REJECTED             4

/* APS Request Or State Information */
#define ELPS_APS_REQ_STATE_NR         0
#define ELPS_APS_REQ_STATE_LO         1
#define ELPS_APS_REQ_STATE_FS         2
#define ELPS_APS_REQ_STATE_SF         3
#define ELPS_APS_REQ_STATE_SF_P       4
#define ELPS_APS_REQ_STATE_MS         5
#define ELPS_APS_REQ_STATE_MS_W       6
#define ELPS_APS_REQ_STATE_WTR        7
#define ELPS_APS_REQ_STATE_EXER       8
#define ELPS_APS_REQ_STATE_RR         9
#define ELPS_APS_REQ_STATE_DNR        10

/* PSC Request Or State Information */
#define ELPS_PSC_REQ_STATE_NR         0
#define ELPS_PSC_REQ_STATE_LO         1
#define ELPS_PSC_REQ_STATE_FS         2
#define ELPS_PSC_REQ_STATE_SF         3
#define ELPS_PSC_REQ_STATE_MS         4 
#define ELPS_PSC_REQ_STATE_WTR        5
#define ELPS_PSC_REQ_STATE_EXER       6
#define ELPS_PSC_REQ_STATE_RR         7
#define ELPS_PSC_REQ_STATE_DNR        8


/* Request/State Information encoded in the APS PDU 
 * Reference: Table 11-1 in standard */
#define ELPS_NA                       -1
#define ELPS_PKT_REQ_STATE_NR         0
#define ELPS_PKT_REQ_STATE_DNR        1
#define ELPS_PKT_REQ_STATE_RR         2
#define ELPS_PKT_REQ_STATE_EXER       4
#define ELPS_PKT_REQ_STATE_WTR        5
#define ELPS_PKT_REQ_STATE_MS_W       6
#define ELPS_PKT_REQ_STATE_MS         7
#define ELPS_PKT_REQ_STATE_SF         11
#define ELPS_PKT_REQ_STATE_FS         13
#define ELPS_PKT_REQ_STATE_SF_P       14
#define ELPS_PKT_REQ_STATE_LO         15
#define ELPS_REQ_CLEAR 16

/*For RFC-6378 the priority will be inverse*/
#define ELPS_PSC_VER_PKT_REQ_STATE_FS         14
#define ELPS_PSC_VER_PKT_REQ_STATE_SF_P       13

/* Request/State Information encoded in the PSC PDU 
 * Reference: Section 4.2.2 in draft-ietf-mpls-tp-linear-protection-02.txt */
#define ELPS_NA                       -1
#define ELPS_PSC_PKT_REQ_STATE_NR         0
#define ELPS_PSC_PKT_REQ_STATE_EXER       1 /* not defined in STD */
#define ELPS_PSC_PKT_REQ_STATE_DNR        2
#define ELPS_PSC_PKT_REQ_STATE_WTR        3
#define ELPS_PSC_PKT_REQ_STATE_MS         4
#define ELPS_PSC_PKT_REQ_STATE_RR             5 /* not defined in STD */
#define ELPS_PSC_PKT_REQ_STATE_SF         6
#define ELPS_PSC_PKT_REQ_STATE_FS         13
#define ELPS_PSC_PKT_REQ_STATE_LO         14

/* Request/State Information encoded in the PSC PDU
 * Reference: Section 4.2.2 of RFC-6378 */
#define ELPS_NA                           -1
#define ELPS_PSC_PKT_RFC_REQ_STATE_NR         0
#define ELPS_PSC_PKT_RFC_REQ_STATE_EXER       2 /* not defined in STD */
#define ELPS_PSC_PKT_RFC_REQ_STATE_DNR        1 
#define ELPS_PSC_PKT_RFC_REQ_STATE_WTR        4
#define ELPS_PSC_PKT_RFC_REQ_STATE_MS         5
#define ELPS_PSC_PKT_RFC_REQ_STATE_RR         3 /* not defined in STD */
#define ELPS_PSC_PKT_RFC_REQ_STATE_SF         10
#define ELPS_PSC_PKT_RFC_REQ_STATE_FS         12
#define ELPS_PSC_PKT_RFC_REQ_STATE_LO         14

/* Request Type */
#define ELPS_INVALID_REQUEST          0
#define ELPS_LOCAL_COMMAND            1
#define ELPS_LOCAL_CONDITION          2
#define ELPS_FAREND_REQUEST           3

/* Trap Type */
#define ELPS_INVALID_TRAP                  0
#define ELPS_TRAP_AUTO_PROT_SWITCH         1
#define ELPS_TRAP_FORCED_SWITCH            2
#define ELPS_TRAP_MANUAL_SWITCH            3
#define ELPS_TRAP_HW_SWITCH_TO_WORK_FAIL   4
#define ELPS_TRAP_HW_SWITCH_TO_PROT_FAIL   5
#define ELPS_TRAP_HW_PG_CRT_FAIL           6
#define ELPS_TRAP_HW_PG_DEL_FAIL           7
#define ELPS_TRAP_APS_PDU_TX_FAIL          8
#define ELPS_TRAP_PG_TYPE_MISMATCH         9
#define ELPS_TRAP_CONF_MISMATCH            10
#define ELPS_TRAP_LOR                      11
#define ELPS_TRAP_A_BIT_MISMATCH           12
#define ELPS_TRAP_D_BIT_MISMATCH           13
#define ELPS_TRAP_R_BIT_MISMATCH           14
#define ELPS_TRAP_MANUAL_SWITCH_W          15

/* Protection Group Name */
#define ELPS_PG_MAX_NAME_LEN           32

/* TRAP Ids */
#define ELPS_TRAP_PROTECTION_SWITCH_ID 1
#define ELPS_TRAP_SWITCH_FAILURE_ID    2
#define ELPS_TRAP_TYPE_MISMATCH_ID     3

/* Trap information string length */
#define ELPS_MAX_TRAP_STR_LEN          32

#define ELPS_ACT_REQ_LOCAL            1
#define ELPS_ACT_REQ_FAR              2

/* Trace String */
#define ELPS_MAX_TRC_STR_COUNT        9
#define ELPS_MAX_TRC_STR_LEN          15

#define ELPS_PG_MIN_HOLD_OFF_TIME      0
#define ELPS_PG_MAX_HOLD_OFF_TIME      1000
#define ELPS_PG_HO_TIME_GRANULARITY    100 /* 100 milli seconds */

#define ELPS_PG_MIN_WTR_TIME           1
#define ELPS_PG_MAX_WTR_TIME           1000
#define ELPS_PG_DEF_WTR_TIME           5
#define ELPS_SECONDS_IN_ONE_MINUTE     60

#define ELPS_PG_MIN_APS_PERIODIC_TIME  1
#define ELPS_PG_MAX_APS_PERIODIC_TIME  1000

#define ELPS_PG_ACTIVATE               1
#define ELPS_PG_DEACTIVATE             2

#define ELPS_R_BIT_VALUE               0x01
#define ELPS_D_BIT_VALUE               0x02
#define ELPS_B_BIT_VALUE               0x04
#define ELPS_A_BIT_VALUE               0x08

#define ELPS_PSC_PKT_REQUEST_MASK      0x3C
#define ELPS_PSC_PKT_VERSION_MASK      0xC0
#define ELPS_PSC_PKT_DIRECTION_MASK    0x02
#define ELPS_PSC_PKT_BRIDGE_MASK       0x01

/* PG Status */
#define ELPS_PG_STATUS_DISABLE             0
#define ELPS_PG_STATUS_WORK_PATH_ACTIVE    1
#define ELPS_PG_STATUS_PROT_PATH_ACTIVE    2
#define ELPS_PG_STATUS_WTR_STATE           3
#define ELPS_PG_STATUS_HOLD_OFF_STATE      4
#define ELPS_PG_STATUS_SWITCHING_FAILED    5
#define ELPS_PG_STATUS_UNAVAILABLE         6
#define ELPS_PG_STATUS_DO_NOT_REVERT       7

#define ELPS_PG_MAX_STATUS                 6

#define ELPS_MAX_REQUESTS              25
#define ELPS_MAX_STATES                14
#define ELPS_MAX_STR_LEN               80

#define ELPS_MAX_CFM_ENTRY_PER_PG      2

#define ELPS_TMR_START                 1
#define ELPS_TMR_STOP                  2 

#define ELPS_RED_HW_AUD_MOD_SHUTDOWN           1
#define ELPS_RED_HW_AUD_MOD_DISABLE            2
#define ELPS_RED_HW_AUD_MOD_ENABLE             3


#define ELPS_ONEPASS                   1
#define ELPS_TWOPASS                   2 

#define ELPS_TMR_HOLD_P                  0x01
#define ELPS_TMR_HOLD_W                  0x02
#define ELPS_TMR_WTR                     0x04  

#define ELPS_INVALID_REQ_VAL1            3
#define ELPS_INVALID_REQ_VAL2            8
#define ELPS_INVALID_REQ_VAL3            9 
#define ELPS_INVALID_REQ_VAL4            10  
#define ELPS_INVALID_REQ_VAL5            12  

#define ELPS_MAX_MAC_STR_LEN             30

/* performance measurement */
#define ELPS_PERF_LR_SF_RX               1
#define ELPS_PERF_LR_SF_TX               2
#define ELPS_PERF_FR_SF_RX               3
#define ELPS_PERF_STATE_CHG              4
#endif /* _ELPSCONS_H */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpscons.h                     */
/*-----------------------------------------------------------------------*/
