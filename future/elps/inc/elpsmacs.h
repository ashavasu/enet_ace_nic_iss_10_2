/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsmacs.h,v 1.8 2012/09/01 10:02:00 siva Exp $
 *
 * Description: This file contains the macros used in ELPS module.
 *****************************************************************************/
#ifndef _ELPSMACS_H
#define _ELPSMACS_H

/* ELPS Module APS Tx Info Database Semaphore */
#define  ELPS_TX_DB_LOCK()      ElpsUtilApsTxDbLock ()
#define  ELPS_TX_DB_UNLOCK()    ElpsUtilApsTxDbUnLock ()

#define ELPS_DATA_ASSIGN_1_BYTE(pMsg, u4Offset, u1Value) \
{\
   UINT1  u1LinearBuf = (UINT1) u1Value;\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u1LinearBuf), u4Offset, 1);\
   u4Offset += 1; \
}

#define ELPS_DATA_ASSIGN_2_BYTE(pMsg, u4Offset, u2Value) \
{\
   UINT2  u2LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u2LinearBuf), u4Offset, 2);\
   u4Offset += 2; \
}

#define ELPS_DATA_ASSIGN_4_BYTE(pMsg, u4Offset, u4Value) \
{\
   UINT4  u4LinearBuf = OSIX_HTONL ((UINT4) u4Value);\
   CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u4LinearBuf), u4Offset, 4);\
   u4Offset += 4; \
}

#define ELPS_LBUF_PUT_1_BYTE(pBuf, u1Value, u2Offset) \
{\
    *pBuf = u1Value; \
    pBuf += 1;\
    u2Offset = (UINT2) (u2Offset + 1);\
}

#define ELPS_GET_VERSION_FROM_PDU(pu1ApsPdu) \
    (pu1ApsPdu[ELPS_APS_OFFSET_OF_VERSION] & 0x1F)

#define ELPS_GET_OPCODE_FROM_PDU(pu1ApsPdu) \
    (pu1ApsPdu[ELPS_APS_OFFSET_OF_OPCODE])

#define ELPS_GET_FLAGS_FROM_PDU(pu1ApsPdu) \
    (pu1ApsPdu[ELPS_APS_OFFSET_OF_FLAGS])

#define ELPS_GET_TLV_OFFSET_FROM_PDU(pu1ApsPdu) \
    (pu1ApsPdu[ELPS_APS_OFFSET_OF_TLV_OFFSET])

#define ELPS_GET_END_TLV_FROM_PDU(pu1ApsPdu) \
    (pu1ApsPdu[ELPS_APS_OFFSET_OF_END_TLV])

#define ELPS_GET_PG_A_BIT_VALUE(u1Value) \
    (u1Value & ELPS_A_BIT_VALUE)

#define ELPS_GET_PG_B_BIT_VALUE(u1Value) \
    (u1Value & ELPS_B_BIT_VALUE)

#define ELPS_GET_PG_D_BIT_VALUE(u1Value) \
    (u1Value & ELPS_D_BIT_VALUE)

#define ELPS_GET_PG_R_BIT_VALUE(u1Value) \
    (u1Value & ELPS_R_BIT_VALUE)

#define ELPS_SET_PG_OPER_TYPE(u1Variable) \
    u1Variable = (UINT1) (u1Variable | ELPS_R_BIT_VALUE)

#define ELPS_SET_PG_PROT_TYPE(u1Variable) \
    u1Variable |= (UINT1) (ELPS_A_BIT_VALUE | ELPS_B_BIT_VALUE | \
                           ELPS_D_BIT_VALUE )

#define ELPS_RESET_PG_OPER_TYPE(u1Variable) \
    u1Variable = (UINT1) (u1Variable & 0xe)

#define ELPS_GET_REQUEST_FROM_PDU(u1Result, pu1Val)\
    u1Result = (UINT1) (pu1Val[0] & 0xF0); \
    u1Result = (UINT1) (u1Result >> 4);

#define ELPS_GET_REQUEST_FROM_PSC_PDU(u1Result, pu1Val)\
    u1Result = (UINT1) (pu1Val[0] & ELPS_PSC_PKT_REQUEST_MASK); \
    u1Result = (UINT1) (u1Result >> 2);

#define ELPS_GET_VERSION_FROM_PSC_PDU(u1Result, pu1Val)\
    u1Result = (UINT1) (pu1Val[0] & ELPS_PSC_PKT_VERSION_MASK); \
    u1Result = (UINT1) (u1Result >> 6); \

#define ELPS_GET_DIRECTION_FROM_PSC_PDU(u1Result, pu1Val)\
    u1Result = (UINT1) (pu1Val[0] & ELPS_PSC_PKT_DIRECTION_MASK); \
    u1Result = (UINT1) (u1Result >> 1);

#define ELPS_GET_BRIDGE_FROM_PSC_PDU(u1Result, pu1Val)\
    u1Result = (UINT1) (pu1Val[0] & ELPS_PSC_PKT_BRIDGE_MASK);

#define ELPS_GET_R_BIT_FROM_PSC_PDU(u1Result, pu1Val)\
    u1Result = (UINT1) (pu1Val[1] & 0x80); \
    u1Result = (UINT1) (u1Result >> 7);


#define ELPS_IS_SF_REASSERTED(pPgInfo) \
 ((pPgInfo->u1WorkingCfmStatus == ELPS_CFM_SIGNAL_FAIL) ? \
  OSIX_TRUE : OSIX_FALSE)

#define ELPS_IS_SF_P_REASSERTED(pPgInfo) \
((pPgInfo->u1ProtectionCfmStatus == ELPS_CFM_SIGNAL_FAIL) ? \
  OSIX_TRUE : OSIX_FALSE)

/* HITLESS RESTART */
#define ELPS_HR_STDY_ST_PKT_REQ        RM_HR_STDY_ST_PKT_REQ
#define ELPS_HR_STDY_ST_PKT_MSG        RM_HR_STDY_ST_PKT_MSG
#define ELPS_HR_STDY_ST_PKT_TAIL       RM_HR_STDY_ST_PKT_TAIL
#define ELPS_HR_STATUS()               ElpsPortRmGetHRFlag()
#define ELPS_HR_STATUS_DISABLE         RM_HR_STATUS_DISABLE
#define ELPS_RM_OFFSET                 0  /* Offset in the RM buffer from where
                                         * protocols can start writing. */
#define ELPS_HR_STDY_ST_REQ_RCVD() gElpsGlobalInfo.RedGlobalInfo.u1StdyStReqRcvd

/* performance measurement */
#define ELPS_APS_IS_SF_FOR_WORKING(u1Data) \
    ((u1Data & 0xb0) == 0xb0)

#define ELPS_APS_IS_SF_FOR_PROTECTION(u1Data) \
    ((u1Data & 0xe0) == 0xe0)

/* performance measurement */

/* Macro to identify SF from PSC packet
 *
 * First check is for RFC upgrade
 * ------------------------------
 * In CRU Buffer the packet is as shown as below.
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |0 0 0 1|Version|  Reserved     |          PSC-CT               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |Ver|Request|PT |R|  Reserved1  |     FPath     |     Path      |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |         TLV Length            |          Reserved2            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       ~                         Optional TLVs                         ~
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

 * Ver = 1 & Request = (10) ==> SF for PSC-RFC upgarded packet
 *
 * ELPS header may be present after the following combinations.
 * 1) MPLS Tunnel Header alone (4bytes)
 * ==> For this case, the offset for [Ver & Request] field is 8.
 * 2) MPLS Tunnel + MPLS PW header (4 + 4) bytes
 * ==> For this case, the offset for [Ver & Request] field is 12.
 * 3) MPLS Tunnel + MPLS RTR + MPLS PW (4 + 4 + 4) bytes
 * ==> For this case, the offset for [Ver & Request] field is 16.
 *
 * Second check is for Draft upgrade
 * ----------------------------------
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |0 0 0 1|Version|  Reserved     |   Channel Type = MPLS-TP PSC  |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                          ACH TLV Header                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       ~                         Optional TLVs                         ~
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |Ver|Request|PT |R|  Reserved   |     FPath     |     Path      |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 *
 * Here Optional TLVs size is zero.
 * Ver = 0 & Request field = (0110) ==> SF for PSC-Draft Packet
 *
 * ELPS header may be present after the following combinations.
 * 1) MPLS Tunnel Header alone (4bytes)
 * ==> For this case, the offset for [Ver & Request] field is 12.
 * 2) MPLS Tunnel + MPLS PW header (4 + 4) bytes
 * ==> For this case, the offset for [Ver & Request] field is 16.
 * 3) MPLS Tunnel + MPLS RTR + MPLS PW (4 + 4 + 4) bytes
 * ==> For this case, the offset for [Ver & Request] field is 20.
 */

#define ELPS_GET_OFFSET_FOR_ELPS_HDR(pCruBuf,u4OffSet) \
     if (pCruBuf->pFirstValidDataDesc->pu1_FirstValidByte[2] & 0x1) \
     {\
        u4OffSet = 8;\
     }\
     else if (pCruBuf->pFirstValidDataDesc->pu1_FirstValidByte[6] & 0x1) \
     {\
        u4OffSet = 12;\
     }\
     else if (pCruBuf->pFirstValidDataDesc->pu1_FirstValidByte[10] & 0x1) \
     {\
        u4OffSet = 16;\
     }
     

#define ELPS_PSC_IS_SF_FOR_WORKING(pCruBuf,u4OffSet) \
    (((pCruBuf->pFirstValidDataDesc->pu1_FirstValidByte[u4OffSet] & 0x68)\
        == 0x68) ||\
     ((pCruBuf->pFirstValidDataDesc->pu1_FirstValidByte[u4OffSet + 4] & 0x18)\
        == 0x18))

#endif /* _ELPSMACS_H*/

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsmacs.h                     */
/*-----------------------------------------------------------------------*/
