/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsinc.h,v 1.8 2013/01/10 12:43:26 siva Exp $
 *
 * Description: This file contains header files included in ELPS module.
 *****************************************************************************/
#ifndef _ELPSINC_H
#define _ELPSINC_H

#include "lr.h"
#include "cfa.h"
#include "snmccons.h"
#include  "fssnmp.h" 
#include "snmputil.h"
#include "ip.h"
#include "l2iwf.h"

#include "fsvlan.h"
#include "vcm.h"
#include "fm.h"
#include "hwaud.h"

#include "elpscons.h"
#include "elps.h"
#include "elpstdfs.h"
#include "elpstrap.h"
#include "elpsprot.h"
#include "elpsmacs.h"
#include "elpssz.h"

#ifdef NPAPI_WANTED
#include "npapi.h"
#include "elpsnp.h"
#include "elpsnpwr.h"
#ifdef MBSM_WANTED
#include "mbsm.h"
#endif
#endif

#ifdef L2RED_WANTED
#include "elpsred.h"
#endif

#ifdef _ELPSMAIN_C_
#include "elpsglob.h"
#else
#include "elpsextn.h"
#endif

#include "fselpslw.h"
#include "fselpswr.h"
#include "elpstrc.h"
#include "elpscli.h"

#endif /* _ELPSINC_H */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsinc.h                      */
/*-----------------------------------------------------------------------*/
