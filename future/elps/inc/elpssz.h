/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpssz.h,v 1.4 2012/03/30 13:25:28 siva Exp $
 *
 * Description: This file contains mempool creation/deletion prototype
 *              declarations in ELPS module.
 *********************************************************************/
enum {
    MAX_ELPS_APS_TX_INFO_SIZING_ID,
    MAX_ELPS_CFM_CONFIG_INFO_SIZING_ID,
    MAX_ELPS_CONTEXTS_SIZING_ID,
    MAX_ELPS_MPLS_IN_API_COUNT_SIZING_ID,
    MAX_ELPS_MPLS_OUT_API_COUNT_SIZING_ID,
    MAX_ELPS_MPLS_PATH_ID_INFO_SIZING_ID,
    MAX_ELPS_PROTECTION_GROUP_INFO_SIZING_ID,
    MAX_ELPS_Q_MESG_SIZING_ID,
    MAX_ELPS_RED_NP_SYNC_ENTRIES_SIZING_ID,
    MAX_ELPS_SERVICE_LIST_INFO_SIZING_ID,
    MAX_ELPS_SIMULTANEOUS_CFM_SIGNAL_SIZING_ID,
    MAX_ELPS_SYS_NUM_SERVICE_PTR_IN_LIST_SIZING_ID,
    ELPS_MAX_SIZING_ID
};


#ifdef  _ELPSSZ_C
tMemPoolId ELPSMemPoolIds[ ELPS_MAX_SIZING_ID];
INT4  ElpsSizingMemCreateMemPools(VOID);
VOID  ElpsSizingMemDeleteMemPools(VOID);
INT4  ElpsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _ELPSSZ_C  */
extern tMemPoolId ELPSMemPoolIds[ ];
extern INT4  ElpsSizingMemCreateMemPools(VOID);
extern VOID  ElpsSizingMemDeleteMemPools(VOID);
extern INT4  ElpsSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _ELPSSZ_C  */


#ifdef  _ELPSSZ_C
tFsModSizingParams FsELPSSizingParams [] = {
{ "tElpsApsTxInfo", "MAX_ELPS_APS_TX_INFO", sizeof(tElpsApsTxInfo),MAX_ELPS_APS_TX_INFO, MAX_ELPS_APS_TX_INFO,0 },
{ "tElpsCfmConfigInfo", "MAX_ELPS_CFM_CONFIG_INFO", sizeof(tElpsCfmConfigInfo),MAX_ELPS_CFM_CONFIG_INFO, MAX_ELPS_CFM_CONFIG_INFO,0 },
{ "tElpsContextInfo", "MAX_ELPS_CONTEXTS", sizeof(tElpsContextInfo),MAX_ELPS_CONTEXTS, MAX_ELPS_CONTEXTS,0 },
{ "tMplsApiInInfo", "MAX_ELPS_MPLS_IN_API_COUNT", sizeof(tMplsApiInInfo),MAX_ELPS_MPLS_IN_API_COUNT, MAX_ELPS_MPLS_IN_API_COUNT,0 },
{ "tMplsApiOutInfo", "MAX_ELPS_MPLS_OUT_API_COUNT", sizeof(tMplsApiOutInfo),MAX_ELPS_MPLS_OUT_API_COUNT, MAX_ELPS_MPLS_OUT_API_COUNT,0 },
{ "tMplsPathId", "MAX_ELPS_MPLS_PATH_ID_INFO", sizeof(tMplsPathId),MAX_ELPS_MPLS_PATH_ID_INFO, MAX_ELPS_MPLS_PATH_ID_INFO,0 },
{ "tElpsPgInfo", "MAX_ELPS_PROTECTION_GROUP_INFO", sizeof(tElpsPgInfo),MAX_ELPS_PROTECTION_GROUP_INFO, MAX_ELPS_PROTECTION_GROUP_INFO,0 },
{ "tElpsQMsg", "MAX_ELPS_Q_MESG", sizeof(tElpsQMsg),MAX_ELPS_Q_MESG, MAX_ELPS_Q_MESG,0 },
{ "tElpsRedNpSyncEntry", "MAX_ELPS_RED_NP_SYNC_ENTRIES", sizeof(tElpsRedNpSyncEntry),MAX_ELPS_RED_NP_SYNC_ENTRIES, MAX_ELPS_RED_NP_SYNC_ENTRIES,0 },
{ "tElpsServiceListInfo", "MAX_ELPS_SERVICE_LIST_INFO", sizeof(tElpsServiceListInfo),MAX_ELPS_SERVICE_LIST_INFO, MAX_ELPS_SERVICE_LIST_INFO,0 },
{ "UINT1[12]", "MAX_ELPS_SIMULTANEOUS_CFM_SIGNAL", sizeof(UINT1[12]),MAX_ELPS_SIMULTANEOUS_CFM_SIGNAL, MAX_ELPS_SIMULTANEOUS_CFM_SIGNAL,0 },
{ "tElpsServiceListPointerInfo", "MAX_ELPS_SYS_NUM_SERVICE_PTR_IN_LIST", sizeof(tElpsServiceListPointerInfo),MAX_ELPS_SYS_NUM_SERVICE_PTR_IN_LIST, MAX_ELPS_SYS_NUM_SERVICE_PTR_IN_LIST,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _ELPSSZ_C  */
extern tFsModSizingParams FsELPSSizingParams [];
#endif /*  _ELPSSZ_C  */


