/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fselpslw.h,v 1.7 2012/08/16 10:32:15 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsStatsOneIsToOneApsPktTxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsElpsStatsOneIsToOneApsPktRxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsElpsStatsOneIsToOneApsPktDiscardCount ARG_LIST((UINT4 *));

INT1
nmhGetFsElpsStatsOnePlusOneApsPktTxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsElpsStatsOnePlusOneApsPktRxCount ARG_LIST((UINT4 *));

INT1
nmhGetFsElpsStatsOnePlusOneApsPktDiscardCount ARG_LIST((UINT4 *));

INT1
nmhGetFsElpsGlobalTraceOption ARG_LIST((INT4 *));

INT1
nmhGetFsElpsPSCChannelCode ARG_LIST((UINT4 *));

INT1
nmhGetFsElpsRapidTxTime ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElpsGlobalTraceOption ARG_LIST((INT4 ));

INT1
nmhSetFsElpsPSCChannelCode ARG_LIST((UINT4 ));

INT1
nmhSetFsElpsRapidTxTime ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElpsGlobalTraceOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsElpsPSCChannelCode ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsElpsRapidTxTime ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElpsGlobalTraceOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsElpsPSCChannelCode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsElpsRapidTxTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElpsContextTable. */
INT1
nmhValidateIndexInstanceFsElpsContextTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElpsContextTable  */

INT1
nmhGetFirstIndexFsElpsContextTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElpsContextTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsContextSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsElpsContextModuleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsElpsContextTraceInputString ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsElpsContextEnableTrap ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsElpsContextVlanGroupManager ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElpsContextSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsElpsContextModuleStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsElpsContextTraceInputString ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsElpsContextEnableTrap ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsElpsContextVlanGroupManager ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElpsContextSystemControl ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsContextModuleStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsContextTraceInputString ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsElpsContextEnableTrap ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsContextVlanGroupManager ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElpsContextTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElpsPgConfigTable. */
INT1
nmhValidateIndexInstanceFsElpsPgConfigTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElpsPgConfigTable  */

INT1
nmhGetFirstIndexFsElpsPgConfigTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElpsPgConfigTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsPgConfigType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigServiceType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigMonitorMechanism ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigIngressPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigWorkingPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigProtectionPort ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigWorkingServiceValue ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgConfigProtectionServiceValue ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgConfigOperType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigProtType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsElpsPgConfigRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgConfigWorkingServicePointer ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsElpsPgConfigWorkingReverseServicePointer ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsElpsPgConfigProtectionServicePointer ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsElpsPgConfigProtectionReverseServicePointer ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsElpsPgConfigWorkingInstanceId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgConfigProtectionInstanceId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgPscVersion ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElpsPgConfigType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigServiceType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigMonitorMechanism ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigIngressPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigWorkingPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigProtectionPort ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigWorkingServiceValue ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgConfigProtectionServiceValue ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgConfigOperType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigProtType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsElpsPgConfigRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgConfigWorkingServicePointer ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsElpsPgConfigWorkingReverseServicePointer ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsElpsPgConfigProtectionServicePointer ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsElpsPgConfigProtectionReverseServicePointer ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsElpsPgConfigWorkingInstanceId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgConfigProtectionInstanceId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgPscVersion ARG_LIST((UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElpsPgConfigType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigServiceType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigMonitorMechanism ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigIngressPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigWorkingPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigProtectionPort ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigWorkingServiceValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgConfigProtectionServiceValue ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgConfigOperType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigProtType ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigName ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsElpsPgConfigRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgConfigWorkingServicePointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsElpsPgConfigWorkingReverseServicePointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsElpsPgConfigProtectionServicePointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsElpsPgConfigProtectionReverseServicePointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsElpsPgConfigWorkingInstanceId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgConfigProtectionInstanceId ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgPscVersion ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElpsPgConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElpsPgCmdTable. */
INT1
nmhValidateIndexInstanceFsElpsPgCmdTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElpsPgCmdTable  */

INT1
nmhGetFirstIndexFsElpsPgCmdTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElpsPgCmdTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsPgCmdHoTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCmdWTR ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCmdExtCmd ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdExtCmdStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdLocalCondition ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdLocalConditionStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdFarEndRequest ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdFarEndRequestStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdActiveRequest ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdSemState ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdPgStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCmdApsPeriodicTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElpsPgCmdHoTime ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCmdWTR ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCmdExtCmd ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgCmdApsPeriodicTime ARG_LIST((UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElpsPgCmdHoTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCmdWTR ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCmdExtCmd ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgCmdApsPeriodicTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElpsPgCmdTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElpsPgCfmTable. */
INT1
nmhValidateIndexInstanceFsElpsPgCfmTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElpsPgCfmTable  */

INT1
nmhGetFirstIndexFsElpsPgCfmTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElpsPgCfmTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsPgCfmWorkingMEG ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmWorkingME ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmWorkingMEP ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmProtectionMEG ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmProtectionME ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmProtectionMEP ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgCfmWorkingReverseMEG ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmWorkingReverseME ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmWorkingReverseMEP ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmProtectionReverseMEG ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmProtectionReverseME ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgCfmProtectionReverseMEP ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElpsPgCfmWorkingMEG ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmWorkingME ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmWorkingMEP ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmProtectionMEG ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmProtectionME ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmProtectionMEP ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsElpsPgCfmWorkingReverseMEG ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmWorkingReverseME ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmWorkingReverseMEP ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmProtectionReverseMEG ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmProtectionReverseME ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsElpsPgCfmProtectionReverseMEP ARG_LIST((UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElpsPgCfmWorkingMEG ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmWorkingME ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmWorkingMEP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmProtectionMEG ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmProtectionME ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmProtectionMEP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsElpsPgCfmWorkingReverseMEG ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmWorkingReverseME ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmWorkingReverseMEP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmProtectionReverseMEG ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmProtectionReverseME ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsElpsPgCfmProtectionReverseMEP ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElpsPgCfmTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElpsPgServiceListTable. */
INT1
nmhValidateIndexInstanceFsElpsPgServiceListTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElpsPgServiceListTable  */

INT1
nmhGetFirstIndexFsElpsPgServiceListTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElpsPgServiceListTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsPgServiceListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElpsPgServiceListRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElpsPgServiceListRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElpsPgServiceListTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElpsPgServiceListPointerTable. */
INT1
nmhValidateIndexInstanceFsElpsPgServiceListPointerTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElpsPgServiceListPointerTable  */

INT1
nmhGetFirstIndexFsElpsPgServiceListPointerTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElpsPgServiceListPointerTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsPgWorkingServiceListPointer ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsElpsPgWorkingReverseServiceListPointer ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsElpsPgProtectionServiceListPointer ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsElpsPgProtectionReverseServiceListPointer ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsElpsPgServiceListPointerRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElpsPgWorkingServiceListPointer ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsElpsPgWorkingReverseServiceListPointer ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsElpsPgProtectionServiceListPointer ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsElpsPgProtectionReverseServiceListPointer ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsElpsPgServiceListPointerRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElpsPgWorkingServiceListPointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsElpsPgWorkingReverseServiceListPointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsElpsPgProtectionServiceListPointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsElpsPgProtectionReverseServiceListPointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsElpsPgServiceListPointerRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElpsPgServiceListPointerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsElpsPgShareTable. */
INT1
nmhValidateIndexInstanceFsElpsPgShareTable ARG_LIST((UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElpsPgShareTable  */

INT1
nmhGetFirstIndexFsElpsPgShareTable ARG_LIST((UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElpsPgShareTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsPgSharePgStatus ARG_LIST((UINT4  , INT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for FsElpsPgStatsTable. */
INT1
nmhValidateIndexInstanceFsElpsPgStatsTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsElpsPgStatsTable  */

INT1
nmhGetFirstIndexFsElpsPgStatsTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsElpsPgStatsTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsElpsPgStatsAutoProtectionSwitchCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgStatsForcedSwitchCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgStatsManualSwitchCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgStatsClearStatistics ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsElpsPgStatsApsPktTxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgStatsApsPktRxCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgStatsApsPktDiscardCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgLRSFRxTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgLRSFTxTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgFRSFRxTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsElpsPgStateChgTime ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsElpsPgStatsClearStatistics ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsElpsPgStatsClearStatistics ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsElpsPgStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */
