/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fselpsdb.h,v 1.9 2013/03/14 14:11:32 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSELPSDB_H
#define _FSELPSDB_H

UINT1 FsElpsContextTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsElpsPgConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsElpsPgCmdTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsElpsPgCfmTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsElpsPgServiceListTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsElpsPgServiceListPointerTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsElpsPgShareTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsElpsPgStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fselps [] ={1,3,6,1,4,1,29601,2,25};
tSNMP_OID_TYPE fselpsOID = {9, fselps};


UINT4 FsElpsGlobalTraceOption [ ] ={1,3,6,1,4,1,29601,2,25,1,1};
UINT4 FsElpsPSCChannelCode [ ] ={1,3,6,1,4,1,29601,2,25,1,2};
UINT4 FsElpsRapidTxTime [ ] ={1,3,6,1,4,1,29601,2,25,1,3};
UINT4 FsElpsContextId [ ] ={1,3,6,1,4,1,29601,2,25,2,1,1,1};
UINT4 FsElpsContextSystemControl [ ] ={1,3,6,1,4,1,29601,2,25,2,1,1,2};
UINT4 FsElpsContextModuleStatus [ ] ={1,3,6,1,4,1,29601,2,25,2,1,1,3};
UINT4 FsElpsContextTraceInputString [ ] ={1,3,6,1,4,1,29601,2,25,2,1,1,4};
UINT4 FsElpsContextEnableTrap [ ] ={1,3,6,1,4,1,29601,2,25,2,1,1,5};
UINT4 FsElpsContextVlanGroupManager [ ] ={1,3,6,1,4,1,29601,2,25,2,1,1,6};
UINT4 FsElpsPgConfigPgId [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,1};
UINT4 FsElpsPgConfigType [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,2};
UINT4 FsElpsPgConfigServiceType [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,3};
UINT4 FsElpsPgConfigMonitorMechanism [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,4};
UINT4 FsElpsPgConfigIngressPort [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,5};
UINT4 FsElpsPgConfigWorkingPort [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,6};
UINT4 FsElpsPgConfigProtectionPort [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,7};
UINT4 FsElpsPgConfigWorkingServiceValue [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,8};
UINT4 FsElpsPgConfigProtectionServiceValue [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,9};
UINT4 FsElpsPgConfigOperType [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,10};
UINT4 FsElpsPgConfigProtType [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,11};
UINT4 FsElpsPgConfigName [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,12};
UINT4 FsElpsPgConfigRowStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,13};
UINT4 FsElpsPgConfigWorkingServicePointer [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,14};
UINT4 FsElpsPgConfigWorkingReverseServicePointer [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,15};
UINT4 FsElpsPgConfigProtectionServicePointer [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,16};
UINT4 FsElpsPgConfigProtectionReverseServicePointer [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,17};
UINT4 FsElpsPgConfigWorkingInstanceId [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,18};
UINT4 FsElpsPgConfigProtectionInstanceId [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,19};
UINT4 FsElpsPgPscVersion [ ] ={1,3,6,1,4,1,29601,2,25,3,1,1,20};
UINT4 FsElpsPgCmdHoTime [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,1};
UINT4 FsElpsPgCmdWTR [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,2};
UINT4 FsElpsPgCmdExtCmd [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,3};
UINT4 FsElpsPgCmdExtCmdStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,4};
UINT4 FsElpsPgCmdLocalCondition [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,5};
UINT4 FsElpsPgCmdLocalConditionStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,6};
UINT4 FsElpsPgCmdFarEndRequest [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,7};
UINT4 FsElpsPgCmdFarEndRequestStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,8};
UINT4 FsElpsPgCmdActiveRequest [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,9};
UINT4 FsElpsPgCmdSemState [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,10};
UINT4 FsElpsPgCmdPgStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,11};
UINT4 FsElpsPgCmdApsPeriodicTime [ ] ={1,3,6,1,4,1,29601,2,25,3,2,1,12};
UINT4 FsElpsPgCfmWorkingMEG [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,1};
UINT4 FsElpsPgCfmWorkingME [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,2};
UINT4 FsElpsPgCfmWorkingMEP [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,3};
UINT4 FsElpsPgCfmProtectionMEG [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,4};
UINT4 FsElpsPgCfmProtectionME [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,5};
UINT4 FsElpsPgCfmProtectionMEP [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,6};
UINT4 FsElpsPgCfmRowStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,7};
UINT4 FsElpsPgCfmWorkingReverseMEG [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,8};
UINT4 FsElpsPgCfmWorkingReverseME [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,9};
UINT4 FsElpsPgCfmWorkingReverseMEP [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,10};
UINT4 FsElpsPgCfmProtectionReverseMEG [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,11};
UINT4 FsElpsPgCfmProtectionReverseME [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,12};
UINT4 FsElpsPgCfmProtectionReverseMEP [ ] ={1,3,6,1,4,1,29601,2,25,3,3,1,13};
UINT4 FsElpsPgServiceListValue [ ] ={1,3,6,1,4,1,29601,2,25,3,4,1,1};
UINT4 FsElpsPgServiceListRowStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,4,1,2};
UINT4 FsElpsPgServiceListId [ ] ={1,3,6,1,4,1,29601,2,25,3,7,1,1};
UINT4 FsElpsPgWorkingServiceListPointer [ ] ={1,3,6,1,4,1,29601,2,25,3,7,1,2};
UINT4 FsElpsPgWorkingReverseServiceListPointer [ ] ={1,3,6,1,4,1,29601,2,25,3,7,1,3};
UINT4 FsElpsPgProtectionServiceListPointer [ ] ={1,3,6,1,4,1,29601,2,25,3,7,1,4};
UINT4 FsElpsPgProtectionReverseServiceListPointer [ ] ={1,3,6,1,4,1,29601,2,25,3,7,1,5};
UINT4 FsElpsPgServiceListPointerRowStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,7,1,6};
UINT4 FsElpsPgShareProtectionPort [ ] ={1,3,6,1,4,1,29601,2,25,3,5,1,1};
UINT4 FsElpsPgSharePgStatus [ ] ={1,3,6,1,4,1,29601,2,25,3,5,1,2};
UINT4 FsElpsPgStatsAutoProtectionSwitchCount [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,1};
UINT4 FsElpsPgStatsForcedSwitchCount [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,2};
UINT4 FsElpsPgStatsManualSwitchCount [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,3};
UINT4 FsElpsPgStatsClearStatistics [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,4};
UINT4 FsElpsPgStatsApsPktTxCount [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,5};
UINT4 FsElpsPgStatsApsPktRxCount [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,6};
UINT4 FsElpsPgStatsApsPktDiscardCount [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,7};
UINT4 FsElpsPgLRSFRxTime [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,8};
UINT4 FsElpsPgLRSFTxTime [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,9};
UINT4 FsElpsPgFRSFRxTime [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,10};
UINT4 FsElpsPgStateChgTime [ ] ={1,3,6,1,4,1,29601,2,25,3,6,1,11};
UINT4 FsElpsTrapContextName [ ] ={1,3,6,1,4,1,29601,2,25,4,1};
UINT4 FsElpsTrapSwitchingMechanism [ ] ={1,3,6,1,4,1,29601,2,25,4,2};
UINT4 FsElpsTrapMismatchType [ ] ={1,3,6,1,4,1,29601,2,25,4,3};
UINT4 FsElpsTypeOfFailure [ ] ={1,3,6,1,4,1,29601,2,25,4,4};
UINT4 FsElpsStatsOneIsToOneApsPktTxCount [ ] ={1,3,6,1,4,1,29601,2,25,5,1};
UINT4 FsElpsStatsOneIsToOneApsPktRxCount [ ] ={1,3,6,1,4,1,29601,2,25,5,2};
UINT4 FsElpsStatsOneIsToOneApsPktDiscardCount [ ] ={1,3,6,1,4,1,29601,2,25,5,3};
UINT4 FsElpsStatsOnePlusOneApsPktTxCount [ ] ={1,3,6,1,4,1,29601,2,25,5,4};
UINT4 FsElpsStatsOnePlusOneApsPktRxCount [ ] ={1,3,6,1,4,1,29601,2,25,5,5};
UINT4 FsElpsStatsOnePlusOneApsPktDiscardCount [ ] ={1,3,6,1,4,1,29601,2,25,5,6};




tMbDbEntry fselpsMibEntry[]= {

{{11,FsElpsGlobalTraceOption}, NULL, FsElpsGlobalTraceOptionGet, FsElpsGlobalTraceOptionSet, FsElpsGlobalTraceOptionTest, FsElpsGlobalTraceOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsElpsPSCChannelCode}, NULL, FsElpsPSCChannelCodeGet, FsElpsPSCChannelCodeSet, FsElpsPSCChannelCodeTest, FsElpsPSCChannelCodeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "9"},

{{11,FsElpsRapidTxTime}, NULL, FsElpsRapidTxTimeGet, FsElpsRapidTxTimeSet, FsElpsRapidTxTimeTest, FsElpsRapidTxTimeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "3300"},

{{13,FsElpsContextId}, GetNextIndexFsElpsContextTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsElpsContextTableINDEX, 1, 0, 0, NULL},

{{13,FsElpsContextSystemControl}, GetNextIndexFsElpsContextTable, FsElpsContextSystemControlGet, FsElpsContextSystemControlSet, FsElpsContextSystemControlTest, FsElpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsContextTableINDEX, 1, 0, 0, "2"},

{{13,FsElpsContextModuleStatus}, GetNextIndexFsElpsContextTable, FsElpsContextModuleStatusGet, FsElpsContextModuleStatusSet, FsElpsContextModuleStatusTest, FsElpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsContextTableINDEX, 1, 0, 0, "2"},

{{13,FsElpsContextTraceInputString}, GetNextIndexFsElpsContextTable, FsElpsContextTraceInputStringGet, FsElpsContextTraceInputStringSet, FsElpsContextTraceInputStringTest, FsElpsContextTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsElpsContextTableINDEX, 1, 0, 0, "critical"},

{{13,FsElpsContextEnableTrap}, GetNextIndexFsElpsContextTable, FsElpsContextEnableTrapGet, FsElpsContextEnableTrapSet, FsElpsContextEnableTrapTest, FsElpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsContextTableINDEX, 1, 0, 0, "1"},

{{13,FsElpsContextVlanGroupManager}, GetNextIndexFsElpsContextTable, FsElpsContextVlanGroupManagerGet, FsElpsContextVlanGroupManagerSet, FsElpsContextVlanGroupManagerTest, FsElpsContextTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsContextTableINDEX, 1, 0, 0, "1"},

{{13,FsElpsPgConfigPgId}, GetNextIndexFsElpsPgConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigType}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigTypeGet, FsElpsPgConfigTypeSet, FsElpsPgConfigTypeTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsElpsPgConfigServiceType}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigServiceTypeGet, FsElpsPgConfigServiceTypeSet, FsElpsPgConfigServiceTypeTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsElpsPgConfigMonitorMechanism}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigMonitorMechanismGet, FsElpsPgConfigMonitorMechanismSet, FsElpsPgConfigMonitorMechanismTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsElpsPgConfigIngressPort}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigIngressPortGet, FsElpsPgConfigIngressPortSet, FsElpsPgConfigIngressPortTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigWorkingPort}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigWorkingPortGet, FsElpsPgConfigWorkingPortSet, FsElpsPgConfigWorkingPortTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigProtectionPort}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigProtectionPortGet, FsElpsPgConfigProtectionPortSet, FsElpsPgConfigProtectionPortTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigWorkingServiceValue}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigWorkingServiceValueGet, FsElpsPgConfigWorkingServiceValueSet, FsElpsPgConfigWorkingServiceValueTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigProtectionServiceValue}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigProtectionServiceValueGet, FsElpsPgConfigProtectionServiceValueSet, FsElpsPgConfigProtectionServiceValueTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigOperType}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigOperTypeGet, FsElpsPgConfigOperTypeSet, FsElpsPgConfigOperTypeTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsElpsPgConfigProtType}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigProtTypeGet, FsElpsPgConfigProtTypeSet, FsElpsPgConfigProtTypeTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, "1"},

{{13,FsElpsPgConfigName}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigNameGet, FsElpsPgConfigNameSet, FsElpsPgConfigNameTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigRowStatus}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigRowStatusGet, FsElpsPgConfigRowStatusSet, FsElpsPgConfigRowStatusTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 1, NULL},

{{13,FsElpsPgConfigWorkingServicePointer}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigWorkingServicePointerGet, FsElpsPgConfigWorkingServicePointerSet, FsElpsPgConfigWorkingServicePointerTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigWorkingReverseServicePointer}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigWorkingReverseServicePointerGet, FsElpsPgConfigWorkingReverseServicePointerSet, FsElpsPgConfigWorkingReverseServicePointerTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigProtectionServicePointer}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigProtectionServicePointerGet, FsElpsPgConfigProtectionServicePointerSet, FsElpsPgConfigProtectionServicePointerTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigProtectionReverseServicePointer}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigProtectionReverseServicePointerGet, FsElpsPgConfigProtectionReverseServicePointerSet, FsElpsPgConfigProtectionReverseServicePointerTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgConfigWorkingInstanceId}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigWorkingInstanceIdGet, FsElpsPgConfigWorkingInstanceIdSet, FsElpsPgConfigWorkingInstanceIdTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, "0"},

{{13,FsElpsPgConfigProtectionInstanceId}, GetNextIndexFsElpsPgConfigTable, FsElpsPgConfigProtectionInstanceIdGet, FsElpsPgConfigProtectionInstanceIdSet, FsElpsPgConfigProtectionInstanceIdTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, "0"},

{{13,FsElpsPgPscVersion}, GetNextIndexFsElpsPgConfigTable, FsElpsPgPscVersionGet, FsElpsPgPscVersionSet, FsElpsPgPscVersionTest, FsElpsPgConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgConfigTableINDEX, 2, 0, 0, "0"},

{{13,FsElpsPgCmdHoTime}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdHoTimeGet, FsElpsPgCmdHoTimeSet, FsElpsPgCmdHoTimeTest, FsElpsPgCmdTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCmdTableINDEX, 2, 0, 0, "0"},

{{13,FsElpsPgCmdWTR}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdWTRGet, FsElpsPgCmdWTRSet, FsElpsPgCmdWTRTest, FsElpsPgCmdTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCmdTableINDEX, 2, 0, 0, "5"},

{{13,FsElpsPgCmdExtCmd}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdExtCmdGet, FsElpsPgCmdExtCmdSet, FsElpsPgCmdExtCmdTest, FsElpsPgCmdTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgCmdTableINDEX, 2, 0, 0, "5"},

{{13,FsElpsPgCmdExtCmdStatus}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdExtCmdStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgCmdTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCmdLocalCondition}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdLocalConditionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgCmdTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCmdLocalConditionStatus}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdLocalConditionStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgCmdTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCmdFarEndRequest}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdFarEndRequestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgCmdTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCmdFarEndRequestStatus}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdFarEndRequestStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgCmdTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCmdActiveRequest}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdActiveRequestGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgCmdTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCmdSemState}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdSemStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgCmdTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCmdPgStatus}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdPgStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgCmdTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCmdApsPeriodicTime}, GetNextIndexFsElpsPgCmdTable, FsElpsPgCmdApsPeriodicTimeGet, FsElpsPgCmdApsPeriodicTimeSet, FsElpsPgCmdApsPeriodicTimeTest, FsElpsPgCmdTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCmdTableINDEX, 2, 0, 0, "5"},

{{13,FsElpsPgCfmWorkingMEG}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmWorkingMEGGet, FsElpsPgCfmWorkingMEGSet, FsElpsPgCfmWorkingMEGTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmWorkingME}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmWorkingMEGet, FsElpsPgCfmWorkingMESet, FsElpsPgCfmWorkingMETest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmWorkingMEP}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmWorkingMEPGet, FsElpsPgCfmWorkingMEPSet, FsElpsPgCfmWorkingMEPTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmProtectionMEG}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmProtectionMEGGet, FsElpsPgCfmProtectionMEGSet, FsElpsPgCfmProtectionMEGTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmProtectionME}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmProtectionMEGet, FsElpsPgCfmProtectionMESet, FsElpsPgCfmProtectionMETest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmProtectionMEP}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmProtectionMEPGet, FsElpsPgCfmProtectionMEPSet, FsElpsPgCfmProtectionMEPTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmRowStatus}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmRowStatusGet, FsElpsPgCfmRowStatusSet, FsElpsPgCfmRowStatusTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 1, NULL},

{{13,FsElpsPgCfmWorkingReverseMEG}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmWorkingReverseMEGGet, FsElpsPgCfmWorkingReverseMEGSet, FsElpsPgCfmWorkingReverseMEGTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmWorkingReverseME}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmWorkingReverseMEGet, FsElpsPgCfmWorkingReverseMESet, FsElpsPgCfmWorkingReverseMETest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmWorkingReverseMEP}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmWorkingReverseMEPGet, FsElpsPgCfmWorkingReverseMEPSet, FsElpsPgCfmWorkingReverseMEPTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmProtectionReverseMEG}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmProtectionReverseMEGGet, FsElpsPgCfmProtectionReverseMEGSet, FsElpsPgCfmProtectionReverseMEGTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmProtectionReverseME}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmProtectionReverseMEGet, FsElpsPgCfmProtectionReverseMESet, FsElpsPgCfmProtectionReverseMETest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgCfmProtectionReverseMEP}, GetNextIndexFsElpsPgCfmTable, FsElpsPgCfmProtectionReverseMEPGet, FsElpsPgCfmProtectionReverseMEPSet, FsElpsPgCfmProtectionReverseMEPTest, FsElpsPgCfmTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsElpsPgCfmTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgServiceListValue}, GetNextIndexFsElpsPgServiceListTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsElpsPgServiceListTableINDEX, 3, 0, 0, NULL},

{{13,FsElpsPgServiceListRowStatus}, GetNextIndexFsElpsPgServiceListTable, FsElpsPgServiceListRowStatusGet, FsElpsPgServiceListRowStatusSet, FsElpsPgServiceListRowStatusTest, FsElpsPgServiceListTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgServiceListTableINDEX, 3, 0, 1, NULL},

{{13,FsElpsPgShareProtectionPort}, GetNextIndexFsElpsPgShareTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsElpsPgShareTableINDEX, 3, 0, 0, NULL},

{{13,FsElpsPgSharePgStatus}, GetNextIndexFsElpsPgShareTable, FsElpsPgSharePgStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsElpsPgShareTableINDEX, 3, 0, 0, NULL},

{{13,FsElpsPgStatsAutoProtectionSwitchCount}, GetNextIndexFsElpsPgStatsTable, FsElpsPgStatsAutoProtectionSwitchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgStatsForcedSwitchCount}, GetNextIndexFsElpsPgStatsTable, FsElpsPgStatsForcedSwitchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgStatsManualSwitchCount}, GetNextIndexFsElpsPgStatsTable, FsElpsPgStatsManualSwitchCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgStatsClearStatistics}, GetNextIndexFsElpsPgStatsTable, FsElpsPgStatsClearStatisticsGet, FsElpsPgStatsClearStatisticsSet, FsElpsPgStatsClearStatisticsTest, FsElpsPgStatsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgStatsApsPktTxCount}, GetNextIndexFsElpsPgStatsTable, FsElpsPgStatsApsPktTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgStatsApsPktRxCount}, GetNextIndexFsElpsPgStatsTable, FsElpsPgStatsApsPktRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgStatsApsPktDiscardCount}, GetNextIndexFsElpsPgStatsTable, FsElpsPgStatsApsPktDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgLRSFRxTime}, GetNextIndexFsElpsPgStatsTable, FsElpsPgLRSFRxTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgLRSFTxTime}, GetNextIndexFsElpsPgStatsTable, FsElpsPgLRSFTxTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgFRSFRxTime}, GetNextIndexFsElpsPgStatsTable, FsElpsPgFRSFRxTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgStateChgTime}, GetNextIndexFsElpsPgStatsTable, FsElpsPgStateChgTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsElpsPgStatsTableINDEX, 2, 0, 0, NULL},

{{13,FsElpsPgServiceListId}, GetNextIndexFsElpsPgServiceListPointerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsElpsPgServiceListPointerTableINDEX, 3, 0, 0, NULL},

{{13,FsElpsPgWorkingServiceListPointer}, GetNextIndexFsElpsPgServiceListPointerTable, FsElpsPgWorkingServiceListPointerGet, FsElpsPgWorkingServiceListPointerSet, FsElpsPgWorkingServiceListPointerTest, FsElpsPgServiceListPointerTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsElpsPgServiceListPointerTableINDEX, 3, 0, 0, NULL},

{{13,FsElpsPgWorkingReverseServiceListPointer}, GetNextIndexFsElpsPgServiceListPointerTable, FsElpsPgWorkingReverseServiceListPointerGet, FsElpsPgWorkingReverseServiceListPointerSet, FsElpsPgWorkingReverseServiceListPointerTest, FsElpsPgServiceListPointerTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsElpsPgServiceListPointerTableINDEX, 3, 0, 0, NULL},

{{13,FsElpsPgProtectionServiceListPointer}, GetNextIndexFsElpsPgServiceListPointerTable, FsElpsPgProtectionServiceListPointerGet, FsElpsPgProtectionServiceListPointerSet, FsElpsPgProtectionServiceListPointerTest, FsElpsPgServiceListPointerTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsElpsPgServiceListPointerTableINDEX, 3, 0, 0, NULL},

{{13,FsElpsPgProtectionReverseServiceListPointer}, GetNextIndexFsElpsPgServiceListPointerTable, FsElpsPgProtectionReverseServiceListPointerGet, FsElpsPgProtectionReverseServiceListPointerSet, FsElpsPgProtectionReverseServiceListPointerTest, FsElpsPgServiceListPointerTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsElpsPgServiceListPointerTableINDEX, 3, 0, 0, NULL},

{{13,FsElpsPgServiceListPointerRowStatus}, GetNextIndexFsElpsPgServiceListPointerTable, FsElpsPgServiceListPointerRowStatusGet, FsElpsPgServiceListPointerRowStatusSet, FsElpsPgServiceListPointerRowStatusTest, FsElpsPgServiceListPointerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsElpsPgServiceListPointerTableINDEX, 3, 0, 1, NULL},

{{11,FsElpsTrapContextName}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsElpsTrapSwitchingMechanism}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsElpsTrapMismatchType}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsElpsTypeOfFailure}, NULL, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_ACCESSIBLEFORNOTIFY, NULL, 0, 0, 0, NULL},

{{11,FsElpsStatsOneIsToOneApsPktTxCount}, NULL, FsElpsStatsOneIsToOneApsPktTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsElpsStatsOneIsToOneApsPktRxCount}, NULL, FsElpsStatsOneIsToOneApsPktRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsElpsStatsOneIsToOneApsPktDiscardCount}, NULL, FsElpsStatsOneIsToOneApsPktDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsElpsStatsOnePlusOneApsPktTxCount}, NULL, FsElpsStatsOnePlusOneApsPktTxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsElpsStatsOnePlusOneApsPktRxCount}, NULL, FsElpsStatsOnePlusOneApsPktRxCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsElpsStatsOnePlusOneApsPktDiscardCount}, NULL, FsElpsStatsOnePlusOneApsPktDiscardCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

};
tMibData fselpsEntry = { 85, fselpsMibEntry };

#endif /* _FSELPSDB_H */

