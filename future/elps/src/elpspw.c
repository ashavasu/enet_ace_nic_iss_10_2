/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpspw.c,v 1.4 2011/06/13 06:59:38 siva Exp $
 *
 * Description: This file contains the Pw related
 * utility functions.
 *****************************************************************************/
#ifndef _ELPSPW_C_
#define _ELPSPW_C_

#include "elpsinc.h"
#include "mplsapi.h"

/*****************************************************************************
 * FUNCTION NAME    : ElpsPwUpdatePwTableBaseOid 
 *
 * DESCRIPTION      : This function updates the global array 'gau4PwTableOid'
 *                    with Pseudowire table base OID
 * 
 * INPUT            : None
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
ElpsPwUpdatePwTableBaseOid (VOID)
{
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "ElpsPwUpdatePwTableBaseOid:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "ElpsPwUpdatePwTableBaseOid:"
                   "Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    pInMplsApiInfo->u4SubReqType = MPLS_GET_BASE_PW_OID;
    pInMplsApiInfo->u4ContextId = 0;
    pInMplsApiInfo->u4SrcModId = ELPS_MODULE;

    /* Here return value is not checked as the MplsApiHandleExternalRequest
     * function will not fail when fetching Pseudowire table base OID */
    ElpsPortMplsApiHandleExtRequest (MPLS_GET_SERVICE_POINTER_OID,
                                     pInMplsApiInfo, pOutMplsApiInfo);

    if (pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen != 0)
    {
        MEMCPY (gau4PwTableOid,
                pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen *
                sizeof (UINT4));
        gu4PwTableBaseOidLen =
            (UINT4) pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen;
    }

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME    : ElpsPwGetPwServicePointer
 *
 * DESCRIPTION      : This function constructs Pseudowire Row Pointer from
 *                    the given Pseudowire Information
 * 
 * INPUT            : pPwServiceInfo- Pointer to Pseudowire Information
 *                    pPgConfigServicePointer- Pointer to the Pseudowire Row
 *                                             pointer
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : None
 ****************************************************************************/
PUBLIC VOID
ElpsPwGetPwServicePointer (tElpsPwInfo * pPwServiceInfo,
                           tSNMP_OID_TYPE * pPgConfigServicePointer)
{
    /* If the value is not configured */
    if (pPwServiceInfo->u4PwIndex == 0)
    {
        MEMSET (pPgConfigServicePointer->pu4_OidList, 0,
                ELPS_PW_TABLE_DEF_OFFSET * sizeof (UINT4));
        pPgConfigServicePointer->u4_Length = ELPS_ERR_OID_LEN;
    }
    else
    {
        pPgConfigServicePointer->u4_Length = ELPS_PW_TABLE_DEF_OFFSET;
        MEMCPY (pPgConfigServicePointer->pu4_OidList,
                gau4PwTableOid, ELPS_PW_TABLE_DEF_OFFSET * sizeof (UINT4));

        /* Fill the Pw Index */
        pPgConfigServicePointer->
            pu4_OidList[ELPS_PW_TABLE_DEF_OFFSET] = pPwServiceInfo->u4PwIndex;
        pPgConfigServicePointer->u4_Length++;
    }
}

/*****************************************************************************
 * FUNCTION NAME    : ElpsPwSetPwServiceInfo
 *
 * DESCRIPTION      : This function gets Pseudowire Information from Row
 *                    Pointer and stores it into given 'tElpsPwInfo' structure.
 * 
 * INPUT            : pPwServiceInfo- Pointer to 'tElpsPwInfo' structure
 *                    pPgConfigServicePointer- Row pointer
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
ElpsPwSetPwServiceInfo (tElpsPwInfo * pPwServiceInfo,
                        tSNMP_OID_TYPE * pPgConfigServicePointer)
{
    UINT4               u4PwIndex = 0;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    UINT4               u4GlobalId = 0;
    UINT4               u4NodeId = 0;

    /* Getting the Pw Index */
    u4PwIndex = pPgConfigServicePointer->pu4_OidList[ELPS_PW_TABLE_DEF_OFFSET];

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "ElpsPwSetPwServiceInfo:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "ElpsPwSetPwServiceInfo:"
                   " Allocation of memory for Mpls Out Api "
                   "Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
    pInMplsApiInfo->u4ContextId = 0;
    pInMplsApiInfo->u4SrcModId = ELPS_MODULE;

    pInMplsApiInfo->InPathId.PwId.u4PwIndex = u4PwIndex;

    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                         pInMplsApiInfo,
                                         pOutMplsApiInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pOutMplsApiInfo);
        return OSIX_FAILURE;
    }

    pPwServiceInfo->u4PwIndex = u4PwIndex;
    pPwServiceInfo->u4VcId = pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId;

    if (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.u4NodeType
        == MPLS_ADDR_TYPE_IPV4)
    {
        pPwServiceInfo->u4PeerAddr = pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
            DstNodeId.MplsRouterId.u4_addr[0];
    }
    else if (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.u4NodeType
             == MPLS_ADDR_TYPE_GLOBAL_NODE_ID)
    {
        u4GlobalId = pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
            MplsGlobalNodeId.u4GlobalId;
        u4NodeId = pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
            MplsGlobalNodeId.u4NodeId;
        MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
        MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));
    }
    pInMplsApiInfo->u4SubReqType = MPLS_GET_LOCALNUM_FROM_GLBNODEID;
    pInMplsApiInfo->u4ContextId = 0;
    pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
    pInMplsApiInfo->InNodeId.MplsGlobalNodeId.u4GlobalId = u4GlobalId;
    pInMplsApiInfo->InNodeId.MplsGlobalNodeId.u4NodeId = u4NodeId;

    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_NODE_ID,
                                         pInMplsApiInfo,
                                         pOutMplsApiInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pOutMplsApiInfo);
        return SNMP_FAILURE;
    }
    pPwServiceInfo->u4PeerAddr = pOutMplsApiInfo->OutLocalNum;

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME    : ElpsPwGetMegInfo
 *
 * DESCRIPTION      : This function is used to get MEG information associated
 *                    with an MPLS Event
 * 
 * INPUT            : pMplsEvent- Pointer to MPLS Event
 *                    pMonitorInfo- Pointer to MEG information
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
ElpsPwGetMegInfo (tMplsEventNotif * pMplsEvent,
                  tElpsCfmMonitorInfo * pMonitorInfo)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;

    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsPwGetMegInfo:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsPwGetMegInfo:"
                   " Allocation of memory for Mpls Out Api "
                   "Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    pMplsApiInInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
    pMplsApiInInfo->u4ContextId = 0;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

    pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
        pMplsEvent->PathId.PwId.u4PwIndex;

    /* Api to get the Pw Information */
    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                         pMplsApiInInfo,
                                         pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);

        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                   "ElpsPwGetMegInfo: Failed to get " "Tunnel Info!!!!\r\n"));
        return OSIX_FAILURE;
    }
    /* Store the Meg Information */
    pMonitorInfo->u4MegId = pMplsApiOutInfo->OutPwInfo.MplsMegId.u4MegIndex;
    pMonitorInfo->u4MeId = pMplsApiOutInfo->OutPwInfo.MplsMegId.u4MeIndex;
    pMonitorInfo->u4MepId = pMplsApiOutInfo->OutPwInfo.MplsMegId.u4MpIndex;

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pMplsApiOutInfo);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 * FUNCTION NAME    : ElpsPwGetPwIndex
 *
 * DESCRIPTION      : This function gets PwIndex from VC Identifier and Peer 
 *                    Address
 * 
 * INPUT            : u4VcId- VC Identifier
 *                    u4PeerAddr- Peer Address
 *                    u4AddressType- Peer Address Type
 *                    pu4PwIndex- Pointer to Pseudowire Index
 * 
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 ****************************************************************************/
PUBLIC INT4
ElpsPwGetPwIndex (UINT4 u4VcId,
                  UINT4 u4PeerAddr, UINT4 u4AddressType, UINT4 *pu4PwIndex)
{
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsPwGetPwIndex:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsPwGetPwIndex:"
                   " Allocation of memory for Mpls Out Api "
                   "Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    if (u4AddressType == ELPS_ADDR_TYPE_LOCAL_MAP_NUM)    /* GId-NId */
    {

        pInMplsApiInfo->u4SubReqType = MPLS_GET_GLBNODEID_FROM_LOCALNUM;
        pInMplsApiInfo->u4ContextId = 0;
        pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
        pInMplsApiInfo->InLocalNum = u4PeerAddr;

        /* Getting GId-NId from LocalMapNum */
        if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_NODE_ID,
                                             pInMplsApiInfo,
                                             pOutMplsApiInfo) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }

        MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

        pInMplsApiInfo->InPathId.PwId.DstNodeId.u4NodeType =
            MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
        pInMplsApiInfo->InPathId.PwId.DstNodeId.MplsGlobalNodeId.u4GlobalId =
            pOutMplsApiInfo->OutNodeId.u4GlobalId;
        pInMplsApiInfo->InPathId.PwId.DstNodeId.MplsGlobalNodeId.u4NodeId =
            pOutMplsApiInfo->OutNodeId.u4NodeId;

        MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));
    }
    else                        /* IP */
    {
        pInMplsApiInfo->InPathId.PwId.DstNodeId.MplsRouterId.u4_addr[0] =
            u4PeerAddr;
    }

    pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
    pInMplsApiInfo->u4ContextId = 0;
    pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
    pInMplsApiInfo->InPathId.PwId.u4VcId = u4VcId;

    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                         pInMplsApiInfo,
                                         pOutMplsApiInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pOutMplsApiInfo);
        return OSIX_FAILURE;
    }

    *pu4PwIndex = pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4PwIndex;

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);
    return OSIX_SUCCESS;
}

#endif /* _ELPSPW_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpspw.c                      */
/*-----------------------------------------------------------------------*/
