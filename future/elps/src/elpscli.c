/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: elpscli.c,v 1.39 2016/03/15 11:41:20 siva Exp $
 *
 * Description: This file contains the ELPS CLI related routines and 
 * utility functions.
 *****************************************************************************/
#ifndef _ELPSCLI_C_
#define _ELPSCLI_C_

#include "elpsinc.h"
#include "elpssem.h"
#include "fselpscli.h"
#include "mplsapi.h"

UINT4               gu4ElpsTrcLvl;

/***************************************************************************
 * FUNCTION NAME    : cli_process_elps_cmd 
 *
 * DESCRIPTION      : This function is exported to CLI module to handle the 
 *                    ELPS cli commands to take the corresponding action. 
 *                    Only SNMP Low level routines are called from CLI.
 *
 * INPUT            : Variable arguments
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
cli_process_elps_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[ELPS_CLI_MAX_ARGS];
    UINT1              *pu1SrvList = NULL;
    INT1                i1ArgNo = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = CLI_GET_CXT_ID ();
    UINT4               u4PgId = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4WtrTime = 0;
    UINT4               u4PeriodicTime = 0;
    UINT4               u4HoldOffTime = 0;
    INT4                i4Inst = 0;
    INT4                i4ConfigType = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4GroupId = 0;
    INT4                i4Args = 0;
    CliRegisterLock (CliHandle, ElpsApiLock, ElpsApiUnLock);

    ELPS_LOCK ();

    /* Check if the command is a switch mode command */
    if (u4ContextId == ELPS_CLI_INVALID_CONTEXT)
    {
        u4ContextId = ELPS_DEFAULT_CONTEXT_ID;
    }

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    /* Walk through the rest of the arguements and store in args array. 
     * Store ELPS_CLI_MAX_ARGS arguements at the max. 
     */

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == ELPS_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_ELPS_SHUTDOWN:

            i4RetStatus = ElpsCliSetSystemControl (CliHandle,
                                                   u4ContextId, ELPS_SHUTDOWN);
            break;

        case CLI_ELPS_START:

            i4RetStatus = ElpsCliSetSystemControl (CliHandle,
                                                   u4ContextId, ELPS_START);
            break;

        case CLI_ELPS_ENABLE:

            i4RetStatus = ElpsCliSetModuleStatus (CliHandle,
                                                  u4ContextId, ELPS_ENABLED);
            break;

        case CLI_ELPS_DISABLE:

            i4RetStatus = ElpsCliSetModuleStatus (CliHandle,
                                                  u4ContextId, ELPS_DISABLED);
            break;

        case CLI_ELPS_PG_CREATE:

            i4RetStatus = ElpsCliConfigProtectionGroup (CliHandle,
                                                        u4ContextId,
                                                        (*(UINT4 *) (args[0])),
                                                        CLI_ELPS_PG_CREATE);
            break;

        case CLI_ELPS_PG_DELETE:

            i4RetStatus = ElpsCliConfigProtectionGroup (CliHandle,
                                                        u4ContextId,
                                                        (*(UINT4 *)
                                                         (args[0])),
                                                        CLI_ELPS_PG_DELETE);
            break;

        case CLI_ELPS_NOTIFY_ENABLE:

            i4RetStatus = ElpsCliSetNotifyStatus (CliHandle,
                                                  u4ContextId, ELPS_SNMP_TRUE);
            break;

        case CLI_ELPS_NOTIFY_DISABLE:

            i4RetStatus = ElpsCliSetNotifyStatus (CliHandle,
                                                  u4ContextId, ELPS_SNMP_FALSE);
            break;

        case CLI_ELPS_SET_GROUP_MANAGER:

            i4RetStatus = ElpsCliSetVlanGroupManager (CliHandle, u4ContextId,
                                                      CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ELPS_PG_ACTIVATE:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliSetPgEnableStatus (CliHandle,
                                                    u4ContextId,
                                                    u4PgId, ELPS_PG_ACTIVATE);
            break;

        case CLI_ELPS_PG_DEACTIVATE:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliSetPgEnableStatus (CliHandle,
                                                    u4ContextId,
                                                    u4PgId, ELPS_PG_DEACTIVATE);
            break;

        case CLI_ELPS_PG_OPER_TYPE_AND_WTR:

            u4PgId = CLI_GET_PG_ID ();

            if (args[1] != NULL)
            {
                u4WtrTime = (*(UINT4 *) (args[1]));
            }

            i4RetStatus = ElpsCliSetPgOperTypeAndWTRTime
                (CliHandle, u4ContextId, u4PgId,
                 CLI_PTR_TO_I4 (args[0]), u4WtrTime);
            break;

        case CLI_ELPS_PG_HOLDOFF_TIME:

            u4PgId = CLI_GET_PG_ID ();

            if (args[0] != NULL)
            {
                u4HoldOffTime = (*(UINT4 *) (args[0]));
            }
            else
            {
                /* Set the default Hold-Off time interval */
                u4HoldOffTime = 0;
            }

            i4RetStatus = ElpsCliSetPgHoldOffTime (CliHandle, u4ContextId,
                                                   u4PgId, u4HoldOffTime);
            break;

        case CLI_ELPS_PG_WTR_TIME:

            u4PgId = CLI_GET_PG_ID ();

            if (args[0] != NULL)
            {
                u4WtrTime = (*(UINT4 *) (args[0]));
            }
            else
            {
                /* Set the default WTR time interval */
                u4WtrTime = ELPS_PG_DEF_WTR_TIME;
            }

            i4RetStatus = ElpsCliSetPgWTRTime (CliHandle, u4ContextId,
                                               u4PgId, u4WtrTime);
            break;

        case CLI_ELPS_PG_TIMERS:

            u4PgId = CLI_GET_PG_ID ();

            if (args[0] != NULL)
            {
                u4PeriodicTime = (*(UINT4 *) (args[0]));
            }
            else
            {
                /* Set the default periodic time interval */
                u4PeriodicTime = ELPS_PERIODIC_APS_PDU_INTERVAL;
            }

            if (args[1] != NULL)
            {
                u4HoldOffTime = (*(UINT4 *) (args[1]));
            }
            else
            {
                /* Set the default Hold-Off time interval */
                u4HoldOffTime = 0;
            }

            i4RetStatus = ElpsCliSetPgTimers (CliHandle, u4ContextId,
                                              u4PgId, u4PeriodicTime,
                                              u4HoldOffTime);
            break;

        case CLI_ELPS_PG_WORKING_INFO:

            u4PgId = CLI_GET_PG_ID ();
            i4RetStatus = CLI_SUCCESS;

            if (ElpsCliSetPgServiceType (CliHandle, u4ContextId, u4PgId,
                                         CLI_PTR_TO_I4 (args[0]))
                == CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }

            i4ConfigType = CLI_PTR_TO_I4 (args[1]);

            if (ElpsCliSetWorkingPort (CliHandle, u4ContextId,
                                       u4PgId, u4IfIndex) == CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pu1SrvList = UtlShMemAllocVlanList ();

            if (pu1SrvList == NULL)
            {
                ELPS_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (i4ConfigType == ELPS_PG_TYPE_INDIVIDUAL)
            {
                i4RetStatus = ElpsCliSetWorkingService
                    (CliHandle, u4ContextId, u4PgId, *((UINT4 *) args[2]));
            }
            else if (i4ConfigType == ELPS_PG_TYPE_LIST)
            {
                MEMSET (pu1SrvList, 0, ELPS_SRV_LIST_SIZE);

                if (CliStrToPortList ((UINT1 *) args[2], pu1SrvList,
                                      ELPS_SRV_LIST_SIZE, CFA_L2VLAN)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "Invalid Service List\r\n");
                    i4RetStatus = CLI_FAILURE;
                    UtlShMemFreeVlanList (pu1SrvList);
                    break;
                }

                i4RetStatus = ElpsCliSetWorkingServiceList
                    (CliHandle, u4ContextId, u4PgId, pu1SrvList);
            }
            else
            {
                i4RetStatus = ElpsCliSetWorkingServiceAsAll
                    (CliHandle, u4ContextId, u4PgId);
            }
            UtlShMemFreeVlanList (pu1SrvList);
            break;

        case CLI_ELPS_PG_PROTECT_INFO:

            u4PgId = CLI_GET_PG_ID ();
            i4RetStatus = CLI_SUCCESS;

            if (ElpsCliTestForWorkingInfo (u4ContextId, u4PgId, 0)
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Configuration Failed. Working "
                           "information needs to be configured prior to "
                           "protection information.\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4ConfigType = CLI_PTR_TO_I4 (args[1]);

            if (ElpsCliTestForConfigType (CliHandle, u4ContextId,
                                          u4PgId, i4ConfigType) == CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (ElpsCliSetProtectionPort (CliHandle, u4ContextId,
                                          u4PgId, u4IfIndex) == CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }

            pu1SrvList = UtlShMemAllocVlanList ();

            if (pu1SrvList == NULL)
            {
                ELPS_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (i4ConfigType == ELPS_PG_TYPE_INDIVIDUAL)
            {
                i4RetStatus = ElpsCliSetProtectionService
                    (CliHandle, u4ContextId, u4PgId, *((UINT4 *) args[2]));
            }
            else if (i4ConfigType == ELPS_PG_TYPE_LIST)
            {
                MEMSET (pu1SrvList, 0, ELPS_SRV_LIST_SIZE);

                if (CliStrToPortList ((UINT1 *) args[2], pu1SrvList,
                                      ELPS_SRV_LIST_SIZE, CFA_L2VLAN)
                    == CLI_FAILURE)
                {
                    CliPrintf (CliHandle, "Invalid Service List\r\n");
                    i4RetStatus = CLI_FAILURE;
                    UtlShMemFreeVlanList (pu1SrvList);
                    break;
                }

                i4RetStatus = ElpsCliSetProtectionServiceList
                    (CliHandle, u4ContextId, u4PgId, pu1SrvList);
            }
            UtlShMemFreeVlanList (pu1SrvList);
            break;

        case CLI_ELPS_PG_INGRESS_PORT:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliSetPgIngressPort (CliHandle, u4ContextId,
                                                   u4PgId, u4IfIndex);
            break;

        case CLI_ELPS_PG_MONITOR_MECH:

            u4PgId = CLI_GET_PG_ID ();

            if (args[1] != NULL)
            {
                i4RetStatus = ElpsCliSetPgPscVer (CliHandle,
                                                  u4ContextId,
                                                  u4PgId, (*(UINT4 *) args[1]));
                if (i4RetStatus == CLI_SUCCESS)
                {
                    i4RetStatus = ElpsCliSetPgMonitor (CliHandle,
                                                       u4ContextId,
                                                       u4PgId,
                                                       CLI_PTR_TO_I4 (args[0]));
                }
                break;
            }
            else
            {
                i4RetStatus = ElpsCliSetPgMonitor (CliHandle,
                                                   u4ContextId,
                                                   u4PgId,
                                                   CLI_PTR_TO_I4 (args[0]));
                break;
            }

        case CLI_ELPS_PG_WRKG_ECFM_CFG:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliSetWorkingCfmCfg (CliHandle,
                                                   u4ContextId,
                                                   u4PgId,
                                                   (*(UINT4 *) args[0]),
                                                   (*(UINT4 *) args[1]),
                                                   (*(UINT4 *) args[2]),
                                                   CLI_PTR_TO_U4 (args[3]));

            break;

        case CLI_ELPS_PG_PROT_ECFM_CFG:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliSetProtectionCfmCfg (CliHandle,
                                                      u4ContextId,
                                                      u4PgId,
                                                      (*(UINT4 *) args[0]),
                                                      (*(UINT4 *) args[1]),
                                                      (*(UINT4 *) args[2]),
                                                      CLI_PTR_TO_U4 (args[3]));
            break;

        case CLI_ELPS_PG_FS:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgLocalCommand (CliHandle,
                                                  u4ContextId,
                                                  u4PgId, ELPS_EXT_CMD_FS);
            break;

        case CLI_ELPS_PG_MS_PROTECTION:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgLocalCommand (CliHandle,
                                                  u4ContextId,
                                                  u4PgId, ELPS_EXT_CMD_MS);
            break;

        case CLI_ELPS_PG_MS_WORKING:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgLocalCommand (CliHandle,
                                                  u4ContextId,
                                                  u4PgId, ELPS_EXT_CMD_MS_W);
            break;

        case CLI_ELPS_PG_LOP:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgLocalCommand (CliHandle,
                                                  u4ContextId,
                                                  u4PgId, ELPS_EXT_CMD_LOP);
            break;

        case CLI_ELPS_PG_EXERCISE:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgLocalCommand (CliHandle,
                                                  u4ContextId,
                                                  u4PgId, ELPS_EXT_CMD_EXER);
            break;

        case CLI_ELPS_PG_CLEAR_EXT_CMD:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgClearExtCmd (CliHandle, u4ContextId, u4PgId,
                                                 CLI_PTR_TO_I4 (args[0]));
            break;

        case CLI_ELPS_PG_CLEAR:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgLocalCommand (CliHandle,
                                                  u4ContextId,
                                                  u4PgId, ELPS_EXT_CMD_CLR);
            break;

        case CLI_ELPS_PG_FREEZE:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgLocalCommand (CliHandle,
                                                  u4ContextId,
                                                  u4PgId, ELPS_EXT_CMD_FREEZE);
            break;

        case CLI_ELPS_PG_CLEAR_FREEZE:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliCfgLocalCommand (CliHandle,
                                                  u4ContextId,
                                                  u4PgId,
                                                  ELPS_EXT_CMD_CLR_FREEZE);
            break;

        case CLI_ELPS_PG_SET_NAME:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliConfigPgName (CliHandle,
                                               u4ContextId,
                                               u4PgId, (UINT1 *) args[0]);
            break;

        case CLI_ELPS_PG_RESET_NAME:

            u4PgId = CLI_GET_PG_ID ();

            i4RetStatus = ElpsCliConfigPgName (CliHandle,
                                               u4ContextId, u4PgId, NULL);
            break;

        case CLI_ELPS_GBL_DEBUG:
            if (args[0] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[0]);
                ElpsCliSetDebugLevel (CliHandle, i4Args);
            }

            i4RetStatus = ElpsCliSetGlobalDebug (CliHandle, ELPS_SNMP_TRUE);
            break;

        case CLI_ELPS_NO_GBL_DEBUG:

            i4RetStatus = ElpsCliSetGlobalDebug (CliHandle, ELPS_SNMP_FALSE);
            break;

        case CLI_ELPS_DEBUG:

            if (args[2] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[2]);
                ElpsCliSetDebugLevel (CliHandle, i4Args);
            }

            if (args[0] != NULL)
            {
                if (ElpsPortVcmIsSwitchExist
                    ((UINT1 *) args[0], &u4ContextId) != OSIX_TRUE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = ELPS_DEFAULT_CONTEXT_ID;
            }

            i4RetStatus = ElpsCliSetDebugs (CliHandle, u4ContextId,
                                            (UINT1 *) (args[1]));
            break;

        case CLI_ELPS_NO_DEBUG:

            if (args[0] != NULL)
            {
                if (ElpsPortVcmIsSwitchExist
                    ((UINT1 *) args[0], &u4ContextId) != OSIX_TRUE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Switch %s Does not exist.\r\n", args[0]);
                    i4RetStatus = CLI_FAILURE;
                    break;
                }
            }
            else
            {
                u4ContextId = ELPS_DEFAULT_CONTEXT_ID;
            }

            i4RetStatus = ElpsCliSetDebugs (CliHandle, u4ContextId,
                                            (UINT1 *) (args[1]));

            break;

        case CLI_ELPS_RAPID_TX_TIME:

            if (args[0] != NULL)
            {
                i4RetStatus = ElpsCliSetRapidTxTime (CliHandle, (*args[0]));
            }
            else
            {
                i4RetStatus = ElpsCliSetRapidTxTime (CliHandle,
                                                     ELPS_MAX_RAPID_TX_TIME);
            }

            break;

        case CLI_ELPS_CHANNEL_CODE:

            if (args[0] != NULL)
            {
                i4RetStatus = ElpsCliSetChannelCode (CliHandle, (*args[0]));
            }
            else
            {
                i4RetStatus =
                    ElpsCliSetChannelCode (CliHandle,
                                           ELPS_MIN_PSC_CHANNEL_CODE);
            }
            break;

        case CLI_ELPS_PG_LSP_INFO:

            u4PgId = CLI_GET_PG_ID ();

            if (ElpsCliSetPgServiceType (CliHandle, u4ContextId, u4PgId,
                                         ELPS_PG_SERVICE_TYPE_LSP)
                == CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4RetStatus = ElpsCliSetLspService (CliHandle,
                                                u4ContextId, u4PgId,
                                                CLI_PTR_TO_U4 (args[0]),
                                                (*args[1]),
                                                (*args[2]),
                                                (*args[3]),
                                                (*args[4]), 0, 0, 0, 0);

            break;

        case CLI_ELPS_PG_LSP_REV_INFO:

            u4PgId = (UINT4) CLI_GET_PG_ID ();

            if (ElpsCliSetPgServiceType (CliHandle, u4ContextId, u4PgId,
                                         ELPS_PG_SERVICE_TYPE_LSP)
                == CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            i4RetStatus = ElpsCliSetLspService (CliHandle,
                                                u4ContextId, u4PgId,
                                                CLI_PTR_TO_U4 (args[0]),
                                                (*args[1]), (*args[2]),
                                                (*args[3]), (*args[4]),
                                                (*args[5]), (*args[6]),
                                                (*args[7]), (*args[8]));
            break;

        case CLI_ELPS_PG_LSP_LIST_INFO:

            u4PgId = CLI_GET_PG_ID ();

            if (args[1] == NULL)
            {
                i4RetStatus = ElpsCliDeleteList (CliHandle, u4ContextId,
                                                 u4PgId, (*args[0]));
            }
            else
            {
                if (ElpsCliSetPgServiceType (CliHandle, u4ContextId, u4PgId,
                                             ELPS_PG_SERVICE_TYPE_LSP)
                    == CLI_FAILURE)
                {
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                if (args[6] != NULL)
                {
                    i4RetStatus = ElpsCliSetLspList (CliHandle,
                                                     u4ContextId, u4PgId,
                                                     (*args[0]),
                                                     CLI_PTR_TO_U4 (args[1]),
                                                     (*args[2]),
                                                     (*args[3]),
                                                     (*args[4]),
                                                     (*args[5]),
                                                     (*args[6]),
                                                     (*args[7]),
                                                     (*args[8]), (*args[9]));
                }
                else
                {
                    i4RetStatus = ElpsCliSetLspList (CliHandle,
                                                     u4ContextId, u4PgId,
                                                     (*args[0]),
                                                     CLI_PTR_TO_U4 (args[1]),
                                                     (*args[2]),
                                                     (*args[3]),
                                                     (*args[4]),
                                                     (*args[5]), 0, 0, 0, 0);
                }
            }
            break;

        case CLI_ELPS_PG_PW_INFO:

            u4PgId = CLI_GET_PG_ID ();

            if (ElpsCliSetPgServiceType (CliHandle, u4ContextId, u4PgId,
                                         ELPS_PG_SERVICE_TYPE_PW)
                == CLI_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }

            if (args[2] != NULL)
            {
                i4RetStatus = ElpsCliSetPwService (CliHandle,
                                                   u4ContextId, u4PgId,
                                                   CLI_PTR_TO_U4 (args[0]),
                                                   (*args[1]), (*args[2]),
                                                   CLI_PTR_TO_U4 (args[3]));
            }
            else
            {
                i4RetStatus = ElpsCliSetPwService (CliHandle,
                                                   u4ContextId, u4PgId,
                                                   CLI_PTR_TO_U4 (args[0]),
                                                   (*args[1]), 0,
                                                   ELPS_ADDR_TYPE_UCAST);
            }

            break;

        case CLI_ELPS_PG_PW_LIST_INFO:

            u4PgId = CLI_GET_PG_ID ();

            if (args[1] == NULL)
            {
                i4RetStatus = ElpsCliDeleteList (CliHandle, u4ContextId,
                                                 u4PgId, (*args[0]));
            }
            else
            {
                if (ElpsCliSetPgServiceType (CliHandle, u4ContextId, u4PgId,
                                             ELPS_PG_SERVICE_TYPE_PW)
                    == CLI_FAILURE)
                {
                    i4RetStatus = CLI_FAILURE;
                    break;
                }

                if (args[3] != NULL)
                {
                    i4RetStatus = ElpsCliSetPwList (CliHandle,
                                                    u4ContextId, u4PgId,
                                                    (*args[0]),
                                                    CLI_PTR_TO_U4 (args[1]),
                                                    (*args[2]), (*args[3]),
                                                    CLI_PTR_TO_U4 (args[4]));
                }
                else
                {
                    i4RetStatus = ElpsCliSetPwList (CliHandle,
                                                    u4ContextId, u4PgId,
                                                    (*args[0]),
                                                    CLI_PTR_TO_U4 (args[1]),
                                                    (*args[2]), 0,
                                                    ELPS_ADDR_TYPE_UCAST);
                }
            }
            break;

        case CLI_ELPS_MAP_WORKING_VLAN_GROUP_TO_PG:

            u4PgId = CLI_GET_PG_ID ();
            MEMCPY (&u4GroupId, args[0], sizeof (UINT4));

            i4RetStatus =
                ElpsCliSetWorkingVlanInstanceToPg (CliHandle, u4ContextId,
                                                   u4PgId, u4GroupId);

            break;

        case CLI_ELPS_MAP_PROTECTION_VLAN_GROUP_TO_PG:

            u4PgId = CLI_GET_PG_ID ();
            MEMCPY (&u4GroupId, args[0], sizeof (UINT4));

            i4RetStatus =
                ElpsCliSetProtectionVlanInstanceToPg (CliHandle, u4ContextId,
                                                      u4PgId, u4GroupId);

            break;

        case CLI_ELPS_UNMAP_WORKING_VLAN_GROUP:

            u4PgId = CLI_GET_PG_ID ();
            MEMCPY (&u4GroupId, args[0], sizeof (UINT4));

            i4RetStatus =
                ElpsCliUnmapWorkingVlanInstanceToPg (CliHandle, u4ContextId,
                                                     u4PgId, u4GroupId);
            break;

        case CLI_ELPS_UNMAP_PROTECTION_VLAN_GROUP:

            u4PgId = CLI_GET_PG_ID ();
            MEMCPY (&u4GroupId, args[0], sizeof (UINT4));

            i4RetStatus =
                ElpsCliUnmapProtectionVlanInstanceToPg (CliHandle, u4ContextId,
                                                        u4PgId, u4GroupId);
            break;
        case CLI_ELPS_PG_PROT_TYPE:

            u4PgId = CLI_GET_PG_ID ();
            if (args[0] != NULL)
            {
                i4RetStatus =
                    ElpsCliSetPgProtType (CliHandle, u4ContextId, u4PgId,
                                          CLI_PTR_TO_I4 (args[0]));
            }
            break;

        default:

            i4RetStatus = CLI_FAILURE;
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_ELPS_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gapc1ElpsCliErrString[u4ErrCode]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetStatus);

    ELPS_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : cli_process_elps_sh_cmd 
 *
 * DESCRIPTION      : This function is exported to CLI module to handle the 
 *                    ELPS Cli show commands. The display is taken care in 
 *                    this function.
 *
 * INPUT            : Variable arguments
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
cli_process_elps_sh_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[ELPS_CLI_MAX_ARGS];
    INT1                i1ArgNo = 0;
    UINT1              *pu1ContextName = NULL;
    UINT4               u4IfIndex = 0;
    UINT4               u4ContextId = ELPS_INVALID_CONTEXT_ID;
    UINT4               u4CurrContextId = ELPS_MAX_UINT4_VALUE;
    UINT4               u4PgId = 0;
    INT4                i4RetVal = CLI_FAILURE;
    INT4                i4Inst = 0;

    CliRegisterLock (CliHandle, ElpsApiLock, ElpsApiUnLock);

    ELPS_LOCK ();

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    /* NOTE: For EXEC mode commands we have to pass the context-name/NULL
     * After the u4IfIndex. (ie) In all the cli commands we are passing 
     * IfIndex as the first argument in variable argument list. Like that 
     * as the second argument we have to pass context-name*/
    pu1ContextName = va_arg (ap, UINT1 *);

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT1 *);
        if (i1ArgNo == ELPS_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    while (ElpsCliGetContextInfoForShowCmd (CliHandle, pu1ContextName,
                                            u4CurrContextId,
                                            &u4ContextId,
                                            u4Command) == OSIX_SUCCESS)
    {
        switch (u4Command)
        {
            case CLI_ELPS_SHOW_GLB_INFO:

                i4RetVal = ElpsCliShowGlobalInfo (CliHandle, u4ContextId);
                break;

            case CLI_ELPS_SHOW_PG_INFO:

                if (args[0] == NULL)
                {
                    u4PgId = 0;
                }
                else
                {
                    u4PgId = (*(UINT4 *) (VOID *) args[0]);
                }

                i4RetVal = ElpsCliShowPgInfo (CliHandle, u4ContextId, u4PgId,
                                              CLI_PTR_TO_I4 (args[1]));

                break;

            case CLI_ELPS_CLEAR_STATS_ALL:

                if (args[0] == NULL)
                {
                    u4PgId = 0;
                }
                else
                {
                    u4PgId = (*(UINT4 *) (VOID *) args[0]);
                }

                i4RetVal = ElpsCliClearPgStats (CliHandle, u4ContextId, u4PgId);
                break;

            case CLI_ELPS_SHOW_LIST_PROT:

                i4RetVal = ElpsCliShowPgListInProtPort (CliHandle, u4IfIndex);
                break;

            case CLI_ELPS_SHOW_DEBUG:

                i4RetVal = ElpsCliShowDebugging (CliHandle, u4ContextId);

                break;

            default:

                CliPrintf (CliHandle, "\r%% Unknown command \r\n");

                ELPS_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
        }

        if (pu1ContextName != NULL)
        {
            break;
        }

        if (u4Command == CLI_ELPS_SHOW_LIST_PROT)
        {
            break;
        }

        u4CurrContextId = u4ContextId;
    }

    CLI_SET_ERR (0);

    CLI_SET_CMD_STATUS (i4RetVal);

    ELPS_UNLOCK ();

    CliUnRegisterLock (CliHandle);

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  ElpsCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
ElpsCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    gu4ElpsTrcLvl = 0;

    UNUSED_PARAM (CliHandle);
    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        gu4ElpsTrcLvl =
            MGMT_TRC | BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC |
            OS_RESOURCE_TRC | CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC |
            ELPS_CRITICAL_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        gu4ElpsTrcLvl = CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC
            | INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC |
            ELPS_CRITICAL_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        gu4ElpsTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        gu4ElpsTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        gu4ElpsTrcLvl = INIT_SHUT_TRC | ALL_FAILURE_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        gu4ElpsTrcLvl = INIT_SHUT_TRC | ELPS_CRITICAL_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsGetElpsCfgPrompt 
 *
 * DESCRIPTION      : This function returns the prompt to be displayed 
 *                    for Protection group. It is exported to CLI module.
 *                    This function will be invoked when the System is 
 *                    running in SI mode.
 *
 * INPUT            : None
 *                    
 *
 * OUTPUT           : pi1ModeName - Mode String
 *                    pi1DispStr - Display string
 *
 * RETURNS          : TRUE/FALSE
 * 
 **************************************************************************/
INT1
ElpsGetElpsCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4PgId = 0;
    UINT4               u4Len = STRLEN ("pg-");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "pg-", u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4PgId = CLI_ATOI (pi1ModeName);

    /* 
     * No need to take lock here, since it is taken by
     * Cli in cli_process_elps_cmd.
     */
    CLI_SET_PG_ID (u4PgId);

    STRCPY (pi1DispStr, "(config-pg)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsGetVcmElpsCfgPrompt 
 *
 * DESCRIPTION      : This function returns the prompt to be displayed 
 *                    for Protection group. It is exported to CLI module.
 *                    This function will be invoked when the System is 
 *                    running in MI mode.
 *
 * INPUT            : None
 *                    
 *
 * OUTPUT           : pi1ModeName - Mode String
 *                    pi1DispStr - Display string
 *
 * RETURNS          : TRUE/FALSE
 * 
 **************************************************************************/
INT1
ElpsGetVcmElpsCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4PgId = 0;
    UINT4               u4Len = STRLEN ("pg-");

    if ((!pi1DispStr) || (!pi1ModeName))
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, "pg-", u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    u4PgId = CLI_ATOI (pi1ModeName);

    /* 
     * No need to take lock here, since it is taken by
     * Cli in cli_process_elps_cmd.
     */
    CLI_SET_PG_ID (u4PgId);

    STRCPY (pi1DispStr, "(config-switch-pg)#");

    return TRUE;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetSystemControl 
 *
 * DESCRIPTION      : This function sets the system control status of ELPS
 *                    module as configured from CLI.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4Status - System control status 
 *                    (ELPS_START/ELPS_SHUTDOWN)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetSystemControl (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsContextSystemControl (&u4ErrorCode, u4ContextId,
                                             i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsContextSystemControl (u4ContextId, i4Status) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetModuleStatus  
 *
 * DESCRIPTION      : This function sets the ELPS module status.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4Status - ELPS_PG_ENABLED/ELPS_PG_DISABLED
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE 
 * 
 **************************************************************************/
INT4
ElpsCliSetModuleStatus (tCliHandle CliHandle, UINT4 u4ContextId, INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsContextModuleStatus (&u4ErrorCode, u4ContextId,
                                            i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsContextModuleStatus (u4ContextId, i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliConfigProtectionGroup 
 *
 * DESCRIPTION      : This function configures the protection group. It also
 *                    changes the mode from VCM to PG mode.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    u1Action - Create/Delete 
 *                    (CLI_ELPS_PG_CREATE/CLI_ELPS_PG_DELETE)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliConfigProtectionGroup (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4PgId, UINT1 u1Action)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrorCode = 0;
    INT4                i4PgConfigRowStatus = 0;

    switch (u1Action)
    {
        case CLI_ELPS_PG_CREATE:

            MEMSET (au1Cmd, 0, sizeof (au1Cmd));

            /* Check if the Protection Group is already present */

            if (nmhGetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                               &i4PgConfigRowStatus)
                != SNMP_SUCCESS)
            {
                /* Create the Entry as it is not present */

                if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrorCode,
                                                      u4ContextId,
                                                      u4PgId,
                                                      CREATE_AND_WAIT) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                                   CREATE_AND_WAIT) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

            }

            /* ENTER Protection Group Mode */
            SPRINTF ((CHR1 *) au1Cmd, "%s%u", CLI_ELPS_PG_MODE, u4PgId);
            if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to enter into Protection "
                           "Group configuration mode\r\n");
                return CLI_FAILURE;
            }

            break;

        case CLI_ELPS_PG_DELETE:

            /* Check if Entry is not present */
            if (nmhGetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                               &i4PgConfigRowStatus)
                != SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%% Protection Group entry %d is not "
                           "present\n", u4PgId);
                return CLI_FAILURE;
            }

            if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrorCode,
                                                  u4ContextId,
                                                  u4PgId,
                                                  DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                               DESTROY) == SNMP_FAILURE)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
            break;

        default:
            return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetNotifyStatus 
 *
 * DESCRIPTION      : This function enables or disables the trap notification
 *                    for ELPS module.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4TrapStatus - Trap Status (SNMP_TRUE/SNMP_FALSE)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetNotifyStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                        INT4 i4TrapStatus)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsContextEnableTrap (&u4ErrorCode, u4ContextId,
                                          i4TrapStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsContextEnableTrap (u4ContextId, i4TrapStatus) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetVlanGroupManager 
 *
 * DESCRIPTION      : This function  configures the module that manages the 
 *                    grouping of vlans in a context.
 *
 * INPUT            : CliHandle   - Handle to the CLI 
 *                  : u4ContextId - Context Identifier
 *                    i4VlanGroupManager - Module Name ( ELPS/ MSTP)
 *
 * OUTPUT           : None.                                
 * 
 * RETURNS          : CLI_SUCCESS / CLI_FAILURE 
 *
 **************************************************************************/
INT4
ElpsCliSetVlanGroupManager (tCliHandle CliHandle, UINT4 u4ContextId,
                            INT4 i4VlanGroupManager)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsContextVlanGroupManager (&u4ErrorCode, u4ContextId,
                                                i4VlanGroupManager) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsContextVlanGroupManager (u4ContextId,
                                             i4VlanGroupManager) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetPgEnableStatus 
 *
 * DESCRIPTION      : This function activates or deactivates the Protection
 *                    group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    i4PgEnableStatus - Activate or Deactivate 
 *                    (ELPS_PG_ACTIVATE/ELPS_PG_DEACTIVATE)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetPgEnableStatus (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4PgId, INT4 i4PgEnableStatus)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4PgConfigRowStatus = 0;
    INT4                i4PgCfmRowStatus = 0;
    INT4                i4MonitorType = 0;

    if (nmhGetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                       &i4PgConfigRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Protection Group Entry %d is not created \r\n",
                   u4PgId);
        return CLI_FAILURE;
    }

    switch (i4PgEnableStatus)
    {
        case ELPS_PG_ACTIVATE:

            if (i4PgConfigRowStatus != ACTIVE)
            {
                nmhGetFsElpsPgConfigMonitorMechanism (u4ContextId, u4PgId,
                                                      &i4MonitorType);

                if (i4MonitorType != ELPS_PG_MONITOR_MECH_NONE)
                {
                    /* The PgCfm Table entry has to be active before activating 
                     * the Pg Config Table. */
                    nmhGetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                                &i4PgCfmRowStatus);

                    if (i4PgCfmRowStatus == NOT_IN_SERVICE)
                    {
                        if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrorCode,
                                                           u4ContextId,
                                                           u4PgId, ACTIVE) ==
                            SNMP_FAILURE)
                        {
                            return CLI_FAILURE;
                        }

                        if (nmhSetFsElpsPgCfmRowStatus (u4ContextId,
                                                        u4PgId, ACTIVE) ==
                            SNMP_FAILURE)
                        {
                            CLI_FATAL_ERROR (CliHandle);
                            return CLI_FAILURE;
                        }
                    }
                    else if (i4PgCfmRowStatus != ACTIVE)
                    {
                        CliPrintf (CliHandle, "\r%% PG CFM entry is Not in "
                                   "Active State.\r\n");
                        return CLI_FAILURE;
                    }
                }
                if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrorCode,
                                                      u4ContextId,
                                                      u4PgId, ACTIVE) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                                   ACTIVE) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Failed to activate the PG; Verify the"
                            " PG configurations\r\n");
                    return CLI_FAILURE;
                }
            }

            break;

        case ELPS_PG_DEACTIVATE:

            if (i4PgConfigRowStatus != NOT_IN_SERVICE)
            {
                if (nmhTestv2FsElpsPgConfigRowStatus (&u4ErrorCode,
                                                      u4ContextId,
                                                      u4PgId,
                                                      NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFsElpsPgConfigRowStatus (u4ContextId,
                                                   u4PgId,
                                                   NOT_IN_SERVICE) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Falied to deactivate the PG\r\n");
                    return CLI_FAILURE;
                }
            }

            break;

        default:
            return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetPgOperTypeAndWTRTime
 *
 * DESCRIPTION      : This function sets the operation type of the protection 
 *                    group. Also when the Operation type is revertive, 
 *                    this command allows the WTR interval configuration.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    i4OperType - Revertive/Non-revertive
 *                    u4WTRTime - Wait-to-Restore Interval
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetPgOperTypeAndWTRTime (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4PgId, INT4 i4OperType, UINT4 u4WTRTime)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4OldType = 0;

    if (nmhGetFsElpsPgConfigOperType (u4ContextId, u4PgId, &i4OldType)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4OldType != i4OperType)
    {
        if (nmhTestv2FsElpsPgConfigOperType (&u4ErrorCode, u4ContextId, u4PgId,
                                             i4OperType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigOperType (u4ContextId, u4PgId, i4OperType) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4WTRTime != 0)
    {
        if (ElpsCliSetPgWTRTime (CliHandle, u4ContextId, u4PgId,
                                 u4WTRTime) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetPgTimers 
 *
 * DESCRIPTION      : This function sets the interval for the Periodic timer 
 *                    and the Hold-Off Timer.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    u4PeriodicTime - Periodic Timer Interval
 *                    u4HoldOffTime - Hold-Off Timer Interval
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetPgTimers (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                    UINT4 u4PeriodicTime, UINT4 u4HoldOffTime)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgCmdApsPeriodicTime (&u4ErrorCode, u4ContextId, u4PgId,
                                             u4PeriodicTime) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgCmdApsPeriodicTime (u4ContextId, u4PgId,
                                          u4PeriodicTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (ElpsCliSetPgHoldOffTime (CliHandle, u4ContextId, u4PgId,
                                 u4HoldOffTime) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetPgHoldOffTime 
 *
 * DESCRIPTION      : This function sets the Hold-Off Timer interval and also
 *                    thereby enabling the Hold-off timer functionality.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    u4HoldOffTime - Hold-Off Timer Interval
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetPgHoldOffTime (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                         UINT4 u4HoldOffTime)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgCmdHoTime (&u4ErrorCode, u4ContextId, u4PgId,
                                    u4HoldOffTime) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgCmdHoTime (u4ContextId, u4PgId, u4HoldOffTime) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetPgWTRTime 
 *
 * DESCRIPTION      : This function sets the Wait To Restore Timer 
 *                    time interval.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    u4WTRTime - Wait To Restore Timer interval
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetPgWTRTime (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                     UINT4 u4WTRTime)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgCmdWTR (&u4ErrorCode, u4ContextId, u4PgId,
                                 u4WTRTime) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgCmdWTR (u4ContextId, u4PgId, u4WTRTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetPgIngressPort
 *
 * DESCRIPTION      : This function sets the Ingress Port for the 
 *                    Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    u4IngressPortId - Ingress Port
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetPgIngressPort (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4PgId, UINT4 u4IngressPortId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgConfigIngressPort (&u4ErrorCode, u4ContextId, u4PgId,
                                            u4IngressPortId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigIngressPort (u4ContextId, u4PgId,
                                         u4IngressPortId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetPgServiceType
 *
 * DESCRIPTION      : This function sets the Service Type for the
 *                    Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    i4ServiceType - Service Type (vlan)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetPgServiceType (tCliHandle CliHandle, UINT4 u4ContextId,
                         UINT4 u4PgId, INT4 i4ServiceType)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgConfigServiceType (&u4ErrorCode, u4ContextId, u4PgId,
                                            i4ServiceType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigServiceType (u4ContextId, u4PgId, i4ServiceType)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetWorkingPort
 *
 * DESCRIPTION      : This function sets the working port for the
 *                    Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    u4IfIndex - Working Interface Index.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetWorkingPort (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4PgId, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgConfigWorkingPort (&u4ErrorCode, u4ContextId, u4PgId,
                                            u4IfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigWorkingPort (u4ContextId, u4PgId, u4IfIndex)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetWorkingService
 *
 * DESCRIPTION      : This function sets the working service for the 
 *                    Protection Group. Also this function will take care of 
 *                    updating the protection group configuration type 
 *                    as 'individual'.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    u4ServiceId - Working Service Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetWorkingService (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4PgId, UINT4 u4ServiceId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CfgType = 0;

    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4CfgType);

    if (i4CfgType != ELPS_PG_TYPE_INDIVIDUAL)
    {
        if (nmhTestv2FsElpsPgConfigType (&u4ErrorCode, u4ContextId, u4PgId,
                                         ELPS_PG_TYPE_INDIVIDUAL)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigType (u4ContextId, u4PgId,
                                      ELPS_PG_TYPE_INDIVIDUAL) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsElpsPgConfigWorkingServiceValue (&u4ErrorCode, u4ContextId,
                                                    u4PgId, u4ServiceId)
        == SNMP_FAILURE)
    {
        if (i4CfgType != ELPS_PG_TYPE_INDIVIDUAL)
        {
            nmhSetFsElpsPgConfigType (u4ContextId, u4PgId, i4CfgType);
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigWorkingServiceValue (u4ContextId, u4PgId,
                                                 u4ServiceId) == SNMP_FAILURE)
    {
        if (i4CfgType != ELPS_PG_TYPE_INDIVIDUAL)
        {
            nmhSetFsElpsPgConfigType (u4ContextId, u4PgId, i4CfgType);
        }
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetWorkingServiceList
 *
 * DESCRIPTION      : This function sets the working service for the 
 *                    Protection Group. Also this function will take care of 
 *                    updating the protection group configuration type 
 *                    as 'individual'.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    pu1SrvList - Service List
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetWorkingServiceList (tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT4 u4PgId, UINT1 *pu1SrvList)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4ServiceId = 0;
    UINT4               u4NextServiceId = 0;
    INT4                i4CfgType = 0;
    INT4                i4RowStatus = 0;
    UINT2               u2ByteIndex = 0;
    UINT2               u2BitIndex = 0;
    UINT1               u1SrvFlag = 0;

    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4CfgType);

    if (i4CfgType != ELPS_PG_TYPE_LIST)
    {
        if (nmhTestv2FsElpsPgConfigType (&u4ErrorCode, u4ContextId, u4PgId,
                                         ELPS_PG_TYPE_LIST) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigType (u4ContextId, u4PgId,
                                      ELPS_PG_TYPE_LIST) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        /* In case - the PG config type is already list (i.e. some set of
         * service list is already in use), calling low level routine is not
         * sufficient, existing values needs to be deleted first. */
        if (nmhGetFirstIndexFsElpsPgServiceListTable (&u4NextContextId,
                                                      &u4NextPgId,
                                                      &u4NextServiceId)
            == SNMP_SUCCESS)
        {
            do
            {
                if ((u4ContextId != (UINT4) u4NextContextId) &&
                    (u4PgId != u4NextPgId))
                {
                    break;
                }

                if (nmhTestv2FsElpsPgServiceListRowStatus
                    (&u4ErrorCode, u4ContextId, u4NextPgId, u4NextServiceId,
                     DESTROY) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (nmhSetFsElpsPgServiceListRowStatus (u4ContextId, u4NextPgId,
                                                        u4NextServiceId,
                                                        DESTROY) ==
                    SNMP_FAILURE)
                {
                    CLI_FATAL_ERROR (CliHandle);
                    return CLI_FAILURE;
                }

                /* u4NextServiceId is deleted now from the service list */

                u4ServiceId = u4NextServiceId;
            }
            while (nmhGetNextIndexFsElpsPgServiceListTable (u4ContextId,
                                                            &u4NextContextId,
                                                            u4PgId, &u4NextPgId,
                                                            u4ServiceId,
                                                            &u4NextServiceId)
                   == SNMP_SUCCESS);
        }
    }

    /* Now set the new service list values */
    for (u2ByteIndex = 0; u2ByteIndex < ELPS_SRV_LIST_SIZE; u2ByteIndex++)
    {
        if (pu1SrvList[u2ByteIndex] != 0)
        {
            u1SrvFlag = pu1SrvList[u2ByteIndex];

            for (u2BitIndex = 0;
                 ((u2BitIndex < ELPS_PORTS_PER_BYTE) && (u1SrvFlag != 0));
                 u2BitIndex++)
            {
                if ((u1SrvFlag & ELPS_BIT8) != 0)
                {
                    u4ServiceId =
                        ((u2ByteIndex * ELPS_PORTS_PER_BYTE) + u2BitIndex + 1);

                    if (nmhGetFsElpsPgServiceListRowStatus
                        (u4ContextId, u4PgId,
                         u4ServiceId, &i4RowStatus) == SNMP_SUCCESS)
                    {
                        if (i4RowStatus == ACTIVE)
                        {
                            u1SrvFlag = (UINT1) (u1SrvFlag << 1);
                            continue;
                        }
                    }

                    if (nmhTestv2FsElpsPgServiceListRowStatus
                        (&u4ErrorCode, u4ContextId, u4PgId, u4ServiceId,
                         CREATE_AND_GO) == SNMP_FAILURE)
                    {
                        u1SrvFlag = (UINT1) (u1SrvFlag << 1);
                        continue;
                    }

                    if (nmhSetFsElpsPgServiceListRowStatus
                        (u4ContextId, u4PgId, u4ServiceId,
                         CREATE_AND_GO) == SNMP_FAILURE)
                    {
                        CLI_FATAL_ERROR (CliHandle);
                        return CLI_FAILURE;
                    }
                }
                u1SrvFlag = (UINT1) (u1SrvFlag << 1);
            }
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetWorkingServiceAsAll
 *
 * DESCRIPTION      : This function sets the protection group configuration 
 *                    type as 'all'.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetWorkingServiceAsAll (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4PgId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CfgType = 0;

    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4CfgType);

    if (i4CfgType != ELPS_PG_TYPE_ALL)
    {
        if (nmhTestv2FsElpsPgConfigType (&u4ErrorCode, u4ContextId, u4PgId,
                                         ELPS_PG_TYPE_ALL) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigType (u4ContextId, u4PgId,
                                      ELPS_PG_TYPE_ALL) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliTestForWorkingInfo
 *
 * DESCRIPTION      : This function check whether the working information 
 *                    is configured for the Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    u4ServiceListId - PG Service List Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
ElpsCliTestForWorkingInfo (UINT4 u4ContextId, UINT4 u4PgId,
                           UINT4 u4ServiceListId)
{
    tSNMP_OID_TYPE      Service;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4Service = 0;
    INT4                i4CfgType = 0;
    INT4                i4ServiceType = 0;
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];

    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4CfgType);
    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4ServiceType);

    switch (i4CfgType)
    {
        case ELPS_PG_TYPE_INDIVIDUAL:

            if (i4ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {
                nmhGetFsElpsPgConfigWorkingPort (u4ContextId, u4PgId,
                                                 &i4IfIndex);

                nmhGetFsElpsPgConfigWorkingServiceValue (u4ContextId, u4PgId,
                                                         &u4Service);
                if ((i4IfIndex == 0) || (u4Service == 0))
                {
                    return CLI_FAILURE;
                }
            }
            else                /* LSP/PW */
            {
                MEMSET (au4ServiceOid, 0, (ELPS_MAX_OID_LEN) * sizeof (UINT4));

                Service.u4_Length = 0;
                Service.pu4_OidList = au4ServiceOid;

                nmhGetFsElpsPgConfigWorkingServicePointer (u4ContextId, u4PgId,
                                                           &Service);

                if (Service.u4_Length == ELPS_ERR_OID_LEN)
                {
                    return CLI_FAILURE;
                }
            }
            break;

        case ELPS_PG_TYPE_LIST:

            if (i4ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {
                nmhGetFsElpsPgConfigWorkingPort (u4ContextId, u4PgId,
                                                 &i4IfIndex);

                if (nmhGetNextIndexFsElpsPgServiceListTable
                    (u4ContextId, &u4NextContextId, u4PgId, &u4NextPgId,
                     0, &u4Service) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if ((u4ContextId != u4NextContextId) || (u4PgId != u4NextPgId)
                    || (i4IfIndex == 0))
                {
                    return CLI_FAILURE;
                }
            }
            else                /* LSP/PW */
            {
                MEMSET (au4ServiceOid, 0, (ELPS_MAX_OID_LEN) * sizeof (UINT4));

                Service.u4_Length = 0;
                Service.pu4_OidList = au4ServiceOid;

                nmhGetFsElpsPgWorkingServiceListPointer (u4ContextId, u4PgId,
                                                         u4ServiceListId,
                                                         &Service);

                if (Service.u4_Length == ELPS_ERR_OID_LEN)
                {
                    return CLI_FAILURE;
                }
            }
            break;

        case ELPS_PG_TYPE_ALL:

            if (i4ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {
                nmhGetFsElpsPgConfigWorkingPort (u4ContextId, u4PgId,
                                                 &i4IfIndex);
                if (i4IfIndex == 0)
                {
                    return CLI_FAILURE;
                }
            }
            else                /* LSP/PW */
            {
                MEMSET (au4ServiceOid, 0, (ELPS_MAX_OID_LEN) * sizeof (UINT4));

                Service.u4_Length = 0;
                Service.pu4_OidList = au4ServiceOid;

                nmhGetFsElpsPgConfigWorkingServicePointer (u4ContextId, u4PgId,
                                                           &Service);

                if (Service.u4_Length == ELPS_ERR_OID_LEN)
                {
                    return CLI_FAILURE;
                }
            }
            break;

        default:
            return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliTestForConfigType
 *
 * DESCRIPTION      : This function test the configuration type before  
 *                    configuring the protection entity information.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliTestForConfigType (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4PgId, INT4 i4CfgType)
{
    INT4                i4OldCfgType = 0;

    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4OldCfgType);

    if (i4CfgType == i4OldCfgType)
    {
        return CLI_SUCCESS;
    }

    if (i4OldCfgType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        CliPrintf (CliHandle, "\r%% Configuration Failed. This Protection "
                   "Group is configured to protect individual service.\n");
    }
    else if (i4OldCfgType == ELPS_PG_TYPE_LIST)
    {
        CliPrintf (CliHandle, "\r%% Configuration Failed. This Protection "
                   "Group is configured to protect list of services.\n");
    }
    else if (i4OldCfgType == ELPS_PG_TYPE_ALL)
    {
        CliPrintf (CliHandle, "\r%% Configuration Failed. This Protection "
                   "Group is configured to protect all the services.\n");
    }

    return CLI_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetProtectionPort
 *
 * DESCRIPTION      : This function sets the protection port for the
 *                    Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    u4IfIndex - Working Interface Index.
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
ElpsCliSetProtectionPort (tCliHandle CliHandle, UINT4 u4ContextId,
                          UINT4 u4PgId, UINT4 u4IfIndex)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgConfigProtectionPort
        (&u4ErrorCode, u4ContextId, u4PgId, u4IfIndex) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigProtectionPort (u4ContextId, u4PgId, u4IfIndex)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetProtectionService
 *
 * DESCRIPTION      : This function sets the protection service for the 
 *                    Protection Group. 
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    u4ServiceId - Working Service Identifier
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
ElpsCliSetProtectionService (tCliHandle CliHandle, UINT4 u4ContextId,
                             UINT4 u4PgId, UINT4 u4ServiceId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgConfigProtectionServiceValue
        (&u4ErrorCode, u4ContextId, u4PgId, u4ServiceId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigProtectionServiceValue
        (u4ContextId, u4PgId, u4ServiceId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetProtectionServiceList
 *
 * DESCRIPTION      : This function sets the protection service list for the 
 *                    Protection Group. 
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    pu1SrvList - Service List
 *
 * OUTPUT           : None
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
INT4
ElpsCliSetProtectionServiceList (tCliHandle CliHandle, UINT4 u4ContextId,
                                 UINT4 u4PgId, UINT1 *pu1SrvList)
{
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4ServiceId = 0;
    UINT4               u4NextServiceId = 0;
    UINT1              *pu1SrvVlanList = NULL;

    pu1SrvVlanList = UtlShMemAllocVlanList ();
    if (pu1SrvVlanList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    MEMSET (pu1SrvVlanList, 0, ELPS_SRV_LIST_SIZE);
    /* In case - the PG config type is already list (i.e. some set of
     * service list is already in use), calling low level routine is not
     * sufficient, existing values needs to be deleted first. */
    if (nmhGetFirstIndexFsElpsPgServiceListTable (&u4NextContextId,
                                                  &u4NextPgId,
                                                  &u4NextServiceId)
        == SNMP_SUCCESS)
    {
        do
        {
            if ((u4ContextId != (UINT4) u4NextContextId) &&
                (u4PgId != u4NextPgId))
            {
                break;
            }

            OSIX_BITLIST_SET_BIT (pu1SrvVlanList, u4NextServiceId,
                                  ELPS_SRV_LIST_SIZE);
            u4ServiceId = u4NextServiceId;
        }
        while (nmhGetNextIndexFsElpsPgServiceListTable (u4ContextId,
                                                        &u4NextContextId,
                                                        u4PgId, &u4NextPgId,
                                                        u4ServiceId,
                                                        &u4NextServiceId)
               == SNMP_SUCCESS);
    }

    if (MEMCMP (pu1SrvVlanList, pu1SrvList, ELPS_SRV_LIST_SIZE) != 0)
    {
        UtlShMemFreeVlanList (pu1SrvVlanList);
        CliPrintf (CliHandle, "\r%% Service List configured is not "
                   "matching with the Service List of Working entity\n");
        return CLI_FAILURE;
    }

    UtlShMemFreeVlanList (pu1SrvVlanList);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetPgMonitor 
 *
 * DESCRIPTION      : This function sets the Monitor Mechanism for the 
 *                    Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    i4PgId - Protection Group Identifier
 *                    i4PgMonitor - Monitor Mechanism (CFM)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetPgMonitor (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                     INT4 i4PgMonitor)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgConfigMonitorMechanism (&u4ErrorCode, u4ContextId,
                                                 u4PgId,
                                                 i4PgMonitor) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Monitoring Mechanism configuration Failed.\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigMonitorMechanism (u4ContextId, u4PgId,
                                              i4PgMonitor) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetWorkingCfmCfg 
 *
 * DESCRIPTION      : This function sets the Working Cfm Properties for 
 *                    Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    u4WorkingMegId - Working MEG
 *                    u4WorkingMeId - Working ME
 *                    u4WorkingMepId - Working MEP
 *                    u4Direction- Direction(forward/reverse)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetWorkingCfmCfg (tCliHandle CliHandle,
                         UINT4 u4ContextId,
                         UINT4 u4PgId,
                         UINT4 u4WorkingMegId,
                         UINT4 u4WorkingMeId,
                         UINT4 u4WorkingMepId, UINT4 u4Direction)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4OldMegId = 0;
    UINT4               u4OldMeId = 0;
    UINT4               u4OldMepId = 0;
    INT4                i4PgCfmRowStatus = 0;
    INT4                i4MonitorType;

    nmhGetFsElpsPgConfigMonitorMechanism (u4ContextId, u4PgId, &i4MonitorType);

    if (i4MonitorType == ELPS_PG_MONITOR_MECH_CFM)
    {
        if ((u4WorkingMepId < ELPS_MIN_MEP_ID) ||
            (u4WorkingMepId > ELPS_MAX_MEP_ID))
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid MEP Id(%d) for CFM Monitoring.\n");
            return CLI_FAILURE;
        }
    }

    nmhGetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, &i4PgCfmRowStatus);

    if (i4PgCfmRowStatus == 0)
    {
        /* Entry not present for the Protection Group in CFM table */
        /* Do create and then set the values */

        if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrorCode, u4ContextId, u4PgId,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (i4PgCfmRowStatus == ACTIVE)
    {
        /* Entry to be made Not_In_Service before updating its fields */
        if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrorCode, u4ContextId, u4PgId,
                                           NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                        NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4Direction == CLI_ELPS_PG_DIR_FWD)
    {
        nmhGetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId, &u4OldMegId);
        nmhGetFsElpsPgCfmWorkingME (u4ContextId, u4PgId, &u4OldMeId);
        nmhGetFsElpsPgCfmWorkingMEP (u4ContextId, u4PgId, &u4OldMepId);

        if (nmhTestv2FsElpsPgCfmWorkingMEG (&u4ErrorCode,
                                            u4ContextId, u4PgId,
                                            u4WorkingMegId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgCfmWorkingME (&u4ErrorCode, u4ContextId, u4PgId,
                                           u4WorkingMeId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgCfmWorkingMEP (&u4ErrorCode, u4ContextId, u4PgId,
                                            u4WorkingMepId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId,
                                         u4WorkingMegId) == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId, u4OldMegId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmWorkingME (u4ContextId, u4PgId, u4WorkingMeId)
            == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId, u4OldMegId);
                nmhSetFsElpsPgCfmWorkingME (u4ContextId, u4PgId, u4OldMeId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmWorkingMEP (u4ContextId, u4PgId, u4WorkingMepId)
            == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId, u4OldMegId);
                nmhSetFsElpsPgCfmWorkingME (u4ContextId, u4PgId, u4OldMeId);
                nmhSetFsElpsPgCfmWorkingMEP (u4ContextId, u4PgId, u4OldMepId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (i4PgCfmRowStatus == ACTIVE)
        {
            /* Entry to be made Active after updating its fields */
            if (nmhTestv2FsElpsPgCfmRowStatus
                (&u4ErrorCode, u4ContextId, u4PgId, ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                            ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId, u4OldMegId);
                nmhSetFsElpsPgCfmWorkingME (u4ContextId, u4PgId, u4OldMeId);
                nmhSetFsElpsPgCfmWorkingMEP (u4ContextId, u4PgId, u4OldMepId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        nmhGetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4PgId, &u4OldMegId);
        nmhGetFsElpsPgCfmWorkingReverseME (u4ContextId, u4PgId, &u4OldMeId);
        nmhGetFsElpsPgCfmWorkingReverseMEP (u4ContextId, u4PgId, &u4OldMepId);

        if (nmhTestv2FsElpsPgCfmWorkingReverseMEG (&u4ErrorCode,
                                                   u4ContextId, u4PgId,
                                                   u4WorkingMegId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgCfmWorkingReverseME (&u4ErrorCode, u4ContextId,
                                                  u4PgId,
                                                  u4WorkingMeId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgCfmWorkingReverseMEP (&u4ErrorCode, u4ContextId,
                                                   u4PgId,
                                                   u4WorkingMepId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4PgId,
                                                u4WorkingMegId) == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4PgId,
                                                    u4OldMegId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmWorkingReverseME (u4ContextId, u4PgId,
                                               u4WorkingMeId) == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4PgId,
                                                    u4OldMegId);
                nmhSetFsElpsPgCfmWorkingReverseME (u4ContextId, u4PgId,
                                                   u4OldMeId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmWorkingReverseMEP (u4ContextId, u4PgId,
                                                u4WorkingMepId) == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4PgId,
                                                    u4OldMegId);
                nmhSetFsElpsPgCfmWorkingReverseME (u4ContextId, u4PgId,
                                                   u4OldMeId);
                nmhSetFsElpsPgCfmWorkingReverseMEP (u4ContextId, u4PgId,
                                                    u4OldMepId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (i4PgCfmRowStatus == ACTIVE)
        {
            /* Entry to be made Active after updating its fields */
            if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrorCode, u4ContextId,
                                               u4PgId, ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                            ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4PgId,
                                                    u4OldMegId);
                nmhSetFsElpsPgCfmWorkingReverseME (u4ContextId, u4PgId,
                                                   u4OldMeId);
                nmhSetFsElpsPgCfmWorkingReverseMEP (u4ContextId, u4PgId,
                                                    u4OldMepId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetProtectionCfmCfg  
 *
 * DESCRIPTION      : This function sets the Protection Cfm Properties for 
 *                    Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    u4ProtectionMegId - Protection MEG
 *                    u4ProtectionMeId - Protection ME
 *                    u4ProtectionMepId - Protection MEP
 *                    u4Direction- Direction (forward/reverse)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetProtectionCfmCfg (tCliHandle CliHandle,
                            UINT4 u4ContextId,
                            UINT4 u4PgId,
                            UINT4 u4ProtectionMegId,
                            UINT4 u4ProtectionMeId,
                            UINT4 u4ProtectionMepId, UINT4 u4Direction)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4OldMegId = 0;
    UINT4               u4OldMeId = 0;
    UINT4               u4OldMepId = 0;
    INT4                i4PgCfmRowStatus = 0;
    INT4                i4MonitorType = 0;

    nmhGetFsElpsPgConfigMonitorMechanism (u4ContextId, u4PgId, &i4MonitorType);

    if (i4MonitorType == ELPS_PG_MONITOR_MECH_CFM)
    {
        if ((u4ProtectionMepId < ELPS_MIN_MEP_ID) ||
            (u4ProtectionMepId > ELPS_MAX_MEP_ID))
        {
            CliPrintf (CliHandle,
                       "\r%% Invalid MEP Id(%d) for CFM Monitoring.\n");
            return CLI_FAILURE;
        }
    }

    nmhGetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, &i4PgCfmRowStatus);

    if (i4PgCfmRowStatus == 0)
    {
        /* Entry not present for the Protection Group in CFM table */
        /* Do create and then set the values */

        if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrorCode, u4ContextId, u4PgId,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                        CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else if (i4PgCfmRowStatus == ACTIVE)
    {
        /* Entry to be made Not_In_Service before updating its fields */
        if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrorCode, u4ContextId, u4PgId,
                                           NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                        NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4Direction == CLI_ELPS_PG_DIR_FWD)
    {
        nmhGetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId, &u4OldMegId);
        nmhGetFsElpsPgCfmProtectionME (u4ContextId, u4PgId, &u4OldMeId);
        nmhGetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId, &u4OldMepId);

        if (nmhTestv2FsElpsPgCfmProtectionMEG (&u4ErrorCode,
                                               u4ContextId, u4PgId,
                                               u4ProtectionMegId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgCfmProtectionME (&u4ErrorCode, u4ContextId,
                                              u4PgId,
                                              u4ProtectionMeId) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgCfmProtectionMEP (&u4ErrorCode, u4ContextId,
                                               u4PgId,
                                               u4ProtectionMepId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId,
                                            u4ProtectionMegId) == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId,
                                                u4OldMegId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmProtectionME (u4ContextId, u4PgId,
                                           u4ProtectionMeId) == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId,
                                                u4OldMegId);
                nmhSetFsElpsPgCfmProtectionME (u4ContextId, u4PgId, u4OldMeId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId,
                                            u4ProtectionMepId) == SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId,
                                                u4OldMegId);
                nmhSetFsElpsPgCfmProtectionME (u4ContextId, u4PgId, u4OldMeId);
                nmhSetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId,
                                                u4OldMepId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (i4PgCfmRowStatus == ACTIVE)
        {
            /* Entry to be made Active after updating its fields */
            if (nmhTestv2FsElpsPgCfmRowStatus
                (&u4ErrorCode, u4ContextId, u4PgId, ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                            ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId,
                                                u4OldMegId);
                nmhSetFsElpsPgCfmProtectionME (u4ContextId, u4PgId, u4OldMeId);
                nmhSetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId,
                                                u4OldMepId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        nmhGetFsElpsPgCfmProtectionReverseMEP (u4ContextId, u4PgId,
                                               &u4OldMegId);
        nmhGetFsElpsPgCfmProtectionReverseME (u4ContextId, u4PgId, &u4OldMeId);
        nmhGetFsElpsPgCfmProtectionReverseMEP (u4ContextId, u4PgId,
                                               &u4OldMepId);

        if (nmhTestv2FsElpsPgCfmProtectionReverseMEG (&u4ErrorCode,
                                                      u4ContextId, u4PgId,
                                                      u4ProtectionMegId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgCfmProtectionReverseME (&u4ErrorCode, u4ContextId,
                                                     u4PgId,
                                                     u4ProtectionMeId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgCfmProtectionReverseMEP (&u4ErrorCode, u4ContextId,
                                                      u4PgId,
                                                      u4ProtectionMepId)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmProtectionReverseMEG (u4ContextId, u4PgId,
                                                   u4ProtectionMegId) ==
            SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmProtectionReverseMEG (u4ContextId, u4PgId,
                                                       u4OldMegId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmProtectionReverseME (u4ContextId, u4PgId,
                                                  u4ProtectionMeId) ==
            SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmProtectionReverseMEG (u4ContextId, u4PgId,
                                                       u4OldMegId);
                nmhSetFsElpsPgCfmProtectionReverseME (u4ContextId, u4PgId,
                                                      u4OldMeId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgCfmProtectionReverseMEP (u4ContextId, u4PgId,
                                                   u4ProtectionMepId) ==
            SNMP_FAILURE)
        {
            if (i4PgCfmRowStatus == ACTIVE)
            {
                nmhSetFsElpsPgCfmProtectionReverseMEG (u4ContextId, u4PgId,
                                                       u4OldMegId);
                nmhSetFsElpsPgCfmProtectionReverseME (u4ContextId, u4PgId,
                                                      u4OldMeId);
                nmhSetFsElpsPgCfmProtectionReverseMEP (u4ContextId, u4PgId,
                                                       u4OldMepId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
            }
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (i4PgCfmRowStatus == ACTIVE)
        {
            /* Entry to be made Active after updating its fields */
            if (nmhTestv2FsElpsPgCfmRowStatus (&u4ErrorCode, u4ContextId,
                                               u4PgId, ACTIVE) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId,
                                            ACTIVE) == SNMP_FAILURE)
            {
                nmhSetFsElpsPgCfmProtectionReverseMEG (u4ContextId, u4PgId,
                                                       u4OldMegId);
                nmhSetFsElpsPgCfmProtectionReverseME (u4ContextId, u4PgId,
                                                      u4OldMeId);
                nmhSetFsElpsPgCfmProtectionReverseMEP (u4ContextId, u4PgId,
                                                       u4OldMepId);
                nmhSetFsElpsPgCfmRowStatus (u4ContextId, u4PgId, ACTIVE);
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliCfgLocalCommand 
 *
 * DESCRIPTION      : This function applies the external command on to the
 *                    Protection Group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    i4ExtCmd - External Command
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliCfgLocalCommand (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                        INT4 i4ExtCmd)
{
    UINT4               u4ErrorCode = 0;

    UNUSED_PARAM (CliHandle);
    if (nmhTestv2FsElpsPgCmdExtCmd (&u4ErrorCode, u4ContextId, u4PgId,
                                    i4ExtCmd) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgCmdExtCmd (u4ContextId, u4PgId, i4ExtCmd) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliCfgClearExtCmd
 *
 * DESCRIPTION      : This function clears the Force switch / Manual switch /
 *                    Lockout / Exercise
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *                    i4Command - Type of clear command. This can be 
 *                                     CLI_ELPS_PG_CLR_FS
 *                                     CLI_ELPS_PG_CLR_MS
 *                                     CLI_ELPS_PG_CLR_MS_W
 *                                     CLI_ELPS_PG_CLR_LOP
 *                                     CLI_ELPS_PG_CLR_EXER
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliCfgClearExtCmd (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                       INT4 i4Command)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Status = 0;

    if ((nmhGetFsElpsPgConfigRowStatus (u4ContextId, u4PgId,
                                        &i4Status) != SNMP_SUCCESS) ||
        (i4Status != ACTIVE))
    {
        CliPrintf (CliHandle, "\r%% Protection Group entry is not active\n");
        return CLI_FAILURE;
    }

    switch (i4Command)
    {
        case CLI_ELPS_PG_CLR_FS:

            if (ElpsSemCheckForActiveRequest (u4ContextId, u4PgId,
                                              ELPS_EXT_CMD_FS) == OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to clear, as the Force "
                           "Switch is not the last active command.\n");
                return CLI_FAILURE;
            }
            break;

        case CLI_ELPS_PG_CLR_MS:
            if (ElpsSemCheckForActiveRequest (u4ContextId, u4PgId,
                                              ELPS_EXT_CMD_MS) == OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to clear, as the Manual "
                           "Switch is not the last active command.\n");
                return CLI_FAILURE;
            }
            break;

        case CLI_ELPS_PG_CLR_MS_W:
            if (ElpsSemCheckForActiveRequest (u4ContextId, u4PgId,
                                              ELPS_EXT_CMD_MS_W) ==
                OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to clear, as the Manual "
                           "Switch is not the last active command.\n");
                return CLI_FAILURE;
            }
            break;

        case CLI_ELPS_PG_CLR_LOP:
            if (ElpsSemCheckForActiveRequest (u4ContextId, u4PgId,
                                              ELPS_EXT_CMD_LOP) == OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to clear, as the Lockout of"
                           " Protection is not the last active command.\n");
                return CLI_FAILURE;
            }
            break;

        case CLI_ELPS_PG_CLR_EXER:

            if (ElpsSemCheckForActiveRequest
                (u4ContextId, u4PgId, ELPS_EXT_CMD_EXER) == OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to clear, as the Exercise "
                           "is not the last active command.\n");
                return CLI_FAILURE;
            }
            break;

        default:
            CliPrintf (CliHandle, "\r%% Unknown clear command\n");
            return CLI_FAILURE;
    }

    if (nmhTestv2FsElpsPgCmdExtCmd (&u4ErrorCode, u4ContextId, u4PgId,
                                    ELPS_EXT_CMD_CLR) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgCmdExtCmd (u4ContextId, u4PgId, ELPS_EXT_CMD_CLR)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetGlobalDebug 
 *
 * DESCRIPTION      : This function sets the Global Debug option
 *
 * INPUT            : i4GlobalTraceOption - Enables/Disables the global
 *                                          trace option
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliSetGlobalDebug (tCliHandle CliHandle, INT4 i4GlobalTraceOption)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsGlobalTraceOption (&u4ErrorCode, i4GlobalTraceOption)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsGlobalTraceOption (i4GlobalTraceOption) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliGetContextInfoForShowCmd 
 *
 * DESCRIPTION      : This Routine validates the string input for the switch
 *                    name or loop through for all the context. It also checks
 *                    if ELPS is Started or not.
 *
 * INPUT            : u4CurrContextId - Context Identifier
 *                    pu1ContextName - Entered Context Name
 *                    u4Command      - Command Identifier
 *
 * OUTPUT           : pu4ContextId - Next Context Id or
 *                                   Context Id of the entered Context name
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliGetContextInfoForShowCmd (tCliHandle CliHandle, UINT1 *pu1ContextName,
                                 UINT4 u4CurrContextId, UINT4 *pu4ContextId,
                                 UINT4 u4Command)
{
    UINT4               u4ContextId = ELPS_INVALID_CONTEXT_ID;
    UINT1               au1ContextName[ELPS_SWITCH_ALIAS_LEN];

    if (u4Command == CLI_ELPS_SHOW_LIST_PROT)
    {
        return OSIX_SUCCESS;
    }

    /* If Switch-name is given then get the Context-Id from it */
    if (pu1ContextName != NULL)
    {
        if (ElpsPortVcmIsSwitchExist
            (pu1ContextName, &u4ContextId) != OSIX_TRUE)
        {
            CliPrintf (CliHandle, "\r%% Switch %s Does not exist.\r\n",
                       pu1ContextName);
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsElpsContextTable (u4CurrContextId,
                                               &u4ContextId) != SNMP_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    do
    {
        u4CurrContextId = u4ContextId;

        MEMSET (au1ContextName, 0, sizeof (au1ContextName));

        if (pu1ContextName == NULL)
        {
            ElpsPortVcmGetAliasName (u4ContextId, au1ContextName);
        }
        else
        {
            MEMCPY (au1ContextName, pu1ContextName, STRLEN (pu1ContextName));
        }

        if (u4Command != CLI_ELPS_CLEAR_STATS_ALL)
        {
            CliPrintf (CliHandle, "\r\n\r Switch %s \r\n", au1ContextName);
        }

        if (ElpsUtilIsElpsStarted (u4ContextId) != OSIX_TRUE)
        {
            if (u4Command != CLI_ELPS_CLEAR_STATS_ALL)
            {
                CliPrintf (CliHandle, "\r\n %%ELPS is ShutDown\r\n");
            }

            if (pu1ContextName != NULL)
            {
                /* Break the loop as the Module is shutdown in the 
                 * entered context 
                 */
                break;
            }

            continue;
        }

        *pu4ContextId = u4ContextId;
        return OSIX_SUCCESS;
    }
    while (nmhGetNextIndexFsElpsContextTable (u4CurrContextId,
                                              &u4ContextId) == SNMP_SUCCESS);

    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowGlobalInfo
 *
 * DESCRIPTION      : This function displays the context related information
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{
    INT4                i4ModuleStatus = 0;
    INT4                i4TrapEnabledStatus = OSIX_FALSE;
    UINT4               u4PSCChannelCode = 0;
    UINT4               u4RapidTxTime = 0;

    nmhGetFsElpsContextModuleStatus (u4ContextId, &i4ModuleStatus);
    nmhGetFsElpsContextEnableTrap (u4ContextId, &i4TrapEnabledStatus);
    nmhGetFsElpsPSCChannelCode (&u4PSCChannelCode);
    nmhGetFsElpsRapidTxTime (&u4RapidTxTime);

    CliPrintf (CliHandle, "\r\n ELPS Global Info \r\n");
    CliPrintf (CliHandle, "----------------------- \r\n");

    if (i4ModuleStatus == ELPS_ENABLED)
    {
        CliPrintf (CliHandle, "%-33s: Enabled\r\n", "Module Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s: Disabled\r\n", "Module Status");
    }

    if (i4TrapEnabledStatus == ELPS_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "%-33s: Enabled\r\n", "Trap Status");
    }
    else
    {
        CliPrintf (CliHandle, "%-33s: Disabled\r\n", "Trap Status");
    }

    CliPrintf (CliHandle, "%-33s: %u\r\n", "PSC Channel Code",
               u4PSCChannelCode);
    CliPrintf (CliHandle, "%-33s: %u\r\n", "Rapid Transmission Time",
               u4RapidTxTime);

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliConfigPgName
 *
 * DESCRIPTION      : This function sets the Name for the 
 *                    Protection Group
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection group identifier
 *                    pu1PgName - Protection Group Name
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliConfigPgName (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId,
                     UINT1 *pu1PgName)
{
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN];
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE PgName;

    if (pu1PgName == NULL)
    {
        MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));
        PgName.pu1_OctetList = au1PgNameStr;
        PgName.i4_Length = STRLEN (au1PgNameStr);
    }
    else
    {
        PgName.pu1_OctetList = pu1PgName;
        PgName.i4_Length = STRLEN (pu1PgName);
    }

    if (nmhTestv2FsElpsPgConfigName (&u4ErrorCode, u4ContextId, u4PgId,
                                     &PgName) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigName (u4ContextId, u4PgId, &PgName) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliClearPgStats
 *
 * DESCRIPTION      : This function clear the Stats of the 
 *                    Protection Group
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection group identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliClearPgStats (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId)
{
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4NextContextId = ELPS_INVALID_CONTEXT_ID;
    UINT4               u4NextPgId = 0;
    UINT4               u4ErrorCode = 0;
    UINT1               u1IsClearAll = OSIX_TRUE;

    UNUSED_PARAM (CliHandle);

    pContextInfo = ElpsCxtGetNode (u4ContextId);

    if (pContextInfo == NULL)
    {
        return CLI_FAILURE;
    }

    /* If Protection Group is given then clear for that PG alone */
    if (u4PgId != 0)
    {
        if (ElpsPgGetNode (pContextInfo, u4PgId) == NULL)
        {
            return CLI_FAILURE;
        }

        u1IsClearAll = OSIX_FALSE;
        u4NextPgId = u4PgId;
        u4NextContextId = u4ContextId;
    }
    else
    {

        if (nmhGetNextIndexFsElpsPgConfigTable (u4ContextId, &u4NextContextId,
                                                u4PgId, &u4NextPgId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    do
    {
        if (u4NextContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4PgId = u4NextPgId;

        if (nmhTestv2FsElpsPgStatsClearStatistics (&u4ErrorCode, u4ContextId,
                                                   u4NextPgId, ELPS_SNMP_TRUE)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgStatsClearStatistics (u4ContextId, u4NextPgId,
                                                ELPS_SNMP_TRUE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (u1IsClearAll == OSIX_FALSE)
        {
            break;
        }
    }
    while (nmhGetNextIndexFsElpsPgConfigTable (u4ContextId, &u4NextContextId,
                                               u4PgId, &u4NextPgId) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowPgInfo
 *
 * DESCRIPTION      : This function displays the configurations, status, 
 *                    statistics, timer info, Near End, Far End status of the 
 *                    Protection Group
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection group identifier
 *                    i4ShowCmdType - Various options of display info
 *
 * OUTPUT           : None
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliShowPgInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                   UINT4 u4PgId, INT4 i4ShowCmdType)
{
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4NextContextId = ELPS_INVALID_CONTEXT_ID;
    UINT4               u4NextPgId = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    INT4                i4ContinueShow = CLI_SUCCESS;
    UINT1               u1IsShowAll = OSIX_TRUE;

    pContextInfo = ElpsCxtGetNode (u4ContextId);

    if (pContextInfo == NULL)
    {
        return CLI_FAILURE;
    }

    /* If Protection Group is given then print for that PG */
    if (u4PgId != 0)
    {
        if (ElpsPgGetNode (pContextInfo, u4PgId) == NULL)
        {
            return CLI_FAILURE;
        }

        u1IsShowAll = OSIX_FALSE;
        u4NextPgId = u4PgId;
        u4NextContextId = u4ContextId;
    }
    else
    {

        if (nmhGetNextIndexFsElpsPgConfigTable (u4ContextId, &u4NextContextId,
                                                u4PgId, &u4NextPgId) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }

    do
    {
        if (u4NextContextId != u4ContextId)
        {
            return CLI_SUCCESS;
        }

        u4PgId = u4NextPgId;

        switch (i4ShowCmdType)
        {
            case ELPS_CLI_SHOW_PG_CONFIG:

                ElpsCliShowPgConfigurations (CliHandle,
                                             u4ContextId, u4NextPgId,
                                             &u4PagingStatus);
                break;

            case ELPS_CLI_SHOW_PG_STATUS:

                ElpsCliShowPgStatus (CliHandle, u4ContextId, u4NextPgId,
                                     &u4PagingStatus);
                break;

            case ELPS_CLI_SHOW_PG_STATS:

                ElpsCliShowPgStatistics (CliHandle, u4ContextId, u4NextPgId,
                                         &u4PagingStatus);
                break;

            case ELPS_CLI_SHOW_PG_TIMER:

                ElpsCliShowPgTimerInfo (CliHandle, u4ContextId, u4NextPgId,
                                        &u4PagingStatus);
                break;

            case ELPS_CLI_SHOW_PG_WE:

                ElpsCliShowPgWorkingEntity (CliHandle, u4ContextId, u4NextPgId,
                                            &u4PagingStatus);
                break;

            case ELPS_CLI_SHOW_PG_PE:

                ElpsCliShowPgProtectionEntity (CliHandle,
                                               u4ContextId, u4NextPgId,
                                               &u4PagingStatus);
                break;

            case ELPS_CLI_SHOW_PG_ALL:

                ElpsCliShowAllPgInfo (CliHandle, u4ContextId, u4NextPgId,
                                      &u4PagingStatus);
                break;

            default:
                /* Invalid show command type */
                return CLI_FAILURE;
        }

        i4ContinueShow = u4PagingStatus;

        if (u1IsShowAll == OSIX_FALSE)
        {
            break;
        }
    }
    while ((nmhGetNextIndexFsElpsPgConfigTable (u4ContextId, &u4NextContextId,
                                                u4PgId, &u4NextPgId) ==
            SNMP_SUCCESS) && (i4ContinueShow != CLI_FAILURE));

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowPgConfigurations 
 *
 * DESCRIPTION      : This function displays the configurations of a protection
 *                    group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *
 * OUTPUT           : pu4PagingStatus - Paging Status 
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
VOID
ElpsCliShowPgConfigurations (tCliHandle CliHandle,
                             UINT4 u4ContextId, UINT4 u4PgId,
                             UINT4 *pu4PagingStatus)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN + 1];
    tSNMP_OCTET_STRING_TYPE PgName;
    UINT4               u4IngressPortId = 0;
    INT4                i4PgProtType = 0;
    INT4                i4PgConfigType = 0;
    INT4                i4PgServiceType = 0;
    INT4                i4PgMonitorMech = 0;
    INT4                i4PgOperType = 0;
    UINT4               u4PgPscVer = 1;

    MEMSET (au1NameStr, 0, sizeof (au1NameStr));
    MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));
    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4PgConfigType);
    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4PgServiceType);
    nmhGetFsElpsPgConfigMonitorMechanism (u4ContextId, u4PgId,
                                          &i4PgMonitorMech);
    nmhGetFsElpsPgConfigOperType (u4ContextId, u4PgId, &i4PgOperType);
    nmhGetFsElpsPgConfigProtType (u4ContextId, u4PgId, &i4PgProtType);
    nmhGetFsElpsPgPscVersion (u4ContextId, u4PgId, &u4PgPscVer);

    nmhGetFsElpsPgConfigIngressPort (u4ContextId, u4PgId, (INT4 *)
                                     &u4IngressPortId);
    ElpsPortCfaCliGetIfName (u4IngressPortId, (INT1 *) au1NameStr);

    PgName.pu1_OctetList = au1PgNameStr;
    PgName.i4_Length = 0;

    nmhGetFsElpsPgConfigName (u4ContextId, u4PgId, &PgName);

    CliPrintf (CliHandle, "\r\nProtection Group Id   %u \r\n", u4PgId);

    if (PgName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Protection Group Name %s\r\n",
                   PgName.pu1_OctetList);
    }
    CliPrintf (CliHandle, "Protection Group Configurations\r\n");
    CliPrintf (CliHandle, "------------------------------------\r\n");

    if (i4PgProtType == ELPS_PG_PROT_TYPE_ONE_ISTO_ONE_BIDIR_APS)
    {
        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
        {
            CliPrintf (CliHandle, "Protection Type     : %s\r\n",
                       "1:1 Bi-Directional with APS Channel");
        }
        else if ((i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                 (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            CliPrintf (CliHandle, "Protection Type     : %s\r\n",
                       "1:1 Bi-Directional with PSC Channel");
        }
    }
    else if (i4PgProtType == ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_BIDIR_APS)
    {
        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
        {
            CliPrintf (CliHandle, "Protection Type     : %s\r\n",
                       "1+1 Bi-Directional with APS Channel");
        }
        else if ((i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                 (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            CliPrintf (CliHandle, "Protection Type     : %s\r\n",
                       "1+1 Bi-Directional with PSC Channel");
        }
    }
    else if (i4PgProtType == ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_APS)
    {
        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
        {
            CliPrintf (CliHandle, "Protection Type     : %s\r\n",
                       "1+1 Uni-Directional with APS Channel");
        }
        else if ((i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                 (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            CliPrintf (CliHandle, "Protection Type     : %s\r\n",
                       "1+1 Uni-Directional with PSC Channel");
        }
    }
    else if (i4PgProtType == ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_NO_APS)
    {
        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
        {
            CliPrintf (CliHandle, "Protection Type     : %s\r\n",
                       "1+1 Uni-Directional without APS Channel");
        }
        else if ((i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                 (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            CliPrintf (CliHandle, "Protection Type     : %s\r\n",
                       "1+1 Uni-Directional without PSC Channel");
        }
    }

    if (i4PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        CliPrintf (CliHandle, "Config Type         : %s\r\n", "Individual");
    }
    else if (i4PgConfigType == ELPS_PG_TYPE_LIST)
    {
        CliPrintf (CliHandle, "Config Type         : %s\r\n", "List");
    }
    else if (i4PgConfigType == ELPS_PG_TYPE_ALL)
    {
        CliPrintf (CliHandle, "Config Type         : %s\r\n", "All");
    }

    if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        CliPrintf (CliHandle, "Service Type        : %s\r\n", "Vlan");
    }
    else if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        CliPrintf (CliHandle, "Service Type        : %s\r\n", "LSP");
    }
    else if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        CliPrintf (CliHandle, "Service Type        : %s\r\n", "PW");
    }

    if (i4PgMonitorMech == ELPS_PG_MONITOR_MECH_CFM)
    {
        CliPrintf (CliHandle, "Monitor Mechanism   : %s\r\n", "CFM");
    }
    else if (i4PgMonitorMech == ELPS_PG_MONITOR_MECH_MPLSOAM)
    {
        CliPrintf (CliHandle, "Monitor Mechanism   : %s\r\n", "MPLS-OAM");
    }
    else if (i4PgMonitorMech == ELPS_PG_MONITOR_MECH_NONE)
    {
        CliPrintf (CliHandle, "Monitor Mechanism   : %s\r\n", "None");
    }

    if (i4PgMonitorMech == ELPS_PG_MONITOR_MECH_MPLSOAM)
    {
        CliPrintf (CliHandle, "PSC Version         : %d\r\n", u4PgPscVer);
    }

    if (i4PgOperType == ELPS_PG_OPER_TYPE_REVERTIVE)
    {
        CliPrintf (CliHandle, "Operation Type      : %s\r\n", "Revertive");
    }
    else if (i4PgOperType == ELPS_PG_OPER_TYPE_NONREVERTIVE)
    {
        CliPrintf (CliHandle, "Operation Type      : %s\r\n", "Non-Revertive");
    }

    CliPrintf (CliHandle, "Ingress Port Id     : %s\r\n", au1NameStr);
    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowPgStatus 
 *
 * DESCRIPTION      : This function displays the Protection Group Status
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *
 * OUTPUT           : pu4PagingStatus - Paging Status 
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
VOID
ElpsCliShowPgStatus (tCliHandle CliHandle,
                     UINT4 u4ContextId, UINT4 u4PgId, UINT4 *pu4PagingStatus)
{
    tSNMP_OCTET_STRING_TYPE PgName;
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN + 1];
    INT4                i4PgSemState = 0;
    INT4                i4PgLastExtCmd = 0;
    INT4                i4PgLastCmdStatus = 0;
    INT4                i4PgLastConditionStatus = 0;
    INT4                i4PgActiveRequest = 0;
    INT4                i4PgLastLocalCond = 0;
    INT4                i4PgLastRemReq = 0;
    INT4                i4PgLastRemReqStatus = 0;
    INT4                i4PgStatus = 0;

    MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));
    nmhGetFsElpsPgCmdSemState (u4ContextId, u4PgId, &i4PgSemState);
    nmhGetFsElpsPgCmdExtCmd (u4ContextId, u4PgId, &i4PgLastExtCmd);
    nmhGetFsElpsPgCmdLocalCondition (u4ContextId, u4PgId, &i4PgLastLocalCond);
    nmhGetFsElpsPgCmdFarEndRequest (u4ContextId, u4PgId, &i4PgLastRemReq);
    nmhGetFsElpsPgCmdExtCmdStatus (u4ContextId, u4PgId, &i4PgLastCmdStatus);
    nmhGetFsElpsPgCmdLocalConditionStatus (u4ContextId, u4PgId,
                                           &i4PgLastConditionStatus);
    nmhGetFsElpsPgCmdFarEndRequestStatus (u4ContextId, u4PgId,
                                          &i4PgLastRemReqStatus);
    nmhGetFsElpsPgCmdActiveRequest (u4ContextId, u4PgId, &i4PgActiveRequest);

    nmhGetFsElpsPgCmdPgStatus (u4ContextId, u4PgId, &i4PgStatus);

    PgName.pu1_OctetList = au1PgNameStr;
    PgName.i4_Length = 0;

    nmhGetFsElpsPgConfigName (u4ContextId, u4PgId, &PgName);

    CliPrintf (CliHandle, "\r\nProtection Group Id   %u \r\n", u4PgId);
    if (PgName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Protection Group Name %s\r\n",
                   PgName.pu1_OctetList);
    }
    CliPrintf (CliHandle, "Protection Group Status\r\n");
    CliPrintf (CliHandle, "---------------------------------\r\n");

    switch (i4PgSemState)
    {
        case ELPS_SEM_STATE_LO:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "LockOut of Protection");
            break;

        case ELPS_SEM_STATE_FS:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Forced Switch");
            break;

        case ELPS_SEM_STATE_MS:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Manual Switch to Protection");
            break;

        case ELPS_SEM_STATE_MS_W:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Manual Switch to Working");
            break;

        case ELPS_SEM_STATE_EXER_W:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Exercise - Working");
            break;

        case ELPS_SEM_STATE_EXER_P:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Exercise - Protection");
            break;

        case ELPS_SEM_STATE_NR_W:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "No Request - Working");
            break;

        case ELPS_SEM_STATE_NR_P:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "No Request - Protection");
            break;

        case ELPS_SEM_STATE_RR_W:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Reverse Request - Working");
            break;

        case ELPS_SEM_STATE_RR_P:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Reverse Request - Protection");
            break;

        case ELPS_SEM_STATE_WTR:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Wait to Restore");
            break;

        case ELPS_SEM_STATE_SF_W:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Signal Fail - Working");
            break;

        case ELPS_SEM_STATE_SF_P:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Signal Fail - Protection");
            break;

        case ELPS_SEM_STATE_DNR:

            CliPrintf (CliHandle, "Current state         : %s\r\n",
                       "Do Not Revert");
            break;

        default:

            CliPrintf (CliHandle, "Current state         : %s\r\n", "None");
            break;

    }

    /* Show the Last Local Command */
    switch (i4PgLastExtCmd)
    {
        case ELPS_EXT_CMD_LOP:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n", "Lockout");
            break;
        case ELPS_EXT_CMD_FS:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n",
                       "Force-Switch");
            break;
        case ELPS_EXT_CMD_MS:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n",
                       "Manual-Switch-to-Protection");
            break;
        case ELPS_EXT_CMD_MS_W:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n",
                       "Manual-Switch-to-Working");
            break;
        case ELPS_EXT_CMD_EXER:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n", "Exercise");
            break;
        case ELPS_EXT_CMD_CLR:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n", "Clear");
            break;
        case ELPS_EXT_CMD_FREEZE:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n", "Freeze");
            break;
        case ELPS_EXT_CMD_CLR_FREEZE:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n",
                       "Clear-Freeze");
            break;
        default:
            CliPrintf (CliHandle, "Last Local Cmd        : %s\r\n", "None");
            break;
    }

    switch (i4PgLastCmdStatus)
    {
        case ELPS_CMD_ACCEPTED:

            CliPrintf (CliHandle, "Last Cmd Status       : %s\r\n", "Accepted");
            break;

        case ELPS_CMD_OVERRULED:

            CliPrintf (CliHandle, "Last Cmd Status       : %s\r\n",
                       "Overruled");
            break;

        case ELPS_CMD_NA:

            CliPrintf (CliHandle, "Last Cmd Status       : %s\r\n",
                       "Not Applicable");
            break;

        case ELPS_CMD_REJECTED:

            CliPrintf (CliHandle, "Last Cmd Status       : %s\r\n", "Rejected");
            break;

        default:

            CliPrintf (CliHandle, "Last Cmd Status       : %s\r\n", "None");
            break;

    }

    /* Show the Last Local Condition */
    switch (i4PgLastLocalCond)
    {
        case ELPS_LOCAL_COND_SF_W:
            CliPrintf (CliHandle, "Last Local Condition  : %s\r\n",
                       "SF On Working");
            break;

        case ELPS_LOCAL_COND_WRK_REC_SF:
            CliPrintf (CliHandle, "Last Local Condition  : %s\r\n",
                       "Working Recovers from SF");
            break;

        case ELPS_LOCAL_COND_SF_P:
            CliPrintf (CliHandle, "Last Local Condition  : %s\r\n",
                       "SF On Protection");
            break;

        case ELPS_LOCAL_COND_PROT_REC_SF:
            CliPrintf (CliHandle, "Last Local Condition  : %s\r\n",
                       "Protection Recovers from SF");
            break;

        case ELPS_LOCAL_COND_WTR_EXP:
            CliPrintf (CliHandle, "Last Local Condition  : %s\r\n",
                       "WTR Timer Expires");
            break;

        default:
            CliPrintf (CliHandle, "Last Local Condition  : %s\r\n", "None");
            break;
    }

    switch (i4PgLastConditionStatus)
    {
        case ELPS_CMD_ACCEPTED:

            CliPrintf (CliHandle, "Last Cond Status      : %s\r\n", "Accepted");
            break;

        case ELPS_CMD_OVERRULED:

            CliPrintf (CliHandle, "Last Cond Status      : %s\r\n",
                       "Overruled");
            break;

        case ELPS_CMD_NA:

            CliPrintf (CliHandle, "Last Cond Status      : %s\r\n",
                       "Not Applicable");
            break;

        case ELPS_CMD_REJECTED:

            CliPrintf (CliHandle, "Last Cond Status      : %s\r\n", "Rejected");
            break;

        default:

            CliPrintf (CliHandle, "Last Cond Status      : %s\r\n", "None");
            break;
    }

    /* Show the Last Far-end request */
    switch (i4PgLastRemReq)
    {
        case ELPS_FAR_REQ_LO:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n", "Lockout");
            break;
        case ELPS_FAR_REQ_SF_P:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "SF On Protection");
            break;
        case ELPS_FAR_REQ_FS:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "Force-Switch");
            break;
        case ELPS_FAR_REQ_SF_W:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "SF on Working");
            break;
        case ELPS_FAR_REQ_MS:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "Manual-Switch-to-Protection");
            break;
        case ELPS_FAR_REQ_MS_W:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "Manual-Switch-to-Working");
            break;
        case ELPS_FAR_REQ_WTR:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n", "WTR");
            break;
        case ELPS_FAR_REQ_EXER_W:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "Exercise on Working");
            break;
        case ELPS_FAR_REQ_EXER_P:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "Exercise on Protection");
            break;
        case ELPS_FAR_REQ_RR_W:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "RR on Working");
            break;
        case ELPS_FAR_REQ_RR_P:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "RR on Protection");
            break;
        case ELPS_FAR_REQ_NR_W:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "NR on Working");
            break;
        case ELPS_FAR_REQ_NR_P:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "NR on Protection");
            break;
        case ELPS_FAR_REQ_DNR:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n",
                       "Do Not Revert");
            break;
        default:
            CliPrintf (CliHandle, "Last Far End Req      : %s\r\n", "None");
            break;
    }

    switch (i4PgLastRemReqStatus)
    {
        case ELPS_CMD_ACCEPTED:

            CliPrintf (CliHandle, "Last Far End Status   : %s\r\n", "Accepted");
            break;

        case ELPS_CMD_OVERRULED:

            CliPrintf (CliHandle, "Last Far End Status   : %s\r\n",
                       "Overruled");
            break;

        case ELPS_CMD_NA:

            CliPrintf (CliHandle, "Last Far End Status   : %s\r\n",
                       "Not Applicable");
            break;

        case ELPS_CMD_REJECTED:

            CliPrintf (CliHandle, "Last Far End Status   : %s\r\n", "Rejected");
            break;

        default:

            CliPrintf (CliHandle, "Last Far End Status   : %s\r\n", "None");
            break;
    }

    /* Show the Active Request */
    CliPrintf (CliHandle, "Active Request        : %s\r\n",
               gaau1EventString[i4PgActiveRequest]);

    /* Show the Pg Status */
    switch (i4PgStatus)
    {
        case ELPS_PG_STATUS_DISABLE:

            CliPrintf (CliHandle, "Status                : %s\r\n",
                       "Protection Disabled");
            break;

        case ELPS_PG_STATUS_WORK_PATH_ACTIVE:

            CliPrintf (CliHandle, "Status                : %s\r\n", "Normal");
            break;

        case ELPS_PG_STATUS_PROT_PATH_ACTIVE:
            if ((i4PgActiveRequest == ELPS_EV_FR_SF) ||
                (i4PgSemState == ELPS_SEM_STATE_SF_W)||
                (i4PgActiveRequest == ELPS_EV_FR_WTR))

            {
                CliPrintf (CliHandle, "Status                : %s\r\n",
                           "Protecting Failure");
            }
            else if ((i4PgSemState == ELPS_SEM_STATE_FS) ||
                     (i4PgActiveRequest == ELPS_EV_FR_FS) ||
                     (i4PgSemState == ELPS_SEM_STATE_MS) ||
                     (i4PgActiveRequest == ELPS_EV_FR_MS))
            {
                CliPrintf (CliHandle, "Status                : %s\r\n",
                           "Protecting Administrative");
            }
            break;

        case ELPS_PG_STATUS_WTR_STATE:

            CliPrintf (CliHandle, "Status                : %s\r\n",
                       "Wait To Restore");
            break;

        case ELPS_PG_STATUS_HOLD_OFF_STATE:

            CliPrintf (CliHandle, "Status                : %s\r\n", "Hold Off");
            break;

        case ELPS_PG_STATUS_SWITCHING_FAILED:

            CliPrintf (CliHandle, "Status                : %s\r\n",
                       "Switching Failed");
            break;

        case ELPS_PG_STATUS_UNAVAILABLE:

            CliPrintf (CliHandle, "Status                : %s\r\n",
                       "Unavailable");
            break;

        case ELPS_PG_STATUS_DO_NOT_REVERT:

            CliPrintf (CliHandle, "Status                : %s\r\n",
                       "Do Not Revert");
            break;
        default:

            CliPrintf (CliHandle, "Status                : %s\r\n", "None");
            break;
    }

    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowPgStatistics 
 *
 * DESCRIPTION      : This functions displays the statistics of a protection
 *                    group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *
 * OUTPUT           : pu4PagingStatus - Paging Status 
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
VOID
ElpsCliShowPgStatistics (tCliHandle CliHandle,
                         UINT4 u4ContextId, UINT4 u4PgId,
                         UINT4 *pu4PagingStatus)
{
    tSNMP_OCTET_STRING_TYPE PgName;
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN + 1];
    UINT4               u4AutoProtSwitchCount = 0;
    UINT4               u4ForcedSwitchCount = 0;
    UINT4               u4ManualSwitchCount = 0;
    UINT4               u4TotalApsTxCount = 0;
    UINT4               u4TotalApsRxCount = 0;
    UINT4               u4TotalApsDiscardCount = 0;
    INT4                i4PgServiceType = 0;

    MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));
    nmhGetFsElpsPgStatsAutoProtectionSwitchCount (u4ContextId, u4PgId,
                                                  &u4AutoProtSwitchCount);
    nmhGetFsElpsPgStatsForcedSwitchCount (u4ContextId, u4PgId,
                                          &u4ForcedSwitchCount);
    nmhGetFsElpsPgStatsManualSwitchCount (u4ContextId, u4PgId,
                                          &u4ManualSwitchCount);

    nmhGetFsElpsPgStatsApsPktTxCount (u4ContextId, u4PgId, &u4TotalApsTxCount);
    nmhGetFsElpsPgStatsApsPktRxCount (u4ContextId, u4PgId, &u4TotalApsRxCount);

    nmhGetFsElpsPgStatsApsPktDiscardCount (u4ContextId, u4PgId,
                                           &u4TotalApsDiscardCount);

    PgName.pu1_OctetList = au1PgNameStr;
    PgName.i4_Length = 0;

    nmhGetFsElpsPgConfigName (u4ContextId, u4PgId, &PgName);

    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4PgServiceType);

    CliPrintf (CliHandle, "\r\nProtection Group Id   %u\r\n", u4PgId);
    if (PgName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Protection Group Name %s\r\n",
                   PgName.pu1_OctetList);
    }
    CliPrintf (CliHandle, "Protection Group Statistics\r\n");
    CliPrintf (CliHandle, "------------------------------------\r\n");

    CliPrintf (CliHandle, "Auto-Protection Switches Count : %d\r\n",
               u4AutoProtSwitchCount);
    CliPrintf (CliHandle, "Forced-Switches Count          : %d\r\n",
               u4ForcedSwitchCount);
    CliPrintf (CliHandle, "Manual Switches Count          : %d\r\n",
               u4ManualSwitchCount);

    if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        CliPrintf (CliHandle, "Total APS Tx Count             : %d\r\n",
                   u4TotalApsTxCount);
        CliPrintf (CliHandle, "Total APS Rx Count             : %d\r\n",
                   u4TotalApsRxCount);
        CliPrintf (CliHandle, "Total APS Discarded Count             : %d\r\n",
                   u4TotalApsDiscardCount);
    }
    else if ((i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
             (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        CliPrintf (CliHandle, "Total PSC Tx Count             : %d\r\n",
                   u4TotalApsTxCount);
        CliPrintf (CliHandle, "Total PSC Rx Count             : %d\r\n",
                   u4TotalApsRxCount);
        CliPrintf (CliHandle, "Total PSC Discarded Count      : %d\r\n",
                   u4TotalApsDiscardCount);
    }

    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowPgTimerInfo 
 *
 * DESCRIPTION      : This function displays the Timer information of the 
 *                    Protection group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *
 * OUTPUT           : pu4PagingStatus - Paging Status 
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
VOID
ElpsCliShowPgTimerInfo (tCliHandle CliHandle,
                        UINT4 u4ContextId, UINT4 u4PgId, UINT4 *pu4PagingStatus)
{
    tSNMP_OCTET_STRING_TYPE PgName;
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN + 1];
    UINT4               u4HoldOffTime = 0;
    UINT4               u4WTRTime = 0;
    UINT4               u4PeriodicTime = 0;
    UINT4               u4RapidTxTime = 0;
    INT4                i4PgServiceType = 0;

    MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));
    nmhGetFsElpsPgCmdHoTime (u4ContextId, u4PgId, &u4HoldOffTime);
    nmhGetFsElpsPgCmdWTR (u4ContextId, u4PgId, &u4WTRTime);
    nmhGetFsElpsPgCmdApsPeriodicTime (u4ContextId, u4PgId, &u4PeriodicTime);
    nmhGetFsElpsRapidTxTime (&u4RapidTxTime);

    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4PgServiceType);

    PgName.pu1_OctetList = au1PgNameStr;
    PgName.i4_Length = 0;

    nmhGetFsElpsPgConfigName (u4ContextId, u4PgId, &PgName);

    CliPrintf (CliHandle, "\r\nProtection Group Id   %u\r\n", u4PgId);
    if (PgName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Protection Group Name %s\r\n",
                   PgName.pu1_OctetList);
    }
    CliPrintf (CliHandle, "Protection Group Timer Info\r\n");
    CliPrintf (CliHandle, "--------------------------------------\r\n");

    CliPrintf (CliHandle, "Hold-Off Timer Interval : %d  milliseconds\r\n",
               (u4HoldOffTime * ELPS_PG_HO_TIME_GRANULARITY));
    CliPrintf (CliHandle, "WTR Timer Interval      : %d  minutes\r\n",
               u4WTRTime);

    if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        CliPrintf (CliHandle, "APS Tx Interval         : %d  seconds\r\n",
                   u4PeriodicTime);
    }
    else if ((i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
             (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        CliPrintf (CliHandle, "PSC Tx Interval         : %d  seconds\r\n",
                   u4PeriodicTime);
    }

    CliPrintf (CliHandle, "APS Rapid Tx Interval   : %d  milliseconds\r\n",
               u4RapidTxTime);

    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowPgWorkingEntity 
 *
 * DESCRIPTION      : This function sets the Working entity of the Protection 
 *                    group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *
 * OUTPUT           : pu4PagingStatus - Paging Status 
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
VOID
ElpsCliShowPgWorkingEntity (tCliHandle CliHandle,
                            UINT4 u4ContextId, UINT4 u4PgId,
                            UINT4 *pu4PagingStatus)
{
    tSNMP_OCTET_STRING_TYPE PgName;
    tSNMP_OID_TYPE      WorkingService;
    tSNMP_OID_TYPE      RvrWorkingService;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN + 1];
    UINT4               u4WorkingPortId = 0;
    UINT4               u4WorkingServiceValue = 0;
    UINT4               u4MegId = 0;
    UINT4               u4MeId = 0;
    UINT4               u4MepId = 0;
    UINT4               u4RevMegId = 0;
    UINT4               u4RevMeId = 0;
    UINT4               u4RevMepId = 0;
    INT4                i4PgServiceType = 0;
    INT4                i4ConfigType = 0;
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4RvrServiceOid[ELPS_MAX_OID_LEN];
    UINT4               u4WorkingInstanceId = 0;
    CHR1               *pu1TempStr = NULL;

    MEMSET (au4ServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));
    MEMSET (au4RvrServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));
    MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));

    WorkingService.u4_Length = 0;
    WorkingService.pu4_OidList = au4ServiceOid;

    RvrWorkingService.u4_Length = 0;
    RvrWorkingService.pu4_OidList = au4RvrServiceOid;

    nmhGetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId, &u4MegId);
    nmhGetFsElpsPgCfmWorkingME (u4ContextId, u4PgId, &u4MeId);
    nmhGetFsElpsPgCfmWorkingMEP (u4ContextId, u4PgId, &u4MepId);

    nmhGetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4PgId, &u4RevMegId);
    nmhGetFsElpsPgCfmWorkingReverseME (u4ContextId, u4PgId, &u4RevMeId);
    nmhGetFsElpsPgCfmWorkingReverseMEP (u4ContextId, u4PgId, &u4RevMepId);
    PgName.pu1_OctetList = au1PgNameStr;
    PgName.i4_Length = 0;

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        CliPrintf (CliHandle, "ElpsCliShowPgWorkingEntity: "
                   "Failed while allocating memory\r\n");
        return;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        CliPrintf (CliHandle, "ElpsCliShowPgWorkingEntity: "
                   "Failed while allocating memory\r\n");
        return;
    }

    nmhGetFsElpsPgConfigName (u4ContextId, u4PgId, &PgName);

    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4PgServiceType);

    CliPrintf (CliHandle, "\r\nProtection Group Id   %u\r\n", u4PgId);
    if (PgName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Protection Group Name %s\r\n",
                   PgName.pu1_OctetList);
    }
    CliPrintf (CliHandle, "Protection Group Working Entity\r\n");
    CliPrintf (CliHandle, "-----------------------------------------\r\n");

    if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));
        nmhGetFsElpsPgConfigWorkingPort (u4ContextId, u4PgId,
                                         (INT4 *) &u4WorkingPortId);
        nmhGetFsElpsPgConfigWorkingServiceValue (u4ContextId, u4PgId,
                                                 &u4WorkingServiceValue);

        MEMSET (au1NameStr, 0, sizeof (au1NameStr));
        ElpsPortCfaCliGetIfName (u4WorkingPortId, (INT1 *) au1NameStr);

        CliPrintf (CliHandle, "Working Service Type  : %s\r\n", "vlan");
        CliPrintf (CliHandle, "Working Port Id       : %s\r\n", au1NameStr);

        nmhGetFsElpsPgConfigWorkingInstanceId (u4ContextId,
                                               u4PgId, &u4WorkingInstanceId);
        CliPrintf (CliHandle, "Working Instance Id   : %d\r\n",
                   u4WorkingInstanceId);

        if (u4WorkingServiceValue != 0)
        {
            CliPrintf (CliHandle, "Working Service Id    : %d\r\n",
                       u4WorkingServiceValue);
        }
        else
        {
            CliPrintf (CliHandle, "Working Service List  :\r\n");
            ElpsCliPrintServiceList (CliHandle, u4ContextId, u4PgId);
        }
        CliPrintf (CliHandle, "Working MEG           : %d\r\n", u4MegId);
        CliPrintf (CliHandle, "Working ME            : %d\r\n", u4MeId);
        CliPrintf (CliHandle, "Working MEP           : %d\r\n", u4MepId);
    }
    else
    {
        nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4ConfigType);

        if (i4ConfigType == ELPS_PG_TYPE_INDIVIDUAL)
        {
            nmhGetFsElpsPgConfigWorkingServicePointer (u4ContextId, u4PgId,
                                                       &WorkingService);
            if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
            {
                CliPrintf (CliHandle, "Working Service Type  : %s\r\n", "LSP");

                if (WorkingService.u4_Length != ELPS_ERR_OID_LEN)
                {
                    CliPrintf (CliHandle, "Working Service Value : \r\n");

                    CliPrintf (CliHandle,
                               "  Tunnel Index                     : %d\r\n",
                               WorkingService.
                               pu4_OidList[ELPS_TNL_INDEX_START_OFFSET]);
                    CliPrintf (CliHandle,
                               "  Tunnel Instance                  : %d\r\n",
                               WorkingService.
                               pu4_OidList[ELPS_TNL_INST_START_OFFSET]);

                    if (ElpsTnlGetTunnelAddrType (WorkingService,
                                                  pInMplsApiInfo,
                                                  pOutMplsApiInfo)
                        == OSIX_SUCCESS)
                    {
                        if (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                            SrcNodeId.u4NodeType == ELPS_ADDR_TYPE_UCAST)
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                       pOutMplsApiInfo->
                                                       OutTeTnlInfo.TnlLspId.
                                                       SrcNodeId.MplsRouterId.
                                                       u4_addr[0]);
                            CliPrintf (CliHandle,
                                       "  Ingress LSR Id                   : "
                                       "%s\r\n", pu1TempStr);

                            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                       pOutMplsApiInfo->
                                                       OutTeTnlInfo.TnlLspId.
                                                       DstNodeId.MplsRouterId.
                                                       u4_addr[0]);
                            CliPrintf (CliHandle,
                                       "  Egress LSR Id                    : "
                                       "%s\r\n", pu1TempStr);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "  Ingress LSR Id (GId:Nid)         : "
                                       "%d:%d\r\n",
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       SrcNodeId.MplsGlobalNodeId.u4GlobalId,
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       SrcNodeId.MplsGlobalNodeId.u4NodeId);

                            CliPrintf (CliHandle,
                                       "  Egress LSR Id (GId:Nid)          : "
                                       "%d:%d\r\n",
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       DstNodeId.MplsGlobalNodeId.u4NodeId);
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "  Ingress LSR Id                   : "
                                   "%d\r\n",
                                   WorkingService.pu4_OidList
                                   [ELPS_TNL_ING_LSRID_START_OFFSET]);
                        CliPrintf (CliHandle,
                                   "  Egress LSR Id                    : %d\r\n",
                                   WorkingService.pu4_OidList
                                   [ELPS_TNL_EGG_LSRID_START_OFFSET]);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "Working Service Value : 0\r\n");
                }

                CliPrintf (CliHandle,
                           "  Working MEG                      : %d\r\n",
                           u4MegId);
                CliPrintf (CliHandle,
                           "  Working ME                       : %d\r\n",
                           u4MeId);
                CliPrintf (CliHandle,
                           "  Working MEP                      : %d\r\n",
                           u4MepId);

                nmhGetFsElpsPgConfigWorkingReverseServicePointer
                    (u4ContextId, u4PgId, &RvrWorkingService);

                if (RvrWorkingService.u4_Length != ELPS_ERR_OID_LEN)
                {

                    CliPrintf (CliHandle,
                               "Reverse Working Service Value : \r\n");

                    CliPrintf (CliHandle,
                               "  Reverse Tunnel Index             : %d\r\n",
                               RvrWorkingService.
                               pu4_OidList[ELPS_TNL_INDEX_START_OFFSET]);
                    CliPrintf (CliHandle,
                               "  Reverse Tunnel Instance          : %d\r\n",
                               RvrWorkingService.
                               pu4_OidList[ELPS_TNL_INST_START_OFFSET]);

                    if (ElpsTnlGetTunnelAddrType (RvrWorkingService,
                                                  pInMplsApiInfo,
                                                  pOutMplsApiInfo)
                        == OSIX_SUCCESS)
                    {
                        if (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                            SrcNodeId.u4NodeType == ELPS_ADDR_TYPE_UCAST)
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                       pOutMplsApiInfo->
                                                       OutTeTnlInfo.TnlLspId.
                                                       SrcNodeId.MplsRouterId.
                                                       u4_addr[0]);
                            CliPrintf (CliHandle,
                                       " Reverse Ingress LSR Id            : "
                                       "%s\r\n", pu1TempStr);

                            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                       pOutMplsApiInfo->
                                                       OutTeTnlInfo.TnlLspId.
                                                       DstNodeId.MplsRouterId.
                                                       u4_addr[0]);
                            CliPrintf (CliHandle,
                                       " Reverse Egress LSR Id             : "
                                       "%s\r\n", pu1TempStr);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       " Reverse Ingress LSR Id (GId:Nid)  : "
                                       "%d:%d\r\n",
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       SrcNodeId.MplsGlobalNodeId.u4GlobalId,
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       SrcNodeId.MplsGlobalNodeId.u4NodeId);

                            CliPrintf (CliHandle,
                                       " Reverse Egress LSR Id (GId:Nid)   : "
                                       "%d:%d\r\n",
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       DstNodeId.MplsGlobalNodeId.u4NodeId);
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   " Reverse Ingress LSR Id            : %d\r\n",
                                   RvrWorkingService.pu4_OidList
                                   [ELPS_TNL_ING_LSRID_START_OFFSET]);
                        CliPrintf (CliHandle,
                                   " Reverse Egress LSR Id             : %d\r\n",
                                   RvrWorkingService.pu4_OidList
                                   [ELPS_TNL_EGG_LSRID_START_OFFSET]);
                    }
                }
                else
                {
                    CliPrintf (CliHandle,
                               "Reverse Working Service Value : 0\r\n");
                }

                CliPrintf (CliHandle,
                           "  Reverse Working MEG              : %d\r\n",
                           u4RevMegId);
                CliPrintf (CliHandle,
                           "  Reverse Working ME               : %d\r\n",
                           u4RevMeId);
                CliPrintf (CliHandle,
                           "  Reverse Working MEP              : %d\r\n",
                           u4RevMepId);
            }
            else if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW)
            {
                MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
                MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

                pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
                pInMplsApiInfo->u4ContextId = 0;
                pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
                pInMplsApiInfo->InPathId.PwId.u4PwIndex =
                    WorkingService.pu4_OidList[ELPS_PW_INDEX_OFFSET];

                CliPrintf (CliHandle, "Working Service Type  : %s\r\n",
                           "Pseudowire");

                if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                                     pInMplsApiInfo,
                                                     pOutMplsApiInfo)
                    == OSIX_FAILURE)
                {
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pInMplsApiInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pOutMplsApiInfo);

                    CliPrintf (CliHandle, "Working Service Value : \r\n");

                    CliPrintf (CliHandle, "  Vc Id            : 0\r\n");
                    CliPrintf (CliHandle, "  Peer Address     : 0\r\n");

                    return;
                }

                CliPrintf (CliHandle, "Working Service Value : \r\n");

                CliPrintf (CliHandle, "  Vc Id            : %d\r\n",
                           pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId);

                if (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
                    u4NodeType == ELPS_ADDR_TYPE_UCAST)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                               pOutMplsApiInfo->OutPwInfo.
                                               MplsPwPathId.DstNodeId.
                                               MplsRouterId.u4_addr[0]);

                    CliPrintf (CliHandle, "  Peer Address     : %s\r\n",
                               pu1TempStr);
                }
                else
                {
                    CliPrintf (CliHandle, "  Peer Address(G:N): %d:%d\r\n",
                               pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                               DstNodeId.MplsGlobalNodeId.u4GlobalId,
                               pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                               DstNodeId.MplsGlobalNodeId.u4NodeId);
                }

                CliPrintf (CliHandle, "  Working MEG             : %d\r\n",
                           u4MegId);
                CliPrintf (CliHandle, "  Working ME              : %d\r\n",
                           u4MeId);
                CliPrintf (CliHandle, "  Working MEP             : %d\r\n",
                           u4MepId);
            }
        }
        else if (i4ConfigType == ELPS_PG_TYPE_LIST)
        {
            CliPrintf (CliHandle, "Working Service List  : \r\n");
            ElpsCliPrintWorkingServiceList (CliHandle, u4ContextId, u4PgId);
        }
    }

    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowPgProtectionEntity 
 *
 * DESCRIPTION      : This function sets the Protection entity 
 *                    configurations of the Protection group. 
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *
 * OUTPUT           : pu4PagingStatus - Paging Status 
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
VOID
ElpsCliShowPgProtectionEntity (tCliHandle CliHandle,
                               UINT4 u4ContextId, UINT4 u4PgId,
                               UINT4 *pu4PagingStatus)
{
    tSNMP_OCTET_STRING_TYPE PgName;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN + 1];
    UINT4               u4ProtectionPortId = 0;
    UINT4               u4ProtectionServiceValue = 0;
    UINT4               u4ProtectionMeg = 0;
    UINT4               u4ProtectionMe = 0;
    UINT4               u4ProtectionMep = 0;
    UINT4               u4ProtectionRevMeg = 0;
    UINT4               u4ProtectionRevMe = 0;
    UINT4               u4ProtectionRevMep = 0;
    INT4                i4PgServiceType = 0;
    UINT4               u4ProtectionInstanceId = 0;
    tSNMP_OID_TYPE      ProtectionService;
    tSNMP_OID_TYPE      RvrProtectionService;
    INT4                i4ConfigType = 0;
    UINT4               au4ProtServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4ProtRvrServiceOid[ELPS_MAX_OID_LEN];
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    CHR1               *pu1TempStr = NULL;

    MEMSET (au4ProtServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));
    MEMSET (au4ProtRvrServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));
    MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));

    ProtectionService.u4_Length = 0;
    ProtectionService.pu4_OidList = au4ProtServiceOid;

    RvrProtectionService.u4_Length = 0;
    RvrProtectionService.pu4_OidList = au4ProtRvrServiceOid;

    nmhGetFsElpsPgConfigProtectionServiceValue (u4ContextId, u4PgId,
                                                &u4ProtectionServiceValue);
    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4PgServiceType);

    nmhGetFsElpsPgConfigProtectionInstanceId (u4ContextId,
                                              u4PgId, &u4ProtectionInstanceId);

    nmhGetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId, &u4ProtectionMeg);
    nmhGetFsElpsPgCfmProtectionME (u4ContextId, u4PgId, &u4ProtectionMe);
    nmhGetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId, &u4ProtectionMep);

    nmhGetFsElpsPgCfmProtectionReverseMEG (u4ContextId, u4PgId,
                                           &u4ProtectionRevMeg);
    nmhGetFsElpsPgCfmProtectionReverseME (u4ContextId, u4PgId,
                                          &u4ProtectionRevMe);
    nmhGetFsElpsPgCfmProtectionReverseMEP (u4ContextId, u4PgId,
                                           &u4ProtectionRevMep);

    PgName.pu1_OctetList = au1PgNameStr;
    PgName.i4_Length = 0;

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        CliPrintf (CliHandle, "ElpsCliShowPgProtectionEntity: "
                   "Failed while allocating memory\r\n");
        return;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        CliPrintf (CliHandle, "ElpsCliShowPgProtectionEntity: "
                   "Failed while allocating memory\r\n");
        return;
    }

    nmhGetFsElpsPgConfigName (u4ContextId, u4PgId, &PgName);

    CliPrintf (CliHandle, "\r\nProtection Group Id   %u\r\n", u4PgId);
    if (PgName.i4_Length != 0)
    {
        CliPrintf (CliHandle, "Protection Group Name %s\r\n",
                   PgName.pu1_OctetList);
    }
    CliPrintf (CliHandle, "Protection Group Protection Entity\r\n");
    CliPrintf (CliHandle, "---------------------------------------------\r\n");

    if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));
        nmhGetFsElpsPgConfigProtectionPort (u4ContextId, u4PgId,
                                            (INT4 *) &u4ProtectionPortId);

        MEMSET (au1NameStr, 0, sizeof (au1NameStr));
        ElpsPortCfaCliGetIfName (u4ProtectionPortId, (INT1 *) au1NameStr);

        CliPrintf (CliHandle, "Protection Port Id       : %s\r\n", au1NameStr);

        CliPrintf (CliHandle, "Protection Instance Id   : %d\r\n",
                   u4ProtectionInstanceId);

        if (u4ProtectionServiceValue != 0)
        {
            CliPrintf (CliHandle, "Protection Service Id    : %d\r\n",
                       u4ProtectionServiceValue);
        }
        else
        {
            CliPrintf (CliHandle, "Protection Service List  :\r\n");
            ElpsCliPrintServiceList (CliHandle, u4ContextId, u4PgId);
        }

        CliPrintf (CliHandle, "Protection MEG           : %d\r\n",
                   u4ProtectionMeg);
        CliPrintf (CliHandle, "Protection ME            : %d\r\n",
                   u4ProtectionMe);
        CliPrintf (CliHandle, "Protection MEP           : %d\r\n",
                   u4ProtectionMep);
    }
    else
    {
        nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4ConfigType);

        nmhGetFsElpsPgConfigProtectionServicePointer (u4ContextId, u4PgId,
                                                      &ProtectionService);
        if (i4ConfigType == ELPS_PG_TYPE_INDIVIDUAL)
        {
            if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)    /* LSP */
            {
                if (ProtectionService.u4_Length != ELPS_ERR_OID_LEN)
                {
                    CliPrintf (CliHandle, "Protection Service Value : \r\n");

                    CliPrintf (CliHandle,
                               "  Tunnel Index                     : %d\r\n",
                               ProtectionService.
                               pu4_OidList[ELPS_TNL_INDEX_START_OFFSET]);
                    CliPrintf (CliHandle,
                               "  Tunnel Instance                  : %d\r\n",
                               ProtectionService.
                               pu4_OidList[ELPS_TNL_INST_START_OFFSET]);

                    if (ElpsTnlGetTunnelAddrType (ProtectionService,
                                                  pInMplsApiInfo,
                                                  pOutMplsApiInfo)
                        == OSIX_SUCCESS)
                    {
                        if (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                            SrcNodeId.u4NodeType == ELPS_ADDR_TYPE_UCAST)
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                       pOutMplsApiInfo->
                                                       OutTeTnlInfo.TnlLspId.
                                                       SrcNodeId.MplsRouterId.
                                                       u4_addr[0]);
                            CliPrintf (CliHandle,
                                       "  Ingress LSR Id                   : "
                                       "%s\r\n", pu1TempStr);

                            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                       pOutMplsApiInfo->
                                                       OutTeTnlInfo.TnlLspId.
                                                       DstNodeId.MplsRouterId.
                                                       u4_addr[0]);
                            CliPrintf (CliHandle,
                                       "  Egress LSR Id                    : "
                                       "%s\r\n", pu1TempStr);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "  Ingress LSR Id (GId:Nid)         : "
                                       "%d:%d\r\n",
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       SrcNodeId.MplsGlobalNodeId.u4GlobalId,
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       SrcNodeId.MplsGlobalNodeId.u4NodeId);

                            CliPrintf (CliHandle,
                                       "  Egress LSR Id (GId:Nid)          : "
                                       "%d:%d\r\n",
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       DstNodeId.MplsGlobalNodeId.u4NodeId);
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "  Ingress LSR Id                   : "
                                   "%d\r\n",
                                   ProtectionService.pu4_OidList
                                   [ELPS_TNL_ING_LSRID_START_OFFSET]);
                        CliPrintf (CliHandle,
                                   "  Egress LSR Id                    : "
                                   "%d\r\n",
                                   ProtectionService.pu4_OidList
                                   [ELPS_TNL_EGG_LSRID_START_OFFSET]);
                    }

                }
                else
                {
                    CliPrintf (CliHandle,
                               "Protection Service Value         : 0 \r\n");
                }
                CliPrintf (CliHandle,
                           "  Protection MEG                   : %d\r\n",
                           u4ProtectionMeg);
                CliPrintf (CliHandle,
                           "  Protection ME                    : %d\r\n",
                           u4ProtectionMe);
                CliPrintf (CliHandle,
                           "  Protection MEP                   : %d\r\n",
                           u4ProtectionMep);
                nmhGetFsElpsPgConfigProtectionReverseServicePointer
                    (u4ContextId, u4PgId, &RvrProtectionService);

                if (RvrProtectionService.u4_Length != ELPS_ERR_OID_LEN)
                {

                    CliPrintf (CliHandle,
                               "Reverse Protection Service Value : \r\n");
                    CliPrintf (CliHandle,
                               "  Reverse Tunnel Index             : %d\r\n",
                               RvrProtectionService.
                               pu4_OidList[ELPS_TNL_INDEX_START_OFFSET]);
                    CliPrintf (CliHandle,
                               "  Reverse Tunnel Instance          : %d\r\n",
                               RvrProtectionService.
                               pu4_OidList[ELPS_TNL_INST_START_OFFSET]);

                    if (ElpsTnlGetTunnelAddrType (RvrProtectionService,
                                                  pInMplsApiInfo,
                                                  pOutMplsApiInfo)
                        == OSIX_SUCCESS)
                    {
                        if (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                            SrcNodeId.u4NodeType == ELPS_ADDR_TYPE_UCAST)
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                       pOutMplsApiInfo->
                                                       OutTeTnlInfo.TnlLspId.
                                                       SrcNodeId.MplsRouterId.
                                                       u4_addr[0]);
                            CliPrintf (CliHandle,
                                       "  Reverse Ingress LSR Id           : "
                                       "%s\r\n", pu1TempStr);

                            CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                       pOutMplsApiInfo->
                                                       OutTeTnlInfo.TnlLspId.
                                                       DstNodeId.MplsRouterId.
                                                       u4_addr[0]);
                            CliPrintf (CliHandle,
                                       "  Reverse Egress LSR Id            : "
                                       "%s\r\n", pu1TempStr);
                        }
                        else
                        {
                            CliPrintf (CliHandle,
                                       "  Reverse Ingress LSR Id (GId:Nid) : "
                                       "%d:%d\r\n",
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       SrcNodeId.MplsGlobalNodeId.u4GlobalId,
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       SrcNodeId.MplsGlobalNodeId.u4NodeId);

                            CliPrintf (CliHandle,
                                       "  Reverse Egress LSR Id (GId:Nid)   : "
                                       "%d:%d\r\n",
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                       pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                       DstNodeId.MplsGlobalNodeId.u4NodeId);
                        }
                    }
                    else
                    {
                        CliPrintf (CliHandle,
                                   "  Reverse Ingress LSR Id           : %d\r\n",
                                   RvrProtectionService.pu4_OidList
                                   [ELPS_TNL_ING_LSRID_START_OFFSET]);
                        CliPrintf (CliHandle,
                                   "  Reverse Egress LSR Id            : %d\r\n",
                                   RvrProtectionService.pu4_OidList
                                   [ELPS_TNL_EGG_LSRID_START_OFFSET]);
                    }

                }
                else
                {
                    CliPrintf (CliHandle,
                               "  Reverse Protection Service Value : 0 \r\n");
                }
                CliPrintf (CliHandle,
                           "  Reverse Protection MEG           : %d\r\n",
                           u4ProtectionRevMeg);
                CliPrintf (CliHandle,
                           "  Reverse Protection ME            : %d\r\n",
                           u4ProtectionRevMe);
                CliPrintf (CliHandle,
                           "  Reverse Protection MEP           : %d\r\n",
                           u4ProtectionRevMep);
            }
            else
            {
                CliPrintf (CliHandle,
                           "  Protection Service Value         : \r\n");

                MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
                MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

                pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
                pInMplsApiInfo->u4ContextId = 0;
                pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
                pInMplsApiInfo->InPathId.PwId.u4PwIndex =
                    ProtectionService.pu4_OidList[ELPS_PW_INDEX_OFFSET];

                if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                                     pInMplsApiInfo,
                                                     pOutMplsApiInfo)
                    == OSIX_FAILURE)
                {
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pInMplsApiInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pOutMplsApiInfo);

                    CliPrintf (CliHandle,
                               "  Vc Id                            : 0\r\n");
                    CliPrintf (CliHandle,
                               "  Peer Address                     : 0\r\n");

                    return;
                }

                CliPrintf (CliHandle,
                           "  Vc Id                            : %d\r\n",
                           pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId);

                if (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
                    u4NodeType == ELPS_ADDR_TYPE_UCAST)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                               pOutMplsApiInfo->OutPwInfo.
                                               MplsPwPathId.DstNodeId.
                                               MplsRouterId.u4_addr[0]);

                    CliPrintf (CliHandle,
                               "  Peer Address                     : %s\r\n",
                               pu1TempStr);
                }
                else
                {
                    CliPrintf (CliHandle,
                               "  Peer Address(G:N)                : %d:%d\r\n",
                               pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                               DstNodeId.MplsGlobalNodeId.u4GlobalId,
                               pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                               DstNodeId.MplsGlobalNodeId.u4NodeId);
                }

                CliPrintf (CliHandle,
                           "  Protection MEG                   : %d\r\n",
                           u4ProtectionMeg);
                CliPrintf (CliHandle,
                           "  Protection ME                    : %d\r\n",
                           u4ProtectionMe);
                CliPrintf (CliHandle,
                           "  Protection MEP                   : %d\r\n",
                           u4ProtectionMep);
            }
        }
        else
        {
            CliPrintf (CliHandle, "Protection Service List  :\r\n");
            ElpsCliPrintProtectionSrvList (CliHandle, u4ContextId, u4PgId);
        }
    }

    *pu4PagingStatus = CliPrintf (CliHandle, "\r\n");
    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowAllPgInfo 
 *
 * DESCRIPTION      : This function displays all the information relating to 
 *                    a Protection group.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId - Protection Group Identifier
 *
 * OUTPUT           : pu4PagingStatus - Paging Status 
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
VOID
ElpsCliShowAllPgInfo (tCliHandle CliHandle,
                      UINT4 u4ContextId, UINT4 u4NextPgId,
                      UINT4 *pu4PagingStatus)
{
    ElpsCliShowPgConfigurations (CliHandle, u4ContextId, u4NextPgId,
                                 pu4PagingStatus);

    ElpsCliShowPgStatus (CliHandle, u4ContextId, u4NextPgId, pu4PagingStatus);

    ElpsCliShowPgStatistics (CliHandle, u4ContextId, u4NextPgId,
                             pu4PagingStatus);

    ElpsCliShowPgTimerInfo (CliHandle, u4ContextId, u4NextPgId,
                            pu4PagingStatus);

    ElpsCliShowPgWorkingEntity (CliHandle, u4ContextId, u4NextPgId,
                                pu4PagingStatus);

    ElpsCliShowPgProtectionEntity (CliHandle, u4ContextId, u4NextPgId,
                                   pu4PagingStatus);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliSetDebugs 
 *
 * DESCRIPTION      : This function sets and resets the Trace input for a 
 *                    context
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    pu1TraceInput - Input Trace string
 *                    
 * OUTPUT           : pi1ModeName - Mode String
 *                    pi1DispStr - Display string
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE 
 * 
 **************************************************************************/
INT4
ElpsCliSetDebugs (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1TraceInput)
{

    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE TraceInput;

    MEMSET (&TraceInput, 0, sizeof (TraceInput));
    TraceInput.pu1_OctetList = pu1TraceInput;
    TraceInput.i4_Length = STRLEN (pu1TraceInput);

    if (nmhTestv2FsElpsContextTraceInputString (&u4ErrorCode, u4ContextId,
                                                &TraceInput) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsContextTraceInputString (u4ContextId, &TraceInput) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowPgListInProtPort 
 *
 * DESCRIPTION      : This function displays the Protection Group list for a 
 *                    Protection Port.
 *
 * INPUT            : u4IfIndex      - Protection Port Identifier
 *                    
 * OUTPUT           : None                     
 *
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliShowPgListInProtPort (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE PgName;
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN + 1];
    UINT4               u4ContextId = ELPS_INVALID_CONTEXT_ID;
    UINT4               u4NextContextId = ELPS_INVALID_CONTEXT_ID;
    UINT4               u4NextIfIndex = 0;
    UINT4               u4PgId = 0;
    UINT4               u4NextPgId = 0;
    INT4                i4PgStatus = 0;
    UINT2               u2LocalPortId = 0;

    if (ElpsPortVcmGetCxtInfoFromIfIndex (u4IfIndex, &u4ContextId,
                                          &u2LocalPortId) == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n %%Port is not mapped to any context\r\n");
        return CLI_FAILURE;
    }

    if (ElpsUtilIsElpsStarted (u4ContextId) != OSIX_TRUE)
    {
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r\n Protection Group Share Info \r\n");
    CliPrintf (CliHandle, "----------------------------------\r\n");

    if (nmhGetNextIndexFsElpsPgShareTable (u4ContextId, &u4NextContextId,
                                           (INT4) u4IfIndex,
                                           (INT4 *) &u4NextIfIndex,
                                           0, &u4NextPgId) == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    MEMSET (au1NameStr, 0, sizeof (au1NameStr));
    ElpsPortCfaCliGetIfName (u4IfIndex, (INT1 *) au1NameStr);

    CliPrintf (CliHandle, "%-15s", "Protection Port");
    CliPrintf (CliHandle, "    ");
    CliPrintf (CliHandle, "%-14s", "PG Id");
    CliPrintf (CliHandle, "    ");
    CliPrintf (CliHandle, "%-33s", "PG Name");
    CliPrintf (CliHandle, "    ");
    CliPrintf (CliHandle, "%-18s", "Status");
    CliPrintf (CliHandle, "\r\n");

    do
    {
        if ((u4ContextId != u4NextContextId) || (u4IfIndex != u4NextIfIndex))
        {
            break;
        }

        MEMSET (au1PgNameStr, 0, sizeof (au1PgNameStr));
        PgName.pu1_OctetList = au1PgNameStr;
        PgName.i4_Length = 0;

        nmhGetFsElpsPgConfigName (u4NextContextId, u4NextPgId, &PgName);

        nmhGetFsElpsPgSharePgStatus (u4NextContextId, u4NextIfIndex,
                                     u4NextPgId, &i4PgStatus);
        CliPrintf (CliHandle, "%-15s", au1NameStr);
        CliPrintf (CliHandle, "    ");
        CliPrintf (CliHandle, "%-14d", u4NextPgId);
        CliPrintf (CliHandle, "    ");
        CliPrintf (CliHandle, "%-33s", PgName.pu1_OctetList);
        CliPrintf (CliHandle, "    ");

        switch (i4PgStatus)
        {
            case ELPS_PG_STATUS_DISABLE:

                CliPrintf (CliHandle, "%-18s", "Disable");
                break;

            case ELPS_PG_STATUS_WORK_PATH_ACTIVE:

                CliPrintf (CliHandle, "%-18s", "Active-Working");
                break;

            case ELPS_PG_STATUS_PROT_PATH_ACTIVE:

                CliPrintf (CliHandle, "%-18s", "Active-Protection");
                break;

            case ELPS_PG_STATUS_WTR_STATE:

                CliPrintf (CliHandle, "%-18s", "Wait To Restore");
                break;

            case ELPS_PG_STATUS_HOLD_OFF_STATE:

                CliPrintf (CliHandle, "%-18s", "Hold Off");
                break;

            case ELPS_PG_STATUS_SWITCHING_FAILED:

                CliPrintf (CliHandle, "%-18s", "Switching Failed");
                break;

            default:
                break;

        }

        CliPrintf (CliHandle, "\r\n");

        u4PgId = u4NextPgId;

    }
    while
        (nmhGetNextIndexFsElpsPgShareTable (u4ContextId, &u4NextContextId,
                                            (INT4) u4IfIndex,
                                            (INT4 *) &u4NextIfIndex,
                                            u4PgId, &u4NextPgId) ==
         SNMP_SUCCESS);

    CliPrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowDebugging 
 *
 * DESCRIPTION      : This function displays the Trace options enabled
 *                    in a context.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None 
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliShowDebugging (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tElpsContextInfo   *pContextInfo = NULL;
    INT4                i4GlobalTraceOption = 0;
    UINT4               u4DbgLevel = 0;

    nmhGetFsElpsGlobalTraceOption (&i4GlobalTraceOption);

    pContextInfo = ElpsCxtGetNode (u4ContextId);

    if (pContextInfo == NULL)
    {
        return CLI_FAILURE;
    }

    if (ElpsUtilIsElpsStarted (u4ContextId) == OSIX_FALSE)
    {
        return CLI_FAILURE;
    }

    u4DbgLevel = pContextInfo->u4TraceInput;

    if ((u4DbgLevel != 0) || (i4GlobalTraceOption == ELPS_SNMP_TRUE))
    {
        CliPrintf (CliHandle, "\rAPS :\n");
    }
    else
    {
        return CLI_SUCCESS;
    }

    if (i4GlobalTraceOption == ELPS_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "  APS Global Trace option is enabled\r\n");
    }
    if (u4DbgLevel == ELPS_ALL_TRC)
    {
        CliPrintf (CliHandle, "  APS  All debugging is on\r\n");
        return CLI_SUCCESS;
    }
    if ((u4DbgLevel & INIT_SHUT_TRC) != 0)
    {
        CliPrintf (CliHandle, "  APS init and shutdown debugging is on\r\n");
    }
    if ((u4DbgLevel & MGMT_TRC) != 0)
    {
        CliPrintf (CliHandle, "  APS management debugging is on\r\n");
    }
    if ((u4DbgLevel & CONTROL_PLANE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  APS control plane debugging is on\r\n");
    }
    if ((u4DbgLevel & DUMP_TRC) != 0)
    {
        CliPrintf (CliHandle, "  APS packet dump debugging is on\r\n");
    }
    if ((u4DbgLevel & OS_RESOURCE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  APS resources debugging is on\r\n");
    }
    if ((u4DbgLevel & ALL_FAILURE_TRC) != 0)
    {
        CliPrintf (CliHandle, "  APS error debugging is on\r\n");
    }
    if ((u4DbgLevel & BUFFER_TRC) != 0)
    {
        CliPrintf (CliHandle, "  APS buffer debugging is on\r\n");
    }
    if ((u4DbgLevel & ELPS_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "  APS critical debugging is on\r\n");
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliShowRunningConfig
 *
 * DESCRIPTION      : This function displays current ELPS configurations    
 *                                                                           
 * INPUT            : CliHandle   - CliContext Information.                   
 *                    u4ContextId - Context Identifier. 
 *
 * OUTPUT           : None 
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliShowRunningConfig (tCliHandle CliHandle, UINT4 u4ContextId)
{
    tSNMP_OCTET_STRING_TYPE PgName;
    UINT4               u4CurrContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4CurrPgId = 0;
    UINT4               u4WorkgServValue = 0;
    UINT4               u4ProtServValue = 0;
    UINT4               u4Value = 0;
    UINT4               u4Value1 = 0;
    UINT4               u4MEP = 0;
    UINT4               u4ME = 0;
    UINT4               u4MEG = 0;
    UINT1               au1WorkNameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1ProtectNameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1PgNameStr[ELPS_PG_MAX_NAME_LEN + 1];
    INT4                i4RowStatus = 0;
    INT4                i4Port = 0;
    INT4                i4Port1 = 0;
    INT4                i4Value = 0;
    UINT1               au1ContextName[ELPS_SWITCH_ALIAS_LEN];
    UINT4               u4PSCChannelCode = 0;
    UINT4               u4RapidTxTime = 0;
    INT4                i4PgServiceType = 0;
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4RvrServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4ProtServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4ProtRvrServiceOid[ELPS_MAX_OID_LEN];
    INT4                i4ElpsCxtVlanGroupManager = 0;
    UINT4               u4WorkingInstanceId = 0;
    UINT4               u4ProtectionInstanceId = 0;
    INT4                i4PgProtType = 1;
    INT1                i1RetVal = 0;

    tSNMP_OID_TYPE      WorkingService, RvrWorkingService;
    tSNMP_OID_TYPE      ProtectionService, RvrProtectionService;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    CHR1               *pu1TempStr = NULL;

    MEMSET (au4ServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));
    MEMSET (au4RvrServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));

    WorkingService.u4_Length = 0;
    WorkingService.pu4_OidList = au4ServiceOid;

    RvrWorkingService.u4_Length = 0;
    RvrWorkingService.pu4_OidList = au4RvrServiceOid;

    MEMSET (au4ProtServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));
    MEMSET (au4ProtRvrServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));

    ProtectionService.u4_Length = 0;
    ProtectionService.pu4_OidList = au4ProtServiceOid;

    RvrProtectionService.u4_Length = 0;
    RvrProtectionService.pu4_OidList = au4ProtRvrServiceOid;

    nmhGetFsElpsPSCChannelCode (&u4PSCChannelCode);
    nmhGetFsElpsRapidTxTime (&u4RapidTxTime);

    if (ELPS_MIN_PSC_CHANNEL_CODE != u4PSCChannelCode)
    {
        CliPrintf (CliHandle, "aps channel-code %u\r\n",
                   u4PSCChannelCode);
    }
    if (ELPS_MAX_RAPID_TX_TIME != u4RapidTxTime)
    {
        CliPrintf (CliHandle, "aps rapid-transmission-time %u\r\n",
                   u4RapidTxTime);
    }

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        CliPrintf (CliHandle, "ElpsCliShowRunningConfig: "
                   "Failed while allocating memory\r\n");
        return CLI_FAILURE;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        CliPrintf (CliHandle, "ElpsCliShowRunningConfig: "
                   "Failed while allocating memory\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsElpsContextSystemControl (u4ContextId, &i4Value);
    if (i4Value == ELPS_SHUTDOWN)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pOutMplsApiInfo);
        return CLI_SUCCESS;
    }

    MEMSET (au1ContextName, 0, ELPS_SWITCH_ALIAS_LEN);
    ElpsPortVcmGetAliasName (u4ContextId, au1ContextName);
    CliPrintf (CliHandle, "\r\nswitch  %s \r\n", au1ContextName);
    CliPrintf (CliHandle, "no shutdown aps linear\r\n");

    nmhGetFsElpsContextModuleStatus (u4ContextId, &i4Value);
    if (i4Value == ELPS_ENABLED)
    {
        CliPrintf (CliHandle, "aps linear enable\r\n");
    }

    nmhGetFsElpsContextEnableTrap (u4ContextId, &i4Value);
    if (i4Value == ELPS_DISABLED)
    {
        CliPrintf (CliHandle, "no aps linear notification enable\r\n");
    }

    nmhGetFsElpsContextVlanGroupManager (u4ContextId,
                                         &i4ElpsCxtVlanGroupManager);
    if (i4ElpsCxtVlanGroupManager == ELPS_VLAN_GROUP_MANAGER_ELPS)
    {
        CliPrintf (CliHandle, "aps linear vlan-group-manager elps\r\n");
    }

    u4CurrContextId = u4ContextId;

    if (nmhGetNextIndexFsElpsPgConfigTable (u4CurrContextId, &u4NextContextId,
                                            u4CurrPgId, &u4NextPgId)
        == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pOutMplsApiInfo);
        return CLI_SUCCESS;
    }
    do
    {
        if (u4CurrContextId != u4NextContextId)
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pOutMplsApiInfo);
            return CLI_SUCCESS;
        }
        i1RetVal = nmhGetFsElpsPgConfigRowStatus (u4NextContextId, u4NextPgId,
                                                  &i4RowStatus);
        CliPrintf (CliHandle, "aps linear group %d\r\n", u4NextPgId);

        MEMSET (au1PgNameStr, 0, sizeof(au1PgNameStr));
        PgName.pu1_OctetList = au1PgNameStr;
        PgName.i4_Length = 0;

        nmhGetFsElpsPgConfigName (u4NextContextId, u4NextPgId, &PgName);

        if (PgName.i4_Length != 0)
        {
            CliPrintf (CliHandle, "aps group-name %s\r\n",
                       PgName.pu1_OctetList);
        }

        nmhGetFsElpsPgConfigServiceType (u4ContextId, u4NextPgId,
                                         &i4PgServiceType);

        nmhGetFsElpsPgConfigProtType (u4ContextId, u4NextPgId, &i4PgProtType);
        if (i4PgProtType == ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_BIDIR_APS)
        {
            CliPrintf (CliHandle, "aps mode one-plus-one bidirectional\r\n");
        }
        else if (i4PgProtType == ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_APS)
        {
            CliPrintf (CliHandle,
                       "aps mode one-plus-one unidirectional aps-channel\r\n");
        }
        else if (i4PgProtType == ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_NO_APS)
        {
            CliPrintf (CliHandle,
                       "aps mode one-plus-one unidirectional no-aps-channel\r\n");
        }

        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
        {
            nmhGetFsElpsPgConfigIngressPort (u4NextContextId, u4NextPgId,
                                             &i4Port);

            if (i4Port != ELPS_INIT_VAL)
            {
                MEMSET (au1NameStr, 0, sizeof (au1NameStr));
                ElpsPortCfaCliConfGetIfName ((UINT4) i4Port,
                                             (INT1 *) au1NameStr);
                CliPrintf (CliHandle, "aps ingress %s \r\n", au1NameStr);
            }

            nmhGetFsElpsPgConfigWorkingPort (u4NextContextId, u4NextPgId,
                                             &i4Port);
            nmhGetFsElpsPgConfigProtectionPort (u4NextContextId, u4NextPgId,
                                                &i4Port1);
            if (i4Port != ELPS_INIT_VAL)
            {
                MEMSET (au1WorkNameStr, 0, sizeof (au1WorkNameStr));
                ElpsPortCfaCliConfGetIfName (i4Port, (INT1 *) au1WorkNameStr);
            }

            if (i4Port1 != ELPS_INIT_VAL)
            {
                MEMSET (au1ProtectNameStr, 0, sizeof (au1ProtectNameStr));
                ElpsPortCfaCliConfGetIfName (i4Port1,
                                             (INT1 *) au1ProtectNameStr);
            }

            nmhGetFsElpsPgConfigWorkingInstanceId (u4NextContextId,
                                                   u4NextPgId,
                                                   &u4WorkingInstanceId);
            if (u4WorkingInstanceId != ELPS_INIT_VAL)
            {
                CliPrintf (CliHandle, "aps working vlan-group %d\r\n",
                           u4WorkingInstanceId);
            }

            nmhGetFsElpsPgConfigProtectionInstanceId (u4NextContextId,
                                                      u4NextPgId,
                                                      &u4ProtectionInstanceId);
            if (u4ProtectionInstanceId != ELPS_INIT_VAL)
            {
                CliPrintf (CliHandle, "aps protection vlan-group %d\r\n",
                           u4ProtectionInstanceId);
            }

        }

        nmhGetFsElpsPgConfigType (u4NextContextId, u4NextPgId, &i4Value);

        if (i4Value == ELPS_PG_TYPE_INDIVIDUAL)
        {
            if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {
                nmhGetFsElpsPgConfigWorkingServiceValue (u4NextContextId,
                                                         u4NextPgId,
                                                         &u4WorkgServValue);
                nmhGetFsElpsPgConfigProtectionServiceValue (u4NextContextId,
                                                            u4NextPgId,
                                                            &u4ProtServValue);
                if (i4Port != ELPS_INIT_VAL)
                {
                    CliPrintf (CliHandle, "aps working %s service %d\r\n",
                               au1WorkNameStr, u4WorkgServValue);
                }
                if (i4Port1 != ELPS_INIT_VAL)
                {
                    CliPrintf (CliHandle, "aps protect %s service %d\r\n",
                               au1ProtectNameStr, u4ProtServValue);
                }
            }
            else
            {
                nmhGetFsElpsPgConfigWorkingServicePointer (u4ContextId,
                                                           u4NextPgId,
                                                           &WorkingService);
                if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
                {
                    if (WorkingService.u4_Length != ELPS_ERR_OID_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "aps working mpls-lsp tunnel %d lsp %d "
                                   "source %d destination %d",
                                   WorkingService.
                                   pu4_OidList[ELPS_TNL_INDEX_START_OFFSET],
                                   WorkingService.
                                   pu4_OidList[ELPS_TNL_INST_START_OFFSET],
                                   WorkingService.pu4_OidList
                                   [ELPS_TNL_ING_LSRID_START_OFFSET],
                                   WorkingService.pu4_OidList
                                   [ELPS_TNL_EGG_LSRID_START_OFFSET]);

                    }

                    nmhGetFsElpsPgConfigWorkingReverseServicePointer
                        (u4ContextId, u4NextPgId, &RvrWorkingService);

                    if (RvrWorkingService.u4_Length != ELPS_ERR_OID_LEN)
                    {
                        CliPrintf (CliHandle, " reverse tunnel %d lsp %d "
                                   "source %d destination %d",
                                   RvrWorkingService.
                                   pu4_OidList[ELPS_TNL_INDEX_START_OFFSET],
                                   RvrWorkingService.
                                   pu4_OidList[ELPS_TNL_INST_START_OFFSET],
                                   RvrWorkingService.pu4_OidList
                                   [ELPS_TNL_ING_LSRID_START_OFFSET],
                                   RvrWorkingService.pu4_OidList
                                   [ELPS_TNL_EGG_LSRID_START_OFFSET]);
                    }
                    CliPrintf (CliHandle, "\r\n");

                }
                else if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW)
                {
                    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
                    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

                    pInMplsApiInfo->u4SubReqType =
                        MPLS_GET_PW_INFO_FROM_PW_INDEX;
                    pInMplsApiInfo->u4ContextId = 0;
                    pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
                    pInMplsApiInfo->InPathId.PwId.u4PwIndex =
                        WorkingService.pu4_OidList[ELPS_PW_INDEX_OFFSET];

                    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                                         pInMplsApiInfo,
                                                         pOutMplsApiInfo)
                        == OSIX_FAILURE)
                    {
                        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                            (UINT1 *) pInMplsApiInfo);
                        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                            (UINT1 *) pOutMplsApiInfo);
                        CliPrintf (CliHandle,
                                   "MplsApiHandleRequest Failed.\r\n");
                        return CLI_FAILURE;
                    }

                    CliPrintf (CliHandle, "aps working mpls-pw vc-id %d",
                               pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId);

                    if (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
                        u4NodeType == ELPS_ADDR_TYPE_LOCAL_MAP_NUM)
                    {
                        /* TODO - convert gid-nid into Loc Num */
                        CliPrintf (CliHandle, " peer-address %d%d\r\n",
                                   pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                                   DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                                   DstNodeId.MplsGlobalNodeId.u4NodeId);
                    }
                    else
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->OutPwInfo.
                                                   MplsPwPathId.DstNodeId.
                                                   MplsRouterId.u4_addr[0]);
                        CliPrintf (CliHandle, " peer-address : %s\r\n",
                                   pu1TempStr);
                    }
                }

                /* Protection Service */
                nmhGetFsElpsPgConfigProtectionServicePointer
                    (u4ContextId, u4NextPgId, &ProtectionService);

                if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
                {
                    if (ProtectionService.u4_Length != ELPS_ERR_OID_LEN)
                    {
                        CliPrintf (CliHandle,
                                   "aps protect mpls-lsp tunnel %d lsp %d "
                                   "source %d destination %d",
                                   ProtectionService.
                                   pu4_OidList[ELPS_TNL_INDEX_START_OFFSET],
                                   ProtectionService.
                                   pu4_OidList[ELPS_TNL_INST_START_OFFSET],
                                   ProtectionService.pu4_OidList
                                   [ELPS_TNL_ING_LSRID_START_OFFSET],
                                   ProtectionService.pu4_OidList
                                   [ELPS_TNL_EGG_LSRID_START_OFFSET]);
                    }
                    nmhGetFsElpsPgConfigProtectionReverseServicePointer
                        (u4ContextId, u4NextPgId, &RvrProtectionService);

                    if (RvrProtectionService.u4_Length != ELPS_ERR_OID_LEN)
                    {
                        CliPrintf (CliHandle, " reverse tunnel %d lsp %d "
                                   "source %d destination %d",
                                   RvrProtectionService.
                                   pu4_OidList[ELPS_TNL_INDEX_START_OFFSET],
                                   RvrProtectionService.
                                   pu4_OidList[ELPS_TNL_INST_START_OFFSET],
                                   RvrProtectionService.pu4_OidList
                                   [ELPS_TNL_ING_LSRID_START_OFFSET],
                                   RvrProtectionService.pu4_OidList
                                   [ELPS_TNL_EGG_LSRID_START_OFFSET]);
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
                else if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW)
                {
                    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
                    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

                    pInMplsApiInfo->u4SubReqType =
                        MPLS_GET_PW_INFO_FROM_PW_INDEX;
                    pInMplsApiInfo->u4ContextId = 0;
                    pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
                    pInMplsApiInfo->InPathId.PwId.u4PwIndex =
                        ProtectionService.pu4_OidList[ELPS_PW_INDEX_OFFSET];

                    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                                         pInMplsApiInfo,
                                                         pOutMplsApiInfo)
                        == OSIX_FAILURE)
                    {
                        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                            (UINT1 *) pInMplsApiInfo);
                        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                            (UINT1 *) pOutMplsApiInfo);
                        CliPrintf (CliHandle,
                                   "MplsApiHandleRequest Failed.\r\n");
                        return CLI_FAILURE;
                    }

                    CliPrintf (CliHandle, "aps protect mpls-pw vc-id %d",
                               pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId);

                    if (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
                        u4NodeType == ELPS_ADDR_TYPE_LOCAL_MAP_NUM)
                    {
                        /* TODO - convert gid-nid into Loc Num */
                        CliPrintf (CliHandle, " peer-address %d%d\r\n",
                                   pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                                   DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                                   DstNodeId.MplsGlobalNodeId.u4NodeId);
                    }
                    else
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->OutPwInfo.
                                                   MplsPwPathId.DstNodeId.
                                                   MplsRouterId.u4_addr[0]);
                        CliPrintf (CliHandle, " peer-address : %s\r\n",
                                   pu1TempStr);
                    }
                }
            }
        }
        else if (i4Value == ELPS_PG_TYPE_LIST)
        {
            if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {
                CliPrintf (CliHandle, "aps working %s service-list ",
                           au1WorkNameStr);
                ElpsCliPrintServiceList (CliHandle, u4NextContextId,
                                         u4NextPgId);
                CliPrintf (CliHandle, "aps protect %s service-list ",
                           au1ProtectNameStr);
                ElpsCliPrintServiceList (CliHandle, u4NextContextId,
                                         u4NextPgId);
            }
            else if ((i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                     (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW))
            {
                CliPrintf (CliHandle, "Working Service List  : ");
                ElpsCliPrintWorkingServiceList (CliHandle, u4ContextId,
                                                u4NextPgId);

                CliPrintf (CliHandle, "Protection Service List  : ");
                ElpsCliPrintProtectionSrvList (CliHandle, u4ContextId,
                                               u4NextPgId);
            }
        }
        else if (i4Value == ELPS_PG_TYPE_ALL)
        {
            CliPrintf (CliHandle, "aps working %s vlan all\r\n", au1WorkNameStr);
            CliPrintf (CliHandle, "aps protect %s vlan all\r\n", au1ProtectNameStr);
        }

        nmhGetFsElpsPgConfigOperType (u4NextContextId, u4NextPgId, &i4Value);
        nmhGetFsElpsPgCmdWTR (u4NextContextId, u4NextPgId, &u4Value1);

        if (i4Value == ELPS_PG_OPER_TYPE_NONREVERTIVE)
        {
            CliPrintf (CliHandle, "no aps revert\r\n");
        }

        if (u4Value1 != ELPS_PG_DEF_WTR_TIME)
        {
            CliPrintf (CliHandle, "aps wait-to-restore %d\r\n", u4Value1);
        }

        if (nmhGetFsElpsPgConfigMonitorMechanism (u4NextContextId, u4NextPgId,
                                                    (INT4*)&u4Value) == SNMP_SUCCESS)
        {
            if (u4Value != ELPS_PG_MONITOR_MECH_CFM)
            {
                if (u4Value == ELPS_PG_MONITOR_MECH_MPLSOAM)
                {
                    CliPrintf (CliHandle, "aps monitor mplsoam");
                    if (nmhGetFsElpsPgPscVersion (u4NextContextId, u4NextPgId,
                                                    &u4Value1) == SNMP_SUCCESS)
                    {
                        if (u4Value1 != 0)
                        {
                            CliPrintf (CliHandle, " version %u",u4Value1);
                        }
                    }
                    CliPrintf (CliHandle, "\r\n");
                }
                else
                {
                    CliPrintf (CliHandle, "aps monitor none\r\n");
                }
            }
        }

        nmhGetFsElpsPgCmdHoTime (u4NextContextId, u4NextPgId, &u4Value);
        nmhGetFsElpsPgCmdApsPeriodicTime (u4NextContextId, u4NextPgId,
                                          &u4Value1);
        if ((u4Value != ELPS_INIT_VAL) &&
            (u4Value1 != ELPS_PERIODIC_APS_PDU_INTERVAL))
        {
            CliPrintf (CliHandle, "aps timers periodic %d hold-off %d\r\n",
                       u4Value1, u4Value);
        }
        else if (u4Value1 != ELPS_PERIODIC_APS_PDU_INTERVAL)
        {
            CliPrintf (CliHandle, "aps timers periodic %d hold-off 0\r\n",
                       u4Value1);
        }
        else if (u4Value != ELPS_INIT_VAL)
        {
            CliPrintf (CliHandle, "aps hold-off %d\r\n", u4Value);
        }


        nmhGetFsElpsPgCmdExtCmd (u4NextContextId, u4NextPgId, &i4Value);
        switch (i4Value)
        {
            case ELPS_EXT_CMD_FS:
                CliPrintf (CliHandle, "aps force\r\n");
                break;
            case ELPS_EXT_CMD_MS:
                CliPrintf (CliHandle, "aps manual protection\r\n");
                break;
            case ELPS_EXT_CMD_MS_W:
                CliPrintf (CliHandle, "aps manual working\r\n");
                break;
            case ELPS_EXT_CMD_LOP:
                CliPrintf (CliHandle, "aps lockout\r\n");
                break;
            case ELPS_EXT_CMD_EXER:
                CliPrintf (CliHandle, "aps exercise\r\n");
                break;
            case ELPS_EXT_CMD_FREEZE:
                CliPrintf (CliHandle, "aps freeze\r\n");
                break;
            case ELPS_EXT_CMD_CLR_FREEZE:
                CliPrintf (CliHandle, "no aps freeze\r\n");
                break;
            default:
                break;
        }

        nmhGetFsElpsPgCfmWorkingMEG (u4NextContextId, u4NextPgId, &u4MEG);
        nmhGetFsElpsPgCfmWorkingME (u4NextContextId, u4NextPgId, &u4ME);
        nmhGetFsElpsPgCfmWorkingMEP (u4NextContextId, u4NextPgId, &u4MEP);

        if ((u4MEG != ELPS_INIT_VAL) && (u4ME != ELPS_INIT_VAL))
        {
            CliPrintf (CliHandle, "aps working meg %d me %d ", u4MEG, u4ME);
        }

        if (u4MEP != ELPS_INIT_VAL)
        {
            CliPrintf (CliHandle, "mep %d\r\n", u4MEP);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }

        nmhGetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4NextPgId, &u4MEG);
        nmhGetFsElpsPgCfmWorkingReverseME (u4ContextId, u4NextPgId, &u4ME);
        nmhGetFsElpsPgCfmWorkingReverseMEP (u4ContextId, u4NextPgId, &u4MEP);

        if ((u4MEG != ELPS_INIT_VAL) && (u4ME != ELPS_INIT_VAL))
        {
            CliPrintf (CliHandle, "aps working reverse meg %d me %d ", u4MEG, u4ME);
        }

        if (u4MEP != ELPS_INIT_VAL)
        {
            CliPrintf (CliHandle, "mep %d\r\n", u4MEP);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }

        nmhGetFsElpsPgCfmProtectionMEG (u4NextContextId, u4NextPgId, &u4MEG);
        nmhGetFsElpsPgCfmProtectionME (u4NextContextId, u4NextPgId, &u4ME);
        nmhGetFsElpsPgCfmProtectionMEP (u4ContextId, u4NextPgId, &u4MEP);

        if ((u4MEG != ELPS_INIT_VAL) && (u4ME != ELPS_INIT_VAL))
        {
            CliPrintf (CliHandle, "aps protect meg %d me %d ", u4MEG, u4ME);
        }

        if (u4MEP != ELPS_INIT_VAL)
        {
            CliPrintf (CliHandle, "mep %d\r\n", u4MEP);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }

        nmhGetFsElpsPgCfmProtectionReverseMEG (u4ContextId, u4NextPgId, &u4MEG);
        nmhGetFsElpsPgCfmProtectionReverseME (u4ContextId, u4NextPgId, &u4ME);
        nmhGetFsElpsPgCfmProtectionReverseMEP (u4ContextId, u4NextPgId, &u4MEP);

        if ((u4MEG != ELPS_INIT_VAL) && (u4ME != ELPS_INIT_VAL))
        {
            CliPrintf (CliHandle, "aps protect reverse meg %d me %d ", u4MEG, u4ME);
        }

        if (u4MEP != ELPS_INIT_VAL)
        {
            CliPrintf (CliHandle, "mep %d\r\n", u4MEP);
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }

        if (i4RowStatus == ACTIVE)
        {
            CliPrintf (CliHandle, "aps group active\r\n");
        }
        u4CurrContextId = u4NextContextId;
        u4CurrPgId = u4NextPgId;
        CliPrintf (CliHandle, "! \r\n");
    }
    while (nmhGetNextIndexFsElpsPgConfigTable (u4CurrContextId,
                                               &u4NextContextId,
                                               u4CurrPgId,
                                               &u4NextPgId) == SNMP_SUCCESS);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);

    UNUSED_PARAM (i1RetVal);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliPrintServiceList
 *
 * DESCRIPTION      : This function prints the service list information    
 *                                                                           
 * INPUT            : CliHandle   - CliContext Information.                   
 *                    u4ContextId - Context Identifier. 
 *                    u4PgId      - Protection Group Identifier
 *
 * OUTPUT           : None 
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliPrintServiceList (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4PgId)
{
    UINT1              *pu1SrvList = NULL;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4Value = 0;
    UINT4               u4NextValue = 0;

    pu1SrvList = UtlShMemAllocVlanList ();
    if (pu1SrvList == NULL)
    {
        CliPrintf (CliHandle, "\r%% Memory allocation for Vlan List"
                   " failed.\r\n");
        return CLI_FAILURE;
    }

    MEMSET (pu1SrvList, 0, ELPS_SRV_LIST_SIZE);

    if (nmhGetNextIndexFsElpsPgServiceListTable (u4ContextId, &u4NextContextId,
                                                 u4PgId, &u4NextPgId, u4Value,
                                                 &u4NextValue) == SNMP_FAILURE)
    {
        UtlShMemFreeVlanList (pu1SrvList);
        CliPrintf (CliHandle, "\r\n");
        return CLI_SUCCESS;
    }

    do
    {
        u4Value = u4NextValue;
        if ((u4ContextId != u4NextContextId) || (u4PgId != u4NextPgId))
        {
            ElpsCliShowServiceListForPg (CliHandle, pu1SrvList);
            CliPrintf (CliHandle, "\r\n");
            UtlShMemFreeVlanList (pu1SrvList);
            return CLI_SUCCESS;
        }

        OSIX_BITLIST_SET_BIT (pu1SrvList, u4Value, ELPS_SRV_LIST_SIZE);

    }
    while (nmhGetNextIndexFsElpsPgServiceListTable
           (u4ContextId, &u4NextContextId, u4PgId, &u4NextPgId, u4Value,
            &u4NextValue) == SNMP_SUCCESS);

    ElpsCliShowServiceListForPg (CliHandle, pu1SrvList);
    CliPrintf (CliHandle, "\r\n");
    UtlShMemFreeVlanList (pu1SrvList);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliShowServiceListForPg                        */
/*                                                                           */
/*     DESCRIPTION      : This function converts the octet list containing   */
/*                        services and displays it in suitable form.         */
/*                                                                           */
/*     INPUT            : CliHandle  - Handle to the cli context             */
/*                        pServiceList - Service List                        */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                           :                */
/*****************************************************************************/
VOID
ElpsCliShowServiceListForPg (tCliHandle CliHandle, UINT1 *pServiceList)
{
    INT4                i4CommaCount = 0;
    UINT2               u2ServiceId = 0;
    UINT2               u2Temp = 0;
    UINT2               u2LastPrint = 0;
    UINT1               u1Flag1 = OSIX_TRUE;
    UINT1               u1Result = OSIX_FALSE;

    for (u2ServiceId = 1; u2ServiceId <= ELPS_MAX_SRV_ID; u2ServiceId++)
    {
        OSIX_BITLIST_IS_BIT_SET (pServiceList, u2ServiceId,
                                 ELPS_SRV_LIST_SIZE, u1Result);

        if (u1Result == OSIX_TRUE)
        {
            if (u1Flag1 != OSIX_TRUE)
            {
                if ((u2ServiceId - u2Temp) != 1)
                {
                    if (u2LastPrint != u2Temp)
                    {
                        CliPrintf (CliHandle, "-");
                        CliPrintf (CliHandle, "%d", u2Temp);
                    }
                    i4CommaCount++;
                    ElpsCliPrintServices (CliHandle, i4CommaCount, u2ServiceId);
                    u2LastPrint = u2ServiceId;
                }
            }
            else
            {

                i4CommaCount++;
                ElpsCliPrintServices (CliHandle, i4CommaCount, u2ServiceId);
                i4CommaCount++;
                u2LastPrint = u2ServiceId;
                u1Flag1 = OSIX_FALSE;
            }
            u2Temp = u2ServiceId;
        }
    }

    OSIX_BITLIST_IS_BIT_SET (pServiceList, u2Temp, ELPS_SRV_LIST_SIZE,
                             u1Result);

    if (u1Result == OSIX_TRUE)
    {
        if (u2LastPrint != u2Temp)
        {
            CliPrintf (CliHandle, "-");
            CliPrintf (CliHandle, "%d", u2Temp);
        }
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliPrintServices                               */
/*                                                                           */
/*     DESCRIPTION      : This function displays the Service List            */
/*                                                                           */
/*     INPUT            :  piIfName - Port Name                              */
/*                         CliHandle-CLI Handler                             */
/*                         i4CommaCount- position of service in service list */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*                                                                           */
/*****************************************************************************/
VOID
ElpsCliPrintServices (tCliHandle CliHandle, INT4 i4CommaCount,
                      UINT2 u2ServiceId)
{
#define ELPS_CLI_MAX_COMMAS_PER_LINE 4
    INT4                i4Times = ELPS_SYS_MAX_NUM_SERVICE_IN_LIST_EXT;
    INT4                i4Loop = 0;
    INT4                i4Count = 1;

    if (i4CommaCount == i4Count)
    {
        CliPrintf (CliHandle, "%d", u2ServiceId);
        return;
    }
    for (i4Loop = 1; i4Loop < i4Times; i4Loop++)
    {
        /* Only 4 commas per line will be displayed */
        if (i4CommaCount <= (ELPS_CLI_MAX_COMMAS_PER_LINE * i4Loop))
        {
            if (i4CommaCount == ((ELPS_CLI_MAX_COMMAS_PER_LINE * (i4Loop - 1))
                                 + 1))
            {
                CliPrintf (CliHandle, ",\r\n                ");
                CliPrintf (CliHandle, "%d", u2ServiceId);
            }
            else
            {
                CliPrintf (CliHandle, ",%d", u2ServiceId);
            }
            break;
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliPrintWorkingServiceList
 *
 * DESCRIPTION      : This function prints the service list information    
 *                                                                           
 * INPUT            : CliHandle   - CliContext Information.                   
 *                    u4ContextId - Context Identifier. 
 *                    u4PgId      - Protection Group Identifier
 *
 * OUTPUT           : None 
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliPrintWorkingServiceList (tCliHandle CliHandle, UINT4 u4ContextId,
                                UINT4 u4PgId)
{
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4RvrServiceOid[ELPS_MAX_OID_LEN];
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4ServiceListId = 0;
    UINT4               u4NextServiceListId = 0;
    tSNMP_OID_TYPE      WorkingService;
    tSNMP_OID_TYPE      RvrWorkingService;
    INT4                i4PgServiceType = 0;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    CHR1               *pu1TempStr = NULL;
    UINT4               u4MegId = 0;
    UINT4               u4MeId = 0;
    UINT4               u4MepId = 0;
    UINT4               u4RevMegId = 0;
    UINT4               u4RevMeId = 0;
    UINT4               u4RevMepId = 0;

    MEMSET (au4ServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));
    MEMSET (au4RvrServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));

    WorkingService.u4_Length = 0;
    WorkingService.pu4_OidList = au4ServiceOid;

    RvrWorkingService.u4_Length = 0;
    RvrWorkingService.pu4_OidList = au4RvrServiceOid;

    nmhGetFsElpsPgCfmWorkingMEG (u4ContextId, u4PgId, &u4MegId);
    nmhGetFsElpsPgCfmWorkingME (u4ContextId, u4PgId, &u4MeId);
    nmhGetFsElpsPgCfmWorkingMEP (u4ContextId, u4PgId, &u4MepId);

    nmhGetFsElpsPgCfmWorkingReverseMEG (u4ContextId, u4PgId, &u4RevMegId);
    nmhGetFsElpsPgCfmWorkingReverseME (u4ContextId, u4PgId, &u4RevMeId);
    nmhGetFsElpsPgCfmWorkingReverseMEP (u4ContextId, u4PgId, &u4RevMepId);

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        CliPrintf (CliHandle, "ElpsCliPrintWorkingServiceList: "
                   "Failed while allocating memory\r\n");
        return CLI_FAILURE;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        CliPrintf (CliHandle, "ElpsCliPrintWorkingServiceList: "
                   "Failed while allocating memory\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4PgServiceType);

    if (nmhGetNextIndexFsElpsPgServiceListPointerTable (u4ContextId,
                                                        &u4NextContextId,
                                                        u4PgId, &u4NextPgId,
                                                        u4ServiceListId,
                                                        &u4NextServiceListId)
        == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pOutMplsApiInfo);
        CliPrintf (CliHandle, "\r\n");

        CliPrintf (CliHandle, "Working Service List         : 0\r\n");
        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            CliPrintf (CliHandle, "Reverse Working Service List : 0\r\n");
        }

        CliPrintf (CliHandle, "  Working MEG             : %d\r\n", u4MegId);
        CliPrintf (CliHandle, "  Working ME              : %d\r\n", u4MeId);
        CliPrintf (CliHandle, "  Working MEP             : %d\r\n", u4MepId);

        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {

            CliPrintf (CliHandle, "  Reverse Working MEG     : %d\r\n",
                       u4RevMegId);
            CliPrintf (CliHandle, "  Reverse Working ME      : %d\r\n",
                       u4RevMeId);
            CliPrintf (CliHandle, "  Reverse Working MEP     : %d\r\n",
                       u4RevMepId);
        }
        return CLI_SUCCESS;
    }

    do
    {
        u4ServiceListId = u4NextServiceListId;
        if ((u4ContextId != u4NextContextId) || (u4PgId != u4NextPgId))
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pOutMplsApiInfo);
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle,
                   "\n Context: %d     Pg Id: %d     List Id: %d\r\n",
                   u4ContextId, u4PgId, u4ServiceListId);

        nmhGetFsElpsPgWorkingServiceListPointer (u4ContextId, u4PgId,
                                                 u4ServiceListId,
                                                 &WorkingService);
        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            if (WorkingService.u4_Length != ELPS_ERR_OID_LEN)
            {
                CliPrintf (CliHandle,
                           "  Working Service Value            : %d, %d",
                           WorkingService.
                           pu4_OidList[ELPS_TNL_INDEX_START_OFFSET],
                           WorkingService.
                           pu4_OidList[ELPS_TNL_INST_START_OFFSET]);

                if (ElpsTnlGetTunnelAddrType (WorkingService,
                                              pInMplsApiInfo,
                                              pOutMplsApiInfo) == OSIX_SUCCESS)
                {
                    if (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                        SrcNodeId.u4NodeType == ELPS_ADDR_TYPE_UCAST)
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->
                                                   OutTeTnlInfo.TnlLspId.
                                                   SrcNodeId.MplsRouterId.
                                                   u4_addr[0]);
                        CliPrintf (CliHandle, ", %s", pu1TempStr);

                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->
                                                   OutTeTnlInfo.TnlLspId.
                                                   DstNodeId.MplsRouterId.
                                                   u4_addr[0]);
                        CliPrintf (CliHandle, ", %s\r\n", pu1TempStr);
                    }
                    else
                    {
                        CliPrintf (CliHandle, ", (%d:%d)",
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   SrcNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   SrcNodeId.MplsGlobalNodeId.u4NodeId);

                        CliPrintf (CliHandle, ", (%d:%d)\r\n",
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   DstNodeId.MplsGlobalNodeId.u4NodeId);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, ", (%d:%d)\r\n",
                               WorkingService.
                               pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET],
                               WorkingService.
                               pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET]);
                }
            }
            else
            {
                CliPrintf (CliHandle, "Working Service List          : 0\r\n");
            }

            nmhGetFsElpsPgWorkingReverseServiceListPointer (u4ContextId, u4PgId,
                                                            u4ServiceListId,
                                                            &RvrWorkingService);

            if (RvrWorkingService.u4_Length != ELPS_ERR_OID_LEN)
            {
                CliPrintf (CliHandle,
                           "  Reverse Working Service Value    : %d, %d, ",
                           RvrWorkingService.
                           pu4_OidList[ELPS_TNL_INDEX_START_OFFSET],
                           RvrWorkingService.
                           pu4_OidList[ELPS_TNL_INST_START_OFFSET]);

                if (ElpsTnlGetTunnelAddrType (RvrWorkingService,
                                              pInMplsApiInfo,
                                              pOutMplsApiInfo) == OSIX_SUCCESS)
                {
                    if (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                        SrcNodeId.u4NodeType == ELPS_ADDR_TYPE_UCAST)
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->
                                                   OutTeTnlInfo.TnlLspId.
                                                   SrcNodeId.MplsRouterId.
                                                   u4_addr[0]);
                        CliPrintf (CliHandle, "%s, ", pu1TempStr);

                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->
                                                   OutTeTnlInfo.TnlLspId.
                                                   DstNodeId.MplsRouterId.
                                                   u4_addr[0]);
                        CliPrintf (CliHandle, "%s\r\n", pu1TempStr);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "(%d:%d), ",
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   SrcNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   SrcNodeId.MplsGlobalNodeId.u4NodeId);

                        CliPrintf (CliHandle, "(%d:%d)\r\n",
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   DstNodeId.MplsGlobalNodeId.u4NodeId);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "(%d:%d)\r\n",
                               RvrWorkingService.
                               pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET],
                               RvrWorkingService.
                               pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET]);
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "  Reverse Working Service Value    : 0\r\n");
            }
        }
        else if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW)
        {
            MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
            MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

            pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
            pInMplsApiInfo->u4ContextId = 0;
            pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
            pInMplsApiInfo->InPathId.PwId.u4PwIndex =
                WorkingService.pu4_OidList[ELPS_PW_INDEX_OFFSET];

            if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                                 pInMplsApiInfo,
                                                 pOutMplsApiInfo)
                == OSIX_FAILURE)
            {
                CliPrintf (CliHandle,
                           "  Working Service Value            : 0, 0\r\n");
                continue;

            }

            CliPrintf (CliHandle, "  Working Service Value            : ");

            CliPrintf (CliHandle, "%d, ",
                       pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId);

            if (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
                u4NodeType == MPLS_ADDR_TYPE_IPV4)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                           pOutMplsApiInfo->OutPwInfo.
                                           MplsPwPathId.DstNodeId.
                                           MplsRouterId.u4_addr[0]);
                CliPrintf (CliHandle, "%s\r\n", pu1TempStr);
            }
            else
            {
                CliPrintf (CliHandle, "(%d:%d)\r\n",
                           pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                           DstNodeId.MplsGlobalNodeId.u4GlobalId,
                           pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                           DstNodeId.MplsGlobalNodeId.u4NodeId);
            }
        }
    }
    while (nmhGetNextIndexFsElpsPgServiceListPointerTable
           (u4ContextId, &u4NextContextId, u4PgId, &u4NextPgId, u4ServiceListId,
            &u4NextServiceListId) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\n  Working MEG                      : %d\r\n",
               u4MegId);
    CliPrintf (CliHandle, "  Working ME                       : %d\r\n",
               u4MeId);
    CliPrintf (CliHandle, "  Working MEP                      : %d\r\n",
               u4MepId);

    if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        CliPrintf (CliHandle, "  Reverse Working MEG              : %d\r\n",
                   u4RevMegId);
        CliPrintf (CliHandle, "  Reverse Working ME               : %d\r\n",
                   u4RevMeId);
        CliPrintf (CliHandle, "  Reverse Working MEP              : %d\r\n",
                   u4RevMepId);
    }

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCliPrintProtectionSrvList
 *
 * DESCRIPTION      : This function prints the service list information    
 *                                                                           
 * INPUT            : CliHandle   - CliContext Information.                   
 *                    u4ContextId - Context Identifier. 
 *                    u4PgId      - Protection Group Identifier
 *
 * OUTPUT           : None 
 * 
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 * 
 **************************************************************************/
INT4
ElpsCliPrintProtectionSrvList (tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT4 u4PgId)
{
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4RvrServiceOid[ELPS_MAX_OID_LEN];
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4ServiceListId = 0;
    UINT4               u4NextServiceListId = 0;
    tSNMP_OID_TYPE      ProtectionService;
    tSNMP_OID_TYPE      RvrProtectionService;
    INT4                i4PgServiceType = 0;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    CHR1               *pu1TempStr = NULL;
    UINT4               u4MegId = 0;
    UINT4               u4MeId = 0;
    UINT4               u4MepId = 0;
    UINT4               u4RevMegId = 0;
    UINT4               u4RevMeId = 0;
    UINT4               u4RevMepId = 0;

    MEMSET (au4ServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));
    MEMSET (au4RvrServiceOid, 0, ELPS_MAX_OID_LEN * sizeof (UINT4));

    ProtectionService.u4_Length = 0;
    ProtectionService.pu4_OidList = au4ServiceOid;

    RvrProtectionService.u4_Length = 0;
    RvrProtectionService.pu4_OidList = au4RvrServiceOid;

    nmhGetFsElpsPgCfmProtectionMEG (u4ContextId, u4PgId, &u4MegId);
    nmhGetFsElpsPgCfmProtectionME (u4ContextId, u4PgId, &u4MeId);
    nmhGetFsElpsPgCfmProtectionMEP (u4ContextId, u4PgId, &u4MepId);

    nmhGetFsElpsPgCfmProtectionReverseMEG (u4ContextId, u4PgId, &u4RevMegId);
    nmhGetFsElpsPgCfmProtectionReverseME (u4ContextId, u4PgId, &u4RevMeId);
    nmhGetFsElpsPgCfmProtectionReverseMEP (u4ContextId, u4PgId, &u4RevMepId);

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        CliPrintf (CliHandle, "ElpsCliPrintProtectionSrvList: "
                   "Failed while allocating memory\r\n");
        return CLI_FAILURE;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        CliPrintf (CliHandle, "ElpsCliPrintProtectionSrvList: "
                   "Failed while allocating memory\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsElpsPgConfigServiceType (u4ContextId, u4PgId, &i4PgServiceType);
    if (nmhGetNextIndexFsElpsPgServiceListPointerTable (u4ContextId,
                                                        &u4NextContextId,
                                                        u4PgId, &u4NextPgId,
                                                        u4ServiceListId,
                                                        &u4NextServiceListId)
        == SNMP_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pOutMplsApiInfo);
        CliPrintf (CliHandle, "\r\n");
        CliPrintf (CliHandle, "Protection Service List         : 0\r\n");

        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            CliPrintf (CliHandle, "Reverse Protection Service List : 0\r\n");
        }

        CliPrintf (CliHandle, "  Protection MEG             : %d\r\n", u4MegId);
        CliPrintf (CliHandle, "  Protection ME              : %d\r\n", u4MeId);
        CliPrintf (CliHandle, "  Protection MEP             : %d\r\n", u4MepId);

        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            CliPrintf (CliHandle, "  Reverse Protection MEG     : %d\r\n",
                       u4RevMegId);
            CliPrintf (CliHandle, "  Reverse Protection ME      : %d\r\n",
                       u4RevMeId);
            CliPrintf (CliHandle, "  Reverse Protection MEP     : %d\r\n",
                       u4RevMepId);
        }
        return CLI_SUCCESS;
    }

    do
    {
        u4ServiceListId = u4NextServiceListId;
        if ((u4ContextId != u4NextContextId) || (u4PgId != u4NextPgId))
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pOutMplsApiInfo);
            return CLI_SUCCESS;
        }

        CliPrintf (CliHandle, "\n Context: %d     Pg Id: %d     "
                   "List Id: %d\r\n", u4ContextId, u4PgId, u4ServiceListId);

        nmhGetFsElpsPgProtectionServiceListPointer (u4ContextId, u4PgId,
                                                    u4ServiceListId,
                                                    &ProtectionService);
        if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            if (ProtectionService.u4_Length != ELPS_ERR_OID_LEN)
            {
                CliPrintf (CliHandle,
                           "  Protection Service Value         : %d, %d, ",
                           ProtectionService.
                           pu4_OidList[ELPS_TNL_INDEX_START_OFFSET],
                           ProtectionService.
                           pu4_OidList[ELPS_TNL_INST_START_OFFSET]);

                if (ElpsTnlGetTunnelAddrType (ProtectionService,
                                              pInMplsApiInfo,
                                              pOutMplsApiInfo) == OSIX_SUCCESS)
                {
                    if (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                        SrcNodeId.u4NodeType == ELPS_ADDR_TYPE_UCAST)
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->
                                                   OutTeTnlInfo.TnlLspId.
                                                   SrcNodeId.MplsRouterId.
                                                   u4_addr[0]);
                        CliPrintf (CliHandle, "%s, ", pu1TempStr);

                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->
                                                   OutTeTnlInfo.TnlLspId.
                                                   DstNodeId.MplsRouterId.
                                                   u4_addr[0]);
                        CliPrintf (CliHandle, "%s\r\n", pu1TempStr);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "(%d:%d), ",
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   SrcNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   SrcNodeId.MplsGlobalNodeId.u4NodeId);

                        CliPrintf (CliHandle, "(%d:%d)\r\n",
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   DstNodeId.MplsGlobalNodeId.u4NodeId);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "(%d:%d)\r\n",
                               ProtectionService.
                               pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET],
                               ProtectionService.
                               pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET]);
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "  Protection Service Value         : 0\r\n");
            }

            nmhGetFsElpsPgProtectionReverseServiceListPointer
                (u4ContextId, u4PgId, u4ServiceListId, &RvrProtectionService);

            if (RvrProtectionService.u4_Length != ELPS_ERR_OID_LEN)
            {
                CliPrintf (CliHandle,
                           "  Reverse Protection Service Value : %d, %d, ",
                           RvrProtectionService.
                           pu4_OidList[ELPS_TNL_INDEX_START_OFFSET],
                           RvrProtectionService.
                           pu4_OidList[ELPS_TNL_INST_START_OFFSET]);

                if (ElpsTnlGetTunnelAddrType (RvrProtectionService,
                                              pInMplsApiInfo,
                                              pOutMplsApiInfo) == OSIX_SUCCESS)
                {
                    if (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                        SrcNodeId.u4NodeType == ELPS_ADDR_TYPE_UCAST)
                    {
                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->
                                                   OutTeTnlInfo.TnlLspId.
                                                   SrcNodeId.MplsRouterId.
                                                   u4_addr[0]);
                        CliPrintf (CliHandle, "%s, ", pu1TempStr);

                        CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                                   pOutMplsApiInfo->
                                                   OutTeTnlInfo.TnlLspId.
                                                   DstNodeId.MplsRouterId.
                                                   u4_addr[0]);
                        CliPrintf (CliHandle, "%s\r\n", pu1TempStr);
                    }
                    else
                    {
                        CliPrintf (CliHandle, "(%d:%d), ",
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   SrcNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   SrcNodeId.MplsGlobalNodeId.u4NodeId);

                        CliPrintf (CliHandle, "(%d:%d)\r\n",
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   DstNodeId.MplsGlobalNodeId.u4GlobalId,
                                   pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                                   DstNodeId.MplsGlobalNodeId.u4NodeId);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "(%d:%d)\r\n",
                               RvrProtectionService.
                               pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET],
                               RvrProtectionService.
                               pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET]);
                }
            }
            else
            {
                CliPrintf (CliHandle,
                           "  Reverse Protection Service Value : 0\r\n");
            }
        }
        else if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_PW)
        {
            MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
            MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

            pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
            pInMplsApiInfo->u4ContextId = 0;
            pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
            pInMplsApiInfo->InPathId.PwId.u4PwIndex =
                ProtectionService.pu4_OidList[ELPS_PW_INDEX_OFFSET];

            if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                                 pInMplsApiInfo,
                                                 pOutMplsApiInfo)
                == OSIX_FAILURE)
            {
                CliPrintf (CliHandle,
                           "  Protection Service Value         : 0, 0\r\n");
                continue;
            }

            CliPrintf (CliHandle, "  Protection Service Value         : ");

            CliPrintf (CliHandle, "%d, ",
                       pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId);

            if (pOutMplsApiInfo->OutPwInfo.MplsPwPathId.DstNodeId.
                u4NodeType == MPLS_ADDR_TYPE_IPV4)
            {
                CLI_CONVERT_IPADDR_TO_STR (pu1TempStr,
                                           pOutMplsApiInfo->OutPwInfo.
                                           MplsPwPathId.DstNodeId.
                                           MplsRouterId.u4_addr[0]);
                CliPrintf (CliHandle, "%s\r\n", pu1TempStr);
            }
            else
            {
                CliPrintf (CliHandle, "(%d:%d)\r\n",
                           pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                           DstNodeId.MplsGlobalNodeId.u4GlobalId,
                           pOutMplsApiInfo->OutPwInfo.MplsPwPathId.
                           DstNodeId.MplsGlobalNodeId.u4NodeId);
            }
        }
    }
    while (nmhGetNextIndexFsElpsPgServiceListPointerTable
           (u4ContextId, &u4NextContextId, u4PgId, &u4NextPgId,
            u4ServiceListId, &u4NextServiceListId) == SNMP_SUCCESS);

    CliPrintf (CliHandle, "\n  Protection MEG                   : %d\r\n",
               u4MegId);
    CliPrintf (CliHandle, "  Protection ME                    : %d\r\n",
               u4MeId);
    CliPrintf (CliHandle, "  Protection MEP                   : %d\r\n",
               u4MepId);

    if (i4PgServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        CliPrintf (CliHandle, "  Reverse Protection MEG           : %d\r\n",
                   u4RevMegId);
        CliPrintf (CliHandle, "  Reverse Protection ME            : %d\r\n",
                   u4RevMeId);
        CliPrintf (CliHandle, "  Reverse Protection MEP           : %d\r\n",
                   u4RevMepId);
    }
    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetRapidTxTime                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the value of the Rapid          */
/*                        Transmission timer                                 */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                         u4RapidTxTime- Rapid Transmission Time            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliSetRapidTxTime (tCliHandle CliHandle, UINT4 u4RapidTxTime)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsRapidTxTime (&u4ErrorCode, u4RapidTxTime)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "Invalid Rapid Transmission Time is configured \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsRapidTxTime (u4RapidTxTime) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetChannelCode                              */
/*                                                                           */
/*     DESCRIPTION      : This function sets the value of the PSC Channel    */
/*                        Code carried in PSC packets                        */
/*                                                                           */
/*     INPUT            :  CliHandle-CLI Handler                             */
/*                         u4ChannelCode- PSC Channel Code                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliSetChannelCode (tCliHandle CliHandle, UINT4 u4ChannelCode)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPSCChannelCode (&u4ErrorCode, u4ChannelCode)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "Invalid Channel Code is Configured \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPSCChannelCode (u4ChannelCode) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetLspService                               */
/*                                                                           */
/*     DESCRIPTION      : This function sets the working service LSP values  */
/*                        for the Protection Group. Also this function will  */
/*                        take care of updating the protection group         */
/*                        configuration type as 'individual'.                */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4EntityType- Type of LSP (working/protected)     */
/*                         u4TnlIndex- MPLS Tunnel Index                     */
/*                         u4TnlInstance- MPLS Tunnel Instance               */
/*                         u4TnlIngressLsrId- Ingress LSR Identifier         */
/*                         u4TnlEgressLsrId- Egress LSR Identifier           */
/*                         u4RvrTnlIndex- Reverse path MPLS Tunnel Index     */
/*                         u4RvrTnlInstance- Reverse Path MPLS Tunnel        */
/*                                           Instance                        */
/*                         u4RvrTnlIngressLsrId- Reverse path Ingress LSR    */
/*                                               Identifier                  */
/*                         u4RvrTnlEgressLsrId- Reverse path Egress LSR      */
/*                                              Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliSetLspService (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT4 u4PgId, UINT4 u4EntityType,
                      UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                      UINT4 u4TnlIngressLsrId, UINT4 u4TnlEgressLsrId,
                      UINT4 u4RvrTnlIndex, UINT4 u4RvrTnlInstance,
                      UINT4 u4RvrTnlIngressLsrId, UINT4 u4RvrTnlEgressLsrId)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OID_TYPE      LspService;
    tSNMP_OID_TYPE      RvrLspService;
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4RvrServiceOid[ELPS_MAX_OID_LEN];

    MEMSET (au4ServiceOid, 0, sizeof (au4ServiceOid));
    MEMSET (au4RvrServiceOid, 0, sizeof (au4RvrServiceOid));

    LspService.u4_Length = 0;
    LspService.pu4_OidList = au4ServiceOid;

    RvrLspService.u4_Length = 0;
    RvrLspService.pu4_OidList = au4RvrServiceOid;

    LspService.u4_Length = ELPS_TNL_TABLE_DEF_OFFSET;
    MEMCPY (LspService.pu4_OidList, gau4TnlTableOid,
            (ELPS_TNL_TABLE_DEF_OFFSET * sizeof (UINT4)));

    /* Fill the Tunnel Index, instance, IngressIsrId, EgressLsrId */
    LspService.pu4_OidList[ELPS_TNL_INDEX_START_OFFSET] = u4TnlIndex;
    LspService.u4_Length++;

    LspService.pu4_OidList[ELPS_TNL_INST_START_OFFSET] = u4TnlInstance;
    LspService.u4_Length++;

    LspService.pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] = u4TnlIngressLsrId;
    LspService.u4_Length++;

    LspService.pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] = u4TnlEgressLsrId;
    LspService.u4_Length++;

    RvrLspService.u4_Length = ELPS_TNL_TABLE_DEF_OFFSET;
    MEMCPY (RvrLspService.pu4_OidList, gau4TnlTableOid,
            (ELPS_TNL_TABLE_DEF_OFFSET * sizeof (UINT4)));
    /* Fill the Tunnel Index, instance, IngressIsrId, EgressLsrId for 
     * reverse path */
    RvrLspService.pu4_OidList[ELPS_TNL_INDEX_START_OFFSET] = u4RvrTnlIndex;
    RvrLspService.u4_Length++;

    RvrLspService.pu4_OidList[ELPS_TNL_INST_START_OFFSET] = u4RvrTnlInstance;
    RvrLspService.u4_Length++;

    RvrLspService.pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] =
        u4RvrTnlIngressLsrId;
    RvrLspService.u4_Length++;

    RvrLspService.pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] =
        u4RvrTnlEgressLsrId;
    RvrLspService.u4_Length++;

    if (u4EntityType == ELPS_ENTITY_WORKING)
    {
        if (nmhTestv2FsElpsPgConfigType (&u4ErrorCode, u4ContextId, u4PgId,
                                         ELPS_PG_TYPE_INDIVIDUAL) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigType (u4ContextId, u4PgId,
                                      ELPS_PG_TYPE_INDIVIDUAL) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgConfigWorkingServicePointer
            (&u4ErrorCode, u4ContextId, u4PgId, &LspService) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (u4RvrTnlIngressLsrId != 0)
        {
            if (nmhTestv2FsElpsPgConfigWorkingReverseServicePointer
                (&u4ErrorCode, u4ContextId, u4PgId,
                 &RvrLspService) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }

        if (nmhSetFsElpsPgConfigWorkingServicePointer (u4ContextId,
                                                       u4PgId,
                                                       &LspService) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (u4RvrTnlIngressLsrId != 0)
        {
            if (nmhSetFsElpsPgConfigWorkingReverseServicePointer
                (u4ContextId, u4PgId, &RvrLspService) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (ElpsCliTestForConfigType (CliHandle, u4ContextId,
                                      u4PgId,
                                      ELPS_PG_TYPE_INDIVIDUAL) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (ElpsCliTestForWorkingInfo (u4ContextId, u4PgId, 0) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Configuration Failed. Working "
                       "information needs to be configured prior to "
                       "protection information.\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgConfigProtectionServicePointer (&u4ErrorCode,
                                                             u4ContextId,
                                                             u4PgId,
                                                             &LspService) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (u4RvrTnlIngressLsrId != 0)
        {
            if (nmhTestv2FsElpsPgConfigProtectionReverseServicePointer
                (&u4ErrorCode, u4ContextId, u4PgId,
                 &RvrLspService) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }

        if (nmhSetFsElpsPgConfigProtectionServicePointer (u4ContextId,
                                                          u4PgId,
                                                          &LspService) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (u4RvrTnlIngressLsrId != 0)
        {
            if (nmhSetFsElpsPgConfigProtectionReverseServicePointer
                (u4ContextId, u4PgId, &RvrLspService) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetLspList                                  */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Protection service LSP      */
/*                        values for the Protection Group. Also this function*/
/*                        will take care of updating the protection group    */
/*                        configuration type as 'list'.                      */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4ListId- Protection Group List Identifier        */
/*                         u4EntityType- Type of LSP (working/protected)     */
/*                         u4TnlIndex- MPLS Tunnel Index                     */
/*                         u4TnlInstance- MPLS Tunnel Instance               */
/*                         u4TnlIngressLsrId- Ingress LSR Identifier         */
/*                         u4TnlEgressLsrId- Egress LSR Identifier           */
/*                         u4RvrTnlIndex- Reverse path MPLS Tunnel Index     */
/*                         u4RvrTnlInstance- Reverse Path MPLS Tunnel        */
/*                                           Instance                        */
/*                         u4RvrTnlIngressLsrId- Reverse path Ingress LSR    */
/*                                               Identifier                  */
/*                         u4RvrTnlEgressLsrId- Reverse path Egress LSR      */
/*                                              Identifier                   */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliSetLspList (tCliHandle CliHandle, UINT4 u4ContextId,
                   UINT4 u4PgId, UINT4 u4ListId, UINT4 u4EntityType,
                   UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                   UINT4 u4TnlIngressLsrId, UINT4 u4TnlEgressLsrId,
                   UINT4 u4RvrTnlIndex, UINT4 u4RvrTnlInstance,
                   UINT4 u4RvrTnlIngressLsrId, UINT4 u4RvrTnlEgressLsrId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CfgType = 0;
    tSNMP_OID_TYPE      LspService, RvrLspService;
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];
    UINT4               au4RvrServiceOid[ELPS_MAX_OID_LEN];
    INT4                i4OldRowStatus;

    MEMSET (au4ServiceOid, 0, sizeof (au4ServiceOid));
    MEMSET (au4RvrServiceOid, 0, sizeof (au4RvrServiceOid));

    LspService.u4_Length = 0;
    LspService.pu4_OidList = au4ServiceOid;

    RvrLspService.u4_Length = 0;
    RvrLspService.pu4_OidList = au4RvrServiceOid;

    LspService.u4_Length = ELPS_TNL_TABLE_DEF_OFFSET;
    MEMCPY (LspService.pu4_OidList, gau4TnlTableOid,
            ELPS_TNL_TABLE_DEF_OFFSET * sizeof (UINT4));

    /* Fill the Tunnel Index, instance, IngressIsrId, EgressLsrId */
    LspService.pu4_OidList[ELPS_TNL_INDEX_START_OFFSET] = u4TnlIndex;
    LspService.u4_Length++;

    LspService.pu4_OidList[ELPS_TNL_INST_START_OFFSET] = u4TnlInstance;
    LspService.u4_Length++;

    LspService.pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] = u4TnlIngressLsrId;
    LspService.u4_Length++;
    LspService.pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] = u4TnlEgressLsrId;
    LspService.u4_Length++;

    RvrLspService.u4_Length = ELPS_TNL_TABLE_DEF_OFFSET;
    MEMCPY (RvrLspService.pu4_OidList, gau4TnlTableOid,
            ELPS_TNL_TABLE_DEF_OFFSET * sizeof (UINT4));

    /* Fill the Tunnel Index, instance, IngressIsrId, EgressLsrId 
     * for reverse path */
    RvrLspService.pu4_OidList[ELPS_TNL_INDEX_START_OFFSET] = u4RvrTnlIndex;
    RvrLspService.u4_Length++;

    RvrLspService.pu4_OidList[ELPS_TNL_INST_START_OFFSET] = u4RvrTnlInstance;
    RvrLspService.u4_Length++;

    RvrLspService.pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] =
        u4RvrTnlIngressLsrId;
    RvrLspService.u4_Length++;

    RvrLspService.pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] =
        u4RvrTnlEgressLsrId;
    RvrLspService.u4_Length++;
    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4CfgType);

    /* If ConfigType is NOT set as 'list' already, set it now */
    if (i4CfgType != ELPS_PG_TYPE_LIST)
    {
        if (nmhTestv2FsElpsPgConfigType (&u4ErrorCode, u4ContextId, u4PgId,
                                         ELPS_PG_TYPE_LIST) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigType (u4ContextId, u4PgId,
                                      ELPS_PG_TYPE_LIST) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checking for RowStatus */
    if (nmhGetFsElpsPgServiceListPointerRowStatus
        (u4ContextId, u4PgId, u4ListId, &i4OldRowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsElpsPgServiceListPointerRowStatus
            (&u4ErrorCode, u4ContextId, u4PgId, u4ListId,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgServiceListPointerRowStatus
            (u4ContextId, u4PgId, u4ListId, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsElpsPgServiceListPointerRowStatus
            (&u4ErrorCode, u4ContextId, u4PgId, u4ListId,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgServiceListPointerRowStatus
            (u4ContextId, u4PgId, u4ListId, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (u4EntityType == ELPS_ENTITY_WORKING)
    {
        if (nmhTestv2FsElpsPgWorkingServiceListPointer (&u4ErrorCode,
                                                        u4ContextId, u4PgId,
                                                        u4ListId,
                                                        &LspService) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (u4RvrTnlIngressLsrId != 0)
        {
            if (nmhTestv2FsElpsPgWorkingReverseServiceListPointer
                (&u4ErrorCode, u4ContextId, u4PgId, u4ListId,
                 &RvrLspService) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }

        if (nmhSetFsElpsPgWorkingServiceListPointer (u4ContextId, u4PgId,
                                                     u4ListId, &LspService)
            != SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (u4RvrTnlIngressLsrId != 0)
        {
            if (nmhSetFsElpsPgWorkingReverseServiceListPointer
                (u4ContextId, u4PgId, u4ListId, &RvrLspService) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }
    else
    {
        if (ElpsCliTestForConfigType (CliHandle, u4ContextId,
                                      u4PgId, ELPS_PG_TYPE_LIST) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (ElpsCliTestForWorkingInfo (u4ContextId, u4PgId, u4ListId)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Configuration Failed. Working "
                       "information needs to be configured prior to "
                       "protection information.\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgProtectionServiceListPointer (&u4ErrorCode,
                                                           u4ContextId, u4PgId,
                                                           u4ListId,
                                                           &LspService) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (u4RvrTnlIngressLsrId != 0)
        {
            if (nmhTestv2FsElpsPgProtectionReverseServiceListPointer
                (&u4ErrorCode, u4ContextId, u4PgId, u4ListId,
                 &RvrLspService) != SNMP_SUCCESS)
            {
                return CLI_FAILURE;
            }
        }

        if (nmhSetFsElpsPgProtectionServiceListPointer (u4ContextId, u4PgId,
                                                        u4ListId,
                                                        &LspService) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        if (u4RvrTnlIngressLsrId != 0)
        {
            if (nmhSetFsElpsPgProtectionReverseServiceListPointer
                (u4ContextId, u4PgId, u4ListId, &RvrLspService) != SNMP_SUCCESS)
            {
                CLI_FATAL_ERROR (CliHandle);
                return CLI_FAILURE;
            }
        }
    }

    if (nmhTestv2FsElpsPgServiceListPointerRowStatus
        (&u4ErrorCode, u4ContextId, u4PgId, u4ListId, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgServiceListPointerRowStatus
        (u4ContextId, u4PgId, u4ListId, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliDeleteList                                  */
/*                                                                           */
/*     DESCRIPTION      : This function deletes a PG Service List            */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Identifier                   */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4ListId- Protection Group List Identifier        */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliDeleteList (tCliHandle CliHandle, UINT4 u4ContextId,
                   UINT4 u4PgId, UINT4 u4ListId)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4ServiceListRowStatus = 0;

    if (nmhGetFsElpsPgServiceListPointerRowStatus (u4ContextId, u4PgId,
                                                   u4ListId,
                                                   &i4ServiceListRowStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Protection Group entry %d is not "
                   "present\n", u4PgId);
        return CLI_FAILURE;
    }
    if (nmhTestv2FsElpsPgServiceListPointerRowStatus (&u4ErrorCode, u4ContextId,
                                                      u4PgId, u4ListId,
                                                      DESTROY) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgServiceListPointerRowStatus (u4ContextId, u4PgId,
                                                   u4ListId,
                                                   DESTROY) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetPwService                                */
/*                                                                           */
/*     DESCRIPTION      : This function sets the working service PW values   */
/*                        for the Protection Group. Also this function will  */
/*                        take care of updating the protection group         */
/*                        configuration type as 'individual'.                */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4EntityType- Type of LSP (working/protected)     */
/*                         u4VcId - Pseudowire VC Identifier                 */
/*                         u4PeerAddr- Pseudowire Peer Address               */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliSetPwService (tCliHandle CliHandle, UINT4 u4ContextId,
                     UINT4 u4PgId, UINT4 u4EntityType,
                     UINT4 u4VcId, UINT4 u4PeerAddr, UINT4 u4AddressType)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OID_TYPE      PwService;
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];
    UINT4               u4PwIndex;

    MEMSET (au4ServiceOid, 0, sizeof (au4ServiceOid));

    PwService.u4_Length = 0;
    PwService.pu4_OidList = au4ServiceOid;

    if (nmhTestv2FsElpsPgConfigType (&u4ErrorCode, u4ContextId, u4PgId,
                                     ELPS_PG_TYPE_INDIVIDUAL) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigType (u4ContextId, u4PgId,
                                  ELPS_PG_TYPE_INDIVIDUAL) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (ElpsCliTestForConfigType (CliHandle, u4ContextId,
                                  u4PgId,
                                  ELPS_PG_TYPE_INDIVIDUAL) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    PwService.u4_Length = ELPS_PW_TABLE_DEF_OFFSET;
    MEMCPY (PwService.pu4_OidList, gau4PwTableOid,
            (ELPS_PW_TABLE_DEF_OFFSET * sizeof (UINT4)));

    if (ElpsPwGetPwIndex (u4VcId, u4PeerAddr, u4AddressType,
                          &u4PwIndex) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Fill the PwIndex */
    PwService.pu4_OidList[ELPS_PW_INDEX_OFFSET] = u4PwIndex;
    PwService.u4_Length++;

    if (u4EntityType == ELPS_ENTITY_WORKING)
    {
        if (nmhTestv2FsElpsPgConfigWorkingServicePointer
            (&u4ErrorCode, u4ContextId, u4PgId, &PwService) != SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigWorkingServicePointer (u4ContextId,
                                                       u4PgId,
                                                       &PwService) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (ElpsCliTestForWorkingInfo (u4ContextId, u4PgId, 0) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Configuration Failed. Working "
                       "information needs to be configured prior to "
                       "protection information.\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgConfigProtectionServicePointer (&u4ErrorCode,
                                                             u4ContextId,
                                                             u4PgId,
                                                             &PwService) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsElpsPgConfigProtectionServicePointer (u4ContextId,
                                                          u4PgId,
                                                          &PwService) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetPwList                                   */
/*                                                                           */
/*     DESCRIPTION      : This function sets the Protection service LSP      */
/*                        values for the Protection Group. Also this function*/
/*                        will take care of updating the protection group    */
/*                        configuration type as 'list'.                      */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4ListId- Protection Group List Identifier        */
/*                         u4EntityType- Type of LSP (working/protected)     */
/*                         u4VcId - Pseudowire VC Identifier                 */
/*                         u4PeerAddr - Pseudowire Peer Address              */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliSetPwList (tCliHandle CliHandle, UINT4 u4ContextId,
                  UINT4 u4PgId, UINT4 u4ListId, UINT4 u4EntityType,
                  UINT4 u4VcId, UINT4 u4PeerAddr, UINT4 u4AddressType)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4CfgType = 0;
    tSNMP_OID_TYPE      PwService;
    UINT4               au4ServiceOid[ELPS_MAX_OID_LEN];
    INT4                i4OldRowStatus;
    UINT4               u4PwIndex;

    MEMSET (au4ServiceOid, 0, sizeof (au4ServiceOid));

    PwService.u4_Length = 0;
    PwService.pu4_OidList = au4ServiceOid;

    nmhGetFsElpsPgConfigType (u4ContextId, u4PgId, &i4CfgType);

    /* If ConfigType is NOT set as 'list' already, set it now */
    if (i4CfgType != ELPS_PG_TYPE_LIST)
    {
        if (nmhTestv2FsElpsPgConfigType (&u4ErrorCode, u4ContextId, u4PgId,
                                         ELPS_PG_TYPE_LIST) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigType (u4ContextId, u4PgId,
                                      ELPS_PG_TYPE_LIST) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    /* Checking for RowStatus */
    if (nmhGetFsElpsPgServiceListPointerRowStatus
        (u4ContextId, u4PgId, u4ListId, &i4OldRowStatus) == SNMP_FAILURE)
    {
        if (nmhTestv2FsElpsPgServiceListPointerRowStatus
            (&u4ErrorCode, u4ContextId, u4PgId, u4ListId,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgServiceListPointerRowStatus
            (u4ContextId, u4PgId, u4ListId, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsElpsPgServiceListPointerRowStatus
            (&u4ErrorCode, u4ContextId, u4PgId, u4ListId,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgServiceListPointerRowStatus
            (u4ContextId, u4PgId, u4ListId, NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    PwService.u4_Length = ELPS_PW_TABLE_DEF_OFFSET;
    MEMCPY (PwService.pu4_OidList, gau4PwTableOid,
            (ELPS_PW_TABLE_DEF_OFFSET * sizeof (UINT4)));

    if (ElpsPwGetPwIndex (u4VcId, u4PeerAddr, u4AddressType,
                          &u4PwIndex) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Fill the PwIndex */
    PwService.pu4_OidList[ELPS_PW_INDEX_OFFSET] = u4PwIndex;
    PwService.u4_Length++;

    if (u4EntityType == ELPS_ENTITY_WORKING)
    {
        if (nmhTestv2FsElpsPgWorkingServiceListPointer (&u4ErrorCode,
                                                        u4ContextId, u4PgId,
                                                        u4ListId,
                                                        &PwService) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgWorkingServiceListPointer (u4ContextId, u4PgId,
                                                     u4ListId,
                                                     &PwService) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (ElpsCliTestForWorkingInfo (u4ContextId, u4PgId, u4ListId)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Configuration Failed. Working "
                       "information needs to be configured prior to "
                       "protection information.\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsElpsPgProtectionServiceListPointer (&u4ErrorCode,
                                                           u4ContextId, u4PgId,
                                                           u4ListId,
                                                           &PwService) !=
            SNMP_SUCCESS)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgProtectionServiceListPointer (u4ContextId, u4PgId,
                                                        u4ListId,
                                                        &PwService) !=
            SNMP_SUCCESS)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsElpsPgServiceListPointerRowStatus
        (&u4ErrorCode, u4ContextId, u4PgId, u4ListId, ACTIVE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgServiceListPointerRowStatus
        (u4ContextId, u4PgId, u4ListId, ACTIVE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetWorkingVlanInstanceToPg                  */
/*                                                                           */
/*     DESCRIPTION      : This function maps the working instance to PG.     */
/*                        When working in nomal mode working instance is     */
/*                        used to protect the list of services.              */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4GroupId- Instance ID                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliSetWorkingVlanInstanceToPg (tCliHandle CliHandle, UINT4 u4ContextId,
                                   UINT4 u4PgId, UINT4 u4GroupId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgConfigWorkingInstanceId
        (&u4ErrorCode, u4ContextId, u4PgId, u4GroupId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigWorkingInstanceId (u4ContextId, u4PgId,
                                               u4GroupId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetProtectionVlanInstanceToPg               */
/*                                                                           */
/*     DESCRIPTION      : This function maps the protection instance to PG.  */
/*                        When working in Protection mode protection instance*/
/*                        is used to protect the list of services.           */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4GroupId- Instance ID                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliSetProtectionVlanInstanceToPg (tCliHandle CliHandle, UINT4 u4ContextId,
                                      UINT4 u4PgId, UINT4 u4GroupId)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsElpsPgConfigProtectionInstanceId
        (&u4ErrorCode, u4ContextId, u4PgId, u4GroupId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigProtectionInstanceId (u4ContextId, u4PgId,
                                                  u4GroupId) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliUnmapWorkingVlanInstanceToPg                */
/*                                                                           */
/*     DESCRIPTION      : This function unmaps the working instance to PG.   */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4GroupId- Instance ID                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliUnmapWorkingVlanInstanceToPg (tCliHandle CliHandle, UINT4 u4ContextId,
                                     UINT4 u4PgId, UINT4 u4GroupId)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4CurrentGroupId = 0;

    if (nmhTestv2FsElpsPgConfigWorkingInstanceId
        (&u4ErrorCode, u4ContextId, u4PgId, u4GroupId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsElpsPgConfigWorkingInstanceId (u4ContextId, u4PgId,
                                               &u4CurrentGroupId) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* check if the Group/Instance exists */
    if (u4GroupId != u4CurrentGroupId)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigWorkingInstanceId (u4ContextId, u4PgId,
                                               ELPS_MIN_VLAN_GROUP_ID) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliUnmapProtectionVlanInstanceToPg             */
/*                                                                           */
/*     DESCRIPTION      : This function Unmaps the protection instance to PG */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         u4GroupId- Instance ID                            */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
INT4
ElpsCliUnmapProtectionVlanInstanceToPg (tCliHandle CliHandle, UINT4 u4ContextId,
                                        UINT4 u4PgId, UINT4 u4GroupId)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4CurrentGroupId = 0;

    if (nmhTestv2FsElpsPgConfigProtectionInstanceId
        (&u4ErrorCode, u4ContextId, u4PgId, u4GroupId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsElpsPgConfigProtectionInstanceId (u4ContextId, u4PgId,
                                                  &u4CurrentGroupId) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* check if the Group/Instance exists */
    if (u4GroupId != u4CurrentGroupId)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsElpsPgConfigProtectionInstanceId (u4ContextId, u4PgId,
                                                  ELPS_MIN_VLAN_GROUP_ID) ==
        SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetPgProtType                               */
/*                                                                           */
/*     DESCRIPTION      : This function Sets the protection type architecture*/
/*                        to PG.                                             */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         i4ProtType- Protection Type                       */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
ElpsCliSetPgProtType (tCliHandle CliHandle, UINT4 u4ContextId,
                      UINT4 u4PgId, INT4 i4ProtType)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4OldType = 0;

    if (nmhGetFsElpsPgConfigProtType (u4ContextId, u4PgId, &i4OldType)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (i4OldType != i4ProtType)
    {
        if (nmhTestv2FsElpsPgConfigProtType (&u4ErrorCode, u4ContextId, u4PgId,
                                             i4ProtType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgConfigProtType (u4ContextId, u4PgId, i4ProtType) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : ElpsCliSetPgPscVer                                 */
/*                                                                           */
/*     DESCRIPTION      : This function Sets the PSC Version type to PG.     */
/*                                                                           */
/*     INPUT            :  u4ContextId- Context Id                           */
/*                         u4PgId- Protection Group Identifier               */
/*                         i4PscVer- PSC Version                             */
/*                                                                           */
/*     OUTPUT           : NONE                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/

INT4
ElpsCliSetPgPscVer (tCliHandle CliHandle, UINT4 u4ContextId,
                    UINT4 u4PgId, UINT4 u4PscVer)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4OldVer = 0;

    if (nmhGetFsElpsPgPscVersion (u4ContextId, u4PgId, &u4OldVer)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (u4OldVer != u4PscVer)
    {
        if (nmhTestv2FsElpsPgPscVersion (&u4ErrorCode, u4ContextId, u4PgId,
                                         u4PscVer) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsElpsPgPscVersion (u4ContextId, u4PgId, u4PscVer) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}
#endif /* _ELPSCLI_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpscli.c                      */
/*-----------------------------------------------------------------------*/
