/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpscfm.c,v 1.15 2015/12/24 10:35:14 siva Exp $
 *
 * Description: This file contains the Cfm Config table related
 * datastructure implementation and utility functions.
 *****************************************************************************/
#ifndef _ELPSCFM_C_
#define _ELPSCFM_C_

#include "elpsinc.h"
#include "elpssem.h"

/* Proto types of the functions private to this file only */

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmCreateTable
 *
 * DESCRIPTION      : This function creates the Cfm configuration table
 *                    for a context.  
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                                   for which the Cfm config table needs
 *                                   to be created.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmCreateTable (tElpsContextInfo * pContextInfo)
{
    UINT4               u4RBNodeOffset = 0;

    u4RBNodeOffset = FSAP_OFFSETOF (tElpsCfmConfigInfo, CfmConfigRBNode);

    if ((pContextInfo->CfmConfigTbl =
         RBTreeCreateEmbedded (u4RBNodeOffset, ElpsCfmRBCmp)) == NULL)
    {
        ELPS_TRC ((pContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsCfmCreateTable: Creation of RBTree FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (pContextInfo->CfmConfigTbl);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmDeleteTable
 *
 * DESCRIPTION      : This function deletes the Cfm Config table.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                                   for which the Cfm config table needs
 *                                   to be deleted.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsCfmDeleteTable (tElpsContextInfo * pContextInfo)
{
    RBTreeDestroy (pContextInfo->CfmConfigTbl, ElpsCfmRBFree, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmRBCmp
 *
 * DESCRIPTION      : RBTree Compare function for Cfm Config table.
 *                    Indices of this table are -
 *                    o MegId
 *                    o MeId
 *                    o MepId
 *                    all these indices are represented through 
 *                    tElpsCfmMonitorInfo structure
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision 
 *                    pRBElem2 - Pointer to the node2 for comparision
 *                    
 * OUTPUT           : None
 *  
 * RETURNS          : ELPS_RB_EQUAL   - if all the keys matched for both
 *                                      the nodes
 *                    ELPS_RB_LESS    - if node pRBElem1's key is less than
 *                                      node pRBElem2's key.
 *                    ELPS_RB_GREATER - if node pRBElem1's key is greater
 *                                      than node pRBElem2's key.
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tElpsCfmConfigInfo *pCfmInfo1 = (tElpsCfmConfigInfo *) pRBElem1;
    tElpsCfmConfigInfo *pCfmInfo2 = (tElpsCfmConfigInfo *) pRBElem2;

    /* 1st Index comparison */
    if (pCfmInfo1->MonitorInfo.u4MegId > pCfmInfo2->MonitorInfo.u4MegId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pCfmInfo1->MonitorInfo.u4MegId < pCfmInfo2->MonitorInfo.u4MegId)
    {
        return ELPS_RB_LESS;
    }

    /* 2nd Index comparison */
    if (pCfmInfo1->MonitorInfo.u4MeId > pCfmInfo2->MonitorInfo.u4MeId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pCfmInfo1->MonitorInfo.u4MeId < pCfmInfo2->MonitorInfo.u4MeId)
    {
        return ELPS_RB_LESS;
    }

    /* 3rd Index comparison */
    if (pCfmInfo1->MonitorInfo.u4MepId > pCfmInfo2->MonitorInfo.u4MepId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pCfmInfo1->MonitorInfo.u4MepId < pCfmInfo2->MonitorInfo.u4MepId)
    {
        return ELPS_RB_LESS;
    }

    /* 4th Index comparison */
    if (pCfmInfo1->u1ServiceType > pCfmInfo2->u1ServiceType)
    {
        return ELPS_RB_GREATER;
    }
    else if (pCfmInfo1->u1ServiceType < pCfmInfo2->u1ServiceType)
    {
        return ELPS_RB_LESS;
    }

    /* 5th Index comparison */
    if (pCfmInfo1->u1ModId > pCfmInfo2->u1ModId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pCfmInfo1->u1ModId < pCfmInfo2->u1ModId)
    {
        return ELPS_RB_LESS;
    }

    return ELPS_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmRBFree
 *
 * DESCRIPTION      : This function clean the Cfm info structure before 
 *                    releasing the memory. this function is registered with
 *                    RBTree as a callback function for free the cfm config
 *                    node after deleting the memory from the RBTree.
 *
 * INPUT            : pRBElem - pointer to the Cfm config Node whose
 *                              memory needs to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    tElpsCfmConfigInfo *pCfmInfo = (tElpsCfmConfigInfo *) pRBElem;

    UNUSED_PARAM (u4Arg);

    /* Clear the link present in the PG info */
    switch (pCfmInfo->u1EntityType)
    {
        case ELPS_ENTITY_TYPE_WORKING:
            if (pCfmInfo->u1Direction == ELPS_SERVICE_DIRECTION_FWD)
            {
                pCfmInfo->pPgInfo->WorkingEntity.pCfmConfig = NULL;
            }
            else
            {
                pCfmInfo->pPgInfo->WorkingReverseEntity.pCfmConfig = NULL;
            }
            break;
        case ELPS_ENTITY_TYPE_PROTECTION:
            if (pCfmInfo->u1Direction == ELPS_SERVICE_DIRECTION_FWD)
            {
                pCfmInfo->pPgInfo->ProtectionEntity.pCfmConfig = NULL;
            }
            else
            {
                pCfmInfo->pPgInfo->ProtectionReverseEntity.pCfmConfig = NULL;
            }
            break;
        default:
            /* do nothing */
            break;
    }

    MemReleaseMemBlock (gElpsGlobalInfo.CfmConfigTablePoolId,
                        (UINT1 *) pCfmInfo);
    pCfmInfo = NULL;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmCreateNode
 *
 * DESCRIPTION      : This function creates the Cfm Config node by allocating
 *                    memory from the mempool (CfmConfigTablePoolId), and
 *                    initializes all the elements of the structure with 
 *                    default values.
 *                    This function does not add the node to the CfmConfigTbl.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsCfmConfigInfo * - Pointer to the Cfm config info
 *                                           structure is return. In case
 *                                           memory allocation failed NULL
 *                                           will be returned.
 * 
 **************************************************************************/
PUBLIC tElpsCfmConfigInfo *
ElpsCfmCreateNode (VOID)
{
    tElpsCfmConfigInfo *pCfmInfo = NULL;

    pCfmInfo = (tElpsCfmConfigInfo *)
        MemAllocMemBlk (gElpsGlobalInfo.CfmConfigTablePoolId);

    if (pCfmInfo == NULL)
    {
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC, "ElpsCfmCreateNode: Allocation "
                   "of memory for CFM Config Node FAILED !!!\r\n"));
        return NULL;
    }

    /* Initialize all the members with default values */
    MEMSET (pCfmInfo, 0, sizeof (tElpsCfmConfigInfo));

    return pCfmInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmAddNodeToCfmTable
 *
 * DESCRIPTION      : This function adds the Cfm Config node to the cfm
 *                    config info table. also update all the related links.
 *
 * INPUT            : pContextInfo - Pointer to the Context information Node
 *                    pCfmInfo     - Pointer to the Cfm Node that needs to be 
 *                                   added in the cfm config table.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmAddNodeToCfmTable (tElpsContextInfo * pContextInfo,
                          tElpsPgInfo * pPgInfo, tElpsCfmConfigInfo * pCfmInfo)
{
    pCfmInfo->pPgInfo = pPgInfo;

    if (RBTreeAdd (pContextInfo->CfmConfigTbl, (tRBElem *) pCfmInfo)
        != RB_SUCCESS)
    {
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsCfmAddNodeToCfmTable: Failed to add node to Cfm "
                   "Config Table for this PG %d\r\n", pPgInfo->u4PgId));
        return OSIX_FAILURE;
    }

    /* add the entity node link present in the PG info */
    switch (pCfmInfo->u1EntityType)
    {
        case ELPS_ENTITY_TYPE_WORKING:
            if (pCfmInfo->u1Direction == ELPS_SERVICE_DIRECTION_FWD)
            {
                pCfmInfo->pPgInfo->WorkingEntity.pCfmConfig = pCfmInfo;
            }
            else
            {
                pCfmInfo->pPgInfo->WorkingReverseEntity.pCfmConfig = pCfmInfo;
            }
            break;

        case ELPS_ENTITY_TYPE_PROTECTION:
            if (pCfmInfo->u1Direction == ELPS_SERVICE_DIRECTION_FWD)
            {
                pCfmInfo->pPgInfo->ProtectionEntity.pCfmConfig = pCfmInfo;
            }
            else
            {
                pCfmInfo->pPgInfo->ProtectionReverseEntity.pCfmConfig =
                    pCfmInfo;
            }
            break;

        default:
            /* do nothing */
            ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                       "ElpsCfmAddNodeToCfmTable: Invalid entity "
                       "type %d\r\n", pCfmInfo->u1EntityType));

            RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *) pCfmInfo);

            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmDelNode
 *
 * DESCRIPTION      : This function remove the cfm node from the Cfm Config 
 *                    Table and then release the memory to the mempool also.
 *
 * INPUT            : pContextInfo - Pointer to the Context information Node
 *                    pCfmInfo     - Pointer to the Cfm info that needs to be 
 *                                   deleted from the cfm config table.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmDelNode (tElpsContextInfo * pContextInfo, tElpsCfmConfigInfo * pCfmInfo)
{
    /* PG Cfm Entry is added into PG Cfm RBTree only if the Cfm Row Status 
     * is Active.
     */
    if ((pCfmInfo->pPgInfo->u1CfmRowStatus == ACTIVE) &&
        (pCfmInfo->MonitorInfo.u4MegId != 0))
    {
        RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *) pCfmInfo);
    }

    ElpsCfmRBFree ((tRBElem *) pCfmInfo, 0);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmGetNode
 *
 * DESCRIPTION      : This function returns the Cfm Config info node based
 *                    on the provided Cfm Monitor Id (i.e MegId, MeId and
 *                    MepId)
 *                    
 * INPUT            : pContextInfo - Pointer to the Context information Node
 *                    pMonitorInfo - pointer to the Monitor Id information.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsCfmConfigInfo* - Pointer to the intended Cfm 
 *                                          config info node is returned.
 *                                          If node for the given ids is not
 *                                          present in the cfm config table
 *                                          NULL will returned.
 * 
 * 
 **************************************************************************/
PUBLIC tElpsCfmConfigInfo *
ElpsCfmGetNode (tElpsContextInfo * pContextInfo,
                tElpsCfmConfigInfo * pMonitorInfo)
{
    tElpsCfmConfigInfo *pCfmInfo = NULL;
    tElpsCfmConfigInfo  TmpCfmInfo;

    MEMCPY (&TmpCfmInfo, pMonitorInfo, sizeof (tElpsCfmConfigInfo));

    pCfmInfo = (tElpsCfmConfigInfo *)
        RBTreeGet (pContextInfo->CfmConfigTbl, (tRBElem *) & TmpCfmInfo);

    return pCfmInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmGetPgInfo
 *
 * DESCRIPTION      : This function search the cfm config info based on the 
 *                    Monitor Id (tElpsCfmMonitorInfo) and returns the 
 *                    associated protection group information.
 *
 * INPUT            : pContextInfo - Pointer to the Context information Node
 *                    pMonitorInfo - pointer to the Monitor Id information.
 *
 * OUTPUT           : None
 * 
 * 
 * RETURNS          : tElpsPgInfo* - Associated protection group information.
 *                                   If entry not present, NULL will be 
 *                                   returned.
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsCfmGetPgInfo (tElpsContextInfo * pContextInfo,
                  tElpsCfmConfigInfo * pMonitorInfo)
{
    tElpsCfmConfigInfo *pCfmInfo = NULL;

    pCfmInfo = ElpsCfmGetNode (pContextInfo, pMonitorInfo);

    if (pCfmInfo != NULL)
    {
        return (pCfmInfo->pPgInfo);
    }

    return NULL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmProcessReceivedSignal
 *
 * DESCRIPTION      : This function process the received signal from the 
 *                    ECFM module. 
 *
 * INPUT            : pCfmInfo  - Pointer to the Cfm Config entry
 *                    u4Signal  - ELPS_CFM_SIGNAL_FAIL/ELPS_CFM_SIGNAL_OK
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsCfmProcessReceivedSignal (tElpsCfmConfigInfo * pCfmInfo, UINT4 u4Signal)
{
    UINT4               u4RemainingTime = 0;
    tElpsHwPgSwitchInfo HwPgSwitchInfo;

    MEMSET (&HwPgSwitchInfo, 0, sizeof (tElpsHwPgSwitchInfo));

    if (pCfmInfo->u1EntityType == ELPS_ENTITY_TYPE_PROTECTION)
    {
        if ((u4Signal == ELPS_CFM_SIGNAL_FAIL) ||
            (u4Signal == ELPS_CFM_SIGNAL_AIS))
        {
            if (pCfmInfo->pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {
                if (pCfmInfo->pPgInfo->u2HoldOffTime != 0)
                {
                    /* HoldOff timer for protection entity is configured.
                     * So start timer and update the protection
                     * Cfm Status variable
                     * to Hold_Off. */
                    pCfmInfo->pPgInfo->u1ProtectionCfmStatus =
                        ELPS_CFM_SIGNAL_FAIL_HOLD_OFF;
                    ElpsTmrStartTimer (pCfmInfo->pPgInfo,
                                       pCfmInfo->pPgInfo->u2HoldOffTime,
                                       ELPS_HOLD_OFF_TMR_P);
                }
                else
                {
                    pCfmInfo->pPgInfo->u1ProtectionCfmStatus =
                        ELPS_CFM_SIGNAL_FAIL;
                    ElpsCfmApplyReceivedSignalToSEM (pCfmInfo->pPgInfo,
                                                     ELPS_LOCAL_COND_SF_P);
                }
            }
            else if ((pCfmInfo->pPgInfo->u1ServiceType ==
                      ELPS_PG_SERVICE_TYPE_LSP) ||
                     (pCfmInfo->pPgInfo->u1ServiceType ==
                      ELPS_PG_SERVICE_TYPE_PW))
            {
                if (pCfmInfo->pPgInfo->u1ServiceType ==
                    ELPS_PG_SERVICE_TYPE_LSP)
                {
                    /* In case of LSP services, if the Protection Cfm Status 
                     * is already in ELPS_CFM_SIGNAL_FAIL, just update the CFM 
                     * status and return. This is because already the FWD/RVR 
                     * path of LSP has defined a ELPS_CFM_SIGNAL_FAIL, 
                     * and necessary steps taken */
                    if (pCfmInfo->pPgInfo->u1ProtectionCfmStatus ==
                        ELPS_CFM_SIGNAL_FAIL)
                    {
                        pCfmInfo->u1Status = ELPS_CFM_SIGNAL_FAIL;
                        return;
                    }
                }
                /* In case of LSP and PW services case, the holdoff timer will 
                 * be started if the received is ELPS_CFM_SIGNAL_AIS */
                if ((pCfmInfo->pPgInfo->u2HoldOffTime != 0) &&
                    (u4Signal == ELPS_CFM_SIGNAL_AIS))
                {
                    /* HoldOff timer for protection entity is configured.
                     * So start timer and update the protection
                     * Cfm Status variable
                     * to Hold_Off. */
                    pCfmInfo->pPgInfo->u1ProtectionCfmStatus =
                        ELPS_CFM_SIGNAL_FAIL_HOLD_OFF;
                    pCfmInfo->u1Status = ELPS_CFM_SIGNAL_FAIL_HOLD_OFF;
                    ElpsTmrStartTimer (pCfmInfo->pPgInfo,
                                       pCfmInfo->pPgInfo->u2HoldOffTime,
                                       ELPS_HOLD_OFF_TMR_P);
                }
                else
                {
                    pCfmInfo->pPgInfo->u1ProtectionCfmStatus =
                        ELPS_CFM_SIGNAL_FAIL;
                    pCfmInfo->u1Status = ELPS_CFM_SIGNAL_FAIL;
                    ElpsCfmApplyReceivedSignalToSEM (pCfmInfo->pPgInfo,
                                                     ELPS_LOCAL_COND_SF_P);
                }
            }

        }
        else if (u4Signal == ELPS_CFM_SIGNAL_OK)
            /* ELPS_LOCAL_COND_PROT_REC_SF */
        {
            /* In case of LSP services, if the ELPS_CFM_SIGNAL_OK is received, 
             * need to check whether reverse/working service is also in 
             * ELPS_CFM_SIGNAL_OK state.if not just update the CFM status
             * and return*/

            if (pCfmInfo->pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
            {
                if (pCfmInfo->pPgInfo->u1PgConfigType == ELPS_PG_TYPE_LIST)
                {
                    ElpsUtilFillHwSwitchInfo (pCfmInfo->pPgInfo->pContextInfo,
                                              &HwPgSwitchInfo,
                                              &(pCfmInfo->pPgInfo->
                                                unServiceList.
                                                pWorkingServiceListPointer->
                                                WorkingInfo),
                                              &(pCfmInfo->pPgInfo->
                                                unServiceList.
                                                pWorkingServiceListPointer->
                                                ProtectionInfo),
                                              &(pCfmInfo->pPgInfo->
                                                unServiceList.
                                                pWorkingServiceListPointer->
                                                WorkingRvrInfo),
                                              &(pCfmInfo->pPgInfo->
                                                unServiceList.
                                                pWorkingServiceListPointer->
                                                ProtectionRvrInfo),
                                              pCfmInfo->pPgInfo->u1ServiceType);
                }
                else
                {
                    ElpsUtilFillHwSwitchInfo (pCfmInfo->pPgInfo->pContextInfo,
                                              &HwPgSwitchInfo,
                                              &(pCfmInfo->pPgInfo->
                                                WorkingEntity.ServiceParam),
                                              &(pCfmInfo->pPgInfo->
                                                ProtectionEntity.ServiceParam),
                                              &(pCfmInfo->pPgInfo->
                                                WorkingReverseEntity.
                                                ServiceParam),
                                              &(pCfmInfo->pPgInfo->
                                                ProtectionReverseEntity.
                                                ServiceParam),
                                              pCfmInfo->pPgInfo->u1ServiceType);
                }

                if ((pCfmInfo->u1Direction == ELPS_SERVICE_DIRECTION_FWD) &&
                    (pCfmInfo->pPgInfo->ProtectionReverseEntity.
                     pCfmConfig->MonitorInfo.u4MegId != 0))
                {
                    if (pCfmInfo->pPgInfo->ProtectionReverseEntity.
                        pCfmConfig->u1Status == ELPS_CFM_SIGNAL_FAIL)
                    {
                        pCfmInfo->u1Status = ELPS_CFM_SIGNAL_OK;
                        return;
                    }
                }
                else if (pCfmInfo->u1Direction == ELPS_SERVICE_DIRECTION_RVR)
                {
                    if (pCfmInfo->pPgInfo->ProtectionEntity.
                        pCfmConfig->u1Status == ELPS_CFM_SIGNAL_FAIL)
                    {
                        pCfmInfo->u1Status = ELPS_CFM_SIGNAL_OK;
                        return;
                    }
                }
                pCfmInfo->u1Status = ELPS_CFM_SIGNAL_OK;
            }

            /* HoldOff timer configured. So get the timer state and
             * update the variable/apply to the SEM accordingly. */
            pCfmInfo->pPgInfo->u1ProtectionCfmStatus = ELPS_CFM_SIGNAL_OK;

            TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                                 &(pCfmInfo->pPgInfo->ProtectionHoldOffTimer.
                                   TimerNode), &u4RemainingTime);

            if (u4RemainingTime == 0)
            {
                ElpsCfmApplyReceivedSignalToSEM
                    (pCfmInfo->pPgInfo, ELPS_LOCAL_COND_PROT_REC_SF);
            }
            else
            {
                /* When Hold-Off timer is running, the local conditions are
                 * not applied to SEM, hence the local condition of Working
                 * path recovers from SF is synchronized here.
                 */
                ElpsRedSendDynamicSyncMsg (pCfmInfo->pPgInfo,
                                           ELPS_MAX_TMR_TYPES, 0);
            }
        }
    }
    else
    {                            /* Working entity */
        if ((u4Signal == ELPS_CFM_SIGNAL_FAIL) ||
            (u4Signal == ELPS_CFM_SIGNAL_AIS))
        {
            if (pCfmInfo->pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {
                if (pCfmInfo->pPgInfo->u2HoldOffTime != 0)
                {
                    /* HoldOff timer configured. So start timer and update the
                     * Working Cfm Status variable to Hold_Off. */

                    pCfmInfo->pPgInfo->u1WorkingCfmStatus =
                        ELPS_CFM_SIGNAL_FAIL_HOLD_OFF;

                    ElpsTmrStartTimer (pCfmInfo->pPgInfo,
                                       pCfmInfo->pPgInfo->u2HoldOffTime,
                                       ELPS_HOLD_OFF_TMR);
                }
                else
                {
                    pCfmInfo->pPgInfo->u1WorkingCfmStatus =
                        ELPS_CFM_SIGNAL_FAIL;
                    ElpsCfmApplyReceivedSignalToSEM (pCfmInfo->pPgInfo,
                                                     ELPS_LOCAL_COND_SF_W);
                }
            }
            else if ((pCfmInfo->pPgInfo->u1ServiceType ==
                      ELPS_PG_SERVICE_TYPE_LSP) ||
                     (pCfmInfo->pPgInfo->u1ServiceType ==
                      ELPS_PG_SERVICE_TYPE_PW))
            {
                if (pCfmInfo->pPgInfo->u1ServiceType ==
                    ELPS_PG_SERVICE_TYPE_LSP)
                {
                    /* In case of LSP services, if the Protection Cfm Status 
                     * is already in ELPS_CFM_SIGNAL_FAIL, just update the CFM 
                     * status and return. This is because already the FWD/RVR 
                     * path of LSP has defined a ELPS_CFM_SIGNAL_FAIL, 
                     * and necessary steps taken */
                    if (pCfmInfo->pPgInfo->u1WorkingCfmStatus ==
                        ELPS_CFM_SIGNAL_FAIL)
                    {
                        pCfmInfo->u1Status = ELPS_CFM_SIGNAL_FAIL;
                        return;
                    }
                }

                /* In case of LSP and PW services case, the holdoff timer will 
                 * be started if the received is ELPS_CFM_SIGNAL_AIS */
                if ((pCfmInfo->pPgInfo->u2HoldOffTime != 0) &&
                    (u4Signal == ELPS_CFM_SIGNAL_AIS))
                {
                    /* HoldOff timer configured. So start timer and update the
                     * Working Cfm Status variable to Hold_Off. */
                    pCfmInfo->pPgInfo->u1WorkingCfmStatus =
                        ELPS_CFM_SIGNAL_FAIL_HOLD_OFF;
                    pCfmInfo->u1Status = ELPS_CFM_SIGNAL_FAIL_HOLD_OFF;
                    ElpsTmrStartTimer (pCfmInfo->pPgInfo,
                                       pCfmInfo->pPgInfo->u2HoldOffTime,
                                       ELPS_HOLD_OFF_TMR);
                }
                else
                {
                    pCfmInfo->pPgInfo->u1WorkingCfmStatus =
                        ELPS_CFM_SIGNAL_FAIL;
                    pCfmInfo->u1Status = ELPS_CFM_SIGNAL_FAIL;
                    ElpsCfmApplyReceivedSignalToSEM (pCfmInfo->pPgInfo,
                                                     ELPS_LOCAL_COND_SF_W);
                }
            }

        }
        else if (u4Signal == ELPS_CFM_SIGNAL_OK)
        {
            /* In case of LSP services, if the ELPS_CFM_SIGNAL_OK is received, 
             * need to check whether reverse/working service is also in 
             * ELPS_CFM_SIGNAL_OK state.if not just update the CFM status
             * and return*/

            if (pCfmInfo->pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
            {
                if (pCfmInfo->pPgInfo->u1PgConfigType == ELPS_PG_TYPE_LIST)
                {
                    ElpsUtilFillHwSwitchInfo (pCfmInfo->pPgInfo->pContextInfo,
                                              &HwPgSwitchInfo,
                                              &(pCfmInfo->pPgInfo->
                                                unServiceList.
                                                pWorkingServiceListPointer->
                                                WorkingInfo),
                                              &(pCfmInfo->pPgInfo->
                                                unServiceList.
                                                pWorkingServiceListPointer->
                                                ProtectionInfo),
                                              &(pCfmInfo->pPgInfo->
                                                unServiceList.
                                                pWorkingServiceListPointer->
                                                WorkingRvrInfo),
                                              &(pCfmInfo->pPgInfo->
                                                unServiceList.
                                                pWorkingServiceListPointer->
                                                ProtectionRvrInfo),
                                              pCfmInfo->pPgInfo->u1ServiceType);
                }
                else
                {
                    ElpsUtilFillHwSwitchInfo (pCfmInfo->pPgInfo->pContextInfo,
                                              &HwPgSwitchInfo,
                                              &(pCfmInfo->pPgInfo->
                                                WorkingEntity.ServiceParam),
                                              &(pCfmInfo->pPgInfo->
                                                ProtectionEntity.ServiceParam),
                                              &(pCfmInfo->pPgInfo->
                                                WorkingReverseEntity.
                                                ServiceParam),
                                              &(pCfmInfo->pPgInfo->
                                                ProtectionReverseEntity.
                                                ServiceParam),
                                              pCfmInfo->pPgInfo->u1ServiceType);
                }
                if ((pCfmInfo->u1Direction == ELPS_SERVICE_DIRECTION_FWD) &&
                    (pCfmInfo->pPgInfo->WorkingReverseEntity.
                     pCfmConfig->MonitorInfo.u4MegId != 0))
                {
                    if (pCfmInfo->pPgInfo->WorkingReverseEntity.
                        pCfmConfig->u1Status == ELPS_CFM_SIGNAL_FAIL)
                    {
                        pCfmInfo->u1Status = ELPS_CFM_SIGNAL_OK;
                        return;
                    }
                }
                else if (pCfmInfo->u1Direction == ELPS_SERVICE_DIRECTION_RVR)
                {
                    if (pCfmInfo->pPgInfo->WorkingEntity.
                        pCfmConfig->u1Status == ELPS_CFM_SIGNAL_FAIL)
                    {
                        pCfmInfo->u1Status = ELPS_CFM_SIGNAL_OK;
                        return;
                    }
                }
                pCfmInfo->u1Status = ELPS_CFM_SIGNAL_OK;
            }

            /* HoldOff timer configured. So get the timer state and
               update the variable/apply to the SEM accordingly. */
            pCfmInfo->pPgInfo->u1WorkingCfmStatus = ELPS_CFM_SIGNAL_OK;

            TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                                 &(pCfmInfo->pPgInfo->HoldOffTimer.TimerNode),
                                 &u4RemainingTime);

            if (u4RemainingTime == 0)
            {
                ElpsCfmApplyReceivedSignalToSEM
                    (pCfmInfo->pPgInfo, ELPS_LOCAL_COND_WRK_REC_SF);
            }
            else
            {
                /* When Hold-Off timer is running, the local conditions are
                 * not applied to SEM, hence the local condition of Working
                 * path recovers from SF is synchronized here.
                 */
                ElpsRedSendDynamicSyncMsg (pCfmInfo->pPgInfo,
                                           ELPS_MAX_TMR_TYPES, 0);
            }
        }
    }
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmApplyReceivedSignalToSEM
 *
 * DESCRIPTION      : This function applies the received signal to the SEM 
 *                    and updates the corresponding variables.
 *
 * INPUT            : pPgInfo  - Pointer to the PG entry
 *                    u1Event  - ELPS_EV_LR_PROT_SF/ 
 *                               ELPS_EV_LR_WRK_SF/ 
 *                               ELPS_EV_LR_PROT_RECOV_SF/ 
 *                               ELPS_EV_LR_WRK_RECOV_SF
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsCfmApplyReceivedSignalToSEM (tElpsPgInfo * pPgInfo, UINT1 u1Event)
{
    UINT1               u1RetVal = 0;
    UINT1               u1Action = 0;

    /* Update the last received request type */
    pPgInfo->u1LastRecvdReqType = ELPS_LOCAL_CONDITION;

    /* Update the Local Indication Command variable present in 
     * the PgInfo structure */
    pPgInfo->LastLocalCond.u1Request = u1Event;
    /* Get the corresponding event from SEM and Apply to SEM */
    u1Event = ElpsSemGetEventIdForReq (ELPS_LOCAL_CONDITION, u1Event);

    /* performance measurement */
    /* The local node received APS-SF message. 
     * This time is noted as "T0".
     */
    if ((u1Event == ELPS_EV_LR_WRK_SF) &&
        (pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_NR_W))
    {
        ElpsUtilMeasureTime (pPgInfo, ELPS_PERF_LR_SF_RX);
    }

    /* Command acceptance check */
    if (ElpsSemValidateLocalRequest (pPgInfo, u1Event) == OSIX_FAILURE)
    {
        if ((u1Event == ELPS_EV_LR_WRK_RECOV_SF) ||
            (u1Event == ELPS_EV_LR_PROT_RECOV_SF))
        {
            pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_REJECTED;
        }
        else
        {
            pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_OVERRULED;
        }
        return;
    }
    /* Global priority check */
    u1Action = ElpsSemApplyGlobalPriorityLogic (pPgInfo, &u1Event);

    /* If the working/protection ports are defined as port-channel, then
     * we need to ensure that the port state of its member ports are properly
     * programmed in hardware after MSR. Generally there will be delay in 
     * adding member ports to a port-channel in hardware after MSR; So whenever
     * DEFECT conditon cleared event is received and LOCKOUT is enabled, we
     * need to explicitly program the port state of member ports in hardware.
     * When a PG is in LOCKEDOUT state, the working port should be in FORWARDING
     * state & protection port should be in BLOCKED state.*/
    if ((pPgInfo->WorkingEntity.u4PortId != pPgInfo->ProtectionEntity.u4PortId)
            && (pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_LO_W)
            && (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN))
    {
        if (u1Event == ELPS_EV_LR_WRK_RECOV_SF)
        {
            if ((pPgInfo->WorkingEntity.u4PortId > BRG_MAX_PHY_PORTS) &&
                (pPgInfo->WorkingEntity.u4PortId <= BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                /* Working Port is a port-channel */
                if (ElpsPgSwitchDataPath(pPgInfo->pContextInfo, pPgInfo,
                                    ELPS_PG_LO_WO_RECOVERY) == OSIX_FAILURE)
                {
                    ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                        "ElpsCfmApplyReceivedSignalToSEM: Failed to program"
                        " the port-state of working port-channel when the "
                        "defect condition is cleared & LO enabled\r\n"));
                }
            }
        }
        else if (u1Event == ELPS_EV_LR_PROT_RECOV_SF)
        {
            if ((pPgInfo->ProtectionEntity.u4PortId > BRG_MAX_PHY_PORTS) &&
                (pPgInfo->ProtectionEntity.u4PortId <= BRG_MAX_PHY_PLUS_LOG_PORTS))
            {
                /* Protection Port is a port-channel */
                if (ElpsPgSwitchDataPath(pPgInfo->pContextInfo, pPgInfo,
                                    ELPS_PG_LO_PRO_RECOVERY) == OSIX_FAILURE)
                {
                    ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                        "ElpsCfmApplyReceivedSignalToSEM: Failed to program"
                        " the port-state of protection port-channel when the"
                        " defect condition is cleared & LO enabled\r\n"));
                }
            }
        }
    }

    u1RetVal = ElpsSemApplyEvent (pPgInfo, u1Event, u1Action);

    /* Update the Local Indication Command status present in 
     * the PgInfo structure */
    pPgInfo->LastLocalCond.u1RequestStatus = u1RetVal;

    if (u1RetVal == ELPS_CMD_ACCEPTED)
    {
        /* Update the local command status */
        ElpsSemModifyLocalCommandStatus (pPgInfo, u1Event);
    }
    /* Send Dynamic Sync-up having the SEM Information. All the local 
     * request events applied to the SEM is synchronized here. 
     */
    ElpsRedSendDynamicSyncMsg (pPgInfo, ELPS_MAX_TMR_TYPES, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmDeActivate
 *
 * DESCRIPTION      : This function de-activates the CFM entry in protection 
 *                    group. This function is indended to be called only form 
 *                    the management interfaces (low level routine). So it is 
 *                    expeceted here that,all the dependencies are resolved
 *                    in the test routine before calling this function.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                    pPgInfo      - Pointer to the protection group info
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmDeActivate (tElpsContextInfo * pContextInfo, tElpsPgInfo * pPgInfo)
{
    if (RBTreeRemove (pContextInfo->CfmConfigTbl,
                      (tRBElem *) pPgInfo->ProtectionEntity.pCfmConfig)
        == RB_FAILURE)
    {
        ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                   "ElpsCfmDeActivate: Failed to delete the Protection "
                   "Cfm entry (for PG-%d) from the RBTree\r\n",
                   pPgInfo->u4PgId));
        return OSIX_FAILURE;
    }

    if (RBTreeRemove (pContextInfo->CfmConfigTbl,
                      (tRBElem *) pPgInfo->WorkingEntity.pCfmConfig)
        == RB_FAILURE)
    {
        ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                   "ElpsCfmDeActivate: Failed to delete the Working "
                   "Cfm entry (for PG-%d) from the RBTree\r\n",
                   pPgInfo->u4PgId));
        return OSIX_FAILURE;
    }

    if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        if (RBTreeRemove (pContextInfo->CfmConfigTbl,
                          (tRBElem *) pPgInfo->WorkingReverseEntity.pCfmConfig)
            == RB_FAILURE)
        {
            ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                       "ElpsCfmDeActivate: Failed to delete the Working "
                       "Reverse Cfm entry (for PG-%d) from the RBTree\r\n",
                       pPgInfo->u4PgId));
            return OSIX_FAILURE;
        }
    }

    if (pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        if (RBTreeRemove (pContextInfo->CfmConfigTbl,
                          (tRBElem *) pPgInfo->ProtectionReverseEntity.
                          pCfmConfig) == RB_FAILURE)
        {
            ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                       "ElpsCfmDeActivate: Failed to delete the Protection "
                       "Reverse Cfm entry (for PG-%d) from the RBTree\r\n",
                       pPgInfo->u4PgId));
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmActivate
 *
 * DESCRIPTION      : This function activates the CFM entry in protection group.
 *                    This function is indended to be called only form the 
 *                    management interfaces (low level routine). So it is 
 *                    expeceted here that,all the dependencies are resolved
 *                    in the test routine before calling this function.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                    pPgInfo      - Pointer to the protection group info
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmActivate (tElpsContextInfo * pContextInfo, tElpsPgInfo * pPgInfo)
{
    tElpsCfmConfigInfo *pUsedCfm = NULL;
    tElpsCfmConfigInfo  CfmInfo;
    MEMSET (&CfmInfo, 0, sizeof (tElpsCfmConfigInfo));
    
    /* Add the Working Entity to the RBTree */
    if (ElpsCfmAddNodeToCfmTable (pContextInfo,
                                  pPgInfo,
                                  pPgInfo->WorkingEntity.pCfmConfig)
        == OSIX_FAILURE)
    {
        ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                   "Unable to add Protection Group Cfm entry"
                   "(working entity) for PG %d\n", pPgInfo->u4PgId));
        return OSIX_FAILURE;
    }

    /* Verify if the same cfm entry is already used by any other PG 
     * in this context. If it is used then return failure */
    /* Working Entity - Reverse */
    if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MepId;
        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                       "Misconfiguration for iReverse working CFM entry - "
                       "MegId=%d, MeId=%d, MepId=%d is already used "
                       "by PG-%d\r\n",
                       CfmInfo.MonitorInfo.u4MegId, CfmInfo.MonitorInfo.u4MeId,
                       CfmInfo.MonitorInfo.u4MepId, pUsedCfm->pPgInfo->u4PgId));
            RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *)
                          pPgInfo->WorkingEntity.pCfmConfig);
            return OSIX_FAILURE;
        }

        /* Add the Working Entity to the RBTree */
        if (ElpsCfmAddNodeToCfmTable (pContextInfo,
                                      pPgInfo,
                                      pPgInfo->WorkingReverseEntity.pCfmConfig)
            == OSIX_FAILURE)
        {
            ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                       "Unable to add Protection Group Cfm entry"
                       "(reverse working entity) for PG %d\n",
                       pPgInfo->u4PgId));
            RBTreeRemove (pContextInfo->CfmConfigTbl,
                          (tRBElem *) pPgInfo->WorkingEntity.pCfmConfig);
            return OSIX_FAILURE;
        }
    }

    /* Protection Entity */
    CfmInfo.MonitorInfo.u4MegId =
        pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId;
    CfmInfo.MonitorInfo.u4MeId =
        pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MeId;
    CfmInfo.MonitorInfo.u4MepId =
        pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MepId;
    CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
    CfmInfo.u1ModId = pPgInfo->u1MonitorType;

    pUsedCfm = ElpsCfmGetNode (pContextInfo, &CfmInfo);

    if (pUsedCfm != NULL)
    {
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                   "Misconfiguration for protection CFM entry - "
                   "MegId=%d, MeId=%d, MepId=%d is already used "
                   "by PG-%d\r\n",
                   CfmInfo.MonitorInfo.u4MegId, CfmInfo.MonitorInfo.u4MeId,
                   CfmInfo.MonitorInfo.u4MepId, pUsedCfm->pPgInfo->u4PgId));

        /* Adding the Protection Cfm entry into Cfm RBTree has failed, hence
         * revert back the addition of Working Cfm entry into Cfm RBTree. 
         */
        RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *)
                      pPgInfo->WorkingEntity.pCfmConfig);
        if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
        {
            RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *)
                          pPgInfo->WorkingReverseEntity.pCfmConfig);
        }
        return OSIX_FAILURE;
    }

    if (ElpsCfmAddNodeToCfmTable (pContextInfo,
                                  pPgInfo,
                                  pPgInfo->ProtectionEntity.pCfmConfig)
        == OSIX_FAILURE)
    {
        ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                   "Unable to add Protection Group Cfm entry"
                   "(protection entity) for PG %d\n", pPgInfo->u4PgId));
        /* Adding the Protection Cfm entry into Cfm RBTree has failed, hence
         * revert back the addition of Working Cfm entry into Cfm RBTree. 
         */
        RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *)
                      pPgInfo->WorkingEntity.pCfmConfig);
        if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
        {
            RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *)
                          pPgInfo->WorkingReverseEntity.pCfmConfig);
        }
        return SNMP_FAILURE;
    }

    /* Verify if the same cfm entry is already used by any other PG 
     * in this context. If it is used then return failure */
    /* Protection Entity - Reverse */
    if (pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MepId;
        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                       "Misconfiguration for Reverse Protection CFM entry - "
                       "MegId=%d, MeId=%d, MepId=%d is already used "
                       "by PG-%d\r\n",
                       CfmInfo.MonitorInfo.u4MegId, CfmInfo.MonitorInfo.u4MeId,
                       CfmInfo.MonitorInfo.u4MepId, pUsedCfm->pPgInfo->u4PgId));
            RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *)
                          pPgInfo->ProtectionEntity.pCfmConfig);
            RBTreeRemove (pContextInfo->CfmConfigTbl, (tRBElem *)
                          pPgInfo->WorkingEntity.pCfmConfig);
            if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId !=
                0)
            {
                RBTreeRemove (pContextInfo->CfmConfigTbl,
                              (tRBElem *) pPgInfo->WorkingReverseEntity.
                              pCfmConfig);
            }
            return OSIX_FAILURE;
        }

        /* Add the Protection Entity to the RBTree */
        if (ElpsCfmAddNodeToCfmTable (pContextInfo,
                                      pPgInfo,
                                      pPgInfo->ProtectionReverseEntity.
                                      pCfmConfig) == OSIX_FAILURE)
        {
            ELPS_TRC ((pContextInfo, ALL_FAILURE_TRC,
                       "Unable to add Protection Group Cfm entry"
                       "(reverse protection entity) for PG %d\n",
                       pPgInfo->u4PgId));
            RBTreeRemove (pContextInfo->CfmConfigTbl,
                          (tRBElem *) pPgInfo->ProtectionEntity.pCfmConfig);
            RBTreeRemove (pContextInfo->CfmConfigTbl,
                          (tRBElem *) pPgInfo->WorkingEntity.pCfmConfig);
            if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId !=
                0)
            {
                RBTreeRemove (pContextInfo->CfmConfigTbl,
                              (tRBElem *) pPgInfo->WorkingReverseEntity.
                              pCfmConfig);
            }
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmUpdateWithServiceType
 *
 * DESCRIPTION      : This function updates the CFM entry in protection group.
 *                    This function is indended to be called only form the 
 *                    management interfaces (low level routine) when the service
 *                    type is changed. 
 *
 * INPUT            : pPgInfo      - Pointer to the protection group info
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmUpdateWithServiceType (tElpsPgInfo * pPgInfo, UINT1 u1ServiceType)
{
    tElpsCfmConfigInfo *pUsedCfm = NULL;
    tElpsCfmConfigInfo  CfmInfo;

    MEMSET (&CfmInfo, 0, sizeof (tElpsCfmConfigInfo));
    if (pPgInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MepId;
        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pPgInfo->pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            /* Remove the existing values */
            if (ElpsCfmDelNode (pPgInfo->pContextInfo,
                                pPgInfo->WorkingEntity.pCfmConfig)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to Delete Protection Group Cfm entry"
                           "(working entity) for PG %d\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }

            /* Update the service values */
            pUsedCfm->u1ServiceType = u1ServiceType;
            /* Add it to the node */
            if (ElpsCfmAddNodeToCfmTable (pPgInfo->pContextInfo,
                                          pPgInfo, pUsedCfm) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "(working entity) for PG %d\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
    }

    if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MepId;

        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pPgInfo->pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            /* Remove the existing values */
            if (ElpsCfmDelNode (pPgInfo->pContextInfo,
                                pPgInfo->WorkingReverseEntity.pCfmConfig)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to Delete Protection Group Cfm entry"
                           "(working reverse entity) for PG %d\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }

            /* Update the service values */
            pUsedCfm->u1ServiceType = u1ServiceType;

            /* Add it to the node */
            if (ElpsCfmAddNodeToCfmTable (pPgInfo->pContextInfo,
                                          pPgInfo, pUsedCfm) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "(working reverse entity) for PG %d\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
    }

    /* Protection Entity */

    if (pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MepId;

        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pPgInfo->pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            /* Remove the existing values */
            if (ElpsCfmDelNode (pPgInfo->pContextInfo,
                                pPgInfo->ProtectionEntity.pCfmConfig)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to Delete Protection Group Cfm entry"
                           "(protection entity) for PG %d\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }

            /* Update the service values */
            pUsedCfm->u1ServiceType = u1ServiceType;

            /* Add it to the node */
            if (ElpsCfmAddNodeToCfmTable (pPgInfo->pContextInfo,
                                          pPgInfo, pUsedCfm) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "(protection entity) for PG %d\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
    }

    if (pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MepId;

        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pPgInfo->pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            /* Remove the existing values */
            if (ElpsCfmDelNode (pPgInfo->pContextInfo,
                                pPgInfo->ProtectionReverseEntity.pCfmConfig)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to Delete Protection Group Cfm entry"
                           "(protection reverse entity) for PG %d\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }

            /* Update the service values */
            pUsedCfm->u1ServiceType = u1ServiceType;

            /* Add it to the node */
            if (ElpsCfmAddNodeToCfmTable (pPgInfo->pContextInfo,
                                          pPgInfo, pUsedCfm) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "(protectionr reverse entity) for PG %d\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCfmUpdateWithMonitorType
 *
 * DESCRIPTION      : This function updates the CFM entry in protection group.
 *                    This function is indended to be called only form the 
 *                    management interfaces (low level routine) when the monitor
 *                    type is changed. 
 *
 * INPUT            : pPgInfo      - Pointer to the protection group info
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCfmUpdateWithMonitorType (tElpsPgInfo * pPgInfo, UINT1 u1MonitorType)
{
    tElpsCfmConfigInfo *pUsedCfm = NULL;
    tElpsCfmConfigInfo  CfmInfo;

    MEMSET (&CfmInfo, 0, sizeof (tElpsCfmConfigInfo));

    if (pPgInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MepId;
        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pPgInfo->pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            /* Remove the existing values */
            if (ElpsCfmDelNode (pPgInfo->pContextInfo,
                                pPgInfo->WorkingEntity.pCfmConfig)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to Delete Protection Group Cfm entry"
                           "(working entity) for PG %d\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }

            /* Update the mod-id values */
            pUsedCfm->u1ModId = u1MonitorType;
            /* Add it to the node */
            if (ElpsCfmAddNodeToCfmTable (pPgInfo->pContextInfo,
                                          pPgInfo, pUsedCfm) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "(working entity) for PG %d\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
    }

    if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MepId;

        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pPgInfo->pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            /* Remove the existing values */
            if (ElpsCfmDelNode (pPgInfo->pContextInfo,
                                pPgInfo->WorkingReverseEntity.pCfmConfig)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to Delete Protection Group Cfm entry"
                           "(working reverse entity) for PG %d\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }

            /* Update the mod-id values */
            pUsedCfm->u1ModId = u1MonitorType;
            /* Add it to the node */
            if (ElpsCfmAddNodeToCfmTable (pPgInfo->pContextInfo,
                                          pPgInfo, pUsedCfm) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "(working reverse entity) for PG %d\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
    }

    /* Protection Entity */

    if (pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MepId;

        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pPgInfo->pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            /* Remove the existing values */
            if (ElpsCfmDelNode (pPgInfo->pContextInfo,
                                pPgInfo->ProtectionEntity.pCfmConfig)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to Delete Protection Group Cfm entry"
                           "(protection entity) for PG %d\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }

            /* Update the mod-id values */
            pUsedCfm->u1ModId = u1MonitorType;
            /* Add it to the node */
            if (ElpsCfmAddNodeToCfmTable (pPgInfo->pContextInfo,
                                          pPgInfo, pUsedCfm) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "(protection entity) for PG %d\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
    }

    if (pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
    {
        CfmInfo.MonitorInfo.u4MegId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId;
        CfmInfo.MonitorInfo.u4MeId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MeId;
        CfmInfo.MonitorInfo.u4MepId =
            pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MepId;

        CfmInfo.u1ServiceType = pPgInfo->u1ServiceType;
        CfmInfo.u1ModId = pPgInfo->u1MonitorType;

        pUsedCfm = ElpsCfmGetNode (pPgInfo->pContextInfo, &CfmInfo);

        if (pUsedCfm != NULL)
        {
            /* Remove the existing values */
            if (ElpsCfmDelNode (pPgInfo->pContextInfo,
                                pPgInfo->ProtectionReverseEntity.pCfmConfig)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to Delete Protection Group Cfm entry"
                           "(protection reverse entity) for PG %d\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }

            /* Update the mod-id values */
            pUsedCfm->u1ModId = u1MonitorType;
            /* Add it to the node */
            if (ElpsCfmAddNodeToCfmTable (pPgInfo->pContextInfo,
                                          pPgInfo, pUsedCfm) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "(protection reverse entity) for PG %d\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

#endif /* _ELPSCFM_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpscfm.c                      */
/*-----------------------------------------------------------------------*/
