/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpspgsh.c,v 1.2 2009/09/24 14:26:28 prabuc Exp $
 *
 * Description: This file contains the Protection Group Share Table related
 * datastructure implementation and utility functions.
 *****************************************************************************/
#ifndef _ELPSPGSH_C_
#define _ELPSPGSH_C_

#include "elpsinc.h"

/***************************************************************************
 * FUNCTION NAME    : ElpsPgShareCreateTable
 *
 * DESCRIPTION      : This function creates the Protection Group Share
 *                    table.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgShareCreateTable (tElpsContextInfo * pContextInfo)
{
    UINT4               u4RBNodeOffset = 0;

    u4RBNodeOffset = FSAP_OFFSETOF (tElpsPgInfo, PgShareRBNode);

    if ((pContextInfo->PgShareTbl =
         RBTreeCreateEmbedded (u4RBNodeOffset, ElpsPgShareRBCmp)) == NULL)
    {
        ELPS_TRC ((pContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsPgShareCreateTable: Creation of RBTree "
                   "FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (pContextInfo->PgShareTbl);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgShareDeleteTable
 *
 * DESCRIPTION      : This function deletes the protection group share 
 *                    table
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsPgShareDeleteTable (tElpsContextInfo * pContextInfo)
{
    RBTreeDestroy (pContextInfo->PgShareTbl, NULL, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgShareRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the 
 *                    protection group share table. Indices of this table
 *                    are - 
 *                    o Protection Port Id
 *                    o Protection Group Id
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 * 
 * RETURNS          : ELPS_RB_EQUAL   - if all the keys matched for both
 *                                      the nodes
 *                    ELPS_RB_LESS    - if node pRBElem1's key is less than
 *                                      node pRBElem2's key.
 *                    ELPS_RB_GREATER - if node pRBElem1's key is greater
 *                                      than node pRBElem2's key.
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgShareRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tElpsPgInfo        *pPgShInfo1 = (tElpsPgInfo *) pRBElem1;
    tElpsPgInfo        *pPgShInfo2 = (tElpsPgInfo *) pRBElem2;

    /* 1st index comparison */
    if (pPgShInfo1->ProtectionEntity.u4PortId >
        pPgShInfo2->ProtectionEntity.u4PortId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pPgShInfo1->ProtectionEntity.u4PortId <
             pPgShInfo2->ProtectionEntity.u4PortId)
    {
        return ELPS_RB_LESS;
    }

    /* 2nd index comparison */
    if (pPgShInfo1->u4PgId > pPgShInfo2->u4PgId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pPgShInfo1->u4PgId < pPgShInfo2->u4PgId)
    {
        return ELPS_RB_LESS;
    }
    return ELPS_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgShareAddNodeToPgShareTable
 *
 * DESCRIPTION      : This function adds the pg share info node to the 
 *                    pg share table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgShInfo - pointer to the pg share info node.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgShareAddNodeToPgShareTable (tElpsContextInfo * pContextInfo,
                                  tElpsPgInfo * pPgShInfo)
{
    if (RBTreeAdd (pContextInfo->PgShareTbl, (tRBElem *) pPgShInfo)
        != RB_SUCCESS)
    {
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsPgShareAddNodeToPgShareTable: Failed to add node to "
                   "PG Share Table\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgShareDelNode
 *
 * DESCRIPTION      : This function deletes the pg share info node from the 
 *                    PG share table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgShInfo - pointer to the PG share info node.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsPgShareDelNode (tElpsContextInfo * pContextInfo, tElpsPgInfo * pPgShInfo)
{
    RBTreeRemove (pContextInfo->PgShareTbl, (tRBElem *) pPgShInfo);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgShareGetFirstNode
 *
 * DESCRIPTION      : This function returns the first PG share info node 
 *                    present in the PG share table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsPgShareInfo * - pointer to the first PG share 
 *                           info node. NULL wil be return in case there is
 *                           no entry present in the PG share table
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgShareGetFirstNode (tElpsContextInfo * pContextInfo)
{
    tElpsPgInfo        *pPgShInfo = NULL;

    pPgShInfo = (tElpsPgInfo *) RBTreeGetFirst (pContextInfo->PgShareTbl);

    return pPgShInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgShareGetNextNode 
 *
 * DESCRIPTION      : This function returns the next PG share info node.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pCurrentPgShInfo - pointer to the current PG share 
 *                                       info node.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsPgInfo * - pointer to the next PG share 
 *                              info node. NULL will be returned if no 
 *                              next node is found in the PG share table.
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgShareGetNextNode (tElpsContextInfo * pContextInfo,
                        tElpsPgInfo * pCurrentPgShInfo)
{
    tElpsPgInfo        *pNextPgShInfo = NULL;

    pNextPgShInfo = (tElpsPgInfo *)
        RBTreeGetNext (pContextInfo->PgShareTbl,
                       (tRBElem *) pCurrentPgShInfo, NULL);

    /* NULL will be returned if no next node is available */
    return pNextPgShInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgShareGetNode
 *
 * DESCRIPTION      : This function search the PG share info node from the 
 *                    PG share table based on the given protection port id
 *                    and PG id.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    u4PortId - protection port id.
 *                    u4PgId   - portection group id
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsPgInfo * - pointer the PG share info node
 *                                 if it is present in the PG share table
 *                                 or NULL will be returned.
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgShareGetNode (tElpsContextInfo * pContextInfo, UINT4 u4PortId,
                    UINT4 u4PgId)
{
    tElpsPgInfo        *pPgShInfo = NULL;
    tElpsPgInfo         TmpPgShInfo;

    /* update the indices in the local defined structure which will be used
     * to searched the requied node in the RBTree */
    TmpPgShInfo.ProtectionEntity.u4PortId = u4PortId;
    TmpPgShInfo.u4PgId = u4PgId;

    pPgShInfo = (tElpsPgInfo *)
        RBTreeGet (pContextInfo->PgShareTbl, (tRBElem *) & TmpPgShInfo);

    return pPgShInfo;
}

#endif /* _ELPSPGSH_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsshrl.c                     */
/*-----------------------------------------------------------------------*/
