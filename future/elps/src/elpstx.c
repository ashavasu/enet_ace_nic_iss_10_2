/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstx.c,v 1.20 2014/02/07 13:26:01 siva Exp $
 *
 * Description: This file contains the Tx Information related
 * datastructure implementation and utility functions.
 *****************************************************************************/
#ifndef _ELPSTX_C_
#define _ELPSTX_C_

#include "elpsinc.h"
#include "elpssem.h"
#include "mplsapi.h"
#include "mplsutl.h"

/* Proto types of the functions private to this file only */
PRIVATE VOID ElpsTxDelEntriesInSll PROTO ((tTMO_SLL *));
PRIVATE VOID ElpsTxFormApsPdu PROTO ((tElpsPgInfo *, UINT1 **));
PRIVATE VOID ElpsTxScanSllsAndTxPdu PROTO ((VOID));
PRIVATE VOID ElpsTxAddApsTxInfoToSll PROTO ((tElpsApsTxInfo *));
PRIVATE VOID ElpsTxCalculateTxEntryCount PROTO ((UINT4 *));
PRIVATE INT4 ElpsTxSendPduToCfm PROTO ((tElpsApsTxInfo *));
PRIVATE UINT1 ElpsTxGetStateForPkt PROTO ((UINT1));
PRIVATE INT4 ElpsTxFormPscPdu PROTO ((tElpsPgInfo *,
                                      tCRU_BUF_CHAIN_HEADER **, UINT4));
PRIVATE VOID ElpsTxFillPathAndFPath PROTO ((tElpsPgInfo *,
                                            tCRU_BUF_CHAIN_HEADER **,
                                            tElpsPscSemState *, UINT4));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxEventHandler
 *                                                                          
 *    DESCRIPTION      : This function handles the Tx Event raised by ELPS 
 *                       main task.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
ElpsTxEventHandler (VOID)
{
    tTMO_SLL           *pTempSll = NULL;
    UINT4               u4Count = 0;

    ELPS_TX_DB_LOCK ();
    gElpsGlobalApsTxInfo.b1InApsTxEventHandler = OSIX_TRUE;

    ElpsTxCalculateTxEntryCount (&u4Count);

    while (u4Count > 0)
    {
        /* Scan all the entries in the SLL and transmit the APS PDU */
        ElpsTxScanSllsAndTxPdu ();

        /* Since the entries present in the third SLL completed sending the 
         * APS PDU (there PDUs) delete the entries present in 3rd SLL */
        ElpsTxDelEntriesInSll (gElpsGlobalApsTxInfo.pApsTxSll[ELPS_TWO]);

        /* Move the SLLs accordingly */
        pTempSll = gElpsGlobalApsTxInfo.pApsTxSll[ELPS_TWO];
        gElpsGlobalApsTxInfo.pApsTxSll[ELPS_TWO] =
            gElpsGlobalApsTxInfo.pApsTxSll[ELPS_ONE];
        gElpsGlobalApsTxInfo.pApsTxSll[ELPS_ONE] =
            gElpsGlobalApsTxInfo.pApsTxSll[ELPS_ZERO];
        gElpsGlobalApsTxInfo.pApsTxSll[ELPS_ZERO] = pTempSll;

        ELPS_TX_DB_UNLOCK ();

        /* Delaying the task by 3.3 milli seconds */
        OsixDelay (gElpsGlobalInfo.u4RapidTxTime, OSIX_MICRO_SECONDS);

        ELPS_TX_DB_LOCK ();
        ElpsTxCalculateTxEntryCount (&u4Count);
    }

    gElpsGlobalApsTxInfo.b1InApsTxEventHandler = OSIX_FALSE;
    ELPS_TX_DB_UNLOCK ();

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxFormAndSendAPSPdu
 *                                                                          
 *    DESCRIPTION      : This function is called by the periodic timer 
 *                       expiry handler routine for transmitting APS/PSC PDU.
 *
 *    INPUT            : pPg - Pointer to the Protection Group structure.
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
ElpsTxFormAndSendAPSPdu (tElpsPgInfo * pPg)
{
    tElpsApsTxInfo      Aps;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4OutIndex = 0;
    UINT1               au1NextHop[MAC_ADDR_LEN];
    UINT1              *pu1Pdu = NULL;

    MEMSET (&Aps, 0, sizeof (tElpsApsTxInfo));

    Aps.u4ContextId = pPg->pContextInfo->u4ContextId;
    Aps.MonitorInfo.u4MegId =
        pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId;
    Aps.MonitorInfo.u4MeId =
        pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MeId;
    Aps.MonitorInfo.u4MepId =
        pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MepId;
    Aps.u1PgServiceType = pPg->u1ServiceType;

    if (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        pu1Pdu = gau1ApsPdu;

        ElpsTxFormApsPdu (pPg, &pu1Pdu);

        Aps.unBufferParam.pu1ApsPdu = gau1ApsPdu;

        ELPS_TRC ((pPg->pContextInfo, DUMP_TRC,
                   "Tx APS PDU [Context-%d, PG-%d]\r\n\r\n",
                   pPg->pContextInfo->u4ContextId, pPg->u4PgId));
        ElpsTrcApsPktDumpTrc (pPg->pContextInfo, Aps.unBufferParam.pu1ApsPdu,
                              ELPS_APS_PDU_TLV_OFFSET);

        if (ElpsTxSendPduToCfm (&Aps) == OSIX_FAILURE)
        {
            ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "ElpsTxFormAndSendAPSPdu: Failed to transmit "
                       "APS PDU!!!!\r\n"));
            return;
        }
    }
    else if ((pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
             (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        MEMSET (au1NextHop, 0, MAC_ADDR_LEN);

        /* Form the MPLS packet with PSC PDU */
        pBuf = ElpsTxFormMplsPkt (pPg, &u4OutIndex, au1NextHop);

        if (pBuf == NULL)
        {
            return;
        }

        Aps.unBufferParam.pPscPdu = pBuf;
        Aps.u4Index = u4OutIndex;
        MEMCPY (Aps.au1DstMac, au1NextHop, MAC_ADDR_LEN);

        /* Send it to MPLS */
        if (ElpsTxSendPscPduToMpls (&Aps) == OSIX_FAILURE)
        {
            ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                       "ElpsTxFormAndSendAPSPdu: Failed to transmit "
                       "APS PDU!!!!\r\n"));
            return;
        }
    }
    /* Upate the Aps Tx counter */
    pPg->Stats.u4TotalApsTxCount++;

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxHandleSemStateChange
 *                                                                          
 *    DESCRIPTION      : This function handles the transmission of 3 APS PDUs
 *                       in a time gap of 3.3 milliseconds on SEM state change.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
ElpsTxHandleSemStateChange (tElpsPgInfo * pPg)
{
    tElpsApsTxInfo     *pAps = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4OutIndex = 0;
    UINT1               au1NextHop[MAC_ADDR_LEN];
    UINT1              *pu1Pdu = NULL;
    UINT1              *pu1TempPdu = NULL;

    if ((pAps = (tElpsApsTxInfo *)
         MemAllocMemBlk (gElpsGlobalApsTxInfo.ApsTxInfoTablePoolId)) == NULL)
    {
        ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsTxHandleSemStateChange: Allocation of memory for "
                   "APS Tx Info FAILED!!!\r\n"));
        return;
    }
    MEMSET (pAps, 0, sizeof (tElpsApsTxInfo));

    pAps->u4ContextId = pPg->pContextInfo->u4ContextId;
    pAps->u4PgId = pPg->u4PgId;
    pAps->u1PgStatus = pPg->u1PgStatus;
    pAps->MonitorInfo.u4MegId =
        pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId;
    pAps->MonitorInfo.u4MeId =
        pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MeId;
    pAps->MonitorInfo.u4MepId =
        pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MepId;
    pAps->u1PgServiceType = pPg->u1ServiceType;
    pAps->u1Count = 0;            /* Initializing the counter */

    if (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        if ((pu1Pdu = MemAllocMemBlk (gElpsGlobalApsTxInfo.ApsTxPduPoolId))
            == NULL)
        {
            ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsTxHandleSemStateChange: Allocation of memory for "
                       "APS Tx PDU FAILED!!!\r\n"));
            MemReleaseMemBlock (gElpsGlobalApsTxInfo.ApsTxInfoTablePoolId,
                                (UINT1 *) pAps);
            return;
        }

        pu1TempPdu = pu1Pdu;

        ElpsTxFormApsPdu (pPg, &pu1TempPdu);

        pAps->unBufferParam.pu1ApsPdu = pu1Pdu;

    }
    else if ((pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
             (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        MEMSET (au1NextHop, 0, MAC_ADDR_LEN);

        /* Form the MPLS packet with PSC PDU */
        pBuf = ElpsTxFormMplsPkt (pPg, &u4OutIndex, au1NextHop);

        if (pBuf == NULL)
        {
            ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsTxHandleSemStateChange: MPLS packet "
                       "formation FAILED!!!\r\n"));
            MemReleaseMemBlock (gElpsGlobalApsTxInfo.ApsTxInfoTablePoolId,
                                (UINT1 *) pAps);
            return;
        }

        pAps->unBufferParam.pPscPdu = pBuf;
        pAps->u4Index = u4OutIndex;
        MEMCPY (pAps->au1DstMac, au1NextHop, MAC_ADDR_LEN);

    }

    ElpsTxAddApsTxInfoToSll (pAps);

    /* Increment the Aps Tx counter by 3. As this APSTx thread is going to
     * transmit 3 APS PDUs.
     * This counter should not be updated from APSTx Task, as there it needs
     * to take ELPS protocol lock, which can degrade the performance. */

    pPg->Stats.u4TotalApsTxCount += ELPS_MAX_APS_PDU_ON_STATE_CHANGE;
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxInitApsTxInfoSLL
 *                                                                          
 *    DESCRIPTION      : This function initializes the SLL maintained for 
 *                       APS Tx information.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
ElpsTxInitApsTxInfoSLL (VOID)
{
    UINT1               u1Index = 0;

    ELPS_TX_DB_LOCK ();
    for (; u1Index < ELPS_MAX_APS_PDU_ON_STATE_CHANGE; u1Index++)
    {
        /* Initialize the SLL */
        TMO_SLL_Init (&(gElpsGlobalApsTxInfo.ApsTxInfoList[u1Index]));
        /* Store the address of the SLL to the below  array of pointers 
         * variable. This will be used for swapping the SLLs. */
        gElpsGlobalApsTxInfo.pApsTxSll[u1Index] =
            &(gElpsGlobalApsTxInfo.ApsTxInfoList[u1Index]);
    }
    ELPS_TX_DB_UNLOCK ();
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxAddApsTxInfoToSll
 *                                                                          
 *    DESCRIPTION      : This function adds the APS Tx Info to the SLL and 
 *                       post the event to APS task. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
ElpsTxAddApsTxInfoToSll (tElpsApsTxInfo * pAps)
{
    ELPS_TX_DB_LOCK ();

    TMO_SLL_Add (gElpsGlobalApsTxInfo.pApsTxSll[0],
                 (tTMO_SLL_NODE *) (VOID *) pAps);
    if (gElpsGlobalApsTxInfo.b1InApsTxEventHandler == OSIX_FALSE)
    {
        ElpsQuePostEventToAPSTxTask ();
    }

    ELPS_TX_DB_UNLOCK ();
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxFormPscPdu
 *                                                                          
 *    DESCRIPTION      : This function forms the Psc PDU with necessary info
 *
 *    INPUT            : pPg       - Pointer to the Protection Group Entry
 *                                                                          
 *    OUTPUT           : pu1ApsPdu - Pointer to the formed Psc PDU
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
ElpsTxFormPscPdu (tElpsPgInfo * pPg, tCRU_BUF_CHAIN_HEADER ** ppu1ApsPdu,
                  UINT4 u4Offset)
{
    tElpsPscSemState    PscSemStateInfo;
    tElpsPscSemState   *pPscSemState = &PscSemStateInfo;
    UINT1               u1Value = 0;
    UINT1               u1Direction = 0;
    UINT1               u1Request = 0;
    UINT1               u1RevertiveField = 0;
    UINT1               u1BridgeType = 0;

    if (ElpsSemPscGetStateInfo (pPg->ApsInfo.u1ApsSemStateId, pPscSemState)
        == OSIX_FAILURE)
    {
        ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "Protection Group is in Invalid State. \r\n"));
        return OSIX_FAILURE;
    }

    /* Version */
    ELPS_PSC_PDU_VERSION (pPg->u1PscVersion, u1Value);
    u1Value = (u1Value << ELPS_SIX);

    /* Request State */
    if (pPg->u1PscVersion == ELPS_ZERO)
    {
        u1Request = ElpsTxGetPscStateForPkt (pPg,
                                             (UINT1) pPscSemState->u4Request);
    }
    else
    {
        u1Request = ElpsTxGetPscStateForPscVersion1 (pPg,
                                                     (UINT1) pPscSemState->
                                                     u4Request);
    }
    u1Value |= (UINT1) ((u1Request << ELPS_TWO) & ELPS_PSC_PKT_REQUEST_MASK);

    /* Bi-directional or Uni-directional */
    u1Direction =
        (UINT1) ELPS_GET_PG_D_BIT_VALUE (pPg->ApsInfo.u1ProtectionType);
    u1Value |= (UINT1) (u1Direction);

    /* Permanent Bridge or Selector Bridge */
    u1BridgeType =
        (UINT1) ELPS_GET_PG_B_BIT_VALUE (pPg->ApsInfo.u1ProtectionType);
    u1BridgeType = (UINT1) (u1BridgeType >> ELPS_TWO);
    if (u1BridgeType == 0)
    {
        u1BridgeType = 1;
    }
    else
    {
        u1BridgeType = 0;
    }
    u1Value |= (UINT1) (u1BridgeType);

    ELPS_DATA_ASSIGN_1_BYTE (*ppu1ApsPdu, u4Offset, u1Value);

    /* Revertive and Reserved bit */
    u1RevertiveField =
        (UINT1) ELPS_GET_PG_R_BIT_VALUE (pPg->ApsInfo.u1ProtectionType);
    u1RevertiveField = (UINT1) (u1RevertiveField << ELPS_SEVEN);
    ELPS_DATA_ASSIGN_1_BYTE (*ppu1ApsPdu, u4Offset, u1RevertiveField);

    /* Fill Path & FPath */
    ElpsTxFillPathAndFPath (pPg, ppu1ApsPdu, pPscSemState, u4Offset);

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxFillPathAndFPath
 *                                                                          
 *    DESCRIPTION      : This function fills the Path and FPath info in the PDU
 *
 *    INPUT            : pPg       - Pointer to the Protection Group Entry
 *                       pPscSemState - Pointer to PSC Sem State
 *                                                                          
 *    OUTPUT           : ppu1ApsPdu - Pointer to the formed PSC PDU
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
ElpsTxFillPathAndFPath (tElpsPgInfo * pPg, tCRU_BUF_CHAIN_HEADER ** ppu1ApsPdu,
                        tElpsPscSemState * pPscSemState, UINT4 u4Offset)
{
    UINT1               u1FPath = 0;
    UINT1               u1Path = 0;

    if (pPg->u1PreviousStateId == ELPS_STATE_SF_W)
    {
        if ((pPg->u1LastActiveReq == ELPS_EV_FR_LO) ||
            (pPg->u1LastActiveReq == ELPS_EV_FR_SF_P))
        {
            u1FPath = ELPS_SIGNAL_NORMAL;
            u1Path = ELPS_SIGNAL_NULL;
        }
        else if (pPg->u1LastActiveReq == ELPS_EV_FR_FS)
        {
            u1FPath = ELPS_SIGNAL_NORMAL;
            u1Path = ELPS_SIGNAL_NORMAL;
        }
        else
        {
            u1FPath = (UINT1) pPscSemState->u4FaultPath;
            u1Path = (UINT1) pPscSemState->u4Path;
        }
    }
    else
    {
        u1FPath = (UINT1) pPscSemState->u4FaultPath;
        u1Path = (UINT1) pPscSemState->u4Path;
    }
    /* Fault Path */
    ELPS_DATA_ASSIGN_1_BYTE (*ppu1ApsPdu, u4Offset, u1FPath);
    /* Path */
    ELPS_DATA_ASSIGN_1_BYTE (*ppu1ApsPdu, u4Offset, u1Path);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxFormApsPdu
 *                                                                          
 *    DESCRIPTION      : This function forms the APS PDU with necessary info
 *
 *    INPUT            : pPg       - Pointer to the Protection Group Entry
 *                                                                          
 *    OUTPUT           : pu1ApsPdu - Pointer to the formed APS PDU
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
ElpsTxFormApsPdu (tElpsPgInfo * pPg, UINT1 **ppu1ApsPdu)
{
    tElpsSemState       SemStateInfo;
    tElpsSemState      *pSemState = &SemStateInfo;
    UINT2               u2Offset = 0;
    UINT1               u1Value = 0;

    /* Currently the MEL, OpCode, Version are filled by ECFM-Y.1731 itself, 
       hence APS specific information alone is filled here */
    if (ElpsSemGetStateInfo (pPg->ApsInfo.u1ApsSemStateId, pSemState)
        == OSIX_FAILURE)
    {
        ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "Protection Group is in Invalid State. \r\n"));
        return;
    }

    /* State & Prot. Type */
    u1Value = (UINT1) pSemState->u4RequestOrState;

    u1Value = ElpsTxGetStateForPkt (u1Value);
    u1Value = (UINT1) (u1Value << ELPS_FOUR);
    u1Value |= pPg->ApsInfo.u1ProtectionType;
    ELPS_LBUF_PUT_1_BYTE (*ppu1ApsPdu, u1Value, u2Offset);

    /* Requested Signal */
    u1Value = (UINT1) pSemState->u4RequestedSignal;
    ELPS_LBUF_PUT_1_BYTE (*ppu1ApsPdu, u1Value, u2Offset);

    /* Bridged Signal 
     * For 1+1 Protection switching bridged signal will always be NORMAL
     */
    if (pPg->ApsInfo.u1ProtectionType & ELPS_B_BIT_VALUE)
    {
        u1Value = (UINT1) pSemState->u4BridgedSignal;
        ELPS_LBUF_PUT_1_BYTE (*ppu1ApsPdu, u1Value, u2Offset);
    }
    else
    {
        u1Value = (UINT1) ELPS_SIGNAL_NORMAL;
        ELPS_LBUF_PUT_1_BYTE (*ppu1ApsPdu, u1Value, u2Offset);
    }

    /* Reserved */
    ELPS_LBUF_PUT_1_BYTE (*ppu1ApsPdu, 0, u2Offset);

    /* End TLV */
    ELPS_LBUF_PUT_1_BYTE (*ppu1ApsPdu, 0, u2Offset);

    return;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ElpsTxFormMplsPkt
 *
 *    DESCRIPTION      : This function is called to form the MPLS Pkt and
 *                       get the outgoing interface and Next Hop Mac
 *                       for the pkt to be sent
 *
 *    INPUT            : pPg - Pointer to the Protection Group structure.
 *                       pu4OutIndex - Pointer to Out going Interface
 *                       pau1NextHop - Pointer to Next hop Mac
 *
 *    OUTPUT           : pBuf - Pointer to CRU buff
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC tCRU_BUF_CHAIN_HEADER *
ElpsTxFormMplsPkt (tElpsPgInfo * pPg, UINT4 *pu4OutIndex, UINT1 *pau1NextHop)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Offset = 0;

    /* Allocate memory */
    pBuf = CRU_BUF_Allocate_MsgBufChain (ELPS_MAX_MPLS_PKT_LEN, 0);

    if (pBuf == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                   "ElpsTxFormMplsPkt: PSC Tx PDU CRU allocation "
                   "FAILED !!!\r\n"));
        return NULL;
    }
    /* Form PSC PDU from the corresponding offset */
    ELPS_MPLS_PKT_PSC_OFF (pPg->u1PscVersion, u4Offset);
    if (ElpsTxFormPscPdu (pPg, &pBuf, u4Offset) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                   "ElpsTxFormMplsPkt: Forming PSC PDU " "FAILED !!!\r\n"));
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return NULL;
    }

    CRU_BUF_Move_ValidOffset (pBuf, u4Offset);

    ELPS_TRC ((pPg->pContextInfo, DUMP_TRC,
               "Tx PSC PDU [Context-%d, PG-%d]\r\n\r\n",
               pPg->pContextInfo->u4ContextId, pPg->u4PgId));

    ElpsTrcPscPktDumpTrc (pPg->pContextInfo, pBuf, 0);

    if (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        /* Fill the MPLS packet with LSP Tunnel Information */
        if (ElpsTxFillLspInfo (pPg, pBuf, pu4OutIndex, pau1NextHop)
            == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                       "ElpsTxFormMplsPkt: Forming Tunnel MPLS related "
                       "Header FAILED !!!\r\n"));
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return NULL;
        }
    }
    else if (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        /* Fill the MPLS packet with PW Information */
        if (ElpsTxFillPwInfo (pPg, pBuf, pu4OutIndex, pau1NextHop)
            == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                       "ElpsTxFormMplsPkt: Forming Pw MPLS related "
                       "Header FAILED !!!\r\n"));
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return NULL;
        }
    }

    return pBuf;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ElpsTxFillLspInfo
 *
 *    DESCRIPTION      : This function is called by ElpsTxFormMplsPkt to
 *                       fill the MPLS Tunnel related headers
 *
 *    INPUT            : pPg - Pointer to the Protection Group structure.
 *                       pBuf - Pointer to the CRU buffer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC INT4
ElpsTxFillLspInfo (tElpsPgInfo * pPg, tCRU_BUF_CHAIN_HEADER * pBuf,
                   UINT4 *pu4OutIndex, UINT1 *pau1NextHop)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tMplsAchTlvHdr      AchTlvHdr;
    tMplsAchHdr         AchHdr;
    tMplsHdr            MplsHdr;
    INT4                i4LabelCount = 0;
    tMplsOutSegInfo    *pOutSegInfo = NULL;
    UINT2               u2Val = 0;
    /*UINT4               u4Offset =0; */

    /*------------------------------------------------------------------------*
     *                         Packet Format                                  *
     *------------------------------------------------------------------------*
     *Ethernet Header|MPLS label stack|GAL|Ach Header|Ach-TLV-Header|
     GAch-TLV list|PSC PDU|*
     *-----------------------------------------------------------------------*/
    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC, "ElpsTxFillLspInfo:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC, "ElpsTxFillLspInfo:"
                   "Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));
    MEMSET (&AchTlvHdr, 0, sizeof (tMplsAchTlvHdr));
    MEMSET (&AchHdr, 0, sizeof (tMplsAchHdr));
    MEMSET (&MplsHdr, 0, sizeof (tMplsHdr));

    /* Get the LSP information for MPLS */
    pMplsApiInInfo->u4SubReqType = 0;
    pMplsApiInInfo->u4ContextId = pPg->pContextInfo->u4ContextId;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

    if (pPg->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
            pPg->ProtectionEntity.ServiceParam.LspServiceInfo.u4TnlIndex;
        pMplsApiInInfo->InPathId.TnlId.u4LspNum =
            pPg->ProtectionEntity.ServiceParam.LspServiceInfo.u4TnlInstance;
        pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] =
            pPg->ProtectionEntity.ServiceParam.LspServiceInfo.u4TnlIngressLsrId;
        pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] =
            pPg->ProtectionEntity.ServiceParam.LspServiceInfo.u4TnlEgressLsrId;
    }
    else if (pPg->u1PgConfigType == ELPS_PG_TYPE_LIST)
    {
        pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
            pPg->unServiceList.pWorkingServiceListPointer->
            ProtectionInfo.LspServiceInfo.u4TnlIndex;

        pMplsApiInInfo->InPathId.TnlId.u4LspNum =
            pPg->unServiceList.pWorkingServiceListPointer->
            ProtectionInfo.LspServiceInfo.u4TnlInstance;

        pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] =
            pPg->unServiceList.pWorkingServiceListPointer->
            ProtectionInfo.LspServiceInfo.u4TnlIngressLsrId;

        pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] =
            pPg->unServiceList.pWorkingServiceListPointer->
            ProtectionInfo.LspServiceInfo.u4TnlEgressLsrId;
    }
    else
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);
        ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                   ALL_FAILURE_TRC,
                   "ElpsTxFillLspInfo: Config type ALL is not supported "
                   "for MPLS Paths!!!!\r\n"));
        return OSIX_FAILURE;
    }

    /* Api to get the Tunnel Information */
    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                         pMplsApiInInfo,
                                         pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);
        ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                   ALL_FAILURE_TRC,
                   "ElpsTxFillLspInfo: Failed to get " "Tunnel Info!!!!\r\n"));
        return OSIX_FAILURE;
    }

    if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
        TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
        {
            pOutSegInfo =
                &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo;
        }
        else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS)
        {
            pOutSegInfo =
                &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;
        }
        else
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "ElpsTxFillLspInfo: Protection Switching is not"
                       "supported for this tunnel Role  !!!!\r\n"));
            return OSIX_FAILURE;
        }
    }
    else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
             TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
    {
        if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
        {
            MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
            pMplsApiInInfo->u4SubReqType = 0;
            pMplsApiInInfo->u4ContextId = pPg->pContextInfo->u4ContextId;
            pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

            if ((pPg->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL) &&
                (pPg->ProtectionReverseEntity.ServiceParam.LspServiceInfo.
                 u4TnlIngressLsrId != 0))
            {
                pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                    pPg->ProtectionReverseEntity.ServiceParam.LspServiceInfo.
                    u4TnlIndex;
                pMplsApiInInfo->InPathId.TnlId.u4LspNum =
                    pPg->ProtectionReverseEntity.ServiceParam.LspServiceInfo.
                    u4TnlInstance;
                pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.
                    u4_addr[0] =
                    pPg->ProtectionReverseEntity.ServiceParam.LspServiceInfo.
                    u4TnlIngressLsrId;
                pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.
                    u4_addr[0] =
                    pPg->ProtectionReverseEntity.ServiceParam.LspServiceInfo.
                    u4TnlEgressLsrId;
            }
            else if ((pPg->u1PgConfigType == ELPS_PG_TYPE_LIST) &&
                     (pPg->unServiceList.pWorkingServiceListPointer->
                      ProtectionRvrInfo.LspServiceInfo.u4TnlIngressLsrId != 0))
            {
                pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                    pPg->unServiceList.pWorkingServiceListPointer->
                    ProtectionRvrInfo.LspServiceInfo.u4TnlIndex;

                pMplsApiInInfo->InPathId.TnlId.u4LspNum =
                    pPg->unServiceList.pWorkingServiceListPointer->
                    ProtectionRvrInfo.LspServiceInfo.u4TnlInstance;

                pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.
                    u4_addr[0] =
                    pPg->unServiceList.pWorkingServiceListPointer->
                    ProtectionRvrInfo.LspServiceInfo.u4TnlIngressLsrId;

                pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.
                    u4_addr[0] =
                    pPg->unServiceList.pWorkingServiceListPointer->
                    ProtectionRvrInfo.LspServiceInfo.u4TnlEgressLsrId;
            }
            else
            {
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                           ALL_FAILURE_TRC,
                           "ElpsTxFillLspInfo: Reverse path is not "
                           "configured or ALL option not supported !!!!\r\n"));
                return OSIX_FAILURE;
            }

            if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                                 pMplsApiInInfo,
                                                 pMplsApiOutInfo)
                == OSIX_FAILURE)
            {
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                           ALL_FAILURE_TRC,
                           "ElpsTxFillLspInfo: Failed to get "
                           "Tunnel Info!!!!\r\n"));
                return OSIX_FAILURE;
            }
            pOutSegInfo =
                &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;
        }
        else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS)
        {
            pOutSegInfo =
                &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;
        }
        else
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "ElpsTxFillLspInfo: Protection Switching is not"
                       "supported for this tunnel Role  !!!!\r\n"));
            return OSIX_FAILURE;
        }

    }
    else
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);
        ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                   ALL_FAILURE_TRC,
                   "ElpsTxFillLspInfo: Uni Directional Protection Switching is"
                   " not supported !!!!\r\n"));
        return OSIX_FAILURE;
    }
    /* Fill the MPLS ach Tlv Header as per weingarten-draft version */
    if (pPg->u1PscVersion == ELPS_ZERO)
    {
        AchTlvHdr.u2TlvHdrLen = 0;
        AchTlvHdr.u2Rsvd = 0;
        MplsUtlFillMplsAchTlvHdr (&AchTlvHdr, pBuf);
    }

    /* Fill the MPLS Ach Header */
    AchHdr.u4AchType = MPLS_ACH_TYPE;
    AchHdr.u1Version = MPLS_ACH_VERSION;
    AchHdr.u1Rsvd = MPLS_ACH_RSVD;

    ELPS_PSC_DEFAULT_CHANNEL_CODE (pPg->u1PscVersion, u2Val);
    AchHdr.u2ChannelType = u2Val;

    MplsUtlFillMplsAchHdr (&AchHdr, pBuf);

    /* Fill the Gal label */
    MplsHdr.u4Lbl = MPLS_GAL_LABEL;
    MplsHdr.Ttl = 1;
    MplsHdr.Exp = 0;
    MplsHdr.SI = 1;
    MplsUtlFillMplsHdr (&MplsHdr, pBuf);

    /* Fill the MPLS Header */
    if (pOutSegInfo->u1LblStkCnt > MPLS_MAX_LABEL_STACK)
    {
        ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                   ALL_FAILURE_TRC,
                   "ElpsTxFillLspInfo: MPLS max Label Stack size"
                   " exceeded!!\r\n"));
        return OSIX_FAILURE;
    }

    for (i4LabelCount = pOutSegInfo->u1LblStkCnt - 1;
         i4LabelCount >= 0; i4LabelCount--)
    {
        MplsHdr.u4Lbl = pOutSegInfo->LblInfo[i4LabelCount].u4Label;
        MplsHdr.Ttl = pOutSegInfo->LblInfo[i4LabelCount].u1Ttl;

        if (i4LabelCount == 0)
        {
            MplsHdr.Exp = MPLS_TC_BEST_EFFORT;
        }
        else
        {
            MplsHdr.Exp = pOutSegInfo->LblInfo[i4LabelCount].u1Exp;
        }

        MplsHdr.SI = 0;

        MplsUtlFillMplsHdr (&MplsHdr, pBuf);
    }

    /* Copy the Out going Interface & Next Hop */
    *pu4OutIndex = pOutSegInfo->u4OutIf;
    MEMCPY (pau1NextHop,
            pMplsApiOutInfo->OutTeTnlInfo.au1NextHopMac, MAC_ADDR_LEN);

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pMplsApiOutInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ElpsTxFillPwInfo
 *
 *    DESCRIPTION      : This function is called by ElpsTxFormMplsPkt to
 *                       fill the MPLS Pw related headers and send it to MPLS
 *
 *    INPUT            : pPg - Pointer to the Protection Group structure.
 *                       pBuf - Pointer to the CRU buffer
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC INT4
ElpsTxFillPwInfo (tElpsPgInfo * pPg, tCRU_BUF_CHAIN_HEADER * pBuf,
                  UINT4 *pu4OutIndex, UINT1 *pau1NextHop)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tMplsAchTlvHdr      AchTlvHdr;
    tMplsAchHdr         AchHdr;
    tMplsHdr            MplsHdr;
    INT4                i4LabelCount = 0;
    UINT1               u1MplsType = 0;
    UINT2               u2Val = 0;
    tMplsOutSegInfo    *pOutSegInfo = NULL;

    /*------------------------------------------------------------------------*
     *                      Packet Format                                     *
     *------------------------------------------------------------------------*
     *Ethernet Header|MPLS label stack|PW Label|Ach Header|Ach-TLV-Header|
      GAch-TLV list|PSC PDU
     *-----------------------------------------------------------------------*/
    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC, "ElpsTxFillPwInfo:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC, "ElpsTxFillPwInfo:"
                   "Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));
    MEMSET (&AchTlvHdr, 0, sizeof (tMplsAchTlvHdr));
    MEMSET (&AchHdr, 0, sizeof (tMplsAchHdr));
    MEMSET (&MplsHdr, 0, sizeof (tMplsHdr));

    /* Get the LSP information for MPLS */
    pMplsApiInInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
    pMplsApiInInfo->u4ContextId = pPg->pContextInfo->u4ContextId;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

    if (pPg->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        /* Configuration type Individual */
        pMplsApiInInfo->InPathId.PwId.u4PwIndex =
            pPg->ProtectionEntity.ServiceParam.PwServiceInfo.u4PwIndex;
    }
    else
    {
        /* Configuration type List */
        pMplsApiInInfo->InPathId.PwId.u4PwIndex =
            pPg->unServiceList.pWorkingServiceListPointer->
            ProtectionInfo.PwServiceInfo.u4PwIndex;
    }

    /* Api to get the PW Information */
    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_PW_INFO,
                                         pMplsApiInInfo,
                                         pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);
        ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                   ALL_FAILURE_TRC,
                   "ElpsTxFillPwInfo: Failed to get "
                   "PW info from MPLS!!!!\r\n"));
        return OSIX_FAILURE;
    }

    /* This can be TE Tunnel/NON-TE Tunnel/PW ONly */
    u1MplsType = pMplsApiOutInfo->OutPwInfo.u1MplsType;
    if (pPg->u1PscVersion == ELPS_ZERO)
    {
        /* Fill the MPLS Gach Tlv Header */
        AchTlvHdr.u2TlvHdrLen = 0;
        AchTlvHdr.u2Rsvd = 0;
        MplsUtlFillMplsAchTlvHdr (&AchTlvHdr, pBuf);
    }

    /* Fill the MPLS Ach Header */
    AchHdr.u4AchType = MPLS_ACH_TYPE;
    AchHdr.u1Version = MPLS_ACH_VERSION;
    AchHdr.u1Rsvd = MPLS_ACH_RSVD;

    ELPS_PSC_DEFAULT_CHANNEL_CODE (pPg->u1PscVersion, u2Val);
    AchHdr.u2ChannelType = u2Val;

    MplsUtlFillMplsAchHdr (&AchHdr, pBuf);

    /* Fill the Pw Label */
    MplsHdr.u4Lbl = pMplsApiOutInfo->OutPwInfo.u4OutVcLabel;

    if (pMplsApiOutInfo->OutPwInfo.u1CcSelected == L2VPN_VCCV_CC_TTL_EXP)
    {
        MplsHdr.Ttl = 1;
    }
    else
    {
        MplsHdr.Ttl = MPLS_DEF_PW_TTL;
    }
    MplsHdr.Exp = 0;
    MplsHdr.SI = 1;
    MplsUtlFillMplsHdr (&MplsHdr, pBuf);

    if (pMplsApiOutInfo->OutPwInfo.u1CcSelected == L2VPN_VCCV_CC_RAL)
    {
        /* Fill the RAL label. */
        MplsHdr.u4Lbl = 1;
        MplsHdr.Ttl = 1;
        MplsHdr.Exp = 0;
        MplsHdr.SI = 0;
        MplsUtlFillMplsHdr (&MplsHdr, pBuf);
    }

    if (u1MplsType != MPLS_PW_TYPE_PWONLY)
    {
        /* If the u1MplsType is not PW only, its underlying Tunnel needs 
           to be found and its corresponding label, out going interface
           & next hop should be used  */
        MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));

        /* Get the underlying Tunnel information for Pw */
        pMplsApiInInfo->u4SubReqType = 0;
        pMplsApiInInfo->u4ContextId = pPg->pContextInfo->u4ContextId;
        pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

        pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
            pMplsApiOutInfo->OutPwInfo.TeTnlId.u4SrcTnlNum;
        pMplsApiInInfo->InPathId.TnlId.u4LspNum =
            pMplsApiOutInfo->OutPwInfo.TeTnlId.u4LspNum;
        pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] =
            pMplsApiOutInfo->OutPwInfo.TeTnlId.SrcNodeId.
            MplsRouterId.u4_addr[0];
        pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] =
            pMplsApiOutInfo->OutPwInfo.TeTnlId.DstNodeId.
            MplsRouterId.u4_addr[0];

        if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                             pMplsApiInInfo,
                                             pMplsApiOutInfo) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "ElpsTxFormAndSendAPSPdu: Failed to get "
                       "PW Tunnel info from MPLS!!!!\r\n"));
            return OSIX_FAILURE;
        }
        if ((pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
             TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
            && (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS))
        {
            pOutSegInfo = &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.
                RevOutSegInfo;
        }
        else
        {
            pOutSegInfo = &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.
                FwdOutSegInfo;
        }

        /* Fill the MPLS Header */
        for (i4LabelCount = 0; i4LabelCount <
             pOutSegInfo->u1LblStkCnt; i4LabelCount++)
        {
            MplsHdr.u4Lbl = pOutSegInfo->LblInfo[i4LabelCount].u4Label;
            MplsHdr.Ttl = pOutSegInfo->LblInfo[i4LabelCount].u1Ttl;
            MplsHdr.Exp = pOutSegInfo->LblInfo[i4LabelCount].u1Exp;
            MplsHdr.SI = 0;
            MplsUtlFillMplsHdr (&MplsHdr, pBuf);
        }

        *pu4OutIndex = pOutSegInfo->u4OutIf;
        MEMCPY (pau1NextHop,
                pMplsApiOutInfo->OutTeTnlInfo.au1NextHopMac, MAC_ADDR_LEN);
    }
    else
    {                            /* If the u1MplsType is PW only, PW,s corresponding out going interface & 
                                   next hop should be used  */
        *pu4OutIndex = pMplsApiOutInfo->OutPwInfo.PwOnlyIfIndex;
        MEMCPY (pau1NextHop,
                pMplsApiOutInfo->OutPwInfo.au1NextHopMac, MAC_ADDR_LEN);

    }
    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pMplsApiOutInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxScanSllsAndTxPdu
 *                                                                          
 *    DESCRIPTION      : This function scans all the 3 SLLs and transmits  
 *                       the APS PDUs accordingly.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
ElpsTxScanSllsAndTxPdu (VOID)
{
    tTMO_SLL_NODE      *pNode = NULL;
    tElpsApsTxInfo     *pTxInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pTmpBuf = NULL;
    tElpsPgInfo        *pPgInfo = NULL;
    UINT4               u4ErrCode = 0;
    UINT4               u4Offset = 0;
    UINT1               u1Index = 0;

    for (; u1Index < ELPS_MAX_APS_PDU_ON_STATE_CHANGE; u1Index++)
    {
        TMO_SLL_Scan (gElpsGlobalApsTxInfo.pApsTxSll[u1Index], pNode,
                      tTMO_SLL_NODE *)
        {
            pTxInfo =
                (tElpsApsTxInfo *) (pNode - FSAP_OFFSETOF (tElpsApsTxInfo,
                                                           SllNode));
            /* performance measurement */
            pPgInfo = ElpsUtilValidateCxtAndPgInfo (pTxInfo->u4ContextId,
                                                    pTxInfo->u4PgId,
                                                    &u4ErrCode);

            if (pPgInfo != NULL)
            {
                /* Signal Fail on working path has detected in local node. 
                 * So APS sends pkt to peer where SF field is set.
                 * This time is noted as "T1". 
                 */
                /* Reason to add check " (u1Index == 0)" in below condition.
                 * --------------------------------------------------------
                 * This APS-Signal Fail packet will be sent thrice. If this
                 * check is not added, then the time of third pkt to be sent
                 * will be stored. But the requirement is to find the time
                 * difference between (the peer node changes its state on 
                 * hearing the APS-SF from remote) to (the first APS-SF 
                 * packet transmitted in the local node). To achieve this, 
                 * the check is additionaly added.
                 */
                if (ELPS_APS_IS_SF_FOR_WORKING
                    (pTxInfo->unBufferParam.pu1ApsPdu[0]) && (u1Index == 0))
                {
                    ElpsUtilMeasureTime (pPgInfo, ELPS_PERF_LR_SF_TX);
                }
            }

            if (pTxInfo->u1PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {

                /* HITLESS RESTART */
                if (ELPS_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE)
                {
                    continue;
                }

                if (ElpsTxSendPduToCfm (pTxInfo) == OSIX_SUCCESS)
                {
                    /* As the transmission is success, Increment the counter. */
                    pTxInfo->u1Count++;
                }
            }
            else if ((pTxInfo->u1PgServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                     (pTxInfo->u1PgServiceType == ELPS_PG_SERVICE_TYPE_PW))
            {
                if (u1Index != (ELPS_MAX_APS_PDU_ON_STATE_CHANGE - 1))
                {
                    pTmpBuf =
                        CRU_BUF_Duplicate_BufChain (pTxInfo->unBufferParam.
                                                    pPscPdu);
                    if (ElpsTxSendPscPduToMpls (pTxInfo) == OSIX_SUCCESS)
                    {
                        /* As the transmission is success, 
                         * Increment the counter. */
                        pTxInfo->u1Count++;
                    }
                    /* performance measurement
                     * the interface [on which working pseudo wire] went 
                     * down. It is captured in this node.
                     * So schedule 3 PSC pkts to indicate SF. This PSC
                     * packet transmission is noted down as "T1".
                     */
                    /* Reason to add check " (u1Index == 0)" in below condition.
                     * --------------------------------------------------------
                     * This PSC-Signal Fail packet will be sent thrice. If this
                     * check is not added, then the time of third pkt to be sent
                     * will be stored. But the requirement is to find the time
                     * difference between (the peer node changes its state on 
                     * hearing the APS-SF from remote) to (the first APS-SF 
                     * packet transmitted in the local node). To achieve this, 
                     * the check is additionaly added.
                     */
                    ELPS_GET_OFFSET_FOR_ELPS_HDR
                        (pTxInfo->unBufferParam.pPscPdu, u4Offset);
                    if ((pPgInfo != NULL) &&
                        (ELPS_PSC_IS_SF_FOR_WORKING
                         (pTxInfo->unBufferParam.pPscPdu, u4Offset)) &&
                        (u1Index == 0))
                    {

                        ElpsUtilMeasureTime (pPgInfo, ELPS_PERF_LR_SF_TX);

                    }
                    pTxInfo->unBufferParam.pPscPdu = pTmpBuf;
                }
                else
                {
                    if (ElpsTxSendPscPduToMpls (pTxInfo) == OSIX_SUCCESS)
                    {
                        /* As the transmission is success,
                         * Increment the counter. */
                        pTxInfo->u1Count++;
                    }
                }
            }
        }
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxSendPscPduToMpls
 *                                                                          
 *    DESCRIPTION      : This function transmits the MPLS Pkt to the MPLS
 *                       module for transmitting out.
 *
 *    INPUT            : pAps  - Pointer to the APS Tx Info structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
ElpsTxSendPscPduToMpls (tElpsApsTxInfo * pAps)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;

    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC, "ElpsTxSendPscPduToMpls:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    pMplsApiInInfo->u4SubReqType = 0;
    pMplsApiInInfo->u4ContextId = pAps->u4ContextId;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

    pMplsApiInInfo->InPktInfo.pBuf = pAps->unBufferParam.pPscPdu;

    pMplsApiInInfo->InPktInfo.u4OutIfIndex = pAps->u4Index;
    pMplsApiInInfo->InPktInfo.bIsMplsLabelledPacket = TRUE;
    MEMCPY (pMplsApiInInfo->InPktInfo.au1DstMac, pAps->au1DstMac, MAC_ADDR_LEN);

    /* Send Packet to MPLS */
    if (ElpsPortMplsApiHandleExtRequest (MPLS_PACKET_HANDLE_FROM_APP,
                                         pMplsApiInInfo, NULL) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC |
                   ALL_FAILURE_TRC,
                   "ElpsTxSendPscPduToMpls: Failed to get "
                   "Tunnel Info!!!!\r\n"));
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pMplsApiInInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxSendPduToCfm
 *                                                                          
 *    DESCRIPTION      : This function transmits the PDU to the CFM module 
 *                       for transmitting out.
 *
 *    INPUT            : pAps  - Pointer to the APS Tx Info structure
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
ElpsTxSendPduToCfm (tElpsApsTxInfo * pAps)
{
    tEcfmMepInfoParams  MepInfo;

    MEMSET (&MepInfo, 0, sizeof (tEcfmMepInfoParams));

    MepInfo.u4ContextId = pAps->u4ContextId;
    MepInfo.u4MdIndex = pAps->MonitorInfo.u4MegId;
    MepInfo.u4MaIndex = pAps->MonitorInfo.u4MeId;
    MepInfo.u2MepId = (UINT2) pAps->MonitorInfo.u4MepId;

    if (ElpsPortEcfmInitiateExPdu (&MepInfo, ELPS_APS_PDU_TLV_OFFSET, 0,
                                   pAps->unBufferParam.pu1ApsPdu,
                                   (ELPS_APS_PDU_LEN - ELPS_APS_PDU_PADDING),
                                   gNullMacAddr, ELPS_APS_PDU_OPCODE)
        == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "ElpsTxSendPduToCfm: APS PDU transmission Failed.\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxDelEntriesInSll
 *                                                                          
 *    DESCRIPTION      : This function deletes all the APS Tx Info entries 
 *                       present in the specified SLL.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
ElpsTxDelEntriesInSll (tTMO_SLL * pTxSll)
{
    tElpsNotifyInfo     NotifyInfo;
    tTMO_SLL_NODE      *pNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    tElpsApsTxInfo     *pTxInfo = NULL;

    TMO_DYN_SLL_Scan (pTxSll, pNode, pTempNode, tTMO_SLL_NODE *)
    {
        pTxInfo =
            (tElpsApsTxInfo *) (pNode - FSAP_OFFSETOF (tElpsApsTxInfo,
                                                       SllNode));
        TMO_SLL_Delete (pTxSll, pNode);

        /* Trap needs to be sent when none of the APS PDU is transmitted */
        if (pTxInfo->u1Count == 0)
        {
            NotifyInfo.u4ContextId = pTxInfo->u4ContextId;
            NotifyInfo.u4PgId = pTxInfo->u4PgId;
            NotifyInfo.u1PgStatus = pTxInfo->u1PgStatus;

            ElpsTrapSendTrapNotifications (&NotifyInfo,
                                           ELPS_TRAP_APS_PDU_TX_FAIL);
            ELPS_TRC ((NULL, ELPS_CRITICAL_TRC, "ElpsTxScanSllsAndTxPdu: "
                       "Failed to transmit APS PDUs!!!!\r\n"));
        }

        if (pTxInfo->u1PgServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
        {
            MemReleaseMemBlock (gElpsGlobalApsTxInfo.ApsTxPduPoolId,
                                pTxInfo->unBufferParam.pu1ApsPdu);
        }
        MemReleaseMemBlock (gElpsGlobalApsTxInfo.ApsTxInfoTablePoolId,
                            (UINT1 *) pTxInfo);
    }
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxCalculateTxEntryCount
 *                                                                          
 *    DESCRIPTION      : This function calculates the number of entries    
 *                       present in all the Tx SLLs.
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Entries Count
 *                                                                          
 ****************************************************************************/
PRIVATE VOID
ElpsTxCalculateTxEntryCount (UINT4 *pu4Count)
{
    UINT4               u4Count = 0;
    UINT1               u1Index = 0;

    for (; u1Index < ELPS_MAX_APS_PDU_ON_STATE_CHANGE; u1Index++)
    {
        u4Count += TMO_SLL_Count (gElpsGlobalApsTxInfo.pApsTxSll[u1Index]);
    }

    *pu4Count = u4Count;
    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxGetPscStateForPkt
 *                                                                          
 *    DESCRIPTION      : This function get the PSC state mentioned in draft 
 *                       for forming the TX packet. 
 *
 *    INPUT            : u1State - Psc State
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : PSC packet state 
 *                                                                          
 ****************************************************************************/
PUBLIC UINT1
ElpsTxGetPscStateForPkt (tElpsPgInfo * pPg, UINT1 u1State)
{
    UINT1               u1Value = 0;

    switch (u1State)
    {
        case ELPS_PSC_REQ_STATE_NR:

            if ((((pPg->u1LastActiveReq == ELPS_EV_FR_LO) ||
                  (pPg->u1LastActiveReq == ELPS_EV_FR_FS) ||
                  (pPg->u1LastActiveReq == ELPS_EV_FR_SF_P)) &&
                 (pPg->u1PreviousStateId == ELPS_STATE_SF_W))
                ||
                ((pPg->u1LastActiveReq == ELPS_EV_FR_LO) &&
                 (pPg->LastLocalCond.u1Request == ELPS_LOCAL_COND_SF_P)))
            {
                u1Value = ELPS_PSC_PKT_REQ_STATE_SF;
            }
            else
            {
                u1Value = ELPS_PSC_PKT_REQ_STATE_NR;
            }
            break;

        case ELPS_PSC_REQ_STATE_LO:
            if (pPg->ProtectionEntity.pCfmConfig->u1Status ==
                ELPS_CFM_SIGNAL_FAIL)
            {
                u1Value = ELPS_PSC_PKT_REQ_STATE_SF;
            }
            else
            {
                u1Value = ELPS_PSC_PKT_REQ_STATE_LO;
            }
            break;

        case ELPS_PSC_REQ_STATE_FS:
            u1Value = ELPS_PSC_PKT_REQ_STATE_FS;
            break;

        case ELPS_PSC_REQ_STATE_SF:
            u1Value = ELPS_PSC_PKT_REQ_STATE_SF;
            break;

        case ELPS_PSC_REQ_STATE_MS:
            u1Value = ELPS_PSC_PKT_REQ_STATE_MS;
            break;

        case ELPS_PSC_REQ_STATE_WTR:
            u1Value = ELPS_PSC_PKT_REQ_STATE_WTR;
            break;
        case ELPS_PSC_REQ_STATE_EXER:
            u1Value = ELPS_PSC_PKT_REQ_STATE_EXER;
            break;

        case ELPS_PSC_REQ_STATE_RR:
            u1Value = ELPS_PSC_PKT_REQ_STATE_RR;
            break;
        case ELPS_PSC_REQ_STATE_DNR:
            u1Value = ELPS_PSC_PKT_REQ_STATE_DNR;
            break;

        default:
            break;
    }

    return u1Value;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsTxGetStateForPkt
 *                                                                          
 *    DESCRIPTION      : This function get the state for forming the TX packet
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : Entries Count
 *                                                                          
 ****************************************************************************/
PRIVATE UINT1
ElpsTxGetStateForPkt (UINT1 u1State)
{
    UINT1               u1Value = 0;

    switch (u1State)
    {
        case ELPS_APS_REQ_STATE_NR:
            u1Value = ELPS_PKT_REQ_STATE_NR;
            break;

        case ELPS_APS_REQ_STATE_LO:
            u1Value = ELPS_PKT_REQ_STATE_LO;
            break;

        case ELPS_APS_REQ_STATE_FS:
            u1Value = ELPS_PKT_REQ_STATE_FS;
            break;

        case ELPS_APS_REQ_STATE_SF:
            u1Value = ELPS_PKT_REQ_STATE_SF;
            break;

        case ELPS_APS_REQ_STATE_SF_P:
            u1Value = ELPS_PKT_REQ_STATE_SF_P;
            break;

        case ELPS_APS_REQ_STATE_MS:
            u1Value = ELPS_PKT_REQ_STATE_MS;
            break;

        case ELPS_APS_REQ_STATE_MS_W:
            u1Value = ELPS_PKT_REQ_STATE_MS_W;
            break;

        case ELPS_APS_REQ_STATE_WTR:
            u1Value = ELPS_PKT_REQ_STATE_WTR;
            break;

        case ELPS_APS_REQ_STATE_EXER:
            u1Value = ELPS_PKT_REQ_STATE_EXER;
            break;

        case ELPS_APS_REQ_STATE_RR:
            u1Value = ELPS_PKT_REQ_STATE_RR;
            break;

        case ELPS_APS_REQ_STATE_DNR:
            u1Value = ELPS_PKT_REQ_STATE_DNR;
            break;

        default:
            break;
    }

    return u1Value;
}

/****************************************************************************
 *
 *   FUNCTION NAME    : ElpsTxGetPscStateForPscVersion1
 *
 *   DESCRIPTION      : This function get the PSC state mentioned in draft
 *                      for forming the TX packet.
 *
 *   INPUT            : u1State - Psc State
 *
 *   OUTPUT           : None
 *
 *   RETURNS          : PSC packet state
 *
 *****************************************************************************/
PUBLIC UINT1
ElpsTxGetPscStateForPscVersion1 (tElpsPgInfo * pPg, UINT1 u1State)
{
    UINT1               u1Value = 0;

    switch (u1State)
    {
        case ELPS_PSC_REQ_STATE_NR:

            if ((((pPg->u1LastActiveReq == ELPS_EV_FR_LO) ||
                  ((pPg->u1LastActiveReq == ELPS_EV_FR_FS) &&
                   (pPg->u1WorkingCfmStatus == ELPS_CFM_SIGNAL_FAIL)) ||
                  (pPg->u1LastActiveReq == ELPS_EV_FR_SF_P)) &&
                 (pPg->u1PreviousStateId == ELPS_STATE_SF_W))
                ||
                ((pPg->u1LastActiveReq == ELPS_EV_FR_LO) &&
                 (pPg->LastLocalCond.u1Request == ELPS_LOCAL_COND_SF_P)))
            {
                u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_SF;
            }
            else
            {
                u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_NR;
            }
            break;

        case ELPS_PSC_REQ_STATE_LO:
            if (pPg->ProtectionEntity.pCfmConfig->u1Status ==
                ELPS_CFM_SIGNAL_FAIL)
            {
                u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_SF;
            }
            else
            {
                u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_LO;
            }
            break;

        case ELPS_PSC_REQ_STATE_FS:
            u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_FS;
            break;

        case ELPS_PSC_REQ_STATE_SF:
            u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_SF;
            break;

        case ELPS_PSC_REQ_STATE_MS:
            u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_MS;
            break;

        case ELPS_PSC_REQ_STATE_WTR:
            u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_WTR;
            break;
        case ELPS_PSC_REQ_STATE_EXER:
            u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_EXER;
            break;

        case ELPS_PSC_REQ_STATE_RR:
            u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_RR;
            break;
        case ELPS_PSC_REQ_STATE_DNR:
            u1Value = ELPS_PSC_PKT_RFC_REQ_STATE_DNR;
            break;

        default:
            break;
    }

    return u1Value;
}

#endif /* _ELPSTX_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpstx.c                       */
/*-----------------------------------------------------------------------*/
