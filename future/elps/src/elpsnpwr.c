/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsnpwr.c,v 1.2 2013/01/10 12:43:27 siva Exp $
 *
 * Description: This file contains the NP wrappers for  ELPS module.
 *****************************************************************************/
/***************************************************************************
 *                                                                          
 *    Function Name       : ElpsNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tElpsNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __ELPS_NP_WR_C
#define __ELPS_NP_WR_C

#include "elpsinc.h"
#include "nputil.h"

PUBLIC UINT1
ElpsNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tElpsNpModInfo     *pElpsNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pElpsNpModInfo = &(pFsHwNp->ElpsNpModInfo);

    if (NULL == pElpsNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MI_ELPS_HW_PG_SWITCH_DATA_PATH:
        {
            tElpsNpWrFsMiElpsHwPgSwitchDataPath *pEntry = NULL;
            pEntry = &pElpsNpModInfo->ElpsNpFsMiElpsHwPgSwitchDataPath;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiElpsHwPgSwitchDataPath (pEntry->u4ContextId, pEntry->u4PgId,
                                            pEntry->pPgHwInfo);
            break;
        }
#ifdef MBSM_WANTED
        case FS_MI_ELPS_MBSM_HW_PG_SWITCH_DATA_PATH:
        {
            tElpsNpWrFsMiElpsMbsmHwPgSwitchDataPath *pEntry = NULL;
            pEntry = &pElpsNpModInfo->ElpsNpFsMiElpsMbsmHwPgSwitchDataPath;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsMiElpsMbsmHwPgSwitchDataPath (pEntry->u4ContextId,
                                                pEntry->u4PgId,
                                                pEntry->pPgHwInfo,
                                                pEntry->pSlotInfo);
            break;
        }
#endif
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}
#endif /*__ELPS_NP_WR_C */
