/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsmbsm.c,v 1.6 2014/02/24 11:30:10 siva Exp $
 *
 * Description: This file contains functions for handling card insertion 
 *              and removal in a chassis system.
 *****************************************************************************/
#ifndef _ELPSMBSM_C_
#define _ELPSMBSM_C_

#include "elpsinc.h"

/* Proto types of the functions private to this file only */
PRIVATE INT4 ElpsMbsmUpdateCardInsertOrRemove PROTO ((tMbsmSlotInfo *,
                                                      tMbsmPortInfo *, UINT4));
PRIVATE INT4 ElpsMbsmHandleCartInsertion PROTO ((tElpsPgInfo *,
                                                 tElpsHwPgSwitchInfo *,
                                                 tMbsmSlotInfo *));
PRIVATE INT4 ElpsMbsmHandleCartRemoval PROTO ((tElpsPgInfo *,
                                               tElpsHwPgSwitchInfo *,
                                               tMbsmSlotInfo *));
PRIVATE INT4 ElpsMbsmCheckForPortInPgInfo PROTO ((tElpsPgInfo *,
                                                  tMbsmPortInfo *));
PRIVATE INT4 ElpsMbsmFillServiceValAndCallNp PROTO ((tElpsPgInfo *,
                                                     tElpsHwPgSwitchInfo *,
                                                     tMbsmSlotInfo *));

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMbsmPostMessage 
 *                                                                          
 *    DESCRIPTION      : Allocates a CRU buffer and enqueues the Line card
 *                       change status to the ELPS Task
 *
 *    INPUT            : pProtoMsg - Contains the Slot and Port Information 
 *                       i4Event  - Line card Up/Down status          
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
ElpsMbsmPostMessage (tMbsmProtoMsg * pProtoMsg, INT4 i4Event)
{
    tElpsQMsg          *pMsg = NULL;
    tMbsmProtoAckMsg    MbsmProtoAckMsg;

    MEMSET (&(MbsmProtoAckMsg), 0, sizeof (tMbsmProtoAckMsg));

    if (pProtoMsg == NULL)
    {
        return MBSM_FAILURE;
    }

    if (gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                   "ElpsMbsmPostMessage: ELPS task is not initialized\r\n"));
        /* If the module is not initialized no need to proceed further, 
         * send the ack to MBSM so that other modules get the LcStatus 
         * Notification. The ack is sent only for hardware updation to 
         * be in synchronization across the dependant modules. 
         */
        MbsmProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
        MbsmProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
        MbsmProtoAckMsg.i4RetStatus = MBSM_SUCCESS;
        MbsmSendAckFromProto (&MbsmProtoAckMsg);
        return MBSM_SUCCESS;
    }

    if ((pMsg = (tElpsQMsg *)
         MemAllocMemBlk (gElpsGlobalInfo.QMsgPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsMbsmPostMessage: Allocation "
                   "of memory for Queue Message FAILED !!!\r\n"));
        return MBSM_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tElpsQMsg));
    pMsg->u4MsgType = (UINT4) i4Event;

    if ((pMsg->unMsgParam.pMbsmProtoMsg =
         MEM_MALLOC (sizeof (tMbsmProtoMsg), tMbsmProtoMsg)) == NULL)
    {
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC, "ElpsMbsmPostMessage: Allocation "
                   "of memory for MBSM information Failed\r\n"));
        MemReleaseMemBlock (gElpsGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
        return MBSM_FAILURE;
    }

    /* Copy the MBSM message */
    MEMCPY (pMsg->unMsgParam.pMbsmProtoMsg, pProtoMsg, sizeof (tMbsmProtoMsg));

    if (ElpsQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC,
                   "ElpsMbsmPostMessage: ElpsQueEnqMsg Failed !!!!\r\n"));
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMbsmHandleLCEvent
 *                                                                          
 *    DESCRIPTION      : This function programs the HW with the ELPS  
 *                       software configuration. 
 *                       This function will be called from the ELPS module
 *                       when an indication for the Card Insertion/Removal   
 *                       is received from the MBSM. 
 *
 *    INPUT            : pBuf  - Buffer containing the protocol message 
 *                               information 
 *                       u4Cmd - Line card - Insert/Remove
 *                                                                          
 *    OUTPUT           : None.
 *                                                                          
 *    RETURNS          : None.
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
ElpsMbsmHandleLCEvent (tMbsmProtoMsg * pProtoMsg, UINT4 u4Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMbsmProtoAckMsg    ProtoAckMsg;

    if ((u4Cmd == MBSM_MSG_CARD_INSERT) || (u4Cmd == MBSM_MSG_CARD_REMOVE))
    {
        if (!MBSM_SLOT_INFO_ISPORTMSG (&(pProtoMsg->MbsmSlotInfo)))
        {
            i4RetStatus = ElpsMbsmUpdateCardInsertOrRemove
                (&(pProtoMsg->MbsmSlotInfo), &(pProtoMsg->MbsmPortInfo), u4Cmd);
        }
    }

    /* Send an acknowledgment to the MBSM module to inform the completion
     * of updation of interfaces in the NP
     */
    ProtoAckMsg.i4RetStatus = i4RetStatus;
    ProtoAckMsg.i4SlotId = pProtoMsg->MbsmSlotInfo.i4SlotId;
    ProtoAckMsg.i4ProtoCookie = pProtoMsg->i4ProtoCookie;
    MbsmSendAckFromProto (&ProtoAckMsg);

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMbsmUpdateCardInsertOrRemove
 *                                                                          
 *    DESCRIPTION      : This function used to perform the necessary actions 
 *                       in HW and SW  when an indication for the 
 *                       Card Insertion/Card Removal is received from the MBSM. 
 *
 *    INPUT            : pPortInfo - Inserted port list     
 *                       pSlotInfo - Information about inserted slot
 *                       u4Event   - MBSM_MSG_CARD_INSERT / 
 *                                   MBSM_MSG_CARD_REMOVE
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : MBSM_SUCCESS/MBSM_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
ElpsMbsmUpdateCardInsertOrRemove (tMbsmSlotInfo * pSlotInfo,
                                  tMbsmPortInfo * pPortInfo, UINT4 u4Event)
{
    tElpsHwPgSwitchInfo HwPgSwitchInfo;
    tElpsPgInfo        *pPg = NULL;
    tElpsPgInfo        *pNextPg = NULL;
    tElpsServiceListPointerInfo *pPgSrvLstPtr = NULL;
    tElpsContextInfo   *pCxt = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4RetVal = 0;
    UINT1               u1BrgPortType = CFA_INVALID_BRIDGE_PORT;

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pCxt = ElpsCxtGetNode (u4ContextId);

        if (pCxt == NULL)
        {
            /* Context Not Created */
            continue;
        }

        if ((ElpsUtilIsElpsStarted (u4ContextId) == OSIX_FALSE) ||
            (pCxt->u1ModuleStatus == ELPS_DISABLED))
        {
            continue;
        }

        pNextPg = ElpsPgGetFirstNodeInContext (pCxt);

        if (pNextPg == NULL)
        {
            continue;
        }

        do
        {
            pPg = pNextPg;

            if (pPg->u1PgRowStatus != ACTIVE)
            {
                continue;
            }

            if (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
            {
                if (ElpsMbsmCheckForPortInPgInfo (pPg, pPortInfo) == OSIX_FALSE)
                {
                    continue;
                }

                HwPgSwitchInfo.PgProtType.u1ArchType = ELPS_PG_ARCH_1_TO_1;
                HwPgSwitchInfo.PgProtType.u1CommChannel =
                    ELPS_PG_COMM_CHANNEL_APS;
                HwPgSwitchInfo.PgProtType.u1Direction = ELPS_PG_BIDIRECTIONAL;
                HwPgSwitchInfo.u4PgIngressPortId = pPg->u4IngressPortId;
                HwPgSwitchInfo.u4PgWorkingPortId = pPg->WorkingEntity.u4PortId;
                HwPgSwitchInfo.u4PgProtectionPortId =
                    pPg->ProtectionEntity.u4PortId;

                ElpsUtilGetPhyPortFromSispPort (&HwPgSwitchInfo.
                                                u4PgIngressPortId);
                ElpsUtilGetPhyPortFromSispPort (&HwPgSwitchInfo.
                                                u4PgWorkingPortId);
                ElpsUtilGetPhyPortFromSispPort (&HwPgSwitchInfo.
                                                u4PgProtectionPortId);

                if (ElpsUtilGetBrgPortType (pPg->u4IngressPortId,
                                            &u1BrgPortType) == OSIX_FAILURE)
                {
                    ELPS_TRC ((pCxt, ELPS_CRITICAL_TRC,
                               "ElpsMbsmUpdateCardInsertOrRemove: "
                               "Retrieving CFA "
                               "information for the port %d Failed !!!!\r\n",
                               pPg->u4IngressPortId));
                    return MBSM_FAILURE;
                }

                HwPgSwitchInfo.u1PgIngressBrgPortType = u1BrgPortType;
            }
            else if ((pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                     (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
                /* Service Type LSP or Pseudowire */
            {
                HwPgSwitchInfo.PgProtType.u1ArchType = ELPS_PG_ARCH_1_TO_1;
                HwPgSwitchInfo.PgProtType.u1CommChannel =
                    ELPS_PG_COMM_CHANNEL_PSC;
                HwPgSwitchInfo.PgProtType.u1Direction = ELPS_PG_BIDIRECTIONAL;

                MEMSET (&HwPgSwitchInfo, 0, sizeof (tElpsHwPgSwitchInfo));

                switch (pPg->u1PgConfigType)
                {
                    case ELPS_PG_TYPE_LIST:
                        /* Get the first service list entry for this PG */
                        pPgSrvLstPtr =
                            pPg->unServiceList.pWorkingServiceListPointer;
                        while (pPgSrvLstPtr != NULL)
                        {
                            if (ElpsUtilFillHwSwitchInfo (pCxt,
                                                          &HwPgSwitchInfo,
                                                          &(pPgSrvLstPtr->
                                                            WorkingInfo),
                                                          &(pPgSrvLstPtr->
                                                            ProtectionInfo),
                                                          &(pPgSrvLstPtr->
                                                            WorkingRvrInfo),
                                                          &(pPgSrvLstPtr->
                                                            ProtectionRvrInfo),
                                                          pPg->u1ServiceType) ==
                                OSIX_FAILURE)
                            {
                                ELPS_TRC ((pCxt, ELPS_CRITICAL_TRC,
                                           "FillHWSwitchInfo Failed !!!!\r\n"));
                            }

                            i4RetVal = ElpsPortHwPgSwitchDataPath
                                (pCxt->u4ContextId, pPg->u4PgId,
                                 &HwPgSwitchInfo);

                            if (i4RetVal == OSIX_FAILURE)
                            {
                                ELPS_TRC ((pCxt, ELPS_CRITICAL_TRC,
                                           "ElpsPgSwitchDataPath Failed !!!!\r\n"));
                            }

                            pPgSrvLstPtr =
                                ElpsSrvListPtrGetNextNodeForPg (pPg->
                                                                pContextInfo,
                                                                pPg,
                                                                pPgSrvLstPtr);
                        }
                        break;

                    case ELPS_PG_TYPE_INDIVIDUAL:

                        if (ElpsUtilFillHwSwitchInfo (pCxt,
                                                      &HwPgSwitchInfo,
                                                      &(pPg->WorkingEntity.
                                                        ServiceParam),
                                                      &(pPg->ProtectionEntity.
                                                        ServiceParam),
                                                      &(pPg->
                                                        WorkingReverseEntity.
                                                        ServiceParam),
                                                      &(pPg->
                                                        ProtectionReverseEntity.
                                                        ServiceParam),
                                                      pPg->u1ServiceType) ==
                            OSIX_FAILURE)
                        {
                            ELPS_TRC ((pCxt, ELPS_CRITICAL_TRC,
                                       "FillHWSwitchInfo Failed !!!!\r\n"));
                        }

                        i4RetVal = ElpsPortHwPgSwitchDataPath
                            (pCxt->u4ContextId, pPg->u4PgId, &HwPgSwitchInfo);

                        if (i4RetVal == OSIX_FAILURE)
                        {
                            ELPS_TRC ((pCxt, ELPS_CRITICAL_TRC,
                                       "ElpsPgSwitchDataPath Failed !!!!\r\n"));
                        }
                        break;

                    default:
                        ELPS_TRC ((pCxt, ELPS_CRITICAL_TRC,
                                   "ElpsPgSwitchDataPath: "
                                   "Invalid PG Config Type\r\n"));
                        break;
                }
            }

            if (u4Event == MBSM_MSG_CARD_INSERT)
            {
                i4RetVal = ElpsMbsmHandleCartInsertion (pPg, &HwPgSwitchInfo,
                                                        pSlotInfo);
            }
            else
            {
                i4RetVal = ElpsMbsmHandleCartRemoval (pPg, &HwPgSwitchInfo,
                                                      pSlotInfo);
            }

            if (i4RetVal == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsMbsmUpdateCardInsertOrRemove: Failed to "
                           "handle Card attach/detach.\r\n"));
                return MBSM_FAILURE;
            }
        }
        while ((pNextPg = ElpsPgGetNextNodeInContext (pCxt, pPg)) != NULL);
    }

    return MBSM_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMbsmHandleCartInsertion
 *                                                                          
 *    DESCRIPTION      : This function handles the Card Insertion event.
 *
 *    INPUT            : pPg       - Pointer to the PG information.
 *                       pHwInfo   - Pointer to the tElpsHwPgSwitchInfo
 *                       pSlotInfo - Pointer to the Information about inserted 
 *                                   slot
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
ElpsMbsmHandleCartInsertion (tElpsPgInfo * pPg, tElpsHwPgSwitchInfo * pHwInfo,
                             tMbsmSlotInfo * pSlotInfo)
{
    tElpsNotifyInfo     NotifyInfo;

    ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC,
               "ElpsMbsmHandleCartInsertion: Entered for PG(id-%d)\r\n",
               pPg->u4PgId));
    /* First Call the NP with action as PG_CREATE */
    pHwInfo->u1PgAction = ELPS_PG_CREATE;

    if (ElpsMbsmFillServiceValAndCallNp (pPg, pHwInfo, pSlotInfo)
        == OSIX_FAILURE)
    {
        pPg->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
        NotifyInfo.u4ContextId = pPg->pContextInfo->u4ContextId;
        NotifyInfo.u4PgId = pPg->u4PgId;
        NotifyInfo.u1PgStatus = pPg->u1PgStatus;

        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_HW_PG_CRT_FAIL);
        ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsMbsmHandleCartInsertion: Failure happens in function "
                   "ElpsMbsmFillServiceValAndCallNp for action %d!!!!\r\n",
                   pHwInfo->u1PgAction));
        return OSIX_FAILURE;
    }

    if ((pPg->u1PgStatus == ELPS_PG_STATUS_PROT_PATH_ACTIVE) ||
        (pPg->u1PgStatus == ELPS_PG_STATUS_WTR_STATE) ||
        (pPg->u1PgStatus == ELPS_PG_STATUS_DO_NOT_REVERT))
    {
        /* If the protection path is active, we have to program the 
         * newly inserted card to work with protection path. */
        pHwInfo->u1PgAction = (UINT1) ELPS_PG_SWITCH_TO_PROTECTION;

        if (ElpsMbsmFillServiceValAndCallNp (pPg, pHwInfo, pSlotInfo)
            == OSIX_FAILURE)
        {
            pPg->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
            NotifyInfo.u4ContextId = pPg->pContextInfo->u4ContextId;
            NotifyInfo.u4PgId = pPg->u4PgId;
            NotifyInfo.u1PgStatus = pPg->u1PgStatus;

            ElpsTrapSendTrapNotifications (&NotifyInfo,
                                           ELPS_TRAP_HW_SWITCH_TO_PROT_FAIL);
            ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsMbsmHandleCartInsertion: Failure happens in "
                       "function ElpsMbsmFillServiceValAndCallNp for action "
                       "%d!!!!\r\n", pHwInfo->u1PgAction));
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMbsmHandleCartRemoval
 *                                                                          
 *    DESCRIPTION      : This function handles the Card Removal event.
 *
 *    INPUT            : pPg       - Pointer to the PG information.
 *                       pHwInfo   - Pointer to the tElpsHwPgSwitchInfo
 *                       pSlotInfo - Pointer to the Information about inserted 
 *                                   slot
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
ElpsMbsmHandleCartRemoval (tElpsPgInfo * pPg, tElpsHwPgSwitchInfo * pHwInfo,
                           tMbsmSlotInfo * pSlotInfo)
{
    tElpsNotifyInfo     NotifyInfo;

    ELPS_TRC ((pPg->pContextInfo, CONTROL_PLANE_TRC,
               "ElpsMbsmHandleCartRemoval: Entered for PG(id-%d)\r\n",
               pPg->u4PgId));
    /* Call the NP with action as PG_DELETE */
    pHwInfo->u1PgAction = ELPS_PG_DELETE;

    if (ElpsMbsmFillServiceValAndCallNp (pPg, pHwInfo, pSlotInfo)
        == OSIX_FAILURE)
    {
        pPg->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
        NotifyInfo.u4ContextId = pPg->pContextInfo->u4ContextId;
        NotifyInfo.u4PgId = pPg->u4PgId;
        NotifyInfo.u1PgStatus = pPg->u1PgStatus;

        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_HW_PG_DEL_FAIL);
        ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsMbsmHandleCartRemoval: Failure happens in function "
                   "ElpsMbsmFillServiceValAndCallNp for action %d!!!!\r\n",
                   pHwInfo->u1PgAction));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#if 0
/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMbsmCheckForMplsPortInPg 
 *                                                                          
 *    DESCRIPTION      : This function checks for the attached/detached ports 
 *                       corresponding to MPLS services in the PG information. 
 *                       If exists, then this function 
 *                       will return OSIX_TRUE, else OSIX_FALSE. 
 *
 *    INPUT            : pPg       - Pointer to the PG information.
 *                       pPortInfo - Pointer to the Information about inserted 
 *                                   ports
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
ElpsMbsmCheckForMplsPortInPgInfo (tElpsPgInfo * pPg, tMbsmPortInfo * pPortInfo)
{
    UINT1               u1Result = OSIX_FALSE;
    UINT4               u4IfIndex = 0;

    if (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        if (pPg->WorkingEntity.ServerParam.LspServiceInfo.u4TnlIngressLsrId
            != 0)
        {
            u1Result = ElpsTnlCheckPortExist (pPg->WorkingEntity.ServerParam.
                                              LspServiceInfo, pPortInfo);
            if (u1Result == OSIX_TRUE)
            {
                return OSIX_TRUE;
            }
        }
        if (pPg->ProtectionEntity.ServerParam.LspServiceInfo.u4TnlIngressLsrId
            != 0)
        {
            u1Result = ElpsTnlCheckPortExist (pPg->ProtectionEntity.ServerParam.
                                              LspServiceInfo, pPortInfo);
            if (u1Result == OSIX_TRUE)
            {
                return OSIX_TRUE;
            }
        }
        if (pPg->WorkingReverseEntity.ServerParam.LspServiceInfo.
            u4TnlIngressLsrId != 0)
        {
            u1Result = ElpsTnlCheckPortExist (pPg->WorkingReverseEntity.
                                              ServerParam.LspServiceInfo,
                                              pPortInfo);
            if (u1Result == OSIX_TRUE)
            {
                return OSIX_TRUE;
            }
        }
        if (pPg->ProtectionReverseEntity.ServerParam.LspServiceInfo.
            u4TnlIngressLsrId != 0)
        {
            u1Result = ElpsTnlCheckPortExist (pPg->ProtectionReverseEntity.
                                              ServerParam.LspServiceInfo,
                                              pPortInfo);
            if (u1Result == OSIX_TRUE)
            {
                return OSIX_TRUE;
            }
        }
    }
    else if (pPg->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        u1Result = ElpsPwCheckPortExist (pPg->WorkingEntity.ServerParam.
                                         PwServiceInfo, pPortInfo);
        if (u1Result == OSIX_TRUE)
        {
            return OSIX_TRUE;
        }

        u1Result = ElpsPwCheckPortExist (pPg->ProtectionEntity.ServerParam.
                                         PwServiceInfo, pPortInfo);
        if (u1Result == OSIX_TRUE)
        {
            return OSIX_TRUE;
        }
    }
    /* Ports in the attached SLOT is not at all relevant to this PG */
    return OSIX_FALSE;
}
#endif

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMbsmCheckForPortInPg 
 *                                                                          
 *    DESCRIPTION      : This function checks for the attached/detached ports 
 *                       in the PG information. If exists, then this function 
 *                       will return OSIX_TRUE, else OSIX_FALSE. 
 *
 *    INPUT            : pPg       - Pointer to the PG information.
 *                       pPortInfo - Pointer to the Information about inserted 
 *                                   ports
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_TRUE/OSIX_FALSE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
ElpsMbsmCheckForPortInPgInfo (tElpsPgInfo * pPg, tMbsmPortInfo * pPortInfo)
{
    UINT1               u1Result = OSIX_FALSE;
    UINT4               u4IfIndex = 0;

    if (pPg->u4IngressPortId != 0)
    {
        u4IfIndex = pPg->u4IngressPortId;
        ElpsUtilGetPhyPortFromSispPort (&u4IfIndex);

        OSIX_BITLIST_IS_BIT_SET (pPortInfo->PortList, u4IfIndex,
                                 BRG_PORT_LIST_SIZE_EXT, u1Result);

        if (u1Result == OSIX_TRUE)
        {
            return OSIX_TRUE;
        }
    }

    u4IfIndex = pPg->WorkingEntity.u4PortId;
    ElpsUtilGetPhyPortFromSispPort (&u4IfIndex);

    OSIX_BITLIST_IS_BIT_SET (pPortInfo->PortList, u4IfIndex,
                             BRG_PORT_LIST_SIZE_EXT, u1Result);

    if (u1Result == OSIX_TRUE)
    {
        return OSIX_TRUE;
    }

    u4IfIndex = pPg->ProtectionEntity.u4PortId;
    ElpsUtilGetPhyPortFromSispPort (&u4IfIndex);

    OSIX_BITLIST_IS_BIT_SET (pPortInfo->PortList,
                             u4IfIndex, BRG_PORT_LIST_SIZE_EXT, u1Result);

    if (u1Result == OSIX_TRUE)
    {
        return OSIX_TRUE;
    }

    /* Ports in the attached SLOT is not at all relevant to this PG */
    return OSIX_FALSE;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMbsmFillServiceValAndCallNp
 *                                                                          
 *    DESCRIPTION      : This function programs the NP by considering the     
 *                       configuration type of a PG.
 *
 *    INPUT            : pPg       - Pointer to the PG information.
 *                       pHwInfo   - Pointer to the tElpsHwPgSwitchInfo
 *                       pSlotInfo - Pointer to the Information about inserted 
 *                                   slot
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *                                                                          
 ****************************************************************************/
PRIVATE INT4
ElpsMbsmFillServiceValAndCallNp (tElpsPgInfo * pPg,
                                 tElpsHwPgSwitchInfo * pHwInfo,
                                 tMbsmSlotInfo * pSlotInfo)
{
    tElpsServiceListInfo *pPgSrvLst = NULL;

    if (pPg->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        pHwInfo->u4PgWorkingServiceValue = pPg->WorkingEntity.ServiceParam.
            u4ServiceId;
        pHwInfo->u4PgProtectionServiceValue =
            pPg->ProtectionEntity.ServiceParam.u4ServiceId;

        if (ElpsFsMiElpsMbsmHwPgSwitchDataPath (pPg->pContextInfo->u4ContextId,
                                                pPg->u4PgId, pHwInfo,
                                                pSlotInfo) == FNP_FAILURE)
        {
            ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsMbsmFillServiceValAndCallNp: Hardware call failed "
                       "for PG Id %d, Action %d!!!!\r\n", pPg->u4PgId,
                       pHwInfo->u1PgAction));
            return OSIX_FAILURE;
        }
    }
    else
    {
        pPgSrvLst = pPg->unServiceList.pWorkingServiceList;

        while (pPgSrvLst != NULL)
        {
            pHwInfo->u4PgWorkingServiceValue = pPgSrvLst->u4ServiceId;
            /* In this case Protection service id will be same as 
             * working service id */
            pHwInfo->u4PgProtectionServiceValue = pPgSrvLst->u4ServiceId;

            if (ElpsFsMiElpsMbsmHwPgSwitchDataPath
                (pPg->pContextInfo->u4ContextId, pPg->u4PgId, pHwInfo,
                 pSlotInfo) == FNP_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsMbsmFillServiceValAndCallNp: Hardware call "
                           "failed for PG Id %d, Action %d!!!!\r\n",
                           pPg->u4PgId, pHwInfo->u1PgAction));
                return OSIX_FAILURE;
            }

            pPgSrvLst = ElpsSrvListGetNextNodeForPg (pPg->pContextInfo,
                                                     pPg, pPgSrvLst);
        }
    }

    return OSIX_SUCCESS;
}

#endif /* _ELPSMBSM_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsmbsm.c                     */
/*-----------------------------------------------------------------------*/
