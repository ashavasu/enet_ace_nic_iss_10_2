/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsutil.c,v 1.25 2015/06/10 05:39:56 siva Exp $
 *
 * Description: This file contains the utility routines used by ELPS module.
 *****************************************************************************/
#ifndef _ELPSUTIL_C_
#define _ELPSUTIL_C_

#include "elpsinc.h"

/* Local Function Prototypes */
PRIVATE INT4        ElpsUtilUpdateDBList (tElpsPgInfo * pPgInfo,
                                          tMplsApiInInfo * pMplsApiInInfo,
                                          tMplsApiOutInfo * pMplsApiOutInfo,
                                          UINT4);

PRIVATE VOID
 
 
 
 ElpsUtilUpdateDBStatus (tMplsApiInInfo * pMplsApiInInfo,
                         unServiceParam * pServiceParam, UINT4 u4EntityType,
                         UINT4 u4ServiceType);

PRIVATE VOID        ElpsUtilFillMplsApiInfo (tMplsApiInInfo * pMplsApiInInfo,
                                             unServiceParam * pServiceParam,
                                             UINT1 u1ServiceType);

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilApsTxDbLock
 *
 * DESCRIPTION      : Function to take the mutual exclusion APS TX database 
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsUtilApsTxDbLock (VOID)
{
    if (OsixSemTake (gElpsGlobalApsTxInfo.ApsTxInfoSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilApsTxDbUnLock
 *
 * DESCRIPTION      : Function to release the mutual exclusion APS TX database 
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsUtilApsTxDbUnLock (VOID)
{
    if (OsixSemGive (gElpsGlobalApsTxInfo.ApsTxInfoSemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilIsElpsStarted
 *
 * DESCRIPTION      : This function will check whether ELPS is started in the 
 *                    System. If Started, this function will return OSIX_TRUE, 
 *                    OSIX_FALSE otherwise.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsUtilIsElpsStarted (UINT4 u4ContextId)
{
    if (u4ContextId >= ELPS_MAX_CONTEXTS)
    {
        return OSIX_FALSE;
    }

    return (((gElpsGlobalInfo.au1SystemCtrl[u4ContextId] == ELPS_START) ?
             OSIX_TRUE : OSIX_FALSE));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilValidateContextInfo
 *
 * DESCRIPTION      : This function will check whether the context is created 
 *                    and also the system control of the context. 
 *
 *                    This function will be called by the nmhGet/nmhSet
 *                    routines and this function will take care of filling the 
 *                    the SNMP error codes, CLI set error and trace messages.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.
 * 
 * RETURNS          : Pointer to the tElpsContextInfo structure.
 * 
 **************************************************************************/
PUBLIC tElpsContextInfo *
ElpsUtilValidateContextInfo (UINT4 u4ContextId, UINT4 *pu4ErrorCode)
{
    tElpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ElpsCxtGetNode (u4ContextId);

    if (pContextInfo == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Context %d not created\n", u4ContextId));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return NULL;
    }

    if (ElpsUtilIsElpsStarted (u4ContextId) == OSIX_FALSE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC, "Module is Shutdown\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MODULE_SHUTDOWN);
        return NULL;
    }

    return pContextInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilValidateCxtAndPgInfo
 *
 * DESCRIPTION      : This function will check whether the context is created 
 *                    and also the system control of the context. After that 
 *                    this function will check for the existance of PG entry 
 *                    to the given PG Id in that context.
 *
 *                    This function will be called by the nmhGet/nmhSet
 *                    routines and this function will take care of filling the 
 *                    the SNMP error codes, CLI set error and trace messages.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId      - Protection Group Identifier
 *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.
 * 
 * RETURNS          : Pointer to the tElpsPgInfo structure.
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsUtilValidateCxtAndPgInfo (UINT4 u4ContextId, UINT4 u4PgId,
                              UINT4 *pu4ErrorCode)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsPgInfo        *pPgInfo = NULL;

    pContextInfo = ElpsUtilValidateContextInfo (u4ContextId, pu4ErrorCode);

    if (pContextInfo == NULL)
    {
        return NULL;
    }

    pPgInfo = ElpsPgGetNode (pContextInfo, u4PgId);

    if (pPgInfo == NULL)
    {
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d not created\n", u4PgId));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return NULL;
    }

    return pPgInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilValidateContext
 *
 * DESCRIPTION      : This function will check whether the context Id is in 
 *                    the specified range and also check whether the context 
 *                    is created as well as the system control of the context. 
 *
 *                    This function will be called by the nmhTest/nmhValidate
 *                    routines and this function will take care of filling the 
 *                    the SNMP error codes, CLI set error and trace messages.
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsUtilValidateContext (UINT4 u4ContextId, UINT4 *pu4ErrorCode)
{
    tElpsContextInfo   *pContextInfo = NULL;

    if (u4ContextId >= ELPS_MAX_CONTEXTS)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Context Identifier %d\r\n", u4ContextId));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    pContextInfo = ElpsUtilValidateContextInfo (u4ContextId, pu4ErrorCode);

    if (pContextInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilValContextAndProtGroup
 *
 * DESCRIPTION      : This function will check whether the context Id and 
 *                    PG Id are in the specified range and also check whether 
 *                    the context is created as well as the system control 
 *                    of the context. 
 *
 *                    This function will be called by the nmhTest/nmhValidate
 *                    routines and this function will take care of filling the 
 *                    the SNMP error codes, CLI set error and trace messages.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId      - Protection Group Identifier
 *
 * OUTPUT           : pu4ErrorCode - Pointer to the error code.
 * 
 * RETURNS          : Pointer to the tElpsPgInfo structure.
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsUtilValContextAndProtGroup (UINT4 u4ContextId, UINT4 u4PgId,
                                UINT4 *pu4ErrorCode)
{
    tElpsPgInfo        *pPgInfo = NULL;

    if (u4PgId == ELPS_INIT_VAL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC, "Invalid Protection "
                   "Group Id %d\r\n", u4PgId));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return NULL;
    }

    pPgInfo = ElpsUtilValidateCxtAndPgInfo (u4ContextId, u4PgId, pu4ErrorCode);

    if (pPgInfo == NULL)
    {
        return NULL;
    }

    return pPgInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilPgCfgNotReadyToNotInServ 
 *
 * DESCRIPTION      : In this function the transition from Not Ready to 
 *                    Not In Service happens based on setting of mandatory
 *                    objects in the PG Config table.
 *
 * INPUT            : pPgConfigInfo - Protection Group Config entry
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsUtilPgCfgNotReadyToNotInServ (tElpsPgInfo * pPgConfigInfo)
{
    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        if ((pPgConfigInfo->WorkingEntity.u4PortId != 0) &&
            (pPgConfigInfo->ProtectionEntity.u4PortId != 0))
        {
            if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
            {
                if ((pPgConfigInfo->WorkingEntity.ServiceParam.u4ServiceId !=
                     0)
                    && (pPgConfigInfo->ProtectionEntity.ServiceParam.
                        u4ServiceId != 0))
                {
                    pPgConfigInfo->u1PgRowStatus = NOT_IN_SERVICE;
                }
            }
            else
            {
                pPgConfigInfo->u1PgRowStatus = NOT_IN_SERVICE;
            }
        }
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
        {
            if ((pPgConfigInfo->WorkingEntity.ServiceParam.LspServiceInfo.
                 u4TnlIngressLsrId != 0)
                && (pPgConfigInfo->ProtectionEntity.ServiceParam.
                    LspServiceInfo.u4TnlIngressLsrId != 0))
            {
                pPgConfigInfo->u1PgRowStatus = NOT_IN_SERVICE;
            }
        }
        else
        {
            pPgConfigInfo->u1PgRowStatus = NOT_IN_SERVICE;
        }
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
        {
            if ((pPgConfigInfo->WorkingEntity.ServiceParam.PwServiceInfo.
                 u4PwIndex != 0) &&
                (pPgConfigInfo->ProtectionEntity.ServiceParam.PwServiceInfo.
                 u4PwIndex != 0))
            {
                pPgConfigInfo->u1PgRowStatus = NOT_IN_SERVICE;
            }
        }
        else
        {
            pPgConfigInfo->u1PgRowStatus = NOT_IN_SERVICE;
        }
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilPgCfmNotReadyToNotInServ 
 *
 * DESCRIPTION      : In this function the transition from Not Ready to 
 *                    Not In Service happens based on setting of mandatory
 *                    objects in the PG Cfm table.
 *
 * INPUT            : pPgConfigInfo - Protection Group entry
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsUtilPgCfmNotReadyToNotInServ (tElpsPgInfo * pPg)
{
    if ((pPg->ProtectionEntity.pCfmConfig != NULL) &&
        (pPg->WorkingEntity.pCfmConfig != NULL))
    {
        if ((pPg->WorkingEntity.pCfmConfig->MonitorInfo.u4MegId != 0) &&
            (pPg->WorkingEntity.pCfmConfig->MonitorInfo.u4MeId != 0) &&
            (pPg->WorkingEntity.pCfmConfig->MonitorInfo.u4MepId != 0) &&
            (pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId != 0) &&
            (pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MeId != 0) &&
            (pPg->ProtectionEntity.pCfmConfig->MonitorInfo.u4MepId != 0))
        {
            /* All three Pg Cfm objects such as MEG, ME and MEP are mandatory
             * parameters to make the Pg Cfm entry Not In Service from 
             * Not Ready state.
             */
            pPg->u1CfmRowStatus = NOT_IN_SERVICE;
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilGetTraceInputValue 
 *
 * DESCRIPTION      : This function gives the trace string depending upon the 
 *                    trace option
 *
 * INPUT            : pu1TraceInput - trace string.
 *                    u4TraceOption  - trace option.
 * 
 * OUTPUT           : Gives the string format trace option.
 * 
 * RETURNS          : trace string length.
 *
 ****************************************************************************/
PUBLIC INT4
ElpsUtilGetTraceInputValue (UINT1 *pu1TraceInput, UINT4 u4TraceOption)
{
    UINT1               u1BitNumber = 0;
    UINT1               u1MaxBits = 0;
    UINT4               u4TrcLen = 0;

    /* trace string */
    UINT1
         
         
         
                au1TraceString[ELPS_MAX_TRC_STR_COUNT][ELPS_MAX_TRC_STR_LEN] = {
        "init-shut ", "mgmt ", "", "ctrl ", "pkt-dump ", "resource ",
        "all-fail ", "buf ", "critical"
    };

    /* All trace */
    if ((u4TraceOption & ELPS_ALL_TRC) == ELPS_ALL_TRC)
    {
        STRNCPY (pu1TraceInput, "all", STRLEN ("all"));
        u4TrcLen = STRLEN ("all");
        return u4TrcLen;
    }

    u1MaxBits = sizeof (u4TraceOption) * BITS_PER_BYTE;

    for (u1BitNumber = 0; ((u1BitNumber < u1MaxBits) &&
                           (u1BitNumber < ELPS_MAX_TRC_STR_COUNT));
         u1BitNumber++)
    {
        if ((u4TraceOption >> u1BitNumber) & OSIX_TRUE)
        {
            STRNCPY (pu1TraceInput, au1TraceString[u1BitNumber],
                     STRLEN (au1TraceString[u1BitNumber]));
            pu1TraceInput += STRLEN (au1TraceString[u1BitNumber]);
            u4TrcLen += STRLEN (au1TraceString[u1BitNumber]);
        }
    }

    return u4TrcLen;
}

/****************************************************************************
 *
 * FUNCTION NAME    : ElpsUtilGetTraceOptionValue
 *
 * DESCRIPTION      : This function process given trace input and sets the
 *                    corresponding option bit.
 *
 * INPUT            : pu1TraceInput - trace string.
 *
 * OUTPUT           : None.
 *
 * RETURNS          : Option Value.
 *
 ****************************************************************************/
PUBLIC INT4
ElpsUtilGetTraceOptionValue (UINT1 *pu1TraceInput, INT4 i4Tracelen)
{
#define ELPS_MAX_TRACE_TOKENS      12
#define ELPS_MAX_TRACE_TOKEN_SIZE  12
#define ELPS_TRACE_TOKEN_DELIMITER ' '    /* space */
    UINT1
         
         
                  aau1Tokens[ELPS_MAX_TRACE_TOKENS][ELPS_MAX_TRACE_TOKEN_SIZE];
    UINT1              *apu1Tokens[ELPS_MAX_TRACE_TOKENS];
    UINT1               u1Count = 0;
    UINT1               u1TokenCount = 0;
    UINT4               u4TraceOption = ELPS_INVALID_TRC;

    MEMSET (aau1Tokens, 0, sizeof (aau1Tokens));

    /* Enable */
    if ((STRNCMP (pu1TraceInput, "enable ", STRLEN ("enable "))) == 0)
    {
        pu1TraceInput += STRLEN ("enable ");
        i4Tracelen -= STRLEN ("enable ");
    }
    /* Disable */
    else if ((STRNCMP (pu1TraceInput, "disable ", STRLEN ("disable "))) == 0)
    {
        pu1TraceInput += STRLEN ("disable ");
        i4Tracelen -= STRLEN ("disable ");
    }
    else
    {
        return ELPS_INVALID_TRC;
    }

    if ((STRNCMP (pu1TraceInput, "all", STRLEN ("all"))) == 0)
    {
        /* For traces,
         * All trace         : all
         * All failure trace : all-fail 
         * the first 3 characters are common(all), so verify that
         * the trace length is equal to 3(strlen("all")), if yes then set
         * all trace and return, else if the trace length is greater
         * than STRLEN ("all"), then verify whether the trace is "all-fail" 
         * or not. If the trace is not "all-fail" then the trace is considered
         * as invalid invalid trace so return invalid trace */
        if (i4Tracelen == STRLEN ("all"))
        {
            u4TraceOption = ELPS_ALL_TRC;
            return u4TraceOption;
        }
        else if (i4Tracelen > ((INT4) STRLEN ("all")))
        {
            if (STRNCMP (pu1TraceInput, "all-fail", STRLEN ("all-fail")) != 0)
            {
                return ELPS_INVALID_TRC;
            }
        }
    }

    /* assign memory address for all the pointers in token array(apu1Tokens) */
    for (u1Count = 0; u1Count < ELPS_MAX_TRACE_TOKENS; u1Count++)
    {
        apu1Tokens[u1Count] = aau1Tokens[u1Count];
    }

    /* get the tokens from the trace input buffer */
    ElpsUtilSplitStrToTokens (pu1TraceInput, i4Tracelen,
                              (UINT1) ELPS_TRACE_TOKEN_DELIMITER,
                              (UINT2) ELPS_MAX_TRACE_TOKENS, apu1Tokens,
                              &u1TokenCount);

    /* get tokens one by one from the token array and set the 
     * trace options based on the tokens */
    for (u1Count = 0; ((u1Count < ELPS_MAX_TRACE_TOKENS) &&
                       (u1Count < u1TokenCount)); u1Count++)
    {
        /* set the trace option based on the give token */
        ElpsUtilSetTraceOption (apu1Tokens[u1Count], &u4TraceOption);

        /* if invalid trace option is returned by the function 
         * ElpsUtilSetTraceOption, then dont continue the for loop, just
         * return with invalid trace option */
        if (u4TraceOption == ELPS_INVALID_TRC)
        {
            return u4TraceOption;
        }
    }
    return u4TraceOption;
}

/******************************************************************************
 * Function Name      : ElpsUtilSetTraceOption 
 *
 * Description        : This function sets the trace option based on the given
 *                      trace token
 *
 * Input(s)           : apu1Token      - Poiner to token array 
 *                      pu4TraceOption - Pointer to trace option 
 *
 * Output(s)          : pu4TraceOption - Pointer to trace option 
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
PUBLIC VOID
ElpsUtilSetTraceOption (UINT1 *pu1Token, UINT4 *pu4TraceOption)
{
    if ((STRNCMP (pu1Token, "init-shut", STRLEN ("init-shut"))) == 0)
    {
        *pu4TraceOption |= INIT_SHUT_TRC;
    }
    else if ((STRNCMP (pu1Token, "mgmt", STRLEN ("mgmt"))) == 0)
    {
        *pu4TraceOption |= MGMT_TRC;
    }
    else if ((STRNCMP (pu1Token, "ctrl", STRLEN ("ctrl"))) == 0)
    {
        *pu4TraceOption |= CONTROL_PLANE_TRC;
    }
    else if ((STRNCMP (pu1Token, "pkt-dump", STRLEN ("pkt-dump"))) == 0)
    {
        *pu4TraceOption |= DUMP_TRC;
    }
    else if ((STRNCMP (pu1Token, "resource", STRLEN ("resource"))) == 0)
    {
        *pu4TraceOption |= OS_RESOURCE_TRC;
    }
    else if ((STRNCMP (pu1Token, "all-fail", STRLEN ("all-fail"))) == 0)
    {
        *pu4TraceOption |= ALL_FAILURE_TRC;
    }
    else if ((STRNCMP (pu1Token, "buf", STRLEN ("buf"))) == 0)
    {
        *pu4TraceOption |= BUFFER_TRC;
    }
    else if ((STRNCMP (pu1Token, "critical", STRLEN ("critical"))) == 0)
    {
        *pu4TraceOption |= ELPS_CRITICAL_TRC;
    }
    else if ((STRNCMP (pu1Token, "all", STRLEN ("all"))) == 0)
    {
        *pu4TraceOption = ELPS_ALL_TRC;
    }
    else
    {
        *pu4TraceOption = ELPS_INVALID_TRC;
    }
    return;
}

/******************************************************************************
 * Function Name      : ElpsUtilSplitStrToTokens 
 *
 * Description        : This function splits the given string into tokens
 *                      based on the given token delimiter and stores the
 *                      tokens in token array and returns the token array.
 *
 * Input(s)           : pu1InputStr  - Pointer to input string
 *                      i4Strlen     - String length
 *                      u1Delimiter  - Delimiter by which the token has to be
 *                                     seperated
 *                      u2MaxToken    - Max token count
 *                      apu1Token     - Poiner to token array
 *                      pu1TokenCount - Token count
 *
 * Output(s)          : apu1Token     - Poiner to token array
 *                      pu1TokenCount - Token count
 *
 * Return Value(s)    : None
 *****************************************************************************/
PUBLIC VOID
ElpsUtilSplitStrToTokens (UINT1 *pu1InputStr, INT4 i4Strlen,
                          UINT1 u1Delimiter, UINT2 u2MaxToken,
                          UINT1 *apu1Token[], UINT1 *pu1TokenCount)
{
    UINT1               u1TokenSize = 0;
    UINT1               u1TokenCount = 0;

    while ((i4Strlen > 0) && (u1TokenCount < u2MaxToken))
    {
        /* reset token size to zero */
        u1TokenSize = 0;
        /* scan for delimiter */
        while ((i4Strlen > 0) && (*(pu1InputStr + u1TokenSize) != u1Delimiter))
        {
            i4Strlen--;
            u1TokenSize++;
        }
        /* since the loop breaks whenever delimiter is found the string length
         * and token size shoud be updated once(outside the loop) by the size 
         * of delimiter(which is 1byte) */
        i4Strlen--;
        u1TokenSize++;
        /* copy the token excluding delimiter */
        STRNCPY (apu1Token[u1TokenCount], pu1InputStr,
                 (u1TokenSize - sizeof (u1Delimiter)));
        /* move the input string pointer to point to next token */
        pu1InputStr += u1TokenSize;
        /* increment the token count */
        u1TokenCount++;
    }
    /* update the token count */
    *pu1TokenCount = u1TokenCount;
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilGetBrgPortType
 *
 * DESCRIPTION      : This function will get the bridge port type of the 
 *                    given port. 
 *
 * INPUT            : u4IfIndex   - Interface Index 
 *
 * OUTPUT           : pu1BrgPortType - Pointer to the Bridge Port Type.
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsUtilGetBrgPortType (UINT4 u4IfIndex, UINT1 *pu1BrgPortType)
{
    tCfaIfInfo          IfInfo;

    if (u4IfIndex == 0)
    {
        *pu1BrgPortType = CFA_INVALID_BRIDGE_PORT;
        return OSIX_SUCCESS;
    }

    if (ElpsPortCfaGetIfInfo (u4IfIndex, &IfInfo) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsUtilGetBrgPortType:"
                   " Retrieving CFA information for the port %d "
                   "Failed !!!!\r\n", u4IfIndex));
        return OSIX_FAILURE;
    }

    *pu1BrgPortType = IfInfo.u1BrgPortType;
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilGetPhyPortFromSispPort 
 *
 * DESCRIPTION      : This function will get the Physical Port IfIndex
 *                    if the given port is Sisp Port
 *
 * INPUT            : None 
 *
 * OUTPUT           : pu4LogIfIndex - Updated Physical IfIndex 
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsUtilGetPhyPortFromSispPort (UINT4 *pu4LogIfIndex)
{
    UINT4               u4PhyIfIndex = 0;

    if (SISP_IS_LOGICAL_PORT (*pu4LogIfIndex) == VCM_TRUE)
    {
        if (ElpsPortVcmSispGetPhysicalPortOfSispPort
            (*pu4LogIfIndex, &u4PhyIfIndex) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ELPS_CRITICAL_TRC,
                       "ElpsUtilGetPhyPortFromSispPort: Retrieving Physical"
                       "IfIndex "
                       "for the sisp port %d Failed !!!!\r\n", *pu4LogIfIndex));
            return;
        }

        *pu4LogIfIndex = u4PhyIfIndex;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilGetPgIdFromPgName
 *
 * DESCRIPTION      : This function will get the Protection Group Id
 *                    based on the Protection Group Name
 *
 * INPUT            : pu1PgName - Protection Group Name
 *
 * OUTPUT           : u4PgId- Protection Group Id
 * 
 * RETURNS          : u4PgId- Protection Group Id. vlaue 0 will be 
 *                            returned in case no valid PG id found
 * 
 **************************************************************************/
PUBLIC UINT4
ElpsUtilGetPgIdFromPgName (UINT4 u4ContextId, UINT1 *pu1PgName)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    tElpsPgInfo        *pNextPgConfigInfo = NULL;

    pContextInfo = ElpsCxtGetNode (u4ContextId);

    if (pContextInfo == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Context %d not created\n", u4ContextId));
        return (0);
    }

    pPgConfigInfo = ElpsPgGetFirstNodeInContext (pContextInfo);

    if (pPgConfigInfo == NULL)
    {
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "No Protection Group in the context\n"));
        return (0);
    }

    pNextPgConfigInfo = pPgConfigInfo;

    do
    {
        pPgConfigInfo = pNextPgConfigInfo;

        if (STRNCMP (pPgConfigInfo->au1PgName, pu1PgName,
                     ELPS_PG_MAX_NAME_LEN) == 0)
        {
            return (pPgConfigInfo->u4PgId);
        }
    }
    while ((pNextPgConfigInfo = ElpsPgGetNextNodeInContext
            (pContextInfo, pPgConfigInfo)) != NULL);

    return (0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilIsHoldOffOrWtrRunning 
 *
 * DESCRIPTION      : The routine gets the remaining time for the timers of the
 *                    protection group. If they are running the timer, 
 *                    remaining time is returned.
 *
 * INPUT            : pPg - Protection Group Information
 *
 * OUTPUT           : pu1TmrType - Hold-Off/WTR
 *                    pu4RemTime - Remaining Time
 * 
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 * 
 **************************************************************************/
PUBLIC UINT1
ElpsUtilIsHoldOffOrWtrRunning (tElpsPgInfo * pPg, UINT1 *pu1TmrType,
                               UINT4 *pu4HoldwRemTime, UINT4 *pu4HoldpRemTime,
                               UINT4 *pu4WtrRemTime)
{
    TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                         &(pPg->HoldOffTimer.TimerNode), pu4HoldwRemTime);

    if (*pu4HoldwRemTime != 0)
    {
        *pu1TmrType = (*pu1TmrType | ELPS_TMR_HOLD_W);
    }

    TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                         &(pPg->ProtectionHoldOffTimer.TimerNode),
                         pu4HoldpRemTime);

    if (*pu4HoldpRemTime != 0)
    {
        *pu1TmrType = (*pu1TmrType | ELPS_TMR_HOLD_P);
    }

    TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                         &(pPg->WTRTimer.TimerNode), pu4WtrRemTime);

    if (*pu4WtrRemTime != 0)
    {
        *pu1TmrType = (*pu1TmrType | ELPS_TMR_WTR);
    }

    if (*pu1TmrType != 0)
    {
        return OSIX_TRUE;
    }
    *pu1TmrType = ELPS_MAX_TMR_TYPES;
    *pu4HoldwRemTime = 0;
    *pu4HoldpRemTime = 0;
    *pu4WtrRemTime = 0;

    return OSIX_FALSE;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilUpdateMplsDb 
 *
 * DESCRIPTION      : This function updates the status of Working Entities
 *                    to MPLS DB Module.
 *
 * INPUT            : pPgInfo - Pointer to PG Information
 *                    u4Status- Status to be Updated to DB Module
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsUtilUpdateMplsDb (tElpsPgInfo * pPgInfo, UINT4 u4Status)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tMplsPathId        *pMplsPathId = NULL;

    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, OS_RESOURCE_TRC,
                   "ElpsUtilUpdateMplsDb:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsUtilUpdateMplsDb:"
                   "Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMplsPathId = (tMplsPathId *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsPathInfoPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsUtilUpdateMplsDb:"
                   "Allocation of memory for Mpls Path Id Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));
    MEMSET (pMplsPathId, 0, sizeof (tMplsPathId));
    pMplsApiInInfo->u4ContextId = 0;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;
    pMplsApiInInfo->u4SubReqType = u4Status;

    if (pPgInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        if (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            ElpsUtilFillMplsApiInfo (pMplsApiInInfo,
                                     &pPgInfo->WorkingEntity.ServiceParam,
                                     ELPS_PG_SERVICE_TYPE_LSP);

            if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                                 pMplsApiInInfo,
                                                 pMplsApiOutInfo)
                == OSIX_FAILURE)
            {
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsPathInfoPoolId,
                                    (UINT1 *) pMplsPathId);
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "Get Tunnel Info Failed for Working Path!!!!\r\n"));
                return OSIX_FAILURE;
            }

            if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS)
            {
                if (u4Status == MPLS_LPS_PROT_AVAILABLE)
                {
                    ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                            &(pPgInfo->ProtectionEntity.
                                              ServiceParam),
                                            ELPS_ENTITY_TYPE_PROTECTION,
                                            ELPS_PG_SERVICE_TYPE_LSP);
                }

                ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                        &(pPgInfo->WorkingEntity.ServiceParam),
                                        ELPS_ENTITY_TYPE_WORKING,
                                        ELPS_PG_SERVICE_TYPE_LSP);
            }
            else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
            {
                /* Working */
                if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                    TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                {
                    ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                            &(pPgInfo->WorkingEntity.
                                              ServiceParam),
                                            ELPS_ENTITY_TYPE_WORKING,
                                            ELPS_PG_SERVICE_TYPE_LSP);
                }
                else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                         TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
                {
                    ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                            &(pPgInfo->WorkingReverseEntity.
                                              ServiceParam),
                                            ELPS_ENTITY_TYPE_WORKING,
                                            ELPS_PG_SERVICE_TYPE_LSP);
                }
                else
                    /* Uni-directional */
                {
                    ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                               "ElpsUtilUpdateMplsDb"
                               "Linear Protection Switching is "
                               "not supported for "
                               "Uni-directional Tunnels %d\r\n",
                               pPgInfo->u4PgId));
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pMplsApiOutInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsPathInfoPoolId,
                                        (UINT1 *) pMplsPathId);

                    return OSIX_FAILURE;
                }

                /* Protection */

                /* Backing up the filled info */
                MEMCPY (pMplsPathId, &(pMplsApiInInfo->InPathId),
                        sizeof (tMplsPathId));

                ElpsUtilFillMplsApiInfo (pMplsApiInInfo,
                                         &pPgInfo->ProtectionEntity.
                                         ServiceParam,
                                         ELPS_PG_SERVICE_TYPE_LSP);

                if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                                     pMplsApiInInfo,
                                                     pMplsApiOutInfo)
                    == OSIX_FAILURE)
                {
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pMplsApiOutInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsPathInfoPoolId,
                                        (UINT1 *) pMplsPathId);
                    ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                               "Get Tunnel Info Failed for "
                               "Protection Path !!!!\r\n"));
                    return OSIX_FAILURE;
                }

                /* Restoring the original info */
                MEMCPY (&(pMplsApiInInfo->InPathId), pMplsPathId,
                        sizeof (tMplsPathId));

                if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                    TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                {
                    if (u4Status == MPLS_LPS_PROT_AVAILABLE)
                    {
                        ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                                &(pPgInfo->ProtectionEntity.
                                                  ServiceParam),
                                                ELPS_ENTITY_TYPE_PROTECTION,
                                                ELPS_PG_SERVICE_TYPE_LSP);
                    }
                }
                else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                         TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
                {
                    if (u4Status == MPLS_LPS_PROT_AVAILABLE)
                    {
                        ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                                &(pPgInfo->
                                                  ProtectionReverseEntity.
                                                  ServiceParam),
                                                ELPS_ENTITY_TYPE_PROTECTION,
                                                ELPS_PG_SERVICE_TYPE_LSP);
                    }
                }
                else
                    /* Uni-directional */
                {
                    ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                               "ElpsUtilUpdateMplsDb"
                               "Linear Protection Switching is not "
                               "supported for "
                               "Uni-directional Tunnels %d\r\n",
                               pPgInfo->u4PgId));
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pMplsApiOutInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsPathInfoPoolId,
                                        (UINT1 *) pMplsPathId);

                    return OSIX_FAILURE;
                }
            }

            pMplsApiInInfo->u4ContextId = 0;
            pMplsApiInInfo->u4SrcModId = ELPS_MODULE;
            pMplsApiInInfo->u4SubReqType = u4Status;

            if (ELPS_GET_PG_B_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
            {
                pMplsApiInInfo->InElpsMode = ELPS_PG_ARCH_1_TO_1;
            }
            else
            {
                pMplsApiInInfo->InElpsMode = ELPS_PG_ARCH_1_PLUS_1;
            }

            if (ElpsPortMplsApiHandleExtRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                                 pMplsApiInInfo,
                                                 NULL) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsUtilUpdateMplsDb"
                           "MPLS Database Update Failed"
                           "%d\r\n", pPgInfo->u4PgId));
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsPathInfoPoolId,
                                    (UINT1 *) pMplsPathId);
                return OSIX_FAILURE;
            }
        }
        else                    /* Pseudowire */
        {
            if (u4Status == MPLS_LPS_PROT_AVAILABLE)
            {
                ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                        &(pPgInfo->ProtectionEntity.
                                          ServiceParam),
                                        ELPS_ENTITY_TYPE_PROTECTION,
                                        ELPS_PG_SERVICE_TYPE_PW);

            }
            ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                    &(pPgInfo->WorkingEntity.ServiceParam),
                                    ELPS_ENTITY_TYPE_WORKING,
                                    ELPS_PG_SERVICE_TYPE_PW);
            if (ELPS_GET_PG_B_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
            {
                pMplsApiInInfo->InElpsMode = ELPS_PG_ARCH_1_TO_1;
            }
            else
            {
                pMplsApiInInfo->InElpsMode = ELPS_PG_ARCH_1_PLUS_1;
            }
            if (ElpsPortMplsApiHandleExtRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                                 pMplsApiInInfo,
                                                 NULL) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsUtilUpdateMplsDb"
                           "MPLS Database Update Failed"
                           "%d\r\n", pPgInfo->u4PgId));
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsPathInfoPoolId,
                                    (UINT1 *) pMplsPathId);

                return OSIX_FAILURE;

            }
        }
    }
    else if (pPgInfo->u1PgConfigType == ELPS_PG_TYPE_LIST)
    {
        if (ElpsUtilUpdateDBList (pPgInfo, pMplsApiInInfo, pMplsApiOutInfo,
                                  u4Status) == OSIX_FAILURE)
        {
            /* The allocated memory is freed inside the ElpsUtilUpdateDBList
             * function during failure */
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsPathInfoPoolId,
                                (UINT1 *) pMplsPathId);

            return OSIX_FAILURE;
        }
    }

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pMplsApiOutInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsPathInfoPoolId,
                        (UINT1 *) pMplsPathId);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilUpdateDBList
 *
 * DESCRIPTION      : This function updates the status of Working Entities
 *                    present in given Protection Group to MPLS DB Module.
 *
 * INPUT            : pPgInfo- Pointer to PG Information
 *                    pMplsApiInInfo- Pointer to MplsApiInInfo
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PRIVATE INT4
ElpsUtilUpdateDBList (tElpsPgInfo * pPgInfo,
                      tMplsApiInInfo * pMplsApiInInfo,
                      tMplsApiOutInfo * pMplsApiOutInfo, UINT4 u4Status)
{
    tElpsServiceListPointerInfo *pElpsServiceListPointerInfo = NULL;
    tMplsPathId         MplsPathId;

    pElpsServiceListPointerInfo =
        ElpsSrvListPtrGetFirstNode (pPgInfo->pContextInfo);

    /* Loop until a service list entry is found for the given PG */
    while ((pElpsServiceListPointerInfo != NULL) &&
           (pElpsServiceListPointerInfo->u4PgId != pPgInfo->u4PgId))
    {
        pElpsServiceListPointerInfo =
            ElpsSrvListPtrGetNextNode (pPgInfo->pContextInfo,
                                       pElpsServiceListPointerInfo);
    }

    if (pElpsServiceListPointerInfo == NULL)
    {
        /* No list entries present for the given PG */
        return OSIX_SUCCESS;
    }

    do
    {
        if (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            ElpsUtilFillMplsApiInfo (pMplsApiInInfo,
                                     &(pElpsServiceListPointerInfo->
                                       WorkingInfo), ELPS_PG_SERVICE_TYPE_LSP);

            if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                                 pMplsApiInInfo,
                                                 pMplsApiOutInfo)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "Get Tunnel Info Failed for Working Path!!!!\r\n"));
                return OSIX_FAILURE;
            }

            if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS)
            {
                if (u4Status == MPLS_LPS_PROT_AVAILABLE)
                {
                    ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                            &(pElpsServiceListPointerInfo->
                                              ProtectionInfo),
                                            ELPS_ENTITY_TYPE_PROTECTION,
                                            ELPS_PG_SERVICE_TYPE_LSP);
                }

                ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                        &(pElpsServiceListPointerInfo->
                                          WorkingInfo),
                                        ELPS_ENTITY_TYPE_WORKING,
                                        ELPS_PG_SERVICE_TYPE_LSP);
            }
            else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
            {
                /* Working */
                if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                    TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                {
                    ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                            &(pElpsServiceListPointerInfo->
                                              WorkingInfo),
                                            ELPS_ENTITY_TYPE_WORKING,
                                            ELPS_PG_SERVICE_TYPE_LSP);
                }
                else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                         TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
                {
                    ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                            &(pElpsServiceListPointerInfo->
                                              WorkingRvrInfo),
                                            ELPS_ENTITY_TYPE_WORKING,
                                            ELPS_PG_SERVICE_TYPE_LSP);
                }
                else
                    /* Uni-directional */
                {
                    ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                               "ElpsUtilUpdateMplsDb"
                               "Linear Protection Switching is "
                               "not supported for "
                               "Uni-directional Tunnels %d\r\n",
                               pPgInfo->u4PgId));

                    return OSIX_FAILURE;
                }

                /* Protection */

                /* Backing up the filled info */
                MEMCPY (&MplsPathId, &(pMplsApiInInfo->InPathId),
                        sizeof (tMplsPathId));

                ElpsUtilFillMplsApiInfo (pMplsApiInInfo,
                                         &(pElpsServiceListPointerInfo->
                                           ProtectionInfo),
                                         ELPS_PG_SERVICE_TYPE_LSP);

                if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                                     pMplsApiInInfo,
                                                     pMplsApiOutInfo)
                    == OSIX_FAILURE)
                {
                    ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                               "Get Tunnel Info Failed for Protection "
                               "Path !!!!\r\n"));
                    return OSIX_FAILURE;
                }

                /* Restoring the original info */
                MEMCPY (&(pMplsApiInInfo->InPathId), &MplsPathId,
                        sizeof (tMplsPathId));

                if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                    TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                {
                    if (u4Status == MPLS_LPS_PROT_AVAILABLE)
                    {
                        ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                                &(pElpsServiceListPointerInfo->
                                                  ProtectionInfo),
                                                ELPS_ENTITY_TYPE_PROTECTION,
                                                ELPS_PG_SERVICE_TYPE_LSP);
                    }
                }
                else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                         TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
                {
                    if (u4Status == MPLS_LPS_PROT_AVAILABLE)
                    {
                        ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                                &(pElpsServiceListPointerInfo->
                                                  ProtectionRvrInfo),
                                                ELPS_ENTITY_TYPE_PROTECTION,
                                                ELPS_PG_SERVICE_TYPE_LSP);
                    }
                }
                else
                    /* Uni-directional */
                {
                    ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                               "ElpsUtilUpdateMplsDb"
                               "Linear Protection Switching is "
                               "not supported for "
                               "Uni-directional Tunnels %d\r\n",
                               pPgInfo->u4PgId));
                    return OSIX_FAILURE;
                }
            }

            pMplsApiInInfo->u4ContextId = 0;
            pMplsApiInInfo->u4SrcModId = ELPS_MODULE;
            pMplsApiInInfo->u4SubReqType = u4Status;

            if (ELPS_GET_PG_B_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
            {
                pMplsApiInInfo->InElpsMode = ELPS_PG_ARCH_1_TO_1;
            }
            else
            {
                pMplsApiInInfo->InElpsMode = ELPS_PG_ARCH_1_PLUS_1;
            }

            if (ElpsPortMplsApiHandleExtRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                                 pMplsApiInInfo,
                                                 NULL) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsUtilUpdateMplsDb"
                           "MPLS Database Update Failed"
                           "%d\r\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
        else                    /* Pseudowire */
        {
            if (u4Status == MPLS_LPS_PROT_AVAILABLE)
            {
                ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                        &(pElpsServiceListPointerInfo->
                                          ProtectionInfo),
                                        ELPS_ENTITY_TYPE_PROTECTION,
                                        ELPS_PG_SERVICE_TYPE_PW);

            }
            ElpsUtilUpdateDBStatus (pMplsApiInInfo,
                                    &(pElpsServiceListPointerInfo->WorkingInfo),
                                    ELPS_ENTITY_TYPE_WORKING,
                                    ELPS_PG_SERVICE_TYPE_PW);
            if (ELPS_GET_PG_B_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
            {
                pMplsApiInInfo->InElpsMode = ELPS_PG_ARCH_1_TO_1;
            }
            else
            {
                pMplsApiInInfo->InElpsMode = ELPS_PG_ARCH_1_PLUS_1;
            }
            if (ElpsPortMplsApiHandleExtRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                                 pMplsApiInInfo,
                                                 NULL) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsUtilUpdateMplsDb"
                           "MPLS Database Update Failed"
                           "%d\r\n", pPgInfo->u4PgId));
                return OSIX_FAILURE;

            }
        }

        /* Get the next service list entry for the given PG */
        pElpsServiceListPointerInfo =
            ElpsSrvListPtrGetNextNodeForPg (pPgInfo->pContextInfo,
                                            pPgInfo,
                                            pElpsServiceListPointerInfo);
    }
    while (pElpsServiceListPointerInfo != NULL);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilUpdateDBStatus
 *
 * DESCRIPTION      : This function fills in the MplsApiInInfo structure
 *                    and calls the MPLS API to updates the status of
 *                    given Entity.
 *
 * INPUT            : pMplsApiInInfo- Pointer to MplsApiInInfo
 *                    pServiceParam- Pointer to Service Parameters
 *                    u4ServiceType- Service Type of Protection Group
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsUtilUpdateDBStatus (tMplsApiInInfo * pMplsApiInInfo,
                        unServiceParam * pServiceParam, UINT4 u4EntityType,
                        UINT4 u4ServiceType)
{
    tMplsPathId        *pPathId = NULL;

    if (u4EntityType == ELPS_ENTITY_TYPE_WORKING)
    {
        pPathId = &(pMplsApiInInfo->InPathId);
    }
    else if (u4EntityType == ELPS_ENTITY_TYPE_PROTECTION)
    {
        pPathId = &(pMplsApiInInfo->InServicePathId);
    }
    if (pPathId == NULL)
    {
        return;
    }
    if (u4ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        pPathId->u4PathType = MPLS_PATH_TYPE_TUNNEL;

        pPathId->TnlId.u4SrcTnlNum = pServiceParam->LspServiceInfo.u4TnlIndex;

        pPathId->TnlId.u4LspNum = pServiceParam->LspServiceInfo.u4TnlInstance;

        pPathId->TnlId.SrcNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
        pPathId->TnlId.SrcNodeId.MplsRouterId.u4_addr[0] =
            pServiceParam->LspServiceInfo.u4TnlIngressLsrId;

        pPathId->TnlId.DstNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;

        pPathId->TnlId.DstNodeId.MplsRouterId.u4_addr[0] =
            pServiceParam->LspServiceInfo.u4TnlEgressLsrId;
    }
    else
    {
        pPathId->u4PathType = MPLS_PATH_TYPE_PW;
        pPathId->PwId.u4VcId = pServiceParam->PwServiceInfo.u4VcId;
    }

}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilFillHwSwitchInfo
 *
 * DESCRIPTION      : This function is called to fill the Hardware
                      Switch Information from the Service Values
 
 * INPUT            : 
                      pElpsHwPgSwitchInfo - Pointer To Hardware Switch Info
                      pWorkingService - Working Service Pointer
                      pProtectionService - Protection Service Pointer
                      pReverseWorkingService - Reverse Working Service 
                                               Pointer
                      pReverseProtectionService - Reverse Protection Service
                                                  Pointer
                      u1ServiceType - Protection Group Service Type
 
 * OUTPUT           : None
  
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
  
 **************************************************************************/
PUBLIC INT4
ElpsUtilFillHwSwitchInfo (tElpsContextInfo * pContextInfo,
                          tElpsHwPgSwitchInfo * pElpsHwPgSwitchInfo,
                          unServiceParam * pWorkingService,
                          unServiceParam * pProtectionService,
                          unServiceParam * pReverseWorkingService,
                          unServiceParam * pReverseProtectionService,
                          UINT1 u1ServiceType)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    UINT1               u1Request = 0;
    tMplsOutSegInfo    *pOutSegInfo = NULL;
    tMplsInSegInfo     *pInSegInfo = NULL;

    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (pContextInfo);

    if (u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        u1Request = MPLS_GET_TUNNEL_INFO;
    }
    else if (u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        u1Request = MPLS_GET_PW_INFO;
    }
    pElpsHwPgSwitchInfo->u1ServiceType = u1ServiceType;

    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC, "ElpsUtilFillHwSwitchInfo:"
                   "Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC, "ElpsUtilFillHwSwitchInfo:"
                   "Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    ElpsUtilFillMplsApiInfo (pMplsApiInInfo, pWorkingService, u1ServiceType);

    if (ElpsPortMplsApiHandleExtRequest (u1Request,
                                         pMplsApiInInfo,
                                         pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                   "Get Tunnel Info Failed for Working Path!!!!\r\n"));
        return OSIX_FAILURE;
    }

    if (u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        /* WorkingEntity */
        pElpsHwPgSwitchInfo->u4WorkingPwIfIndex =
            pMplsApiOutInfo->OutPwInfo.u4PwIfIndex;
        pElpsHwPgSwitchInfo->u4WorkingPwVcIndex =
            pMplsApiOutInfo->OutPwInfo.u4PwVcIndex;
        pElpsHwPgSwitchInfo->u4WorkingVpnId =
            pMplsApiOutInfo->OutPwInfo.u4VpnId;
        MEMCPY (pElpsHwPgSwitchInfo->au1WorkingNextHopMac,
                pMplsApiOutInfo->OutPwInfo.au1NextHopMac, MAC_ADDR_LEN);

        pElpsHwPgSwitchInfo->u4PgWorkingServiceValue =
            pMplsApiOutInfo->OutPwInfo.u4OutVcLabel;

        pElpsHwPgSwitchInfo->u4PgWorkingPortId =
            pMplsApiOutInfo->OutPwInfo.u4OutIf;
        pElpsHwPgSwitchInfo->u2PgWorkingVlan =
            pMplsApiOutInfo->OutPwInfo.u2VlanId;
        pElpsHwPgSwitchInfo->u4PgWorkingPhyPort =
            pMplsApiOutInfo->OutPwInfo.u4PhyPort;

        /* Protection Entity */
        MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
        MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

        ElpsUtilFillMplsApiInfo (pMplsApiInInfo, pProtectionService,
                                 u1ServiceType);

        if (ElpsPortMplsApiHandleExtRequest (u1Request,
                                             pMplsApiInInfo,
                                             pMplsApiOutInfo) == OSIX_FAILURE)

            pElpsHwPgSwitchInfo->u4ProtectionPwIfIndex =
                pMplsApiOutInfo->OutPwInfo.u4PwIfIndex;
        pElpsHwPgSwitchInfo->u4ProtectionPwVcIndex =
            pMplsApiOutInfo->OutPwInfo.u4PwVcIndex;
        pElpsHwPgSwitchInfo->u4ProtectionVpnId =
            pMplsApiOutInfo->OutPwInfo.u4VpnId;

        MEMCPY (pElpsHwPgSwitchInfo->au1ProtectionNextHopMac,
                pMplsApiOutInfo->OutPwInfo.au1NextHopMac, MAC_ADDR_LEN);

        pElpsHwPgSwitchInfo->u4PgProtectionServiceValue =
            pMplsApiOutInfo->OutPwInfo.u4OutVcLabel;

        pElpsHwPgSwitchInfo->u4PgProtectionPortId =
            pMplsApiOutInfo->OutPwInfo.u4OutIf;
        pElpsHwPgSwitchInfo->u2PgProtectionVlan =
            pMplsApiOutInfo->OutPwInfo.u2VlanId;
        pElpsHwPgSwitchInfo->u4PgProtectionPhyPort =
            pMplsApiOutInfo->OutPwInfo.u4PhyPort;

    }
    else if (u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)    /* LSP */
    {
        /* WorkingEntity */
        if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
            TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        {
            if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
            {
                pOutSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo;
                pInSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo;
            }
            else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS)
            {
                pOutSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;
                pInSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevInSegInfo;
            }
            else
            {
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsUtilFillHwSwitchInfo: Protection"
                           "Switching is not"
                           "supported for this tunnel Role  !!!!\r\n"));
                return OSIX_FAILURE;
            }
            pElpsHwPgSwitchInfo->u4PgWorkingPortId = pOutSegInfo->u4OutIf;
            pElpsHwPgSwitchInfo->u4PgWorkingServiceValue = pOutSegInfo->
                LblInfo[0].u4Label;
            pElpsHwPgSwitchInfo->u2PgWorkingVlan = pOutSegInfo->u2VlanId;
            pElpsHwPgSwitchInfo->u4PgWorkingPhyPort = pOutSegInfo->u4PhyPort;
            MEMCPY (pElpsHwPgSwitchInfo->au1WorkingNextHopMac,
                    pMplsApiOutInfo->OutTeTnlInfo.au1NextHopMac, MAC_ADDR_LEN);

            pElpsHwPgSwitchInfo->u4PgWorkingReversePathPortId =
                pInSegInfo->u4InIf;
            pElpsHwPgSwitchInfo->u4PgWorkingReverseServiceValue =
                pInSegInfo->LblInfo[0].u4Label;
            pElpsHwPgSwitchInfo->u1WorkTnlHwStatus =
                pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus;
        }
        else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                 TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
        {
            if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
            {
                pInSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo;

                pElpsHwPgSwitchInfo->u4PgWorkingReversePathPortId =
                    pInSegInfo->u4InIf;
                pElpsHwPgSwitchInfo->u4PgWorkingReverseServiceValue =
                    pInSegInfo->LblInfo[0].u4Label;

                ElpsUtilFillMplsApiInfo (pMplsApiInInfo, pReverseWorkingService,
                                         u1ServiceType);

                if (ElpsPortMplsApiHandleExtRequest (u1Request,
                                                     pMplsApiInInfo,
                                                     pMplsApiOutInfo) ==
                    OSIX_FAILURE)
                {
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pMplsApiOutInfo);
                    ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                               "Get Tunnel Info Failed for Working "
                               "Path!!!!\r\n"));
                    return OSIX_FAILURE;
                }
                pOutSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;

                pElpsHwPgSwitchInfo->u4PgWorkingPortId = pOutSegInfo->u4OutIf;
                pElpsHwPgSwitchInfo->u4PgWorkingServiceValue = pOutSegInfo->
                    LblInfo[0].u4Label;
                MEMCPY (pElpsHwPgSwitchInfo->au1WorkingNextHopMac,
                        pMplsApiOutInfo->OutTeTnlInfo.au1NextHopMac,
                        MAC_ADDR_LEN);
                pElpsHwPgSwitchInfo->u2PgWorkingVlan = pOutSegInfo->u2VlanId;
                pElpsHwPgSwitchInfo->u4PgWorkingPhyPort =
                    pOutSegInfo->u4PhyPort;
                pElpsHwPgSwitchInfo->u1WorkTnlHwStatus =
                    pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus;
            }
            else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS)
            {
                pOutSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;

                pElpsHwPgSwitchInfo->u4PgWorkingPortId = pOutSegInfo->u4OutIf;
                pElpsHwPgSwitchInfo->u4PgWorkingServiceValue = pOutSegInfo->
                    LblInfo[0].u4Label;
                MEMCPY (pElpsHwPgSwitchInfo->au1WorkingNextHopMac,
                        pMplsApiOutInfo->OutTeTnlInfo.au1NextHopMac,
                        MAC_ADDR_LEN);

                ElpsUtilFillMplsApiInfo (pMplsApiInInfo, pReverseWorkingService,
                                         u1ServiceType);

                if (ElpsPortMplsApiHandleExtRequest (u1Request,
                                                     pMplsApiInInfo,
                                                     pMplsApiOutInfo) ==
                    OSIX_FAILURE)
                {
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pMplsApiOutInfo);
                    ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                               "Get Tunnel Info Failed for Working "
                               "Path!!!!\r\n"));
                    return OSIX_FAILURE;
                }

                pInSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo;

                pElpsHwPgSwitchInfo->u4PgWorkingReversePathPortId =
                    pInSegInfo->u4InIf;
                pElpsHwPgSwitchInfo->u4PgWorkingReverseServiceValue =
                    pInSegInfo->LblInfo[0].u4Label;
                pElpsHwPgSwitchInfo->u2PgWorkingVlan = pOutSegInfo->u2VlanId;
                pElpsHwPgSwitchInfo->u4PgWorkingPhyPort =
                    pOutSegInfo->u4PhyPort;
                pElpsHwPgSwitchInfo->u1WorkTnlHwStatus =
                    pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus;

            }
            else
            {
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsUtilFillHwSwitchInfo: Protection Switching is not"
                           "supported for this working tunnel Role  !!!!\r\n"));
                return OSIX_FAILURE;
            }
        }
        else
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            ELPS_TRC ((pContextInfo, CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "ElpsUtilFillHwSwitchInfo: Uni Directional Protection"
                       "Switching is not supported !!!!\r\n"));
            return OSIX_FAILURE;
        }

        /* Protection Entity */
        MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
        MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

        ElpsUtilFillMplsApiInfo (pMplsApiInInfo, pProtectionService,
                                 u1ServiceType);

        if (ElpsPortMplsApiHandleExtRequest (u1Request,
                                             pMplsApiInInfo,
                                             pMplsApiOutInfo) == OSIX_FAILURE)
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                       "Get Tunnel Info Failed for Protection Path!!!!\r\n"));
            return OSIX_FAILURE;
        }
        if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
            TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        {
            if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
            {
                pOutSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo;
                pInSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo;
            }
            else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS)
            {
                pOutSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;
                pInSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.RevInSegInfo;
            }
            else
            {
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsUtilFillHwSwitchInfo: Protection Switching is not"
                           "supported for this protection "
                           "tunnel Role  !!!!\r\n"));
                return OSIX_FAILURE;
            }
            pElpsHwPgSwitchInfo->u4PgProtectionPortId = pOutSegInfo->u4OutIf;
            pElpsHwPgSwitchInfo->u4PgProtectionServiceValue = pOutSegInfo->
                LblInfo[0].u4Label;
            MEMCPY (pElpsHwPgSwitchInfo->au1ProtectionNextHopMac,
                    pMplsApiOutInfo->OutTeTnlInfo.au1NextHopMac, MAC_ADDR_LEN);
            pElpsHwPgSwitchInfo->u2PgProtectionVlan = pOutSegInfo->u2VlanId;
            pElpsHwPgSwitchInfo->u4PgProtectionPhyPort = pOutSegInfo->u4PhyPort;

            pElpsHwPgSwitchInfo->u4PgProtectionReversePathPortId =
                pInSegInfo->u4InIf;
            pElpsHwPgSwitchInfo->u4PgProtectionReverseServiceValue =
                pInSegInfo->LblInfo[0].u4Label;

            pElpsHwPgSwitchInfo->u1ProtTnlHwStatus =
                pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus;
        }
        else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlMode ==
                 TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
        {
            if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS)
            {
                pInSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo;

                pElpsHwPgSwitchInfo->u4PgProtectionReversePathPortId =
                    pInSegInfo->u4InIf;
                pElpsHwPgSwitchInfo->u4PgProtectionReverseServiceValue =
                    pInSegInfo->LblInfo[0].u4Label;

                ElpsUtilFillMplsApiInfo (pMplsApiInInfo,
                                         pReverseProtectionService,
                                         u1ServiceType);

                if (ElpsPortMplsApiHandleExtRequest (u1Request,
                                                     pMplsApiInInfo,
                                                     pMplsApiOutInfo) ==
                    OSIX_FAILURE)
                {
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pMplsApiOutInfo);
                    ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                               "Get Tunnel Info Failed for "
                               "Protection Path!!!!\r\n"));
                    return OSIX_FAILURE;
                }
                pOutSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;

                pElpsHwPgSwitchInfo->u4PgProtectionPortId =
                    pOutSegInfo->u4OutIf;
                pElpsHwPgSwitchInfo->u4PgProtectionServiceValue =
                    pOutSegInfo->LblInfo[0].u4Label;
                MEMCPY (pElpsHwPgSwitchInfo->au1ProtectionNextHopMac,
                        pMplsApiOutInfo->OutTeTnlInfo.au1NextHopMac,
                        MAC_ADDR_LEN);
                pElpsHwPgSwitchInfo->u2PgProtectionVlan = pOutSegInfo->u2VlanId;
                pElpsHwPgSwitchInfo->u4PgProtectionPhyPort =
                    pOutSegInfo->u4PhyPort;
                pElpsHwPgSwitchInfo->u1ProtTnlHwStatus =
                    pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus;

            }
            else if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS)
            {
                pOutSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo;

                pElpsHwPgSwitchInfo->u4PgProtectionPortId =
                    pOutSegInfo->u4OutIf;
                pElpsHwPgSwitchInfo->u4PgProtectionServiceValue =
                    pOutSegInfo->LblInfo[0].u4Label;
                MEMCPY (pElpsHwPgSwitchInfo->au1ProtectionNextHopMac,
                        pMplsApiOutInfo->OutTeTnlInfo.au1NextHopMac,
                        MAC_ADDR_LEN);

                ElpsUtilFillMplsApiInfo (pMplsApiInInfo,
                                         pReverseProtectionService,
                                         u1ServiceType);

                if (ElpsPortMplsApiHandleExtRequest (u1Request,
                                                     pMplsApiInInfo,
                                                     pMplsApiOutInfo) ==
                    OSIX_FAILURE)
                {
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pMplsApiOutInfo);
                    ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                               "Get Tunnel Info Failed for "
                               "Protection Path!!!!\r\n"));
                    return OSIX_FAILURE;
                }

                pInSegInfo =
                    &pMplsApiOutInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo;

                pElpsHwPgSwitchInfo->u4PgProtectionReversePathPortId =
                    pInSegInfo->u4InIf;
                pElpsHwPgSwitchInfo->u4PgProtectionReverseServiceValue =
                    pInSegInfo->LblInfo[0].u4Label;
                pElpsHwPgSwitchInfo->u2PgProtectionVlan = pOutSegInfo->u2VlanId;
                pElpsHwPgSwitchInfo->u4PgProtectionPhyPort =
                    pOutSegInfo->u4PhyPort;
                pElpsHwPgSwitchInfo->u1ProtTnlHwStatus =
                    pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus;

            }
            else
            {
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsUtilFillHwSwitchInfo: Protection Switching is not"
                           "supported for this protection "
                           "tunnel Role  !!!!\r\n"));
                return OSIX_FAILURE;
            }
        }
        else
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            ELPS_TRC ((pContextInfo, CONTROL_PLANE_TRC |
                       ALL_FAILURE_TRC,
                       "ElpsUtilFillHwSwitchInfo: Uni Directional Protection"
                       "Switching is not supported !!!!\r\n"));
            return OSIX_FAILURE;
        }
    }

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pMplsApiOutInfo);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilFillMplsApiInfo 
 *
 * DESCRIPTION      : Fills the required information in tMplsApiInInfo 
 
 * INPUT            : 
                      pMplsApiInInfo - Pointer to MplsApiInInfo
                      pServiceParam - Service Parameter
                      u1ServiceType - Protection Group Service Type
 
 * OUTPUT           : None
  
 * RETURNS          : None 
  
 **************************************************************************/
PRIVATE VOID
ElpsUtilFillMplsApiInfo (tMplsApiInInfo * pMplsApiInInfo,
                         unServiceParam * pServiceParam, UINT1 u1ServiceType)
{
    pMplsApiInInfo->u4SubReqType = 0;
    pMplsApiInInfo->u4ContextId = 0;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

    if (u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        (pMplsApiInInfo->InPathId).u4PathType = MPLS_PATH_TYPE_TUNNEL;
        (pMplsApiInInfo->InPathId).TnlId.u4SrcTnlNum =
            pServiceParam->LspServiceInfo.u4TnlIndex;
        (pMplsApiInInfo->InPathId).TnlId.u4LspNum =
            pServiceParam->LspServiceInfo.u4TnlInstance;

        (pMplsApiInInfo->InPathId).TnlId.SrcNodeId.u4NodeType =
            MPLS_ADDR_TYPE_IPV4;
        (pMplsApiInInfo->InPathId).TnlId.SrcNodeId.MplsRouterId.u4_addr[0] =
            pServiceParam->LspServiceInfo.u4TnlIngressLsrId;

        (pMplsApiInInfo->InPathId).TnlId.DstNodeId.u4NodeType =
            MPLS_ADDR_TYPE_IPV4;

        (pMplsApiInInfo->InPathId).TnlId.DstNodeId.MplsRouterId.u4_addr[0] =
            pServiceParam->LspServiceInfo.u4TnlEgressLsrId;
    }
    else if (u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)    /* Pseudowire */
    {
        pMplsApiInInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
        pMplsApiInInfo->u4ContextId = 0;
        pMplsApiInInfo->u4SrcModId = ELPS_MODULE;
        (pMplsApiInInfo->InPathId).u4PathType = MPLS_PATH_TYPE_PW;
        (pMplsApiInInfo->InPathId).PwId.u4PwIndex =
            pServiceParam->PwServiceInfo.u4PwIndex;
    }

    return;
}

/* HITLESS RESTART */

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilGetPeriodicTime
 *
 * DESCRIPTION      : This is the function used to get the value of the
 *                    APS Periodic Timeout
 *
 * INPUT            : u4IfIndex - Interface Index
 *
 * OUTPUT           : pu4RetValElpsApsPeriodicTime - Periodic Timeout
 *                                                   Value
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/

PUBLIC INT4
ElpsUtilGetPeriodicTime (UINT4 u4IfIndex, UINT4 *pu4RetValElpsApsPeriodicTime)
{
    tElpsPgInfo        *pNextPg = NULL;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4ContextId = 0;

    *pu4RetValElpsApsPeriodicTime = 0;
    while ((pNextPg = ElpsPgGetNextNode (u4ContextId, u4PgId,
                                         &u4NextContextId,
                                         &u4NextPgId)) != NULL)
    {
        u4ContextId = u4NextContextId;
        u4PgId = u4NextPgId;

        if (pNextPg->ProtectionEntity.u4PortId == u4IfIndex)
        {
            *pu4RetValElpsApsPeriodicTime = (UINT4) pNextPg->u2PeriodicTime;
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilSetVlanGroupIdForVlan
 *
 * DESCRIPTION      : This function mapped a vlan to a vlan group.
 *
 * INPUT            : u4ContextId - virtual context id
 *                    VlanId      - vlan id that needs to be mapped to the
 *                                  provided vlan group.
 *                    u2VlanGroupId - vlan group id.
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ElpsUtilSetVlanGroupIdForVlan (UINT4 u4ContextId, tVlanId VlanId,
                               UINT2 u2VlanGroupId)
{

    tStpInstanceInfo    InstInfo;
    UINT2               u2OldGroupId = 0;
    UINT4               u4Port = 0;
    MEMSET (&InstInfo, 0, sizeof (InstInfo));
    if (u2VlanGroupId == ELPS_DEFAULT_VLAN_GROUP_ID)
    {
        /* Get the currently mapped instance id for this vlan */
        u2OldGroupId = L2IwfMiGetVlanInstMapping (u4ContextId, VlanId);

        /* unmapping the vlan from instance u2VlanGroupId and
         * adding it to default group. */
        InstInfo.u1RequestFlag = L2IWF_REQ_INSTANCE_MAP_COUNT;
        if (L2IwfGetInstanceInfo (u4ContextId, u2OldGroupId, &InstInfo)
            == L2IWF_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (L2IwfSetVlanInstMapping (u4ContextId, VlanId, u2VlanGroupId)
            == L2IWF_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (InstInfo.u2MappingCount == 1)
        {
            /* Delete this instance */
            L2IwfDeleteSpanningTreeInstance (u4ContextId, u2OldGroupId);
        }
    }
    else
    {
        InstInfo.u1RequestFlag = L2IWF_REQ_INSTANCE_STATUS;
        if (L2IwfGetInstanceInfo (u4ContextId, u2VlanGroupId, &InstInfo)
            == L2IWF_FAILURE)
        {
            return OSIX_FAILURE;
        }

        if (InstInfo.u1InstanceStatus == OSIX_FALSE)
        {
            /* Given Instance is not Active */
            if (L2IwfCreateSpanningTreeInstance (u4ContextId, u2VlanGroupId)
                == L2IWF_FAILURE)
            {
                return OSIX_FAILURE;
            }

            for (u4Port = 1; u4Port <= ELPS_NP_MAX_IF_INDEX; u4Port++)
            {
                if (L2IwfSetInstPortState (u2VlanGroupId, u4Port,
                                           ELPS_PORT_STATE_UNBLOCKING) ==
                    L2IWF_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }
        }

        if (L2IwfSetVlanInstMapping (u4ContextId, VlanId, u2VlanGroupId)
            == L2IWF_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUtilUpdateL2iwfOnPgDisable
 *
 * DESCRIPTION      : This function will check if any instance is created
 *                     in L2iwf and deletes the instance. Instance is created
 *                     only when a service vlan or service list is already
 *                     configured.
 *
 * INPUT            : pPgInfo - Protection Group
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 **************************************************************************/
INT4
ElpsUtilUpdateL2iwfOnPgDisable (tElpsPgInfo * pPgInfo)
{
    if ((pPgInfo->pContextInfo->i4VlanGroupManager ==
         ELPS_VLAN_GROUP_MANAGER_ELPS)
        && (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN))
    {
        if ((pPgInfo->u1PgConfigType == ELPS_PG_TYPE_LIST) &&
            (pPgInfo->unServiceList.pWorkingServiceList != NULL))
        {
            if (L2IwfDeleteSpanningTreeInstance
                (pPgInfo->pContextInfo->u4ContextId,
                 pPgInfo->u4WorkingInstanceId) == L2IWF_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        else if (pPgInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
        {
            if (pPgInfo->WorkingEntity.ServiceParam.u4ServiceId != 0)
            {
                if (L2IwfDeleteSpanningTreeInstance
                    (pPgInfo->pContextInfo->u4ContextId,
                     pPgInfo->u4WorkingInstanceId) == L2IWF_FAILURE)
                {
                    return OSIX_FAILURE;
                }
            }

            if (pPgInfo->ProtectionEntity.ServiceParam.u4ServiceId != 0)
            {
                if (L2IwfDeleteSpanningTreeInstance
                    (pPgInfo->pContextInfo->u4ContextId,
                     pPgInfo->u4ProtectionInstanceId) == L2IWF_FAILURE)
                {
                    return OSIX_FAILURE;
                }

            }
        }
    }
    return OSIX_SUCCESS;
}
/*****************************************************************************/
/* Function Name      : ElpsGetPktCountForAllContexts                        */
/*                                                                           */
/* Description        : This routine is called to get the statistics for     */
/*                      all the protection groups in all contexts for the    */
/*                      given protection type.                               */
/*                                                                           */
/* Input(s)           : pu4PktCount - Total packet Count for the given       */
/*                                    protection type.                       */
/*                      StatFunc    - Pointer to the function to get         */
/*                                    the statistics.                        */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Owner Protocol Id.                                   */
/*****************************************************************************/
UINT4
ElpsGetPktCountForAllContexts (INT1 u1BridgeType,
                               INT1 (*pCallBk) (UINT4, UINT4, UINT4 *))
{
    tElpsContextInfo   *pCxt = NULL;
    tElpsPgInfo        *pPg = NULL;
    tElpsPgInfo        *pNextPg = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4ApsCount = 0;
    UINT4               u4PktCount = 0;
    UINT1               u1LocalVal = ELPS_B_BIT_VALUE;

    /* Get No. of Packets Discarded for protection groups in all contexts.
     * based on the bridge type.*/

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pCxt = ElpsCxtGetNode (u4ContextId);

        if (pCxt == NULL)
        {
            /* Context Not Created */
            continue;
        }

        if ((ElpsUtilIsElpsStarted (u4ContextId) == OSIX_FALSE) ||
            (pCxt->u1ModuleStatus == ELPS_DISABLED))
        {
            continue;
        }

        pNextPg = ElpsPgGetFirstNodeInContext (pCxt);

        if (pNextPg == NULL)
        {
            continue;
        }

        do
        {
            pPg = pNextPg;
            u1LocalVal =
                (UINT1) ELPS_GET_PG_B_BIT_VALUE (pPg->ApsInfo.u1ProtectionType);

            if (u1LocalVal == u1BridgeType)
            {
                pCallBk (u4ContextId, pPg->u4PgId, &u4ApsCount);
                u4PktCount += u4ApsCount;
            }

        }
        while ((pNextPg = ElpsPgGetNextNodeInContext (pCxt, pPg)) != NULL);
    }
    return u4PktCount;
}

/* ELPS performance measurement */
/************************************************************************/
/*  Function Name   : ElpsGetSysTime                                    */
/*  Description     : Returns the system time in STUPS.                 */
/*  Input(s)        : None                                              */
/*  Output(s)       : pSysTime - The System time.                       */
/*  Returns         : None                                              */
/************************************************************************/
VOID
ElpsGetSysTime (tOsixSysTime * pSysTime)
{
    tUtlSysPreciseTime  SysPreciseTime;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));
    /* Function used to return the system time in seconds and nanoseconds */
    UtlGetPreciseSysTime (&SysPreciseTime);
    /* Time returned in milliseconds */
    *pSysTime =
        ((SysPreciseTime.u4Sec * 1000) + (SysPreciseTime.u4NanoSec / 1000000));
    return;
}

/***********************************************************************/
/*  Function Name   : ElpsUtilMeasureTime                              */
/*  Description     : This is the function used to Store the System    */
/*                    current time when topology change occurs.        */
/*  Input(s)        : pPgInfo  - Protection group information.         */
/*                    u1Flag   - Flag to update the respective object. */
/*  Output(s)       : pSysTime - The System time.                      */
/*  Returns         : None                                             */
/* *********************************************************************/
VOID
ElpsUtilMeasureTime (tElpsPgInfo * pPgInfo, UINT1 u1Flag)
{
    tOsixSysTime        SysTime;

    MEMSET (&SysTime, 0, sizeof (tOsixSysTime));

    switch (u1Flag)
    {
        case ELPS_PERF_LR_SF_RX:
            ElpsGetSysTime (&SysTime);
            pPgInfo->Perf.u4LRSFRxTime = SysTime;
            break;

        case ELPS_PERF_LR_SF_TX:
            ElpsGetSysTime (&SysTime);
            pPgInfo->Perf.u4LRSFTxTime = SysTime;
            break;

        case ELPS_PERF_FR_SF_RX:
            ElpsGetSysTime (&SysTime);
            pPgInfo->Perf.u4FRSFRxTime = SysTime;
            break;

        case ELPS_PERF_STATE_CHG:
            ElpsGetSysTime (&SysTime);
            pPgInfo->Perf.u4StateChgTime = SysTime;
            break;

        default:
            break;
    }
}

#endif /* _ELPSUTIL_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsutil.c                     */
/*------ -----------------------------------------------------------------*/
