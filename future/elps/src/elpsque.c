/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsque.c,v 1.5 2012/09/01 10:02:02 siva Exp $
 *
 * Description: This file contains procedures related to:
 *               - Processing QMsgs
 *               - Enqueuing QMsg from external modules to ELPS task
 *****************************************************************************/
#ifndef _ELPSQUE_C_
#define _ELPSQUE_C_

#include "elpsinc.h"

/* Proto types of the functions private to this file only */
PRIVATE VOID ElpsQueHandleRxPdu PROTO ((tElpsQMsg * pMsg));
PRIVATE VOID ElpsQueHandleCfmSignal PROTO ((tElpsQMsg * pMsg));

/***************************************************************************
 * FUNCTION NAME    : ElpsQueEnqMsg
 *
 * DESCRIPTION      : Function is used to post queue message to ELPS task.
 *
 * INPUT            : pMsg - Pointer to the message to be posted.
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ElpsQueEnqMsg (tElpsQMsg * pMsg)
{
    if (OsixQueSend (gElpsGlobalInfo.TaskQId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        ElpsMainReleaseQMemory (pMsg);
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsQueEnqMsg: Osix Queue Send"
                   " Failed!!!\r\n"));
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (gElpsGlobalInfo.MainTaskId, ELPS_QMSG_EVENT)
        == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsQueEnqMsg: Osix Event Send"
                   " Failed!!!\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsQuePostEventToAPSTxTask
 *
 * DESCRIPTION      : This function is used to post the ELPS_APS_TX_EVENT 
 *                    to the APS TX Task.
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE
 *
 **************************************************************************/
PUBLIC VOID
ElpsQuePostEventToAPSTxTask (VOID)
{
    if (OsixEvtSend (gElpsGlobalApsTxInfo.ApsTxTaskId, ELPS_APS_TX_EVENT)
        == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsQuePostEventToAPSTxTask: "
                   "Osix Event Send Failed!!!\r\n"));
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsQueMsgHandler  
 *
 * DESCRIPTION      : Function is used to post queue message to ELPS task.
 *
 * INPUT            : pMsg - Pointer to the message to be posted. 
 *
 * OUTPUT           : NONE 
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsQueMsgHandler (VOID)
{
    tElpsQMsg          *pMsg = NULL;

    /* Event received, dequeue messages for processing */
    while (OsixQueRecv (gElpsGlobalInfo.TaskQId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pMsg->u4MsgType)
        {
            case ELPS_CREATE_CONTEXT_MSG:
                if (ElpsCxtCreateContext (pMsg->u4ContextId) == OSIX_FAILURE)
                {
                    ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsQueMsgHandler: "
                               "Context Creation FAILED!!!\r\n"));
                }
                L2MI_SYNC_GIVE_SEM ();
                break;

            case ELPS_DELETE_CONTEXT_MSG:
                if (ElpsCxtDeleteContext (pMsg->u4ContextId) == OSIX_FAILURE)
                {
                    ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsQueMsgHandler: "
                               "Context Deletion FAILED!!!\r\n"));
                }
                L2MI_SYNC_GIVE_SEM ();
                break;

            case ELPS_APS_PDU_MSG:
            case ELPS_PSC_PDU_MSG:
                ElpsQueHandleRxPdu (pMsg);
                CRU_BUF_Release_MsgBufChain (pMsg->unMsgParam.ApsPduMsg.pApsPdu,
                                             FALSE);
                break;

            case ELPS_CFM_SIGNAL_MSG:
                ElpsQueHandleCfmSignal (pMsg);
                break;

#ifdef MBSM_WANTED
            case MBSM_MSG_CARD_INSERT:
            case MBSM_MSG_CARD_REMOVE:
                ElpsMbsmHandleLCEvent (pMsg->unMsgParam.pMbsmProtoMsg,
                                       pMsg->u4MsgType);
                MEM_FREE (pMsg->unMsgParam.pMbsmProtoMsg);
                break;

#endif /* MBSM_WANTED */

            case ELPS_RM_MSG:

                ElpsRedHandleRmEvents (pMsg);
                break;

            default:
                ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsQueMsgHandler: "
                           "Unknown message type received\r\n"));
                break;
        }
        /* Release the buffer to pool */
        if (MemReleaseMemBlock (gElpsGlobalInfo.QMsgPoolId, (UINT1 *) pMsg)
            == MEM_FAILURE)
        {
            ELPS_TRC ((NULL, ELPS_CRITICAL_TRC | ALL_FAILURE_TRC,
                       "ElpsQueMsgHandler: Free MemBlock QMsg FAILED!!!\r\n"));
            return;
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsQueHandleRxPdu
 *
 * DESCRIPTION      : This function is called whenever an APS PDU is    
 *                    recevied for an MEP.
 *
 * INPUT            : pMsg - Pointer to the message to be posted. 
 *
 * OUTPUT           : NONE 
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsQueHandleRxPdu (tElpsQMsg * pMsg)
{
    tElpsContextInfo   *pCxt = NULL;

    if (ElpsUtilIsElpsStarted (pMsg->u4ContextId) == OSIX_FALSE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsQueHandleRxPdu: "
                   "ELPS is not started in this context\r\n"));
        return;
    }

    if ((pCxt = ElpsCxtGetNode (pMsg->u4ContextId)) == NULL)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsQueHandleRxPdu: "
                   "Signal received for unknown context\r\n"));
        return;
    }

    if (pCxt->u1ModuleStatus == ELPS_DISABLED)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsQueHandleRxPdu: "
                   "ELPS Disabled in this context\r\n"));
        return;
    }
    if (pMsg->u4MsgType == ELPS_APS_PDU_MSG)
    {
        ElpsRxProcessAPSPdu (pCxt, pMsg);
    }
    else if (pMsg->u4MsgType == ELPS_PSC_PDU_MSG)
    {
        ElpsRxProcessPSCPdu (pCxt, pMsg);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsQueHandleCfmSignal
 *
 * DESCRIPTION      : This function is called whenever an indication is    
 *                    recevied for an MEP.
 *
 * INPUT            : pMsg - Pointer to the message to be posted. 
 *
 * OUTPUT           : NONE 
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsQueHandleCfmSignal (tElpsQMsg * pMsg)
{
    tElpsContextInfo   *pCxt = NULL;
    tElpsCfmConfigInfo *pCfm = NULL;
    tElpsCfmConfigInfo  CfmInfo;

    MEMSET (&CfmInfo, 0, sizeof (tElpsCfmConfigInfo));
    pCxt = ElpsCxtGetNode (pMsg->u4ContextId);

    if (pCxt == NULL)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsQueHandleCfmSignal: "
                   "Context %d not exists.\r\n", pMsg->u4ContextId));
        return;
    }

    if (ElpsUtilIsElpsStarted (pMsg->u4ContextId) == OSIX_FALSE)
    {
        ELPS_TRC ((pCxt, CONTROL_PLANE_TRC, "ElpsQueHandleCfmSignal: "
                   "ELPS is not started in this context\r\n"));
        return;
    }

    if (pCxt->u1ModuleStatus == ELPS_DISABLED)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsQueHandleCfmSignal: "
                   "ELPS Disabled in this context\r\n"));
        return;
    }

    CfmInfo.MonitorInfo.u4MegId =
        pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MegId;
    CfmInfo.MonitorInfo.u4MeId =
        pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MeId;
    CfmInfo.MonitorInfo.u4MepId =
        pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MepId;
    CfmInfo.u1ServiceType = pMsg->u1ServiceType;
    CfmInfo.u1ModId = pMsg->u1ModId;

    if ((pCfm = ElpsCfmGetNode (pCxt, &CfmInfo)) == NULL)
    {
        ELPS_TRC ((pCxt, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                   "ElpsQueHandleCfmSignal: Unable to get the "
                   "PG Info for the given Monitor Id.\r\n"));
        return;
    }

    if (pCfm->pPgInfo->u1PgRowStatus != ACTIVE)
    {
        ELPS_TRC ((pCxt, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                   "ElpsQueHandleCfmSignal: Protection Group is "
                   "not active.\r\n"));
        return;
    }
    /* performance measurement
     * when the peer node interface gets down on which PW is running,
     * ECFM gives SF indication to ELPS at this point.
     * This time is noted down as "T0"
     */
    if ((pCfm->pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW) ||
        (pCfm->pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP))
    {
        ElpsUtilMeasureTime (pCfm->pPgInfo, ELPS_PERF_LR_SF_RX);
    }

    ElpsCfmProcessReceivedSignal (pCfm,
                                  pMsg->unMsgParam.MonitorStatus.u4Status);

    return;
}

#endif /* _ELPSQUE_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsque.c                      */
/*-----------------------------------------------------------------------*/
