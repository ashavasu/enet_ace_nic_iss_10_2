/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpssz.c,v 1.3 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _ELPSSZ_C
#include "elpsinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
ElpsSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ELPS_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsELPSSizingParams[i4SizingId].u4StructSize,
                              FsELPSSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(ELPSMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            ElpsSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
ElpsSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsELPSSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, ELPSMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
ElpsSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < ELPS_MAX_SIZING_ID; i4SizingId++)
    {
        if (ELPSMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (ELPSMemPoolIds[i4SizingId]);
            ELPSMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
