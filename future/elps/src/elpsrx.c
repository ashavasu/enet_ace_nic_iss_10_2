
/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsrx.c,v 1.15 2015/09/02 11:56:59 siva Exp $
 *
 * Description: This file contains the Protection Group Table related
 * datastructure implementation and utility functions.
 *****************************************************************************/
#ifndef _ELPSRX_C_
#define _ELPSRX_C_

#include "elpsinc.h"
#include "elpssem.h"

/* Proto types of the functions private to this file only */
PRIVATE INT4 ElpsRxValidateApsPdu PROTO ((tElpsPgInfo *, tElpsCfmConfigInfo *,
                                          UINT1 *));
PRIVATE VOID ElpsRxApplyFarEndRequestToSEM PROTO ((tElpsPgInfo *, UINT1 *));
PRIVATE VOID ElpsRxApplyPscFarEndRequestToSEM PROTO ((tElpsPgInfo *, UINT1 *));
PRIVATE UINT1 ElpsRxGetStateFromPkt PROTO ((UINT1, UINT1));

/***************************************************************************
 * FUNCTION NAME    : ElpsRxProcessPSCPdu   
 *
 * DESCRIPTION      : This function is used to process the PSC PDU. 
 *
 * Input(s)         : pCtxtInfo    - Pointer to the context info structure
 *                    pQMsg        - Pointer to the Q Message structure     
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
ElpsRxProcessPSCPdu (tElpsContextInfo * pCtxtInfo, tElpsQMsg * pQMsg)
{
    tElpsPscSemState    PscSemStateInfo;
    tElpsCfmConfigInfo  CfmMonitorInfo;
    tElpsPgInfo        *pPgInfo = NULL;
    UINT2               u2TlvLength = 0;
    UINT1               au1PscData[ELPS_PSC_PDU_LEN];
    UINT1              *pPscSpecificInfo = NULL;

    MEMSET (&CfmMonitorInfo, 0, sizeof (tElpsCfmConfigInfo));

    /* Get the length of the ACH TLV */
    CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.ApsPduMsg.pApsPdu,
                               ((UINT1 *) &u2TlvLength),
                               (pQMsg->unMsgParam.ApsPduMsg.u1TlvOffset
                                + ELPS_PSC_GACH_HDR_LEN), ELPS_TWO);

    CfmMonitorInfo.MonitorInfo.u4MegId =
        pQMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MegId;
    CfmMonitorInfo.MonitorInfo.u4MeId =
        pQMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MeId;
    CfmMonitorInfo.MonitorInfo.u4MepId =
        pQMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MepId;
    CfmMonitorInfo.u1ServiceType = pQMsg->u1ServiceType;
    CfmMonitorInfo.u1ModId = pQMsg->u1ModId;

    pPgInfo = ElpsCfmGetPgInfo (pCtxtInfo, &CfmMonitorInfo);

    if (pPgInfo == NULL)
    {
        ELPS_TRC ((pCtxtInfo, CONTROL_PLANE_TRC, "ElpsRxProcessPSCPdu: "
                   "PG Entry not created. \r\n"));
        return OSIX_FAILURE;
    }

    if (pPgInfo->u1PscVersion == ELPS_ZERO)
    {
        /* Get the packet */
        CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.ApsPduMsg.pApsPdu,
                                   au1PscData,
                                   (pQMsg->unMsgParam.ApsPduMsg.u1TlvOffset +
                                    ELPS_PSC_GACH_HDR_LEN +
                                    ELPS_PSC_ACH_TLV_HDR_LEN + u2TlvLength),
                                   ELPS_PSC_PDU_LEN);
    }
    else
    {
        /* Get the packet */
        CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.ApsPduMsg.pApsPdu,
                                   au1PscData,
                                   (pQMsg->unMsgParam.ApsPduMsg.u1TlvOffset +
                                    ELPS_PSC_GACH_HDR_LEN), ELPS_PSC_PDU_LEN);
    }

    pPscSpecificInfo = au1PscData;
    ELPS_TRC ((pPgInfo->pContextInfo, DUMP_TRC,
               "Rx PSC PDU [Context-%d, PG-%d]\r\n\r\n",
               pCtxtInfo->u4ContextId, pPgInfo->u4PgId));

    ElpsTrcPscPktDumpTrc (pPgInfo->pContextInfo,
                          pQMsg->unMsgParam.ApsPduMsg.pApsPdu,
                          pQMsg->unMsgParam.ApsPduMsg.u1TlvOffset);

    pPgInfo->Stats.u4TotalApsRxCount++;

    if (pPgInfo->u1PgRowStatus != ACTIVE)
    {
        ELPS_TRC ((pCtxtInfo, CONTROL_PLANE_TRC, "ElpsRxProcessPSCPdu: "
                   "PG Entry is not Active. \r\n"));
        pPgInfo->Stats.u4TotalApsDiscardCount++;
        return OSIX_FAILURE;
    }

    if (ElpsRxValidatePscPdu (pPgInfo, &CfmMonitorInfo,
                              pPscSpecificInfo) == OSIX_FAILURE)
    {
        pPgInfo->Stats.u4TotalApsDiscardCount++;
        return OSIX_FAILURE;
    }

    if (ElpsSemPscGetStateInfo
        (pPgInfo->ApsInfo.u1ApsSemStateId, &PscSemStateInfo) == OSIX_FAILURE)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "Protection Group is in Invalid State. \r\n"));
        pPgInfo->Stats.u4TotalApsDiscardCount++;
        return OSIX_FAILURE;
    }

    /* If the bridged paths are same then stop the 
     * LOR timer. This is required in 1+1/1:1 Bi-directional protection types.*/
    if (ELPS_GET_PG_D_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
    {
        if (PscSemStateInfo.u4Path == pPscSpecificInfo[ELPS_THREE])
        {
            ElpsTmrStopTimer (pPgInfo, ELPS_LOR_TMR);
        }
    }

    /* No Need to call the State Machine functions if 1+1 Uni-directional 
     * APS protection type is used and PSC version is Zero.*/
    if (!((pPgInfo->u1PscVersion == ELPS_ZERO) &&
          (ELPS_GET_PG_D_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType) ==
           ELPS_ZERO)))
    {
        ElpsRxApplyPscFarEndRequestToSEM (pPgInfo, pPscSpecificInfo);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRxProcessApsPdu   
 *
 * DESCRIPTION      : This function is used to process the APS PDU. 
 *
 * Input(s)         : pCtxtInfo    - Pointer to the context info structure
 *                    pQMsg        - Pointer to the Q Message structure     
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
ElpsRxProcessAPSPdu (tElpsContextInfo * pCtxtInfo, tElpsQMsg * pQMsg)
{
    tElpsSemState       SemStateInfo;
    tElpsCfmConfigInfo  CfmMonitorInfo;
    tElpsPgInfo        *pPgInfo = NULL;
    UINT1               au1ApsData[ELPS_APS_PDU_LEN];
    UINT1              *pApsSpecificInfo = NULL;

    MEMSET (&CfmMonitorInfo, 0, sizeof (tElpsCfmConfigInfo));
    CRU_BUF_Copy_FromBufChain (pQMsg->unMsgParam.ApsPduMsg.pApsPdu,
                               au1ApsData,
                               pQMsg->unMsgParam.ApsPduMsg.u1TlvOffset,
                               ELPS_APS_PDU_LEN);

    pApsSpecificInfo = au1ApsData + ELPS_APS_PDU_HDR_LEN;
    CfmMonitorInfo.MonitorInfo.u4MegId =
        pQMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MegId;
    CfmMonitorInfo.MonitorInfo.u4MeId =
        pQMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MeId;
    CfmMonitorInfo.MonitorInfo.u4MepId =
        pQMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MepId;
    CfmMonitorInfo.u1ServiceType = pQMsg->u1ServiceType;
    CfmMonitorInfo.u1ModId = pQMsg->u1ModId;

    pPgInfo = ElpsCfmGetPgInfo (pCtxtInfo, &CfmMonitorInfo);

    if (pPgInfo == NULL)
    {
        ELPS_TRC ((pCtxtInfo, CONTROL_PLANE_TRC, "ElpsRxProcessAPSPdu: "
                   "PG Entry not created. \r\n"));
        return OSIX_FAILURE;
    }

    ELPS_TRC ((pPgInfo->pContextInfo, DUMP_TRC,
               "Rx APS PDU [Context-%d, PG-%d]\r\n\r\n",
               pCtxtInfo->u4ContextId, pPgInfo->u4PgId));
    ElpsTrcApsPktDumpTrc (pPgInfo->pContextInfo, pApsSpecificInfo,
                          ELPS_APS_PDU_TLV_OFFSET);

    pPgInfo->Stats.u4TotalApsRxCount++;

    if (pPgInfo->u1PgRowStatus != ACTIVE)
    {
        ELPS_TRC ((pCtxtInfo, CONTROL_PLANE_TRC, "ElpsRxProcessAPSPdu: "
                   "PG Entry is not Active. \r\n"));
        pPgInfo->Stats.u4TotalApsDiscardCount++;
        return OSIX_FAILURE;
    }

    if (ElpsRxValidateApsPdu (pPgInfo, &CfmMonitorInfo,
                              au1ApsData) == OSIX_FAILURE)
    {
        pPgInfo->Stats.u4TotalApsDiscardCount++;
        return OSIX_FAILURE;
    }

    if (ElpsSemGetStateInfo (pPgInfo->ApsInfo.u1ApsSemStateId, &SemStateInfo)
        == OSIX_FAILURE)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC | ALL_FAILURE_TRC,
                   "Protection Group is in Invalid State. \r\n"));
        pPgInfo->Stats.u4TotalApsDiscardCount++;
        return OSIX_FAILURE;
    }

    /* If the requested signal and bridged signal are same then stop the 
     * LOR timer. This is valid in 1:1/1+1 Bidirectional protection types.*/
    if (ELPS_GET_PG_D_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
    {
        if (SemStateInfo.u4RequestedSignal == pApsSpecificInfo[ELPS_TWO])
        {
            ElpsTmrStopTimer (pPgInfo, ELPS_LOR_TMR);
        }
    }

    /* Need to call the State Machine functions if 1+1/1:1 Bi-directional 
     * protection type is used.*/
    if (ELPS_GET_PG_D_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
    {
        ElpsRxApplyFarEndRequestToSEM (pPgInfo, pApsSpecificInfo);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRxValidatePscPdu
 *
 * DESCRIPTION      : Function is used to validate the received PSC PDU. 
 *
 * Input(s)         : pPgInfo  - Pointer to the protection group info.
 *                    pCfmInfo - Pointer to the CFM structure.
 *                    pu1ApsPdu - pointer to the PSC PDU 
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
ElpsRxValidatePscPdu (tElpsPgInfo * pPgInfo, tElpsCfmConfigInfo * pMonitorInfo,
                      UINT1 *pu1PscPdu)
{
    tElpsNotifyInfo     NotifyInfo;
    tElpsCfmConfigInfo *pCfmConfigInfo = NULL;
    UINT1               u1LocalVal = OSIX_FALSE;
    UINT1               u1PscPduVal = OSIX_FALSE;
    UINT1               u1Version = 0;
    UINT1               u1FaultPath = 0;
    UINT1               u1Path = 0;

    /* Validate the Version field */
    ELPS_GET_VERSION_FROM_PSC_PDU (u1Version, pu1PscPdu);
    if (u1Version != pPgInfo->u1PscVersion)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidatePscPdu: Version field value is %d. "
                   "PSC PDU Dropped.\r\n", u1Version));
        return OSIX_FAILURE;
    }

    NotifyInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
    NotifyInfo.u4PgId = pPgInfo->u4PgId;
    NotifyInfo.u1PgStatus = pPgInfo->u1PgStatus;

    /* Check whether the Request State is valid */
    ELPS_GET_REQUEST_FROM_PSC_PDU (u1PscPduVal, pu1PscPdu);
    if (pPgInfo->u1PscVersion == ELPS_ZERO)
    {
        if ((u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_NR) &&
            (u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_DNR) &&
            (u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_WTR) &&
            (u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_MS) &&
            (u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_SF) &&
            (u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_FS) &&
            (u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_LO) &&
            (u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_RR) &&
            (u1PscPduVal != ELPS_PSC_PKT_REQ_STATE_EXER))
        {
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsRxValidatePscPdu: Invalid Request State %d"
                       " found. PSC PDU Dropped.\r\n", u1PscPduVal));
            return OSIX_FAILURE;
        }
    }
    else
    {
        if ((u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_NR) &&
            (u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_DNR) &&
            (u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_WTR) &&
            (u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_MS) &&
            (u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_SF) &&
            (u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_FS) &&
            (u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_LO) &&
            (u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_RR) &&
            (u1PscPduVal != ELPS_PSC_PKT_RFC_REQ_STATE_EXER))
        {
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsRxValidatePscPdu: Invalid Request State %d"
                       " found. PSC PDU Dropped.\r\n", u1PscPduVal));
            return OSIX_FAILURE;
        }
    }

    /* Check if the 'B' bit mismatches */
    u1LocalVal =
        (UINT1) ELPS_GET_PG_B_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType);
    u1LocalVal = (UINT1) (u1LocalVal >> ELPS_TWO);
    ELPS_GET_BRIDGE_FROM_PSC_PDU (u1PscPduVal, pu1PscPdu);

    if (u1PscPduVal == 0)
    {
        u1PscPduVal = 1;
    }
    else
    {
        u1PscPduVal = 0;
    }

    if (u1LocalVal != u1PscPduVal)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_PG_TYPE_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidatePscPdu: Protection Group Bridge "
                   "Type Mismatch " "found. PSC PDU Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    pCfmConfigInfo = ElpsCfmGetNode (pPgInfo->pContextInfo, pMonitorInfo);

    if (pCfmConfigInfo == NULL)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                   "ElpsRxProcessAPSPdu: CFM Entry not created. PSC PDU "
                   "Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    /* check if the entity type which is receiving the APS PDU is working */
    if (pCfmConfigInfo->u1EntityType == ELPS_ENTITY_TYPE_WORKING)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_CONF_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidatePscPdu: Configuration Mismatch found. "
                   "PSC PDU Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    /* Check if the 'D' bit mismatches */
    u1LocalVal =
        (UINT1) ELPS_GET_PG_D_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType);
    u1LocalVal = (UINT1) (u1LocalVal >> 1);
    ELPS_GET_DIRECTION_FROM_PSC_PDU (u1PscPduVal, pu1PscPdu);

    if (u1LocalVal != u1PscPduVal)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_D_BIT_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidatePscPdu: Direction Mismatch found. "
                   "PSC PDU Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    /* Check if the 'R' bit mismatches */
    u1LocalVal =
        (UINT1) ELPS_GET_PG_R_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType);
    ELPS_GET_R_BIT_FROM_PSC_PDU (u1PscPduVal, pu1PscPdu);

    if (u1LocalVal != u1PscPduVal)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_R_BIT_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidatePscPdu: Revertive bit Mismatch found.\r\n"));

        /* Return failure if the Protection Group is configured for
         * MPLS-TP paths as the behaviour is undefined */
        if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
            (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            return OSIX_FAILURE;
        }
    }

    /* Validate the Fault Path Signal */
    u1FaultPath = (UINT1) pu1PscPdu[ELPS_TWO];
    if (u1FaultPath > ELPS_SIGNAL_NORMAL)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidatePscPdu: Invalid Fault Path Value (%d) "
                   "in PSC PDU.\r\n", u1FaultPath));
        return OSIX_FAILURE;
    }

    /* Validate the Path value */
    u1Path = (UINT1) pu1PscPdu[ELPS_THREE];
    if (u1Path > ELPS_SIGNAL_NORMAL)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidatePscPdu: Invalid Path Value (%d) "
                   "in PSC PDU.\r\n", u1Path));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRxValidateApsPdu
 *
 * DESCRIPTION      : Function is used to validate the received APS PDU. 
 *
 * Input(s)         : pPgInfo  - Pointer to the protection group info.
 *                    pCfmInfo - Pointer to the CFM structure.
 *                    pu1ApsPdu - pointer to the APS PDU 
 *                                [MEL + Version + OpCode+Flags + TLV Offset
 *                                + APS-Specific Information + END TLV]
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PRIVATE INT4
ElpsRxValidateApsPdu (tElpsPgInfo * pPgInfo, tElpsCfmConfigInfo * pMonitorInfo,
                      UINT1 *pu1ApsPdu)
{
    tElpsNotifyInfo     NotifyInfo;
    tElpsCfmConfigInfo *pCfmConfigInfo = NULL;
    UINT1              *pu1ApsSpecificInfo = NULL;
    UINT1               u1LocalVal = OSIX_FALSE;
    UINT1               u1ApsPduVal = OSIX_FALSE;
    UINT1               u1Version = 0;
    UINT1               u1OpCode = 0;
    UINT1               u1Flags = 0;
    UINT1               u1TlvOffset = 0;
    UINT1               u1EndTlv = 0;
    UINT1               u1PduProtType = 0;
    UINT1               u1RequestedSignal = 0;
    UINT1               u1BridgedSignal = 0;
    UINT1               u1ApsState = 0;

    /* Validate the Version field */
    u1Version = ELPS_GET_VERSION_FROM_PDU (pu1ApsPdu);
    if (u1Version != ELPS_APS_PDU_VERSION)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: Version field value is %d. "
                   "APS PDU Dropped.\r\n", u1Version));
        return OSIX_FAILURE;
    }
    /* Validate the OpCode filed */
    u1OpCode = ELPS_GET_OPCODE_FROM_PDU (pu1ApsPdu);
    if (u1OpCode != ELPS_APS_PDU_OPCODE)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: OpCode field value is %d. "
                   "APS PDU Dropped.\r\n", u1OpCode));
        return OSIX_FAILURE;
    }
    /* Validate the Flags field */
    u1Flags = ELPS_GET_FLAGS_FROM_PDU (pu1ApsPdu);
    if (u1Flags != ELPS_APS_PDU_FLAGS)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: Flags field value is %d. "
                   "APS PDU Dropped.\r\n", u1Flags));
        return OSIX_FAILURE;
    }
    /* Validate the TLV Offset */
    u1TlvOffset = ELPS_GET_TLV_OFFSET_FROM_PDU (pu1ApsPdu);
    if (u1TlvOffset != ELPS_APS_PDU_TLV_OFFSET)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: Tlv Offset field value "
                   "is %d.APS PDU Dropped.\r\n", u1TlvOffset));
        return OSIX_FAILURE;
    }
    /* Validate the END TLV field */
    u1EndTlv = ELPS_GET_END_TLV_FROM_PDU (pu1ApsPdu);
    if (u1EndTlv != ELPS_APS_PDU_END_TLV)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: End Tlv value is %d."
                   "APS PDU Dropped.\r\n", u1EndTlv));
        return OSIX_FAILURE;
    }
    ELPS_GET_REQUEST_FROM_PDU (u1ApsState, (pu1ApsPdu +
                                            ELPS_APS_OFFSET_OF_REQ_OFFSET));
    if ((u1ApsState == ELPS_INVALID_REQ_VAL1) ||
        (u1ApsState == ELPS_INVALID_REQ_VAL2) ||
        (u1ApsState == ELPS_INVALID_REQ_VAL3) ||
        (u1ApsState == ELPS_INVALID_REQ_VAL4) ||
        (u1ApsState == ELPS_INVALID_REQ_VAL5))
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: Req/State value is %d."
                   "APS PDU Dropped.\r\n", u1ApsState));
        return OSIX_FAILURE;
    }
    NotifyInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
    NotifyInfo.u4PgId = pPgInfo->u4PgId;
    NotifyInfo.u1PgStatus = pPgInfo->u1PgStatus;

    pu1ApsSpecificInfo = pu1ApsPdu + ELPS_APS_PDU_HDR_LEN;
    u1PduProtType = pu1ApsSpecificInfo[0];

    /* Check if the 'B' bit mismatches */
    u1LocalVal =
        (UINT1) ELPS_GET_PG_B_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType);
    u1ApsPduVal = (UINT1) ELPS_GET_PG_B_BIT_VALUE (u1PduProtType);

    if (u1LocalVal != u1ApsPduVal)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_PG_TYPE_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: Protection Group (B-bit) Mismatch "
                   "found. APS PDU Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    pCfmConfigInfo = ElpsCfmGetNode (pPgInfo->pContextInfo, pMonitorInfo);

    if (pCfmConfigInfo == NULL)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                   "ElpsRxProcessAPSPdu: CFM Entry not created. APS PDU "
                   "Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    /* check if the entity type which is receiving the APS PDU is working */
    if (pCfmConfigInfo->u1EntityType == ELPS_ENTITY_TYPE_WORKING)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_CONF_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: Configuration Mismatch found. "
                   "APS PDU Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    /* Check if the 'A' bit mismatches */
    u1LocalVal =
        (UINT1) ELPS_GET_PG_A_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType);
    u1ApsPduVal = (UINT1) ELPS_GET_PG_A_BIT_VALUE (u1PduProtType);

    if (u1LocalVal != u1ApsPduVal)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_A_BIT_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: A-bit Mismatch found. "
                   "APS PDU Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    /* Check if the 'D' bit mismatches */
    u1LocalVal =
        (UINT1) ELPS_GET_PG_D_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType);
    u1ApsPduVal = (UINT1) ELPS_GET_PG_D_BIT_VALUE (u1PduProtType);

    if (u1LocalVal != u1ApsPduVal)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_D_BIT_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: D-bit Mismatch found. "
                   "APS PDU Dropped.\r\n"));
        return OSIX_FAILURE;
    }

    /* Check if the 'R' bit mismatches */
    u1LocalVal =
        (UINT1) ELPS_GET_PG_R_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType);
    u1ApsPduVal = (UINT1) ELPS_GET_PG_R_BIT_VALUE (u1PduProtType);

    if (u1LocalVal != u1ApsPduVal)
    {
        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_R_BIT_MISMATCH);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: R-bit Mismatch found.\r\n"));
    }

    /* Validate the Requested Signal */
    u1RequestedSignal = (UINT1) pu1ApsSpecificInfo[1];
    if (u1RequestedSignal > ELPS_SIGNAL_NORMAL)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: Invalid Requested Signal Value (%d) "
                   "in APS PDU.\r\n", u1RequestedSignal));
        return OSIX_FAILURE;
    }

    /* Validate the Bridged Signal */
    u1BridgedSignal = (UINT1) pu1ApsSpecificInfo[ELPS_TWO];
    if (u1BridgedSignal > ELPS_SIGNAL_NORMAL)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsRxValidateApsPdu: Invalid Bridged Signal Value (%d) "
                   "in APS PDU.\r\n", u1BridgedSignal));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRxApplyPscFarEndRequestToSEM   
 *
 * DESCRIPTION      : Function is used to apply the Far end request to the 
 *                    SEM.
 *
 * Input(s)         : pPgInfo - Pointer to the protection group info.
 *                    pApsPdu - Pointer to the PDU that contains the APS 
 *                              specific information
 *
 * OUTPUT           : 
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PRIVATE VOID
ElpsRxApplyPscFarEndRequestToSEM (tElpsPgInfo * pPgInfo, UINT1 *pu1PscPdu)
{
    UINT1               u1Request = 0;
    UINT1               u1Event = 0;
    UINT1               u1RetVal = 0;
    UINT1               u1FPath = 0;
    UINT1               u1Path = 0;

    ELPS_GET_REQUEST_FROM_PSC_PDU (u1Request, pu1PscPdu);

    u1FPath = pu1PscPdu[ELPS_TWO];
    u1Path = pu1PscPdu[ELPS_THREE];

    if (pPgInfo->u1PscVersion == ELPS_ZERO)
    {
        u1Request = ElpsRxGetStateFromPscPkt (u1Request, u1FPath, u1Path);
    }
    else
    {
        u1Request = ElpsRxGetStateFromPscPktForPSCVersion1 (u1Request,
                                                            u1FPath, u1Path);
    }

    pPgInfo->u1LastRecvdReqType = ELPS_FAREND_REQUEST;
    pPgInfo->LastFarEndReq.u1Request = u1Request;

    u1Event = ElpsSemGetEventIdForReq (ELPS_FAREND_REQUEST, u1Request);

    /* performance measurement */
    /* APS-SF packet is received from peer node. Before started processing,
     * and before changing working to protection path, the time is noted 
     * as "T2".
     */
    if ((u1Event == ELPS_EV_FR_SF) &&
        (pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_NR_W))
    {
        ElpsUtilMeasureTime (pPgInfo, ELPS_PERF_FR_SF_RX);
    }
    if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
        (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        if ((u1Event == ELPS_EV_FR_WTR) &&
            (pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_WTR_P))
        {
            pPgInfo->LastFarEndReq.u1RequestStatus = ELPS_CMD_REJECTED;
            return;
        }
    }

    if (pPgInfo->u1PscVersion == ELPS_ONE &&
        pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_SEM_STATE_FS &&
        u1Event == ELPS_EV_FR_SF_P)
    {
        /* Far end request has lower priority than the existing
         * Local condition, so apply local condition to SEM */
        pPgInfo->LastFarEndReq.u1RequestStatus = ELPS_CMD_REJECTED;
        return;
    }

    /* Far end request can be directly applied to the State machine.
     * So this is considered as a ONe_PASS */
    u1RetVal = ElpsSemApplyEvent (pPgInfo, u1Event, ELPS_ONEPASS);

    pPgInfo->LastFarEndReq.u1RequestStatus = u1RetVal;

    if (u1RetVal == ELPS_CMD_ACCEPTED)
    {
        /* Update the local command status */
        ElpsSemModifyLocalCommandStatus (pPgInfo, u1Event);
    }
    /* Send Dynamic Sync-up having the SEM Information. All the Far End
     * request events applied to the SEM is synchronized here. 
     */
    ElpsRedSendDynamicSyncMsg (pPgInfo, ELPS_MAX_TMR_TYPES, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRxApplyFarEndRequestToSEM   
 *
 * DESCRIPTION      : Function is used to apply the Far end request to the 
 *                    SEM.
 *
 * Input(s)         : pPgInfo - Pointer to the protection group info.
 *                    pApsPdu - Pointer to the PDU that contains the APS 
 *                              specific information
 *
 * OUTPUT           : 
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PRIVATE VOID
ElpsRxApplyFarEndRequestToSEM (tElpsPgInfo * pPgInfo, UINT1 *pu1ApsPdu)
{
    UINT1               u1Request = 0;
    UINT1               u1Event = 0;
    UINT1               u1RetVal = ELPS_CMD_REJECTED;
    UINT1               u1RequestedSignal = 0;
    UINT1               u1LocalCond = ELPS_MAX_LOCAL_CONDITION;

    ELPS_GET_REQUEST_FROM_PDU (u1Request, pu1ApsPdu);

    u1RequestedSignal = pu1ApsPdu[ELPS_ONE];

    u1Request = ElpsRxGetStateFromPkt (u1Request, u1RequestedSignal);
    pPgInfo->u1LastRecvdReqType = ELPS_FAREND_REQUEST;
    pPgInfo->LastFarEndReq.u1Request = u1Request;

    u1Event = ElpsSemGetEventIdForReq (ELPS_FAREND_REQUEST, u1Request);

    /* performance measurement */
    /* APS-SF packet is received from peer node. Before started processing,
     * and before changing working to protection path, the time is noted 
     * as "T2".
     */
    if ((u1Event == ELPS_EV_FR_SF) &&
        (pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_NR_W))
    {
        ElpsUtilMeasureTime (pPgInfo, ELPS_PERF_FR_SF_RX);
    }

    /* If any Local condition exists, take it into account */
    if ((ELPS_IS_SF_REASSERTED (pPgInfo) == OSIX_TRUE) ||
        (ELPS_IS_SF_P_REASSERTED (pPgInfo) == OSIX_TRUE))
    {
        if (ELPS_IS_SF_P_REASSERTED (pPgInfo) == OSIX_TRUE)
        {
            u1LocalCond = ELPS_LOCAL_COND_SF_P;
        }
        else
        {
            u1LocalCond = ELPS_LOCAL_COND_SF_W;
        }

        if ((u1Event < ELPS_SEM_MAX_EVENTS))
        {
            if ((gai1ReqMapPriority[u1Event]) <=
                (gai1ReqMapPriority[gau1ElpsLocalCondToEvMap[u1LocalCond]]))
            {
                if (!(((pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_SF_W) &&
                       (u1LocalCond == ELPS_LOCAL_COND_SF_W)) ||
                      ((pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_SF_P) &&
                       (u1LocalCond == ELPS_LOCAL_COND_SF_P))))
                {
                    /* Far end request has lower priority than the existing
                     * Local condition, so apply local condition to SEM */
                    pPgInfo->LastFarEndReq.u1RequestStatus = ELPS_CMD_REJECTED;
                    pPgInfo->u1LastRecvdReqType = ELPS_LOCAL_CONDITION;
                    pPgInfo->LastLocalCond.u1Request = u1LocalCond;

                    u1Event = gau1ElpsLocalCondToEvMap[u1LocalCond];
                }
            }
        }
    }

    u1RetVal = ElpsSemApplyEvent (pPgInfo, u1Event, ELPS_ONEPASS);

    /* In port-service disjoint scenario when we receive WTR from the far-end then we need
     * to ensure that the protection port is a member of working service; it is required to
     * ensure uninterrupted data flow */
    if ((ELPS_FAR_REQ_WTR == u1Request) &&
        (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)) 
    {
        if (ElpsPgSwitchDataPath(pPgInfo->pContextInfo, pPgInfo,
                                     ELPS_PG_FAR_END_WTR) == OSIX_FAILURE)
        {
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC, 
                "ElpsRxApplyFarEndRequestToSEM: Failed while programming "
                    "ELPS_PG_FAR_END_WTR in H/W\r\n"));
        }
    }

    if (pPgInfo->u1LastRecvdReqType == ELPS_LOCAL_CONDITION)
    {
        pPgInfo->LastLocalCond.u1RequestStatus = u1RetVal;
    }
    else
    {
        pPgInfo->LastFarEndReq.u1RequestStatus = u1RetVal;
    }

    if (u1RetVal == ELPS_CMD_ACCEPTED)
    {
        /* Update the local command status */
        ElpsSemModifyLocalCommandStatus (pPgInfo, u1Event);
    }
    /* Send Dynamic Sync-up having the SEM Information. All the Far End
     * request events applied to the SEM is synchronized here. 
     */
    ElpsRedSendDynamicSyncMsg (pPgInfo, ELPS_MAX_TMR_TYPES, 0);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsRxGetStateFromPkt
 *                                                                          
 *    DESCRIPTION      : This function gets the state from the packet.   
 *
 *    INPUT            : state, Requested signal
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : state
 *                                                                          
 ****************************************************************************/
PRIVATE UINT1
ElpsRxGetStateFromPkt (UINT1 u1State, UINT1 u1RequestedSignal)
{
    UINT1               u1Value = 0;

    switch (u1State)
    {
        case ELPS_PKT_REQ_STATE_NR:
            if (u1RequestedSignal == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_NR_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_NR_P;
            }
            break;

        case ELPS_PKT_REQ_STATE_LO:
            u1Value = ELPS_FAR_REQ_LO;
            break;

        case ELPS_PKT_REQ_STATE_FS:
            u1Value = ELPS_FAR_REQ_FS;
            break;

        case ELPS_PKT_REQ_STATE_SF:
            u1Value = ELPS_FAR_REQ_SF_W;
            break;

        case ELPS_PKT_REQ_STATE_SF_P:
            u1Value = ELPS_FAR_REQ_SF_P;
            break;

        case ELPS_PKT_REQ_STATE_MS:
            u1Value = ELPS_FAR_REQ_MS;
            break;

        case ELPS_PKT_REQ_STATE_MS_W:
            u1Value = ELPS_FAR_REQ_MS_W;
            break;

        case ELPS_PKT_REQ_STATE_WTR:
            u1Value = ELPS_FAR_REQ_WTR;
            break;

        case ELPS_PKT_REQ_STATE_EXER:
            if (u1RequestedSignal == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_EXER_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_EXER_P;
            }
            break;

        case ELPS_PKT_REQ_STATE_RR:
            if (u1RequestedSignal == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_RR_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_RR_P;
            }
            break;

        case ELPS_PKT_REQ_STATE_DNR:
            u1Value = ELPS_FAR_REQ_DNR;
            break;

        default:
            break;
    }

    return u1Value;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsRxGetStateFromPScPkt
 *                                                                          
 *    DESCRIPTION      : This function gets the state from the packet.   
 *
 *    INPUT            : state, Requested signal
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : state
 *                                                                          
 ****************************************************************************/
PUBLIC UINT1
ElpsRxGetStateFromPscPkt (UINT1 u1State, UINT1 u1FaultPath, UINT1 u1Path)
{
    UINT1               u1Value = 0;

    switch (u1State)
    {
        case ELPS_PSC_PKT_REQ_STATE_NR:
            if (u1Path == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_NR_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_NR_P;
            }
            break;

        case ELPS_PSC_PKT_REQ_STATE_LO:
            u1Value = ELPS_FAR_REQ_LO;
            break;

        case ELPS_PSC_PKT_REQ_STATE_FS:
            u1Value = ELPS_FAR_REQ_FS;
            break;

        case ELPS_PSC_PKT_REQ_STATE_SF:
            if (u1FaultPath == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_SF_P;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_SF_W;
            }

            break;

        case ELPS_PSC_PKT_REQ_STATE_MS:
            if (u1Path == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_MS_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_MS;
            }

            break;

        case ELPS_PSC_PKT_REQ_STATE_WTR:
            u1Value = ELPS_FAR_REQ_WTR;
            break;
        case ELPS_PSC_PKT_REQ_STATE_EXER:
            if (u1Path == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_EXER_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_EXER_P;
            }
            break;

        case ELPS_PSC_PKT_REQ_STATE_RR:
            if (u1Path == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_RR_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_RR_P;
            }
            break;
        case ELPS_PSC_PKT_REQ_STATE_DNR:
            u1Value = ELPS_FAR_REQ_DNR;
            break;

        default:
            break;
    }

    return u1Value;
}

/****************************************************************************
 *
 *   FUNCTION NAME    : ElpsRxGetStateFromPscPktForPSCVersion1
 *
 *   DESCRIPTION      : This function gets the state from the packet when 
 *                      PSC version used is 1.
 *
 *   INPUT            : state, Requested signal
 *
 *   OUTPUT           : None
 *
 *   RETURNS          : state
 *
 *****************************************************************************/
PUBLIC UINT1
ElpsRxGetStateFromPscPktForPSCVersion1 (UINT1 u1State,
                                        UINT1 u1FaultPath, UINT1 u1Path)
{
    UINT1               u1Value = 0;

    switch (u1State)
    {
        case ELPS_PSC_PKT_RFC_REQ_STATE_NR:
            if (u1Path == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_NR_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_NR_P;
            }
            break;

        case ELPS_PSC_PKT_RFC_REQ_STATE_LO:
            u1Value = ELPS_FAR_REQ_LO;
            break;

        case ELPS_PSC_PKT_RFC_REQ_STATE_FS:
            u1Value = ELPS_FAR_REQ_FS;
            break;
        case ELPS_PSC_PKT_RFC_REQ_STATE_SF:
            if (u1FaultPath == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_SF_P;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_SF_W;
            }

            break;

        case ELPS_PSC_PKT_RFC_REQ_STATE_MS:
            if (u1Path == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_MS_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_MS;
            }

            break;

        case ELPS_PSC_PKT_RFC_REQ_STATE_WTR:
            u1Value = ELPS_FAR_REQ_WTR;
            break;
        case ELPS_PSC_PKT_RFC_REQ_STATE_EXER:
            if (u1Path == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_EXER_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_EXER_P;
            }
            break;

        case ELPS_PSC_PKT_RFC_REQ_STATE_RR:
            if (u1Path == ELPS_SIGNAL_NULL)
            {
                u1Value = ELPS_FAR_REQ_RR_W;
            }
            else
            {
                u1Value = ELPS_FAR_REQ_RR_P;
            }
            break;
        case ELPS_PSC_PKT_RFC_REQ_STATE_DNR:
            u1Value = ELPS_FAR_REQ_DNR;
            break;

        default:
            break;
    }

    return u1Value;
}

#endif /* _ELPSRX_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsrx.c                      */
/*-----------------------------------------------------------------------*/
