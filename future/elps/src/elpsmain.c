/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsmain.c,v 1.11 2014/07/09 12:01:37 siva Exp $
 *
 * Description: This file contains ELPS task main loop and 
 *              initialization routines.
 *****************************************************************************/
#ifndef _ELPSMAIN_C_
#define _ELPSMAIN_C_

#include "elpsinc.h"
#include "mplsapi.h"

/***************************************************************************
 * FUNCTION NAME    : ElpsMainTask
 *
 * DESCRIPTION      : Entry point function of ELPS task.
 *
 * INPUT            : pi1Arg - Pointer to the arguments
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsMainTask (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pi1Arg);

    if (ElpsMainTaskInit () == OSIX_FAILURE)
    {
        ElpsMainTaskDeInit ();

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Register the protocol MIBS with SNMP */
    RegisterFSELPS ();

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gElpsGlobalInfo.MainTaskId, ELPS_ALL_EVENTS,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            /* This routine handles the events and process them. This 
             * routine is introduced to enable System time relinquish 
             * method. In this method, any long time operation can 
             * relinquish system time by calling a routine which will
             * receive events (Non-Blocking call) and process the events
             * by calling this routine. By this way system time is relinquished
             * as well as control returns to the original routine.
             */
            ElpsMainProcessEvent (u4Events);
        }
    }                            /* end of while */
}

/***************************************************************************
 * FUNCTION NAME    : ElpsMainApsTxTask
 *
 * DESCRIPTION      : Entry point function of ELPS APS Transmission task.
 *
 * INPUT            : pi1Arg - Pointer to the arguments
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsMainApsTxTask (INT1 *pi1Arg)
{
    UINT4               u4Events = 0;

    UNUSED_PARAM (pi1Arg);

    if (OsixGetTaskId (SELF, ELPS_APS_TX_TASK_NAME,
                       &(gElpsGlobalApsTxInfo.ApsTxTaskId)) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                   "ElpsMainApsTxTask: Obtaining Task Id FAILED !!!\r\n"));

        /* Indicate the status of initialization to main routine */
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Indicate the status of initialization to main routine */
    lrInitComplete (OSIX_SUCCESS);

    while (1)
    {
        if (OsixEvtRecv (gElpsGlobalApsTxInfo.ApsTxTaskId, ELPS_APS_TX_EVENT,
                         OSIX_WAIT, &u4Events) == OSIX_SUCCESS)
        {
            ElpsTxEventHandler ();
        }
    }                            /* end of while */
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMainTaskInit 
 *                                                                          
 *    DESCRIPTION      : This function creates the task queue, allocates 
 *                       MemPools for task queue messages and creates protocol
 *                       semaphore. 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
ElpsMainTaskInit (VOID)
{
    MEMSET (&gElpsGlobalInfo, 0, sizeof (tElpsGlobalInfo));

    /* ELPS Protocol semaphore */
    if (OsixSemCrt (ELPS_PROTO_SEM, &(gElpsGlobalInfo.SemId)) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                   "ElpsMainTaskInit: Elps protocol semaphore creation "
                   "FAILED!!!\r\n"));
        return (OSIX_FAILURE);
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gElpsGlobalInfo.SemId);

    /* Semaphore for protecting APS Tx Info */
    if (OsixSemCrt (ELPS_APS_TX_DB_SEM,
                    &(gElpsGlobalApsTxInfo.ApsTxInfoSemId)) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                   "ElpsMainTaskInit: Elps APS Tx info database semaphore "
                   "creation FAILED!!!\r\n"));
        return (OSIX_FAILURE);
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (gElpsGlobalApsTxInfo.ApsTxInfoSemId);

    /* ELPS Task Q */
    if (OsixQueCrt ((UINT1 *) ELPS_TASK_QUEUE_NAME, OSIX_MAX_Q_MSG_LEN,
                    ELPS_MAX_Q_DEPTH, &(gElpsGlobalInfo.TaskQId))
        == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                   "ElpsMainTaskInit: ELPS Task Q Creation FAILED !!!\r\n"));
        return (OSIX_FAILURE);
    }

    if (ElpsMainModuleStart () == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "ElpsMainTaskInit: ELPS Module Start FAILED !!!\r\n"));
        return (OSIX_FAILURE);
    }

    if (OsixGetTaskId (SELF, ELPS_TASK_NAME, &(gElpsGlobalInfo.MainTaskId))
        == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC | ELPS_CRITICAL_TRC,
                   "ElpsMainTaskInit: Obtaining Task Id FAILED !!!\r\n"));
        return (OSIX_FAILURE);
    }

    return (OSIX_SUCCESS);
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMainTaskDeInit 
 *                                                                          
 *    DESCRIPTION      : This function deletes the task queue and protocol   
 *                       semaphore, de-allocates the task Q message MemPool 
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
ElpsMainTaskDeInit (VOID)
{
    ElpsMainModuleShutDown ();

    if (gElpsGlobalInfo.TaskQId != 0)
    {
        OsixQueDel (gElpsGlobalInfo.TaskQId);
        gElpsGlobalInfo.TaskQId = 0;
    }

    if (gElpsGlobalApsTxInfo.ApsTxInfoSemId != 0)
    {
        OsixSemDel (gElpsGlobalApsTxInfo.ApsTxInfoSemId);
        gElpsGlobalApsTxInfo.ApsTxInfoSemId = 0;
    }

    if (gElpsGlobalInfo.SemId != 0)
    {
        OsixSemDel (gElpsGlobalInfo.SemId);
        gElpsGlobalInfo.SemId = 0;
    }

    return;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMainModuleStart
 *                                                                          
 *    DESCRIPTION      : This function allocates memory pools for all tables 
 *                       in ELPS module. It also initalizes the global       
 *                       structure.                                          
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 *                                                                          
 ****************************************************************************/
PUBLIC INT4
ElpsMainModuleStart (VOID)
{
    UINT4               u4ContextId = 0;

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        gElpsGlobalInfo.au1SystemCtrl[u4ContextId] = ELPS_SHUTDOWN;
    }
    /* Initialize the global variables */
    gElpsGlobalInfo.u1GlobalTrcOption = ELPS_SNMP_FALSE;
    gElpsGlobalInfo.u4RapidTxTime = ELPS_MAX_RAPID_TX_TIME;
    gElpsGlobalInfo.u4PscChannelCode = ELPS_MIN_PSC_CHANNEL_CODE;
    MEMSET (gau4TnlTableOid, 0, ELPS_MAX_OID_LEN);
    MEMSET (gau4PwTableOid, 0, ELPS_MAX_OID_LEN);

    /* Initialize Database Mempools */
    if (ElpsSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC, "ElpsMainModuleStart:"
                   " Memory Initialization FAILED !!!\r\n"));
        ElpsSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }
    /*Assigning the respective mempools */
    ElpsMainAssignMempoolIds ();

    /* Initialize the SLLs */
    ElpsTxInitApsTxInfoSLL ();

    /* Timer Initialization */
    if (ElpsTmrInit () == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC, "ElpsMainModuleStart:"
                   " Timer Initialization FAILED !!!\r\n"));
        ElpsSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (ElpsRedInitGlobalInfo () == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC, "ElpsMainModuleStart:"
                   " Redundancy Global Info Initialization FAILED !!!\r\n"));
        ElpsTmrDeInit ();
        ElpsSizingMemDeleteMemPools ();
        return OSIX_FAILURE;
    }

    if (ElpsCxtCreateContext (ELPS_DEFAULT_CONTEXT_ID) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC, "ElpsMainModuleStart:"
                   " Default Context Initialization FAILED !!!\r\n"));
        ElpsTmrDeInit ();
        ElpsSizingMemDeleteMemPools ();
        ElpsRedDeInitGlobalInfo ();
        return OSIX_FAILURE;
    }

    gElpsGlobalInfo.u1IsElpsInitialized = OSIX_TRUE;
    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          
 *    FUNCTION NAME    : ElpsMainModuleShutDown
 *                                                                          
 *    DESCRIPTION      : This function shuts down the entire ELPS module.    
 *
 *    INPUT            : None
 *                                                                          
 *    OUTPUT           : None
 *                                                                          
 *    RETURNS          : None
 *                                                                          
 ****************************************************************************/
PUBLIC VOID
ElpsMainModuleShutDown (VOID)
{
    tElpsContextInfo   *pCxt = NULL;
    tElpsQMsg          *pMsg = NULL;
    UINT4               u4ContextId = 0;

    ELPS_LOCK ();

    if (gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE)
    {
        ELPS_UNLOCK ();
        return;
    }

    gElpsGlobalInfo.u1IsElpsInitialized = OSIX_FALSE;

    /* Block the access of Np Sync Table in case of standby and
     * block the sending of Np Sync-ups in case of active node during 
     * module shutdown.
     */
    ElpsRedHwAuditIncBlkCounter ();

    while (OsixQueRecv (gElpsGlobalInfo.TaskQId, (UINT1 *) &pMsg,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        ElpsMainReleaseQMemory (pMsg);
    }

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pCxt = ElpsCxtGetNode (u4ContextId);

        if (pCxt == NULL)
        {
            continue;
        }

        ElpsCxtDeleteContext (u4ContextId);
    }

    if (ElpsTmrDeInit () == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "ElpsMainModuleShutDown: Tmr DeInit FAILED !!!\r\n"));
    }

    /* Reset the Block the access of Np Sync Table in case of standby and
     * reset block the sending of Np Sync-ups in case of active node during 
     * module shutdown. Decrement the count.
     */
    ElpsRedHwAuditDecBlkCounter ();

    ElpsSizingMemDeleteMemPools ();
    ElpsRedDeInitGlobalInfo ();

    ELPS_UNLOCK ();
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsMainProcessEvent 
 *
 * DESCRIPTION      : This routine process the event and calls the event 
 *                    handler routine
 *
 * INPUT            : u4Event - Events
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsMainProcessEvent (UINT4 u4Events)
{
    /* Mutual exclusion flag ON */
    ELPS_LOCK ();

    if (u4Events & ELPS_QMSG_EVENT)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsMainProcessEvent: Received Q Event\r\n"));
        ElpsQueMsgHandler ();
    }

    if (u4Events & ELPS_TMR_EXPIRY_EVENT)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsMainProcessEvent: Received Timer Exp Event\r\n"));
        ElpsTmrExpHandler ();
    }

    /* Mutual exclusion flag OFF */
    ELPS_UNLOCK ();

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsMainReleaseQMemory 
 *
 * DESCRIPTION      : This routine releases the memory of the message in the 
 *                    queue. The data from RM, packet from ECFM, and MBSM are
 *                    released before releasing the Queue msg memory.
 *
 * INPUT            : pMsg - Queue Memory
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsMainReleaseQMemory (tElpsQMsg * pMsg)
{
    if ((pMsg->u4MsgType == ELPS_APS_PDU_MSG) ||
        (pMsg->u4MsgType == ELPS_PSC_PDU_MSG))
    {
        if (pMsg->unMsgParam.ApsPduMsg.pApsPdu != NULL)
        {
            /* Release the CRU buffer of the APS PDU enqueued by ECFM */
            CRU_BUF_Release_MsgBufChain (pMsg->unMsgParam.ApsPduMsg.pApsPdu,
                                         FALSE);
        }
    }
#ifdef L2RED_WANTED
    if (pMsg->u4MsgType == ELPS_RM_MSG)
    {
        if (pMsg->unMsgParam.RmCtrlMsg.pData != NULL)
        {
            if (pMsg->unMsgParam.RmCtrlMsg.u1Event == RM_MESSAGE)
            {
                /* Release the CRU buffer of the RM Message enqueued by RM */
                RM_FREE (pMsg->unMsgParam.RmCtrlMsg.pData);
            }
            else if ((pMsg->unMsgParam.RmCtrlMsg.u1Event == RM_STANDBY_UP) ||
                     (pMsg->unMsgParam.RmCtrlMsg.u1Event == RM_STANDBY_DOWN))
            {
                ElpsPortRmReleaseMemoryForMsg ((UINT1 *) pMsg->unMsgParam.
                                               RmCtrlMsg.pData);
            }
        }
    }

#endif

#ifdef MBSM_WANTED
    if ((pMsg->u4MsgType == MBSM_MSG_CARD_INSERT) ||
        (pMsg->u4MsgType == MBSM_MSG_CARD_REMOVE))
    {
        if (pMsg->unMsgParam.pMbsmProtoMsg != NULL)
        {
            /* Release the MBSM Acknowledgement message */
            MEM_FREE (pMsg->unMsgParam.pMbsmProtoMsg);
        }
    }
#endif

    MemReleaseMemBlock (gElpsGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
    return;
}

/*****************************************************************************/
/* Function Name      : ElpsMainAssignMempoolIds                             */
/*                                                                           */
/* Description        : This function is used to assign respective mempool   */
/*                       ID's                                                */
/*                                                                           */
/* Input (s)          : NONE.                                                */
/*                                                                           */
/* Output (s)         : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
ElpsMainAssignMempoolIds (VOID)
{
    /*tElpsQMsg */
    gElpsGlobalInfo.QMsgPoolId = ELPSMemPoolIds[MAX_ELPS_Q_MESG_SIZING_ID];

    /*tElpsContextInfo */
    gElpsGlobalInfo.ContextTablePoolId =
        ELPSMemPoolIds[MAX_ELPS_CONTEXTS_SIZING_ID];

    /*tElpsPgInfo */
    gElpsGlobalInfo.PgTablePoolId =
        ELPSMemPoolIds[MAX_ELPS_PROTECTION_GROUP_INFO_SIZING_ID];

    /*tElpsServiceListInfo */
    gElpsGlobalInfo.ServiceListTablePoolId =
        ELPSMemPoolIds[MAX_ELPS_SERVICE_LIST_INFO_SIZING_ID];

    /*tElpsServiceListPointerInfo */
    gElpsGlobalInfo.ServiceListPointerTablePoolId =
        ELPSMemPoolIds[MAX_ELPS_SYS_NUM_SERVICE_PTR_IN_LIST_SIZING_ID];

    /*tElpsCfmConfigInfo */
    gElpsGlobalInfo.CfmConfigTablePoolId =
        ELPSMemPoolIds[MAX_ELPS_CFM_CONFIG_INFO_SIZING_ID];

    /*tElpsApsTxInfo */
    gElpsGlobalApsTxInfo.ApsTxInfoTablePoolId =
        ELPSMemPoolIds[MAX_ELPS_APS_TX_INFO_SIZING_ID];

    gElpsGlobalApsTxInfo.ApsTxPduPoolId =
        ELPSMemPoolIds[MAX_ELPS_SIMULTANEOUS_CFM_SIGNAL_SIZING_ID];

    /*tElpsRedNpSyncEntry */
    gElpsGlobalInfo.RedGlobalInfo.HwAuditTablePoolId =
        ELPSMemPoolIds[MAX_ELPS_RED_NP_SYNC_ENTRIES_SIZING_ID];

    /*tMplsApiInInfo */
    gElpsGlobalInfo.MplsInApiPoolId =
        ELPSMemPoolIds[MAX_ELPS_MPLS_IN_API_COUNT_SIZING_ID];

    /*tMplsApiOutInfo */
    gElpsGlobalInfo.MplsOutApiPoolId =
        ELPSMemPoolIds[MAX_ELPS_MPLS_OUT_API_COUNT_SIZING_ID];

    /*tMplsPathId */
    gElpsGlobalInfo.MplsPathInfoPoolId =
        ELPSMemPoolIds[MAX_ELPS_MPLS_PATH_ID_INFO_SIZING_ID];
}

#endif /* _ELPSMAIN_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsmain.c                     */
/*-----------------------------------------------------------------------*/
