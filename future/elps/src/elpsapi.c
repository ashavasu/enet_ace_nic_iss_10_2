/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsapi.c,v 1.7 2010/11/28 18:24:02 siva Exp $
 *
 * Description: This file contains the APIs exported by ELPS module.
 *****************************************************************************/
#ifndef _ELPSAPI_C_
#define _ELPSAPI_C_

#include "elpsinc.h"
#include "mplsapi.h"

/***************************************************************************
 * FUNCTION NAME    : ElpsLock
 *
 * DESCRIPTION      : Function to take the mutual exclusion protocol 
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiLock (VOID)
{
    if (OsixSemTake (gElpsGlobalInfo.SemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsUnLock
 *
 * DESCRIPTION      : Function to release the mutual exclusion protocol 
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiUnLock (VOID)
{
    if (OsixSemGive (gElpsGlobalInfo.SemId) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiCreateContext
 *
 * DESCRIPTION      : This function posts a message to ELPS task's message
 *                    queue to indicate the creation of a context
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiCreateContext (UINT4 u4ContextId)
{
    tElpsQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE)
    {
        /* ELPS Task is not initialized */
        return OSIX_SUCCESS;
    }

    if ((pMsg = (tElpsQMsg *)
         MemAllocMemBlk (gElpsGlobalInfo.QMsgPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsApiCreateContext: Allocation "
                   "of memory for Queue Message FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    pMsg->u4MsgType = ELPS_CREATE_CONTEXT_MSG;
    pMsg->u4ContextId = u4ContextId;

    i4RetVal = ElpsQueEnqMsg (pMsg);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiDeleteContext
 *
 * DESCRIPTION      : This function posts a message to ELPS task's message
 *                    queue to indicate the deletion of a context
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiDeleteContext (UINT4 u4ContextId)
{
    tElpsQMsg          *pMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if (gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE)
    {
        /* ELPS Task is not initialized */
        return OSIX_SUCCESS;
    }

    if ((pMsg = (tElpsQMsg *)
         MemAllocMemBlk (gElpsGlobalInfo.QMsgPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsApiDeleteContext: Allocation "
                   "of memory for Queue Message FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    pMsg->u4MsgType = ELPS_DELETE_CONTEXT_MSG;
    pMsg->u4ContextId = u4ContextId;

    i4RetVal = ElpsQueEnqMsg (pMsg);

    if (i4RetVal == OSIX_SUCCESS)
    {
        L2MI_SYNC_TAKE_SEM ();
    }

    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiEcfmIndCallBack
 *
 * DESCRIPTION      : For receiving the APS, Signal Fail, Signal Ok for MEPs 
 *                    this function was registered with ECFM module. 
 *                    ECFM module will call this callback function whenever 
 *                    a Signal Fail/Signal Ok happens for an MEP, as well as 
 *                    on reception of the APS PDU.
 *
 * INPUT            : pEcfmEventNotification - Pointer to the structure 
 *                                             which consist of the necessary 
 *                                             information to identify the 
 *                                             SF, OK, APS PDU reception.
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE
 *
 **************************************************************************/
PUBLIC VOID
ElpsApiEcfmIndCallBack (tEcfmEventNotification * pEvent)
{
    tElpsQMsg          *pMsg = NULL;

    if (gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE)
    {
        /* ELPS task is not initialized */
        return;
    }

    if (ElpsUtilIsElpsStarted (pEvent->u4ContextId) == OSIX_FALSE)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                   "ElpsApiEcfmIndCallBack: ELPS module not started "
                   "in the context\r\n"));
        return;
    }

    if (ElpsRedGetNodeState () != RM_ACTIVE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsApiEcfmIndCallBack: Events from ECFM are not handled "
                   "at standby node\r\n"));
        return;
    }

    if ((pMsg = (tElpsQMsg *)
         MemAllocMemBlk (gElpsGlobalInfo.QMsgPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsApiEcfmIndCallBack: Allocation "
                   "of memory for Queue Message FAILED !!!\r\n"));
        return;
    }

    switch (pEvent->u4Event)
    {
        case ECFM_APS_FRAME_RECEIVED:
            pMsg->u4MsgType = ELPS_APS_PDU_MSG;
            pMsg->u4ContextId = pEvent->u4ContextId;
            pMsg->u1ServiceType = ELPS_PG_SERVICE_TYPE_VLAN;
            pMsg->u1ModId = ELPS_ECFM_APP_ID;
            pMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MegId = pEvent->u4MdIndex;
            pMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MeId = pEvent->u4MaIndex;
            pMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MepId = pEvent->u2MepId;
            pMsg->unMsgParam.ApsPduMsg.u1TlvOffset =
                pEvent->PduInfo.u1CfmPduOffset;
            pMsg->unMsgParam.ApsPduMsg.pApsPdu = pEvent->PduInfo.pu1Data;
            break;

        case ECFM_DEFECT_CONDITION_ENCOUNTERED:
            /* Fall Through */
        case ECFM_DEFECT_CONDITION_CLEARED:
            pMsg->u4MsgType = ELPS_CFM_SIGNAL_MSG;
            pMsg->u4ContextId = pEvent->u4ContextId;
            pMsg->u1ServiceType = ELPS_PG_SERVICE_TYPE_VLAN;
            pMsg->u1ModId = ELPS_ECFM_APP_ID;
            pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MegId =
                pEvent->u4MdIndex;
            pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MeId =
                pEvent->u4MaIndex;
            pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MepId =
                pEvent->u2MepId;

            if (pEvent->u4Event == ECFM_DEFECT_CONDITION_ENCOUNTERED)
            {
                pMsg->unMsgParam.MonitorStatus.u4Status = ELPS_CFM_SIGNAL_FAIL;
            }
            else
            {
                pMsg->unMsgParam.MonitorStatus.u4Status = ELPS_CFM_SIGNAL_OK;
            }
            break;

        default:
            MemReleaseMemBlock (gElpsGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
            return;
    }

    ElpsQueEnqMsg (pMsg);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiModuleShutDown 
 *
 * DESCRIPTION      : This API completely shuts the module down. RM uses this
 *                    API to restart the module to handle Communication lost 
 *                    and restored scenario.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsApiModuleShutDown (VOID)
{
    /* ELPS_LOCK and ELPS_UNLOCK is done inside this function itself */
    ElpsMainModuleShutDown ();
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiGetPscChannelCode 
 *
 * DESCRIPTION      : This API is used to get the PSC Channel Code Value.
 *
 * INPUT            : None
 *
 * OUTPUT           : pu4PscChannelCode - Pointer to PSC channel Code
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsApiGetPscChannelCode (UINT4 *pu4PscChannelCode)
{
    /* This API is used to fetch the administrator configured value 
     * for the PSC channel code. This support is provided as standards are yet 
     * to define a value to be used for PSC channel code.
     *  
     * Once IANA defines a value for PSC channel code, Hash defined constant 
     * shall be used wherever required, in place of this API.
     **/

    /* This API  is not protected by semaphore because: 
     *
     *  The MPLS-RTR module requires to invokes this API for checking 
     *  whether the received packet is a PSC packet or not.
     *
     *  As per the architecture, the MPLS-RTR module is not advised to 
     *  invoke higher layer module's API's with semaphore in order to 
     *  avoid possibility of dead-locks. 
     *
     *  Also since the channel code is a scalar value, 
     *  there is no risk of not protecting by semaphore.
     **/

    *pu4PscChannelCode = gElpsGlobalInfo.u4PscChannelCode;
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiNotificationRoutine 
 *
 * DESCRIPTION      : For receiving the Signal Fail, Signal Ok for MEPs 
 *                    this function was registered with MPLS OAM  module. 
 *                    MPLS OAM  module will call this callback function 
 *                    whenever a Signal Fail/Signal Ok 
 *                    happens for an MEP 
 *
 * INPUT            : u4ModId - Id of the Module that calls this  function.
 *                       pEventNotification - Pointer to the structure 
 *                                         which consist of the necessary 
 *                                         information to identify the 
 *                                         SF/OK.
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : NONE
 *
 **************************************************************************/
PUBLIC INT4
ElpsApiNotificationRoutine (UINT4 u4ModId, VOID *pEventNotification)
{
    tElpsQMsg          *pMsg = NULL;
    tMplsEventNotif    *pMplsEvent = NULL;

    if (gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE)
    {
        /* ELPS task is not initialized */
        return OSIX_FAILURE;
    }

    if (ElpsRedGetNodeState () != RM_ACTIVE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsApiNotificationRoutine: Events are not handled "
                   "at standby node\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMsg = (tElpsQMsg *)
         MemAllocMemBlk (gElpsGlobalInfo.QMsgPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsApiNotificationCallBack: "
                   "Allocation of memory for Queue Message FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    if (u4ModId == ELPS_MPLS_APP_ID)
    {
        pMplsEvent = (tMplsEventNotif *) pEventNotification;

        if (ElpsUtilIsElpsStarted (pMplsEvent->u4ContextId) == OSIX_FALSE)
        {
            MemReleaseMemBlock (gElpsGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
            ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                       "ElpsApiNotificationCallBack: ELPS module not started "
                       "in the context\r\n"));
            return OSIX_FAILURE;
        }

        switch (pMplsEvent->u2Event)
        {
            case MPLS_PSC_PACKET:
                pMsg->u4MsgType = ELPS_PSC_PDU_MSG;
                pMsg->u4ContextId = pMplsEvent->u4ContextId;
                pMsg->unMsgParam.ApsPduMsg.u1TlvOffset =
                    pMplsEvent->u1PayloadOffset;
                pMsg->unMsgParam.ApsPduMsg.pApsPdu = pMplsEvent->pBuf;

                if (pMplsEvent->u1ServiceType == OAM_SERVICE_TYPE_LSP)
                {
                    pMsg->u1ServiceType = ELPS_PG_SERVICE_TYPE_LSP;
                }
                else if (pMplsEvent->u1ServiceType == OAM_SERVICE_TYPE_PW)
                {
                    pMsg->u1ServiceType = ELPS_PG_SERVICE_TYPE_PW;
                }
                pMsg->u1ModId = ELPS_MPLS_APP_ID;
                /* Store the Meg information */
                if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_MEG_ID)
                {
                    pMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MegId =
                        pMplsEvent->PathId.MegId.u4MegIndex;
                    pMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MeId =
                        pMplsEvent->PathId.MegId.u4MeIndex;
                    pMsg->unMsgParam.ApsPduMsg.MonitorInfo.u4MepId =
                        pMplsEvent->PathId.MegId.u4MpIndex;
                }
                else if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_TUNNEL)
                {
                    if (ElpsTnlGetMegInfo (pMplsEvent,
                                           &pMsg->unMsgParam.ApsPduMsg.
                                           MonitorInfo) == OSIX_FAILURE)
                    {
                        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                   "ElpsApiNotificationRoutine: Failed to get "
                                   "Tunnel Info!!!!\r\n"));
                    }
                }
                else if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_PW)
                {
                    if (ElpsPwGetMegInfo (pMplsEvent,
                                          &pMsg->unMsgParam.ApsPduMsg.
                                          MonitorInfo) == OSIX_FAILURE)
                    {
                        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                   "ElpsApiNotificationRoutine: Failed to get "
                                   "Pseudowire Info!!!!\r\n"));
                    }
                }
                break;

            case MPLS_MEG_DOWN_EVENT:
                /* Fall Through */
            case MPLS_MEG_UP_EVENT:
            case MPLS_AIS_CONDITION_ENCOUNTERED:
            case MPLS_AIS_CONDITION_CLEARED:
                pMsg->u4MsgType = ELPS_CFM_SIGNAL_MSG;
                pMsg->u4ContextId = pMplsEvent->u4ContextId;
                pMsg->u1ModId = ELPS_MPLS_APP_ID;

                if (pMplsEvent->u1ServiceType == OAM_SERVICE_TYPE_LSP)
                {
                    pMsg->u1ServiceType = ELPS_PG_SERVICE_TYPE_LSP;
                }
                else if (pMplsEvent->u1ServiceType == OAM_SERVICE_TYPE_PW)
                {
                    pMsg->u1ServiceType = ELPS_PG_SERVICE_TYPE_PW;
                }
                /* Store the Meg information */
                if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_MEG_ID)
                {
                    pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MegId =
                        pMplsEvent->PathId.MegId.u4MegIndex;
                    pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MeId =
                        pMplsEvent->PathId.MegId.u4MeIndex;
                    pMsg->unMsgParam.MonitorStatus.MonitorInfo.u4MepId =
                        pMplsEvent->PathId.MegId.u4MpIndex;
                }
                else if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_TUNNEL)
                {
                    if (ElpsTnlGetMegInfo (pMplsEvent,
                                           &pMsg->unMsgParam.ApsPduMsg.
                                           MonitorInfo) == OSIX_FAILURE)
                    {
                        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                   "ElpsApiNotificationRoutine: Failed to get "
                                   "Tunnel Info!!!!\r\n"));
                    }
                }
                else if (pMplsEvent->PathId.u4PathType == MPLS_PATH_TYPE_PW)
                {
                    if (ElpsPwGetMegInfo (pMplsEvent,
                                          &pMsg->unMsgParam.ApsPduMsg.
                                          MonitorInfo) == OSIX_FAILURE)
                    {
                        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                                   "ElpsApiNotificationRoutine: Failed to get "
                                   "Pseudowire Info!!!!\r\n"));
                    }
                }
                if (pMplsEvent->u2Event == MPLS_MEG_DOWN_EVENT)
                {
                    pMsg->unMsgParam.MonitorStatus.u4Status =
                        ELPS_CFM_SIGNAL_FAIL;
                }
                else if (pMplsEvent->u2Event == MPLS_AIS_CONDITION_ENCOUNTERED)
                {
                    pMsg->unMsgParam.MonitorStatus.u4Status =
                        ELPS_CFM_SIGNAL_AIS;
                }
                else if (pMplsEvent->u2Event == MPLS_AIS_CONDITION_CLEARED)
                {
                    pMsg->unMsgParam.MonitorStatus.u4Status =
                        ELPS_CFM_SIGNAL_AIS_CLEAR;
                }
                else if (pMplsEvent->u2Event == MPLS_MEG_UP_EVENT)
                {
                    pMsg->unMsgParam.MonitorStatus.u4Status =
                        ELPS_CFM_SIGNAL_OK;
                }
                break;

            default:
                MemReleaseMemBlock (gElpsGlobalInfo.QMsgPoolId, (UINT1 *) pMsg);
                return OSIX_FAILURE;
        }
    }

    ElpsQueEnqMsg (pMsg);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiModuleStart 
 *
 * DESCRIPTION      : This API completely starts the module. RM uses this
 *                    API to restart the module to handle Communication lost 
 *                    and restored scenario.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC INT4
ElpsApiModuleStart (VOID)
{
    ELPS_LOCK ();
    if (ElpsMainModuleStart () == OSIX_FAILURE)
    {
        ELPS_UNLOCK ();
        return OSIX_FAILURE;
    }

    ELPS_UNLOCK ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsApiIsElpsStartedInContext
 *
 * DESCRIPTION      : API to query ELPS if ELPS is started in the given context
 *                    
 *
 * INPUT            : u4ContextId - Context Identifier
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_TRUE/OSIX_FALSE
 * 
 **************************************************************************/
PUBLIC INT1
ElpsApiIsElpsStartedInContext (UINT4 u4ContextId)
{
    INT1                i1Result = OSIX_FALSE;

    ELPS_LOCK ();

    i1Result = (INT1) ElpsUtilIsElpsStarted (u4ContextId);

    ELPS_UNLOCK ();
    return i1Result;
}

#endif /* _ELPSAPI_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsapi.c                      */
/*-----------------------------------------------------------------------*/
