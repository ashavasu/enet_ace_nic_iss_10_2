/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpspg.c,v 1.25 2016/09/23 10:13:25 siva Exp $
 *
 * Description: This file contains the Protection Group Table related
 * datastructure implementation and utility functions.
 *****************************************************************************/
#ifndef _ELPSPG_C_
#define _ELPSPG_C_

#include "elpsinc.h"
#include "mplsapi.h"

/***************************************************************************
 * FUNCTION NAME    : ElpsPgCreateTable
 *
 * DESCRIPTION      : This function creates the Protection Group Table i.e.
 *                    a RBTree. 
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                    for which the PG table needs to be created.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgCreateTable (tElpsContextInfo * pContextInfo)
{
    UINT4               u4RBNodeOffset = 0;

    u4RBNodeOffset = FSAP_OFFSETOF (tElpsPgInfo, PgInfoRBNode);

    if ((pContextInfo->PgInfoTbl =
         RBTreeCreateEmbedded (u4RBNodeOffset, ElpsPgRBCmp)) == NULL)
    {
        ELPS_TRC ((pContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsPgCreateTable: Creation of RBTree FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgDeleteTable
 *
 * DESCRIPTION      : This function deletes the Protection Group Table
 *                    (i.e. a RBTree). 
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                    for which the PG table needs to be deleted.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsPgDeleteTable (tElpsContextInfo * pContextInfo)
{
    RBTreeDestroy (pContextInfo->PgInfoTbl, ElpsPgRBFree, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgRBCmp
 *
 * DESCRIPTION      : RBTree Compare function for the Protection Group Table.
 *                    Index of this table is u4PgId.
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 * 
 * RETURNS          : ELPS_RB_EQUAL   - if all the keys matched for both
 *                                      the nodes
 *                    ELPS_RB_LESS    - if node pRBElem1's key is less than
 *                                      node pRBElem2's key.
 *                    ELPS_RB_GREATER - if node pRBElem1's key is greater
 *                                      than node pRBElem2's key.
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tElpsPgInfo        *pPgInfo1 = (tElpsPgInfo *) pRBElem1;
    tElpsPgInfo        *pPgInfo2 = (tElpsPgInfo *) pRBElem2;

    if (pPgInfo1->u4PgId > pPgInfo2->u4PgId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pPgInfo1->u4PgId < pPgInfo2->u4PgId)
    {
        return ELPS_RB_LESS;
    }
    return ELPS_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgRBFree
 *
 * DESCRIPTION      : This is a callback function registered with RBTree.
 *                    Whenever a PG Node is deleted from the PgInfoTbl RBTree
 *                    this function is called. This function clean all the 
 *                    links present within the PG Node and release the 
 *                    memory used by the PG node.
 *
 * INPUT            : pRBElem - pointer to the PG Node whose memory needs
 *                          to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    tElpsPgInfo        *pPgInfo = (tElpsPgInfo *) pRBElem;

    UNUSED_PARAM (u4Arg);

    ElpsPgDeActivate (pPgInfo->pContextInfo, pPgInfo);

    /* Delete the associated Cfm information */
    if (pPgInfo->WorkingEntity.pCfmConfig != NULL)
    {
        if (pPgInfo->u1CfmRowStatus != ACTIVE)
            /* Entry is not present in the CFM RBTree */
        {
            /* Release the memory alone */
            ElpsCfmRBFree ((tRBElem *) pPgInfo->WorkingEntity.pCfmConfig, 0);
            ElpsCfmRBFree ((tRBElem *) pPgInfo->WorkingReverseEntity.pCfmConfig,
                           0);
        }
        else
        {
            ElpsCfmDelNode (pPgInfo->pContextInfo,
                            pPgInfo->WorkingEntity.pCfmConfig);
            ElpsCfmDelNode (pPgInfo->pContextInfo,
                            pPgInfo->WorkingReverseEntity.pCfmConfig);
        }
    }
    if (pPgInfo->ProtectionEntity.pCfmConfig != NULL)
    {
        if (pPgInfo->u1CfmRowStatus != ACTIVE)
            /* Entry is not present in the CFM RBTree */
        {
            /* Release the memory alone */
            ElpsCfmRBFree ((tRBElem *) pPgInfo->ProtectionEntity.pCfmConfig, 0);
            ElpsCfmRBFree ((tRBElem *) pPgInfo->ProtectionReverseEntity.
                           pCfmConfig, 0);
        }
        else
        {
            ElpsCfmDelNode (pPgInfo->pContextInfo,
                            pPgInfo->ProtectionEntity.pCfmConfig);
            ElpsCfmDelNode (pPgInfo->pContextInfo,
                            pPgInfo->ProtectionReverseEntity.pCfmConfig);
        }
    }

    if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN) &&
        (pPgInfo->u1PgConfigType == ELPS_PG_TYPE_LIST))
    {
        /* Delete the associated service list info */
        ElpsSrvListDelAllNodesForPg (pPgInfo->pContextInfo, pPgInfo);
    }
    else if (((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
              (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)) &&
             (pPgInfo->u1PgConfigType == ELPS_PG_TYPE_LIST))
    {
        /* Delete the associated service list pointer info */
        ElpsSrvListPtrDelAllNodesForPg (pPgInfo->pContextInfo, pPgInfo);
    }

    /* Delete the associated PG share info */
    ElpsPgHandleProtPortConfig (pPgInfo->pContextInfo, pPgInfo, 0);
    /* setting prtection port value to invalid value will delete the
     * share table entry */

    pPgInfo->u1PgHwStatus = FALSE;

    RBTreeRemove (pPgInfo->pContextInfo->PgInfoTbl, (tRBElem *) pPgInfo);
    /* Release memory used by PG Node */
    MemReleaseMemBlock (gElpsGlobalInfo.PgTablePoolId, (UINT1 *) pPgInfo);

    pPgInfo = NULL;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgCreateNode
 *
 * DESCRIPTION      : This function creates the PG Info node. allocate the 
 *                    Memory form the pool and initialize the elements of
 *                    PG Info. 
 *                    Note: this call will not add the PG Node to the
 *                    PG Table. once PG node informations are filled up
 *                    properly the caller should call 
 *                    ElpsPgAddNodeToPgTable(). 
 *                    Or ElpsPgDelNode() to release the node.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsPgInfo * - pointer to the allocated memory block
 *                          for the PG Info node.
 *                    Or NULL  - In case memory allocation fails.
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgCreateNode (VOID)
{
    tElpsPgInfo        *pPgInfo = NULL;

    pPgInfo = (tElpsPgInfo *) MemAllocMemBlk (gElpsGlobalInfo.PgTablePoolId);

    if (pPgInfo == NULL)
    {
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC, "ElpsPgCreateNode: Allocation "
                   "of memory for PG Node FAILED !!!\r\n"));
        return NULL;
    }

    /* Initialize the PG Node information */
    MEMSET (pPgInfo, 0, sizeof (tElpsPgInfo));

    return pPgInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgAddNodeToPgTable
 *
 * DESCRIPTION      : This function add the PG Node to the PG Table. It also
 *                    update all the related links.
 *
 * INPUT            : pContextInfo - Pointer to the Context information Node
 *                    pPgInfo      - pointer to the PG Node that needs to be
 *                                   added in the PG Table.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgAddNodeToPgTable (tElpsContextInfo * pContextInfo, tElpsPgInfo * pPgInfo)
{
    /* Update the back pointer to the Context Info structure */
    pPgInfo->pContextInfo = pContextInfo;

    /* Add the node to the RBTree */
    if (RBTreeAdd (pContextInfo->PgInfoTbl, (tRBElem *) pPgInfo) != RB_SUCCESS)
    {
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC, "ElpsPgAddNodeToPgTable: "
                   "Failed to add node to PG Table\r\n"));
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgDelNode
 *
 * DESCRIPTION      : This function remove the PG Node from the PG Table, and
 *                    release the memory used by that node.
 *
 * INPUT            : pPgInfo      - pointer to the PG Node that needs to be
 *                                   added in the PG Table.
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsPgDelNode (tElpsPgInfo * pPgInfo)
{
    ElpsPgRBFree ((tRBElem *) pPgInfo, 0);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgGetFirstNodeInContext
 *
 * DESCRIPTION      : This function returns the first PG node from the 
 *                    PG table for a Context
 *
 * INPUT            : pContextInfo - Pointer to the Context information Node
 *
 * OUTPUT           : pPgInfo      - pointer to the PG Node that needs to be
 *                                   added in the PG Table.
 * 
 * RETURNS          : tElpsPgInfo *  - Pointer to the first PG node in the 
 *                                     PG table
 *                    Or NULL        - If no node is present in the PG table
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgGetFirstNodeInContext (tElpsContextInfo * pContextInfo)
{
    tElpsPgInfo        *pPgInfo = NULL;

    pPgInfo = (tElpsPgInfo *) RBTreeGetFirst (pContextInfo->PgInfoTbl);

    return pPgInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgGetNextNodeInContext
 *
 * DESCRIPTION      : This function returns the next PG node form the PG table
 *                    for the same context.
 *
 * INPUT            : pContextInfo - pointer to the Context information Node
 *                                   where the PG table is present.
 *                    pCurrentPgInfo      - Current Node
 *                    
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsPgInfo * - Returns the pointer to the next PG node
 *                                    in the PG table. if no next entry is 
 *                                    available in the PG table then it
 *                                    returns NULL.
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgGetNextNodeInContext (tElpsContextInfo * pContextInfo,
                            tElpsPgInfo * pCurrentPgInfo)
{
    tElpsPgInfo        *pNextPgInfo = NULL;

    pNextPgInfo = (tElpsPgInfo *)
        RBTreeGetNext (pContextInfo->PgInfoTbl,
                       (tRBElem *) pCurrentPgInfo, NULL);
    return pNextPgInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgGetNode
 *
 * DESCRIPTION      : This function serach the PG node in the PG table
 *                    based on the PGid, and returns the pointer to that
 *                    PG Node.
 *
 * INPUT            : pContextInfo - Pointer to the Context information Node
 *                    u4PgId       - Protection Group Id. (search index)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsPgInfo * - pointer to the PG node.
 *                                    In case no entry found in the PG table
 *                                    NULL will be returned.
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgGetNode (tElpsContextInfo * pContextInfo, UINT4 u4PgId)
{
    tElpsPgInfo        *pPgInfo = NULL;
    tElpsPgInfo         TmpPgInfo;

    TmpPgInfo.u4PgId = u4PgId;

    pPgInfo = (tElpsPgInfo *)
        RBTreeGet (pContextInfo->PgInfoTbl, (tRBElem *) & TmpPgInfo);

    return pPgInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgActivate
 *
 * DESCRIPTION      : This function activates a protection group.
 *
 *                    Note: RowStatus variable will not be updated here.
 *                    caller should update it as required.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                    pPgInfo      - Pointer to the protection group info
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgActivate (tElpsContextInfo * pContextInfo, tElpsPgInfo * pPgInfo)
{
    tElpsNotifyInfo     NotifyInfo;
    /* initialize the state event machine */

    /* Reset the state event machine variables */
    ElpsSemResetSemVariables (pPgInfo);

    pPgInfo->u1PgStatus = ELPS_PG_STATUS_WORK_PATH_ACTIVE;
    if (ElpsPortEcfmRegisterPerMep (pPgInfo) != OSIX_SUCCESS)
    {
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC, "ElpsPgActivate: "
                   "Failed to Register Maintenance Entities for PG %d\r\n",
                   pPgInfo->u4PgId));
        return OSIX_FAILURE;
    }

    /* Start the periodic timers */
    if (ELPS_GET_PG_A_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
    {
        ElpsTmrStartTimer (pPgInfo, ELPS_PERIODIC_APS_PDU_INTERVAL,
                           ELPS_APS_PERIODIC_TMR);
    }

    if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
        (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        /* Update MPLS DB Module */
        if (ElpsUtilUpdateMplsDb (pPgInfo,
                                  MPLS_LPS_PROT_AVAILABLE) == OSIX_FAILURE)
        {
            ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                       "nmhSetFsElpsPgConfigRowStatus: "
                       "MPLS Database Update Failed for PG(id-%d)"
                       "%d\r\n", pPgInfo->u4PgId));
            return OSIX_FAILURE;
        }

    }

    if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN) &&
                                    (pPgInfo->u1PgHwStatus == TRUE))
    {
        /* This scenario is hit, if there is any disturbance either in working
         * or protection path; In this scenario, the PG would have been created 
         * already. So, skip the PG creation */
        return OSIX_SUCCESS;
    }
    /* Indicate hw regarding the PG entry creation */
    if (ElpsPgSwitchDataPath (pContextInfo, pPgInfo, ELPS_PG_CREATE)
        != OSIX_SUCCESS)
    {
        pPgInfo->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
        pPgInfo->u1PgHwStatus = FALSE;
        NotifyInfo.u4ContextId = pContextInfo->u4ContextId;
        NotifyInfo.u4PgId = pPgInfo->u4PgId;
        NotifyInfo.u1PgStatus = pPgInfo->u1PgStatus;

        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_HW_PG_CRT_FAIL);
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC, "ElpsPgActivate: "
                   "Failed to program the hardware for PG(id-%d) with status "
                   "%d\r\n", pPgInfo->u4PgId, ELPS_PG_CREATE));
        return OSIX_FAILURE;
    }
    else
    {
        pPgInfo->u1PgHwStatus = TRUE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgDeActivate
 *
 * DESCRIPTION      : This function de-actives a protection group.
 *
 *                    Note: RowStatus variable will not be updated here.
 *                    caller should update it as required.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                    pPgInfo      - Pointer to the protection group info
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgDeActivate (tElpsContextInfo * pContextInfo, tElpsPgInfo * pPgInfo)
{
    tElpsNotifyInfo     NotifyInfo;

    if (pPgInfo->u1PgStatus == ELPS_PG_STATUS_DISABLE)
    {
        /* PG is already de-activated */
        return OSIX_SUCCESS;
    }
    /* Indicate hw regarding the PG entry deletion */
    if (ElpsPgSwitchDataPath (pContextInfo, pPgInfo, ELPS_PG_DELETE)
        != OSIX_SUCCESS)
    {
        pPgInfo->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
        NotifyInfo.u4ContextId = pContextInfo->u4ContextId;
        NotifyInfo.u4PgId = pPgInfo->u4PgId;
        NotifyInfo.u1PgStatus = pPgInfo->u1PgStatus;

        ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_HW_PG_DEL_FAIL);
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC, "ElpsPgDeActivate: "
                   "Failed to program the hardware for PG(id-%d) with status "
                   "%d\r\n", pPgInfo->u4PgId, ELPS_PG_DELETE));
    }

    /*setting PG Hardware status to FALSE */
    pPgInfo->u1PgHwStatus = FALSE;

    /* Stop all the timers associated with this PG */
    ElpsTmrStopTimer (pPgInfo, ELPS_HOLD_OFF_TMR);
    ElpsTmrStopTimer (pPgInfo, ELPS_HOLD_OFF_TMR_P);
    ElpsTmrStopTimer (pPgInfo, ELPS_WTR_TMR);
    ElpsTmrStopTimer (pPgInfo, ELPS_APS_PERIODIC_TMR);
    ElpsTmrStopTimer (pPgInfo, ELPS_LOR_TMR);

    /* Reset the state event machine variables */
    ElpsSemResetSemVariables (pPgInfo);

    ElpsPortEcfmDeRegisterPerMep (pPgInfo);

    /* Update the PG status */
    pPgInfo->u1PgStatus = ELPS_PG_STATUS_DISABLE;

    if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
        (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        /* Update MPLS DB Module */
        if (ElpsUtilUpdateMplsDb (pPgInfo,
                                  MPLS_LPS_PROT_NOT_APPLICABLE) == OSIX_FAILURE)
        {
            ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC, "ElpsPgDeActivate: "
                       "MPLS Database Update Failed for PG(id-%d)"
                       "%d\r\n", pPgInfo->u4PgId));
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgClearStatistics
 *
 * DESCRIPTION      : This function clears all the statistics for a 
 *                    protection group.
 *
 * INPUT            : pPgInfo - Pointer to the protection group whose 
 *                              statistics are need to be cleared
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsPgClearStatistics (tElpsPgInfo * pPgInfo)
{
    MEMSET (&(pPgInfo->Stats), 0, sizeof (tElpsStats));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgHandleProtPortConfig
 *
 * DESCRIPTION      : This function handles the protection port configuration
 *                    for a protection group. Based on the protection port
 *                    configuration change corresponding entry needs to be
 *                    added or deleted form the PgShareTbl. This function 
 *                    handles the required updation in PgShareTbl.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure
 *                    pPgInfo      - Pointer to the protection group 
 *                                   information
 *                    u4ProtPortId - Changed protection port id. If value 0 is
 *                                   passed as u4ProtPortId, it will delete
 *                                   the current entry form the table.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgHandleProtPortConfig (tElpsContextInfo * pContextInfo,
                            tElpsPgInfo * pPgInfo, UINT4 u4ProtPortId)
{
    tElpsPgInfo        *pPgShInfo = NULL;

    /* 1. Delete the existing node (if exist) */
    if (pPgInfo->ProtectionEntity.u4PortId != 0)
    {
        /* Entry present in the PgShareTbl for this protection port and
         * PG id, delete this entry */
        pPgShInfo =
            ElpsPgShareGetNode (pContextInfo,
                                pPgInfo->ProtectionEntity.u4PortId,
                                pPgInfo->u4PgId);
        /* Delete the node */
        ElpsPgShareDelNode (pContextInfo, pPgShInfo);
    }

    /* 2. Update the new protection port in the PG info */
    pPgInfo->ProtectionEntity.u4PortId = u4ProtPortId;

    /* 3. Add the new node to the PG Share table if PortId is not 0 */
    if (pPgInfo->ProtectionEntity.u4PortId != 0)
    {
        if (ElpsPgShareAddNodeToPgShareTable (pContextInfo, pPgInfo)
            != OSIX_SUCCESS)
        {
            ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsPgHandleProtPortConfig: Failed to change the "
                       "protection port information\r\n"));
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgEnableAllPgEntries
 *
 * DESCRIPTION      : This function activates all the PG entries present in 
 *                    the specified context.
 *
 * INPUT            : pCxt  - Pointer to the context information
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgEnableAllPgEntries (tElpsContextInfo * pCxt)
{
    tElpsPgInfo        *pPgInfo = NULL;
    tElpsPgInfo        *pNextPgInfo = NULL;

    pPgInfo = (tElpsPgInfo *) RBTreeGetFirst (pCxt->PgInfoTbl);

    while (pPgInfo != NULL)
    {
        if (pPgInfo->u1PgRowStatus == ACTIVE)
        {
            if (ElpsPgActivate (pCxt, pPgInfo) == OSIX_FAILURE)
            {
                ELPS_TRC ((pCxt, ALL_FAILURE_TRC, "ElpsPgEnableAllPgEntries: "
                           "ElpsPgActivate Failed for PG(id-%d)\r\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }
        pNextPgInfo = (tElpsPgInfo *) RBTreeGetNext (pCxt->PgInfoTbl,
                                                     (tRBElem *) pPgInfo, NULL);
        pPgInfo = pNextPgInfo;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgDisableAllPgEntries
 *
 * DESCRIPTION      : This function deactivates all the PG entries present in 
 *                    the specified context.
 *
 * INPUT            : pCxt  - Pointer to the context information
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgDisableAllPgEntries (tElpsContextInfo * pCxt)
{
    tElpsPgInfo        *pPgInfo = NULL;
    tElpsPgInfo        *pNextPgInfo = NULL;

    pPgInfo = (tElpsPgInfo *) RBTreeGetFirst (pCxt->PgInfoTbl);

    while (pPgInfo != NULL)
    {
        if (pPgInfo->u1PgRowStatus == ACTIVE)
        {
            if (ElpsPgDeActivate (pCxt, pPgInfo) == OSIX_FAILURE)
            {
                ELPS_TRC ((pCxt, ALL_FAILURE_TRC, "ElpsPgDisableAllPgEntries: "
                           "ElpsPgDeActivate Failed for PG(id-%d)\r\n",
                           pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
        }

        if (ElpsUtilUpdateL2iwfOnPgDisable (pPgInfo) == OSIX_FAILURE)
        {
            ELPS_TRC ((pCxt, ALL_FAILURE_TRC, "ElpsPgDisableAllPgEntries: "
                       "ElpsUtilUpdateL2iwfOnPgDisable Failed for PG(id-%d)\r\n",
                       pPgInfo->u4PgId));
            return OSIX_FAILURE;
        }

        pNextPgInfo = (tElpsPgInfo *) RBTreeGetNext (pCxt->PgInfoTbl,
                                                     (tRBElem *) pPgInfo, NULL);
        pPgInfo = pNextPgInfo;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgSwitchDataPath
 *
 * DESCRIPTION      : This function is called whenever a protection group
 *                    is created, deleted, or switching happens.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgInfo      - pointer to the protection group info 
 *                    u4Action     - status of the protection group. This 
 *                                   can contain the following values -
 *                       o ELPS_PG_CREATE - When the PG is made active, this 
 *                                          function will be called with 
 *                                          ELPS_PG_CREATE option.
 *                       o ELPS_PG_DELETE - When the PG is made in-active, 
 *                                          this function will be called with 
 *                                          ELPS_PG_DELETE option.
 *                       o ELPS_PG_SWITCH_TO_WORKING - This Action will be 
 *                                          set when there is a need to 
 *                                          switch the traffic back to 
 *                                          working entity (as per SEM). 
 *                       o ELPS_PG_SWITCH_TO_PROTECTION - This option will be 
 *                                          set when there is a need to 
 *                                          switch the traffic to protection 
 *                                          entity (Auto Protection Swithcing, 
 *                                          Force Switch, Manual Switch).
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPgSwitchDataPath (tElpsContextInfo * pContextInfo,
                      tElpsPgInfo * pPgInfo, UINT4 u4Action)
{
    tElpsHwPgSwitchInfo HwPgSwitchInfo;
    tElpsServiceListInfo *pPgSrvLst = NULL;
    tElpsServiceListPointerInfo *pPgSrvLstPtr = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1BrgPortType = CFA_INVALID_BRIDGE_PORT;
    UINT1               au1Temp1[ELPS_MAX_MAC_STR_LEN];
    UINT1               au1Temp2[ELPS_MAX_MAC_STR_LEN];

    MEMSET (&HwPgSwitchInfo, 0, sizeof (tElpsHwPgSwitchInfo));

    HwPgSwitchInfo.u1ServiceType = pPgInfo->u1ServiceType;
    if (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        if (ELPS_GET_PG_B_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
        {
            HwPgSwitchInfo.PgProtType.u1ArchType = ELPS_PG_ARCH_1_TO_1;
        }
        else
        {
            HwPgSwitchInfo.PgProtType.u1ArchType = ELPS_PG_ARCH_1_PLUS_1;
        }

        if (ELPS_GET_PG_A_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
        {
            HwPgSwitchInfo.PgProtType.u1CommChannel = ELPS_PG_COMM_CHANNEL_APS;
        }
        else
        {
            HwPgSwitchInfo.PgProtType.u1CommChannel =
                ELPS_PG_COMM_CHANNEL_NO_APS;
        }

        if (ELPS_GET_PG_A_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
        {
            HwPgSwitchInfo.PgProtType.u1Direction = ELPS_PG_BIDIRECTIONAL;
        }
        else
        {
            HwPgSwitchInfo.PgProtType.u1Direction = ELPS_PG_UNIDIRECTIONAL;
        }

        HwPgSwitchInfo.u4PgIngressPortId = pPgInfo->u4IngressPortId;
        HwPgSwitchInfo.u4PgWorkingPortId = pPgInfo->WorkingEntity.u4PortId;
        HwPgSwitchInfo.u4PgProtectionPortId =
            pPgInfo->ProtectionEntity.u4PortId;
        /* Adding the Physical ports which will be used for remote port check */
        HwPgSwitchInfo.u4PgWorkingPhyPort = pPgInfo->WorkingEntity.u4PortId;
        HwPgSwitchInfo.u4PgProtectionPhyPort =
            pPgInfo->ProtectionEntity.u4PortId;

        HwPgSwitchInfo.u1PgAction = (UINT1) u4Action;

        HwPgSwitchInfo.u4PgWorkingInstanceId = pPgInfo->u4WorkingInstanceId;
        HwPgSwitchInfo.u4PgProtectionInstanceId =
            pPgInfo->u4ProtectionInstanceId;
        HwPgSwitchInfo.i4PgVlanGroupManager = pContextInfo->i4VlanGroupManager;

        ElpsUtilGetPhyPortFromSispPort (&HwPgSwitchInfo.u4PgIngressPortId);
        ElpsUtilGetPhyPortFromSispPort (&HwPgSwitchInfo.u4PgWorkingPortId);
        ElpsUtilGetPhyPortFromSispPort (&HwPgSwitchInfo.u4PgProtectionPortId);

        if (ElpsUtilGetBrgPortType (pPgInfo->u4IngressPortId,
                                    &u1BrgPortType) == OSIX_FAILURE)
        {
            ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsPgSwitchDataPath: Retrieving Bridge port-type "
                       "information for the port %d Failed !!!!\r\n",
                       pPgInfo->u4IngressPortId));
            return OSIX_FAILURE;
        }

        HwPgSwitchInfo.u1PgIngressBrgPortType = u1BrgPortType;

        /* Send the Np Sync before calling NPAPI */
        ElpsHwAdSendPgNpSync (pContextInfo->u4ContextId, pPgInfo->u4PgId,
                              u4Action);
        if (ElpsRedGetNodeState () != RM_ACTIVE)
        {
            return OSIX_SUCCESS;
        }

        switch (pPgInfo->u1PgConfigType)
        {
            case ELPS_PG_TYPE_LIST:
                /* Get the first service list entry for this pg */
                pPgSrvLst = pPgInfo->unServiceList.pWorkingServiceList;
                while (pPgSrvLst != NULL)
                {
                    HwPgSwitchInfo.u4PgWorkingServiceValue =
                        pPgSrvLst->u4ServiceId;
                    /* In this case Protection service id will be same as
                     * working service id */
                    HwPgSwitchInfo.u4PgProtectionServiceValue =
                        pPgSrvLst->u4ServiceId;

                    /* Verify whether the working port is a member of working
                     * service or not*/

                    if (L2IwfMiIsVlanMemberPort (pContextInfo->u4ContextId,
                            (tVlanId)HwPgSwitchInfo.u4PgWorkingServiceValue,
                            HwPgSwitchInfo.u4PgWorkingPortId) == OSIX_TRUE)
                    {
                        HwPgSwitchInfo.u1PortVlanMembership = OSIX_TRUE;
                    }
                    else
                    {
                        HwPgSwitchInfo.u1PortVlanMembership = OSIX_FALSE;
                    }

                    i4RetVal = ElpsPortHwPgSwitchDataPath
                        (pContextInfo->u4ContextId, pPgInfo->u4PgId,
                         &HwPgSwitchInfo);

                    if (i4RetVal == OSIX_FAILURE)
                    {
                        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                                   "ElpsPgSwitchDataPath Failed !!!!\r\n"));
                        break;
                    }

                    pPgSrvLst =
                        ElpsSrvListGetNextNodeForPg (pPgInfo->pContextInfo,
                                                     pPgInfo, pPgSrvLst);
                }
                break;

            case ELPS_PG_TYPE_INDIVIDUAL:
                /* Fall through */
            case ELPS_PG_TYPE_ALL:

                HwPgSwitchInfo.u4PgWorkingServiceValue
                    = pPgInfo->WorkingEntity.ServiceParam.u4ServiceId;
                HwPgSwitchInfo.u4PgProtectionServiceValue
                    = pPgInfo->ProtectionEntity.ServiceParam.u4ServiceId;

                /* Verify whether the working port is a member of working
                 * service or not*/

                if (L2IwfMiIsVlanMemberPort (pContextInfo->u4ContextId,
                        (tVlanId)HwPgSwitchInfo.u4PgWorkingServiceValue,
                        HwPgSwitchInfo.u4PgWorkingPortId) == OSIX_TRUE)
                {
                    HwPgSwitchInfo.u1PortVlanMembership = OSIX_TRUE;
                }
                else
                {
                    HwPgSwitchInfo.u1PortVlanMembership = OSIX_FALSE;
                }

                i4RetVal = ElpsPortHwPgSwitchDataPath
                    (pContextInfo->u4ContextId, pPgInfo->u4PgId,
                     &HwPgSwitchInfo);

                if (i4RetVal == OSIX_FAILURE)
                {
                    ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                               "ElpsPgSwitchDataPath Failed !!!!\r\n"));
                }
                break;

            default:
                ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsPgSwitchDataPath: Invalid PG Config Type\r\n"));
                break;
        }
    }
    else if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
             (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        /* Service Type LSP or Pseudowire */
    {
        if (ELPS_GET_PG_B_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
        {
            HwPgSwitchInfo.PgProtType.u1ArchType = ELPS_PG_ARCH_1_TO_1;
        }
        else
        {
            HwPgSwitchInfo.PgProtType.u1ArchType = ELPS_PG_ARCH_1_PLUS_1;
        }

        if (ELPS_GET_PG_A_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
        {
            HwPgSwitchInfo.PgProtType.u1CommChannel = ELPS_PG_COMM_CHANNEL_PSC;
        }
        else
        {
            HwPgSwitchInfo.PgProtType.u1CommChannel =
                ELPS_PG_COMM_CHANNEL_NO_PSC;
        }

        if (ELPS_GET_PG_A_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
        {
            HwPgSwitchInfo.PgProtType.u1Direction = ELPS_PG_BIDIRECTIONAL;
        }
        else
        {
            HwPgSwitchInfo.PgProtType.u1Direction = ELPS_PG_UNIDIRECTIONAL;
        }

        HwPgSwitchInfo.u1PgAction = (UINT1) u4Action;

        /* Send the Np Sync before calling NPAPI */
        ElpsHwAdSendPgNpSync (pContextInfo->u4ContextId, pPgInfo->u4PgId,
                              u4Action);
        if (ElpsRedGetNodeState () != RM_ACTIVE)
        {
            return OSIX_SUCCESS;
        }

        switch (pPgInfo->u1PgConfigType)
        {
            case ELPS_PG_TYPE_LIST:
                /* Get the first service list entry for this PG */
                pPgSrvLstPtr =
                    pPgInfo->unServiceList.pWorkingServiceListPointer;
                while (pPgSrvLstPtr != NULL)
                {
                    if (ElpsUtilFillHwSwitchInfo (pContextInfo,
                                                  &HwPgSwitchInfo,
                                                  &(pPgSrvLstPtr->WorkingInfo),
                                                  &(pPgSrvLstPtr->
                                                    ProtectionInfo),
                                                  &(pPgSrvLstPtr->
                                                    WorkingRvrInfo),
                                                  &(pPgSrvLstPtr->
                                                    ProtectionRvrInfo),
                                                  pPgInfo->u1ServiceType) ==
                        OSIX_FAILURE)
                    {
                        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                                   "FillHWSwitchInfo Failed !!!!\r\n"));
                        break;
                    }

                    i4RetVal = ElpsPortHwPgSwitchDataPath
                        (pContextInfo->u4ContextId, pPgInfo->u4PgId,
                         &HwPgSwitchInfo);

                    if (i4RetVal == OSIX_FAILURE)
                    {
                        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                                   "ElpsPgSwitchDataPath Failed !!!!\r\n"));
                        break;
                    }

                    pPgSrvLstPtr =
                        ElpsSrvListPtrGetNextNodeForPg (pPgInfo->pContextInfo,
                                                        pPgInfo, pPgSrvLstPtr);
                }
                break;

            case ELPS_PG_TYPE_INDIVIDUAL:

                if (ElpsUtilFillHwSwitchInfo (pContextInfo,
                                              &HwPgSwitchInfo,
                                              &(pPgInfo->WorkingEntity.
                                                ServiceParam),
                                              &(pPgInfo->ProtectionEntity.
                                                ServiceParam),
                                              &(pPgInfo->WorkingReverseEntity.
                                                ServiceParam),
                                              &(pPgInfo->
                                                ProtectionReverseEntity.
                                                ServiceParam),
                                              pPgInfo->u1ServiceType) ==
                    OSIX_FAILURE)
                {
                    ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                               "FillHWSwitchInfo Failed !!!!\r\n"));
                }
                if (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
                {
                    ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                               "ElpsPgSwitchDataPath: ContextId: %d, PgId: %d, "
                               "InPort: %d, W.Port: %d, P.Port: %d, "
                               "W.Service: %d, "
                               "P.Service: %d, InPortType: %d, Action: %d\r\n",
                               pContextInfo->u4ContextId, pPgInfo->u4PgId,
                               HwPgSwitchInfo.u4PgIngressPortId,
                               HwPgSwitchInfo.u4PgWorkingPortId,
                               HwPgSwitchInfo.u4PgProtectionPortId,
                               HwPgSwitchInfo.u4PgWorkingServiceValue,
                               HwPgSwitchInfo.u4PgProtectionServiceValue,
                               HwPgSwitchInfo.u1PgIngressBrgPortType,
                               HwPgSwitchInfo.u1PgAction));
                }
                else if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                         (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
                {
                    MEMSET (au1Temp1, '\0', ELPS_MAX_MAC_STR_LEN);
                    MEMSET (au1Temp2, '\0', ELPS_MAX_MAC_STR_LEN);

                    SPRINTF ((CHR1 *) au1Temp1, "%02x:%02x:%02x:%02x:%02x:%02x",
                             HwPgSwitchInfo.au1WorkingNextHopMac[0],
                             HwPgSwitchInfo.au1WorkingNextHopMac[1],
                             HwPgSwitchInfo.au1WorkingNextHopMac[2],
                             HwPgSwitchInfo.au1WorkingNextHopMac[3],
                             HwPgSwitchInfo.au1WorkingNextHopMac[4],
                             HwPgSwitchInfo.au1WorkingNextHopMac[5]);

                    SPRINTF ((CHR1 *) au1Temp2, "%02x:%02x:%02x:%02x:%02x:%02x",
                             HwPgSwitchInfo.au1ProtectionNextHopMac[0],
                             HwPgSwitchInfo.au1ProtectionNextHopMac[1],
                             HwPgSwitchInfo.au1ProtectionNextHopMac[2],
                             HwPgSwitchInfo.au1ProtectionNextHopMac[3],
                             HwPgSwitchInfo.au1ProtectionNextHopMac[4],
                             HwPgSwitchInfo.au1ProtectionNextHopMac[5]);
                    ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                               "ElpsPgSwitchDataPath: ContextId: %d,"
                               "PgId: %d, "
                               "ArchxType: %d, Comm Channel: %d, Dir: %d, "
                               "W.Port: %d, W.R.Port: %d, "
                               "P.Port: %d, P.R.Port: %d, "
                               "W.Service: %d, W.R.Service: %d, "
                               "P.Service: %d, P.R.Service: %d, "
                               "Action: %d\r\n",
                               /*, W.NxtHopMac: %s, P.NxtHopMac: %s\r\n", */
                               pContextInfo->u4ContextId, pPgInfo->u4PgId,
                               HwPgSwitchInfo.PgProtType.u1ArchType,
                               HwPgSwitchInfo.PgProtType.u1CommChannel,
                               HwPgSwitchInfo.PgProtType.u1Direction,
                               HwPgSwitchInfo.u4PgWorkingPortId,
                               HwPgSwitchInfo.u4PgWorkingReversePathPortId,
                               HwPgSwitchInfo.u4PgProtectionPortId,
                               HwPgSwitchInfo.u4PgProtectionReversePathPortId,
                               HwPgSwitchInfo.u4PgWorkingServiceValue,
                               HwPgSwitchInfo.u4PgWorkingReverseServiceValue,
                               HwPgSwitchInfo.u4PgProtectionServiceValue,
                               HwPgSwitchInfo.u4PgProtectionReverseServiceValue,
                               HwPgSwitchInfo.
                               u1PgAction /*, au1Temp1, au1Temp2 */ ));
                }
                HwPgSwitchInfo.u4WorkingTnlHwId =
                    pPgInfo->ProtectionEntity.ServiceParam.LspServiceInfo.
                    u4WorkingHwTnlEgrIf;

                HwPgSwitchInfo.u4ProtectionTnlHwId =
                    pPgInfo->ProtectionEntity.ServiceParam.LspServiceInfo.
                    u4ProtectionHwTnlEgrIf;

                i4RetVal = ElpsPortHwPgSwitchDataPath
                    (pContextInfo->u4ContextId, pPgInfo->u4PgId,
                     &HwPgSwitchInfo);

                if (i4RetVal == OSIX_SUCCESS)
                {
                    pPgInfo->ProtectionEntity.ServiceParam.LspServiceInfo.
                        u4WorkingHwTnlEgrIf = HwPgSwitchInfo.u4WorkingTnlHwId;

                    pPgInfo->ProtectionEntity.ServiceParam.LspServiceInfo.
                        u4ProtectionHwTnlEgrIf =
                        HwPgSwitchInfo.u4ProtectionTnlHwId;
                }
                else
                {
                    ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                               "ElpsPgSwitchDataPath Failed !!!!\r\n"));
                }
                break;

            default:
                ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsPgSwitchDataPath: Invalid PG Config Type\r\n"));
                break;
        }
    }

    if (i4RetVal == OSIX_FAILURE)
    {
        /* Send the Np Sync if NPAPI Call has failed */
        ElpsHwAdSendPgNpSync (pContextInfo->u4ContextId, pPgInfo->u4PgId,
                              u4Action);
    }
    return i4RetVal;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgGetFirstNode 
 *
 * DESCRIPTION      : This routine returns the first protection group entry
 *                    of the first context. Considering the indices as context,
 *                    pg the indices are returned.
 *
 * INPUT            : None 
 *
 * OUTPUT           : pu4ContextId - Context Id
 *                    pu4PgId - Pg Id
 *                    pPg - Pointer to Pg information
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgGetFirstNode (UINT4 *pu4ContextId, UINT4 *pu4PgId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4Result = 0;

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pContextInfo = ElpsCxtGetNode (u4ContextId);
        i4Result = ElpsUtilIsElpsStarted (u4ContextId);

        if ((pContextInfo != NULL) && (i4Result == OSIX_TRUE))
        {
            pPgConfigInfo = ElpsPgGetFirstNodeInContext (pContextInfo);
            if (pPgConfigInfo != NULL)
            {
                *pu4ContextId = u4ContextId;
                *pu4PgId = pPgConfigInfo->u4PgId;
                return pPgConfigInfo;
            }
        }
    }
    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "No Pg Entry available in the system. \r\n"));
    return NULL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPgGetNextNode 
 *
 * DESCRIPTION      : This routine returns the next protection group entry
 *                    to the input entry. Considering the indices as context,
 *                    pg the indices are returned.
 *
 * INPUT            : u4ContextId - Context Id
 *                    u4PgId - Pg Id
 *
 * OUTPUT           : pu4NextContextId - Next Context Id
 *                    pu4NextPgId - Next Pg Id
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC tElpsPgInfo *
ElpsPgGetNextNode (UINT4 u4ContextId, UINT4 u4PgId,
                   UINT4 *pu4NextContextId, UINT4 *pu4NextPgId)
{
    tElpsPgInfo         PgConfigInfo;
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsPgInfo        *pNextPgConfigInfo = NULL;
    INT4                i4Result = 0;
    UINT4               u4CxtLoopCnt = 0;

    for (u4CxtLoopCnt = u4ContextId; u4CxtLoopCnt < ELPS_MAX_CONTEXTS;
         u4CxtLoopCnt++)
    {

        pContextInfo = ElpsCxtGetNode (u4CxtLoopCnt);
        i4Result = ElpsUtilIsElpsStarted (u4CxtLoopCnt);

        if ((pContextInfo != NULL) && (i4Result == OSIX_TRUE))
        {
            if (u4CxtLoopCnt != u4ContextId)
            {
                pNextPgConfigInfo = ElpsPgGetFirstNodeInContext (pContextInfo);
            }
            else
            {
                MEMSET (&PgConfigInfo, 0, sizeof (tElpsPgInfo));

                PgConfigInfo.u4PgId = u4PgId;

                pNextPgConfigInfo = ElpsPgGetNextNodeInContext (pContextInfo,
                                                                &PgConfigInfo);

            }

            if (pNextPgConfigInfo != NULL)
            {
                *pu4NextContextId = u4CxtLoopCnt;
                *pu4NextPgId = pNextPgConfigInfo->u4PgId;
                return pNextPgConfigInfo;
            }
        }

    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Next Protection Group Entry does not exist.\r\n"));
    return NULL;
}
#endif /* _ELPSPG_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpspg.c                      */
/*-----------------------------------------------------------------------*/
