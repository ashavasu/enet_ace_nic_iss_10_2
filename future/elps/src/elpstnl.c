/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstnl.c,v 1.2 2010/10/22 14:16:21 prabuc Exp $
 *
 * Description: This file contains the Mpls Tunnel related
 * utility functions.
 *****************************************************************************/
#ifndef _ELPSTNL_C_
#define _ELPSTNL_C_

#include "elpsinc.h"
#include "mplsapi.h"

/******************************************************************************
 * FUNCTION NAME    : ElpsTnlUpdateTnlTableBaseOid
 *
 * DESCRIPTION      : This function updates the global array 'gau4TnlTableOid'
 *                    with Tunnel table base OID
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 *****************************************************************************/
PUBLIC INT4
ElpsTnlUpdateTnlTableBaseOid (VOID)
{
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "ElpsTnlUpdateTnlTableBaseOid:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pOutMplsApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
             "ElpsTnlUpdateTnlTableBaseOid:"
             " Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    pInMplsApiInfo->u4SubReqType = MPLS_GET_BASE_TNL_OID;
    pInMplsApiInfo->u4ContextId = 0;
    pInMplsApiInfo->u4SrcModId = ELPS_MODULE;

    /* Here return value is not checked as the MplsApiHandleExternalRequest
     * function will not fail while fetching Tunnel table base OID */
    ElpsPortMplsApiHandleExtRequest (MPLS_GET_SERVICE_POINTER_OID,
                                     pInMplsApiInfo, pOutMplsApiInfo);

    if (pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen != 0)
    {
        MEMCPY (gau4TnlTableOid,
                pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen *
                sizeof (UINT4));
        gu4TnlTableBaseOidLen =
            (UINT4) pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen;
    }

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pOutMplsApiInfo);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTnlGetTnlServicePointer
 *
 * DESCRIPTION      : This function constructs Tunnel Row Pointer from 
 *                    Tunnel Information.
 *
 * INPUT            : pLspServiceInfo- Pointer to Tunnel Information
 *                    pPgConfigServicePointer- Pointer to the Tunnel Row
 *                                             Pointer 
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsTnlGetTnlServicePointer (tElpsLspInfo * pLspServiceInfo,
                             tSNMP_OID_TYPE * pPgConfigServicePointer)
{
    /* If the value is not configured */
    if (pLspServiceInfo->u4TnlIngressLsrId == 0)
    {
        MEMSET (pPgConfigServicePointer->pu4_OidList, 0,
                ELPS_TNL_TABLE_DEF_OFFSET * sizeof (UINT4));
        pPgConfigServicePointer->u4_Length = ELPS_ERR_OID_LEN;
    }
    else
    {
        pPgConfigServicePointer->u4_Length = ELPS_TNL_TABLE_DEF_OFFSET;
        MEMCPY (pPgConfigServicePointer->pu4_OidList, gau4TnlTableOid,
                ELPS_TNL_TABLE_DEF_OFFSET * sizeof (UINT4));

        /* Fill the Tunnel Index, instance, IngressIsrId, EgressLsrId */
        pPgConfigServicePointer->pu4_OidList[ELPS_TNL_INDEX_START_OFFSET] =
            pLspServiceInfo->u4TnlIndex;
        pPgConfigServicePointer->u4_Length++;

        pPgConfigServicePointer->pu4_OidList[ELPS_TNL_INST_START_OFFSET] =
            pLspServiceInfo->u4TnlInstance;
        pPgConfigServicePointer->u4_Length++;

        pPgConfigServicePointer->pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] =
            pLspServiceInfo->u4TnlIngressLsrId;
        pPgConfigServicePointer->u4_Length++;

        pPgConfigServicePointer->pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] =
            pLspServiceInfo->u4TnlEgressLsrId;
        pPgConfigServicePointer->u4_Length++;
    }

}

/***************************************************************************
 * FUNCTION NAME    : ElpsTnlGetMegInfo
 *
 * DESCRIPTION      : This function is used to get MEG information associated
 *                    with an MPLS Event
 *
 * INPUT            : pMplsEvent- Pointer to MPLS Event
 *                    pMonitorInfo- Pointer to MEG information
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsTnlGetMegInfo (tMplsEventNotif * pMplsEvent,
                   tElpsCfmMonitorInfo * pMonitorInfo)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;

    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsTnlGetMegInfo:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsTnlGetMegInfo:"
                   "Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Get the LSP information for MPLS */
    pMplsApiInInfo->u4SubReqType = 0;
    pMplsApiInInfo->u4ContextId = 0;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

    pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
        pMplsEvent->PathId.TnlId.u4SrcTnlNum;
    pMplsApiInInfo->InPathId.TnlId.u4LspNum = pMplsEvent->PathId.TnlId.u4LspNum;

    pMplsApiInInfo->InPathId.TnlId.SrcNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] =
        pMplsEvent->PathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0];

    pMplsApiInInfo->InPathId.TnlId.DstNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;

    pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] =
        pMplsEvent->PathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0];

    /* Api to get the Tunnel Information */
    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                         pMplsApiInInfo,
                                         pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);

        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                   "ElpsTnlGetMegInfo: Failed to get " "Tunnel Info!!!!\r\n"));
        return OSIX_FAILURE;
    }
    /* Store the Meg Information */
    pMonitorInfo->u4MegId = pMplsApiOutInfo->OutTeTnlInfo.MplsMegId.u4MegIndex;
    pMonitorInfo->u4MeId = pMplsApiOutInfo->OutTeTnlInfo.MplsMegId.u4MeIndex;
    pMonitorInfo->u4MepId = pMplsApiOutInfo->OutTeTnlInfo.MplsMegId.u4MpIndex;

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pMplsApiInInfo);
    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                        (UINT1 *) pMplsApiOutInfo);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTnlGetTunnelAddrType
 *
 * DESCRIPTION      : This function is used to get Address Type of the
 *                    Tunnel's Ingress and Egress Nodes.
 *
 * INPUT            : Service- Service Identifier
 *                    pMplsApiInInfo- Pointer to MplsApiInInfo
 *                    pMplsApiOutInfo- Pointer to MplsApiOutInfo
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsTnlGetTunnelAddrType (tSNMP_OID_TYPE Service,
                          tMplsApiInInfo * pMplsApiInInfo,
                          tMplsApiOutInfo * pMplsApiOutInfo)
{
    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Get the LSP information from SrcTnlNum and LspNum */
    pMplsApiInInfo->u4SubReqType = 0;
    pMplsApiInInfo->u4ContextId = 0;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

    pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
        Service.pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];
    pMplsApiInInfo->InPathId.TnlId.u4LspNum =
        Service.pu4_OidList[ELPS_TNL_INST_START_OFFSET];

    pMplsApiInInfo->InPathId.TnlId.SrcNodeId.u4NodeType = ELPS_ADDR_TYPE_UCAST;
    pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] =
        Service.pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];
    pMplsApiInInfo->InPathId.TnlId.DstNodeId.u4NodeType = ELPS_ADDR_TYPE_UCAST;
    pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] =
        Service.pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];
    /* Api to get the Tunnel Information */
    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                         pMplsApiInInfo,
                                         pMplsApiOutInfo) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                   "ElpsTnlGetTunnelAddrType: Failed to get "
                   "Tunnel Info!!!!\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#if 0
/***************************************************************************
 * FUNCTION NAME    : ElpsTnlCheckPortExist
 *
 * DESCRIPTION      : This function is used to get the Port related 
 *                    information associated and check whether it is
 *                    associated with given ports.
 *
 * INPUT            : pMplsEvent- Pointer to MPLS Event
 *                    pMonitorInfo- Pointer to MEG information
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsTnlCheckPortExist (tElpsLspInfo * pLspServiceInfo,
                       tMbsmPortInfo * pPortInfo)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4IfIndex = 0;
    UINT2               u2VlanId = 0;

    if ((pMplsApiInInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsTnlCheckPortExist:"
                   " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsTnlCheckPortExist:"
                   "Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

    /* Get the LSP information for MPLS */
    pMplsApiInInfo->u4SubReqType = 0;
    pMplsApiInInfo->u4ContextId = 0;
    pMplsApiInInfo->u4SrcModId = ELPS_MODULE;

    pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum = pLspServiceInfo->u4TnlIndex;
    pMplsApiInInfo->InPathId.TnlId.u4LspNum = pLspServiceInfo->u4TnlInstance;
    pMplsApiInInfo->InPathId.TnlId.u4SrcNodeId =
        pLspServiceInfo->u4TnlIngressLsrId;
    pMplsApiInInfo->InPathId.TnlId.u4DstNodeId =
        pLspServiceInfo->u4TnlEgressLsrId;

    /* Api to get the Tunnel Information */
    if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                         pMplsApiInInfo,
                                         pMplsApiOutInfo) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);

        ELPS_TRC ((NULL, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                   "ElpsTnlCheckPortExist: Failed to get "
                   "Tunnel Info!!!!\r\n"));
        return OSIX_FAILURE;
    }

    if (CfaGetIfInfo (pMplsApiOutInfo->OutTeTnlInfo.u4TnlIfIndex,
                      &CfaIfInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (CfaIfInfo.u1IfType == CFA_MPLS_TUNNEL)
    {
        if (CfaUtilGetIfIndexFromMplsTnlIf (pMplsApiOutInfo->OutTeTnlInfo.
                                            u4TnlIfIndex,
                                            &u4IfIndex, TRUE) == CFA_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* L3 interface */
        u4IfIndex = pMplsApiOutInfo->OutTeTnlInfo.u4TnlIfIndex;
    }

    if (CfaGetIfInfo (u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u2VlanId = CfaIfInfo.u2VlanId;
}
#endif

#endif /* _ELPSTNL_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpstnl.c                      */
/*-----------------------------------------------------------------------*/
