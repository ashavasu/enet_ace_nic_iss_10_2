/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstmr.c,v 1.8 2011/09/12 07:11:22 siva Exp $
 *
 * Description: This file contains ELPS timer related functions.
 *****************************************************************************/
#ifndef _ELPSTMR_C_
#define _ELPSTMR_C_

#include "elpsinc.h"

/* Proto types of the functions private to this file only */
PRIVATE VOID ElpsTmrInitTmrDesc PROTO ((VOID));
PRIVATE VOID ElpsTmrHoldOffTmrExp PROTO ((VOID *pArg));
PRIVATE VOID ElpsTmrApsPeriodicTmrExp PROTO ((VOID *pArg));
PRIVATE VOID ElpsTmrWtrTmrExp PROTO ((VOID *pArg));
PRIVATE VOID ElpsTmrLorTmrExp PROTO ((VOID *pArg));
PRIVATE VOID ElpsTmrProtectionHoldOffTmrExp PROTO ((VOID *pArg));
PUBLIC tElpsSemState gaElpsSemStateInfo[];
/***************************************************************************
 * FUNCTION NAME    : ElpsTmrInit
 *
 * DESCRIPTION      : This function creates a timer list for all the timers 
 *                    in ELPS module.
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsTmrInit (VOID)
{
    if (TmrCreateTimerList (ELPS_TASK_NAME, ELPS_TMR_EXPIRY_EVENT,
                            NULL, &(gElpsGlobalInfo.TmrListId)) == TMR_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "ElpsTmrInit: Timer list creation FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    ElpsTmrInitTmrDesc ();

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrDeInit
 *
 * DESCRIPTION      : This function deletes the timer list. 
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsTmrDeInit (VOID)
{
    if (TmrDeleteTimerList (gElpsGlobalInfo.TmrListId) == TMR_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                   "ElpsTmrDeInit: Timer list deletion FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (gElpsGlobalInfo.aTmrDesc, 0,
            (sizeof (tTmrDesc) * ELPS_MAX_TMR_TYPES));

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrInitTmrDesc
 *
 * DESCRIPTION      : This function intializes the timer desc for all 
 *                    the timers in ELPS module.  
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsTmrInitTmrDesc (VOID)
{
    gElpsGlobalInfo.aTmrDesc[ELPS_HOLD_OFF_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tElpsPgInfo, HoldOffTimer);
    gElpsGlobalInfo.aTmrDesc[ELPS_HOLD_OFF_TMR].TmrExpFn = ElpsTmrHoldOffTmrExp;

    gElpsGlobalInfo.aTmrDesc[ELPS_HOLD_OFF_TMR_P].i2Offset =
        (INT2) FSAP_OFFSETOF (tElpsPgInfo, ProtectionHoldOffTimer);
    gElpsGlobalInfo.aTmrDesc[ELPS_HOLD_OFF_TMR_P].TmrExpFn =
        ElpsTmrProtectionHoldOffTmrExp;

    gElpsGlobalInfo.aTmrDesc[ELPS_WTR_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tElpsPgInfo, WTRTimer);
    gElpsGlobalInfo.aTmrDesc[ELPS_WTR_TMR].TmrExpFn = ElpsTmrWtrTmrExp;
    gElpsGlobalInfo.aTmrDesc[ELPS_APS_PERIODIC_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tElpsPgInfo, PeriodicTimer);
    gElpsGlobalInfo.aTmrDesc[ELPS_APS_PERIODIC_TMR].TmrExpFn =
        ElpsTmrApsPeriodicTmrExp;
    gElpsGlobalInfo.aTmrDesc[ELPS_LOR_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tElpsPgInfo, LORTimer);
    gElpsGlobalInfo.aTmrDesc[ELPS_LOR_TMR].TmrExpFn = ElpsTmrLorTmrExp;
}

/****************************************************************************
 *
 *    FUNCTION NAME    : ElpsTmrExpHandler
 *
 *    DESCRIPTION      : This function is called whenever a timer expiry
 *                       event is received by ELPS task. Different timer
 *                       expiry handlers are called based on the timer type.
 *
 *    INPUT            : None
 *
 *    OUTPUT           : None
 *
 *    RETURNS          : None
 *
 ****************************************************************************/
PUBLIC VOID
ElpsTmrExpHandler (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = 0;
    INT2                i2Offset = 0;

    /* HITLESS RESTART */
    UINT1               u1HRTmrFlag = 0;

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gElpsGlobalInfo.TmrListId)) != NULL)
    {
        u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        if (u1TimerId < ELPS_MAX_TMR_TYPES)
        {
            i2Offset = gElpsGlobalInfo.aTmrDesc[u1TimerId].i2Offset;
            if (ElpsRedGetNodeState () != RM_ACTIVE)
            {
                /* No Processing of Timer expiry is done at Standby or Idle */
                ELPS_TRC ((NULL, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
                           "ElpsTmrExpHandler: "
                           "Timer Id %d expiry is not handled at "
                           "standby node\r\n", u1TimerId));
                continue;
            }

            /* Call the registered expired handler function */
            (*(gElpsGlobalInfo.aTmrDesc[u1TimerId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);

            /* HITLESS RESTART */
            if ((u1TimerId == ELPS_APS_PERIODIC_TMR) &&
                (ELPS_HR_STDY_ST_REQ_RCVD () == OSIX_TRUE) &&
                (u1HRTmrFlag == 0))
            {
                /* Periodic timer has been made to expire after processing
                 * steady state pkt request from RM. This flag indicates
                 * to send steady tail msg after sending all the steady
                 * state packets of ERPS to RM. */
                u1HRTmrFlag = 1;
            }
        }
    }                            /* End of while */

    /* HITLESS RESTART */
    if (u1HRTmrFlag == 1)
    {
        /* sending steady state tail msg to RM */
        ElpsRedHRSendStdyStTailMsg ();
        ELPS_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrHoldOffTmrExp
 *
 * DESCRIPTION      : This function handles the expiry of the Hold-Off timer
 *                    for working entity.
 *
 * INPUT            : pArg - Pointer to the structure whose timer has expired.
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsTmrHoldOffTmrExp (VOID *pArg)
{
    tElpsPgInfo        *pPg = (tElpsPgInfo *) pArg;

    ELPS_TRC ((pPg->pContextInfo, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
               "ElpsTmrHoldOffTmrExp: Hold Off timer expired. \r\n"));

    if (pPg->u1WorkingCfmStatus == ELPS_CFM_SIGNAL_FAIL_HOLD_OFF)
    {
        pPg->u1WorkingCfmStatus = ELPS_CFM_SIGNAL_FAIL;
        ElpsCfmApplyReceivedSignalToSEM (pPg, ELPS_LOCAL_COND_SF_W);
    }
    else
    {
        /* If the SF-W is recovered and the PgState is still in hold-off state 
           and SF-P has not declared , Revert the PG status based upon the 
           current state */
        if (pPg->u1PgStatus == ELPS_PG_STATUS_HOLD_OFF_STATE)
        {
            if (pPg->u1ProtectionCfmStatus != ELPS_CFM_SIGNAL_FAIL_HOLD_OFF)
            {
                /* Revert the PG status */
                if (gaElpsSemStateInfo[pPg->ApsInfo.u1ApsSemStateId].
                    u4RequestOrState == ELPS_APS_REQ_STATE_WTR)
                {
                    pPg->u1PgStatus = ELPS_PG_STATUS_WTR_STATE;
                }
                else
                {
                    pPg->u1PgStatus =
                        (gaElpsSemStateInfo[pPg->ApsInfo.u1ApsSemStateId].
                         u4WorkingStatus == ELPS_ACTIVE) ?
                        ELPS_PG_STATUS_WORK_PATH_ACTIVE :
                        ELPS_PG_STATUS_PROT_PATH_ACTIVE;
                }
            }
        }
        /* Send Dynamic sync-up message to stop the Hold-Off Timer at 
         * standby node as it has expired at active node.
         */
        ElpsRedSendDynamicSyncMsg (pPg, ELPS_HOLD_OFF_TMR, ELPS_TMR_STOP);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrProtectionHoldOffTmrExp
 *
 * DESCRIPTION      : This function handles the expiry of the Hold-Off timer
 *                    protection entity.
 *
 * INPUT            : pArg - Pointer to the structure whose timer has expired.
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsTmrProtectionHoldOffTmrExp (VOID *pArg)
{
    tElpsPgInfo        *pPg = (tElpsPgInfo *) pArg;

    ELPS_TRC ((pPg->pContextInfo, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
               "ElpsTmrHoldOffTmrExp: Hold Off timer expired. \r\n"));

    if (pPg->u1ProtectionCfmStatus == ELPS_CFM_SIGNAL_FAIL_HOLD_OFF)
    {
        pPg->u1ProtectionCfmStatus = ELPS_CFM_SIGNAL_FAIL;
        ElpsCfmApplyReceivedSignalToSEM (pPg, ELPS_LOCAL_COND_SF_P);
    }
    else
    {
        /* If the SF-P is recovered and the PgState is still in hold-off state 
           and SF-W has not declared , Revert the PG status based upon the  
           current state */
        if (pPg->u1PgStatus == ELPS_PG_STATUS_HOLD_OFF_STATE)
        {
            if (pPg->u1WorkingCfmStatus != ELPS_CFM_SIGNAL_FAIL_HOLD_OFF)
            {
                if (gaElpsSemStateInfo[pPg->ApsInfo.u1ApsSemStateId].
                    u4RequestOrState == ELPS_APS_REQ_STATE_WTR)
                {
                    pPg->u1PgStatus = ELPS_PG_STATUS_WTR_STATE;
                }
                else
                {
                    pPg->u1PgStatus =
                        (gaElpsSemStateInfo[pPg->ApsInfo.u1ApsSemStateId].
                         u4WorkingStatus == ELPS_ACTIVE) ?
                        ELPS_PG_STATUS_WORK_PATH_ACTIVE :
                        ELPS_PG_STATUS_PROT_PATH_ACTIVE;
                }
            }
        }

        /* Send Dynamic sync-up message to stop the Hold-Off Timer at 
         * standby node as it has expired at active node.
         */
        ElpsRedSendDynamicSyncMsg (pPg, ELPS_HOLD_OFF_TMR_P, ELPS_TMR_STOP);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrWtrTmrExp
 *
 * DESCRIPTION      : This function handles the expiry of the WTR timer.
 *
 * INPUT            : pArg - Pointer to the structure whose timer has expired.
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsTmrWtrTmrExp (VOID *pArg)
{
    tElpsPgInfo        *pPg = (tElpsPgInfo *) pArg;

    ELPS_TRC ((pPg->pContextInfo, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
               "ElpsTmrWtrTmrExp: WTR timer expired. \r\n"));

    ElpsCfmApplyReceivedSignalToSEM (pPg, ELPS_LOCAL_COND_WTR_EXP);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrApsPeriodicTmrExp
 *
 * DESCRIPTION      : This function handles the expiry of the APS periodic 
 *                    timer.
 *
 * INPUT            : pArg - Pointer to the structure whose timer has expired.
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsTmrApsPeriodicTmrExp (VOID *pArg)
{
    tElpsPgInfo        *pPg = (tElpsPgInfo *) pArg;

    ELPS_TRC ((pPg->pContextInfo, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
               "ElpsTmrApsPeriodicTmrExp: APS periodic timer expired. \r\n"));

    ElpsTxFormAndSendAPSPdu (pPg);

    if (ElpsTmrStartTimer (pPg, pPg->u2PeriodicTime,
                           ELPS_APS_PERIODIC_TMR) == OSIX_FAILURE)
    {
        ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                   "ElpsTmrApsPeriodicTmrExp: Timer Start FAILED !!!\r\n"));
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrLorTmrExp
 *
 * DESCRIPTION      : This function handles the expiry of the Lack of response
 *                    timer.
 *
 * INPUT            : pArg - Pointer to the structure whose timer has expired.
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PRIVATE VOID
ElpsTmrLorTmrExp (VOID *pArg)
{
    tElpsNotifyInfo     NotifyInfo;
    tElpsPgInfo        *pPg = (tElpsPgInfo *) pArg;

    ELPS_TRC ((pPg->pContextInfo, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
               "ElpsTmrLorTmrExp: LOR timer expired. \r\n"));

    NotifyInfo.u4ContextId = pPg->pContextInfo->u4ContextId;
    NotifyInfo.u4PgId = pPg->u4PgId;
    NotifyInfo.u1PgStatus = pPg->u1PgStatus;

    ElpsTrapSendTrapNotifications (&NotifyInfo, ELPS_TRAP_LOR);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrStartTimer
 *
 * DESCRIPTION      : This function starts the specified timer.
 *
 * INPUT            : pPg       - Pointer to the Protection Group structure.
 *                    u4TmrInterval - Time interval for which timer must run
 *                    u1TmrType - Indicates which timer to start.
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsTmrStartTimer (tElpsPgInfo * pPg, UINT4 u4TmrInterval, UINT1 u1TmrType)
{
    UINT4               u4RemainingTime = 0;

    ELPS_TRC ((pPg->pContextInfo, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
               "ElpsTmrStartTimer: Timer started for type %d. \r\n",
               u1TmrType));

    switch (u1TmrType)
    {
        case ELPS_HOLD_OFF_TMR:
            TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                                 &(pPg->HoldOffTimer.TimerNode),
                                 &u4RemainingTime);

            if (u4RemainingTime == 0)
            {
                if (TmrStart (gElpsGlobalInfo.TmrListId, &(pPg->HoldOffTimer),
                              u1TmrType, 0, (u4TmrInterval *
                                             ELPS_PG_HO_TIME_GRANULARITY)) ==
                    TMR_FAILURE)
                {
                    ELPS_TRC ((pPg->pContextInfo,
                               OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                               "ElpsTmrStartTimer: Hold Off Timer Start "
                               "FAILED !!!\r\n"));
                    return OSIX_FAILURE;
                }
                /* update the PG Status */
                pPg->u1PgStatus = ELPS_PG_STATUS_HOLD_OFF_STATE;

                /* Send the Dynamic message to synchronize the working entity 
                 * Hold-Off timer here.
                 */
                ElpsRedSendDynamicSyncMsg (pPg, ELPS_HOLD_OFF_TMR,
                                           ELPS_TMR_START);
            }
            break;

        case ELPS_HOLD_OFF_TMR_P:
            TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                                 &(pPg->ProtectionHoldOffTimer.TimerNode),
                                 &u4RemainingTime);

            if (u4RemainingTime == 0)
            {
                if (TmrStart
                    (gElpsGlobalInfo.TmrListId, &(pPg->ProtectionHoldOffTimer),
                     u1TmrType, 0,
                     (u4TmrInterval * ELPS_PG_HO_TIME_GRANULARITY)) ==
                    TMR_FAILURE)
                {
                    ELPS_TRC ((pPg->pContextInfo,
                               OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                               "ElpsTmrStartTimer:Protection entity Hold Off "
                               "Timer Start FAILED !!!\r\n"));
                    return OSIX_FAILURE;
                }
                /* update the PG Status */
                pPg->u1PgStatus = ELPS_PG_STATUS_HOLD_OFF_STATE;

                /* Send the Dynamic message to synchronize the Protection entity 
                 * Hold-Off timer here.
                 */
                ElpsRedSendDynamicSyncMsg (pPg, ELPS_HOLD_OFF_TMR_P,
                                           ELPS_TMR_START);
            }
            break;

        case ELPS_WTR_TMR:
            if (TmrStart (gElpsGlobalInfo.TmrListId, &(pPg->WTRTimer),
                          u1TmrType,
                          (u4TmrInterval * ELPS_SECONDS_IN_ONE_MINUTE), 0)
                == TMR_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsTmrStartTimer: WTR Timer Start FAILED.\r\n"));
                return OSIX_FAILURE;
            }
            break;

        case ELPS_APS_PERIODIC_TMR:
            if (TmrStart (gElpsGlobalInfo.TmrListId, &(pPg->PeriodicTimer),
                          u1TmrType, u4TmrInterval, 0) == TMR_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsTmrStartTimer: APS Periodic Timer Start "
                           "FAILED !!!\r\n"));
                return OSIX_FAILURE;
            }
            break;

        case ELPS_LOR_TMR:
            if (TmrRestart (gElpsGlobalInfo.TmrListId, &(pPg->LORTimer),
                            u1TmrType, 0, u4TmrInterval) == TMR_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsTmrStartTimer: LOR Timer Start FAILED.\r\n"));
                return OSIX_FAILURE;
            }
            break;

        default:
            ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "ElpsTmrStartTimer: Invalid Timer type FAILED !!!\r\n"));
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsTmrStopTimer
 *
 * DESCRIPTION      : This function stops the specified timer.
 *
 * INPUT            : pPg       - Pointer to the Protection Group structure.
 *                    u1TmrType - Indicates which timer to stop.
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsTmrStopTimer (tElpsPgInfo * pPg, UINT1 u1TmrType)
{
    ELPS_TRC ((pPg->pContextInfo, (CONTROL_PLANE_TRC | ALL_FAILURE_TRC),
               "ElpsTmrStopTimer: Timer stopped for type %d. \r\n", u1TmrType));

    switch (u1TmrType)
    {
        case ELPS_HOLD_OFF_TMR:
            if (pPg->u2HoldOffTime == 0)
            {
                /* If the duration of the timer is 0, then timer was not  
                 * started previosly. */
                return OSIX_SUCCESS;
            }
            if (TmrStop (gElpsGlobalInfo.TmrListId,
                         &(pPg->HoldOffTimer)) == TMR_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsTmrStopTimer: Working Hold Off Timer Stop "
                           "FAILED !!!\r\n"));
                return OSIX_FAILURE;
            }
            /* If the SF-W is recovered and the PgState is still in hold-off state
             * and SF-P has not declared , Revert the PG status to Working
             * active */
            if (pPg->u1PgStatus == ELPS_PG_STATUS_HOLD_OFF_STATE)
            {
                if (pPg->u1ProtectionCfmStatus != ELPS_CFM_SIGNAL_FAIL_HOLD_OFF)
                {
                    /* Revert the PG status */
                    pPg->u1PgStatus = ELPS_PG_STATUS_WORK_PATH_ACTIVE;
                }
            }

            break;

        case ELPS_HOLD_OFF_TMR_P:
            if (pPg->u2HoldOffTime == 0)
            {
                /* If the duration of the timer is 0, then timer was not  
                 * started previosly. */
                return OSIX_SUCCESS;
            }
            if (TmrStop (gElpsGlobalInfo.TmrListId,
                         &(pPg->ProtectionHoldOffTimer)) == TMR_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsTmrStopTimer: Protection Hold Off Timer Stop "
                           "FAILED !!!\r\n"));
                return OSIX_FAILURE;
            }
            /* If the SF-P is recovered and the PgState is still in hold-off state
             * and SF-W has not declared , Revert the PG status to Protection
             * active */
            if (pPg->u1PgStatus == ELPS_PG_STATUS_HOLD_OFF_STATE)
            {
                if (pPg->u1WorkingCfmStatus != ELPS_CFM_SIGNAL_FAIL_HOLD_OFF)
                {
                    /* Revert the PG status */
                    pPg->u1PgStatus = ELPS_PG_STATUS_PROT_PATH_ACTIVE;
                }
            }
            break;

        case ELPS_WTR_TMR:
            if (pPg->u2WtrTime == 0)
            {
                /* If the duration of the timer is 0, then timer was not  
                 * started previosly. */
                return OSIX_SUCCESS;
            }
            if (TmrStop (gElpsGlobalInfo.TmrListId,
                         &(pPg->WTRTimer)) == TMR_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsTmrStopTimer: WTR Timer Stop FAILED !!!\r\n"));
                return OSIX_FAILURE;
            }
            break;

        case ELPS_APS_PERIODIC_TMR:
            if (TmrStop (gElpsGlobalInfo.TmrListId,
                         &(pPg->PeriodicTimer)) == TMR_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsTmrStopTimer: APS Periodic Timer Stop "
                           "FAILED !!!\r\n"));
                return OSIX_FAILURE;
            }
            break;

        case ELPS_LOR_TMR:
            if (TmrStop (gElpsGlobalInfo.TmrListId,
                         &(pPg->LORTimer)) == TMR_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsTmrStopTimer: LOR Timer Stop FAILED !!!\r\n"));
                return OSIX_FAILURE;
            }
            break;

        default:
            ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                       "ElpsTmrStopTimer: Invalid Timer type FAILED !!!\r\n"));
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif /* _ELPSTMR_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpstmr.c                      */
/*-----------------------------------------------------------------------*/
