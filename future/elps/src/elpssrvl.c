/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpssrvl.c,v 1.5 2011/11/30 06:06:12 siva Exp $
 *
 * Description: This file contains the Protection Group Service List Table
 * related datastructure implementation and utility functions.
 *****************************************************************************/
#ifndef _ELPSSRVL_C_
#define _ELPSSRVL_C_

#include "elpsinc.h"

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListCreateTable
 *
 * DESCRIPTION      : This function creats the PG Service List table.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSrvListCreateTable (tElpsContextInfo * pContextInfo)
{
    UINT4               u4RBNodeOffset = 0;

    u4RBNodeOffset = FSAP_OFFSETOF (tElpsServiceListInfo, PgServiceListRBNode);

    if ((pContextInfo->PgServiceListTbl =
         RBTreeCreateEmbedded (u4RBNodeOffset, ElpsSrvListRBCmp)) == NULL)
    {
        ELPS_TRC ((pContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsSrvListCreateTable: Creation of RBTree "
                   "FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (pContextInfo->PgServiceListTbl);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListDeleteTable
 *
 * DESCRIPTION      : This function Deletes the PG Service List Table.
 *                    ElpsSrvListDeleteTable should not be called without 
 *                    destroying the PG table.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSrvListDeleteTable (tElpsContextInfo * pContextInfo)
{
    RBTreeDestroy (pContextInfo->PgServiceListTbl, ElpsSrvListRBFree, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the PG 
 *                    Service List Table. Indices of this table are -
 *                    o Protection Group Id
 *                    o Service Id
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 * 
 * RETURNS          : ELPS_RB_EQUAL   - if all the keys matched for both
 *                                      the nodes
 *                    ELPS_RB_LESS    - if node pRBElem1's key is less than
 *                                      node pRBElem2's key.
 *                    ELPS_RB_GREATER - if node pRBElem1's key is greater
 *                                      than node pRBElem2's key.
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSrvListRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tElpsServiceListInfo *pPgSrvLst1 = (tElpsServiceListInfo *) pRBElem1;
    tElpsServiceListInfo *pPgSrvLst2 = (tElpsServiceListInfo *) pRBElem2;

    /* 1st Index comparison */
    if (pPgSrvLst1->u4PgId > pPgSrvLst2->u4PgId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pPgSrvLst1->u4PgId < pPgSrvLst2->u4PgId)
    {
        return ELPS_RB_LESS;
    }

    /* 2nd Index comparison */
    if (pPgSrvLst1->u4ServiceId > pPgSrvLst2->u4ServiceId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pPgSrvLst1->u4ServiceId < pPgSrvLst2->u4ServiceId)
    {
        return ELPS_RB_LESS;
    }
    return ELPS_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a Service list node, and then release the
 *                    memory used by the node to the memory pool.
 *
 * INPUT            : pRBElem - pointer to the PG Node whose memory needs
 *                          to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSrvListRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    tElpsServiceListInfo *pPgSrvLst = (tElpsServiceListInfo *) pRBElem;

    UNUSED_PARAM (u4Arg);

    MemReleaseMemBlock (gElpsGlobalInfo.ServiceListTablePoolId,
                        (UINT1 *) pPgSrvLst);
    pPgSrvLst = NULL;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListCreateNode
 *
 * DESCRIPTION      : This function creates a service list node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListInfo * - Pointer to the created 
 *                           service list node. NULL will be returned if
 *                           node creation failed.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListInfo *
ElpsSrvListCreateNode (VOID)
{
    tElpsServiceListInfo *pPgSrvLst = NULL;

    pPgSrvLst = (tElpsServiceListInfo *)
        MemAllocMemBlk (gElpsGlobalInfo.ServiceListTablePoolId);

    if (pPgSrvLst == NULL)
    {
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC, "ElpsSrvListCreateNode: Allocation"
                   " of memory for Service List Node FAILED !!!\r\n"));
        return NULL;
    }

    /* Initialize the PG Service List information with default value */
    MEMSET (pPgSrvLst, 0, sizeof (tElpsServiceListInfo));

    return pPgSrvLst;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListAddNodeToSrvListTable
 *
 * DESCRIPTION      : This function add the service list node the the 
 *                    service list table, and takes care of updating all 
 *                    the required datastructure links.
 *
 * INPUT            : pContextInfo - Context information pointer
 *                    pPgInfo - pointer to protection group information node
 *                    pPgSrvLst - pointer to the service list node that 
 *                                needs to be added to the service
 *                                list table
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSrvListAddNodeToSrvListTable (tElpsContextInfo * pContextInfo,
                                  tElpsPgInfo * pPgInfo,
                                  tElpsServiceListInfo * pPgSrvLst)
{
    if (RBTreeAdd (pContextInfo->PgServiceListTbl, (tRBElem *) pPgSrvLst)
        != RB_SUCCESS)
    {
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsSrvListAddNodeToSrvListTable: Failed to add node to "
                   "PG Service List Table\r\n"));
        return OSIX_FAILURE;
    }

    /* Updation of first node pointer in PG info node */
    /* Following 3 situation is possible while adding a new entry
     * 1. New entry is the very first service list entry for a PG:
     *    If the pPgInfo->unServiceList.pWorkingServiceList is NULL 
     that means that
     *    currnet entry to be added is the very first entry for this 
     *    PG.
     * 2. Existing First service list entry's Service Id  is greater than 
     *    the New service list entry for this PG:
     *    By comparing the new nodes service id with the old first node
     *    service id, can identify this scenario.
     * 3. Adding a intermediate service list node for this PG:
     *    Nothing to be done for this scenario.
     *
     * In case of 1 and 2, update the fist node link in the PG entry,
     * after adding the new service list node to the RBTree
     */
    if ((pPgInfo->unServiceList.pWorkingServiceList == NULL)
        || (pPgSrvLst->u4ServiceId <
            pPgInfo->unServiceList.pWorkingServiceList->u4ServiceId))
    {
        pPgInfo->unServiceList.pWorkingServiceList = pPgSrvLst;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListRmvNodeInSrvListTable
 *
 * DESCRIPTION      : This function removes the service list node the the
 *                    service list table, and takes care of updating all
 *                    the required datastructure links.
 *
 * INPUT            : pContextInfo - Context information pointer
 *                    pPgInfo - pointer to protection group information node
 *                    pPgSrvLst - pointer to the service list node that
 *                                needs to be added to the service
 *                                list table
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ElpsSrvListRmvNodeInSrvListTable (tElpsContextInfo * pContextInfo,
                                  tElpsPgInfo * pPgInfo,
                                  tElpsServiceListInfo * pPgSrvLst)
{
    /* Updation of first node pointer in PG info node */
    /* Following 2 situations are possible while deleting a service list node
     * for a PG.
     * 1. Deleting the first service list node for this PG:
     *    If the node to be deleted is equal to pvtPgInfo->pWorkingServiceList
     *    that means we are deleting the first node. Get the next service list
     *    node for this PG and update the link in the PG info node, before
     *    deleting the service list node.
     * 2. Deleting intermediate service list node for this PG:
     *    Nothing to be done for this scenario.
     */
    if (pPgSrvLst == pPgInfo->unServiceList.pWorkingServiceList)
    {
        pPgInfo->unServiceList.pWorkingServiceList =
            ElpsSrvListGetNextNodeForPg (pContextInfo, pPgInfo, pPgSrvLst);
        /* If no more service list  node is present for this group NULL
         * will be returned, and stored in the pPgInfo->pWorkingServiceList */
    }

    if (RBTreeRemove (pContextInfo->PgServiceListTbl, (tRBElem *) pPgSrvLst)
        != RB_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListDelNode
 *
 * DESCRIPTION      : This function delete a service list node from the
 *                    service list table and release the memory used by that
 *                    node to the memory pool. It also takes care of 
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pContextInfo - Context information pointer
 *                    pPgInfo - pointer to protection group information node
 *                    pPgSrvLst - pointer to the service list node that 
 *                                needs to be deleted from the service
 *                                list table
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSrvListDelNode (tElpsContextInfo * pContextInfo,
                    tElpsPgInfo * pPgInfo, tElpsServiceListInfo * pPgSrvLst)
{
    /* Updation of first node pointer in PG info node */
    /* Following 2 situations are possible while deleting a service list node
     * for a PG.
     * 1. Deleting the first service list node for this PG:
     *    If the node to be deleted is equal to pvtPgInfo->pWorkingServiceList
     *    that means we are deleting the first node. Get the next service list
     *    node for this PG and update the link in the PG info node, before 
     *    deleting the service list node.
     * 2. Deleting intermediate service list node for this PG:
     *    Nothing to be done for this scenario.
     */
    if (pPgSrvLst == pPgInfo->unServiceList.pWorkingServiceList)
    {
        pPgInfo->unServiceList.pWorkingServiceList =
            ElpsSrvListGetNextNodeForPg (pContextInfo, pPgInfo, pPgSrvLst);
        /* If no more service list  node is present for this group NULL
         * will be returned, and stored in the 
         pPgInfo->unServiceList.pWorkingServiceList */
    }

    RBTreeRemove (pContextInfo->PgServiceListTbl, (tRBElem *) pPgSrvLst);

    ElpsSrvListRBFree ((tRBElem *) pPgSrvLst, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListDelAllNodesForPg
 *
 * DESCRIPTION      : This function deletes all the service list nodes 
 *                    associated with a protection group, and release the 
 *                    memory used by them. It also takes care of updating 
 *                    all the associated datastructure links.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgInfo      - pointer to the protection group info
 *                                   structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSrvListDelAllNodesForPg (tElpsContextInfo * pContextInfo,
                             tElpsPgInfo * pPgInfo)
{
    tElpsServiceListInfo *pFirstPgSrvLst = NULL;
    tElpsServiceListInfo *pNextPgSrvLst = NULL;

    /* Get the first PG linfo */
    pFirstPgSrvLst = pPgInfo->unServiceList.pWorkingServiceList;

    if (pFirstPgSrvLst == NULL)
    {
        /* no service list entry is present for this PG */
        return;
    }

    /* pFirstPgSrvLst is used to get the next node, so first node is 
     * deleted at the end */
    pNextPgSrvLst = ElpsSrvListGetNextNodeForPg (pContextInfo, pPgInfo,
                                                 pFirstPgSrvLst);

    while (pNextPgSrvLst != NULL)
    {

        /* Update the L2IWF to unmap the service list from u4WorkingInstanceId
         * Instance to default instance */
        if (pContextInfo->i4VlanGroupManager == ELPS_VLAN_GROUP_MANAGER_ELPS)
        {
            ElpsUtilSetVlanGroupIdForVlan (pContextInfo->u4ContextId,
                                           pNextPgSrvLst->u4ServiceId,
                                           ELPS_DEFAULT_VLAN_GROUP_ID);
        }

        ElpsSrvListDelNode (pContextInfo, pPgInfo, pNextPgSrvLst);
        pNextPgSrvLst = ElpsSrvListGetNextNodeForPg (pContextInfo, pPgInfo,
                                                     pFirstPgSrvLst);
    }

    if (pContextInfo->i4VlanGroupManager == ELPS_VLAN_GROUP_MANAGER_ELPS)
    {
        ElpsUtilSetVlanGroupIdForVlan (pContextInfo->u4ContextId,
                                       pFirstPgSrvLst->u4ServiceId,
                                       ELPS_DEFAULT_VLAN_GROUP_ID);
    }
    /* Delete the first node now */
    ElpsSrvListDelNode (pContextInfo, pPgInfo, pFirstPgSrvLst);

    /* Update the service list link in PG info node */
    pPgInfo->unServiceList.pWorkingServiceList = NULL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListGetFirstNode
 *
 * DESCRIPTION      : This function returns the first service list node
 *                    from the PG service list table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListInfo * - pointer to the first service
 *                           list info. NULL will be returned if there is
 *                           no node present in this table.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListInfo *
ElpsSrvListGetFirstNode (tElpsContextInfo * pContextInfo)
{
    tElpsServiceListInfo *pPgSrvLst = NULL;

    pPgSrvLst = (tElpsServiceListInfo *)
        RBTreeGetFirst (pContextInfo->PgServiceListTbl);

    return pPgSrvLst;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListGetNextNode
 *
 * DESCRIPTION      : This function returns the next service list info
 *                    from the PG service list table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pCurrentPgSrvLst - pointer to the current PG service
 *                                       list info.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListInfo * - Pointer to the next PG 
 *                           service list info. NULL will be returned if
 *                           no next node is found in this table.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListInfo *
ElpsSrvListGetNextNode (tElpsContextInfo * pContextInfo,
                        tElpsServiceListInfo * pCurrentPgSrvLst)
{
    tElpsServiceListInfo *pNextPgSrvLst = NULL;

    pNextPgSrvLst = (tElpsServiceListInfo *)
        RBTreeGetNext (pContextInfo->PgServiceListTbl,
                       (tRBElem *) pCurrentPgSrvLst, NULL);
    /* NULL will be returned if no next entry is present. */
    return pNextPgSrvLst;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListGetNextNodeForPg
 *
 * DESCRIPTION      : This function returns the next PG service list node
 *                    that is associated with a particular Protection Group.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgInfo - pointer to the protection group info 
 *                              structure
 *                    pCurrentPgSrvLst - pointer to the current PG service
 *                                       list info structure.
 *                    
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListInfo * - pointer to the next PG 
 *                            service list info node that is associated 
 *                            with the current protection group info.
 *                            NULL will be returned if there is no next
 *                            PG service list info node found for this
 *                            Protection group.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListInfo *
ElpsSrvListGetNextNodeForPg (tElpsContextInfo * pContextInfo,
                             tElpsPgInfo * pPgInfo,
                             tElpsServiceListInfo * pCurrentPgSrvLst)
{
    tElpsServiceListInfo *pNextPgSrvLst = NULL;

    pNextPgSrvLst = (tElpsServiceListInfo *)
        RBTreeGetNext (pContextInfo->PgServiceListTbl,
                       (tRBElem *) pCurrentPgSrvLst, NULL);

    if ((pNextPgSrvLst != NULL) && (pNextPgSrvLst->u4PgId == pPgInfo->u4PgId))
    {
        return pNextPgSrvLst;
    }

    return NULL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListGetNode
 *
 * DESCRIPTION      : This function helps to find a service list node from
 *                    the service list table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    u4PgId  - Protection group Id
 *                    u4ServiceId - Service Id
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListInfo * - Pointer to the service list
 *                           node that is found during search. NULL will be
 *                           returned if required node is not found in the
 *                           service list table.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListInfo *
ElpsSrvListGetNode (tElpsContextInfo * pContextInfo, UINT4 u4PgId,
                    UINT4 u4ServiceId)
{
    tElpsServiceListInfo *pPgSrvLst = NULL;
    tElpsServiceListInfo PgSrvLst;

    PgSrvLst.u4PgId = u4PgId;
    PgSrvLst.u4ServiceId = u4ServiceId;

    pPgSrvLst = (tElpsServiceListInfo *)
        RBTreeGet (pContextInfo->PgServiceListTbl, (tRBElem *) & PgSrvLst);

    return pPgSrvLst;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPointerCreateTable
 *
 * DESCRIPTION      : This function creats the PG Service List table.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSrvListPointerCreateTable (tElpsContextInfo * pContextInfo)
{
    UINT4               u4RBNodeOffset = 0;

    u4RBNodeOffset =
        FSAP_OFFSETOF (tElpsServiceListPointerInfo, PgServiceListPointerRBNode);

    if ((pContextInfo->PgServiceListPointerTbl =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               ElpsSrvListPointerRBCmp)) == NULL)
    {
        ELPS_TRC ((pContextInfo, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsSrvListPointerCreateTable: Creation of RBTree "
                   "FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    RBTreeDisableMutualExclusion (pContextInfo->PgServiceListPointerTbl);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPointerDeleteTable
 *
 * DESCRIPTION      : This function Deletes the PG Service List Table.
 *                    ElpsSrvListDeleteTable should not be called without 
 *                    destroying the PG table.
 *
 * INPUT            : pContextInfo - Pointer to the context info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSrvListPointerDeleteTable (tElpsContextInfo * pContextInfo)
{
    RBTreeDestroy (pContextInfo->PgServiceListPointerTbl,
                   ElpsSrvListPointerRBFree, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPointerRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the PG 
 *                    Service List Table. Indices of this table are -
 *                    o Protection Group Id
 *                    o Service Id
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 * 
 * RETURNS          : ELPS_RB_EQUAL   - if all the keys matched for both
 *                                      the nodes
 *                    ELPS_RB_LESS    - if node pRBElem1's key is less than
 *                                      node pRBElem2's key.
 *                    ELPS_RB_GREATER - if node pRBElem1's key is greater
 *                                      than node pRBElem2's key.
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSrvListPointerRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tElpsServiceListPointerInfo *pPgSrvLstPointer1 =
        (tElpsServiceListPointerInfo *) pRBElem1;
    tElpsServiceListPointerInfo *pPgSrvLstpointer2 =
        (tElpsServiceListPointerInfo *) pRBElem2;

    /* 1st Index comparison */
    if (pPgSrvLstPointer1->u4PgId > pPgSrvLstpointer2->u4PgId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pPgSrvLstPointer1->u4PgId < pPgSrvLstpointer2->u4PgId)
    {
        return ELPS_RB_LESS;
    }

    /* 2nd Index comparison */
    if (pPgSrvLstPointer1->u4PgServiceListId >
        pPgSrvLstpointer2->u4PgServiceListId)
    {
        return ELPS_RB_GREATER;
    }
    else if (pPgSrvLstPointer1->u4PgServiceListId <
             pPgSrvLstpointer2->u4PgServiceListId)
    {
        return ELPS_RB_LESS;
    }

    return ELPS_RB_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPointerRBFree
 *
 * DESCRIPTION      : This function is used to clean all the links
 *                    related to a Service list node, and then release the
 *                    memory used by the node to the memory pool.
 *
 * INPUT            : pRBElem - pointer to the PG Node whose memory needs
 *                          to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSrvListPointerRBFree (tRBElem * pRBElem, UINT4 u4Arg)
{
    tElpsServiceListPointerInfo *pPgSrvLstPtr =
        (tElpsServiceListPointerInfo *) pRBElem;

    UNUSED_PARAM (u4Arg);

    MemReleaseMemBlock (gElpsGlobalInfo.ServiceListPointerTablePoolId,
                        (UINT1 *) pPgSrvLstPtr);
    pPgSrvLstPtr = NULL;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPointerCreateNode
 *
 * DESCRIPTION      : This function creates a service list node (allocate
 *                    memory needed by the node), and initialize it with
 *                    default values.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListPointerInfo * - Pointer to the created 
 *                           service list node. NULL will be returned if
 *                           node creation failed.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListPointerInfo *
ElpsSrvListPointerCreateNode (VOID)
{
    tElpsServiceListPointerInfo *pPgSrvLstPtr = NULL;

    pPgSrvLstPtr = (tElpsServiceListPointerInfo *)
        MemAllocMemBlk (gElpsGlobalInfo.ServiceListPointerTablePoolId);

    if (pPgSrvLstPtr == NULL)
    {
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC,
                   "ElpsSrvListPointerCreateNode: Allocation"
                   " of memory for Service List Node FAILED !!!\r\n"));
        return NULL;
    }

    /* Initialize the PG Service List information with default value */
    MEMSET (pPgSrvLstPtr, 0, sizeof (tElpsServiceListPointerInfo));

    return pPgSrvLstPtr;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListAddNodeToSrvListPtrTable
 *
 * DESCRIPTION      : This function add the service list node the the 
 *                    service list table, and takes care of updating all 
 *                    the required datastructure links.
 *
 * INPUT            : pContextInfo - Context information pointer
 *                    pPgInfo - pointer to protection group information node
 *                    pPgSrvLstPtr - pointer to the service list node that 
 *                                needs to be added to the service
 *                                list table
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSrvListAddNodeToSrvListPtrTable (tElpsContextInfo * pContextInfo,
                                     tElpsPgInfo * pPgInfo,
                                     tElpsServiceListPointerInfo * pPgSrvLstPtr)
{
    if (RBTreeAdd
        (pContextInfo->PgServiceListPointerTbl,
         (tRBElem *) pPgSrvLstPtr) != RB_SUCCESS)
    {
        ELPS_TRC ((pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsSrvListAddNodeToSrvListPtrTable: Failed to add node to "
                   "PG Service List pointer Table\r\n"));
        return OSIX_FAILURE;
    }

    /* Updation of first node pointer in PG info node */
    /* Following 3 situation is possible while adding a new entry
     * 1. New entry is the very first service list entry for a PG:
     *    If the pPgInfo->pWorkingServicePointerList is NULL that means that
     *    currnet entry to be added is the very first entry for this 
     *    PG.
     * 2. Existing First service list entry's Service Id  is greater than 
     *    the New service list entry for this PG:
     *    By comparing the new nodes service id with the old first node
     *    service id, can identify this scenario.
     * 3. Adding a intermediate service list node for this PG:
     *    Nothing to be done for this scenario.
     *
     * In case of 1 and 2, update the fist node link in the PG entry,
     * after adding the new service list node to the RBTree
     */
    if ((pPgInfo->unServiceList.pWorkingServiceListPointer == NULL)
        || (pPgSrvLstPtr->u4PgServiceListId <
            pPgInfo->unServiceList.pWorkingServiceListPointer->
            u4PgServiceListId))
    {
        pPgInfo->unServiceList.pWorkingServiceListPointer = pPgSrvLstPtr;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListRmvNodeInSrvListPtrTable
 *
 * DESCRIPTION      : This function removes the service list node the the
 *                    service list table, and takes care of updating all
 *                    the required datastructure links.
 *
 * INPUT            : pContextInfo - Context information pointer
 *                    pPgInfo - pointer to protection group information node
 *                    pPgSrvLst - pointer to the service list node that
 *                                needs to be added to the service
 *                                list table
 *
 * OUTPUT           : None
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
ElpsSrvListRmvNodeInSrvListPtrTable (tElpsContextInfo * pContextInfo,
                                     tElpsPgInfo * pPgInfo,
                                     tElpsServiceListPointerInfo * pPgSrvLstPtr)
{
    /* Updation of first node pointer in PG info node */
    /* Following 2 situations are possible while deleting a service list node
     * for a PG.
     * 1. Deleting the first service list node for this PG:
     *    If the node to be deleted is equal to 
     pvtPgInfo->unServiceList.pWorkingServiceListPointer
     *    that means we are deleting the first node. Get the next service list
     *    node for this PG and update the link in the PG info node, before
     *    deleting the service list node.
     * 2. Deleting intermediate service list node for this PG:
     *    Nothing to be done for this scenario.
     */
    if (pPgSrvLstPtr == pPgInfo->unServiceList.pWorkingServiceListPointer)
    {
        pPgInfo->unServiceList.pWorkingServiceListPointer =
            ElpsSrvListPtrGetNextNodeForPg (pContextInfo, pPgInfo,
                                            pPgSrvLstPtr);
        /* If no more service list  node is present for this group NULL
         * will be returned, and stored in the 
         pPgInfo->unServiceList.pWorkingServiceList */
    }

    if (RBTreeRemove
        (pContextInfo->PgServiceListPointerTbl,
         (tRBElem *) pPgSrvLstPtr) != RB_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPtrDelNode
 *
 * DESCRIPTION      : This function delete a service list node from the
 *                    service list table and release the memory used by that
 *                    node to the memory pool. It also takes care of 
 *                    updating all the associated datastructure links before
 *                    deleting the node.
 *
 * INPUT            : pContextInfo - Context information pointer
 *                    pPgInfo - pointer to protection group information node
 *                    pPgSrvLstPtr - pointer to the service list node that 
 *                                needs to be deleted from the service
 *                                list table
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSrvListPtrDelNode (tElpsContextInfo * pContextInfo,
                       tElpsPgInfo * pPgInfo,
                       tElpsServiceListPointerInfo * pPgSrvLstPtr)
{
    /* Updation of first node pointer in PG info node */
    /* Following 2 situations are possible while deleting a service list node
     * for a PG.
     * 1. Deleting the first service list node for this PG:
     *    If the node to be deleted is equal to 
     *    pPgInfo->unServiceList.pWorkingServiceListPointer
     *    that means we are deleting the first node. Get the next service list
     *    node for this PG and update the link in the PG info node, before 
     *    deleting the service list node.
     * 2. Deleting intermediate service list node for this PG:
     *    Nothing to be done for this scenario.
     */
    if (pPgSrvLstPtr == pPgInfo->unServiceList.pWorkingServiceListPointer)
    {
        pPgInfo->unServiceList.pWorkingServiceListPointer =
            ElpsSrvListPtrGetNextNodeForPg (pContextInfo, pPgInfo,
                                            pPgSrvLstPtr);
        /* If no more service list  node is present for this group NULL
         * will be returned, and stored in the 
         * pPgInfo->unServiceList.pWorkingServiceListPointer */
    }

    RBTreeRemove (pContextInfo->PgServiceListPointerTbl,
                  (tRBElem *) pPgSrvLstPtr);

    ElpsSrvListPointerRBFree ((tRBElem *) pPgSrvLstPtr, 0);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPtrDelAllNodesForPg
 *
 * DESCRIPTION      : This function deletes all the service list nodes 
 *                    associated with a protection group, and release the 
 *                    memory used by them. It also takes care of updating 
 *                    all the associated datastructure links.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgInfo      - pointer to the protection group info
 *                                   structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSrvListPtrDelAllNodesForPg (tElpsContextInfo * pContextInfo,
                                tElpsPgInfo * pPgInfo)
{
    tElpsServiceListPointerInfo *pFirstPgSrvLstPtr = NULL;
    tElpsServiceListPointerInfo *pNextPgSrvLstPtr = NULL;

    /* Get the first PG linfo */
    pFirstPgSrvLstPtr = pPgInfo->unServiceList.pWorkingServiceListPointer;

    if (pFirstPgSrvLstPtr == NULL)
    {
        /* no service list entry is present for this PG */
        return;
    }

    /* pFirstPgSrvLst is used to get the next node, so first node is 
     * deleted at the end */
    pNextPgSrvLstPtr = ElpsSrvListPtrGetNextNodeForPg (pContextInfo, pPgInfo,
                                                       pFirstPgSrvLstPtr);

    while (pNextPgSrvLstPtr != NULL)
    {
        ElpsSrvListPtrDelNode (pContextInfo, pPgInfo, pNextPgSrvLstPtr);
        pNextPgSrvLstPtr =
            ElpsSrvListPtrGetNextNodeForPg (pContextInfo, pPgInfo,
                                            pFirstPgSrvLstPtr);
    }
    /* Delete the first node now */
    ElpsSrvListPtrDelNode (pContextInfo, pPgInfo, pFirstPgSrvLstPtr);

    /* Update the service list link in PG info node */
    pPgInfo->unServiceList.pWorkingServiceListPointer = NULL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPtrGetFirstNode
 *
 * DESCRIPTION      : This function returns the first service list node
 *                    from the PG service list table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListPointerInfo * - pointer to the first service
 *                           list info. NULL will be returned if there is
 *                           no node present in this table.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListPointerInfo *
ElpsSrvListPtrGetFirstNode (tElpsContextInfo * pContextInfo)
{
    tElpsServiceListPointerInfo *pPgSrvLstPtr = NULL;

    pPgSrvLstPtr = (tElpsServiceListPointerInfo *)
        RBTreeGetFirst (pContextInfo->PgServiceListPointerTbl);

    return pPgSrvLstPtr;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPtrGetNextNode
 *
 * DESCRIPTION      : This function returns the next service list info
 *                    from the PG service list table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pCurrentPgSrvLstPtr - pointer to the current PG service
 *                                       list info.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListPointerInfo * - Pointer to the next PG 
 *                           service list info. NULL will be returned if
 *                           no next node is found in this table.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListPointerInfo *
ElpsSrvListPtrGetNextNode (tElpsContextInfo * pContextInfo,
                           tElpsServiceListPointerInfo * pCurrentPgSrvLstPtr)
{
    tElpsServiceListPointerInfo *pNextPgSrvLstPtr = NULL;

    pNextPgSrvLstPtr = (tElpsServiceListPointerInfo *)
        RBTreeGetNext (pContextInfo->PgServiceListPointerTbl,
                       (tRBElem *) pCurrentPgSrvLstPtr, NULL);
    /* NULL will be returned if no next entry is present. */
    return pNextPgSrvLstPtr;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPtrGetNextNodeForPg
 *
 * DESCRIPTION      : This function returns the next PG service list node
 *                    that is associated with a particular Protection Group.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgInfo - pointer to the protection group info 
 *                              structure
 *                    pCurrentPgSrvLstPtr - pointer to the current PG service
 *                                       list info structure.
 *                    
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListPointerInfo * - pointer to the next PG 
 *                            service list info node that is associated 
 *                            with the current protection group info.
 *                            NULL will be returned if there is no next
 *                            PG service list info node found for this
 *                            Protection group.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListPointerInfo *
ElpsSrvListPtrGetNextNodeForPg (tElpsContextInfo * pContextInfo,
                                tElpsPgInfo * pPgInfo,
                                tElpsServiceListPointerInfo *
                                pCurrentPgSrvLstPtr)
{
    tElpsServiceListPointerInfo *pNextPgSrvLstPtr = NULL;

    pNextPgSrvLstPtr = (tElpsServiceListPointerInfo *)
        RBTreeGetNext (pContextInfo->PgServiceListPointerTbl,
                       (tRBElem *) pCurrentPgSrvLstPtr, NULL);

    if ((pNextPgSrvLstPtr != NULL)
        && (pNextPgSrvLstPtr->u4PgId == pPgInfo->u4PgId))
    {
        return pNextPgSrvLstPtr;
    }

    return NULL;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSrvListPtrGetNode
 *
 * DESCRIPTION      : This function helps to find a service list node from
 *                    the service list table.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    u4PgId  - Protection group Id
 *                    u4ServiceId - Service Id
 *
 * OUTPUT           : None
 * 
 * RETURNS          : tElpsServiceListPointerInfo * - Pointer to the service list
 *                           node that is found during search. NULL will be
 *                           returned if required node is not found in the
 *                           service list table.
 * 
 **************************************************************************/
PUBLIC tElpsServiceListPointerInfo *
ElpsSrvListPtrGetNode (tElpsContextInfo * pContextInfo, UINT4 u4PgId,
                       UINT4 u4PgServiceListId)
{
    tElpsServiceListPointerInfo *pPgSrvLstPtr = NULL;
    tElpsServiceListPointerInfo PgSrvLstPtr;

    PgSrvLstPtr.u4PgId = u4PgId;
    PgSrvLstPtr.u4PgServiceListId = u4PgServiceListId;

    pPgSrvLstPtr = (tElpsServiceListPointerInfo *)
        RBTreeGet (pContextInfo->PgServiceListPointerTbl,
                   (tRBElem *) & PgSrvLstPtr);

    return pPgSrvLstPtr;
}

#endif /* _ELPSSRVL_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpssrvl.c                     */
/*-----------------------------------------------------------------------*/
