/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpssem.c,v 1.28 2016/01/04 10:48:36 siva Exp $
 *
 * Description: This file contains the ELPS modules state event
 *              machine implimentation routines.
 *****************************************************************************/
#ifndef _ELPSSEM_C_
#define _ELPSSEM_C_

#include "elpsinc.h"
#include "elpssem.h"

PRIVATE VOID ElpsSemChangeState PROTO ((tElpsPgInfo *, UINT4));
PRIVATE UINT1 ElpsSemTakeAction PROTO ((tElpsPgInfo *, UINT4, UINT1, UINT1));
PRIVATE UINT4 ElpsSemManipulateAction PROTO ((tElpsPgInfo *, UINT1, UINT1 *));
PRIVATE UINT4 ElpsSemManipulateActionOnClear PROTO ((tElpsPgInfo *, UINT1 *));
PRIVATE UINT4 ElpsSemManipulateActionOnNRW PROTO ((tElpsPgInfo *, UINT1 *));
PRIVATE UINT4 ElpsSemManipulateActionOnSFRecP PROTO ((tElpsPgInfo *, UINT1 *));
PRIVATE UINT4 ElpsSemManipulateActionOnNRP PROTO ((tElpsPgInfo * pPgInfo,
                                                   UINT1 *));
PRIVATE UINT4 ElpsSemManipulateActionOnWTRP PROTO ((tElpsPgInfo * pPgInfo,
                                                    UINT1 *));
PRIVATE UINT4 ElpsSemManipulateActionOnPSCVersion PROTO
    ((tElpsPgInfo * pPgInfo, UINT1, UINT1));

/***************************************************************************
 * FUNCTION NAME    : ElpsSemApplyEvent
 *
 * DESCRIPTION      : This function apply the given event to the APS state
 *                       event machine and return the request status -
 *                       Accepted, overruled, or not applicable.
 *
 * INPUT            : pPgInfo - pointer to the protection group information.
 *                    u1Event - requested event that needs to be applied 
 *                                 on the state event machine. u1Event Id 
 *                                 should be fetched from 
 *                                 ElpsSemGetEventIdForReq() call before giving
 *                                 it to this function.
 *                    u1Action - ELPS_ONEPASS/ ELPS_TWOPASS
 *
 * OUTPUT           : None
 * 
 * RETURNS          : Request Status - ELPS_CMD_ACCEPTED,
 *                                     ELPS_CMD_OVERRULED or
 *                                     ELPS_CMD_NA
 **************************************************************************/
PUBLIC UINT1
ElpsSemApplyEvent (tElpsPgInfo * pPgInfo, UINT1 u1Event, UINT1 u1Action)
{
    UINT4               u4CurrentState = pPgInfo->ApsInfo.u1ApsSemStateId;
    UINT4               u4ActionId = ELPS_INVALID_ACTION;
    UINT4               u4ActionInfo = 0;
    UINT1               u1CmdRetStatus = ELPS_CMD_NA;
    UINT1               u1PrevEvent = u1Event;

    pPgInfo->u1InterMediateStateId = pPgInfo->ApsInfo.u1ApsSemStateId;

    if ((u1Event >= ELPS_SEM_MAX_EVENTS)
        || (u4CurrentState >= ELPS_SEM_MAX_STATES))
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                   "ElpsSemApplyEvent: Invalid state: %d or Event: %d for "
                   "PG - %d\r\n", u4CurrentState, u1Event, pPgInfo->u4PgId));
        return ELPS_CMD_NA;
    }

    ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
               "ElpsSemApplyEvent: [PG %d] State: %s, "
               "Event: %s \r\n", pPgInfo->u4PgId,
               gaau1StateString[u4CurrentState], gaau1EventString[u1Event]));

    if (pPgInfo->u1FreezeFlag == OSIX_TRUE)
    {
        /* Freeze is set */
        ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                   "ElpsSemApplyEvent: Event is rejected/ignored as freeze "
                   "command is applied to Protection Group %d\r\n",
                   pPgInfo->u4PgId));
        return ELPS_CMD_OVERRULED;
    }

    /* A state transition by one of three local requests, CLEAR command, 
     * clearance of SF(-P) and expiration of WTR timer are considered as 
     * TWO_PASS and other requests as ONE_PASS.
     * Far end request is also considered as ONE_PASS.
     *
     * TWO_PASS state shall be calculated based on the top priority local 
     * request and state transition table for local request to obtain the 
     * intermediate state.On this intermediate state, the last received far 
     * end request and state transition table for far end request are used 
     * to obtain the final state. 
     * For the other commands, the top priority local request is higher or 
     * equal priority compare to last received far end request then state 
     * transition for local request is used to determine the final state.
     * Otherwise last received far end request (Request/State information of 
     * the APS PDU) is used with the state transition table for far end 
     * request to determine the final state.*/

    if (pPgInfo->ApsInfo.u1ProtectionType & ELPS_R_BIT_VALUE)
    {
        /* revertive mode */
        if (pPgInfo->u1PscVersion == ELPS_ONE)
        {
            u4ActionId =
                ElpsSemManipulateActionOnPSCVersion (pPgInfo, u1Event,
                                                     u1Action);
        }
        else
        {
            u4ActionId = gaau4Elps1T1BRSem[u1Event][u4CurrentState];
        }

        if (u1Action == ELPS_TWOPASS)
        {
            u4ActionInfo = gau4ActionInfo[u4ActionId];
            switch (u4ActionInfo)
            {
                case ELPS_MULTIPLE_ACTION:
                    /* In case of multiple action, find the intermediate state
                     * by calling the function ElpsSemManipulateAction */
                    u4ActionInfo = ElpsSemManipulateAction (pPgInfo, u1Event,
                                                            &u1CmdRetStatus);
                    break;
                case ELPS_COMMAND_OVERRULED:
                case ELPS_COMMAND_NA:
                case ELPS_INVALID_ACTION:
                    /* In case of O,NA and Invalid action, intermediate state 
                     * will be current state*/
                    u4ActionInfo = u4CurrentState;
                    break;
                default:
                    break;
            }

            /* 'u1InterMediateStateId' will be used inside the funtions   
             * ElpsSemManipulateActionOnNRP(), 
             * ElpsSemManipulateActionOnSFRecP(),
             * ElpsSemManipulateActionOnNRW(), 
             * ElpsSemManipulateActionOnClear() to
             * determine the next state in case of 2 pass logic*/
            pPgInfo->u1InterMediateStateId = (UINT1) u4ActionInfo;

            /* Find the final state using the last received far end request 
             * and the intermediate state*/
            u1Event =
                gau1ElpsFarEndReqToEvMap[pPgInfo->LastFarEndReq.u1Request];
            /* revertive mode */
            if (pPgInfo->u1PscVersion == ELPS_ONE)
            {
                u4ActionId = ElpsSemManipulateActionOnPSCVersion (pPgInfo,
                                                                  u1Event,
                                                                  u1Action);
            }
            else
            {
                u4ActionId = gaau4Elps1T1BRSem[u1Event][u4ActionInfo];
            }
        }
    }
    else
    {
        /* non-revertive mode */
        if (pPgInfo->u1PscVersion == ELPS_ONE)
        {
            u4ActionId =
                ElpsSemManipulateActionOnPSCVersion (pPgInfo, u1Event,
                                                     u1Action);
        }
        else
        {
            u4ActionId = gaau4Elps1T1BNRSem[u1Event][u4CurrentState];
        }

        if (u1Action == ELPS_TWOPASS)
        {
            u4ActionInfo = gau4ActionInfo[u4ActionId];
            switch (u4ActionInfo)
            {
                case ELPS_MULTIPLE_ACTION:
                    /* In case of multiple action, find the intermediate 
                     * state by calling the function ElpsSemManipulateAction*/
                    u4ActionInfo = ElpsSemManipulateAction (pPgInfo, u1Event,
                                                            &u1CmdRetStatus);
                    break;
                case ELPS_COMMAND_OVERRULED:
                case ELPS_COMMAND_NA:
                case ELPS_INVALID_ACTION:
                    /* In case of O,NA and Invalid action, intermediate state 
                     * will be current state*/
                    u4ActionInfo = u4CurrentState;
                    break;
                default:
                    break;
            }
            /* 'u1InterMediateStateId' will be used inside the funtions
             * ElpsSemManipulateActionOnNRP(), 
             * ElpsSemManipulateActionOnSFRecP(),
             * ElpsSemManipulateActionOnNRW(), 
             * ElpsSemManipulateActionOnClear() to
             * determine the next state in case of 2 pass logic*/
            pPgInfo->u1InterMediateStateId = (UINT1) u4ActionInfo;

            /* Find the final state using the last received far end request 
             * and the intermediate state*/
            u1Event =
                gau1ElpsFarEndReqToEvMap[pPgInfo->LastFarEndReq.u1Request];
            /* Non-revertive mode */
            if (pPgInfo->u1PscVersion == ELPS_ONE)
            {
                u4ActionId = ElpsSemManipulateActionOnPSCVersion (pPgInfo,
                                                                  u1Event,
                                                                  u1Action);
            }
            else
            {
                u4ActionId = gaau4Elps1T1BNRSem[u1Event][u4ActionInfo];
            }
        }
    }

    return (ElpsSemTakeAction (pPgInfo, u4ActionId, u1PrevEvent, u1Event));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemTakeAction
 *
 * DESCRIPTION      : This function takes the required SEM action.
 *
 * INPUT            : pPgInfo - pointer to the protection group information.
 *                    u4ActionId - Id for the Action to be taken on SEM.
 *                    u1Event    - Requested Event.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : Request Status - ELPS_CMD_ACCEPTED,
 *                       ELPS_CMD_OVERRULED or
 *                       ELPS_CMD_NA
 **************************************************************************/
PRIVATE UINT1
ElpsSemTakeAction (tElpsPgInfo * pPgInfo, UINT4 u4ActionId,
                   UINT1 u1PrevEvent, UINT1 u1Event)
{
    UINT1               u1CmdRetStatus = ELPS_CMD_NA;
    UINT4               u4ActionInfo = ELPS_INVALID_ACTION;

    ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
               "ElpsSemTakeAction: [PG %d] ActionId: %d, "
               "Event: %s \r\n", pPgInfo->u4PgId, u4ActionId,
               gaau1EventString[u1Event]));

    if (u4ActionId < ELPS_SEM_MAX_ACTIONS)
    {
        u4ActionInfo = gau4ActionInfo[u4ActionId];
    }

    switch (u4ActionInfo)
    {
        case ELPS_COMMAND_OVERRULED:
            ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                       "ElpsSemTakeAction: [PG %d] "
                       "Request Over-ruled\r\n", pPgInfo->u4PgId));
            u1CmdRetStatus = ELPS_CMD_OVERRULED;
            break;

        case ELPS_COMMAND_NA:
            ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                       "ElpsSemTakeAction: [PG %d] "
                       "Request Not Applicable\r\n", pPgInfo->u4PgId));
            u1CmdRetStatus = ELPS_CMD_NA;
            break;

        case ELPS_MULTIPLE_ACTION:
            /* Manipulate the ActionInfo based on the current condition */
            ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                       "ElpsSemTakeAction: [PG %d] "
                       "Multiple possible actions available\r\n",
                       pPgInfo->u4PgId));
            u4ActionInfo =
                ElpsSemManipulateAction (pPgInfo, u1Event, &u1CmdRetStatus);
            break;

        case ELPS_INVALID_ACTION:
            ELPS_TRC ((pPgInfo->pContextInfo, (ALL_FAILURE_TRC |
                                               CONTROL_PLANE_TRC),
                       "ElpsSemTakeAction: [PG %d] "
                       "Invalid action info: %d for ",
                       pPgInfo->u4PgId, u4ActionInfo));
            u1CmdRetStatus = ELPS_CMD_NA;
            break;

        default:
            /* State change indication received */
            u1CmdRetStatus = ELPS_CMD_ACCEPTED;
            break;
    }
    /* In case of 2 pass logic if the far end request has less priority 
     * than local priority request and the final state is over ruled or N/A,
     * the new action should be based upon the intermediate state.
     */
    if ((u1CmdRetStatus != ELPS_CMD_ACCEPTED) &&
        (pPgInfo->ApsInfo.u1ApsSemStateId != pPgInfo->u1InterMediateStateId))
    {
        /* Reset current event to the last received event */
        u1Event = u1PrevEvent;
        u4ActionInfo = pPgInfo->u1InterMediateStateId;
        u1CmdRetStatus = ELPS_CMD_ACCEPTED;
    }

    if (u1CmdRetStatus == ELPS_CMD_ACCEPTED)
    {
        /* Update the LastActiveReq variable here instead of doing 
         * at multiple places */
        pPgInfo->u1LastActiveReq = u1Event;
        if (pPgInfo->ApsInfo.u1ApsSemStateId != u4ActionInfo)
        {
            /* There is a state change. handle it */
            ElpsSemChangeState (pPgInfo, u4ActionInfo);
        }
        else
        {
            /* Set the PG Status */
            if (u4ActionInfo < ELPS_SEM_MAX_STATES)
            {
                ElpsSemChangePgStatus (u4ActionInfo,
                                       gaElpsSemStateInfo[u4ActionInfo].
                                       u4WorkingStatus, pPgInfo);
            }
            else
            {
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemTakeAction: [PG %d] " "No State Change\r\n",
                           pPgInfo->u4PgId));

                u1CmdRetStatus = ELPS_CMD_REJECTED;
            }
        }
    }

    return u1CmdRetStatus;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemChangeState
 *
 * DESCRIPTION      : This function handles the APS state change, and takes
 *                       all the necessary actions.
 *
 * INPUT            : pPgInfo - pointer to the protection group information
 *                    u4NewStateId - New State Id.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PRIVATE VOID
ElpsSemChangeState (tElpsPgInfo * pPgInfo, UINT4 u4NewStateId)
{
    tElpsNotifyInfo     NotifyInfo;
    UINT1               u1TrapType = ELPS_INVALID_TRAP;

    UINT4               u4OldStateId = pPgInfo->ApsInfo.u1ApsSemStateId;
    UINT4               u4NewWrkStatus = 0;
    UINT4               u4OldWrkStatus = 0;
    UINT4               u4PgStatus = ELPS_PG_STATUS_INVALID;

    if (u4NewStateId < ELPS_SEM_MAX_STATES)
    {
        /* Get the new working entity status from state info database */
        u4NewWrkStatus = gaElpsSemStateInfo[u4NewStateId].u4WorkingStatus;
    }
    else
    {
        /* invalid New state.
         * So stay in the current state */
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsSemChangeState: [PG:%d] Invalid state: %d, "
                   "State change is not possible\r\n", pPgInfo->u4PgId,
                   u4NewStateId));
        return;
    }

    if (u4OldStateId < ELPS_SEM_MAX_STATES)
    {
        u4OldWrkStatus = gaElpsSemStateInfo[u4OldStateId].u4WorkingStatus;
    }

    /* Store the previous state here. This will be useful in case of 
     * mulple action to find out the final state. 
     * For Example :- 
     To activate WTR timer appropriately even when both ends 
     concurrently detect clearance of SF, when the local state 
     transits from SF to NR with the requested signal number 1, 
     the previous local state, SF, should be memorized */
    pPgInfo->u1PreviousStateId = pPgInfo->ApsInfo.u1ApsSemStateId;

    /* Update the state variable in PgInfo */
    pPgInfo->ApsInfo.u1ApsSemStateId = (UINT1) u4NewStateId;
    ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
               "ElpsSemChangeState: [PG:%d] ::SEM State Changed:: "
               "Old State: %s, New State: %s\r\n", pPgInfo->u4PgId,
               gaau1StateString[u4OldStateId], gaau1StateString[u4NewStateId]));

    if (u4NewStateId == ELPS_STATE_WTR_P)
    {
        /* start the WTR timer and return */
        ElpsTmrStartTimer (pPgInfo, pPgInfo->u2WtrTime, ELPS_WTR_TMR);
        /* Set the PG status */
        pPgInfo->u1PgStatus = ELPS_PG_STATUS_WTR_STATE;
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                   "ElpsSemChangeState: [PG:%d] Entered to "
                   "WTR State.\r\n", pPgInfo->u4PgId));
    }
    else if (u4OldStateId == ELPS_STATE_WTR_P)
    {
        /* Coming out from WTR state, before WTR timer expiry. So stop
           WTR timer here */
        ElpsTmrStopTimer (pPgInfo, ELPS_WTR_TMR);
    }

    /* Invoke APS pdu transmission on state change */
    if (ELPS_GET_PG_A_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
    {
        ElpsTxHandleSemStateChange (pPgInfo);
    }

    /* As per RFC-6378, In 1+1 Unidirectional LPS received request will be applied
     * on the state maching but continue to receive the traffic based on previous local
     * request only.*/
    if ((pPgInfo->u1PscVersion == ELPS_ONE) &&
        (!ELPS_GET_PG_D_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType)) &&
        (pPgInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_NR_P))
    {
        /* Swithing is not required */
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                   "ElpsSemChangeState: [PG:%d] "
                   "Data Path Switching not needed\r\n", pPgInfo->u4PgId));
        return;
    }

    /* Set the PG Status */
    ElpsSemChangePgStatus (u4NewStateId, u4NewWrkStatus, pPgInfo);

    /* Identify the bridge/switching request */
    if (u4OldWrkStatus == u4NewWrkStatus)
    {
        /* validating Old and New protection status for a PG is not required
         * to identify the switching request.
         * if working entity status changes that itself is sufficient to 
         * determine this
         */
        /*Since Hardware has been programmed, there is no need to program the
         *hardware */
        if (pPgInfo->u1PgHwStatus == TRUE)
        {
            /* Swithing is not required */
            ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                       "ElpsSemChangeState: [PG:%d] "
                       "Data Path Switching not needed\r\n", pPgInfo->u4PgId));
            return;
        }
    }

    /*--------------------------------------------------------------------
     *  Switching is decided by the SEM. take the necessary actions here
     *--------------------------------------------------------------------*/

    /* Switch the traffic to working/protection entity */
    if (u4NewWrkStatus == ELPS_ACTIVE)
    {
        u4PgStatus = ELPS_PG_SWITCH_TO_WORKING;
        /* If switching failes need to send the trap, so set the trap type
         * here */
        u1TrapType = ELPS_TRAP_HW_SWITCH_TO_WORK_FAIL;
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                   "ElpsSemChangeState: [PG:%d] "
                   "Switching the traffic to working path\r\n",
                   pPgInfo->u4PgId));

    }
    else
    {
        u4PgStatus = ELPS_PG_SWITCH_TO_PROTECTION;
        /* If switching failes need to send the trap, so set the trap type
         * here */
        u1TrapType = ELPS_TRAP_HW_SWITCH_TO_PROT_FAIL;
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                   "ElpsSemChangeState: [PG:%d] "
                   "Switching the traffic to protection path\r\n",
                   pPgInfo->u4PgId));

    }

    /* performance measurement */
    /* The status of local node is changed to working to protection
     * after processing the peer sent APS-SF packet.
     * This time is noted as "T3".
     */
    if ((u4PgStatus == ELPS_PG_SWITCH_TO_PROTECTION) ||
        (u4PgStatus == ELPS_PG_SWITCH_TO_WORKING))
    {
        ElpsUtilMeasureTime (pPgInfo, ELPS_PERF_STATE_CHG);
    }

    /*If the varaible u1PgHwStatus is FALSE,
     *ElpsPgSwitchDataPath must be called with ELPS_PG_CREATE*/
    if (pPgInfo->u1PgHwStatus == FALSE)
    {
        if (ElpsPgSwitchDataPath
            (pPgInfo->pContextInfo, pPgInfo, ELPS_PG_CREATE) != OSIX_SUCCESS)
        {
            pPgInfo->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
            NotifyInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
            NotifyInfo.u4PgId = pPgInfo->u4PgId;
            NotifyInfo.u1PgStatus = pPgInfo->u1PgStatus;
            ElpsTrapSendTrapNotifications (&NotifyInfo,
                                           ELPS_TRAP_HW_PG_CRT_FAIL);
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsPgActivate: "
                       "Failed to program the hardware for PG(id-%d) with status "
                       "%d\r\n", pPgInfo->u4PgId, ELPS_PG_CREATE));
            return;
        }
        else
        {
            pPgInfo->u1PgHwStatus = TRUE;
        }
    }

    /*Again the function ElpsPgSwitchDataPath is called with u4PgStatus */
    if (ElpsPgSwitchDataPath (pPgInfo->pContextInfo, pPgInfo,
                              u4PgStatus) != OSIX_SUCCESS)
    {
        pPgInfo->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
        NotifyInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
        NotifyInfo.u4PgId = pPgInfo->u4PgId;
        NotifyInfo.u1PgStatus = pPgInfo->u1PgStatus;

        ElpsTrapSendTrapNotifications (&NotifyInfo, u1TrapType);
        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                   "ElpsSemChangeState: Failed to program the hardware for "
                   "PG(id-%d) with status %d\r\n", pPgInfo->u4PgId,
                   u4PgStatus));
        return;
    }

    /* Start Lor timer If protection type is 1+1/1:1 Bi-directional 
       protection type. */
    if (ELPS_GET_PG_A_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType) &&
        ELPS_GET_PG_D_BIT_VALUE (pPgInfo->ApsInfo.u1ProtectionType))
    {
        ElpsTmrStartTimer (pPgInfo, ELPS_LOR_INTERVAL, ELPS_LOR_TMR);
    }

    /* Form the Notification Trap */
    NotifyInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
    NotifyInfo.u4PgId = pPgInfo->u4PgId;
    NotifyInfo.u1PgStatus = pPgInfo->u1PgStatus;

    /* determind the trap type and increment the statistics counter */
    if (u4NewStateId == ELPS_STATE_FS_P)
    {
        u1TrapType = ELPS_TRAP_FORCED_SWITCH;
        /* increment the force switch statistics */
        pPgInfo->Stats.u4ForceSwitchCount++;
    }
    else if (u4NewStateId == ELPS_STATE_MS_P)
    {
        u1TrapType = ELPS_TRAP_MANUAL_SWITCH;
        /* increment the manual switch statistics */
        pPgInfo->Stats.u4ManualSwitchCount++;
    }
    else if (u4NewStateId == ELPS_STATE_MS_W)
    {
        u1TrapType = ELPS_TRAP_MANUAL_SWITCH_W;
        /* increment the manual switch statistics */
        pPgInfo->Stats.u4ManualSwitchCount++;
    }

    else
    {
        u1TrapType = ELPS_TRAP_AUTO_PROT_SWITCH;
        /* increment the auto switch statistics */
        pPgInfo->Stats.u4AutoSwitchCount++;
    }

    /* Send the trap notification */
    ElpsTrapSendTrapNotifications (&NotifyInfo, u1TrapType);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemFreeze
 *
 * DESCRIPTION      : This function handles the freeze command. 
 *                     This freezes the APS state of the protection group.
 *                     until the freeze is cleared {by ElpsSemClearFreeze()
 *                     call}, all additional near end commands are rejected.
 *                     Condition changes and received APS information are 
 *                     ignored.
 *
 * INPUT            : pPgInfo - pointer to the protection group information
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSemFreeze (tElpsPgInfo * pPgInfo)
{
    pPgInfo->u1FreezeFlag = OSIX_TRUE;

    if (((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
         (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        &&
        ((pPgInfo->u1PgStatus != ELPS_PG_STATUS_PROT_PATH_ACTIVE) &&
         (pPgInfo->u1PgStatus != ELPS_PG_STATUS_WTR_STATE) &&
         (pPgInfo->u1PgStatus != ELPS_PG_STATUS_DO_NOT_REVERT) &&
         (pPgInfo->u1PgStatus != ELPS_PG_STATUS_UNAVAILABLE)))
    {
        /* Update MPLS DB Module */
        if (ElpsUtilUpdateMplsDb (pPgInfo, MPLS_LPS_PROT_NOT_AVAILABLE) ==
            OSIX_FAILURE)
        {
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsSemFreeze: "
                       "MPLS Database Update Failed for PG(id-%d)"
                       "%d\r\n", pPgInfo->u4PgId));
        }
    }
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemClearFreeze
 *
 * DESCRIPTION      : This function clear the freeze state command.
 *                       APS State of the protection group is recomputed based
 *                       on the latest condition and last APS information 
 *                       received (refering the last request received).
 *
 * INPUT            : pPgInfo - pointer to the protection group information
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSemClearFreeze (tElpsPgInfo * pPgInfo)
{
    tElpsRequest       *pLastReq = NULL;
    UINT1               u1SemEvent = ELPS_SEM_MAX_EVENTS;
    UINT1               u1CmdStatus = ELPS_CMD_NA;
    UINT1               u1Action = ELPS_ONEPASS;

    /* Reset the freeze flag */
    pPgInfo->u1FreezeFlag = OSIX_FALSE;

    /* If during freeze, status was updated as NOT AVAILABLE from AVAILABLE
       and no local condition has occured, reset it here.
       Other cases will be taken care inside SEM */
    if (((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
         (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        &&
        ((pPgInfo->u1PgStatus != ELPS_PG_STATUS_PROT_PATH_ACTIVE) &&
         (pPgInfo->u1PgStatus != ELPS_PG_STATUS_WTR_STATE) &&
         (pPgInfo->u1PgStatus != ELPS_PG_STATUS_DO_NOT_REVERT) &&
         (pPgInfo->u1PgStatus != ELPS_PG_STATUS_UNAVAILABLE)))
    {
        if ((pPgInfo->u1WorkingCfmStatus == ELPS_CFM_SIGNAL_OK) &&
            (pPgInfo->u1ProtectionCfmStatus == ELPS_CFM_SIGNAL_OK))
        {
            /* Update MPLS DB Module */
            if (ElpsUtilUpdateMplsDb (pPgInfo,
                                      MPLS_LPS_PROT_AVAILABLE) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsPortEcfmRegisterPerMep: "
                           "MPLS Database Update Failed for PG(id-%d)"
                           "%d\r\n", pPgInfo->u4PgId));
                return;
            }
        }
    }

    /* recompute the state based on the last received local 
     * condition and last received far end request
     * if last received command is local command ignore it */
    if (pPgInfo->u1LastRecvdReqType == ELPS_LOCAL_CONDITION)
    {
        /* get the last received condition information */
        pLastReq = &(pPgInfo->LastLocalCond);
    }
    else if (pPgInfo->u1LastRecvdReqType == ELPS_FAREND_REQUEST)
    {
        pLastReq = &(pPgInfo->LastFarEndReq);
    }
    else
    {
        /* local commands are not used to recompute the protection group 
         * */
        return;
    }

    /* Get the event Id for SEM */
    u1SemEvent = ElpsSemGetEventIdForReq (pPgInfo->u1LastRecvdReqType,
                                          pLastReq->u1Request);
    if (pPgInfo->u1LastRecvdReqType == ELPS_LOCAL_CONDITION)
    {
        if (ElpsSemValidateLocalRequest (pPgInfo, u1SemEvent) == OSIX_FAILURE)
        {
            pPgInfo->LastLocalCmd.u1RequestStatus = ELPS_CMD_REJECTED;
            return;
        }

        if (u1SemEvent >= ELPS_SEM_MAX_EVENTS)
        {
            return;
        }
        /* Global priority check */
        u1Action = ElpsSemApplyGlobalPriorityLogic (pPgInfo, &u1SemEvent);
    }

    /* Apply to SEM */
    u1CmdStatus = ElpsSemApplyEvent (pPgInfo, u1SemEvent, u1Action);

    /* Update the command status */
    pLastReq->u1RequestStatus = u1CmdStatus;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemGetEventIdForReq
 *
 * DESCRIPTION      : This function helps to get the Event Id, that is 
 *                    understandable by the APS state event machine.
 *
 * INPUT            : u1ReqType  - Type of the request.
 *                    u1Event    - event id as per the MIB object.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : u1SemEventId - Event Id according to the APS SEM
 * 
 **************************************************************************/
PUBLIC UINT1
ElpsSemGetEventIdForReq (UINT1 u1ReqType, UINT1 u1Event)
{
    UINT1               u1SemEventId = ELPS_SEM_MAX_EVENTS;
    /* Consider this as invalid Sem Event */
    switch (u1ReqType)
    {
        case ELPS_LOCAL_COMMAND:
            if (u1Event < ELPS_MAX_LOCAL_COMMAND)
            {
                u1SemEventId = gau1ElpsLocalCmdToEvMap[u1Event];
            }
            break;
        case ELPS_LOCAL_CONDITION:
            if (u1Event < ELPS_MAX_LOCAL_CONDITION)
            {
                u1SemEventId = gau1ElpsLocalCondToEvMap[u1Event];
            }
            break;
        case ELPS_FAREND_REQUEST:
            if (u1Event < ELPS_MAX_FAR_REQUEST)
            {
                u1SemEventId = gau1ElpsFarEndReqToEvMap[u1Event];
            }
            break;
        default:
            ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsSemGetEventIdForReq: "
                       "Invalid request type %d\r\n", u1ReqType));
            break;
    }
    return u1SemEventId;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemGetStateInfo 
 *
 * DESCRIPTION      : This function returns the APS SEM state information
 *                    (maintained by the APS SEM) for a particular state id.
 *                     
 * INPUT            : u1SemStateId - Id of the Sem State, for which the
 *                                   state information is needed.
 *
 *                    
 * OUTPUT           : pSemStateInfo - fetched SEM state information are 
 *                                    copied to this structure
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSemGetStateInfo (UINT1 u1SemStateId, tElpsSemState * pSemStateInfo)
{
    if (u1SemStateId >= ELPS_SEM_MAX_STATES)
    {
        /* Invalid state id */
        return OSIX_FAILURE;
    }

    MEMCPY (pSemStateInfo, &gaElpsSemStateInfo[u1SemStateId],
            sizeof (tElpsSemState));

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemPscGetStateInfo 
 *
 * DESCRIPTION      : This function returns the APS SEM state information
 *                    (maintained by the APS SEM) for a particular state id.
 *                     
 * INPUT            : u1SemStateId - Id of the Sem State, for which the
 *                                   state information is needed.
 *
 *                    
 * OUTPUT           : pSemStateInfo - fetched SEM state information are 
 *                                    copied to this structure
 *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSemPscGetStateInfo (UINT1 u1SemStateId, tElpsPscSemState * pSemStateInfo)
{
    if (u1SemStateId >= ELPS_SEM_MAX_STATES)
    {
        /* Invalid state id */
        return OSIX_FAILURE;
    }

    MEMCPY (pSemStateInfo, &gaElpsPscSemStateInfo[u1SemStateId],
            sizeof (tElpsPscSemState));

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemManipulateAction
 *
 * DESCRIPTION      : In some cases one single event for a particular state
 *                    can results in different state transition, instead of
 *                    one state transition, based on the current protection
 *                    group entity conditions. In such cases this function
 *                    helps to determind the currect state transition, based
 *                    on the condition.
 *
 * INPUT            : pPgInfo - pointer to the protection group information.
 *                    u1Event - Requested Event.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : u4ActionInfo - Currect Action to be taken
 * 
 **************************************************************************/
PRIVATE UINT4
ElpsSemManipulateAction (tElpsPgInfo * pPgInfo, UINT1 u1Event,
                         UINT1 *pu1CmdStatus)
{
    UINT4               u4NewActionInfo = pPgInfo->u1InterMediateStateId;
    ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
               "ElpsSemManipulateAction: [PG %d] \r\n", pPgInfo->u4PgId));

    switch ((UINT4) u1Event)
    {
        case ELPS_EV_LR_CLEAR:
            u4NewActionInfo = ElpsSemManipulateActionOnClear (pPgInfo,
                                                              pu1CmdStatus);
            break;

        case ELPS_EV_FR_NR_P:
            u4NewActionInfo = ElpsSemManipulateActionOnNRP (pPgInfo,
                                                            pu1CmdStatus);
            break;

        case ELPS_EV_FR_NR_W:
            u4NewActionInfo = ElpsSemManipulateActionOnNRW (pPgInfo,
                                                            pu1CmdStatus);
            break;

        case ELPS_EV_LR_PROT_RECOV_SF:
            u4NewActionInfo = ElpsSemManipulateActionOnSFRecP (pPgInfo,
                                                               pu1CmdStatus);
            break;

        case ELPS_EV_FR_WTR:
            u4NewActionInfo = ElpsSemManipulateActionOnWTRP (pPgInfo,
                                                             pu1CmdStatus);
            break;

        default:
            /* This is an invalid case. If this occures
             * it will return the current state (i.e. no state change
             * will happen)
             */
            break;
    }
    ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
               "ElpsSemManipulateAction: [PG %d] , returning the new "
               "action info id- %d\r\n", pPgInfo->u4PgId, u4NewActionInfo));
    return u4NewActionInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemManipulateActionOnClear
 *
 * DESCRIPTION      : If Clear event is received, 
 *                    based on the current protection group entity 
 *                    conditions, it can results in transition to different
 *                    states.
 *                    In such cases this function
 *                    helps to determind the currect state transition, based
 *                    on the condition.
 *
 * INPUT            : pPgInfo - pointer to the protection group information.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : u4ActionInfo - Currect Action to be taken
 * 
 **************************************************************************/
PRIVATE UINT4
ElpsSemManipulateActionOnClear (tElpsPgInfo * pPgInfo, UINT1 *pu1CmdStatus)
{
    UINT4               u4NewActionInfo = pPgInfo->u1InterMediateStateId;
    /* Initialize with current state id */
    *pu1CmdStatus = ELPS_CMD_ACCEPTED;
    switch (pPgInfo->u1InterMediateStateId)    /* current state */
    {
        case ELPS_STATE_LO_W:
            if (ELPS_IS_SF_P_REASSERTED (pPgInfo) == OSIX_TRUE)
            {
                /* if SF-P is reasserted */
                u4NewActionInfo = ELPS_STATE_SF_P;
                pPgInfo->LastLocalCond.u1Request = ELPS_LOCAL_COND_SF_P;
                pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_ACCEPTED;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnClear: [PG %d] "
                           "SF-P is reasserted. New State- SF_P\r\n",
                           pPgInfo->u4PgId));
            }
            else if (ELPS_IS_SF_REASSERTED (pPgInfo) == OSIX_TRUE)
            {
                /* if SF is reasserted */
                u4NewActionInfo = ELPS_STATE_SF_W;
                pPgInfo->LastLocalCond.u1Request = ELPS_LOCAL_COND_SF_W;
                pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_ACCEPTED;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnClear: [PG %d] "
                           "SF is reasserted. New State- SF_W\r\n",
                           pPgInfo->u4PgId));
            }
            else
            {
                /* Clear */
                u4NewActionInfo = ELPS_STATE_NR_W;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnClear: [PG %d] "
                           "SF/SF-P is not reasserted. "
                           "New State- NR_W\r\n", pPgInfo->u4PgId));
            }
            break;

        case ELPS_STATE_FS_P:
            if (ELPS_IS_SF_REASSERTED (pPgInfo) == OSIX_TRUE)
            {
                /* if SF is reasserted */
                u4NewActionInfo = ELPS_STATE_SF_W;
                pPgInfo->LastLocalCond.u1Request = ELPS_LOCAL_COND_SF_W;
                pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_ACCEPTED;

                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnClear: [PG %d] "
                           "SF is reasserted. New State- SF_W\r\n",
                           pPgInfo->u4PgId));
            }
            else
            {
                /* Revertive Mode */
                if (pPgInfo->ApsInfo.u1ProtectionType & ELPS_R_BIT_VALUE)
                {
                    /* No Request - Working */
                    u4NewActionInfo = ELPS_STATE_NR_W;
                    ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                               "ElpsSemManipulateActionOnClear: [PG %d] "
                               "SF is not reasserted. New State- NR_W\r\n",
                               pPgInfo->u4PgId));
                }
                else            /* Non-revertive mode */
                {
                    if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                        (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
                    {
                        u4NewActionInfo = ELPS_STATE_NR_W;
                        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                                   "ElpsSemManipulateActionOnClear: [PG %d] "
                                   "SF is not reasserted. New State- NR_W\r\n",
                                   pPgInfo->u4PgId));
                    }
                    else
                    {
                        /* Do not revert */
                        u4NewActionInfo = ELPS_STATE_DNR_P;
                        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                                   "ElpsSemManipulateActionOnClear: [PG %d] "
                                   "SF is not reasserted. New State- DNR_P\r\n",
                                   pPgInfo->u4PgId));
                    }
                }
            }
            break;

        case ELPS_STATE_MS_P:
            /* This block will be hit only for Non-revertive mode */
            if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
            {
                /* No Request - Working */
                u4NewActionInfo = ELPS_STATE_NR_W;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnClear: [PG %d] "
                           "New State- NR_W\r\n", pPgInfo->u4PgId));
            }
            else
            {
                /* Do not revert */
                u4NewActionInfo = ELPS_STATE_DNR_P;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnClear: [PG %d] "
                           "New State- DNR_P\r\n", pPgInfo->u4PgId));
            }
            break;
        case ELPS_STATE_WTR_P:
        {
            if (pPgInfo->u1PscVersion == ELPS_ONE)
            {
                /* Clear */
                u4NewActionInfo = ELPS_STATE_NR_W;
            }
        }
            break;
        default:
            /* This is an invalid case. If this occures
             * it will return the current state (i.e. no state change
             * will happen) */
            *pu1CmdStatus = ELPS_CMD_NA;
            break;
    }
    return u4NewActionInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemManipulateActionNRW
 *
 * DESCRIPTION      : If NR [r/b=null] event is received through APS PDU, 
 *                    based on the current protection group entity 
 *                    conditions, it can results in transition to different
 *                    states.
 *                    In such cases this function
 *                    helps to determind the currect state transition, based
 *                    on the condition.
 *
 * INPUT            : pPgInfo - pointer to the protection group information.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : u4ActionInfo - Currect Action to be taken
 * 
 **************************************************************************/
PRIVATE UINT4
ElpsSemManipulateActionOnNRW (tElpsPgInfo * pPgInfo, UINT1 *pu1CmdStatus)
{
    UINT4               u4RemainingTime = 0;
    UINT4               u4NewActionInfo = pPgInfo->u1InterMediateStateId;

    *pu1CmdStatus = ELPS_CMD_ACCEPTED;
    /* Initialize with the current state */
    switch (pPgInfo->u1InterMediateStateId)
    {
        case ELPS_STATE_NR_W:
            if (ELPS_IS_SF_P_REASSERTED (pPgInfo) == OSIX_TRUE)
            {
                /* if SF-P is reasserted */
                u4NewActionInfo = ELPS_STATE_SF_P;
                pPgInfo->LastLocalCond.u1Request = ELPS_LOCAL_COND_SF_P;
                pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_ACCEPTED;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnNRW: [PG %d] "
                           "SF-P is re-asserted. New state- SF_P\r\n",
                           pPgInfo->u4PgId));
            }
            else if (ELPS_IS_SF_REASSERTED (pPgInfo) == OSIX_TRUE)
            {
                /* if SF is reasserted */
                u4NewActionInfo = ELPS_STATE_SF_W;
                pPgInfo->LastLocalCond.u1Request = ELPS_LOCAL_COND_SF_W;
                pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_ACCEPTED;

                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnNRW: [PG %d] "
                           "SF is re-asserted. New State- SF_W\r\n",
                           pPgInfo->u4PgId));
            }
            else
            {
                u4NewActionInfo = ELPS_STATE_NR_W;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnNRW: [PG %d] "
                           "SF/SF-P is not re-asserted. New State- "
                           "NR_W\r\n", pPgInfo->u4PgId));
            }
            break;

        case ELPS_STATE_NR_P:
            if (ELPS_IS_SF_REASSERTED (pPgInfo) == OSIX_TRUE)
            {
                /* if SF is reasserted */
                u4NewActionInfo = ELPS_STATE_SF_W;
                pPgInfo->LastLocalCond.u1Request = ELPS_LOCAL_COND_SF_W;
                pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_ACCEPTED;

                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnNRW: [PG %d] "
                           "SF is re-asserted. New State- SF_W\r\n",
                           pPgInfo->u4PgId));
            }
            else
            {
                u4NewActionInfo = ELPS_STATE_NR_W;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnNRW: [PG %d] "
                           "SF is not re-asserted. New State- NR_W\r\n",
                           pPgInfo->u4PgId));
            }
            break;

        case ELPS_STATE_WTR_P:
            if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
            {
                TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                                     &(pPgInfo->WTRTimer.
                                       TimerNode), &u4RemainingTime);

                if (u4RemainingTime == 0)
                {
                    u4NewActionInfo = ELPS_STATE_NR_W;
                    break;
                }
            }
            *pu1CmdStatus = ELPS_CMD_NA;
            break;

        default:
            /* This is an invalid case. If this occures
             * it will return the current state (i.e. no state change
             * will happen) */
            break;
    }
    return u4NewActionInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemManipulateActionWTRP
 *
 * DESCRIPTION      : If NR [r/b=normal] event is received through APS/PSC PDU, 
 *                    based on the current protection group entity 
 *                    conditions, it can results in transition to different
 *                    states.
 *                    In such cases this function
 *                    helps to determine the currect state transition, based
 *                    on the condition.
 *
 * INPUT            : pPgInfo - pointer to the protection group information.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : u4ActionInfo - Currect Action to be taken
 * 
 **************************************************************************/
PRIVATE UINT4
ElpsSemManipulateActionOnWTRP (tElpsPgInfo * pPgInfo, UINT1 *pu1CmdStatus)
{
    UINT4               u4NewActionInfo = pPgInfo->u1InterMediateStateId;
    /* Initialize with the current state */

    *pu1CmdStatus = ELPS_CMD_ACCEPTED;
    /* Revertive Mode */
    if (pPgInfo->ApsInfo.u1ProtectionType & ELPS_R_BIT_VALUE)
    {
        switch (pPgInfo->u1InterMediateStateId)
        {
            case ELPS_STATE_NR_P:

                if (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
                {
                    u4NewActionInfo = ELPS_STATE_NR_P;
                }
                else if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                         (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
                {
                    if (pPgInfo->u1LastActiveReq == ELPS_EV_FR_SF)
                    {
                        u4NewActionInfo = ELPS_STATE_WTR_P;
                    }
                }

                break;

            default:
                /* This is an invalid case. If this occures
                 * it will return the current state (i.e. no state change
                 * will happen) */
                *pu1CmdStatus = ELPS_CMD_NA;
                break;
        }
    }
    return u4NewActionInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemManipulateActionNRP
 *
 * DESCRIPTION      : If NR [r/b=null] event is received through APS PDU, 
 *                    based on the current protection group entity 
 *                    conditions, it can results in transition to different
 *                    states.
 *                    In such cases this function
 *                    helps to determind the currect state transition, based
 *                    on the condition.
 *
 * INPUT            : pPgInfo - pointer to the protection group information.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : u4ActionInfo - Currect Action to be taken
 * 
 **************************************************************************/
PRIVATE UINT4
ElpsSemManipulateActionOnNRP (tElpsPgInfo * pPgInfo, UINT1 *pu1CmdStatus)
{
    UINT4               u4NewActionInfo = pPgInfo->u1InterMediateStateId;
    UINT4               u4RemainingTime = 0;
    /* Initialize with the current state */

    *pu1CmdStatus = ELPS_CMD_ACCEPTED;
    /* Revertive Mode */
    if (pPgInfo->ApsInfo.u1ProtectionType & ELPS_R_BIT_VALUE)
    {
        switch (pPgInfo->u1InterMediateStateId)
        {
            case ELPS_STATE_NR_P:
                if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW) ||
                    (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP))
                {

                    if (pPgInfo->u1PreviousStateId == ELPS_STATE_WTR_P)
                    {
                        TmrGetRemainingTime (gElpsGlobalInfo.TmrListId,
                                             &(pPgInfo->WTRTimer.
                                               TimerNode), &u4RemainingTime);

                        if (u4RemainingTime == 0)
                        {
                            u4NewActionInfo = ELPS_STATE_NR_W;
                        }
                        else
                        {
                            u4NewActionInfo = ELPS_STATE_NR_P;
                        }
                        break;
                    }

                    /* When FS is given during SF-W and then the FS is
                     * cleared the state should move to SF-W, provided the
                     * SF still remains. Based on response from the Draft
                     * authors, reassertion of SF is based on implementation.
                     * For our implementation of ELPS where the SF indication
                     * will not come more than once from OAM, the following 3 events
                     * have to be processed differently from draft specification.
                     * Hence the check is removed for the protocol to work.

                     else if ((pPgInfo->u1LastActiveReq == ELPS_EV_FR_SF) ||
                     (pPgInfo->u1LastActiveReq == ELPS_EV_FR_FS) ||
                     (pPgInfo->u1LastActiveReq == ELPS_EV_FR_MS))
                     {
                     *pu1CmdStatus = ELPS_CMD_OVERRULED;
                     break;
                     }
                     */

                }

                if (pPgInfo->u1PreviousStateId == ELPS_STATE_SF_W)
                {
                    /* If the previous local state, SF , move to WTR state */
                    u4NewActionInfo = ELPS_STATE_WTR_P;
                    ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                               "ElpsSemManipulateActionOnNRW: [PG %d] "
                               "SF is re-asserted. New State- SF_W\r\n",
                               pPgInfo->u4PgId));
                }
                else
                {
                    u4NewActionInfo = ELPS_STATE_NR_W;
                    ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                               "ElpsSemManipulateActionOnNRW: [PG %d] "
                               "SF is not re-asserted. New State- NR_W\r\n",
                               pPgInfo->u4PgId));
                }
                break;

            default:
                /* This is an invalid case. If this occures
                 * it will return the current state (i.e. no state change
                 * will happen) */
                *pu1CmdStatus = ELPS_CMD_NA;
                break;
        }
    }
    return u4NewActionInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemManipulateActionOnSFRecP
 *
 * DESCRIPTION      : If SF-P recovery event is received, 
 *                    based on the current protection group entity 
 *                    conditions, it can results in transition to different
 *                    states.
 *                    In such cases this function
 *                    helps to determind the currect state transition, based
 *                    on the condition.
 *
 * INPUT            : pPgInfo - pointer to the protection group information.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : u4ActionInfo - Currect Action to be taken
 * 
 **************************************************************************/
PRIVATE UINT4
ElpsSemManipulateActionOnSFRecP (tElpsPgInfo * pPgInfo, UINT1 *pu1CmdStatus)
{
    UINT4               u4NewActionInfo = pPgInfo->u1InterMediateStateId;
    /* Initialize with current state id */

    *pu1CmdStatus = ELPS_CMD_ACCEPTED;
    switch (pPgInfo->u1InterMediateStateId)    /* current state */
    {
        case ELPS_STATE_SF_P:

            if (ELPS_IS_SF_REASSERTED (pPgInfo) == OSIX_TRUE)
            {
                /* if SF is reasserted */
                u4NewActionInfo = ELPS_STATE_SF_W;

                pPgInfo->LastLocalCond.u1Request = ELPS_LOCAL_COND_SF_W;
                pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_ACCEPTED;

                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnClear: [PG %d] "
                           "SF is reasserted. New State- SF_W\r\n",
                           pPgInfo->u4PgId));
            }
            else
            {
                /* No Request - Working */
                u4NewActionInfo = ELPS_STATE_NR_W;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemManipulateActionOnClear: [PG %d] "
                           "SF is not reasserted. "
                           "New State- NR_W\r\n", pPgInfo->u4PgId));
            }
            break;

        default:
            /* This is an invalid case. If this occures
             * it will return the current state (i.e. no state change
             * will happen) */
            *pu1CmdStatus = ELPS_CMD_NA;
            break;
    }
    return u4NewActionInfo;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemCheckForActiveRequest
 *
 * DESCRIPTION      : This function check the active request with the 
 *                    given local command.
 *
 * INPUT            : u4ContextId - Context identifier
 *                    u4PgId      - Protection Group identifier
 *                    i4LocalCmd  - Local Command
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsSemCheckForActiveRequest (UINT4 u4ContextId, UINT4 u4PgId, INT4 i4LocalCmd)
{
    tElpsPgInfo        *pPgInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgInfo = ElpsUtilValidateCxtAndPgInfo (u4ContextId, u4PgId, &u4Dummy);

    if (pPgInfo == NULL)
    {
        return OSIX_FAILURE;
    }

    switch (i4LocalCmd)
    {
        case ELPS_EXT_CMD_FS:
            if (pPgInfo->u1LastActiveReq != ELPS_EV_LR_FS)
            {
                return OSIX_FAILURE;
            }
            break;

        case ELPS_EXT_CMD_MS:
            if (pPgInfo->u1LastActiveReq != ELPS_EV_LR_MS)
            {
                return OSIX_FAILURE;
            }
            break;

        case ELPS_EXT_CMD_MS_W:
            if (pPgInfo->u1LastActiveReq != ELPS_EV_LR_MS_W)
            {
                return OSIX_FAILURE;
            }
            break;

        case ELPS_EXT_CMD_LOP:
            if (pPgInfo->u1LastActiveReq != ELPS_EV_LR_LO)
            {
                return OSIX_FAILURE;
            }
            break;

        case ELPS_EXT_CMD_EXER:
            break;

        default:
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemResetSemVariables
 *
 * DESCRIPTION      : This function reset the variables those are use by 
 *                    the state event machine.
 *
 * INPUT            : pPgInfo   - pointer to the protection group information.
 *                                All the state event machines related 
 *                                variables are associated by this pointer as
 *                                the state event machine is running per 
 *                                protection group basis.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSemResetSemVariables (tElpsPgInfo * pPgInfo)
{
    pPgInfo->ApsInfo.u1ApsSemStateId = 0;    /* ELPS_STATE_NR_W */

    pPgInfo->u1LastActiveReq =
        ElpsSemGetEventIdForReq (ELPS_LOCAL_COMMAND, ELPS_EXT_CMD_CLR);
    pPgInfo->LastLocalCmd.u1Request = ELPS_EXT_CMD_CLR;
    pPgInfo->LastLocalCmd.u1RequestStatus = ELPS_CMD_ACCEPTED;
    pPgInfo->LastLocalCond.u1Request = ELPS_LOCAL_COND_WRK_REC_SF;
    pPgInfo->LastLocalCond.u1RequestStatus = ELPS_CMD_NA;
    pPgInfo->LastFarEndReq.u1Request = ELPS_FAR_REQ_NR_W;
    pPgInfo->LastFarEndReq.u1RequestStatus = ELPS_CMD_NA;
    pPgInfo->u1FreezeFlag = OSIX_FALSE;
    pPgInfo->u1WorkingCfmStatus = ELPS_CFM_SIGNAL_OK;
    pPgInfo->u1ProtectionCfmStatus = ELPS_CFM_SIGNAL_OK;

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemApplyGlobalPriorityLogic
 *
 * DESCRIPTION      : This function implements the global priority logic  
 *                    for the state event machine.
 *
 * INPUT            : pPgInfo   - pointer to the protection group information.
 *                                All the state event machines related 
 *                                variables are associated by this pointer as
 *                                the state event machine is running per 
 *                                protection group basis.
 *
 * OUTPUT           : pu1Event  - Indicates the event to be applied on the 
 *                                State Machine
 * 
 * RETURNS          : ELPS_TWOPASS/ELPS_ONEPASS
 * 
 **************************************************************************/
PUBLIC UINT1
ElpsSemApplyGlobalPriorityLogic (tElpsPgInfo * pPgInfo, UINT1 *pu1Event)
{
    /* If the local request is one of the three local requests, CLEAR command,
     * clearance of SF(-P) and expiration of WTR timer which doesn't have the 
     * priority is considered as a 2-pass request. 
     * And rest of the commands are considered as 1-pass.
     * 
     * 2-pass request shall be calculated based on the top priority local 
     * request and state transition table for local request to obtain the 
     * intermediate state.On this intermediate state, the last received 
     * far end request and state transition table for far end request are 
     * used to obtain the final state.
     * 
     * For the other commands, the top priority local request is the 
     * higher or equal priority compare to last received far end request 
     * then local request is used to determine the final state.
     * Otherwise last received far end request is used to determine 
     * the final state.*/

    if ((gai1ReqMapPriority[*pu1Event]) == ELPS_NA)
    {
        /* Indicates the local request either CLEAR command, 
         * Clearance of SF(-P) or expiration of WTR timer*/
        return ELPS_TWOPASS;
    }
    else if (pPgInfo->u1PscVersion == ELPS_ONE
             && gai1ReqMapPriorityPSCVersion[*pu1Event] >=
             (gai1ReqMapPriorityPSCVersion
              [gau1ElpsFarEndReqToEvMap[pPgInfo->LastFarEndReq.u1Request]]))
    {
        /* Event id will be the recevived local reuest */
        return ELPS_ONEPASS;
    }
    else if ((gai1ReqMapPriority[*pu1Event]) >=
             (gai1ReqMapPriority
              [gau1ElpsFarEndReqToEvMap[pPgInfo->LastFarEndReq.u1Request]]))
    {
        /* Event id will be the recevived local reuest */
        return ELPS_ONEPASS;
    }

    /* Event Id is modified to last received far end request */
    *pu1Event = gau1ElpsFarEndReqToEvMap[pPgInfo->LastFarEndReq.u1Request];
    return ELPS_ONEPASS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemValidateLocalRequest 
 *
 * DESCRIPTION      : This routine will decide to accept or reject the local 
 *                    request.
 *
 * INPUT            : pPgInfo  - Protection Group Information
 *                    u1Event - Local Request
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC UINT1
ElpsSemValidateLocalRequest (tElpsPgInfo * pPgInfo, UINT1 u1Event)
{
    UINT4               u4StateId = pPgInfo->ApsInfo.u1ApsSemStateId;

    switch (u1Event)
    {
            /* The CLEAR command is accepted if the state of the
             * protection group is LO, FS, MS, EXER or WTR and
             * rejected otherwise */
        case ELPS_EV_LR_CLEAR:
        {
            if ((u4StateId == ELPS_STATE_LO_W) ||
                (u4StateId == ELPS_STATE_FS_P) ||
                (u4StateId == ELPS_STATE_MS_P) ||
                (u4StateId == ELPS_STATE_MS_W) ||
                (u4StateId == ELPS_STATE_WTR_P) ||
                (u4StateId == ELPS_STATE_EXER_W) ||
                (u4StateId == ELPS_STATE_EXER_P))
            {
                return OSIX_SUCCESS;
            }
            break;

        }
            /* Clearence of SF on working is accepted only if the last active 
             * request is SF on working */
        case ELPS_EV_LR_WRK_RECOV_SF:
        {
            return OSIX_SUCCESS;
        }
            /* Clearence of SF on protection is accepted only if the last active 
             * request is SF on protection */
        case ELPS_EV_LR_PROT_RECOV_SF:
        {
            return OSIX_SUCCESS;
        }
            /* Expiry of WTR timer is accepted only if the state is WTR */
        case ELPS_EV_LR_WTR_EXP:
        {
            return OSIX_SUCCESS;
        }
            /* In case of SF on Working and SF on Protection, should be 
             * accepted always */
        case ELPS_EV_LR_WRK_SF:
        case ELPS_EV_LR_PROT_SF:
        {
            if (pPgInfo->u1PscVersion == ELPS_ZERO)
            {
                if ((gai1ReqMapPriority[u1Event]) >=
                    (gai1ReqMapPriority[pPgInfo->u1LastActiveReq]))
                {
                    return OSIX_SUCCESS;
                }
            }
            else
            {
                if ((gai1ReqMapPriorityPSCVersion[u1Event]) >=
                    (gai1ReqMapPriorityPSCVersion[pPgInfo->u1LastActiveReq]))
                {
                    return OSIX_SUCCESS;
                }
            }
        }
            /* Other local requests should be compared against the last 
             * active request.
             * The received local command is accepted if it has higher or equal 
             * priority compared to the last active request.Rejected otherwise*/
        default:
        {
            if (u1Event >= ELPS_SEM_MAX_EVENTS)
            {
                ELPS_TRC ((pPgInfo->pContextInfo,
                           ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                           "ElpsSemValidateLocalRequest : Invalid Event: %d "
                           "for PG - %d\r\n", u1Event, pPgInfo->u4PgId));
                return OSIX_FAILURE;
            }
            if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
            {
                if ((u4StateId == ELPS_STATE_NR_W) &&
                    (u1Event == ELPS_EV_LR_MS_W))
                {
                    return OSIX_FAILURE;
                }
            }
            if (pPgInfo->u1PscVersion == ELPS_ONE)
            {
                if (gai1ReqMapPriorityPSCVersion[pPgInfo->u1LastActiveReq] ==
                    ELPS_NA)
                {
                    if ((gai1ReqMapPriorityPSCVersion[u1Event]) >
                        (gai1StateMapPriorityPSCVersion
                         [pPgInfo->ApsInfo.u1ApsSemStateId]))
                    {
                        return OSIX_SUCCESS;
                    }
                }
                else
                {
                    if ((gai1ReqMapPriorityPSCVersion[u1Event]) >
                        (gai1ReqMapPriorityPSCVersion
                         [pPgInfo->u1LastActiveReq]))
                    {
                        return OSIX_SUCCESS;
                    }
                }
            }
            else
            {
                if (gai1ReqMapPriority[pPgInfo->u1LastActiveReq] == ELPS_NA)
                {
                    if ((gai1ReqMapPriority[u1Event]) >
                        (gai1StateMapPriority
                         [pPgInfo->ApsInfo.u1ApsSemStateId]))
                    {
                        return OSIX_SUCCESS;
                    }
                }
                else
                {
                    if ((gai1ReqMapPriority[u1Event]) >=
                        (gai1ReqMapPriority[pPgInfo->u1LastActiveReq]))
                    {
                        return OSIX_SUCCESS;
                    }
                }
            }
            break;
        }
    }
    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemModifyLocalCommandStatus 
 *
 * DESCRIPTION      : This routine will modify the local command status to 
 *                    depends upon priority of the far end request/local 
 *                    condition
 *
 * INPUT            : pPgInfo  - Protection Group Information
 *                    u1Event - Local Request
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSemModifyLocalCommandStatus (tElpsPgInfo * pPgInfo, UINT1 u1Event)
{
    if (u1Event >= ELPS_SEM_MAX_EVENTS)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, ALL_FAILURE_TRC | CONTROL_PLANE_TRC,
                   "ElpsSemModifyLocalCommandStatus : Invalid Event: %d for "
                   "PG - %d\r\n", u1Event, pPgInfo->u4PgId));
        return;
    }

    /* If the last local command is CLEAR then do not update the status as
       overruled upon reception of high-priority far end requests */
    if (pPgInfo->LastLocalCmd.u1Request == ELPS_EXT_CMD_CLR)
    {
        ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                   "ElpsSemModifyLocalCommandStatus : Event: %d Ignored for "
                   "PG - %d\r\n", u1Event, pPgInfo->u4PgId));
        return;
    }

    /* If a command is overridden by a condition or APS request, 
       that command is forgotten. */
    if (pPgInfo->u1PscVersion == ELPS_ZERO)
    {
        if ((pPgInfo->LastLocalCmd.u1RequestStatus == ELPS_CMD_ACCEPTED) &&
            (gai1ReqMapPriority[u1Event]) >
            (gai1ReqMapPriority
             [gau1ElpsLocalCmdToEvMap[pPgInfo->LastLocalCmd.u1Request]]))
        {
            pPgInfo->LastLocalCmd.u1RequestStatus = ELPS_CMD_OVERRULED;
        }
    }
    else
    {
        if ((pPgInfo->LastLocalCmd.u1RequestStatus == ELPS_CMD_ACCEPTED) &&
            (gai1ReqMapPriorityPSCVersion[u1Event]) >
            (gai1ReqMapPriorityPSCVersion
             [gau1ElpsLocalCmdToEvMap[pPgInfo->LastLocalCmd.u1Request]]))
        {
            pPgInfo->LastLocalCmd.u1RequestStatus = ELPS_CMD_OVERRULED;
        }
    }
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemChangePgStatus 
 *
 * DESCRIPTION      : This routine will modify the PgStatus of the protection
 *                    group based on the state, working status and the last 
 *                    active request. 
 *
 * INPUT            : u4StateId  - State of the Protection Group
 *                    u4Status - Working Status of the protection group 
 *                    pPgInfo  - Protection Group Information
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : NONE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsSemChangePgStatus (UINT4 u4StateId, UINT4 u4Status, tElpsPgInfo * pPgInfo)
{
    UINT4               u4PgStatus;
    UINT4               u4OldPgStatus;

    u4OldPgStatus = pPgInfo->u1PgStatus;

    if (u4StateId != ELPS_STATE_WTR_P)
    {
        if (u4Status == ELPS_ACTIVE)
        {
            if ((u4StateId == ELPS_STATE_NR_W) &&
                ((pPgInfo->u1LastActiveReq == ELPS_EV_FR_LO) ||
                 (pPgInfo->u1LastActiveReq == ELPS_EV_FR_SF_P)))
            {
                pPgInfo->u1PgStatus = ELPS_PG_STATUS_UNAVAILABLE;
                u4PgStatus = MPLS_LPS_PROT_NOT_AVAILABLE;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemChangePgStatus: [PG:%d] Entered to "
                           "Unavailable State.\r\n", pPgInfo->u4PgId));
            }
            else if ((u4StateId == ELPS_STATE_LO_W) ||
                     (u4StateId == ELPS_STATE_SF_P) ||
                     (u4StateId == ELPS_STATE_NR_P))
            {
                /* Set the PG status */
                pPgInfo->u1PgStatus = ELPS_PG_STATUS_UNAVAILABLE;
                u4PgStatus = MPLS_LPS_PROT_NOT_AVAILABLE;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemChangePgStatus: [PG:%d] Entered to "
                           "Unavailable State.\r\n", pPgInfo->u4PgId));

            }
            else
            {
                pPgInfo->u1PgStatus = ELPS_PG_STATUS_WORK_PATH_ACTIVE;
                u4PgStatus = MPLS_LPS_PROT_AVAILABLE;
            }
        }
        else
        {
            if (u4StateId == ELPS_STATE_DNR_P)
            {
                /* Set the PG status */
                pPgInfo->u1PgStatus = ELPS_PG_STATUS_DO_NOT_REVERT;
                u4PgStatus = MPLS_LPS_PROT_IN_USE;
                ELPS_TRC ((pPgInfo->pContextInfo, CONTROL_PLANE_TRC,
                           "ElpsSemChangePgStatus: [PG:%d] Entered to "
                           "DNR State.\r\n", pPgInfo->u4PgId));
            }
            else
            {
                /* If the previous status updated to MPLS DB is NOT AVAILABLE,
                 * then MPLS DB has to be updated with status as AVAILABLE before
                 * updating the status as PROTECTION IN USE */
                if ((pPgInfo->u1PgStatus == ELPS_PG_STATUS_UNAVAILABLE) &&
                    ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                     (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)))
                {
                    /* Update MPLS DB Module */
                    if (ElpsUtilUpdateMplsDb (pPgInfo, MPLS_LPS_PROT_AVAILABLE)
                        == OSIX_FAILURE)
                    {
                        ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                                   "ElpsSemChangePgStatus: "
                                   "MPLS Database Update Failed for PG(id-%d)"
                                   "%d\r\n", pPgInfo->u4PgId));
                        return;
                    }
                }
                pPgInfo->u1PgStatus = ELPS_PG_STATUS_PROT_PATH_ACTIVE;
                u4PgStatus = MPLS_LPS_PROT_IN_USE;
            }
        }

        /* Prevent Unnecessary MPLS DB Update */
        if (u4OldPgStatus == pPgInfo->u1PgStatus)
        {
            return;
        }

        if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
            (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            /* Update MPLS DB Module */
            if (ElpsUtilUpdateMplsDb (pPgInfo, u4PgStatus) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsSemChangePgStatus: "
                           "MPLS Database Update Failed for PG(id-%d)"
                           "%d\r\n", pPgInfo->u4PgId));
                return;
            }
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsSemManipulateActionOnPSCVersion
 *
 * DESCRIPTION      : This function apply the given event to the APS state m/c
 *                    if the PSC version is 1 and corresponding action will be 
 *                    returned.
 *       
 * INPUT            : pPgInfo - pointer to the protection group information.
 *                    u1Event - Event.
 *               
 * OUTPUT           : None
 *
 * RETURNS          : u4ActionInfo - Currect Action to be taken
 ***************************************************************************/
UINT4
ElpsSemManipulateActionOnPSCVersion (tElpsPgInfo * pPgInfo,
                                     UINT1 u1Event, UINT1 u1Action)
{
    UINT4               u4CurrentState = pPgInfo->ApsInfo.u1ApsSemStateId;
    UINT4               u4ActionId = ELPS_INVALID_ACTION;
    if (u1Action == ELPS_TWOPASS)
    {
        if (pPgInfo->u1PscVersion == ELPS_ONE)
        {
            u4CurrentState = pPgInfo->u1InterMediateStateId;
        }
    }
    if (u1Event == ELPS_EV_LR_PROT_SF && u4CurrentState == ELPS_SEM_STATE_FS)
    {
        u4ActionId = A14;
    }
    else if (u1Event == ELPS_EV_LR_FS && u4CurrentState == ELPS_SEM_STATE_SF_P)
    {
        u4ActionId = A3;
    }
    else if (u1Event == ELPS_EV_FR_FS && u4CurrentState == ELPS_SEM_STATE_SF_P)
    {
        u4ActionId = A1;
    }
    else if (u1Event == ELPS_EV_LR_FS &&
             pPgInfo->u1LastActiveReq == ELPS_EV_FR_SF_P)
    {
        u4ActionId = A3;
    }
    else if (pPgInfo->ApsInfo.u1ProtectionType & ELPS_R_BIT_VALUE)
    {
        u4ActionId = gaau4Elps1T1BRSem[u1Event][u4CurrentState];
    }
    else if (!(pPgInfo->ApsInfo.u1ProtectionType & ELPS_R_BIT_VALUE))
    {
        u4ActionId = gaau4Elps1T1BNRSem[u1Event][u4CurrentState];
    }

    return u4ActionId;
}

#endif /* _ELPSSEM_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpssem.c                      */
/*-----------------------------------------------------------------------*/
