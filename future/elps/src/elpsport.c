/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsport.c,v 1.22 2015/08/26 10:58:12 siva Exp $
 *
 * Description: This file contains the portable routines which calls APIs 
 *              given by other modules.
 *****************************************************************************/
#ifndef _ELPSPORT_C_
#define _ELPSPORT_C_

#include "elpsinc.h"
#include "mplsapi.h"
#include "snmputil.h"

extern UINT4        fselps[ELPS_NINE];
extern UINT4        FsElpsContextSystemControl[ELPS_THIRTEEN];
/***************************************************************************
 * FUNCTION NAME    : ElpsPortEcfmRegisterPerMep
 *
 * DESCRIPTION      : This function is used by ELPS to register with 
 *                    ECFM/MPLS-OAM.
 *                    This function call API provided by ECFM/MPLS-OAM 
 *                    module to register for the indication from the working 
 *                    and  protection MEPs of a protection group. 
 *                    This registration is done for the following 
 *                    indication from ECFM module -
 *                    o ECFM_APS_FRAME_RECEIVED - Receive the APS PDU
 *                    o ECFM_DEFECT_CONDITION_ENCOUNTERED - get Signal 
 *                                                          fault
 *                    o ECFM_DEFECT_CONDITION_CLEARED - get Signal OK
 *                    
 *                    After registering for the working and Protection MEP,
 *                    this function also gets the current signal status of
 *                    working and protection MEP and apply the current 
 *                    status on ELPS state event machine.
 *
 * INPUT            : pPgInfo    - Pointer to the protection group 
 *                                 information structure
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS - If registration is successful
 *                    OSIX_FAILURE - If registration fails for working or
 *                                   protection mep.
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortEcfmRegisterPerMep (tElpsPgInfo * pPgInfo)
{
    tElpsCfmConfigInfo *pWorkingCfmInfo = NULL;
    tElpsCfmConfigInfo *pProtectionCfmInfo = NULL;
    tEcfmRegParams      EcfmReg;
    tEcfmEventNotification MepInfo;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;

    if (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        MEMSET (&EcfmReg, 0, sizeof (tEcfmRegParams));
        EcfmReg.u4ModId = ECFM_ELPS_APP_ID;
        EcfmReg.u4EventsId = (ECFM_APS_FRAME_RECEIVED |
                              ECFM_DEFECT_CONDITION_CLEARED |
                              ECFM_DEFECT_CONDITION_ENCOUNTERED);
        EcfmReg.pFnRcvPkt = ElpsApiEcfmIndCallBack;

        /* Register for Working MEP */
        MEMSET (&MepInfo, 0, sizeof (tEcfmEventNotification));
        pWorkingCfmInfo = pPgInfo->WorkingEntity.pCfmConfig;

        MepInfo.u4MdIndex = pWorkingCfmInfo->MonitorInfo.u4MegId;
        MepInfo.u4MaIndex = pWorkingCfmInfo->MonitorInfo.u4MeId;
        MepInfo.u2MepId = (UINT2) pWorkingCfmInfo->MonitorInfo.u4MepId;
        MepInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
        MepInfo.u4Event = ECFM_DEFECT_CONDITION_CLEARED;    /* Using it as 
                                                               a default 
                                                               value */

        if (EcfmMepRegisterAndGetFltStatus (&EcfmReg, &MepInfo) != ECFM_SUCCESS)
        {
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsPortEcfmRegisterPerMep: [PG: %d] "
                       "Registration failed for working MEP, "
                       "meg=%d, me=%d, mep=%d\r\n",
                       pPgInfo->u4PgId, MepInfo.u4MdIndex, MepInfo.u4MaIndex,
                       MepInfo.u2MepId));
            return OSIX_FAILURE;
        }

        /* Apply current status of working MEP on PG */
        if (MepInfo.u4Event == ECFM_DEFECT_CONDITION_ENCOUNTERED)
        {
            ElpsCfmProcessReceivedSignal (pWorkingCfmInfo,
                                          ELPS_CFM_SIGNAL_FAIL);
            /* If there is signal failure either on working or protection path,
             * then the function "ElpsCfmProcessReceivedSignal" will create PG and
             * make the port status either blockd or forwarding depending on the 
             * failure condition; but the function "ElpsPgActivate" which calls this
             * function, will also create PG. Once, PG is created via the function
             * ElpsCfmProcessReceivedSignal,then we should not create it again in
             * "ElpsPgActivate"; So update the flag pPgInfo->u1PgHwStatus to TRUE 
             * and use it to skip the PG creation  in "ElpsPgActivate".*/
            pPgInfo->u1PgHwStatus = TRUE;
        }

        /* Register for Protection MEP */
        MEMSET (&MepInfo, 0, sizeof (tEcfmEventNotification));
        pProtectionCfmInfo = pPgInfo->ProtectionEntity.pCfmConfig;

        MepInfo.u4MdIndex = pProtectionCfmInfo->MonitorInfo.u4MegId;
        MepInfo.u4MaIndex = pProtectionCfmInfo->MonitorInfo.u4MeId;
        MepInfo.u2MepId = (UINT2) pProtectionCfmInfo->MonitorInfo.u4MepId;
        MepInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
        MepInfo.u4Event = ECFM_DEFECT_CONDITION_CLEARED;    /* Using it as 
                                                               a default 
                                                               value */

        if (EcfmMepRegisterAndGetFltStatus (&EcfmReg, &MepInfo) != ECFM_SUCCESS)
        {
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsPortEcfmRegisterPerMep: [PG: %d] "
                       "Registration failed for protection MEP, "
                       "meg=%d, me=%d, mep=%d\r\n",
                       pPgInfo->u4PgId, MepInfo.u4MdIndex, MepInfo.u4MaIndex,
                       MepInfo.u2MepId));

            /* De-register the previously registered info for working MEP */
            MEMSET (&MepInfo, 0, sizeof (tEcfmEventNotification));
            MepInfo.u4MdIndex = pWorkingCfmInfo->MonitorInfo.u4MegId;
            MepInfo.u4MaIndex = pWorkingCfmInfo->MonitorInfo.u4MeId;
            MepInfo.u2MepId = (UINT2) pWorkingCfmInfo->MonitorInfo.u4MepId;
            MepInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;

            EcfmMepDeRegister (&EcfmReg, &MepInfo);

            return OSIX_FAILURE;
        }

        /* Apply current status of protection MEP on PG */
        if (MepInfo.u4Event == ECFM_DEFECT_CONDITION_ENCOUNTERED)
        {
            pPgInfo->u1PgHwStatus = FALSE;
            ElpsCfmProcessReceivedSignal (pProtectionCfmInfo,
                                          ELPS_CFM_SIGNAL_FAIL);
            pPgInfo->u1PgHwStatus = TRUE;
        }
    }
    else if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
             (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        if ((pInMplsApiInfo = (tMplsApiInInfo *)
             MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
        {
            ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "ElpsPortEcfmRegisterPerMep:"
                       " Allocation of memory for Mpls In Api "
                       "Info FAILED!!!\r\n"));
            return OSIX_FAILURE;
        }

        MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

        pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
        pInMplsApiInfo->InRegParams.u4Events = (MPLS_MEG_DOWN_EVENT |
                                                MPLS_MEG_UP_EVENT |
                                                MPLS_AIS_CONDITION_ENCOUNTERED |
                                                MPLS_AIS_CONDITION_CLEARED |
                                                MPLS_PSC_PACKET);
        pInMplsApiInfo->InRegParams.pFnRcvPkt = ElpsApiNotificationRoutine;

        pInMplsApiInfo->u4SubReqType = MPLS_APP_REGISTER;
        /* Register for Working MEP */
        pWorkingCfmInfo = pPgInfo->WorkingEntity.pCfmConfig;

        pInMplsApiInfo->InPathId.MegId.u4MegIndex =
            pWorkingCfmInfo->MonitorInfo.u4MegId;
        pInMplsApiInfo->InPathId.MegId.u4MeIndex =
            pWorkingCfmInfo->MonitorInfo.u4MeId;
        pInMplsApiInfo->InPathId.MegId.u4MpIndex =
            pWorkingCfmInfo->MonitorInfo.u4MepId;
        pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;
        pInMplsApiInfo->InRegParams.u4ModId = MPLS_ELPS_APP_ID;
        pInMplsApiInfo->InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;

        if (ElpsPortMplsApiHandleExtRequest (MPLS_OAM_REGISTER_MEG_FOR_NOTIF,
                                             pInMplsApiInfo,
                                             NULL) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pInMplsApiInfo);
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsPortEcfmRegisterPerMep: [PG: %d] "
                       "Registration failed for working MEP, "
                       "meg=%d, me=%d, mep=%d\r\n",
                       pPgInfo->u4PgId,
                       pInMplsApiInfo->InPathId.MegId.u4MegIndex,
                       pInMplsApiInfo->InPathId.MegId.u4MeIndex,
                       pInMplsApiInfo->InPathId.MegId.u4MpIndex));
            return OSIX_FAILURE;
        }

        if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
        {
            /* Register for Working Reverse MEP */
            pWorkingCfmInfo = pPgInfo->WorkingReverseEntity.pCfmConfig;

            pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                pWorkingCfmInfo->MonitorInfo.u4MegId;
            pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                pWorkingCfmInfo->MonitorInfo.u4MeId;
            pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                pWorkingCfmInfo->MonitorInfo.u4MepId;
            pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;

            if (ElpsPortMplsApiHandleExtRequest
                (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo,
                 NULL) != OSIX_SUCCESS)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsPortEcfmRegisterPerMep: [PG: %d] "
                           "Registration failed for working Reverse MEP, "
                           "meg=%d, me=%d, mep=%d\r\n",
                           pPgInfo->u4PgId,
                           pInMplsApiInfo->InPathId.MegId.u4MegIndex,
                           pInMplsApiInfo->InPathId.MegId.u4MeIndex,
                           pInMplsApiInfo->InPathId.MegId.u4MpIndex));

                /* De-register the previously registered info for working MEP */
                pInMplsApiInfo->u4SubReqType = MPLS_APP_DEREGISTER;
                pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MegId;
                pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MeId;
                pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MepId;
                pInMplsApiInfo->u4ContextId =
                    pPgInfo->pContextInfo->u4ContextId;

                ElpsPortMplsApiHandleExtRequest
                    (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo, NULL);

                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pInMplsApiInfo);
                return OSIX_FAILURE;
            }

        }
        /* Register for Protection MEP */
        pProtectionCfmInfo = pPgInfo->ProtectionEntity.pCfmConfig;

        pInMplsApiInfo->InPathId.MegId.u4MegIndex =
            pProtectionCfmInfo->MonitorInfo.u4MegId;
        pInMplsApiInfo->InPathId.MegId.u4MeIndex =
            pProtectionCfmInfo->MonitorInfo.u4MeId;
        pInMplsApiInfo->InPathId.MegId.u4MpIndex =
            pProtectionCfmInfo->MonitorInfo.u4MepId;
        pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;

        if (ElpsPortMplsApiHandleExtRequest (MPLS_OAM_REGISTER_MEG_FOR_NOTIF,
                                             pInMplsApiInfo,
                                             NULL) != OSIX_SUCCESS)
        {
            ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsPortEcfmRegisterPerMep: [PG: %d] "
                       "Registration failed for protection MEP, "
                       "meg=%d, me=%d, mep=%d\r\n",
                       pPgInfo->u4PgId,
                       pInMplsApiInfo->InPathId.MegId.u4MegIndex,
                       pInMplsApiInfo->InPathId.MegId.u4MeIndex,
                       pInMplsApiInfo->InPathId.MegId.u4MpIndex));

            pInMplsApiInfo->u4SubReqType = MPLS_APP_DEREGISTER;
            /* De-register the previously registered info for working MEP */
            pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                pWorkingCfmInfo->MonitorInfo.u4MegId;
            pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                pWorkingCfmInfo->MonitorInfo.u4MeId;
            pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                pWorkingCfmInfo->MonitorInfo.u4MepId;
            pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;

            ElpsPortMplsApiHandleExtRequest (MPLS_OAM_REGISTER_MEG_FOR_NOTIF,
                                             pInMplsApiInfo, NULL);

            if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId
                != 0)
            {
                /* De-Register for Working Reverse MEP */
                pWorkingCfmInfo = pPgInfo->WorkingReverseEntity.pCfmConfig;

                pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MegId;
                pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MeId;
                pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MepId;
                pInMplsApiInfo->u4ContextId =
                    pPgInfo->pContextInfo->u4ContextId;

                ElpsPortMplsApiHandleExtRequest
                    (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo, NULL);
            }

            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pInMplsApiInfo);
            return OSIX_FAILURE;
        }

        if (pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId
            != 0)
        {
            /* Register for Protection Reverse MEP */
            pProtectionCfmInfo = pPgInfo->ProtectionReverseEntity.pCfmConfig;

            pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                pProtectionCfmInfo->MonitorInfo.u4MegId;
            pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                pProtectionCfmInfo->MonitorInfo.u4MeId;
            pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                pProtectionCfmInfo->MonitorInfo.u4MepId;
            pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;

            if (ElpsPortMplsApiHandleExtRequest
                (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo,
                 NULL) != OSIX_SUCCESS)
            {
                ELPS_TRC ((pPgInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsPortEcfmRegisterPerMep: [PG: %d] "
                           "Registration failed for protecrtion Reverse MEP, "
                           "meg=%d, me=%d, mep=%d\r\n",
                           pPgInfo->u4PgId,
                           pInMplsApiInfo->InPathId.MegId.u4MegIndex,
                           pInMplsApiInfo->InPathId.MegId.u4MeIndex,
                           pInMplsApiInfo->InPathId.MegId.u4MpIndex));

                pInMplsApiInfo->u4SubReqType = MPLS_APP_DEREGISTER;
                /* De-register the previously registered info for working MEP */
                pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MegId;
                pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MeId;
                pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                    pWorkingCfmInfo->MonitorInfo.u4MepId;
                pInMplsApiInfo->u4ContextId =
                    pPgInfo->pContextInfo->u4ContextId;

                ElpsPortMplsApiHandleExtRequest
                    (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo, NULL);

                if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.
                    u4MegId != 0)
                {
                    /* De-Register for Working Reverse MEP */
                    pWorkingCfmInfo = pPgInfo->WorkingReverseEntity.pCfmConfig;

                    pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                        pWorkingCfmInfo->MonitorInfo.u4MegId;
                    pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                        pWorkingCfmInfo->MonitorInfo.u4MeId;
                    pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                        pWorkingCfmInfo->MonitorInfo.u4MepId;
                    pInMplsApiInfo->u4ContextId =
                        pPgInfo->pContextInfo->u4ContextId;

                    ElpsPortMplsApiHandleExtRequest
                        (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo, NULL);
                }

                /* De-register the previously registered info for 
                 * protection MEP */

                pProtectionCfmInfo = pPgInfo->WorkingReverseEntity.pCfmConfig;
                pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                    pProtectionCfmInfo->MonitorInfo.u4MegId;
                pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                    pProtectionCfmInfo->MonitorInfo.u4MeId;
                pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                    pProtectionCfmInfo->MonitorInfo.u4MepId;
                pInMplsApiInfo->u4ContextId =
                    pPgInfo->pContextInfo->u4ContextId;

                ElpsPortMplsApiHandleExtRequest
                    (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo, NULL);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pInMplsApiInfo);
                return OSIX_FAILURE;
            }
        }

        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortEcfmDeRegisterPerMep
 *
 * DESCRIPTION      : This function is used by ELPS to de-register with 
 *                    ECFM/MPLS-OAM.
 *                    It de-register for indication from the 
 *                    working and protection MEPs for a protection group.
 *
 * INPUT            : pEcfmReg    - Pointer to the tEcfmRegParams structure
 *                    u4ContextId - Context Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsPortEcfmDeRegisterPerMep (tElpsPgInfo * pPgInfo)
{
    tElpsCfmConfigInfo *pWorkingCfmInfo = NULL;
    tElpsCfmConfigInfo *pProtectionCfmInfo = NULL;
    tEcfmRegParams      EcfmReg;
    tEcfmEventNotification MepInfo;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;

    if (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        MEMSET (&EcfmReg, 0, sizeof (tEcfmRegParams));

        EcfmReg.u4ModId = ECFM_ELPS_APP_ID;
        EcfmReg.u4EventsId = (ECFM_APS_FRAME_RECEIVED |
                              ECFM_DEFECT_CONDITION_CLEARED |
                              ECFM_DEFECT_CONDITION_ENCOUNTERED);
        EcfmReg.pFnRcvPkt = ElpsApiEcfmIndCallBack;

        /* De-Register Working MEP */
        MEMSET (&MepInfo, 0, sizeof (tEcfmEventNotification));
        pWorkingCfmInfo = pPgInfo->WorkingEntity.pCfmConfig;

        MepInfo.u4MdIndex = pWorkingCfmInfo->MonitorInfo.u4MegId;
        MepInfo.u4MaIndex = pWorkingCfmInfo->MonitorInfo.u4MeId;
        MepInfo.u2MepId = (UINT2) pWorkingCfmInfo->MonitorInfo.u4MepId;
        MepInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
        EcfmMepDeRegister (&EcfmReg, &MepInfo);

        /* De-Register Protection MEP */
        MEMSET (&MepInfo, 0, sizeof (tEcfmEventNotification));
        pProtectionCfmInfo = pPgInfo->ProtectionEntity.pCfmConfig;

        MepInfo.u4MdIndex = pProtectionCfmInfo->MonitorInfo.u4MegId;
        MepInfo.u4MaIndex = pProtectionCfmInfo->MonitorInfo.u4MeId;
        MepInfo.u2MepId = (UINT2) pProtectionCfmInfo->MonitorInfo.u4MepId;
        MepInfo.u4ContextId = pPgInfo->pContextInfo->u4ContextId;
        EcfmMepDeRegister (&EcfmReg, &MepInfo);
    }
    else if ((pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
             (pPgInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        if ((pInMplsApiInfo = (tMplsApiInInfo *)
             MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
        {
            ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC,
                       "ElpsPortEcfmDeRegisterPerMep:"
                       " Allocation of memory for Mpls In Api "
                       "Info FAILED!!!\r\n"));
            return;
        }

        MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

        pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
        pInMplsApiInfo->InRegParams.u4Events = (MPLS_MEG_DOWN_EVENT |
                                                MPLS_MEG_UP_EVENT |
                                                MPLS_AIS_CONDITION_ENCOUNTERED |
                                                MPLS_AIS_CONDITION_CLEARED |
                                                MPLS_PSC_PACKET);
        pInMplsApiInfo->InRegParams.pFnRcvPkt = ElpsApiNotificationRoutine;

        pInMplsApiInfo->InRegParams.u4ModId = MPLS_ELPS_APP_ID;
        pInMplsApiInfo->InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;

        pInMplsApiInfo->u4SubReqType = MPLS_APP_DEREGISTER;
        /* De-Register Working MEP */
        pWorkingCfmInfo = pPgInfo->WorkingEntity.pCfmConfig;

        pInMplsApiInfo->InPathId.MegId.u4MegIndex =
            pWorkingCfmInfo->MonitorInfo.u4MegId;
        pInMplsApiInfo->InPathId.MegId.u4MeIndex =
            pWorkingCfmInfo->MonitorInfo.u4MeId;
        pInMplsApiInfo->InPathId.MegId.u4MpIndex =
            pWorkingCfmInfo->MonitorInfo.u4MepId;
        pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;

        ElpsPortMplsApiHandleExtRequest (MPLS_OAM_REGISTER_MEG_FOR_NOTIF,
                                         pInMplsApiInfo, NULL);

        if (pPgInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId != 0)
        {
            /* De-Register for Working Reverse MEP */
            pWorkingCfmInfo = pPgInfo->WorkingReverseEntity.pCfmConfig;

            pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                pWorkingCfmInfo->MonitorInfo.u4MegId;
            pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                pWorkingCfmInfo->MonitorInfo.u4MeId;
            pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                pWorkingCfmInfo->MonitorInfo.u4MepId;
            pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;

            ElpsPortMplsApiHandleExtRequest
                (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo, NULL);
        }

        /* De-Register Protection MEP */
        pProtectionCfmInfo = pPgInfo->ProtectionEntity.pCfmConfig;

        pInMplsApiInfo->InPathId.MegId.u4MegIndex =
            pProtectionCfmInfo->MonitorInfo.u4MegId;
        pInMplsApiInfo->InPathId.MegId.u4MeIndex =
            pProtectionCfmInfo->MonitorInfo.u4MeId;
        pInMplsApiInfo->InPathId.MegId.u4MpIndex =
            pProtectionCfmInfo->MonitorInfo.u4MepId;
        pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;

        ElpsPortMplsApiHandleExtRequest (MPLS_OAM_REGISTER_MEG_FOR_NOTIF,
                                         pInMplsApiInfo, NULL);

        if (pPgInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId
            != 0)
        {
            /* De-Register for Protection Reverse MEP */
            pProtectionCfmInfo = pPgInfo->ProtectionReverseEntity.pCfmConfig;

            pInMplsApiInfo->InPathId.MegId.u4MegIndex =
                pProtectionCfmInfo->MonitorInfo.u4MegId;
            pInMplsApiInfo->InPathId.MegId.u4MeIndex =
                pProtectionCfmInfo->MonitorInfo.u4MeId;
            pInMplsApiInfo->InPathId.MegId.u4MpIndex =
                pProtectionCfmInfo->MonitorInfo.u4MepId;
            pInMplsApiInfo->u4ContextId = pPgInfo->pContextInfo->u4ContextId;

            ElpsPortMplsApiHandleExtRequest
                (MPLS_OAM_REGISTER_MEG_FOR_NOTIF, pInMplsApiInfo, NULL);
        }

        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortMplsApiHandleExtRequest
 *
 * DESCRIPTION      : This function gets the MPLS information based on the
 *                    request (Tunnel/Pw). This function is also used to send
 *                    out PSC pkt out throught MPLS.
 *
 * INPUT            : u4MainReqType - Request type 
 *                    pInMplsApiInfo - Pointer to the input API information.
 *
 * OUTPUT           : pOutMplsApiInfo - Pointer to the output API information.
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortMplsApiHandleExtRequest (UINT4 u4MainReqType,
                                 tMplsApiInInfo * pInMplsApiInfo,
                                 tMplsApiOutInfo * pOutMplsApiInfo)
{
#ifdef MPLS_WANTED
    if (MplsApiHandleExternalRequest (u4MainReqType, pInMplsApiInfo,
                                      pOutMplsApiInfo) == OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
#else
    UNUSED_PARAM (u4MainReqType);
    UNUSED_PARAM (pInMplsApiInfo);
    UNUSED_PARAM (pOutMplsApiInfo);
#endif
    return OSIX_FAILURE;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortEcfmInitiateExPdu
 *
 * DESCRIPTION      : Routine used by protocols to register with ECFM.
 *
 * INPUT            : pMepInfo      - Pointer to structure having information 
 *                                    to select a MEP node
 *                    u1TlvOffset   - TLV offset for the PDU
 *                    u1SubOpCode   - SubOpCode for the External PDU
 *                    pu1PduData    - Pointer to External data
 *                    u4PduDataLen  - Length of External data
 *                    TxDestMacAddr - Destination Mac Address
 *                    u1OpCode      - Opcode of the PDU to be sent
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortEcfmInitiateExPdu (tEcfmMepInfoParams * pMepInfo, UINT1 u1TlvOffset,
                           UINT1 u1SubOpCode, UINT1 *pu1PduData,
                           UINT4 u4PduDataLen, tMacAddr TxDestMacAddr,
                           UINT1 u1OpCode)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = EcfmInitiateExPdu (pMepInfo, u1TlvOffset, u1SubOpCode,
                                  pu1PduData, u4PduDataLen, TxDestMacAddr,
                                  u1OpCode);

    return (((i4RetVal == ECFM_SUCCESS) ? OSIX_SUCCESS : OSIX_FAILURE));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortVcmGetCxtInfoFromIfIndex 
 *
 * DESCRIPTION      : Routine used to get the Context Information from
 *                    ifIndex.
 *
 * INPUT            : uIfIndex - Port Identifier
 *
 * OUTPUT           : pu4ContextId - Context Identifier
 *                    pu2LocalPortId - Local Port Identifier
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortVcmGetCxtInfoFromIfIndex (UINT4 u4IfIndex, UINT4 *pu4ContextId,
                                  UINT2 *pu2LocalPortId)
{
    INT4                i4RetVal = VCM_FAILURE;

    i4RetVal = VcmGetContextInfoFromIfIndex (u4IfIndex, pu4ContextId,
                                             pu2LocalPortId);

    return (((i4RetVal == VCM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortVcmIsSwitchExist 
 *
 * DESCRIPTION      : Routine used to get if the context exists for the input 
 *                    Switch Alias name Information 
 *
 * INPUT            : pu1Alias - Context Name
 *
 * OUTPUT           : pu4ContextId - Context Identifier  
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortVcmIsSwitchExist (UINT1 *pu1Alias, UINT4 *pu4ContextId)
{
    INT4                i4RetVal = VCM_FALSE;

    i4RetVal = VcmIsSwitchExist (pu1Alias, pu4ContextId);

    return (((i4RetVal == VCM_FALSE) ? OSIX_FALSE : OSIX_TRUE));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortVcmGetAliasName 
 *
 * DESCRIPTION      : Routine used to get the Alias Name for the context 
 *
 * INPUT            : u4ContextId - Context Identifier 
 *
 * OUTPUT           : pu1Alias - Context Name
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    INT4                i4RetVal = VCM_FAILURE;

    i4RetVal = VcmGetAliasName (u4ContextId, pu1Alias);

    return (((i4RetVal == VCM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortCfaGetIfInfo 
 *
 * DESCRIPTION      : This function returns the interface related params
 *                    assigned to this interface to the external modules.
 *
 * INPUT            : u4IfIndex - Port Identifier
 *
 * OUTPUT           : pIfInfo   - Pointer to the tCfaIfInfo
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortCfaGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    INT4                i4RetVal = CFA_FAILURE;

    i4RetVal = CfaGetIfInfo (u4IfIndex, pIfInfo);

    return (((i4RetVal == CFA_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortCfaCliGetIfName 
 *
 * DESCRIPTION      : Routine used to get the Alias Name for IfIndex
 *
 * INPUT            : u4IfIndex - Port Identifier
 *
 * OUTPUT           : pi1IfName - IfIndex Alias Name
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortCfaCliGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = CfaCliGetIfName (u4IfIndex, pi1IfName);

    return (((i4RetVal == CFA_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortCfaCliConfGetIfName 
 *
 * DESCRIPTION      : Routine used to get the Alias Name for IfInde
 *
 * INPUT            : u4IfIndex - Port Identifier
 *
 * OUTPUT           : pi1IfName - IfIndex Alias Name
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortCfaCliConfGetIfName (UINT4 u4IfIndex, INT1 *pi1IfName)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = CfaCliConfGetIfName (u4IfIndex, pi1IfName);

    return (((i4RetVal == CFA_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortHwPgSwitchDataPath
 *
 * DESCRIPTION      : This function is called whenever a protection group
 *                    is created, deleted, or switching happens.
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4PgId      - Protection Group Identifier
 *                    pHwPgSwitchInfo - Pointer to the hardware switch Info.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortHwPgSwitchDataPath (UINT4 u4ContextId, UINT4 u4PgId,
                            tElpsHwPgSwitchInfo * pHwPgSwitchInfo)
{

#ifdef NPAPI_WANTED
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    if ((pHwPgSwitchInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
        (pHwPgSwitchInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        pContextInfo = ElpsUtilValidateContextInfo (u4ContextId, &u4Dummy);

        if (pContextInfo == NULL)
        {
            return OSIX_FAILURE;
        }

        pPgConfigInfo = ElpsPgGetNode (pContextInfo, u4PgId);

        if (pPgConfigInfo == NULL)
        {
            ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protectioin Group entry %d is not created\n", u4PgId));
            return OSIX_FAILURE;
        }

        if ((pMplsApiInInfo = (tMplsApiInInfo *)
             MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
        {
            ELPS_TRC ((pPgConfigInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsTxFillPwInfo:"
                       " Allocation of memory for Mpls In Api Info FAILED!!!\r\n"));
            return OSIX_FAILURE;
        }

        if ((pMplsApiOutInfo = (tMplsApiOutInfo *)
             MemAllocMemBlk (gElpsGlobalInfo.MplsOutApiPoolId)) == NULL)
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            ELPS_TRC ((pPgConfigInfo->pContextInfo, ELPS_CRITICAL_TRC,
                       "ElpsTxFillPwInfo:"
                       "Allocation of memory for Mpls Out Api Info FAILED!!!\r\n"));
            return OSIX_FAILURE;
        }
        /*Get Tunnel info from MPLS for Service Type ELPS_PG_SERVICE_TYPE_LSP */
        if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST)
            {
                pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                    pPgConfigInfo->unServiceList.pWorkingServiceListPointer->
                    WorkingInfo.LspServiceInfo.u4TnlIndex;
                pMplsApiInInfo->InPathId.TnlId.u4LspNum =
                    pPgConfigInfo->unServiceList.pWorkingServiceListPointer->
                    WorkingInfo.LspServiceInfo.u4TnlInstance;
                pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.
                    u4_addr[0] =
                    pPgConfigInfo->unServiceList.pWorkingServiceListPointer->
                    WorkingInfo.LspServiceInfo.u4TnlIngressLsrId;
                pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.
                    u4_addr[0] =
                    pPgConfigInfo->unServiceList.pWorkingServiceListPointer->
                    WorkingInfo.LspServiceInfo.u4TnlEgressLsrId;
            }
            else
            {

                pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                    pPgConfigInfo->WorkingEntity.ServiceParam.LspServiceInfo.
                    u4TnlIndex;
                pMplsApiInInfo->InPathId.TnlId.u4LspNum =
                    pPgConfigInfo->WorkingEntity.ServiceParam.LspServiceInfo.
                    u4TnlInstance;
                pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.
                    u4_addr[0] =
                    pPgConfigInfo->WorkingEntity.ServiceParam.LspServiceInfo.
                    u4TnlIngressLsrId;
                pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.
                    u4_addr[0] =
                    pPgConfigInfo->WorkingEntity.ServiceParam.LspServiceInfo.
                    u4TnlEgressLsrId;
            }

            /* Api to get the Tunnel Information */
            if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                                 pMplsApiInInfo,
                                                 pMplsApiOutInfo) ==
                OSIX_FAILURE)
            {
                MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                    (UINT1 *) pMplsApiInInfo);
                MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                    (UINT1 *) pMplsApiOutInfo);
                ELPS_TRC ((pPgConfigInfo->pContextInfo, ELPS_CRITICAL_TRC,
                           "ElpsPortHwPgSwitchDataPath:"
                           "Working Tunnel is not found!!!\r\n"));
                return OSIX_FAILURE;
            }

            if (pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus == TRUE)
            {
                MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
                MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

                if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST)
                {
                    pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                        pPgConfigInfo->unServiceList.
                        pWorkingServiceListPointer->ProtectionInfo.
                        LspServiceInfo.u4TnlIndex;
                    pMplsApiInInfo->InPathId.TnlId.u4LspNum =
                        pPgConfigInfo->unServiceList.
                        pWorkingServiceListPointer->ProtectionInfo.
                        LspServiceInfo.u4TnlInstance;
                    pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.
                        u4_addr[0] =
                        pPgConfigInfo->unServiceList.
                        pWorkingServiceListPointer->ProtectionInfo.
                        LspServiceInfo.u4TnlIngressLsrId;
                    pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.
                        u4_addr[0] =
                        pPgConfigInfo->unServiceList.
                        pWorkingServiceListPointer->ProtectionInfo.
                        LspServiceInfo.u4TnlEgressLsrId;
                }
                else
                {
                    pMplsApiInInfo->InPathId.TnlId.u4SrcTnlNum =
                        pPgConfigInfo->ProtectionEntity.ServiceParam.
                        LspServiceInfo.u4TnlIndex;
                    pMplsApiInInfo->InPathId.TnlId.u4LspNum =
                        pPgConfigInfo->ProtectionEntity.ServiceParam.
                        LspServiceInfo.u4TnlInstance;
                    pMplsApiInInfo->InPathId.TnlId.SrcNodeId.MplsRouterId.
                        u4_addr[0] =
                        pPgConfigInfo->ProtectionEntity.ServiceParam.
                        LspServiceInfo.u4TnlIngressLsrId;
                    pMplsApiInInfo->InPathId.TnlId.DstNodeId.MplsRouterId.
                        u4_addr[0] =
                        pPgConfigInfo->ProtectionEntity.ServiceParam.
                        LspServiceInfo.u4TnlEgressLsrId;
                }

                /* Api to get the Tunnel Information */
                if (ElpsPortMplsApiHandleExtRequest (MPLS_GET_TUNNEL_INFO,
                                                     pMplsApiInInfo,
                                                     pMplsApiOutInfo)
                    == OSIX_FAILURE)
                {
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                        (UINT1 *) pMplsApiInInfo);
                    MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                        (UINT1 *) pMplsApiOutInfo);
                    ELPS_TRC ((pPgConfigInfo->pContextInfo, ELPS_CRITICAL_TRC,
                               "ElpsPortHwPgSwitchDataPath:"
                               "Protection Tunnel is not found!!!\r\n"));
                    return OSIX_FAILURE;
                }
            }
        }

        /* Verifying the NP programming of the tunnels */
        if ((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) &&
            ((pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus == FALSE) ||
             ((pPgConfigInfo->u1PgHwStatus == TRUE) &&
              (pHwPgSwitchInfo->u1PgAction == ELPS_PG_CREATE) &&
              (pPgConfigInfo->u1PgConfigType != ELPS_PG_TYPE_LIST))))
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            ELPS_TRC ((NULL, ELPS_CRITICAL_TRC,
                       "ElpsPortHwPgSwitchDataPath: Tunnel is not programmed yet"
                       " or PG is already programmed\r\n"));

            return OSIX_SUCCESS;
        }

        if ((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) &&
            (pMplsApiOutInfo->OutTeTnlInfo.u1TnlHwStatus == FALSE) &&
            ((pHwPgSwitchInfo->u1PgAction == ELPS_PG_SWITCH_TO_WORKING) ||
             (pHwPgSwitchInfo->u1PgAction == ELPS_PG_SWITCH_TO_PROTECTION)))
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
            ELPS_TRC ((NULL, ELPS_CRITICAL_TRC,
                       "ElpsPortHwPgSwitchDataPath: Tunnel is not programmed yet"
                       "Cannot switch from working to protection or vice versa"
                       "\r\n"));
            return OSIX_SUCCESS;
        }
    }
    if (ElpsFsMiElpsHwPgSwitchDataPath (u4ContextId, u4PgId,
                                        pHwPgSwitchInfo) != FNP_SUCCESS)
    {
        if ((pHwPgSwitchInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
            (pHwPgSwitchInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                                (UINT1 *) pMplsApiOutInfo);
        }
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC,
                   "ElpsPortHwPgSwitchDataPath: NP call Failed !!!!\r\n"));
        return OSIX_FAILURE;
    }

    if ((pHwPgSwitchInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
        (pHwPgSwitchInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
    {
        /*PG hardware status is TRUE */
        pPgConfigInfo->u1PgHwStatus = TRUE;

        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (gElpsGlobalInfo.MplsOutApiPoolId,
                            (UINT1 *) pMplsApiOutInfo);
    }
#else
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4PgId);
    UNUSED_PARAM (pHwPgSwitchInfo);
#endif /* NPAPI_WANTED */

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortFmNotifyFaults
 *
 * DESCRIPTION      : This function Sends the trap message to the Fault  
 *                    Manager.
 *
 * INPUT            : pContextInfo - pointer to the context info structure
 *                    pPgInfo - pointer to the protection group info structure
 *                    u4PgStatus - status of the protection group. this 
 *                                 can contain the following values -
 *                                 o ELPS_PG_CREATE
 *                                 o ELPS_PG_DELETE
 *                                 o ELPS_PG_SWITCH_TO_WORKING
 *                                 o ELPS_PG_SWITCH_TO_PROTECTION
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsPortFmNotifyFaults (tSNMP_OID_TYPE * pEnterpriseOid, UINT4 u4GenTrapType,
                        UINT4 u4SpecTrapType, tSNMP_VAR_BIND * pTrapMsg,
                        UINT1 *pu1Msg, tSNMP_OCTET_STRING_TYPE * pContextName)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));
    FmFaultMsg.pEnterpriseOid = pEnterpriseOid;
    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pCxtName = pContextName;
    FmFaultMsg.pSyslogMsg = pu1Msg;
    FmFaultMsg.u4GenTrapType = u4GenTrapType;
    FmFaultMsg.u4SpecTrapType = u4SpecTrapType;
    FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_ELPS;

    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsPortFmNotifyFaults: Sending "
                   "Notification to FM failed.\r\n"));
        SNMP_AGT_FreeVarBindList (pTrapMsg);
        SNMP_FreeOid (pEnterpriseOid);
    }
#else
    SNMP_AGT_FreeVarBindList (pTrapMsg);
    SNMP_FreeOid (pEnterpriseOid);
    UNUSED_PARAM (u4GenTrapType);
    UNUSED_PARAM (u4SpecTrapType);
    UNUSED_PARAM (pu1Msg);
    UNUSED_PARAM (pContextName);
#endif
    return;
}

/******************************************************************************
 * Function Name      : ElpsPortNotifyProtocolShutStatus
 *
 * Description        : This function sends indication to MSR on module 
 *                      shutdown in a context
 *
 * Input(s)           : apu1Token      - Poiner to token array 
 *                      pu4TraceOption - Pointer to trace option 
 *
 * Output(s)          : pu4TraceOption - Pointer to trace option 
 *
 * Return Value(s)    : VOID
 *****************************************************************************/
PUBLIC VOID
ElpsPortNotifyProtocolShutStatus (INT4 i4ContextId)
{
#ifdef ISS_WANTED
    UINT1               au1ObjectOid[ELPS_TWO][SNMP_MAX_OID_LENGTH];
    UINT2               u2Objects = ELPS_TWO;

    /* The oid list contains the list of Oids that has been registered 
     * for the protocol + the oid of the SystemControl object. */
    SNMPGetOidString (fselps, (sizeof (fselps) / sizeof (UINT4)),
                      au1ObjectOid[0]);

    SNMPGetOidString (FsElpsContextSystemControl,
                      (sizeof (FsElpsContextSystemControl) / sizeof (UINT4)),
                      au1ObjectOid[1]);

    /* Send a notification to MSR to process the elps shutdown, with 
     * elps oids and its system control object
     */
    MsrNotifyProtocolShutdown (au1ObjectOid, u2Objects, i4ContextId);
#else
    UNUSED_PARAM (i4ContextId);
#endif
    return;
}

/******************************************************************************
 * Function Name      : ElpsPortVcmSispGetPhysicalPortOfSispPort 
 *
 * Description        : This function gets the Physical Port's IfIndex
 *                      of the Sisp ingress or working or protection Port as 
 *                      needed for the ELPS to call its NPAPI.
 *
 * Input(s)           : u4LogicalIfIndex - Sisp Port's IfIndex 
 *
 * Output(s)          : pu4PhyIfIndex - Physical Port's IfIndex 
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
ElpsPortVcmSispGetPhysicalPortOfSispPort (UINT4 u4LogicalIfIndex,
                                          UINT4 *pu4PhyIfIndex)
{
    INT4                i4RetVal = OSIX_FAILURE;

    i4RetVal = VcmSispGetPhysicalPortOfSispPort (u4LogicalIfIndex,
                                                 pu4PhyIfIndex);

    return (((i4RetVal == VCM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/******************************************************************************
 * Function Name      : ElpsPortSNMPCheckForNVTChars
 *
 * Description        : This function finds if any non-printable characters
 *                      present in the given string ( includes special chars,
 *                      non-US ascii chars). Useful to check for valid inputs
 *                      for DisplayString Type objects.(as per RFC: 1903).
 *
 * Input(s)           : pu1TestStr  - pointer to string to be validated
 *                      i4Length    - Length upto which the string will be
 *                                    validated.
 *
 * Output(s)          : None
 *
 * Return Value(s)    : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
PUBLIC INT4
ElpsPortSNMPCheckForNVTChars (UINT1 *pu1TestStr, INT4 i4Length)
{
#ifdef SNMP_2_WANTED
    return (SNMPCheckForNVTChars (pu1TestStr, i4Length));
#else
    UNUSED_PARAM (pu1TestStr);
    UNUSED_PARAM (i4Length);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsPortRmEnqMsgToRm                                 */
/*                                                                           */
/* Description        : This function calls the RM Module to enqueue the     */
/*                      message from applications to RM task.                */
/*                                                                           */
/* Input(s)           : pRmMsg - msg from appl.                              */
/*                      u2DataLen - Len of msg.                              */
/*                      u4SrcEntId - EntId from which the msg has arrived.   */
/*                      u4DestEntId - EntId to which the msg has to be sent. */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
ElpsPortRmEnqMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen,
                      UINT4 u4SrcEntId, UINT4 u4DestEntId)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
                                     u4SrcEntId, u4DestEntId);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pRmMsg);
    UNUSED_PARAM (u2DataLen);
    UNUSED_PARAM (u4SrcEntId);
    UNUSED_PARAM (u4DestEntId);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsPortRmGetNodeState                               */
/*                                                                           */
/* Description        : This function calls the RM module to get the node    */
/*                      state.                                               */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_ACTIVE/RM_INIT_/RM_STANDBY                        */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
ElpsPortRmGetNodeState (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetNodeState ());
#else
    return RM_ACTIVE;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsPortRmGetStandbyNodeCount                        */
/*                                                                           */
/* Description        : This function calls the RM module to get the number  */
/*                      of peer nodes that are up.                           */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : Number of Booted up standby nodes                    */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
ElpsPortRmGetStandbyNodeCount (VOID)
{
#ifdef L2RED_WANTED
    return (RmGetStandbyNodeCount ());
#else
    return 0;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsPortRmApiHandleProtocolEvent                     */
/*                                                                           */
/* Description        : This function calls the RM module to intimate about  */
/*                      the protocol operations                              */
/*                                                                           */
/* Input(s)           : pEvt->u4AppId - Application Id                       */
/*                      pEvt->u4Event - Event send by protocols to RM        */
/*                                      (RM_PROTOCOL_BULK_UPDT_COMPLETION /  */
/*                                      RM_BULK_UPDT_ABORT /                 */
/*                                      RM_STANDBY_TO_ACTIVE_EVT_PROCESSED / */
/*                                      RM_IDLE_TO_ACTIVE_EVT_PROCESSED /    */
/*                                      RM_STANDBY_EVT_PROCESSED)            */
/*                      pEvt->u4Err   - Error code (RM_MEMALLOC_FAIL /       */
/*                                      RM_SENDTO_FAIL / RM_PROCESS_FAIL)    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT1
ElpsPortRmApiHandleProtocolEvent (tRmProtoEvt * pEvt)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmApiHandleProtocolEvent (pEvt);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pEvt);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsPortRmRegisterProtocols                          */
/*                                                                           */
/* Description        : This function calls the RM module to register.       */
/*                                                                           */
/* Input(s)           : tRmRegParams - Reg. params to be provided by         */
/*                      protocols.                                           */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
ElpsPortRmRegisterProtocols (tRmRegParams * pRmReg)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmRegisterProtocols (pRmReg);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pRmReg);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsPortRmDeRegisterProtocols                        */
/*                                                                           */
/* Description        : This function deregisters PBB-TE with RM.            */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RM_SUCCESS/RM_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC UINT4
ElpsPortRmDeRegisterProtocols (VOID)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmDeRegisterProtocols (RM_ELPS_APP_ID);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsPortRmReleaseMemoryForMsg                        */
/*                                                                           */
/* Description        : This function calls the RM module to release the     */
/*                      memory allocated for sending the Standby node count. */
/*                                                                           */
/* Input(s)           : pu1Block - Mmemory block.                            */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ElpsPortRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmReleaseMemoryForMsg (pu1Block);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsPortRmSetBulkUpdatesStatus                       */
/*                                                                           */
/* Description        : This function calls the RM module to set the Status  */
/*                      of the bulk updates.                                 */
/*                                                                           */
/* Input(s)           : u4AppId - Application Identifier.                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
ElpsPortRmSetBulkUpdatesStatus (UINT4 u4AppId)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmSetBulkUpdatesStatus (u4AppId);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (u4AppId);
    return OSIX_SUCCESS;
#endif
}

/***************************************************************************
 * FUNCTION NAME    : ElpsPortRmApiSendProtoAckToRM 
 *
 * DESCRIPTION      : This is the function used by protocols/applications 
 *                    to send acknowledgement to RM after processing the
 *                    sync-up message.
 *
 * INPUT            : tRmProtoAck contains
 *                    u4AppId  - Protocol Identifier
 *                    u4SeqNum - Sequence number of the RM message for
 *                    which this ACK is generated.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsPortRmApiSendProtoAckToRM (tRmProtoAck * pProtoAck)
{
#ifdef L2RED_WANTED
    INT4                i4RetVal = OSIX_FAILURE;
    i4RetVal = RmApiSendProtoAckToRM (pProtoAck);
    return (((i4RetVal == RM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
#else
    UNUSED_PARAM (pProtoAck);
    return OSIX_SUCCESS;
#endif
}

/* HITLESS RESTART */

/******************************************************************************
 * Function           : ElpsPortRmGetHRFlag
 * Input(s)           : None
 * Output(s)          : None
 * Returns            : Hitless restart flag value.
 * Action             : This API returns the hitless restart flag value.
 ******************************************************************************/
UINT1
ElpsPortRmGetHRFlag (VOID)
{
#ifdef L2RED_WANTED
    UINT1               u1HRFlag = 0;

    u1HRFlag = RmGetHRFlag ();

    return (u1HRFlag);
#else
    return OSIX_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name      : ElpsRedHRSendStdyStPkt                               */
/*                                                                           */
/* Description        : This function sends steady state packet to RM.       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : SUCCESS/FAILURE.                                     */
/*****************************************************************************/
INT1
ElpsRedHRSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktLen,
                        UINT2 u2Port, UINT4 u4TimeOut)
{
#ifdef L2RED_WANTED
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4MsgLen = 0;
    UINT4               u4BufSize = 0;

    if (ElpsRedGetNodeState () != RM_ACTIVE)
    {
        /*Only the Active node needs to send the SyncUp message */
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS: Node is not active. "
                   "Steady state packet is not sent to RM.\n"));
        return OSIX_SUCCESS;
    }
    if (ELPS_HR_STATUS () == ELPS_HR_STATUS_DISABLE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS: Hitless restart is not"
                   " enabled. Steady state tail msg is not sent to RM.\n"));
        return OSIX_SUCCESS;
    }
    ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS: sending steady "
               "state tail message to RM.\n"));
    /* Forming  steady state packet
     *
     *
     *   <- 24B --><--------- 1B ---------><-- 2B -->< 2B -><-- 4B -->< 60 B >
     *   _____________________________________________________________________
     *   |        |                       |         |      |         |        |
     *   | RM Hdr | RM_HR_STDY_ST_PKT_MSG | Msg Len | PORT | TimeOut | Buffer |
     *   |________|_______________________|_________|______|_________|________|
     *
     * The RM Hdr shall be included by RM.
     */
    u4BufSize = ELPS_RED_TYPE_FIELD_SIZE + ELPS_RED_LEN_FIELD_SIZE + sizeof (UINT2)    /* for port */
        + sizeof (UINT4) /* Timeout */  + u4PktLen;
    /* Allocate memory for data to be sent to RM. This memory will be freed by
     *      *      *      *      *      * RM after Txmitting  */
    if ((pMsg = RM_ALLOC_TX_BUF (u4BufSize)) == NULL)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "RM ALLOC FAILED\n"));
        return (OSIX_FAILURE);
    }
    /* Fill the message type. */
    u4MsgLen = u4PktLen + sizeof (UINT4) + sizeof (UINT2);

    ELPS_RM_PUT_1_BYTE (pMsg, &u4Offset, RM_HR_STDY_ST_PKT_MSG);
    ELPS_RM_PUT_2_BYTE (pMsg, &u4Offset, (UINT2) u4MsgLen);
    ELPS_RM_PUT_2_BYTE (pMsg, &u4Offset, u2Port);
    ELPS_RM_PUT_4_BYTE (pMsg, &u4Offset, u4TimeOut);

    /* copying the packet into the RM steady state msg */
    CRU_BUF_Concat_MsgBufChains (pMsg, pBuf);

    /* Send the packet to RM */
    if (ElpsRedSendMsgToRm (pMsg, (UINT2) u4BufSize) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS: steady state pkt"
                   " sending to RM is failed\n"));
    }
    return OSIX_SUCCESS;
#else
    UNUSED_PARAM (pBuf);
    UNUSED_PARAM (u4PktLen);
    UNUSED_PARAM (u2Port);
    UNUSED_PARAM (u4TimeOut);
    return OSIX_SUCCESS;
#endif
}

/****************************************************************************/
/* FUNCTION NAME    : ElpsPortSendStdyStPkt                                 */
/*                                                                          */
/* DESCRIPTION      : This function is called by L2IWF Module when steady   */
/*                    state packet is to be sent during hitless restart     */
/*                    ELPS_LOCK should be taken before calling this API     */
/*                                                                          */
/* INPUT            : pBuf      - R-APS PDU                                 */
/*                    u4PktSize - Packet Size                               */
/*                    u4IfIndex - Interface Index                           */
/*                                                                          */
/* OUTPUT           : None                                                  */
/*                                                                          */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE.                            */
/*                                                                          */
/****************************************************************************/
PUBLIC INT4
ElpsPortSendStdyStPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4PktSize,
                       UINT4 u4IfIndex)
{
    UINT4               u4PeriodicTime = 0;

    if (ELPS_HR_STDY_ST_REQ_RCVD () == OSIX_FALSE)
    {
        return OSIX_FAILURE;
    }

    if (ElpsUtilGetPeriodicTime (u4IfIndex, &u4PeriodicTime) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (ElpsRedHRSendStdyStPkt (pBuf, u4PktSize, (UINT2) u4IfIndex,
                                u4PeriodicTime) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************/
/* FUNCTION NAME    : ElpsPortSendChannelCode                               */
/*                                                                          */
/* DESCRIPTION      : This function is to send the PSC Channel code to      */
/*                    MPLS OAM Module.                                      */
/*                                                                          */
/* INPUT            : u4ChannelCode - PSC Channel code                      */
/*                                                                          */
/* OUTPUT           : None                                                  */
/*                                                                          */
/* RETURNS          : OSIX_SUCCESS/OSIX_FAILURE.                            */
/*                                                                          */
/****************************************************************************/
PUBLIC INT4
ElpsPortSendChannelCode (UINT4 u4ChannelCode)
{
    tMplsApiInInfo     *pInMplsApiInfo = NULL;

    if ((pInMplsApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.MplsInApiPoolId)) == NULL)
    {
        return OSIX_FAILURE;
    }

    pInMplsApiInfo->u4ContextId = 0;
    pInMplsApiInfo->u4SrcModId = ELPS_MODULE;
    pInMplsApiInfo->InAchChnlCode = (UINT2) u4ChannelCode;

    if (ElpsPortMplsApiHandleExtRequest (MPLS_OAM_UPDATE_ACH_CHNL_CODE,
                                         pInMplsApiInfo, NULL) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                            (UINT1 *) pInMplsApiInfo);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (gElpsGlobalInfo.MplsInApiPoolId,
                        (UINT1 *) pInMplsApiInfo);

    return OSIX_SUCCESS;

}
#endif /* _ELPSPORT_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsport.c                     */
/*-----------------------------------------------------------------------*/
