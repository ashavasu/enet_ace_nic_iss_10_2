/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstrc.c,v 1.5 2014/03/14 12:56:12 siva Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/
#include "elpsinc.h"

/****************************************************************************
 *                                                                           
 * Function     : ElpsTrcTrace                                                 
 *                                                                           
 * Description  : converts variable argument in to string depending on flag  
 *                                                                           
 * Input        : u4ContextId - Context Identifier 
 *                u4Flags  - Trace flag 
 *                fmt  - format strong, variable argument
 *                                                                           
 * Output       : None                                                       
 *                                                                           
 * Returns      : VOID                                             
 *                                                                           
 *****************************************************************************/
VOID
ElpsTrcTrace (tElpsContextInfo * pContextInfo, UINT4 u4Flags,
              const char *fmt, ...)
{
    va_list             ap;
    static CHR1         ac1Buf[ELPS_TRC_BUF_SIZE];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT4               u4OffSet = 0;

    if (pContextInfo == NULL)
    {
        if (gElpsGlobalInfo.u1GlobalTrcOption == ELPS_SNMP_TRUE)
        {
            MEMSET (ac1Buf, 0, sizeof (ac1Buf));
            SNPRINTF (ac1Buf, sizeof (ac1Buf), "ELPS: ");
        }
        else
        {
            return;
        }
    }
    else
    {
        if ((u4Flags & (pContextInfo->u4TraceInput)) == 0)
        {
            return;
        }

        MEMSET (ac1Buf, 0, sizeof (ac1Buf));
        MEMSET (au1ContextName, 0, sizeof (au1ContextName));
        ElpsPortVcmGetAliasName (pContextInfo->u4ContextId, au1ContextName);

        SNPRINTF (ac1Buf, sizeof (ac1Buf), "ELPS[%s]", au1ContextName);
    }

    u4OffSet = STRLEN (ac1Buf);

    va_start (ap, fmt);
    vsprintf (&ac1Buf[u4OffSet], fmt, ap);
    va_end (ap);

    UtlTrcPrint ((const char *) ac1Buf);
    return;
}

/******************************************************************************
 * 
 * Function           : ElpsTrcApsPktDumpTrc
 * 
 * Description        : This function dumps the packet and also print the
 *                      details of the APS header. The APS Specific information
 *                      is parsed and the details are shown. The entire 
 *                      Packet contents are shown with Hexa values at the end.
 * 
 * Input(s)           : pBuf      - Liner buffer pointer for the packet to be
 *                                  dumped
 *                      Length    - Length of the packet to be dumped
 *                      Str       -
 * 
 * Output(s)          : None.
 * 
 * Returns            : None. 
 ******************************************************************************/
VOID
ElpsTrcApsPktDumpTrc (tElpsContextInfo * pContextInfo,
                      UINT1 *pu1Buf, UINT4 u4Length)
{
#define ELPS_MAX_DUMP_LEN        512
#define ELPS_PRINT_BUF_LEN       80

    UINT4               u4Count = 0;
    UINT4               u4PrintCount = 0;
    CHR1                ac1OutBuf[ELPS_PRINT_BUF_LEN];    /* Print Buf */
    UINT1               u1ApsEndTLV = 0;
    UINT1               u1ApsState = 0;
    UINT1               u1ApsProtectionType = 0;
    UINT1               u1ApsReqSignal = 0;
    UINT1               u1ApsBridgedSignal = 0;

    if (pContextInfo == NULL)
    {
        return;
    }

    if ((pContextInfo->u4TraceInput & DUMP_TRC) != DUMP_TRC)
    {
        return;
    }

    if (u4Length > ELPS_MAX_DUMP_LEN)
    {
        u4Length = ELPS_MAX_DUMP_LEN;
    }

#define APS_HDR_OFF_MEL 0
#define APS_HDR_OFF_VERSION 0
#define APS_HDR_OFF_OPCODE 1
#define APS_HDR_OFF_FLAGS 2
#define APS_HDR_OFF_TLV 3
#define APS_PAY_OFF_STATE 0
#define APS_PAY_OFF_PROT_TYPE 0
#define APS_PAY_OFF_REQ_SIG 1
#define APS_PAY_OFF_BRG_SIG 2
#define APS_PAY_OFF_RES 3
#define APS_TAIL_OFF_TLV 4

    /*************** Read APS PayLoad ****************************/

    /* Request/State in the APS PDU */
    ELPS_GET_REQUEST_FROM_PDU (u1ApsState, pu1Buf);

    /* Protection Type in the APS PDU */
    u1ApsProtectionType = ((*(pu1Buf + APS_PAY_OFF_PROT_TYPE) & 0x0f));

    /* Requested Signal in the APS PDU */
    u1ApsReqSignal = (*(pu1Buf + APS_PAY_OFF_REQ_SIG));

    /* Bridged Signal in the APS PDU */
    u1ApsBridgedSignal = (*(pu1Buf + APS_PAY_OFF_BRG_SIG));

    /*************** Read APS Tail ******************************/

    /* End TLV in the APS PDU */
    u1ApsEndTLV = (*(pu1Buf + APS_TAIL_OFF_TLV));

    /*************** Print the APS PayLoad ************************/
    UtlTrcPrint ("\r\n");
    UtlTrcPrint ("----------------- APS PayLoad ----------------------\r\n");

    /* Parse and Print APS Request/State */

    switch (u1ApsState)
    {
        case ELPS_PKT_REQ_STATE_LO:
            MEMSET (&ac1OutBuf[0], 0, sizeof (ac1OutBuf));
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Lockout of Protection (LO)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_SF_P:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Signal Fail for Protection (SF-P)",
                     u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_FS:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Forced Switch (FS)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_SF:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Signal Fail for Working (SF-W)",
                     u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_MS:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Manual Switch (MS-P)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_MS_W:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Manual Switch (MS-W)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_WTR:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Wait To Restore (WTR)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_EXER:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Exercise (EXER)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_RR:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Reverse Request (RR)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_DNR:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Do Not Revert (DNR)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PKT_REQ_STATE_NR:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "No Request (NR)", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;

        default:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Not Supported states", u1ApsState);
            UtlTrcPrint (ac1OutBuf);
            break;
    }

    /* Parse and Print APS Protection Type */

    if ((u1ApsProtectionType & ELPS_A_BIT_VALUE) == 0)
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - A field", "No APS Channel", 0);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - A field", "APS Channel", 1);
        UtlTrcPrint (ac1OutBuf);
    }

    if ((u1ApsProtectionType & ELPS_B_BIT_VALUE) == 0)
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - B field", "1+1 (Permanent Bridge)", 0);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - B field", "1:1 (no Permanent Bridge)", 1);
        UtlTrcPrint (ac1OutBuf);
    }

    if ((u1ApsProtectionType & ELPS_D_BIT_VALUE) == 0)
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - D field", "UniDirectional Switching", 0);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - D field", "BiDirectional Switching", 1);
        UtlTrcPrint (ac1OutBuf);
    }

    if ((u1ApsProtectionType & ELPS_R_BIT_VALUE) == 0)
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - R field", "Non-Revertive Operation", 0);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - R field", "Revertive Operation", 1);
        UtlTrcPrint (ac1OutBuf);
    }

    /* Parse and Print APS Requested Signal */

    MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
    if (u1ApsReqSignal == ELPS_SIGNAL_NULL)
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Requested Signal", "Null Signal - Working Entity",
                 u1ApsReqSignal);
        UtlTrcPrint (ac1OutBuf);
    }
    else if (u1ApsReqSignal == ELPS_SIGNAL_NORMAL)
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Requested Signal", "Normal Traffic Signal"
                 "- Protection Entity", u1ApsReqSignal);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Requested Signal", "Reserved for future use", u1ApsReqSignal);
        UtlTrcPrint (ac1OutBuf);
    }

    /* Parse and Print APS Bridged Signal */

    MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
    if (u1ApsBridgedSignal == ELPS_SIGNAL_NULL)
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Bridged Signal", "Null Signal - Working Entity",
                 u1ApsBridgedSignal);
        UtlTrcPrint (ac1OutBuf);
    }
    else if (u1ApsBridgedSignal == ELPS_SIGNAL_NORMAL)
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Bridged Signal", "Normal Traffic Signal"
                 "- Protection Entity", u1ApsBridgedSignal);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Bridged Signal", "Reserved for future use",
                 u1ApsBridgedSignal);
        UtlTrcPrint (ac1OutBuf);
    }

    MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
    SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %d\r\n", "End TLV", u1ApsEndTLV);
    UtlTrcPrint (ac1OutBuf);

    /* Read and Print the Entire Packet in Hexa Format */
    UtlTrcPrint ("----- PACKET CONTENTS: -------\r\n");
    MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);

    u4PrintCount = 0;
    while (u4Count < u4Length)
    {
        SPRINTF ((CHR1 *) & ac1OutBuf[u4PrintCount], "%02x ",
                 *(pu1Buf + u4Count));

        u4Count++;

        /* need 3 bytes for "%02x " */
        u4PrintCount = u4PrintCount + ELPS_THREE;

        if (((u4Count % 16) == 0) || (u4Count == u4Length))
        {
            SPRINTF ((CHR1 *) & ac1OutBuf[u4PrintCount], "\n");
            /* Trailing '\0' char will be added along with the above 
             * statement */
            UtlTrcPrint (ac1OutBuf);
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            u4PrintCount = 0;
        }
    }
    UtlTrcPrint
        ("====================== END ============================\r\n\n");

    return;
}

/******************************************************************************
 * 
 * Function           : ElpsTrcPscPktDumpTrc
 * 
 * Description        : This function dumps the packet and also print the
 *                      details of the PSC header. The PSC Specific information
 *                      is parsed and the details are shown. The entire 
 *                      Packet contents are shown with Hexa values at the end.
 * 
 * Input(s)           : pBuf      - CRU buffer pointer for the packet to be
 *                                  dumped
 *                      Length    - Length of the packet to be dumped
 * 
 * Output(s)          : None.
 * 
 * Returns            : None. 
 ******************************************************************************/
VOID
ElpsTrcPscPktDumpTrc (tElpsContextInfo * pContextInfo,
                      tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Offset)
{
#define ELPS_PRINT_BUF_LEN       80

    UINT4               u4Count = 0;
    UINT4               u4PrintCount = 0;
    UINT4               u4Len = 0;
    UINT2               u2TlvLength = 0;
    CHR1                ac1OutBuf[ELPS_PRINT_BUF_LEN];    /* Print Buf */
    UINT1               u1PscVersion = 0;
    UINT1               u1PscState = 0;
    UINT1               u1PscProtectionType = 0;
    UINT1               u1PscDirection = 0;
    UINT1               u1PscFaultPath = 0;
    UINT1               u1PscPath = 0;
    UINT1               u1PscRevertMode = 0;
    UINT1               au1PscData[ELPS_PSC_PDU_LEN];
    UINT1              *pu1Buf = NULL;

    if (pContextInfo == NULL)
    {
        return;
    }

    if ((pContextInfo->u4TraceInput & DUMP_TRC) != DUMP_TRC)
    {
        return;
    }
#define PSC_ACH_HDR_OFF_LENGTH 2
#define PSC_PAY_OFF_FPATH      2
#define PSC_PAY_OFF_PATH       3

    if (u1Offset != 0)
    {
        /* Get the length of the ACH TLV */
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   ((UINT1 *) &u2TlvLength),
                                   (u1Offset + ELPS_PSC_GACH_HDR_LEN),
                                   ELPS_TWO);
        /* Get the PSC packet */
        CRU_BUF_Copy_FromBufChain (pBuf,
                                   au1PscData,
                                   (u1Offset +
                                    ELPS_PSC_GACH_HDR_LEN +
                                    ELPS_PSC_ACH_TLV_HDR_LEN
                                    + u2TlvLength), ELPS_PSC_PDU_LEN);
    }
    else
    {
        /* Get the packet */
        CRU_BUF_Copy_FromBufChain (pBuf, au1PscData, 0, ELPS_PSC_PDU_LEN);
    }

    pu1Buf = au1PscData;

    u4Len = ELPS_PSC_PDU_LEN;

    /*************** Read PSC PayLoad ****************************/

    /* Version in the PSC PDU */
    ELPS_GET_VERSION_FROM_PSC_PDU (u1PscVersion, pu1Buf);

    /* Request/State in the PSC PDU */
    ELPS_GET_REQUEST_FROM_PSC_PDU (u1PscState, pu1Buf);

    /* Protection Type in the PSC PDU */
    ELPS_GET_BRIDGE_FROM_PSC_PDU (u1PscProtectionType, pu1Buf);

    /* Direction in the PSC PDU */
    ELPS_GET_DIRECTION_FROM_PSC_PDU (u1PscDirection, pu1Buf);

    /* Revertive mode in the PSC PDU */
    ELPS_GET_R_BIT_FROM_PSC_PDU (u1PscRevertMode, pu1Buf);

    /* Fault Path in the PSC PDU */
    u1PscFaultPath = (*(pu1Buf + PSC_PAY_OFF_FPATH));

    /* Path in the PSC PDU */
    u1PscPath = (*(pu1Buf + PSC_PAY_OFF_PATH));

    /*************** Print the PSC PayLoad ************************/
    UtlTrcPrint ("\r\n");
    UtlTrcPrint ("----------------- PSC PayLoad ----------------------\r\n");

    /* Parse and Print PSC Request/State */

    switch (u1PscState)
    {
        case ELPS_PSC_PKT_REQ_STATE_LO:
            MEMSET (&ac1OutBuf[0], 0, sizeof (ac1OutBuf));
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Lockout of Protection (LO)", u1PscState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PSC_PKT_REQ_STATE_SF:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Signal Fail", u1PscState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PSC_PKT_REQ_STATE_FS:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Forced Switch (FS)", u1PscState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PSC_PKT_REQ_STATE_MS:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Manual Switch ", u1PscState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PSC_PKT_REQ_STATE_WTR:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Wait To Restore (WTR)", u1PscState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PSC_PKT_REQ_STATE_DNR:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Do Not Revert (DNR)", u1PscState);
            UtlTrcPrint (ac1OutBuf);
            break;

        case ELPS_PSC_PKT_REQ_STATE_NR:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "No Request (NR)", u1PscState);
            UtlTrcPrint (ac1OutBuf);
            break;

        default:
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                     "Request/State", "Not Supported states", u1PscState);
            UtlTrcPrint (ac1OutBuf);
            break;
    }

    /* Parse and Print APS Protection Type */

    if (u1PscProtectionType == 1)
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type field", "1+1 (Permanent Bridge)", 1);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type field", "1:1 (no Permanent Bridge)", 0);
        UtlTrcPrint (ac1OutBuf);
    }

    if (u1PscDirection == 0)
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - D field", "UniDirectional Switching", 0);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - D field", "BiDirectional Switching", 1);
        UtlTrcPrint (ac1OutBuf);
    }

    if (u1PscRevertMode == 0)
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - R field", "Non-Revertive Operation", 0);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s : %s 0x%02x\r\n",
                 "Protection Type - R field", "Revertive Operation", 1);
        UtlTrcPrint (ac1OutBuf);
    }

    /* Parse and Print PSC Fault Path */

    MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
    if (u1PscFaultPath == ELPS_SIGNAL_NULL)
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Fault Signal", "Fault on - Protection Entity",
                 u1PscFaultPath);
        UtlTrcPrint (ac1OutBuf);
    }
    else if (u1PscFaultPath == ELPS_SIGNAL_NORMAL)
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Fault Signal", "Fault on" "- Working Entity", u1PscFaultPath);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Fault Signal", "Reserved for future use", u1PscFaultPath);
        UtlTrcPrint (ac1OutBuf);
    }

    /* Parse and Print PSC Path */

    MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
    if (u1PscPath == ELPS_SIGNAL_NULL)
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Bridged Signal", "Traffic bridged on - Working Entity",
                 u1PscPath);
        UtlTrcPrint (ac1OutBuf);
    }
    else if (u1PscPath == ELPS_SIGNAL_NORMAL)
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Bridged Signal", "Traffic bridged on - "
                 "- Protection Entity", u1PscPath);
        UtlTrcPrint (ac1OutBuf);
    }
    else
    {
        SPRINTF ((CHR1 *) ac1OutBuf, "%-22s    : %s 0x%02x\r\n",
                 "Bridged Signal", "Reserved for future use", u1PscPath);
        UtlTrcPrint (ac1OutBuf);
    }

    /* Read and Print the Entire Packet in Hexa Format */
    UtlTrcPrint ("----- PACKET CONTENTS: -------\r\n");
    MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);

    u4PrintCount = 0;
    while (u4Count < u4Len)
    {
        SPRINTF ((CHR1 *) & ac1OutBuf[u4PrintCount], "%02x ",
                 *(au1PscData + u4Count));

        u4Count++;
        /* need 3 bytes for "%02x " */
        u4PrintCount = u4PrintCount + ELPS_THREE;

        if (((u4Count % 16) == 0) || (u4Count == u4Len))
        {
            SPRINTF ((CHR1 *) & ac1OutBuf[u4PrintCount], "\n");
            /* Trailing '\0' char will be added along with the above 
             * statement */
            UtlTrcPrint (ac1OutBuf);
            MEMSET (&ac1OutBuf[0], 0, ELPS_PRINT_BUF_LEN);
            u4PrintCount = 0;
        }
    }
    UtlTrcPrint
        ("====================== END ============================\r\n\n");

    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpstrc.c                      */
/*-----------------------------------------------------------------------*/
