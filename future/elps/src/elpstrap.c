/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpstrap.c,v 1.9 2014/03/01 11:39:03 siva Exp $
 *
 * Description: This file contains debugging related functions
 *****************************************************************************/
#include "elpsinc.h"

#ifdef SNMP_3_WANTED
#include "fselps.h"

CHR1               *gac1ElpsTrapMsg[] = {
    NULL,
    "AutoProtectionSwitch",
    "ForcedSwitch",
    "ManualSwitch-Protection",
    "HwSwitchToWorkingPathFailed",
    "HwSwitchToProtectionPathFailed",
    "HwProtectionGroupCreationFailed",
    "HwProtectionGroupDeletionFailed",
    "ApsPduTxFailed",
    "BbitMismatch",
    "ConfigurationMismatch",
    "LackOfResponse",
    "AbitMismatch",
    "DbitMismatch",
    "RbitMismatch",
    "ManualSwitch-Working"
};

CHR1               *gac1ElpsSyslogMsg[] = {
    NULL,
    "Switching due to Auto Protection Switch",
    "Switching due to Force Switch",
    "Switching to Protection due to Manual Switch",
    "Switching to Working Path Failed in Hardware",
    "Switching to Protection Path Failed in Hardware",
    "Creating the Protection Group Failed in Hardware",
    "Deleting the Protection Group Failed in Hardware",
    "APS PDU Transmission Failed",
    "B-bit Mismatch found in the Received APS PDU",
    "APS PDU received on working transport entity",
    "Lack of Response to the Requested Signal",
    "A-bit Mismatch found in the Received APS PDU",
    "D-bit Mismatch found in the Received APS PDU",
    "R-bit Mismatch found in the Received APS PDU",
    "Switching to Working due to Manual Switch",
};

/* Proto types of the functions private to this file only */
PRIVATE tSNMP_OID_TYPE *ElpsTrapMakeObjIdFrmString
PROTO ((INT1 *pi1TextStr, UINT1 *pu1TableName));

PRIVATE INT4
    ElpsTrapParseSubIdNew PROTO ((UINT1 **ppu1TempPtr, UINT4 *pu4Value));

/******************************************************************************
* Function :   ElpsTrapMakeObjIdFrmString
*
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*              pTableName - TableName has to be fetched.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/
PRIVATE tSNMP_OID_TYPE *
ElpsTrapMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1                ai1TempBuffer[ELPS_OBJECT_NAME_LEN + 1];
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    MEMSET (ai1TempBuffer, 0, sizeof (ai1TempBuffer));

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TextStr) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) &&
              (u2Index < ELPS_OBJECT_NAME_LEN)); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP (pTableName[u2Index].pName, (INT1 *) ai1TempBuffer)
                 == 0) && (STRLEN ((INT1 *) ai1TempBuffer) ==
                           STRLEN (pTableName[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         STRLEN (pTableName[u2Index].pNumber));
                break;
            }
        }
        ai1TempBuffer[STRLEN (pTableName[u2Index].pNumber)] = '\0';
        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 STRLEN (pi1DotPtr) + 1);
    }
    else
    {
        /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 STRLEN (pi1TextStr));
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    for (u2Index = 0; ((u2Index <= ELPS_OBJECT_NAME_LEN) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (ElpsTrapParseSubIdNew
            (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function :   ElpsTrapParseSubIdNew
* 
* Description : Parse the string format in number.number..format.
*
* Input       : ppu1TempPtr - pointer to the string.
*               pu4Value    - Pointer the OID List value.
*               
* Output      : value of ppu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/
PRIVATE INT4
ElpsTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}
#endif

/*****************************************************************************/
/* Function Name      : ElpsTrapSendTrapNotifications                        */
/*                                                                           */
/* Description        : This function will send an SNMP trap to the          */
/*                      administrator for various ELPS conditions.           */
/*                                                                           */
/* Input(s)           : pNotifyInfo - Information to be sent in the Trap     */
/*                                   message.                                */
/*                      u1TrapType - Specific Type for Trap Message          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
VOID
ElpsTrapSendTrapNotifications (tElpsNotifyInfo * pNotifyInfo, UINT1 u1TrapType)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring2 = NULL;
    tSNMP_OCTET_STRING_TYPE *pContextName = NULL;
    tElpsContextInfo   *pContextInfo = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               au4TrapOid[] = { 1, 3, 6, 1, 4, 1, 29601, 2, 25, 4 };
    UINT4               u4GenTrapType = ENTERPRISE_SPECIFIC;
    UINT4               u4SpecTrapType = ELPS_INVALID_TRAP;
    UINT1               au1ContextName[ELPS_SWITCH_ALIAS_LEN];
    UINT1               au1Buf[ELPS_OBJECT_NAME_LEN];
    UINT1               au1TrapDisplayStr[ELPS_MAX_TRAP_STR_LEN];

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    if (ElpsRedGetNodeState () != RM_ACTIVE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ElpsTrapSendTrapNotifications: "
                   "Traps are not sent at standby node\r\n"));
        return;
    }

    switch (u1TrapType)
    {
        case ELPS_TRAP_FORCED_SWITCH:
        case ELPS_TRAP_MANUAL_SWITCH:
        case ELPS_TRAP_MANUAL_SWITCH_W:
        case ELPS_TRAP_AUTO_PROT_SWITCH:
            u4SpecTrapType = ELPS_TRAP_PROTECTION_SWITCH_ID;
            break;

        case ELPS_TRAP_HW_PG_CRT_FAIL:
        case ELPS_TRAP_HW_PG_DEL_FAIL:
        case ELPS_TRAP_HW_SWITCH_TO_WORK_FAIL:
        case ELPS_TRAP_HW_SWITCH_TO_PROT_FAIL:
        case ELPS_TRAP_APS_PDU_TX_FAIL:
            u4SpecTrapType = ELPS_TRAP_SWITCH_FAILURE_ID;
            break;

        case ELPS_TRAP_PG_TYPE_MISMATCH:
        case ELPS_TRAP_CONF_MISMATCH:
        case ELPS_TRAP_LOR:
        case ELPS_TRAP_A_BIT_MISMATCH:
        case ELPS_TRAP_D_BIT_MISMATCH:
        case ELPS_TRAP_R_BIT_MISMATCH:
            u4SpecTrapType = ELPS_TRAP_TYPE_MISMATCH_ID;
            break;

        default:
            ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapSendNotification: "
                       "Invalid Trap Type. Failed!!!\r\n"));
            return;
    }

    pContextInfo = ElpsCxtGetNode (pNotifyInfo->u4ContextId);

    if (pContextInfo == NULL)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapSendNotification: Invalid "
                   "Context specified. Failed!!!\r\n"));
        return;
    }

    if (pContextInfo->u1TrapStatusFlag == ELPS_SNMP_FALSE)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapSendNotification: "
                   "Transmission option for Trap is disabled.\r\n"));
        return;
    }

    /* Filling the Enterprise OID */
    pEnterpriseOid = alloc_oid (ELPS_SNMPV2_TRAP_OID_LEN);

    if (pEnterpriseOid == NULL)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapSendNotification: "
                   "OID Memory Allocation Failed\r\n"));
        return;
    }

    MEMCPY (pEnterpriseOid->pu4_OidList, au4TrapOid, sizeof (au4TrapOid));

    /* Filling the Context-Name */
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SPRINTF ((char *) au1Buf, "fsElpsTrapContextName");

    pOid = ElpsTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                       (UINT1 *) fs_elps_orig_mib_oid_table);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: OID "
                   "for fsElpsTrapContextName Not Found. Failed!!!\r\n"));
        return;
    }

    MEMSET (au1ContextName, 0, sizeof (au1ContextName));

    ElpsPortVcmGetAliasName (pNotifyInfo->u4ContextId, au1ContextName);
    pOstring = SNMP_AGT_FormOctetString (au1ContextName,
                                         (ELPS_SWITCH_ALIAS_LEN - 1));

    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: Failed "
                   "to form Octet string for fsElpsTrapContextName.\r\n"));
        return;
    }

    pVbList = SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                                    pOstring, NULL, SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: Failed "
                   "to form VarBind for fsElpsTrapContextName.\r\n"));
        return;
    }

    pStartVb = pVbList;

    /* Filling the Protection Group Status */
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SPRINTF ((char *) au1Buf, "fsElpsPgCmdPgStatus");

    pOid = ElpsTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                       (UINT1 *) fs_elps_orig_mib_oid_table);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: OID "
                   "for fsElpsPgCmdPgStatus Not Found. Failed!!!\r\n"));
        return;
    }

    /* The Object is tabular. Hence append the Index value. */
    pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4ContextId;
    pOid->u4_Length++;
    pOid->pu4_OidList[pOid->u4_Length] = pNotifyInfo->u4PgId;
    pOid->u4_Length++;

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_INTEGER32, 0, pNotifyInfo->u1PgStatus,
         NULL, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: Failed "
                   "to form VarBind for object fsElpsPgCmdPgStatus.\r\n"));
        return;
    }

    pVbList = pVbList->pNextVarBind;

    /* Filling the Other information depending on the Trap */
    MEMSET (au1Buf, 0, sizeof (au1Buf));

    switch (u4SpecTrapType)
    {
        case ELPS_TRAP_PROTECTION_SWITCH_ID:
            SPRINTF ((char *) au1Buf, "fsElpsTrapSwitchingMechanism");
            break;

        case ELPS_TRAP_SWITCH_FAILURE_ID:
            SPRINTF ((char *) au1Buf, "fsElpsTypeOfFailure");
            break;

        default:
            SPRINTF ((char *) au1Buf, "fsElpsTrapMismatchType");
            break;

    }

    pOid = ElpsTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                       (UINT1 *) fs_elps_orig_mib_oid_table);

    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: OID "
                   "Not Found. Failed!!!\r\n"));
        return;
    }

    MEMSET (au1TrapDisplayStr, 0, sizeof (au1TrapDisplayStr));
    STRNCPY (au1TrapDisplayStr, gac1ElpsTrapMsg[u1TrapType],
             STRLEN (gac1ElpsTrapMsg[u1TrapType]));

    pOstring2 =
        SNMP_AGT_FormOctetString (au1TrapDisplayStr, ELPS_MAX_TRAP_STR_LEN);

    if (pOstring2 == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: Failed "
                   "to form Octet string.\r\n"));
        return;
    }

    pVbList->pNextVarBind =
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                              pOstring2, NULL, SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring2);
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: Failed "
                   "to form VarBind.\r\n"));
        return;
    }

    pContextName = SNMP_AGT_FormOctetString (au1ContextName,
                                             (ELPS_SWITCH_ALIAS_LEN - 1));

    if (pContextName == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ElpsTrapMakeObjIdFrmString: Failed "
                   "to form Octet string for ContextName.\r\n"));
        return;
    }

    ElpsPortFmNotifyFaults (pEnterpriseOid, u4GenTrapType, u4SpecTrapType,
                            pStartVb, (UINT1 *) gac1ElpsSyslogMsg[u1TrapType],
                            pContextName);

    SNMP_AGT_FreeOctetString (pContextName);
#else
    UNUSED_PARAM (pNotifyInfo);
    UNUSED_PARAM (u1TrapType);
#endif
    return;
}
