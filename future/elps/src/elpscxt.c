/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpscxt.c,v 1.5 2011/11/30 06:06:12 siva Exp $
 *
 * Description: This file contains the Context Information Table related
 * datastructure implementation and utility functions.
 *****************************************************************************/
#ifndef _ELPSCXT_C_
#define _ELPSCXT_C_

#include "elpsinc.h"

/* Proto types of the functions private to this file only */
PRIVATE INT4 ElpsCxtCreateRBTrees PROTO ((tElpsContextInfo * pCxt));
PRIVATE VOID ElpsCxtDeleteRBTrees PROTO ((tElpsContextInfo * pCxt));

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtCreateContext
 *
 * DESCRIPTION      : This function Creates the context in the ELPS module.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCxtCreateContext (UINT4 u4ContextId)
{
    tElpsContextInfo   *pCxt = NULL;

    if ((pCxt = (tElpsContextInfo *)
         MemAllocMemBlk (gElpsGlobalInfo.ContextTablePoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsCxtCreateContext: Allocation "
                   "of memory for Context Info FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    MEMSET (pCxt, 0, sizeof (tElpsContextInfo));

    gElpsGlobalInfo.apContextInfo[u4ContextId] = pCxt;

    pCxt->u4ContextId = u4ContextId;
    pCxt->u1ModuleStatus = ELPS_DISABLED;
    pCxt->u4TraceInput = ELPS_CRITICAL_TRC;
    pCxt->u1TrapStatusFlag = ELPS_SNMP_TRUE;
    pCxt->i4VlanGroupManager = ELPS_VLAN_GROUP_MANAGER_MSTP;
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtDeleteContext
 *
 * DESCRIPTION      : This function deletes the context from the ELPS module.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCxtDeleteContext (UINT4 u4ContextId)
{
    tElpsContextInfo   *pCxt = NULL;

    pCxt = ElpsCxtGetNode (u4ContextId);

    if (pCxt == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsCxtDeleteContext: Context "
                   "not create (or) already deleted !!!\r\n"));
        return OSIX_FAILURE;
    }

    /* If ELPS is started, then first shutdown the ELPS in that context. */
    if (gElpsGlobalInfo.au1SystemCtrl[u4ContextId] == ELPS_START)
    {
        if (ElpsCxtShutdown (pCxt) == OSIX_FAILURE)
        {
            ELPS_TRC ((pCxt, ALL_FAILURE_TRC, "ElpsCxtDeleteContext: "
                       "ElpsCxtShutdown Failed \r\n"));
            return OSIX_FAILURE;
        }
    }

    MemReleaseMemBlock (gElpsGlobalInfo.ContextTablePoolId, (UINT1 *) pCxt);

    gElpsGlobalInfo.au1SystemCtrl[u4ContextId] = ELPS_SHUTDOWN;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtStart
 *
 * DESCRIPTION      : This function starts the ELPS module in the specified 
 *                    context.
 *
 * INPUT            : pCxt   - Pointer to the context info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCxtStart (tElpsContextInfo * pCxt)
{
    if (ElpsCxtCreateRBTrees (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsCxtStart: Creation of RBTrees FAILED !!!\r\n"));
        ElpsCxtDeleteRBTrees (pCxt);
        return OSIX_FAILURE;
    }

    gElpsGlobalInfo.au1SystemCtrl[pCxt->u4ContextId] = ELPS_START;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtShutdown
 *
 * DESCRIPTION      : This function shutsdown the ELPS module in the specified 
 *                    context.
 *
 * INPUT            : pCxt   - Pointer to the Context Info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCxtShutdown (tElpsContextInfo * pCxt)
{
    /* Send the Np Sync about the Context Shutdown. Block the Sending of
     * NP Sync-ups so that Deletion of All protection groups does not 
     * generate NP-Syncups. Increment the Block Counter.
     */

    ElpsHwAdSendCxtSync (pCxt->u4ContextId, ELPS_RED_HW_AUD_MOD_SHUTDOWN);
    ElpsRedHwAuditIncBlkCounter ();

    /* Need to switch/bridge the normal traffic from protection port to 
     * working port. */

    if (ElpsPgDisableAllPgEntries (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, ALL_FAILURE_TRC, "ElpsCxtShutdown: "
                   "ElpsPgDisableAllPgEntries Failed \r\n"));
        return OSIX_FAILURE;
    }

    ElpsCxtDeleteRBTrees (pCxt);

    gElpsGlobalInfo.au1SystemCtrl[pCxt->u4ContextId] = ELPS_SHUTDOWN;

    /* Reset the Blocking of Np sync-ups as the Context Shutdown is done.
     * Decrement the block counter.
     */
    ElpsRedHwAuditDecBlkCounter ();
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtEnable
 *
 * DESCRIPTION      : This function enables the ELPS module in the specified 
 *                    context.
 *
 * INPUT            : pCxt   - Pointer to the context information structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCxtEnable (tElpsContextInfo * pCxt)
{
    /* Send the Np Sync about the Context Enable. Block the Sending of
     * NP Sync-ups so that Creation of All protection groups does not 
     * generate NP-Syncups.Increment the block counter. 
     */

    if (ElpsTnlUpdateTnlTableBaseOid () == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC, "ElpsMainModuleStart:"
                   "Getting Tunnel Table Base OID FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    if (ElpsPwUpdatePwTableBaseOid () == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC, "ElpsMainModuleStart:"
                   "Getting Pseudowire Table Base OID FAILED!!!\r\n"));
        return OSIX_FAILURE;
    }

    ElpsHwAdSendCxtSync (pCxt->u4ContextId, ELPS_RED_HW_AUD_MOD_ENABLE);
    ElpsRedHwAuditIncBlkCounter ();

    if (ElpsPgEnableAllPgEntries (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, ALL_FAILURE_TRC, "ElpsCxtEnable: "
                   "ElpsPgEnableAllPgEntries Failed \r\n"));
        return OSIX_FAILURE;
    }

    pCxt->u1ModuleStatus = ELPS_ENABLED;

    /* Reset the Blocking of Np sync-ups as the Context Enable is done.
     * Decrement the Block Counter.
     */
    ElpsRedHwAuditDecBlkCounter ();

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtDisable
 *
 * DESCRIPTION      : This function disables the ELPS module in the specified 
 *                    context.
 *
 * INPUT            : pCxt   - Pointer to the context information structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsCxtDisable (tElpsContextInfo * pCxt)
{
    /* Send the Np Sync about the Context Disable Block the Sending of
     * NP Sync-ups so that Deletion of All protection groups does not 
     * generate NP-Syncups. Increment the Block counter.
     */

    ElpsHwAdSendCxtSync (pCxt->u4ContextId, ELPS_RED_HW_AUD_MOD_DISABLE);
    ElpsRedHwAuditIncBlkCounter ();

    if (ElpsPgDisableAllPgEntries (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, ALL_FAILURE_TRC, "ElpsCxtDisable: "
                   "ElpsPgDisableAllPgEntries Failed \r\n"));
        return OSIX_FAILURE;
    }

    pCxt->u1ModuleStatus = ELPS_DISABLED;

    /* Reset the Blocking of Np sync-ups as the Context Disable is done.
     * Decrement the Block counter.
     */
    ElpsRedHwAuditDecBlkCounter ();

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtGetNode
 *
 * DESCRIPTION      : This function returns the pointer to the Context info 
 *                    structure.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC tElpsContextInfo *
ElpsCxtGetNode (UINT4 u4ContextId)
{
    if (u4ContextId >= ELPS_MAX_CONTEXTS)
    {
        /* Context Id not present in the valid range */
        return NULL;
    }

    return (gElpsGlobalInfo.apContextInfo[u4ContextId]);
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtCreateRBTrees
 *
 * DESCRIPTION      : This function creates all the RBTree required in the 
 *                    specified context.
 *
 * INPUT            : pCxt - Pointer to the Context Info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PRIVATE INT4
ElpsCxtCreateRBTrees (tElpsContextInfo * pCxt)
{
    if (ElpsPgCreateTable (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsCxtCreateRBTrees: ElpsPgCreateTable FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    if (ElpsCfmCreateTable (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsCxtCreateRBTrees: ElpsCfmCreateTable FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    if (ElpsSrvListCreateTable (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsCxtCreateRBTrees: ElpsSrvListCreateTable "
                   "FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    if (ElpsSrvListPointerCreateTable (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsCxtCreateRBTrees: ElpsSrvListPointerCreateTable "
                   "FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    if (ElpsPgShareCreateTable (pCxt) == OSIX_FAILURE)
    {
        ELPS_TRC ((pCxt, (INIT_SHUT_TRC | ALL_FAILURE_TRC),
                   "ElpsCxtCreateRBTrees: ElpsPgShareCreateTable "
                   "FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsCxtDeleteRBTrees
 *
 * DESCRIPTION      : This function deletes all the RBTree created in the 
 *                    specified context.
 *
 * INPUT            : pCxt - Pointer to the Context Info structure.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PRIVATE VOID
ElpsCxtDeleteRBTrees (tElpsContextInfo * pCxt)
{
    if (pCxt->PgInfoTbl != NULL)
    {
        ElpsPgDeleteTable (pCxt);
        pCxt->PgInfoTbl = NULL;
    }
    if (pCxt->PgShareTbl != NULL)
    {
        ElpsPgShareDeleteTable (pCxt);
        pCxt->PgShareTbl = NULL;
    }

    if (pCxt->PgServiceListTbl != NULL)
    {
        ElpsSrvListDeleteTable (pCxt);
        pCxt->PgServiceListTbl = NULL;
    }

    if (pCxt->PgServiceListPointerTbl != NULL)
    {
        ElpsSrvListPointerDeleteTable (pCxt);
        pCxt->PgServiceListPointerTbl = NULL;
    }

    if (pCxt->CfmConfigTbl != NULL)
    {
        ElpsCfmDeleteTable (pCxt);
        pCxt->CfmConfigTbl = NULL;
    }

    return;
}

#endif /* _ELPSCXT_C_ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  elpscxt.c                      */
/*-----------------------------------------------------------------------*/
