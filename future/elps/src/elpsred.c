/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsred.c,v 1.16 2013/12/07 11:08:41 siva Exp $
 *
 * Description: This file contains the ELPS Redundancy support routines and
 * utility routines.
 *****************************************************************************/
#ifndef _ELPSRED_C_
#define _ELPSRED_C_

#include "elpsinc.h"

PUBLIC tElpsSemState gaElpsSemStateInfo[];

static UINT1        u1MemEstFlag = 1;
extern INT4         IssSzUpdateSizingInfoForHR (CHR1 * pu1ModName,
                                                CHR1 * pu1StName,
                                                UINT4 u4BulkUnitSize);
/***************************************************************************
 * FUNCTION NAME    : ElpsRedInitGlobalInfo  
 *
 * DESCRIPTION      : Initializes redundancy global variables.This function 
 *                    will get invoked during BOOTUP. It registers with RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
ElpsRedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_ELPS_APP_ID;
    RmRegParams.pFnRcvPkt = ElpsRedRmCallBack;

    if (ElpsPortRmRegisterProtocols (&RmRegParams) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, INIT_SHUT_TRC | ALL_FAILURE_TRC | ELPS_CRITICAL_TRC,
                   "ElpsRedInitGlobalInfo:"
                   " Registration with RM FAILED !!!\r\n"));
        return OSIX_FAILURE;
    }

    /* Default value of Node Status is IDLE */
    gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_INIT;
    gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp =
        ElpsPortRmGetStandbyNodeCount ();
    gElpsGlobalInfo.RedGlobalInfo.bBulkReqRecvd = OSIX_FALSE;
    gElpsGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount = 0;

    /* NP-Syncups are not to be sent from Idle node. NPSync buffer Table 
     * should not be accessed when the node is Idle. Set the Count value to
     * 1 so that the Np sync table is blocked.
     */
    ElpsRedHwAuditIncBlkCounter ();

    /* Hardware Audit Np-Sync Buffer Mempool */
    gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId = 0;

    /* HITLESS RESTART */
    ELPS_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedDeInitGlobalInfo  
 *
 * DESCRIPTION      : Deinitializes redundancy global variables.This function 
 *                    will get invoked during BOOTUP failure. Also De-registers
 *                    with RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedDeInitGlobalInfo (VOID)
{
    if (gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId != 0)
    {
        OsixTskDel (gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId);
        gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId = 0;
    }
    ElpsPortRmDeRegisterProtocols ();
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedRmCallBack 
 *
 * DESCRIPTION      : This function will be invoked by the RM module to
 *                    enqueue events and post messages to ELPS module.
 *
 * INPUT            : u1Event - Event given by RM module.
 *                    pData   - Msg to be enqueued, if u1Event is
 *                              valid.
 *                    u2DataLen - Size of the update message.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : RM_SUCCESS/RM_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsRedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tElpsQMsg          *pMsg = NULL;

    /* If the received event is not any of the following, then just return
     * without processing the event */
    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != L2_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE) &&
        (u1Event != RM_INIT_HW_AUDIT))
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC | BUFFER_TRC,
                   "ElpsRedRmCallBack: This "
                   "Event is not associated with RM !!!\r\n"));
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send these events to ELPS task. */
        ELPS_TRC ((NULL, ALL_FAILURE_TRC | BUFFER_TRC,
                   "ElpsRedRmCallBack: Queue Message "
                   "associated with Event is not sent by RM !!!\r\n"));
        return OSIX_FAILURE;
    }

    if (gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE)
    {
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            ElpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                   "ElpsRedRmCallBack: ELPS task is not initialized\r\n"));
        return OSIX_FAILURE;
    }

    if ((pMsg = (tElpsQMsg *)
         MemAllocMemBlk (gElpsGlobalInfo.QMsgPoolId)) == NULL)
    {
        ELPS_TRC ((NULL, OS_RESOURCE_TRC, "ElpsRedRmCallBack: Allocation "
                   "of memory for Queue Message FAILED !!!\r\n"));
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            ElpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        return OSIX_FAILURE;
    }

    MEMSET (pMsg, 0, sizeof (tElpsQMsg));

    pMsg->u4MsgType = ELPS_RM_MSG;
    pMsg->unMsgParam.RmCtrlMsg.pData = pData;
    pMsg->unMsgParam.RmCtrlMsg.u1Event = u1Event;
    pMsg->unMsgParam.RmCtrlMsg.u2DataLen = u2DataLen;

    if (ElpsQueEnqMsg (pMsg) == OSIX_FAILURE)
    {
        if (u1Event == RM_MESSAGE)
        {
            /* RM CRU Buffer Memory Release */
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            /* Call RM API to release memory of data of these events */
            ElpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        /* Memory for the Queue Message will be released inside the 
         * ElpsQueEnqMsg routine itself. 
         */
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC,
                   "ElpsRedRmCallBack: ElpsQueEnqMsg Failed !!!!\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHandleRmEvents
 *
 * DESCRIPTION      : This function process all the events and messages from 
 *                    RM module.
 *
 * INPUT            : pMsg - pointer to ELPS Queue message.
 *
 * OUTPUT           : None.
 * 
 * RETURNS          : None.
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHandleRmEvents (tElpsQMsg * pMsg)
{
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    tRmNodeInfo        *pData = NULL;
    UINT4               u4SeqNum = 0;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    switch (pMsg->unMsgParam.RmCtrlMsg.u1Event)
    {
        case GO_ACTIVE:

            ElpsRedHandleGoActive ();
            break;

        case GO_STANDBY:

            ElpsRedHandleGoStandby ();
            break;

        case RM_INIT_HW_AUDIT:

            if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {
                ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                           "ElpsRedHandleRmEvents: INIT_HW_AUDIT event reached"
                           " Initiating Hardware Audit.\r\n"));
                /* Hardware Audit for ELPS is performed here
                 */
                ElpsRedInitHardwareAudit ();
            }
            break;

        case RM_STANDBY_UP:

            ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                       "ElpsRedHandleRmEvents: RM_STANDBY_UP event reached"
                       " Updating Standby Nodes Count.\r\n"));

            pData = (tRmNodeInfo *) pMsg->unMsgParam.RmCtrlMsg.pData;

            if (pMsg->unMsgParam.RmCtrlMsg.u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                /* Data length is incorrect */
                ElpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            }
            gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp =
                pData->u1NumStandby;
            ElpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);

            /* Before the arrival of STANDBY_UP event, Bulk Request has 
             * arrived from Peer ELPS in the Standby node. Hence send the 
             * Bulk updates Now.
             */

            if (gElpsGlobalInfo.RedGlobalInfo.bBulkReqRecvd == OSIX_TRUE)
            {
                gElpsGlobalInfo.RedGlobalInfo.bBulkReqRecvd = OSIX_FALSE;
                ElpsRedSendDynamicBulkMsg ();
            }

            /* During Static Bulk update and dynamic bulk updates, active 
             * should not send Np Sync-ups. Np Sync-up is blocked when this 
             * event is processed, indicating the start of Static bulk update.
             * After the sending of Bulk update Tail message, the Np sync-up 
             * block count is reset to indicate the completion of bulk updates
             */
            ElpsRedHwAuditIncBlkCounter ();
            break;

        case RM_STANDBY_DOWN:

            ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                       "ElpsRedHandleRmEvents: RM_STANDBY_DOWN event reached"
                       " Updating Standby Nodes Count.\r\n"));

            pData = (tRmNodeInfo *) pMsg->unMsgParam.RmCtrlMsg.pData;

            if (pMsg->unMsgParam.RmCtrlMsg.u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                /* Data length is incorrect */
                ElpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;
            }

            gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp =
                pData->u1NumStandby;
            ElpsPortRmReleaseMemoryForMsg ((UINT1 *) pData);
            break;

        case RM_CONFIG_RESTORE_COMPLETE:

            if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_INIT)
            {
                if (ElpsPortRmGetNodeState () == RM_STANDBY)
                {
                    ElpsRedHandleIdleToStandby ();

                    ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                    if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) ==
                        OSIX_FAILURE)
                    {
                        ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                                   "ElpsRedHandleGoStandby: Acknowledgement "
                                   " to RM for GO_STANDBY event failed!!!!"
                                   "\r\n"));
                    }
                }
            }

            if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {
                /* CONFIG_RESTORE_COMPLETE event reached active node. Reset
                 * the Np sync block flag so that the Np Sync-ups are sent 
                 * on active node. By default it is blocked because during 
                 * MSR Restoration in the active node, Np sync-ups should
                 * not be sent.
                 */
                ElpsRedHwAuditDecBlkCounter ();
            }

            break;

        case L2_INITIATE_BULK_UPDATES:

            ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                       "ElpsRedHandleRmEvents: L2_INITIATE_BULK_UPDATES event"
                       " reached, Sending Bulk Request!!\r\n"));

            ElpsRedSendBulkReqMsg ();
            break;

        case RM_MESSAGE:

            /* Read the sequence number from the RM Header */
            RM_PKT_GET_SEQNUM (pMsg->unMsgParam.RmCtrlMsg.pData, &u4SeqNum);
            /* Remove the RM Header */
            RM_STRIP_OFF_RM_HDR (pMsg->unMsgParam.RmCtrlMsg.pData,
                                 pMsg->unMsgParam.RmCtrlMsg.u2DataLen);

            ProtoAck.u4AppId = RM_ELPS_APP_ID;
            ProtoAck.u4SeqNumber = u4SeqNum;

            if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
            {
                ElpsRedProcessPeerMsgAtActive (pMsg->unMsgParam.RmCtrlMsg.pData,
                                               pMsg->unMsgParam.RmCtrlMsg.
                                               u2DataLen);
            }
            else if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_STANDBY)
            {
                ElpsRedProcessPeerMsgAtStandby (pMsg->unMsgParam.RmCtrlMsg.
                                                pData,
                                                pMsg->unMsgParam.RmCtrlMsg.
                                                u2DataLen);
            }
            else
            {
                ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                           "ElpsRedHandleRmEvents: Sync-up message"
                           "received at Idle Node!!!!\r\n"));
            }

            RM_FREE (pMsg->unMsgParam.RmCtrlMsg.pData);

            /* Sending ACK to RM */
            ElpsPortRmApiSendProtoAckToRM (&ProtoAck);
            break;

        default:
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedHandleRmEvents: Invalid"
                       " RM event received\r\n"));
            break;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHandleGoActive 
 *
 * DESCRIPTION      : This routine handles the GO_ACTIVE event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsRedHandleGoActive: GO_ACTIVE event reached"
                   " when node is already active!!!!\r\n"));
        return;
    }

    if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsRedHandleGoActive: Idle to Active"
                   " transition...\r\n"));

        ElpsRedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsRedHandleGoActive: Standby to Active"
                   " transition...\r\n"));
        ElpsRedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }

    if (gElpsGlobalInfo.RedGlobalInfo.bBulkReqRecvd == OSIX_TRUE)
    {
        /* Before the arrival of GO_ACTIVE event, Bulk Request has 
         * arrived from Peer ELPS in the Standby node. Hence send the 
         * Bulk updates Now.
         */
        gElpsGlobalInfo.RedGlobalInfo.bBulkReqRecvd = OSIX_FALSE;
        ElpsRedSendDynamicBulkMsg ();
    }

    if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                   "ElpsRedHandleGoActive: Acknowledgement to RM for"
                   "GO_ACTIVE event failed!!!!\r\n"));
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHandleGoStandby 
 *
 * DESCRIPTION      : This routine handles the GO_STANDBY event and responds
 *                    to RM with an acknowledgement.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHandleGoStandby (VOID)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_STANDBY)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsRedHandleGoStandby: GO_STANDBY event reached"
                   " when node is already standby!!!!\r\n"));
        return;
    }
    /* Create the Hardware Audit Np sync Buffer table */
    ElpsRedHwAudCrtNpSyncBufferTable ();

    if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_INIT)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsRedHandleGoStandby: GO_STANDBY event received"
                   " when state is Idle.\r\n"));

        /* GO_STANDBY event is not processed here. It is done when 
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk 
         * update will be completed only by then, the acknowledgement can be 
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM 
         * to send dynamic bulk update event to modules.
         */
    }
    else if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsRedHandleGoStandby: Active to Standby"
                   " transition...\r\n"));
        ElpsRedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedHandleGoStandby: Acknowledgement to RM for"
                       "GO_STANDBY event failed!!!!\r\n"));
        }
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHandleStandbyToActive 
 *
 * DESCRIPTION      : On Standby to Active the following actions are performed
 *                    1. Update the Node Status and standby node count
 *                    2. Send a APS packet and Start the APS Tx timer for all
 *                       PG.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHandleStandbyToActive (VOID)
{
    tElpsContextInfo   *pCxt = NULL;
    tElpsPgInfo        *pPg = NULL;
    tElpsPgInfo        *pNextPg = NULL;
    UINT4               u4ContextId = 0;

    /* Update the Node Status and standby node count */
    gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp =
        ElpsPortRmGetStandbyNodeCount ();

    /* Send an APS packet and Start the APS Tx timer for all active 
     * protection groups in all contexts. 
     */

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pCxt = ElpsCxtGetNode (u4ContextId);

        if (pCxt == NULL)
        {
            /* Context Not Created */
            continue;
        }

        if ((ElpsUtilIsElpsStarted (u4ContextId) == OSIX_FALSE) ||
            (pCxt->u1ModuleStatus == ELPS_DISABLED))
        {
            continue;
        }

        pNextPg = ElpsPgGetFirstNodeInContext (pCxt);

        if (pNextPg == NULL)
        {
            continue;
        }

        do
        {
            pPg = pNextPg;

            if (pPg->u1PgRowStatus != ACTIVE)
            {
                continue;
            }

            /* Invoke APS pdu transmission on state change if protection type 
             * supports APS channel communication. */
            if (ELPS_GET_PG_A_BIT_VALUE (pPg->ApsInfo.u1ProtectionType))
            {

                ElpsTxFormAndSendAPSPdu (pPg);

                if (ElpsTmrStartTimer (pPg, ELPS_PERIODIC_APS_PDU_INTERVAL,
                                       ELPS_APS_PERIODIC_TMR) == OSIX_FAILURE)
                {
                    ELPS_TRC ((pPg->pContextInfo,
                               OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                               "ElpsRedHandleStandbyToActive: Timer Start FAILED"
                               " !!!\r\n"));
                }
            }
            /* Reset the Counters at the active node */
            MEMSET (&(pPg->Stats), 0, sizeof (tElpsStats));
        }
        while ((pNextPg = ElpsPgGetNextNodeInContext (pCxt, pPg)) != NULL);
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHandleActiveToStandby 
 *
 * DESCRIPTION      : On Active to Standby transition, the following actions 
 *                    are performed,
 *                    1. Update the Node Status
 *                    2. Disable and enable the module
 *                    3. Delete the Hardware audit task, if it is running.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHandleActiveToStandby (VOID)
{
    tElpsContextInfo   *pCxt = NULL;
    UINT4               u4ContextId = 0;

    gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp = 0;

    /*
     * Stop auditing if it is running. This condition should not arise.
     * Ideally RM should prevent this frequent switchover between active
     * to standby.
     */
    if (gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId != 0)
    {
        OsixTskDel (gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId);
        gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId = 0;
    }
    /* During Force-switchover, the Np Sync-ups has to be blocked here, so 
     * that dynamic bulk updates does not access the Np Sync Buffer table 
     */
    ElpsRedHwAuditIncBlkCounter ();

    /* Disable and Enable the module for the contexts which are 
     * enabled 
     */
    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pCxt = ElpsCxtGetNode (u4ContextId);

        if (pCxt == NULL)
        {
            /* Context Not Created */
            continue;
        }

        if ((ElpsUtilIsElpsStarted (u4ContextId) == OSIX_FALSE) ||
            (pCxt->u1ModuleStatus == ELPS_DISABLED))
        {
            continue;
        }

        ElpsCxtDisable (pCxt);
        ELPS_TRC ((pCxt, CONTROL_PLANE_TRC,
                   "ElpsRedHandleActiveToStandby: Module Disabled"
                   " to stop all the timers...\r\n"));
        ElpsCxtEnable (pCxt);
        ELPS_TRC ((pCxt, CONTROL_PLANE_TRC,
                   "ElpsRedHandleActiveToStandby: Module Enabled"
                   " again...\r\n"));
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHandleIdleToStandby 
 *
 * DESCRIPTION      : This routine updates the node status.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHandleIdleToStandby (VOID)
{
    gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_STANDBY;
    gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp = 0;

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHandleIdleToActive 
 *
 * DESCRIPTION      : This routine updates the node status on transition from
 *                    Idle to Active.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHandleIdleToActive (VOID)
{
    gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus = RM_ACTIVE;
    gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp =
        ElpsPortRmGetStandbyNodeCount ();

    /* Np Sync-ups must be sent from Active node, hence the unblocking 
     * by decrementing the counter. It was blocked on Boot-up.
     */
    ElpsRedHwAuditDecBlkCounter ();

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedProcessPeerMsgAtActive
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Standby) at Active. The messages that are handled in
 *                    Active node are
 *                    1. RM_BULK_UPDT_REQ_MSG
 *                    2. ELPS_HR_STDY_ST_PKT_REQ
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;

    ELPS_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);
    ELPS_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

    if (u2OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at 
         * active node is Bulk Request message which has only Type and 
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedProcessPeerMsgAtActive: Indication of error"
                       " to RM to process Bulk request" " failed!!!!\r\n"));
        }

        return;
    }

    if (u2Length != ELPS_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedProcessPeerMsgAtActive: Indication of error"
                       " to RM to process Bulk request" " failed!!!!\r\n"));
        }

        return;
    }

    if (u1MsgType == ELPS_RED_BULK_REQ_MESSAGE)
    {
        if (gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp == 0)
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gElpsGlobalInfo.RedGlobalInfo.bBulkReqRecvd = OSIX_TRUE;
            return;
        }

        ElpsRedSendDynamicBulkMsg ();
    }

    /* HITLESS RESTART */
    if (u1MsgType == ELPS_HR_STDY_ST_PKT_REQ)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS: Received Steady State "
                   "Pkt Request msg. \n"));
        ElpsRedHRProcStdyStPktReq ();
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedProcessPeerMsgAtStandby
 *
 * DESCRIPTION      : This routine handles messages from the other node
 *                    (Active) at standby. The messages that are handled in
 *                    Standby node are
 *                    1. RM_BULK_UPDT_TAIL_MSG
 *                    2. Dynamic sync-up messages
 *                    3. Dynamic bulk messages
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UINT2               u2OffSet = 0;
    UINT2               u2Length = 0;
    UINT2               u2RemMsgLen = 0;
    UINT2               u2MinLen = 0;
    UINT1               u1MsgType = 0;

    u2MinLen = ELPS_RED_TYPE_FIELD_SIZE + ELPS_RED_LEN_FIELD_SIZE;

    while ((u2OffSet + u2MinLen) <= u2DataLen)
    {
        ELPS_RM_GET_1_BYTE (pMsg, &u2OffSet, u1MsgType);

        ELPS_RM_GET_2_BYTE (pMsg, &u2OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum 
             * number of bytes, which is MessageType + Length. 
             */
            u2OffSet += u2Length;
            continue;
        }

        u2RemMsgLen = (UINT2) (u2Length - u2MinLen);

        if ((u2OffSet + u2RemMsgLen) > u2DataLen)
        {
            /* The Length field in the RM packet is wrong, hence continuing 
             * with the next packet */
            u2OffSet = u2DataLen;
            continue;
        }

        switch (u1MsgType)
        {
            case ELPS_RED_BULK_UPD_TAIL_MESSAGE:

                u2OffSet = 0;
                ElpsRedProcessBulkTailMsg (pMsg, &u2OffSet);
                break;

            case ELPS_RED_DYN_SEM_INFO_MSG:

                ElpsRedProcessDynSyncSemInfoMsg (pMsg, &u2OffSet, u2RemMsgLen);
                break;

            case ELPS_RED_DYN_REM_TIME_INFO_MSG:

                ElpsRedProcessDynSyncRemTimeMsg (pMsg, &u2OffSet, u2RemMsgLen);
                break;

            case ELPS_RED_NP_SYNC_INFO_MSG:

                ElpsHwAdProcessNpSyncMsg (pMsg, &u2OffSet);
                break;

            default:
                u2OffSet += u2Length;    /* Skip the attribute */
                break;
        }
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedProcessBulkTailMsg 
 *
 * DESCRIPTION      : This routine process the bulk update tail message and 
 *                    send bulk updates.
 *
 * INPUT            : pMsg - RM Data buffer holding messages
 *                    u2DataLen - Length of data in buffer.
 *
 * OUTPUT           : pu2OffSet - Offset Value
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedProcessBulkTailMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    tRmProtoEvt         ProtoEvt;
    UINT2               u2Length = 0;
    UINT1               u1MsgType = 0;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;

    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, u1MsgType);
    ELPS_RM_GET_2_BYTE (pMsg, pu2OffSet, u2Length);

    if (u2Length != ELPS_RED_BULK_UPD_TAIL_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedProcessBulkTailMsg: Indication of error"
                       " to RM to process Bulk update tail message"
                       " failed!!!!\r\n"));
        }

        return;
    }

    if (u1MsgType == ELPS_RED_BULK_UPD_TAIL_MESSAGE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ElpsRedProcessBulkTailMsg: Bulk Update Tail Message"
                   " received at Standby node...\r\n"));

        /* Since the Dynamic bulk update is successfully completed, the 
         * NpSync Block Count can be reset at standby, so that the NPSYNC
         * Buffer Table is accessed from now on. 
         */
        ElpsRedHwAuditDecBlkCounter ();

        ProtoEvt.u4Error = RM_NONE;
        ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedProcessBulkTailMsg: Indication of error"
                       " to RM to process Bulk update completion"
                       " failed!!!!\r\n"));
        }
    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedRelinquishSystemTime 
 *
 * DESCRIPTION      : This routine looks for the pending events and processes
 *                    them. After processing the event the control returns to
 *                    the caller of this routine. By calling this routine, the 
 *                    calling function relinquishes the system time voluntarily
 *
 * INPUT            : None 
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedRelinquishSystemTime (VOID)
{
    UINT4               u4Events = 0;

    if (OsixEvtRecv (gElpsGlobalInfo.MainTaskId, ELPS_ALL_EVENTS,
                     OSIX_NO_WAIT, &u4Events) == OSIX_SUCCESS)
    {
        ElpsMainProcessEvent (u4Events);
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedSendDynamicBulkMsg 
 *
 * DESCRIPTION      : This function sends the Bulk update messages to the
 *                    peer standby ELPS. After sending bulk messages, sends the
 *                    bulk update tail message and sets the bulk update 
 *                    complete status at active RM.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedSendDynamicBulkMsg (VOID)
{
    if ((gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE) ||
        (gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp == 0))
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                   "ElpsRedRmCallBack: ELPS task is not initialized or"
                   " No Standby node available\r\n"));

        ElpsPortRmSetBulkUpdatesStatus (RM_ELPS_APP_ID);
        /* Send the tail msg to indicate the completion of Bulk
         * update process.
         */
        ElpsRedSendBulkUpdTailMsg ();
        return;
    }

    ElpsRedSendPgDynamicBulkMsg ();

    ElpsPortRmSetBulkUpdatesStatus (RM_ELPS_APP_ID);
    /* Send the tail msg to indicate the completion of Bulk
     * update process.
     */
    ElpsRedSendBulkUpdTailMsg ();

    /* The Bulk updates are completed for active node as it has sent the 
     * bulk update tail message. So the Np sync-up block can be reset. This 
     * was blocked during RM_STANDBY_UP Event
     */
    ElpsRedHwAuditDecBlkCounter ();
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedSendPgDynamicBulkMsg 
 *
 * DESCRIPTION      : This routine perform the Dynamic Bulk updates of 
 *                    protection groups. The mechanism employed is CPU 
 *                    Relinquish Method.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedSendPgDynamicBulkMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    tElpsPgInfo        *pNextPg = NULL;
    tOsixSysTime        StartSysTime = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4HoldwRemTime = 0;
    UINT4               u4HoldpRemTime = 0;
    UINT4               u4WtrRemTime = 0;
    UINT4               u4PgMsgLen = 0;
    UINT4               u4BulkUnitSize = 0;
    UINT2               u2Offset = 0;
    UINT1               u1IsTmrRunning = OSIX_FALSE;
    UINT1               u1TimerType = 0;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Initial System time is recorded here. The StartSysTime will be in 
     * STUPS. The threshold value will also be in same units 
     */
    OsixGetSysTime (&StartSysTime);

    while ((pNextPg = ElpsPgGetNextNode (u4ContextId, u4PgId,
                                         &u4NextContextId,
                                         &u4NextPgId)) != NULL)
    {
        u4ContextId = u4NextContextId;
        u4PgId = u4NextPgId;

        u4HoldwRemTime = 0;
        u4HoldpRemTime = 0;
        u4WtrRemTime = 0;
        u1TimerType = 0;

        /* Bulk updates to be done for protection groups that are disabled
         * also. 
         */
        u4PgMsgLen = ELPS_RED_TYPE_FIELD_SIZE + ELPS_RED_LEN_FIELD_SIZE +
            ELPS_RED_DYN_SEM_INFO_VALUE_SIZE;

        u1IsTmrRunning = ElpsUtilIsHoldOffOrWtrRunning (pNextPg,
                                                        &u1TimerType,
                                                        &u4HoldwRemTime,
                                                        &u4HoldpRemTime,
                                                        &u4WtrRemTime);
        if (u1IsTmrRunning == OSIX_TRUE)
        {
            if (u1TimerType & ELPS_TMR_HOLD_W)
            {
                u4PgMsgLen = u4PgMsgLen + ELPS_RED_TYPE_FIELD_SIZE +
                    ELPS_RED_LEN_FIELD_SIZE +
                    ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE;
            }

            if (u1TimerType & ELPS_TMR_HOLD_P)
            {
                u4PgMsgLen = u4PgMsgLen + ELPS_RED_TYPE_FIELD_SIZE +
                    ELPS_RED_LEN_FIELD_SIZE +
                    ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE;
            }

            if (u1TimerType & ELPS_TMR_WTR)
            {
                u4PgMsgLen = u4PgMsgLen + ELPS_RED_TYPE_FIELD_SIZE +
                    ELPS_RED_LEN_FIELD_SIZE +
                    ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE;
            }
        }

        if (pMsg == NULL)
        {
            pMsg = ElpsRedGetMsgBuffer (ELPS_RED_MAX_MSG_SIZE);

            if (pMsg == NULL)
            {
                return;
            }
        }

        if ((UINT4) (ELPS_RED_MAX_MSG_SIZE - u2Offset) < u4PgMsgLen)
        {
            /* Send the Message as no room available for a full 
             * protection group information */
            /* This routine sends the message to RM and in case of failure 
             * releases the RM buffer memory
             */

            if (ElpsRedSendMsgToRm (pMsg, u2Offset) != OSIX_SUCCESS)
            {
                ProtoEvt.u4Error = RM_SENDTO_FAIL;
                if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) ==
                    OSIX_FAILURE)
                {
                    ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                               "ElpsRedSendPgDynamicBulkMsg: ACK to RM -"
                               " Send to RM failed!!!!\r\n"));
                }

                /* Continue with the loop, as the send to RM will be tried 
                 * again.
                 */
                continue;
            }

            /* Now Allocate Memory for more protection groups */
            pMsg = ElpsRedGetMsgBuffer (ELPS_RED_MAX_MSG_SIZE);

            if (pMsg == NULL)
            {
                return;
            }

            u2Offset = 0;
        }

        ElpsRedFormSemInfoSyncMsg (pNextPg, pMsg, &u2Offset, OSIX_TRUE);

        if (u1IsTmrRunning == OSIX_TRUE)
        {
            /* Sync the Hold-Off and Wtr remaining time */
            if (u1TimerType & ELPS_TMR_HOLD_W)
            {
                ElpsRedFormRemTimeSyncMsg (pNextPg, pMsg, &u2Offset,
                                           ELPS_HOLD_OFF_TMR, u4HoldwRemTime);
            }

            if (u1TimerType & ELPS_TMR_HOLD_P)
            {
                ElpsRedFormRemTimeSyncMsg (pNextPg, pMsg, &u2Offset,
                                           ELPS_HOLD_OFF_TMR_P, u4HoldpRemTime);
            }

            if (u1TimerType & ELPS_TMR_WTR)
            {
                ElpsRedFormRemTimeSyncMsg (pNextPg, pMsg, &u2Offset,
                                           ELPS_WTR_TMR, u4WtrRemTime);
            }
        }

        /* System Time Relinquish Method is applied here. It is applied before
         * getting an entry(Context Entry or Protection Group entry). This is 
         * because, when this process relinquishes system time for other 
         * process, the entry may be deleted or modified.
         */

        if (ElpsRedApplySysTimeRelinquish (&StartSysTime, &pMsg,
                                           &u2Offset) == OSIX_FAILURE)
        {
            /* If Applying CPU Relinquish fails, there can only be two reasons
             * 1. Send the Packet to RM fails. In this case we cannot continue
             *    with Dynamic bulk updates, hence we return and send the bulk
             *    update completion to active RM.
             * 2. Number of Peer Standby Nodes in the system is 0. This can 
             *    happen if an RM Message of RM_STANDBY_DOWN is received when 
             *    applying System time relinquish (Process of pending events).
             *    In this case also we can return from here and send the bulk 
             *    update completion to active RM.
             */
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedSendPgDynamicBulkMsg: System Time Relinquish has"
                       " failed, hence aborting dynamic bulk updates\r\n"));
            return;
        }
    }
    if ((u1MemEstFlag == 1) && (ELPS_HR_STATUS () != ELPS_HR_STATUS_DISABLE))

    {

        u4BulkUnitSize = ELPS_RED_DYN_SEM_INFO_VALUE_SIZE - 2;
        IssSzUpdateSizingInfoForHR ((CHR1 *) "ELPS", (CHR1 *) "tElpsPgInfo",
                                    u4BulkUnitSize);
        u4BulkUnitSize = ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE;
        IssSzUpdateSizingInfoForHR ((CHR1 *) "ELPS",
                                    (CHR1 *) "tElpsPgInfo", u4BulkUnitSize);
        u4BulkUnitSize = ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE;
        IssSzUpdateSizingInfoForHR ((CHR1 *) "ELPS",
                                    (CHR1 *) "tElpsPgInfo", u4BulkUnitSize);
        u4BulkUnitSize = ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE;
        IssSzUpdateSizingInfoForHR ((CHR1 *) "ELPS",
                                    (CHR1 *) "tElpsPgInfo", u4BulkUnitSize);
        u1MemEstFlag = 0;
    }
    if ((u2Offset != 0) &&
        (ElpsRedSendMsgToRm (pMsg, u2Offset) != OSIX_SUCCESS))
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedSendPgDynamicBulkMsg: ACK to RM -"
                       " Send to RM failed!!!!\r\n"));
        }
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedGetMsgBuffer 
 *
 * DESCRIPTION      : This routine returns CRU buffer memory from RM. In case 
 *                    of failure, it sends indication to RM about it.
 *
 * INPUT            : u2BufSize - Buffer Size.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : pMsg - Allocated Tx Buffer
 * 
 **************************************************************************/
PUBLIC tRmMsg      *
ElpsRedGetMsgBuffer (UINT2 u2BufSize)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;

    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;

        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedGetMsgBuffer: ACK to RM - "
                       "Sync-up Tx"
                       " buffer Memory allocation failed!!!!\r\n"));
        }
    }
    return pMsg;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedApplySysTimeRelinquish 
 *
 * DESCRIPTION      : This routine checks if the caller process uses the system
 *                    time more than the allocated threshold. If so, then the 
 *                    control is taken out of caller process to let other 
 *                    threads process.
 *
 * INPUT            : StartSysTime - Start System Time of the caller process
 *                    pMsg - RM Sync-up message buffer
 *                    u2Offset - OffSet Value
 *
 * OUTPUT           : StartSysTime - Start System Time of the caller process
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC INT4
ElpsRedApplySysTimeRelinquish (tOsixSysTime * pStartSysTime, tRmMsg ** ppMsg,
                               UINT2 *pu2Offset)
{
    tRmProtoEvt         ProtoEvt;
    tOsixSysTime        EndSysTime = 0;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    OsixGetSysTime (&EndSysTime);

    if ((EndSysTime - *pStartSysTime) >= ELPS_RED_DYN_BULK_THRESHOLD_SYS_TIME)
    {
        /* Relinquish System Time here, as the threshold is reached for
         * Caller process. Send the packet here. It need not be full.
         */
        if (ElpsRedSendMsgToRm (*ppMsg, *pu2Offset) != OSIX_SUCCESS)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
            {
                ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                           "ElpsRedApplySysTimeRelinquish: ACK to RM -"
                           " Send to RM failed!!!!\r\n"));
            }

            return OSIX_FAILURE;
        }

        *ppMsg = NULL;
        *pu2Offset = 0;

        /* Release the Lock here. */

        ELPS_UNLOCK ();

        ElpsRedRelinquishSystemTime ();

        /* Reset the Start system time and Get the Start System time 
         * again. Take Lock before continuing.
         */
        ELPS_LOCK ();

        if (gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp == 0)
        {
            return OSIX_FAILURE;
        }

        *pStartSysTime = 0;
        OsixGetSysTime (pStartSysTime);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedSendBulkUpdTailMsg 
 *
 * DESCRIPTION      : This function will send the bulk update tail msg to the
 *                    standby node, which indicates the completion of Bulk
 *                    update process.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT2               u2OffSet = 0;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><--2 Byte-->
     ***************************************************
     *        *                            *           *
     * RM Hdr * ELPS_RED_BULK_UPD_TAIL_MSG *Msg Length *
     *        *                            *           *
     ***************************************************

     * The RM Hdr shall be included by RM.
     */

    pMsg = ElpsRedGetMsgBuffer (ELPS_RED_BULK_UPD_TAIL_MSG_SIZE);

    if (pMsg == NULL)
    {
        return;
    }

    u2OffSet = 0;

    ELPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, ELPS_RED_BULK_UPD_TAIL_MESSAGE);
    ELPS_RM_PUT_2_BYTE (pMsg, &u2OffSet, ELPS_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure 
     * releases the RM buffer memory
     */
    if (ElpsRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedSendPgDynamicBulkMsg: ACK to RM -"
                       " Send to RM failed!!!!\r\n"));
        }

        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedSendBulkReqMsg 
 *
 * DESCRIPTION      : This routine sends the bulk request message from standby
 *                    to active node to initiate bulk update process.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus != RM_STANDBY)
    {
        /* Node not in Standby State, so dont send 
         * Bulk request message
         */
        return;
    }

    /*
     *    ELPS Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |     (1)   |   3        |
     *    |-------------------------
     *
     *
     */
    pMsg = ElpsRedGetMsgBuffer (ELPS_RED_BULK_REQ_MSG_SIZE);

    if (pMsg == NULL)
    {
        return;
    }

    u2OffSet = 0;

    ELPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, ELPS_RED_BULK_REQ_MESSAGE);

    ELPS_RM_PUT_2_BYTE (pMsg, &u2OffSet, ELPS_RED_BULK_REQ_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure 
     * releases the RM buffer memory
     */
    if (ElpsRedSendMsgToRm (pMsg, u2OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedSendPgDynamicBulkMsg: ACK to RM -"
                       " Send to RM failed!!!!\r\n"));
        }

        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedSendDynamicSyncMsg 
 *
 * DESCRIPTION      : This function forms and sends the dynamic sync-up message
 *                    to Active RM. The sync-up messages are
 *                    1. SEM Info sync-up
 *                    2. Hold-Off Timer start sync-up
 *                    3. Hold-Off Timer stop sync-up
 *                    4. Hold-Off Timer expiry sync-up
 *                    5. WTR Timer start sync-up
 *                    6. WTR Timer stop sync-up
 *                    7. WTR Timer expiry sync-up
 *
 * INPUT            : pPg - pointer to PG Config Table entry for which sync-up
 *                          to be sent.
 *                    u1TmrType - Hold-Off/WTR Timer
 *                    u1TmrEvent - Start/Stop/Expiry
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedSendDynamicSyncMsg (tElpsPgInfo * pPg, UINT1 u1TmrType, UINT1 u1TmrEvent)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2Offset = 0;
    UINT2               u2MsgLen = 0;

    if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus != RM_ACTIVE)
    {
        /* Only Active node sends dynamic sync-up message */
        return;
    }

    if (gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp == 0)
    {
        /* No Standby nodes are up, hence no need to send dynamic 
         * sync-up message */
        return;
    }

    /* SEM Information Dynamic Update format 
     * 
     * <- 1 byte ->|<- 2 bytes->|<- n bytes  -> |
     * ------------------------------------------
     * | Msg. Type |   Length    | SEM Info     |
     * |     (1)   |1 + 2 +      | 17 bytes     |
     * |           |SEM Info     |              |
     * |-----------------------------------------
     *
     * The RM Header will be appended by RM.
     *
     */

    u2MsgLen = ELPS_RED_TYPE_FIELD_SIZE + ELPS_RED_LEN_FIELD_SIZE +
        ELPS_RED_DYN_SEM_INFO_VALUE_SIZE;

    pMsg = ElpsRedGetMsgBuffer (u2MsgLen);

    if (pMsg == NULL)
    {
        return;
    }

    u2Offset = 0;

    ElpsRedFormSemInfoSyncMsg (pPg, pMsg, &u2Offset, OSIX_FALSE);

    if (pPg->ApsInfo.u1ApsSemStateId == ELPS_SEM_STATE_WTR)
    {
        u1TmrType = ELPS_WTR_TMR;
    }

    /* The Timer Type indicates the timer to be acted upon in the standby. 
     * A valid value in timer type (Hold-Off or WTR) is filled by dynamic 
     * updates only. Invalid value is filled in case of dynamic bulk message.
     * This differentiation is done in order to start the timer for configured
     * interval (in case of dynamic updates) or from the remaining time (bulk)
     */
    ELPS_RM_PUT_1_BYTE (pMsg, &u2Offset, u1TmrType);
    ELPS_RM_PUT_1_BYTE (pMsg, &u2Offset, u1TmrEvent);

    if (ElpsRedSendMsgToRm (pMsg, u2Offset) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedSendPgDynamicBulkMsg: ACK to RM -"
                       " Send to RM failed!!!!\r\n"));
        }

        return;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedFormSemInfoSyncMsg 
 *
 * DESCRIPTION      : This routine forms the RM Sync-up packet with the SEM
 *                    information of the protection group and sends it to the
 *                    RM.
 *
 * INPUT            : pPg - Protection group  information
 *                    pMsg - RM Sync-up message
 *                    pu2OffSet - Offset value
 *                    u1IsBulkPacket - Is the packet framed for bulk update or
 *                    not (OSIX_TRUE/OSIX_FALSE)
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedFormSemInfoSyncMsg (tElpsPgInfo * pPg, tRmMsg * pMsg, UINT2 *pu2OffSet,
                           UINT1 u1IsBulkPacket)
{
    UINT2               u2PgMsgLen = 0;

    u2PgMsgLen = ELPS_RED_TYPE_FIELD_SIZE + ELPS_RED_LEN_FIELD_SIZE +
        ELPS_RED_DYN_SEM_INFO_VALUE_SIZE;

    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, ELPS_RED_DYN_SEM_INFO_MSG);
    ELPS_RM_PUT_2_BYTE (pMsg, pu2OffSet, u2PgMsgLen);
    ELPS_RM_PUT_4_BYTE (pMsg, pu2OffSet, pPg->pContextInfo->u4ContextId);
    ELPS_RM_PUT_4_BYTE (pMsg, pu2OffSet, pPg->u4PgId);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->ApsInfo.u1ApsSemStateId);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->LastLocalCond.u1Request);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->LastLocalCond.u1RequestStatus);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->LastFarEndReq.u1Request);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->LastFarEndReq.u1RequestStatus);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->u1LastRecvdReqType);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->u1LastActiveReq);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->u1WorkingCfmStatus);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->u1ProtectionCfmStatus);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->u1PgStatus);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, pPg->u1PreviousStateId);

    if (u1IsBulkPacket == OSIX_TRUE)
    {
        ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, ELPS_MAX_TMR_TYPES);
        ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, 0);
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedFormRemTimeSyncMsg 
 *
 * DESCRIPTION      : This routine forms the RM sync-up message with Remaining
 *                    time of the timer which is running for a pg. This is 
 *                    called during dynamic bulk update only.
 *
 * INPUT            : pPg - Protection Group Information
 *                    pMsg - RM Packet buffer
 *                    pu2OffSet - OffSet value
 *                    u1TimerType - Type of Timer (Hold-Off or WTR timer)
 *                    u4RemTime - Remaining time of the timer
 *
 * OUTPUT           : pu2OffSet - OffSet value
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedFormRemTimeSyncMsg (tElpsPgInfo * pPg, tRmMsg * pMsg, UINT2 *pu2OffSet,
                           UINT1 u1TimerType, UINT4 u4RemTime)
{
    UINT2               u2PgRemTimeMsgLen = 0;

    u2PgRemTimeMsgLen = ELPS_RED_TYPE_FIELD_SIZE + ELPS_RED_LEN_FIELD_SIZE +
        ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE;

    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, ELPS_RED_DYN_REM_TIME_INFO_MSG);
    ELPS_RM_PUT_2_BYTE (pMsg, pu2OffSet, u2PgRemTimeMsgLen);
    ELPS_RM_PUT_4_BYTE (pMsg, pu2OffSet, pPg->pContextInfo->u4ContextId);
    ELPS_RM_PUT_4_BYTE (pMsg, pu2OffSet, pPg->u4PgId);
    ELPS_RM_PUT_1_BYTE (pMsg, pu2OffSet, u1TimerType);
    ELPS_RM_PUT_4_BYTE (pMsg, pu2OffSet, u4RemTime);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedProcessDynSyncSemInfoMsg 
 *
 * DESCRIPTION      : This routine decodes the sync-up message from peer active
 *                    ELPS and updates the SEM information in the standby ELPS.
 *                    The timers are also started/stopped at the standby node.
 *
 * INPUT            : pMsg - RM Sync-up message
 *                    u2RemMsgLen - Length of value of Message
 *                    
 * OUTPUT           : u2OffSet - Offset value 
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedProcessDynSyncSemInfoMsg (tRmMsg * pMsg, UINT2 *pu2OffSet,
                                 UINT2 u2RemMsgLen)
{
    tRmProtoEvt         ProtoEvt;
    tElpsContextInfo   *pCxt = NULL;
    tElpsPgInfo        *pPg = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4PgId = 0;
    UINT1               u1SemStateId = 0;
    UINT1               u1OldPgStatus = 0;
    UINT1               u1PgStatus = 0;
    UINT1               u1TmrType = ELPS_MAX_TMR_TYPES;
    UINT1               u1TmrEvent = 0;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != ELPS_RED_DYN_SEM_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedProcessDynSyncSemInfoMsg: Indication of error"
                       "to RM to process Dynamic update" " failed!!!!\r\n"));
        }

        *pu2OffSet += u2RemMsgLen;
        return;
    }

    ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4ContextId);
    ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4PgId);

    pCxt = ElpsCxtGetNode (u4ContextId);

    if (pCxt == NULL)
    {
        /* Context Not Created */
        return;
    }

    pPg = ElpsPgGetNode (pCxt, u4PgId);

    if (pPg == NULL)
    {
        /* Protection Group Not created */
        return;
    }

    u1OldPgStatus = pPg->u1PgStatus;

    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, u1SemStateId);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->LastLocalCond.u1Request);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->LastLocalCond.u1RequestStatus);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->LastFarEndReq.u1Request);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->LastFarEndReq.u1RequestStatus);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->u1LastRecvdReqType);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->u1LastActiveReq);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->u1WorkingCfmStatus);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->u1ProtectionCfmStatus);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, u1PgStatus);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, pPg->u1PreviousStateId);

    pPg->u1PgStatus = u1PgStatus;

    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, u1TmrType);
    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, u1TmrEvent);

    /* Wait To Restore Timer is started or stopped in standby based on the 
     * the previous and current SEM State.
     */
    if (pPg->ApsInfo.u1ApsSemStateId != u1SemStateId)
    {
        if ((u1SemStateId == ELPS_SEM_STATE_WTR) && (u1TmrType == ELPS_WTR_TMR))
        {
            /* If the current state is WTR and if the timer type is mentioned 
             * as WTR then the WTR timer is started in the standby node. The 
             * timer type is set to WTR in dynamic message only. The bulk 
             * update message sets this type to invalid value always.
             */
            if (ElpsTmrStartTimer (pPg, pPg->u2WtrTime,
                                   ELPS_WTR_TMR) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncSemInfoMsg: "
                           "Timer Start FAILED !!!\r\n"));
            }
        }

        if (pPg->ApsInfo.u1ApsSemStateId == ELPS_SEM_STATE_WTR)
        {
            /* If the previous state is WTR, then the WTR timer is stopped. 
             * During dynamic bulk updates, the previous state can never be 
             * WTR state as it is caused only by a local or far end requests.
             */
            if (ElpsTmrStopTimer (pPg, ELPS_WTR_TMR) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncSemInfoMsg: "
                           "Timer Stop FAILED !!!\r\n"));
            }
        }
    }

    /* Update the SEM State Id */
    pPg->ApsInfo.u1ApsSemStateId = u1SemStateId;

    if (u1TmrType == ELPS_HOLD_OFF_TMR)
    {
        if (u1TmrEvent == ELPS_TMR_START)
        {
            if (ElpsTmrStartTimer (pPg, pPg->u2HoldOffTime,
                                   ELPS_HOLD_OFF_TMR) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncSemInfoMsg: "
                           "Timer Start FAILED !!!\r\n"));
            }
        }
        else
        {
            if (ElpsTmrStopTimer (pPg, ELPS_HOLD_OFF_TMR) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncSemInfoMsg: "
                           "Timer Stop FAILED !!!\r\n"));
            }
        }
    }

    if (u1TmrType == ELPS_HOLD_OFF_TMR_P)
    {
        if (u1TmrEvent == ELPS_TMR_START)
        {
            if (ElpsTmrStartTimer (pPg, pPg->u2HoldOffTime,
                                   ELPS_HOLD_OFF_TMR_P) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncSemInfoMsg: "
                           "Timer Start FAILED !!!\r\n"));
            }
        }
        else
        {
            if (ElpsTmrStopTimer (pPg, ELPS_HOLD_OFF_TMR_P) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncSemInfoMsg: "
                           "Timer Stop FAILED !!!\r\n"));
            }
        }
    }
    /* Access the Np sync-buffer table to remove the entry created by 
     * Np Sync-ups. This routine will identify the NP Call and correspodingly 
     * access the buffer 
     */
    ElpsRedHwAuditFindAndUpdNpSync (pPg, u1OldPgStatus);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedProcessDynSyncRemTimeMsg 
 *
 * DESCRIPTION      : This routine handles the Remaining Time sync-up of the 
 *                    protection group. To start the timer with the remaining
 *                    time, this sync-up is used. Either Hold-Off or WTR timer
 *                    is run.
 *
 * INPUT            : pMsg - Data Buffer
 *                    u2RemMsgLen - Value field of remaining message
 *
 * OUTPUT           : pu2OffSet - OffSet value
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedProcessDynSyncRemTimeMsg (tRmMsg * pMsg, UINT2 *pu2OffSet,
                                 UINT2 u2RemMsgLen)
{
    tRmProtoEvt         ProtoEvt;
    tElpsContextInfo   *pCxt = NULL;
    tElpsPgInfo        *pPg = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4RemTime = 0;
    UINT1               u1TmrType = ELPS_MAX_TMR_TYPES;

    ProtoEvt.u4AppId = RM_ELPS_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    if (u2RemMsgLen != ELPS_RED_DYN_REM_TIME_INFO_VALUE_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        if (ElpsPortRmApiHandleProtocolEvent (&ProtoEvt) == OSIX_FAILURE)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedProcessDynSyncRemTimeMsg: Indication of error"
                       "to RM to process Dynamic update" " failed!!!!\r\n"));
        }
        *pu2OffSet += u2RemMsgLen;
        return;
    }

    ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4ContextId);
    ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4PgId);

    pCxt = ElpsCxtGetNode (u4ContextId);

    if (pCxt == NULL)
    {
        /* Context Not Created */
        return;
    }

    pPg = ElpsPgGetNode (pCxt, u4PgId);

    if (pPg == NULL)
    {
        /* Protection Group Not created */
        return;
    }

    ELPS_RM_GET_1_BYTE (pMsg, pu2OffSet, u1TmrType);
    ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet, u4RemTime);

    switch (u1TmrType)
    {
        case ELPS_WTR_TMR:

            /* WTR Timer Started for the remaining time at standby. This 
             * info is from the dynamic bulk message from active node. 
             */

            /* The timer Id is set here, as it is needed to identify the 
             * timer during expiry.
             */
            pPg->WTRTimer.u1TimerId = ELPS_WTR_TMR;
            if (TmrStartTimer (gElpsGlobalInfo.TmrListId,
                               &(pPg->WTRTimer.TimerNode),
                               u4RemTime) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncRemTimeMsg: "
                           "Timer Start FAILED !!!\r\n"));
            }

            break;

        case ELPS_HOLD_OFF_TMR:

            /* Hold-Off Timer Started for the remaining time at standby. This 
             * info is from the dynamic bulk message from active node. 
             */
            /* The timer Id is set here, as it is needed to identify the 
             * timer during expiry.
             */
            pPg->HoldOffTimer.u1TimerId = ELPS_HOLD_OFF_TMR;
            if (TmrStartTimer (gElpsGlobalInfo.TmrListId,
                               &(pPg->HoldOffTimer.TimerNode),
                               u4RemTime) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncRemTimeMsg: "
                           "Timer Start FAILED !!!\r\n"));
            }

            break;

        case ELPS_HOLD_OFF_TMR_P:

            /* Hold-Off Timer Started for the remaining time at standby. This 
             * info is from the dynamic bulk message from active node. 
             */
            /* The timer Id is set here, as it is needed to identify the 
             * timer during expiry.
             */
            pPg->ProtectionHoldOffTimer.u1TimerId = ELPS_HOLD_OFF_TMR_P;
            if (TmrStartTimer (gElpsGlobalInfo.TmrListId,
                               &(pPg->ProtectionHoldOffTimer.TimerNode),
                               u4RemTime) == OSIX_FAILURE)
            {
                ELPS_TRC ((pPg->pContextInfo, OS_RESOURCE_TRC | ALL_FAILURE_TRC,
                           "ElpsRedProcessDynSyncRemTimeMsg: "
                           "Timer Start FAILED !!!\r\n"));
            }

            break;

        default:

            ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                       "ElpsRedProcessDynSyncRemTimeMsg: Wrong Timer"
                       " Type indicated by Remaining time sync-up message\r\n"));
            break;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedSendMsgToRm 
 *
 * DESCRIPTION      : This routine enqueues the Message to RM. If the Sending
 *                    fails, frees the memory.
 *
 * INPUT            : pMsg - RM Message Data Buffer
 *                    u2Length - Length of the message 
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC INT4
ElpsRedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = 0;

    u4RetVal = ElpsPortRmEnqMsgToRm (pMsg, u2Length,
                                     RM_ELPS_APP_ID, RM_ELPS_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedInitHardwareAudit 
 *
 * DESCRIPTION      : This function spawns a low priority task to perform 
 *                    hardware audit. It is called in active node only.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedInitHardwareAudit (VOID)
{
    INT4                i4RetVal = 0;

    if (gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId == 0)
    {
        /* Doing audit for the first time */
        i4RetVal = OsixTskCrt (ELPS_HW_AUDIT_TASK,
                               ELPS_HW_AUDIT_TASK_PRIORITY,
                               OSIX_DEFAULT_STACK_SIZE,
                               (OsixTskEntry) ElpsRedHwAuditTask,
                               0, &gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId);

        if (i4RetVal != OSIX_SUCCESS)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC,
                       "ElpsRedInitHardwareAudit: Hardware Audit "
                       "task creation failed!!!!\r\n"));
            return;
        }

    }
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHwAudCrtNpSyncBufferTable 
 *
 * DESCRIPTION      : This routine creates Hardware audit buffer table in the
 *                    standby node. Previosly present entries are deleted
 *                    before creating the table.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHwAudCrtNpSyncBufferTable (VOID)
{
    tElpsRedNpSyncEntry *pBuf = NULL;
    tElpsRedNpSyncEntry *pTempBuf = NULL;

    /* If the SLL is not empty then delete all the buffer entries. 
     */
    if (TMO_SLL_Count (&gElpsGlobalInfo.RedGlobalInfo.HwAuditTable) != 0)
    {
        TMO_DYN_SLL_Scan (&gElpsGlobalInfo.RedGlobalInfo.HwAuditTable, pBuf,
                          pTempBuf, tElpsRedNpSyncEntry *)
        {
            TMO_SLL_Delete (&gElpsGlobalInfo.RedGlobalInfo.HwAuditTable,
                            &(pBuf->Node));
            MemReleaseMemBlock (gElpsGlobalInfo.RedGlobalInfo.
                                HwAuditTablePoolId, (UINT1 *) pBuf);
            pBuf = pTempBuf;
        }
    }

    TMO_SLL_Init (&(gElpsGlobalInfo.RedGlobalInfo.HwAuditTable));
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHwAuditTask 
 *
 * DESCRIPTION      : This routine handles the Hardware audit process of the 
 *                    active node after switchover. It scans the Np Sync buffer
 *                    table and performs hardware audit for the entries present
 *                    in them. The precedence is given to software and the 
 *                    hardware is programmed for the software entries.
 *
 * INPUT            : None 
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHwAuditTask (VOID)
{
    tElpsRedNpSyncEntry *pBuf = NULL;
    tElpsRedNpSyncEntry *pTempBuf = NULL;

    ELPS_LOCK ();

    TMO_DYN_SLL_Scan (&gElpsGlobalInfo.RedGlobalInfo.HwAuditTable, pBuf,
                      pTempBuf, tElpsRedNpSyncEntry *)
    {
        switch (pBuf->u4NpApiId)
        {
            case ELPS_RED_HW_AUD_PG_NP_SYNC:

                ElpsRedHwAuditHandlePgAudit
                    (pBuf->unNpData.ElpsNpSyncPgEntry.u4ContextId,
                     pBuf->unNpData.ElpsNpSyncPgEntry.u4PgId,
                     pBuf->unNpData.ElpsNpSyncPgEntry.u4PgStatus);

                TMO_SLL_Delete (&gElpsGlobalInfo.RedGlobalInfo.
                                HwAuditTable, &(pBuf->Node));
                MemReleaseMemBlock (gElpsGlobalInfo.RedGlobalInfo.
                                    HwAuditTablePoolId, (UINT1 *) pBuf);
                pBuf = pTempBuf;
                break;

            case ELPS_RED_HW_AUD_CXT_NP_SYNC:

                ElpsRedHwAudHandleContextAudit
                    (pBuf->unNpData.ElpsNpSyncCxtEntry.u4ContextId,
                     pBuf->unNpData.ElpsNpSyncCxtEntry.u4ModStatus);

                TMO_SLL_Delete (&gElpsGlobalInfo.RedGlobalInfo.
                                HwAuditTable, &(pBuf->Node));
                MemReleaseMemBlock (gElpsGlobalInfo.RedGlobalInfo.
                                    HwAuditTablePoolId, (UINT1 *) pBuf);
                pBuf = pTempBuf;
                break;

            default:

                TMO_SLL_Delete (&gElpsGlobalInfo.RedGlobalInfo.
                                HwAuditTable, &(pBuf->Node));
                MemReleaseMemBlock (gElpsGlobalInfo.RedGlobalInfo.
                                    HwAuditTablePoolId, (UINT1 *) pBuf);
                pBuf = pTempBuf;
                break;
        }
    }

    gElpsGlobalInfo.RedGlobalInfo.HwAuditTaskId = 0;

    ELPS_UNLOCK ();

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHwAudUpdateNpSyncBuffer
 *
 * DESCRIPTION      : This routine scans the hardware audit buffer table for
 *                    the input entry. If entry is present it is removed from 
 *                    the table and added otherwise. This routine will be 
 *                    called from
 *                    1. NPAPI Sync-up Processing function
 *                    2. Software update (static and dynamic update)
 *                    This is needed because in case of static update, the
 *                    sequence number is reserved by RM, hence even though NP
 *                    SYNC is sent earlier, software update will be processed
 *                    first at standby, hence it will create entry which will
 *                    be flushed by NPAPI sync-up.
 *
 * INPUT            : punNpSync - NPAPI structure
 *                    u4NpApiId - NPAPI Id
 *                    u4EventId - Special Event Id
 *                    
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHwAudUpdateNpSyncBuffer (unNpSync * pNpSync, UINT4 u4NpApiId,
                                UINT4 u4EventId)
{
    tElpsRedNpSyncEntry *pBuf = NULL;
    tElpsRedNpSyncEntry *pTempBuf = NULL;
    tElpsRedNpSyncEntry *pPrevBuf = NULL;
    UINT1               u1Match = OSIX_FALSE;

    TMO_DYN_SLL_Scan (&gElpsGlobalInfo.RedGlobalInfo.HwAuditTable, pBuf,
                      pTempBuf, tElpsRedNpSyncEntry *)
    {
        if ((u4NpApiId != 0) && (u4NpApiId == pBuf->u4NpApiId))
        {
            if (MEMCMP (&(pBuf->unNpData), pNpSync, sizeof (unNpSync)) == 0)
            {
                TMO_SLL_Delete (&gElpsGlobalInfo.RedGlobalInfo.
                                HwAuditTable, &(pBuf->Node));
                MemReleaseMemBlock (gElpsGlobalInfo.RedGlobalInfo.
                                    HwAuditTablePoolId, (UINT1 *) pBuf);
                pBuf = NULL;
                u1Match = OSIX_TRUE;
                break;
            }
        }

        pPrevBuf = pBuf;
    }

    if (u1Match == OSIX_FALSE)
    {
        pTempBuf = NULL;

        pTempBuf = (tElpsRedNpSyncEntry *) MemAllocMemBlk
            (gElpsGlobalInfo.RedGlobalInfo.HwAuditTablePoolId);

        if (pTempBuf == NULL)
        {
            ELPS_TRC ((NULL, ALL_FAILURE_TRC | ELPS_CRITICAL_TRC,
                       "ElpsRedHwAudUpdateNpSyncBuffer: Hardware Audit "
                       "entry memory allocation failed!!!!\r\n"));
            return;
        }

        TMO_SLL_Init_Node ((tTMO_SLL_NODE *) & (pTempBuf->Node));

        MEMCPY (&pTempBuf->unNpData, pNpSync, sizeof (unNpSync));

        pTempBuf->u4NpApiId = u4NpApiId;

        pTempBuf->u4EventId = u4EventId;

        if (pPrevBuf == NULL)
        {
            /* Node to be inserted in first position */
            TMO_SLL_Insert (&gElpsGlobalInfo.RedGlobalInfo.HwAuditTable, NULL,
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
        else
        {
            /* Node to be inserted in last position */
            TMO_SLL_Insert (&gElpsGlobalInfo.RedGlobalInfo.HwAuditTable,
                            (tTMO_SLL_NODE *) & (pPrevBuf->Node),
                            (tTMO_SLL_NODE *) & (pTempBuf->Node));
        }
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHwAuditHandlePgAudit 
 *
 * DESCRIPTION      : This routine handles the hardware audit for a protection
 *                    group. Based on the protection group status in the
 *                    software for PG, the hardware is programmed.
 *
 * INPUT            : pPg - pointer to Protection group
 *                    u1PgStatus - Value of PG Status from H/w Audit entry.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHwAuditHandlePgAudit (UINT4 u4ContextId, UINT4 u4PgId, UINT4 u4PgStatus)
{
    tElpsNotifyInfo     NotifyInfo;
    tElpsContextInfo   *pCxt = NULL;
    tElpsPgInfo        *pPg = NULL;
    UINT4               u4SwPgStatus = 0;

    pCxt = ElpsCxtGetNode (u4ContextId);

    if (pCxt == NULL)
    {
        /* Context Not Created */
        return;
    }

    pPg = ElpsPgGetNode (pCxt, u4PgId);

    if (pPg == NULL)
    {
        /* Protection Group Not created */
        /* During the creation of protection group entry, hardware call 
         * is not made. During activating protection group entry only, 
         * hardware call be made.*/
        /* This Scenario is invalid, when pg is present in NP sync-up 
         * table, it has to be present in software (standby) also 
         */
        return;
    }
    else
    {
        if (pPg->u1PgStatus == ELPS_PG_STATUS_WORK_PATH_ACTIVE)
        {
            u4SwPgStatus = ELPS_PG_SWITCH_TO_WORKING;
        }
        else if (pPg->u1PgStatus == ELPS_PG_STATUS_HOLD_OFF_STATE)
        {
            u4SwPgStatus = (gaElpsSemStateInfo[pPg->ApsInfo.u1ApsSemStateId].
                            u4WorkingStatus == ELPS_ACTIVE) ?
                ELPS_PG_SWITCH_TO_WORKING : ELPS_PG_SWITCH_TO_PROTECTION;

        }
        else if ((pPg->u1PgStatus == ELPS_PG_STATUS_PROT_PATH_ACTIVE) ||
                 (pPg->u1PgStatus == ELPS_PG_STATUS_WTR_STATE) ||
                 (pPg->u1PgStatus == ELPS_PG_STATUS_DO_NOT_REVERT))
        {
            u4SwPgStatus = ELPS_PG_SWITCH_TO_PROTECTION;
        }
        else if (pPg->u1PgStatus == ELPS_PG_STATUS_DISABLE)
        {
            u4SwPgStatus = ELPS_PG_DELETE;
        }
        else
        {
            ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                       "ElpsRedHwAuditHandlePgAudit: Hardware Audit need not "
                       "be done in case of NP call failure\r\n"));
        }
    }

    switch (u4PgStatus)
    {
        case ELPS_PG_CREATE:

            if (u4SwPgStatus == ELPS_PG_DELETE)
            {
                /* Indicate hw regarding the PG entry creation */
                if (ElpsPgSwitchDataPath (pCxt, pPg, ELPS_PG_DELETE)
                    != OSIX_SUCCESS)
                {
                    pPg->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
                    NotifyInfo.u4ContextId = pCxt->u4ContextId;
                    NotifyInfo.u4PgId = pPg->u4PgId;
                    NotifyInfo.u1PgStatus = pPg->u1PgStatus;

                    ElpsTrapSendTrapNotifications (&NotifyInfo,
                                                   ELPS_TRAP_HW_PG_DEL_FAIL);
                    ELPS_TRC ((pCxt, ELPS_CRITICAL_TRC,
                               "ElpsRedHwAuditHandlePgAudit: "
                               "Failed to program the hardware for PG(id-%d)"
                               " with status "
                               "%d\r\n", pPg->u4PgId, ELPS_PG_DELETE));
                    return;
                }
            }
            else
            {
                ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                           "ElpsRedHwAuditHandlePgAudit: This scenario is not "
                           "possible where, the entry in software status other"
                           " than Delete.\r\n"));
            }
            break;

        case ELPS_PG_DELETE:

            if ((u4SwPgStatus != ELPS_PG_DELETE) &&
                (u4SwPgStatus != ELPS_PG_STATUS_INVALID))
            {
                if (ElpsPgSwitchDataPath (pCxt, pPg, ELPS_PG_CREATE)
                    != OSIX_SUCCESS)
                {
                    pPg->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
                    NotifyInfo.u4ContextId = pCxt->u4ContextId;
                    NotifyInfo.u4PgId = pPg->u4PgId;
                    NotifyInfo.u1PgStatus = pPg->u1PgStatus;

                    ElpsTrapSendTrapNotifications (&NotifyInfo,
                                                   ELPS_TRAP_HW_PG_CRT_FAIL);
                    ELPS_TRC ((pCxt, ELPS_CRITICAL_TRC,
                               "ElpsRedHwAuditHandlePgAudit: "
                               "Failed to program the hardware for PG(id-%d)"
                               " with status "
                               "%d\r\n", pPg->u4PgId, ELPS_PG_CREATE));
                    return;
                }

                if (ElpsPgSwitchDataPath (pCxt, pPg, u4SwPgStatus)
                    != OSIX_SUCCESS)
                {
                    pPg->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
                    return;
                }
            }
            else
            {
                ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                           "ElpsRedHwAuditHandlePgAudit: This scenario is not"
                           " possible where, the entry in software status"
                           " other than Delete and Invalid."
                           " Nothing to be done.\r\n"));
            }
            break;

        case ELPS_PG_SWITCH_TO_WORKING:
            /* Intentional Fall Through */
        case ELPS_PG_SWITCH_TO_PROTECTION:

            if ((u4SwPgStatus != ELPS_PG_DELETE) &&
                (u4SwPgStatus != ELPS_PG_STATUS_INVALID))
            {
                if (ElpsPgSwitchDataPath (pCxt, pPg, u4SwPgStatus)
                    != OSIX_SUCCESS)
                {
                    pPg->u1PgStatus = ELPS_PG_STATUS_SWITCHING_FAILED;
                    return;
                }
            }
            else
            {
                ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                           "ElpsRedHwAuditHandlePgAudit: This scenario is not"
                           " possible where, the entry in software status"
                           " other than Delete and Invalid."
                           " Nothing to be done.\r\n"));
            }
            break;

        default:

            ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                       "ElpsRedHwAuditHandlePgAudit: This scenario is not "
                       "possible where, the entry in Np sync status is"
                       " Invalid.\r\n"));
            break;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHwAudHandleContextAudit 
 *
 * DESCRIPTION      : This routine handles the hardware audit for a context.
 *                    This is done to handle crash during 
 *                    following configurations
 *                    1. Context Delete
 *                    2. Module shutdown
 *                    3. Module disable
 *                    4. Module enable
 *
 *                    The configuration is re-applied as part of handling
 *                    hardware audit
 *
 * INPUT            : u4ContextId - Context Identifier
 *                    u4ModStatus - Identifier of the Configuration
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHwAudHandleContextAudit (UINT4 u4ContextId, UINT4 u4ModStatus)
{
    tElpsContextInfo   *pCxt = NULL;

    pCxt = ElpsCxtGetNode (u4ContextId);

    if (pCxt == NULL)
    {
        /* Context Not Created */
        return;
    }

    switch (u4ModStatus)
    {
        case ELPS_RED_HW_AUD_MOD_SHUTDOWN:

            if (ElpsCxtShutdown (pCxt) == OSIX_FAILURE)
            {
                ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                           "ElpsRedHwAudHandleContextAudit: "
                           "ElpsCxtShutdown Failed \r\n"));
                return;
            }
            break;

        case ELPS_RED_HW_AUD_MOD_DISABLE:

            if (ElpsCxtDisable (pCxt) == OSIX_FAILURE)
            {
                ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                           "ElpsRedHwAudHandleContextAudit: "
                           "ElpsCxtDisable Failed \r\n"));
                return;
            }
            break;

        case ELPS_RED_HW_AUD_MOD_ENABLE:

            if (ElpsCxtEnable (pCxt) == OSIX_FAILURE)
            {
                ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                           "ElpsRedHwAudHandleContextAudit: "
                           "ElpsCxtEnable Failed \r\n"));
                return;
            }
            break;

        default:
            ELPS_TRC ((pCxt, ALL_FAILURE_TRC,
                       "ElpsRedHwAudHandleContextAudit: "
                       "Invalid identifier of the Configuration\r\n"));
            break;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHwAuditFindAndUpdNpSync 
 *
 * DESCRIPTION      : This routine finds out if an NPAPI call was made on 
 *                    active node based on the Old Pg Status and New Pg status
 *                    from the dynamic update. If so it calls the function to 
 *                    update the Np sync table. This routine is used by standby
 *                    node alone.
 *
 * INPUT            : pPg - Protection Group Info
 *                    u1OldPgStatus - Old Protection group status.
 *
 * OUTPUT           : None
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHwAuditFindAndUpdNpSync (tElpsPgInfo * pPg, UINT1 u1OldPgStatus)
{
    UINT4               u4Action = ELPS_PG_STATUS_INVALID;

    if ((u1OldPgStatus >= ELPS_PG_MAX_STATUS) ||
        (pPg->u1PgStatus >= ELPS_PG_MAX_STATUS))
    {
        return;
    }
    u4Action = gau4ElpsRedPgStatusChgToActMap[u1OldPgStatus][pPg->u1PgStatus];

    if (u4Action == ELPS_PG_STATUS_INVALID)
    {
        /* No NP Call was done at active, hence no NP sync-up sent to standby */
        return;
    }

    ElpsHwAdSendPgNpSync (pPg->pContextInfo->u4ContextId, pPg->u4PgId,
                          u4Action);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHwAuditIncBlkCounter 
 *
 * DESCRIPTION      : This routine increments the NP Sync Block Counter by one.
 *                    If the value of this Block flag is not equal to 0, then
 *                    it is intended to block NP Sync-ups.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHwAuditIncBlkCounter (VOID)
{
    gElpsGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount++;
    ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
               "ElpsRedHwAuditIncBlkCounter: "
               "Incrementing the NP Sync Block Counter Value to %d \r\n",
               gElpsGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedHwAuditDecBlkCounter 
 *
 * DESCRIPTION      : This routine decrements the NP Sync Block Counter by one.
 *                    If the value of this Block flag is not equal to 0, then
 *                    it is intended to block NP Sync-ups. This routine also
 *                    checks if the value is lesser than 0, displayes critical
 *                    trace and sets the value to 0.
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsRedHwAuditDecBlkCounter (VOID)
{
    if (gElpsGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount == 0)
    {
        ELPS_TRC ((NULL, ELPS_CRITICAL_TRC,
                   "ElpsRedHwAuditIncBlkCounter: NP Sync Block Counter"
                   " Value is being decremented less than 0 "
                   "Resetting the NP Sync Block Counter Value to 0\r\n"));
        return;
    }

    gElpsGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount--;
    ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
               "ElpsRedHwAuditIncBlkCounter: "
               "Decrementing the NP Sync Block Counter Value to %d \r\n",
               gElpsGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount));

    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsRedGetNodeState 
 *
 * DESCRIPTION      : This routine returns the Node State
 *
 * INPUT            : None
 *
 * OUTPUT           : None
 * 
 * RETURNS          : NodeState (RM_INIT/RM_ACTIVE/RM_STANDBY)
 * 
 **************************************************************************/
PUBLIC UINT1
ElpsRedGetNodeState (VOID)
{
    return (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus);
}

/* HITLESS RESTART */
/*****************************************************************************/
/* Function Name      : ElpsRedHRProcStdyStPktReq                            */
/*                                                                           */
/* Description        : This function triggers the steady state packet from  */
/*                      ELPS to the RM by setting the periodic timeout value */
/*                      as zero                                              */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
ElpsRedHRProcStdyStPktReq (VOID)
{
    tElpsPgInfo        *pNextPg = NULL;
    UINT4               u4NextContextId = 0;
    UINT4               u4NextPgId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1TailFlag = 0;

    if (gElpsGlobalInfo.u1IsElpsInitialized == OSIX_FALSE)
    {
        /* Module is Not Started. So peroidic timers will not be running.
         * TO move next moduls, send tail msg here */
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS Module is Not Started; "
                   "sending steady state tail message.\n"));
        ElpsRedHRSendStdyStTailMsg ();
        return;
    }
    if (ElpsRedGetNodeState () != RM_ACTIVE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS: Node is not active. "
                   "Processing of steady state pkt request failed.\n"));
        return;
    }
    if (ELPS_HR_STATUS () == ELPS_HR_STATUS_DISABLE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ELPS: Hitless restart is not enabled."
                   " Processing of steady state pkt request failed.\n"));
        return;
    }
    ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS: starts sending steady state "
               "packets to RM.\n"));

    /* This flag is set to indicate the ELPS timer expiry handler that steady  
     * state request had received from RM */

    ELPS_HR_STDY_ST_REQ_RCVD () = OSIX_TRUE;

    while ((pNextPg = ElpsPgGetNextNode (u4ContextId, u4PgId,
                                         &u4NextContextId,
                                         &u4NextPgId)) != NULL)
    {
        u4ContextId = u4NextContextId;
        u4PgId = u4NextPgId;

        /* Stop the Elps Aps Periodic Timer and Restart it again with 
         * duration as 0 to get the steady state packet
         */

        ElpsTmrStopTimer (pNextPg, ELPS_APS_PERIODIC_TMR);
        if (ElpsTmrStartTimer (pNextPg, 0 /* Duration */ ,
                               ELPS_APS_PERIODIC_TMR) == OSIX_FAILURE)
        {
            ELPS_TRC ((pNextPg->pContextInfo, OS_RESOURCE_TRC |
                       ALL_FAILURE_TRC, "ElpsTmrApsPeriodicTmrExp: "
                       "Timer Start FAILED !!!\r\n"));
        }
        if (u1TailFlag == 0)
        {
            u1TailFlag = ELPS_HR_STDY_ST_PKT_TAIL;
        }
    }
    /* This steady state tail msg sending is done in case of ELPS APS 
     * periodic timers are not running. */

    if (u1TailFlag != ELPS_HR_STDY_ST_PKT_TAIL)
    {
        ElpsRedHRSendStdyStTailMsg ();

        /* This flag is set to indicate the ELPS timer expiry handler 
         * that steady state request had received from RM */
        ELPS_HR_STDY_ST_REQ_RCVD () = OSIX_FALSE;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : ElpsRedHRSendStdyStTailMsg                           */
/*                                                                           */
/* Description        : This function is called when all the steady state    */
/*                      pkts were sent to the RM. It sends steady state tail */
/*                      message to RM module.                                */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
INT1
ElpsRedHRSendStdyStTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = ELPS_RM_OFFSET;
    UINT2               u2BufSize = 0;

    if (ElpsRedGetNodeState () != RM_ACTIVE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC, "ELPS: Node is not active. "
                   "Steady state tail message is not sent to RM.\n"));
        return OSIX_SUCCESS;
    }
    if (ELPS_HR_STATUS () == ELPS_HR_STATUS_DISABLE)
    {
        ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
                   "ELPS: Hitless restart is not enabled. "
                   "Steady state tail message is not sent to RM.\n"));
        return OSIX_SUCCESS;
    }
    ELPS_TRC ((NULL, CONTROL_PLANE_TRC,
               "ELPS: sending steady state tail message to RM.\n"));

    u2BufSize = ELPS_RED_TYPE_FIELD_SIZE + ELPS_RED_LEN_FIELD_SIZE;

    /* Allocate memory for data to be sent to RM. This memory will be freed
     * by RM  */
    if ((pMsg = RM_ALLOC_TX_BUF (u2BufSize)) == NULL)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "Rm alloc failed\n"));
        return (OSIX_FAILURE);
    }

    /* Form a steady state tail message.
     *
     *             <--------1 Byte----------><---2 Byte--->
     *   __________________________________________________
     *   |        |                          |             |
     *   | RM Hdr | ELPS_HR_STDY_ST_PKT_TAIL | Msg Length  |
     *   |________|__________________________|_____________|
     *
     *  The RM Hdr shall be included by RM.
     */

    /* Fill the message type. */
    ELPS_RM_PUT_1_BYTE (pMsg, &u4Offset, ELPS_HR_STDY_ST_PKT_TAIL);
    ELPS_RM_PUT_2_BYTE (pMsg, &u4Offset, u2BufSize);

    if (ElpsRedSendMsgToRm (pMsg, u2BufSize) == OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, ALL_FAILURE_TRC, "ELPS: steady state tail message "
                   "sending is failed\n"));
    }
    return OSIX_SUCCESS;
}

#endif /* _ELPSRED_C_ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  elpsred.c                      */
/*-----------------------------------------------------------------------*/
