/*****************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: elpsnpapi.c,v 1.1 2013/01/10 12:44:54 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of  ELPS module.
 *****************************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : ElpsFsMiElpsHwPgSwitchDataPath                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiElpsHwPgSwitchDataPath
 *                                                                          
 *    Input(s)            : Arguments of FsMiElpsHwPgSwitchDataPath
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __ELPS_NP_API_C
#define __ELPS_NP_API_C

#include "nputil.h"

UINT1
ElpsFsMiElpsHwPgSwitchDataPath (UINT4 u4ContextId, UINT4 u4PgId,
                                tElpsHwPgSwitchInfo * pPgHwInfo)
{
    tFsHwNp             FsHwNp;
    tElpsNpModInfo     *pElpsNpModInfo = NULL;
    tElpsNpWrFsMiElpsHwPgSwitchDataPath *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ELPS_MOD,    /* Module ID */
                         FS_MI_ELPS_HW_PG_SWITCH_DATA_PATH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pElpsNpModInfo = &(FsHwNp.ElpsNpModInfo);
    pEntry = &pElpsNpModInfo->ElpsNpFsMiElpsHwPgSwitchDataPath;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4PgId = u4PgId;
    pEntry->pPgHwInfo = pPgHwInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MBSM_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : ElpsFsMiElpsMbsmHwPgSwitchDataPath                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMiElpsMbsmHwPgSwitchDataPath
 *                                                                          
 *    Input(s)            : Arguments of FsMiElpsMbsmHwPgSwitchDataPath
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
ElpsFsMiElpsMbsmHwPgSwitchDataPath (UINT4 u4ContextId, UINT4 u4PgId,
                                    tElpsHwPgSwitchInfo * pPgHwInfo,
                                    tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tElpsNpModInfo     *pElpsNpModInfo = NULL;
    tElpsNpWrFsMiElpsMbsmHwPgSwitchDataPath *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_ELPS_MOD,    /* Module ID */
                         FS_MI_ELPS_MBSM_HW_PG_SWITCH_DATA_PATH,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pElpsNpModInfo = &(FsHwNp.ElpsNpModInfo);
    pEntry = &pElpsNpModInfo->ElpsNpFsMiElpsMbsmHwPgSwitchDataPath;

    pEntry->u4ContextId = u4ContextId;
    pEntry->u4PgId = u4PgId;
    pEntry->pPgHwInfo = pPgHwInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif

#endif /*__ELPS_NP_API_C */
