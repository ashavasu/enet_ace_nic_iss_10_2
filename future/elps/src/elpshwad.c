/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: elpshwad.c,v 1.5 2014/02/11 12:49:46 siva Exp $
 *
 * Description: This file contains the ELPS Hardware Audit Np Sync-up support 
 * routines and utility routines.
 *****************************************************************************/
#ifndef _ELPS_HWAD_C_
#define _ELPS_HWAD_C_
#include "elpsinc.h"
/***************************************************************************
 * FUNCTION NAME    : ElpsHwAdSendPgNpSync 
 *
 * DESCRIPTION      : This routine sends the PG Np sync-up to RM if the node is
 *                    active. If the node state is Standby, the Np-Syncup
 *                    buffer table is updated.
 *
 * INPUT            : u4ContextId - Context Id
 *                    u4PgId - Protection Group Id
 *                    u4PgStatus - NPAPI Action
 *
 * OUTPUT           : None
 * 
 * RETURNS          : None 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsHwAdSendPgNpSync (UINT4 u4ContextId, UINT4 u4PgId, UINT4 u4PgStatus)
{
    unNpSync            unNpInfo;

    MEMSET (&unNpInfo, 0, sizeof (unNpInfo));
    unNpInfo.ElpsNpSyncPgEntry.u4ContextId = u4ContextId;
    unNpInfo.ElpsNpSyncPgEntry.u4PgId = u4PgId;
    unNpInfo.ElpsNpSyncPgEntry.u4PgStatus = u4PgStatus;
    if (gElpsGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount == 0)
    {
        if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
        {
            if (gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp != 0)
            {
                ElpsHwAdFormAndSendPgNpSync (unNpInfo.ElpsNpSyncPgEntry,
                                             RM_ELPS_APP_ID,
                                             ELPS_RED_HW_AUD_PG_NP_SYNC);
            }
        }
        else if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_STANDBY)
        {
            ElpsRedHwAudUpdateNpSyncBuffer (&unNpInfo,
                                            ELPS_RED_HW_AUD_PG_NP_SYNC, 0);
        }
    }
}

/***************************************************************************
 * FUNCTION NAME    : ElpsHwAdSendCxtSync
 *
 * DESCRIPTION      : This routine sends the Context Np sync-up to RM if the 
 *                    node is active. If the node state is Standby, 
 *                    the Np-Syncup buffer table is updated.
 *
 * INPUT            : u4ContextId - Context Id
 *                    u4ModStatus - Shutdown/Enable/Disable
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsHwAdSendCxtSync (UINT4 u4ContextId, UINT4 u4ModStatus)
{
    unNpSync            unNpInfo;

    MEMSET (&unNpInfo, 0, sizeof (unNpInfo));
    unNpInfo.ElpsNpSyncCxtEntry.u4ContextId = u4ContextId;
    unNpInfo.ElpsNpSyncCxtEntry.u4ModStatus = u4ModStatus;
    if (gElpsGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount == 0)
    {
        if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_ACTIVE)
        {
            if (gElpsGlobalInfo.RedGlobalInfo.u1NumOfPeerNodesUp != 0)
            {
                ElpsHwAdFormAndSendCxtNpSync (unNpInfo.ElpsNpSyncCxtEntry,
                                              RM_ELPS_APP_ID,
                                              ELPS_RED_HW_AUD_CXT_NP_SYNC);
            }
        }
        else if (gElpsGlobalInfo.RedGlobalInfo.u1NodeStatus == RM_STANDBY)
        {
            ElpsRedHwAudUpdateNpSyncBuffer (&unNpInfo,
                                            ELPS_RED_HW_AUD_CXT_NP_SYNC, 0);
        }
    }
}

/***************************************************************************
 * FUNCTION NAME    : ElpsHwAdFormAndSendPgNpSync 
 *
 * DESCRIPTION      : This routine forms the Pg Np sync-up to RM 
 *
 * INPUT            : NpSyncPgEntry - Pg Information to be synched 
 *                    u4AppId - ELPS AppId
 *                    u4NpApiId - Sync Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsHwAdFormAndSendPgNpSync (tElpsNpSyncPgEntry NpSyncPgEntry,
                             UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;
    UINT2               u2MsgSize = 0;
    UINT1               u1RetVal = 0;
    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        u1RetVal = ElpsPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        ELPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, ELPS_RED_NP_SYNC_INFO_MSG);
        ELPS_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        ELPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        ELPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, NpSyncPgEntry.u4ContextId);
        ELPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, NpSyncPgEntry.u4PgId);
        ELPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, NpSyncPgEntry.u4PgStatus);
        if (ElpsPortRmEnqMsgToRm (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            u1RetVal = ElpsPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    UNUSED_PARAM (u1RetVal);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsHwAdFormAndSendCxtNpSync 
 *
 * DESCRIPTION      : This routine forms the Context Np sync-up to RM 
 *
 * INPUT            : NpSyncCxtEntry - Context Information to be synched 
 *                    u4AppId - ELPS AppId
 *                    u4NpApiId - Sync Identifier
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsHwAdFormAndSendCxtNpSync (tElpsNpSyncCxtEntry NpSyncCxtEntry,
                              UINT4 u4AppId, UINT4 u4NpApiId)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT2               u2OffSet = 0;
    UINT2               u2MsgSize = 0;
    UINT1               u1RetVal = 0;
    ProtoEvt.u4AppId = u4AppId;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    u2MsgSize =
        NPSYNC_MSGTYPE_SIZE + NPSYNC_MSG_LENGTH + NPSYNC_MSG_NPAPI_ID_SIZE +
        sizeof (UINT4) + sizeof (UINT4);
    if ((pMsg = RM_ALLOC_TX_BUF (u2MsgSize)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        u1RetVal = ElpsPortRmApiHandleProtocolEvent (&ProtoEvt);
    }
    else
    {
        u2OffSet = 0;

        ELPS_RM_PUT_1_BYTE (pMsg, &u2OffSet, ELPS_RED_NP_SYNC_INFO_MSG);
        ELPS_RM_PUT_2_BYTE (pMsg, &u2OffSet, u2MsgSize);
        ELPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, u4NpApiId);
        ELPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, NpSyncCxtEntry.u4ContextId);
        ELPS_RM_PUT_4_BYTE (pMsg, &u2OffSet, NpSyncCxtEntry.u4ModStatus);
        if (ElpsPortRmEnqMsgToRm (pMsg, u2OffSet, u4AppId,
                                  u4AppId) == RM_FAILURE)
        {
            ProtoEvt.u4Error = RM_SENDTO_FAIL;
            u1RetVal = ElpsPortRmApiHandleProtocolEvent (&ProtoEvt);
        }
    }
    UNUSED_PARAM (u1RetVal);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : ElpsHwAdProcessNpSyncMsg 
 *
 * DESCRIPTION      : This routine process the Np sync-up from RM at the 
 *                    standby node. It calls the NP sync-Buffer Update function
 *
 * INPUT            : pMsg - Message Buffer 
 *                    pu2OffSet - OffSet Value
 *
 * OUTPUT           : None
 * 
 * RETURNS          : VOID 
 * 
 **************************************************************************/
PUBLIC VOID
ElpsHwAdProcessNpSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet)
{
    INT4                i4NpApiId = 0;
    unNpSync            unNpInfo;

    MEMSET (&unNpInfo, 0, sizeof (unNpInfo));

    ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet, i4NpApiId);

    switch (i4NpApiId)
    {

        case ELPS_RED_HW_AUD_PG_NP_SYNC:
            ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                unNpInfo.ElpsNpSyncPgEntry.u4ContextId);
            ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                unNpInfo.ElpsNpSyncPgEntry.u4PgId);
            ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                unNpInfo.ElpsNpSyncPgEntry.u4PgStatus);
            ElpsRedHwAudUpdateNpSyncBuffer (&unNpInfo,
                                            ELPS_RED_HW_AUD_PG_NP_SYNC, 0);
            break;

        case ELPS_RED_HW_AUD_CXT_NP_SYNC:
            ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                unNpInfo.ElpsNpSyncCxtEntry.u4ContextId);
            ELPS_RM_GET_4_BYTE (pMsg, pu2OffSet,
                                unNpInfo.ElpsNpSyncCxtEntry.u4ModStatus);
            ElpsRedHwAudUpdateNpSyncBuffer (&unNpInfo,
                                            ELPS_RED_HW_AUD_CXT_NP_SYNC, 0);
            break;

        default:
            break;

    }                            /* End of switch */

    return;
}
#endif
