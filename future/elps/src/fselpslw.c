/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fselpslw.c,v 1.31 2016/03/03 10:32:05 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "elpsinc.h"
#include "mplsapi.h"
#include "elpssem.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsGlobalTraceOption
 Input       :  The Indices

                The Object 
                retValFsElpsGlobalTraceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsGlobalTraceOption (INT4 *pi4RetValFsElpsGlobalTraceOption)
{
    *pi4RetValFsElpsGlobalTraceOption = gElpsGlobalInfo.u1GlobalTrcOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPSCChannelCode
 Input       :  The Indices

                The Object
                retValFsElpsPSCChannelCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPSCChannelCode (UINT4 *pu4RetValFsElpsPSCChannelCode)
{
    *pu4RetValFsElpsPSCChannelCode = gElpsGlobalInfo.u4PscChannelCode;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsRapidTxTime
 Input       :  The Indices

                The Object
                retValFsElpsRapidTxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsRapidTxTime (UINT4 *pu4RetValFsElpsRapidTxTime)
{
    *pu4RetValFsElpsRapidTxTime = gElpsGlobalInfo.u4RapidTxTime;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElpsGlobalTraceOption
 Input       :  The Indices

                The Object 
                setValFsElpsGlobalTraceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsGlobalTraceOption (INT4 i4SetValFsElpsGlobalTraceOption)
{
    gElpsGlobalInfo.u1GlobalTrcOption = (UINT1) i4SetValFsElpsGlobalTraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPSCChannelCode
 Input       :  The Indices

                The Object
                setValFsElpsPSCChannelCode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPSCChannelCode (UINT4 u4SetValFsElpsPSCChannelCode)
{
    UINT4               u4PrevChannelCode = gElpsGlobalInfo.u4PscChannelCode;

    if (u4PrevChannelCode == u4SetValFsElpsPSCChannelCode)
    {
        return SNMP_SUCCESS;
    }
    gElpsGlobalInfo.u4PscChannelCode = u4SetValFsElpsPSCChannelCode;

    if (ElpsPortSendChannelCode (gElpsGlobalInfo.u4PscChannelCode)
        == OSIX_FAILURE)
    {
        gElpsGlobalInfo.u4PscChannelCode = u4PrevChannelCode;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsRapidTxTime
 Input       :  The Indices

                The Object
                setValFsElpsRapidTxTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsRapidTxTime (UINT4 u4SetValFsElpsRapidTxTime)
{
    gElpsGlobalInfo.u4RapidTxTime = u4SetValFsElpsRapidTxTime;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElpsGlobalTraceOption
 Input       :  The Indices

                The Object 
                testValFsElpsGlobalTraceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsGlobalTraceOption (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsElpsGlobalTraceOption)
{
    if ((i4TestValFsElpsGlobalTraceOption != ELPS_SNMP_TRUE) &&
        (i4TestValFsElpsGlobalTraceOption != ELPS_SNMP_FALSE))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Input for Global Trace Option\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPSCChannelCode
 Input       :  The Indices

                The Object
                testValFsElpsPSCChannelCode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPSCChannelCode (UINT4 *pu4ErrorCode,
                               UINT4 u4TestValFsElpsPSCChannelCode)
{
    if ((u4TestValFsElpsPSCChannelCode < ELPS_MIN_PSC_CHANNEL_CODE) ||
        (u4TestValFsElpsPSCChannelCode > ELPS_MAX_PSC_CHANNEL_CODE))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Input for Psc Channel code\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsRapidTxTime
 Input       :  The Indices

                The Object
                testValFsElpsRapidTxTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsRapidTxTime (UINT4 *pu4ErrorCode,
                            UINT4 u4TestValFsElpsRapidTxTime)
{
    if ((u4TestValFsElpsRapidTxTime < ELPS_MIN_RAPID_TX_TIME) ||
        (u4TestValFsElpsRapidTxTime > ELPS_MAX_RAPID_TX_TIME))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Input for Global Rapid Tx Time\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElpsGlobalTraceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsGlobalTraceOption (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsElpsPSCChannelCode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsPSCChannelCode (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsElpsRapidTxTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsRapidTxTime (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElpsContextTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElpsContextTable
 Input       :  The Indices
                FsElpsContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElpsContextTable (UINT4 u4FsElpsContextId)
{
    if (u4FsElpsContextId >= ELPS_MAX_CONTEXTS)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Context Identifier %d\r\n", u4FsElpsContextId));
        return SNMP_FAILURE;
    }

    if (ElpsCxtGetNode (u4FsElpsContextId) == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Context %d not created\n", u4FsElpsContextId));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElpsContextTable
 Input       :  The Indices
                FsElpsContextId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElpsContextTable (UINT4 *pu4FsElpsContextId)
{
    UINT4               u4ContextId = 0;

    *pu4FsElpsContextId = 0;

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        if (ElpsCxtGetNode (u4ContextId) != NULL)
        {
            *pu4FsElpsContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElpsContextTable
 Input       :  The Indices
                FsElpsContextId
                nextFsElpsContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElpsContextTable (UINT4 u4FsElpsContextId,
                                   UINT4 *pu4NextFsElpsContextId)
{
    UINT4               u4ContextId = u4FsElpsContextId + 1;

    *pu4NextFsElpsContextId = 0;

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        if (ElpsCxtGetNode (u4ContextId) != NULL)
        {
            *pu4NextFsElpsContextId = u4ContextId;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsContextSystemControl
 Input       :  The Indices
                FsElpsContextId

                The Object 
                retValFsElpsContextSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsContextSystemControl (UINT4 u4FsElpsContextId,
                                  INT4 *pi4RetValFsElpsContextSystemControl)
{
    if (ElpsUtilIsElpsStarted (u4FsElpsContextId) == OSIX_FALSE)
    {
        *pi4RetValFsElpsContextSystemControl = ELPS_SHUTDOWN;
    }
    else
    {
        *pi4RetValFsElpsContextSystemControl = ELPS_START;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsContextModuleStatus
 Input       :  The Indices
                FsElpsContextId

                The Object 
                retValFsElpsContextModuleStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsContextModuleStatus (UINT4 u4FsElpsContextId,
                                 INT4 *pi4RetValFsElpsContextModuleStatus)
{
    tElpsContextInfo   *pContextInfo = NULL;

    if (ElpsUtilIsElpsStarted (u4FsElpsContextId) == OSIX_FALSE)
    {
        *pi4RetValFsElpsContextModuleStatus = ELPS_DISABLED;
    }
    else
    {
        pContextInfo = ElpsCxtGetNode (u4FsElpsContextId);

        if (pContextInfo == NULL)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Context %d not created\n", u4FsElpsContextId));
            return SNMP_FAILURE;
        }
        *pi4RetValFsElpsContextModuleStatus = pContextInfo->u1ModuleStatus;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsContextTraceInputString
 Input       :  The Indices
                FsElpsContextId

                The Object 
                retValFsElpsContextTraceInputString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsContextTraceInputString (UINT4 u4FsElpsContextId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValFsElpsContextTraceInputString)
{
    tElpsContextInfo   *pContextInfo = NULL;

    if (ElpsUtilIsElpsStarted (u4FsElpsContextId) == OSIX_FALSE)
    {
        pRetValFsElpsContextTraceInputString->i4_Length =
            ElpsUtilGetTraceInputValue (pRetValFsElpsContextTraceInputString->
                                        pu1_OctetList, ELPS_CRITICAL_TRC);
    }
    else
    {
        pContextInfo = ElpsCxtGetNode (u4FsElpsContextId);

        if (pContextInfo == NULL)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Context %d not created\n", u4FsElpsContextId));
            return SNMP_FAILURE;
        }

        pRetValFsElpsContextTraceInputString->i4_Length =
            ElpsUtilGetTraceInputValue (pRetValFsElpsContextTraceInputString->
                                        pu1_OctetList,
                                        pContextInfo->u4TraceInput);

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsContextEnableTrap
 Input       :  The Indices
                FsElpsContextId

                The Object 
                retValFsElpsContextEnableTrap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsContextEnableTrap (UINT4 u4FsElpsContextId,
                               INT4 *pi4RetValFsElpsContextEnableTrap)
{
    tElpsContextInfo   *pContextInfo = NULL;

    if (ElpsUtilIsElpsStarted (u4FsElpsContextId) == OSIX_FALSE)
    {
        *pi4RetValFsElpsContextEnableTrap = ELPS_SNMP_TRUE;
    }
    else
    {
        pContextInfo = ElpsCxtGetNode (u4FsElpsContextId);

        if (pContextInfo == NULL)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Context %d not created\n", u4FsElpsContextId));
            return SNMP_FAILURE;
        }
        *pi4RetValFsElpsContextEnableTrap = pContextInfo->u1TrapStatusFlag;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsContextVlanGroupManager
 Input       :  The Indices
                FsElpsContextId

                The Object 
                retValFsElpsContextVlanGroupManager
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsContextVlanGroupManager (UINT4 u4FsElpsContextId,
                                     INT4
                                     *pi4RetValFsElpsContextVlanGroupManager)
{
    tElpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ElpsCxtGetNode (u4FsElpsContextId);

    if (pContextInfo == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Context %d not created\n", u4FsElpsContextId));
        return SNMP_FAILURE;
    }
    *pi4RetValFsElpsContextVlanGroupManager = pContextInfo->i4VlanGroupManager;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElpsContextSystemControl
 Input       :  The Indices
                FsElpsContextId

                The Object 
                setValFsElpsContextSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsContextSystemControl (UINT4 u4FsElpsContextId,
                                  INT4 i4SetValFsElpsContextSystemControl)
{
    tElpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ElpsCxtGetNode (u4FsElpsContextId);

    if (pContextInfo == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Context %d not created\n", u4FsElpsContextId));
        return SNMP_FAILURE;
    }

    if (i4SetValFsElpsContextSystemControl ==
        gElpsGlobalInfo.au1SystemCtrl[u4FsElpsContextId])
    {
        return SNMP_SUCCESS;
    }

    if (i4SetValFsElpsContextSystemControl == ELPS_START)
    {
        ElpsCxtStart (pContextInfo);
    }
    else
    {
        ElpsCxtShutdown (pContextInfo);
        ElpsPortNotifyProtocolShutStatus ((INT4) u4FsElpsContextId);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsContextModuleStatus
 Input       :  The Indices
                FsElpsContextId

                The Object 
                setValFsElpsContextModuleStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsContextModuleStatus (UINT4 u4FsElpsContextId,
                                 INT4 i4SetValFsElpsContextModuleStatus)
{
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4Dummy = 0;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pContextInfo->u1ModuleStatus == i4SetValFsElpsContextModuleStatus)
    {
        /* If ModuleStatus is same as configuring value, return Success. */
        return SNMP_SUCCESS;
    }

    if (i4SetValFsElpsContextModuleStatus == ELPS_ENABLED)
    {
        if (ElpsCxtEnable (pContextInfo) == OSIX_FAILURE)
        {
            ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                       "Unable to Enable the Module\n"));
            return SNMP_FAILURE;
        }
    }
    else if (i4SetValFsElpsContextModuleStatus == ELPS_DISABLED)
    {
        ElpsCxtDisable (pContextInfo);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsContextTraceInputString
 Input       :  The Indices
                FsElpsContextId

                The Object 
                setValFsElpsContextTraceInputString
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsContextTraceInputString (UINT4 u4FsElpsContextId,
                                     tSNMP_OCTET_STRING_TYPE
                                     * pSetValFsElpsContextTraceInputString)
{
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4TrcOption = 0;
    UINT4               u4Dummy = 0;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    u4TrcOption = ElpsUtilGetTraceOptionValue
        (pSetValFsElpsContextTraceInputString->pu1_OctetList,
         pSetValFsElpsContextTraceInputString->i4_Length);

    if ((STRNCMP (pSetValFsElpsContextTraceInputString->pu1_OctetList,
                  "enable", STRLEN ("enable"))) == 0)
    {
        pContextInfo->u4TraceInput |= u4TrcOption;
    }
    else
    {
        pContextInfo->u4TraceInput &= ~u4TrcOption;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsContextEnableTrap
 Input       :  The Indices
                FsElpsContextId

                The Object 
                setValFsElpsContextEnableTrap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsContextEnableTrap (UINT4 u4FsElpsContextId,
                               INT4 i4SetValFsElpsContextEnableTrap)
{
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4Dummy = 0;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pContextInfo->u1TrapStatusFlag = (UINT1) i4SetValFsElpsContextEnableTrap;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsElpsContextVlanGroupManager
 Input       :  The Indices
                FsElpsContextId

                The Object 
                setValFsElpsContextVlanGroupManager
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsContextVlanGroupManager (UINT4 u4FsElpsContextId,
                                     INT4 i4SetValFsElpsContextVlanGroupManager)
{
    tElpsContextInfo   *pContextInfo = NULL;

    pContextInfo = ElpsCxtGetNode (u4FsElpsContextId);
    if (pContextInfo == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Context %d not created\n", u4FsElpsContextId));
        return SNMP_FAILURE;
    }

    pContextInfo->i4VlanGroupManager = i4SetValFsElpsContextVlanGroupManager;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsContextSystemControl
 Input       :  The Indices
                FsElpsContextId

                The Object 
                testValFsElpsContextSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsContextSystemControl (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsElpsContextId,
                                     INT4 i4TestValFsElpsContextSystemControl)
{
    tElpsContextInfo   *pContextInfo = NULL;

    if (u4FsElpsContextId >= ELPS_MAX_CONTEXTS)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Context Identifier %d\r\n", u4FsElpsContextId));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsElpsContextSystemControl != ELPS_START) &&
        (i4TestValFsElpsContextSystemControl != ELPS_SHUTDOWN))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid System Control\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = ElpsCxtGetNode (u4FsElpsContextId);

    if (pContextInfo == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Context %d not created\n", u4FsElpsContextId));
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsContextModuleStatus
 Input       :  The Indices
                FsElpsContextId

                The Object 
                testValFsElpsContextModuleStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsContextModuleStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsElpsContextId,
                                    INT4 i4TestValFsElpsContextModuleStatus)
{
    if ((i4TestValFsElpsContextModuleStatus != ELPS_ENABLED) &&
        (i4TestValFsElpsContextModuleStatus != ELPS_DISABLED))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Module Status\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ElpsUtilValidateContext (u4FsElpsContextId, pu4ErrorCode)
        == OSIX_FAILURE)
    {
        /* As the function itself will take care of filling the error code, 
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsContextTraceInputString
 Input       :  The Indices
                FsElpsContextId

                The Object 
                testValFsElpsContextTraceInputString
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsContextTraceInputString (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsElpsContextId,
                                        tSNMP_OCTET_STRING_TYPE
                                        * pTestValFsElpsContextTraceInputString)
{
    UINT4               u4TrcOption = 0;

    if ((pTestValFsElpsContextTraceInputString->i4_Length >
         ELPS_TRC_MAX_SIZE) ||
        (pTestValFsElpsContextTraceInputString->i4_Length < ELPS_TRC_MIN_SIZE))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Trace Input length \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    u4TrcOption = ElpsUtilGetTraceOptionValue
        (pTestValFsElpsContextTraceInputString->pu1_OctetList,
         pTestValFsElpsContextTraceInputString->i4_Length);

    if (u4TrcOption == ELPS_INVALID_TRC)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Trace Input string \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ElpsUtilValidateContext (u4FsElpsContextId, pu4ErrorCode)
        == OSIX_FAILURE)
    {
        /* As the function itself will take care of filling the error code, 
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsContextEnableTrap
 Input       :  The Indices
                FsElpsContextId

                The Object 
                testValFsElpsContextEnableTrap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsContextEnableTrap (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsElpsContextId,
                                  INT4 i4TestValFsElpsContextEnableTrap)
{
    if ((i4TestValFsElpsContextEnableTrap != ELPS_SNMP_TRUE) &&
        (i4TestValFsElpsContextEnableTrap != ELPS_SNMP_FALSE))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Trap Status \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (ElpsUtilValidateContext (u4FsElpsContextId,
                                 pu4ErrorCode) == OSIX_FAILURE)
    {
        /* As the function itself will take care of filling the error code, 
           CLI set error and trace messages. So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsElpsContextVlanGroupManager
 Input       :  The Indices
                FsElpsContextId

                The Object 
                testValFsElpsContextVlanGroupManager
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsContextVlanGroupManager (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsElpsContextId,
                                        INT4
                                        i4TestValFsElpsContextVlanGroupManager)
{
    tElpsContextInfo   *pContextInfo = NULL;

    if ((i4TestValFsElpsContextVlanGroupManager != ELPS_VLAN_GROUP_MANAGER_MSTP)
        && (i4TestValFsElpsContextVlanGroupManager !=
            ELPS_VLAN_GROUP_MANAGER_ELPS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pContextInfo = ElpsCxtGetNode (u4FsElpsContextId);

    if (pContextInfo == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Context %d not created\n", u4FsElpsContextId));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsElpsContextTable
 Input       :  The Indices
                FsElpsContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsContextTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElpsPgConfigTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElpsPgConfigTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElpsPgConfigTable (UINT4 u4FsElpsContextId,
                                             UINT4 u4FsElpsPgConfigPgId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElpsPgConfigTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElpsPgConfigTable (UINT4 *pu4FsElpsContextId,
                                     UINT4 *pu4FsElpsPgConfigPgId)
{
    tElpsPgInfo        *pPg = NULL;

    pPg = ElpsPgGetFirstNode (pu4FsElpsContextId, pu4FsElpsPgConfigPgId);
    return (((pPg == NULL) ? SNMP_FAILURE : SNMP_SUCCESS));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElpsPgConfigTable
 Input       :  The Indices
                FsElpsContextId
                nextFsElpsContextId
                FsElpsPgConfigPgId
                nextFsElpsPgConfigPgId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElpsPgConfigTable (UINT4 u4FsElpsContextId,
                                    UINT4 *pu4NextFsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    UINT4 *pu4NextFsElpsPgConfigPgId)
{
    tElpsPgInfo        *pPg = NULL;

    pPg = ElpsPgGetNextNode (u4FsElpsContextId,
                             u4FsElpsPgConfigPgId,
                             pu4NextFsElpsContextId, pu4NextFsElpsPgConfigPgId);
    return (((pPg == NULL) ? SNMP_FAILURE : SNMP_SUCCESS));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigType (UINT4 u4FsElpsContextId,
                          UINT4 u4FsElpsPgConfigPgId,
                          INT4 *pi4RetValFsElpsPgConfigType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgConfigType = (INT4) pPgConfigInfo->u1PgConfigType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigServiceType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigServiceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigServiceType (UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 *pi4RetValFsElpsPgConfigServiceType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgConfigServiceType = (INT4) pPgConfigInfo->u1ServiceType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigMonitorMechanism
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigMonitorMechanism
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigMonitorMechanism (UINT4 u4FsElpsContextId,
                                      UINT4 u4FsElpsPgConfigPgId,
                                      INT4
                                      *pi4RetValFsElpsPgConfigMonitorMechanism)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgConfigMonitorMechanism = (INT4)
        pPgConfigInfo->u1MonitorType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigIngressPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigIngressPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigIngressPort (UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 *pi4RetValFsElpsPgConfigIngressPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgConfigIngressPort = (INT4) pPgConfigInfo->u4IngressPortId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigWorkingPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigWorkingPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigWorkingPort (UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 *pi4RetValFsElpsPgConfigWorkingPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgConfigWorkingPort = (INT4)
        pPgConfigInfo->WorkingEntity.u4PortId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigProtectionPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigProtectionPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigProtectionPort (UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    INT4 *pi4RetValFsElpsPgConfigProtectionPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgConfigProtectionPort = (INT4)
        pPgConfigInfo->ProtectionEntity.u4PortId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigWorkingServiceValue
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigWorkingServiceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigWorkingServiceValue (UINT4 u4FsElpsContextId,
                                         UINT4 u4FsElpsPgConfigPgId,
                                         UINT4
                                         *pu4RetValFsElpsPgConfigWorkingServiceValue)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgConfigWorkingServiceValue =
        pPgConfigInfo->WorkingEntity.ServiceParam.u4ServiceId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigProtectionServiceValue
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigProtectionServiceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigProtectionServiceValue (UINT4 u4FsElpsContextId,
                                            UINT4 u4FsElpsPgConfigPgId,
                                            UINT4
                                            *pu4RetValFsElpsPgConfigProtectionServiceValue)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgConfigProtectionServiceValue =
        pPgConfigInfo->ProtectionEntity.ServiceParam.u4ServiceId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigOperType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigOperType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigOperType (UINT4 u4FsElpsContextId,
                              UINT4 u4FsElpsPgConfigPgId,
                              INT4 *pi4RetValFsElpsPgConfigOperType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;
    UINT1               u1Value = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    u1Value =
        (UINT1) ELPS_GET_PG_R_BIT_VALUE (pPgConfigInfo->ApsInfo.
                                         u1ProtectionType);

    *pi4RetValFsElpsPgConfigOperType =
        ((u1Value == OSIX_TRUE) ? ELPS_PG_OPER_TYPE_REVERTIVE :
         ELPS_PG_OPER_TYPE_NONREVERTIVE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigProtType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigProtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigProtType (UINT4 u4FsElpsContextId,
                              UINT4 u4FsElpsPgConfigPgId,
                              INT4 *pi4RetValFsElpsPgConfigProtType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;
    UINT1               u1Value = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    u1Value =
        (UINT1) ELPS_GET_PG_R_BIT_VALUE (pPgConfigInfo->ApsInfo.
                                         u1ProtectionType);

    if (pPgConfigInfo->ApsInfo.u1ProtectionType == (UINT1) (ELPS_A_BIT_VALUE |
                                                            ELPS_B_BIT_VALUE |
                                                            ELPS_D_BIT_VALUE |
                                                            u1Value))
    {
        *pi4RetValFsElpsPgConfigProtType =
            ELPS_PG_PROT_TYPE_ONE_ISTO_ONE_BIDIR_APS;
    }
    else if (pPgConfigInfo->ApsInfo.u1ProtectionType ==
             (UINT1) (ELPS_A_BIT_VALUE | ELPS_D_BIT_VALUE | u1Value))
    {
        *pi4RetValFsElpsPgConfigProtType =
            ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_BIDIR_APS;
    }
    else if (pPgConfigInfo->ApsInfo.u1ProtectionType ==
             (UINT1) (ELPS_A_BIT_VALUE | u1Value))
    {
        *pi4RetValFsElpsPgConfigProtType =
            ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_APS;
    }
    else if (pPgConfigInfo->ApsInfo.u1ProtectionType ==
             (UINT1) (ELPS_ZERO | u1Value))
    {
        *pi4RetValFsElpsPgConfigProtType =
            ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_NO_APS;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigName
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigName (UINT4 u4FsElpsContextId,
                          UINT4 u4FsElpsPgConfigPgId,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsElpsPgConfigName)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pRetValFsElpsPgConfigName->i4_Length = STRLEN (pPgConfigInfo->au1PgName);
    STRNCPY (pRetValFsElpsPgConfigName->pu1_OctetList, pPgConfigInfo->au1PgName,
             pRetValFsElpsPgConfigName->i4_Length);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigRowStatus (UINT4 u4FsElpsContextId,
                               UINT4 u4FsElpsPgConfigPgId,
                               INT4 *pi4RetValFsElpsPgConfigRowStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgConfigRowStatus = (INT4) pPgConfigInfo->u1PgRowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigWorkingServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgConfigWorkingServicePointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigWorkingServicePointer (UINT4 u4FsElpsContextId,
                                           UINT4 u4FsElpsPgConfigPgId,
                                           tSNMP_OID_TYPE *
                                           pRetValFsElpsPgConfigWorkingServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        ElpsTnlGetTnlServicePointer
            (&pPgConfigInfo->WorkingEntity.ServiceParam.LspServiceInfo,
             pRetValFsElpsPgConfigWorkingServicePointer);
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        ElpsPwGetPwServicePointer (&pPgConfigInfo->WorkingEntity.ServiceParam.
                                   PwServiceInfo,
                                   pRetValFsElpsPgConfigWorkingServicePointer);
    }
    else
    {
        MEMSET (pRetValFsElpsPgConfigWorkingServicePointer->
                pu4_OidList, 0, ELPS_ERR_OID_LEN * sizeof (UINT4));
        pRetValFsElpsPgConfigWorkingServicePointer->u4_Length =
            ELPS_ERR_OID_LEN;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigWorkingReverseServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgConfigWorkingReverseServicePointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigWorkingReverseServicePointer (UINT4 u4FsElpsContextId,
                                                  UINT4 u4FsElpsPgConfigPgId,
                                                  tSNMP_OID_TYPE *
                                                  pRetValFsElpsPgConfigWorkingReverseServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        ElpsTnlGetTnlServicePointer
            (&pPgConfigInfo->WorkingReverseEntity.ServiceParam.
             LspServiceInfo, pRetValFsElpsPgConfigWorkingReverseServicePointer);
    }
    else
    {
        MEMSET (pRetValFsElpsPgConfigWorkingReverseServicePointer->
                pu4_OidList, 0, ELPS_ERR_OID_LEN * sizeof (UINT4));
        pRetValFsElpsPgConfigWorkingReverseServicePointer->u4_Length =
            ELPS_ERR_OID_LEN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigProtectionServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgConfigProtectionServicePointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigProtectionServicePointer (UINT4 u4FsElpsContextId,
                                              UINT4 u4FsElpsPgConfigPgId,
                                              tSNMP_OID_TYPE *
                                              pRetValFsElpsPgConfigProtectionServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        ElpsTnlGetTnlServicePointer
            (&pPgConfigInfo->ProtectionEntity.ServiceParam.
             LspServiceInfo, pRetValFsElpsPgConfigProtectionServicePointer);
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        ElpsPwGetPwServicePointer (&pPgConfigInfo->ProtectionEntity.
                                   ServiceParam.PwServiceInfo,
                                   pRetValFsElpsPgConfigProtectionServicePointer);
    }
    else
    {
        MEMSET (pRetValFsElpsPgConfigProtectionServicePointer->
                pu4_OidList, 0, ELPS_ERR_OID_LEN * sizeof (UINT4));
        pRetValFsElpsPgConfigProtectionServicePointer->u4_Length =
            ELPS_ERR_OID_LEN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigProtectionReverseServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgConfigProtectionReverseServicePointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigProtectionReverseServicePointer (UINT4 u4FsElpsContextId,
                                                     UINT4 u4FsElpsPgConfigPgId,
                                                     tSNMP_OID_TYPE *
                                                     pRetValFsElpsPgConfigProtectionReverseServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        ElpsTnlGetTnlServicePointer
            (&pPgConfigInfo->ProtectionReverseEntity.
             ServiceParam.LspServiceInfo,
             pRetValFsElpsPgConfigProtectionReverseServicePointer);
    }
    else
    {
        MEMSET (pRetValFsElpsPgConfigProtectionReverseServicePointer->
                pu4_OidList, 0, ELPS_ERR_OID_LEN * sizeof (UINT4));
        pRetValFsElpsPgConfigProtectionReverseServicePointer->u4_Length =
            ELPS_ERR_OID_LEN;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigWorkingInstanceId
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigWorkingInstanceId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigWorkingInstanceId (UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       UINT4
                                       *pu4RetValFsElpsPgConfigWorkingInstanceId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgConfigWorkingInstanceId =
        pPgConfigInfo->u4WorkingInstanceId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElpsPgConfigProtectionInstanceId
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgConfigProtectionInstanceId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgConfigProtectionInstanceId (UINT4 u4FsElpsContextId,
                                          UINT4 u4FsElpsPgConfigPgId,
                                          UINT4
                                          *pu4RetValFsElpsPgConfigProtectionInstanceId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgConfigProtectionInstanceId =
        pPgConfigInfo->u4ProtectionInstanceId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFsElpsPgPscVersion
 *  Input       :  The Indices
 *                 FsElpsContextId
 *                 FsElpsPgConfigPgId
 *                 The Object
 *                 retValFsElpsPgPscVersion
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *                 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsElpsPgPscVersion (UINT4 u4FsElpsContextId,
                          UINT4 u4FsElpsPgConfigPgId,
                          UINT4 *pu4RetValFsElpsPgPscVersion)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgPscVersion = (UINT4) pPgConfigInfo->u1PscVersion;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigType (UINT4 u4FsElpsContextId,
                          UINT4 u4FsElpsPgConfigPgId,
                          INT4 i4SetValFsElpsPgConfigType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsElpsPgConfigType == (INT4) pPgConfigInfo->u1PgConfigType)
    {
        return SNMP_SUCCESS;
    }

    /* ConfigTpe is changed from Individual to list/All. so Update the 
     * L2IWF to unmap the service list from u4WorkingInstanceId instance 
     * to default instance */
    if ((pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL) &&
        (pPgConfigInfo->pContextInfo->i4VlanGroupManager ==
         ELPS_VLAN_GROUP_MANAGER_ELPS))
    {

        ElpsUtilSetVlanGroupIdForVlan (u4FsElpsContextId,
                                       pPgConfigInfo->WorkingEntity.
                                       ServiceParam.u4ServiceId,
                                       ELPS_DEFAULT_VLAN_GROUP_ID);

    }

    MEMSET (&(pPgConfigInfo->WorkingEntity.ServiceParam), 0,
            sizeof (unServiceParam));
    MEMSET (&(pPgConfigInfo->ProtectionEntity.ServiceParam), 0,
            sizeof (unServiceParam));

    MEMSET (&(pPgConfigInfo->WorkingReverseEntity.ServiceParam), 0,
            sizeof (unServiceParam));
    MEMSET (&(pPgConfigInfo->ProtectionReverseEntity.ServiceParam), 0,
            sizeof (unServiceParam));

    if (i4SetValFsElpsPgConfigType == ELPS_PG_TYPE_LIST)
    {
        /* Changing the config type from individual to list */
        pPgConfigInfo->u1PgRowStatus = NOT_READY;
    }
    else
    {
        /* Changing the config type from list to individual/all. */
        /*Delele all the PG-Services */
        if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
        {
            ElpsSrvListDelAllNodesForPg (pPgConfigInfo->pContextInfo,
                                         pPgConfigInfo);

            pPgConfigInfo->unServiceList.pWorkingServiceList = NULL;
        }
        else if ((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                 (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            ElpsSrvListPtrDelAllNodesForPg (pPgConfigInfo->pContextInfo,
                                            pPgConfigInfo);

            pPgConfigInfo->unServiceList.pWorkingServiceListPointer = NULL;
        }

        pPgConfigInfo->u1PgRowStatus =
            (i4SetValFsElpsPgConfigType == ELPS_PG_TYPE_ALL) ?
            NOT_IN_SERVICE : NOT_READY;
    }

    pPgConfigInfo->u1PgConfigType = (UINT1) i4SetValFsElpsPgConfigType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigServiceType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigServiceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigServiceType (UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 i4SetValFsElpsPgConfigServiceType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == i4SetValFsElpsPgConfigServiceType)
    {
        return SNMP_SUCCESS;
    }

    if (pPgConfigInfo->WorkingEntity.pCfmConfig != NULL)
    {
        if (ElpsCfmUpdateWithServiceType (pPgConfigInfo,
                                          (UINT1)
                                          i4SetValFsElpsPgConfigServiceType) ==
            OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST)
    {
        if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
        {
            ElpsSrvListDelAllNodesForPg (pPgConfigInfo->pContextInfo,
                                         pPgConfigInfo);

            pPgConfigInfo->unServiceList.pWorkingServiceList = NULL;
        }
        else if ((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
                 (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            ElpsSrvListPtrDelAllNodesForPg (pPgConfigInfo->pContextInfo,
                                            pPgConfigInfo);

            pPgConfigInfo->unServiceList.pWorkingServiceListPointer = NULL;
        }

        pPgConfigInfo->u1PgRowStatus = NOT_READY;
    }
    else
    {
        MEMSET (&(pPgConfigInfo->WorkingEntity.ServiceParam), 0,
                sizeof (unServiceParam));
        MEMSET (&(pPgConfigInfo->ProtectionEntity.ServiceParam), 0,
                sizeof (unServiceParam));

        MEMSET (&(pPgConfigInfo->WorkingReverseEntity.ServiceParam), 0,
                sizeof (unServiceParam));
        MEMSET (&(pPgConfigInfo->ProtectionReverseEntity.ServiceParam), 0,
                sizeof (unServiceParam));
        pPgConfigInfo->u1PgRowStatus = NOT_READY;
    }

    pPgConfigInfo->u1ServiceType = (UINT1) i4SetValFsElpsPgConfigServiceType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigMonitorMechanism
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigMonitorMechanism
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigMonitorMechanism (UINT4 u4FsElpsContextId,
                                      UINT4 u4FsElpsPgConfigPgId,
                                      INT4
                                      i4SetValFsElpsPgConfigMonitorMechanism)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1MonitorType == i4SetValFsElpsPgConfigMonitorMechanism)
    {
        return SNMP_SUCCESS;
    }

    if ((i4SetValFsElpsPgConfigMonitorMechanism != ELPS_PG_MONITOR_MECH_NONE) &&
        (pPgConfigInfo->WorkingEntity.pCfmConfig != NULL))
    {
        if (ElpsCfmUpdateWithMonitorType (pPgConfigInfo, (UINT1)
                                          i4SetValFsElpsPgConfigMonitorMechanism)
            == OSIX_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    pPgConfigInfo->u1MonitorType = (UINT1)
        i4SetValFsElpsPgConfigMonitorMechanism;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigIngressPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigIngressPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigIngressPort (UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 i4SetValFsElpsPgConfigIngressPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo->u4IngressPortId = (UINT4) i4SetValFsElpsPgConfigIngressPort;

    ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigWorkingPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigWorkingPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigWorkingPort (UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 i4SetValFsElpsPgConfigWorkingPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo->WorkingEntity.u4PortId = (UINT4)
        i4SetValFsElpsPgConfigWorkingPort;

    ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigProtectionPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigProtectionPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigProtectionPort (UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    INT4 i4SetValFsElpsPgConfigProtectionPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    ElpsPgHandleProtPortConfig (pPgConfigInfo->pContextInfo, pPgConfigInfo,
                                (UINT4) i4SetValFsElpsPgConfigProtectionPort);

    ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigWorkingServiceValue
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigWorkingServiceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigWorkingServiceValue (UINT4 u4FsElpsContextId,
                                         UINT4 u4FsElpsPgConfigPgId,
                                         UINT4
                                         u4SetValFsElpsPgConfigWorkingServiceValue)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo->WorkingEntity.ServiceParam.u4ServiceId =
        u4SetValFsElpsPgConfigWorkingServiceValue;

    /* Update L2IWF with this Vlan to Instance mapping */
    if (pPgConfigInfo->pContextInfo->i4VlanGroupManager ==
        ELPS_VLAN_GROUP_MANAGER_ELPS)
    {
        ElpsUtilSetVlanGroupIdForVlan (u4FsElpsContextId,
                                       u4SetValFsElpsPgConfigWorkingServiceValue,
                                       pPgConfigInfo->u4WorkingInstanceId);
    }

    ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigProtectionServiceValue
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigProtectionServiceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigProtectionServiceValue (UINT4 u4FsElpsContextId,
                                            UINT4 u4FsElpsPgConfigPgId,
                                            UINT4
                                            u4SetValFsElpsPgConfigProtectionServiceValue)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo->ProtectionEntity.ServiceParam.u4ServiceId =
        u4SetValFsElpsPgConfigProtectionServiceValue;

    /* Update L2IWF with this Vlan to Instance mapping */
    if (pPgConfigInfo->pContextInfo->i4VlanGroupManager ==
        ELPS_VLAN_GROUP_MANAGER_ELPS)
    {
        ElpsUtilSetVlanGroupIdForVlan (u4FsElpsContextId,
                                       u4SetValFsElpsPgConfigProtectionServiceValue,
                                       pPgConfigInfo->u4ProtectionInstanceId);
    }

    ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigOperType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigOperType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigOperType (UINT4 u4FsElpsContextId,
                              UINT4 u4FsElpsPgConfigPgId,
                              INT4 i4SetValFsElpsPgConfigOperType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsElpsPgConfigOperType == ELPS_PG_OPER_TYPE_REVERTIVE)
    {
        ELPS_SET_PG_OPER_TYPE (pPgConfigInfo->ApsInfo.u1ProtectionType);
    }
    else
    {
        ELPS_RESET_PG_OPER_TYPE (pPgConfigInfo->ApsInfo.u1ProtectionType);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigProtType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigProtType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigProtType (UINT4 u4FsElpsContextId,
                              UINT4 u4FsElpsPgConfigPgId,
                              INT4 i4SetValFsElpsPgConfigProtType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;
    UINT1               u1Value = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    u1Value =
        (UINT1) ELPS_GET_PG_R_BIT_VALUE (pPgConfigInfo->ApsInfo.
                                         u1ProtectionType);

    if (i4SetValFsElpsPgConfigProtType ==
        ELPS_PG_PROT_TYPE_ONE_ISTO_ONE_BIDIR_APS)
    {
        pPgConfigInfo->ApsInfo.u1ProtectionType = (UINT1) (ELPS_A_BIT_VALUE |
                                                           ELPS_B_BIT_VALUE |
                                                           ELPS_D_BIT_VALUE |
                                                           u1Value);
    }
    else if (i4SetValFsElpsPgConfigProtType ==
             ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_BIDIR_APS)
    {
        pPgConfigInfo->ApsInfo.u1ProtectionType = (UINT1) (ELPS_A_BIT_VALUE |
                                                           ELPS_D_BIT_VALUE |
                                                           u1Value);
    }
    else if (i4SetValFsElpsPgConfigProtType ==
             ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_APS)
    {
        pPgConfigInfo->ApsInfo.u1ProtectionType = (UINT1) (ELPS_A_BIT_VALUE |
                                                           u1Value);
    }
    else if (i4SetValFsElpsPgConfigProtType ==
             ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_NO_APS)
    {
        pPgConfigInfo->ApsInfo.u1ProtectionType = (UINT1) (ELPS_ZERO | u1Value);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigName
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigName (UINT4 u4FsElpsContextId,
                          UINT4 u4FsElpsPgConfigPgId,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsElpsPgConfigName)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* The Pg Name array is memset to zero because when the string lesser 
     * than already existing name given as input, then this will prevent 
     * from presence of any junk characters in the name. If the length is 
     * zero, then reset of Pg Name is done here.
     */
    MEMSET (pPgConfigInfo->au1PgName, 0, sizeof (pPgConfigInfo->au1PgName));

    if (pSetValFsElpsPgConfigName->i4_Length != 0)
    {
        STRNCPY (pPgConfigInfo->au1PgName,
                 pSetValFsElpsPgConfigName->pu1_OctetList,
                 pSetValFsElpsPgConfigName->i4_Length);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigRowStatus (UINT4 u4FsElpsContextId,
                               UINT4 u4FsElpsPgConfigPgId,
                               INT4 i4SetValFsElpsPgConfigRowStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4Dummy = 0;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsElpsPgConfigRowStatus)
    {
        case CREATE_AND_WAIT:

            pPgConfigInfo = ElpsPgCreateNode ();
            if (pPgConfigInfo == NULL)
            {
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Cannot Create Protection Group entry %d\n",
                           u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }

            pPgConfigInfo->u4PgId = u4FsElpsPgConfigPgId;

            if (ElpsPgAddNodeToPgTable (pContextInfo, pPgConfigInfo) ==
                OSIX_FAILURE)
            {
                /* Release memory allocated for PG Node */
                MemReleaseMemBlock (gElpsGlobalInfo.PgTablePoolId,
                                    (UINT1 *) pPgConfigInfo);
                pPgConfigInfo = NULL;
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Protection Group entry %d is not created\n",
                           u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }

            /* Set the Default Values */
            pPgConfigInfo->u1PgConfigType = ELPS_PG_TYPE_INDIVIDUAL;
            pPgConfigInfo->u1ServiceType = ELPS_PG_SERVICE_TYPE_VLAN;
            pPgConfigInfo->u1MonitorType = ELPS_PG_MONITOR_MECH_CFM;

            pPgConfigInfo->ApsInfo.u1ProtectionType =
                (ELPS_A_BIT_VALUE | ELPS_B_BIT_VALUE | ELPS_D_BIT_VALUE |
                 ELPS_R_BIT_VALUE);

            pPgConfigInfo->u2HoldOffTime = ELPS_PG_MIN_HOLD_OFF_TIME;
            pPgConfigInfo->u2WtrTime = ELPS_PG_DEF_WTR_TIME;
            pPgConfigInfo->u2PeriodicTime = ELPS_PERIODIC_APS_PDU_INTERVAL;

            /*Set PSC PDU version to 0 by default */
            pPgConfigInfo->u1PscVersion = ELPS_ZERO;

            /* Reset the state event machine variables */
            ElpsSemResetSemVariables (pPgConfigInfo);

            pPgConfigInfo->u1PgStatus = ELPS_PG_STATUS_DISABLE;
            MEMSET (pPgConfigInfo->au1PgName, 0,
                    sizeof (pPgConfigInfo->au1PgName));

            /* Set the RowStatus as Not Ready, to be moved to Not In Service
             * only when the mandatory objects for the Table are set. 
             */
            pPgConfigInfo->u1PgRowStatus = NOT_READY;
            pPgConfigInfo->u1PgHwStatus = FALSE;
            break;

        case NOT_IN_SERVICE:

            pPgConfigInfo = ElpsPgGetNode (pContextInfo, u4FsElpsPgConfigPgId);

            if (pPgConfigInfo == NULL)
            {
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Protection Group entry %d is not created\n",
                           u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }

            if (pPgConfigInfo->u1PgRowStatus == NOT_IN_SERVICE)
            {
                return SNMP_SUCCESS;
            }

            if (pContextInfo->u1ModuleStatus == ELPS_ENABLED)
            {
                /* Delete the Protection Group Properties from Hardware */
                ElpsPgDeActivate (pContextInfo, pPgConfigInfo);
                nmhSetFsElpsPgCmdExtCmd (pContextInfo->u4ContextId,
                                         pPgConfigInfo->u4PgId,
                                         ELPS_EXT_CMD_CLR);
            }
            else
            {
                /* ELPS module is disabled. in this context so just update the
                 * the required variables. Cant call ElpsPgDeActivate() now,
                 * this will be called when module will be enabled if needed.*/

                /* Reset the state event machine variables */
                ElpsSemResetSemVariables (pPgConfigInfo);

                /* Update the PG status */
                pPgConfigInfo->u1PgStatus = ELPS_PG_STATUS_DISABLE;
            }

            pPgConfigInfo->u1PgRowStatus = NOT_IN_SERVICE;

            break;

        case DESTROY:

            pPgConfigInfo = ElpsPgGetNode (pContextInfo, u4FsElpsPgConfigPgId);

            if (pPgConfigInfo == NULL)
            {
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Protection Group entry %d is not created\n",
                           u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }

            ElpsPgDelNode (pPgConfigInfo);
            break;

        case ACTIVE:

            pPgConfigInfo = ElpsPgGetNode (pContextInfo, u4FsElpsPgConfigPgId);

            if (pPgConfigInfo == NULL)
            {
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Protection Group entry %d is not created\n",
                           u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }

            if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }

            if (pContextInfo->u1ModuleStatus == ELPS_ENABLED)
            {
                if (ElpsPgActivate (pContextInfo, pPgConfigInfo) ==
                    OSIX_FAILURE)
                {
                    ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                               "Unable to activate Protection Group entry %d \n",
                               u4FsElpsPgConfigPgId));
                    return SNMP_FAILURE;
                }
            }
            else
            {
                /* ELPS module is disabled in this context so just update the
                 * required variables. Cant call ElpsPgActivate() now,
                 * this will be called when module will be enabled. */

                /* Reset the state event machine variables */
                ElpsSemResetSemVariables (pPgConfigInfo);

                /* As the module status is disabled so PG status should also
                 * show as Disabled */
                pPgConfigInfo->u1PgStatus = ELPS_PG_STATUS_DISABLE;
            }

            pPgConfigInfo->u1PgRowStatus = ACTIVE;

            break;

        default:

            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigWorkingServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgConfigWorkingServicePointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigWorkingServicePointer (UINT4 u4FsElpsContextId,
                                           UINT4 u4FsElpsPgConfigPgId,
                                           tSNMP_OID_TYPE *
                                           pSetValFsElpsPgConfigWorkingServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        /* Tunnel Index */
        pPgConfigInfo->WorkingEntity.ServiceParam.LspServiceInfo.u4TnlIndex =
            pSetValFsElpsPgConfigWorkingServicePointer->
            pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];

        /* Tunnel Instance */
        pPgConfigInfo->WorkingEntity.ServiceParam.LspServiceInfo.u4TnlInstance =
            pSetValFsElpsPgConfigWorkingServicePointer->
            pu4_OidList[ELPS_TNL_INST_START_OFFSET];

        /* IngressLsrId */
        pPgConfigInfo->WorkingEntity.ServiceParam.
            LspServiceInfo.u4TnlIngressLsrId =
            pSetValFsElpsPgConfigWorkingServicePointer->
            pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];

        /* EgressLsrId */
        pPgConfigInfo->WorkingEntity.ServiceParam.
            LspServiceInfo.u4TnlEgressLsrId =
            pSetValFsElpsPgConfigWorkingServicePointer->
            pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        if (ElpsPwSetPwServiceInfo
            (&pPgConfigInfo->WorkingEntity.ServiceParam.PwServiceInfo,
             pSetValFsElpsPgConfigWorkingServicePointer) == OSIX_FAILURE)
        {
            ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                       "nmhSetFsElpsPgConfigWorkingServicePointer:"
                       " Allocation of memory for "
                       "Mpls In Api Info FAILED!!!\r\n"));
            return SNMP_FAILURE;
        }
    }

    ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigWorkingReverseServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgConfigWorkingReverseServicePointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigWorkingReverseServicePointer (UINT4 u4FsElpsContextId,
                                                  UINT4 u4FsElpsPgConfigPgId,
                                                  tSNMP_OID_TYPE *
                                                  pSetValFsElpsPgConfigWorkingReverseServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Tunnel Index */
    pPgConfigInfo->WorkingReverseEntity.ServiceParam.
        LspServiceInfo.u4TnlIndex =
        pSetValFsElpsPgConfigWorkingReverseServicePointer->
        pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];

    /* Tunnel Instance */
    pPgConfigInfo->WorkingReverseEntity.ServiceParam.
        LspServiceInfo.u4TnlInstance =
        pSetValFsElpsPgConfigWorkingReverseServicePointer->
        pu4_OidList[ELPS_TNL_INST_START_OFFSET];

    /* IngressLsrId */
    pPgConfigInfo->WorkingReverseEntity.ServiceParam.
        LspServiceInfo.u4TnlIngressLsrId =
        pSetValFsElpsPgConfigWorkingReverseServicePointer->
        pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];

    /* EgressLsrId */
    pPgConfigInfo->WorkingReverseEntity.ServiceParam.
        LspServiceInfo.u4TnlEgressLsrId =
        pSetValFsElpsPgConfigWorkingReverseServicePointer->
        pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigProtectionServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgConfigProtectionServicePointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigProtectionServicePointer (UINT4 u4FsElpsContextId,
                                              UINT4 u4FsElpsPgConfigPgId,
                                              tSNMP_OID_TYPE *
                                              pSetValFsElpsPgConfigProtectionServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);
    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        /* Tunnel Index */
        pPgConfigInfo->ProtectionEntity.ServiceParam.LspServiceInfo.u4TnlIndex =
            pSetValFsElpsPgConfigProtectionServicePointer->
            pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];

        /* Tunnel Instance */
        pPgConfigInfo->ProtectionEntity.ServiceParam.
            LspServiceInfo.u4TnlInstance =
            pSetValFsElpsPgConfigProtectionServicePointer->
            pu4_OidList[ELPS_TNL_INST_START_OFFSET];

        /* IngressLsrId */
        pPgConfigInfo->ProtectionEntity.ServiceParam.
            LspServiceInfo.u4TnlIngressLsrId =
            pSetValFsElpsPgConfigProtectionServicePointer->
            pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];

        /* EgressLsrId */
        pPgConfigInfo->ProtectionEntity.ServiceParam.
            LspServiceInfo.u4TnlEgressLsrId =
            pSetValFsElpsPgConfigProtectionServicePointer->
            pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        if (ElpsPwSetPwServiceInfo
            (&pPgConfigInfo->ProtectionEntity.ServiceParam.PwServiceInfo,
             pSetValFsElpsPgConfigProtectionServicePointer) == OSIX_FAILURE)
        {
            ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                       "nmhSetFsElpsPgConfigProtectionServicePointer:"
                       " Allocation of memory for "
                       "Mpls In Api Info FAILED!!!\r\n"));
            return SNMP_FAILURE;
        }
    }
    ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigProtectionReverseServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgConfigProtectionReverseServicePointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigProtectionReverseServicePointer (UINT4 u4FsElpsContextId,
                                                     UINT4 u4FsElpsPgConfigPgId,
                                                     tSNMP_OID_TYPE *
                                                     pSetValFsElpsPgConfigProtectionReverseServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);
    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Tunnel Index */
    pPgConfigInfo->ProtectionReverseEntity.ServiceParam.
        LspServiceInfo.u4TnlIndex =
        pSetValFsElpsPgConfigProtectionReverseServicePointer->
        pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];

    /* Tunnel Instance */
    pPgConfigInfo->ProtectionReverseEntity.ServiceParam.
        LspServiceInfo.u4TnlInstance =
        pSetValFsElpsPgConfigProtectionReverseServicePointer->
        pu4_OidList[ELPS_TNL_INST_START_OFFSET];

    /* IngressLsrId */
    pPgConfigInfo->ProtectionReverseEntity.ServiceParam.
        LspServiceInfo.u4TnlIngressLsrId =
        pSetValFsElpsPgConfigProtectionReverseServicePointer->
        pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];

    /* EgressLsrId */
    pPgConfigInfo->ProtectionReverseEntity.ServiceParam.
        LspServiceInfo.u4TnlEgressLsrId =
        pSetValFsElpsPgConfigProtectionReverseServicePointer->
        pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigWorkingInstanceId
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigWorkingInstanceId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigWorkingInstanceId (UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       UINT4
                                       u4SetValFsElpsPgConfigWorkingInstanceId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;
    tElpsPgInfo        *pPg = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4NextContextId = 0;
    UINT4               u4PgId = 0;
    UINT4               u4NextPgId = 0;

    if (u4SetValFsElpsPgConfigWorkingInstanceId != 0)
    {
        pPg = ElpsPgGetFirstNode (&u4NextContextId, &u4NextPgId);

        if (pPg != NULL)
        {
            do
            {
                if ((pPg->u4WorkingInstanceId == u4SetValFsElpsPgConfigWorkingInstanceId) &&
                    (pPg->u4PgId != u4FsElpsPgConfigPgId) && (u4FsElpsContextId == u4NextContextId))
                {
                    ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                               "Working group Id already configured for other"
                               "Protection Group entry \r\n "));
                   /* *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;*/
                    CLI_SET_ERR (CLI_ELPS_ERR_GROUP_ID_EXIST);                
                    return SNMP_FAILURE;
                }
                u4ContextId = u4NextContextId;
                u4PgId = u4NextPgId;

            }
            while ((pPg = ElpsPgGetNextNode (u4ContextId,
                                             u4PgId,
                                             &u4NextContextId,
                                             &u4NextPgId)) != NULL);
        }
    }
    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);
    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo->u4WorkingInstanceId =
        u4SetValFsElpsPgConfigWorkingInstanceId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsElpsPgConfigProtectionInstanceId
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgConfigProtectionInstanceId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgConfigProtectionInstanceId (UINT4 u4FsElpsContextId,
                                          UINT4 u4FsElpsPgConfigPgId,
                                          UINT4
                                          u4SetValFsElpsPgConfigProtectionInstanceId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);
    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo->u4ProtectionInstanceId =
        u4SetValFsElpsPgConfigProtectionInstanceId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhSetFsElpsPgPscVersion
 *  Input       :  The Indices
 *                 FsElpsContextId
 *                 FsElpsPgConfigPgId
 *                 The Object
 *                 setValFsElpsPgPscVersion
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsElpsPgPscVersion (UINT4 u4FsElpsContextId,
                          UINT4 u4FsElpsPgConfigPgId,
                          UINT4 u4SetValFsElpsPgPscVersion)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;
    UINT4               u4PrevChannelCode = gElpsGlobalInfo.u4PscChannelCode;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);
    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PscVersion == (UINT1) u4SetValFsElpsPgPscVersion)
    {
        return SNMP_SUCCESS;
    }

    pPgConfigInfo->u1PscVersion = (UINT1) u4SetValFsElpsPgPscVersion;
    if (pPgConfigInfo->u1PscVersion == ELPS_ONE)
    {
        gElpsGlobalInfo.u4PscChannelCode = 0x24;
        if (ElpsPortSendChannelCode (gElpsGlobalInfo.u4PscChannelCode) ==
            OSIX_FAILURE)
        {
            gElpsGlobalInfo.u4PscChannelCode = u4PrevChannelCode;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigType (UINT4 *pu4ErrorCode,
                             UINT4 u4FsElpsContextId,
                             UINT4 u4FsElpsPgConfigPgId,
                             INT4 i4TestValFsElpsPgConfigType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((i4TestValFsElpsPgConfigType != ELPS_PG_TYPE_INDIVIDUAL) &&
        (i4TestValFsElpsPgConfigType != ELPS_PG_TYPE_LIST) &&
        (i4TestValFsElpsPgConfigType != ELPS_PG_TYPE_ALL))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
         (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)) &&
        (i4TestValFsElpsPgConfigType == ELPS_PG_TYPE_ALL))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type 'ALL' is not valid"
                   "for LSP/PW services\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_ALL_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigServiceType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigServiceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigServiceType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    INT4 i4TestValFsElpsPgConfigServiceType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((i4TestValFsElpsPgConfigServiceType != ELPS_PG_SERVICE_TYPE_VLAN) &&
        (i4TestValFsElpsPgConfigServiceType != ELPS_PG_SERVICE_TYPE_LSP) &&
        (i4TestValFsElpsPgConfigServiceType != ELPS_PG_SERVICE_TYPE_PW))

    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service Type is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4FsElpsContextId != 0) &&
        ((i4TestValFsElpsPgConfigServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
         (i4TestValFsElpsPgConfigServiceType == ELPS_PG_SERVICE_TYPE_PW)))

    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service Type MPLS-LSP/PW"
                   " is supported only in default context\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_SUPPORT_IN_DEFAULT_CXT);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service Type cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigMonitorMechanism
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigMonitorMechanism
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigMonitorMechanism (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsElpsContextId,
                                         UINT4 u4FsElpsPgConfigPgId,
                                         INT4
                                         i4TestValFsElpsPgConfigMonitorMechanism)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((i4TestValFsElpsPgConfigMonitorMechanism != ELPS_PG_MONITOR_MECH_CFM) &&
        (i4TestValFsElpsPgConfigMonitorMechanism !=
         ELPS_PG_MONITOR_MECH_MPLSOAM) &&
        (i4TestValFsElpsPgConfigMonitorMechanism != ELPS_PG_MONITOR_MECH_NONE))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Monitoring Mechanism is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsElpsPgConfigMonitorMechanism == ELPS_PG_MONITOR_MECH_NONE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Monitoring Mechanism None is not supported"
                   "for Bidirectional Protection switching with APS/PSC \n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Montior Mechanism cannot be"
                   " configured when PG is Active\n"));
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigIngressPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigIngressPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigIngressPort (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    INT4 i4TestValFsElpsPgConfigIngressPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    if ((i4TestValFsElpsPgConfigIngressPort < 0) ||
        (i4TestValFsElpsPgConfigIngressPort >
         BRG_MAX_PHY_PLUS_LAG_INT_PORTS_EXT))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Ingress Port number is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Ingress Port cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsElpsPgConfigIngressPort != 0) &&
        ((ElpsPortVcmGetCxtInfoFromIfIndex
          (i4TestValFsElpsPgConfigIngressPort,
           &u4ContextId, &u2LocalPortId) ==
          OSIX_FAILURE) || (u4ContextId != u4FsElpsContextId)))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Port is not mapped to this context\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PORT_UNMAPPED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigWorkingPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigWorkingPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigWorkingPort (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    INT4 i4TestValFsElpsPgConfigWorkingPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
         (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)) &&
        (i4TestValFsElpsPgConfigWorkingPort != 0))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Port number is not valid"
                   "for LSP & PW Services\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        if ((i4TestValFsElpsPgConfigWorkingPort <= 0) ||
            (i4TestValFsElpsPgConfigWorkingPort >
             BRG_MAX_PHY_PLUS_LAG_INT_PORTS_EXT))
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Working Port number is not valid\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Port cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if ((ElpsPortVcmGetCxtInfoFromIfIndex
         (i4TestValFsElpsPgConfigWorkingPort,
          &u4ContextId, &u2LocalPortId) ==
         OSIX_FAILURE) || (u4ContextId != u4FsElpsContextId))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Port is not mapped to this context\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PORT_UNMAPPED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigProtectionPort
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigProtectionPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigProtectionPort (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       INT4
                                       i4TestValFsElpsPgConfigProtectionPort)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2LocalPortId = 0;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
         (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)) &&
        (i4TestValFsElpsPgConfigProtectionPort != 0))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection Port number is not valid"
                   "for LSP & PW Services\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        if ((i4TestValFsElpsPgConfigProtectionPort <= 0) ||
            (i4TestValFsElpsPgConfigProtectionPort >
             BRG_MAX_PHY_PLUS_LAG_INT_PORTS_EXT))
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Protection Port number is not valid\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection Port cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if ((ElpsPortVcmGetCxtInfoFromIfIndex
         (i4TestValFsElpsPgConfigProtectionPort,
          &u4ContextId, &u2LocalPortId) ==
         OSIX_FAILURE) || (u4ContextId != u4FsElpsContextId))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Port is not mapped to this context\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PORT_UNMAPPED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigWorkingServiceValue
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigWorkingServiceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigWorkingServiceValue (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsElpsContextId,
                                            UINT4 u4FsElpsPgConfigPgId,
                                            UINT4
                                            u4TestValFsElpsPgConfigWorkingServiceValue)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Currently supported Service is Vlan/LSp/PW and to be updated on 
     * further addition to service type. 
     */
    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_VLAN)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Value"
                   " is supported only for VLAN services\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_SRV_VAL_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        if ((u4TestValFsElpsPgConfigWorkingServiceValue == 0) ||
            (u4TestValFsElpsPgConfigWorkingServiceValue >
             ELPS_SYS_MAX_NUM_SERVICE_IN_LIST_EXT))
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Working Service is not valid\r\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if ((pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST) ||
             (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_ALL))
    {
        if (u4TestValFsElpsPgConfigWorkingServiceValue != 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Config Type is list / all"
                       "but Working Service Id is not set to 0\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is not set properly\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigProtectionServiceValue
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigProtectionServiceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigProtectionServiceValue (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsElpsContextId,
                                               UINT4 u4FsElpsPgConfigPgId,
                                               UINT4
                                               u4TestValFsElpsPgConfigProtectionServiceValue)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    /* Currently supported Service is Vlan and to be updated on 
     * further addition to service type. 
     */
    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_VLAN)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection Service "
                   "Value is supported only for VLAN services\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_SRV_VAL_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        if ((u4TestValFsElpsPgConfigProtectionServiceValue <= 0) ||
            (u4TestValFsElpsPgConfigProtectionServiceValue >
             ELPS_SYS_MAX_NUM_SERVICE_IN_LIST_EXT))
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Protection Service is not valid\r\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if ((pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST) ||
             (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_ALL))
    {
        if (u4TestValFsElpsPgConfigProtectionServiceValue != 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Config Type is list / all"
                       "but Working Service Id is not set to 0\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is not " "set properly\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection Service cannot be"
                   " configured when PG is Active\n"));
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigOperType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigOperType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigOperType (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 i4TestValFsElpsPgConfigOperType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((i4TestValFsElpsPgConfigOperType != ELPS_PG_OPER_TYPE_REVERTIVE) &&
        (i4TestValFsElpsPgConfigOperType != ELPS_PG_OPER_TYPE_NONREVERTIVE))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Operation Type is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Oper Type cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigProtType
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigProtType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigProtType (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 i4TestValFsElpsPgConfigProtType)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (!((i4TestValFsElpsPgConfigProtType ==
           ELPS_PG_PROT_TYPE_ONE_ISTO_ONE_BIDIR_APS) ||
          (i4TestValFsElpsPgConfigProtType ==
           ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_BIDIR_APS) ||
          (i4TestValFsElpsPgConfigProtType ==
           ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_APS) ||
          (i4TestValFsElpsPgConfigProtType ==
           ELPS_PG_PROT_TYPE_ONE_PLUS_ONE_UNIDIR_NO_APS)))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection Type is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection Type cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigName
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigName (UINT4 *pu4ErrorCode, UINT4 u4FsElpsContextId,
                             UINT4 u4FsElpsPgConfigPgId,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsElpsPgConfigName)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4PgId = 0;

    if ((pTestValFsElpsPgConfigName->i4_Length < 0) ||
        (pTestValFsElpsPgConfigName->i4_Length > ELPS_PG_MAX_NAME_LEN))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Name string length is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (ElpsPortSNMPCheckForNVTChars (pTestValFsElpsPgConfigName->pu1_OctetList,
                                      pTestValFsElpsPgConfigName->i4_Length) ==
        OSIX_FAILURE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Name string is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pTestValFsElpsPgConfigName->i4_Length == 0)
    {
        /* Resetting the Pg Name to NULL, hence no need to validate Pg Name
         * availability.
         */
        return SNMP_SUCCESS;
    }

    u4PgId = ElpsUtilGetPgIdFromPgName (u4FsElpsContextId,
                                        pTestValFsElpsPgConfigName->
                                        pu1_OctetList);

    if ((u4PgId != 0) && (u4PgId != u4FsElpsPgConfigPgId))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Name string is already used for Pg %d\n",
                   u4PgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PGNAME_ALREADY_USED);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsElpsContextId,
                                  UINT4 u4FsElpsPgConfigPgId,
                                  INT4 i4TestValFsElpsPgConfigRowStatus)
{
    tElpsServiceListInfo *pServiceListInfo = NULL;
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    tElpsContextInfo   *pContextInfo = NULL;

    if (u4FsElpsPgConfigPgId == 0)
    {
        CLI_SET_ERR (CLI_ELPS_ERR_INVALID_PG_ID);
        return SNMP_FAILURE;
    }
    pContextInfo =
        ElpsUtilValidateContextInfo (u4FsElpsContextId, pu4ErrorCode);

    if (pContextInfo == NULL)
    {
        /* As the function ElpsUtilValidateContextInfo itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsPgGetNode (pContextInfo, u4FsElpsPgConfigPgId);

    if ((pPgConfigInfo == NULL) &&
        ((i4TestValFsElpsPgConfigRowStatus == NOT_IN_SERVICE) ||
         (i4TestValFsElpsPgConfigRowStatus == ACTIVE)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    switch (i4TestValFsElpsPgConfigRowStatus)
    {
            /* Intentional Fall-Thorugh */
        case CREATE_AND_WAIT:
            if (pPgConfigInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Protection Group %d is already created\n",
                           u4FsElpsPgConfigPgId));
                CLI_SET_ERR (CLI_ELPS_ERR_PG_ALREADY_CREATED);
                return SNMP_FAILURE;
            }
            break;
        case DESTROY:
            break;
        case NOT_IN_SERVICE:
            if (pPgConfigInfo->u1PgRowStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Protection Group %d cannot move from Not "
                           "Ready to Not In Service\n", u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:

            /* The RowStatus can be set to Active only if all the mandatory 
             * objects in the Table are set. When they are not set, the 
             * RowStatus will be in Not Ready State. RowStatus can move to 
             * Active from Not In Service State only 
             */

            if (pPgConfigInfo->u1PgRowStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Protection Group %d is not ready to be activated\n",
                           u4FsElpsPgConfigPgId));
                CLI_SET_ERR (CLI_ELPS_ERR_PG_NOT_READY);
                return SNMP_FAILURE;
            }

            if ((pPgConfigInfo->u1MonitorType == ELPS_PG_MONITOR_MECH_CFM) &&
                (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_VLAN))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Monitoring method for Protection Group %d is "
                           "not consistent with Service Type\n",
                           u4FsElpsPgConfigPgId));
                CLI_SET_ERR (CLI_ELPS_ERR_MONITOR_INCONSISTENT);
                return SNMP_FAILURE;
            }

            if ((pPgConfigInfo->u1MonitorType == ELPS_PG_MONITOR_MECH_MPLSOAM)
                && ((pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
                    &&
                    (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_PW)))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                           "Monitoring method for Protection Group %d is "
                           "not consistent with Service Type\n",
                           u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }

            /* PG CFM Table entry should already be active to make this entry 
             * active 
             */
            if ((pPgConfigInfo->u1MonitorType == ELPS_PG_MONITOR_MECH_CFM) ||
                (pPgConfigInfo->u1MonitorType == ELPS_PG_MONITOR_MECH_MPLSOAM))

            {
                if (pPgConfigInfo->u1CfmRowStatus != ACTIVE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                               "CFM Table entry for Protection Group %d is "
                               "not active\n", u4FsElpsPgConfigPgId));
                    CLI_SET_ERR (CLI_ELPS_ERR_PG_CFM_ENTRY_NOT_ACTIVE);
                    return SNMP_FAILURE;
                }
            }

            if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST)
            {
                /* Service List Entry must be created and made active for a 
                 * Protection Group which is of type LIST.
                 */
                if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
                {
                    pServiceListInfo =
                        pPgConfigInfo->unServiceList.pWorkingServiceList;

                    if ((pServiceListInfo == NULL) ||
                        (pServiceListInfo->u1RowStatus != ACTIVE))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                                   "Service List entry for List Protection "
                                   "Group %d is not created/active \n",
                                   u4FsElpsPgConfigPgId));
                        CLI_SET_ERR
                            (CLI_ELPS_ERR_PG_SERVICE_LIST_ENTRY_NOT_CREATED);
                        return SNMP_FAILURE;
                    }

                    /* The Working and Protection Service Values should not be 
                     * configured in PG Config Table for a Protection Group 
                     * which is of type LIST.
                     */
                    if ((pPgConfigInfo->WorkingEntity.ServiceParam.
                         u4ServiceId != 0)
                        || (pPgConfigInfo->ProtectionEntity.ServiceParam.
                            u4ServiceId != 0))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        ELPS_TRC ((pContextInfo, MGMT_TRC |
                                   ALL_FAILURE_TRC,
                                   "Working or Protection Service entity "
                                   "should not be configured for List "
                                   "Protection Group %d in PG Config Table\n",
                                   u4FsElpsPgConfigPgId));
                        return SNMP_FAILURE;
                    }
                }
                else if ((pPgConfigInfo->u1ServiceType ==
                          ELPS_PG_SERVICE_TYPE_LSP) ||
                         (pPgConfigInfo->u1ServiceType ==
                          ELPS_PG_SERVICE_TYPE_PW))
                {
                    pServiceListPointerInfo =
                        pPgConfigInfo->unServiceList.pWorkingServiceListPointer;

                    if ((pServiceListPointerInfo == NULL) ||
                        (pServiceListPointerInfo->u1RowStatus != ACTIVE))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                                   "Service List entry for List Protection "
                                   "Group %d is not created/active \n",
                                   u4FsElpsPgConfigPgId));
                        CLI_SET_ERR
                            (CLI_ELPS_ERR_PG_SERVICE_LIST_ENTRY_NOT_CREATED);
                        return SNMP_FAILURE;
                    }

                    /* The Working and Protection Service Values should not be
                     * configured in PG Config Table for a Protection Group 
                     * which is of type LIST.
                     */
                    if (pPgConfigInfo->u1ServiceType ==
                        ELPS_PG_SERVICE_TYPE_LSP)
                    {
                        if ((pPgConfigInfo->WorkingEntity.ServiceParam.
                             LspServiceInfo.u4TnlIngressLsrId != 0)
                            || (pPgConfigInfo->ProtectionEntity.ServiceParam.
                                LspServiceInfo.u4TnlIngressLsrId != 0))
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            ELPS_TRC ((pContextInfo, MGMT_TRC |
                                       ALL_FAILURE_TRC,
                                       "Working or Protection Service entity "
                                       "should not be configured for List "
                                       "Protection Group %d in PG "
                                       "Config Table\n", u4FsElpsPgConfigPgId));
                            return SNMP_FAILURE;
                        }
                    }
                    else if (pPgConfigInfo->u1ServiceType ==
                             ELPS_PG_SERVICE_TYPE_PW)
                    {
                        if ((pPgConfigInfo->WorkingEntity.ServiceParam.
                             PwServiceInfo.u4PwIndex != 0)
                            || (pPgConfigInfo->ProtectionEntity.ServiceParam.
                                PwServiceInfo.u4PwIndex != 0))
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            ELPS_TRC ((pContextInfo, MGMT_TRC |
                                       ALL_FAILURE_TRC,
                                       "Working or Protection Service entity "
                                       "should not be configured for List "
                                       "Protection Group %d in PG "
                                       "Config Table\n", u4FsElpsPgConfigPgId));
                            return SNMP_FAILURE;
                        }
                    }

                }
            }

            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigWorkingServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgConfigWorkingServicePointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigWorkingServicePointer (UINT4 *pu4ErrorCode,
                                              UINT4 u4FsElpsContextId,
                                              UINT4 u4FsElpsPgConfigPgId,
                                              tSNMP_OID_TYPE *
                                              pTestValFsElpsPgConfigWorkingServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if ((pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP) &&
        (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_PW))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Pointer "
                   "is not supported\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_SRV_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            /* Need to validate the Tunnel Table Base Oid */
            if (MEMCMP
                (pTestValFsElpsPgConfigWorkingServicePointer->pu4_OidList,
                 gau4TnlTableOid,
                 ((ELPS_TNL_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if ((pTestValFsElpsPgConfigWorkingServicePointer->
                 pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] == 0) ||
                (pTestValFsElpsPgConfigWorkingServicePointer->
                 pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] == 0))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
        {
            /* Need to validate the PW Table Base Oid */
            if (MEMCMP
                (pTestValFsElpsPgConfigWorkingServicePointer->pu4_OidList,
                 gau4PwTableOid,
                 ((ELPS_PW_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    else if ((pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST) ||
             (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_ALL))
    {
        if (pTestValFsElpsPgConfigWorkingServicePointer->u4_Length != 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Config Type is list / all"
                       "but Working Service Pointer Id is not set to 0\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is not set properly\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Pointer cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigWorkingReverseServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgConfigWorkingReverseServicePointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigWorkingReverseServicePointer (UINT4 *pu4ErrorCode,
                                                     UINT4 u4FsElpsContextId,
                                                     UINT4 u4FsElpsPgConfigPgId,
                                                     tSNMP_OID_TYPE *
                                                     pTestValFsElpsPgConfigWorkingReverseServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Pointer "
                   "is not supported for this Service\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_SRV_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        /* Need to validate the Tunnel Table Base Oid */
        if (MEMCMP
            (pTestValFsElpsPgConfigWorkingReverseServicePointer->
             pu4_OidList, gau4TnlTableOid,
             ((ELPS_TNL_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((pTestValFsElpsPgConfigWorkingReverseServicePointer->
             pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] == 0) ||
            (pTestValFsElpsPgConfigWorkingReverseServicePointer->
             pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] == 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if ((pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST) ||
             (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_ALL))
    {
        if (pTestValFsElpsPgConfigWorkingReverseServicePointer->u4_Length != 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Config Type is list / all"
                       "but Working Service Pointer Id is not set to 0\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is not set properly\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Pointer cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigProtectionServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgConfigProtectionServicePointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigProtectionServicePointer (UINT4 *pu4ErrorCode,
                                                 UINT4 u4FsElpsContextId,
                                                 UINT4 u4FsElpsPgConfigPgId,
                                                 tSNMP_OID_TYPE *
                                                 pTestValFsElpsPgConfigProtectionServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if ((pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP) &&
        (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_PW))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Pointer "
                   "is not supported\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_SRV_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
        {
            /* Need to validate the Tunnel Table Base Oid */
            if (MEMCMP
                (pTestValFsElpsPgConfigProtectionServicePointer->pu4_OidList,
                 gau4TnlTableOid,
                 ((ELPS_TNL_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            if ((pTestValFsElpsPgConfigProtectionServicePointer->
                 pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] == 0) ||
                (pTestValFsElpsPgConfigProtectionServicePointer->
                 pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] == 0))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
        }
        else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
        {
            /* Need to validate the PW Table Base Oid */
            if (MEMCMP
                (pTestValFsElpsPgConfigProtectionServicePointer->pu4_OidList,
                 gau4PwTableOid,
                 ((ELPS_PW_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

        }

    }
    else if ((pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST) ||
             (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_ALL))
    {
        if (pTestValFsElpsPgConfigProtectionServicePointer->u4_Length != 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Config Type is list / all"
                       "but Working Service Pointer Id is not set to 0\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is not set properly\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Pointer cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigProtectionReverseServicePointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgConfigProtectionReverseServicePointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigProtectionReverseServicePointer (UINT4 *pu4ErrorCode,
                                                        UINT4 u4FsElpsContextId,
                                                        UINT4
                                                        u4FsElpsPgConfigPgId,
                                                        tSNMP_OID_TYPE *
                                                        pTestValFsElpsPgConfigProtectionReverseServicePointer)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Pointer is "
                   "not supported for this service\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_SRV_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        /* Need to validate the Tunnel Table Base Oid */
        if (MEMCMP
            (pTestValFsElpsPgConfigProtectionReverseServicePointer->
             pu4_OidList, gau4TnlTableOid,
             ((ELPS_TNL_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        if ((pTestValFsElpsPgConfigProtectionReverseServicePointer->
             pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] == 0) ||
            (pTestValFsElpsPgConfigProtectionReverseServicePointer->
             pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] == 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if ((pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_LIST) ||
             (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_ALL))
    {
        if (pTestValFsElpsPgConfigProtectionReverseServicePointer->
            u4_Length != 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Config Type is list / all"
                       "but Working Service Pointer Id is not set to 0\r\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is not set properly\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Service Pointer cannot be"
                   " configured when PG is Active\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigWorkingInstanceId
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigWorkingInstanceId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigWorkingInstanceId (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsElpsContextId,
                                          UINT4 u4FsElpsPgConfigPgId,
                                          UINT4
                                          u4TestValFsElpsPgConfigWorkingInstanceId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (u4TestValFsElpsPgConfigWorkingInstanceId > ELPS_MAX_VLAN_GROUP_ID)
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                   "Working group id configured for Protection Group entry"
                   " %d is not valid \r\n ", u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                   "working Group Id can't be mapped with the Protection Group "
                   "%d while the row status is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgConfigProtectionInstanceId
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgConfigProtectionInstanceId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgConfigProtectionInstanceId (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsElpsContextId,
                                             UINT4 u4FsElpsPgConfigPgId,
                                             UINT4
                                             u4TestValFsElpsPgConfigProtectionInstanceId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (u4TestValFsElpsPgConfigProtectionInstanceId > ELPS_MAX_VLAN_GROUP_ID)
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                   "Protection group id configured for Protection Group entry"
                   " %d is not valid \r\n ", u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                   "Protection Group Id can't be mapped with the Protection Group "
                   "%d while the row status is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhTestv2FsElpsPgPscVersion
 *  Input       :  The Indices
 *                 FsElpsContextId
 *                 FsElpsPgConfigPgId
 *                 The Object
 *                 testValFsElpsPgPscVersion
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *  Error Codes :  The following error codes are to be returned
 *                 SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                 SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FsElpsPgPscVersion (UINT4 *pu4ErrorCode,
                             UINT4 u4FsElpsContextId,
                             UINT4 u4FsElpsPgConfigPgId,
                             UINT4 u4TestValFsElpsPgPscVersion)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (u4TestValFsElpsPgPscVersion > ELPS_ONE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                   "Protection group PSC Version configured for Protection Group entry"
                   " %d is not valid \r\n ", u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                   "Protection Group Id can't be mapped with the Protection Group "
                   "%d while the row status is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhDepv2FsElpsPgConfigTable
Input       :  The Indices
FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsPgConfigTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElpsPgCmdTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElpsPgCmdTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElpsPgCmdTable (UINT4 u4FsElpsContextId,
                                          UINT4 u4FsElpsPgConfigPgId)
{
    return (nmhValidateIndexInstanceFsElpsPgConfigTable (u4FsElpsContextId,
                                                         u4FsElpsPgConfigPgId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElpsPgCmdTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElpsPgCmdTable (UINT4 *pu4FsElpsContextId,
                                  UINT4 *pu4FsElpsPgConfigPgId)
{
    return (nmhGetFirstIndexFsElpsPgConfigTable (pu4FsElpsContextId,
                                                 pu4FsElpsPgConfigPgId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElpsPgCmdTable
 Input       :  The Indices
                FsElpsContextId
                nextFsElpsContextId
                FsElpsPgConfigPgId
                nextFsElpsPgConfigPgId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElpsPgCmdTable (UINT4 u4FsElpsContextId,
                                 UINT4 *pu4NextFsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 UINT4 *pu4NextFsElpsPgConfigPgId)
{
    return (nmhGetNextIndexFsElpsPgConfigTable (u4FsElpsContextId,
                                                pu4NextFsElpsContextId,
                                                u4FsElpsPgConfigPgId,
                                                pu4NextFsElpsPgConfigPgId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdHoTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdHoTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdHoTime (UINT4 u4FsElpsContextId,
                         UINT4 u4FsElpsPgConfigPgId,
                         UINT4 *pu4RetValFsElpsPgCmdHoTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgCmdHoTime = (UINT4) pPgConfigInfo->u2HoldOffTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdWTR
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdWTR
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdWTR (UINT4 u4FsElpsContextId,
                      UINT4 u4FsElpsPgConfigPgId,
                      UINT4 *pu4RetValFsElpsPgCmdWTR)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgCmdWTR = (UINT4) pPgConfigInfo->u2WtrTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdExtCmd
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdExtCmd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdExtCmd (UINT4 u4FsElpsContextId,
                         UINT4 u4FsElpsPgConfigPgId,
                         INT4 *pi4RetValFsElpsPgCmdExtCmd)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdExtCmd = (INT4) pPgConfigInfo->LastLocalCmd.u1Request;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdExtCmdStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdExtCmdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdExtCmdStatus (UINT4 u4FsElpsContextId,
                               UINT4 u4FsElpsPgConfigPgId,
                               INT4 *pi4RetValFsElpsPgCmdExtCmdStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdExtCmdStatus = (INT4)
        pPgConfigInfo->LastLocalCmd.u1RequestStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdLocalCondition
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdLocalCondition
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdLocalCondition (UINT4 u4FsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 INT4 *pi4RetValFsElpsPgCmdLocalCondition)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdLocalCondition = (INT4)
        pPgConfigInfo->LastLocalCond.u1Request;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdLocalConditionStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdLocalConditionStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdLocalConditionStatus (UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       INT4
                                       *pi4RetValFsElpsPgCmdLocalConditionStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdLocalConditionStatus = (INT4)
        pPgConfigInfo->LastLocalCond.u1RequestStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdFarEndRequest
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdFarEndRequest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdFarEndRequest (UINT4 u4FsElpsContextId,
                                UINT4 u4FsElpsPgConfigPgId,
                                INT4 *pi4RetValFsElpsPgCmdFarEndRequest)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdFarEndRequest = (INT4)
        pPgConfigInfo->LastFarEndReq.u1Request;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdFarEndRequestStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdFarEndRequestStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdFarEndRequestStatus (UINT4 u4FsElpsContextId,
                                      UINT4 u4FsElpsPgConfigPgId,
                                      INT4
                                      *pi4RetValFsElpsPgCmdFarEndRequestStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdFarEndRequestStatus = (INT4)
        pPgConfigInfo->LastFarEndReq.u1RequestStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdActiveRequest
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdActiveRequest
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdActiveRequest (UINT4 u4FsElpsContextId,
                                UINT4 u4FsElpsPgConfigPgId,
                                INT4 *pi4RetValFsElpsPgCmdActiveRequest)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdActiveRequest = pPgConfigInfo->u1LastActiveReq;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdSemState
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdSemState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdSemState (UINT4 u4FsElpsContextId,
                           UINT4 u4FsElpsPgConfigPgId,
                           INT4 *pi4RetValFsElpsPgCmdSemState)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdSemState = (INT4)
        pPgConfigInfo->ApsInfo.u1ApsSemStateId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdPgStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgCmdPgStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdPgStatus (UINT4 u4FsElpsContextId,
                           UINT4 u4FsElpsPgConfigPgId,
                           INT4 *pi4RetValFsElpsPgCmdPgStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCmdPgStatus = pPgConfigInfo->u1PgStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCmdApsPeriodicTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCmdApsPeriodicTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCmdApsPeriodicTime (UINT4 u4FsElpsContextId,
                                  UINT4 u4FsElpsPgConfigPgId,
                                  UINT4 *pu4RetValFsElpsPgCmdApsPeriodicTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgCmdApsPeriodicTime =
        (UINT4) pPgConfigInfo->u2PeriodicTime;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElpsPgCmdHoTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCmdHoTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCmdHoTime (UINT4 u4FsElpsContextId,
                         UINT4 u4FsElpsPgConfigPgId,
                         UINT4 u4SetValFsElpsPgCmdHoTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo->u2HoldOffTime = (UINT2) u4SetValFsElpsPgCmdHoTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCmdWTR
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCmdWTR
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCmdWTR (UINT4 u4FsElpsContextId,
                      UINT4 u4FsElpsPgConfigPgId, UINT4 u4SetValFsElpsPgCmdWTR)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo->u2WtrTime = (UINT2) u4SetValFsElpsPgCmdWTR;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCmdExtCmd
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCmdExtCmd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCmdExtCmd (UINT4 u4FsElpsContextId,
                         UINT4 u4FsElpsPgConfigPgId,
                         INT4 i4SetValFsElpsPgCmdExtCmd)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;
    UINT1               u1CmdStatus = 0;
    UINT1               u1Event = 0;
    UINT1               u1Action = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        CLI_SET_ERR (CLI_ELPS_ERR_PG_NOT_CREATE);
        return SNMP_FAILURE;
    }

    if (i4SetValFsElpsPgCmdExtCmd == ELPS_EXT_CMD_FREEZE)
    {
        ElpsSemFreeze (pPgConfigInfo);
    }
    else if (i4SetValFsElpsPgCmdExtCmd == ELPS_EXT_CMD_CLR_FREEZE)
    {
        ElpsSemClearFreeze (pPgConfigInfo);
    }

    /* Update the Local Command variable in Pg config structure */
    pPgConfigInfo->u1LastRecvdReqType = ELPS_LOCAL_COMMAND;
    pPgConfigInfo->LastLocalCmd.u1Request = (UINT1) i4SetValFsElpsPgCmdExtCmd;

    if ((i4SetValFsElpsPgCmdExtCmd == ELPS_EXT_CMD_CLR_FREEZE)
        || (i4SetValFsElpsPgCmdExtCmd == ELPS_EXT_CMD_FREEZE))
    {
        pPgConfigInfo->LastLocalCmd.u1RequestStatus = ELPS_CMD_ACCEPTED;
        return SNMP_SUCCESS;
    }

    u1Event = ElpsSemGetEventIdForReq (ELPS_LOCAL_COMMAND,
                                       pPgConfigInfo->LastLocalCmd.u1Request);

    if (u1Event >= ELPS_SEM_MAX_EVENTS)
    {
        CLI_SET_ERR (CLI_ELPS_ERR_CMD_REJECTED);
        pPgConfigInfo->LastLocalCmd.u1RequestStatus = ELPS_CMD_REJECTED;
        return SNMP_FAILURE;
    }

    /* EXER command is only accepted in Bi-directional Protection Type */
    if (u1Event == ELPS_EV_LR_EXER)
    {
        if (!ELPS_GET_PG_D_BIT_VALUE (pPgConfigInfo->ApsInfo.u1ProtectionType))
        {
            pPgConfigInfo->LastLocalCmd.u1RequestStatus = ELPS_CMD_NA;
            return SNMP_SUCCESS;
        }
    }

    /* Local command acceptance check */
    if (ElpsSemValidateLocalRequest (pPgConfigInfo, u1Event) == OSIX_FAILURE)
    {
        CLI_SET_ERR (CLI_ELPS_ERR_CMD_REJECTED);
        pPgConfigInfo->LastLocalCmd.u1RequestStatus = ELPS_CMD_REJECTED;
        return SNMP_FAILURE;
    }

    /* Global priority check */
    u1Action = ElpsSemApplyGlobalPriorityLogic (pPgConfigInfo, &u1Event);

    /* Apply the External Command as an event to the SEM */
    u1CmdStatus = ElpsSemApplyEvent (pPgConfigInfo, u1Event, u1Action);

    /* Update the Local Command status present in 
     * PG Config Structure 
     */
    pPgConfigInfo->LastLocalCmd.u1RequestStatus = u1CmdStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCmdApsPeriodicTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCmdApsPeriodicTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCmdApsPeriodicTime (UINT4 u4FsElpsContextId,
                                  UINT4 u4FsElpsPgConfigPgId,
                                  UINT4 u4SetValFsElpsPgCmdApsPeriodicTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;
    INT4                i4PgConfigRowStatus = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if(pPgConfigInfo->u2PeriodicTime != u4SetValFsElpsPgCmdApsPeriodicTime)
    {    
        pPgConfigInfo->u2PeriodicTime = (UINT2) u4SetValFsElpsPgCmdApsPeriodicTime;
    }

    nmhGetFsElpsPgConfigRowStatus (u4FsElpsContextId, u4FsElpsPgConfigPgId,
                                    &i4PgConfigRowStatus);
    if(i4PgConfigRowStatus == ACTIVE)
    {
        ElpsTmrStopTimer (pPgConfigInfo, ELPS_APS_PERIODIC_TMR);
        ElpsTmrStartTimer (pPgConfigInfo, pPgConfigInfo->u2PeriodicTime,
                            ELPS_APS_PERIODIC_TMR);
    }


    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCmdHoTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCmdHoTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCmdHoTime (UINT4 *pu4ErrorCode,
                            UINT4 u4FsElpsContextId,
                            UINT4 u4FsElpsPgConfigPgId,
                            UINT4 u4TestValFsElpsPgCmdHoTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCmdHoTime > ELPS_PG_MAX_HOLD_OFF_TIME)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Hold-Off Time Interval is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCmdWTR
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCmdWTR
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCmdWTR (UINT4 *pu4ErrorCode,
                         UINT4 u4FsElpsContextId,
                         UINT4 u4FsElpsPgConfigPgId,
                         UINT4 u4TestValFsElpsPgCmdWTR)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((u4TestValFsElpsPgCmdWTR < ELPS_PG_MIN_WTR_TIME)
        || (u4TestValFsElpsPgCmdWTR > ELPS_PG_MAX_WTR_TIME))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group WTR Time Interval is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCmdExtCmd
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCmdExtCmd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCmdExtCmd (UINT4 *pu4ErrorCode,
                            UINT4 u4FsElpsContextId,
                            UINT4 u4FsElpsPgConfigPgId,
                            INT4 i4TestValFsElpsPgCmdExtCmd)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT1               u1OperType = 0;

    if ((i4TestValFsElpsPgCmdExtCmd <= ELPS_MIN_LOCAL_COMMAND) ||
        (i4TestValFsElpsPgCmdExtCmd >= ELPS_MAX_LOCAL_COMMAND))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group External Command is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->pContextInfo->u1ModuleStatus == ELPS_DISABLED)
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC, "Module is Disabled\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MODULE_DISABLED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus != ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                   "Protection Group config entry %d is not active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_NOT_ACTIVE);
        return SNMP_FAILURE;
    }

    if ((pPgConfigInfo->u1FreezeFlag == OSIX_TRUE)
        && (i4TestValFsElpsPgCmdExtCmd != ELPS_EXT_CMD_CLR_FREEZE))
    {
        ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                   "Protection Group entry %d is in freeze state, "
                   "No command will be accepted, except clear freeze\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_FREEZED);
        return SNMP_FAILURE;
    }

    /* Manual-switch to working command is valid only in Non-revertive mode */
    if (i4TestValFsElpsPgCmdExtCmd == ELPS_EXT_CMD_MS_W)
    {
        u1OperType = (UINT1)
            ELPS_GET_PG_R_BIT_VALUE (pPgConfigInfo->ApsInfo.u1ProtectionType);
        if (u1OperType == OSIX_TRUE)
        {
            ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC,
                       "Manual Switch to Working command is invalid in "
                       "revertive mode \n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ELPS_ERR_MSW_INVALID_IN_REVERTIVE);
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValFsElpsPgCmdExtCmd == ELPS_EXT_CMD_EXER)
    {
        if ((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
            (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW))
        {
            ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC, "EXER is not"
                       "supported for LSP/PW Services\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ELPS_ERR_CMD_REJECTED);
            return SNMP_FAILURE;
        }
    }
    else if (i4TestValFsElpsPgCmdExtCmd == ELPS_EXT_CMD_CLR)
    {
        if ((pPgConfigInfo->u1PscVersion == ELPS_ZERO) &&
            (pPgConfigInfo->ApsInfo.u1ApsSemStateId == ELPS_STATE_WTR_P) &&
            ((pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP) ||
             (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)))
        {
            ELPS_TRC ((NULL, MGMT_TRC | CONTROL_PLANE_TRC, "Clear command"
                       " cannot be applied when in WTR State\n"));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ELPS_ERR_CMD_REJECTED);
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCmdApsPeriodicTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCmdApsPeriodicTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCmdApsPeriodicTime (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsElpsContextId,
                                     UINT4 u4FsElpsPgConfigPgId,
                                     UINT4 u4TestValFsElpsPgCmdApsPeriodicTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((u4TestValFsElpsPgCmdApsPeriodicTime <
         ELPS_PG_MIN_APS_PERIODIC_TIME)
        || (u4TestValFsElpsPgCmdApsPeriodicTime >
            ELPS_PG_MAX_APS_PERIODIC_TIME))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC, "Protection Group "
                   "APS Periodic Time Interval is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElpsPgCmdTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsPgCmdTable (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElpsPgCfmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElpsPgCfmTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElpsPgCfmTable (UINT4 u4FsElpsContextId,
                                          UINT4 u4FsElpsPgConfigPgId)
{
    return (nmhValidateIndexInstanceFsElpsPgConfigTable (u4FsElpsContextId,
                                                         u4FsElpsPgConfigPgId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElpsPgCfmTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElpsPgCfmTable (UINT4 *pu4FsElpsContextId,
                                  UINT4 *pu4FsElpsPgConfigPgId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsPgGetFirstNode (pu4FsElpsContextId,
                                        pu4FsElpsPgConfigPgId);

    if (pPgConfigInfo != NULL)
    {
        do
        {
            if (pPgConfigInfo->u1CfmRowStatus != 0)
            {
                return SNMP_SUCCESS;
            }

        }
        while ((pPgConfigInfo = ElpsPgGetNextNode (*pu4FsElpsContextId,
                                                   *pu4FsElpsPgConfigPgId,
                                                   pu4FsElpsContextId,
                                                   pu4FsElpsPgConfigPgId))
               != NULL);
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Protection Group Cfm Table is empty.\r\n"));
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElpsPgCfmTable
 Input       :  The Indices
                FsElpsContextId
                nextFsElpsContextId
                FsElpsPgConfigPgId
                nextFsElpsPgConfigPgId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElpsPgCfmTable (UINT4 u4FsElpsContextId,
                                 UINT4 *pu4NextFsElpsContextId,
                                 UINT4 u4FsElpsPgConfigPgId,
                                 UINT4 *pu4NextFsElpsPgConfigPgId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    while ((pPgConfigInfo = ElpsPgGetNextNode (u4FsElpsContextId,
                                               u4FsElpsPgConfigPgId,
                                               pu4NextFsElpsContextId,
                                               pu4NextFsElpsPgConfigPgId))
           != NULL)
    {
        if (pPgConfigInfo->u1CfmRowStatus != 0)
        {
            return SNMP_SUCCESS;
        }

        /* Update the current index to fetch the next index value */
        u4FsElpsContextId = *pu4NextFsElpsContextId;
        u4FsElpsPgConfigPgId = *pu4NextFsElpsPgConfigPgId;
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Next Protection Group Cfm Entry does not exist.\r\n"));
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmWorkingMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCfmWorkingMEG
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmWorkingMEG (UINT4 u4FsElpsContextId,
                             UINT4 u4FsElpsPgConfigPgId,
                             UINT4 *pu4RetValFsElpsPgCfmWorkingMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmWorkingMEG = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmWorkingMEG =
        pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MegId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmWorkingME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCfmWorkingME
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmWorkingME (UINT4 u4FsElpsContextId,
                            UINT4 u4FsElpsPgConfigPgId,
                            UINT4 *pu4RetValFsElpsPgCfmWorkingME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmWorkingME = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmWorkingME =
        pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MeId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmWorkingMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCfmWorkingMEP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmWorkingMEP (UINT4 u4FsElpsContextId,
                             UINT4 u4FsElpsPgConfigPgId,
                             UINT4 *pu4RetValFsElpsPgCfmWorkingMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmWorkingMEP = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmWorkingMEP =
        pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MepId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmProtectionMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCfmProtectionMEG
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmProtectionMEG (UINT4 u4FsElpsContextId,
                                UINT4 u4FsElpsPgConfigPgId,
                                UINT4 *pu4RetValFsElpsPgCfmProtectionMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmProtectionMEG = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmProtectionMEG =
        pPgConfigInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmProtectionME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCfmProtectionME
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmProtectionME (UINT4 u4FsElpsContextId,
                               UINT4 u4FsElpsPgConfigPgId,
                               UINT4 *pu4RetValFsElpsPgCfmProtectionME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmProtectionME = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmProtectionME =
        pPgConfigInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MeId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmProtectionMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCfmProtectionMEP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmProtectionMEP (UINT4 u4FsElpsContextId,
                                UINT4 u4FsElpsPgConfigPgId,
                                UINT4 *pu4RetValFsElpsPgCfmProtectionMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmProtectionMEP = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmProtectionMEP =
        pPgConfigInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MepId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgCfmRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmRowStatus (UINT4 u4FsElpsContextId,
                            UINT4 u4FsElpsPgConfigPgId,
                            INT4 *pi4RetValFsElpsPgCfmRowStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1CfmRowStatus == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgCfmRowStatus = pPgConfigInfo->u1CfmRowStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmWorkingReverseMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgCfmWorkingReverseMEG
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmWorkingReverseMEG (UINT4
                                    u4FsElpsContextId,
                                    UINT4
                                    u4FsElpsPgConfigPgId,
                                    UINT4
                                    *pu4RetValFsElpsPgCfmWorkingReverseMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmWorkingReverseMEG = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmWorkingReverseMEG =
        pPgConfigInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmWorkingReverseME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgCfmWorkingReverseME
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmWorkingReverseME (UINT4
                                   u4FsElpsContextId,
                                   UINT4
                                   u4FsElpsPgConfigPgId,
                                   UINT4 *pu4RetValFsElpsPgCfmWorkingReverseME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmWorkingReverseME = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmWorkingReverseME =
        pPgConfigInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MeId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmWorkingReverseMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgCfmWorkingReverseMEP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmWorkingReverseMEP (UINT4
                                    u4FsElpsContextId,
                                    UINT4
                                    u4FsElpsPgConfigPgId,
                                    UINT4
                                    *pu4RetValFsElpsPgCfmWorkingReverseMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmWorkingReverseMEP = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmWorkingReverseMEP =
        pPgConfigInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MepId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmProtectionReverseMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgCfmProtectionReverseMEG
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmProtectionReverseMEG (UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       UINT4
                                       *pu4RetValFsElpsPgCfmProtectionReverseMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmProtectionReverseMEG = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmProtectionReverseMEG =
        pPgConfigInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElpsPgCfmProtectionReverseME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgCfmProtectionReverseME
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmProtectionReverseME (UINT4
                                      u4FsElpsContextId,
                                      UINT4
                                      u4FsElpsPgConfigPgId,
                                      UINT4
                                      *pu4RetValFsElpsPgCfmProtectionReverseME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmProtectionReverseME = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmProtectionReverseME =
        pPgConfigInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MeId;
    return SNMP_SUCCESS;

}

/** **************************************************************************
 Function    :  nmhGetFsElpsPgCfmProtectionReverseMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgCfmProtectionReverseMEP
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgCfmProtectionReverseMEP (UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       UINT4
                                       *pu4RetValFsElpsPgCfmProtectionReverseMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        *pu4RetValFsElpsPgCfmProtectionReverseMEP = 0;
        return SNMP_SUCCESS;
    }

    *pu4RetValFsElpsPgCfmProtectionReverseMEP =
        pPgConfigInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MepId;
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmWorkingMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCfmWorkingMEG
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmWorkingMEG (UINT4 u4FsElpsContextId,
                             UINT4 u4FsElpsPgConfigPgId,
                             UINT4 u4SetValFsElpsPgCfmWorkingMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MegId =
        u4SetValFsElpsPgCfmWorkingMEG;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmWorkingME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCfmWorkingME
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmWorkingME (UINT4 u4FsElpsContextId,
                            UINT4 u4FsElpsPgConfigPgId,
                            UINT4 u4SetValFsElpsPgCfmWorkingME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MeId =
        u4SetValFsElpsPgCfmWorkingME;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmWorkingMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCfmWorkingMEP
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmWorkingMEP (UINT4 u4FsElpsContextId,
                             UINT4 u4FsElpsPgConfigPgId,
                             UINT4 u4SetValFsElpsPgCfmWorkingMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MepId =
        u4SetValFsElpsPgCfmWorkingMEP;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmProtectionMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCfmProtectionMEG
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmProtectionMEG (UINT4 u4FsElpsContextId,
                                UINT4 u4FsElpsPgConfigPgId,
                                UINT4 u4SetValFsElpsPgCfmProtectionMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MegId =
        u4SetValFsElpsPgCfmProtectionMEG;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmProtectionME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCfmProtectionME
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmProtectionME (UINT4 u4FsElpsContextId,
                               UINT4 u4FsElpsPgConfigPgId,
                               UINT4 u4SetValFsElpsPgCfmProtectionME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MeId =
        u4SetValFsElpsPgCfmProtectionME;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmProtectionMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCfmProtectionMEP
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmProtectionMEP (UINT4 u4FsElpsContextId,
                                UINT4 u4FsElpsPgConfigPgId,
                                UINT4 u4SetValFsElpsPgCfmProtectionMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->ProtectionEntity.pCfmConfig->MonitorInfo.u4MepId =
        u4SetValFsElpsPgCfmProtectionMEP;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgCfmRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmRowStatus (UINT4 u4FsElpsContextId,
                            UINT4 u4FsElpsPgConfigPgId,
                            INT4 i4SetValFsElpsPgCfmRowStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    tElpsCfmConfigInfo *pCfmConfigInfo = NULL;
    tElpsCfmConfigInfo *pRvrCfmConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsElpsPgCfmRowStatus)
    {
        case CREATE_AND_WAIT:

            /* Create Two Nodes - One for Working and another for 
             * Protection entity
             */

            /* For Working Entity */

            pCfmConfigInfo = ElpsCfmCreateNode ();
            if (pCfmConfigInfo == NULL)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to create Protection Group Cfm entry"
                           "%d\n", u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }

            pRvrCfmConfigInfo = ElpsCfmCreateNode ();
            if (pRvrCfmConfigInfo == NULL)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to create Protection Group Cfm entry"
                           "%d\n", u4FsElpsPgConfigPgId));
                MemReleaseMemBlock (gElpsGlobalInfo.CfmConfigTablePoolId,
                                    (UINT1 *) pCfmConfigInfo);
                return SNMP_FAILURE;
            }

            pCfmConfigInfo->u1EntityType = ELPS_ENTITY_TYPE_WORKING;
            pCfmConfigInfo->u1ServiceType = pPgConfigInfo->u1ServiceType;
            pCfmConfigInfo->u1ModId = pPgConfigInfo->u1MonitorType;
            pCfmConfigInfo->u1Status = ELPS_CFM_SIGNAL_OK;
            pCfmConfigInfo->u1Direction = ELPS_SERVICE_DIRECTION_FWD;

            pRvrCfmConfigInfo->u1EntityType = ELPS_ENTITY_TYPE_WORKING;
            pRvrCfmConfigInfo->u1ServiceType = pPgConfigInfo->u1ServiceType;
            pRvrCfmConfigInfo->u1ModId = pPgConfigInfo->u1MonitorType;
            pRvrCfmConfigInfo->u1Status = ELPS_CFM_SIGNAL_OK;
            pRvrCfmConfigInfo->u1Direction = ELPS_SERVICE_DIRECTION_RVR;

            pPgConfigInfo->WorkingEntity.pCfmConfig = pCfmConfigInfo;
            pPgConfigInfo->WorkingReverseEntity.pCfmConfig = pRvrCfmConfigInfo;

            /* Update the back pointer */
            pCfmConfigInfo->pPgInfo = pPgConfigInfo;
            pRvrCfmConfigInfo->pPgInfo = pPgConfigInfo;

            pCfmConfigInfo = NULL;
            pRvrCfmConfigInfo = NULL;

            /* For Protection Entity */
            pCfmConfigInfo = ElpsCfmCreateNode ();
            if (pCfmConfigInfo == NULL)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to create Protection Group Cfm entry"
                           "%d\n", u4FsElpsPgConfigPgId));
                return SNMP_FAILURE;
            }

            pRvrCfmConfigInfo = ElpsCfmCreateNode ();
            if (pRvrCfmConfigInfo == NULL)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to create Protection Group Cfm entry"
                           "%d\n", u4FsElpsPgConfigPgId));
                MemReleaseMemBlock (gElpsGlobalInfo.CfmConfigTablePoolId,
                                    (UINT1 *) pCfmConfigInfo);
                return SNMP_FAILURE;
            }

            pCfmConfigInfo->u1EntityType = ELPS_ENTITY_TYPE_PROTECTION;
            pCfmConfigInfo->u1ServiceType = pPgConfigInfo->u1ServiceType;
            pCfmConfigInfo->u1ModId = pPgConfigInfo->u1MonitorType;
            pCfmConfigInfo->u1Status = ELPS_CFM_SIGNAL_OK;
            pCfmConfigInfo->u1Direction = ELPS_SERVICE_DIRECTION_FWD;

            pRvrCfmConfigInfo->u1EntityType = ELPS_ENTITY_TYPE_PROTECTION;
            pRvrCfmConfigInfo->u1ServiceType = pPgConfigInfo->u1ServiceType;
            pRvrCfmConfigInfo->u1ModId = pPgConfigInfo->u1MonitorType;
            pRvrCfmConfigInfo->u1Status = ELPS_CFM_SIGNAL_OK;
            pRvrCfmConfigInfo->u1Direction = ELPS_SERVICE_DIRECTION_RVR;

            pPgConfigInfo->ProtectionEntity.pCfmConfig = pCfmConfigInfo;
            pPgConfigInfo->ProtectionReverseEntity.pCfmConfig =
                pRvrCfmConfigInfo;

            /* Update the back pointer */
            pCfmConfigInfo->pPgInfo = pPgConfigInfo;
            pRvrCfmConfigInfo->pPgInfo = pPgConfigInfo;

            /* Set the RowStatus as Not Ready, to be moved to Not In Service
             * only when the mandatory objects for the Table are set. 
             */
            pPgConfigInfo->u1CfmRowStatus = NOT_READY;

            break;

        case NOT_IN_SERVICE:

            if (pPgConfigInfo->u1CfmRowStatus == NOT_IN_SERVICE)
            {
                return SNMP_SUCCESS;
            }
            if (ElpsCfmDeActivate (pPgConfigInfo->pContextInfo,
                                   pPgConfigInfo) != OSIX_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            pPgConfigInfo->u1CfmRowStatus = NOT_IN_SERVICE;

            break;

        case DESTROY:

            if (pPgConfigInfo->WorkingEntity.pCfmConfig != NULL)
            {
                ElpsCfmDelNode (pPgConfigInfo->pContextInfo, pPgConfigInfo->
                                WorkingEntity.pCfmConfig);
            }

            if (pPgConfigInfo->ProtectionEntity.pCfmConfig != NULL)
            {
                ElpsCfmDelNode (pPgConfigInfo->pContextInfo, pPgConfigInfo->
                                ProtectionEntity.pCfmConfig);
            }

            if (pPgConfigInfo->WorkingReverseEntity.pCfmConfig != NULL)
            {
                ElpsCfmDelNode (pPgConfigInfo->pContextInfo, pPgConfigInfo->
                                WorkingReverseEntity.pCfmConfig);
            }

            if (pPgConfigInfo->ProtectionReverseEntity.pCfmConfig != NULL)
            {
                ElpsCfmDelNode (pPgConfigInfo->pContextInfo, pPgConfigInfo->
                                ProtectionReverseEntity.pCfmConfig);
            }

            /* update the Row status */
            pPgConfigInfo->u1CfmRowStatus = 0;
            break;

        case ACTIVE:

            if (pPgConfigInfo->u1CfmRowStatus != NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }

            if (ElpsCfmActivate (pPgConfigInfo->pContextInfo,
                                 pPgConfigInfo) != OSIX_SUCCESS)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to add Protection Group Cfm entry"
                           "%d\n", u4FsElpsPgConfigPgId));
                pPgConfigInfo->u1CfmRowStatus = NOT_READY;
                return SNMP_FAILURE;
            }

            pPgConfigInfo->u1CfmRowStatus = ACTIVE;
            break;

        default:

            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmWorkingReverseMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgCfmWorkingReverseMEG
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmWorkingReverseMEG (UINT4
                                    u4FsElpsContextId,
                                    UINT4
                                    u4FsElpsPgConfigPgId,
                                    UINT4 u4SetValFsElpsPgCfmWorkingReverseMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MegId =
        u4SetValFsElpsPgCfmWorkingReverseMEG;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmWorkingReverseME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgCfmWorkingReverseME
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmWorkingReverseME (UINT4
                                   u4FsElpsContextId,
                                   UINT4
                                   u4FsElpsPgConfigPgId,
                                   UINT4 u4SetValFsElpsPgCfmWorkingReverseME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MeId =
        u4SetValFsElpsPgCfmWorkingReverseME;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmWorkingReverseMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgCfmWorkingReverseMEP
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmWorkingReverseMEP (UINT4
                                    u4FsElpsContextId,
                                    UINT4
                                    u4FsElpsPgConfigPgId,
                                    UINT4 u4SetValFsElpsPgCfmWorkingReverseMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->WorkingReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->WorkingReverseEntity.pCfmConfig->MonitorInfo.u4MepId =
        u4SetValFsElpsPgCfmWorkingReverseMEP;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmProtectionReverseMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgCfmProtectionReverseMEG
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmProtectionReverseMEG (UINT4
                                       u4FsElpsContextId,
                                       UINT4
                                       u4FsElpsPgConfigPgId,
                                       UINT4
                                       u4SetValFsElpsPgCfmProtectionReverseMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MegId =
        u4SetValFsElpsPgCfmProtectionReverseMEG;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmProtectionReverseME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgCfmProtectionReverseME
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmProtectionReverseME (UINT4
                                      u4FsElpsContextId,
                                      UINT4
                                      u4FsElpsPgConfigPgId,
                                      UINT4
                                      u4SetValFsElpsPgCfmProtectionReverseME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MeId =
        u4SetValFsElpsPgCfmProtectionReverseME;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsElpsPgCfmProtectionReverseMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                setValFsElpsPgCfmProtectionReverseMEP
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgCfmProtectionReverseMEP (UINT4
                                       u4FsElpsContextId,
                                       UINT4
                                       u4FsElpsPgConfigPgId,
                                       UINT4
                                       u4SetValFsElpsPgCfmProtectionReverseMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->ProtectionReverseEntity.pCfmConfig == NULL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group %d Cfm Entry is not created\n",
                   u4FsElpsPgConfigPgId));
        return SNMP_FAILURE;
    }

    pPgConfigInfo->ProtectionReverseEntity.pCfmConfig->MonitorInfo.u4MepId =
        u4SetValFsElpsPgCfmProtectionReverseMEP;

    ElpsUtilPgCfmNotReadyToNotInServ (pPgConfigInfo);

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmWorkingMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCfmWorkingMEG
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmWorkingMEG (UINT4 *pu4ErrorCode,
                                UINT4 u4FsElpsContextId,
                                UINT4 u4FsElpsPgConfigPgId,
                                UINT4 u4TestValFsElpsPgCfmWorkingMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCfmWorkingMEG == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working MEG is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MEG_ME_ZERO);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmWorkingME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCfmWorkingME
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmWorkingME (UINT4 *pu4ErrorCode,
                               UINT4 u4FsElpsContextId,
                               UINT4 u4FsElpsPgConfigPgId,
                               UINT4 u4TestValFsElpsPgCfmWorkingME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCfmWorkingME == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working ME is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MEG_ME_ZERO);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmWorkingMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCfmWorkingMEP
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmWorkingMEP (UINT4 *pu4ErrorCode,
                                UINT4 u4FsElpsContextId,
                                UINT4 u4FsElpsPgConfigPgId,
                                UINT4 u4TestValFsElpsPgCfmWorkingMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        if ((u4TestValFsElpsPgCfmWorkingMEP < ELPS_MIN_MEP_ID) ||
            (u4TestValFsElpsPgCfmWorkingMEP > ELPS_MAX_MEP_ID))
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Working MEP is not valid\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (u4TestValFsElpsPgCfmWorkingMEP == 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Working MEP is not valid\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmProtectionMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCfmProtectionMEG
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmProtectionMEG (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsElpsContextId,
                                   UINT4 u4FsElpsPgConfigPgId,
                                   UINT4 u4TestValFsElpsPgCfmProtectionMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCfmProtectionMEG == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection MEG is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MEG_ME_ZERO);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmProtectionME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCfmProtectionME
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmProtectionME (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsElpsContextId,
                                  UINT4 u4FsElpsPgConfigPgId,
                                  UINT4 u4TestValFsElpsPgCfmProtectionME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCfmProtectionME == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection ME is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MEG_ME_ZERO);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmProtectionMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCfmProtectionMEP
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmProtectionMEP (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsElpsContextId,
                                   UINT4 u4FsElpsPgConfigPgId,
                                   UINT4 u4TestValFsElpsPgCfmProtectionMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_VLAN)
    {
        if ((u4TestValFsElpsPgCfmProtectionMEP < ELPS_MIN_MEP_ID) ||
            (u4TestValFsElpsPgCfmProtectionMEP > ELPS_MAX_MEP_ID))
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Working MEP is not valid\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (u4TestValFsElpsPgCfmProtectionMEP == 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Working MEP is not valid\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgCfmRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmRowStatus (UINT4 *pu4ErrorCode,
                               UINT4 u4FsElpsContextId,
                               UINT4 u4FsElpsPgConfigPgId,
                               INT4 i4TestValFsElpsPgCfmRowStatus)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    tElpsCfmConfigInfo *pUsedCfm = NULL;
    tElpsCfmConfigInfo  CfmInfo;
    MEMSET (&CfmInfo, 0, sizeof (tElpsCfmConfigInfo));

    if ((i4TestValFsElpsPgCfmRowStatus < ACTIVE) ||
        (i4TestValFsElpsPgCfmRowStatus > DESTROY))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group CFM Rowstatus is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsElpsPgCfmRowStatus == CREATE_AND_WAIT) &&
        (pPgConfigInfo->u1CfmRowStatus == NOT_READY))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "PG CFM entry already exists.\n", u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* The RowStatus can be set to Active only if all the mandatory 
     * objects in the Table are set. When they are not set, the 
     * RowStatus will be in Not Ready State. RowStatus can move to 
     * Active from Not In Service State only 
     */

    if ((i4TestValFsElpsPgCfmRowStatus == ACTIVE)
        || (i4TestValFsElpsPgCfmRowStatus == NOT_IN_SERVICE))
    {
        if (pPgConfigInfo->u1CfmRowStatus == NOT_READY)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group %d CFM entry is not ready to"
                       " be activated\n", u4FsElpsPgConfigPgId));
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (CLI_ELPS_ERR_CFM_NOT_READY);
            return SNMP_FAILURE;
        }
    }

    /* Verify if the same cfm entry is already used by any other PG
     * in this context. If it is used then return failure */
    /* Working Entity */
    if (i4TestValFsElpsPgCfmRowStatus == ACTIVE)
    {
        CfmInfo.MonitorInfo.u4MegId =
	    pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MegId;
	CfmInfo.MonitorInfo.u4MeId =
	    pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MeId;
	CfmInfo.MonitorInfo.u4MepId =
	    pPgConfigInfo->WorkingEntity.pCfmConfig->MonitorInfo.u4MepId;
	CfmInfo.u1ServiceType = pPgConfigInfo->u1ServiceType;
	    CfmInfo.u1ModId = pPgConfigInfo->u1MonitorType;

	pUsedCfm = ElpsCfmGetNode (pPgConfigInfo->pContextInfo, &CfmInfo);

	if (pUsedCfm != NULL)
	{
	    ELPS_TRC ((pPgConfigInfo->pContextInfo, ELPS_CRITICAL_TRC,
				    "MisConfiguration for working CFM entry - "
				    "MegId=%d, MeId=%d, MepId=%d is already used "
				    "by PG-%d\r\n",
				    CfmInfo.MonitorInfo.u4MegId, CfmInfo.MonitorInfo.u4MeId,
				    CfmInfo.MonitorInfo.u4MepId, pUsedCfm->pPgInfo->u4PgId));
	    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	    CLI_SET_ERR (CLI_ELPS_ERR_WORKING_CFM_ENTRY_ALREADY_USED);
	    return SNMP_FAILURE;
	}
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmWorkingReverseMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgCfmWorkingReverseMEG
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmWorkingReverseMEG (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4FsElpsContextId,
                                       UINT4
                                       u4FsElpsPgConfigPgId,
                                       UINT4
                                       u4TestValFsElpsPgCfmWorkingReverseMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCfmWorkingReverseMEG == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working MEG is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MEG_ME_ZERO);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Reverse MEG is not valid"
                   " for VLAN/PW services\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_CFM_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }
    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmWorkingReverseME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgCfmWorkingReverseME
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmWorkingReverseME (UINT4
                                      *pu4ErrorCode,
                                      UINT4
                                      u4FsElpsContextId,
                                      UINT4
                                      u4FsElpsPgConfigPgId,
                                      UINT4
                                      u4TestValFsElpsPgCfmWorkingReverseME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCfmWorkingReverseME == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working ME is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MEG_ME_ZERO);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working Reverse ME is not valid"
                   " for VLAN/PW services\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_CFM_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmWorkingReverseMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgCfmWorkingReverseMEP
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmWorkingReverseMEP (UINT4
                                       *pu4ErrorCode,
                                       UINT4
                                       u4FsElpsContextId,
                                       UINT4
                                       u4FsElpsPgConfigPgId,
                                       UINT4
                                       u4TestValFsElpsPgCfmWorkingReverseMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Working MEP is not valid"
                   " for VLAN/PW services\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_CFM_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }
    else
    {
        if (u4TestValFsElpsPgCfmWorkingReverseMEP == 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Working Reverse MEP is not valid\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmProtectionReverseMEG
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgCfmProtectionReverseMEG
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmProtectionReverseMEG (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsElpsContextId,
                                          UINT4 u4FsElpsPgConfigPgId,
                                          UINT4
                                          u4TestValFsElpsPgCfmProtectionReverseMEG)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCfmProtectionReverseMEG == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection MEG is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MEG_ME_ZERO);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection Reverse MEG is not valid"
                   " for VLAN/PW services\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_CFM_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmProtectionReverseME
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgCfmProtectionReverseME
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmProtectionReverseME (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsElpsContextId,
                                         UINT4 u4FsElpsPgConfigPgId,
                                         UINT4
                                         u4TestValFsElpsPgCfmProtectionReverseME)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if (u4TestValFsElpsPgCfmProtectionReverseME == 0)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection ME is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_MEG_ME_ZERO);
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection Reverse ME is not valid"
                   " for VLAN/PW services\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_CFM_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgCfmProtectionReverseMEP
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                testValFsElpsPgCfmProtectionReverseMEP
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgCfmProtectionReverseMEP (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsElpsContextId,
                                          UINT4 u4FsElpsPgConfigPgId,
                                          UINT4
                                          u4TestValFsElpsPgCfmProtectionReverseMEP)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }
    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Protection MEP is not valid"
                   " for VLAN/PW services\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_CFM_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }
    else
    {
        if (u4TestValFsElpsPgCfmProtectionReverseMEP == 0)
        {
            ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                       "Protection Group Protection Reverse MEP "
                       "is not valid\n"));
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (pPgConfigInfo->u1CfmRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of CFM Entry is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_CFM_ROW_ACTIVE);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElpsPgCfmTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsPgCfmTable (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElpsPgServiceListTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElpsPgServiceListTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListValue
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElpsPgServiceListTable (UINT4
                                                  u4FsElpsContextId,
                                                  UINT4
                                                  u4FsElpsPgConfigPgId,
                                                  UINT4
                                                  u4FsElpsPgServiceListValue)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Currently supported Service is Vlan/mpls-LSP/mpls-PW and to be updated on 
     * further addition to service type. 
     */
    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_VLAN)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "ServiceListTable is not supported for LSP/PW Protection "
                   "Group Service Type \r\n"));
        return SNMP_FAILURE;
    }

    if ((u4FsElpsPgServiceListValue == 0) ||
        (u4FsElpsPgServiceListValue > ELPS_SYS_MAX_NUM_SERVICE_IN_LIST_EXT))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC, "Protection Group "
                   "Invalid Service Id %d \r\n", u4FsElpsPgServiceListValue));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElpsPgServiceListTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListValue
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElpsPgServiceListTable (UINT4 *pu4FsElpsContextId,
                                          UINT4 *pu4FsElpsPgConfigPgId,
                                          UINT4 *pu4FsElpsPgServiceListValue)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListInfo *pServiceListInfo = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4Result = 0;

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pContextInfo = ElpsCxtGetNode (u4ContextId);
        i4Result = ElpsUtilIsElpsStarted (u4ContextId);

        if ((pContextInfo != NULL) && (i4Result == OSIX_TRUE))
        {
            pServiceListInfo = ElpsSrvListGetFirstNode (pContextInfo);

            if (pServiceListInfo != NULL)
            {
                *pu4FsElpsContextId = u4ContextId;
                *pu4FsElpsPgConfigPgId = pServiceListInfo->u4PgId;
                *pu4FsElpsPgServiceListValue = pServiceListInfo->u4ServiceId;
                return SNMP_SUCCESS;
            }
        }
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Protection Group Service List Table is empty. \r\n"));
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElpsPgServiceListTable
 Input       :  The Indices
                FsElpsContextId
                nextFsElpsContextId
                FsElpsPgConfigPgId
                nextFsElpsPgConfigPgId
                FsElpsPgServiceListValue
                nextFsElpsPgServiceListValue
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElpsPgServiceListTable (UINT4 u4FsElpsContextId,
                                         UINT4 *pu4NextFsElpsContextId,
                                         UINT4 u4FsElpsPgConfigPgId,
                                         UINT4
                                         *pu4NextFsElpsPgConfigPgId,
                                         UINT4
                                         u4FsElpsPgServiceListValue,
                                         UINT4 *pu4NextFsElpsPgServiceListValue)
{
    tElpsServiceListInfo ServiceListInfo;
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListInfo *pNextServiceListInfo = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4Result = 0;

    ServiceListInfo.u4PgId = u4FsElpsPgConfigPgId;
    ServiceListInfo.u4ServiceId = u4FsElpsPgServiceListValue;

    for (u4ContextId = u4FsElpsContextId; u4ContextId < ELPS_MAX_CONTEXTS;
         u4ContextId++)
    {
        pContextInfo = ElpsCxtGetNode (u4ContextId);
        i4Result = ElpsUtilIsElpsStarted (u4ContextId);

        if ((pContextInfo != NULL) && (i4Result == OSIX_TRUE))
        {
            if (u4ContextId != u4FsElpsContextId)
            {
                pNextServiceListInfo = ElpsSrvListGetFirstNode (pContextInfo);
            }
            else
            {
                pNextServiceListInfo =
                    ElpsSrvListGetNextNode (pContextInfo, &ServiceListInfo);
            }

            if (pNextServiceListInfo != NULL)
            {
                *pu4NextFsElpsContextId = u4ContextId;
                *pu4NextFsElpsPgConfigPgId = pNextServiceListInfo->u4PgId;
                *pu4NextFsElpsPgServiceListValue =
                    pNextServiceListInfo->u4ServiceId;
                return SNMP_SUCCESS;
            }
        }
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Next Protection Group Service List Entry does not exist\n"));
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsPgServiceListRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListValue

                The Object 
                retValFsElpsPgServiceListRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgServiceListRowStatus (UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    UINT4 u4FsElpsPgServiceListValue,
                                    INT4 *pi4RetValFsElpsPgServiceListRowStatus)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListInfo *pServiceListInfo = NULL;
    UINT4               u4Dummy = 0;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListInfo =
        ElpsSrvListGetNode (pContextInfo,
                            u4FsElpsPgConfigPgId, u4FsElpsPgServiceListValue);

    if (pServiceListInfo == NULL)
    {
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List PgId: %d "
                   "and Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListValue));
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgServiceListRowStatus = pServiceListInfo->u1RowStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElpsPgServiceListRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListValue

                The Object 
                setValFsElpsPgServiceListRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgServiceListRowStatus (UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    UINT4 u4FsElpsPgServiceListValue,
                                    INT4 i4SetValFsElpsPgServiceListRowStatus)
{
    tElpsServiceListInfo *pServiceListInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListInfo = ElpsSrvListGetNode (pPgConfigInfo->pContextInfo,
                                           u4FsElpsPgConfigPgId,
                                           u4FsElpsPgServiceListValue);

    if ((pServiceListInfo == NULL) &&
        (i4SetValFsElpsPgServiceListRowStatus != CREATE_AND_GO))
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListValue));
        return SNMP_FAILURE;
    }
    else if ((pServiceListInfo != NULL) &&
             (i4SetValFsElpsPgServiceListRowStatus == CREATE_AND_GO))
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List entry Pg Id: %d and "
                   "Service List Value: %d is already exists. \r\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListValue));
        return SNMP_FAILURE;
    }

    switch (i4SetValFsElpsPgServiceListRowStatus)
    {
        case CREATE_AND_GO:

            pServiceListInfo = ElpsSrvListCreateNode ();

            if (pServiceListInfo == NULL)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to create Protection Group List entry Pg Id:"
                           " %d and Service List Value: %d\n",
                           u4FsElpsPgConfigPgId, u4FsElpsPgServiceListValue));
                return SNMP_FAILURE;
            }

            pServiceListInfo->u4PgId = u4FsElpsPgConfigPgId;
            pServiceListInfo->u4ServiceId = u4FsElpsPgServiceListValue;

            if (ElpsSrvListAddNodeToSrvListTable (pPgConfigInfo->pContextInfo,
                                                  pPgConfigInfo,
                                                  pServiceListInfo)
                == OSIX_FAILURE)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to Add Protection Group List entry Pg Id:"
                           " %d and Service List Value: %d\n",
                           u4FsElpsPgConfigPgId, u4FsElpsPgServiceListValue));
                return SNMP_FAILURE;
            }

            /* Update L2IWF with this Vlan to Instance mapping */
            if (pPgConfigInfo->pContextInfo->i4VlanGroupManager
                == ELPS_VLAN_GROUP_MANAGER_ELPS)
            {
                ElpsUtilSetVlanGroupIdForVlan (u4FsElpsContextId,
                                               u4FsElpsPgServiceListValue,
                                               pPgConfigInfo->
                                               u4WorkingInstanceId);
            }

            pServiceListInfo->u1RowStatus = ACTIVE;
            /* Make the PG status to Not in service */
            ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);

            break;

        case DESTROY:

            /* Update L2IWF with this Vlan to Instance mapping */
            if (pPgConfigInfo->pContextInfo->i4VlanGroupManager
                == ELPS_VLAN_GROUP_MANAGER_ELPS)
            {
                ElpsUtilSetVlanGroupIdForVlan (u4FsElpsContextId,
                                               pServiceListInfo->u4ServiceId,
                                               ELPS_DEFAULT_VLAN_GROUP_ID);
            }

            ElpsSrvListDelNode (pPgConfigInfo->pContextInfo, pPgConfigInfo,
                                pServiceListInfo);
            break;

        default:

            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgServiceListRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListValue

                The Object 
                testValFsElpsPgServiceListRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgServiceListRowStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       UINT4 u4FsElpsPgServiceListValue,
                                       INT4
                                       i4TestValFsElpsPgServiceListRowStatus)
{
    tElpsServiceListInfo *pServiceListInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((i4TestValFsElpsPgServiceListRowStatus != CREATE_AND_GO) &&
        (i4TestValFsElpsPgServiceListRowStatus != DESTROY))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Service List RowStatus is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care
         * of filling the error code, CLI set error and trace messages.
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of PG Entry %d is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is Individual."
                   "This Configuration is not allowed. \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pServiceListInfo = ElpsSrvListGetNode (pPgConfigInfo->pContextInfo,
                                           u4FsElpsPgConfigPgId,
                                           u4FsElpsPgServiceListValue);

    if ((pServiceListInfo != NULL) &&
        (i4TestValFsElpsPgServiceListRowStatus == CREATE_AND_GO))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List entry for Pg Id: %d and "
                   "Service List Value: %d is already created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListValue));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else if ((pServiceListInfo == NULL) &&
             (i4TestValFsElpsPgServiceListRowStatus != CREATE_AND_GO))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List entry for Pg Id: %d and "
                   "Service List Value: %d is Not created for"
                   " Rowstatus to be updated\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListValue));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElpsPgServiceListTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsPgServiceListTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElpsPgServiceListPointerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElpsPgServiceListPointerTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElpsPgServiceListPointerTable (UINT4
                                                         u4FsElpsContextId,
                                                         UINT4
                                                         u4FsElpsPgConfigPgId,
                                                         UINT4
                                                         u4FsElpsPgServiceListId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    UNUSED_PARAM (u4FsElpsPgServiceListId);
    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /*Currently Service list pointer table is supported for  LSP and PW 
     * services, and not supported for VLAN services */

    if ((pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP) &&
        (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_PW))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service Pointer Table is not"
                   "supported for VLAN Service\r\n"));
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElpsPgServiceListPointerTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElpsPgServiceListPointerTable (UINT4
                                                 *pu4FsElpsContextId,
                                                 UINT4
                                                 *pu4FsElpsPgConfigPgId,
                                                 UINT4
                                                 *pu4FsElpsPgServiceListId)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4Result = 0;

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pContextInfo = ElpsCxtGetNode (u4ContextId);
        i4Result = ElpsUtilIsElpsStarted (u4ContextId);

        if ((pContextInfo != NULL) && (i4Result == OSIX_TRUE))
        {
            pServiceListPointerInfo = ElpsSrvListPtrGetFirstNode (pContextInfo);

            if (pServiceListPointerInfo != NULL)
            {
                *pu4FsElpsContextId = u4ContextId;
                *pu4FsElpsPgConfigPgId = pServiceListPointerInfo->u4PgId;
                *pu4FsElpsPgServiceListId =
                    pServiceListPointerInfo->u4PgServiceListId;
                return SNMP_SUCCESS;
            }
        }
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Protection Group Service List Pointer Table is empty. \r\n"));
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElpsPgServiceListPointerTable
 Input       :  The Indices
                FsElpsContextId
                nextFsElpsContextId
                FsElpsPgConfigPgId
                nextFsElpsPgConfigPgId
                FsElpsPgServiceListId
                nextFsElpsPgServiceListId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElpsPgServiceListPointerTable (UINT4
                                                u4FsElpsContextId,
                                                UINT4
                                                *pu4NextFsElpsContextId,
                                                UINT4
                                                u4FsElpsPgConfigPgId,
                                                UINT4
                                                *pu4NextFsElpsPgConfigPgId,
                                                UINT4
                                                u4FsElpsPgServiceListId,
                                                UINT4
                                                *pu4NextFsElpsPgServiceListId)
{
    tElpsServiceListPointerInfo ServiceListPointerInfo;
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListPointerInfo *pNextServiceListPointerInfo = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4Result = 0;

    ServiceListPointerInfo.u4PgId = u4FsElpsPgConfigPgId;
    ServiceListPointerInfo.u4PgServiceListId = u4FsElpsPgServiceListId;

    for (u4ContextId = u4FsElpsContextId; u4ContextId < ELPS_MAX_CONTEXTS;
         u4ContextId++)
    {
        pContextInfo = ElpsCxtGetNode (u4ContextId);
        i4Result = ElpsUtilIsElpsStarted (u4ContextId);

        if ((pContextInfo != NULL) && (i4Result == OSIX_TRUE))
        {
            if (u4ContextId != u4FsElpsContextId)
            {
                pNextServiceListPointerInfo =
                    ElpsSrvListPtrGetFirstNode (pContextInfo);
            }
            else
            {
                pNextServiceListPointerInfo =
                    ElpsSrvListPtrGetNextNode (pContextInfo,
                                               &ServiceListPointerInfo);
            }

            if (pNextServiceListPointerInfo != NULL)
            {
                *pu4NextFsElpsContextId = u4ContextId;
                *pu4NextFsElpsPgConfigPgId =
                    pNextServiceListPointerInfo->u4PgId;
                *pu4NextFsElpsPgServiceListId =
                    pNextServiceListPointerInfo->u4PgServiceListId;
                return SNMP_SUCCESS;
            }
        }
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Next Protection Group Service List Pointer \
               Entry does not exist\n"));
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsPgWorkingServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                retValFsElpsPgWorkingServiceListPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgWorkingServiceListPointer (UINT4 u4FsElpsContextId,
                                         UINT4 u4FsElpsPgConfigPgId,
                                         UINT4 u4FsElpsPgServiceListId,
                                         tSNMP_OID_TYPE *
                                         pRetValFsElpsPgWorkingServiceListPointer)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    UINT4               u4Dummy = 0;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer PgId: %d "
                   "and Service List Id: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        ElpsTnlGetTnlServicePointer (&pServiceListPointerInfo->WorkingInfo.
                                     LspServiceInfo,
                                     pRetValFsElpsPgWorkingServiceListPointer);
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        ElpsPwGetPwServicePointer (&pServiceListPointerInfo->WorkingInfo.
                                   PwServiceInfo,
                                   pRetValFsElpsPgWorkingServiceListPointer);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgWorkingReverseServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                retValFsElpsPgWorkingReverseServiceListPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgWorkingReverseServiceListPointer (UINT4 u4FsElpsContextId,
                                                UINT4 u4FsElpsPgConfigPgId,
                                                UINT4 u4FsElpsPgServiceListId,
                                                tSNMP_OID_TYPE *
                                                pRetValFsElpsPgWorkingReverseServiceListPointer)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    UINT4               u4Dummy = 0;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer PgId: %d "
                   "and Service List Id: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        ElpsTnlGetTnlServicePointer
            (&pServiceListPointerInfo->WorkingRvrInfo.LspServiceInfo,
             pRetValFsElpsPgWorkingReverseServiceListPointer);
    }
    else
    {
        MEMSET (pRetValFsElpsPgWorkingReverseServiceListPointer->
                pu4_OidList, 0, ELPS_ERR_OID_LEN * sizeof (UINT4));
        pRetValFsElpsPgWorkingReverseServiceListPointer->u4_Length =
            ELPS_ERR_OID_LEN;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgProtectionServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                retValFsElpsPgProtectionServiceListPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgProtectionServiceListPointer (UINT4 u4FsElpsContextId,
                                            UINT4 u4FsElpsPgConfigPgId,
                                            UINT4 u4FsElpsPgServiceListId,
                                            tSNMP_OID_TYPE *
                                            pRetValFsElpsPgProtectionServiceListPointer)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    UINT4               u4Dummy = 0;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer PgId: %d "
                   "and Service List Id: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        ElpsTnlGetTnlServicePointer (&pServiceListPointerInfo->ProtectionInfo.
                                     LspServiceInfo,
                                     pRetValFsElpsPgProtectionServiceListPointer);
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        ElpsPwGetPwServicePointer (&pServiceListPointerInfo->ProtectionInfo.
                                   PwServiceInfo,
                                   pRetValFsElpsPgProtectionServiceListPointer);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgProtectionReverseServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                retValFsElpsPgProtectionReverseServiceListPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgProtectionReverseServiceListPointer (UINT4 u4FsElpsContextId,
                                                   UINT4 u4FsElpsPgConfigPgId,
                                                   UINT4
                                                   u4FsElpsPgServiceListId,
                                                   tSNMP_OID_TYPE *
                                                   pRetValFsElpsPgProtectionReverseServiceListPointer)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    UINT4               u4Dummy = 0;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer PgId: %d "
                   "and Service List Id: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        ElpsTnlGetTnlServicePointer
            (&pServiceListPointerInfo->ProtectionRvrInfo.LspServiceInfo,
             pRetValFsElpsPgProtectionReverseServiceListPointer);
    }
    else
    {
        MEMSET (pRetValFsElpsPgProtectionReverseServiceListPointer->
                pu4_OidList, 0, ELPS_ERR_OID_LEN * sizeof (UINT4));
        pRetValFsElpsPgProtectionReverseServiceListPointer->u4_Length =
            ELPS_ERR_OID_LEN;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElpsPgServiceListPointerRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                retValFsElpsPgServiceListPointerRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgServiceListPointerRowStatus (UINT4 u4FsElpsContextId,
                                           UINT4 u4FsElpsPgConfigPgId,
                                           UINT4 u4FsElpsPgServiceListId,
                                           INT4
                                           *pi4RetValFsElpsPgServiceListPointerRowStatus)
{
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    UINT4               u4Dummy = 0;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer PgId: %d "
                   "and Service List Id: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgServiceListPointerRowStatus =
        pServiceListPointerInfo->u1RowStatus;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElpsPgWorkingServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                setValFsElpsPgWorkingServiceListPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgWorkingServiceListPointer (UINT4 u4FsElpsContextId,
                                         UINT4 u4FsElpsPgConfigPgId,
                                         UINT4 u4FsElpsPgServiceListId,
                                         tSNMP_OID_TYPE *
                                         pSetValFsElpsPgWorkingServiceListPointer)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);
    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        /* Tunnel Index */
        pServiceListPointerInfo->WorkingInfo.LspServiceInfo.u4TnlIndex =
            pSetValFsElpsPgWorkingServiceListPointer->
            pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];

        /* Tunnel Instance */
        pServiceListPointerInfo->WorkingInfo.LspServiceInfo.u4TnlInstance =
            pSetValFsElpsPgWorkingServiceListPointer->
            pu4_OidList[ELPS_TNL_INST_START_OFFSET];

        /* IngressLsrId */
        pServiceListPointerInfo->WorkingInfo.LspServiceInfo.u4TnlIngressLsrId =
            pSetValFsElpsPgWorkingServiceListPointer->
            pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];

        /* EgressLsrId */
        pServiceListPointerInfo->WorkingInfo.LspServiceInfo.u4TnlEgressLsrId =
            pSetValFsElpsPgWorkingServiceListPointer->
            pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        if (ElpsPwSetPwServiceInfo
            (&pServiceListPointerInfo->WorkingInfo.PwServiceInfo,
             pSetValFsElpsPgWorkingServiceListPointer) == OSIX_FAILURE)
        {
            ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                       "nmhSetFsElpsPgWorkingServiceListPointer:"
                       " Allocation of memory for Mpls In Api Info "
                       "FAILED!!!\r\n"));
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgWorkingReverseServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                setValFsElpsPgWorkingReverseServiceListPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgWorkingReverseServiceListPointer (UINT4 u4FsElpsContextId,
                                                UINT4 u4FsElpsPgConfigPgId,
                                                UINT4 u4FsElpsPgServiceListId,
                                                tSNMP_OID_TYPE *
                                                pSetValFsElpsPgWorkingReverseServiceListPointer)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);
    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    /* Tunnel Index */
    pServiceListPointerInfo->WorkingRvrInfo.LspServiceInfo.u4TnlIndex =
        pSetValFsElpsPgWorkingReverseServiceListPointer->
        pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];

    /* Tunnel Instance */
    pServiceListPointerInfo->WorkingRvrInfo.LspServiceInfo.u4TnlInstance =
        pSetValFsElpsPgWorkingReverseServiceListPointer->
        pu4_OidList[ELPS_TNL_INST_START_OFFSET];

    /* IngressLsrId */
    pServiceListPointerInfo->WorkingRvrInfo.
        LspServiceInfo.u4TnlIngressLsrId =
        pSetValFsElpsPgWorkingReverseServiceListPointer->
        pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];

    /* EgressLsrId */
    pServiceListPointerInfo->WorkingRvrInfo.
        LspServiceInfo.u4TnlEgressLsrId =
        pSetValFsElpsPgWorkingReverseServiceListPointer->
        pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgProtectionServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                setValFsElpsPgProtectionServiceListPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgProtectionServiceListPointer (UINT4 u4FsElpsContextId,
                                            UINT4 u4FsElpsPgConfigPgId,
                                            UINT4 u4FsElpsPgServiceListId,
                                            tSNMP_OID_TYPE *
                                            pSetValFsElpsPgProtectionServiceListPointer)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);
    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        /* Tunnel Index */
        pServiceListPointerInfo->ProtectionInfo.
            LspServiceInfo.u4TnlIndex =
            pSetValFsElpsPgProtectionServiceListPointer->
            pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];

        /* Getting the Tunnel Instance */
        pServiceListPointerInfo->ProtectionInfo.
            LspServiceInfo.u4TnlInstance =
            pSetValFsElpsPgProtectionServiceListPointer->
            pu4_OidList[ELPS_TNL_INST_START_OFFSET];

        /* Getting the IngressLsrId */
        pServiceListPointerInfo->ProtectionInfo.
            LspServiceInfo.u4TnlIngressLsrId =
            pSetValFsElpsPgProtectionServiceListPointer->
            pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];

        /* Getting the EgressLsrId */
        pServiceListPointerInfo->ProtectionInfo.
            LspServiceInfo.u4TnlEgressLsrId =
            pSetValFsElpsPgProtectionServiceListPointer->
            pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        if (ElpsPwSetPwServiceInfo
            (&pServiceListPointerInfo->ProtectionInfo.PwServiceInfo,
             pSetValFsElpsPgProtectionServiceListPointer) == OSIX_FAILURE)
        {
            ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                       "nmhSetFsElpsPgWorkingServiceListPointer:"
                       " Allocation of memory for Mpls In Api Info "
                       "FAILED!!!\r\n"));
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsElpsPgProtectionReverseServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                setValFsElpsPgProtectionReverseServiceListPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgProtectionReverseServiceListPointer (UINT4 u4FsElpsContextId,
                                                   UINT4 u4FsElpsPgConfigPgId,
                                                   UINT4
                                                   u4FsElpsPgServiceListId,
                                                   tSNMP_OID_TYPE *
                                                   pSetValFsElpsPgProtectionReverseServiceListPointer)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);
    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    /* Tunnel Index */
    pServiceListPointerInfo->ProtectionRvrInfo.
        LspServiceInfo.u4TnlIndex =
        pSetValFsElpsPgProtectionReverseServiceListPointer->
        pu4_OidList[ELPS_TNL_INDEX_START_OFFSET];

    /* Tunnel Instance */
    pServiceListPointerInfo->ProtectionRvrInfo.
        LspServiceInfo.u4TnlInstance =
        pSetValFsElpsPgProtectionReverseServiceListPointer->
        pu4_OidList[ELPS_TNL_INST_START_OFFSET];

    /* IngressLsrId */
    pServiceListPointerInfo->ProtectionRvrInfo.
        LspServiceInfo.u4TnlIngressLsrId =
        pSetValFsElpsPgProtectionReverseServiceListPointer->
        pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET];

    /* EgressLsrId */
    pServiceListPointerInfo->ProtectionRvrInfo.
        LspServiceInfo.u4TnlEgressLsrId =
        pSetValFsElpsPgProtectionReverseServiceListPointer->
        pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET];

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsElpsPgServiceListPointerRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                setValFsElpsPgServiceListPointerRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgServiceListPointerRowStatus (UINT4 u4FsElpsContextId,
                                           UINT4 u4FsElpsPgConfigPgId,
                                           UINT4 u4FsElpsPgServiceListId,
                                           INT4
                                           i4SetValFsElpsPgServiceListPointerRowStatus)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if ((pServiceListPointerInfo == NULL) &&
        ((i4SetValFsElpsPgServiceListPointerRowStatus == NOT_IN_SERVICE) ||
         (i4SetValFsElpsPgServiceListPointerRowStatus == DESTROY) ||
         (i4SetValFsElpsPgServiceListPointerRowStatus == ACTIVE)))
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }
    else if ((pServiceListPointerInfo != NULL) &&
             (i4SetValFsElpsPgServiceListPointerRowStatus == CREATE_AND_WAIT))
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List entry Pg Id: %d and "
                   "Service List Value: %d is already exists. \r\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        return SNMP_FAILURE;
    }

    switch (i4SetValFsElpsPgServiceListPointerRowStatus)
    {
        case CREATE_AND_WAIT:

            pServiceListPointerInfo = ElpsSrvListPointerCreateNode ();

            if (pServiceListPointerInfo == NULL)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to create Protection Group List Pointer"
                           " entry PgId:%d and Service List Value: %d\n",
                           u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
                return SNMP_FAILURE;
            }

            pServiceListPointerInfo->u4PgId = u4FsElpsPgConfigPgId;
            pServiceListPointerInfo->u4PgServiceListId =
                u4FsElpsPgServiceListId;

            pServiceListPointerInfo->u1RowStatus = NOT_IN_SERVICE;

            if (ElpsSrvListAddNodeToSrvListPtrTable
                (pPgConfigInfo->pContextInfo, pPgConfigInfo,
                 pServiceListPointerInfo) == OSIX_FAILURE)
            {
                ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                           "Unable to Add Protection Group List Pointer"
                           " entry Pg Id:%d and Service List Value: %d\n",
                           u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
                return SNMP_FAILURE;
            }

            break;

        case NOT_IN_SERVICE:

            if (pServiceListPointerInfo->u1RowStatus == NOT_IN_SERVICE)
            {
                return SNMP_SUCCESS;
            }
            pServiceListPointerInfo->u1RowStatus = NOT_IN_SERVICE;

            break;

        case DESTROY:

            ElpsSrvListPtrDelNode (pPgConfigInfo->pContextInfo,
                                   pPgConfigInfo, pServiceListPointerInfo);
            break;

        case ACTIVE:

            if (pServiceListPointerInfo->u1RowStatus != NOT_IN_SERVICE)
            {
                return SNMP_FAILURE;
            }

            pServiceListPointerInfo->u1RowStatus = ACTIVE;
            /* Make the PG status to Not in service */
            ElpsUtilPgCfgNotReadyToNotInServ (pPgConfigInfo);
            break;

        default:

            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgWorkingServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                testValFsElpsPgWorkingServiceListPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgWorkingServiceListPointer (UINT4 *pu4ErrorCode,
                                            UINT4 u4FsElpsContextId,
                                            UINT4 u4FsElpsPgConfigPgId,
                                            UINT4 u4FsElpsPgServiceListId,
                                            tSNMP_OID_TYPE *
                                            pTestValFsElpsPgWorkingServiceListPointer)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of PG Entry %d is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is Individual."
                   "This Configuration is not allowed. \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP) &&
        (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_PW))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Table is not supported"
                   " for Vlan service\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_SRV_PTR_TABLE_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        /* Need to validate the Tunnel Table Base Oid */
        if (MEMCMP (pTestValFsElpsPgWorkingServiceListPointer->pu4_OidList,
                    gau4TnlTableOid,
                    ((ELPS_TNL_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((pTestValFsElpsPgWorkingServiceListPointer->
             pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] == 0) ||
            (pTestValFsElpsPgWorkingServiceListPointer->
             pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] == 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        /* Need to validate the PW Table Base Oid */
        if (MEMCMP (pTestValFsElpsPgWorkingServiceListPointer->
                    pu4_OidList,
                    gau4PwTableOid,
                    ((ELPS_PW_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgWorkingReverseServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                testValFsElpsPgWorkingReverseServiceListPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgWorkingReverseServiceListPointer (UINT4 *pu4ErrorCode,
                                                   UINT4 u4FsElpsContextId,
                                                   UINT4 u4FsElpsPgConfigPgId,
                                                   UINT4
                                                   u4FsElpsPgServiceListId,
                                                   tSNMP_OID_TYPE *
                                                   pTestValFsElpsPgWorkingReverseServiceListPointer)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of PG Entry %d is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is Individual."
                   "This Configuration is not allowed. \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Reverse Service Type is not "
                   "supported for this service\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_SRV_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Need to validate the Tunnel Table Base Oid */
    if (MEMCMP (pTestValFsElpsPgWorkingReverseServiceListPointer->
                pu4_OidList,
                gau4TnlTableOid,
                ((ELPS_TNL_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValFsElpsPgWorkingReverseServiceListPointer->
         pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] == 0) ||
        (pTestValFsElpsPgWorkingReverseServiceListPointer->
         pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgProtectionServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                testValFsElpsPgProtectionServiceListPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgProtectionServiceListPointer (UINT4 *pu4ErrorCode,
                                               UINT4 u4FsElpsContextId,
                                               UINT4 u4FsElpsPgConfigPgId,
                                               UINT4 u4FsElpsPgServiceListId,
                                               tSNMP_OID_TYPE *
                                               pTestValFsElpsPgProtectionServiceListPointer)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of PG Entry %d is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is Individual."
                   "This Configuration is not allowed. \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP) &&
        (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_PW))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service Type is not "
                   "supported for this service\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_SRV_PTR_TABLE_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_LSP)
    {
        /* Need to validate the Tunnel Table Base Oid */
        if (MEMCMP (pTestValFsElpsPgProtectionServiceListPointer->pu4_OidList,
                    gau4TnlTableOid,
                    ((ELPS_TNL_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        if ((pTestValFsElpsPgProtectionServiceListPointer->
             pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] == 0) ||
            (pTestValFsElpsPgProtectionServiceListPointer->
             pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] == 0))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pPgConfigInfo->u1ServiceType == ELPS_PG_SERVICE_TYPE_PW)
    {
        /* Need to validate the PW Table Base Oid */
        if (MEMCMP (pTestValFsElpsPgProtectionServiceListPointer->
                    pu4_OidList,
                    gau4PwTableOid,
                    ((ELPS_PW_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgProtectionReverseServiceListPointer
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                testValFsElpsPgProtectionReverseServiceListPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgProtectionReverseServiceListPointer (UINT4 *pu4ErrorCode,
                                                      UINT4 u4FsElpsContextId,
                                                      UINT4
                                                      u4FsElpsPgConfigPgId,
                                                      UINT4
                                                      u4FsElpsPgServiceListId,
                                                      tSNMP_OID_TYPE *
                                                      pTestValFsElpsPgProtectionReverseServiceListPointer)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of PG Entry %d is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is Individual."
                   "This Configuration is not allowed. \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service Pointer Table is not supported"
                   " for this Service type\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_RVR_SRV_PTR_NOT_SUPPORTED);
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if (pServiceListPointerInfo == NULL)
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List Pointer entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    /* Need to validate the Tunnel Table Base Oid */
    if (MEMCMP
        (pTestValFsElpsPgProtectionReverseServiceListPointer->pu4_OidList,
         gau4TnlTableOid, ((ELPS_TNL_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValFsElpsPgProtectionReverseServiceListPointer->
         pu4_OidList[ELPS_TNL_ING_LSRID_START_OFFSET] == 0) ||
        (pTestValFsElpsPgProtectionReverseServiceListPointer->
         pu4_OidList[ELPS_TNL_EGG_LSRID_START_OFFSET] == 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgServiceListPointerRowStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId

                The Object
                testValFsElpsPgServiceListPointerRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgServiceListPointerRowStatus (UINT4 *pu4ErrorCode,
                                              UINT4 u4FsElpsContextId,
                                              UINT4 u4FsElpsPgConfigPgId,
                                              UINT4 u4FsElpsPgServiceListId,
                                              INT4
                                              i4TestValFsElpsPgServiceListPointerRowStatus)
{
    tElpsServiceListPointerInfo *pServiceListPointerInfo = NULL;
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((i4TestValFsElpsPgServiceListPointerRowStatus < ACTIVE) ||
        (i4TestValFsElpsPgServiceListPointerRowStatus > DESTROY))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Service List Pointer Rowstatus is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsElpsPgServiceListPointerRowStatus == CREATE_AND_GO)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Service List Pointer Rowstatus is not valid\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgRowStatus == ACTIVE)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Row Status of PG Entry %d is Active\n",
                   u4FsElpsPgConfigPgId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_ELPS_ERR_PG_ACTIVE);
        return SNMP_FAILURE;
    }

    if (pPgConfigInfo->u1PgConfigType == ELPS_PG_TYPE_INDIVIDUAL)
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Config Type is Individual."
                   "This Configuration is not allowed. \r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_LSP) &&
        (pPgConfigInfo->u1ServiceType != ELPS_PG_SERVICE_TYPE_PW))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service Pointer Table is not supported"
                   " for this Service type\r\n"));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pServiceListPointerInfo =
        ElpsSrvListPtrGetNode (pPgConfigInfo->pContextInfo,
                               u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId);

    if ((pServiceListPointerInfo == NULL) &&
        ((i4TestValFsElpsPgServiceListPointerRowStatus == NOT_IN_SERVICE) ||
         (i4TestValFsElpsPgServiceListPointerRowStatus == DESTROY) ||
         (i4TestValFsElpsPgServiceListPointerRowStatus == ACTIVE)))
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List entry Pg Id: %d and "
                   "Service List Value: %d is not created\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else if ((pServiceListPointerInfo != NULL) &&
             (i4TestValFsElpsPgServiceListPointerRowStatus == CREATE_AND_WAIT))
    {
        ELPS_TRC ((pPgConfigInfo->pContextInfo, MGMT_TRC | ALL_FAILURE_TRC,
                   "Protection Group Service List entry Pg Id: %d and "
                   "Service List Value: %d is already exists. \r\n",
                   u4FsElpsPgConfigPgId, u4FsElpsPgServiceListId));
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElpsPgServiceListPointerTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
                FsElpsPgServiceListId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsPgServiceListPointerTable (UINT4
                                         *pu4ErrorCode,
                                         tSnmpIndexList
                                         *
                                         pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsElpsPgShareTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElpsPgShareTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgShareProtectionPort
                FsElpsPgConfigPgId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElpsPgShareTable (UINT4 u4FsElpsContextId,
                                            INT4
                                            i4FsElpsPgShareProtectionPort,
                                            UINT4 u4FsElpsPgConfigPgId)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    if ((i4FsElpsPgShareProtectionPort <= 0) ||
        (i4FsElpsPgShareProtectionPort > BRG_MAX_PHY_PLUS_LAG_INT_PORTS_EXT))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC, "Invalid "
                   "Protection Port Id %d \r\n",
                   i4FsElpsPgShareProtectionPort));
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElpsPgShareTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgShareProtectionPort
                FsElpsPgConfigPgId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElpsPgShareTable (UINT4 *pu4FsElpsContextId,
                                    INT4
                                    *pi4FsElpsPgShareProtectionPort,
                                    UINT4 *pu4FsElpsPgConfigPgId)
{
    tElpsPgInfo        *pPgShareInfo = NULL;
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4Result = 0;

    for (; u4ContextId < ELPS_MAX_CONTEXTS; u4ContextId++)
    {
        pContextInfo = ElpsCxtGetNode (u4ContextId);
        i4Result = ElpsUtilIsElpsStarted (u4ContextId);

        if ((pContextInfo != NULL) && (i4Result == OSIX_TRUE))
        {
            pPgShareInfo = ElpsPgShareGetFirstNode (pContextInfo);

            if (pPgShareInfo != NULL)
            {
                *pu4FsElpsContextId = u4ContextId;
                *pi4FsElpsPgShareProtectionPort =
                    pPgShareInfo->ProtectionEntity.u4PortId;
                *pu4FsElpsPgConfigPgId = pPgShareInfo->u4PgId;
                return SNMP_SUCCESS;
            }
        }
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "No Protection Group Share Table entry exists.\r\n"));

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElpsPgShareTable
 Input       :  The Indices
                FsElpsContextId
                nextFsElpsContextId
                FsElpsPgShareProtectionPort
                nextFsElpsPgShareProtectionPort
                FsElpsPgConfigPgId
                nextFsElpsPgConfigPgId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElpsPgShareTable (UINT4 u4FsElpsContextId,
                                   UINT4 *pu4NextFsElpsContextId,
                                   INT4 i4FsElpsPgShareProtectionPort,
                                   INT4
                                   *pi4NextFsElpsPgShareProtectionPort,
                                   UINT4 u4FsElpsPgConfigPgId,
                                   UINT4 *pu4NextFsElpsPgConfigPgId)
{
    tElpsPgInfo         PgShareInfo;
    tElpsContextInfo   *pContextInfo = NULL;
    tElpsPgInfo        *pNextPgShareInfo = NULL;
    UINT4               u4ContextId = 0;
    INT4                i4Result = 0;

    MEMSET (&PgShareInfo, 0, sizeof (tElpsPgInfo));

    PgShareInfo.ProtectionEntity.u4PortId =
        (UINT4) i4FsElpsPgShareProtectionPort;
    PgShareInfo.u4PgId = u4FsElpsPgConfigPgId;

    for (u4ContextId = u4FsElpsContextId; u4ContextId < ELPS_MAX_CONTEXTS;
         u4ContextId++)
    {
        pContextInfo = ElpsCxtGetNode (u4ContextId);
        i4Result = ElpsUtilIsElpsStarted (u4ContextId);

        if ((pContextInfo != NULL) && (i4Result == OSIX_TRUE))
        {
            if (u4ContextId != u4FsElpsContextId)
            {
                pNextPgShareInfo = ElpsPgShareGetFirstNode (pContextInfo);
            }
            else
            {
                pNextPgShareInfo = ElpsPgShareGetNextNode (pContextInfo,
                                                           &PgShareInfo);
            }

            if (pNextPgShareInfo != NULL)
            {
                *pu4NextFsElpsContextId = u4ContextId;
                *pi4NextFsElpsPgShareProtectionPort =
                    (INT4) pNextPgShareInfo->ProtectionEntity.u4PortId;
                *pu4NextFsElpsPgConfigPgId = pNextPgShareInfo->u4PgId;
                return SNMP_SUCCESS;
            }
        }
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Next Protection Group Share Table entry to Prot Port: %d and "
               "Pg Id: %d is Not created for\n",
               i4FsElpsPgShareProtectionPort, u4FsElpsPgConfigPgId));

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsPgSharePgStatus
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgShareProtectionPort
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgSharePgStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgSharePgStatus (UINT4 u4FsElpsContextId,
                             INT4 i4FsElpsPgShareProtectionPort,
                             UINT4 u4FsElpsPgConfigPgId,
                             INT4 *pi4RetValFsElpsPgSharePgStatus)
{
    tElpsPgInfo        *pPgShareInfo = NULL;
    tElpsContextInfo   *pContextInfo = NULL;
    UINT4               u4Dummy = 0;

    pContextInfo = ElpsUtilValidateContextInfo (u4FsElpsContextId, &u4Dummy);

    if (pContextInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pPgShareInfo = ElpsPgShareGetNode (pContextInfo,
                                       i4FsElpsPgShareProtectionPort,
                                       u4FsElpsPgConfigPgId);

    if (pPgShareInfo != NULL)
    {
        *pi4RetValFsElpsPgSharePgStatus = pPgShareInfo->u1PgStatus;
        return SNMP_SUCCESS;
    }

    ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
               "Protection Group Share Table entry to Prot Portd: %d and "
               "Pg Id: %d is Not created for\n",
               i4FsElpsPgShareProtectionPort, u4FsElpsPgConfigPgId));
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsElpsPgStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsElpsPgStatsTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsElpsPgStatsTable (UINT4 u4FsElpsContextId,
                                            UINT4 u4FsElpsPgConfigPgId)
{
    return (nmhValidateIndexInstanceFsElpsPgConfigTable (u4FsElpsContextId,
                                                         u4FsElpsPgConfigPgId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsElpsPgStatsTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsElpsPgStatsTable (UINT4 *pu4FsElpsContextId,
                                    UINT4 *pu4FsElpsPgConfigPgId)
{
    return (nmhGetFirstIndexFsElpsPgConfigTable (pu4FsElpsContextId,
                                                 pu4FsElpsPgConfigPgId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsElpsPgStatsTable
 Input       :  The Indices
                FsElpsContextId
                nextFsElpsContextId
                FsElpsPgConfigPgId
                nextFsElpsPgConfigPgId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsElpsPgStatsTable (UINT4 u4FsElpsContextId,
                                   UINT4 *pu4NextFsElpsContextId,
                                   UINT4 u4FsElpsPgConfigPgId,
                                   UINT4 *pu4NextFsElpsPgConfigPgId)
{
    return (nmhGetNextIndexFsElpsPgConfigTable (u4FsElpsContextId,
                                                pu4NextFsElpsContextId,
                                                u4FsElpsPgConfigPgId,
                                                pu4NextFsElpsPgConfigPgId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsElpsPgStatsAutoProtectionSwitchCount
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgStatsAutoProtectionSwitchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgStatsAutoProtectionSwitchCount (UINT4 u4FsElpsContextId,
                                              UINT4 u4FsElpsPgConfigPgId,
                                              UINT4
                                              *pu4RetValFsElpsPgStatsAutoProtectionSwitchCount)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgStatsAutoProtectionSwitchCount =
        pPgConfigInfo->Stats.u4AutoSwitchCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgStatsForcedSwitchCount
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgStatsForcedSwitchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgStatsForcedSwitchCount (UINT4 u4FsElpsContextId,
                                      UINT4 u4FsElpsPgConfigPgId,
                                      UINT4
                                      *pu4RetValFsElpsPgStatsForcedSwitchCount)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgStatsForcedSwitchCount =
        pPgConfigInfo->Stats.u4ForceSwitchCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgStatsManualSwitchCount
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgStatsManualSwitchCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgStatsManualSwitchCount (UINT4 u4FsElpsContextId,
                                      UINT4 u4FsElpsPgConfigPgId,
                                      UINT4
                                      *pu4RetValFsElpsPgStatsManualSwitchCount)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgStatsManualSwitchCount =
        pPgConfigInfo->Stats.u4ManualSwitchCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgStatsClearStatistics
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgStatsClearStatistics
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgStatsClearStatistics (UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    INT4 *pi4RetValFsElpsPgStatsClearStatistics)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsElpsPgStatsClearStatistics = ELPS_SNMP_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgStatsApsPktTxCount
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgStatsApsPktTxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgStatsApsPktTxCount (UINT4 u4FsElpsContextId,
                                  UINT4 u4FsElpsPgConfigPgId,
                                  UINT4 *pu4RetValFsElpsPgStatsApsPktTxCount)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgStatsApsPktTxCount =
        pPgConfigInfo->Stats.u4TotalApsTxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgStatsApsPktRxCount
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgStatsApsPktRxCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgStatsApsPktRxCount (UINT4 u4FsElpsContextId,
                                  UINT4 u4FsElpsPgConfigPgId,
                                  UINT4 *pu4RetValFsElpsPgStatsApsPktRxCount)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgStatsApsPktRxCount =
        pPgConfigInfo->Stats.u4TotalApsRxCount;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgStatsApsPktDiscardCount
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object
                retValFsElpsPgStatsApsPktDiscardCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgStatsApsPktDiscardCount (UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       UINT4
                                       *pu4RetValFsElpsPgStatsApsPktDiscardCount)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgStatsApsPktDiscardCount =
        pPgConfigInfo->Stats.u4TotalApsDiscardCount;

    return SNMP_SUCCESS;
}

/* performance measurement */
/****************************************************************************
 Function    :  nmhGetFsElpsPgLRSFRxTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgLRSFRxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgLRSFRxTime (UINT4 u4FsElpsContextId, UINT4 u4FsElpsPgConfigPgId,
                          UINT4 *pu4RetValFsElpsPgLRSFRxTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4ErrCode = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4ErrCode);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgLRSFRxTime = pPgConfigInfo->Perf.u4LRSFRxTime;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsElpsPgLRSFTxTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgLRSFTxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgLRSFTxTime (UINT4 u4FsElpsContextId, UINT4 u4FsElpsPgConfigPgId,
                          UINT4 *pu4RetValFsElpsPgLRSFTxTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4ErrCode = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4ErrCode);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgLRSFTxTime = pPgConfigInfo->Perf.u4LRSFTxTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgFRSFRxTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgFRSFRxTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgFRSFRxTime (UINT4 u4FsElpsContextId, UINT4 u4FsElpsPgConfigPgId,
                          UINT4 *pu4RetValFsElpsPgFRSFRxTime)
{

    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4ErrCode = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4ErrCode);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgFRSFRxTime = pPgConfigInfo->Perf.u4FRSFRxTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsElpsPgStateChgTime
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                retValFsElpsPgStateChgTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsElpsPgStateChgTime (UINT4 u4FsElpsContextId, UINT4 u4FsElpsPgConfigPgId,
                            UINT4 *pu4RetValFsElpsPgStateChgTime)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4ErrCode = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4ErrCode);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsElpsPgStateChgTime = pPgConfigInfo->Perf.u4StateChgTime;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsElpsPgStatsClearStatistics
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                setValFsElpsPgStatsClearStatistics
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsElpsPgStatsClearStatistics (UINT4 u4FsElpsContextId,
                                    UINT4 u4FsElpsPgConfigPgId,
                                    INT4 i4SetValFsElpsPgStatsClearStatistics)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;
    UINT4               u4Dummy = 0;

    pPgConfigInfo = ElpsUtilValidateCxtAndPgInfo
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, &u4Dummy);

    if (pPgConfigInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsElpsPgStatsClearStatistics == ELPS_SNMP_TRUE)
    {
        pPgConfigInfo->Stats.u4AutoSwitchCount = 0;
        pPgConfigInfo->Stats.u4ForceSwitchCount = 0;
        pPgConfigInfo->Stats.u4ManualSwitchCount = 0;
        pPgConfigInfo->Stats.u4TotalApsTxCount = 0;
        pPgConfigInfo->Stats.u4TotalApsRxCount = 0;
        pPgConfigInfo->Stats.u4TotalApsDiscardCount = 0;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsElpsPgStatsClearStatistics
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId

                The Object 
                testValFsElpsPgStatsClearStatistics
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsElpsPgStatsClearStatistics (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsElpsContextId,
                                       UINT4 u4FsElpsPgConfigPgId,
                                       INT4
                                       i4TestValFsElpsPgStatsClearStatistics)
{
    tElpsPgInfo        *pPgConfigInfo = NULL;

    if ((i4TestValFsElpsPgStatsClearStatistics != ELPS_SNMP_TRUE) &&
        (i4TestValFsElpsPgStatsClearStatistics != ELPS_SNMP_FALSE))
    {
        ELPS_TRC ((NULL, MGMT_TRC | ALL_FAILURE_TRC,
                   "Invalid Value as input \r\n"));
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pPgConfigInfo = ElpsUtilValContextAndProtGroup
        (u4FsElpsContextId, u4FsElpsPgConfigPgId, pu4ErrorCode);

    if (pPgConfigInfo == NULL)
    {
        /* As the function ElpsUtilValContextAndProtGroup itself will take care 
         * of filling the error code, CLI set error and trace messages. 
         * So simply return failure. */
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsElpsPgStatsTable
 Input       :  The Indices
                FsElpsContextId
                FsElpsPgConfigPgId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsElpsPgStatsTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 *  Function    :  nmhGetFsElpsStatsOneIsToOneApsPktTxCount
 *  Input       :  The Indices
 *                 The Object
 *                 retValFsElpsStatsOneIsToOneApsPktTxCount
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1                nmhGetFsElpsStatsOneIsToOneApsPktTxCount
    (UINT4 *pu4RetValFsElpsStatsOneIsToOneApsPktTxCount)
{
    /* Get No. of Packets Sent for all protection groups in all contexts
     * when protection type is 1:1 */

    *pu4RetValFsElpsStatsOneIsToOneApsPktTxCount =
        ElpsGetPktCountForAllContexts (ELPS_B_BIT_VALUE,
                                       nmhGetFsElpsPgStatsApsPktTxCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsElpsStatsOneIsToOneApsPktRxCount
 *  Input       :  The Indices
 *                 The Object
 *                 retValFsElpsStatsOneIsToOneApsPktRxCount
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1                nmhGetFsElpsStatsOneIsToOneApsPktRxCount
    (UINT4 *pu4RetValFsElpsStatsOneIsToOneApsPktRxCount)
{
    /* Get No. of Packets received for all protection groups in all contexts
     * when protection type is 1:1 */

    *pu4RetValFsElpsStatsOneIsToOneApsPktRxCount =
        ElpsGetPktCountForAllContexts (ELPS_B_BIT_VALUE,
                                       nmhGetFsElpsPgStatsApsPktRxCount);
    return SNMP_SUCCESS;

}

/****************************************************************************
 *  Function    :  nmhGetFsElpsStatsOneIsToOneApsPktDiscardCount
 *  Input       :  The Indices
 *                 The Object
 *                 retValFsElpsStatsOneIsToOneApsPktDiscardCount
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1                nmhGetFsElpsStatsOneIsToOneApsPktDiscardCount
    (UINT4 *pu4RetValFsElpsStatsOneIsToOneApsPktDiscardCount)
{
    /* Get No. of Packets discarded for all protection groups in all contexts
     * when protection type is 1:1 */

    *pu4RetValFsElpsStatsOneIsToOneApsPktDiscardCount =
        ElpsGetPktCountForAllContexts (ELPS_B_BIT_VALUE,
                                       nmhGetFsElpsPgStatsApsPktDiscardCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsElpsStatsOnePlusOneApsPktTxCount
 *  Input       :  The Indices
 *                 The Object
 *                 retValFsElpsStatsOnePlusOneApsPktTxCount
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1                nmhGetFsElpsStatsOnePlusOneApsPktTxCount
    (UINT4 *pu4RetValFsElpsStatsOnePlusOneApsPktTxCount)
{
    /* Get No. of Packets Sent for all protection groups in all contexts
     * when protection type is 1+1 */

    *pu4RetValFsElpsStatsOnePlusOneApsPktTxCount =
        ElpsGetPktCountForAllContexts (ELPS_ZERO,
                                       nmhGetFsElpsPgStatsApsPktTxCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsElpsStatsOnePlusOneApsPktRxCount
 *  Input       :  The Indices
 *                 The Object
 *                 retValFsElpsStatsOnePlusOneApsPktRxCount
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1                nmhGetFsElpsStatsOnePlusOneApsPktRxCount
    (UINT4 *pu4RetValFsElpsStatsOnePlusOneApsPktRxCount)
{
    /* Get No. of Packets received for all protection groups in all contexts
     * when protection type is 1+1 */

    *pu4RetValFsElpsStatsOnePlusOneApsPktRxCount =
        ElpsGetPktCountForAllContexts (ELPS_ZERO,
                                       nmhGetFsElpsPgStatsApsPktRxCount);
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsElpsStatsOnePlusOneApsPktDiscardCount
 *  Input       :  The Indices
 *                 The Object
 *                 retValFsElpsStatsOnePlusOneApsPktDiscardCount
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1                nmhGetFsElpsStatsOnePlusOneApsPktDiscardCount
    (UINT4 *pu4RetValFsElpsStatsOnePlusOneApsPktDiscardCount)
{
    /* Get No. of Packets discarded for all protection groups in all contexts
     * when protection type is 1+1 */

    *pu4RetValFsElpsStatsOnePlusOneApsPktDiscardCount =
        ElpsGetPktCountForAllContexts (ELPS_ZERO,
                                       nmhGetFsElpsPgStatsApsPktDiscardCount);

    return SNMP_SUCCESS;
}
