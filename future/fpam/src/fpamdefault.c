/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamdefault.c,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Fpam 
*********************************************************************/

#include "fpaminc.h"

/****************************************************************************
* Function    : FpamInitializeFsusrMgmtTable
* Input       : pFpamFsUsrMgmtEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
FpamInitializeFsusrMgmtTable (tFpamFsUsrMgmtEntry * pFpamFsUsrMgmtEntry)
{
    if (pFpamFsUsrMgmtEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((FpamInitializeMibFsusrMgmtTable (pFpamFsUsrMgmtEntry)) == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}
