/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamdbg.c,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Fpam 
*********************************************************************/

#include "fpaminc.h"

/****************************************************************************
 Function    :  FpamGetAllFsusrMgmtTable
 Input       :  pFpamGetFsUsrMgmtEntry
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetAllFsusrMgmtTable (tFpamFsUsrMgmtEntry * pFpamGetFsUsrMgmtEntry)
{
    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;

    /* Check whether the node is already present */
    pFpamFsUsrMgmtEntry =
        RBTreeGet (gFpamGlobals.FpamGlbMib.FsusrMgmtTable,
                   (tRBElem *) pFpamGetFsUsrMgmtEntry);

    if (pFpamFsUsrMgmtEntry == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetAllFsusrMgmtTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pFpamGetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
            pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen);

    MEMCPY (&(pFpamGetFsUsrMgmtEntry->MibObject.
              TmFsusrMgmtUserPasswordCreationTime),
            &(pFpamFsUsrMgmtEntry->MibObject.
              TmFsusrMgmtUserPasswordCreationTime), sizeof (tFpamUtlTm));

    pFpamGetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen =
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen;

    pFpamGetFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege =
        pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege;

    pFpamGetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserLoginCount =
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserLoginCount;

    pFpamGetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus =
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus;

    pFpamGetFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime =
        pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime;

    pFpamGetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus =
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus;

    MEMCPY (pFpamGetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserConfirmPwd,
            pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserConfirmPwd,
            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen);

    pFpamGetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen =
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen;

    pFpamGetFsUsrMgmtEntry->MibObject.i4FsusrMgmtLoginAttempts =
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtLoginAttempts;

    if (FpamGetAllUtlFsusrMgmtTable
        (pFpamGetFsUsrMgmtEntry, pFpamFsUsrMgmtEntry) == OSIX_FAILURE)

    {
        FPAM_TRC ((FPAM_UTIL_TRC, "FpamGetAllFsusrMgmtTable:"
                   "FpamGetAllUtlFsusrMgmtTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamSetAllFsusrMgmtTable
 Input       :  pFpamSetFsUsrMgmtEntry
                pFpamIsSetFsUsrMgmtEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamSetAllFsusrMgmtTable (tFpamFsUsrMgmtEntry * pFpamSetFsUsrMgmtEntry,
                          tFpamIsSetFsUsrMgmtEntry * pFpamIsSetFsUsrMgmtEntry,
                          INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;
    tFpamFsUsrMgmtEntry FpamOldFsUsrMgmtEntry;
    tFpamFsUsrMgmtEntry FpamTrgFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamTrgIsSetFsUsrMgmtEntry;
    INT4                i4RowMakeActive = FALSE;
    BOOL1               bConfirmPwd = FALSE;

    MEMSET (&FpamOldFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamTrgFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamTrgIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Check whether the node is already present */
    pFpamFsUsrMgmtEntry =
        RBTreeGet (gFpamGlobals.FpamGlbMib.FsusrMgmtTable,
                   (tRBElem *) pFpamSetFsUsrMgmtEntry);

    if (FPAM_SUCCESS ==
        FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
    {
        bConfirmPwd = TRUE;
    }

    if (pFpamFsUsrMgmtEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
             CREATE_AND_WAIT)
            || (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
                CREATE_AND_GO)
            ||
            ((pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pFpamFsUsrMgmtEntry =
                (tFpamFsUsrMgmtEntry *)
                MemAllocMemBlk (FPAM_FSUSRMGMTTABLE_POOLID);
            if (pFpamFsUsrMgmtEntry == NULL)
            {
                if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                                     pFpamIsSetFsUsrMgmtEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamSetAllFsusrMgmtTable:FpamSetAllFsusrMgmtTableTrigger function fails\r\n"));

                }
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamSetAllFsusrMgmtTable: Fail to Allocate Memory.\r\n"));
                return OSIX_FAILURE;
            }

            MEMSET (pFpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
            if ((FpamInitializeFsusrMgmtTable (pFpamFsUsrMgmtEntry)) ==
                OSIX_FAILURE)
            {
                if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                                     pFpamIsSetFsUsrMgmtEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamSetAllFsusrMgmtTable:FpamSetAllFsusrMgmtTableTrigger function fails\r\n"));

                }
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamSetAllFsusrMgmtTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (FPAM_FSUSRMGMTTABLE_POOLID,
                                    (UINT1 *) pFpamFsUsrMgmtEntry);

                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserName != OSIX_FALSE)
            {
                MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName,
                        pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName,
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtUserNameLen);

                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen =
                    pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen;
            }

            if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtAuthString != OSIX_FALSE)
            {
                MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString,
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        au1FsusrMgmtAuthString,
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtAuthStringLen);

                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen =
                    pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen;
            }

            if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword != OSIX_FALSE)
            {
                MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        au1FsusrMgmtUserPassword,
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtUserPasswordLen);

                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen =
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserPasswordLen;

                if (pFpamIsSetFsUsrMgmtEntry->
                    bTmFsusrMgmtUserPasswordCreationTime != OSIX_FALSE)
                {
                    FpamGetCurrentTime (&(pFpamFsUsrMgmtEntry->MibObject.
                                          TmFsusrMgmtUserPasswordCreationTime));
                }
                else
                {
                    MEMCPY (&(pFpamFsUsrMgmtEntry->MibObject.
                              TmFsusrMgmtUserPasswordCreationTime),
                            &(pFpamSetFsUsrMgmtEntry->MibObject.
                              TmFsusrMgmtUserPasswordCreationTime),
                            sizeof (tFpamUtlTm));

                }
                if (bConfirmPwd == FALSE)
                {
                    MEMCPY (pFpamFsUsrMgmtEntry->MibObject.
                            au1FsusrMgmtUserConfirmPwd,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            au1FsusrMgmtUserPassword,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            i4FsusrMgmtUserPasswordLen);

                    pFpamFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtUserConfirmPwdLen =
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtUserPasswordLen;
                }
            }

            if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege != OSIX_FALSE)
            {
                pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege =
                    pFpamSetFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege;
            }

            if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus != OSIX_FALSE)
            {
                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus =
                    pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus;
            }

            if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime !=
                OSIX_FALSE)
            {
                pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime =
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    u4FsusrMgmtUserLockRelTime;
            }

            if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus != OSIX_FALSE)
            {
                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus =
                    pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus;
            }

            if (bConfirmPwd == TRUE)
            {
                if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserConfirmPwd !=
                    OSIX_FALSE)
                {
                    MEMCPY (pFpamFsUsrMgmtEntry->MibObject.
                            au1FsusrMgmtUserConfirmPwd,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            au1FsusrMgmtUserConfirmPwd,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            i4FsusrMgmtUserConfirmPwdLen);

                    pFpamFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtUserConfirmPwdLen =
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtUserConfirmPwdLen;
                }
            }

            if (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
                CREATE_AND_GO)
            {
                gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsEnableUsers++;
                gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsNumOfUsers++;
                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus =
                    ACTIVE;
            }
            if ((i4RowCreateOption == 1)
                && (pFpamSetFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserRowStatus == ACTIVE))
            {
                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus =
                    ACTIVE;
            }
            else if (pFpamSetFsUsrMgmtEntry->MibObject.
                     i4FsusrMgmtUserRowStatus == CREATE_AND_WAIT)
            {
                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gFpamGlobals.FpamGlbMib.FsusrMgmtTable,
                 (tRBElem *) pFpamFsUsrMgmtEntry) != RB_SUCCESS)
            {
                if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                                     pFpamIsSetFsUsrMgmtEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
                }
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamSetAllFsusrMgmtTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (FPAM_FSUSRMGMTTABLE_POOLID,
                                    (UINT1 *) pFpamFsUsrMgmtEntry);
                return OSIX_FAILURE;
            }
            if (FpamUtilUpdateFsusrMgmtTable
                (NULL, pFpamFsUsrMgmtEntry,
                 pFpamIsSetFsUsrMgmtEntry) != OSIX_SUCCESS)
            {
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamSetAllFsusrMgmtTable: FpamUtilUpdateFsusrMgmtTable function returns failure.\r\n"));

                if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                                     pFpamIsSetFsUsrMgmtEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));

                }
                RBTreeRem (gFpamGlobals.FpamGlbMib.FsusrMgmtTable,
                           pFpamFsUsrMgmtEntry);
                MemReleaseMemBlock (FPAM_FSUSRMGMTTABLE_POOLID,
                                    (UINT1 *) pFpamFsUsrMgmtEntry);
                return OSIX_FAILURE;
            }

            if ((pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pFpamSetFsUsrMgmtEntry->MibObject.
                                        i4FsusrMgmtUserRowStatus == ACTIVE)))
            {

                if (pFpamSetFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserRowStatus == CREATE_AND_GO)
                {
                    /* For MSR and RM Trigger */
                    MEMCPY (FpamTrgFsUsrMgmtEntry.MibObject.
                            au1FsusrMgmtUserName,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            au1FsusrMgmtUserName,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            i4FsusrMgmtUserNameLen);

                    FpamTrgFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtUserNameLen;
                    MEMCPY (FpamTrgFsUsrMgmtEntry.MibObject.
                            au1FsusrMgmtAuthString,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            au1FsusrMgmtAuthString,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            i4FsusrMgmtAuthStringLen);

                    FpamTrgFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtAuthStringLen;
                    FpamTrgFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus =
                        CREATE_AND_GO;
                    FpamTrgIsSetFsUsrMgmtEntry.bFsusrMgmtUserRowStatus =
                        OSIX_TRUE;

                }
                else
                {
                    /* For MSR and RM Trigger */
                    MEMCPY (FpamTrgFsUsrMgmtEntry.MibObject.
                            au1FsusrMgmtUserName,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            au1FsusrMgmtUserName,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            i4FsusrMgmtUserNameLen);

                    FpamTrgFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtUserNameLen;
                    MEMCPY (FpamTrgFsUsrMgmtEntry.MibObject.
                            au1FsusrMgmtAuthString,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            au1FsusrMgmtAuthString,
                            pFpamSetFsUsrMgmtEntry->MibObject.
                            i4FsusrMgmtAuthStringLen);

                    FpamTrgFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
                        pFpamSetFsUsrMgmtEntry->MibObject.
                        i4FsusrMgmtAuthStringLen;
                    FpamTrgFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus =
                        CREATE_AND_WAIT;
                    FpamTrgIsSetFsUsrMgmtEntry.bFsusrMgmtUserRowStatus =
                        OSIX_TRUE;

                }
                if (FpamSetAllFsusrMgmtTableTrigger (&FpamTrgFsUsrMgmtEntry,
                                                     &FpamTrgIsSetFsUsrMgmtEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (FPAM_FSUSRMGMTTABLE_POOLID,
                                        (UINT1 *) pFpamFsUsrMgmtEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pFpamSetFsUsrMgmtEntry->MibObject.
                     i4FsusrMgmtUserRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                FpamTrgFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus =
                    CREATE_AND_WAIT;
                FpamTrgIsSetFsUsrMgmtEntry.bFsusrMgmtUserRowStatus = OSIX_TRUE;

                if (FpamSetAllFsusrMgmtTableTrigger (&FpamTrgFsUsrMgmtEntry,
                                                     &FpamTrgIsSetFsUsrMgmtEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)
                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (FPAM_FSUSRMGMTTABLE_POOLID,
                                        (UINT1 *) pFpamFsUsrMgmtEntry);
                    return OSIX_FAILURE;
                }
            }
            if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                                 pFpamIsSetFsUsrMgmtEntry,
                                                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamSetAllFsusrMgmtTable:  FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
            }
            if (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
                ACTIVE)
            {
                /* Incrementing number of users */
                gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsNumOfUsers++;
                if (FPAM_USER_ENABLE ==
                    pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus)
                {
                    gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsEnableUsers++;
                }
                else
                {
                    gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsDisableUsers++;
                }
            }
            return OSIX_SUCCESS;

        }
        else
        {
            if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                                 pFpamIsSetFsUsrMgmtEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
            }
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamSetAllFsusrMgmtTable: Failure.\r\n"));
            return OSIX_FAILURE;
        }
    }
    else if ((pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
              CREATE_AND_WAIT)
             || (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
                 CREATE_AND_GO))
    {
        if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                             pFpamIsSetFsUsrMgmtEntry,
                                             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
        }
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetAllFsusrMgmtTable: The row is already present.\r\n"));
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (&FpamOldFsUsrMgmtEntry, pFpamFsUsrMgmtEntry,
            sizeof (tFpamFsUsrMgmtEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus == DESTROY)
    {
        if (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus == ACTIVE)
        {
            if (FPAM_USER_ENABLE ==
                pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus)
            {
                gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsEnableUsers--;
            }
            else
            {
                gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsDisableUsers--;
            }
            /* Decrementing number of users - In case of DESTROY */
            gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsNumOfUsers--;
        }

        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus = DESTROY;

        if (FpamUtilUpdateFsusrMgmtTable (&FpamOldFsUsrMgmtEntry,
                                          pFpamFsUsrMgmtEntry,
                                          pFpamIsSetFsUsrMgmtEntry) !=
            OSIX_SUCCESS)
        {

            if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                                 pFpamIsSetFsUsrMgmtEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
            }
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamSetAllFsusrMgmtTable: FpamUtilUpdateFsusrMgmtTable function returns failure.\r\n"));
        }
        RBTreeRem (gFpamGlobals.FpamGlbMib.FsusrMgmtTable, pFpamFsUsrMgmtEntry);
        if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                             pFpamIsSetFsUsrMgmtEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (FPAM_FSUSRMGMTTABLE_POOLID,
                            (UINT1 *) pFpamFsUsrMgmtEntry);

        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsusrMgmtTableFilterInputs (pFpamFsUsrMgmtEntry, pFpamSetFsUsrMgmtEntry,
                                    pFpamIsSetFsUsrMgmtEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus == ACTIVE) &&
        (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus !=
         NOT_IN_SERVICE))
    {
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        FpamTrgFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus =
            NOT_IN_SERVICE;
        FpamTrgIsSetFsUsrMgmtEntry.bFsusrMgmtUserRowStatus = OSIX_TRUE;

        if (FpamUtilUpdateFsusrMgmtTable (&FpamOldFsUsrMgmtEntry,
                                          pFpamFsUsrMgmtEntry,
                                          pFpamIsSetFsUsrMgmtEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pFpamFsUsrMgmtEntry, &FpamOldFsUsrMgmtEntry,
                    sizeof (tFpamFsUsrMgmtEntry));
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamSetAllFsusrMgmtTable:                 FpamUtilUpdateFsusrMgmtTable Function returns failure.\r\n"));

            if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                                 pFpamIsSetFsUsrMgmtEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
            }
            return OSIX_FAILURE;
        }

        if (FpamSetAllFsusrMgmtTableTrigger (&FpamTrgFsUsrMgmtEntry,
                                             &FpamTrgIsSetFsUsrMgmtEntry,
                                             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
        }
    }

    if (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword != OSIX_FALSE)
    {
        MEMSET (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword, 0,
                FPAM_MAX_PASSWD_LEN);
        MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
                pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen);

        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen =
            pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen;

        if (pFpamIsSetFsUsrMgmtEntry->bTmFsusrMgmtUserPasswordCreationTime !=
            OSIX_FALSE)
        {
            FpamGetCurrentTime (&(pFpamFsUsrMgmtEntry->MibObject.
                                  TmFsusrMgmtUserPasswordCreationTime));
        }
        else
        {
            MEMCPY (&(pFpamFsUsrMgmtEntry->MibObject.
                      TmFsusrMgmtUserPasswordCreationTime),
                    &(pFpamSetFsUsrMgmtEntry->MibObject.
                      TmFsusrMgmtUserPasswordCreationTime),
                    sizeof (tFpamUtlTm));

        }
        if (bConfirmPwd == FALSE)
        {
            MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserConfirmPwd,
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    au1FsusrMgmtUserPassword,
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserPasswordLen);

            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen =
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen;
        }
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege != OSIX_FALSE)
    {
        pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege =
            pFpamSetFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus != OSIX_FALSE)
    {
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus =
            pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime != OSIX_FALSE)
    {
        pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime =
            pFpamSetFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus != OSIX_FALSE)
    {
        if (i4RowMakeActive != TRUE)
        {
            if ((ACTIVE ==
                 pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus)
                &&
                ((NOT_IN_SERVICE ==
                  pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus)
                 || (NOT_READY ==
                     pFpamSetFsUsrMgmtEntry->MibObject.
                     i4FsusrMgmtUserRowStatus)))
            {
                if (FPAM_USER_ENABLE ==
                    pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus)
                {
                    gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsEnableUsers--;
                }
                else
                {
                    gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsDisableUsers--;
                }
                /* Decrementing number of users - In case of NOT_IN_SERVICE or NOT_READY */
                gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsNumOfUsers--;
            }
            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus =
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus;
        }
    }
    if (bConfirmPwd == TRUE)
    {
        if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserConfirmPwd != OSIX_FALSE)
        {
            MEMSET (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserConfirmPwd,
                    0, FPAM_MAX_PASSWD_LEN);
            MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserConfirmPwd,
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    au1FsusrMgmtUserConfirmPwd,
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserConfirmPwdLen);

            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen =
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen;

        }
    }
    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus = ACTIVE;
    }

    if (FpamUtilUpdateFsusrMgmtTable (&FpamOldFsUsrMgmtEntry,
                                      pFpamFsUsrMgmtEntry,
                                      pFpamIsSetFsUsrMgmtEntry) != OSIX_SUCCESS)
    {

        if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                             pFpamIsSetFsUsrMgmtEntry,
                                             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));

        }
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetAllFsusrMgmtTable: FpamUtilUpdateFsusrMgmtTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pFpamFsUsrMgmtEntry, &FpamOldFsUsrMgmtEntry,
                sizeof (tFpamFsUsrMgmtEntry));
        return OSIX_FAILURE;

    }
    if (FpamSetAllFsusrMgmtTableTrigger (pFpamSetFsUsrMgmtEntry,
                                         pFpamIsSetFsUsrMgmtEntry,
                                         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetAllFsusrMgmtTable: FpamSetAllFsusrMgmtTableTrigger function returns failure.\r\n"));
    }
    if (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus == ACTIVE)
    {
        /* Incrementing number of users */
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsNumOfUsers++;
        if (FPAM_USER_ENABLE ==
            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus)
        {
            gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsEnableUsers++;
        }
        else
        {
            gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsDisableUsers++;
        }
    }
    /* User Count Updation */
    return OSIX_SUCCESS;

}
