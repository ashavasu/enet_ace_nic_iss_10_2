/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fpamtask.c,v 1.1 2012/01/18 13:28:28 siva Exp $
 *
 * Description:This file contains procedures related to
 *             FPAM - Task Initialization
 *******************************************************************/

#include "fpaminc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : FpamInit()                                    */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Fpam Task              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS, if FPAM Task is successfully spawned         */
/*                OSIX_FAILURE, otherwise                                    */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
FpamInit (INT1 *pi1Arg)
{

    UNUSED_PARAM (pi1Arg);
    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC:FpamInit\n"));

    /* task initializations */
    if (FpamMainTaskInit () == OSIX_FAILURE)
    {
        FPAM_TRC ((FPAM_TASK_TRC, " !!!!! FPAM TASK INIT FAILURE  !!!!! \n"));
        FpamMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    FpamTaskRegisterFpamMibs ();
    lrInitComplete (OSIX_SUCCESS);
    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "EXIT:FpamInit\n"));
    return;
}

PUBLIC INT4 FpamTaskRegisterFpamMibs ()
{
   RegisterFSUSER ();
   return OSIX_SUCCESS;
}

/*------------------------------------------------------------------------*/
/*                        End of the file  fpamtask.c                     */
/*------------------------------------------------------------------------*/
