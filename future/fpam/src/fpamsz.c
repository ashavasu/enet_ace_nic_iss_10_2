/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fpamsz.c,v 1.3 2013/04/16 13:31:01 siva Exp $
*
* Description: This file contains the fpam sizing related 
*              informations.
*
*******************************************************************/
#define _FPAMSZ_C
#include "fpaminc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams,
                                                     UINT4 u4ArraySize);
INT4
FpamSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < FPAM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsFPAMSizingParams[i4SizingId].u4StructSize,
                              FsFPAMSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(FPAMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            FpamSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
FpamSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    UINT4               u4StCount = 0;
    u4StCount = sizeof (FsFPAMSizingParams) / sizeof (tFsModSizingParams);
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsFPAMSizingParams, u4StCount);
    return OSIX_SUCCESS;
}

VOID
FpamSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < FPAM_MAX_SIZING_ID; i4SizingId++)
    {
        if (FPAMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (FPAMMemPoolIds[i4SizingId]);
            FPAMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
