/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamlwg.c,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains the low level routines
*               for the module Fpam 
*********************************************************************/

#include "fpaminc.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexFsusrMgmtTable
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsusrMgmtTable (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                                tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString)
{

    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;

    pFpamFsUsrMgmtEntry = FpamGetFirstFsusrMgmtTable ();

    if (pFpamFsUsrMgmtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    MEMCPY (pFsusrMgmtUserName->pu1_OctetList,
            pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName,
            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen);
    pFsusrMgmtUserName->i4_Length =
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen;

    MEMCPY (pFsusrMgmtAuthString->pu1_OctetList,
            pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString,
            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen);
    pFsusrMgmtAuthString->i4_Length =
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsusrMgmtTable
 Input       :  The Indices
                FsusrMgmtUserName
                nextFsusrMgmtUserName
                FsusrMgmtAuthString
                nextFsusrMgmtAuthString
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsusrMgmtTable (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                               tSNMP_OCTET_STRING_TYPE * pNextFsusrMgmtUserName,
                               tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                               tSNMP_OCTET_STRING_TYPE *
                               pNextFsusrMgmtAuthString)
{

    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamFsUsrMgmtEntry *pNextFpamFsUsrMgmtEntry = NULL;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    if ((pFsusrMgmtUserName->i4_Length > FPAM_MAX_USERNAME_LEN) ||
        (pFsusrMgmtAuthString->i4_Length > FPAM_AUTH_STRING_MAX_LEN) ||
        (pFsusrMgmtUserName->i4_Length < FPAM_MIN_USERNAME_LEN) ||
        (pFsusrMgmtAuthString->i4_Length < FPAM_AUTH_STRING_MIN_LEN))
    {
        return SNMP_FAILURE;
    }
    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            pFsusrMgmtAuthString->pu1_OctetList,
            pFsusrMgmtAuthString->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        pFsusrMgmtAuthString->i4_Length;

    pNextFpamFsUsrMgmtEntry = FpamGetNextFsusrMgmtTable (&FpamFsUsrMgmtEntry);

    if (pNextFpamFsUsrMgmtEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    MEMCPY (pNextFsusrMgmtUserName->pu1_OctetList,
            pNextFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName,
            pNextFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen);
    pNextFsusrMgmtUserName->i4_Length =
        pNextFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen;

    MEMCPY (pNextFsusrMgmtAuthString->pu1_OctetList,
            pNextFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString,
            pNextFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen);
    pNextFsusrMgmtAuthString->i4_Length =
        pNextFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtStatsNumOfUsers
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtStatsNumOfUsers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtStatsNumOfUsers (UINT4 *pu4RetValFsusrMgmtStatsNumOfUsers)
{
    if (FpamGetFsusrMgmtStatsNumOfUsers (pu4RetValFsusrMgmtStatsNumOfUsers) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtStatsActiveUsers
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtStatsActiveUsers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtStatsActiveUsers (UINT4 *pu4RetValFsusrMgmtStatsActiveUsers)
{
    if (FpamGetFsusrMgmtStatsActiveUsers (pu4RetValFsusrMgmtStatsActiveUsers) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtMinPasswordLen
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtMinPasswordLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtMinPasswordLen (UINT4 *pu4RetValFsusrMgmtMinPasswordLen)
{
    if (FpamGetFsusrMgmtMinPasswordLen (pu4RetValFsusrMgmtMinPasswordLen) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtPasswdMaxLifeTime
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtPasswdMaxLifeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtPasswdMaxLifeTime (UINT4 *pu4RetValFsusrMgmtPasswdMaxLifeTime)
{
    if (FpamGetFsusrMgmtPasswdMaxLifeTime (pu4RetValFsusrMgmtPasswdMaxLifeTime)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtUserPassword
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsusrMgmtUserPassword
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtUserPassword (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                             tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsusrMgmtUserPassword)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsusrMgmtUserPassword->pu1_OctetList,
            FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserPassword,
            FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserPasswordLen);
    pRetValFsusrMgmtUserPassword->i4_Length =
        FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserPasswordLen;

    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtUserPrivilege
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                UINT4 *pu4RetValFsusrMgmtUserPrivilege
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtUserPrivilege (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                              tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                              UINT4 *pu4RetValFsusrMgmtUserPrivilege)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsusrMgmtUserPrivilege =
        FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserPrivilege;

    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtUserLoginCount
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                INT4 *pi4RetValFsusrMgmtUserLoginCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtUserLoginCount (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                               tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                               INT4 *pi4RetValFsusrMgmtUserLoginCount)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsusrMgmtUserLoginCount =
        FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserLoginCount;

    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtUserStatus
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                INT4 *pi4RetValFsusrMgmtUserStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtUserStatus (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                           tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                           INT4 *pi4RetValFsusrMgmtUserStatus)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsusrMgmtUserStatus =
        FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserStatus;

    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtUserLockRelTime
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                UINT4 *pu4RetValFsusrMgmtUserLockRelTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtUserLockRelTime (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                                tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                                UINT4 *pu4RetValFsusrMgmtUserLockRelTime)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsusrMgmtUserLockRelTime =
        FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserLockRelTime;

    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtUserRowStatus
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                INT4 *pi4RetValFsusrMgmtUserRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtUserRowStatus (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                              tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                              INT4 *pi4RetValFsusrMgmtUserRowStatus)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsusrMgmtUserRowStatus =
        FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus;

    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtPasswdValidationChars
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtPasswdValidationChars
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtPasswdValidationChars (UINT4
                                      *pu4RetValFsusrMgmtPasswdValidationChars)
{
    if (FpamGetFsusrMgmtPasswdValidationChars
        (pu4RetValFsusrMgmtPasswdValidationChars) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtPasswdValidateNoOfLowerCase
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtPasswdValidateNoOfLowerCase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtPasswdValidateNoOfLowerCase (UINT4
                                            *pu4RetValFsusrMgmtPasswdValidateNoOfLowerCase)
{
    if (FpamGetFsusrMgmtPasswdValidateNoOfLowerCase
        (pu4RetValFsusrMgmtPasswdValidateNoOfLowerCase) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtPasswdValidateNoOfUpperCase
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtPasswdValidateNoOfUpperCase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtPasswdValidateNoOfUpperCase (UINT4
                                            *pu4RetValFsusrMgmtPasswdValidateNoOfUpperCase)
{
    if (FpamGetFsusrMgmtPasswdValidateNoOfUpperCase
        (pu4RetValFsusrMgmtPasswdValidateNoOfUpperCase) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtPasswdValidateNoOfNumericals
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtPasswdValidateNoOfNumericals
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtPasswdValidateNoOfNumericals (UINT4
                                             *pu4RetValFsusrMgmtPasswdValidateNoOfNumericals)
{
    if (FpamGetFsusrMgmtPasswdValidateNoOfNumericals
        (pu4RetValFsusrMgmtPasswdValidateNoOfNumericals) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtPasswdValidateNoOfSplChars
 Input       :  The Indices

                The Object 
                UINT4 *pu4RetValFsusrMgmtPasswdValidateNoOfSplChars
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtPasswdValidateNoOfSplChars (UINT4
                                           *pu4RetValFsusrMgmtPasswdValidateNoOfSplChars)
{
    if (FpamGetFsusrMgmtPasswdValidateNoOfSplChars
        (pu4RetValFsusrMgmtPasswdValidateNoOfSplChars) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtStatsEnableUsers
 Input       :  The Indices

                The Object 
                retValFsusrMgmtStatsEnableUsers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtStatsEnableUsers (UINT4 *pu4RetValFsusrMgmtStatsEnableUsers)
{
    if (FpamGetFsusrMgmtStatsEnableUsers (pu4RetValFsusrMgmtStatsEnableUsers) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtStatsDisableUsers
 Input       :  The Indices

                The Object 
                retValFsusrMgmtStatsDisableUsers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtStatsDisableUsers (UINT4 *pu4RetValFsusrMgmtStatsDisableUsers)
{
    if (FpamGetFsusrMgmtStatsDisableUsers (pu4RetValFsusrMgmtStatsDisableUsers)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsusrMgmtUserConfirmPwd
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                retValFsusrMgmtUserConfirmPwd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsusrMgmtUserConfirmPwd (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                               tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValFsusrMgmtUserConfirmPwd)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsusrMgmtUserConfirmPwd->pu1_OctetList,
            FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserConfirmPwd,
            FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserConfirmPwdLen);
    pRetValFsusrMgmtUserConfirmPwd->i4_Length =
        FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserConfirmPwdLen;

    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtMinPasswordLen
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsusrMgmtMinPasswordLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtMinPasswordLen (UINT4 u4SetValFsusrMgmtMinPasswordLen)
{
    if (FpamSetFsusrMgmtMinPasswordLen (u4SetValFsusrMgmtMinPasswordLen) !=
        OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtPasswdMaxLifeTime
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsusrMgmtPasswdMaxLifeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtPasswdMaxLifeTime (UINT4 u4SetValFsusrMgmtPasswdMaxLifeTime)
{
    if (FpamSetFsusrMgmtPasswdMaxLifeTime (u4SetValFsusrMgmtPasswdMaxLifeTime)
        != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtUserPassword
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsusrMgmtUserPassword
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtUserPassword (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                             tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsusrMgmtUserPassword)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;
    UINT1               au1FsusrMgmtPasswd[FPAM_MAX_PASSWD_LEN + 1];

    MEMSET (au1FsusrMgmtPasswd, 0, FPAM_MAX_PASSWD_LEN);
    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    STRNCPY (au1FsusrMgmtPasswd, pSetValFsusrMgmtUserPassword->pu1_OctetList,
             pSetValFsusrMgmtUserPassword->i4_Length);
    au1FsusrMgmtPasswd[pSetValFsusrMgmtUserPassword->i4_Length] = '\0';
    /* Assign the value */
    /* Encryption of password done here */
    /* Encryption is prevented in Standby since it is already encrypted in Active mode */
#ifdef RM_WANTED
    if(RmGetNodeState () != RM_STANDBY)
#endif
    {
	FpamUtlEncriptPasswd ((CHR1 *) au1FsusrMgmtPasswd);

    } 
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserPassword,
            au1FsusrMgmtPasswd, STRLEN (au1FsusrMgmtPasswd));
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserPasswordLen =
        pSetValFsusrMgmtUserPassword->i4_Length;

    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserPassword = OSIX_TRUE;
    FpamIsSetFsUsrMgmtEntry.bTmFsusrMgmtUserPasswordCreationTime = OSIX_TRUE;

    if (FpamSetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry, &FpamIsSetFsUsrMgmtEntry,
                                  OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

#ifdef CLI_WANTED
    if ((gu4FpamFeatureAddn & FPAM_USER_FORCED_LOGOUT) ==
        FPAM_USER_FORCED_LOGOUT)
    {
        CliLogOutUser ((INT1 *) FpamFsUsrMgmtEntry.MibObject.
                       au1FsusrMgmtUserName);
    }
#endif

    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtUserPrivilege
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
             :  UINT4 u4SetValFsusrMgmtUserPrivilege
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtUserPrivilege (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                              tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                              UINT4 u4SetValFsusrMgmtUserPrivilege)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserPrivilege =
        u4SetValFsusrMgmtUserPrivilege;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserPrivilege = OSIX_TRUE;

    if (FpamSetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry, &FpamIsSetFsUsrMgmtEntry,
                                  OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtUserStatus
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
             :  INT4 i4SetValFsusrMgmtUserStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtUserStatus (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                           tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                           INT4 i4SetValFsusrMgmtUserStatus)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserStatus =
        i4SetValFsusrMgmtUserStatus;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserStatus = OSIX_TRUE;

    if (FpamSetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry, &FpamIsSetFsUsrMgmtEntry,
                                  OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtUserLockRelTime
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
             :  UINT4 u4SetValFsusrMgmtUserLockRelTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtUserLockRelTime (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                                tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                                UINT4 u4SetValFsusrMgmtUserLockRelTime)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserLockRelTime =
        u4SetValFsusrMgmtUserLockRelTime;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserLockRelTime = OSIX_TRUE;

    if (FpamSetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry, &FpamIsSetFsUsrMgmtEntry,
                                  OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtUserRowStatus
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
             :  INT4 i4SetValFsusrMgmtUserRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtUserRowStatus (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                              tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                              INT4 i4SetValFsusrMgmtUserRowStatus)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus =
        i4SetValFsusrMgmtUserRowStatus;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserRowStatus = OSIX_TRUE;

    if (FpamSetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry, &FpamIsSetFsUsrMgmtEntry,
                                  OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    FpamWriteUsers ();
    FPAM_UNLOCK;
    FpamReadUsers ();
    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtPasswdValidationChars
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsusrMgmtPasswdValidationChars
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtPasswdValidationChars (UINT4
                                      u4SetValFsusrMgmtPasswdValidationChars)
{
    if (FpamSetFsusrMgmtPasswdValidationChars
        (u4SetValFsusrMgmtPasswdValidationChars) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtPasswdValidateNoOfLowerCase
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsusrMgmtPasswdValidateNoOfLowerCase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtPasswdValidateNoOfLowerCase (UINT4
                                            u4SetValFsusrMgmtPasswdValidateNoOfLowerCase)
{
    if (FpamSetFsusrMgmtPasswdValidateNoOfLowerCase
        (u4SetValFsusrMgmtPasswdValidateNoOfLowerCase) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtPasswdValidateNoOfUpperCase
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsusrMgmtPasswdValidateNoOfUpperCase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtPasswdValidateNoOfUpperCase (UINT4
                                            u4SetValFsusrMgmtPasswdValidateNoOfUpperCase)
{
    if (FpamSetFsusrMgmtPasswdValidateNoOfUpperCase
        (u4SetValFsusrMgmtPasswdValidateNoOfUpperCase) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtPasswdValidateNoOfNumericals
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsusrMgmtPasswdValidateNoOfNumericals
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtPasswdValidateNoOfNumericals (UINT4
                                             u4SetValFsusrMgmtPasswdValidateNoOfNumericals)
{
    if (FpamSetFsusrMgmtPasswdValidateNoOfNumericals
        (u4SetValFsusrMgmtPasswdValidateNoOfNumericals) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtPasswdValidateNoOfSplChars
 Input       :  The Indices

                The Object 
             :  UINT4 u4SetValFsusrMgmtPasswdValidateNoOfSplChars
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtPasswdValidateNoOfSplChars (UINT4
                                           u4SetValFsusrMgmtPasswdValidateNoOfSplChars)
{
    if (FpamSetFsusrMgmtPasswdValidateNoOfSplChars
        (u4SetValFsusrMgmtPasswdValidateNoOfSplChars) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsusrMgmtUserConfirmPwd
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                setValFsusrMgmtUserConfirmPwd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsusrMgmtUserConfirmPwd (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                               tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsusrMgmtUserConfirmPwd)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;
    UINT1               au1FsusrMgmtUserConfirmPwd[FPAM_MAX_PASSWD_LEN + 1];

    MEMSET (au1FsusrMgmtUserConfirmPwd, 0, FPAM_MAX_PASSWD_LEN);
    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    STRNCPY (au1FsusrMgmtUserConfirmPwd,
             pSetValFsusrMgmtUserConfirmPwd->pu1_OctetList,
             pSetValFsusrMgmtUserConfirmPwd->i4_Length);
    au1FsusrMgmtUserConfirmPwd[pSetValFsusrMgmtUserConfirmPwd->i4_Length] =
        '\0';
    /* Assign the value */
    /* Encryption of password done here */
    FpamUtlEncriptPasswd ((CHR1 *) au1FsusrMgmtUserConfirmPwd);
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserConfirmPwd,
            au1FsusrMgmtUserConfirmPwd, STRLEN (au1FsusrMgmtUserConfirmPwd));
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserConfirmPwdLen =
        pSetValFsusrMgmtUserConfirmPwd->i4_Length;

    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserConfirmPwd = OSIX_TRUE;

    if (FpamSetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry, &FpamIsSetFsUsrMgmtEntry,
                                  OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pFsusrMgmtAuthString);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtMinPasswordLen
 Input       :  The Indices

                The Object 
                testValFsusrMgmtMinPasswordLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtMinPasswordLen (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFsusrMgmtMinPasswordLen)
{
    if (FpamTestFsusrMgmtMinPasswordLen
        (pu4ErrorCode, u4TestValFsusrMgmtMinPasswordLen) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtUserPassword
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                testValFsusrMgmtUserPassword
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtUserPassword (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                                tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsusrMgmtUserPassword)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            pFsusrMgmtAuthString->pu1_OctetList,
            pFsusrMgmtAuthString->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        pFsusrMgmtAuthString->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserPassword,
            pTestValFsusrMgmtUserPassword->pu1_OctetList,
            pTestValFsusrMgmtUserPassword->i4_Length);
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserPasswordLen =
        pTestValFsusrMgmtUserPassword->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserPassword = OSIX_TRUE;

    if ((FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserPasswordLen >
         FPAM_MAX_PASSWD_LEN)
        || (FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserPasswordLen <
            FPAM_MIN_PASSWD_LEN))
    {
        return SNMP_FAILURE;
    }

    if (FpamTestAllFsusrMgmtTable (pu4ErrorCode, &FpamFsUsrMgmtEntry,
                                   &FpamIsSetFsUsrMgmtEntry, OSIX_FALSE,
                                   OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtPasswdMaxLifeTime
 Input       :  The Indices

                The Object 
                testValFsusrMgmtPasswdMaxLifeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtPasswdMaxLifeTime (UINT4 *pu4ErrorCode,
                                     UINT4 u4TestValFsusrMgmtPasswdMaxLifeTime)
{
    if (FpamTestFsusrMgmtPasswdMaxLifeTime
        (pu4ErrorCode, u4TestValFsusrMgmtPasswdMaxLifeTime) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtUserPrivilege
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                testValFsusrMgmtUserPrivilege
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtUserPrivilege (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                                 tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                                 UINT4 u4TestValFsusrMgmtUserPrivilege)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            pFsusrMgmtAuthString->pu1_OctetList,
            pFsusrMgmtAuthString->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        pFsusrMgmtAuthString->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserPrivilege =
        u4TestValFsusrMgmtUserPrivilege;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserPrivilege = OSIX_TRUE;

    if (FpamTestAllFsusrMgmtTable (pu4ErrorCode, &FpamFsUsrMgmtEntry,
                                   &FpamIsSetFsUsrMgmtEntry, OSIX_FALSE,
                                   OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtUserStatus
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                testValFsusrMgmtUserStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtUserStatus (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                              tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                              INT4 i4TestValFsusrMgmtUserStatus)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            pFsusrMgmtAuthString->pu1_OctetList,
            pFsusrMgmtAuthString->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        pFsusrMgmtAuthString->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserStatus =
        i4TestValFsusrMgmtUserStatus;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserStatus = OSIX_TRUE;

    if (FpamTestAllFsusrMgmtTable (pu4ErrorCode, &FpamFsUsrMgmtEntry,
                                   &FpamIsSetFsUsrMgmtEntry, OSIX_FALSE,
                                   OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtUserLockRelTime
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                testValFsusrMgmtUserLockRelTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtUserLockRelTime (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsusrMgmtAuthString,
                                   UINT4 u4TestValFsusrMgmtUserLockRelTime)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            pFsusrMgmtAuthString->pu1_OctetList,
            pFsusrMgmtAuthString->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        pFsusrMgmtAuthString->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserLockRelTime =
        u4TestValFsusrMgmtUserLockRelTime;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserLockRelTime = OSIX_TRUE;

    if (FpamTestAllFsusrMgmtTable (pu4ErrorCode, &FpamFsUsrMgmtEntry,
                                   &FpamIsSetFsUsrMgmtEntry, OSIX_FALSE,
                                   OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtUserRowStatus
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                testValFsusrMgmtUserRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtUserRowStatus (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                                 tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString,
                                 INT4 i4TestValFsusrMgmtUserRowStatus)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            pFsusrMgmtAuthString->pu1_OctetList,
            pFsusrMgmtAuthString->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        pFsusrMgmtAuthString->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus =
        i4TestValFsusrMgmtUserRowStatus;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserRowStatus = OSIX_TRUE;

    if (FpamTestAllFsusrMgmtTable (pu4ErrorCode, &FpamFsUsrMgmtEntry,
                                   &FpamIsSetFsUsrMgmtEntry, OSIX_FALSE,
                                   OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtPasswdValidationChars
 Input       :  The Indices

                The Object 
                testValFsusrMgmtPasswdValidationChars
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtPasswdValidationChars (UINT4 *pu4ErrorCode,
                                         UINT4
                                         u4TestValFsusrMgmtPasswdValidationChars)
{
    if (FpamTestFsusrMgmtPasswdValidationChars
        (pu4ErrorCode, u4TestValFsusrMgmtPasswdValidationChars) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtPasswdValidateNoOfLowerCase
 Input       :  The Indices

                The Object 
                testValFsusrMgmtPasswdValidateNoOfLowerCase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtPasswdValidateNoOfLowerCase (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4TestValFsusrMgmtPasswdValidateNoOfLowerCase)
{
    if (FpamTestFsusrMgmtPasswdValidateNoOfLowerCase
        (pu4ErrorCode,
         u4TestValFsusrMgmtPasswdValidateNoOfLowerCase) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtPasswdValidateNoOfUpperCase
 Input       :  The Indices

                The Object 
                testValFsusrMgmtPasswdValidateNoOfUpperCase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtPasswdValidateNoOfUpperCase (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4TestValFsusrMgmtPasswdValidateNoOfUpperCase)
{
    if (FpamTestFsusrMgmtPasswdValidateNoOfUpperCase
        (pu4ErrorCode,
         u4TestValFsusrMgmtPasswdValidateNoOfUpperCase) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtPasswdValidateNoOfNumericals
 Input       :  The Indices

                The Object 
                testValFsusrMgmtPasswdValidateNoOfNumericals
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtPasswdValidateNoOfNumericals (UINT4 *pu4ErrorCode,
                                                UINT4
                                                u4TestValFsusrMgmtPasswdValidateNoOfNumericals)
{
    if (FpamTestFsusrMgmtPasswdValidateNoOfNumericals
        (pu4ErrorCode,
         u4TestValFsusrMgmtPasswdValidateNoOfNumericals) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtPasswdValidateNoOfSplChars
 Input       :  The Indices

                The Object 
                testValFsusrMgmtPasswdValidateNoOfSplChars
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtPasswdValidateNoOfSplChars (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4TestValFsusrMgmtPasswdValidateNoOfSplChars)
{
    if (FpamTestFsusrMgmtPasswdValidateNoOfSplChars
        (pu4ErrorCode,
         u4TestValFsusrMgmtPasswdValidateNoOfSplChars) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsusrMgmtUserConfirmPwd
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString

                The Object 
                testValFsusrMgmtUserConfirmPwd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsusrMgmtUserConfirmPwd (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsusrMgmtAuthString,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsusrMgmtUserConfirmPwd)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            pFsusrMgmtAuthString->pu1_OctetList,
            pFsusrMgmtAuthString->i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        pFsusrMgmtAuthString->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserConfirmPwd,
            pTestValFsusrMgmtUserConfirmPwd->pu1_OctetList,
            pTestValFsusrMgmtUserConfirmPwd->i4_Length);
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserConfirmPwdLen =
        pTestValFsusrMgmtUserConfirmPwd->i4_Length;
    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserConfirmPwd = OSIX_TRUE;

    if ((FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserConfirmPwdLen >
         FPAM_MAX_PASSWD_LEN)
        || (FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserConfirmPwdLen <
            FPAM_MIN_PASSWD_LEN))
    {
        return SNMP_FAILURE;
    }

    if (FpamTestAllFsusrMgmtTable (pu4ErrorCode, &FpamFsUsrMgmtEntry,
                                   &FpamIsSetFsUsrMgmtEntry, OSIX_FALSE,
                                   OSIX_FALSE) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FsusrMgmtMinPasswordLen
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsusrMgmtMinPasswordLen (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsusrMgmtTable
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsusrMgmtTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsusrMgmtPasswdValidationChars
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsusrMgmtPasswdValidationChars (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsusrMgmtPasswdValidateNoOfLowerCase
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsusrMgmtPasswdValidateNoOfLowerCase (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsusrMgmtPasswdValidateNoOfUpperCase
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsusrMgmtPasswdValidateNoOfUpperCase (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsusrMgmtPasswdMaxLifeTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsusrMgmtPasswdMaxLifeTime (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsusrMgmtPasswdValidateNoOfNumericals
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsusrMgmtPasswdValidateNoOfNumericals (UINT4 *pu4ErrorCode,
                                               tSnmpIndexList * pSnmpIndexList,
                                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsusrMgmtPasswdValidateNoOfSplChars
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* Low Level Dependency Routines for All Objects  */

INT1
nmhDepv2FsusrMgmtPasswdValidateNoOfSplChars (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
