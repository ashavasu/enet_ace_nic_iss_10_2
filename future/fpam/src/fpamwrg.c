/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: fpamwrg.c,v 1.5 2012/05/26 13:03:02 siva Exp $
*
* Description: This file contains the wrapperfunctions for Fpam 
*********************************************************************/

#include "lr.h"
#include "fssnmp.h"
#include "fpamtdfsg.h"
#include "fpamtdfs.h"
#include "fpamlwg.h"
#include "fpamwrg.h"
#include "fpamprotg.h"
#include "fpamtmr.h"
#include "fpamprot.h"
#include "fsuserdb.h"
VOID
RegisterFSUSER ()
{
    SNMPRegisterMibWithLock (&fsuserOID, &fsuserEntry, FpamMainTaskLock,
                             FpamMainTaskUnLock, SNMP_MSR_TGR_TRUE);

    SNMPAddSysorEntry (&fsuserOID, (const UINT1 *) "fsuser");
}

VOID
UnRegisterFSUSER ()
{
    SNMPUnRegisterMib (&fsuserOID, &fsuserEntry);
    SNMPDelSysorEntry (&fsuserOID, (const UINT1 *) "fsuser");
}

INT4
GetNextIndexFsusrMgmtTable (tSnmpIndex * pFirstMultiIndex,
                            tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsusrMgmtTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsusrMgmtTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsusrMgmtStatsNumOfUsersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtStatsNumOfUsers (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtStatsActiveUsersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtStatsActiveUsers (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtMinPasswordLenGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtMinPasswordLen (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtPasswdValidationCharsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtPasswdValidationChars
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtPasswdValidateNoOfLowerCaseGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtPasswdValidateNoOfLowerCase
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtPasswdValidateNoOfUpperCaseGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtPasswdValidateNoOfUpperCase
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtPasswdValidateNoOfNumericalsGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtPasswdValidateNoOfNumericals
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtPasswdValidateNoOfSplCharsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtPasswdValidateNoOfSplChars
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtPasswdMaxLifeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtPasswdMaxLifeTime (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtUserPasswordGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsusrMgmtTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsusrMgmtUserPassword (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsusrMgmtUserPrivilegeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsusrMgmtTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsusrMgmtUserPrivilege (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsusrMgmtUserLoginCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsusrMgmtTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsusrMgmtUserLoginCount
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsusrMgmtUserStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsusrMgmtTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsusrMgmtUserStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsusrMgmtUserLockRelTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsusrMgmtTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsusrMgmtUserLockRelTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsusrMgmtUserRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsusrMgmtTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsusrMgmtUserRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));
}

INT4
FsusrMgmtStatsEnableUsersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtStatsEnableUsers (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtStatsDisableUsersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsusrMgmtStatsDisableUsers (&(pMultiData->u4_ULongValue)));
}

INT4
FsusrMgmtUserConfirmPwdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsusrMgmtTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsusrMgmtUserConfirmPwd
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsusrMgmtMinPasswordLenTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsusrMgmtMinPasswordLen
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidationCharsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsusrMgmtPasswdValidationChars
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidateNoOfLowerCaseTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsusrMgmtPasswdValidateNoOfLowerCase
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidateNoOfUpperCaseTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsusrMgmtPasswdValidateNoOfUpperCase
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidateNoOfNumericalsTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsusrMgmtPasswdValidateNoOfNumericals
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidateNoOfSplCharsTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsusrMgmtPasswdValidateNoOfSplChars
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdMaxLifeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsusrMgmtPasswdMaxLifeTime
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtUserPasswordTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsusrMgmtUserPassword (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsusrMgmtUserPrivilegeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsusrMgmtUserPrivilege (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->u4_ULongValue));

}

INT4
FsusrMgmtUserStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsusrMgmtUserStatus (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsusrMgmtUserLockRelTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsusrMgmtUserLockRelTime (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiData->u4_ULongValue));

}

INT4
FsusrMgmtUserRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsusrMgmtUserRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));
}

INT4
FsusrMgmtUserConfirmPwdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsusrMgmtUserConfirmPwd (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              pOctetStrValue,
                                              pMultiData->pOctetStrValue));

}

INT4
FsusrMgmtMinPasswordLenSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsusrMgmtMinPasswordLen (pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidationCharsSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsusrMgmtPasswdValidationChars (pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidateNoOfLowerCaseSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsusrMgmtPasswdValidateNoOfLowerCase
            (pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidateNoOfUpperCaseSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsusrMgmtPasswdValidateNoOfUpperCase
            (pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidateNoOfNumericalsSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsusrMgmtPasswdValidateNoOfNumericals
            (pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdValidateNoOfSplCharsSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsusrMgmtPasswdValidateNoOfSplChars
            (pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtPasswdMaxLifeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsusrMgmtPasswdMaxLifeTime (pMultiData->u4_ULongValue));
}

INT4
FsusrMgmtUserPasswordSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsusrMgmtUserPassword (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsusrMgmtUserPrivilegeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsusrMgmtUserPrivilege (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsusrMgmtUserStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsusrMgmtUserStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].pOctetStrValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsusrMgmtUserLockRelTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsusrMgmtUserLockRelTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->u4_ULongValue));

}

INT4
FsusrMgmtUserRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsusrMgmtUserRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsusrMgmtUserConfirmPwdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsusrMgmtUserConfirmPwd
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
FsusrMgmtMinPasswordLenDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsusrMgmtMinPasswordLen
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsusrMgmtPasswdValidationCharsDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsusrMgmtPasswdValidationChars
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsusrMgmtPasswdValidateNoOfLowerCaseDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsusrMgmtPasswdValidateNoOfLowerCase
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsusrMgmtPasswdValidateNoOfUpperCaseDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsusrMgmtPasswdValidateNoOfUpperCase
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsusrMgmtPasswdValidateNoOfNumericalsDep (UINT4 *pu4Error,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsusrMgmtPasswdValidateNoOfNumericals
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsusrMgmtPasswdValidateNoOfSplCharsDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsusrMgmtPasswdValidateNoOfSplChars
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsusrMgmtPasswdMaxLifeTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsusrMgmtPasswdMaxLifeTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsusrMgmtTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsusrMgmtTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
