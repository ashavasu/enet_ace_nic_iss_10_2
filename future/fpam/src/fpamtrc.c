/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fpamtrc.c,v 1.2 2013/04/16 13:31:01 siva Exp $
 *
 * Description : This file contains procedures containing Fpam Trace
 *               related operations
 *****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "fpaminc.h"

/****************************************************************************
*                                                                           *
* Function     : FpamTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
FpamTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        u4SysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }
    OsixGetSysTime (&u4SysTime);
    printf ("FPAM: %d:%s:%d:   %s", u4SysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : FpamTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
FpamTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : FpamTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
FpamTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define FPAM_TRC_BUF_SIZE    2000
    static CHR1         au1Buf[FPAM_TRC_BUF_SIZE];

    if ((u4Flags & FPAM_TRC_FLAG) > 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&au1Buf[0], fmt, ap);
    va_end (ap);

    return (&au1Buf[0]);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fpamtrc.c                      */
/*-----------------------------------------------------------------------*/
