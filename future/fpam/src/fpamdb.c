/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamdb.c,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Fpam 
*********************************************************************/

#include "fpaminc.h"

/****************************************************************************
 Function    :  FpamTestAllFsusrMgmtTable
 Input       :  pu4ErrorCode
                pFpamSetFsUsrMgmtEntry
                pFpamIsSetFsUsrMgmtEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
FpamTestAllFsusrMgmtTable (UINT4 *pu4ErrorCode,
                           tFpamFsUsrMgmtEntry * pFpamSetFsUsrMgmtEntry,
                           tFpamIsSetFsUsrMgmtEntry * pFpamIsSetFsUsrMgmtEntry,
                           INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStr;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUsrName;
    INT4                i4RetValFsusrMgmtUserRowStatus = 0;
    INT4                i4UsrMgmtUserLoginCount = 0;
    INT4                i4RetValGetloginCount = SNMP_FAILURE;
    INT4                i4RetValCheckAuthString = FPAM_FAILURE;

    MEMSET (&FsusrMgmtUsrName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FsusrMgmtAuthStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    FsusrMgmtUsrName.pu1_OctetList =
        pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName;
    FsusrMgmtUsrName.i4_Length =
        pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen;
    FsusrMgmtAuthStr.pu1_OctetList =
        pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString;
    FsusrMgmtAuthStr.i4_Length =
        pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen;

    /* Authentication string validation */
    FPAM_UNLOCK;
    i4RetValCheckAuthString =
        FpamUtlChkForFsusrMgmtAuthString (&FsusrMgmtAuthStr, OSIX_TRUE);
    FPAM_LOCK;

    if ((pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege)
        || (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime)
        || (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus))
    {
        if (i4RetValCheckAuthString == FPAM_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    /* Active Row Status Check */
    if ((pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword)
        || (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege)
        || (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime)
        || (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus))
    {
        if (nmhGetFsusrMgmtUserRowStatus
            (&FsusrMgmtUsrName, &FsusrMgmtAuthStr,
             &i4RetValFsusrMgmtUserRowStatus) == SNMP_SUCCESS)
        {
            if (i4RetValFsusrMgmtUserRowStatus == ACTIVE)
            {
                /* To Avoid Active row modification */
                return OSIX_FAILURE;
            }
        }
    }

    i4RetValGetloginCount =
        nmhGetFsusrMgmtUserLoginCount (&FsusrMgmtUsrName,
                                       &FsusrMgmtAuthStr,
                                       &i4UsrMgmtUserLoginCount);
    /* Active User Check */
    if ((pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege)
        || (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime)
        || (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus))
    {
        if (i4RetValGetloginCount == SNMP_SUCCESS)
        {
            if (i4UsrMgmtUserLoginCount > 0)
            {
                return OSIX_FAILURE;
            }
        }
    }

    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword != OSIX_FALSE)
    {
        FPAM_UNLOCK;
        if (FPAM_SUCCESS !=
            FpamTestPasswd (pFpamSetFsUsrMgmtEntry,
                            pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserConfirmPwd))
        {
            FPAM_LOCK;
            return OSIX_FAILURE;
        }
        FPAM_LOCK;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege != OSIX_FALSE)
    {
        /* To avoid Root User Updation via snmp */
        if (STRCMP (FsusrMgmtUsrName.pu1_OctetList, FPAM_ROOT_USER) == 0)
        {
            return OSIX_FAILURE;
        }
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus != OSIX_FALSE)
    {
        if (FPAM_SUCCESS == FpamIsSetUserMgmtFeature (FPAM_USER_STATUS_FEATURE))
        {
            if (STRNCMP
                (FsusrMgmtUsrName.pu1_OctetList, FsusrMgmtAuthStr.pu1_OctetList,
                 FsusrMgmtUsrName.i4_Length) == 0)
            {
                return OSIX_FAILURE;
            }
        }
        else
        {
            return OSIX_FAILURE;
        }
        if (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus ==
            FPAM_USER_DISABLE)
        {
            FPAM_UNLOCK;
            if (FpamUtlSearchRootPrivilege (&FsusrMgmtUsrName) != FPAM_SUCCESS)
            {
                FPAM_LOCK;
                return OSIX_FAILURE;
            }
            FPAM_LOCK;
        }
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime != OSIX_FALSE)
    {
        return OSIX_FAILURE;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus != OSIX_FALSE)
    {
        FPAM_UNLOCK;        
        if (FPAM_SUCCESS != FpamTestRowStatus (pFpamSetFsUsrMgmtEntry))
        {
            FPAM_LOCK;
            return OSIX_FAILURE;
        }
        FPAM_LOCK;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserConfirmPwd != OSIX_FALSE)
    {
        if (FPAM_SUCCESS ==
            FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
        {
            FPAM_UNLOCK;
            if (FPAM_SUCCESS !=
                FpamTestPasswd (pFpamSetFsUsrMgmtEntry,
                                pFpamIsSetFsUsrMgmtEntry->
                                bFsusrMgmtUserConfirmPwd))
            {
                FPAM_LOCK;
                return OSIX_FAILURE;
            }
            FPAM_LOCK;
        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamTestFsusrMgmtMinPasswordLen
 Input       :  pu4ErrorCode
                u4FsusrMgmtMinPasswordLen
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamTestFsusrMgmtMinPasswordLen (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsusrMgmtMinPasswordLen)
{

    if ((u4FsusrMgmtMinPasswordLen < FPAM_MIN_SET_PASSWD_LEN)
        || (u4FsusrMgmtMinPasswordLen > FPAM_MAX_PASSWD_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamTestFsusrMgmtPasswdValidationChars
 Input       :  pu4ErrorCode
                u4FsusrMgmtPasswdValidationChars
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamTestFsusrMgmtPasswdValidationChars (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsusrMgmtPasswdValidationChars)
{
    if ((u4FsusrMgmtPasswdValidationChars > FPAM_MAX_SET_PASSWD_VALIDATION_CHAR)
        || (u4FsusrMgmtPasswdValidationChars <
            FPAM_MIN_SET_PASSWD_VALIDATION_CHAR))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (FpamValidatePasswordRules (u4FsusrMgmtPasswdValidationChars,
                                   FPAM_MIN_LOWERCASE_COUNT,
                                   FPAM_MIN_UPPERCASE_COUNT,
                                   FPAM_MIN_NUMERICALS_COUNT,
                                   FPAM_MIN_SPL_CHARS_COUNT) == FPAM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamTestFsusrMgmtPasswdValidateNoOfLowerCase
 Input       :  pu4ErrorCode
                u4FsusrMgmtPasswdValidateNoOfLowerCase
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamTestFsusrMgmtPasswdValidateNoOfLowerCase (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4FsusrMgmtPasswdValidateNoOfLowerCase)
{
    if (u4FsusrMgmtPasswdValidateNoOfLowerCase >
        FPAM_MAX_SET_PASSWD_MAX_LOWER_CASE_CHAR)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (FpamValidatePasswordRules (FPAM_PASSWORD_VALIDATE_MASK,
                                   u4FsusrMgmtPasswdValidateNoOfLowerCase,
                                   FPAM_MIN_UPPERCASE_COUNT,
                                   FPAM_MIN_NUMERICALS_COUNT,
                                   FPAM_MIN_SPL_CHARS_COUNT) == FPAM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamTestFsusrMgmtPasswdValidateNoOfUpperCase
 Input       :  pu4ErrorCode
                u4FsusrMgmtPasswdValidateNoOfUpperCase
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamTestFsusrMgmtPasswdValidateNoOfUpperCase (UINT4 *pu4ErrorCode,
                                              UINT4
                                              u4FsusrMgmtPasswdValidateNoOfUpperCase)
{
    if (u4FsusrMgmtPasswdValidateNoOfUpperCase >
        FPAM_MAX_SET_PASSWD_MAX_UPPER_CASE_CHAR)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (FpamValidatePasswordRules (FPAM_PASSWORD_VALIDATE_MASK,
                                   FPAM_MIN_LOWERCASE_COUNT,
                                   u4FsusrMgmtPasswdValidateNoOfUpperCase,
                                   FPAM_MIN_NUMERICALS_COUNT,
                                   FPAM_MIN_SPL_CHARS_COUNT) == FPAM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamTestFsusrMgmtPasswdValidateNoOfNumericals
 Input       :  pu4ErrorCode
                u4FsusrMgmtPasswdValidateNoOfNumericals
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamTestFsusrMgmtPasswdValidateNoOfNumericals (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4FsusrMgmtPasswdValidateNoOfNumericals)
{
    if (u4FsusrMgmtPasswdValidateNoOfNumericals >
        FPAM_MAX_SET_PASSWD_MAX_NUMERIC_CHAR)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (FpamValidatePasswordRules (FPAM_PASSWORD_VALIDATE_MASK,
                                   FPAM_MIN_LOWERCASE_COUNT,
                                   FPAM_MIN_UPPERCASE_COUNT,
                                   u4FsusrMgmtPasswdValidateNoOfNumericals,
                                   FPAM_MIN_SPL_CHARS_COUNT) == FPAM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamTestFsusrMgmtPasswdValidateNoOfSplChars
 Input       :  pu4ErrorCode
                u4FsusrMgmtPasswdValidateNoOfSplChars
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamTestFsusrMgmtPasswdValidateNoOfSplChars (UINT4 *pu4ErrorCode,
                                             UINT4
                                             u4FsusrMgmtPasswdValidateNoOfSplChars)
{
    if (u4FsusrMgmtPasswdValidateNoOfSplChars >
        FPAM_MAX_SET_PASSWD_MAX_SPECIAL_CHAR)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    if (FpamValidatePasswordRules (FPAM_PASSWORD_VALIDATE_MASK,
                                   FPAM_MIN_LOWERCASE_COUNT,
                                   FPAM_MIN_UPPERCASE_COUNT,
                                   FPAM_MIN_NUMERICALS_COUNT,
                                   u4FsusrMgmtPasswdValidateNoOfSplChars) ==
        FPAM_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamTestFsusrMgmtPasswdMaxLifeTime
 Input       :  pu4ErrorCode
                u4FsusrMgmtPasswdMaxLifeTime
 Description :  This Routine will Test 
                the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamTestFsusrMgmtPasswdMaxLifeTime (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsusrMgmtPasswdMaxLifeTime)
{
    if (u4FsusrMgmtPasswdMaxLifeTime > FPAM_MAX_SET_PASSWORD_LIFE_TIME)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
