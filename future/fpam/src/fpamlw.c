/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamlw.c,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains some of low level functions used by protocol in Fpam module
*********************************************************************/

#include "fpaminc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsusrMgmtTable
 Input       :  The Indices
                FsusrMgmtUserName
                FsusrMgmtAuthString
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsusrMgmtTable (tSNMP_OCTET_STRING_TYPE *
                                        pFsusrMgmtUserName,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsusrMgmtAuthString)
{
    INT1                i1Index = 0;

    /* Authentication String check */
    if ((pFsusrMgmtAuthString->pu1_OctetList == NULL)
        || (pFsusrMgmtAuthString->i4_Length < FPAM_AUTH_STRING_MIN_LEN)
        || (pFsusrMgmtAuthString->i4_Length > FPAM_AUTH_STRING_MAX_LEN))
    {
        /* FPAM: Authentication String failure */
        mmi_printf("\r\n%% Authentication String failure!!\r\n");
        return SNMP_FAILURE;
    }

    /* length check */
    if ((pFsusrMgmtUserName->i4_Length > FPAM_MAX_USERNAME_LEN)
        || (pFsusrMgmtUserName->i4_Length < FPAM_MIN_USERNAME_LEN))
    {
        /* FPAM: User Name exceeded size limit */
        mmi_printf ("\r\n%% User Name exceeded size limit!!\r\n"
                     "Also ensure username does not have special characters\r\n");
        return SNMP_FAILURE;
    }

    for (i1Index = 0; i1Index < pFsusrMgmtUserName->i4_Length; i1Index++)
    {
        if ((pFsusrMgmtUserName->pu1_OctetList[i1Index] ==
             FPAM_SPL_CHAR_EXCLAIM)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] == FPAM_SPL_CHAR_AT)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] ==
                FPAM_SPL_CHAR_HASH)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] ==
                FPAM_SPL_CHAR_DOLLAR)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] == FPAM_SPL_CHAR_MOD)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] == FPAM_SPL_CHAR_XOR)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] == FPAM_SPL_CHAR_AND)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] ==
                FPAM_SPL_CHAR_ASTERIK)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] ==
                FPAM_SPL_CHAR_OPEN_BRACES)
            || (pFsusrMgmtUserName->pu1_OctetList[i1Index] ==
                FPAM_SPL_CHAR_CLOSE_BRACES))
        {
            /* FPAM: User Name has Special characters */
            mmi_printf ("\r\n%% User Name has Special characters\r\n"
                        "%% Also ensure User Name length does not exceed %d\r\n",
                         FPAM_MAX_USERNAME_LEN);
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}
