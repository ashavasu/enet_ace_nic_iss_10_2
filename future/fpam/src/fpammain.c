/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fpammain.c,v 1.10 2017/09/12 13:27:05 siva Exp $
*
* Description: This file contains the fpam task main loop
*              and the initialization routines.
*
*******************************************************************/
#define __FPAMMAIN_C__
#include "fpaminc.h"
#include "fips.h"
/* Proto types of the functions private to this file only */

PRIVATE UINT4 FpamMainMemInit PROTO ((VOID));
PRIVATE VOID FpamMainMemClear PROTO ((VOID));

/* sets the default fpam user */
INT1               *gpi1FpamDefaultRootUser = (INT1 *) FPAM_DEF_ROOT_USER;
INT1               *gpi1FpamDefaultRootUserPasswd =
    (INT1 *) FPAM_DEF_ROOT_PASSWD;

/****************************************************************************
*                                                                           *
* Function     : FpamMainTaskInit                                           *
*                                                                           *
* Description  : FPAM initialization routine.                               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
FpamMainTaskInit (VOID)
{

    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC:FpamMainTaskInit\n"));
#ifdef FPAM_TRACE_WANTED
    FPAM_TRC_FLAG = ~((UINT4) 0);
#endif

    MEMSET (&gFpamGlobals, 0, sizeof (gFpamGlobals));
    MEMCPY (gFpamGlobals.au1TaskSemName, FPAM_MUT_EXCL_SEM_NAME, OSIX_NAME_LEN);

    if (OsixCreateSem (FPAM_MUT_EXCL_SEM_NAME, FPAM_SEM_CREATE_INIT_CNT, 0,
                       &gFpamGlobals.fpamTaskSemId) == OSIX_FAILURE)
    {
        FPAM_TRC ((FPAM_MAIN_TRC,
                   "Seamphore Creation failure for %s \n",
                   FPAM_MUT_EXCL_SEM_NAME));
        return OSIX_FAILURE;
    }
    if (FpamUtlCreateRBTree () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Create buffer pools for data structures */

    if (FpamMainMemInit () == OSIX_FAILURE)
    {
        FPAM_TRC ((FPAM_MAIN_TRC, "Memory Pool Creation Failed\n"));
        return OSIX_FAILURE;
    }

    if (OsixCreateQ (FPAM_QUEUE_NAME, FPAM_QUEUE_DEPTH, (UINT4) 0,
                     (tOsixQId *) & (gFpamGlobals.fpamQueId)) == OSIX_FAILURE)
    {
        FPAM_TRC ((FPAM_MAIN_TRC, "IAPQ Creation Failed\n"));
        return OSIX_FAILURE;
    }

    FpamTmrInitTmrDesc ();
    gb1FpamDefUserInit = OSIX_FALSE;
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtMinPasswordLen = FPAM_MIN_SET_PASSWD_LEN;
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidationChars =
        FPAM_DEF_PASSWORD_VALIDATION_CHARS;
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfLowerCase =
        FPAM_DEF_PASSWORD_LOWER_CASE_CHARS;
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfUpperCase =
        FPAM_DEF_PASSWORD_UPPER_CASE_CHARS;
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfNumericals =
        FPAM_DEF_PASSWORD_NUMERIC_CHARS;
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfSplChars =
        FPAM_DEF_PASSWORD_SPECIAL_CHARS;
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdMaxLifeTime =
        FPAM_DEF_PASSWORD_MAX_LIFE_TIME;

    /* Initialising the global variable which contains information for 
     * enabling customer specific features in FPAM*/
    /* In order to enable features set the particular bit of gu4FpamFeatureAddn 
     * through function call FpamSetUserMgmtFeature in isscust.c. */
    gu4FpamFeatureAddn = 0x00000;

    /* To populate the DataStructure */
    if (FpamReadUsers () == FPAM_FAILURE)
    {
        gb1FpamDefUserInit = OSIX_TRUE;
        FpamWriteUsersDefault ();
        gb1FpamDefUserInit = OSIX_FALSE;
        FpamReadUsers ();
    }
#ifdef MBSM_WANTED
    if (FpamUtlMbsmUserUpdate () == FPAM_FAILURE)
    {
        FPAM_TRC ((FPAM_MAIN_TRC, "FPAM : Chassisuser Creation Failed\n"));
    }
#endif
    /* The following routine initialises the timer descriptor structure */
/*
    FpamUtilInitGlobals();
*/
/*    if (FpamUtlCreatMemPool() == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }*/
    /*if (FpamSizingMemCreateMemPools () == OSIX_FAILURE)
       {
       return OSIX_FAILURE;
       } */

    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "FUNC:FpamMainTaskInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FpamMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
FpamMainDeInit (VOID)
{

    FpamMainMemClear ();

    if (gFpamGlobals.fpamTaskSemId)
    {
        OsixSemDel (gFpamGlobals.fpamTaskSemId);
    }
    if (gFpamGlobals.fpamQueId)
    {
        OsixQueDel (gFpamGlobals.fpamQueId);
    }

    if (gFpamGlobals.fpamTmrLst != NULL)
    {
        TmrDeleteTimerList (gFpamGlobals.fpamTmrLst);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FpamMainTaskLock                                           */
/*                                                                           */
/* Description  : Lock the Fpam Main Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
FpamMainTaskLock (VOID)
{
    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC:FpamMainTaskLock\n"));

    if (OsixSemTake (gFpamGlobals.fpamTaskSemId) == OSIX_FAILURE)
    {
        FPAM_TRC ((FPAM_MAIN_TRC,
                   "TakeSem failure for %s \n", FPAM_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }

    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "EXIT:FpamMainTaskLock\n"));
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FpamMainTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the FPAM Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
FpamMainTaskUnLock (VOID)
{
    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC:FpamMainTaskUnLock\n"));

    OsixSemGive (gFpamGlobals.fpamTaskSemId);

    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "EXIT:FpamMainTaskUnLock\n"));
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FpamMainMemInit                                            */
/*                                                                           */
/* Description  : Allocates all the memory that is required for FPAM         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
FpamMainMemInit (VOID)
{
    if (FpamSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : FpamMainMemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
FpamMainMemClear (VOID)
{
    FpamSizingMemDeleteMemPools ();
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  fpammain.c                     */
/*-----------------------------------------------------------------------*/
