/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: fpamutl.c,v 1.45 2017/09/12 13:27:05 siva Exp $
 *
 * Description: This file contains utility functions used by protocol Fpam
 *********************************************************************/

#include "fpaminc.h"
#include "isscli.h"

#include "fips.h"
#define MAX_PASSWORD_TRIES 3
#define FPAM_TWO 2
/****************************************************************************
 * Function    :  FpamGetAllUtlFsusrMgmtTable
 * Input       :  pFpamGetFsUsrMgmtEntry
 * pFpamdsFsUsrMgmtEntry
 * Description :  This routine Gets the
 * value of needed.
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
FpamGetAllUtlFsusrMgmtTable (tFpamFsUsrMgmtEntry * pFpamGetFsUsrMgmtEntry,
                             tFpamFsUsrMgmtEntry * pFpamdsFsUsrMgmtEntry)
{
    UNUSED_PARAM (pFpamGetFsUsrMgmtEntry);
    UNUSED_PARAM (pFpamdsFsUsrMgmtEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  FpamUtilUpdateFsusrMgmtTable
 * Input       :  pFpamOldFsUsrMgmtEntry
 *                pFpamFsUsrMgmtEntry
 *                pFpamIsSetFsUsrMgmtEntry
 * Description :  This Routine checks set value
 *                with that of the value in database
 *                and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
FpamUtilUpdateFsusrMgmtTable (tFpamFsUsrMgmtEntry * pFpamOldFsUsrMgmtEntry,
                              tFpamFsUsrMgmtEntry * pFpamFsUsrMgmtEntry,
                              tFpamIsSetFsUsrMgmtEntry *
                              pFpamIsSetFsUsrMgmtEntry)
{

    UNUSED_PARAM (pFpamOldFsUsrMgmtEntry);
    UNUSED_PARAM (pFpamFsUsrMgmtEntry);
    UNUSED_PARAM (pFpamIsSetFsUsrMgmtEntry);
    return OSIX_SUCCESS;
}

/**************************************************************************
 * Function    : FpamUtlChkForFsusrMgmtAuthString
 * Input       : pFsusrMgmtAuthStringValidate
 * Description : Used to validate the Authentication string
 *               on adding new users.
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT4
FpamUtlChkForFsusrMgmtAuthString (tSNMP_OCTET_STRING_TYPE *
                                  pFsusrMgmtAuthStringValidate,
                                  BOOL1 bPrivilCheck)
{
    UINT1              *pu1UserName = NULL;
    UINT1              *pu1Passwd = NULL;
    UINT1              *pu1AuthString = NULL;
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserNameSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtUserName;
    UINT1               au1FsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               au1NextFsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStringSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtAuthString;
    UINT1               au1FsusrMgmtAuthString[FPAM_AUTH_STRING_MAX_LEN];
    UINT1               au1NextFsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1AuthString[FPAM_AUTH_STRING_MAX_LEN + 1];
    tCliSpecialPriv     CliSpecialPriv;

    if ((pFsusrMgmtAuthStringValidate == NULL) ||
        (pFsusrMgmtAuthStringValidate->i4_Length < FPAM_AUTH_STRING_MIN_LEN) ||
        (pFsusrMgmtAuthStringValidate->i4_Length > FPAM_AUTH_STRING_MAX_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlChkForFsusrMgmtAuthString: Entry NULL.\r\n"));
        return FPAM_FAILURE;
    }
    MEMSET (au1FsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (au1NextFsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (&FsusrMgmtUserNameSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsusrMgmtAuthString, 0, FPAM_AUTH_STRING_MAX_LEN);
    MEMSET (au1AuthString, 0, FPAM_AUTH_STRING_MAX_LEN + 1);
    MEMSET (au1NextFsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));
    MEMSET (&FsusrMgmtAuthStringSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&CliSpecialPriv, 0, sizeof (tCliSpecialPriv));

    FsusrMgmtUserNameSrc.pu1_OctetList = au1FsusrMgmtUserName;
    FsusrMgmtUserNameSrc.i4_Length = 0;
    FsusrMgmtAuthStringSrc.pu1_OctetList = au1FsusrMgmtAuthString;
    FsusrMgmtAuthStringSrc.i4_Length = 0;
    NextFsusrMgmtUserName.pu1_OctetList = au1NextFsusrMgmtUserName;
    NextFsusrMgmtUserName.i4_Length = 0;
    NextFsusrMgmtAuthString.pu1_OctetList = au1NextFsusrMgmtAuthString;
    NextFsusrMgmtAuthString.i4_Length = 0;
    MEMCPY (au1AuthString,
            pFsusrMgmtAuthStringValidate->pu1_OctetList,
            pFsusrMgmtAuthStringValidate->i4_Length);
    au1AuthString[pFsusrMgmtAuthStringValidate->i4_Length] = '\0';

    pu1UserName = au1AuthString;
    pu1AuthString = au1AuthString;

#ifdef CLI_WANTED
    CliGetSpecialPriv (&CliSpecialPriv);
#endif
    while ((*pu1AuthString != ':') && (*pu1AuthString != '\0'))
    {
        pu1AuthString++;
    }

    *pu1AuthString = '\0';
    pu1AuthString++;
    pu1Passwd = pu1AuthString;

    FpamUtlEncriptPasswd ((CHR1 *) pu1Passwd);

    FPAM_LOCK;
    if (SNMP_SUCCESS != nmhGetFirstIndexFsusrMgmtTable
        (&FsusrMgmtUserNameSrc, &FsusrMgmtAuthStringSrc))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlChkForFsusrMgmtAuthString: "
                   "nmhGetFirstIndexFsusrMgmtTable function "
                   "returns failure.\r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &FsusrMgmtUserNameSrc,
                              &FsusrMgmtAuthStringSrc);

    while (1)
    {
        if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlChkForFsusrMgmtAuthString: "
                       "FpamGetAllFsusrMgmtTable function "
                       "returns failure.\r\n"));
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
        if ((STRLEN (pu1UserName) ==
             STRLEN (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName))
            && (STRLEN (pu1Passwd) ==
                STRLEN (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserPassword)))
        {
            if (MEMCMP
                (pu1UserName, FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
                 FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen) == 0)
            {
                if (MEMCMP
                    (pu1Passwd,
                     FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserPassword,
                     STRLEN (pu1Passwd)) == 0)
                {
                    if (bPrivilCheck == OSIX_TRUE)
                    {
#ifdef CLI_WANTED
                        if (CliSpecialPriv.b1UsernameBased == OSIX_TRUE)
                        {
                            if (STRCMP (pu1UserName, CliSpecialPriv.au1UserName)
                                == 0)
                            {
                                FPAM_TRC ((FPAM_UTIL_TRC,
                                           "FpamUtlChkForFsusrMgmtAuthString: "
                                           "Authentication is successful - "
                                           "UsernameBased.\r\n"));
                                FPAM_UNLOCK;
                                return FPAM_SUCCESS;
                            }
                        }
                        if (CliSpecialPriv.b1PrivilegeBased == OSIX_TRUE)
                        {
                            if ((FpamFsUsrMgmtEntry.MibObject.
                                 u4FsusrMgmtUserPrivilege &
                                 FPAM_PRIVILEGE_ID_MASK) >=
                                CliSpecialPriv.u1PrivilegeId)
                            {
                                FPAM_TRC ((FPAM_UTIL_TRC,
                                           "FpamUtlChkForFsusrMgmtAuthString: "
                                           "Authentication is successful "
                                           "- PrivilegeBased.\r\n"));
                                FPAM_UNLOCK;
                                return FPAM_SUCCESS;
                            }
                        }

#else
                        if (STRLEN (pu1UserName) == STRLEN (FPAM_ROOT_USER))
                        {
                            if (MEMCMP
                                (pu1UserName, FPAM_ROOT_USER,
                                 STRLEN (pu1UserName)) == 0)
                            {
                                FPAM_TRC ((FPAM_UTIL_TRC,
                                           "FpamUtlChkForFsusrMgmtAuthString: "
                                           "Authentication is "
                                           "successful.\r\n"));
                                FPAM_UNLOCK;
                                return FPAM_SUCCESS;
                            }
                        }
#endif
                    }
                    else
                    {
                        FPAM_UNLOCK;
                        return FPAM_SUCCESS;
                    }
                }
                else
                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamUtlChkForFsusrMgmtAuthString: "
                               "Authstring has wrong password.\r\n"));
                    FPAM_UNLOCK;
                    return FPAM_FAILURE;

                }
            }
        }
        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsusrMgmtTable (&FsusrMgmtUserNameSrc,
                                           &NextFsusrMgmtUserName,
                                           &FsusrMgmtAuthStringSrc,
                                           &NextFsusrMgmtAuthString))
        {
            break;
        }

        Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &NextFsusrMgmtUserName,
                                  &NextFsusrMgmtAuthString);

        FsusrMgmtUserNameSrc.pu1_OctetList =
            NextFsusrMgmtUserName.pu1_OctetList;
        FsusrMgmtUserNameSrc.i4_Length = NextFsusrMgmtUserName.i4_Length;
    }
    FPAM_UNLOCK;
    return FPAM_FAILURE;
}

/**************************************************************************
 * Function    : FpamUtlAddUser
 * Input       : pointer to the tFpamAddUser structure
 * Description : To Add a new user this utl is called by cli.
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT4
FpamUtlAddUser (tFpamAddUser * pFpamAddUser)
{
    tSNMP_OCTET_STRING_TYPE AddFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE AddFsusrMgmtUserPassword;
    tSNMP_OCTET_STRING_TYPE AddFsusrMgmtAuthString;
    tSNMP_OCTET_STRING_TYPE AddFsusrMgmtUserConfirmPwd;
    INT4                i4RetValFsusrMgmtUserStatus = 0;
#ifdef CLI_WANTED
    INT2                i2OldPrivilege = 0;
#endif
    BOOL1               bEditRoot = OSIX_FALSE;
    BOOL1               bConfirmPwd = OSIX_FALSE;
    BOOL1               bErrorTrace = OSIX_FALSE;
    BOOL1               bEditPrev = OSIX_FALSE;

    MEMSET (&AddFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&AddFsusrMgmtUserPassword, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&AddFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&AddFsusrMgmtUserConfirmPwd, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (FPAM_SUCCESS ==
        FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
    {
        bConfirmPwd = OSIX_TRUE;
    }
    if (pFpamAddUser == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlAddUser: NULL argument is passed \r\n"));
        return FPAM_FAILURE;
    }
#ifdef CLI_WANTED
    CliGetUserPrivilege ((CONST CHR1 *) (pFpamAddUser->pi1NewUsrName),
                         &i2OldPrivilege);
    if (pFpamAddUser->i4PrivilegeLevel == (INT4) i2OldPrivilege)
    {
        bEditPrev = OSIX_TRUE;
    }
    if (bConfirmPwd == OSIX_TRUE)
    {
        if (pFpamAddUser->pi1ConfirmPasswd == NULL)
        {
            if (bEditPrev == OSIX_TRUE)
            {
                bErrorTrace = OSIX_TRUE;
            }
        }
    }
#endif
    /* check to see the basic argument are provided to proceed */
    if ((((pFpamAddUser->pi1NewPasswd == NULL) && (bEditPrev == OSIX_TRUE))
         || ((pFpamAddUser->i4PrivilegeLevel & FPAM_PRIVILEGE_ID_MASK) >
             FPAM_ROOT_PRIVILEGE_ID)) != 0)
    {
        bErrorTrace = OSIX_TRUE;
    }
    if (bErrorTrace == OSIX_TRUE)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlAddUser: Basic Argument checks fails "
                   "at Add user \r\n"));
        return FPAM_FAILURE;
    }
    AddFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pFpamAddUser->pi1NewUsrName;
    AddFsusrMgmtUserName.i4_Length = STRLEN (pFpamAddUser->pi1NewUsrName);
    /* check when passwd is not altered */
    if (pFpamAddUser->pi1NewPasswd != NULL)
    {
        AddFsusrMgmtUserPassword.pu1_OctetList =
            (UINT1 *) pFpamAddUser->pi1NewPasswd;
        AddFsusrMgmtUserPassword.i4_Length =
            STRLEN (pFpamAddUser->pi1NewPasswd);
    }
    AddFsusrMgmtAuthString.pu1_OctetList = (UINT1 *) FPAM_AUTH_STRING;
    AddFsusrMgmtAuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);
    if (pFpamAddUser->pi1ConfirmPasswd != NULL)
    {
        if (bConfirmPwd == OSIX_TRUE)
        {
            AddFsusrMgmtUserConfirmPwd.pu1_OctetList =
                (UINT1 *) pFpamAddUser->pi1ConfirmPasswd;
            AddFsusrMgmtUserConfirmPwd.i4_Length =
                STRLEN (pFpamAddUser->pi1ConfirmPasswd);
        }
    }

    if (gb1FpamDefUserInit == OSIX_FALSE)
    {
        FPAM_LOCK;
        if (nmhValidateIndexInstanceFsusrMgmtTable
            (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString) == SNMP_FAILURE)
        {
            /* Validation failed */
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlAddUser: nmhValidateIndexInstanceFsusrMgmtTable "
                       "function returns failure.\r\n"));
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
        /* check when passwd is not altered */
        if (pFpamAddUser->pi1NewPasswd != NULL)
        {
            if (FpamUserPasswdConfCheck
                (&AddFsusrMgmtUserName,
                 &AddFsusrMgmtUserPassword) == FPAM_FAILURE)
            {
                /* Password Test for Password Configuration rules */
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamUtlAddUser: FpamUserPasswdConfCheck function "
                           "returns failure.\r\n"));
                FPAM_UNLOCK;
                return FPAM_FAILURE;
            }
        }
        if (STRCMP (pFpamAddUser->pi1NewUsrName, FPAM_ROOT_USER) == 0)
        {
            if (FPAM_SUCCESS ==
                FpamIsSetUserMgmtFeature (FPAM_CHECK_EDITPASSWORD_ROOT_USER))
            {
                if (FPAM_ROOT_PRIVILEGE_ID == pFpamAddUser->i4PrivilegeLevel)
                {
                    bEditRoot = OSIX_TRUE;
                }
            }
            if (OSIX_FALSE == bEditRoot)
            {
#ifdef CLI_WANTED
                mmi_printf ("\r\n%%Cannot Create or Edit root user !!\r\n");
#endif
                FPAM_UNLOCK;
                return FPAM_FAILURE;
            }
        }
        FPAM_UNLOCK;
    }
    if (FpamUtlSearchUserName (&AddFsusrMgmtUserName) == FPAM_SUCCESS)
    {
        /* User Already exist test */
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlAddUser: FpamUtlSearchUserName function "
                   "returns Success. User Already present.\r\n"));
        FPAM_LOCK;
        if (nmhSetFsusrMgmtUserRowStatus
            (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlAddUser: nmhSetFsusrMgmtUserRowStatus "
                       "function returns failure.\r\n"));
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
    }
    else
    {
        if (pFpamAddUser->i1UserStatus == FPAM_USER_DISABLE)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlAddUser: Can't create disabled user\r\n"));
            return FPAM_FAILURE;
        }
        FPAM_LOCK;
        if (nmhSetFsusrMgmtUserRowStatus
            (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlAddUser: nmhSetFsusrMgmtUserRowStatus "
                       "returns failure.\r\n"));
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
        /* If user does not specified the user status while adding new user */
        if (FPAM_USER_STATUS_NA == pFpamAddUser->i1UserStatus)
        {
            pFpamAddUser->i1UserStatus = FPAM_USER_ENABLE;
        }
    }

    if (nmhGetFsusrMgmtUserStatus
        (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString,
         &i4RetValFsusrMgmtUserStatus) == SNMP_SUCCESS)
    {
        if (pFpamAddUser->i1UserStatus != FPAM_USER_ENABLE)
        {
            if (FPAM_USER_DISABLE == i4RetValFsusrMgmtUserStatus)
            {
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "FpamUtlAddUser: User disabled "
                           "can't edit password.\r\n"));
                FPAM_UNLOCK;
                return FPAM_FAILURE;
            }
        }
    }

    /* check when passwd is not altered */
    if (pFpamAddUser->pi1NewPasswd != NULL)
    {
        if (nmhSetFsusrMgmtUserPassword
            (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString,
             &AddFsusrMgmtUserPassword) == SNMP_FAILURE)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlAddUser: nmhSetFsusrMgmtUserPassword "
                       "returns failure.\r\n"));
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
    }

    if (OSIX_TRUE == bConfirmPwd)
    {
        if (nmhSetFsusrMgmtUserConfirmPwd
            (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString,
             &AddFsusrMgmtUserConfirmPwd) == SNMP_FAILURE)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlAddUser: nmhSetFsusrMgmtUserConfirmPwd "
                       "returns failure.\r\n"));
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
    }

    if (FPAM_USER_STATUS_NA != pFpamAddUser->i1UserStatus)
    {
        if (nmhSetFsusrMgmtUserStatus
            (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString,
             (INT4) pFpamAddUser->i1UserStatus) == SNMP_FAILURE)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlAddUser: nmhSetFsusrMgmtUserStatus "
                       "returns failure.\r\n"));
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
    }
    if (nmhSetFsusrMgmtUserPrivilege
        (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString,
         pFpamAddUser->i4PrivilegeLevel) == SNMP_FAILURE)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlAddUser: nmhSetFsusrMgmtUserPrivilege "
                   "returns failure.\r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    if (nmhSetFsusrMgmtUserRowStatus
        (&AddFsusrMgmtUserName, &AddFsusrMgmtAuthString,
         ACTIVE) == SNMP_FAILURE)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlAddUser: nmhSetFsusrMgmtUserRowStatus "
                   "returns failure.\r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }
    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}

/********************************************************************
 * Function    : FpamUtlDelUser
 * Input       : pi1NewUsrName
 * Description : To Delete a existing user this utl is called by cli
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT4
FpamUtlDelUser (INT1 *pi1NewUsrName)
{
    tSNMP_OCTET_STRING_TYPE DelFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE DelFsusrMgmtAuthString;
    UINT1               au1AuthString[STRLEN (FPAM_AUTH_STRING) + 1];
    INT4                i4RetValFsusrMgmtUserRowStatus = 0;
    UINT4               u4Privilege = 0;
    INT4                i4FipsOperMode = 0;

    MEMSET (&DelFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DelFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (pi1NewUsrName == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC, "FpamUtlDelUser: username is null \r\n"));
        return FPAM_FAILURE;
    }
    DelFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pi1NewUsrName;
    DelFsusrMgmtUserName.i4_Length = STRLEN (pi1NewUsrName);
    DelFsusrMgmtAuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);
    STRCPY (au1AuthString, FPAM_AUTH_STRING);
    DelFsusrMgmtAuthString.pu1_OctetList = (UINT1 *) au1AuthString;

    i4FipsOperMode = FipsGetFipsCurrOperMode ();

    if (FpamUtlSearchUserName (&DelFsusrMgmtUserName) != FPAM_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlDelUser: FpamUtlSearchUserName function "
                   "returns Failure. User is not present. Deletion of user failed.\r\n"));
        return FPAM_FAILURE;
    }

    if ((STRCMP (pi1NewUsrName, FPAM_ROOT_USER) == 0) &&
        (LEGACY_MODE == i4FipsOperMode))
    {
#ifdef CLI_WANTED
        mmi_printf ("\r\n%%Cannot delete root user !!\r\n");
#endif
        return FPAM_FAILURE;
    }
    FPAM_LOCK;
    if (nmhValidateIndexInstanceFsusrMgmtTable
        (&DelFsusrMgmtUserName, &DelFsusrMgmtAuthString) == SNMP_SUCCESS)
    {
        /* Validation Passed */
        if (nmhGetFsusrMgmtUserPrivilege
            (&DelFsusrMgmtUserName, &DelFsusrMgmtAuthString,
             &u4Privilege) == SNMP_SUCCESS)
        {
            if ((FPAM_ROOT_PRIVILEGE_ID == u4Privilege) &&
                (LEGACY_MODE == i4FipsOperMode))
            {
                FPAM_UNLOCK;
                if (FpamUtlSearchRootPrivilege (&DelFsusrMgmtUserName) !=
                    FPAM_SUCCESS)
                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamUtlDelUser: Deletion of user Failed\r\n"));
                    return FPAM_FAILURE;
                }
                FPAM_LOCK;
            }
            if (nmhGetFsusrMgmtUserRowStatus
                (&DelFsusrMgmtUserName, &DelFsusrMgmtAuthString,
                 &i4RetValFsusrMgmtUserRowStatus) == SNMP_SUCCESS)
            {
                /* Get Passed */
                if (i4RetValFsusrMgmtUserRowStatus == ACTIVE)
                {
                    /* Only Active Row can be deleted */
                    if (nmhSetFsusrMgmtUserRowStatus
                        (&DelFsusrMgmtUserName, &DelFsusrMgmtAuthString,
                         DESTROY) == SNMP_SUCCESS)
                    {
                        /* Destroy Set */
                        FPAM_TRC ((FPAM_UTIL_TRC,
                                   "FpamUtlDelUser: "
                                   "Deletion of user success\r\n"));
#ifdef CLI_WANTED
                        if ((gu4FpamFeatureAddn & FPAM_USER_FORCED_LOGOUT) ==
                            FPAM_USER_FORCED_LOGOUT)
                        {
                            /* To forcefully terminate any cli sessions of the 
                             * deleted user */
                            CliLogOutUser (pi1NewUsrName);
                        }
#endif
                        FPAM_UNLOCK;
                        return FPAM_SUCCESS;
                    }
                }
            }
        }
    }
    FPAM_TRC ((FPAM_UTIL_TRC, "FpamUtlDelUser: Deletion of user Failed\r\n"));
    FPAM_UNLOCK;
    return FPAM_FAILURE;
}

/********************************************************************
 * Function    : FpamWriteUsers
 * Input       : None
 * Description : Users file maintained by switches will be updated here
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/

INT1
FpamWriteUsers ()
{
    INT4                i4Fd = 0;
    INT1                ai1Buf[FPAM_MAX_USERS_LINE_LEN];
    INT2                i2Len = 0;
    UINT2               u2CkSum = 0;
    UINT4               u4Sum = 0;
    INT2                i2Length = 0;
    INT1                ai1TimeStampString[FPAM_TIME_STAMP_LEN];

    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserNameSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStringSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtAuthString;
    UINT1               au1FsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1FsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               au1NextFsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1NextFsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];

    MEMSET (ai1TimeStampString, 0, FPAM_TIME_STAMP_LEN);
    MEMSET (au1FsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (au1NextFsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (&FsusrMgmtUserNameSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));
    MEMSET (au1NextFsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));
    MEMSET (&FsusrMgmtAuthStringSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    FsusrMgmtUserNameSrc.pu1_OctetList = au1FsusrMgmtUserName;
    FsusrMgmtAuthStringSrc.pu1_OctetList = au1FsusrMgmtAuthString;
    NextFsusrMgmtAuthString.pu1_OctetList = au1NextFsusrMgmtAuthString;
    NextFsusrMgmtUserName.pu1_OctetList = au1NextFsusrMgmtUserName;

    MEMSET (ai1Buf, 0, sizeof (ai1Buf));
    FileDelete ((UINT1 *) FPAM_USER_FILE_NAME);    /* remove the existing file */
    i4Fd = FpamOpenFile (FPAM_USER_FILE_NAME, FPAM_USER_FILE_WRITE_ONLY);
    if (i4Fd < 0)
    {
        return FPAM_FAILURE;
    }

    if (SNMP_SUCCESS != nmhGetFirstIndexFsusrMgmtTable
        (&FsusrMgmtUserNameSrc, &FsusrMgmtAuthStringSrc))
    {
        FileClose (i4Fd);
        return FPAM_FAILURE;
    }
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            FsusrMgmtUserNameSrc.pu1_OctetList, FsusrMgmtUserNameSrc.i4_Length);
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        FsusrMgmtUserNameSrc.i4_Length;
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));
    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    UtilSetChkSumLastStat (0);
    while (1)
    {
        if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) == OSIX_SUCCESS)
        {
            SPRINTF ((CHR1 *) ai1TimeStampString,
                     "%d|%d|%d|%d|%d|%d|%d|%d|%d",
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_sec,
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_min,
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_hour,
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_mday,
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_mon,
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_year,
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_wday,
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_yday,
                     FpamFsUsrMgmtEntry.MibObject.
                     TmFsusrMgmtUserPasswordCreationTime.tm_isdst);

            i2Len = (INT2)
                SPRINTF ((CHR1 *) ai1Buf, "%s:%d:%s:%s:%s:%d:%d\n",
                         (CHR1 *) FpamFsUsrMgmtEntry.MibObject.
                         au1FsusrMgmtUserName,
                         FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserPrivilege,
                         (CHR1 *) FPAM_DEF_MODE,
                         (CHR1 *) FpamFsUsrMgmtEntry.MibObject.
                         au1FsusrMgmtUserPassword,
                         (CHR1 *) ai1TimeStampString,
                         FpamFsUsrMgmtEntry.MibObject.
                         u4FsusrMgmtUserLockRelTime,
                         FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserStatus);
            /* Calculating checksum */
            UtilCalcCheckSum ((UINT1 *) ai1Buf, i2Len, &u4Sum, &u2CkSum,
                              CKSUM_DATA);
            FileWrite (i4Fd, (CHR1 *) ai1Buf, i2Len);
            i2Length += i2Len;
            if (SNMP_SUCCESS !=
                nmhGetNextIndexFsusrMgmtTable (&FsusrMgmtUserNameSrc,
                                               &NextFsusrMgmtUserName,
                                               &FsusrMgmtAuthStringSrc,
                                               &NextFsusrMgmtAuthString))
            {
                break;
            }

            Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry,
                                      &NextFsusrMgmtUserName,
                                      &NextFsusrMgmtAuthString);

            FsusrMgmtUserNameSrc.pu1_OctetList =
                NextFsusrMgmtUserName.pu1_OctetList;
            FsusrMgmtUserNameSrc.i4_Length = NextFsusrMgmtUserName.i4_Length;

        }
    }

    if (FpamFindCksum (i4Fd, i2Length, u4Sum) == FPAM_FAILURE)
    {
        FileClose (i4Fd);
        return FPAM_FAILURE;
    }

    FileClose (i4Fd);
    return FPAM_SUCCESS;
}

/******************************************************************
 * Function    : FpamReadUsers
 * Input       : None
 * Description : Users file details will be populated into data structure
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/

INT1
FpamReadUsers ()
{
    INT1                ai1Buf[FPAM_MAX_USERS_LINE_LEN + 1];
    INT1                ai1TempBuf[FPAM_MAX_USERS_LINE_LEN + 1];
    INT1               *pi1UsrName = NULL;
    INT1               *pi1UserPriv = NULL;
    INT1               *pi1Mode = NULL;
    INT1               *pi1UserPswd = NULL;
    INT1               *pi1BlockUserRelTime = NULL;
    INT1               *pi1UserPasswordCreationTime = NULL;
    INT1               *pi1UserStatus = NULL;
    INT2                i2ReadLen = 0;
    INT2                i2TotLen = 0;
    INT2                i2StrLen = 0;
    INT4                i4Fd = 0;
    UINT4               u4Sum = 0;
    UINT2               u2CkSum = 0;

    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamIsSetFsUsrMgmtEntry FpamIsSetFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (&FpamIsSetFsUsrMgmtEntry, 0, sizeof (tFpamIsSetFsUsrMgmtEntry));

    i4Fd = FpamOpenFile (FPAM_USER_FILE_NAME, FPAM_USER_FILE_READ_ONLY);
    if (i4Fd < 0)
    {
        return (FPAM_FAILURE);
    }

    UtilSetChkSumLastStat (0);

    MEMSET (ai1Buf, 0, FPAM_MAX_USERS_LINE_LEN + 1);
    MEMSET (ai1TempBuf, 0, FPAM_MAX_USERS_LINE_LEN + 1);
    while (FpamReadLineFromFile (i4Fd, ai1Buf, FPAM_MAX_USERS_LINE_LEN,
                                 &i2ReadLen) != FPAM_EOF)
    {
        /*  MEMCPY(ai1TempBuf,ai1Buf, STRLEN(ai1Buf)); */
        STRCPY (ai1TempBuf, ai1Buf);
        if (i2ReadLen > 0)
        {

            UtilCalcCheckSum ((UINT1 *) ai1TempBuf, i2ReadLen, &u4Sum,
                              &u2CkSum, CKSUM_DATA);
            i2TotLen += i2ReadLen;
            i2StrLen = STRLEN (ai1TempBuf);
            if (i2StrLen > 0)
            {
                ai1TempBuf[i2StrLen - 1] = '\0';
            }
            if ((FpamSplitUserBuf
                 (ai1TempBuf, &pi1UsrName, &pi1UserPriv, &pi1Mode, &pi1UserPswd,
                  &pi1UserPasswordCreationTime, &pi1BlockUserRelTime,
                  &pi1UserStatus) != FPAM_FAILURE) && ((pi1UsrName != NULL)
                                                       && (pi1UserPriv != NULL)
                                                       && (pi1UserPswd !=
                                                           NULL)))
            {
                FPAM_LOCK;
                /* MEMSET for loop */
                MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
                MEMSET (&FpamIsSetFsUsrMgmtEntry, 0,
                        sizeof (tFpamIsSetFsUsrMgmtEntry));
                /* Assign the index */
                MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
                        (UINT1 *) pi1UsrName, STRLEN (pi1UsrName));

                FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
                    STRLEN (pi1UsrName);
                FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserName = OSIX_TRUE;
                MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
                        (UINT1 *) FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

                FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
                    STRLEN (FPAM_AUTH_STRING);
                FpamIsSetFsUsrMgmtEntry.bFsusrMgmtAuthString = OSIX_TRUE;

                if (STRLEN (pi1UserPswd) > FPAM_MAX_PASSWD_LEN)
                {
                    FPAM_TRC ((FPAM_UTIL_TRC,
                               "FpamReadUsers: users file has been changed"
                               "Password exceeds the maximum length.\r\n"));
                    FPAM_UNLOCK;
                    FileClose (i4Fd);
                    return FPAM_FAILURE;
                }

                /* Assign the value */
                MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserPassword,
                        (UINT1 *) pi1UserPswd, STRLEN (pi1UserPswd));
                FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserPasswordLen =
                    STRLEN (pi1UserPswd);
                FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserPassword = OSIX_TRUE;

                FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserPrivilege =
                    ATOI (pi1UserPriv);
                FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserPrivilege = OSIX_TRUE;

                if (pi1UserStatus != NULL)
                {
                    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserStatus =
                        ATOI (pi1UserStatus);
                    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserStatus = OSIX_TRUE;
                }
                else
                {
                    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserStatus =
                        FPAM_USER_ENABLE;
                    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserStatus = OSIX_TRUE;
                }

                if (pi1UserPasswordCreationTime != NULL)
                {
                    FpamReadUserPasswordCreationTime ((UINT1 *)
                                                      pi1UserPasswordCreationTime,
                                                      &(FpamFsUsrMgmtEntry.
                                                        MibObject.
                                                        TmFsusrMgmtUserPasswordCreationTime));
                }
                else
                {
                    FpamGetCurrentTime (&(FpamFsUsrMgmtEntry.MibObject.
                                          TmFsusrMgmtUserPasswordCreationTime));
                }
                FpamIsSetFsUsrMgmtEntry.bTmFsusrMgmtUserPasswordCreationTime =
                    OSIX_FALSE;

                if (pi1BlockUserRelTime != NULL)
                {
                    FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserLockRelTime =
                        ATOI (pi1BlockUserRelTime);
                    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserLockRelTime =
                        OSIX_TRUE;
                }
                else
                {
                    FpamFsUsrMgmtEntry.MibObject.u4FsusrMgmtUserLockRelTime =
                        FPAM_DEF_FSUSRMGMTUSERLOCKRELTIME;
                    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserLockRelTime =
                        OSIX_TRUE;
                }

                if (FPAM_SUCCESS ==
                    FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
                {
                    MEMCPY (FpamFsUsrMgmtEntry.MibObject.
                            au1FsusrMgmtUserConfirmPwd, (UINT1 *) pi1UserPswd,
                            STRLEN (pi1UserPswd));
                    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserConfirmPwdLen =
                        STRLEN (pi1UserPswd);
                    FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserConfirmPwd =
                        OSIX_TRUE;
                }
                /* Creating new user */
                FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserRowStatus = OSIX_TRUE;
                FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus =
                    CREATE_AND_WAIT;
                if (FpamSetAllFsusrMgmtTable
                    (&FpamFsUsrMgmtEntry, &FpamIsSetFsUsrMgmtEntry, OSIX_TRUE,
                     OSIX_FALSE) != OSIX_SUCCESS)
                {
                    FPAM_UNLOCK;
                    FileClose (i4Fd);
                    return FPAM_FAILURE;
                }
                FpamIsSetFsUsrMgmtEntry.bFsusrMgmtUserRowStatus = OSIX_TRUE;
                FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserRowStatus = ACTIVE;

                if (FpamSetAllFsusrMgmtTable
                    (&FpamFsUsrMgmtEntry, &FpamIsSetFsUsrMgmtEntry, OSIX_TRUE,
                     OSIX_FALSE) != OSIX_SUCCESS)
                {
                    FPAM_UNLOCK;
                    FileClose (i4Fd);
                    return FPAM_FAILURE;
                }
                FPAM_UNLOCK;
            }
            else
            {
                /* Split buffer not success 
                 * or Last line reached in Users file */
                FPAM_TRC ((FPAM_UTIL_TRC,
                           "Split buffer not success "
                           "or Last line reached in Users file. \r\n"));
                break;
            }
        }
        MEMSET (ai1Buf, 0, FPAM_MAX_USERS_LINE_LEN + 1);
    }
    /* Verification of checksum for the last line */
    if (FpamVerifyCheckSum (ai1Buf, i2TotLen, i2ReadLen, u4Sum, u2CkSum) ==
        FPAM_FAILURE)
    {
        FileClose (i4Fd);
        return FPAM_FAILURE;
    }
    FileClose (i4Fd);
    return FPAM_SUCCESS;

}

/******************************************************************
 * Function    : FpamSplitUserBuf
 * Input       : pi1Buf, ppi1UserName, ppi1UserPriv, ppi1Mode, ppi1UserStatus,
 *               ppi1ReleaseTime, ppi1Passwd
 * Description : To read the user file and split up the user details
 * Output      : Splits up the buffer.
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/

INT4
FpamSplitUserBuf (INT1 *pi1Buf, INT1 **ppi1UserName, INT1 **ppi1UserPriv,
                  INT1 **ppi1Mode, INT1 **ppi1Passwd,
                  INT1 **ppi1UserPasswdCreationTime, INT1 **ppi1ReleaseTime,
                  INT1 **ppi1UserStatus)
{
    INT1               *pi1Ptr = NULL;
    INT1               *pi1TempPtr = NULL;
    UINT1               u1DelimiterCnt = 0;
    UINT1               u1PassDelimiterCnt = 0;

    /* Buffer has the user details to get Split up. */
    pi1Ptr = pi1Buf;
    pi1TempPtr = pi1Buf;

    if (pi1Buf == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "Split buffer not success. The Buffer is NULL \r\n"));
        return FPAM_FAILURE;
    }

    if ((STRNCMP (pi1Ptr, "CKSUM", STRLEN ("CKSUM")) == 0))
    {
        return FPAM_FAILURE;
    }

    /* Encryped Password might contain delimiter ':'
     * Calculate the number of extra delimiter in the given string */
    while (*pi1TempPtr != '\0')
    {
        if (*pi1TempPtr == ':')
        {
            u1DelimiterCnt++;
        }
        pi1TempPtr++;
    }
    /* Delimiter present in the password string 
     * Assuming the delimiter count less than 6 is treated 
     * Backward compatabilty mode */
    if ((u1DelimiterCnt > FPAM_MAX_BC_DELIMITER) &&
        (u1DelimiterCnt < FPAM_MAX_DELIMITER))
    {
        u1PassDelimiterCnt = (UINT1) (u1DelimiterCnt - FPAM_MAX_BC_DELIMITER);
    }
    else if (u1DelimiterCnt > FPAM_MAX_DELIMITER)
    {
        u1PassDelimiterCnt = (UINT1) (u1DelimiterCnt - FPAM_MAX_DELIMITER);
    }

    *ppi1UserName = pi1Ptr;

    /* Spliting the username */
    while (*pi1Ptr != ':')
    {
        if (*pi1Ptr == '\0')
        {
            *ppi1UserName = NULL;
            break;
        }
        pi1Ptr++;
    }
    if (*pi1Ptr != '\0')
    {
        *pi1Ptr = '\0';
        pi1Ptr++;
    }
    /* Spliting the user privilege */
    *ppi1UserPriv = pi1Ptr;

    while (*pi1Ptr != ':')
    {
        if (*pi1Ptr == '\0')
        {
            *ppi1UserPriv = NULL;
            break;
        }
        pi1Ptr++;
    }
    if (*pi1Ptr != '\0')
    {
        *pi1Ptr = '\0';
        pi1Ptr++;
    }
    /* Spliting the user mode */
    *ppi1Mode = pi1Ptr;

    while (*pi1Ptr != ':')
    {
        if (*pi1Ptr == '\0')
        {
            *ppi1Mode = NULL;
            break;
        }
        pi1Ptr++;
    }

    if (*pi1Ptr != '\0')
    {
        *pi1Ptr = '\0';
        pi1Ptr++;
    }
    /* Spliting the user password */
    *ppi1Passwd = pi1Ptr;

    while ((*pi1Ptr != ':') || (u1PassDelimiterCnt > 0))
    {
        if (*pi1Ptr == '\0')
        {
            *ppi1UserPasswdCreationTime = NULL;
            break;
        }
        if (*pi1Ptr == ':')
        {
            u1PassDelimiterCnt--;
        }
        pi1Ptr++;
    }
    if (*pi1Ptr != '\0')
    {
        *pi1Ptr = '\0';
        pi1Ptr++;
        *ppi1UserPasswdCreationTime = pi1Ptr;
    }

    /* Spliting the user password creation time */
    while (*pi1Ptr != ':')
    {
        if (*pi1Ptr == '\0')
        {
            *ppi1ReleaseTime = NULL;
            break;
        }
        pi1Ptr++;
    }
    if (*pi1Ptr != '\0')
    {
        *pi1Ptr = '\0';
        pi1Ptr++;

        *ppi1ReleaseTime = pi1Ptr;
    }

    /* Spliting the user release time */
    while (*pi1Ptr != ':')
    {
        if (*pi1Ptr == '\0')
        {
            *ppi1UserStatus = NULL;
            break;
        }
        pi1Ptr++;
    }
    if (*pi1Ptr != '\0')
    {
        *pi1Ptr = '\0';
        pi1Ptr++;

        /* Spliting the user status */
        *ppi1UserStatus = pi1Ptr;
    }
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamUtlSearchUserName
 * Description : To Search a user in the data structure.
 * Input       : pFpamFsUsrMgmtEntry
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT4
FpamUtlSearchUserName (tSNMP_OCTET_STRING_TYPE * pFpamFsUsrMgmtEntry)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserNameSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStringSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtAuthString;
    UINT1               au1FsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1FsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               au1NextFsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1NextFsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];

    MEMSET (au1FsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (&FsusrMgmtUserNameSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));
    MEMSET (&FsusrMgmtAuthStringSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextFsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (au1NextFsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));

    FsusrMgmtUserNameSrc.pu1_OctetList = au1FsusrMgmtUserName;
    FsusrMgmtAuthStringSrc.pu1_OctetList = au1FsusrMgmtAuthString;
    NextFsusrMgmtAuthString.pu1_OctetList = au1NextFsusrMgmtAuthString;
    NextFsusrMgmtUserName.pu1_OctetList = au1NextFsusrMgmtUserName;
    if ((pFpamFsUsrMgmtEntry == NULL) ||
        (pFpamFsUsrMgmtEntry->i4_Length > FPAM_MAX_USERNAME_LEN) ||
        (pFpamFsUsrMgmtEntry->i4_Length < FPAM_MIN_USERNAME_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC, "FpamUtlSearchUserName : Null Entry. \r\n"));
        return FPAM_FAILURE;
    }
    FPAM_LOCK;
    if (SNMP_SUCCESS != nmhGetFirstIndexFsusrMgmtTable
        (&FsusrMgmtUserNameSrc, &FsusrMgmtAuthStringSrc))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlSearchUserName : nmhGetFirstIndexFsusrMgmtTable \
                   failed. \r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &FsusrMgmtUserNameSrc,
                              &FsusrMgmtAuthStringSrc);

    while (1)
    {
        if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
        {
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
        if (STRCMP (pFpamFsUsrMgmtEntry->pu1_OctetList,
                    FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName) == 0)
        {
            /* user present in the table */
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlSearchUserName : Search Success "
                       "user present \r\n"));
            FPAM_UNLOCK;
            return FPAM_SUCCESS;
        }

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsusrMgmtTable (&FsusrMgmtUserNameSrc,
                                           &NextFsusrMgmtUserName,
                                           &FsusrMgmtAuthStringSrc,
                                           &NextFsusrMgmtAuthString))
        {
            break;
        }

        Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &NextFsusrMgmtUserName,
                                  &NextFsusrMgmtAuthString);

        FsusrMgmtUserNameSrc.pu1_OctetList =
            NextFsusrMgmtUserName.pu1_OctetList;
        FsusrMgmtUserNameSrc.i4_Length = NextFsusrMgmtUserName.i4_Length;
    }
    FPAM_UNLOCK;
    return FPAM_FAILURE;
}

/**************************************************************************
 * Function    : FpamGetPrivilege
 * Description : To Read the privilege value of a particular user
 * Input       : pi1UserName
 * Output      : pi2UserPrivilege
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamGetPrivilege (CHR1 * pi1UserName, INT2 *pi2UserPrivilege)
{
    UINT4               u4Privilege = 0;
    tSNMP_OCTET_STRING_TYPE PrivFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE PrivFsusrMgmtAuthString;

    MEMSET (&PrivFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&PrivFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if ((pi1UserName == NULL) || (pi2UserPrivilege == NULL))
    {
        FPAM_TRC ((FPAM_UTIL_TRC, "FpamGetPrivilege : Null Entry. \r\n"));
        return FPAM_FAILURE;
    }

    *pi2UserPrivilege = 0;
    PrivFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pi1UserName;
    PrivFsusrMgmtUserName.i4_Length = STRLEN (pi1UserName);
    PrivFsusrMgmtAuthString.pu1_OctetList = (UINT1 *) FPAM_AUTH_STRING;
    PrivFsusrMgmtAuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);
    if ((PrivFsusrMgmtUserName.i4_Length < FPAM_MIN_USERNAME_LEN) ||
        (PrivFsusrMgmtUserName.i4_Length > FPAM_MAX_USERNAME_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetPrivilege : Length Check Fail. \r\n"));
        return FPAM_FAILURE;
    }
    FPAM_LOCK;
    if (nmhGetFsusrMgmtUserPrivilege
        (&PrivFsusrMgmtUserName, &PrivFsusrMgmtAuthString,
         &u4Privilege) == SNMP_FAILURE)
    {
        /* Privilege Get */
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetPrivilege : privilege value not found "
                   "for the user \r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }
    FPAM_UNLOCK;
    *pi2UserPrivilege = (INT2) u4Privilege;
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamGetCurrentTime
 * Description : To get the current time
 * Input       : NONE 
 * Output      : pTmCurrentTime
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamGetCurrentTime (tFpamUtlTm * pTmCurrentTime)
{
    UtlGetTime (pTmCurrentTime);
    (pTmCurrentTime->tm_wday)++;
    (pTmCurrentTime->tm_mon)++;
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamGetUserPasswordCreationTime
 * Description : To get the time at which the user last changed their password.
 * Input       : pi1UserName
 * Output      : pTmUserPasswordLastChanged
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamGetUserPasswordCreationTime (INT1 *pi1UserName,
                                 tFpamUtlTm * pTmUserPasswordLastChanged)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    UINT4               u4UserNameLen = 0;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    if ((pi1UserName == NULL) || (pTmUserPasswordLastChanged == NULL))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetPasswordLifeTime : Null Entry. \r\n"));
        return FPAM_FAILURE;
    }
    u4UserNameLen = STRLEN (pi1UserName);
    if ((u4UserNameLen < FPAM_MIN_USERNAME_LEN) ||
        (u4UserNameLen > FPAM_MAX_USERNAME_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetPasswordLifeTime : Length Check Fail. \r\n"));
        return FPAM_FAILURE;
    }
    FPAM_LOCK;

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pi1UserName, u4UserNameLen);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen = (INT4) u4UserNameLen;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        return FPAM_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pTmUserPasswordLastChanged,
            &(FpamFsUsrMgmtEntry.MibObject.TmFsusrMgmtUserPasswordCreationTime),
            sizeof (tFpamUtlTm));

    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}

/**************************************************************************
* Function    : FpamPasswordValidateRules
* Description : Used to set values for fpam password validate objects from cli.
* Input       : u4Case : The variable to be set.
*               u4Value: The value to be set for the variable.
* Output      : None
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT1
FpamPasswordValidateRules (UINT4 u4Case, UINT4 u4Value)
{
#ifdef CLI_WANTED
    UINT4               u4ErrorCode = 0;
    UINT4               u4RulesMask = 0;
    UINT4               u4LowerCaseCount = 0;
    UINT4               u4UpperCaseCount = 0;
    UINT4               u4NumberCount = 0;
    UINT4               u4SymbolCount = 0;
    switch (u4Case)
    {
        case CLI_SET_PASSWORD_VALIDATE_MASK:
            if (FpamSetPasswdValidateMask (&u4ErrorCode, u4Value) ==
                FPAM_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
                {
                    mmi_printf ("\r\n%% Configured total number of "
                                "password characters exceed the "
                                "max password length\r\n");
                }
                else if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                {
                    mmi_printf ("\r\n%% Password validate mask should "
                                "be between %d and %d\r\n",
                                FPAM_MIN_SET_PASSWD_VALIDATION_CHAR,
                                FPAM_MAX_SET_PASSWD_VALIDATION_CHAR);
                }
                return CLI_FAILURE;
            }
            break;

        case CLI_SET_PASSWORD_LOWERCASE_CHAR_COUNT:
            if (FpamSetPwdValidateLwrCaseCharCnt (&u4ErrorCode, u4Value)
                == FPAM_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
                {
                    mmi_printf ("\r\n%% Configured total number of "
                                "password characters exceed the "
                                "max password length\r\n");
                }
                else if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                {
                    mmi_printf ("\r\n%% Password minimum lowercase "
                                "character count should be between 0 to %d\r\n",
                                FPAM_MAX_SET_PASSWD_MAX_LOWER_CASE_CHAR);
                }
                return CLI_FAILURE;
            }
            break;

        case CLI_SET_PASSWORD_UPPERCASE_CHAR_COUNT:
            if (FpamSetPwdValidateUprCaseCharCnt (&u4ErrorCode, u4Value)
                == FPAM_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
                {
                    mmi_printf ("\r\n%% Configured total number of "
                                "password characters exceed the "
                                "max password length\r\n");
                }
                else if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                {
                    mmi_printf ("\r\n%% Password minimum uppercase "
                                "character count should be between 0 to %d\r\n",
                                FPAM_MAX_SET_PASSWD_MAX_UPPER_CASE_CHAR);
                }
                return CLI_FAILURE;
            }
            break;

        case CLI_SET_PASSWORD_NUMBER_COUNT:
            if (FpamSetPasswdValidateNumberCount (&u4ErrorCode, u4Value) ==
                FPAM_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
                {
                    mmi_printf ("\r\n%% Configured total number of "
                                "password characters exceed the "
                                "max password length\r\n");
                }
                else if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                {
                    mmi_printf ("\r\n%% Password minimum numeric "
                                "character count should be between 0 to %d\r\n",
                                FPAM_MAX_SET_PASSWD_MAX_NUMERIC_CHAR);
                }
                return CLI_FAILURE;
            }
            break;

        case CLI_SET_PASSWORD_SYMBOL_COUNT:
            if (FpamSetPasswdValidateSymbolCount (&u4ErrorCode, u4Value) ==
                FPAM_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
                {
                    mmi_printf ("\r\n%% Configured total number of "
                                "password characters exceed the "
                                "max password length\r\n");
                }
                else if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
                {
                    mmi_printf ("\r\n%% Password minimum symbol "
                                "character count should be between 0 to %d\r\n",
                                FPAM_MAX_SET_PASSWD_MAX_SPECIAL_CHAR);
                }
                return CLI_FAILURE;
            }
            break;

        case CLI_SHOW_PASSWORD_VALIDATE_RULES:
            if (FpamShowPasswdValidateRules (&u4RulesMask,
                                             &u4LowerCaseCount,
                                             &u4UpperCaseCount,
                                             &u4NumberCount,
                                             &u4SymbolCount) == FPAM_FAILURE)
            {
                return CLI_FAILURE;
            }
            else
            {
                mmi_printf ("\rPassword Validation Mask : %x\r\n"
                            "Min Lowercase char count : %u\r\n"
                            "Min Uppercase char count : %u\r\n"
                            "Min Numeric char count   : %u\r\n"
                            "Min Symbol char count    : %u\r\n",
                            u4RulesMask, u4LowerCaseCount, u4UpperCaseCount,
                            u4NumberCount, u4SymbolCount);
            }
            break;
        default:
            return CLI_FAILURE;
    }
#else
    UNUSED_PARAM (u4Case);
    UNUSED_PARAM (u4Value);
#endif
    return (CLI_SUCCESS);
}

/**************************************************************************
 * Function    : FpamShowPasswdMaxLifeTime
 * Description : Displays the Password Max Life Time
 * Input       : NONE
 * Output      : pu4MaxPasswdLifeTime
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 * ****************************************************************************/
INT1
FpamShowPasswdMaxLifeTime (UINT4 *pu4MaxPasswdLifeTime)
{

    if (nmhGetFsusrMgmtPasswdMaxLifeTime (pu4MaxPasswdLifeTime) != SNMP_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamShowPasswdMaxLifeTime: nmhGetFsusrMgmtPasswdMaxLifeTime"
                   " failed.\r\n"));
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);

}

/**************************************************************************
* Function    : FpamSetPasswdMaxLifeTime
* Description : To set the password max life time
* Input       : u4PasswdLifeTime
* Output      : None
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT1
FpamSetPasswdMaxLifeTime (UINT4 u4PasswdLifeTime)
{
    UINT4               u4Error;
    if (nmhTestv2FsusrMgmtPasswdMaxLifeTime (&u4Error, u4PasswdLifeTime) !=
        SNMP_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetPasswdMaxLifeTime: Password Max life Time should "
                   "be between 0-366 days\r\n"));
        return FPAM_FAILURE;
    }
    if (nmhSetFsusrMgmtPasswdMaxLifeTime (u4PasswdLifeTime) == SNMP_FAILURE)
    {
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);
}

/**************************************************************************
* Function    : FpamSetPasswdValidateMask
* Description : To set the password validate mask
* Input       : u4Mask 
* Output      : pu4Error
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT1
FpamSetPasswdValidateMask (UINT4 *pu4Error, UINT4 u4Mask)
{
    if (nmhTestv2FsusrMgmtPasswdValidationChars (pu4Error, u4Mask) !=
        SNMP_SUCCESS)
    {

        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetPasswdValidateMask: Password Validation Mask "
                   "test failed \r\n"));
        return FPAM_FAILURE;
    }

    if (nmhSetFsusrMgmtPasswdValidationChars (u4Mask) == SNMP_FAILURE)
    {
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);
}

/**************************************************************************
* Function    : FpamSetPwdValidateLwrCaseCharCnt
* Description : To set the minimum count of lower case characters to be
*               present for password validation.
*
* Input       : u4LowerCaseCharCount
* Output      : pu4Error
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT1
FpamSetPwdValidateLwrCaseCharCnt (UINT4 *pu4Error, UINT4 u4LowerCaseCharCount)
{
    if (nmhTestv2FsusrMgmtPasswdValidateNoOfLowerCase
        (pu4Error, u4LowerCaseCharCount) != SNMP_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetPwdValidateLwrCaseCharCnt: Password Validate "
                   "lowercase char count test failed \r\n"));
        return FPAM_FAILURE;
    }
    if (nmhSetFsusrMgmtPasswdValidateNoOfLowerCase (u4LowerCaseCharCount) ==
        SNMP_FAILURE)
    {
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);
}

/**************************************************************************
* Function    : FpamSetPwdValidateUprCaseCharCnt
* Description : To set the minimum count of upper case characters to be
*               present for password validation.
*
* Input       : u4UpperCaseCharCount 
* Output      : pu4Error
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT1
FpamSetPwdValidateUprCaseCharCnt (UINT4 *pu4Error, UINT4 u4UpperCaseCharCount)
{
    if (nmhTestv2FsusrMgmtPasswdValidateNoOfUpperCase
        (pu4Error, u4UpperCaseCharCount) != SNMP_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetPwdValidateUprCaseCharCnt: Password Validate "
                   "Uppercase char count test failed \r\n"));
        return FPAM_FAILURE;
    }
    if (nmhSetFsusrMgmtPasswdValidateNoOfUpperCase (u4UpperCaseCharCount) ==
        SNMP_FAILURE)
    {
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);
}

/**************************************************************************
* Function    : FpamSetPasswdValidateNumberCount
* Description : To set the minimum count of numeric characters to be
*               present for password validation.
*
* Input       : u4NumberCount 
* Output      : pu4Error
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT1
FpamSetPasswdValidateNumberCount (UINT4 *pu4Error, UINT4 u4NumberCount)
{
    if (nmhTestv2FsusrMgmtPasswdValidateNoOfNumericals (pu4Error, u4NumberCount)
        != SNMP_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetPasswdValidateMask: Password Validate "
                   "Numeric char count test failed\r\n"));
        return FPAM_FAILURE;
    }
    if (nmhSetFsusrMgmtPasswdValidateNoOfNumericals (u4NumberCount) ==
        SNMP_FAILURE)
    {
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);
}

/**************************************************************************
* Function    : FpamSetPasswdValidateSymbolCount
* Description : To set the minimum count of special characters to be
*               present for password validation.
*
* Input       : u4SplCharCount 
* Output      : pu4Error 
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT1
FpamSetPasswdValidateSymbolCount (UINT4 *pu4Error, UINT4 u4SplCharCount)
{
    if (nmhTestv2FsusrMgmtPasswdValidateNoOfSplChars (pu4Error, u4SplCharCount)
        != SNMP_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetPasswdValidateMask: Password Validate "
                   "Symbols count test failed\r\n"));
        return FPAM_FAILURE;
    }
    if (nmhSetFsusrMgmtPasswdValidateNoOfSplChars (u4SplCharCount) ==
        SNMP_FAILURE)
    {
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);
}

/**************************************************************************
* Function    : FpamValidatePasswordRules
* Description : To check if valid password rules are correctly set.
* Input       : u4PasswordMask,
*         u4LowerCaseCount,
*         u4UpperCaseCount,
*         u4NumberCount,
*         u4SplCharCount 
* Output      : None
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT1
FpamValidatePasswordRules (UINT4 u4PasswordMask,
                           UINT4 u4LowerCaseCount,
                           UINT4 u4UpperCaseCount,
                           UINT4 u4NumberCount, UINT4 u4SplCharCount)
{
    UINT4               u4TempPasswordLength = 0;

    if ((u4PasswordMask & FPAM_LOWERCASE_MASK) == FPAM_LOWERCASE_MASK)
    {
        u4TempPasswordLength += u4LowerCaseCount;
    }

    if ((u4PasswordMask & FPAM_UPPERCASE_MASK) == FPAM_UPPERCASE_MASK)
    {
        u4TempPasswordLength += u4UpperCaseCount;
    }

    if ((u4PasswordMask & FPAM_NUMBER_MASK) == FPAM_NUMBER_MASK)
    {
        u4TempPasswordLength += u4NumberCount;
    }

    if ((u4PasswordMask & FPAM_SYMBOL_MASK) == FPAM_SYMBOL_MASK)
    {
        u4TempPasswordLength += u4SplCharCount;
    }

    if (u4TempPasswordLength > FPAM_MAX_PASSWD_LEN)
    {
        return FPAM_FAILURE;
    }

    return FPAM_SUCCESS;
}

/**************************************************************************
* Function    : FpamShowPasswdValidateRules 
* Description : To display the password validation rules related configuration.
*
* Input       : None 
* Output      : pu4RulesMask : returns the password validation rules mask.
*               pu4LowerCaseCount: returns the lowercase character count.
*               pu4UpperCaseCount: returns the uppercase character count.
*               pu4NumberCount:    returns the numeric character count.
*               pu4SymbolCount:    returns the special character count.
* Returns     : FPAM_SUCCESS
*****************************************************************************/
INT1
FpamShowPasswdValidateRules (UINT4 *pu4RulesMask, UINT4 *pu4LowerCaseCount,
                             UINT4 *pu4UpperCaseCount, UINT4 *pu4NumberCount,
                             UINT4 *pu4SymbolCount)
{
    nmhGetFsusrMgmtPasswdValidationChars (pu4RulesMask);
    nmhGetFsusrMgmtPasswdValidateNoOfLowerCase (pu4LowerCaseCount);
    nmhGetFsusrMgmtPasswdValidateNoOfUpperCase (pu4UpperCaseCount);
    nmhGetFsusrMgmtPasswdValidateNoOfNumericals (pu4NumberCount);
    nmhGetFsusrMgmtPasswdValidateNoOfSplChars (pu4SymbolCount);

    return (FPAM_SUCCESS);
}

/**************************************************************************
* Function    : FpamSetMinPasswordLen
* Description : To check the Min password range.
* Input       : i4MinPassLen
* Output      : None
* Returns     : FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/

INT1
FpamSetMinPasswordLen (INT4 i4MinPassLen)
{
    UINT4               u4Error;
    if (nmhTestv2FsusrMgmtMinPasswordLen (&u4Error,
                                          i4MinPassLen) != SNMP_SUCCESS)

    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamTestPasswordLen:Password minimum length value can be "
                   "only between 8-20 \r\n"));
        return FPAM_FAILURE;
    }
    if (nmhSetFsusrMgmtMinPasswordLen (i4MinPassLen) == SNMP_FAILURE)
    {
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);
}

/**************************************************************************
 * Function    : FpamDisplayMinPasswordLen
 * Description : Show Min set password length.
 * Input       : i4MinPassLen
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 * ***************************************************************************/

INT4
FpamDisplayMinPasswordLen (UINT4 *i4MinPassLen)
{

    if (nmhGetFsusrMgmtMinPasswordLen (i4MinPassLen) != SNMP_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC, "Display Error \r\n"));
        return (FPAM_FAILURE);
    }
    return (FPAM_SUCCESS);

}

/**************************************************************************
 * Function    : FpamReadUserPasswordCreationTime
 * Description : To fill the time structure from time string. 
 * Input       : pi1UserPasswordCreationTime, pTmFsusrMgmtUserPasswordCreationTime 
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamReadUserPasswordCreationTime (UINT1 *pi1UserPasswordCreationTime,
                                  tFpamUtlTm *
                                  pTmFsusrMgmtUserPasswordCreationTime)
{
    UINT4               u4Index = 0;
    UINT4               u4TimestampArrIdxCounter = 0;
    UINT4               u4TimeIndex = 0;
    UINT4               au4TimeStampTempArray[FPAM_NO_OF_TIME_FIELDS];
    UINT1              *pi1String = NULL;

    if (pTmFsusrMgmtUserPasswordCreationTime == NULL)
    {
        return FPAM_FAILURE;
    }

    MEMSET (au4TimeStampTempArray, 0, sizeof (au4TimeStampTempArray));

    pi1String = pi1UserPasswordCreationTime;
    while (pi1UserPasswordCreationTime[u4Index] != '\0')
    {
        if (pi1UserPasswordCreationTime[u4Index] == FPAM_TIME_FIELD_SEPERATOR)
        {
            if (u4TimeIndex >= FPAM_NO_OF_TIME_FIELDS)
            {
                return FPAM_FAILURE;
            }
            pi1UserPasswordCreationTime[u4Index] = '\0';
            au4TimeStampTempArray[u4TimeIndex++] = ATOI (pi1String);

            pi1String = &pi1UserPasswordCreationTime[u4Index + 1];
        }
        u4Index++;
    }

    if (u4TimeIndex < FPAM_NO_OF_TIME_FIELDS)
    {
        au4TimeStampTempArray[u4TimeIndex++] = ATOI (pi1String);
    }

    if (u4TimeIndex < FPAM_NO_OF_TIME_FIELDS)
    {
        return FPAM_FAILURE;
    }

    pTmFsusrMgmtUserPasswordCreationTime->tm_sec =
        au4TimeStampTempArray[u4TimestampArrIdxCounter];
    pTmFsusrMgmtUserPasswordCreationTime->tm_min =
        au4TimeStampTempArray[++u4TimestampArrIdxCounter];
    pTmFsusrMgmtUserPasswordCreationTime->tm_hour =
        au4TimeStampTempArray[++u4TimestampArrIdxCounter];
    pTmFsusrMgmtUserPasswordCreationTime->tm_mday =
        au4TimeStampTempArray[++u4TimestampArrIdxCounter];
    pTmFsusrMgmtUserPasswordCreationTime->tm_mon =
        au4TimeStampTempArray[++u4TimestampArrIdxCounter];
    pTmFsusrMgmtUserPasswordCreationTime->tm_year =
        au4TimeStampTempArray[++u4TimestampArrIdxCounter];
    pTmFsusrMgmtUserPasswordCreationTime->tm_wday =
        au4TimeStampTempArray[++u4TimestampArrIdxCounter];
    pTmFsusrMgmtUserPasswordCreationTime->tm_yday =
        au4TimeStampTempArray[++u4TimestampArrIdxCounter];
    pTmFsusrMgmtUserPasswordCreationTime->tm_isdst =
        au4TimeStampTempArray[++u4TimestampArrIdxCounter];

    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamCheckPassword
 * Description : To check the password configuration rules.
 * Input       : pFpamFsUsrMgmtEntry
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamCheckPassword (CHR1 * pi1UserName, CONST INT1 *pi1UserPassword)
{
    tSNMP_OCTET_STRING_TYPE CheckFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE CheckFsusrMgmtAuthString;
    tSNMP_OCTET_STRING_TYPE CheckFsusrMgmtUserPassword;
    INT1                ai1TempPasswd[FPAM_MAX_PASSWD_LEN + 1];
    INT1                ai1Passwd[FPAM_MAX_PASSWD_LEN + 1];
    UINT1               au1Pswd[FPAM_MAX_PASSWD_LEN + 1];
    UINT4               u4PswdLen = 0;
    INT4                i4UserStatus = FPAM_USER_DISABLE;

    if ((pi1UserName == NULL) || (pi1UserPassword == NULL))
    {
        FPAM_TRC ((FPAM_UTIL_TRC, "FpamCheckPassword : Null Entry. \r\n"));
        return FPAM_FAILURE;
    }

    MEMSET (&CheckFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&CheckFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&CheckFsusrMgmtUserPassword, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Pswd, 0, (FPAM_MAX_PASSWD_LEN + 1));
    MEMSET (ai1Passwd, 0, (FPAM_MAX_PASSWD_LEN + 1));
    MEMSET (ai1TempPasswd, 0, (FPAM_MAX_PASSWD_LEN + 1));

    CheckFsusrMgmtUserPassword.pu1_OctetList = au1Pswd;
    CheckFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pi1UserName;
    CheckFsusrMgmtUserName.i4_Length = STRLEN (pi1UserName);
    CheckFsusrMgmtAuthString.pu1_OctetList = (UINT1 *) FPAM_AUTH_STRING;
    CheckFsusrMgmtAuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);

    u4PswdLen = STRLEN (pi1UserPassword);

    if ((u4PswdLen > FPAM_MAX_PASSWD_LEN) ||
        (u4PswdLen < FPAM_MIN_PASSWD_LEN) ||
        (CheckFsusrMgmtUserName.i4_Length > FPAM_MAX_USERNAME_LEN) ||
        (CheckFsusrMgmtUserName.i4_Length < FPAM_MIN_USERNAME_LEN))
    {
        /* Password  and Username Length CHECK */
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamCheckPassword : Length of password or username "
                   "check fails \r\n"));
        return FPAM_FAILURE;
    }
    if (FpamGetUserStatus (pi1UserName, &i4UserStatus) == FPAM_FAILURE)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamCheckPassword :  userstatus read fails \r\n"));
        return FPAM_FAILURE;
    }
    if (FPAM_USER_DISABLE == i4UserStatus)
    {
        /* Via Web FpamCheckPassword is hit directly during login
         * to avoid disabled user login this check is done here */
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamCheckPassword : UserStatus is disabled \r\n"));
        return FPAM_FAILURE;
    }
    FPAM_LOCK;
    if (nmhGetFsusrMgmtUserPassword
        (&CheckFsusrMgmtUserName, &CheckFsusrMgmtAuthString,
         &CheckFsusrMgmtUserPassword) == SNMP_FAILURE)
    {
        /* No Such User */
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamCheckPassword : No Such User present to check "
                   "password \r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }
    FPAM_UNLOCK;
    STRCPY (ai1TempPasswd, pi1UserPassword);
    ai1TempPasswd[u4PswdLen] = '\0';
    STRNCPY (ai1Passwd, CheckFsusrMgmtUserPassword.pu1_OctetList,
             CheckFsusrMgmtUserPassword.i4_Length);
    ai1Passwd[CheckFsusrMgmtUserPassword.i4_Length] = '\0';

    FpamUtlEncriptPasswd ((CHR1 *) ai1TempPasswd);
    if (STRLEN (ai1TempPasswd) == STRLEN (ai1Passwd))
    {
        if (MEMCMP (ai1TempPasswd, ai1Passwd, STRLEN (ai1TempPasswd)) != 0)
        {
            return FPAM_FAILURE;
        }
    }
    else
    {
        return FPAM_FAILURE;
    }
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamChangePassword
 * Description : To change the user password.
 * Input       : pi1UserName, pi1NewPassword.
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamChangePassword (INT1 *pi1UserName, INT1 *pi1NewPasswd)
{
    tSNMP_OCTET_STRING_TYPE UserName;
    tSNMP_OCTET_STRING_TYPE UserNewPassword;
    tSNMP_OCTET_STRING_TYPE AuthString;
    UINT1               au1UserName[FPAM_MAX_USERNAME_LEN + 1];

    MEMSET (&UserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserNewPassword, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&AuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MEMSET (au1UserName, 0, FPAM_MAX_USERNAME_LEN + 1);
    STRNCPY (au1UserName, pi1UserName, FPAM_MAX_USERNAME_LEN);

    UserName.pu1_OctetList = (UINT1 *) au1UserName;
    UserName.i4_Length = STRLEN (au1UserName);
    UserNewPassword.pu1_OctetList = (UINT1 *) pi1NewPasswd;
    UserNewPassword.i4_Length = STRLEN (pi1NewPasswd);
    AuthString.pu1_OctetList = (UINT1 *) FPAM_AUTH_STRING;
    AuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);

    FPAM_LOCK;
    if (FpamUserPasswdConfCheck (&UserName, &UserNewPassword) == FPAM_FAILURE)
    {
        return FPAM_FAILURE;
    }

    if (nmhSetFsusrMgmtUserRowStatus
        (&UserName, &AuthString, NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    if (nmhSetFsusrMgmtUserPassword
        (&UserName, &AuthString, &UserNewPassword) == SNMP_FAILURE)
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    if (FPAM_SUCCESS ==
        FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
    {
        if (nmhSetFsusrMgmtUserConfirmPwd
            (&UserName, &AuthString, &UserNewPassword) == SNMP_FAILURE)
        {
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
    }
    if (nmhSetFsusrMgmtUserRowStatus
        (&UserName, &AuthString, ACTIVE) == SNMP_FAILURE)
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamPasswordExpiryCheck
 * Description : To check if the user password has expired.
 * Input       : pi1UserName
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamPasswordExpiryCheck (INT1 *pi1UserName)
{
    UINT4               u4PasswdMaxLifeTime = 0;
    INT4                i4Diff = 0;
    UINT4               u4DayDiff = 0;
    UINT4               u4YearDiff = 0;
    tFpamUtlTm          TmUserPasswordLastChanged;
    tFpamUtlTm          TmLoginTime;

    MEMSET (&TmUserPasswordLastChanged, 0, sizeof (tFpamUtlTm));
    MEMSET (&TmLoginTime, 0, sizeof (tFpamUtlTm));

    u4PasswdMaxLifeTime = FPAM_PASSWORD_MAX_LIFE_TIME;
    if (u4PasswdMaxLifeTime == FPAM_PASSWORD_NO_EXPIRY)
    {
        return FPAM_SUCCESS;
    }

    FpamGetUserPasswordCreationTime (pi1UserName, &TmUserPasswordLastChanged);
    FpamGetCurrentTime (&TmLoginTime);

    i4Diff = TmLoginTime.tm_yday - TmUserPasswordLastChanged.tm_yday;
    u4DayDiff =
        FPAM_LEAP_YEARS_BETWEEN (TmLoginTime.tm_year,
                                 TmUserPasswordLastChanged.tm_year);
    if (i4Diff < 0)
    {
        TmLoginTime.tm_year -= 1;
        i4Diff += FPAM_DAYS_IN_A_YEAR;
    }

    u4YearDiff = (TmLoginTime.tm_year - TmUserPasswordLastChanged.tm_year);
    u4DayDiff += (u4YearDiff * FPAM_DAYS_IN_A_YEAR) + i4Diff;

    if (u4DayDiff >= u4PasswdMaxLifeTime)
    {
        return FPAM_FAILURE;
    }

    return FPAM_SUCCESS;
}

INT4
FpamUtlShowRunningConfig (tCliHandle CliHandle)
{
#ifdef CLI_WANTED
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserNameSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStringSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtAuthString;
    UINT1               au1FsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1FsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               au1NextFsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1NextFsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               u1FipsEnabled = FPAM_FALSE;

#ifdef FIPS_WANTED
    INT4                i4FipsOperMode = 0;
#endif
    MEMSET (au1FsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (&FsusrMgmtUserNameSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));
    MEMSET (&FsusrMgmtAuthStringSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (au1NextFsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (au1NextFsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));

    FsusrMgmtUserNameSrc.pu1_OctetList = au1FsusrMgmtUserName;
    FsusrMgmtAuthStringSrc.pu1_OctetList = au1FsusrMgmtAuthString;
    NextFsusrMgmtAuthString.pu1_OctetList = au1NextFsusrMgmtAuthString;
    NextFsusrMgmtUserName.pu1_OctetList = au1NextFsusrMgmtUserName;

    FPAM_LOCK;
    if (SNMP_SUCCESS != nmhGetFirstIndexFsusrMgmtTable
        (&FsusrMgmtUserNameSrc, &FsusrMgmtAuthStringSrc))
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &FsusrMgmtUserNameSrc,
                              &FsusrMgmtAuthStringSrc);

    while (1)
    {
        if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
        {
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }

#ifdef FIPS_WANTED
        i4FipsOperMode = FipsGetFipsCurrOperMode ();

        if ((FIPS_MODE == i4FipsOperMode) || (CNSA_MODE == i4FipsOperMode))
        {
            u1FipsEnabled = FPAM_TRUE;
        }
#endif
        if ((STRCMP
             (FPAM_ROOT_USER,
              FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName) != 0)
            &&
            ((STRCMP
              (FPAM_GUEST_USER,
               FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName) != 0)
             || (u1FipsEnabled == FPAM_TRUE)))
        {
            CliPrintf (CliHandle,
                       "username %s password xxxxxxxx privilege %d\n\r",
                       FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
                       (FpamFsUsrMgmtEntry.MibObject.
                        u4FsusrMgmtUserPrivilege & FPAM_PRIVILEGE_ID_MASK));
        }

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsusrMgmtTable (&FsusrMgmtUserNameSrc,
                                           &NextFsusrMgmtUserName,
                                           &FsusrMgmtAuthStringSrc,
                                           &NextFsusrMgmtAuthString))
        {
            break;
        }

        Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &NextFsusrMgmtUserName,
                                  &NextFsusrMgmtAuthString);

        FsusrMgmtUserNameSrc.pu1_OctetList =
            NextFsusrMgmtUserName.pu1_OctetList;
        FsusrMgmtUserNameSrc.i4_Length = NextFsusrMgmtUserName.i4_Length;
    }
    FPAM_UNLOCK;
#else
    UNUSED_PARAM (CliHandle);
#endif
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamUtlListUsers
 * Description : To list number of users present in the system
 * Input       : None
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT4
FpamUtlListUsers ()
{
#ifdef CLI_WANTED
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserNameSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStringSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtAuthString;
    UINT1               au1FsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1FsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               au1NextFsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1NextFsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];

    MEMSET (au1FsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (&FsusrMgmtUserNameSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));
    MEMSET (&FsusrMgmtAuthStringSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));
    MEMSET (au1NextFsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (au1NextFsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));

    FsusrMgmtUserNameSrc.pu1_OctetList = au1FsusrMgmtUserName;
    FsusrMgmtAuthStringSrc.pu1_OctetList = au1FsusrMgmtAuthString;
    NextFsusrMgmtAuthString.pu1_OctetList = au1NextFsusrMgmtAuthString;
    NextFsusrMgmtUserName.pu1_OctetList = au1NextFsusrMgmtUserName;

    FPAM_LOCK;
    if (SNMP_SUCCESS != nmhGetFirstIndexFsusrMgmtTable
        (&FsusrMgmtUserNameSrc, &FsusrMgmtAuthStringSrc))
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &FsusrMgmtUserNameSrc,
                              &FsusrMgmtAuthStringSrc);

    while (1)
    {
        if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
        {
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
        mmi_printf ("%-20s     %-20s       %-20d\n\r",
                    FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
                    FPAM_DEF_MODE,
                    ((FpamFsUsrMgmtEntry.MibObject.
                      u4FsusrMgmtUserPrivilege) & FPAM_PRIVILEGE_ID_MASK));

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsusrMgmtTable (&FsusrMgmtUserNameSrc,
                                           &NextFsusrMgmtUserName,
                                           &FsusrMgmtAuthStringSrc,
                                           &NextFsusrMgmtAuthString))
        {
            break;
        }

        Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &NextFsusrMgmtUserName,
                                  &NextFsusrMgmtAuthString);

        FsusrMgmtUserNameSrc.pu1_OctetList =
            NextFsusrMgmtUserName.pu1_OctetList;
        FsusrMgmtUserNameSrc.i4_Length = NextFsusrMgmtUserName.i4_Length;
    }
    FPAM_UNLOCK;
#endif
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamGetReleaseTime
 * Description : To read the release time of the user
 * Input       : pi1UserName
 * Output      : u4RetValFsusrMgmtUserLockRelTime
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamGetReleaseTime (CHR1 * pi1UserName, UINT4 *pu4BlockTime)
{
    tSNMP_OCTET_STRING_TYPE RelFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE RelFsusrMgmtAuthString;

    MEMSET (&RelFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RelFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if ((pi1UserName == NULL) || (pu4BlockTime == NULL))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetReleaseTime :  Release time Entry NULL \r\n"));
        return FPAM_FAILURE;
    }
    RelFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pi1UserName;
    RelFsusrMgmtUserName.i4_Length = STRLEN (pi1UserName);
    RelFsusrMgmtAuthString.pu1_OctetList = (UINT1 *) FPAM_AUTH_STRING;
    RelFsusrMgmtAuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);
    if ((RelFsusrMgmtUserName.i4_Length < FPAM_MIN_USERNAME_LEN) ||
        (RelFsusrMgmtUserName.i4_Length > FPAM_MAX_USERNAME_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetReleaseTime :  Length Check Fails \r\n"));
        return FPAM_FAILURE;
    }
    FPAM_LOCK;
    if (nmhGetFsusrMgmtUserLockRelTime
        (&RelFsusrMgmtUserName, &RelFsusrMgmtAuthString,
         pu4BlockTime) == SNMP_FAILURE)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetReleaseTime :  Release time get failure from "
                   "nmhGetFsusrMgmtUserLockRelTime \r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }
    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamSetReleaseTime
 * Description : To Set new release time to the user.
 * Input       : pi1UserName
 * Output      : u4RetValFsusrMgmtUserLockRelTime
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamSetReleaseTime (CHR1 * pi1UserName, UINT4 u4RetValFsusrMgmtUserLockRelTime)
{
    tSNMP_OCTET_STRING_TYPE RelFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE RelFsusrMgmtAuthString;
    INT4                i4RetValFsusrMgmtUserRowStatus;

    MEMSET (&RelFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&RelFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (pi1UserName == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetReleaseTime :  Release time Entry NULL \r\n"));
        return FPAM_FAILURE;
    }

    RelFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pi1UserName;
    RelFsusrMgmtUserName.i4_Length = STRLEN (pi1UserName);
    RelFsusrMgmtAuthString.pu1_OctetList = (UINT1 *) FPAM_AUTH_STRING;
    RelFsusrMgmtAuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);

    if ((RelFsusrMgmtUserName.i4_Length < FPAM_MIN_USERNAME_LEN) ||
        (RelFsusrMgmtUserName.i4_Length > FPAM_MAX_USERNAME_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetReleaseTime :  Length Check Fails \r\n"));
        return FPAM_FAILURE;
    }
    FPAM_LOCK;
    if (nmhGetFsusrMgmtUserRowStatus
        (&RelFsusrMgmtUserName, &RelFsusrMgmtAuthString,
         &i4RetValFsusrMgmtUserRowStatus) == SNMP_SUCCESS)
    {
        /* Get Rowstatus */
        if (i4RetValFsusrMgmtUserRowStatus == ACTIVE)
        {
            /* Only Active Row can be Modified */
            if (nmhSetFsusrMgmtUserRowStatus
                (&RelFsusrMgmtUserName, &RelFsusrMgmtAuthString,
                 NOT_IN_SERVICE) == SNMP_SUCCESS)
            {
                /* Not in service Set */
                if (nmhSetFsusrMgmtUserLockRelTime
                    (&RelFsusrMgmtUserName, &RelFsusrMgmtAuthString,
                     u4RetValFsusrMgmtUserLockRelTime) == SNMP_SUCCESS)
                {
                    /* Release Time updated */
                    if (nmhSetFsusrMgmtUserRowStatus
                        (&RelFsusrMgmtUserName, &RelFsusrMgmtAuthString,
                         ACTIVE) == SNMP_SUCCESS)
                    {
                        FPAM_TRC ((FPAM_UTIL_TRC,
                                   "FpamSetReleaseTime :  Release time Set "
                                   "Success  \r\n"));
                        FPAM_UNLOCK;
                        return FPAM_SUCCESS;
                    }
                }
            }
        }
    }
    FPAM_TRC ((FPAM_UTIL_TRC,
               "FpamSetReleaseTime :  Release time Set failure  \r\n"));
    FPAM_UNLOCK;
    return FPAM_FAILURE;
}

/********************************************************************
 * Function    : FpamWriteUsersDefault
 * Description : To write a fresh users file in case of files damage.
 * Input       : None
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamWriteUsersDefault ()
{
    tFpamAddUser        RootAddUser;
    tFpamAddUser        GuestAddUser;
    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;
    tFpamFsUsrMgmtEntry *pTempFpamFsUsrMgmtEntry = NULL;

#ifdef MBSM_WANTED
    tFpamAddUser        MbsmAddUser;
#endif

#ifdef FIPS_WANTED
    INT4                i4FipsOperMode = 0;
#endif
    MEMSET (&RootAddUser, 0, sizeof (tFpamAddUser));

    pFpamFsUsrMgmtEntry = FpamGetFirstFsusrMgmtTable ();
    while (pFpamFsUsrMgmtEntry != NULL)
    {
        pTempFpamFsUsrMgmtEntry =
            FpamGetNextFsusrMgmtTable (pFpamFsUsrMgmtEntry);
        FpamUtlDelUser
            ((INT1 *) &((pFpamFsUsrMgmtEntry->MibObject).au1FsusrMgmtUserName));
        pFpamFsUsrMgmtEntry = pTempFpamFsUsrMgmtEntry;
    }

    RootAddUser.pi1NewUsrName = (INT1 *) FPAM_ROOT_USER;
    RootAddUser.pi1NewPasswd = (INT1 *) FPAM_ROOT_PSWD;
    RootAddUser.i4PrivilegeLevel = FPAM_ROOT_PRIVILEGE_ID;
    RootAddUser.i1UserStatus = FPAM_USER_ENABLE;
    RootAddUser.pi1ConfirmPasswd = (INT1 *) FPAM_ROOT_PSWD;

    if (FpamUtlAddUser (&RootAddUser) != FPAM_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamWriteUsersDefault : Default user (root/admin)"
                   " creation failed\r\n"));
        return FPAM_FAILURE;
    }

#ifdef MBSM_WANTED
    MEMSET (&MbsmAddUser, 0, sizeof (tFpamAddUser));

    MbsmAddUser.pi1NewUsrName = (INT1 *) FPAM_MBSM_USER;
    MbsmAddUser.pi1NewPasswd = (INT1 *) FPAM_MBSM_PSWD;
    MbsmAddUser.i4PrivilegeLevel = FPAM_MBSM_PRIVILEGE_ID;
    MbsmAddUser.i1UserStatus = FPAM_USER_ENABLE;
    MbsmAddUser.pi1ConfirmPasswd = (INT1 *) FPAM_MBSM_PSWD;

    if (FpamUtlAddUser (&MbsmAddUser) != FPAM_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamWriteUsersDefault : MBSM Users creation failed\r\n"));
        return FPAM_FAILURE;
    }
#endif

#ifdef FIPS_WANTED
    i4FipsOperMode = FipsGetFipsCurrOperMode ();

    if ((FIPS_MODE == i4FipsOperMode) || (CNSA_MODE == i4FipsOperMode))
    {
        /* In FIPS_MODE,CNSA_MODE we dont need to create Guest user.
         * So skipping creation of guest user. */
        return FPAM_SUCCESS;
    }
#endif

    MEMSET (&GuestAddUser, 0, sizeof (tFpamAddUser));

    GuestAddUser.pi1NewUsrName = (INT1 *) FPAM_GUEST_USER;
    GuestAddUser.pi1NewPasswd = (INT1 *) FPAM_GUEST_PSWD;
    GuestAddUser.i4PrivilegeLevel = FPAM_DEF_FSUSRMGMTUSERPRIVILEGE;
    GuestAddUser.i1UserStatus = FPAM_USER_ENABLE;
    GuestAddUser.pi1ConfirmPasswd = (INT1 *) FPAM_GUEST_PSWD;

    if (FpamUtlAddUser (&GuestAddUser) != FPAM_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamWriteUsersDefault : Guest user creation failed\r\n"));
        return FPAM_FAILURE;
    }

    return FPAM_SUCCESS;
}

/********************************************************************
 * Function    : FpamOpenFile
 * Description : This routine opens a file given as its Argument with the mode
 *               specified in the argument and returns the File descriptor of the
 *               file opened
 * Input       : None
 * Output      : None
 * Returns     : returns fileid
 *****************************************************************************/
INT4
FpamOpenFile (CONST CHR1 * pi1Filename, INT4 i4Flag)
{
    INT4                i4FileFd;

    if (FPAM_USER_FILE_READ_ONLY == i4Flag)
    {
        i4Flag = OSIX_FILE_RO;
    }
    else if (FPAM_USER_FILE_WRITE_ONLY == i4Flag)
    {
        i4Flag = OSIX_FILE_WO | OSIX_FILE_CR | OSIX_FILE_AP;
    }
    if ((i4FileFd = FileOpen ((CONST UINT1 *) pi1Filename, i4Flag)) < 0)
    {
        return -1;
    }
    return i4FileFd;
}

/***************************************************************************
 * Function Name : FpamFindCksum                                        *
 * Description   : This function will checksum for the lastline and    *
 *                 append it into the file                             *
 * Input(s)      : File descriptor, Sum of otherlines and              *
 *                            calculated checksum                      *
 * Output(s)     : NULL                                                *
 * Returns       : SNMP_SUCCESS/SNMP_FAILURE                             *
 ***************************************************************************/
INT1
FpamFindCksum (INT4 i4Fd, INT2 i2Length, UINT4 u4Sum)
{
    UINT2               u2Csum = 0;
    INT1                i1Temp = 0;
    INT1               *pi1Temp = NULL;
    INT2                i2Len = 0;
    INT1                ai1Buf[FPAM_MAX_USERS_LINE_LEN];

    i2Len = 0;
    MEMSET (ai1Buf, 0, FPAM_MAX_USERS_LINE_LEN);
    pi1Temp = ai1Buf;
    sprintf ((char *) pi1Temp, "CKSUM:");
    i2Len = STRLEN (pi1Temp);
    pi1Temp += STRLEN (pi1Temp);

    /* Appending Checksum with the word CKSUM: " */
    MEMCPY (pi1Temp, &u2Csum, sizeof (UINT2));
    i2Len += sizeof (UINT2);
    pi1Temp += sizeof (UINT2);
    i2Length += i2Len;
    /* Checking the length whether it is odd or even */
    if ((i2Length % FPAM_TWO) == 1)
    {
        MEMCPY (pi1Temp, &i1Temp, sizeof (INT1));
        i2Length++;
        i2Len++;
    }
    /* Calculating checksum for the line CkSUM */
    UtilCalcCheckSum ((UINT1 *) ai1Buf, i2Len, &u4Sum, &u2Csum, CKSUM_LASTDATA);
    pi1Temp -= FPAM_TWO;
    MEMCPY (pi1Temp, &u2Csum, sizeof (UINT2));
    pi1Temp = ai1Buf;

    /*** appending the checksum value into the file */
    FileWrite (i4Fd, (CHR1 *) pi1Temp,
               MEM_MAX_BYTES (FPAM_MAX_USERS_LINE_LEN, i2Len));
    return FPAM_SUCCESS;
}

/***************************************************************************
 * Function Name : FpamReadLineFromFile                                *
 * Description   : This function will read a line from the file        *
 * Input(s)      : File id, Buffer, maximum length, Read length              *
 * Output(s)     : NULL                                                *
 * Returns       : SNMP_SUCCESS/SNMP_FAILURE                             *
 ***************************************************************************/
INT1
FpamReadLineFromFile (INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen)
{
    INT1                i1Char;

    *pi2ReadLen = 0;

    /*while (FileRead (i4Fd, &i1Char, 1) == 1) */
    while (FileRead (i4Fd, (CHR1 *) & i1Char, 1) == 1)
    {
        pi1Buf[*pi2ReadLen] = i1Char;
        (*pi2ReadLen)++;
        if (i1Char == '\n')
        {
            return (FPAM_NO_EOF);
        }
        if (*pi2ReadLen == i2MaxLen)
        {
            *pi2ReadLen = 0;
            break;
        }
    }
    pi1Buf[*pi2ReadLen] = '\0';
    return (FPAM_EOF);

}

/***************************************************************************
 * Function Name : FpamVerifyCheckSum                                   *
 * Description   : This function will verify checksum during           *
 *                 read operation                                      *
 * Input(s)      : read line from the file, totallength, line length   *
 *                  calculated sum, checksum                           *
 * Output(s)     : NULL                                                *
 * Returns       : SNMP_SUCCESS/SNMP_FAILURE                             *
 ***************************************************************************/
INT1
FpamVerifyCheckSum (INT1 *pData, INT2 i2TotLen, INT2 i2Length,
                    UINT4 u4Sum, UINT2 u2CkSum)
{
    UINT2               u2CheckSum = 0;
    INT1               *pi1Tmp = NULL;

    /* Calculate checksum for the last line */
    if ((STRNCMP (pData, "CKSUM:", STRLEN ("CKSUM:")) != 0))
    {
        return FPAM_FAILURE;
    }
    i2TotLen += i2Length;
    pi1Tmp = pData;
    pi1Tmp += STRLEN ("CKSUM:");
    MEMCPY (&u2CheckSum, pi1Tmp, sizeof (UINT2));
    MEMSET (pi1Tmp, 0, sizeof (UINT2));
    if ((i2TotLen % FPAM_TWO) == 1)
    {
        i2Length++;
    }
    UtilCalcCheckSum ((UINT1 *) pData, i2Length, &u4Sum,
                      &u2CkSum, CKSUM_LASTDATA);
    if (u2CkSum == u2CheckSum)
    {
        return FPAM_SUCCESS;
    }
    return FPAM_FAILURE;
}

/***************************************************************************
 * Function Name : FpamUtlEncriptPasswd                                      *
 * Description   : This utility is used to encrypt the password              *
 * Input(s)      : Password to be encrypted                                  *
 * Output(s)     : encrypted password                                        *
 * Returns       : SNMP_SUCCESS/SNMP_FAILURE                                 *
 ***************************************************************************/
CHR1               *
FpamUtlEncriptPasswd (CHR1 * i1passwd)
{
    UINT4               u4Len = 0, u4Index = 0;
    u4Len = STRLEN (i1passwd);
    for (u4Index = 0; u4Index < u4Len; u4Index++)
    {
        if (u4Index != u4Len - 1)
        {
            i1passwd[u4Index] ^= i1passwd[u4Index + 1];
        }
        else
        {
            i1passwd[u4Index] ^= FPAM_SPL_CHAR_GREATER_THAN;
        }
        if (i1passwd[u4Index] == '\n')
        {
            i1passwd[u4Index] = FPAM_SPL_CHAR_GREATER_THAN;
        }
        if (i1passwd[u4Index] == '\0')
        {
            i1passwd[u4Index] = FPAM_SPL_CHAR_GREATER_THAN + 1;
        }
    }
    i1passwd[u4Index] = '\0';
    return i1passwd;
}

/***************************************************************************
 * Function Name : FpamUtlDecriptPasswd                                      *
 * Description   : This utility is used to decrypt the password              *
 * Input(s)      : Password to be decrypted                                  *
 * Output(s)     : decrypted password                                        *
 * Returns       : SNMP_SUCCESS/SNMP_FAILURE                                 *
 ***************************************************************************/
CHR1               *
FpamUtlDecriptPasswd (CHR1 * i1Passwd)
{
    UINT4               u4Len = 0;
    INT4                i4Index = 0;
    CHR1                i1PrevChar = FPAM_SPL_CHAR_GREATER_THAN;

    u4Len = STRLEN (i1Passwd);
    for (i4Index = u4Len - 1; i4Index >= 0; i4Index--)
    {
        if (i1Passwd[i4Index] == FPAM_SPL_CHAR_GREATER_THAN)
        {
            i1Passwd[i4Index] = '\n';
        }
        else if (i1Passwd[i4Index] == (FPAM_SPL_CHAR_GREATER_THAN + 1))
        {
            i1Passwd[i4Index] = '\0';
        }
        i1Passwd[i4Index] ^= i1PrevChar;
        i1PrevChar = i1Passwd[i4Index];
    }
    return i1Passwd;
}

/*******************************************************************************
 * Function Name : Fpam_copy_index_to_entry                                    *
 * Description   : This function is to copy the string values into             *
 *                 user management entry                                       *
 * Input(s)      : FpamFsUsrMgmtEntry, pFsusrMgmtUserName, pFsusrMgmtAuthString*
 * Output(s)     : NULL                                                        *
 * Returns       : SNMP_SUCCESS/SNMP_FAILURE                                   *
 ******************************************************************************/
INT4
Fpam_copy_index_to_entry (tFpamFsUsrMgmtEntry * pFpamFsUsrMgmtEntry,
                          tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName,
                          tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString)
{
    if ((pFsusrMgmtUserName == NULL) || (pFsusrMgmtAuthString == NULL)
        || (pFpamFsUsrMgmtEntry == NULL))
    {
        return FPAM_FAILURE;
    }
    MEMSET (pFpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName,
            pFsusrMgmtUserName->pu1_OctetList, pFsusrMgmtUserName->i4_Length);
    pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen =
        pFsusrMgmtUserName->i4_Length;
    MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString,
            pFsusrMgmtAuthString->pu1_OctetList,
            pFsusrMgmtAuthString->i4_Length);
    pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen =
        pFsusrMgmtAuthString->i4_Length;
    return FPAM_SUCCESS;

}

/*******************************************************************************
 * Function Name : FpamUserCompareOldPasswd                                    *
 * Description   : This function is to check the difference between new        *
 *                 password and old password                                   *
 * Input(s)      : pCheckFsusrMgmtUserName      : Corresponding username       *
 *                 pCheckFsusrMgmtUserPassword  : new password                 *
 * Output(s)     : NULL                                                        *
 * Returns       : FPAM_SUCCESS/FPAM_FAILURE                                   *
 ******************************************************************************/
INT4
FpamUserCompareOldPasswd (tSNMP_OCTET_STRING_TYPE * pUserName,
                          tSNMP_OCTET_STRING_TYPE * pNewPasswd)
{
    UINT4               u4Index = 0;
    UINT4               u4Index2 = 0;
    UINT4               u4MatchFlag = 0;
    UINT4               u4NewSymbolCount = 0;
    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;
    UINT1               au1OldPasswd[FPAM_MAX_PASSWD_LEN + 1];
    MEMSET (au1OldPasswd, 0, FPAM_MAX_PASSWD_LEN + 1);

    pFpamFsUsrMgmtEntry = FpamGetFirstFsusrMgmtTable ();
    while (pFpamFsUsrMgmtEntry != NULL)
    {
        if (STRCMP (pUserName->pu1_OctetList,
                    pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName)
            == FPAM_SUCCESS)
        {
            STRCPY (au1OldPasswd,
                    pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword);
            break;
        }
        pFpamFsUsrMgmtEntry = FpamGetNextFsusrMgmtTable (pFpamFsUsrMgmtEntry);
    }

    FpamUtlDecriptPasswd ((CHR1 *) au1OldPasswd);
    for (u4Index = 0; u4Index < (UINT4) pNewPasswd->i4_Length; u4Index++)
    {
        u4MatchFlag = 0;
        for (u4Index2 = 0; au1OldPasswd[u4Index2] != '\0'; u4Index2++)
        {
            if (pNewPasswd->pu1_OctetList[u4Index] == au1OldPasswd[u4Index2])
            {
                u4MatchFlag = 1;
                break;
            }
        }

        if (u4MatchFlag != 1)
        {
            u4NewSymbolCount++;
            if (u4NewSymbolCount >= FPAM_MIN_DIFF_FROM_OLD_PWD)
            {
                return FPAM_SUCCESS;
            }
        }
    }
    return FPAM_FAILURE;
}

/*******************************************************************************
 * Function Name : FpamUserPasswdConfCheck                                     *
 * Description   : This function is to check the configuration rules of        *
 *                 password trying to get set.                                 *
 * Input(s)      : pCheckFsusrMgmtUserName      : corresponding username       *
 *                 pCheckFsusrMgmtUserPassword  : new password                 *
 * Output(s)     : NULL                                                        *
 * Returns       : SNMP_SUCCESS/SNMP_FAILURE                                   *
 ******************************************************************************/

INT4
FpamUserPasswdConfCheck (tSNMP_OCTET_STRING_TYPE * pCheckFsusrMgmtUserName,
                         tSNMP_OCTET_STRING_TYPE * pCheckFsusrMgmtUserPassword)
{

    UINT4               u4PasswdLen = 0;
    UINT4               u4UpperCaseCount = 0;
    UINT4               u4LowerCaseCount = 0;
    UINT4               u4NumberCount = 0;
    UINT4               u4SplCharCount = 0;
    UINT4               u4Index = 0;
    UINT4               u4SymbolIndex = 0;
    UINT4               u4PasswordValidationCharMask = 0x0;

    if (pCheckFsusrMgmtUserPassword == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUserPasswdConfCheck: Password Entry NULL.\r\n"));
        return FPAM_FAILURE;
    }
    u4PasswdLen = pCheckFsusrMgmtUserPassword->i4_Length;
    if ((u4PasswdLen > FPAM_MAX_PASSWD_LEN) ||
        (u4PasswdLen <
         (UINT1) gFpamGlobals.FpamGlbMib.u4FsusrMgmtMinPasswordLen))
    {
        /* Password Length CHECK */
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUserPasswdConfCheck: Length check failure.\r\n"));
#ifdef CLI_WANTED
        mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                    "\r%% Password length should be in the range of %d - %d !! "
                    "characters\r\n",
                    gFpamGlobals.FpamGlbMib.u4FsusrMgmtMinPasswordLen,
                    FPAM_MAX_PASSWD_LEN);
        PRINT_PASSWORD_RULES ();

#endif
        return FPAM_FAILURE;
    }
    for (u4Index = 0; u4Index < u4PasswdLen; u4Index++)
    {
        if ((pCheckFsusrMgmtUserPassword->pu1_OctetList[u4Index] >=
             FPAM_UPPER_CHAR_START)
            && (pCheckFsusrMgmtUserPassword->pu1_OctetList[u4Index] <=
                FPAM_UPPER_CHAR_END))
        {
            u4UpperCaseCount += 1;
        }
        else if ((pCheckFsusrMgmtUserPassword->pu1_OctetList[u4Index] >=
                  FPAM_LOWER_CHAR_START)
                 && (pCheckFsusrMgmtUserPassword->pu1_OctetList[u4Index] <=
                     FPAM_LOWER_CHAR_END))
        {
            u4LowerCaseCount += 1;
        }
        else if ((pCheckFsusrMgmtUserPassword->pu1_OctetList[u4Index] >=
                  FPAM_NUM_CHAR_START)
                 && (pCheckFsusrMgmtUserPassword->pu1_OctetList[u4Index] <=
                     FPAM_NUM_CHAR_END))
        {
            u4NumberCount += 1;
        }
        else
        {
            for (u4SymbolIndex = 0;
                 FPAM_ALLOWED_SPL_CHAR_LIST[u4SymbolIndex] != '\0';
                 u4SymbolIndex++)
            {
                if (pCheckFsusrMgmtUserPassword->pu1_OctetList[u4Index] ==
                    FPAM_ALLOWED_SPL_CHAR_LIST[u4SymbolIndex])
                {
                    u4SplCharCount += 1;
                    break;
                }
            }
        }
    }

    u4PasswordValidationCharMask = FPAM_PASSWORD_VALIDATE_MASK;
    if ((u4PasswordValidationCharMask & FPAM_LOWERCASE_MASK) ==
        FPAM_LOWERCASE_MASK)
    {
        if (u4LowerCaseCount < FPAM_MIN_LOWERCASE_COUNT)
        {
#ifdef CLI_WANTED
            mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                        "\r%% Password should contain atleast %d lowercase "
                        "characters !!\r\n", FPAM_MIN_LOWERCASE_COUNT);
            PRINT_PASSWORD_RULES ();
#endif
            return FPAM_FAILURE;
        }
    }
    else
    {
        if (u4LowerCaseCount != 0)
        {
#ifdef CLI_WANTED
            mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                        "\r%% Passwords with lowercase characters are disabled "
                        "by the administrator !!\r\n");
            PRINT_PASSWORD_RULES ();
#endif
            return FPAM_FAILURE;
        }
    }

    if ((u4PasswordValidationCharMask & FPAM_UPPERCASE_MASK) ==
        FPAM_UPPERCASE_MASK)
    {
        if (u4UpperCaseCount < FPAM_MIN_UPPERCASE_COUNT)
        {
#ifdef CLI_WANTED
            mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                        "\r%% Password should contain atleast %d uppercase "
                        "characters !!\r\n", FPAM_MIN_UPPERCASE_COUNT);

            PRINT_PASSWORD_RULES ();
#endif
            return FPAM_FAILURE;
        }
    }
    else
    {
        if (u4UpperCaseCount != 0)
        {
#ifdef CLI_WANTED
            mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                        "\r%% Passwords with uppercase characters are disabled "
                        "by the administrator !!\r\n");

            PRINT_PASSWORD_RULES ();
#endif
            return FPAM_FAILURE;
        }
    }

    if ((u4PasswordValidationCharMask & FPAM_NUMBER_MASK) == FPAM_NUMBER_MASK)
    {
        if (u4NumberCount < FPAM_MIN_NUMERICALS_COUNT)
        {
#ifdef CLI_WANTED
            mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                        "\r%% Password should contain atleast %d numerical "
                        "characters !!\r\n", FPAM_MIN_NUMERICALS_COUNT);
            PRINT_PASSWORD_RULES ();
#endif
            return FPAM_FAILURE;
        }
    }
    else
    {
        if (u4NumberCount != 0)
        {
#ifdef CLI_WANTED
            mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                        "\r%% Passwords with numeric characters are disabled"
                        " by the administrator !!\r\n");
            PRINT_PASSWORD_RULES ();
#endif
            return FPAM_FAILURE;
        }
    }

    if ((u4PasswordValidationCharMask & FPAM_SYMBOL_MASK) == FPAM_SYMBOL_MASK)
    {
        if (u4SplCharCount < FPAM_MIN_SPL_CHARS_COUNT)
        {
#ifdef CLI_WANTED
            mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                        "\r%% Password should contain atleast %d special "
                        "characters !!\r\n", FPAM_MIN_SPL_CHARS_COUNT);
            PRINT_PASSWORD_RULES ();
#endif
            return FPAM_FAILURE;
        }
    }
    else
    {
        if (u4SplCharCount != 0)
        {
#ifdef CLI_WANTED
            mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                        "\r%% Passwords with symbols are disabled are disabled"
                        " by the administrator !!\r\n");
            PRINT_PASSWORD_RULES ();
#endif
            return FPAM_FAILURE;
        }
    }

    if (FpamUserCompareOldPasswd
        (pCheckFsusrMgmtUserName, pCheckFsusrMgmtUserPassword) == FPAM_FAILURE)
    {
#ifdef CLI_WANTED
        mmi_printf ("\r%% Password doesnt comply with password rules.\r\n"
                    "\r%% New Password should contain atleast %d characters "
                    "different from old password !!\r\n",
                    FPAM_MIN_DIFF_FROM_OLD_PWD);
        PRINT_PASSWORD_RULES ();
#endif
        return FPAM_FAILURE;
    }

    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamUtlSearchRootPrivilege
 * Description : To search user available with privilege of ROOT user
 * Input       : None
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT4
FpamUtlSearchRootPrivilege (tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserNameSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStringSrc;
    tSNMP_OCTET_STRING_TYPE NextFsusrMgmtAuthString;
    UINT1               au1FsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1FsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               au1NextFsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1NextFsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];

    MEMSET (au1FsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (&FsusrMgmtUserNameSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));
    MEMSET (&FsusrMgmtAuthStringSrc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1NextFsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (au1NextFsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));

    FsusrMgmtUserNameSrc.pu1_OctetList = au1FsusrMgmtUserName;
    FsusrMgmtAuthStringSrc.pu1_OctetList = au1FsusrMgmtAuthString;
    NextFsusrMgmtAuthString.pu1_OctetList = au1NextFsusrMgmtAuthString;
    NextFsusrMgmtUserName.pu1_OctetList = au1NextFsusrMgmtUserName;
    FPAM_LOCK;
    if (SNMP_SUCCESS != nmhGetFirstIndexFsusrMgmtTable
        (&FsusrMgmtUserNameSrc, &FsusrMgmtAuthStringSrc))
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &FsusrMgmtUserNameSrc,
                              &FsusrMgmtAuthStringSrc);

    while (1)
    {
        if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
        {
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
        if (((FpamFsUsrMgmtEntry.MibObject.
              u4FsusrMgmtUserPrivilege & FPAM_PRIVILEGE_ID_MASK) ==
             FPAM_ROOT_PRIVILEGE_ID)
            &&
            (STRCMP
             (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
              pFsusrMgmtUserName->pu1_OctetList) != 0)
            && (FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserStatus ==
                FPAM_USER_ENABLE))

        {
            FPAM_UNLOCK;
            return FPAM_SUCCESS;
        }

        if (SNMP_SUCCESS !=
            nmhGetNextIndexFsusrMgmtTable (&FsusrMgmtUserNameSrc,
                                           &NextFsusrMgmtUserName,
                                           &FsusrMgmtAuthStringSrc,
                                           &NextFsusrMgmtAuthString))
        {
            break;
        }

        Fpam_copy_index_to_entry (&FpamFsUsrMgmtEntry, &NextFsusrMgmtUserName,
                                  &NextFsusrMgmtAuthString);

        FsusrMgmtUserNameSrc.pu1_OctetList =
            NextFsusrMgmtUserName.pu1_OctetList;
        FsusrMgmtUserNameSrc.i4_Length = NextFsusrMgmtUserName.i4_Length;
    }
    FPAM_UNLOCK;
    return FPAM_FAILURE;
}

/****************************************************************************
* Function    :  FpamUtlGetNumOfUsers
* Input       :  The Indices
* Input       :  pu4FsusrMgmtStatsNumOfUsers
* Descritpion :  This Routine will Get
* the Value accordingly.
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/
INT4
FpamUtlGetNumOfUsers (UINT4 *pu4FsusrMgmtStatsNumOfUsers)
{
    if (pu4FsusrMgmtStatsNumOfUsers == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlGetNumOfUsers: Password Entry NULL.\r\n"));
        return FPAM_FAILURE;
    }
    FPAM_LOCK;
    if (nmhGetFsusrMgmtStatsNumOfUsers (pu4FsusrMgmtStatsNumOfUsers) ==
        SNMP_FAILURE)
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }
    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}

/****************************************************************************
* Function    :  FpamUtlGetUserMode
* Input       :  The Indices
* Descritpion :  This Routine will Get
                  the Value accordingly.
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
*****************************************************************************/
INT4
FpamUtlGetUserMode (CONST CHR1 * pi1UserName, INT1 *pi1UserMode)
{
    tSNMP_OCTET_STRING_TYPE SearchFsusrMgmtUserName;
    UINT1               au1UserName[FPAM_MAX_USERNAME_LEN];

    MEMSET (&SearchFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1UserName, 0, (FPAM_MAX_USERNAME_LEN));

    if ((pi1UserName == NULL) || (pi1UserMode == NULL))
    {
        FPAM_TRC ((FPAM_UTIL_TRC, "FpamUtlGetUserMode:  Entry NULL.\r\n"));
        return FPAM_FAILURE;
    }

    SearchFsusrMgmtUserName.pu1_OctetList = au1UserName;
    MEMCPY (SearchFsusrMgmtUserName.pu1_OctetList, pi1UserName,
            STRLEN (pi1UserName));
    SearchFsusrMgmtUserName.i4_Length = STRLEN (pi1UserName);

    if ((SearchFsusrMgmtUserName.i4_Length < FPAM_MIN_USERNAME_LEN) ||
        (SearchFsusrMgmtUserName.i4_Length > FPAM_MAX_USERNAME_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlGetUserMode :  Length Check Fails \r\n"));
        return FPAM_FAILURE;
    }

    if (FpamUtlSearchUserName (&SearchFsusrMgmtUserName) != FPAM_SUCCESS)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlGetUserMode : No Such user present.\r\n"));
        return FPAM_FAILURE;
    }
    STRCPY (pi1UserMode, (INT1 *) FPAM_DEF_MODE);
    return FPAM_SUCCESS;
}

/**************************************************************************
 * Function    : FpamGetUserStatus
 * Description : To read the release time of the user
 * Input       : pi1UserName
 * Output      : UserStatus
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT1
FpamGetUserStatus (CHR1 * pi1UserName, INT4 *pi4UserStatus)
{
    tSNMP_OCTET_STRING_TYPE UserStatFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE UserStatFsusrMgmtAuthString;

    MEMSET (&UserStatFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserStatFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if ((pi1UserName == NULL) || (pi4UserStatus == NULL))
    {
        FPAM_TRC ((FPAM_UTIL_TRC, "FpamGetUserStatus:  Entry NULL.\r\n"));
        return FPAM_FAILURE;
    }

    UserStatFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pi1UserName;
    UserStatFsusrMgmtUserName.i4_Length = STRLEN (pi1UserName);
    UserStatFsusrMgmtAuthString.pu1_OctetList = (UINT1 *) FPAM_AUTH_STRING;
    UserStatFsusrMgmtAuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);

    if ((UserStatFsusrMgmtUserName.i4_Length < FPAM_MIN_USERNAME_LEN) ||
        (UserStatFsusrMgmtUserName.i4_Length > FPAM_MAX_USERNAME_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlGetUserMode :  Length Check Fails \r\n"));
        return FPAM_FAILURE;
    }

    FPAM_LOCK;
    if (nmhGetFsusrMgmtUserStatus
        (&UserStatFsusrMgmtUserName, &UserStatFsusrMgmtAuthString,
         pi4UserStatus) == SNMP_FAILURE)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetUserStatus :  User Status get failure from "
                   "nmhGetFsusrMgmtUserStatus \r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }
    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}

/****************************************************************************
* Function    :  FpamUtlUpdateActiveUsers
* Input       :  The Indices
* Input       :  Login/Logout
* Descritpion :  This Routine will Get
*                the Value accordingly.
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/
INT4
FpamUtlUpdateActiveUsers (CHR1 * pi1UserName, UINT4 u4LoginLogoutDetail)
{

    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;
    tSNMP_OCTET_STRING_TYPE UserStatFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE UserStatFsusrMgmtAuthString;

    MEMSET (&UserStatFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&UserStatFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    if (pi1UserName == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlUpdateActiveUsers:  Entry NULL.\r\n"));
        return FPAM_FAILURE;
    }

    UserStatFsusrMgmtUserName.pu1_OctetList = (UINT1 *) pi1UserName;
    UserStatFsusrMgmtUserName.i4_Length = STRLEN (pi1UserName);
    UserStatFsusrMgmtAuthString.pu1_OctetList = (UINT1 *) FPAM_AUTH_STRING;
    UserStatFsusrMgmtAuthString.i4_Length = STRLEN (FPAM_AUTH_STRING);

    if ((UserStatFsusrMgmtUserName.i4_Length < FPAM_MIN_USERNAME_LEN) ||
        (UserStatFsusrMgmtUserName.i4_Length > FPAM_MAX_USERNAME_LEN))
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlGetUserMode :  Length Check Fails \r\n"));
        return FPAM_FAILURE;
    }

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    /* Assign the index */
    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            UserStatFsusrMgmtUserName.pu1_OctetList,
            UserStatFsusrMgmtUserName.i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen =
        UserStatFsusrMgmtUserName.i4_Length;

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            UserStatFsusrMgmtAuthString.pu1_OctetList,
            UserStatFsusrMgmtAuthString.i4_Length);

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        UserStatFsusrMgmtAuthString.i4_Length;

    /* Check whether the node is already present */
    pFpamFsUsrMgmtEntry =
        RBTreeGet (gFpamGlobals.FpamGlbMib.FsusrMgmtTable,
                   (tRBElem *) (&FpamFsUsrMgmtEntry));

    if (pFpamFsUsrMgmtEntry == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamGetAllFsusrMgmtTable: Entry doesn't exist\r\n"));
        return FPAM_FAILURE;
    }

    if (u4LoginLogoutDetail == FPAM_USER_LOGGED_IN)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlUpdateActiveUsers : Active user count "
                   "needs to be incremented on Login of user \r\n"));
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsActiveUsers++;
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserLoginCount++;
    }
    else
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamUtlUpdateActiveUsers : Active user count "
                   "needs to be decremented on Logout of user \r\n"));
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsActiveUsers--;
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserLoginCount--;
    }
    return FPAM_SUCCESS;
}

/****************************************************************************
* Function    :  FpamUtlMbsmUserUpdate
* Input       :  Void
* Descritpion :  This Routine will add
*                MBSM user if required.
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/
INT4
FpamUtlMbsmUserUpdate ()
{
    tFpamAddUser        MbsmAddUser;
    tSNMP_OCTET_STRING_TYPE MbsmFsusrMgmtUserName;
    UINT1               au1UserName[FPAM_MAX_USERNAME_LEN];

    MEMSET (&MbsmAddUser, 0, sizeof (tFpamAddUser));
    MbsmAddUser.pi1NewUsrName = (INT1 *) FPAM_MBSM_USER;
    MbsmAddUser.pi1NewPasswd = (INT1 *) FPAM_MBSM_PSWD;
    MbsmAddUser.i4PrivilegeLevel = FPAM_MBSM_PRIVILEGE_ID;
    MbsmAddUser.i1UserStatus = FPAM_USER_ENABLE;
    MEMSET (&MbsmFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1UserName, 0, (FPAM_MAX_USERNAME_LEN));

    MbsmFsusrMgmtUserName.pu1_OctetList = au1UserName;
    MEMCPY (MbsmFsusrMgmtUserName.pu1_OctetList, MbsmAddUser.pi1NewUsrName,
            STRLEN (MbsmAddUser.pi1NewUsrName));
    MbsmFsusrMgmtUserName.i4_Length = STRLEN (MbsmAddUser.pi1NewUsrName);
    MbsmAddUser.pi1ConfirmPasswd = (INT1 *) FPAM_MBSM_PSWD;

    if (FpamUtlSearchUserName (&MbsmFsusrMgmtUserName) != FPAM_SUCCESS)
    {
        /* Flag is set to skip password validation of chassis user */
        gb1FpamDefUserInit = OSIX_TRUE;
        if (FpamUtlAddUser (&MbsmAddUser) == FPAM_SUCCESS)
        {
            gb1FpamDefUserInit = OSIX_FALSE;
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlMbsmUserUpdate : MBSM user "
                       "added to existing users file \r\n"));
            return FPAM_SUCCESS;
        }
        else
        {
            gb1FpamDefUserInit = OSIX_FALSE;
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamUtlMbsmUserUpdate : MBSM user "
                       "addtion to existing user file failed \r\n"));
            return FPAM_FAILURE;
        }
    }
    FPAM_TRC ((FPAM_UTIL_TRC,
               "FpamUtlMbsmUserUpdate : Chassis user already present \r\n"));
    return FPAM_SUCCESS;
}

/********************************************************************
 * Function    : FpamUtlDelAllUsers
 * Input       : None
 * Description : To Delete all the  existing users. Required while switching to 
 *                  FIPS mode.
 * Output      : None
 * Returns     : FPAM_SUCCESS/FPAM_FAILURE
 *****************************************************************************/
INT4
FpamUtlDelAllUsers ()
{
    tSNMP_OCTET_STRING_TYPE DelFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE DelFsusrMgmtAuthString;
    tSNMP_OCTET_STRING_TYPE DelNextFsusrMgmtUserName;
    tSNMP_OCTET_STRING_TYPE DelNextFsusrMgmtAuthString;
    UINT1               au1FsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               au1NextFsusrMgmtUserName[FPAM_MAX_USERNAME_LEN];
    UINT1               au1FsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    UINT1               au1NextFsusrMgmtAuthString[STRLEN (FPAM_AUTH_STRING)];
    INT4                i4FipsOperMode = 0;

    MEMSET (&DelFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DelFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DelNextFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&DelNextFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1FsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (au1FsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));
    MEMSET (au1NextFsusrMgmtUserName, 0, FPAM_MAX_USERNAME_LEN);
    MEMSET (au1NextFsusrMgmtAuthString, 0, STRLEN (FPAM_AUTH_STRING));

    DelFsusrMgmtUserName.pu1_OctetList = au1FsusrMgmtUserName;
    DelFsusrMgmtAuthString.pu1_OctetList = au1FsusrMgmtAuthString;
    DelNextFsusrMgmtUserName.pu1_OctetList = au1NextFsusrMgmtAuthString;
    DelNextFsusrMgmtAuthString.pu1_OctetList = au1NextFsusrMgmtUserName;

    UNUSED_PARAM (i4FipsOperMode);
    FPAM_LOCK;
    if (SNMP_SUCCESS != nmhGetFirstIndexFsusrMgmtTable
        (&DelFsusrMgmtUserName, &DelFsusrMgmtAuthString))
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    while (1)
    {
        /* Only Active Row can be deleted */
        if (nmhSetFsusrMgmtUserRowStatus
            (&DelFsusrMgmtUserName, &DelFsusrMgmtAuthString,
             DESTROY) != SNMP_SUCCESS)
        {

            /* Destroy Set */ FPAM_TRC ((FPAM_UTIL_TRC,
                                         "FpamUtlDelAllUsers: Deletion of user Failed\r\n"));
            FPAM_UNLOCK;
            return FPAM_FAILURE;
        }
        MEMSET (&DelFsusrMgmtUserName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (&DelFsusrMgmtAuthString, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        if (nmhGetNextIndexFsusrMgmtTable
            (&DelFsusrMgmtUserName, &DelNextFsusrMgmtUserName,
             &DelFsusrMgmtAuthString,
             &DelNextFsusrMgmtAuthString) == SNMP_FAILURE)
        {
            break;
        }

        DelFsusrMgmtUserName.pu1_OctetList =
            DelNextFsusrMgmtUserName.pu1_OctetList;
        DelFsusrMgmtUserName.i4_Length = DelNextFsusrMgmtUserName.i4_Length;
        DelFsusrMgmtAuthString.pu1_OctetList =
            DelNextFsusrMgmtAuthString.pu1_OctetList;
        DelFsusrMgmtAuthString.i4_Length = DelNextFsusrMgmtAuthString.i4_Length;
    }
    FpamWriteUsers ();

    FPAM_TRC ((FPAM_UTIL_TRC,
               "FpamUtlDelAllUsers: Deletion of All Users Successful\r\n"));
    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}

/****************************************************************************
* Function    :  FpamIsSetUserMgmtFeature
* Input       :  None
* Descritpion :  This Routine will return the global value which contains
*                what are all the features are enabled
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/
UINT4
FpamIsSetUserMgmtFeature (UINT4 u4CheckUserMgmtFeature)
{
    if ((gu4FpamFeatureAddn & u4CheckUserMgmtFeature) != 0)
    {
        return FPAM_SUCCESS;
    }
    return FPAM_FAILURE;
}

/****************************************************************************
* Function    :  FpamSetUserMgmtFeatureAddn
* Input       :  None
* Descritpion :  This Routine will set the value for the global value 
*                which contains what are all the features are enabled
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/
UINT4
FpamSetUserMgmtFeature (UINT4 u4SetUserMgmtFeature)
{
    gu4FpamFeatureAddn = u4SetUserMgmtFeature;
    return FPAM_SUCCESS;
}

/****************************************************************************
* Function    :  FpamTestPasswd
* Input       :  tFpamFsUsrMgmtEntry
*                bConfirmPwd
* Descritpion :  This Routine will do the password validation
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/
INT4
FpamTestPasswd (tFpamFsUsrMgmtEntry * pFpamSetFsUsrMgmtEntry, BOOL1 bConfirmPwd)
{
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStr;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUsrName;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserPasswd;
    INT4                i4UsrMgmtUserLoginCount = 0;
    INT4                i4RetValFsusrMgmtUserStatus = 0;
    INT4                i4RetValCheckAuthString = FPAM_FAILURE;
    UINT4               u4PasswdLen = 0;
    UINT1               au1UserPasswd[FPAM_MAX_PASSWD_LEN + 1];

    MEMSET (&FsusrMgmtUsrName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FsusrMgmtAuthStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1UserPasswd, 0, FPAM_MAX_PASSWD_LEN + 1);

    FsusrMgmtUsrName.pu1_OctetList =
        pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName;
    FsusrMgmtUsrName.i4_Length =
        pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen;
    FsusrMgmtAuthStr.pu1_OctetList =
        pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString;
    FsusrMgmtAuthStr.i4_Length =
        pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen;

    FsusrMgmtUserPasswd.pu1_OctetList = au1UserPasswd;
    FsusrMgmtUserPasswd.i4_Length = 0;
    if (OSIX_FALSE == bConfirmPwd)
    {
        FsusrMgmtUserPasswd.pu1_OctetList =
            pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword;
        FsusrMgmtUserPasswd.i4_Length =
            pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen;
    }
    else
    {
        FsusrMgmtUserPasswd.pu1_OctetList =
            pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserConfirmPwd;
        FsusrMgmtUserPasswd.i4_Length =
            pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen;
    }

    if (FPAM_SUCCESS !=
        FpamIsSetUserMgmtFeature (FPAM_CHECK_EDITPASSWORD_ROOT_USER))
    {
        /* To avoid Root User Updation via snmp */
        if (STRCMP (FsusrMgmtUsrName.pu1_OctetList, FPAM_ROOT_USER) == 0)
        {
            return FPAM_FAILURE;
        }
    }

    i4RetValCheckAuthString =
        FpamUtlChkForFsusrMgmtAuthString (&FsusrMgmtAuthStr, OSIX_TRUE);
    if (FPAM_SUCCESS !=
        FpamIsSetUserMgmtFeature (FPAM_CHECK_ACTIVE_USER_TO_EDIT_PSWD))
    {
        if (i4RetValCheckAuthString == FPAM_FAILURE)
        {
            return FPAM_FAILURE;
        }
        if (nmhGetFsusrMgmtUserLoginCount (&FsusrMgmtUsrName,
                                           &FsusrMgmtAuthStr,
                                           &i4UsrMgmtUserLoginCount) ==
            SNMP_SUCCESS)
        {
            if (i4UsrMgmtUserLoginCount > 0)
            {
                return FPAM_FAILURE;
            }
        }
    }
    else
    {
        if (STRNCMP
            (FsusrMgmtUsrName.pu1_OctetList, FsusrMgmtAuthStr.pu1_OctetList,
             FsusrMgmtUsrName.i4_Length) == 0)
        {
            /* if the authentication string is self user, 
             * no need to check for privilege */
            if (FpamUtlChkForFsusrMgmtAuthString
                (&FsusrMgmtAuthStr, OSIX_FALSE) == FPAM_FAILURE)
            {
                return FPAM_FAILURE;
            }
        }
        else
        {
            if (i4RetValCheckAuthString == FPAM_FAILURE)
            {
                return FPAM_FAILURE;
            }
        }
    }
    if (FPAM_SUCCESS ==
        FpamIsSetUserMgmtFeature (FPAM_CHECK_DISABLE_USER_EDIT_PSWD))
    {
        if (nmhGetFsusrMgmtUserStatus
            (&FsusrMgmtUsrName, &FsusrMgmtAuthStr,
             &i4RetValFsusrMgmtUserStatus) == SNMP_SUCCESS)
        {
            if (FPAM_USER_DISABLE == i4RetValFsusrMgmtUserStatus)
            {
                return FPAM_FAILURE;
            }
        }
    }
    if (OSIX_TRUE == bConfirmPwd)
    {
        u4PasswdLen = FsusrMgmtUserPasswd.i4_Length;
        if ((u4PasswdLen > FPAM_MAX_PASSWD_LEN) ||
            (u4PasswdLen <
             (UINT1) gFpamGlobals.FpamGlbMib.u4FsusrMgmtMinPasswordLen))
        {
            /* Password Length CHECK */
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamTestPasswd: Confirm Password "
                       "Length check failure.\r\n"));
            return FPAM_FAILURE;
        }
    }
    else
    {
        if (FpamUserPasswdConfCheck (&FsusrMgmtUsrName, &FsusrMgmtUserPasswd) ==
            FPAM_FAILURE)
        {
            return FPAM_FAILURE;
        }
    }
    return FPAM_SUCCESS;
}

/****************************************************************************
* Function    :  FpamTestRowStatus
* Input       :  None
* Descritpion :  This Routine will do the RowStatus validation
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/
INT4
FpamTestRowStatus (tFpamFsUsrMgmtEntry * pFpamSetFsUsrMgmtEntry)
{
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUsrName;
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStr;
    tSNMP_OCTET_STRING_TYPE FsTestusrMgmtUserPassword;
    tSNMP_OCTET_STRING_TYPE FsTestusrMgmtUserConfirmPwd;
    INT4                i4RetValCheckAuthString = FPAM_FAILURE;
    INT4                i4UsrMgmtUserLoginCount = 0;
    INT4                i4RetValGetPswd = SNMP_FAILURE;
    INT4                i4RetValGetConfirmPswd = SNMP_FAILURE;
    UINT1               au1Password[FPAM_MAX_PASSWD_LEN + 1];
    UINT1               au1ConfirmPassword[FPAM_MAX_PASSWD_LEN + 1];

    MEMSET (&FsusrMgmtUsrName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FsusrMgmtAuthStr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FsTestusrMgmtUserPassword, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&FsTestusrMgmtUserConfirmPwd, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1Password, 0, (FPAM_MAX_PASSWD_LEN + 1));
    MEMSET (au1ConfirmPassword, 0, (FPAM_MAX_PASSWD_LEN + 1));

    FsusrMgmtUsrName.pu1_OctetList =
        pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName;
    FsusrMgmtUsrName.i4_Length =
        pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen;
    FsusrMgmtAuthStr.pu1_OctetList =
        pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString;
    FsusrMgmtAuthStr.i4_Length =
        pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen;
    FsTestusrMgmtUserPassword.pu1_OctetList = au1Password;
    FsTestusrMgmtUserConfirmPwd.pu1_OctetList = au1ConfirmPassword;

    /* To avoid Active User deletion via snmp */
    if ((pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus == DESTROY))
    {
        if (nmhGetFsusrMgmtUserLoginCount (&FsusrMgmtUsrName,
                                           &FsusrMgmtAuthStr,
                                           &i4UsrMgmtUserLoginCount) ==
            SNMP_SUCCESS)
        {
            if (i4UsrMgmtUserLoginCount > 0)
            {
                return FPAM_FAILURE;
            }
        }
        /* To avoid Root User Deletion via snmp */
        if (STRCMP (FsusrMgmtUsrName.pu1_OctetList, FPAM_ROOT_USER) == 0)
        {
            return FPAM_FAILURE;
        }
        if (FpamUtlSearchRootPrivilege (&FsusrMgmtUsrName) != FPAM_SUCCESS)
        {
            return FPAM_FAILURE;
        }
    }
    else
    {
        i4RetValCheckAuthString =
            FpamUtlChkForFsusrMgmtAuthString (&FsusrMgmtAuthStr, OSIX_TRUE);
        if (FPAM_SUCCESS !=
            FpamIsSetUserMgmtFeature (FPAM_CHECK_ACTIVE_USER_TO_EDIT_PSWD))
        {
            if (i4RetValCheckAuthString == FPAM_FAILURE)
            {
                return FPAM_FAILURE;
            }
        }
        else
        {
            if (STRNCMP
                (FsusrMgmtUsrName.pu1_OctetList, FsusrMgmtAuthStr.pu1_OctetList,
                 FsusrMgmtUsrName.i4_Length) == 0)
            {
                /* if the authentication string is self user, 
                 * no need to check for privilege */
                if (FpamUtlChkForFsusrMgmtAuthString
                    (&FsusrMgmtAuthStr, OSIX_FALSE) == FPAM_FAILURE)
                {
                    return FPAM_FAILURE;
                }
            }
            else
            {
                if (i4RetValCheckAuthString == FPAM_FAILURE)
                {
                    return FPAM_FAILURE;
                }
            }
        }
    }
    if (pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus == ACTIVE)
    {
        if (FPAM_SUCCESS ==
            FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
        {

            i4RetValGetPswd =
                nmhGetFsusrMgmtUserPassword (&FsusrMgmtUsrName,
                                             &FsusrMgmtAuthStr,
                                             &FsTestusrMgmtUserPassword);
            i4RetValGetConfirmPswd =
                nmhGetFsusrMgmtUserConfirmPwd (&FsusrMgmtUsrName,
                                               &FsusrMgmtAuthStr,
                                               &FsTestusrMgmtUserConfirmPwd);
            if ((i4RetValGetPswd == SNMP_SUCCESS)
                && (i4RetValGetConfirmPswd == SNMP_SUCCESS))
            {
                if ((FsTestusrMgmtUserPassword.i4_Length !=
                     FsTestusrMgmtUserConfirmPwd.i4_Length)
                    ||
                    (STRCMP
                     (FsTestusrMgmtUserPassword.pu1_OctetList,
                      FsTestusrMgmtUserConfirmPwd.pu1_OctetList) != 0))
                {
                    return FPAM_FAILURE;
                }
            }
        }
    }
    return FPAM_SUCCESS;
}

/**************************************************************************
*     Function Name : FpamGetDefaultRootUser                              *
*     Description   : This function returns the default root user         *
*     Input(s)      : None                                                *
*     Output(s)     : None                                                *
*     Returns       : None                                                *
***************************************************************************/
INT1               *
FpamGetDefaultRootUser ()
{
    return gpi1FpamDefaultRootUser;
}

/**************************************************************************
*     Function Name : FpamGetDefaultRootUserDefaultPasswd                 *
*     Description   : This function returns the default root user's       *
*                     default password                                    *
*     Input(s)      : None                                                *
*     Output(s)     : None                                                *
*     Returns       : None                                                *
***************************************************************************/
INT1               *
FpamGetDefaultRootUsrDefaultPwd ()
{
    return gpi1FpamDefaultRootUserPasswd;
}

/**************************************************************************
*     Function Name : FpamSetDefaultRootUser                              *
*     Description   : This function is to set the default root user       *
*     Input(s)      : None                                                *
*     Output(s)     : None                                                *
*     Returns       : None                                                *
***************************************************************************/
VOID
FpamSetDefaultRootUser (INT1 *pi1RootUserName)
{
    gpi1FpamDefaultRootUser = pi1RootUserName;
}

/**************************************************************************
*     Function Name : FpamLogAuditDisableStatus                           *
*     Description   : This function is to set the default root user's     *
*                     default password                                    *
*     Input(s)      : None                                                *
*     Output(s)     : None                                                *
*     Returns       : None                                                *
***************************************************************************/
VOID
FpamSetDefaultRootUserPasswd (INT1 *pi1RootUserPasswd)
{
    gpi1FpamDefaultRootUserPasswd = pi1RootUserPasswd;
}

/**************************************************************************
*     Function Name : FpamReInit                                      
*     Description   : Reinit the FPAM module to populate the users, 
*                     groups file received from active
*                                                                           
*     Input(s)      : None                                                
*     Output(s)     : None                                                
*     Returns       : None                                                
***************************************************************************/
#ifdef RM_WANTED
VOID
FpamReInit (VOID)
{
    FpamMainDeInit ();
    if (FpamMainTaskInit () == OSIX_FAILURE)
    {
        FpamMainDeInit ();
        FPAM_TRC ((FPAM_UTIL_TRC, "FpamReInitUser: FAILED\r\n"));
        return;
    }
}
#endif
/********************************************************************
 * Function    : FpamClrCfgWriteUsersDefault
 * Description : To write a fresh users file in case of files damage.
 * Input       : None
 * Output      : None
 * Returns     : None
 *****************************************************************************/
VOID
FpamClrCfgWriteUsersDefault (VOID)
{
    gb1FpamDefUserInit = OSIX_TRUE;
    FpamWriteUsersDefault ();
    gb1FpamDefUserInit = OSIX_FALSE;    /*Resting the gb1FpamDefUserInit to validate the addition of new user */
    return;
}

/****************************************************************************
* Function    :  FpamGetLoginAttempts
* Input       :  LoginUser and pi4LoginAttempts to retrieve login attempts count
* Descritpion :  This Routine will do the Get LoginAttempt count
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/

INT4
FpamGetLoginAttempts (CHR1 * pc1UserName, INT4 *pi4LoginAttempts)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pc1UserName, STRLEN (pc1UserName));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen = STRLEN (pc1UserName);

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    FPAM_LOCK;

    if (FpamGetAllFsusrMgmtTable (&FpamFsUsrMgmtEntry) != OSIX_SUCCESS)
    {
        FPAM_UNLOCK;
        return FPAM_FAILURE;
    }

    *pi4LoginAttempts = FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtLoginAttempts;
    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}

/****************************************************************************
* Function    :  FpamSetLoginAttempts
* Input       :  LoginUser and LoginAttempts to set value count 
* Descritpion :  This Routine will do the set Login Attempts counts
* Returns     :  FPAM_SUCCESS/FPAM_FAILURE
****************************************************************************/

INT4
FpamSetLoginAttempts (CHR1 * pc1UserName, INT4 i4LoginAttempt)
{
    tFpamFsUsrMgmtEntry FpamFsUsrMgmtEntry;
    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;

    MEMSET (&FpamFsUsrMgmtEntry, 0, sizeof (tFpamFsUsrMgmtEntry));

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtUserName,
            pc1UserName, STRLEN (pc1UserName));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtUserNameLen = STRLEN (pc1UserName);

    MEMCPY (FpamFsUsrMgmtEntry.MibObject.au1FsusrMgmtAuthString,
            FPAM_AUTH_STRING, STRLEN (FPAM_AUTH_STRING));

    FpamFsUsrMgmtEntry.MibObject.i4FsusrMgmtAuthStringLen =
        STRLEN (FPAM_AUTH_STRING);

    pFpamFsUsrMgmtEntry =
        RBTreeGet (gFpamGlobals.FpamGlbMib.FsusrMgmtTable,
                   (tRBElem *) & FpamFsUsrMgmtEntry);
    FPAM_LOCK;
    if (pFpamFsUsrMgmtEntry == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamSetLoginAttempts: Entry doesn't exist\r\n"));
        FPAM_UNLOCK;
        return FPAM_FAILURE;

    }

    if (i4LoginAttempt == FPAM_RESET_LOGIN_ATTEMPT)
    {
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtLoginAttempts = 0;
    }
    else
    {
        (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtLoginAttempts)++;
    }
    if (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtLoginAttempts ==
        MAX_PASSWORD_TRIES + 1)
    {
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtLoginAttempts = 1;
    }

    FPAM_UNLOCK;
    return FPAM_SUCCESS;
}
