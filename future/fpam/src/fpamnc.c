
/* $Id: fpamnc.c,v 1.1 2016/07/06 10:56:26 siva Exp $
   ISS Wrapper module
   module ARICENT-USERMGM-MIB

*/
# include "lr.h"
# include "fpaminc.h"
# include "fpamnc.h"
# include "isscli.h"
# include "fips.h"
/********************************************************************
 * FUNCTION NcFsusrMgmtStatsNumOfUsersGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtStatsNumOfUsersGet (
                UINT4 *pu4FsusrMgmtStatsNumOfUsers )
{

        INT1 i1RetVal;
        if (pu4FsusrMgmtStatsNumOfUsers == NULL)
        {
                FPAM_TRC ((FPAM_UTIL_TRC,
                                        "FpamUtlGetNumOfUsers: Password Entry NULL.\r\n"));
                return FPAM_FAILURE;
        }

        FPAM_LOCK;
        i1RetVal = nmhGetFsusrMgmtStatsNumOfUsers(
                        pu4FsusrMgmtStatsNumOfUsers );
        if(i1RetVal==SNMP_FAILURE)
        {
                FPAM_UNLOCK;
                return FPAM_FAILURE;
        }

        FPAM_UNLOCK;
        return i1RetVal;


} /* NcFsusrMgmtStatsNumOfUsersGet */

/********************************************************************
 * FUNCTION NcFsusrMgmtStatsActiveUsersGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtStatsActiveUsersGet (
                UINT4 *pu4FsusrMgmtStatsActiveUsers )
{

        INT1 i1RetVal;
        i1RetVal = nmhGetFsusrMgmtStatsActiveUsers(
                        pu4FsusrMgmtStatsActiveUsers );

        return i1RetVal;


} /* NcFsusrMgmtStatsActiveUsersGet */

/********************************************************************
 * FUNCTION NcFsusrMgmtMinPasswordLenSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtMinPasswordLenSet (
                UINT4 u4FsusrMgmtMinPasswordLen )
{

        INT1 i1RetVal;

        i1RetVal = nmhSetFsusrMgmtMinPasswordLen(
                        u4FsusrMgmtMinPasswordLen);

        return i1RetVal;


} /* NcFsusrMgmtMinPasswordLenSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtMinPasswordLenTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtMinPasswordLenTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtMinPasswordLen )
{

        INT1 i1RetVal;

        i1RetVal = nmhTestv2FsusrMgmtMinPasswordLen(pu4Error,
                        u4FsusrMgmtMinPasswordLen);

        return i1RetVal;


} /* NcFsusrMgmtMinPasswordLenTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidationCharsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidationCharsSet (
                UINT4 u4FsusrMgmtPasswdValidationChars )
{

        INT1 i1RetVal;

        i1RetVal = nmhSetFsusrMgmtPasswdValidationChars(
                        u4FsusrMgmtPasswdValidationChars);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidationCharsSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidationCharsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidationCharsTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidationChars )
{

        INT1 i1RetVal;

        i1RetVal = nmhTestv2FsusrMgmtPasswdValidationChars(pu4Error,
                        u4FsusrMgmtPasswdValidationChars);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidationCharsTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidateNoOfLowerCaseSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidateNoOfLowerCaseSet (
                UINT4 u4FsusrMgmtPasswdValidateNoOfLowerCase )
{

        INT1 i1RetVal;

        i1RetVal = nmhSetFsusrMgmtPasswdValidateNoOfLowerCase(
                        u4FsusrMgmtPasswdValidateNoOfLowerCase);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidateNoOfLowerCaseSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidateNoOfLowerCaseTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidateNoOfLowerCaseTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidateNoOfLowerCase )
{

        INT1 i1RetVal;

        i1RetVal = nmhTestv2FsusrMgmtPasswdValidateNoOfLowerCase(pu4Error,
                        u4FsusrMgmtPasswdValidateNoOfLowerCase);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidateNoOfLowerCaseTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidateNoOfUpperCaseSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidateNoOfUpperCaseSet (
                UINT4 u4FsusrMgmtPasswdValidateNoOfUpperCase )
{

        INT1 i1RetVal;

        i1RetVal = nmhSetFsusrMgmtPasswdValidateNoOfUpperCase(
                        u4FsusrMgmtPasswdValidateNoOfUpperCase);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidateNoOfUpperCaseSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidateNoOfUpperCaseTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidateNoOfUpperCaseTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidateNoOfUpperCase )
{

        INT1 i1RetVal;

        i1RetVal = nmhTestv2FsusrMgmtPasswdValidateNoOfUpperCase(pu4Error,
                        u4FsusrMgmtPasswdValidateNoOfUpperCase);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidateNoOfUpperCaseTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidateNoOfNumericalsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidateNoOfNumericalsSet (
                UINT4 u4FsusrMgmtPasswdValidateNoOfNumericals )
{

        INT1 i1RetVal;

        i1RetVal = nmhSetFsusrMgmtPasswdValidateNoOfNumericals(
                        u4FsusrMgmtPasswdValidateNoOfNumericals);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidateNoOfNumericalsSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidateNoOfNumericalsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidateNoOfNumericalsTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidateNoOfNumericals )
{

        INT1 i1RetVal;

        i1RetVal = nmhTestv2FsusrMgmtPasswdValidateNoOfNumericals(pu4Error,
                        u4FsusrMgmtPasswdValidateNoOfNumericals);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidateNoOfNumericalsTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidateNoOfSplCharsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidateNoOfSplCharsSet (
                UINT4 u4FsusrMgmtPasswdValidateNoOfSplChars )
{

        INT1 i1RetVal;

        i1RetVal = nmhSetFsusrMgmtPasswdValidateNoOfSplChars(
                        u4FsusrMgmtPasswdValidateNoOfSplChars);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidateNoOfSplCharsSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdValidateNoOfSplCharsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdValidateNoOfSplCharsTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidateNoOfSplChars )
{

        INT1 i1RetVal;

        i1RetVal = nmhTestv2FsusrMgmtPasswdValidateNoOfSplChars(pu4Error,
                        u4FsusrMgmtPasswdValidateNoOfSplChars);

        return i1RetVal;


} /* NcFsusrMgmtPasswdValidateNoOfSplCharsTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdMaxLifeTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdMaxLifeTimeSet (
                UINT4 u4FsusrMgmtPasswdMaxLifeTime )
{

        INT1 i1RetVal;

        i1RetVal = nmhSetFsusrMgmtPasswdMaxLifeTime(
                        u4FsusrMgmtPasswdMaxLifeTime);

        return i1RetVal;


} /* NcFsusrMgmtPasswdMaxLifeTimeSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtPasswdMaxLifeTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtPasswdMaxLifeTimeTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdMaxLifeTime )
{

        INT1 i1RetVal;

        i1RetVal = nmhTestv2FsusrMgmtPasswdMaxLifeTime(pu4Error,
                        u4FsusrMgmtPasswdMaxLifeTime);

        return i1RetVal;


} /* NcFsusrMgmtPasswdMaxLifeTimeTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtStatsEnableUsersGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtStatsEnableUsersGet (
                UINT4 *pu4FsusrMgmtStatsEnableUsers )
{

        INT1 i1RetVal;
        i1RetVal = nmhGetFsusrMgmtStatsEnableUsers(
                        pu4FsusrMgmtStatsEnableUsers );

        return i1RetVal;


} /* NcFsusrMgmtStatsEnableUsersGet */

/********************************************************************
 * * FUNCTION NcFsusrMgmtStatsDisableUsersGet
 * *
 * * Wrapper for nmhGet API's
 * *
 * * RETURNS:
 * *     error status
 * ********************************************************************/
INT1 NcFsusrMgmtStatsDisableUsersGet (
                UINT4 *pu4FsusrMgmtStatsDisableUsers )
{
        INT1 i1RetVal;
        i1RetVal = nmhGetFsusrMgmtStatsDisableUsers(
                        pu4FsusrMgmtStatsDisableUsers );

        return i1RetVal;


} /* NcFsusrMgmtStatsDisableUsersGet */


/********************************************************************
 * FUNCTION NcFsusrMgmtUserPasswordSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserPasswordSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT1 *pFsusrMgmtUserPassword )
{

        INT1 i1RetVal;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserpassword;
        MEMSET (&FsusrMgmtuserpassword, 0,  sizeof (FsusrMgmtuserpassword));

        FsusrMgmtuserpassword.i4_Length = (INT4) STRLEN (pFsusrMgmtUserPassword);
        FsusrMgmtuserpassword.pu1_OctetList = pFsusrMgmtUserPassword;

        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhSetFsusrMgmtUserPassword(
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        &FsusrMgmtuserpassword);

        return i1RetVal;
} /* NcFsusrMgmtUserPasswordSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserPasswordTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserPasswordTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT1 *pFsusrMgmtUserPassword )
{

        INT1 i1RetVal;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserPassword;
        MEMSET (&FsusrMgmtuserPassword, 0,  sizeof (FsusrMgmtuserPassword));

        FsusrMgmtuserPassword.i4_Length = (INT4) STRLEN (pFsusrMgmtUserPassword);
        FsusrMgmtuserPassword.pu1_OctetList = pFsusrMgmtUserPassword;

        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhTestv2FsusrMgmtUserPassword(pu4Error,
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        &FsusrMgmtuserPassword);

        return i1RetVal;


} /* NcFsusrMgmtUserPasswordTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserPrivilegeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserPrivilegeSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT4 u4FsusrMgmtUserPrivilege )
{

        INT1 i1RetVal;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;

        i1RetVal = nmhSetFsusrMgmtUserPrivilege(
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        u4FsusrMgmtUserPrivilege);

        return i1RetVal;


} /* NcFsusrMgmtUserPrivilegeSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserPrivilegeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserPrivilegeTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT4 u4FsusrMgmtUserPrivilege )
{

        INT1 i1RetVal;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;

        i1RetVal = nmhTestv2FsusrMgmtUserPrivilege(pu4Error,
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        u4FsusrMgmtUserPrivilege);

        return i1RetVal;


} /* NcFsusrMgmtUserPrivilegeTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserLoginCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserLoginCountGet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 *pi4FsusrMgmtUserLoginCount )
{

        INT1 i1RetVal;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;

        if (nmhValidateIndexInstanceFsusrMgmtTable(
                                &FsusrMgmtuserName,
                                &FsusrMgmtAuthstring) == SNMP_FAILURE)

        {
                return SNMP_FAILURE;
        }

        i1RetVal = nmhGetFsusrMgmtUserLoginCount(
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        pi4FsusrMgmtUserLoginCount );

        return i1RetVal;


} /* NcFsusrMgmtUserLoginCountGet */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserStatusSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 i4FsusrMgmtUserStatus )
{

        INT1 i1RetVal;

        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhSetFsusrMgmtUserStatus(
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        i4FsusrMgmtUserStatus);

        return i1RetVal;


} /* NcFsusrMgmtUserStatusSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserStatusTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 i4FsusrMgmtUserStatus )
{

        INT1 i1RetVal;

        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhTestv2FsusrMgmtUserStatus(pu4Error,
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        i4FsusrMgmtUserStatus);

        return i1RetVal;


} /* NcFsusrMgmtUserStatusTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserLockRelTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserLockRelTimeSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT4 u4FsusrMgmtUserLockRelTime )
{

        INT1 i1RetVal;

        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhSetFsusrMgmtUserLockRelTime(
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        u4FsusrMgmtUserLockRelTime);

        return i1RetVal;


} /* NcFsusrMgmtUserLockRelTimeSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserLockRelTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserLockRelTimeTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT4 u4FsusrMgmtUserLockRelTime )
{

        INT1 i1RetVal;

        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhTestv2FsusrMgmtUserLockRelTime(pu4Error,
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        u4FsusrMgmtUserLockRelTime);

        return i1RetVal;


} /* NcFsusrMgmtUserLockRelTimeTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserRowStatusSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 i4FsusrMgmtUserRowStatus )
{

        INT1 i1RetVal;

        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhSetFsusrMgmtUserRowStatus(
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        i4FsusrMgmtUserRowStatus);

        return i1RetVal;


} /* NcFsusrMgmtUserRowStatusSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserRowStatusTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 i4FsusrMgmtUserRowStatus )
{

        INT1 i1RetVal;

        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhTestv2FsusrMgmtUserRowStatus(pu4Error,
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        i4FsusrMgmtUserRowStatus);

        return i1RetVal;


} /* NcFsusrMgmtUserRowStatusTest */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserConfirmPwdSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserConfirmPwdSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT1 *pFsusrMgmtUserConfirmPwd )
{

        INT1 i1RetVal;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserConfirmPwd;

        FsusrMgmtuserConfirmPwd.i4_Length = (INT4) STRLEN (pFsusrMgmtUserConfirmPwd);
        FsusrMgmtuserConfirmPwd.pu1_OctetList = pFsusrMgmtUserConfirmPwd;
        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;

        i1RetVal = nmhSetFsusrMgmtUserConfirmPwd(
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        &FsusrMgmtuserConfirmPwd);

        return i1RetVal;


} /* NcFsusrMgmtUserConfirmPwdSet */

/********************************************************************
 * FUNCTION NcFsusrMgmtUserConfirmPwdTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsusrMgmtUserConfirmPwdTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT1 *pFsusrMgmtUserConfirmPwd )
{

        INT1 i1RetVal;
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserName;
        MEMSET (&FsusrMgmtuserName, 0,  sizeof (FsusrMgmtuserName));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthstring;
        MEMSET (&FsusrMgmtAuthstring, 0,  sizeof (FsusrMgmtAuthstring));
        tSNMP_OCTET_STRING_TYPE FsusrMgmtuserConfirmPwd;
        MEMSET (&FsusrMgmtuserConfirmPwd, 0,  sizeof (FsusrMgmtUserConfirmPwd));

        FsusrMgmtuserConfirmPwd.i4_Length = (INT4) STRLEN (pFsusrMgmtUserConfirmPwd);
        FsusrMgmtuserConfirmPwd.pu1_OctetList = pFsusrMgmtUserConfirmPwd;

        FsusrMgmtuserName.i4_Length = (INT4) STRLEN (pFsusrMgmtUserName);
        FsusrMgmtuserName.pu1_OctetList = pFsusrMgmtUserName;
        FsusrMgmtAuthstring.i4_Length = (INT4) STRLEN (pFsusrMgmtAuthString);
        FsusrMgmtAuthstring.pu1_OctetList = pFsusrMgmtAuthString;
        i1RetVal = nmhTestv2FsusrMgmtUserConfirmPwd(pu4Error,
                        &FsusrMgmtuserName,
                        &FsusrMgmtAuthstring,
                        &FsusrMgmtuserConfirmPwd);

        return i1RetVal;


} /* NcFsusrMgmtUserConfirmPwdTest */

/* END i_ARICENT_USERMGM_MIB.c */
