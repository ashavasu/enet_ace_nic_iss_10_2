/******************************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fpamtmr.c,v 1.1 2012/01/18 13:28:28 siva Exp $
 *
 * Description : This file contains procedures containing Fpam Timer
 *               related operations
 *****************************************************************************/
#include "fpaminc.h"

PRIVATE tFpamTmrDesc gaFpamTmrDesc[FPAM_MAX_TMRS];

/****************************************************************************
*                                                                           *
* Function     : FpamTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize Fpam Timer Decriptors                           *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
FpamTmrInitTmrDesc ()
{

    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC:FpamTmrInitTmrDesc\n"));

/*
    gaFpamTmrDesc[FPAM_PMR_TMR].pTmrExpFunc = FpamTmrPmr;
    gaFpamTmrDesc[FPAM_PMR_TMR].i2Offset =
        (INT2) (FPAM_GET_OFFSET (tFpamPmr, pmrTmr));
*/

    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "FUNC:EXIT FpamTmrInitTmrDesc\n"));

}

/****************************************************************************
*                                                                           *
* Function     : FpamTmrStartTmr                                            *
*                                                                           *
* Description  : Starts Fpam Timer                                          *
*                                                                           *
* Input        : pFpamTmr - pointer to FpamTmr structure                    *
*                eFpamTmrId - FPAM timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
FpamTmrStartTmr (tFpamTmr * pFpamTmr, enFpamTmrId eFpamTmrId, UINT4 u4Secs)
{

    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC:FpamTmrStartTmr\n"));

    pFpamTmr->eFpamTmrId = eFpamTmrId;

    if (TmrStartTimer (gFpamGlobals.fpamTmrLst, &(pFpamTmr->tmrNode),
                       (UINT4) NO_OF_TICKS_PER_SEC * u4Secs) == TMR_FAILURE)
    {
        FPAM_TRC ((FPAM_TMR_TRC, "Tmr Start Failure\n"));
    }

    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "FUNC: Exit FpamTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : FpamTmrRestartTmr                                          *
*                                                                           *
* Description  :  ReStarts Fpam Timer                                       *
*                                                                           *
* Input        : pFpamTmr - pointer to FpamTmr structure                    *
*                eFpamTmrId - FPAM timer ID                                 *
*                u4Secs     - ticks in seconds                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
FpamTmrRestartTmr (tFpamTmr * pFpamTmr, enFpamTmrId eFpamTmrId, UINT4 u4Secs)
{

    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC:FpamTmrRestartTmr\n"));

    TmrStopTimer (gFpamGlobals.fpamTmrLst, &(pFpamTmr->tmrNode));
    FpamTmrStartTmr (pFpamTmr, eFpamTmrId, u4Secs);

    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "FUNC: Exit FpamTmrStartTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : FpamTmrStopTmr                                             *
*                                                                           *
* Description  : Restarts Fpam Timer                                        *
*                                                                           *
* Input        : pFpamTmr - pointer to FpamTmr structure                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
FpamTmrStopTmr (tFpamTmr * pFpamTmr)
{

    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC:FpamTmrStopTmr\n"));

    TmrStopTimer (gFpamGlobals.fpamTmrLst, &(pFpamTmr->tmrNode));

    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "FUNC: Exit FpamTmrStopTmr\n"));
    return;
}

/****************************************************************************
*                                                                           *
* Function     : FpamTmrHandleExpiry                                        *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
FpamTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    enFpamTmrId         eFpamTmrId = FPAM_TMR1; 
    INT2                i2Offset = 0;

    FPAM_TRC_FUNC ((FPAM_FN_ENTRY, "FUNC: FpamTmrHandleExpiry\n"));
    while ((pExpiredTimers = TmrGetNextExpiredTimer (gFpamGlobals.fpamTmrLst))
           != NULL)
    {

        eFpamTmrId = ((tFpamTmr *) pExpiredTimers)->eFpamTmrId;
        i2Offset = gaFpamTmrDesc[eFpamTmrId].i2Offset;

        if (i2Offset < 0)
        {

            /* The timer function does not take any parameter */

            (*(gaFpamTmrDesc[eFpamTmrId].pTmrExpFunc)) (NULL);

        }
        else
        {

            /* The timer function requires a parameter */

            (*(gaFpamTmrDesc[eFpamTmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    FPAM_TRC_FUNC ((FPAM_FN_EXIT, "FUNC: Exit FpamTmrHandleExpiry\n"));
    return;

}


/*-----------------------------------------------------------------------*/
/*                       End of the file  fpamtmr.c                      */
/*-----------------------------------------------------------------------*/
