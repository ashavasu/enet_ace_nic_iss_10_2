/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamdsg.c,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Fpam 
*********************************************************************/

#include "fpaminc.h"
/****************************************************************************
 Function    :  FpamGetFsusrMgmtStatsNumOfUsers
 Input       :  The Indices
 Input       :  pu4FsusrMgmtStatsNumOfUsers
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtStatsNumOfUsers (UINT4 *pu4FsusrMgmtStatsNumOfUsers)
{
    *pu4FsusrMgmtStatsNumOfUsers =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsNumOfUsers;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtStatsActiveUsers
 Input       :  The Indices
 Input       :  pu4FsusrMgmtStatsActiveUsers
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtStatsActiveUsers (UINT4 *pu4FsusrMgmtStatsActiveUsers)
{
    *pu4FsusrMgmtStatsActiveUsers =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsActiveUsers;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtMinPasswordLen
 Input       :  The Indices
 Input       :  pu4FsusrMgmtMinPasswordLen
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtMinPasswordLen (UINT4 *pu4FsusrMgmtMinPasswordLen)
{
    *pu4FsusrMgmtMinPasswordLen =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtMinPasswordLen;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamSetFsusrMgmtMinPasswordLen
 Input       :  u4FsusrMgmtMinPasswordLen
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
FpamSetFsusrMgmtMinPasswordLen (UINT4 u4FsusrMgmtMinPasswordLen)
{
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtMinPasswordLen =
        u4FsusrMgmtMinPasswordLen;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtPasswdValidationChars
 Input       :  The Indices
 Input       :  pu4FsusrMgmtPasswdValidationChars
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtPasswdValidationChars (UINT4 *pu4FsusrMgmtPasswdValidationChars)
{
    *pu4FsusrMgmtPasswdValidationChars =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidationChars;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamSetFsusrMgmtPasswdValidationChars
 Input       :  u4FsusrMgmtPasswdValidationChars
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
FpamSetFsusrMgmtPasswdValidationChars (UINT4 u4FsusrMgmtPasswdValidationChars)
{
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidationChars =
        u4FsusrMgmtPasswdValidationChars;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtPasswdValidateNoOfLowerCase
 Input       :  The Indices
 Input       :  pu4FsusrMgmtPasswdValidateNoOfLowerCase
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtPasswdValidateNoOfLowerCase (UINT4
                                             *pu4FsusrMgmtPasswdValidateNoOfLowerCase)
{
    *pu4FsusrMgmtPasswdValidateNoOfLowerCase =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfLowerCase;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamSetFsusrMgmtPasswdValidateNoOfLowerCase
 Input       :  u4FsusrMgmtPasswdValidateNoOfLowerCase
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
FpamSetFsusrMgmtPasswdValidateNoOfLowerCase (UINT4
                                             u4FsusrMgmtPasswdValidateNoOfLowerCase)
{
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfLowerCase =
        u4FsusrMgmtPasswdValidateNoOfLowerCase;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtPasswdValidateNoOfUpperCase
 Input       :  The Indices
 Input       :  pu4FsusrMgmtPasswdValidateNoOfUpperCase
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtPasswdValidateNoOfUpperCase (UINT4
                                             *pu4FsusrMgmtPasswdValidateNoOfUpperCase)
{
    *pu4FsusrMgmtPasswdValidateNoOfUpperCase =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfUpperCase;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamSetFsusrMgmtPasswdValidateNoOfUpperCase
 Input       :  u4FsusrMgmtPasswdValidateNoOfUpperCase
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
FpamSetFsusrMgmtPasswdValidateNoOfUpperCase (UINT4
                                             u4FsusrMgmtPasswdValidateNoOfUpperCase)
{
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfUpperCase =
        u4FsusrMgmtPasswdValidateNoOfUpperCase;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtPasswdValidateNoOfNumericals
 Input       :  The Indices
 Input       :  pu4FsusrMgmtPasswdValidateNoOfNumericals
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtPasswdValidateNoOfNumericals (UINT4
                                              *pu4FsusrMgmtPasswdValidateNoOfNumericals)
{
    *pu4FsusrMgmtPasswdValidateNoOfNumericals =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfNumericals;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamSetFsusrMgmtPasswdValidateNoOfNumericals
 Input       :  u4FsusrMgmtPasswdValidateNoOfNumericals
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
FpamSetFsusrMgmtPasswdValidateNoOfNumericals (UINT4
                                              u4FsusrMgmtPasswdValidateNoOfNumericals)
{
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfNumericals =
        u4FsusrMgmtPasswdValidateNoOfNumericals;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtPasswdValidateNoOfSplChars
 Input       :  The Indices
 Input       :  pu4FsusrMgmtPasswdValidateNoOfSplChars
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtPasswdValidateNoOfSplChars (UINT4
                                            *pu4FsusrMgmtPasswdValidateNoOfSplChars)
{
    *pu4FsusrMgmtPasswdValidateNoOfSplChars =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfSplChars;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamSetFsusrMgmtPasswdValidateNoOfSplChars
 Input       :  u4FsusrMgmtPasswdValidateNoOfSplChars
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
FpamSetFsusrMgmtPasswdValidateNoOfSplChars (UINT4
                                            u4FsusrMgmtPasswdValidateNoOfSplChars)
{
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfSplChars =
        u4FsusrMgmtPasswdValidateNoOfSplChars;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtPasswdMaxLifeTime
 Input       :  The Indices
 Input       :  pu4FsusrMgmtPasswdMaxLifeTime
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtPasswdMaxLifeTime (UINT4 *pu4FsusrMgmtPasswdMaxLifeTime)
{
    *pu4FsusrMgmtPasswdMaxLifeTime =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdMaxLifeTime;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamSetFsusrMgmtPasswdMaxLifeTime
 Input       :  u4FsusrMgmtPasswdMaxLifeTime
 Description :  This Routine will Set 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

INT4
FpamSetFsusrMgmtPasswdMaxLifeTime (UINT4 u4FsusrMgmtPasswdMaxLifeTime)
{
    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdMaxLifeTime =
        u4FsusrMgmtPasswdMaxLifeTime;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtStatsEnableUsers
 Input       :  The Indices
                pu4FsusrMgmtStatsEnableUsers
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtStatsEnableUsers (UINT4 *pu4FsusrMgmtStatsEnableUsers)
{
    *pu4FsusrMgmtStatsEnableUsers =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsEnableUsers;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtStatsDisableUsers
 Input       :  The Indices
                pu4FsusrMgmtStatsDisableUsers
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
FpamGetFsusrMgmtStatsDisableUsers (UINT4 *pu4FsusrMgmtStatsDisableUsers)
{
    *pu4FsusrMgmtStatsDisableUsers =
        gFpamGlobals.FpamGlbMib.u4FsusrMgmtStatsDisableUsers;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  FpamGetFirstFsusrMgmtTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pFpamFsUsrMgmtEntry or NULL
****************************************************************************/
tFpamFsUsrMgmtEntry *
FpamGetFirstFsusrMgmtTable ()
{
    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;

    pFpamFsUsrMgmtEntry =
        (tFpamFsUsrMgmtEntry *) RBTreeGetFirst (gFpamGlobals.FpamGlbMib.
                                                FsusrMgmtTable);

    return pFpamFsUsrMgmtEntry;
}

/****************************************************************************
 Function    :  FpamGetNextFsusrMgmtTable
 Input       :  pCurrentFpamFsUsrMgmtEntry
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextFpamFsUsrMgmtEntry or NULL
****************************************************************************/
tFpamFsUsrMgmtEntry *
FpamGetNextFsusrMgmtTable (tFpamFsUsrMgmtEntry * pCurrentFpamFsUsrMgmtEntry)
{
    tFpamFsUsrMgmtEntry *pNextFpamFsUsrMgmtEntry = NULL;

    pNextFpamFsUsrMgmtEntry =
        (tFpamFsUsrMgmtEntry *) RBTreeGetNext (gFpamGlobals.FpamGlbMib.
                                               FsusrMgmtTable,
                                               (tRBElem *)
                                               pCurrentFpamFsUsrMgmtEntry,
                                               NULL);

    return pNextFpamFsUsrMgmtEntry;
}

/****************************************************************************
 Function    :  FpamGetFsusrMgmtTable
 Input       :  pFpamFsUsrMgmtEntry
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetFpamFsUsrMgmtEntry or NULL
****************************************************************************/
tFpamFsUsrMgmtEntry *
FpamGetFsusrMgmtTable (tFpamFsUsrMgmtEntry * pFpamFsUsrMgmtEntry)
{
    tFpamFsUsrMgmtEntry *pGetFpamFsUsrMgmtEntry = NULL;

    pGetFpamFsUsrMgmtEntry =
        (tFpamFsUsrMgmtEntry *) RBTreeGet (gFpamGlobals.FpamGlbMib.
                                           FsusrMgmtTable,
                                           (tRBElem *) pFpamFsUsrMgmtEntry);

    return pGetFpamFsUsrMgmtEntry;
}
