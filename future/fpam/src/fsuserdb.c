/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsuserdb.c,v 1.1 2012/01/18 13:28:28 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module FPAM 
*********************************************************************/


/****************************************************************************
 Function    :  fpamGetAllFsusrMgmtTable
 Input       :  The Indices
                pfpamFsusrMgmtTable
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4 fpamGetAllFsusrMgmtTable (tfpamFsusrMgmtTable *pfpamGetFsusrMgmtTable)
{
	tfpamFsusrMgmtTable  *pfpamFsusrMgmtTable = NULL;

	/* Check whether the node is already present */
	pfpamFsusrMgmtTable = RBTreeGet (gfpamGblInfo.FsusrMgmtTable, (tRBElem *) pfpamGetFsusrMgmtTable);

	if (pfpamFsusrMgmtTable == NULL)
	{
		return SNMP_FAILURE;
	}
	
	/* Assign values from the existing node*/
	MEMCPY (pfpamGetFsusrMgmtTable->au1FsusrMgmtUserPassword,
			pfpamFsusrMgmtTable->au1FsusrMgmtUserPassword, pfpamFsusrMgmtTable->i4FsusrMgmtUserPassword_Length);

	pfpamGetFsusrMgmtTable->u4FsusrMgmtUserPrivilege = 
		pfpamFsusrMgmtTable->u4FsusrMgmtUserPrivilege;

	pfpamGetFsusrMgmtTable->i4FsusrMgmtUserLoginCount = 
		pfpamFsusrMgmtTable->i4FsusrMgmtUserLoginCount;

	pfpamGetFsusrMgmtTable->i4FsusrMgmtUserStatus = 
		pfpamFsusrMgmtTable->i4FsusrMgmtUserStatus;

	pfpamGetFsusrMgmtTable->u4FsusrMgmtUserLockRelTime = 
		pfpamFsusrMgmtTable->u4FsusrMgmtUserLockRelTime;

	pfpamGetFsusrMgmtTable->i4FsusrMgmtUserRowStatus = 
		pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus;

	return SNMP_SUCCESS;
}



/****************************************************************************
 Function    :  fpamSetAllFsusrMgmtTable
 Input       :  The Indices
                pfpamFsusrMgmtTable
                pfpamIsSetFsusrMgmtTable
                i4RowStatusLogic
                i4RowCreateOption
 Output      :  This Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4 fpamSetAllFsusrMgmtTable (tfpamFsusrMgmtTable *pfpamSetFsusrMgmtTable,
				 tfpamIsSetFsusrMgmtTable *pfpamIsSetFsusrMgmtTable, 
					INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
	tfpamFsusrMgmtTable       *pfpamFsusrMgmtTable = NULL;
	tfpamFsusrMgmtTable       fpamOldFsusrMgmtTable;
	tfpamFsusrMgmtTable       fpamTrgFsusrMgmtTable;
	tfpamIsSetFsusrMgmtTable  fpamTrgIsSetFsusrMgmtTable;
	INT4      i4RowMakeActive = FALSE;

	MEMSET (&fpamOldFsusrMgmtTable, 0 , sizeof (tfpamFsusrMgmtTable));
	MEMSET (&fpamTrgFsusrMgmtTable, 0 , sizeof (tfpamFsusrMgmtTable));
	MEMSET (&fpamTrgIsSetFsusrMgmtTable, 0 , sizeof (tfpamIsSetFsusrMgmtTable));

	/* Check whether the node is already present */
	pfpamFsusrMgmtTable = RBTreeGet (gfpamGblInfo.FsusrMgmtTable, (tRBElem *) pfpamSetFsusrMgmtTable);

	if (pfpamFsusrMgmtTable == NULL)
	{
		/* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO*/
		if ((pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus == CREATE_AND_WAIT) || 
			(pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus == CREATE_AND_GO) || 
			((pfpamSetFsusrMgmtTable->FsusrMgmtUserRowStatus == ACTIVE) && (i4RowCreateOption == 1)))
		{
			/* Allocate memory for the new node*/
			pfpamFsusrMgmtTable = (tfpamFsusrMgmtTable *) MemAllocMemBlk (FPAM_FSUSRMGMTTABLE_POOLID);
			if (pfpamFsusrMgmtTable == NULL)
			{
				return SNMP_FAILURE;
			}

			/* Assign values for the new node*/
			if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserName != OSIX_FALSE)
			{
				MEMCPY (pfpamFsusrMgmtTable->au1FsusrMgmtUserName,
					pfpamSetFsusrMgmtTable->au1FsusrMgmtUserName,pfpamSetFsusrMgmtTable->i4FsusrMgmtUserName_Length);
			}

			if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtAuthString != OSIX_FALSE)
			{
				MEMCPY (pfpamFsusrMgmtTable->au1FsusrMgmtAuthString,
					pfpamSetFsusrMgmtTable->au1FsusrMgmtAuthString,pfpamSetFsusrMgmtTable->i4FsusrMgmtAuthString_Length);
			}

			if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserPassword != OSIX_FALSE)
			{
				MEMCPY (pfpamFsusrMgmtTable->au1FsusrMgmtUserPassword,
					pfpamSetFsusrMgmtTable->au1FsusrMgmtUserPassword,pfpamSetFsusrMgmtTable->i4FsusrMgmtUserPassword_Length);
			}
			else
			{
			STRCPY (pfpamFsusrMgmtTable->au1FsusrMgmtUserPassword,
						Password123);
			}

			if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserPrivilege != OSIX_FALSE)
			{
				pfpamFsusrMgmtTable->u4FsusrMgmtUserPrivilege = 
					pfpamSetFsusrMgmtTable->u4FsusrMgmtUserPrivilege;
			}
			else
			{
				pfpamFsusrMgmtTable->u4FsusrMgmtUserPrivilege = 1;
			}

			if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserStatus != OSIX_FALSE)
			{
				pfpamFsusrMgmtTable->i4FsusrMgmtUserStatus = 
					pfpamSetFsusrMgmtTable->i4FsusrMgmtUserStatus;
			}
			else
			{
				pfpamFsusrMgmtTable->i4FsusrMgmtUserStatus = 1;
			}

			if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserLockRelTime != OSIX_FALSE)
			{
				pfpamFsusrMgmtTable->u4FsusrMgmtUserLockRelTime = 
					pfpamSetFsusrMgmtTable->u4FsusrMgmtUserLockRelTime;
			}
			else
			{
				pfpamFsusrMgmtTable->u4FsusrMgmtUserLockRelTime = 0;
			}

			if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserRowStatus != OSIX_FALSE)
			{
				pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus = 
					pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus;
			}

			if ((pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus == CREATE_AND_GO) ||
					(i4RowCreateOption == 1))
			{
				pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus = ACTIVE;
				/* For MSR and RM Trigger*/
				fpamTrgFsusrMgmtTable->i4FsusrMgmtUserRowStatus = CREATE_AND_GO;
				fpamTrgIsSetFsusrMgmtTable->bFsusrMgmtUserRowStatus = OSIX_TRUE;

				if (fpamSetAllFsusrMgmtTableTrigger (&fpamTrgFsusrMgmtTable, 
						&fpamTrgIsSetFsusrMgmtTable, OSIX_FALSE) != SNMP_SUCCESS)
				{
					return SNMP_FAILURE;
				}
			}
			else if (pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus == CREATE_AND_WAIT)
			{
				pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus = NOT_READY;
				/* For MSR and RM Trigger*/
				fpamTrgFsusrMgmtTable->i4FsusrMgmtUserRowStatus = CREATE_AND_WAIT;
				fpamTrgIsSetFsusrMgmtTable->bFsusrMgmtUserRowStatus = OSIX_TRUE;

				if (fpamSetAllFsusrMgmtTableTrigger (&fpamTrgFsusrMgmtTable, 
						&fpamTrgIsSetFsusrMgmtTable, OSIX_FALSE) != SNMP_SUCCESS)
				{
					return SNMP_FAILURE;
				}
			}

			/* Add the new node to the database*/
			if (RBTreeAdd (gfpamGblInfo.FsusrMgmtTable, pfpamFsusrMgmtTable) != RB_SUCCESS)
			{
				return SNMP_FAILURE;
			}
			if(fpamUtilUpdateFsusrMgmtTable(NULL, pfpamFsusrMgmtTable) != OSIX_TRUE)
			{
				RBTreeRem (gfpamGblInfo.FsusrMgmtTable, pfpamFsusrMgmtTable);
				return SNMP_FAILURE;
			}

			if (fpamSetAllFsusrMgmtTableTrigger (pfpamSetFsusrMgmtTable, 
				pfpamIsSetFsusrMgmtTable, OSIX_FALSE) != SNMP_SUCCESS)
			{
				return SNMP_FAILURE;
			}
			return SNMP_SUCCESS;

		}
		else
		{
			return SNMP_FAILURE;
		}
	}
	else if ((pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus == CREATE_AND_WAIT) || 
			(pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus == CREATE_AND_GO))
	{
		return SNMP_FAILURE;
	}
	/* Copy the previous values before setting the new values*/
	MEMCPY (&fpamOldFsusrMgmtTable, pfpamFsusrMgmtTable, sizeof (tfpamFsusrMgmtTable));

	/*Delete the node from the database if the RowStatus given is DESTROY*/
	if (pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus == DESTROY)
	{
		pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus = DESTROY;

		if(fpamUtilUpdateFsusrMgmtTable(&fpamOldFsusrMgmtTable,
				 pfpamFsusrMgmtTable) != OSIX_TRUE)
		{
			return SNMP_FAILURE;
		}
		RBTreeRem (gfpamGblInfo.FsusrMgmtTable, pfpamFsusrMgmtTable);
		MemReleaseMemBlock (FPAM_FSUSRMGMTTABLE_POOLID, (UINT1 *) pfpamFsusrMgmtTable);

		if (fpamSetAllFsusrMgmtTableTrigger (pfpamSetFsusrMgmtTable, 
				pfpamIsSetFsusrMgmtTable, OSIX_FALSE) != SNMP_SUCCESS)
		{
			return SNMP_FAILURE;
		}
		return SNMP_SUCCESS;
	}

	/*Function to check whether the given input is same as there in database*/
	if(FsusrMgmtTableFilterInputs (pfpamFsusrMgmtTable, pfpamSetFsusrMgmtTable,
						 pfpamIsSetFsusrMgmtTable) != OSIX_FALSE)
	{
		return SNMP_SUCCESS;
	}

	/*This condtion is to make the row NOT_IN_SERVICE before modifying the values*/
	if((i4RowStatusLogic == TRUE) && 
		(pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus == ACTIVE))
	{
		pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus = NOT_IN_SERVICE;
		i4RowMakeActive = TRUE;

		/* For MSR and RM Trigger*/
		fpamTrgFsusrMgmtTable->i4FsusrMgmtUserRowStatus = NOT_IN_SERVICE;
		fpamTrgIsSetFsusrMgmtTable->bFsusrMgmtUserRowStatus = OSIX_TRUE;

		if(fpamUtilUpdateFsusrMgmtTable(&fpamOldFsusrMgmtTable, 
				pfpamFsusrMgmtTable) != OSIX_TRUE)
		{
			/*Restore back with previous values*/
			MEMCPY (pfpamFsusrMgmtTable, &fpamOldFsusrMgmtTable,
					 sizeof (tfpamFsusrMgmtTable));
			return SNMP_FAILURE;
		}

		if (fpamSetAllFsusrMgmtTableTrigger (&fpamTrgFsusrMgmtTable, 
				&fpamTrgIsSetFsusrMgmtTable, OSIX_FALSE) != SNMP_SUCCESS)
		{
			return SNMP_FAILURE;
		}
	}

	if (pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus == ACTIVE)
	{
		i4RowMakeActive = TRUE;
	}

	/* Assign values for the existing node*/
	if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserPassword != OSIX_FALSE)
	{
		MEMCPY (pfpamFsusrMgmtTable->au1FsusrMgmtUserPassword,
					pfpamSetFsusrMgmtTable->au1FsusrMgmtUserPassword,pfpamSetFsusrMgmtTable->i4FsusrMgmtUserPassword_Length);
	}
	if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserPrivilege != OSIX_FALSE)
	{
		pfpamFsusrMgmtTable->u4FsusrMgmtUserPrivilege = 
					pfpamSetFsusrMgmtTable->u4FsusrMgmtUserPrivilege;
	}
	if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserStatus != OSIX_FALSE)
	{
		pfpamFsusrMgmtTable->i4FsusrMgmtUserStatus = 
					pfpamSetFsusrMgmtTable->i4FsusrMgmtUserStatus;
	}
	if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserLockRelTime != OSIX_FALSE)
	{
		pfpamFsusrMgmtTable->u4FsusrMgmtUserLockRelTime = 
					pfpamSetFsusrMgmtTable->u4FsusrMgmtUserLockRelTime;
	}
	if (pfpamIsSetFsusrMgmtTable->bFsusrMgmtUserRowStatus != OSIX_FALSE)
	{
		pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus = 
					pfpamSetFsusrMgmtTable->i4FsusrMgmtUserRowStatus;
	}

	/*This condtion is to make the row back to ACTIVE after modifying the values*/
	if(i4RowMakeActive == TRUE)
	{
		pfpamFsusrMgmtTable->i4FsusrMgmtUserRowStatus = ACTIVE;
	}

	if(fpamUtilUpdateFsusrMgmtTable(&fpamOldFsusrMgmtTable,
			 pfpamFsusrMgmtTable) != OSIX_TRUE)
	{
		/*Restore back with previous values*/
		MEMCPY (pfpamFsusrMgmtTable, &fpamOldFsusrMgmtTable,
					 sizeof (tfpamFsusrMgmtTable));
		return SNMP_FAILURE;
	}

	if (fpamSetAllFsusrMgmtTableTrigger (pfpamSetFsusrMgmtTable, 
				pfpamIsSetFsusrMgmtTable, OSIX_FALSE) != SNMP_SUCCESS)
	{
		return SNMP_FAILURE;
	}

	return SNMP_SUCCESS;
}




/****************************************************************************
 Function    :  fpamTestAllFsusrMgmtTable
 Input       :  The Indices
                pu4ErrorCode
                pfpamFsusrMgmtTable
                pfpamIsSetFsusrMgmtTable
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4 fpamTestAllFsusrMgmtTable(UINT4 *pu4ErrorCode, tfpamFsusrMgmtTable *pfpamSetFsusrMgmtTable,
				 tfpamIsSetFsusrMgmtTable *pfpamIsSetFsusrMgmtTable,
				 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
	return SNMP_SUCCESS;
}

