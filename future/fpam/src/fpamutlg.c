/************************************ ********************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* $Id: fpamutlg.c,v 1.4 2015/09/21 13:24:52 siva Exp $
*
* Description: This file contains utility functions used by protocol Fpam
*********************************************************************/

#include "fpaminc.h"
#include "snmputil.h"

/****************************************************************************
 Function    :  FpamUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
FpamUtlCreateRBTree ()
{

    if (FpamFsusrMgmtTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamFsusrMgmtTableCreate
 Input       :  None
 Description :  This function creates the FsusrMgmtTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
FpamFsusrMgmtTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tFpamFsUsrMgmtEntry, MibObject.FsusrMgmtTableNode);

    if ((gFpamGlobals.FpamGlbMib.FsusrMgmtTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsusrMgmtTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsusrMgmtTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsusrMgmtTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsusrMgmtTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tFpamFsUsrMgmtEntry *pFsUsrMgmtEntry1 = (tFpamFsUsrMgmtEntry *) pRBElem1;
    tFpamFsUsrMgmtEntry *pFsUsrMgmtEntry2 = (tFpamFsUsrMgmtEntry *) pRBElem2;
    if (pFsUsrMgmtEntry1->MibObject.i4FsusrMgmtUserNameLen >
        pFsUsrMgmtEntry2->MibObject.i4FsusrMgmtUserNameLen)
    {
	return 1;    
    }	
    else if(pFsUsrMgmtEntry1->MibObject.i4FsusrMgmtUserNameLen <
            pFsUsrMgmtEntry2->MibObject.i4FsusrMgmtUserNameLen)
    {
	return -1;
    }
    /* Both length's are equal. checking the contents now */

    if (MEMCMP
        (pFsUsrMgmtEntry1->MibObject.au1FsusrMgmtUserName,
	 pFsUsrMgmtEntry2->MibObject.au1FsusrMgmtUserName, 
	 pFsUsrMgmtEntry1->MibObject.i4FsusrMgmtUserNameLen) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsUsrMgmtEntry1->MibObject.au1FsusrMgmtUserName,
              pFsUsrMgmtEntry2->MibObject.au1FsusrMgmtUserName,
              pFsUsrMgmtEntry1->MibObject.i4FsusrMgmtUserNameLen) < 0) 
    {
        return -1;
    }
    /* First index is equal, now check for second index */
    if (pFsUsrMgmtEntry1->MibObject.i4FsusrMgmtAuthStringLen >
        pFsUsrMgmtEntry2->MibObject.i4FsusrMgmtAuthStringLen)
    {
   	return 1; 
    }
    else if (pFsUsrMgmtEntry1->MibObject.i4FsusrMgmtAuthStringLen <
  	     pFsUsrMgmtEntry2->MibObject.i4FsusrMgmtAuthStringLen)
    {
	return -1;    
    }
    /* Both length's are equal. checking the contents now */
    if (MEMCMP
        (pFsUsrMgmtEntry1->MibObject.au1FsusrMgmtAuthString,
         pFsUsrMgmtEntry2->MibObject.au1FsusrMgmtAuthString,
	 pFsUsrMgmtEntry1->MibObject.i4FsusrMgmtAuthStringLen) > 0)
    {
        return 1;
    }
    else if (MEMCMP
             (pFsUsrMgmtEntry1->MibObject.au1FsusrMgmtAuthString,
              pFsUsrMgmtEntry2->MibObject.au1FsusrMgmtAuthString,
              pFsUsrMgmtEntry1->MibObject.i4FsusrMgmtAuthStringLen) < 0)
    {
        return -1;
    }
    return 0;
}

/****************************************************************************
 Function    :  FsusrMgmtTableFilterInputs
 Input       :  The Indices
                pFpamFsUsrMgmtEntry
                pFpamSetFsUsrMgmtEntry
                pFpamIsSetFsUsrMgmtEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsusrMgmtTableFilterInputs (tFpamFsUsrMgmtEntry * pFpamFsUsrMgmtEntry,
                            tFpamFsUsrMgmtEntry * pFpamSetFsUsrMgmtEntry,
                            tFpamIsSetFsUsrMgmtEntry * pFpamIsSetFsUsrMgmtEntry)
{
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserName == OSIX_TRUE)
    {
        if ((MEMCMP (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName,
                     pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName,
                     pFpamSetFsUsrMgmtEntry->MibObject.
                     i4FsusrMgmtUserNameLen) == 0)
            && (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen ==
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen))
            pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserName = OSIX_FALSE;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtAuthString == OSIX_TRUE)
    {
        if ((MEMCMP (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString,
                     pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString,
                     pFpamSetFsUsrMgmtEntry->MibObject.
                     i4FsusrMgmtAuthStringLen) == 0)
            && (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen ==
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen))
            pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtAuthString = OSIX_FALSE;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword == OSIX_TRUE)
    {
        if ((MEMCMP (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
                     pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
                     pFpamSetFsUsrMgmtEntry->MibObject.
                     i4FsusrMgmtUserPasswordLen) == 0)
            && (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen ==
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen))
            pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword = OSIX_FALSE;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege == OSIX_TRUE)
    {
        if (pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege ==
            pFpamSetFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege)
            pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege = OSIX_FALSE;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus == OSIX_TRUE)
    {
        if (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus ==
            pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus)
            pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus = OSIX_FALSE;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime == OSIX_TRUE)
    {
        if (pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime ==
            pFpamSetFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime)
            pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime = OSIX_FALSE;
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus == OSIX_TRUE)
    {
        if (pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus ==
            pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus)
            pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus = OSIX_FALSE;
    }
    if (FPAM_SUCCESS ==
        FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
    {
        if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserConfirmPwd == OSIX_TRUE)
        {
            if ((MEMCMP
                 (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserConfirmPwd,
                  pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserConfirmPwd,
                  pFpamSetFsUsrMgmtEntry->MibObject.
                  i4FsusrMgmtUserConfirmPwdLen) == 0)
                && (pFpamFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserConfirmPwdLen ==
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserConfirmPwdLen))
            {
                pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserConfirmPwd = OSIX_FALSE;
            }
        }
    }
    if ((pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserName == OSIX_FALSE)
        && (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtAuthString == OSIX_FALSE)
        && (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword == OSIX_FALSE)
        && (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege == OSIX_FALSE)
        && (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus == OSIX_FALSE)
        && (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime == OSIX_FALSE)
        && (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus == OSIX_FALSE))
    {
        if (FPAM_SUCCESS ==
            FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
        {
            if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserConfirmPwd ==
                OSIX_FALSE)
            {
                return OSIX_FALSE;
            }
            else
            {
                return OSIX_TRUE;
            }
        }
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FpamSetAllFsusrMgmtTableTrigger
 Input       :  The Indices
                pFpamSetFsUsrMgmtEntry
                pFpamIsSetFsUsrMgmtEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FpamSetAllFsusrMgmtTableTrigger (tFpamFsUsrMgmtEntry * pFpamSetFsUsrMgmtEntry,
                                 tFpamIsSetFsUsrMgmtEntry *
                                 pFpamIsSetFsUsrMgmtEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserNameVal;
    UINT1               au1FsusrMgmtUserNameVal[256];
    tSNMP_OCTET_STRING_TYPE FsusrMgmtAuthStringVal;
    UINT1               au1FsusrMgmtAuthStringVal[256];
    tSNMP_OCTET_STRING_TYPE FsusrMgmtUserPasswordVal;
    UINT1               au1FsusrMgmtUserPasswordVal[256];
    tSNMP_OCTET_STRING_TYPE *pFsusrMgmtUserConfirmPwdVal = NULL;
    BOOL1               bConfirmPwd = OSIX_FALSE;

    if (FPAM_SUCCESS ==
        FpamIsSetUserMgmtFeature (FPAM_CONFIRM_PASSWORD_FEATURE))
    {
        bConfirmPwd = OSIX_TRUE;
    }
    MEMSET (au1FsusrMgmtUserNameVal, 0, sizeof (au1FsusrMgmtUserNameVal));
    FsusrMgmtUserNameVal.pu1_OctetList = au1FsusrMgmtUserNameVal;
    FsusrMgmtUserNameVal.i4_Length = 0;

    MEMSET (au1FsusrMgmtAuthStringVal, 0, sizeof (au1FsusrMgmtAuthStringVal));
    FsusrMgmtAuthStringVal.pu1_OctetList = au1FsusrMgmtAuthStringVal;
    FsusrMgmtAuthStringVal.i4_Length = 0;

    MEMSET (au1FsusrMgmtUserPasswordVal, 0,
            sizeof (au1FsusrMgmtUserPasswordVal));
    FsusrMgmtUserPasswordVal.pu1_OctetList = au1FsusrMgmtUserPasswordVal;
    FsusrMgmtUserPasswordVal.i4_Length = 0;

    pFsusrMgmtUserConfirmPwdVal =
        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (256);
    if (pFsusrMgmtUserConfirmPwdVal == NULL)
    {
        return OSIX_FAILURE;
    }
    pFsusrMgmtUserConfirmPwdVal->i4_Length = 0;
    MEMCPY (FsusrMgmtUserNameVal.pu1_OctetList,
	    pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName,
	    pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen);
    FsusrMgmtUserNameVal.i4_Length =
	pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen;

    MEMCPY (FsusrMgmtAuthStringVal.pu1_OctetList,
	    pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString,
	    pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen);
    FsusrMgmtAuthStringVal.i4_Length =
	pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen;

    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserName == OSIX_TRUE)
    {
        nmhSetCmnNew (FsusrMgmtUserName, 13, FpamMainTaskLock,
                      FpamMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %s %s",
                      &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                      &FsusrMgmtUserNameVal);
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtAuthString == OSIX_TRUE)
    {
        nmhSetCmnNew (FsusrMgmtAuthString, 13, FpamMainTaskLock,
                      FpamMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %s %s",
                      &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                      &FsusrMgmtAuthStringVal);
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword == OSIX_TRUE)
    {
        MEMCPY (FsusrMgmtUserPasswordVal.pu1_OctetList,
                pFpamSetFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen);
        FsusrMgmtUserPasswordVal.i4_Length =
            pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen;

        nmhSetCmnNew (FsusrMgmtUserPassword, 13, FpamMainTaskLock,
                      FpamMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %s %s",
                      &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                      &FsusrMgmtUserPasswordVal);
        if (bConfirmPwd == OSIX_FALSE)
        {
            MEMCPY (pFsusrMgmtUserConfirmPwdVal->pu1_OctetList,
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    au1FsusrMgmtUserConfirmPwd,
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserConfirmPwdLen);
            pFsusrMgmtUserConfirmPwdVal->i4_Length =
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen;

            nmhSetCmnNew (FsusrMgmtUserConfirmPwd, 13, FpamMainTaskLock,
                          FpamMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %s %s",
                          &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                          pFsusrMgmtUserConfirmPwdVal);
        }
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege == OSIX_TRUE)
    {
        nmhSetCmnNew (FsusrMgmtUserPrivilege, 13, FpamMainTaskLock,
                      FpamMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %s %u",
                      &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                      pFpamSetFsUsrMgmtEntry->MibObject.
                      u4FsusrMgmtUserPrivilege);
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsusrMgmtUserStatus, 13, FpamMainTaskLock,
                      FpamMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %s %i",
                      &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                      pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus);
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime == OSIX_TRUE)
    {
        nmhSetCmnNew (FsusrMgmtUserLockRelTime, 13, FpamMainTaskLock,
                      FpamMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %s %u",
                      &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                      pFpamSetFsUsrMgmtEntry->MibObject.
                      u4FsusrMgmtUserLockRelTime);
    }
    if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsusrMgmtUserRowStatus, 13, FpamMainTaskLock,
                      FpamMainTaskUnLock, 0, 1, 2, i4SetOption, "%s %s %i",
                      &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                      pFpamSetFsUsrMgmtEntry->MibObject.
                      i4FsusrMgmtUserRowStatus);
    }
    if (bConfirmPwd == OSIX_TRUE)
    {
        if (pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserConfirmPwd == OSIX_TRUE)
        {
            MEMCPY (pFsusrMgmtUserConfirmPwdVal->pu1_OctetList,
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    au1FsusrMgmtUserConfirmPwd,
                    pFpamSetFsUsrMgmtEntry->MibObject.
                    i4FsusrMgmtUserConfirmPwdLen);
            pFsusrMgmtUserConfirmPwdVal->i4_Length =
                pFpamSetFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserConfirmPwdLen;

            nmhSetCmnNew (FsusrMgmtUserConfirmPwd, 13, FpamMainTaskLock,
                          FpamMainTaskUnLock, 0, 0, 2, i4SetOption, "%s %s %s",
                          &FsusrMgmtUserNameVal, &FsusrMgmtAuthStringVal,
                          pFsusrMgmtUserConfirmPwdVal);
        }
    }
    free_octetstring (pFsusrMgmtUserConfirmPwdVal);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FpamFsusrMgmtTableCreateApi
 Input       :  pFpamFsUsrMgmtEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tFpamFsUsrMgmtEntry *
FpamFsusrMgmtTableCreateApi (tFpamFsUsrMgmtEntry * pSetFpamFsUsrMgmtEntry)
{
    tFpamFsUsrMgmtEntry *pFpamFsUsrMgmtEntry = NULL;

    if (pSetFpamFsUsrMgmtEntry == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamFsusrMgmtTableCreatApi: pSetFpamFsUsrMgmtEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pFpamFsUsrMgmtEntry =
        (tFpamFsUsrMgmtEntry *) MemAllocMemBlk (FPAM_FSUSRMGMTTABLE_POOLID);
    if (pFpamFsUsrMgmtEntry == NULL)
    {
        FPAM_TRC ((FPAM_UTIL_TRC,
                   "FpamFsusrMgmtTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pFpamFsUsrMgmtEntry, pSetFpamFsUsrMgmtEntry,
                sizeof (tFpamFsUsrMgmtEntry));
        if (RBTreeAdd
            (gFpamGlobals.FpamGlbMib.FsusrMgmtTable,
             (tRBElem *) pFpamFsUsrMgmtEntry) != RB_SUCCESS)
        {
            FPAM_TRC ((FPAM_UTIL_TRC,
                       "FpamFsusrMgmtTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (FPAM_FSUSRMGMTTABLE_POOLID,
                                (UINT1 *) pFpamFsUsrMgmtEntry);
            return NULL;
        }
        return pFpamFsUsrMgmtEntry;
    }
}
