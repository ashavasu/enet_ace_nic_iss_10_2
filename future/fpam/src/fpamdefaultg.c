/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamdefaultg.c,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Fpam 
*********************************************************************/

#include "fpaminc.h"

/****************************************************************************
* Function    : FpamInitializeMibFsusrMgmtTable
* Input       : pFpamFsUsrMgmtEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
FpamInitializeMibFsusrMgmtTable (tFpamFsUsrMgmtEntry * pFpamFsUsrMgmtEntry)
{


    MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword,
            FPAM_DEF_FSUSRMGMTUSERPASSWORD,
            pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen);

    pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen = FPAM_DEF_FSUSRMGMTUSERPASSWORD_LEN;

    pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege = FPAM_DEF_FSUSRMGMTUSERPRIVILEGE;

    pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus = FPAM_DEF_FSUSRMGMTUSERSTATUS;

    pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime = FPAM_DEF_FSUSRMGMTUSERLOCKRELTIME;

    return OSIX_SUCCESS;
}
