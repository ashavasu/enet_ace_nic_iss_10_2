/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamwrg.h,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains the prototype(wr) for Fpam 
*********************************************************************/
#ifndef _FPAMWR_H
#define _FPAMWR_H


INT4 GetNextIndexFsusrMgmtTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsusrMgmtStatsNumOfUsersGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtStatsActiveUsersGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtMinPasswordLenGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidationCharsGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfLowerCaseGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfUpperCaseGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfNumericalsGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfSplCharsGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdMaxLifeTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserPasswordGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserPrivilegeGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserLoginCountGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserLockRelTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtStatsEnableUsersGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtStatsDisableUsersGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserConfirmPwdGet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtMinPasswordLenTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidationCharsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfLowerCaseTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfUpperCaseTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfNumericalsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfSplCharsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdMaxLifeTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserPasswordTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserPrivilegeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserLockRelTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserConfirmPwdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtMinPasswordLenSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidationCharsSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfLowerCaseSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfUpperCaseSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfNumericalsSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdValidateNoOfSplCharsSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtPasswdMaxLifeTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserPasswordSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserPrivilegeSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserLockRelTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtUserConfirmPwdSet(tSnmpIndex *, tRetVal *);
INT4 FsusrMgmtMinPasswordLenDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsusrMgmtPasswdValidationCharsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsusrMgmtPasswdValidateNoOfLowerCaseDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsusrMgmtPasswdValidateNoOfUpperCaseDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsusrMgmtPasswdValidateNoOfNumericalsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsusrMgmtPasswdValidateNoOfSplCharsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsusrMgmtPasswdMaxLifeTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsusrMgmtTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

VOID  RegisterFSUSER PROTO ((VOID));
VOID  UnRegisterFSUSER PROTO ((VOID));

#endif /* _FPAMWR_H */
