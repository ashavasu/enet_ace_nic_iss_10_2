/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Id: fpamextn.h,v 1.1 2012/01/17 10:10:10 siva Exp $
 *
 * Description: This file contains extern declaration of global 
 *              variables of the fpam module.
 *******************************************************************/

#ifndef __FPAMEXTN_H__
#define __FPAMEXTN_H__

PUBLIC tFpamGlobals gFpamGlobals;
PUBLIC BOOL1 gb1FpamDefUserInit;
PUBLIC UINT4 gu4FpamFeatureAddn;
PUBLIC INT1 *gpi1FpamDefaultRootUser;
PUBLIC INT1 *gpi1FpamDefaultRootUserPasswd;

#endif/*__FPAMEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file fpamextn.h                      */
/*-----------------------------------------------------------------------*/

