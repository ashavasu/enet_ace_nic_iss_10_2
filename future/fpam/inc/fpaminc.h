/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fpaminc.h,v 1.4 2012/05/26 13:10:09 siva Exp $
 *
 * Description: This file contains include files of fpam module.
 *******************************************************************/

#ifndef __FPAMINC_H__
#define __FPAMINC_H__ 

#include "lr.h"
#include "cli.h"
#include "fssnmp.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "fpam.h"
#include "ip.h"
#include "iss.h"
#include "snmputil.h"
#include "fpamdefn.h"
#include "fpamtdfsg.h"
#include "fpamtdfs.h"
#include "fpamtmr.h"
#include "fpamtrc.h"

#ifdef __FPAMMAIN_C__

#include "fpamglob.h"
#include "fpamlwg.h"
#include "fpamdefg.h"
#include "fpamsz.h"
#include "fpamwrg.h"

#else

#include "fpamextn.h"
#include "fpammibclig.h"
#include "fpamlwg.h"
#include "fpamdefg.h"
#include "fpamsz.h"
#include "fpamwrg.h"

#endif /* __FPAMMAIN_C__*/

#include "fpamprot.h"
#include "fpamprotg.h"

#endif   /* __FPAMINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file fpaminc.h                       */
/*-----------------------------------------------------------------------*/

