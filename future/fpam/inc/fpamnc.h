/* $Id: fpamnc.h,v 1.1 2016/07/06 10:56:26 siva Exp $
    ISS Wrapper header
    module ARICENT-USERMGM-MIB

 */

#ifndef _H_i_ARICENT_USERMGM_MIB
#define _H_i_ARICENT_USERMGM_MIB

/********************************************************************
* FUNCTION NcFsusrMgmtStatsNumOfUsersGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtStatsNumOfUsersGet (
                UINT4 *pu4FsusrMgmtStatsNumOfUsers );

/********************************************************************
* FUNCTION NcFsusrMgmtStatsActiveUsersGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtStatsActiveUsersGet (
                UINT4 *pu4FsusrMgmtStatsActiveUsers );

/********************************************************************
* FUNCTION NcFsusrMgmtMinPasswordLenSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtMinPasswordLenSet (
                UINT4 u4FsusrMgmtMinPasswordLen );

/********************************************************************
* FUNCTION NcFsusrMgmtMinPasswordLenTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtMinPasswordLenTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtMinPasswordLen );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidationCharsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidationCharsSet (
                UINT4 u4FsusrMgmtPasswdValidationChars );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidationCharsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidationCharsTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidationChars );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidateNoOfLowerCaseSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidateNoOfLowerCaseSet (
                UINT4 u4FsusrMgmtPasswdValidateNoOfLowerCase );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidateNoOfLowerCaseTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidateNoOfLowerCaseTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidateNoOfLowerCase );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidateNoOfUpperCaseSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidateNoOfUpperCaseSet (
                UINT4 u4FsusrMgmtPasswdValidateNoOfUpperCase );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidateNoOfUpperCaseTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidateNoOfUpperCaseTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidateNoOfUpperCase );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidateNoOfNumericalsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidateNoOfNumericalsSet (
                UINT4 u4FsusrMgmtPasswdValidateNoOfNumericals );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidateNoOfNumericalsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidateNoOfNumericalsTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidateNoOfNumericals );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidateNoOfSplCharsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidateNoOfSplCharsSet (
                UINT4 u4FsusrMgmtPasswdValidateNoOfSplChars );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdValidateNoOfSplCharsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdValidateNoOfSplCharsTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdValidateNoOfSplChars );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdMaxLifeTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdMaxLifeTimeSet (
                UINT4 u4FsusrMgmtPasswdMaxLifeTime );

/********************************************************************
* FUNCTION NcFsusrMgmtPasswdMaxLifeTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtPasswdMaxLifeTimeTest (UINT4 *pu4Error,
                UINT4 u4FsusrMgmtPasswdMaxLifeTime );

/********************************************************************
* FUNCTION NcFsusrMgmtStatsEnableUsersGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtStatsEnableUsersGet (
                UINT4 *pu4FsusrMgmtStatsEnableUsers );

/********************************************************************
 * * FUNCTION NcFsusrMgmtStatsDisableUsersGet
 * *
 * * Wrapper for nmhGet API's
 * *
 * * RETURNS:
 * *     error status
 * ********************************************************************/
extern INT1 NcFsusrMgmtStatsDisableUsersGet (
                UINT4 *pu4FsusrMgmtStatsDisableUsers );


/********************************************************************
* FUNCTION NcFsusrMgmtUserPasswordSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserPasswordSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT1 *pFsusrMgmtUserPassword );

/********************************************************************
* FUNCTION NcFsusrMgmtUserPasswordTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserPasswordTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT1 *pFsusrMgmtUserPassword );

/********************************************************************
* FUNCTION NcFsusrMgmtUserPrivilegeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserPrivilegeSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT4 u4FsusrMgmtUserPrivilege );

/********************************************************************
* FUNCTION NcFsusrMgmtUserPrivilegeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserPrivilegeTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT4 u4FsusrMgmtUserPrivilege );

/********************************************************************
* FUNCTION NcFsusrMgmtUserLoginCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserLoginCountGet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 *pi4FsusrMgmtUserLoginCount );

/********************************************************************
* FUNCTION NcFsusrMgmtUserStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserStatusSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 i4FsusrMgmtUserStatus );

/********************************************************************
* FUNCTION NcFsusrMgmtUserStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserStatusTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 i4FsusrMgmtUserStatus );

/********************************************************************
* FUNCTION NcFsusrMgmtUserLockRelTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserLockRelTimeSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT4 u4FsusrMgmtUserLockRelTime );

/********************************************************************
* FUNCTION NcFsusrMgmtUserLockRelTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserLockRelTimeTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT4 u4FsusrMgmtUserLockRelTime );

/********************************************************************
* FUNCTION NcFsusrMgmtUserRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserRowStatusSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 i4FsusrMgmtUserRowStatus );

/********************************************************************
* FUNCTION NcFsusrMgmtUserRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserRowStatusTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                INT4 i4FsusrMgmtUserRowStatus );

/********************************************************************
* FUNCTION NcFsusrMgmtUserConfirmPwdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserConfirmPwdSet (
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT1 *pFsusrMgmtUserConfirmPwd );

/********************************************************************
* FUNCTION NcFsusrMgmtUserConfirmPwdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsusrMgmtUserConfirmPwdTest (UINT4 *pu4Error,
                UINT1 *pFsusrMgmtUserName,
                UINT1 *pFsusrMgmtAuthString,
                UINT1 *pFsusrMgmtUserConfirmPwd );

/* END i_ARICENT_USERMGM_MIB.c */


#endif
