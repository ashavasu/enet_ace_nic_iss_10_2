/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsuserdb.h,v 1.6 2012/05/26 13:01:26 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSUSERDB_H
#define _FSUSERDB_H

UINT1 FsusrMgmtTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsuser [] ={1,3,6,1,4,1,29601,2,70};
tSNMP_OID_TYPE fsuserOID = {9, fsuser};


UINT4 FsusrMgmtStatsNumOfUsers [ ] ={1,3,6,1,4,1,29601,2,70,1,1};
UINT4 FsusrMgmtStatsActiveUsers [ ] ={1,3,6,1,4,1,29601,2,70,1,2};
UINT4 FsusrMgmtMinPasswordLen [ ] ={1,3,6,1,4,1,29601,2,70,1,3};
UINT4 FsusrMgmtPasswdValidationChars [ ] ={1,3,6,1,4,1,29601,2,70,1,4};
UINT4 FsusrMgmtPasswdValidateNoOfLowerCase [ ] ={1,3,6,1,4,1,29601,2,70,1,5};
UINT4 FsusrMgmtPasswdValidateNoOfUpperCase [ ] ={1,3,6,1,4,1,29601,2,70,1,6};
UINT4 FsusrMgmtPasswdValidateNoOfNumericals [ ] ={1,3,6,1,4,1,29601,2,70,1,7};
UINT4 FsusrMgmtPasswdValidateNoOfSplChars [ ] ={1,3,6,1,4,1,29601,2,70,1,8};
UINT4 FsusrMgmtPasswdMaxLifeTime [ ] ={1,3,6,1,4,1,29601,2,70,1,9};
UINT4 FsusrMgmtStatsEnableUsers [ ] ={1,3,6,1,4,1,29601,2,70,1,10};
UINT4 FsusrMgmtStatsDisableUsers [ ] ={1,3,6,1,4,1,29601,2,70,1,11};
UINT4 FsusrMgmtUserName [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,1};
UINT4 FsusrMgmtAuthString [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,2};
UINT4 FsusrMgmtUserPassword [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,3};
UINT4 FsusrMgmtUserPrivilege [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,4};
UINT4 FsusrMgmtUserLoginCount [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,5};
UINT4 FsusrMgmtUserStatus [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,6};
UINT4 FsusrMgmtUserLockRelTime [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,7};
UINT4 FsusrMgmtUserRowStatus [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,8};
UINT4 FsusrMgmtUserConfirmPwd [ ] ={1,3,6,1,4,1,29601,2,70,2,1,1,9};




tMbDbEntry fsuserMibEntry[]= {

{{11,FsusrMgmtStatsNumOfUsers}, NULL, FsusrMgmtStatsNumOfUsersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsusrMgmtStatsActiveUsers}, NULL, FsusrMgmtStatsActiveUsersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsusrMgmtMinPasswordLen}, NULL, FsusrMgmtMinPasswordLenGet, FsusrMgmtMinPasswordLenSet, FsusrMgmtMinPasswordLenTest, FsusrMgmtMinPasswordLenDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsusrMgmtPasswdValidationChars}, NULL, FsusrMgmtPasswdValidationCharsGet, FsusrMgmtPasswdValidationCharsSet, FsusrMgmtPasswdValidationCharsTest, FsusrMgmtPasswdValidationCharsDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "15"},

{{11,FsusrMgmtPasswdValidateNoOfLowerCase}, NULL, FsusrMgmtPasswdValidateNoOfLowerCaseGet, FsusrMgmtPasswdValidateNoOfLowerCaseSet, FsusrMgmtPasswdValidateNoOfLowerCaseTest, FsusrMgmtPasswdValidateNoOfLowerCaseDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsusrMgmtPasswdValidateNoOfUpperCase}, NULL, FsusrMgmtPasswdValidateNoOfUpperCaseGet, FsusrMgmtPasswdValidateNoOfUpperCaseSet, FsusrMgmtPasswdValidateNoOfUpperCaseTest, FsusrMgmtPasswdValidateNoOfUpperCaseDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsusrMgmtPasswdValidateNoOfNumericals}, NULL, FsusrMgmtPasswdValidateNoOfNumericalsGet, FsusrMgmtPasswdValidateNoOfNumericalsSet, FsusrMgmtPasswdValidateNoOfNumericalsTest, FsusrMgmtPasswdValidateNoOfNumericalsDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsusrMgmtPasswdValidateNoOfSplChars}, NULL, FsusrMgmtPasswdValidateNoOfSplCharsGet, FsusrMgmtPasswdValidateNoOfSplCharsSet, FsusrMgmtPasswdValidateNoOfSplCharsTest, FsusrMgmtPasswdValidateNoOfSplCharsDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsusrMgmtPasswdMaxLifeTime}, NULL, FsusrMgmtPasswdMaxLifeTimeGet, FsusrMgmtPasswdMaxLifeTimeSet, FsusrMgmtPasswdMaxLifeTimeTest, FsusrMgmtPasswdMaxLifeTimeDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsusrMgmtStatsEnableUsers}, NULL, FsusrMgmtStatsEnableUsersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsusrMgmtStatsDisableUsers}, NULL, FsusrMgmtStatsDisableUsersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsusrMgmtUserName}, GetNextIndexFsusrMgmtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsusrMgmtTableINDEX, 2, 0, 0, NULL},

{{13,FsusrMgmtAuthString}, GetNextIndexFsusrMgmtTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsusrMgmtTableINDEX, 2, 0, 0, NULL},

{{13,FsusrMgmtUserPassword}, GetNextIndexFsusrMgmtTable, FsusrMgmtUserPasswordGet, FsusrMgmtUserPasswordSet, FsusrMgmtUserPasswordTest, FsusrMgmtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsusrMgmtTableINDEX, 2, 0, 0, "Password123#"},

{{13,FsusrMgmtUserPrivilege}, GetNextIndexFsusrMgmtTable, FsusrMgmtUserPrivilegeGet, FsusrMgmtUserPrivilegeSet, FsusrMgmtUserPrivilegeTest, FsusrMgmtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsusrMgmtTableINDEX, 2, 0, 0, "1"},

{{13,FsusrMgmtUserLoginCount}, GetNextIndexFsusrMgmtTable, FsusrMgmtUserLoginCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsusrMgmtTableINDEX, 2, 0, 0, NULL},

{{13,FsusrMgmtUserStatus}, GetNextIndexFsusrMgmtTable, FsusrMgmtUserStatusGet, FsusrMgmtUserStatusSet, FsusrMgmtUserStatusTest, FsusrMgmtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsusrMgmtTableINDEX, 2, 0, 0, "1"},

{{13,FsusrMgmtUserLockRelTime}, GetNextIndexFsusrMgmtTable, FsusrMgmtUserLockRelTimeGet, FsusrMgmtUserLockRelTimeSet, FsusrMgmtUserLockRelTimeTest, FsusrMgmtTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsusrMgmtTableINDEX, 2, 0, 0, "0"},

{{13,FsusrMgmtUserRowStatus}, GetNextIndexFsusrMgmtTable, FsusrMgmtUserRowStatusGet, FsusrMgmtUserRowStatusSet, FsusrMgmtUserRowStatusTest, FsusrMgmtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsusrMgmtTableINDEX, 2, 0, 1, NULL},

{{13,FsusrMgmtUserConfirmPwd}, GetNextIndexFsusrMgmtTable, FsusrMgmtUserConfirmPwdGet, FsusrMgmtUserConfirmPwdSet, FsusrMgmtUserConfirmPwdTest, FsusrMgmtTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsusrMgmtTableINDEX, 2, 0, 0, "Password123#"},
};
tMibData fsuserEntry = { 20, fsuserMibEntry };

#endif /* _FSUSERDB_H */

