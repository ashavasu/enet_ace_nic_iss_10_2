/********************************************************************
 * Copyright (C) Future Software Limited, 2001
 *
 * Id: fpamtmr.h,v 1.1 2012/01/17 10:10:10 siva Exp $
 *
 * Description: This file contains definitions for fpam Timer
 *******************************************************************/

#ifndef __FPAMTMR_H__
#define __FPAMTMR_H__



/* constants for timer types */
typedef enum {
	FPAM_TMR1 =0,
	FPAM_MAX_TMRS = 1 
} enFpamTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _FPAM_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tFpamTmrDesc;

typedef struct _FPAM_TIMER {
       tTmrAppTimer    tmrNode;
       enFpamTmrId     eFpamTmrId;
} tFpamTmr;




#endif  /* __FPAMTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  fpamtmr.h                      */
/*-----------------------------------------------------------------------*/
