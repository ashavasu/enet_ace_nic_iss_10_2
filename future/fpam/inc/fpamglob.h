/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Id: fpamglob.h,v 1.1 2012/01/17 10:10:10 siva Exp $
 *
 * Description: This file contains declaration of global variables 
 *              of fpam module.
 *******************************************************************/

#ifndef __FPAMGLOBAL_H__
#define __FPAMGLOBAL_H__

tFpamGlobals gFpamGlobals;
BOOL1 gb1FpamDefUserInit;
UINT4 gu4FpamFeatureAddn;

#endif  /* __FPAMGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file fpamglob.h                      */
/*-----------------------------------------------------------------------*/
