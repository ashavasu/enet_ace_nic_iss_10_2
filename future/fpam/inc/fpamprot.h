/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fpamprot.h,v 1.12 2017/09/12 13:26:54 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of fpam module.
 *******************************************************************/

#ifndef __FPAMPROT_H__
#define __FPAMPROT_H__ 
#include "fpamtmr.h"

/* Add Prototypes here */

/**************************fpammain.c*******************************/
PUBLIC UINT4 FpamMainTaskInit         PROTO ((VOID));
PUBLIC VOID FpamMainDeInit            PROTO ((VOID));
PUBLIC INT4 FpamMainTaskLock          PROTO ((VOID));
PUBLIC INT4 FpamMainTaskUnLock        PROTO ((VOID));



/**************************fpamque.c********************************/
PUBLIC VOID FpamQueProcessMsgs        PROTO ((VOID));

/**************************fpampkt.c*******************************/
#if 0
PUBLIC tFpamPkt *FpamPktAlloc        PROTO(( VOID ));
PUBLIC VOID FpamPktFree              PROTO(( tFpamPkt *pFpamPkt ));
PUBLIC UINT4  FpamPktSend            PROTO(( tFpamPkt *pFpamPkt ));
PUBLIC UINT4 FpamPktRecv             PROTO(( tFpamPkt *pFpamPkt));
#endif
/**************************fpamtmr.c*******************************/
PUBLIC VOID  FpamTmrInitTmrDesc PROTO (( VOID));
PUBLIC VOID  FpamTmrStartTmr  PROTO ((tFpamTmr * pFpamTmr, enFpamTmrId eFpamTmrId, UINT4 u4Secs));
PUBLIC VOID  FpamTmrRestartTmr PROTO ((tFpamTmr * pFpamTmr, enFpamTmrId eFpamTmrId, UINT4 u4Secs));
PUBLIC VOID  FpamTmrStopTmr          PROTO((tFpamTmr * pFpamTmr));
PUBLIC VOID  FpamTmrHandleExpiry     PROTO((VOID));

/**************************fpamtrc.c*******************************/


CHR1  *FpamTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   FpamTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   FpamTrcWrite      PROTO(( CHR1 *s));

/**************************fpamutl.c********************************/
PUBLIC INT1
FpamSetPasswdValidateChar PROTO ((UINT4 u4MaskValue, UINT4 u4LowerCase, UINT4 u4UpperCase, UINT4 u4Numbers, UINT4 u4Symbols));

PUBLIC INT1
FpamSetPasswdValidateMask PROTO ((UINT4 *pu4Error, UINT4 u4MaskValue));

PUBLIC INT1
FpamSetPwdValidateLwrCaseCharCnt PROTO ((UINT4 *pu4Error, UINT4 u4LowerCase));

PUBLIC INT1
FpamSetPwdValidateUprCaseCharCnt PROTO ((UINT4 *pu4Error, UINT4 u4UpperCase));

PUBLIC INT1
FpamSetPasswdValidateNumberCount PROTO ((UINT4 *pu4Error, UINT4 u4Numbers));

PUBLIC INT1
FpamSetPasswdValidateSymbolCount PROTO ((UINT4 *pu4Error, UINT4 u4Symbols));

PUBLIC INT1
FpamValidatePasswordRules PROTO ((UINT4 u4PasswordMask, UINT4 u4LowerCaseCount,
       UINT4 u4UpperCaseCount, UINT4 u4NumberCount, 
       UINT4 u4SplCharCount));

PUBLIC INT1
FpamShowPasswdValidateRules PROTO ((UINT4 *pu4MaskValue, UINT4 *pu4LowerCase, UINT4 *pu4UpperCase, UINT4 *pu4Numbers, UINT4 *pu4Symbols));

PUBLIC INT1
FpamGetCurrentTime PROTO ((tFpamUtlTm *pTmLoginTime));

PUBLIC INT1
FpamReadUserPasswordCreationTime PROTO ((UINT1 *pi1UserPasswordCreationTime,
     tFpamUtlTm *pTmFsusrMgmtUserPasswordCreationTime)); 

/**************************fpamtask.c*******************************/

PUBLIC INT4 FpamTaskRegisterFpamMibs (VOID);

PUBLIC INT1
FpamWriteUsers PROTO ((VOID));
PUBLIC INT1
FpamReadUsers PROTO ((VOID));
PUBLIC INT4
FpamUtlChkForFsusrMgmtAuthString PROTO ((tSNMP_OCTET_STRING_TYPE * pFsusrMgmtAuthString, BOOL1 bPrivilCheck));
PUBLIC INT4
FpamSplitUserBuf PROTO ((INT1 *pi1Buf, INT1 **ppi1UserName, INT1 **ppi1UserPriv,
                    INT1 **ppi1Mode, INT1 **ppi1Passwd, INT1 **ppi1PasswdCreationTime,
                    INT1 **ppi1ReleaseTime, INT1 **ppi1UserStatus));
PUBLIC INT4
FpamOpenFile PROTO ((CONST CHR1 * pi1Filename, INT4 i4Flag));
PUBLIC INT1
FpamFindCksum PROTO ((INT4 i4Fd, INT2 i2Length, UINT4 u4Sum));
PUBLIC INT1
FpamReadLineFromFile PROTO ((INT4 i4Fd, INT1 *pi1Buf, INT2 i2MaxLen, INT2 *pi2ReadLen));
PUBLIC INT1
FpamVerifyCheckSum PROTO ((INT1 *pData, INT2 i2TotLen, INT2 i2Length,
                   UINT4 u4Sum, UINT2 u2CkSum));
PUBLIC CHR1               *
FpamUtlEncriptPasswd PROTO ((CHR1 * i1passwd));
PUBLIC CHR1               *
FpamUtlDecriptPasswd PROTO ((CHR1 * i1passwd));
PUBLIC INT4
Fpam_copy_index_to_entry PROTO ((tFpamFsUsrMgmtEntry *FpamFsUsrMgmtEntry,tSNMP_OCTET_STRING_TYPE *pFsusrMgmtUserName,tSNMP_OCTET_STRING_TYPE *pFsusrMgmtAuthString));
PUBLIC INT4
FpamUserCompareOldPasswd PROTO ((tSNMP_OCTET_STRING_TYPE * pCheckFsusrMgmtUserName, tSNMP_OCTET_STRING_TYPE * pCheckFsusrMgmtUserPassword));
PUBLIC INT4
FpamUtlSearchRootPrivilege PROTO ((tSNMP_OCTET_STRING_TYPE * pFsusrMgmtUserName));
PUBLIC INT4
FpamUtlMbsmUserUpdate PROTO ((VOID));
PUBLIC INT4
FpamTestPasswd PROTO ((tFpamFsUsrMgmtEntry *pFpamSetFsUsrMgmtEntry, BOOL1 bConfirmPwd));
PUBLIC INT4
FpamTestRowStatus PROTO ((tFpamFsUsrMgmtEntry *pFpamSetFsUsrMgmtEntry));

#endif   /* __FPAMPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  fpamprot.h                     */
/*-----------------------------------------------------------------------*/
