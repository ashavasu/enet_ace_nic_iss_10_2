/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpammibclig.h,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsusrMgmtMinPasswordLen[11];

extern UINT4 FsusrMgmtPasswdValidationChars[13];

extern UINT4 FsusrMgmtPasswdValidateNoOfLowerCase[13];

extern UINT4 FsusrMgmtPasswdValidateNoOfUpperCase[13];

extern UINT4 FsusrMgmtPasswdValidateNoOfNumericals[13];

extern UINT4 FsusrMgmtPasswdValidateNoOfSplChars[13];

extern UINT4 FsusrMgmtPasswdMaxLifeTime[13];

extern UINT4 FsusrMgmtUserName[13];

extern UINT4 FsusrMgmtAuthString[13];

extern UINT4 FsusrMgmtUserPassword[13];

extern UINT4 FsusrMgmtUserPrivilege[13];

extern UINT4 FsusrMgmtUserStatus[13];

extern UINT4 FsusrMgmtUserLockRelTime[13];

extern UINT4 FsusrMgmtUserRowStatus[13];

extern UINT4 FsusrMgmtUserConfirmPwd[13];
