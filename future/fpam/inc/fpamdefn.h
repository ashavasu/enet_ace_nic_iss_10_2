/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Id: fpamdefn.h,v 1.1 2012/01/17 10:10:10 siva Exp $
 *
 * Description: This file contains definitions for fpam module.
 *******************************************************************/

#ifndef __FPAMDEFN_H__
#define __FPAMDEFN_H__

#define FPAM_SUCCESS        (OSIX_SUCCESS)
#define FPAM_FAILURE        (OSIX_FAILURE)

#define  FPAM_TASK_PRIORITY              100
#define  FPAM_QUEUE_NAME                 (const UINT1 *) "FPAMQ"
#define  FPAM_MUT_EXCL_SEM_NAME          (const UINT1 *) "FPAMM"
#define  FPAM_QUEUE_DEPTH                100
#define  FPAM_SEM_CREATE_INIT_CNT        1

#define FPAM_LOCK  FpamMainTaskLock ()
#define FPAM_UNLOCK  FpamMainTaskUnLock ()


#define  FPAM_ENABLED                    1
#define  FPAM_DISABLED                   2

#define  FPAM_TIMER_EVENT             0x00000001 
#define  FPAM_QUEUE_EVENT             0x00000002
#define  MAX_FPAM_DUMMY                   1

#define FPAM_MAX_PASSWD_LEN       20
#define FPAM_MIN_PASSWD_LEN       8
#define FPAM_MIN_SET_PASSWD_LEN   8
#define FPAM_MAX_USERNAME_LEN     20
#define FPAM_MIN_USERNAME_LEN       1
#define FPAM_MIN_SET_PASSWD_VALIDATION_CHAR      1
#define FPAM_MAX_SET_PASSWD_VALIDATION_CHAR      15
#define FPAM_MAX_SET_PASSWD_MAX_LOWER_CASE_CHAR  FPAM_MAX_PASSWD_LEN 
#define FPAM_MAX_SET_PASSWD_MAX_UPPER_CASE_CHAR  FPAM_MAX_PASSWD_LEN 
#define FPAM_MAX_SET_PASSWD_MAX_NUMERIC_CHAR     FPAM_MAX_PASSWD_LEN 
#define FPAM_MAX_SET_PASSWD_MAX_SPECIAL_CHAR     FPAM_MAX_PASSWD_LEN 
#define FPAM_MAX_SET_PASSWORD_LIFE_TIME   366

#define FPAM_DEF_PASSWORD_VALIDATION_CHARS 15
#define FPAM_DEF_PASSWORD_LOWER_CASE_CHARS 1
#define FPAM_DEF_PASSWORD_UPPER_CASE_CHARS 1 
#define FPAM_DEF_PASSWORD_NUMERIC_CHARS    1
#define FPAM_DEF_PASSWORD_SPECIAL_CHARS    1
#define FPAM_DEF_PASSWORD_MAX_LIFE_TIME    0
#define    FPAM_MAX_USERS_LINE_LEN     200
#define    FPAM_MAX_USERS 20

/* For Password Configuration */
/* To check the password has numeral character within it - 0(ascii 48) to 9(ascii 57) */
#define FPAM_NUM_CHAR_START 48
#define FPAM_NUM_CHAR_END   57
/* To check the password has upper case alphabet within it - A(ascii 65) to Z(ascii 90) */
#define FPAM_UPPER_CHAR_START  65
#define FPAM_UPPER_CHAR_END    90
/* To check the password has lower case alphabet within it - a(ascii 97) to z(ascii 122) */
#define FPAM_LOWER_CHAR_START  97
#define FPAM_LOWER_CHAR_END    122
/* Special Character */
#define FPAM_ALLOWED_SPL_CHAR_LIST  "!@#$%^&*()>`~_- +=|\\{}[]:;\"\'<,.?/"

#define FPAM_SPL_CHAR_EXCLAIM     33    /* ! */
#define FPAM_SPL_CHAR_AT     64    /* @ */
#define FPAM_SPL_CHAR_HASH     35    /* # */
#define FPAM_SPL_CHAR_DOLLAR     36    /* $ */
#define FPAM_SPL_CHAR_MOD     37    /* % */
#define FPAM_SPL_CHAR_XOR     94    /* ^ */
#define FPAM_SPL_CHAR_AND     38    /* & */
#define FPAM_SPL_CHAR_ASTERIK     42    /* * */
#define FPAM_SPL_CHAR_OPEN_BRACES     40    /* ( */
#define FPAM_SPL_CHAR_CLOSE_BRACES    41    /* ) */
#define FPAM_SPL_CHAR_GREATER_THAN    62    /* > */

#define FPAM_MIN_DIFF_FROM_OLD_PWD  4

#define FPAM_DAYS_IN_A_YEAR 365
#define FPAM_LEAP_YEARS_BETWEEN(year1, year2) (((year1) - (year2))/4) 
#define FPAM_NO_OF_TIME_FIELDS 9
#define FPAM_TIME_FIELD_SEPERATOR  '|'
#define FPAM_TIME_STAMP_LEN  100
#define FPAM_PASSWORD_VALIDATE_MASK gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidationChars 
#define FPAM_MIN_LOWERCASE_COUNT    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfLowerCase
#define FPAM_MIN_UPPERCASE_COUNT    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfUpperCase
#define FPAM_MIN_NUMERICALS_COUNT   gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfNumericals 
#define FPAM_MIN_SPL_CHARS_COUNT    gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdValidateNoOfSplChars
#define FPAM_PASSWORD_MAX_LIFE_TIME gFpamGlobals.FpamGlbMib.u4FsusrMgmtPasswdMaxLifeTime
#define FPAM_PASSWORD_NO_EXPIRY 0

#define FPAM_LOWERCASE_MASK 0x1
#define FPAM_UPPERCASE_MASK 0x2
#define FPAM_NUMBER_MASK    0x4
#define FPAM_SYMBOL_MASK    0x8
#define FPAM_MAX_DELIMITER  6
/* Backward compatabilty - delimiter value */
#define FPAM_MAX_BC_DELIMITER  3


#define FPAM_GET_IFACE_TYPE       1
#define FPAM_GET_IFACE_POS        2
#define FPAM_GET_IFACE_STATUS     3

#define MAX_BUF_LEN                    64
#define MAX_IFC_LEN                4

#define FPAM_ROOT_PRIVILEGE_ID 15
#define FPAM_ROOT_USER            gpi1FpamDefaultRootUser  
#define FPAM_ROOT_PSWD            gpi1FpamDefaultRootUserPasswd
#define FPAM_DEF_ROOT_USER          "root"
#define FPAM_DEF_ROOT_PASSWD        "admin123"
#define FPAM_AUTH_STRING "authenticationstring"
#define FPAM_AUTH_STRING_MAX_LEN FPAM_MAX_PASSWD_LEN+FPAM_MAX_USERNAME_LEN+1
#define FPAM_AUTH_STRING_MIN_LEN FPAM_MIN_PASSWD_LEN+FPAM_MIN_USERNAME_LEN+1
#define FPAM_GUEST_USER ISS_CUST_SYS_DEF_GUEST_USER
#define FPAM_GUEST_PSWD ISS_CUST_SYS_DEF_GUEST_PASSWD
#define FPAM_MBSM_USER     "chassisuser"
#define FPAM_MBSM_PSWD   "chassis123"
#define FPAM_MBSM_PRIVILEGE_ID  1
#define FPAM_MIN_LEN 0
#define FPAM_MAX_USERS_LINE_LEN 200
#define FPAM_USER_FILE_NAME FLASH_USERS ISS_USERS_FILE_NAME
#define FPAM_USER_FILE_WRITE_ONLY 2
#define FPAM_USER_FILE_READ_ONLY 1
/* File Handling */
/* Not End Of File */
#define FPAM_NO_EOF 1
/* End Of File */
#define FPAM_EOF    2

#define FPAM_FALSE  0
#define FPAM_TRUE   1

#define FPAM_DEF_MODE "/"

#define PRINT_PASSWORD_RULES() \
  mmi_printf ("\r\n%% Ensure the following rules are met:\r\n"\
              "\r%% Password length should be in the range of %d - %d !! "\
               "characters\r\n",\
               gFpamGlobals.FpamGlbMib.u4FsusrMgmtMinPasswordLen,\
               FPAM_MAX_PASSWD_LEN);\
if((FPAM_PASSWORD_VALIDATE_MASK & FPAM_LOWERCASE_MASK) == FPAM_LOWERCASE_MASK)\
{\
  mmi_printf ("\r%% Password should contain atleast %d lowercase "\
                                  "characters !!\r\n", FPAM_MIN_LOWERCASE_COUNT);\
}\
else\
{\
 mmi_printf ("\r%% Passwords with lowercase characters are disabled "\
                                 "by the administrator !!\r\n");\
}\
if((FPAM_PASSWORD_VALIDATE_MASK & FPAM_UPPERCASE_MASK) == FPAM_UPPERCASE_MASK)\
{\
  mmi_printf ("\r%% Password should contain atleast %d uppercase "\
                                  "characters !!\r\n", FPAM_MIN_UPPERCASE_COUNT);\
} else\
{\
  mmi_printf ("\r%% Passwords with uppercase characters are disabled "\
                                  "by the administrator !!\r\n");\
}\
if((FPAM_PASSWORD_VALIDATE_MASK & FPAM_NUMBER_MASK) == FPAM_NUMBER_MASK)\
{\
  mmi_printf ("\r%% Password should contain atleast %d numerical "\
                                  "characters !!\r\n", FPAM_MIN_NUMERICALS_COUNT);\
}\
else\
{\
   mmi_printf ("\r%% Passwords with numeric characters are disabled"\
                                   " by the administrator !!\r\n");\
}\
if((FPAM_PASSWORD_VALIDATE_MASK & FPAM_SYMBOL_MASK) == FPAM_SYMBOL_MASK)\
{\
   mmi_printf ("\r%% Password should contain atleast %d special "\
                                   "characters !!\r\n", FPAM_MIN_SPL_CHARS_COUNT);\
}\
else\
{\
    mmi_printf ("\r%% Passwords with symbols are disabled are disabled"\
                                    " by the administrator !!\r\n");\
}\
 mmi_printf ("\r%% New Password should contain atleast %d characters "\
                             "different from old password !!\r\n",\
                                                 FPAM_MIN_DIFF_FROM_OLD_PWD);
#endif  /* __FPAMDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file fpamdefn.h                      */
/*-----------------------------------------------------------------------*/

