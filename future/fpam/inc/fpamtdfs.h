/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamtdfs.h,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains type definitions for Fpam module.
*********************************************************************/

typedef struct

{

 tRBTree   FsusrMgmtTable; UINT4 u4FsusrMgmtStatsNumOfUsers;
 UINT4 u4FsusrMgmtStatsActiveUsers;
 UINT4 u4FsusrMgmtMinPasswordLen;
 UINT4 u4FsusrMgmtPasswdValidationChars;
 UINT4 u4FsusrMgmtPasswdValidateNoOfLowerCase;
 UINT4 u4FsusrMgmtPasswdValidateNoOfUpperCase;
 UINT4 u4FsusrMgmtPasswdValidateNoOfNumericals;
 UINT4 u4FsusrMgmtPasswdValidateNoOfSplChars;
 UINT4 u4FsusrMgmtPasswdMaxLifeTime;
 UINT4 u4FsusrMgmtStatsEnableUsers;
 UINT4 u4FsusrMgmtStatsDisableUsers;
} tFpamGlbMib;

typedef struct
{
 tFpamMibFsUsrMgmtEntry       MibObject;
} tFpamFsUsrMgmtEntry;



#ifndef __FPAMTDFS_H__
#define __FPAMTDFS_H__

/* -------------------------------------------------------
 *
 *                FPAM   Data Structures
 *
 * ------------------------------------------------------*/


typedef struct FPAM_GLOBALS {
 tTimerListId        fpamTmrLst;
 UINT4               u4FpamTrc;
 tOsixTaskId         fpamTaskId;
 UINT1               au1TaskSemName[8];
 tOsixSemId          fpamTaskSemId;
 tOsixQId            fpamQueId;
 tFpamGlbMib           FpamGlbMib;
} tFpamGlobals;




#endif  /* __FPAMTDFS_H__ */
/*-----------------------------------------------------------------------*/


