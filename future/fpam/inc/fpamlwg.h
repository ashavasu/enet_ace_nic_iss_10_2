/********************************************************************
* Copyright (C) 20112006 Aricent Inc . All Rights Reserved
*
* Id: fpamlwg.h,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsusrMgmtTable. */
INT1
nmhValidateIndexInstanceFsusrMgmtTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsusrMgmtTable  */

INT1
nmhGetFirstIndexFsusrMgmtTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsusrMgmtTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtStatsNumOfUsers ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtStatsActiveUsers ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtMinPasswordLen ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtPasswdValidationChars ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtPasswdValidateNoOfLowerCase ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtPasswdValidateNoOfUpperCase ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtPasswdValidateNoOfNumericals ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtPasswdValidateNoOfSplChars ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtPasswdMaxLifeTime ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtUserPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtUserPrivilege ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtUserLoginCount ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtUserStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtUserLockRelTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtUserRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtStatsEnableUsers ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtStatsDisableUsers ARG_LIST((UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsusrMgmtUserConfirmPwd ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtMinPasswordLen ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtPasswdValidationChars ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtPasswdValidateNoOfLowerCase ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtPasswdValidateNoOfUpperCase ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtPasswdValidateNoOfNumericals ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtPasswdValidateNoOfSplChars ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtPasswdMaxLifeTime ARG_LIST((UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtUserPassword ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtUserPrivilege ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtUserStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtUserLockRelTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtUserRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsusrMgmtUserConfirmPwd ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtMinPasswordLen ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtPasswdValidationChars ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtPasswdValidateNoOfLowerCase ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtPasswdValidateNoOfUpperCase ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtPasswdValidateNoOfNumericals ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtPasswdValidateNoOfSplChars ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtPasswdMaxLifeTime ARG_LIST((UINT4 * , UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtUserPassword ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtUserPrivilege ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtUserStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtUserLockRelTime ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtUserRowStatus ARG_LIST((UINT4 * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsusrMgmtUserConfirmPwd ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsusrMgmtMinPasswordLen ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsusrMgmtPasswdValidationChars ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsusrMgmtPasswdValidateNoOfLowerCase ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsusrMgmtPasswdValidateNoOfUpperCase ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsusrMgmtPasswdValidateNoOfNumericals ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsusrMgmtPasswdValidateNoOfSplChars ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsusrMgmtPasswdMaxLifeTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsusrMgmtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
