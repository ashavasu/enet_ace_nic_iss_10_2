/********************************************************************
 * Copyright (C) Future Software Limited,1997-98,2001
 *
 * Id: fpamtrc.h,v 1.1 2012/01/17 10:10:10 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef __FPAMTRC_H__
#define __FPAMTRC_H__

#define  FPAM_TRC_FLAG  gFpamGlobals.u4FpamTrc
#define  FPAM_NAME      "FPAM"               

#ifdef FPAM_TRACE_WANTED



#define  FPAM_TRC(x)       FpamTrcPrint( __FILE__, __LINE__, FpamTrc x)
#define  FPAM_TRC_FUNC(x)  FpamTrcPrint( __FILE__, __LINE__, FpamTrc x)
#define  FPAM_TRC_CRIT(x)  FpamTrcPrint( __FILE__, __LINE__, FpamTrc x)
#define  FPAM_TRC_PKT(x)   FpamTrcWrite( FpamTrc x)




#else /* FPAM_TRACE_WANTED */

#define  FPAM_TRC(x) 
#define  FPAM_TRC_FUNC(x)
#define  FPAM_TRC_CRIT(x)
#define  FPAM_TRC_PKT(x)

#endif /* FPAM_TRACE_WANTED */


#define  FPAM_FN_ENTRY  (0x1 << 9)
#define  FPAM_FN_EXIT   (0x1 << 10)
#define  FPAM_CLI_TRC   (0x1 << 11)
#define  FPAM_MAIN_TRC  (0x1 << 12)
#define  FPAM_PKT_TRC   (0x1 << 13)
#define  FPAM_QUE_TRC   (0x1 << 14)
#define  FPAM_TASK_TRC  (0x1 << 15)
#define  FPAM_TMR_TRC   (0x1 << 16)
#define  FPAM_UTIL_TRC  (0x1 << 17)
#define  FPAM_ALL_TRC   FPAM_FN_ENTRY |\
                        FPAM_FN_EXIT  |\
                        FPAM_CLI_TRC  |\
                        FPAM_MAIN_TRC |\
                        FPAM_PKT_TRC  |\
                        FPAM_QUE_TRC  |\
                        FPAM_TASK_TRC |\
                        FPAM_TMR_TRC  |\
                        FPAM_UTIL_TRC


#endif /* _FPAMTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  fpamtrc.h                      */
/*-----------------------------------------------------------------------*/
