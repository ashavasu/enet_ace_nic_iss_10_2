/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * Id: fpamdefg.h,v 1.1 2012/01/17 10:10:10 siva Exp $
 *
 * Description: Macros used to fill the CLI structure and 
 index value in the respective structure.
 *********************************************************************/


#define FPAM_FSUSRMGMTTABLE_POOLID   FPAMMemPoolIds[MAX_FPAM_FSUSRMGMTTABLE_SIZING_ID]

/* Macro used to fill the CLI structure for FsUsrMgmtEntry 
   using the input given in def file */

#define  FPAM_FILL_FSUSRMGMTTABLE_ARGS(pFpamFsUsrMgmtEntry,\
                                       pFpamIsSetFsUsrMgmtEntry,\
                                       pau1FsusrMgmtUserName,\
                                       pau1FsusrMgmtUserNameLen,\
                                       pau1FsusrMgmtAuthString,\
                                       pau1FsusrMgmtAuthStringLen,\
                                       pau1FsusrMgmtUserPassword,\
                                       pau1FsusrMgmtUserPasswordLen,\
                                       pau1FsusrMgmtUserPrivilege,\
                                       pau1FsusrMgmtUserStatus,\
                                       pau1FsusrMgmtUserLockRelTime,\
                                       pau1FsusrMgmtUserRowStatus)\
{\
    if (pau1FsusrMgmtUserName != NULL)\
    {\
        MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName, pau1FsusrMgmtUserName, *(INT4 *)pau1FsusrMgmtUserNameLen);\
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen = *(INT4 *)pau1FsusrMgmtUserNameLen;\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserName = OSIX_TRUE;\
    }\
    else\
    {\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserName = OSIX_FALSE;\
    }\
    if (pau1FsusrMgmtAuthString != NULL)\
    {\
        MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString, pau1FsusrMgmtAuthString, *(INT4 *)pau1FsusrMgmtAuthStringLen);\
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen = *(INT4 *)pau1FsusrMgmtAuthStringLen;\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtAuthString = OSIX_TRUE;\
    }\
    else\
    {\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtAuthString = OSIX_FALSE;\
    }\
    if (pau1FsusrMgmtUserPassword != NULL)\
    {\
        MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserPassword, pau1FsusrMgmtUserPassword, *(INT4 *)pau1FsusrMgmtUserPasswordLen);\
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserPasswordLen = *(INT4 *)pau1FsusrMgmtUserPasswordLen;\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword = OSIX_TRUE;\
    }\
    else\
    {\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPassword = OSIX_FALSE;\
    }\
    if (pau1FsusrMgmtUserPrivilege != NULL)\
    {\
        pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserPrivilege = *(UINT4 *) (pau1FsusrMgmtUserPrivilege);\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege = OSIX_TRUE;\
    }\
    else\
    {\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserPrivilege = OSIX_FALSE;\
    }\
    if (pau1FsusrMgmtUserStatus != NULL)\
    {\
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserStatus = *(INT4 *) (pau1FsusrMgmtUserStatus);\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus = OSIX_TRUE;\
    }\
    else\
    {\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserStatus = OSIX_FALSE;\
    }\
    if (pau1FsusrMgmtUserLockRelTime != NULL)\
    {\
        pFpamFsUsrMgmtEntry->MibObject.u4FsusrMgmtUserLockRelTime = *(UINT4 *) (pau1FsusrMgmtUserLockRelTime);\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime = OSIX_TRUE;\
    }\
    else\
    {\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserLockRelTime = OSIX_FALSE;\
    }\
    if (pau1FsusrMgmtUserRowStatus != NULL)\
    {\
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserRowStatus = *(INT4 *) (pau1FsusrMgmtUserRowStatus);\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus = OSIX_TRUE;\
    }\
    else\
    {\
        pFpamIsSetFsUsrMgmtEntry->bFsusrMgmtUserRowStatus = OSIX_FALSE;\
    }\
}
/* Macro used to fill the index values in  structure for FsUsrMgmtEntry 
   using the input given in def file */

#define  FPAM_FILL_FSUSRMGMTTABLE_INDEX(pFpamFsUsrMgmtEntry,\
                                        pau1FsusrMgmtUserName,\
                                        pau1FsusrMgmtAuthString)\
{\
    if (pau1FsusrMgmtUserName != NULL)\
    {\
        MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtUserName, pau1FsusrMgmtUserName, *(INT4 *)pau1FsusrMgmtUserNameLen);\
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtUserNameLen = *(INT4 *)pau1FsusrMgmtUserNameLen;\
    }\
    if (pau1FsusrMgmtAuthString != NULL)\
    {\
        MEMCPY (pFpamFsUsrMgmtEntry->MibObject.au1FsusrMgmtAuthString, pau1FsusrMgmtAuthString, *(INT4 *)pau1FsusrMgmtAuthStringLen);\
        pFpamFsUsrMgmtEntry->MibObject.i4FsusrMgmtAuthStringLen = *(INT4 *)pau1FsusrMgmtAuthStringLen;\
    }\
}




/* Macro used to convert the input 
   to required datatype for FsusrMgmtMinPasswordLen*/

#define  FPAM_FILL_FSUSRMGMTMINPASSWORDLEN(u4FsusrMgmtMinPasswordLen,\
                                           pau1FsusrMgmtMinPasswordLen)\
{\
    if (pau1FsusrMgmtMinPasswordLen != NULL)\
    {\
        u4FsusrMgmtMinPasswordLen = *(UINT4 *) (pau1FsusrMgmtMinPasswordLen);\
    }\
}


/* Macro used to convert the input 
   to required datatype for FsusrMgmtPasswdValidationChars*/

#define  FPAM_FILL_FSUSRMGMTPASSWDVALIDATIONCHARS(u4FsusrMgmtPasswdValidationChars,\
                                                  pau1FsusrMgmtPasswdValidationChars)\
{\
    if (pau1FsusrMgmtPasswdValidationChars != NULL)\
    {\
        u4FsusrMgmtPasswdValidationChars = *(UINT4 *) (pau1FsusrMgmtPasswdValidationChars);\
    }\
}


/* Macro used to convert the input 
   to required datatype for FsusrMgmtPasswdValidateNoOfLowerCase*/

#define  FPAM_FILL_FSUSRMGMTPASSWDVALIDATENOOFLOWERCASE(u4FsusrMgmtPasswdValidateNoOfLowerCase,\
                                                        pau1FsusrMgmtPasswdValidateNoOfLowerCase)\
{\
    if (pau1FsusrMgmtPasswdValidateNoOfLowerCase != NULL)\
    {\
        u4FsusrMgmtPasswdValidateNoOfLowerCase = *(UINT4 *) (pau1FsusrMgmtPasswdValidateNoOfLowerCase);\
    }\
}


/* Macro used to convert the input 
   to required datatype for FsusrMgmtPasswdValidateNoOfUpperCase*/

#define  FPAM_FILL_FSUSRMGMTPASSWDVALIDATENOOFUPPERCASE(u4FsusrMgmtPasswdValidateNoOfUpperCase,\
                                                        pau1FsusrMgmtPasswdValidateNoOfUpperCase)\
{\
    if (pau1FsusrMgmtPasswdValidateNoOfUpperCase != NULL)\
    {\
        u4FsusrMgmtPasswdValidateNoOfUpperCase = *(UINT4 *) (pau1FsusrMgmtPasswdValidateNoOfUpperCase);\
    }\
}


/* Macro used to convert the input 
   to required datatype for FsusrMgmtPasswdValidateNoOfNumericals*/

#define  FPAM_FILL_FSUSRMGMTPASSWDVALIDATENOOFNUMERICALS(u4FsusrMgmtPasswdValidateNoOfNumericals,\
                                                         pau1FsusrMgmtPasswdValidateNoOfNumericals)\
{\
    if (pau1FsusrMgmtPasswdValidateNoOfNumericals != NULL)\
    {\
        u4FsusrMgmtPasswdValidateNoOfNumericals = *(UINT4 *) (pau1FsusrMgmtPasswdValidateNoOfNumericals);\
    }\
}


/* Macro used to convert the input 
   to required datatype for FsusrMgmtPasswdValidateNoOfSplChars*/

#define  FPAM_FILL_FSUSRMGMTPASSWDVALIDATENOOFSPLCHARS(u4FsusrMgmtPasswdValidateNoOfSplChars,\
                                                       pau1FsusrMgmtPasswdValidateNoOfSplChars)\
{\
    if (pau1FsusrMgmtPasswdValidateNoOfSplChars != NULL)\
    {\
        u4FsusrMgmtPasswdValidateNoOfSplChars = *(UINT4 *) (pau1FsusrMgmtPasswdValidateNoOfSplChars);\
    }\
}


/* Macro used to convert the input 
   to required datatype for FsusrMgmtPasswdMaxLifeTime*/

#define  FPAM_FILL_FSUSRMGMTPASSWDMAXLIFETIME(u4FsusrMgmtPasswdMaxLifeTime,\
                                              pau1FsusrMgmtPasswdMaxLifeTime)\
{\
    if (pau1FsusrMgmtPasswdMaxLifeTime != NULL)\
    {\
        u4FsusrMgmtPasswdMaxLifeTime = *(UINT4 *) (pau1FsusrMgmtPasswdMaxLifeTime);\
    }\
}


