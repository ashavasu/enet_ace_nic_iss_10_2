/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
 Aricent Inc . All Rights Reserved
*
* Id: fpamprotg.h,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* This file contains prototypes for functions defined in Fpam.
*********************************************************************/


#include "cli.h"

INT4 FpamCliSetFsusrMgmtMinPasswordLen (tCliHandle CliHandle,UINT4 *pFsusrMgmtMinPasswordLen);


INT4 FpamCliSetFsusrMgmtPasswdValidationChars (tCliHandle CliHandle,UINT4 *pFsusrMgmtPasswdValidationChars);


INT4 FpamCliSetFsusrMgmtPasswdValidateNoOfLowerCase (tCliHandle CliHandle,UINT4 *pFsusrMgmtPasswdValidateNoOfLowerCase);


INT4 FpamCliSetFsusrMgmtPasswdValidateNoOfUpperCase (tCliHandle CliHandle,UINT4 *pFsusrMgmtPasswdValidateNoOfUpperCase);


INT4 FpamCliSetFsusrMgmtPasswdValidateNoOfNumericals (tCliHandle CliHandle,UINT4 *pFsusrMgmtPasswdValidateNoOfNumericals);


INT4 FpamCliSetFsusrMgmtPasswdValidateNoOfSplChars (tCliHandle CliHandle,UINT4 *pFsusrMgmtPasswdValidateNoOfSplChars);


INT4 FpamCliSetFsusrMgmtPasswdMaxLifeTime (tCliHandle CliHandle,UINT4 *pFsusrMgmtPasswdMaxLifeTime);


INT4 FpamCliSetFsusrMgmtTable (tCliHandle CliHandle,tFpamFsUsrMgmtEntry * pFpamSetFsUsrMgmtEntry,tFpamIsSetFsUsrMgmtEntry * pFpamIsSetFsUsrMgmtEntry);


PUBLIC INT4 FpamUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 FpamLock (VOID);

PUBLIC INT4 FpamUnLock (VOID);
PUBLIC INT4 FsusrMgmtTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 FpamFsusrMgmtTableCreate PROTO ((VOID));

tFpamFsUsrMgmtEntry * FpamFsusrMgmtTableCreateApi PROTO ((tFpamFsUsrMgmtEntry *));
INT4 FpamGetFsusrMgmtStatsNumOfUsers PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtStatsActiveUsers PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtMinPasswordLen PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtPasswdValidationChars PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtPasswdValidateNoOfLowerCase PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtPasswdValidateNoOfUpperCase PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtPasswdValidateNoOfNumericals PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtPasswdValidateNoOfSplChars PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtPasswdMaxLifeTime PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtStatsEnableUsers PROTO ((UINT4 *));
INT4 FpamGetFsusrMgmtStatsDisableUsers PROTO ((UINT4 *));
INT4 FpamGetAllFsusrMgmtTable PROTO ((tFpamFsUsrMgmtEntry *));
INT4 FpamGetAllUtlFsusrMgmtTable PROTO((tFpamFsUsrMgmtEntry *, tFpamFsUsrMgmtEntry *));
INT4 FpamSetFsusrMgmtMinPasswordLen PROTO ((UINT4 ));
INT4 FpamSetFsusrMgmtPasswdValidationChars PROTO ((UINT4 ));
INT4 FpamSetFsusrMgmtPasswdValidateNoOfLowerCase PROTO ((UINT4 ));
INT4 FpamSetFsusrMgmtPasswdValidateNoOfUpperCase PROTO ((UINT4 ));
INT4 FpamSetFsusrMgmtPasswdValidateNoOfNumericals PROTO ((UINT4 ));
INT4 FpamSetFsusrMgmtPasswdValidateNoOfSplChars PROTO ((UINT4 ));
INT4 FpamSetFsusrMgmtPasswdMaxLifeTime PROTO ((UINT4 ));
INT4 FpamSetAllFsusrMgmtTable PROTO ((tFpamFsUsrMgmtEntry *, tFpamIsSetFsUsrMgmtEntry *, INT4 , INT4 ));

INT4 FpamInitializeFsusrMgmtTable PROTO ((tFpamFsUsrMgmtEntry *));

INT4 FpamInitializeMibFsusrMgmtTable PROTO ((tFpamFsUsrMgmtEntry *));
INT4 FpamTestFsusrMgmtMinPasswordLen PROTO ((UINT4 *,UINT4 ));
INT4 FpamTestFsusrMgmtPasswdValidationChars PROTO ((UINT4 *,UINT4 ));
INT4 FpamTestFsusrMgmtPasswdValidateNoOfLowerCase PROTO ((UINT4 *,UINT4 ));
INT4 FpamTestFsusrMgmtPasswdValidateNoOfUpperCase PROTO ((UINT4 *,UINT4 ));
INT4 FpamTestFsusrMgmtPasswdValidateNoOfNumericals PROTO ((UINT4 *,UINT4 ));
INT4 FpamTestFsusrMgmtPasswdValidateNoOfSplChars PROTO ((UINT4 *,UINT4 ));
INT4 FpamTestFsusrMgmtPasswdMaxLifeTime PROTO ((UINT4 *,UINT4 ));
INT4 FpamTestAllFsusrMgmtTable PROTO ((UINT4 *, tFpamFsUsrMgmtEntry *, tFpamIsSetFsUsrMgmtEntry *, INT4 , INT4));
tFpamFsUsrMgmtEntry * FpamGetFsusrMgmtTable PROTO ((tFpamFsUsrMgmtEntry *));
tFpamFsUsrMgmtEntry * FpamGetFirstFsusrMgmtTable PROTO ((VOID));
tFpamFsUsrMgmtEntry * FpamGetNextFsusrMgmtTable PROTO ((tFpamFsUsrMgmtEntry *));
INT4 FsusrMgmtTableFilterInputs PROTO ((tFpamFsUsrMgmtEntry *, tFpamFsUsrMgmtEntry *, tFpamIsSetFsUsrMgmtEntry *));
INT4 FpamUtilUpdateFsusrMgmtTable PROTO ((tFpamFsUsrMgmtEntry *, tFpamFsUsrMgmtEntry *, tFpamIsSetFsUsrMgmtEntry *));
INT4 FpamSetAllFsusrMgmtTableTrigger PROTO ((tFpamFsUsrMgmtEntry *, tFpamIsSetFsUsrMgmtEntry *, INT4));
