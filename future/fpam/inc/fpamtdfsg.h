/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
* Id: fpamtdfsg.h,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* Description: This file contains data structures defined for Fpam module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in FsUsrMgmtEntry */
#include "fpamdefn.h"

typedef struct
{
 BOOL1  bFsusrMgmtUserName;
 BOOL1  bFsusrMgmtAuthString;
 BOOL1  bFsusrMgmtUserPassword;
 BOOL1  bFsusrMgmtUserPrivilege;
 BOOL1  bFsusrMgmtUserStatus;
 BOOL1  bFsusrMgmtUserLockRelTime;
 BOOL1  bFsusrMgmtUserRowStatus;
        BOOL1  bTmFsusrMgmtUserPasswordCreationTime;
 BOOL1  bFsusrMgmtUserConfirmPwd;
} tFpamIsSetFsUsrMgmtEntry;



/* Structure used by Fpam protocol for FsUsrMgmtEntry */
typedef tUtlTm tFpamUtlTm;

typedef struct
{
 tRBNodeEmbd  FsusrMgmtTableNode;
        tFpamUtlTm TmFsusrMgmtUserPasswordCreationTime;
 UINT4 u4FsusrMgmtUserPrivilege;
 UINT4 u4FsusrMgmtUserLockRelTime;
 INT4 i4FsusrMgmtUserLoginCount;
 INT4 i4FsusrMgmtUserStatus;
 INT4 i4FsusrMgmtUserRowStatus;
 INT4 i4FsusrMgmtUserNameLen;
 INT4 i4FsusrMgmtAuthStringLen;
 INT4 i4FsusrMgmtLoginAttempts;
 INT4 i4FsusrMgmtUserPasswordLen;
 INT4 i4FsusrMgmtUserConfirmPwdLen;
 UINT1 au1FsusrMgmtUserName[FPAM_MAX_USERNAME_LEN+1];
 UINT1 au1FsusrMgmtAuthString[FPAM_AUTH_STRING_MAX_LEN+1];
 UINT1 au1FsusrMgmtUserPassword[FPAM_MAX_PASSWD_LEN+1];
 UINT1 au1FsusrMgmtUserConfirmPwd[FPAM_MAX_PASSWD_LEN+1];
 UINT1 au1Pad[3];
} tFpamMibFsUsrMgmtEntry;
