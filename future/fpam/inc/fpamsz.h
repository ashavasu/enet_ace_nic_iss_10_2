/********************************************************************
* Copyright (C) 2011 Aricent Inc . All Rights Reserved
*
 Aricent Inc . All Rights Reserved
*
* Id: fpamsz.h,v 1.1 2012/01/17 10:10:10 siva Exp $
*
* This file contains prototypes for functions defined in Fpam.
*********************************************************************/

enum {
    MAX_FPAM_FSUSRMGMTTABLE_SIZING_ID,
    FPAM_MAX_SIZING_ID
};


#ifdef  _FPAMSZ_C
tMemPoolId FPAMMemPoolIds[ FPAM_MAX_SIZING_ID];
INT4  FpamSizingMemCreateMemPools(VOID);
VOID  FpamSizingMemDeleteMemPools(VOID);
INT4  FpamSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _FPAMSZ_C  */
extern tMemPoolId FPAMMemPoolIds[ ];
extern INT4  FpamSizingMemCreateMemPools(VOID);
extern VOID  FpamSizingMemDeleteMemPools(VOID);
extern INT4  FpamSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _FPAMSZ_C  */


#ifdef  _FPAMSZ_C
tFsModSizingParams FsFPAMSizingParams [] = {
{ "tFpamFsUsrMgmtEntry", "MAX_FPAM_FSUSRMGMTTABLE", sizeof(tFpamFsUsrMgmtEntry),MAX_FPAM_FSUSRMGMTTABLE, MAX_FPAM_FSUSRMGMTTABLE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _FPAMSZ_C  */
extern tFsModSizingParams FsFPAMSizingParams [];
#endif /*  _FPAMSZ_C  */



