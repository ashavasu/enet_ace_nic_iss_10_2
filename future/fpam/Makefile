#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved        ####
#### $Id: Makefile,v 1.5 2016/07/02 10:01:19 siva Exp $          ####
##|                                                               |##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.                   |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  FPAM                           |##
##|                                                               |##
##|    MODULE NAME             ::  FPAM                           |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  18 Jan 2012                    |##
##|                                                               |##
##|    DESCRIPTION             ::  MAKEFILE for FPAM	          |##
##|                                Final Object                   |##
##|                                                               |##
##|###############################################################|##

.PHONY:  all clean tags tgz

include ../LR/make.h
include ../LR/make.rule
FPAM = ${BASE_DIR}/fpam

FPAMSRC=$(FPAM)/src/
FPAMOBJ=$(FPAM)/obj/
FPAMINC=$(FPAM)/inc/
FPAMINCL= \
    -I$(BASE_DIR)/fpam\
    -I$(BASE_DIR)/fpam/src\
    -I$(BASE_DIR)/fpam/inc

FPAM_HEADER_FILES= \
    $(FPAMINC)fpamsz.h \
    $(FPAMINC)fpamtrc.h \
    $(FPAMINC)fpamdefn.h \
    $(FPAMINC)fpamdefg.h \
    $(FPAMINC)fpamextn.h \
    $(FPAMINC)fpaminc.h \
    $(FPAMINC)fpamtdfsg.h \
    $(FPAMINC)fpamglob.h \
    $(FPAMINC)fpamlwg.h \
    $(FPAMINC)fpamtdfs.h \
    $(FPAMINC)fpamprotg.h \
    $(FPAMINC)fpamprot.h \
    $(FPAMINC)fpammibclig.h \
    $(FPAMINC)fpamtmr.h \
    $(FPAMINC)fpamwrg.h 

ifeq ($(NETCONF), YES)
FPAM_HEADER_FILES +=  $(FPAMINC)fpamnc.h
endif
FPAM_INCLUDES=$(COMMON_INCLUDE_DIRS) $(FPAMINCL)

FPAM_COMPILATION_SWITCHES = -DDFPAM_TRACE_WANTED $(FPAM_EXP_FLAGS)

FPAM_FLAGS =$(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)\
            $(FPAM_COMPILATION_SWITCHES)

CC+=$(CC_FLAGS)
FINAL_FPAM_OBJ=$(FPAMOBJ)FutureFpam.o

FPAM_OBJS= \
    $(FPAMOBJ)fpamdbg.o \
    $(FPAMOBJ)fpamlwg.o \
    $(FPAMOBJ)fpamtmr.o \
    $(FPAMOBJ)fpamwrg.o \
    $(FPAMOBJ)fpamdefault.o \
    $(FPAMOBJ)fpamtask.o \
    $(FPAMOBJ)fpamsz.o \
    $(FPAMOBJ)fpamdefaultg.o \
    $(FPAMOBJ)fpamutlg.o \
    $(FPAMOBJ)fpammain.o \
    $(FPAMOBJ)fpamlw.o \
    $(FPAMOBJ)fpamdb.o \
    $(FPAMOBJ)fpamtrc.o \
    $(FPAMOBJ)fpamdsg.o \
    $(FPAMOBJ)fpamutl.o 

ifeq ($(NETCONF), YES)
FPAM_OBJS += \
    $(FPAMOBJ)fpamnc.o
endif

all:  CHECK_OBJ_DIR	${FPAM_OBJS}
	${LD} ${LD_FLAGS} -o $(FINAL_FPAM_OBJ) $(FPAM_OBJS)

CHECK_OBJ_DIR:
ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) obj
endif

ifeq ($(NETCONF), YES)
$(FPAMOBJ)fpamnc.o :  $(FPAMSRC)fpamnc.c  $(FPAM_HEADER_FILES)
	 $(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamnc.c  -o $@
endif 
$(FPAMOBJ)fpamdbg.o :	$(FPAMSRC)fpamdbg.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamdbg.c  -o $@

$(FPAMOBJ)fpamlwg.o :	$(FPAMSRC)fpamlwg.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamlwg.c  -o $@

$(FPAMOBJ)fpamtmr.o :	$(FPAMSRC)fpamtmr.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamtmr.c  -o $@

$(FPAMOBJ)fpamwrg.o :	$(FPAMSRC)fpamwrg.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamwrg.c  -o $@

$(FPAMOBJ)fpamdefault.o :	$(FPAMSRC)fpamdefault.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamdefault.c  -o $@

$(FPAMOBJ)fpamtask.o :	$(FPAMSRC)fpamtask.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamtask.c  -o $@

$(FPAMOBJ)fpamsz.o :	$(FPAMSRC)fpamsz.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamsz.c  -o $@

$(FPAMOBJ)fpamdefaultg.o :	$(FPAMSRC)fpamdefaultg.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamdefaultg.c  -o $@

$(FPAMOBJ)fpamutlg.o :	$(FPAMSRC)fpamutlg.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamutlg.c  -o $@

$(FPAMOBJ)fpammain.o :	$(FPAMSRC)fpammain.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpammain.c  -o $@

$(FPAMOBJ)fpamlw.o :	$(FPAMSRC)fpamlw.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamlw.c  -o $@

$(FPAMOBJ)fpamdb.o :	$(FPAMSRC)fpamdb.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamdb.c  -o $@

$(FPAMOBJ)fpamtrc.o :	$(FPAMSRC)fpamtrc.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamtrc.c  -o $@

$(FPAMOBJ)fpamdsg.o :	$(FPAMSRC)fpamdsg.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamdsg.c  -o $@

$(FPAMOBJ)fpamutl.o :	$(FPAMSRC)fpamutl.c   $(FPAM_HEADER_FILES) 
	$(CC) $(FPAM_FLAGS)  $(FPAM_INCLUDES)  $(FPAMSRC)fpamutl.c  -o $@

clean:
	rm -f $(FPAMOBJ)*.o
tgz:
	make clean
	tar cvfz fpam.tgz src/ inc/ Makefile make.h
tags:
	ctags -uR src/ inc/
pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf ${ISS_PKG_CREATE_DIR}/fpam.tgz  -T ${BASE_DIR}/fpam/FILES.NEW;\
        cd ${CUR_PWD};

