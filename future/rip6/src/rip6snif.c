/* $Id: rip6snif.c,v 1.5 2013/11/22 11:39:24 siva Exp $ */

#include "rip6inc.h"

/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    rip6snif.c
 *    PRINCIPAL AUTHOR             :    Aricent Inc.
 *    SUBSYSTEM NAME               :    RIP6
 *    LANGUAGE                     :    C
 *    TARGET ENVIRONMENT           :    UNIX
 *    DATE OF CREATION             :    9th Jan 2002
 *    DESCRIPTION                  :    This file contains the call back
 *                                      routines provided by RIP6 to get
 *                                      Interface State chage and
 *                                      Route Change Notifications from IP6
 *-----------------------------------------------------------------------------
 * Revision History |
 *-----------------------------------------------------------------------------
 * Name/ Date       |  Reason For Change         |  Description
 *-----------------------------------------------------------------------------
 * SIVARKA/9-01-2002| IP6-RIP6 Decoupling        | Moved RIP6-LLR interfaces 
 *                  |                            | from ip6snif.c to this file.
 *-----------------------------------------------------------------------------
 */

/******************************************************************************
 * DESCRIPTION : Called by LLR to get the Next Profile Index of the given Index.
 * INPUTS      : u1ProfIndex
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS, if entry exists OR RIP6_FAILURE otherwise.
 * NOTES       :
 ******************************************************************************/
INT4
Rip6ProfileGetNextIndex (UINT4 u4InstanceId, UINT2 *u2ProfIndex)
{
    UINT1               u1I;
    INT4                i4MaxProfs = 0;

    i4MaxProfs = RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                           FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                           u4PreAllocatedUnits);

    for (u1I = (UINT1) ((*u2ProfIndex) + RIP6_INITIALIZE_ONE);
         u1I < (UINT1) i4MaxProfs; u1I++)
    {
        if (garip6InstanceDatabase[u4InstanceId]->arip6Profile[u1I]->u1Status
            == IP6_RIP_PROFILE_VALID)
        {
            (*u2ProfIndex) = u1I;
            return RIP6_SUCCESS;
        }
    }

    return RIP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : o Called by RIP6 LLR 
 *               o Checks whether entry exists in Profile table for given Index.
 * INPUTS      : u2ProfIndex
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCES, if entry exists OR RIP6_FAILURE otherwise.
 * NOTES       :
 ******************************************************************************/
INT1
Rip6ProfileEntryExists (UINT4 u4InstanceId, UINT2 u2ProfIndex)
{
    INT4                i4MaxProfs = 0;

    i4MaxProfs = RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                           FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                           u4PreAllocatedUnits);
    if (u2ProfIndex >= i4MaxProfs)
    {
        return RIP6_FAILURE;
    }
    if (garip6InstanceStatus[u4InstanceId] == RIP6_INST_UP)
    {
        if (garip6InstanceDatabase[u4InstanceId]->arip6Profile[u2ProfIndex]->
            u1Status == IP6_RIP_PROFILE_VALID)
        {
            return RIP6_SUCCESS;
        }
    }
    return RIP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : o Called by RIP6 LLR.
 *               o Checks whether entry exists in If Table for given u2IfIndex
 * INPUTS      : u4IfIndex
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCES, if entry exists OR RIP6_FAILURE otherwise.
 * NOTES       :
 ******************************************************************************/
INT1
Rip6IfEntryExists (UINT4 u4IfIndex)
{
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((u4IfIndex == 0) || (u4IfIndex > u4MaxIfs))
    {
        return RIP6_FAILURE;
    }
    if (RIP6_GET_INST_IF_MAP_ENTRY (u4IfIndex)->u4InsIfStatus ==
        RIP6_IFACE_ATTACHED)
    {
        if ((RIP6_GET_IF_ENTRY (u4IfIndex)->u1Status & RIP6_IFACE_ALLOCATED) ==
            RIP6_IFACE_ALLOCATED)
        {
            return RIP6_SUCCESS;
        }
    }
    return RIP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : o Called by RIP6 LLR.
 *               o Checks if Prefix is greater.
 * INPUTS      : pIp6Addr, u1PrefixLen, i1Proto, pRtEntry
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS, if Prefix is Greater OR RIP6_FAILURE otherwise.
 * NOTES       :
 ******************************************************************************/
INT1
Rip6IsPrefixGreater (tIp6Addr * pIp6Addr, UINT1 u1PrefixLen, INT1 i1Proto,
                     tRip6RtEntry * pRtEntry)
{
    if (Ip6IsAddrGreater (pIp6Addr, &pRtEntry->dst) == RIP6_SUCCESS)
    {
        return RIP6_SUCCESS;
    }
    if (Ip6AddrMatch (pIp6Addr, &pRtEntry->dst, IP6_ADDR_SIZE_IN_BITS)
        != RIP6_FALSE)
    {
        if (pRtEntry->u1Prefixlen > u1PrefixLen)
        {
            return RIP6_SUCCESS;
        }

        if (pRtEntry->u1Prefixlen == u1PrefixLen)
        {
            if (pRtEntry->i1Proto > i1Proto)
            {
                return RIP6_SUCCESS;
            }
        }
    }
    return RIP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : o Called by RIP6 LLR.
 *               o Checks if Prefix is Inbetween two given Prefixes.
 * INPUTS      : pIp6Addr, u1PrefixLen, i1Proto, pRtTmp, pRtEntry
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS, if Prefix is Inbetween OR RIP6_FAILURE otherwise.
 * NOTES       :
 ******************************************************************************/
INT1
Rip6IsPrefixInBetween (tIp6Addr * pIp6Addr, UINT1 u1PrefixLen, INT1 i1Proto,
                       tRip6RtEntry * pRtTmp, tRip6RtEntry * pRtEntry)
{
    if (Ip6IsAddrInBetween (pIp6Addr, &pRtTmp->dst,
                            &pRtEntry->dst) == RIP6_SUCCESS)
    {
        return RIP6_SUCCESS;
    }
    if (Ip6AddrMatch (&pRtTmp->dst, &pRtEntry->dst, IP6_ADDR_SIZE_IN_BITS)
        != RIP6_FALSE)
    {
        if (pRtEntry->u1Prefixlen < pRtTmp->u1Prefixlen)
        {
            return RIP6_SUCCESS;
        }
        if (pRtEntry->u1Prefixlen == pRtTmp->u1Prefixlen)
        {
            if (pRtEntry->i1Proto < pRtTmp->i1Proto)
            {
                return RIP6_SUCCESS;
            }
        }
    }
    UNUSED_PARAM (u1PrefixLen);
    UNUSED_PARAM (i1Proto);
    return RIP6_FAILURE;
}

tRip6RtEntry       *
Rip6RouteFindEntry (tIp6Addr * pIp6Dest, UINT1 u1PrefixLen, INT1 i1Proto,
                    UINT4 u4IfaceIndex)
{
    tRip6RtEntry       *pRt = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4InstanceId;
    UINT1               u1LookUp;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((u4IfaceIndex < RIP6_INITIALIZE_ONE) || (u4IfaceIndex > u4MaxIfs))
    {
        return NULL;
    }

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4IfaceIndex)->u4InstanceId;

    if (i1Proto == RIPNG)
    {
        u1LookUp = RIP6_DYNAMIC_LOOKUP;
    }
    else
    {
        u1LookUp = RIP6_STATIC_LOOKUP;
    }

    if (Rip6TrieBestEntry (pIp6Dest, u1LookUp, u1PrefixLen, u4InstanceId,
                           &pRt, &pRibNode) == RIP6_FAILURE)
    {
        return NULL;
    }

    while (pRt)
    {
        if ((pRt->i1Proto == i1Proto) && (pRt->u4Index == u4IfaceIndex))
        {
            /* Found Matching Route. */
            return pRt;
        }
        pRt = pRt->pNext;
    }
    return NULL;
}
