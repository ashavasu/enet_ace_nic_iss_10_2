/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6ip6if.c,v 1.11 2017/12/05 09:37:47 siva Exp $
 *
 * Description: This file contains the call back routines 
 *              provided by RIP6 to get Interface state change and
 *              Route change Notifications from IP6
 *
 *******************************************************************/

#include "rip6inc.h"

/******************************************************************************
 * DESCRIPTION : o Called by IP6 to give Address State Notifications 
 * INPUTS      : pNetIpv6HlParams
 * OUTPUTS     : None
 * RETURNS     : INT1
 * NOTES       : 
 ******************************************************************************/

INT1
Rip6Ip6AddrStatusNotify (tNetIpv6HliParams * pNetIpv6HlParams)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tRip6Ip6Params     *pParams = NULL;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Index > u4MaxIfs)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                      "RIP6: Invalid Interface Index for Address Status "
                      "Change Notification");
        return RIP6_FAILURE;
    }

    /* Check for the Address Type. RIP6 is interest in dealing only with
     * the change in Link-Local/unicast/anycast Address. */
    if (!((pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type ==
           ADDR6_LLOCAL)
          || (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type
              == ADDR6_UNICAST)
          || (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type
              == ADDR6_ANYCAST)))
    {
        /* Address change notification for non Link-Local Address.
         * This is not handled. */
        return RIP6_SUCCESS;
    }

    if (IS_ADDR_UNSPECIFIED
        (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr))
    {
        /* Address is not valid. */
        return RIP6_FAILURE;
    }

    pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tRip6Ip6Params),
                                         RIP6_INITIALIZE_ZERO);
    if (pBuf == NULL)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                      "IP6ADDR: In Rip6Ip6AddrStatusNotify alloc of buffer"
                      " for indicating to RIP failed ");
        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC,
                       RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p",
                       ERROR_FATAL, ERR_BUF_ALLOC, RIP6_INITIALIZE_ZERO);
        return RIP6_FAILURE;
    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "Rip6Ip6Ntfy");

    pParams = (tRip6Ip6Params *) (VOID *) RIP6_BUF_DATA_PTR (pBuf);
    pParams->u4Command = RIP6_ADDR_STATUS_CHANGE;
    pParams->u4Index = pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Index;
    pParams->u1Prefixlen =
        (UINT1) pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.
        Ipv6AddrInfo.u4PrefixLength;
    MEMSET (&pParams->addr, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&pParams->addr,
                 &pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.
                 Ip6Addr);

    if (pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Mask ==
        NETIPV6_ADDRESS_ADD)
    {
        pParams->u1Type = RIP6_ADDR_ADD;
    }
    else
    {
        pParams->u1Type = RIP6_ADDR_DEL;
    }

    if (OsixSendToQ (SELF, (const UINT1 *) RIP6_TASK_CONTROL_QUEUE, pBuf,
                     OSIX_MSG_URGENT) == OSIX_FAILURE)
    {
        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6: In Rip6Ip6AddrStatusChange, enq failed to RIP,"
                       "  Q = %d Buf = %p\n", RIP6_TASK_CONTROL_QUEUE, pBuf);
        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                       "%s gen err: Type = %d ", ERROR_FATAL,
                       ERR_GEN_TQUEUE_FAIL);
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buffer    error occurred, Type = %d Bufptr = %p",
                           ERROR_FATAL, ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) RIP6_TASK_NAME,
                       RIP6_CONTROL_EVENT) == OSIX_FAILURE)
    {
        /* Packet Already posted to Queue. Dont Free the Buffer. */
        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6: In Rip6Ip6AddrStatusChange, event posting failed "
                       "to RIP Q = %d Buf = %p\n",
                       RIP6_TASK_CONTROL_QUEUE, pBuf);
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : o Called by IP6 to give Iface State Notifications 
 * INPUTS      : pNetIpv6HlParams
 * OUTPUTS     : None
 * RETURNS     : INT1
 * NOTES       : 
 ******************************************************************************/

INT1
Rip6Ip6ifStatusNotify (tNetIpv6HliParams * pNetIpv6HlParams)
{

    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tRip6Ip6Params     *pParams = NULL;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    /*
     * Indicate to RIP6 module to add a local route corresponding to
     * this address in the routing table
     */

    /* Assuming IP6 IfIndex 0 is auto tunnel block this update */
    if (pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4Index == 0 ||
        pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4Index > u4MaxIfs)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                      "RIP6: Ignoring the control message as it"
                      "is for IfIndex 0, which is auto tunnel interface");
        return RIP6_FAILURE;
    }

    pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tRip6Ip6Params),
                                         RIP6_INITIALIZE_ZERO);
    if (pBuf == NULL)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                      "IP6ADDR: In Rip6Ip6ifStatusNotify alloc of buffer"
                      " for indicating to RIP failed ");
        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC,
                       RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p",
                       ERROR_FATAL, ERR_BUF_ALLOC, RIP6_INITIALIZE_ZERO);
        return RIP6_FAILURE;
    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "Rip6Ip6Ntfy");
    pParams = (tRip6Ip6Params *) (VOID *) RIP6_BUF_DATA_PTR (pBuf);
    pParams->u4Command = RIP6_INTF_STATUS_CHANGE;
    pParams->u4Index = pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4Index;
    pParams->u1Type =
        (UINT1) pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4Mask;
    pParams->u1Stat =
        (UINT1) pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4IfStat;
    pParams->u1Admin =
        (UINT1) pNetIpv6HlParams->unIpv6HlCmdType.IfStatChange.u4OperStatus;
    if (OsixSendToQ (SELF, (const UINT1 *) RIP6_TASK_CONTROL_QUEUE, pBuf,
                     OSIX_MSG_URGENT) == OSIX_FAILURE)
    {
        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6: In Rip6Ip6ifStatusNotify, enq failed to RIP,"
                       "  Q = %d Buf = %p\n", RIP6_TASK_CONTROL_QUEUE, pBuf);
        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                       "%s gen err: Type = %d ", ERROR_FATAL,
                       ERR_GEN_TQUEUE_FAIL);
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buffer    error occurred, Type = %d Bufptr = %p",
                           ERROR_FATAL, ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) RIP6_TASK_NAME,
                       RIP6_CONTROL_EVENT) == OSIX_FAILURE)
    {
        /* Packet Already posted to Queue. Dont Free the Buffer. */
        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6: In Rip6Ip6ifStatusNotify, event posting failed "
                       "to RIP Q = %d Buf = %p\n",
                       RIP6_TASK_CONTROL_QUEUE, pBuf);
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : o Called by RIP6 to give Route Change Notifications to IP6 
 *             : o This would facilitate IP6, to Add/Delete RIP6 learnt Routes.
 * INPUTS      : pIp6Addr, u1Prefixlen, pNextHop, u1Metric, i1Proto, 
 *             : u4Index, i1RtChgFlag
 * OUTPUTS     : None
 * RETURNS     : VOID
 * NOTES       :
 ******************************************************************************/
INT4
Rip6SendRtChgNotification (tIp6Addr * pIp6Addr, UINT1 u1Prefixlen,
                           tIp6Addr * pNextHop, UINT1 u1Metric, INT1 i1Proto,
                           UINT4 u4Index, UINT1 u1Distance, UINT1 u1RtChgFlag)
{
    tNetIpv6RtInfo      Ip6RtInfo;
#ifdef IP6_WANTED
    INT4                i4Status;
#endif

    MEMSET (&Ip6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    if (i1Proto != IP6_RIP_PROTOID)
    {
        return RIP6_FAILURE;
    }

    Ip6AddrCopy (&Ip6RtInfo.Ip6Dst, pIp6Addr);
    Ip6AddrCopy (&Ip6RtInfo.NextHop, pNextHop);

    Ip6RtInfo.u1Prefixlen = u1Prefixlen;
    Ip6RtInfo.u4Metric = u1Metric;
    Ip6RtInfo.i1Proto = i1Proto;
    Ip6RtInfo.u4Index = u4Index;
    Ip6RtInfo.i1Type = IP6_ROUTE_TYPE_INDIRECT;
    if ((IS_ADDR_UNSPECIFIED (Ip6RtInfo.Ip6Dst))
        && (Ip6RtInfo.u1Prefixlen == 0))
    {
        Ip6RtInfo.i1DefRtrFlag = IP6_DEF_RTR_FLAG;
    }
    else
    {
        Ip6RtInfo.i1DefRtrFlag = 0;
    }
    if (u1RtChgFlag == NETIPV6_DELETE_ROUTE)
    {
        Ip6RtInfo.u4RowStatus = DESTROY;
    }
    else
    {
        Ip6RtInfo.u4RowStatus = ACTIVE;
    }
    Ip6RtInfo.u1Preference = u1Distance;

#ifdef IP6_WANTED
    i4Status = NetIpv6LeakRoute (u1RtChgFlag, &Ip6RtInfo);
    UNUSED_PARAM (i4Status);
#endif
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : IssHealthChkClearCtrRip6
  * INPUTS      : VOID
  * OUTPUTS     : None
 * RETURNS     : None
  ******************************************************************************/

VOID
Rip6HealthChkClearCtr ()
{

    UINT4               u4InstanceId = 0;
    UINT4               u4MaxCxts = 0;
    INT4                i4Ipv6RipIfIndex = 0;
    INT4                i4Ipv6PrevRipIfIndex = 0;

    u4MaxCxts = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                          FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                          u4PreAllocatedUnits);
    for (u4InstanceId = RIP6_INITIALIZE_ZERO; u4InstanceId < u4MaxCxts;
         u4InstanceId++)
    {

        if (nmhGetFirstIndexFsMIrip6RipIfTable
            (u4InstanceId, &i4Ipv6RipIfIndex) != RIP6_FAILURE)
        {
            i4Ipv6RipIfIndex = i4Ipv6RipIfIndex;
            do
            {
                if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
                {
                    return;
                }
                RIP6_TASK_LOCK ();
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4InMessages =
                    0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4InReq = 0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4InRes = 0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4UnknownCmds =
                    0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4OtherVer = 0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4Discards = 0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4OutMessages =
                    0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4OutReq = 0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4OutRes = 0;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4TrigUpdates =
                    0;
                RIP6_TASK_UNLOCK ();
                i4Ipv6PrevRipIfIndex = i4Ipv6RipIfIndex;

            }
            while (nmhGetNextIndexFsMIrip6RipIfTable
                   (u4InstanceId, i4Ipv6PrevRipIfIndex,
                    &i4Ipv6RipIfIndex) != SNMP_FAILURE);
        }

    }

    return;
}

/*PopulateRip6Counters is test code.This function is invoked from ISS.
  It populates the Rip6 counters/statistics for verfying the Rip6 clear counters*/
/******************************************************************************
 * DESCRIPTION : PopulateRip6Counters
 * INPUTS      : VOID
 * OUTPUTS     : None
 * RETURNS     : None
 ******************************************************************************/
VOID
PopulateRip6Counters ()
{

    UINT4               u4InstanceId = 0;
    UINT4               u4MaxCxts = 0;
    INT4                i4Ipv6RipIfIndex = 0;
    INT4                i4Ipv6PrevRipIfIndex = 0;
    UINT4               u4Value = 5;

    u4MaxCxts = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                          FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                          u4PreAllocatedUnits);
    for (u4InstanceId = RIP6_INITIALIZE_ZERO; u4InstanceId < u4MaxCxts;
         u4InstanceId++)
    {

        if (nmhGetFirstIndexFsMIrip6RipIfTable
            (u4InstanceId, &i4Ipv6RipIfIndex) != RIP6_FAILURE)
        {
            if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
            {
                return;
            }

            do
            {
                RIP6_TASK_LOCK ();
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4InMessages =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4InReq =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4InRes =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4UnknownCmds =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4OtherVer =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4Discards =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4OutMessages =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4OutReq =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4OutRes =
                    u4Value;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        i4Ipv6RipIfIndex)->stats.u4TrigUpdates =
                    u4Value;
                RIP6_TASK_UNLOCK ();
                i4Ipv6PrevRipIfIndex = i4Ipv6RipIfIndex;

            }
            while (nmhGetNextIndexFsMIrip6RipIfTable
                   (u4InstanceId, i4Ipv6PrevRipIfIndex,
                    &i4Ipv6RipIfIndex) != SNMP_FAILURE);
        }

    }

    return;
}
