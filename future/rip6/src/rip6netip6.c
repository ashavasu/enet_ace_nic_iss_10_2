/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6netip6.c,v 1.19 2017/12/05 09:37:48 siva Exp $
 *
 * Description: This file contains the routines for 
 *              interface calls for the netip6 and
 *              rtm6
 *
 *******************************************************************/

#include "rip6inc.h"
#include "rip6cli.h"
#include "rmap.h"

tTMO_SLL            gRip6RedistributionList;
/******************************************************************************
** DESCRIPTION : This function is used to get the Interface MTU. 
** INPUTS      : u4IfIndex   - The Interface Index Number
** OUTPUTS     : None
** RETURNS     : The Interface MTU 
** NOTES       :
*******************************************************************************/
UINT4
Rip6GetIfMtu (UINT4 u4IfIndex)
{

    tNetIpv6IfInfo      NetIpv6IfInfo;
    INT4                i4Status = NETIPV6_FAILURE;

    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));

#ifdef IP6_WANTED
    i4Status = NetIpv6GetIfInfo (u4IfIndex, &NetIpv6IfInfo);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    if (i4Status != NETIPV6_FAILURE)
    {
        return NetIpv6IfInfo.u4Mtu;
    }
    else
    {
        return 0;
    }
}

/******************************************************************************
* ** DESCRIPTION : This function is used to get the Link Local Address
* ** INPUTS      : u4IfIndex   - The Interface Index Number
* **               pLlocalAddr - The Link Local Address
* ** OUTPUTS     : None
* ** RETURNS     : Link-Local Address if present. Else returns NULL
* ** NOTES       :
* *******************************************************************************/
tIp6Addr           *
Rip6GetLlocalAddr (UINT4 u4IfIndex)
{
    tIp6Addr            Ip6Addr;

    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));

    if (MEMCMP (&RIP6_GET_IF_ENTRY (u4IfIndex)->LinkLocalAddr, &Ip6Addr,
                sizeof (tIp6Addr)) == 0)
    {
        /* Link-Local Address has not yet configured. So RIP6
         * Packets cannot be sent out. */
        return NULL;
    }

    return (&RIP6_GET_IF_ENTRY (u4IfIndex)->LinkLocalAddr);
}

/******************************************************************************
 * ** DESCRIPTION : This function is used to get the Global Address
 * ** INPUTS      : u4IfIndex   - The Interface Index Number
 * **               pDstAddr    - The Dest Address for which source addr is required
 * ** OUTPUTS     : pSrcAddr - Matching source address
 * ** RETURNS     : RIP6 SUCCESS/FAILURE
 * ** NOTES       :
 * *******************************************************************************/

INT4
Rip6GetGlobalAddr (UINT4 u4IfIndex, tIp6Addr * pDstAddr, tIp6Addr * pSrcAddr)
{
    tNetIpv6AddrInfo    netIp6FirstAddrInfo;
    tNetIpv6AddrInfo    netIp6NextAddrInfo;
    tIp6Addr           *pAddr = NULL;
    tIp6Addr           *pStoredAddr = NULL;
    INT4                i4Status = NETIPV6_FAILURE, i4Count;

#ifdef IP6_WANTED
    i4Status = NetIpv6GetFirstIfAddr (u4IfIndex, &netIp6FirstAddrInfo);
#else
    UNUSED_PARAM (u4IfIndex);
#endif
    if (i4Status != NETIPV6_FAILURE)
    {
        do
        {
            if (Ip6AddrMatch (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                              netIp6FirstAddrInfo.u4PrefixLength) == TRUE)
            {
                pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                return RIP6_SUCCESS;
            }
            else
            {
                pAddr = &netIp6FirstAddrInfo.Ip6Addr;
            }
#ifdef IP6_WANTED
            i4Status = NetIpv6GetNextIfAddr (u4IfIndex, &netIp6FirstAddrInfo,
                                             &netIp6NextAddrInfo);
#endif
            if (i4Status != NETIPV6_FAILURE)
                MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                        sizeof (tNetIpv6AddrInfo));
        }
        while (i4Status != NETIPV6_FAILURE);
        MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
        return RIP6_SUCCESS;
    }
    else
    {
        for (i4Count = 0; i4Count < MAX_IP6_INTERFACES; i4Count++)
        {
#ifdef IP6_WANTED
            i4Status = NetIpv6GetFirstIfAddr (i4Count, &netIp6FirstAddrInfo);
#endif
            if (i4Status != NETIPV6_FAILURE)
            {
                do
                {
                    if (Ip6AddrMatch (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                                      netIp6FirstAddrInfo.u4PrefixLength) ==
                        TRUE)
                    {
                        pStoredAddr = &netIp6FirstAddrInfo.Ip6Addr;
                        MEMCPY (pSrcAddr, pStoredAddr, sizeof (tIp6Addr));
                        return RIP6_SUCCESS;
                    }
                    else
                    {
                        pAddr = &netIp6FirstAddrInfo.Ip6Addr;
                    }
#ifdef IP6_WANTED
                    i4Status = NetIpv6GetNextIfAddr (u4IfIndex,
                                                     &netIp6FirstAddrInfo,
                                                     &netIp6NextAddrInfo);
#endif
                    if (i4Status != NETIPV6_FAILURE)
                    {
                        MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                                sizeof (tNetIpv6AddrInfo));
                    }
                }
                while (i4Status != NETIPV6_FAILURE);

                MEMCPY (pSrcAddr, pAddr, sizeof (tIp6Addr));
                return RIP6_SUCCESS;
            }
        }
    }

    return RIP6_FAILURE;
}

INT4
Rip6IsOurAddr (tIp6Addr * pDstAddr, UINT4 *pu4Index)
{
    UINT4               u4IfIndex;
    UINT1               u1Type = ADDR6_INVALID;
    UINT1               u1LlocalChk = 1;
    tNetIpv6IfInfo      NetIpv6IfInfo;

    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));

    /* check in link - local address list also */
    u1Type = Ip6AddrType (pDstAddr);

    /* u1LlocalChk is just a check in Ip6IsPktToMe */
    if (u1Type == ADDR6_LLOCAL)
        u1LlocalChk = ADDR6_LLOCAL;
    else if (u1Type == ADDR6_UNICAST)
        u1LlocalChk = ADDR6_UNICAST;

#ifdef IP6_WANTED
    if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        *pu4Index = 0;
        return RIP6_FAILURE;
    }
#endif
    for (;;)
    {

        if ((NetIpv6IfInfo.u4Admin == RIP6_ADMIN_VALID) ||
            (NetIpv6IfInfo.u4Admin == RIP6_ADMIN_UP))
        {
            if (Rip6IsPktToMe (&u1Type, u1LlocalChk, pDstAddr,
                               &NetIpv6IfInfo) == RIP6_SUCCESS)
            {
                *pu4Index = NetIpv6IfInfo.u4IfIndex;
                return RIP6_SUCCESS;
            }
        }

        /* Get the next interface info entry. */
        u4IfIndex = NetIpv6IfInfo.u4IfIndex;
        MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
#ifdef IP6_WANTED
        if (NetIpv6GetNextIfInfo (u4IfIndex, &NetIpv6IfInfo) == NETIPV6_FAILURE)
        {
            break;
        }
#endif
    }

    *pu4Index = 0;
    return RIP6_FAILURE;
}

INT4
Rip6IsPktToMe (UINT1 *pu1Type, UINT1 u1LlocalChk, tIp6Addr * pDstAddr,
               tNetIpv6IfInfo * pNetIpv6IfInfo)
{
    tNetIpv6AddrInfo    netIp6FirstAddrInfo;
    tNetIpv6AddrInfo    netIp6NextAddrInfo;
    INT4                i4Status = NETIPV6_FAILURE;

    if (!pDstAddr)
    {
        RIP6_TRC_ARG2 (RIP6_MOD_TRC, CONTROL_PLANE_TRC, RIP6_NAME,
                       "IP6ADDR: RIp6IsPktToMe: Rcvd address is NULL %s "
                       "gen err: Type = %d\n",
                       ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
        return RIP6_FAILURE;
    }

    /* If the address Loop back address packet is destined to this router
     * So return success */
    if (IS_ADDR_LOOPBACK (*pDstAddr))
    {
        return RIP6_SUCCESS;
    }

    if ((*pu1Type) == ADDR6_MULTI)
    {
        if (!pNetIpv6IfInfo)
        {
            RIP6_TRC_ARG2 (RIP6_MOD_TRC, CONTROL_PLANE_TRC, RIP6_NAME,
                           "IP6ADDR: RIp6IsPktToMe: Rcvd address is NULL %s "
                           "gen err: Type = %d\n",
                           ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
            return RIP6_FAILURE;
        }
        if (IS_CONSTANT_MULTI (*pDstAddr))
        {

            return RIP6_SUCCESS;
        }
    }
    else if (*pu1Type == ADDR6_LLOCAL)
    {
        if (!pNetIpv6IfInfo)
        {
            RIP6_TRC_ARG2 (RIP6_MOD_TRC, CONTROL_PLANE_TRC, RIP6_NAME,
                           "IP6ADDR: RIp6IsPktToMe: Rcvd address is NULL %s "
                           "gen err: Type = %d\n",
                           ERROR_FATAL_STR, ERR_GEN_NULL_PTR);
            return RIP6_FAILURE;
        }
        if (u1LlocalChk)
        {
            if (MEMCMP (pDstAddr, &pNetIpv6IfInfo->Ip6Addr, sizeof (tIp6Addr))
                == 0)
            {
                return RIP6_SUCCESS;
            }
            return RIP6_FAILURE;
        }
    }
    else
    {
#ifdef IP6_WANTED
        i4Status =
            NetIpv6GetFirstIfAddr (pNetIpv6IfInfo->u4IfIndex,
                                   &netIp6FirstAddrInfo);
#endif
        if (i4Status == NETIPV6_SUCCESS)
        {
            do
            {
                if (Ip6AddrMatch (&netIp6FirstAddrInfo.Ip6Addr, pDstAddr,
                                  IP6_ADDR_SIZE_IN_BITS))
                {
                    return RIP6_SUCCESS;
                }
#ifdef IP6_WANTED
                i4Status = NetIpv6GetNextIfAddr (pNetIpv6IfInfo->u4IfIndex,
                                                 &netIp6FirstAddrInfo,
                                                 &netIp6NextAddrInfo);
#endif
                if (i4Status == NETIPV6_SUCCESS)
                {
                    MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
                            sizeof (tNetIpv6AddrInfo));
                }
            }
            while (i4Status != NETIPV6_FAILURE);
        }

        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
** DESCRIPTION : This function is used to register the callback function with
**               RTM6 for processing the route update and change Notifications.
** INPUTS      : None
** OUTPUTS     : None
** RETURNS     : RIP6_FAILURE/RIP6_SUCCESS
** NOTES       :
*******************************************************************************/
INT4
Rip6Rtm6Register ()
{
    tRtm6RegnId         RegnId;

    MEMSET (&RegnId, 0, sizeof (tRtm6RegnId));
    RegnId.u2ProtoId = IP6_RIP_PROTOID;
    RegnId.u4ContextId = RTM6_DEFAULT_CXT_ID;
#ifdef IP6_WANTED
    if (Rtm6Register (&RegnId, SET_PROTO_MASK, Rip6RecvRtm6Msg) != RTM6_SUCCESS)
    {
        return RIP6_FAILURE;
    }
#endif
    return RIP6_SUCCESS;
}

VOID
Rip6InitRedistribution ()
{
    gRip6Redistribution.u4RRDAdminStatus = RIP6_RRD_ENABLE;
    gRip6Redistribution.u4ProtoMask = 0;
    gRip6Redistribution.u1DefMetric = 0;
}

/******************************************************************************
** DESCRIPTION : This function is the callback function that is registered 
**               during the registration with RTM6. This function processes 
**               the RTM mesaage.
** INPUTS      : pRtm6Hdr         - pointer to the RTM6 meassage header
**               pRtm6RespInfo    - pointer to RTM6 response message info.
** OUTPUTS     : None
** RETURNS     : RIP6_FAILURE/RIP6_SUCCESS
** NOTES       :
*******************************************************************************/
INT4
Rip6RecvRtm6Msg (tRtm6RespInfo * pRtm6RespInfo, tRtm6MsgHdr * pRtm6Hdr)
{
    tNetIpv6RtInfo     *pNetIp6RtInfo;
    INT4                i4Result = 0;

    if ((pRtm6RespInfo == NULL) || (pRtm6Hdr == NULL))
    {
        return RIP6_FAILURE;
    }

    pNetIp6RtInfo = pRtm6RespInfo->pRtInfo;
    IP6_TASK_UNLOCK ();
    switch (pRtm6Hdr->u1MessageType)
    {
        case RTM6_ROUTE_UPDATE_MESSAGE:
            i4Result = Rip6Rtm6RtAdd (pRtm6RespInfo, pRtm6Hdr);
            IP6_TASK_LOCK ();
            return i4Result;

        case RTM6_ROUTE_CHANGE_NOTIFY_MESSAGE:
            if (pNetIp6RtInfo->u4RowStatus == RTM6_DESTROY)
            {
                i4Result = Rip6Rtm6RtDel (pRtm6RespInfo, pRtm6Hdr);
                IP6_TASK_LOCK ();
                return i4Result;
            }
            else if (pNetIp6RtInfo->u4RowStatus == RTM6_ACTIVE)
            {
                i4Result = Rip6Rtm6RtAdd (pRtm6RespInfo, pRtm6Hdr);
                IP6_TASK_LOCK ();
                return i4Result;
            }
            else
                break;
        default:
            break;
    }

    IP6_TASK_LOCK ();
    return RIP6_SUCCESS;
}

/******************************************************************************
** DESCRIPTION : This function scans the route update message from rtm6 and 
**               adds the route to the RIB of RIP6.
** INPUTS      : pRtm6Hdr         - pointer to the RTM6 meassage header
**               pRtm6RespInfo    - pointer to RTM6 response message info.
** OUTPUTS     : None
** RETURNS     : RIP6_FAILURE/RIP6_SUCCESS
** NOTES       :
*******************************************************************************/
INT4
Rip6Rtm6RtAdd (tRtm6RespInfo * pRtm6RespInfo, tRtm6MsgHdr * pRtm6Hdr)
{
    tNetIpv6RtInfo     *pNetIp6RtInfo;
    tRip6RrdRouteInfo  *pRip6RrdRouteInfo = NULL;

    UNUSED_PARAM (pRtm6Hdr);
    if (gRip6Redistribution.u4RRDAdminStatus != RIP6_RRD_ENABLE)
    {
        return RIP6_FAILURE;
    }

    pNetIp6RtInfo = pRtm6RespInfo->pRtInfo;

    pRip6RrdRouteInfo =
        (tRip6RrdRouteInfo *) MemAllocMemBlk ((tMemPoolId)
                                              i4Rip6RrdRouteInfoId);

    if (NULL == pRip6RrdRouteInfo)
    {
        return RIP6_FAILURE;
    }

    TMO_SLL_Init_Node (&pRip6RrdRouteInfo->Next);

    pRip6RrdRouteInfo->pRtEntry.u1Prefixlen = pNetIp6RtInfo->u1Prefixlen;
    if (gRip6Redistribution.u1DefMetric != 0)
    {
        pRip6RrdRouteInfo->pRtEntry.u1Metric = gRip6Redistribution.u1DefMetric;
    }
    else
    {
        if (pNetIp6RtInfo->u4Metric < RIP6_INFINITY)
        {
            pRip6RrdRouteInfo->pRtEntry.u1Metric =
                (UINT1) pNetIp6RtInfo->u4Metric;
        }
        else
        {
            pRip6RrdRouteInfo->pRtEntry.u1Metric = RIP6_INFINITY;
        }
    }
    pRip6RrdRouteInfo->pRtEntry.i1Proto = pNetIp6RtInfo->i1Proto;
    pRip6RrdRouteInfo->pRtEntry.u2RtTag = (UINT2) pNetIp6RtInfo->u4RouteTag;
    pRip6RrdRouteInfo->pRtEntry.u4Index = pNetIp6RtInfo->u4Index;
    pRip6RrdRouteInfo->pRtEntry.u1Level = pNetIp6RtInfo->u1MetricType;
    pRip6RrdRouteInfo->pRtEntry.i1Type = INDIRECT;
    pRip6RrdRouteInfo->pRtEntry.u1RtFlag = RIP6_INITIALIZE_ONE;
    pRip6RrdRouteInfo->pRtEntry.i1AddrCnt = RIP6_INITIALIZE_ONE;
    MEMCPY (&pRip6RrdRouteInfo->pRtEntry.dst, &pNetIp6RtInfo->Ip6Dst,
            sizeof (tIp6Addr));
    MEMCPY (&pRip6RrdRouteInfo->pRtEntry.nexthop, &pNetIp6RtInfo->NextHop,
            sizeof (tIp6Addr));
    if ((gRip6Redistribution.u1DefMetric == RIP6_DFLT_COST) &&
        (pRip6RrdRouteInfo->pRtEntry.i1Proto != RIPNG))
    {
        if (((RIP6_GET_IF_ENTRY (pRip6RrdRouteInfo->pRtEntry.u4Index)->u1Status
              & RIP6_IFACE_ENABLED) ==
             RIP6_IFACE_ENABLED)
            && (pRip6RrdRouteInfo->pRtEntry.i1Proto != RIP6_STATIC))
        {
            /* Check if the interface is already enabled */
            pRip6RrdRouteInfo->pRtEntry.u1Metric =
                gRip6Redistribution.u1DefMetric;
        }
        else
        {
            pRip6RrdRouteInfo->pRtEntry.u1Metric = 3;

        }
    }

    if (pRip6RrdRouteInfo->pRtEntry.u1Metric < RIP6_INFINITY)
    {
        pRip6RrdRouteInfo->pRtEntry.u2Flag = RIP6_ROUTE_FEASIBLE;
    }
    else
    {
        pRip6RrdRouteInfo->pRtEntry.u2Flag = RIP6_ROUTE_WITHDRAWN;
    }

    OsixSemTake (gRip6Rtm6SemLockId);
    TMO_SLL_Add (&gRip6RedistributionList, &(pRip6RrdRouteInfo->Next));
    OsixSemGive (gRip6Rtm6SemLockId);

    if (OsixSendEvent
        (SELF, (const UINT1 *) RIP6_TASK_NAME,
         RIP6_1S_TIMER_EVENT) == OSIX_FAILURE)
    {
        return RIP6_FAILURE;
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
** DESCRIPTION : This function scans the route update message from rtm6 and 
**               updates the RIB of RIP6.
** INPUTS      : pRtm6Hdr         - pointer to the RTM6 meassage header
**               pRtm6RespInfo    - pointer to RTM6 response message info.
** OUTPUTS     : None
** RETURNS     : RIP6_FAILURE/RIP6_SUCCESS
** NOTES       :
*******************************************************************************/
INT4
Rip6Rtm6RtDel (tRtm6RespInfo * pRtm6RespInfo, tRtm6MsgHdr * pRtm6Hdr)
{
    tNetIpv6RtInfo     *pNetIp6RtInfo;
    tRip6RrdRouteInfo  *pRip6RrdRouteInfo = NULL;

    UNUSED_PARAM (pRtm6Hdr);

    pNetIp6RtInfo = pRtm6RespInfo->pRtInfo;

    if (gRip6Redistribution.u4RRDAdminStatus != RIP6_RRD_ENABLE)
    {
        return RIP6_FAILURE;
    }

    pRip6RrdRouteInfo =
        (tRip6RrdRouteInfo *) MemAllocMemBlk ((tMemPoolId)
                                              i4Rip6RrdRouteInfoId);
    if (pRip6RrdRouteInfo == NULL)
    {
        return RIP6_FAILURE;
    }
    TMO_SLL_Init_Node (&pRip6RrdRouteInfo->Next);

    pRip6RrdRouteInfo->pRtEntry.u1Prefixlen = pNetIp6RtInfo->u1Prefixlen;
    if (gRip6Redistribution.u1DefMetric != 0)
    {
        pRip6RrdRouteInfo->pRtEntry.u1Metric = gRip6Redistribution.u1DefMetric;
    }
    else
    {
        pRip6RrdRouteInfo->pRtEntry.u1Metric = (UINT1) pNetIp6RtInfo->u4Metric;
    }
    pRip6RrdRouteInfo->pRtEntry.i1Proto = pNetIp6RtInfo->i1Proto;
    pRip6RrdRouteInfo->pRtEntry.u2RtTag = (UINT2) pNetIp6RtInfo->u4RouteTag;
    pRip6RrdRouteInfo->pRtEntry.u4Index = pNetIp6RtInfo->u4Index;
    pRip6RrdRouteInfo->pRtEntry.i1Type = INDIRECT;
    pRip6RrdRouteInfo->pRtEntry.u1RtFlag = RIP6_INITIALIZE_ONE;
    pRip6RrdRouteInfo->pRtEntry.i1AddrCnt = RIP6_INITIALIZE_ONE;
    pRip6RrdRouteInfo->pRtEntry.u2Flag = RIP6_ROUTE_WITHDRAWN;

    MEMCPY (&pRip6RrdRouteInfo->pRtEntry.dst, &pNetIp6RtInfo->Ip6Dst,
            sizeof (tIp6Addr));
    MEMCPY (&pRip6RrdRouteInfo->pRtEntry.nexthop, &pNetIp6RtInfo->NextHop,
            sizeof (tIp6Addr));

    OsixSemTake (gRip6Rtm6SemLockId);
    TMO_SLL_Add (&gRip6RedistributionList, &(pRip6RrdRouteInfo->Next));
    OsixSemGive (gRip6Rtm6SemLockId);

    if (OsixSendEvent
        (SELF, (const UINT1 *) RIP6_TASK_NAME,
         RIP6_1S_TIMER_EVENT) == OSIX_FAILURE)
    {
        return RIP6_FAILURE;
    }

    return RIP6_SUCCESS;
}

/****************************************************************************
 *  Function    :  SendingMessageToRRDQueue
 *  Description :  This function is called to send messages to the RTM input Q
 *  Input       :  The bit mask correspoding to protocols with which RRD is enabled
 *                 The type of the message to be sent
 *  Output      :  none
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT4
Rip6SendMsgToRTMQueue (UINT4 u4ProtoMask, UINT1 u1MessageType,
                       UINT1 *pu1RMapName)
{

    tOsixMsg           *pBuf = NULL;
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;
    UINT2               u2Bitmask = 0;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];    /*trailing zero */

    /**prepare intermediate buf for route-map,
        ensure no garbage after map name */
    MEMSET (au1RMapName, 0, sizeof (au1RMapName));
    if (NULL != pu1RMapName)
    {
        STRNCPY (au1RMapName, pu1RMapName, RMAP_MAX_NAME_LEN + 4 - 1);
        au1RMapName[(sizeof (au1RMapName) - 1)] = '\0';
    }

    if ((pBuf =
         CRU_BUF_Allocate_MsgBufChain (sizeof (tRtm6MsgHdr) + sizeof (UINT2) +
                                       RMAP_MAX_NAME_LEN, 0)) == NULL)
    {
        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p",
                       ERROR_FATAL, ERR_BUF_ALLOC, RIP6_INITIALIZE_ZERO);
        return SNMP_FAILURE;
    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "Rip6SndRTMQ");
    pRtm6MsgHdr = (tRtm6MsgHdr *) CRU_BUF_Get_ModuleData (pBuf);
    pRtm6MsgHdr->u1MessageType = u1MessageType;
    pRtm6MsgHdr->RegnId.u2ProtoId = IP6_RIP_PROTOID;
    pRtm6MsgHdr->u2MsgLen = sizeof (UINT2) + RMAP_MAX_NAME_LEN;

    /* Setting the Particular Protocol Bit and Sending the
     *  Message to the RTM queue
     */
    if ((u4ProtoMask & RIP6_IMPORT_STATIC) == RIP6_IMPORT_STATIC)
    {
        u2Bitmask |= RTM6_STATIC_MASK;
    }
    if ((u4ProtoMask & RIP6_IMPORT_DIRECT) == RIP6_IMPORT_DIRECT)
    {
        u2Bitmask |= RTM6_DIRECT_MASK;
    }
    if ((u4ProtoMask & RIP6_IMPORT_BGP) == RIP6_IMPORT_BGP)
    {
        u2Bitmask |= RTM6_BGP_MASK;
    }
    if ((u4ProtoMask & RIP6_IMPORT_OSPF) == RIP6_IMPORT_OSPF)
    {
        u2Bitmask |= RTM6_OSPF_MASK;
    }
    if ((u4ProtoMask & RIP6_IMPORT_ISISL1) == RIP6_IMPORT_ISISL1)
    {
        u2Bitmask |= RTM6_ISISL1_MASK;
    }
    if ((u4ProtoMask & RIP6_IMPORT_ISISL2) == RIP6_IMPORT_ISISL2)
    {
        u2Bitmask |= RTM6_ISISL2_MASK;
    }

    /*put protocol-mask in packet on offset 0 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, (UINT1 *) &u2Bitmask, 0, sizeof (u2Bitmask)) == CRU_FAILURE)
    {
        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p",
                       ERROR_FATAL, ERR_BUF_ALLOC, RIP6_INITIALIZE_ZERO);
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }
    /*put route-map in packet on offset 2 */
    if (CRU_BUF_Copy_OverBufChain
        (pBuf, au1RMapName, sizeof (u2Bitmask), RMAP_MAX_NAME_LEN)
        == CRU_FAILURE)
    {
        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p",
                       ERROR_FATAL, ERR_BUF_ALLOC, RIP6_INITIALIZE_ZERO);
        CRU_BUF_Release_MsgBufChain (pBuf, 0);
        return SNMP_FAILURE;
    }
#ifdef IP6_WANTED
    if (RpsEnqueuePktToRtm6 (pBuf) != RTM6_SUCCESS)
    {
        return SNMP_FAILURE;
    }
#endif
    return SNMP_SUCCESS;
}

INT4
Rip6DeleteRedisRoutes (INT4 i4RRDSrcProtoMaskForDisable)
{
    tRip6RtEntry       *pCurRt = NULL;
    tRip6RtEntry       *pNextRt = NULL;
    tRip6ScanParam      ScanParams;
    UINT4               u4InstanceId;
    INT4                i4RouteProtoMask = 0;
    UINT4               u4MaxCxts = 0;
    INT4                i4Result = RIP6_FALSE;

    u4MaxCxts = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                          FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                          u4PreAllocatedUnits);

    pNextRt = (tRip6RtEntry *) RBTreeGetFirst (gRip6RBTree);
    pCurRt = pNextRt;

    while ((pCurRt = pNextRt) != NULL)
    {
        pNextRt = (tRip6RtEntry *) RBTreeGetNext (gRip6RBTree,
                                                  (tRBElem *) pCurRt,
                                                  Rip6ComapreRoutes);
        /* pNextRt = (tRip6RtEntry *) RBTreeGetNext (gRip6RBTree,
           (tRBElem *) pCurRt);
         */

        RTM6_SET_BIT (i4RouteProtoMask, pCurRt->i1Proto);
        if (pCurRt->i1Proto == ISIS6_ID)
        {
            if ((pCurRt->u1Level == IP_ISIS_LEVEL1) &&
                (i4RRDSrcProtoMaskForDisable & RIP6_IMPORT_ISISL1))
            {
                i4Result = RIP6_TRUE;
            }
            else if ((pCurRt->u1Level == IP_ISIS_LEVEL2) &&
                     (i4RRDSrcProtoMaskForDisable & RIP6_IMPORT_ISISL2))
            {
                i4Result = RIP6_TRUE;
            }
        }
        else if (i4RRDSrcProtoMaskForDisable & i4RouteProtoMask)
        {
            i4Result = RIP6_TRUE;
        }
        if (i4Result == RIP6_TRUE)
        {
            if (RBTreeRemove (gRip6RBTree, (tRBElem *) pCurRt) == RB_SUCCESS)
            {
                if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                        (UINT1 *) pCurRt) != RIP6_SUCCESS)
                {
                    RIP6_TRC_ARG1 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                   "RIP6 : rip6 add prefix to rt unable to"
                                   " release mem to pool id = %d \n",
                                   i4Rip6rtId);
                    RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                   "%s mem err: Type = %d  PoolId = %d mptr = %p",
                                   ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                   RIP6_INITIALIZE_ZERO, i4Rip6rtId);
                }
                pCurRt = NULL;
            }
        }
    }

    /* Scan through the TRIE instances to find the corresponding routes and
     * remove the routes. */
    for (u4InstanceId = RIP6_INITIALIZE_ZERO; u4InstanceId < u4MaxCxts;
         u4InstanceId++)
    {
        if (garip6InstanceDatabase[u4InstanceId] == NULL)
        {
            continue;
        }

        /* Update the Metric */
        ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_REDIS_METRIC_UPD;
        ScanParams.u4InstanceId = u4InstanceId;
        ScanParams.i4ProtoMask = i4RRDSrcProtoMaskForDisable;
        Rip6TrieScan (&ScanParams);

        /* Send Trigger update for all the interface over this instance. */
        Rip6CheckTrigUpdateSendForResponseMsg (u4InstanceId, RIP6_BULK_DELETE);

        /* Delete all the modifed routes. */
        ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_REDIS_ROUTE_DEL;
        ScanParams.u4InstanceId = u4InstanceId;
        ScanParams.i4ProtoMask = i4RRDSrcProtoMaskForDisable;
        Rip6TrieScan (&ScanParams);
    }

    return RIP6_SUCCESS;
}

VOID
Rip6CreateKey (tRip6RtKey * Rip6Key, tRip6RtEntry * pRtEntry)
{

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) Rip6Key, &pRtEntry->dst,
                     pRtEntry->u1Prefixlen);
    MEMCPY (((UINT1 *) Rip6Key + sizeof (tIp6Addr)), &pRtEntry->u1Prefixlen,
            sizeof (UINT1));
    return;
}

INT4
Rip6ComapreRoutes (tRBElem * pRt1, tRBElem * pRt2)
{
    tRip6RtKey          Key1;
    tRip6RtKey          Key2;

    MEMSET (&Key1, 0, sizeof (tRip6RtKey));
    MEMSET (&Key2, 0, sizeof (tRip6RtKey));
    Rip6CreateKey (&Key1, (tRip6RtEntry *) pRt1);
    Rip6CreateKey (&Key2, (tRip6RtEntry *) pRt2);

    return MEMCMP (&Key1, &Key2, (sizeof (UINT1) + sizeof (tIp6Addr)));
}
