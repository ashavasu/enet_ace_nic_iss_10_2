
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6rbutl.c,v 1.1 2013/11/22 11:43:42 siva Exp $
 *
 * Description: This file contains RB tree utilities regarding            
 *              RIP6_ARRAY_TO_RBTREE_WANTED flag. 
 *
 *******************************************************************/

#include "rip6inc.h"

/*--------  RBTREE UTILITIES FOR gRip6IfInfoTbl -----------*/

/*****************************************************************************/
/* Function Name      : Rip6CreateRip6IfInfoTbl                              */
/*                                                                           */
/* Description        : This routine creates gRip6IfInfoTbl.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RIP6_SUCCESS/RIP6_FAILURE                            */
/*****************************************************************************/
INT4
Rip6CreateRip6IfInfoTbl ()
{
    /* RBTree creation for Rip6IfInfoTbl. */
    gRip6IfInfoTbl = RBTreeCreateEmbedded (0, Rip6IfInfoTblCmp);

    if (gRip6IfInfoTbl == NULL)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "RB Tree creation for Rip6IfInfoTbl is failed\r\n");
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rip6DeleteRip6IfInfoTbl                              */
/*                                                                           */
/* Description        : This routine deletes gRip6IfInfoTbl.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
Rip6DeleteRip6IfInfoTbl ()
{
    /* RBTree deletion for Rip6IfInfoTbl. */
    Rip6DelAllRip6IfInfoTblEntries ();
    if (gRip6IfInfoTbl != NULL)
    {
        RBTreeDestroy (gRip6IfInfoTbl, NULL, 0);
        gRip6IfInfoTbl = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : Rip6IfInfoTblCmp                                     */
/*                                                                           */
/* Description        : This routine is used in gRip6IfInfoTbl for           */
/*                      comparing two keys used in RBTree functionality.     */
/*                                                                           */
/* Input(s)           : pNode     - Key 1                                    */
/*                      pNodeIn   - Key 2                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
Rip6IfInfoTblCmp (tRBElem * Node, tRBElem * NodeIn)
{
    tRip6IfInfoNode     *pNode = NULL;
    tRip6IfInfoNode     *pNodeIn = NULL;
  
    pNode = (tRip6IfInfoNode *)Node;
    pNodeIn = (tRip6IfInfoNode *) NodeIn;

    /* key 1 --> Interface index.
     */
    if (pNode->u4Index < pNodeIn->u4Index)
    {
        return (RIP6_RBTREE_KEY_LESSER);
    }
    else if (pNode->u4Index > pNodeIn->u4Index)
    {
        return (RIP6_RBTREE_KEY_GREATER);
    }
    return (RIP6_RBTREE_KEY_EQUAL);
}

/*****************************************************************************/
/* Function Name      : Rip6AddRip6IfInfoTblEntry                            */
/*                                                                           */
/* Description        : This routine add a tRip6IfInfoNode  in               */
/*                      gRip6IfInfoTbl.                                      */
/*                                                                           */
/* Input(s)           : u4Index    - Interface index.                        */
/*                                                                           */
/* Output(s)          : ppRip6IfInfoNode  - pointer to the pRip6IfInfoNode.  */
/*                                                                           */
/* Return Value(s)    : RIP6_SUCCESS / RIP6_FAILURE                          */
/*****************************************************************************/
INT4
Rip6AddRip6IfInfoTblEntry (UINT4 u4Index, tRip6IfInfoNode **ppRip6IfInfoNode)
{
    (*ppRip6IfInfoNode) = NULL;

    if (((*ppRip6IfInfoNode) = Rip6GetRip6IfInfoTblEntry (u4Index)) != NULL) 
    {
        return RIP6_SUCCESS;
    }

    (*ppRip6IfInfoNode) = (tRip6IfInfoNode *)(VOID *) 
               MemAllocMemBlk ((tMemPoolId)i4Rip6IfInfoPoolId);
    if ((*ppRip6IfInfoNode) == NULL)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                   "Memory allocation for tRip6IfInfoNode failed\r\n");
        return RIP6_FAILURE;
    }

    MEMSET ((*ppRip6IfInfoNode), 0, sizeof (tRip6IfInfoNode));

    (*ppRip6IfInfoNode)->u4Index = u4Index;

    if (RBTreeAdd (gRip6IfInfoTbl, (tRBElem *)
                       (*ppRip6IfInfoNode)) == RB_FAILURE)
    {
        MemReleaseMemBlock ((tMemPoolId)i4Rip6IfInfoPoolId,
                            (UINT1 *) (*ppRip6IfInfoNode));
        (*ppRip6IfInfoNode) = NULL;
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                         "Global RBTree Addition Failed for "
                         "gRip6IfInfoTbl Entry \r\n");
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rip6GetRip6IfInfoTblEntry                            */
/*                                                                           */
/* Description        : This routine gets a tRip6IfInfoNode  for the         */
/*                      given u4Index.                                       */
/*                                                                           */
/* Input(s)           : u4Index    - Interface index           .             */
/*                                                                           */
/* Output(s)          : pRip6IfInfoNode  - pointer to the pRip6IfInfoNode    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
tRip6IfInfoNode *
Rip6GetRip6IfInfoTblEntry (UINT4 u4Index)
{
    tRip6IfInfoNode  *pRip6IfInfoNode = NULL;
    tRip6IfInfoNode   Rip6IfInfoNode;

    MEMSET (&Rip6IfInfoNode, 0, sizeof (tRip6IfInfoNode));

    Rip6IfInfoNode.u4Index = u4Index;

    pRip6IfInfoNode  = (tRip6IfInfoNode *) RBTreeGet 
        ((gRip6IfInfoTbl), (tRBElem *) &Rip6IfInfoNode);

    return pRip6IfInfoNode;
}

/*****************************************************************************/
/* Function Name      : Rip6GetFirstRip6IfInfoTblEntry                       */
/*                                                                           */
/* Description        : This routine is used to get first tRip6IfInfoNode    */
/*                      from gRip6IfInfoTbl.                                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : pRip6IfInfoNode - pointer to the tRip6IfInfoNode     */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
tRip6IfInfoNode  *
Rip6GetFirstRip6IfInfoTblEntry ()
{
    tRip6IfInfoNode    *pRip6IfInfoNode = NULL;

    pRip6IfInfoNode = (tRip6IfInfoNode *)
                    RBTreeGetFirst (gRip6IfInfoTbl);

    return pRip6IfInfoNode;
}

/*****************************************************************************/
/* Function Name      : Rip6GetNextRip6IfInfoTblEntry                        */
/*                                                                           */
/* Description        : This routine is used to get the next                 */
/*                      tRip6IfInfoNode from gRip6IfInfoTbl.                 */
/*                                                                           */
/* Input(s)           : u4Index    - Interface index.                        */
/*                                                                           */
/* Output(s)          : pRip6IfInfoNode - pointer to the tRip6IfInfoNode     */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
tRip6IfInfoNode *
Rip6GetNextRip6IfInfoTblEntry (UINT4 u4Index)
{
    tRip6IfInfoNode  *pRip6IfInfoNode = NULL;
    tRip6IfInfoNode   Rip6IfInfoNode;

    MEMSET (&Rip6IfInfoNode, 0, sizeof (tRip6IfInfoNode));
    
    Rip6IfInfoNode.u4Index = u4Index;

    pRip6IfInfoNode  = (tRip6IfInfoNode *) RBTreeGetNext 
      ((gRip6IfInfoTbl), (tRBElem *) &Rip6IfInfoNode, NULL);

    return pRip6IfInfoNode;
}

/*****************************************************************************/
/* Function Name      : Rip6DelRip6IfInfoTblEntry                            */
/*                                                                           */
/* Description        : This routine deletes a tRip6IfInfoNode from the      */
/*                      gRip6IfInfoTbl.                                      */
/*                                                                           */
/* Input(s)           : pRip6IfInfoNode - node to be deleted.                */
/*                                                                           */
/* Return Value(s)    : RIP6_SUCCESS/RIP6_FAILURE                            */
/*****************************************************************************/
INT4
Rip6DelRip6IfInfoTblEntry (tRip6IfInfoNode *pRip6IfInfoNode)
{
    if (pRip6IfInfoNode == NULL)
    {
        return RIP6_FAILURE;
    }
    if (RBTreeRemove ((gRip6IfInfoTbl), 
            (tRBElem *) pRip6IfInfoNode) == RB_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                 "RBTree remove failed \r\n");
        return RIP6_FAILURE;
    }
    MemReleaseMemBlock ((tMemPoolId) i4Rip6IfInfoPoolId,
                        (UINT1 *) (pRip6IfInfoNode));
    pRip6IfInfoNode = NULL;
    return RIP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rip6DelAllRip6IfInfoTblEntries                       */
/*                                                                           */
/* Description        : This function deletes all the tRip6IfInfoNode        */
/*                      entries of gRip6IfInfoTbl.                           */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
Rip6DelAllRip6IfInfoTblEntries ()
{
    tRip6IfInfoNode   *pRip6IfInfoNode = NULL;
    tRip6IfInfoNode   *pNextRip6IfInfoNode = NULL;

    pRip6IfInfoNode = Rip6GetFirstRip6IfInfoTblEntry ();

    while (pRip6IfInfoNode != NULL)
    {
        /* Delete all the nodes */
        pNextRip6IfInfoNode = Rip6GetNextRip6IfInfoTblEntry 
            (pRip6IfInfoNode->u4Index);

        Rip6DelRip6IfInfoTblEntry (pRip6IfInfoNode);

        pRip6IfInfoNode = pNextRip6IfInfoNode;
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : Rip6SetInstIfaceMapInRip6IfInfoTbl                  */
 /*                                                                           */
 /* Description         : This function adds tRip6IfInfoNode in Rip6IfInfoTbl */
 /*                       for the given u4Index and sets pRip6InstIfaceMap.   */
 /*                                                                           */
 /* Input(s)            : u4Index       - Interface index.                    */
 /*                       pRip6InstIfaceMap   - pointer to be set.            */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
Rip6SetInstIfaceMapInRip6IfInfoTbl (UINT4 u4Index, 
                          tInstanceInterfaceMap *pRip6InstIfaceMap)
{
    tRip6IfInfoNode     *pRip6IfInfoNode = NULL;

    if (Rip6AddRip6IfInfoTblEntry (u4Index, &pRip6IfInfoNode) == RIP6_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                         "Adding tRip6IfInfoNode is failed\r\n");
        return;
    }
    pRip6IfInfoNode->pRip6InstIfaceMap = pRip6InstIfaceMap;
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : Rip6GetInstIfaceMapInRip6IfInfoTbl                  */
 /*                                                                           */
 /* Description         : This function gets the pRip6InstIfaceMap  for the gn*/
 /*                       u4Index in tRip6IfInfoNode in Rip6IfInfoTbl.        */
 /*                                                                           */
 /* Input(s)            : u4Index       - Interface index.                    */
 /*                                                                           */
 /* Output(s)           : pRip6InstIfaceMap   - pointer to the tRip6RAEntry   */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
tInstanceInterfaceMap *
Rip6GetInstIfaceMapInRip6IfInfoTbl (UINT4 u4Index)
{
    tRip6IfInfoNode    *pRip6IfInfoNode = NULL;

    pRip6IfInfoNode = Rip6GetRip6IfInfoTblEntry (u4Index);

    if (pRip6IfInfoNode == NULL)
    {
        return NULL;
    }
    return (pRip6IfInfoNode->pRip6InstIfaceMap);
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : Rip6SetIfInRip6IfInfoTbl                            */
 /*                                                                           */
 /* Description         : This function adds tRip6IfInfoNode in Rip6IfInfoTbl */
 /*                       for the given u4Index and sets pRip6If.             */
 /*                                                                           */
 /* Input(s)            : u4Index       - Interface index.                    */
 /*                       pRip6If       - pointer to be set.                  */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
Rip6SetIfInRip6IfInfoTbl (UINT4 u4Index, tRip6If *pRip6If)
{
    tRip6IfInfoNode     *pRip6IfInfoNode = NULL;

    if (Rip6AddRip6IfInfoTblEntry (u4Index, &pRip6IfInfoNode) == RIP6_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                         "Adding tRip6IfInfoNode is failed\r\n");
        return;
    }
    pRip6IfInfoNode->pRip6If = pRip6If;
    return;
}
 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : Rip6GetIfInRip6IfInfoTbl                            */
 /*                                                                           */
 /* Description         : This function gets the pRip6If for the given        */
 /*                       u4Index in tRip6IfInfoNode in Rip6IfInfoTbl.        */
 /*                                                                           */
 /* Input(s)            : u4Index       - Interface index.                    */
 /*                                                                           */
 /* Output(s)           : pRip6If        - pointer to the tRip6If.            */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
tRip6If *
Rip6GetIfInRip6IfInfoTbl (UINT4 u4Index)
{
    tRip6IfInfoNode    *pRip6IfInfoNode = NULL;

    pRip6IfInfoNode = Rip6GetRip6IfInfoTblEntry (u4Index);

    if (pRip6IfInfoNode == NULL)
    {
        return NULL;
    }
    return (pRip6IfInfoNode->pRip6If);
}

/*--------  RBTREE UTILITIES FOR gRip6InstIfTbl -----------*/

/*****************************************************************************/
/* Function Name      : Rip6CreateRip6InstIfTbl                              */
/*                                                                           */
/* Description        : This routine creates gRip6InstIfTbl.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : RIP6_SUCCESS/RIP6_FAILURE                            */
/*****************************************************************************/
INT4
Rip6CreateRip6InstIfTbl ()
{
    /* RBTree creation for Rip6InstIfTbl. */
    gRip6InstIfTbl = RBTreeCreateEmbedded (0, Rip6InstIfTblCmp);

    if (gRip6InstIfTbl == NULL)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "RB Tree creation for Rip6InstIfTbl is failed\r\n");
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rip6DeleteRip6InstIfTbl                              */
/*                                                                           */
/* Description        : This routine deletes gRip6InstIfTbl.                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
VOID
Rip6DeleteRip6InstIfTbl ()
{
    /* RBTree deletion for Rip6InstIfTbl. */
    Rip6DelAllRip6InstIfTblEntries ();
    if (gRip6InstIfTbl != NULL)
    {
        RBTreeDestroy (gRip6InstIfTbl, NULL, 0);
        gRip6InstIfTbl = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name      : Rip6InstIfTblCmp                                     */
/*                                                                           */
/* Description        : This routine is used in gRip6InstIfTbl for           */
/*                      comparing two keys used in RBTree functionality.     */
/*                                                                           */
/* Input(s)           : pNode     - Key 1                                    */
/*                      pNodeIn   - Key 2                                    */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : -1/1 -> when any key element is Less/Greater         */
/*                      0    -> when all key elements are Equal              */
/*****************************************************************************/
INT4
Rip6InstIfTblCmp (tRBElem * Node, tRBElem * NodeIn)
{
    tRip6InstIfNode     *pNode = NULL;
    tRip6InstIfNode     *pNodeIn = NULL;
  
    pNode = (tRip6InstIfNode *)Node;
    pNodeIn = (tRip6InstIfNode *) NodeIn;

    /* key 1 --> Instance identifier.
     * key 2 --> Interface index.
     */
    if (pNode->u4Instance < pNodeIn->u4Instance)
    {
        return (RIP6_RBTREE_KEY_LESSER);
    }
    else if (pNode->u4Instance > pNodeIn->u4Instance)
    {
        return (RIP6_RBTREE_KEY_GREATER);
    }
    if (pNode->u4Index < pNodeIn->u4Index)
    {
        return (RIP6_RBTREE_KEY_LESSER);
    }
    else if (pNode->u4Index > pNodeIn->u4Index)
    {
        return (RIP6_RBTREE_KEY_GREATER);
    }
    return (RIP6_RBTREE_KEY_EQUAL);
}

/*****************************************************************************/
/* Function Name      : Rip6AddRip6InstIfTblEntry                            */
/*                                                                           */
/* Description        : This routine add a tRip6InstIfNode  in               */
/*                      gRip6InstIfTbl.                                      */
/*                                                                           */
/* Input(s)           : u4Index    - Interface index.                        */
/*                      u4InstId   - Instance identifier.                    */
/*                                                                           */
/* Output(s)          : ppRip6InstIfNode  - pointer to the pRip6InstIfNode.  */
/*                                                                           */
/* Return Value(s)    : RIP6_SUCCESS / RIP6_FAILURE                          */
/*****************************************************************************/
INT4
Rip6AddRip6InstIfTblEntry (UINT4 u4InstId, UINT4 u4Index, 
                           tRip6InstIfNode **ppRip6InstIfNode)
{
    (*ppRip6InstIfNode) = NULL;

    if (((*ppRip6InstIfNode) = Rip6GetRip6InstIfTblEntry (u4InstId, 
            u4Index)) != NULL) 
    {
        return RIP6_SUCCESS;
    }

    (*ppRip6InstIfNode) = (tRip6InstIfNode *)(VOID *) 
               MemAllocMemBlk ((tMemPoolId) i4Rip6InstIfPoolId);
    if ((*ppRip6InstIfNode) == NULL)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                   "Memory allocation for tRip6InstIfNode failed\r\n");
        return RIP6_FAILURE;
    }

    MEMSET ((*ppRip6InstIfNode), 0, sizeof (tRip6InstIfNode));

    (*ppRip6InstIfNode)->u4Index = u4Index;
    (*ppRip6InstIfNode)->u4Instance = u4InstId;

    if (RBTreeAdd (gRip6InstIfTbl, (tRBElem *)
                       (*ppRip6InstIfNode)) == RB_FAILURE)
    {
        MemReleaseMemBlock ((tMemPoolId) i4Rip6InstIfPoolId,
                            (UINT1 *) (*ppRip6InstIfNode));
        (*ppRip6InstIfNode) = NULL;
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                         "Global RBTree Addition Failed for "
                         "gRip6InstIfTbl Entry \r\n");
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rip6GetRip6InstIfTblEntry                            */
/*                                                                           */
/* Description        : This routine gets a tRip6InstIfNode  for the         */
/*                      given u4Index.                                       */
/*                                                                           */
/* Input(s)           : u4Index    - Interface index           .             */
/*                      u4InstId   - Instance identifier.                    */
/*                                                                           */
/* Output(s)          : pRip6InstIfNode  - pointer to the pRip6InstIfNode    */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*****************************************************************************/
tRip6InstIfNode *
Rip6GetRip6InstIfTblEntry (UINT4 u4InstId, UINT4 u4Index)
{
    tRip6InstIfNode  *pRip6InstIfNode = NULL;
    tRip6InstIfNode   Rip6InstIfNode;

    MEMSET (&Rip6InstIfNode, 0, sizeof (tRip6InstIfNode));

    Rip6InstIfNode.u4Index = u4Index;
    Rip6InstIfNode.u4Instance = u4InstId;

    pRip6InstIfNode  = (tRip6InstIfNode *) RBTreeGet 
        ((gRip6InstIfTbl), (tRBElem *) &Rip6InstIfNode);

    return pRip6InstIfNode;
}

/*****************************************************************************/
/* Function Name      : Rip6GetFirstRip6InstIfTblEntry                       */
/*                                                                           */
/* Description        : This routine is used to get first tRip6InstIfNode    */
/*                      from gRip6InstIfTbl.                                 */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : pRip6InstIfNode - pointer to the tRip6InstIfNode     */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
tRip6InstIfNode  *
Rip6GetFirstRip6InstIfTblEntry ()
{
    tRip6InstIfNode    *pRip6InstIfNode = NULL;

    pRip6InstIfNode = (tRip6InstIfNode *)
                    RBTreeGetFirst (gRip6InstIfTbl);

    return pRip6InstIfNode;
}

/*****************************************************************************/
/* Function Name      : Rip6GetNextRip6InstIfTblEntry                        */
/*                                                                           */
/* Description        : This routine is used to get the next                 */
/*                      tRip6InstIfNode from gRip6InstIfTbl.                 */
/*                                                                           */
/* Input(s)           : u4Index    - Interface index.                        */
/*                      u4InstId   - Instance identifier.                    */
/*                                                                           */
/* Output(s)          : pRip6InstIfNode - pointer to the tRip6InstIfNode     */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
tRip6InstIfNode *
Rip6GetNextRip6InstIfTblEntry (UINT4 u4InstId, UINT4 u4Index)
{
    tRip6InstIfNode  *pRip6InstIfNode = NULL;
    tRip6InstIfNode   Rip6InstIfNode;

    MEMSET (&Rip6InstIfNode, 0, sizeof (tRip6InstIfNode));
    
    Rip6InstIfNode.u4Index = u4Index;
    Rip6InstIfNode.u4Instance = u4InstId;

    pRip6InstIfNode  = (tRip6InstIfNode *) RBTreeGetNext 
      ((gRip6InstIfTbl), (tRBElem *) &Rip6InstIfNode, NULL);

    return pRip6InstIfNode;
}

/*****************************************************************************/
/* Function Name      : Rip6DelRip6InstIfTblEntry                            */
/*                                                                           */
/* Description        : This routine deletes a tRip6InstIfNode from the      */
/*                      gRip6InstIfTbl.                                      */
/*                                                                           */
/* Input(s)           : pRip6InstIfNode - node to be deleted.                */
/*                                                                           */
/* Return Value(s)    : RIP6_SUCCESS/RIP6_FAILURE                            */
/*****************************************************************************/
INT4
Rip6DelRip6InstIfTblEntry (tRip6InstIfNode *pRip6InstIfNode)
{
    if (pRip6InstIfNode == NULL)
    {
        return RIP6_FAILURE;
    }
    if (RBTreeRemove ((gRip6InstIfTbl), 
            (tRBElem *) pRip6InstIfNode) == RB_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                 "RBTree remove failed \r\n");
        return RIP6_FAILURE;
    }
    MemReleaseMemBlock ((tMemPoolId) i4Rip6InstIfPoolId,
                        (UINT1 *) (pRip6InstIfNode));
    pRip6InstIfNode = NULL;
    return RIP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : Rip6DelAllRip6InstIfTblEntries                       */
/*                                                                           */
/* Description        : This function deletes all the tRip6InstIfNode        */
/*                      entries of gRip6InstIfTbl.                           */
/*                                                                           */
/* Input(s)           : NONE                                                 */
/*                                                                           */
/* Output(s)          : NONE                                                 */
/*                                                                           */
/* Return Value(s)    : NONE                                                 */
/*****************************************************************************/
VOID
Rip6DelAllRip6InstIfTblEntries ()
{
    tRip6InstIfNode   *pRip6InstIfNode = NULL;
    tRip6InstIfNode   *pNextRip6InstIfNode = NULL;

    pRip6InstIfNode = Rip6GetFirstRip6InstIfTblEntry ();

    while (pRip6InstIfNode != NULL)
    {
        /* Delete all the nodes */
        pNextRip6InstIfNode = Rip6GetNextRip6InstIfTblEntry 
            (pRip6InstIfNode->u4Instance, pRip6InstIfNode->u4Index);

        Rip6DelRip6InstIfTblEntry (pRip6InstIfNode);

        pRip6InstIfNode = pNextRip6InstIfNode;
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : Rip6SetIfEntryInRip6InstIfTbl                       */
 /*                                                                           */
 /* Description         : This function adds tRip6InstIfNode in Rip6InstIfTbl */
 /*                       for the given u4InstId, u4Index and sets            */
 /*                        pRip6If if pRip6If is not NULL.                    */
 /*                       If pRip6If is NULL, then tRip6InstIfNode for the    */
 /*                       given u4InstId, u4Index is deleted.                 */
 /*                                                                           */
 /* Input(s)            : u4Index       - Interface index.                    */
 /*                       u4InstId   - Instance identifier.                   */
 /*                       pRip6If    -  pointer to be set.                    */
 /*                                                                           */
 /* Output(s)           : NONE                                                */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
VOID
Rip6SetIfEntryInRip6InstIfTbl (UINT4 u4InstId, UINT4 u4Index, 
                               tRip6If *pRip6If)
{
    tRip6InstIfNode     *pRip6InstIfNode = NULL;

    if (pRip6If != NULL)
    {
        if (Rip6AddRip6InstIfTblEntry (u4InstId, u4Index, &pRip6InstIfNode)
                == RIP6_FAILURE)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                    "Adding tRip6InstIfNode is failed\r\n");
            return;
        }
        pRip6InstIfNode->pRip6If = pRip6If;
    }
    else
    {
        pRip6InstIfNode = Rip6GetRip6InstIfTblEntry (u4InstId, u4Index);
        if (pRip6InstIfNode == NULL)
        {
            return;
        }
        if (Rip6DelRip6InstIfTblEntry (pRip6InstIfNode) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                    "Deling tRip6InstIfNode is failed\r\n");
            return;
        }
    }
    return;
}

 /*****************************************************************************/
 /*                                                                           */
 /* Function Name       : Rip6GetIfEntryInRip6InstIfTbl                       */
 /*                                                                           */
 /* Description         : This function gets the pRip6If  for the gn u4Inst,  */
 /*                       u4Index in tRip6InstIfNode in Rip6InstIfTbl.        */
 /*                                                                           */
 /* Input(s)            : u4Index       - Interface index.                    */
 /*                       u4InstId   - Instance identifier.                   */
 /*                                                                           */
 /* Output(s)           : pRip6If  - pointer to the tRip6If.                  */
 /*                                                                           */
 /* Returns             : NONE                                                */
 /*****************************************************************************/
tRip6If *
Rip6GetIfEntryInRip6InstIfTbl  (UINT4 u4InstId, UINT4 u4Index)
{
    tRip6InstIfNode    *pRip6InstIfNode = NULL;

    pRip6InstIfNode = Rip6GetRip6InstIfTblEntry (u4InstId, u4Index);

    if (pRip6InstIfNode == NULL)
    {
        return NULL;
    }
    return (pRip6InstIfNode->pRip6If);
}


