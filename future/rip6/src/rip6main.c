/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6main.c,v 1.41 2017/12/14 10:26:43 siva Exp $
 *
 * Description: This file contains the routines for 
 *              handling the task related things
 *              and the interface related things along
 *
 *******************************************************************/

#include "rip6inc.h"
#ifdef NPAPI_WANTED
#include "npapi.h"
#endif
#ifdef FUTURE_SNMP_WANTED
#include "include.h"
#include "ipv6.h"

/*
 * Register with SNMP
 ********************/

#include "fsrip6_snmpmbdb.h"

INT4 RegisterFSRIP6withFutureSNMP PROTO ((void));

extern INT4 SNMP_AGT_RegisterMib PROTO ((tSNMP_GroupOIDType *,
                                         tSNMP_BaseOIDType *,
                                         tSNMP_MIBObjectDescrType *,
                                         tSNMP_GLOBAL_STRUCT, INT4));
#endif /* FUTURE_SNMP_WANTED */

extern VOID RegisterFSRIP6 PROTO ((void));
UINT4               gu4HoldOnFlag = OSIX_FALSE;
/*
 * Private Routines
 ******************/

PRIVATE INT4 Rip6TaskInit PROTO ((VOID));
PRIVATE INT4 Rip6IfInit PROTO ((VOID));

PRIVATE INT4 Rip6RtInit PROTO ((VOID));

PRIVATE INT4 Rip6InstanceDatabaseInit PROTO ((VOID));
PRIVATE INT4 Rip6InstanceInterfaceInit PROTO ((VOID));

PRIVATE INT4 Rip6ProcessRedisRoutes PROTO ((VOID));
PRIVATE INT4 Rip6ProfileInit PROTO ((VOID));
PRIVATE VOID Rip6ProcessRMapHandler PROTO ((tOsixMsg * pBuf));
PRIVATE INT4 Rip6RedisRouteAdd PROTO ((tRip6RtEntry * pRrdRtEntry));
PRIVATE INT4        Rip6AddNodeToRBTree
PROTO ((struct rbtree * pRBTree, tRip6RtEntry * pRrdRtEntry));
PRIVATE INT4 Rip6CheckExtRoutes PROTO ((UINT4 u4Index, INT4 i4Action));

#ifndef RIP6_SINGLE_INSTANCE
PRIVATE INT4 Rip6RedisRouteDelete PROTO ((tRip6RtEntry * pRrdRtEntry));
#endif

/*
 * Globals
 *********/

tOsixTaskId         i4Rip6TaskId;
tOsixTaskId         i4RR6TaskId;

tOsixSemId          gRip6SemTaskLockId;
tOsixSemId          gRip6Rtm6SemLockId;

/* provided for waiting in Udp Socket */
INT4                gi4Rip6SockId = -1;

/* Trace functionality */
UINT4               gu4Rip6DbgMap = 0;

 /* The id for the pool of routing entries */
INT4                i4Rip6rtId = 0;
INT4                i4Rip6RrdRouteInfoId = 0;
INT4                i4Rip6IfInfoPoolId = 0;
INT4                i4Rip6InstIfPoolId = 0;

#ifdef RIP6_ARRAY_TO_RBTREE_WANTED
tRBTree             gRip6IfInfoTbl;    /* It is added instead of garip6If and
                                       garip6InstanceInterfaceMap */
tRBTree             gRip6InstIfTbl;    /* It is added instead of arip6If in
                                       tInstanceDatabase */
#else
/* The RIP6 global interface table, to be used only when interface 
does not belong to an instance  */
tRip6If            *garip6If[MAX_RIP6_IFACES_LIMIT + 2];
/* The RIP6 Instance Interface Mapping table */
tInstanceInterfaceMap *garip6InstanceInterfaceMap[MAX_RIP6_IFACES_LIMIT + 2];
#endif
/* The RIP6 Instance Database Table */
tInstanceDatabase  *garip6InstanceDatabase[MAX_RIP6_CONTEXTS_LIMIT];

/* Current Instance Id of the event being processed */
UINT4               gInstanceId;    /*Used for Main module */
UINT4               gu4InstanceIndex;    /*Used for SNMP */
/* Instance Status indicating, if it's enabled or disabled */
UINT1               garip6InstanceStatus[MAX_RIP6_CONTEXTS_LIMIT];

tTimerListId        gRip6TimerListId;
tTimerListId        gRip6IntfTimerListId;
tTimerListId        gRip6RedisTmrListId;

INT4                i4Rip6ParamId = 0;

/* RIP6 If Table Pool Id */
INT4                i4Rip6IfPoolId;
INT4                i4Rip6ProfilePoolId;
INT4                i4Rip6InstancePoolId;
INT4                i4Rip6InstanceInterfacePoolId;
INT4                i4Rip6BufPoolId;

/* RM Pool Id */
tMemPoolId          gRip6RMPoolId;

tDbTblDescriptor    gRip6DynInfoList;
tDbDataDescInfo     gaRip6DynDataDescList[RIP6_MAX_DYN_INFO_TYPE];
tRip6RtMarker       gRip6RedRtMarker;
tOsixQId            gRip6RmPktQId;
UINT4               gu4Rip6SysLogId;
tRip6RedGblInfo     gRip6RedGblInfo;

tDbOffsetTemplate   gaRip6RtOffsetTbl[] =
    { {(FSAP_OFFSETOF (tRip6RtEntry, u4Index) -
        FSAP_OFFSETOF (tRip6RtEntry, RouteDbNode) - sizeof (tDbTblNode)), 4}
,
{(FSAP_OFFSETOF (tRip6RtEntry, u2RtTag) -
  FSAP_OFFSETOF (tRip6RtEntry, RouteDbNode) - sizeof (tDbTblNode)), 2}
,
{-1, 0}
};

tRip6Redistribution gRip6Redistribution;    /*Redistribution Global Info */

tFilteringRMap     *gpDistributeInFilterRMap = NULL;    /* Config for Distribute in filtering feature */
tFilteringRMap     *gpDistributeOutFilterRMap = NULL;    /* Config for Distribute out filtering feature */
tFilteringRMap     *gpDistanceFilterRMap = NULL;    /* Config for Distance filtering feature */

UINT1               gu1Rip6Distance = IP6_PREFERENCE_RIP;
INT4                gi4Rip6BulkUpdTestStatus = RIP6_INITIALIZE_ONE;
INT4                gi4Rip6DynUpdTestStatus = RIP6_INITIALIZE_ONE;

struct rbtree      *gRip6RBTree = NULL;
/******************************************************************************
 * DESCRIPTION : Called from Root Task. Initialises global parameters and
 *               calls routine for initializing RIP6 task. Registers RIP6
 *               with IP6.  
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/
VOID
Rip6Init ()
{
    Rip6SpawnTasks ();
    RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                  "RIP6 Task init success \n");
}

/******************************************************************************
 * DESCRIPTION : Does the various initializations and also waits on events from
 *               the Queues - RIP6_TASK_CONTROL_QUEUE  and
 *               RIP6_RIP_MESG_RECD_EVENT and also waits for Timeout indication
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     :  
 * NOTES       :
 ******************************************************************************/
VOID
Rip6TaskMain (INT1 *pDummy)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Event, u4Loop1;
    INT4                i4rcv_count;
    INT4                i4ip6local = NETIPV6_FAILURE;
    INT4                i4MaxIfs = 0;
    UINT4               u4Result = 0;
    struct sockaddr_in6 Rip6DestAddr;

    fd_set              ReadFds;    /* ReadBits */

    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    tIp6Addr            Ip6Addr;
    INT1                i1RegFlag = 0;
    INT4                i4RetVal = 0;
    struct cmsghdr      CmsgInfo;
    UINT1              *pau1Rip6Buf = NULL;
#ifdef IP6_WANTED
    INT4                i4Result = 0;
#endif
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4RetCRUVal, i4RelCRUVal;
    UINT4               u4DestAddrLen = sizeof (Rip6DestAddr);
    tRip6Udp6Params     Udp6Rip6Params;
    UINT4               u4BufLen;
#else
    INT4                i4OptEnable = 1;
    tRip6Udp6Params    *pUdp6Rip6Params = NULL;
    struct cmsghdr     *pCmsg;
    UINT1              *pu1CmsgInfo = NULL;
    struct iovec        Iov;
#endif
    UINT4               u4InstanceId = 0;
    tOsixQId            QId;
    UINT4               u4MaxCxts = 0;

    MEMSET (&ReadFds, 0, sizeof (fd_set));
    u4MaxCxts =
        RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                  FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                  u4PreAllocatedUnits);

    UNUSED_PARAM (pDummy);
    if (OsixCreateQ
        ((const UINT1 *) RIP6_TASK_CONTROL_QUEUE, RIP6_Q_DEPTH_DEF,
         RIP6_INITIALIZE_ZERO, &QId) != OSIX_SUCCESS)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                      RIP6_NAME, "RIP6_TASK_CONTROL_QUEUE Creation Failed \n");

        RIP6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#ifdef SNMP_2_WANTED
    RegisterFSRIP6 ();
/*     RegisterSTDRIP6 ();*/
#endif
    u4Loop1 = RIP6_INITIALIZE_TWO;    /* Just to avoid warning */
    /* Do initialization of global variables and tables */

    if (RIP6_CREATE_IF_RBTREE () == RIP6_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                      RIP6_NAME, "RIP6_CREATE_IF_RBTREE Creation Failed \n");

        RIP6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (RIP6_CREATE_INST_IF_RBTREE () == RIP6_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                      RIP6_NAME,
                      "RIP6_CREATE_INST_IF_RBTREE Creation Failed \n");

        RIP6_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    if (Rip6TaskInit () == RIP6_FAILURE)
    {
        Rip6Shutdown ();
        return;
    }

    /* Wait to Register with IP6, until IP6 Task Comes up */
    do
    {
#ifdef IP6_WANTED
        i4ip6local = NetIpv6RegisterHigherLayerProtocol (IP6_RIP_PROTOID,
                                                         NETIPV6_INTERFACE_PARAMETER_CHANGE,
                                                         (VOID *)
                                                         Rip6Ip6ifStatusNotify);
#endif
        if (i4ip6local == NETIPV6_SUCCESS)
        {
            i1RegFlag = RIP6_TRUE;
        }
    }
    while (i1RegFlag != RIP6_TRUE);

#ifdef IP6_WANTED
    /* Register with IP6, for the Address Change Notification. */
    i4Result = NetIpv6RegisterHigherLayerProtocol (IP6_RIP_PROTOID,
                                                   NETIPV6_ADDRESS_CHANGE,
                                                   (VOID *)
                                                   Rip6Ip6AddrStatusNotify);
    UNUSED_PARAM (i4Result);
#endif
    if (Rip6Rtm6Register () == RIP6_FAILURE)
    {
        return;
    }

    Rip6InitRedistribution ();

    /* Initialize RIPng timer list */
    u4Result = TmrCreateTimerList ((const UINT1 *) RIP6_TASK_NAME,
                                   (UINT4) RIP6_TIMER0_EVENT, NULL,
                                   &gRip6TimerListId);

    u4Result = TmrCreateTimerList ((const UINT1 *) RIP6_TASK_NAME,
                                   (UINT4) RIP6_TIMER1_EVENT, NULL,
                                   &gRip6IntfTimerListId);

    u4Result = TmrCreateTimerList ((const UINT1 *) RIP6_TASK_NAME,
                                   (UINT4) RIP6_1S_TIMER_EVENT, NULL,
                                   &gRip6RedisTmrListId);

    if (Rip6CreatePktRcvSocket () == RIP6_FAILURE)
    {
        return;
    }

    MEMSET (garip6InstanceStatus, RIP6_INITIALIZE_ZERO,
            MAX_RIP6_CONTEXTS_LIMIT);

    TMO_SLL_Init (&gRip6RedistributionList);

    gu4InstanceIndex = RIP6_DEFAULT_INSTANCE;
    gInstanceId = RIP6_DEFAULT_INSTANCE;

    /* create the RB tree for stroing the redistributed routes */
    gRip6RBTree = (struct rbtree *) RBTreeCreate ((MAX_RIP6_ROUTE_ENTRIES) / 2,
                                                  (tRBCompareFn)
                                                  Rip6ComapreRoutes);
    if (gRip6RBTree == NULL)
    {
        return;
    }

    /* Creation of the default Instance for RIP6 */
    Rip6InstanceCreation (RIP6_DEFAULT_INSTANCE);

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
#ifdef  RIP6_SINGLE_INSTANCE

    for (u4Loop1 = RIP6_INITIALIZE_ONE; u4Loop1 <= (UINT4) i4MaxIfs; u4Loop1++)
    {
        Rip6InterfaceInstanceAttach (RIP6_DEFAULT_INSTANCE, u4Loop1);
    }
#endif

    /* register callback for route map updates */
#ifdef ROUTEMAP_WANTED
    RMapRegister (RMAP_APP_RIP6, Rip6SendRouteMapUpdateMsg);
#endif

    RIP6_INIT_COMPLETE (OSIX_SUCCESS);
    /* Wait infinitely for the events to occur */
    pau1Rip6Buf = (UINT1 *) MemAllocMemBlk ((tMemPoolId) i4Rip6BufPoolId);

    if (NULL == pau1Rip6Buf)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                      RIP6_NAME,
                      "RIP6 : Memory not available for pau1Rip6Buf \n");

        RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                       RIP6_NAME,
                       "%s mem err: Type = %d  PoolId = %d Memptr = %p ",
                       ERROR_FATAL_STR,
                       ERR_MEM_CREATE, (UINT1 *) pau1Rip6Buf,
                       RIP6_INITIALIZE_ZERO);
        return;

    }
    MEMSET (pau1Rip6Buf, 0, sizeof (tRip6Buf));

#ifdef BSDCOMP_SLI_WANTED

    pu1CmsgInfo = (UINT1 *) MemAllocMemBlk ((tMemPoolId) i4Rip6BufPoolId);

    if (NULL == pu1CmsgInfo)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                      RIP6_NAME,
                      "RIP6 : Memory not available for pu1CmsgInfo \n");

        RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                       RIP6_NAME,
                       "%s mem err: Type = %d  PoolId = %d Memptr = %p ",
                       ERROR_FATAL_STR,
                       ERR_MEM_CREATE, (UINT1 *) pu1CmsgInfo,
                       RIP6_INITIALIZE_ZERO);

        if (MemReleaseMemBlock
            ((tMemPoolId) i4Rip6BufPoolId,
             (UINT1 *) pau1Rip6Buf) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                          "RIP6 : rip6BufPoolID, could not "
                          "release mem to pool \n");
            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = "
                           "%d Memptr = %p",
                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                           RIP6_INITIALIZE_ZERO, i4Rip6BufPoolId);
        }

        return;

    }

#endif

    while (RIP6_TRUE)
    {
        u4Event = 0;
        if (OsixReceiveEvent ((RIP6_CONTROL_EVENT | RIP6_TIMER0_EVENT
                               | RIP6_TIMER1_EVENT | RIP6_DATA_EVENT
                               | RIP6_1S_TIMER_EVENT |
                               RIP6_ROUTEMAP_UPDATE_EVENT |
                               RIP6_RM_PKT_EVENT |
                               RIP6_RM_PEND_RT_SYNC_EVENT |
                               RIP6_RM_START_TIMER_EVENT),
                              (OSIX_WAIT | OSIX_EV_ANY), RIP6_INITIALIZE_ZERO,
                              &u4Event) == RIP6_SUCCESS)
        {

            if (RIP6_TASK_LOCK () == SNMP_FAILURE)
            {
                continue;
            }

            if ((u4Event & RIP6_CONTROL_EVENT) == RIP6_CONTROL_EVENT)
            {
                while (OsixReceiveFromQ
                       (SELF, (const UINT1 *) RIP6_TASK_CONTROL_QUEUE,
                        OSIX_NO_WAIT, RIP6_INITIALIZE_ZERO,
                        &pBuf) == OSIX_SUCCESS)

                {
                    if (pBuf == NULL)
                    {
                        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                      "RIP6 : RIP6 task main,Got a NULL Buf ptr"
                                      "while processing RIP6_CONTROL_EVENT \n ");

                        continue;
                    }
#ifdef MBSM_WANTED
                    if ((CRU_BUF_Get_SourceModuleId (pBuf) ==
                         MBSM_MSG_CARD_INSERT)
                        || (CRU_BUF_Get_SourceModuleId (pBuf) ==
                            MBSM_MSG_CARD_REMOVE))
                    {
                        Rip6MbsmUpdateLCStatus (pBuf,
                                                CRU_BUF_Get_SourceModuleId
                                                (pBuf));
                    }
                    else
                    {
                        Rip6WrapperControl (pBuf);
                    }
#else
                    Rip6WrapperControl (pBuf);
#endif
                }

            }

            if ((u4Event & RIP6_ROUTEMAP_UPDATE_EVENT) ==
                RIP6_ROUTEMAP_UPDATE_EVENT)
            {
                while (OsixReceiveFromQ
                       (SELF, (const UINT1 *) RIP6_TASK_CONTROL_QUEUE,
                        OSIX_NO_WAIT, RIP6_INITIALIZE_ZERO,
                        &pBuf) == OSIX_SUCCESS)

                {
                    if (pBuf == NULL)
                    {
                        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                      "RIP6 : RIP6 task main,Got a NULL Buf ptr"
                                      "while processing RIP6_CONTROL_EVENT \n ");

                        continue;
                    }
                    Rip6ProcessRMapHandler (pBuf);
                }

            }

            if ((u4Event & RIP6_TIMER0_EVENT) == RIP6_TIMER0_EVENT)
            {
                /*Rip6TimerHandler (); */
                Rip6WrapperTimer ();
            }

            if ((u4Event & RIP6_TIMER1_EVENT) == RIP6_TIMER1_EVENT)
            {
                Rip6IntfWrapperTimer ();
            }

            if ((u4Event & RIP6_1S_TIMER_EVENT) == RIP6_1S_TIMER_EVENT)
            {
                Rip6ProcessRedisRoutes ();
            }

            if ((u4Event & RIP6_RM_PKT_EVENT) == RIP6_RM_PKT_EVENT)
            {
                Rip6RedHandleRmEvents (RIP6_PROCESS_ALL_MSG);
            }

            if ((u4Event & RIP6_RM_START_TIMER_EVENT) ==
                RIP6_RM_START_TIMER_EVENT)
            {
                Rip6RedStartTimers ();
            }

            if ((u4Event & RIP6_RM_PEND_RT_SYNC_EVENT) ==
                RIP6_RM_PEND_RT_SYNC_EVENT)
            {
                Rip6RedAddAllRouteNodeInDbTbl ();
            }

            FD_SET (gi4Rip6SockId, &ReadFds);
#ifndef BSDCOMP_SLI_WANTED

            u4BufLen = IP6_DEFAULT_MTU;
#endif

            Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

            if ((u4Event & RIP6_DATA_EVENT) == RIP6_DATA_EVENT)
            {
                while (RIP6_INITIALIZE_ONE)
                {
#ifndef BSDCOMP_SLI_WANTED
                    SET_ADDR_UNSPECIFIED (Ip6Addr);
                    Ip6AddrCopy ((tIp6Addr *) (VOID *)
                                 Rip6DestAddr.sin6_addr.s6_addr, &Ip6Addr);
                    Rip6DestAddr.sin6_port = RIP6_INITIALIZE_ZERO;

                    /* Wait to receive data on the socket */
                    i4rcv_count =
                        recvfrom (gi4Rip6SockId, (char *) pau1Rip6Buf,
                                  u4BufLen, RIP6_INITIALIZE_ZERO,
                                  (struct sockaddr *) &Rip6DestAddr,
                                  (INT4 *) &u4DestAddrLen);
                    if (i4rcv_count >= RIP6_FALSE)
                    {
                        recvmsg (gi4Rip6SockId, &Ip6MsgHdr,
                                 RIP6_INITIALIZE_ZERO);
                        pIn6Pktinfo =
                            (struct in6_pktinfo *) (VOID *)
                            CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

                        if (pIn6Pktinfo == NULL)
                        {
                            break;
                        }

                        /* store the required values from SOCKET LAYER to 
                           Params Structure */
                        Udp6Rip6Params.u4Index = pIn6Pktinfo->ipi6_ifindex;
                        Udp6Rip6Params.u2SrcPort =
                            OSIX_NTOHS (Rip6DestAddr.sin6_port);
                        Udp6Rip6Params.u2DstPort = (UINT2) RIP6_PORT;
                        Udp6Rip6Params.u4Len = i4rcv_count;
                        Udp6Rip6Params.i4Hlim = pIn6Pktinfo->in6_hoplimit;

                        i4RetVal = (INT4) sizeof (UINT1);

                        Ip6AddrCopy (&Udp6Rip6Params.srcAddr,
                                     (tIp6Addr *) (VOID *)
                                     &Rip6DestAddr.sin6_addr.s6_addr);
                        Ip6AddrCopy (&Udp6Rip6Params.dstAddr,
                                     (tIp6Addr *) (VOID *)
                                     &pIn6Pktinfo->ipi6_addr.s6_addr);

                        pBuf = CRU_BUF_Allocate_MsgBufChain (i4rcv_count,
                                                             RIP6_INITIALIZE_ZERO);
                        if (pBuf != NULL)
                        {
                            MEMSET (pBuf->ModuleData.au1ModuleInfo, 0,
                                    CRU_BUF_NAME_LEN);
                            CRU_BUF_UPDATE_MODULE_INFO (pBuf, "Rip6Tsk");
                            i4RetCRUVal = CRU_BUF_Copy_OverBufChain
                                (pBuf, pau1Rip6Buf, RIP6_INITIALIZE_ZERO,
                                 i4rcv_count);
                            if (i4RetCRUVal != RIP6_SUCCESS)
                            {
                                /* Must Free the Allocated Buffer, 
                                   if the control returns  */
                                i4RelCRUVal = CRU_BUF_Release_MsgBufChain
                                    (pBuf, FALSE);
                                if (i4RelCRUVal == RIP6_FAILURE)
                                {
                                    RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                                   RIP6_BUFFER_TRC |
                                                   RIP6_ALL_FAIL_TRC,
                                                   RIP6_NAME,
                                                   "RIP6 process control msg, "
                                                   "Failure in releasing Buff "
                                                   "= %p \n", pBuf);
                                }
                                break;
                            }
                        }
                        else
                        {
                            break;
                        }
                        Rip6WrapperInput (&Udp6Rip6Params, pBuf);
                    }            /* rcv_count */
                    else
                    {            /* no more packets */
                        break;
                    }
#else
                    if (setsockopt
                        (gi4Rip6SockId, IPPROTO_IPV6, IPV6_RECVPKTINFO,
                         &(i4OptEnable), sizeof (INT4)) < 0)
                    {
                        break;
                    }

                    SET_ADDR_UNSPECIFIED (Ip6Addr);
                    Ip6AddrCopy ((tIp6Addr *) (VOID *) Rip6DestAddr.sin6_addr.
                                 s6_addr, &Ip6Addr);
                    Rip6DestAddr.sin6_port = 0;

                    /* Wait to receive data on the socket */
                    MEMSET (pu1CmsgInfo, 0, sizeof (tRip6Buf));
                    MEMSET (pau1Rip6Buf, 0, sizeof (tRip6Buf));

                    Ip6MsgHdr.msg_name = (void *) &Rip6DestAddr;
                    Ip6MsgHdr.msg_namelen = sizeof (Rip6DestAddr);
                    Iov.iov_base = pau1Rip6Buf;
                    Iov.iov_len = IP6_DEFAULT_MTU;
                    Ip6MsgHdr.msg_iov = &Iov;
                    Ip6MsgHdr.msg_iovlen = 1;
                    Ip6MsgHdr.msg_control = (VOID *) pu1CmsgInfo;
                    Ip6MsgHdr.msg_controllen = IP6_DEFAULT_MTU;

                    i4rcv_count = recvmsg (gi4Rip6SockId, &Ip6MsgHdr, 0);
                    if (i4rcv_count > 0)
                    {
                        /* Check for the type of HDR and then process */

                        pIn6Pktinfo = NULL;
                        for (pCmsg = CMSG_FIRSTHDR (&Ip6MsgHdr); pCmsg != NULL;
                             pCmsg = CMSG_NXTHDR (&Ip6MsgHdr, pCmsg))
                        {

                            if ((pCmsg->cmsg_level == IPPROTO_IPV6) &&
                                (pCmsg->cmsg_type == IPV6_PKTINFO))
                            {
                                pIn6Pktinfo =
                                    (struct in6_pktinfo *) (VOID *)
                                    CMSG_DATA (pCmsg);
                            }
                        }

                        if (NULL == pIn6Pktinfo)
                        {
                            continue;
                        }

                        pIn6Pktinfo = (struct in6_pktinfo *) (VOID *)
                            CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

                        pUdp6Rip6Params =
                            MEM_MALLOC (sizeof (tRip6Udp6Params),
                                        tRip6Udp6Params);
                        if (pUdp6Rip6Params == NULL)
                        {
                            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC |
                                           RIP6_ALL_FAIL_TRC, RIP6_NAME,
                                           "RIP6 : Malloc called failed\n",
                                           pBuf);
                            break;
                        }

                        /*
                         * Store the required values from SOCKET LAYER to 
                         * Params Structure.
                         */

                        NetIpv6GetCfaIfIndexFromPort (pIn6Pktinfo->ipi6_ifindex,
                                                      &pUdp6Rip6Params->
                                                      u4Index);
                        pUdp6Rip6Params->u2SrcPort =
                            OSIX_NTOHS (Rip6DestAddr.sin6_port);
                        pUdp6Rip6Params->u2DstPort = (UINT2) RIP6_PORT;
                        pUdp6Rip6Params->u4Len = i4rcv_count;

                        i4RetVal = (INT4) sizeof (UINT1);

                        if (getsockopt (gi4Rip6SockId, IPPROTO_IPV6,
                                        IPV6_MULTICAST_HOPS,
                                        &pUdp6Rip6Params->i4Hlim,
                                        &i4RetVal) < 0)
                        {
                            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC |
                                          RIP6_ALL_FAIL_TRC, RIP6_NAME,
                                          "RIP6 : getsockopt failed\n");
                            MEM_FREE (pUdp6Rip6Params);
                            continue;
                        }

                        Ip6AddrCopy (&pUdp6Rip6Params->srcAddr,
                                     (tIp6Addr *) (VOID *) &Rip6DestAddr.
                                     sin6_addr.s6_addr);
                        Ip6AddrCopy (&pUdp6Rip6Params->dstAddr,
                                     (tIp6Addr *) (VOID *) &pIn6Pktinfo->
                                     ipi6_addr.s6_addr);

                        if ((pBuf = CRU_BUF_Allocate_MsgBufChain
                             (i4rcv_count, 0)) != NULL)
                        {
                            MEMSET (pBuf->ModuleData.au1ModuleInfo, 0,
                                    CRU_BUF_NAME_LEN);
                            CRU_BUF_UPDATE_MODULE_INFO (pBuf, "Rip6TaskMain");
                            if (CRU_BUF_Copy_OverBufChain
                                (pBuf, pau1Rip6Buf, 0,
                                 i4rcv_count) != RIP6_SUCCESS)
                            {
                                /*
                                 * Must Free the Allocated Buffer, if the control
                                 * returns.
                                 */
                                if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE)
                                    == RIP6_FAILURE)
                                {
                                    RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                                   RIP6_BUFFER_TRC |
                                                   RIP6_ALL_FAIL_TRC, RIP6_NAME,
                                                   "RIP6 : RIP6 process control"
                                                   " msg, Failure in releasing "
                                                   "Buff == %p\n", pBuf);
                                }
                                MEM_FREE (pUdp6Rip6Params);
                            }
                        }
                        else
                        {
                            MEM_FREE (pUdp6Rip6Params);
                        }
                        Rip6Input (pUdp6Rip6Params, pBuf);
                    }            /* rcv_count */
                    else
                    {            /* no more packets */
                        break;
                    }
#endif
                }                /* select */

                /* Have completed processing all the packets. Perform Trigger
                 * update if necessary. */
                for (u4InstanceId = 0; u4InstanceId < u4MaxCxts; u4InstanceId++)
                {
                    if (garip6InstanceDatabase[u4InstanceId]->u1TrigFlag ==
                        RIP6_TRIGGERED_UPDATE_NEEDED)
                    {
                        Rip6CheckTrigUpdateSendForResponseMsg
                            (u4InstanceId, (UINT1) ~RIP6_BULK_DELETE);
                        garip6InstanceDatabase[u4InstanceId]->u1TrigFlag =
                            RIP6_TRIGGERED_UPDATE_NOT_NEEDED;
                    }
                }
            }
            RIP6_TASK_UNLOCK ();
        }
    }                            /* while */

    /* Memory Release */

    if (MemReleaseMemBlock ((tMemPoolId) i4Rip6BufPoolId, (UINT1 *) pau1Rip6Buf)
        != RIP6_SUCCESS)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "RIP6 : rip6BufPoolID, could not "
                      "release mem to pool \n");
        RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                       "%s mem err: Type = %d  PoolId = "
                       "%d Memptr = %p",
                       ERROR_FATAL_STR, ERR_MEM_RELEASE,
                       RIP6_INITIALIZE_ZERO, i4Rip6BufPoolId);
    }

#ifdef BSDCOMP_SLI_WANTED

    if (MemReleaseMemBlock ((tMemPoolId) i4Rip6BufPoolId, (UINT1 *) pu1CmsgInfo)
        != RIP6_SUCCESS)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "RIP6 : rip6 static add, could not "
                      "release mem to pool \n");
        RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                       "%s mem err: Type = %d  PoolId = "
                       "%d Memptr = %p",
                       ERROR_FATAL_STR, ERR_MEM_RELEASE,
                       RIP6_INITIALIZE_ZERO, i4Rip6BufPoolId);
    }

#endif
    UNUSED_PARAM (u4Result);
    UNUSED_PARAM (i4RetVal);
}

/******************************************************************************
 * DESCRIPTION : Creates a socket, binds and sets option to receive the
 *               packets sent by UDP6 to RIP6 through the Socket Layer 
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/
INT4
Rip6CreatePktRcvSocket ()
{

    struct sockaddr_in6 Rip6SrcAddr;
    INT4                i4Optval = RIP6_INITIALIZE_ONE;
    INT4                i4HopLimit = 255;
    tIp6Addr            Ip6Addr;

    /* Fill in local address details */
    Rip6SrcAddr.sin6_family = AF_INET6;
    Rip6SrcAddr.sin6_port = OSIX_HTONS (RIP6_PORT);
    Rip6SrcAddr.sin6_flowinfo = RIP6_INITIALIZE_ZERO;

    SET_ADDR_UNSPECIFIED (Ip6Addr);
    Ip6AddrCopy ((tIp6Addr *) (VOID *) &Rip6SrcAddr.sin6_addr.s6_addr,
                 &Ip6Addr);

    /* Create socket */
    gi4Rip6SockId = socket (AF_INET6, SOCK_DGRAM, RIP6_INITIALIZE_ZERO);
    if (gi4Rip6SockId == RIP6_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                      RIP6_NAME, "RIP6: socket Creation Failure \n");
        return RIP6_FAILURE;
    }
    if (fcntl (gi4Rip6SockId, F_SETFL, O_NONBLOCK) != RIP6_SUCCESS)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "RIP6: Unbale to set socket in non-blocking mode \n");
        return RIP6_FAILURE;
    }

    /* Bind address details to local socket */
    if (bind (gi4Rip6SockId,
              (struct sockaddr *) (VOID *) &Rip6SrcAddr,
              sizeof (Rip6SrcAddr)) == RIP6_SUCCESS)
    {
        if (setsockopt (gi4Rip6SockId, IPPROTO_IPV6,
                        IP_PKTINFO,
                        &i4Optval, sizeof (i4Optval)) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                          RIP6_NAME,
                          "RIP6: setsockopt(IPPROTO_IPv6)  Failure \n");
            return RIP6_FAILURE;
        }
        if (setsockopt (gi4Rip6SockId, IPPROTO_IPV6,
                        IPV6_RECVHOPLIMIT,
                        &i4Optval, sizeof (INT4)) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                          RIP6_NAME,
                          "RIP6: setsockopt(IPV6_RECVHOPLIMIT)  Failure \n");
            return RIP6_FAILURE;
        }

        if (setsockopt (gi4Rip6SockId, IPPROTO_IPV6, IPV6_UNICAST_HOPS,
                        &i4HopLimit, sizeof (INT4)) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                          RIP6_NAME,
                          "RIP6: setsockopt(IPV6_UNICAST_HOPS)  Failure \n");
            return RIP6_FAILURE;
        }

        if (setsockopt (gi4Rip6SockId, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                        &i4HopLimit, sizeof (INT4)) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                          RIP6_NAME,
                          "RIP6: setsockopt(IPV6_MULTICAST_HOPS)  Failure \n");
            return RIP6_FAILURE;
        }

        if (fcntl (gi4Rip6SockId, F_SETFL, O_NONBLOCK) < 0)
        {
            close (gi4Rip6SockId);
            gi4Rip6SockId = -1;
            return RIP6_FAILURE;
        }

        return RIP6_SUCCESS;
    }

    RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC,
                  RIP6_NAME, "RIP6: bind Failure \n");
    return RIP6_FAILURE;

}

/******************************************************************************
 * DESCRIPTION : This function is responsible for creation of the instance.
 * INPUTS      : NONE
 * OUTPUTS     : NONE
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/
INT4
Rip6InstanceCreation (UINT4 u4InstId)
{

    UINT1               u1Loop1;
    INT4                i4TrieCrStatus = RIP6_SUCCESS;
    INT4                i4MaxProfs = 0;

    i4MaxProfs = RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                           FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                           u4PreAllocatedUnits);
    garip6InstanceDatabase[u4InstId]->u4RtTableRoutes = RIP6_INITIALIZE_ZERO;
    garip6InstanceDatabase[u4InstId]->u4Rip6EnabledIfs = RIP6_INITIALIZE_ZERO;
    garip6InstanceDatabase[u4InstId]->u4LookupPreference =
        RIP6_BEST_METRIC_LOOKUP;
    garip6InstanceDatabase[u4InstId]->u4Rip6PeerFlag = RIP6_DENY;
    garip6InstanceDatabase[u4InstId]->u4Rip6AdvFltFlag = RIP6_DISABLE;

    i4TrieCrStatus = Rip6TrieCreate (u4InstId);
    if (i4TrieCrStatus == RIP6_FAILURE)
    {
        return RIP6_FAILURE;
    }

    for (u1Loop1 = RIP6_INITIALIZE_ZERO; u1Loop1 < i4MaxProfs; u1Loop1++)
    {
        garip6InstanceDatabase[u4InstId]->arip6Profile[u1Loop1] =
            (tRip6Profile *) MemAllocMemBlk ((tMemPoolId) i4Rip6ProfilePoolId);
        if (garip6InstanceDatabase[u4InstId]->arip6Profile[u1Loop1] != NULL)
        {
            garip6InstanceDatabase[u4InstId]->arip6Profile[u1Loop1]->u1Status =
                RIP6_PROFILE_INVALID;
            garip6InstanceDatabase[u4InstId]->arip6Profile[u1Loop1]->u1Horizon =
                RIP6_DFLT_HORIZON;
            garip6InstanceDatabase[u4InstId]->arip6Profile[u1Loop1]->u4GcTime =
                RIP6_DFLT_GARBAGE_COLLECT_TIME;
            garip6InstanceDatabase[u4InstId]->arip6Profile[u1Loop1]->
                u4RouteAge = RIP6_DFLT_ROUTE_AGE;
            garip6InstanceDatabase[u4InstId]->arip6Profile[u1Loop1]->
                u4TrigDelayTime = RIP6_DFLT_TRIG_DELAY_TIME;
            garip6InstanceDatabase[u4InstId]->arip6Profile[u1Loop1]->
                u4PeriodicUpdTime = RIP6_DFLT_PERIODIC_UPD_TIME;
        }
        else
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                           RIP6_NAME,
                           "RIP6 : Memory not available for arip6Profile[%d]\n",
                           u1Loop1);

            RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                           RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                           ERROR_FATAL_STR,
                           ERR_MEM_CREATE,
                           (UINT1 *) garip6InstanceDatabase[u4InstId]->
                           arip6Profile[u1Loop1], RIP6_INITIALIZE_ZERO);

            return RIP6_FAILURE;
        }
    }
    /* Initializing Profile Table for Default Profile Index 0 */
    garip6InstanceDatabase[u4InstId]->arip6Profile[RIP6_INITIALIZE_ZERO]->
        u1Status = (UINT1) RIP6_PROFILE_UP;

    garip6InstanceStatus[u4InstId] = RIP6_INST_UP;    /* Making the Instance
                                                       Status UP */

    garip6InstanceDatabase[u4InstId]->Rip6RtGlobal.u4InstanceId = u4InstId;

    garip6InstanceDatabase[u4InstId]->u1TrigFlag =
        RIP6_TRIGGERED_UPDATE_NOT_NEEDED;

    Rip6TmrStart (RIP6_Regular_TIMER_ID, gRip6TimerListId,
                  &(garip6InstanceDatabase[u4InstId]->Rip6RtGlobal.timer),
                  RIP6_INITIALIZE_TWENTYONE);

    Rip6TmrStart (RIP6_1S_TIMER_ID, gRip6RedisTmrListId,
                  &(garip6InstanceDatabase[u4InstId]->Rip6RrdGlobal.timer),
                  RIP6_INITIALIZE_ONE);

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function is responsible for deletion of the instance.
 * INPUTS      : NONE
 * OUTPUTS     : NONE
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/
INT4
Rip6InstanceDeletion (UINT4 u4InstId)
{

    Rip6MIPurgeAll (u4InstId);

    if (Rip6TrieDelete (u4InstId) == RIP6_FAILURE)
    {
        return RIP6_FAILURE;
    }

    MEMSET (garip6InstanceDatabase[u4InstId], RIP6_INITIALIZE_ZERO,
            sizeof (tInstanceDatabase));

    garip6InstanceStatus[u4InstId] = RIP6_INST_DOWN;
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function is responsible for attaching of interface to 
 *               an instance. 
 * INPUTS      : NONE
 * OUTPUTS     : NONE
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/
INT4
Rip6InterfaceInstanceAttach (UINT4 u4InstId, UINT4 u4IntrId)
{
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    UNUSED_PARAM (u4MaxIfs);
#ifndef RIP6_SINGLE_INSTANCE

    if (u4IntrId != u4MaxIfs)
    {
        if (!
            ((RIP6_GET_IF_ENTRY (u4IntrId)->u1Status & RIP6_IFACE_UP) ==
             RIP6_IFACE_UP))
        {
            return RIP6_FAILURE;
        }
    }
#endif
    RIP6_SET_INST_IF_ENTRY (u4InstId, u4IntrId, RIP6_GET_IF_ENTRY (u4IntrId));
    RIP6_GET_INST_IF_MAP_ENTRY (u4IntrId)->u4InsIfStatus = RIP6_IFACE_ATTACHED;
    RIP6_GET_INST_IF_MAP_ENTRY (u4IntrId)->u4InstanceId = u4InstId;

    return RIP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : This function is responsible for detaching of an interface 
 *               from an instance.
 * INPUTS      : NONE
 * OUTPUTS     : NONE
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/
INT4
Rip6InterfaceInstanceDetach (UINT4 u4IntrId)
{

    tIp6Addr            LinkLocalAddr;
    UINT4               u4DefRouteAdvt;
    UINT4               u4InstanceId;
    UINT1               u1Status, u1Cost;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    UNUSED_PARAM (u4MaxIfs);

#ifndef RIP6_SINGLE_INSTANCE
    if (u4IntrId != u4MaxIfs)
    {
        if ((RIP6_GET_IF_ENTRY (u4IntrId)->u1Status & RIP6_IFACE_ENABLED) ==
            RIP6_IFACE_ENABLED)
        {
            return RIP6_FAILURE;
        }
    }
#endif

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4IntrId)->u4InstanceId;

    /* Stop periodic and triggered timers */
    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->
        periodicTimer.u1Id != RIP6_INITIALIZE_ZERO)
    {
        Rip6TmrStop (RIP6_UPDATE_TIMER_ID,
                     gRip6IntfTimerListId,
                     &(RIP6_UPDATE_TIMER_NODE (u4IntrId)));
    }

    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->
        trigDelayTimer.u1Id != RIP6_INITIALIZE_ZERO)
    {
        Rip6TmrStop (RIP6_TRIG_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_DELAY_TIMER_NODE (u4IntrId)));
    }
    /* Reading the IfStatus before initialize it to Zero */
    u1Status = RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->u1Status;
    u1Cost = RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->u1Cost;
    /* Reading the Link-Local Address and Default Route Advt Policy. */
    Ip6AddrCopy (&LinkLocalAddr,
                 &RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->
                 LinkLocalAddr);
    u4DefRouteAdvt =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->u4DefRouteAdvt;

    MEMSET (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId),
            RIP6_INITIALIZE_ZERO, sizeof (tRip6If));

    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->u4Index = u4IntrId;

    /*Assigning the IfStatus back to the existing Status at the 
       time of detachment */
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->u1Status = u1Status;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->u1Cost = u1Cost;
    /* Assigning the Link-Local Address and Default Route Advt Policy. */
    Ip6AddrCopy (&RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->
                 LinkLocalAddr, &LinkLocalAddr);
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4IntrId)->u4DefRouteAdvt =
        u4DefRouteAdvt;

    /* Dtaching the Interface from the Global If database */
    RIP6_SET_INST_IF_ENTRY (u4InstanceId, u4IntrId, NULL);

    RIP6_GET_INST_IF_MAP_ENTRY (u4IntrId)->u4InsIfStatus = RIP6_IFACE_DETACHED;
    RIP6_GET_INST_IF_MAP_ENTRY (u4IntrId)->u4InstanceId
        = (UINT4) RIP6_INVALID_INSTANCE;
    if (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs
        != RIP6_INITIALIZE_ZERO)
    {
        --(garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs);
    }

    return RIP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : Does the initialization of the globals and various tables
 *               related to RIP6
 * INPUTS      : NONE
 * OUTPUTS     : NONE
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/
PRIVATE INT4
Rip6TaskInit ()
{
    /* Create Mempools required by the module */
    Rip6SizingMemCreateMemPools ();

    /* Mempool identifier Variable assignment */
    i4Rip6rtId = RIP6MemPoolIds[MAX_RIP6_ROUTE_ENTRIES_SIZING_ID];
    i4Rip6IfPoolId = RIP6MemPoolIds[MAX_RIP6_IFACES_SIZING_ID];
    i4Rip6InstancePoolId = RIP6MemPoolIds[MAX_RIP6_INSTANCE_SIZING_ID];
    i4Rip6InstanceInterfacePoolId =
        RIP6MemPoolIds[MAX_RIP6_IFACES_INSTANCE_MAP_SIZING_ID];
    i4Rip6ProfilePoolId = RIP6MemPoolIds[MAX_RIP6_PROFILES_SIZING_ID];
    i4Rip6RrdRouteInfoId = RIP6MemPoolIds[MAX_RIP6_RRD_ROUTES_SIZING_ID];
    i4Rip6BufPoolId = RIP6MemPoolIds[MAX_RIP6_BUF_SIZING_ID];
    gRip6RMPoolId = RIP6MemPoolIds[MAX_RIP6_RM_QUEUE_DEPTH_SIZING_ID];
    i4Rip6IfInfoPoolId = RIP6MemPoolIds[MAX_RIP6_IF_INFO_ENTRIES_SIZING_ID];
    i4Rip6InstIfPoolId = RIP6MemPoolIds[MAX_RIP6_INST_IF_ENTRIES_SIZING_ID];

    if ((Rip6TaskSemInit () == RIP6_FAILURE) ||
        (Rip6IfInit () == RIP6_FAILURE) ||
        (Rip6RtInit () == RIP6_FAILURE) ||
        (Rip6ProfileInit () == RIP6_FAILURE) ||
        (Rip6InstanceDatabaseInit () == RIP6_FAILURE) ||
        (Rip6InstanceInterfaceInit () == RIP6_FAILURE) ||
        (Rip6RedInitGlobalInfo () == RIP6_FAILURE))
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                      RIP6_NAME, "IPv6 init failed \n");

        return RIP6_FAILURE;
    }

    Rip6RedDynDataDescInit ();

    Rip6RedDescrTblInit ();
#ifdef NPAPI_WANTED
    /* Initialise the NPAPI Related data structures. */
    if (Rip6FsNpRip6Init () != FNP_SUCCESS)
    {
        return (RIP6_FAILURE);
    }
#endif

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Does the initialization of RIP6 task 
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/
INT4
Rip6SpawnTasks ()
{
    RIP6_INIT_COMPLETE (OSIX_SUCCESS);
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Initializes the routing table related structures for RIP6
 * INPUTS      : NONE
 * OUTPUTS     : NONE
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/

PRIVATE INT4
Rip6RtInit ()
{

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Initializes the RIP6_IF table
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/
PRIVATE INT4
Rip6IfInit ()
{
    tRip6If            *pRip6If = NULL;
    UINT2               u2i;
    INT4                i4MaxIfs = 0;

    i4MaxIfs = (INT4) RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                                FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                                u4PreAllocatedUnits);

    for (u2i = RIP6_INITIALIZE_ONE;
         (u2i <= i4MaxIfs && u2i < MAX_RIP6_IFACES_LIMIT + 2); u2i++)
    {
        if (NULL == (pRip6If = (tRip6If *)
                     MemAllocMemBlk ((tMemPoolId) i4Rip6IfPoolId)))
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                           RIP6_NAME,
                           "RIP6 : Memory not available for garip6If[%d] \n",
                           u2i);

            RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                           RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = %d Memptr = %p ",
                           ERROR_FATAL_STR,
                           ERR_MEM_CREATE, (UINT1 *) pRip6If,
                           RIP6_INITIALIZE_ZERO);

            return RIP6_FAILURE;
        }
        RIP6_SET_IF_ENTRY (u2i, pRip6If);
        MEMSET (RIP6_GET_IF_ENTRY (u2i), RIP6_INITIALIZE_ZERO,
                sizeof (tRip6If));
        RIP6_GET_IF_ENTRY (u2i)->u4Index = u2i;
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Initializes the RIP6_Instance_database table
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/
PRIVATE INT4
Rip6InstanceDatabaseInit ()
{

    UINT4               u4Loop1;
    UINT4               u4MaxCxts = 0;

    u4MaxCxts = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                          FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                          u4PreAllocatedUnits);

    for (u4Loop1 = RIP6_INITIALIZE_ZERO; u4Loop1 < u4MaxCxts; u4Loop1++)
    {
        if (NULL == (garip6InstanceDatabase[u4Loop1] = (tInstanceDatabase *)
                     MemAllocMemBlk ((tMemPoolId) i4Rip6InstancePoolId)))
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                           RIP6_NAME,
                           "RIP6 : Memory not available for"
                           " garip6InstanceDatabase[%d] \n", u4Loop1);

            RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                           RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = %d Memptr = %p ",
                           ERROR_FATAL_STR,
                           ERR_MEM_CREATE,
                           (UINT1 *) garip6InstanceDatabase[u4Loop1],
                           RIP6_INITIALIZE_ZERO);

            return RIP6_FAILURE;
        }

        MEMSET (garip6InstanceDatabase[u4Loop1], RIP6_INITIALIZE_ZERO,
                sizeof (tInstanceDatabase));

    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Initializes the RIP6 Instance Interface Mapping Table
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/
PRIVATE INT4
Rip6InstanceInterfaceInit ()
{
    tInstanceInterfaceMap *pInstIfaceMap = NULL;
    UINT2               u2i;
    INT4                i4MaxIfs = 0;

    i4MaxIfs = (INT4) RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                                FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                                u4PreAllocatedUnits);

    for (u2i = RIP6_INITIALIZE_ONE;
         (u2i <= i4MaxIfs && u2i < MAX_RIP6_IFACES_LIMIT + 2); u2i++)
    {
        if (NULL == (pInstIfaceMap = (tInstanceInterfaceMap *)
                     MemAllocMemBlk ((tMemPoolId)
                                     i4Rip6InstanceInterfacePoolId)))
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                           RIP6_NAME,
                           "RIP6 : Memory not available for "
                           "garip6InstanceInterfaceMap[%d] \n", u2i);

            RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                           RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = %d Memptr = %p ",
                           ERROR_FATAL_STR,
                           ERR_MEM_CREATE,
                           (UINT1 *) pInstIfaceMap, RIP6_INITIALIZE_ZERO);

            return RIP6_FAILURE;
        }
        RIP6_SET_INST_IF_MAP_ENTRY (u2i, pInstIfaceMap);

        MEMSET (RIP6_GET_INST_IF_MAP_ENTRY (u2i), RIP6_INITIALIZE_ZERO,
                sizeof (tInstanceInterfaceMap));
        RIP6_GET_INST_IF_MAP_ENTRY (u2i)->u4IfIndex = u2i;
        RIP6_GET_INST_IF_MAP_ENTRY (u2i)->u4InstanceId =
            (UINT4) RIP6_INVALID_INSTANCE;
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Initializes the RIP6 Profile table
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS - if initialization are through
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/

PRIVATE INT4
Rip6ProfileInit ()
{
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : This function is responsible to decipher between the events 
          belonging to different Instances, and set the Global 
         Variable InstanceId, so that the handler is aware of the 
         instance, before handling the event.
 * INPUTS      : The buffer containing the control informations (pBuf)
 * OUTPUTS     : None
 * RETURNS     : None 
 * NOTES       :
 ******************************************************************************/

VOID
Rip6WrapperControl (pBuf)
     tCRU_BUF_CHAIN_HEADER *pBuf;
{
    tRip6Ip6Params     *pRip6Ip6Params = NULL;

    /* Get tRip6Ip6Params from buffer */
    pRip6Ip6Params = (tRip6Ip6Params *) (VOID *) RIP6_BUF_DATA_PTR (pBuf);

    if (RIP6_GET_INST_IF_MAP_ENTRY (pRip6Ip6Params->u4Index) == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }
    gInstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRip6Ip6Params->
                                              u4Index)->u4InstanceId;

    Rip6ProcessControlMsg (pBuf);

}

/******************************************************************************
 * DESCRIPTION : Does the processing of the control messages received from the 
 *               IP6 module 
 * INPUTS      : The buffer containing the control informations (pBuf)
 * OUTPUTS     : None
 * RETURNS     : None 
 * NOTES       :
 ******************************************************************************/

INT4
Rip6ProcessControlMsg (pBuf)
     tCRU_BUF_CHAIN_HEADER *pBuf;
{
    tRip6Ip6Params     *pRip6Ip6Params = NULL;
    INT4                i4Status = RIP6_SUCCESS;

    /* Get tRip6Ip6Params from buffer */
    pRip6Ip6Params = (tRip6Ip6Params *) (VOID *) RIP6_BUF_DATA_PTR (pBuf);

    switch (pRip6Ip6Params->u4Command)
    {
        case RIP6_INTF_STATUS_CHANGE:
            switch (pRip6Ip6Params->u1Stat)
            {
                case NETIPV6_IF_CREATE:
                    i4Status = Rip6IfCreate (pRip6Ip6Params->u4Index);
                    break;

                case NETIPV6_IF_DELETE:
                    i4Status = Rip6IfDelete (pRip6Ip6Params->u4Index);
                    break;

                case NETIPV6_IF_UP:
                    i4Status = Rip6IfUp (pRip6Ip6Params->u4Index);
                    break;

                case NETIPV6_IF_DOWN:
                    i4Status = Rip6IfDown (pRip6Ip6Params->u4Index);
                    break;

                default:
                    i4Status = RIP6_FAILURE;
                    break;
            }
            break;

        case RIP6_ADDR_STATUS_CHANGE:
            if (IS_ADDR_LLOCAL (pRip6Ip6Params->addr))
            {
                /* Handle the change in the IPv6 Link-Local Address */
                i4Status = Rip6ProcLinkLocalAddrChg (&pRip6Ip6Params->addr,
                                                     pRip6Ip6Params->u4Index,
                                                     pRip6Ip6Params->u1Type);
            }
            else
            {
                /* Handle the change in the IPv6 unicast/anycast address */
                i4Status = Rip6ProcIp6AddrChg (&pRip6Ip6Params->addr,
                                               pRip6Ip6Params->u1Prefixlen,
                                               pRip6Ip6Params->u4Index,
                                               pRip6Ip6Params->u1Type);
            }
            break;
    }

    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
    {
        RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                       RIP6_BUFFER_TRC |
                       RIP6_ALL_FAIL_TRC, RIP6_NAME,
                       "RIP6 : RIP6 process control msg, "
                       "Failure in releasing Buff = %p \n", pBuf);
    }
    return i4Status;
}

/******************************************************************************
 * DESCRIPTION : Set the preference for lookup to best metric
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : The global u4LookupPreference reflects the lookup
 *               preference
 ******************************************************************************/
VOID
Rip6SetBestMetricLookup (UINT4 u4InstanceId)
{
    garip6InstanceDatabase[u4InstanceId]->u4LookupPreference =
        RIP6_BEST_METRIC_LOOKUP;
}

/******************************************************************************
 * DESCRIPTION : Set the preference for lookup to static lookup 
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : The global u4LookupPreference reflects the lookup
 *               preference
 ******************************************************************************/
VOID
Rip6SetStaticLookup (UINT4 u4InstanceId)
{
    garip6InstanceDatabase[u4InstanceId]->u4LookupPreference =
        RIP6_STATIC_LOOKUP;
}

/******************************************************************************
 * DESCRIPTION : Set the preference for lookup to dynamic
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : The global u4LookupPreference reflects the lookup
 *               preference
 ******************************************************************************/
VOID
Rip6SetDynamicLookup (UINT4 u4InstanceId)
{
    garip6InstanceDatabase[u4InstanceId]->u4LookupPreference =
        RIP6_DYNAMIC_LOOKUP;
}

/******************************************************************************
 * DESCRIPTION : Enable RIP6 on an interface 
 * INPUTS      : The logical interface (u4Index)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a Control message from IP6 module
 ******************************************************************************/

INT4
Rip6IfEnable (UINT4 u4Index)
{
    tIp6Addr            dstAddr, null_addr;
    tIp6Addr            Rip6MultiAddr;
    tNetIpv6AddrInfo    Ipv6AddrInfo;
    tNetIpv6AddrInfo    Ipv6PrevAddrInfo;
    tRip6RtEntry       *pRtEntry = NULL;
    UINT4               u4InstanceId;
    INT4                i4Status = NETIPV6_SUCCESS;
    UINT1               u1Metric = 0;
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;
#ifdef LNXIP6_WANTED
    tNetIpv6IfInfo      NetIpv6IfInfo;
#endif
#endif
    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ENABLED) ==
        RIP6_IFACE_ENABLED)
    {
        /* Check if the interface is already enabled */
        return RIP6_SUCCESS;
    }

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Interface is not created at the lower layer. */
        return RIP6_FAILURE;
    }

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        /* Interface is not attached with any instance. */
        return RIP6_FAILURE;
    }

    RIP6_GET_IF_ENTRY (u4Index)->u1Status |= RIP6_IFACE_ENABLED;

    /* Attach ALL-RIP6-ROUTER-MULTI-CAST address to this interface */
    MEMSET (&Rip6MultiAddr, 0, sizeof (tIp6Addr));
    Rip6MultiAddr.u1_addr[0] = 0xFF;
    Rip6MultiAddr.u1_addr[1] = 0x02;
    Rip6MultiAddr.u1_addr[15] = 0x09;

#ifdef BSDCOMP_SLI_WANTED

    MEMCPY (&(ip6Mreq.ipv6mr_multiaddr), &Rip6MultiAddr, IPVX_IPV6_ADDR_LEN);
#ifdef LNXIP6_WANTED
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    if (NetIpv6GetIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                      "No info present for the index\n");
        return RIP6_FAILURE;
    }
    ip6Mreq.ipv6mr_interface = NetIpv6IfInfo.u4IpPort;
#else
    ip6Mreq.ipv6mr_interface = u4Index;
#endif
    if (setsockopt
        (gi4Rip6SockId, IPPROTO_IPV6, IPV6_JOIN_GROUP,
         &(ip6Mreq), sizeof (struct ipv6_mreq)) < 0)
    {
        return RIP6_FAILURE;
    }
#endif
#ifdef IP6_WANTED
    NetIpv6McastJoin (u4Index, &Rip6MultiAddr);
#endif

    if (IS_ADDR_UNSPECIFIED (RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr))
    {
        /* Link-Local Address not available for this interface. No need
         * to proceed further. */
        return RIP6_SUCCESS;
    }

    /* Get Addresses configured for the interface and 
     * add it to the RipRBTree */
    MEMSET (&Ipv6AddrInfo, 0, sizeof (tNetIpv6AddrInfo));
    SET_ADDR_UNSPECIFIED (null_addr);
    if (gRip6Redistribution.u1DefMetric != 0)
    {
        u1Metric = gRip6Redistribution.u1DefMetric;
    }
    else
    {
        u1Metric = RIP6_INITIALIZE_ONE;
    }
    i4Status = NetIpv6GetFirstIfAddr (u4Index, &Ipv6AddrInfo);

    while (i4Status == NETIPV6_SUCCESS)
    {
        Ip6CopyAddrBits (&dstAddr, &Ipv6AddrInfo.Ip6Addr,
                         Ipv6AddrInfo.u4PrefixLength);
        pRtEntry = Rip6RtEntryFill (&dstAddr,
                                    (UINT1) Ipv6AddrInfo.u4PrefixLength,
                                    &null_addr, u1Metric, DIRECT, RIP6_LOCAL,
                                    RIP6_INITIALIZE_ONE, u4Index,
                                    RIP6_INITIALIZE_ZERO);
        if (pRtEntry == NULL)
        {
            break;
        }

        Rip6AddNodeToRBTree (gRip6RBTree, pRtEntry);

        MEMCPY (&Ipv6PrevAddrInfo, &Ipv6AddrInfo, sizeof (tNetIpv6AddrInfo));
        i4Status = NetIpv6GetNextIfAddr (u4Index, &Ipv6PrevAddrInfo,
                                         &Ipv6AddrInfo);
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_UP) == RIP6_IFACE_UP)
    {
        RIP6_TRC_ARG2 (RIP6_MOD_TRC,
                       RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6 : Starting Update timer on If = %d Duration = %d \n",
                       u4Index,
                       garip6InstanceDatabase[u4InstanceId]->
                       arip6Profile[RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                            u4Index)->
                                    u2ProfIndex]->u4PeriodicUpdTime);

        (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs)++;

        if (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs ==
            RIP6_INITIALIZE_ONE)
        {
            SET_ADDR_UNSPECIFIED (null_addr);
        }

        Rip6CheckExtRoutes (u4Index, RIP6_ENABLED);
        Rip6SendRequest (u4Index);
        /* Send the regular update and then start the periodic timer */
        if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes !=
            RIP6_INITIALIZE_ZERO)
        {
            SET_ALL_RIP_ROUTERS (dstAddr);
            /* Send the regular update */
            /* Initiatilise the Throttle Prefix/Prefix Len */
            MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->PeriodicThrotInitPrefix, 0,
                    sizeof (tIp6Addr));
            RIP6_GET_IF_ENTRY (u4Index)->u1ThrotInitPrefixLen = 0;
            RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag =
                RIP6_THROT_NOT_IN_PROGRESS;
            Rip6SendResponse (RIP6_PERIODIC_UPDATE, RIP6_PORT, u4Index, u4Index,
                              &dstAddr);
        }

        if (RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag ==
            RIP6_THROT_NOT_IN_PROGRESS)
        {
            /* Start the update timer if the update-interval is not zero */
            Rip6IfTimerStart (RIP6_UPDATE_TIMER_ID, u4Index);
        }
        else
        {
            /* Periodic Update is not completed. Wait for 1 sec and then schedule
             * to process remaining routes */
            Rip6TmrStart (RIP6_UPDATE_TIMER_ID, gRip6IntfTimerListId,
                          &(RIP6_UPDATE_TIMER_NODE (u4Index)),
                          RIP6_THROT_INIT_DELAY);
        }
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Disables RIP6 on an interface
 * INPUTS      : The logical interface (u4Index) 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/

INT4
Rip6IfDisable (UINT4 u4Index)
{
    tIp6Addr            Rip6MultiAddr;
    UINT4               u4InstanceId;
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq    ip6Mreq;
#ifdef LNXIP6_WANTED
    tNetIpv6IfInfo      NetIpv6IfInfo;
#endif
#endif

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Interface is not created at the lower layer. */
        RIP6_GET_IF_ENTRY (u4Index)->u1Status &= ~RIP6_IFACE_ENABLED;
        return RIP6_FAILURE;
    }

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        /* This interface is not associated with any instance. */
        RIP6_GET_IF_ENTRY (u4Index)->u1Status &= ~RIP6_IFACE_ENABLED;
        return RIP6_FAILURE;
    }

    /* Detach ALL-RIP6-ROUTER-MULTI-CAST address from this interface */
    MEMSET (&Rip6MultiAddr, 0, sizeof (tIp6Addr));
    Rip6MultiAddr.u1_addr[0] = 0xFF;
    Rip6MultiAddr.u1_addr[1] = 0x02;
    Rip6MultiAddr.u1_addr[15] = 0x09;

#ifdef BSDCOMP_SLI_WANTED
    MEMCPY (&(ip6Mreq.ipv6mr_multiaddr), &Rip6MultiAddr, IPVX_IPV6_ADDR_LEN);
#ifdef LNXIP6_WANTED
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    if (NetIpv6GetIfInfo (u4Index, &NetIpv6IfInfo) == NETIPV6_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                      "No info present for the index\n");
        return RIP6_FAILURE;
    }
    ip6Mreq.ipv6mr_interface = NetIpv6IfInfo.u4IpPort;
#else
    ip6Mreq.ipv6mr_interface = u4Index;
#endif
    if (setsockopt (gi4Rip6SockId, IPPROTO_IPV6, IPV6_LEAVE_GROUP,
                    &ip6Mreq, sizeof (struct ipv6_mreq)) < 0)
    {
        return RIP6_FAILURE;
    }
#endif
#ifdef IP6_WANTED
    NetIpv6McastLeave (u4Index, &Rip6MultiAddr);
#endif

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED)
    {
        /* RIP6 is already disabled over this interface. */
        return RIP6_SUCCESS;
    }

    /* Means interface is down */
    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_UP) != RIP6_IFACE_UP)
    {
        /* Interface Status is already DOWN. So just clearing  the RIP Enable
         * flag is sufficient. */
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &=
            ~RIP6_IFACE_ENABLED;
        return RIP6_SUCCESS;
    }

    if (IS_ADDR_UNSPECIFIED (RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr))
    {
        /* Link-Local Address not available for this interface. No need
         * to proceed further. */
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &=
            ~RIP6_IFACE_ENABLED;
        return RIP6_SUCCESS;
    }

    if (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs != 0)
    {
        --(garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs);
    }

    /* Initiatilise the Throttle Prefix/Prefix Len */
    MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->PeriodicThrotInitPrefix, 0,
            sizeof (tIp6Addr));
    RIP6_GET_IF_ENTRY (u4Index)->u1ThrotInitPrefixLen = 0;
    RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag = RIP6_THROT_NOT_IN_PROGRESS;

    Rip6CheckExtRoutes (u4Index, RIP6_DISABLED);
#ifndef RIP6_SINGLE_INSTANCE
    Rip6SendRtOnIf (u4Index, RIP6_SEND_ALL);
#else
    Rip6SendRtOnIf (u4Index, RIP6_SEND_DYNAMIC);
#endif

    /* Purge the Dynamic routes learnt through this interface */
    Rip6PurgeRtLearnt (RIP6_PURGE_DYNAMIC, u4Index);

    RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                   RIP6_NAME, "RIP6 : Stopping timers on If = %d\n", u4Index);

    /* Stop periodic and triggered timers */
    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
        periodicTimer.u1Id != RIP6_FALSE)
    {
        Rip6TmrStop (RIP6_UPDATE_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_UPDATE_TIMER_NODE (u4Index)));
    }

    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
        trigDelayTimer.u1Id != RIP6_FALSE)
    {
        Rip6TmrStop (RIP6_TRIG_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_DELAY_TIMER_NODE (u4Index)));
    }

    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u4NextRegularUpdate
        = RIP6_INITIALIZE_ZERO;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag &=
        ~RIP6_TRIGGERED_DELAY_NOT_RUNNING;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag |=
        RIP6_TRIGGERED_DELAY_RUNNING;

    /* Status set to DISABLED */
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &=
        ~RIP6_IFACE_ENABLED;
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Relects that interface is made UP
 * INPUTS      : The logical interface (u4Index) 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/

INT4
Rip6IfUp (UINT4 u4Index)
{
    tIp6Addr            dstAddr;
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_UP) ==
        RIP6_IFACE_UP)
    {
        /* Check if the interface is already UP */
        return RIP6_SUCCESS;
    }

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Create the Interface and then process the interface UP event. */
        if (Rip6IfCreate (u4Index) == RIP6_FAILURE)
        {
            /* Unable to create the interface. */
            return RIP6_FAILURE;
        }
    }

    RIP6_GET_IF_ENTRY (u4Index)->u1Status |= RIP6_IFACE_UP;

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        /* Interface not yet associated with any instance. */
        return RIP6_SUCCESS;
    }

    if (IS_ADDR_UNSPECIFIED (RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr))
    {
        /* Link-Local Address not available for this interface. No need
         * to proceed further. */
        return RIP6_SUCCESS;
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED)
    {

        RIP6_TRC_ARG2 (RIP6_MOD_TRC,
                       RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6 : Starting Update timer on If = %d Duration = %d \n",
                       u4Index,
                       garip6InstanceDatabase[u4InstanceId]->
                       arip6Profile[RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                            u4Index)->
                                    u2ProfIndex]->u4PeriodicUpdTime);
        (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs)++;

        Rip6CheckExtRoutes (u4Index, RIP6_ENABLED);
        Rip6SendRequest (u4Index);

        SET_ALL_RIP_ROUTERS (dstAddr);
        /* Send the regular update and then start the periodic timer */
        if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes !=
            RIP6_INITIALIZE_ZERO)
        {
            /* Send the regular update */
            /* Initiatilise the Throttle Prefix/Prefix Len */
            MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->PeriodicThrotInitPrefix, 0,
                    sizeof (tIp6Addr));
            RIP6_GET_IF_ENTRY (u4Index)->u1ThrotInitPrefixLen = 0;
            RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag =
                RIP6_THROT_NOT_IN_PROGRESS;
            Rip6SendResponse (RIP6_PERIODIC_UPDATE, RIP6_PORT, u4Index, u4Index,
                              &dstAddr);
        }

        if (RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag ==
            RIP6_THROT_NOT_IN_PROGRESS)
        {
            /* Start the update timer if the update-interval is not zero */
            Rip6IfTimerStart (RIP6_UPDATE_TIMER_ID, u4Index);
        }
        else
        {
            /* Periodic Update is not completed. Wait for 1 sec and then schedule
             * to process remaining routes */
            Rip6TmrStart (RIP6_UPDATE_TIMER_ID, gRip6IntfTimerListId,
                          &(RIP6_UPDATE_TIMER_NODE (u4Index)),
                          RIP6_THROT_INIT_DELAY);
        }
    }
    return RIP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : Relects that interface is made DOWN
 * INPUTS      : The logical interface (u4Index) 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/

INT4
Rip6IfDown (UINT4 u4Index)
{
    UINT4               u4InstanceId;

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Interface is created at the lower layer and the status is DOWN. */
        if (Rip6IfCreate (u4Index) == RIP6_FAILURE)
        {
            /* Unable to create the interface. */
            return RIP6_FAILURE;
        }
        RIP6_GET_IF_ENTRY (u4Index)->u1Status &= ~RIP6_IFACE_UP;
        return RIP6_SUCCESS;
    }

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        /* Interface is not associated with any instance */
        RIP6_GET_IF_ENTRY (u4Index)->u1Status &= ~RIP6_IFACE_UP;
        return RIP6_FAILURE;
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status
         & RIP6_IFACE_UP) != RIP6_IFACE_UP)
    {
        /* Check if the interface is already DOWN */
        return RIP6_SUCCESS;
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED)
    {
        /* RIP6 is not enabled over this interface. No need to proceed any
         * further. */
        RIP6_GET_IF_ENTRY (u4Index)->u1Status &= ~RIP6_IFACE_UP;
        return RIP6_SUCCESS;
    }

    if (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs != 0)
    {
        --(garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs);
    }

    /* Process all the routes learnt over this interface */
    /* Initiatilise the Throttle Prefix/Prefix Len */
    MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->PeriodicThrotInitPrefix, 0,
            sizeof (tIp6Addr));
    RIP6_GET_IF_ENTRY (u4Index)->u1ThrotInitPrefixLen = 0;
    RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag = RIP6_THROT_NOT_IN_PROGRESS;

    Rip6CheckExtRoutes (u4Index, RIP6_DISABLED);
#ifndef RIP6_SINGLE_INSTANCE
    Rip6SendRtOnIf (u4Index, RIP6_SEND_ALL);
#else
    Rip6SendRtOnIf (u4Index, RIP6_SEND_DYNAMIC);
#endif

    /* Purge the Dynamic routes learnt through this interface */
    Rip6PurgeRtLearnt (RIP6_PURGE_DYNAMIC, u4Index);

    /* Set interface down */
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &= ~RIP6_IFACE_UP;
    RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                   RIP6_NAME, "RIP6 : Stopping timers on If = %d\n", u4Index);

    /* Stop periodic and triggered timers */
    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
        periodicTimer.u1Id != RIP6_FALSE)
    {
        Rip6TmrStop (RIP6_UPDATE_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_UPDATE_TIMER_NODE (u4Index)));
    }

    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
        trigDelayTimer.u1Id != RIP6_FALSE)
    {
        Rip6TmrStop (RIP6_TRIG_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_DELAY_TIMER_NODE (u4Index)));
    }

    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u4NextRegularUpdate
        = RIP6_INITIALIZE_ZERO;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag &=
        ~RIP6_TRIGGERED_DELAY_NOT_RUNNING;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag |=
        RIP6_TRIGGERED_DELAY_RUNNING;
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Relects that Link-Local Address is successfully added over the
 *               interface.
 * INPUTS      : The logical interface (u4Index)
 *               The Link-Local Address (pLLAddr)
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS/RIP6_FAILURE
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/
INT4
Rip6IfLLAddrEnable (UINT4 u4Index, tIp6Addr * pLLAddr)
{
    tIp6Addr            dstAddr;
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Create the Interface and then process the address create event. */
        if (Rip6IfCreate (u4Index) == RIP6_FAILURE)
        {
            /* Unable to create the interface. */
            return RIP6_FAILURE;
        }
        MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0,
                sizeof (tIp6Addr));
        Ip6AddrCopy (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, pLLAddr);
        return RIP6_SUCCESS;
    }

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        /* Interface not yet associated with any instance. */
        MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0,
                sizeof (tIp6Addr));
        Ip6AddrCopy (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, pLLAddr);
        return RIP6_SUCCESS;
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_UP) != RIP6_IFACE_UP)
    {
        /* Lower Layer interface status is DOWN. */
        MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0,
                sizeof (tIp6Addr));
        Ip6AddrCopy (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, pLLAddr);
        return RIP6_SUCCESS;
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED)
    {
        /* RIP6 is not enabled on the interface. */
        MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0,
                sizeof (tIp6Addr));
        Ip6AddrCopy (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, pLLAddr);
        return RIP6_SUCCESS;
    }

    SET_ALL_RIP_ROUTERS (dstAddr);
    /* Check whether the old Link-Local Address is still present or not.
     * If present, then first send Response message from that address with
     * metric as 16. */
    if ((IS_ADDR_UNSPECIFIED (RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr) == 0)
        &&
        (MEMCMP
         (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, pLLAddr,
          sizeof (tIp6Addr)) != 0))
    {
        /* Old Link-Local Address is present. */
        Rip6SendResponse ((UINT1) (RIP6_FINAL_UPDATE | RIP6_SEND_ALL),
                          RIP6_PORT, u4Index, u4Index, &dstAddr);
    }
    else if (IS_ADDR_UNSPECIFIED (RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr))
    {
        /* Going to configure the address for first time. */
        (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs)++;
    }

    /* Now update the interface with the new link-local Address */
    MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0, sizeof (tIp6Addr));
    Ip6AddrCopy (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, pLLAddr);

    Rip6SendRequest (u4Index);

    /* Send the regular update and then start the periodic timer */
    if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes !=
        RIP6_INITIALIZE_ZERO)
    {
        /* Send the regular update */
        /* Initiatilise the Throttle Prefix/Prefix Len */
        MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->PeriodicThrotInitPrefix, 0,
                sizeof (tIp6Addr));
        RIP6_GET_IF_ENTRY (u4Index)->u1ThrotInitPrefixLen = 0;
        RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag = RIP6_THROT_NOT_IN_PROGRESS;
        Rip6SendResponse (RIP6_PERIODIC_UPDATE,
                          RIP6_PORT, u4Index, u4Index, &dstAddr);
    }

    if (RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag == RIP6_THROT_NOT_IN_PROGRESS)
    {
        /* Start the update timer if the update-interval is not zero */
        Rip6IfTimerStart (RIP6_UPDATE_TIMER_ID, u4Index);
    }
    else
    {
        /* Periodic Update is not completed. Wait for 1 sec and then schedule
         * to process remaining routes */
        Rip6TmrStart (RIP6_UPDATE_TIMER_ID, gRip6IntfTimerListId,
                      &(RIP6_UPDATE_TIMER_NODE (u4Index)),
                      RIP6_THROT_INIT_DELAY);
    }
    return RIP6_SUCCESS;

}

/******************************************************************************
 * DESCRIPTION : Relects that Link-Local Address is removed from the interface.
 * INPUTS      : The logical interface (u4Index)
 *               The Link-Local Address (pLLAddr)
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS/RIP6_FAILURE
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/
INT4
Rip6IfLLAddrDisable (UINT4 u4Index, tIp6Addr * pLLAddr)
{
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Interface is not yet created. */
        return RIP6_FAILURE;
    }

    if (IS_ADDR_UNSPECIFIED (RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr))
    {
        /* Link-Local Address in not set for this interface. No need for
         * any handling. */
        return RIP6_SUCCESS;
    }

    if (MEMCMP
        (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, pLLAddr,
         sizeof (tIp6Addr)) != 0)
    {
        /* Conflicting Link-Local Address. */
        return RIP6_FAILURE;
    }

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        /* Interface not yet associated with any instance. */
        MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0,
                sizeof (tIp6Addr));
        return RIP6_SUCCESS;
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_UP) != RIP6_IFACE_UP)
    {
        /* Lower Layer interface status is DOWN. */
        MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0,
                sizeof (tIp6Addr));
        return RIP6_SUCCESS;
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
         RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED)
    {
        /* RIP6 is not enabled on the interface. */
        MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0,
                sizeof (tIp6Addr));
        return RIP6_SUCCESS;
    }

    if (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs != 0)
    {
        --(garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs);
    }

    /* Need to send all routes with metric 16 over this interface */
    /* Initiatilise the Throttle Prefix/Prefix Len */
    MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->PeriodicThrotInitPrefix, 0,
            sizeof (tIp6Addr));
    RIP6_GET_IF_ENTRY (u4Index)->u1ThrotInitPrefixLen = 0;
    RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag = RIP6_THROT_NOT_IN_PROGRESS;

    RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                   RIP6_NAME, "RIP6 : Stopping timers on If = %d\n", u4Index);

    /* Stop periodic and triggered timers */
    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
        periodicTimer.u1Id != RIP6_FALSE)
    {
        Rip6TmrStop (RIP6_UPDATE_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_UPDATE_TIMER_NODE (u4Index)));
    }

    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
        trigDelayTimer.u1Id != RIP6_FALSE)
    {
        Rip6TmrStop (RIP6_TRIG_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_DELAY_TIMER_NODE (u4Index)));
    }

    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u4NextRegularUpdate
        = RIP6_INITIALIZE_ZERO;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag &=
        ~RIP6_TRIGGERED_DELAY_NOT_RUNNING;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag |=
        RIP6_TRIGGERED_DELAY_RUNNING;

    /* Also Clear the LinkLocal Address. */
    MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->LinkLocalAddr, 0, sizeof (tIp6Addr));
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Relects that interface is CREATED
 * INPUTS      : The logical interface (u4Index) 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/

INT4
Rip6IfCreate (UINT4 u4Index)
{

    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    if (u4Index > u4MaxIfs)
    {
        return RIP6_FAILURE;
    }

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Initialize the fields of garip6If */
        MEMSET (RIP6_GET_IF_ENTRY (u4Index), RIP6_INITIALIZE_ZERO,
                sizeof (tRip6If));
        RIP6_GET_IF_ENTRY (u4Index)->u1Cost = RIP6_DFLT_COST;
        RIP6_GET_IF_ENTRY (u4Index)->u4Index = u4Index;
        RIP6_GET_IF_ENTRY (u4Index)->u1Status |= RIP6_IFACE_ALLOCATED;
        return RIP6_SUCCESS;
    }
    else
    {
        /* Interface is already created. */
        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                       "RIP6 : Interface %d already Created \n", u4Index);
        return RIP6_FAILURE;
    }
}

/******************************************************************************
 * DESCRIPTION : Relects that interface is DELETED
 * INPUTS      : The logical interface (u4Index) 
 * OUTPUTS     : None
 * RETURNS     : None
 *
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/

INT4
Rip6IfDelete (UINT4 u4Index)
{
    UINT4               u4InstanceId;

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Interface was not created earlier. */
        return RIP6_FAILURE;
    }

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        /* Interface is not yet associated with any instance. */
        RIP6_GET_IF_ENTRY (u4Index)->u1Status &=
            ~(RIP6_IFACE_ALLOCATED | RIP6_IFACE_UP);
        return RIP6_FAILURE;
    }

    if (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs != 0)
    {
        --(garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs);
    }

    if (((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
          RIP6_IFACE_UP) == RIP6_IFACE_UP) &&
        ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &
          RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED))
    {
        /* RIP6 Enabled over this interface */
        Rip6SendRtOnIf (u4Index, RIP6_SEND_ALL);

        Rip6CheckExtRoutes (u4Index, RIP6_DISABLED);

        /* Purge out all routes learnt over this interface */
        Rip6PurgeRtLearnt ((UINT1) (~RIP6_PURGE_DYNAMIC), u4Index);

        /* Disable RIP6 over this interface. */
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &=
            ~RIP6_IFACE_ENABLED;
    }

    RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                   RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                   "RIP6 : Stopping timers on If = %d\n", u4Index);

    /* Stop periodic and triggered timers */
    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
        periodicTimer.u1Id != RIP6_FALSE)
    {
        Rip6TmrStop (RIP6_UPDATE_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_UPDATE_TIMER_NODE (u4Index)));
    }

    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
        trigDelayTimer.u1Id != RIP6_FALSE)
    {
        Rip6TmrStop (RIP6_TRIG_TIMER_ID,
                     gRip6IntfTimerListId, &(RIP6_DELAY_TIMER_NODE (u4Index)));
    }

    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u4NextRegularUpdate
        = RIP6_INITIALIZE_ZERO;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag &=
        ~RIP6_TRIGGERED_DELAY_NOT_RUNNING;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag |=
        RIP6_TRIGGERED_DELAY_RUNNING;

    /* Means made status FREE */
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1Status &=
        ~(RIP6_IFACE_ALLOCATED | RIP6_IFACE_UP | RIP6_IFACE_ENABLED);

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : To Set the Router forwarding status and accordingly Change the
 *               Interface status Up or Down.
 *      
 * INPUTS      : The logical interface (u4Index)
 *               The Link-Local Address whose status have changed (pLLAddr)
 *               The Status of the Address (u1AddrStatus)
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS/RIP6_FAILURE
 * NOTES       : Called upon getting a control message from IP6 module for
 *               processing change in Link Local Address of the interface.
 ******************************************************************************/
INT4
Rip6ProcLinkLocalAddrChg (tIp6Addr * pLLAddr, UINT4 u4Index, UINT1 u1AddrStatus)
{
    INT4                i4RetVal = RIP6_SUCCESS;

    if (u1AddrStatus == RIP6_ADDR_ADD)
    {
        i4RetVal = Rip6IfLLAddrEnable (u4Index, pLLAddr);
    }
    else
    {
        i4RetVal = Rip6IfLLAddrDisable (u4Index, pLLAddr);
    }
    return i4RetVal;
}

/* 
 * The timer handler routines 
 ****************************/

/******************************************************************************
 * DESCRIPTION : Called upon getting a Timeout event, to determine the instance 
          to which the timer is associated and pass the control to the 
         respective function handler. 
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Rip6WrapperTimer ()
{

    tRip6RtEntry       *pRip6RtEntry = NULL;
    tRip6RtEntry       *pNextRtEntry = NULL;
    tRip6RtEntry       *pNextRip6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    tTmrAppTimer       *pListHead = NULL, *pTimer = NULL;
    UINT4               u4CurrentTime;
    UINT4               u4InstId;
    UINT4               u4RemainingTime = 0;
    INT4                i4RetVal = RIP6_FAILURE;
    UINT1               u1Id;

    TmrGetExpiredTimers (gRip6TimerListId, &pListHead);

    OsixGetSysTime (&u4CurrentTime);

    while (pListHead != NULL)
    {
        pTimer = pListHead;
        pListHead = TmrGetNextExpiredTimer (gRip6TimerListId);

        u4InstId = ((tRip6RtGlobal *) pTimer)->u4InstanceId;

        u1Id = ((tRip6RtTimer *) pTimer)->u1Id;
        switch (u1Id)
        {
            case RIP6_Regular_TIMER_ID:
                /* Process all the routes associated with this instance. If the
                 * route has been timedout, then call the TimeOut handler for
                 * that route. */
                Rip6TrieGetFirstEntry (&pNextRip6RtEntry, &pRibNode, u4InstId);

                while ((pRip6RtEntry = pNextRip6RtEntry) != NULL)
                {
                    /* Get the Next Route entry. */
                    Rip6TrieGetNextEntry (&pNextRip6RtEntry, pRibNode,
                                          &pNextRibNode, u4InstId);

                    while (pRip6RtEntry)
                    {
                        pNextRtEntry = pRip6RtEntry->pNext;

                        /* Process the current route. */
                        if (pRip6RtEntry->i1Proto == RIPNG)
                        {
                            i4RetVal =
                                TmrRip6MIRemainingTime (pRip6RtEntry,
                                                        &u4RemainingTime);
                            if (i4RetVal == RIP6_FAILURE)
                            {
                                Rip6RtTimeoutHandler (pRip6RtEntry->
                                                      rtEntryTimer.u1Id,
                                                      pRip6RtEntry);
                            }
                        }
                        pRip6RtEntry = pNextRtEntry;
                        pNextRtEntry = NULL;
                    }

                    /* Update Next Node */
                    pRibNode = pNextRibNode;
                    pNextRibNode = NULL;
                }

                Rip6TmrStop (RIP6_Regular_TIMER_ID, gRip6TimerListId,
                             &(garip6InstanceDatabase[u4InstId]->Rip6RtGlobal.
                               timer));

                Rip6TmrStart (RIP6_Regular_TIMER_ID, gRip6TimerListId,
                              &(garip6InstanceDatabase[u4InstId]->Rip6RtGlobal.
                                timer), RIP6_INITIALIZE_TWENTYONE);

                break;

            case RIP6_HOLD_ON_TIMER_ID:
                RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                              "RIP6 : Rip6WrapperTimer, resetting "
                              "hold on timer \n");
                gu4HoldOnFlag = OSIX_FALSE;
                break;

            default:
                break;
        }

    }

}

/******************************************************************************
 * DESCRIPTION : Called upon getting a Timeout event 
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Rip6IntfWrapperTimer ()
{

    UINT1               u1Id;
    tTmrAppTimer       *pListHead = NULL, *pTimer = NULL;
    tRip6If            *pRip6if = NULL;

    TmrGetExpiredTimers (gRip6IntfTimerListId, &pListHead);

    while (pListHead != NULL)
    {
        pTimer = pListHead;
        pListHead = TmrGetNextExpiredTimer (gRip6IntfTimerListId);

        u1Id = ((tRip6RtTimer *) pTimer)->u1Id;
        RIP6_TRC_ARG2 (RIP6_MOD_TRC,
                       RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "The timer ptr = %p id = %d \n", pTimer, u1Id);

        switch (u1Id)
        {
            case RIP6_UPDATE_TIMER_ID:

                pRip6if = RIP6IF_FROM_TIMER (pTimer, periodicTimer);
                gInstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRip6if->u4Index)->
                    u4InstanceId;
                if (pRip6if->u1ThrotFlag == RIP6_THROT_IN_PROGRESS)
                {
                    /* Process the pending Periodic Response event. */
                    Rip6PendPeriodicUpdateHandler ((tTmrAppTimer *) pTimer);
                }
                else
                {
                    Rip6PeriodicTimeoutHandler ((tTmrAppTimer *) pTimer);
                }
                break;

            case RIP6_TRIG_TIMER_ID:
                pRip6if = RIP6IF_FROM_TIMER (pTimer, trigDelayTimer);
                gInstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRip6if->u4Index)->
                    u4InstanceId;
                Rip6TrigDelayHandler ((tTmrAppTimer *) pTimer);
                break;

            default:
                break;
        }
    }
}

/******************************************************************************
 * DESCRIPTION : Handling the generation of periodic updates
 * INPUTS      : The timer node (pTimer)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

INT4
Rip6PeriodicTimeoutHandler (pTimer)
     tTmrAppTimer       *pTimer;
{
    tRip6If            *pRip6if = NULL;
    tIp6Addr            dstAddr;
    UINT4               u4InstanceId;

    pRip6if = RIP6IF_FROM_TIMER (pTimer, periodicTimer);
    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRip6if->u4Index)->u4InstanceId;

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        return RIP6_FAILURE;
    }

    RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                   RIP6_NAME,
                   "RIP6 : Got a update timer expiry on If = %d\n",
                   pRip6if->u4Index);

    SET_ALL_RIP_ROUTERS (dstAddr);
    if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes !=
        RIP6_INITIALIZE_ZERO)
    {
        /* Send the regular update */
        /* Initiatilise the Throttle Prefix/Prefix Len */
        MEMSET (&pRip6if->PeriodicThrotInitPrefix, 0, sizeof (tIp6Addr));
        pRip6if->u1ThrotInitPrefixLen = 0;
        pRip6if->u1ThrotFlag = RIP6_THROT_NOT_IN_PROGRESS;
        Rip6SendResponse (RIP6_PERIODIC_UPDATE,
                          RIP6_PORT, pRip6if->u4Index,
                          pRip6if->u4Index, &dstAddr);
    }

    if (pRip6if->u1ThrotFlag == RIP6_THROT_NOT_IN_PROGRESS)
    {
        /* Start the update timer if the update-interval is not zero */
        Rip6IfTimerStart (RIP6_UPDATE_TIMER_ID, pRip6if->u4Index);
    }
    else
    {
        /* Periodic Update is not completed. Wait for 1 sec and then schedule
         * to process remaining routes */
        Rip6TmrStart (RIP6_UPDATE_TIMER_ID, gRip6IntfTimerListId,
                      &(RIP6_UPDATE_TIMER_NODE (pRip6if->u4Index)),
                      RIP6_THROT_INIT_DELAY);
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handling the generation of pending periodic updates
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS/RIP6_FAILURE
 * NOTES       :
 ******************************************************************************/
INT4
Rip6PendPeriodicUpdateHandler (tTmrAppTimer * pTimer)
{
    tRip6If            *pRip6if = NULL;
    tIp6Addr            dstAddr;
    UINT4               u4InstanceId;

    pRip6if = RIP6IF_FROM_TIMER (pTimer, periodicTimer);
    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRip6if->u4Index)->u4InstanceId;

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        return RIP6_FAILURE;
    }

    RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_MGMT_TRC,
                   RIP6_NAME,
                   "RIP6 : Got a Pend-Periodic update timer expiry on If = %d\n",
                   pRip6if->u4Index);

    SET_ALL_RIP_ROUTERS (dstAddr);
    if (pRip6if->u1ThrotFlag == RIP6_THROT_IN_PROGRESS)
    {
        Rip6SendResponse (RIP6_PERIODIC_UPDATE, RIP6_PORT, pRip6if->u4Index,
                          pRip6if->u4Index, &dstAddr);
    }

    if (pRip6if->u1ThrotFlag == RIP6_THROT_NOT_IN_PROGRESS)
    {
        /* Start the update timer if the update-interval is not zero */
        Rip6IfTimerStart (RIP6_UPDATE_TIMER_ID, pRip6if->u4Index);
    }
    else
    {
        /* Periodic Update is not completed. Wait for 1 sec and then schedule
         * to process remaining routes */
        Rip6TmrStart (RIP6_UPDATE_TIMER_ID, gRip6IntfTimerListId,
                      &(RIP6_UPDATE_TIMER_NODE (pRip6if->u4Index)),
                      RIP6_THROT_INIT_DELAY);
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handling the triggered delay timeouts 
 * INPUTS      : The timer node (pTimer)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

INT4
Rip6TrigDelayHandler (pTimer)
     tTmrAppTimer       *pTimer;
{
    INT2                i2i;
    tRip6If            *pRip6if = NULL;
    tIp6Addr            dstAddr;
    tRip6ScanParam      ScanParams;
    UINT4               u4InstanceId;
    INT4                i4MaxIfs;

    pRip6if = RIP6IF_FROM_TIMER (pTimer, trigDelayTimer);
    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRip6if->u4Index)->u4InstanceId;

    RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                   RIP6_MGMT_TRC |
                   RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                   "RIP6 : Got trig delay expiry on index = %d \n",
                   pRip6if->u4Index);
    pRip6if->u1TrigFlag &= ~RIP6_TRIGGERED_DELAY_RUNNING;
    pRip6if->u1TrigFlag |= RIP6_TRIGGERED_DELAY_NOT_RUNNING;

    if ((pRip6if->u1TrigFlag & RIP6_TRIGGERED_UPDATE_NEEDED) ==
        RIP6_TRIGGERED_UPDATE_NEEDED)
    {
        if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes == 0)
        {
            /*
             * No routes there in the routing table to be put.So
             * the Trig update not sent.
             */
            return RIP6_SUCCESS;
        }

        /* Send Triggered update on this interface */
        SET_ALL_RIP_ROUTERS (dstAddr);

        Rip6SendResponse (RIP6_TRIG_UPDATE, RIP6_PORT,
                          pRip6if->u4Index, pRip6if->u4Index, &dstAddr);

        i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                             FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                             u4PreAllocatedUnits);
        /* Check if any other triggered delay timer is running */
        for (i2i = RIP6_INITIALIZE_ONE; i2i <= i4MaxIfs; i2i++)
        {
            if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, i2i) == NULL)
            {
                continue;
            }
            if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, i2i)->u1TrigFlag
                 & RIP6_TRIGGERED_DELAY_RUNNING) ==
                RIP6_TRIGGERED_DELAY_RUNNING)
            {
                return SUCCESS;
            }
        }

        /* Reset all route change flags as no trig delay timer is running */
        ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_TRIG_CLEAR;
        ScanParams.u4InstanceId = u4InstanceId;
        Rip6TrieScan (&ScanParams);
        /*
         * Start the triggered delay timer only if the interface
         * is UP and ripng is ENABLED on it
         */
        if (((RIP6_GET_INST_IF_ENTRY (u4InstanceId, pRip6if->u4Index)->
              u1Status & RIP6_IFACE_ALLOCATED) == RIP6_IFACE_ALLOCATED) &&
            ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, pRip6if->u4Index)->
              u1Status & RIP6_IFACE_UP) == RIP6_IFACE_UP) &&
            ((RIP6_GET_INST_IF_MAP_ENTRY (pRip6if->u4Index)->u4InsIfStatus &
              RIP6_IFACE_ATTACHED) == RIP6_IFACE_ATTACHED))
        {

            /*
             * Triggered update was sent and the interface was
             * RIPng enabled and UP,so delay timer started
             */
            Rip6IfTimerStart (RIP6_TRIG_TIMER_ID, pRip6if->u4Index);
        }

        /*
         * Set the flag telling that the triggered delay timer running
         * for this logical interface
         */
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, pRip6if->u4Index)->
            u1TrigFlag &= ~RIP6_TRIGGERED_DELAY_NOT_RUNNING;

        RIP6_GET_INST_IF_ENTRY (u4InstanceId, pRip6if->u4Index)->
            u1TrigFlag |= RIP6_TRIGGERED_DELAY_RUNNING;

    }
    return SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Handling the route age and garbage collect timeouts
 * INPUTS      : The identifier (u1Id) telling whether route has aged out
 *               or has garbage collect has expired and the routing table
 *               entry (pRtEntry)
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/

VOID
Rip6RtTimeoutHandler (UINT1 u1Id, tRip6RtEntry * pRtEntry)
{
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRtEntry->u4Index)->u4InstanceId;
    switch (u1Id)
    {
        case RIP6_AGE_OUT_TIMER_ID:

            RIP6_TRC_ARG2 (RIP6_MOD_TRC,
                           RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                           "RIP6 : Route age timer timed out, "
                           "route Dst = %s Pfx = %d\n",
                           Ip6PrintAddr (&pRtEntry->dst),
                           pRtEntry->u1Prefixlen);
            /* This entry has ageout so start the garbage collect on this */
            Rip6RouteAgeout (pRtEntry);

            /* This sending of triggered update could not be done within
             * the route delete routine because when we get a response
             * then we have to do send triggered update everytime we
             * have to route delete but now this processing can be 
             * saved as we will be having a flag which will tell us
             * to do this after we process all the RTE in the response
             */
            Rip6CheckTrigUpdateSend (RIP6_CHECK_ALL, pRtEntry->u4Index,
                                     pRtEntry);
            break;

        case RIP6_GARB_COLLECT_TIMER_ID:

            RIP6_TRC_ARG2 (RIP6_MOD_TRC,
                           RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                           "RIP6 : Garbage collect timer timed out,"
                           " route Dst = %s Pfx = %d\n",
                           Ip6PrintAddr (&pRtEntry->dst),
                           pRtEntry->u1Prefixlen);

            /* intimate change of route in Rip6 Route Table to IP6 */
            Rip6TrieDeleteEntry (pRtEntry, u4InstanceId);
            break;
        default:
            break;
    }
}

/******************************************************************************
 * DESCRIPTION : Handles the starting of the interface timers like 
 *               periodic update and triggered delay timers
 * INPUTS      : The identifier (u1Id) the type of the timer and 
 *               and the interface (u4Index) over which to start the timer
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : For the Periodic Update timer check is made if the 
 *               timer interval is less than or equal to 0.If it is 
 *               then the timer is not started.
 ******************************************************************************/

INT4
Rip6IfTimerStart (UINT1 u1Id, UINT4 u4Index)
{
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    switch (u1Id)
    {
        case RIP6_UPDATE_TIMER_ID:
            if (RIP6_UPDATE_TIMER_INTERVAL (u4Index) <= RIP6_INITIALIZE_ZERO)
            {
                return RIP6_SUCCESS;
            }

            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                periodicTimer.u1Id = RIP6_UPDATE_TIMER_ID;

            Rip6TmrStart (RIP6_UPDATE_TIMER_ID, gRip6IntfTimerListId,
                          &(RIP6_UPDATE_TIMER_NODE (u4Index)),
                          RIP6_UPDATE_TIMER_INTERVAL (u4Index));

            OsixGetSysTime (&(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                              u4NextRegularUpdate));
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                u4NextRegularUpdate += (SYS_NUM_OF_TIME_UNITS_IN_A_SEC
                                        * RIP6_UPDATE_TIMER_INTERVAL (u4Index));
            break;

        case RIP6_TRIG_TIMER_ID:

            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                trigDelayTimer.u1Id = RIP6_TRIG_TIMER_ID;

            Rip6TmrStart (RIP6_TRIG_TIMER_ID, gRip6IntfTimerListId,
                          &(RIP6_DELAY_TIMER_NODE (u4Index)),
                          RIP6_TRIG_DELAY_INTERVAL (u4Index));
            break;

        default:
            break;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Routine to add values to the profile table
 * INPUTS      : u4Index         : The index to the profile table 
 *               u1Horizon       : The horizoning  -- whether NO_HORIZONING,
 *                                 SPLIT_HORIZONING or POISON_REVERSE
 *               u4PeriodicTime  : The periodicity at which the regular updates
 *                                 are to be sent
 *               u4DelayTime     : The triggered update delay interval 
 *               u4RouteAge      : The route age out interval 
 *               u4GcTime        : The garbage collect interval 
 *
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/

VOID
Rip6AddToProfTbl (UINT2 u2Index, UINT1 u1Horizon,
                  UINT4 u4PeriodicTime, UINT4 u4DelayTime, UINT4 u4RouteAge,
                  UINT4 u4GcTime)
{

    garip6InstanceDatabase[gu4InstanceIndex]->arip6Profile[u2Index]->u1Status =
        RIP6_PROFILE_UP;
    garip6InstanceDatabase[gu4InstanceIndex]->arip6Profile[u2Index]->u1Horizon =
        u1Horizon;
    garip6InstanceDatabase[gu4InstanceIndex]->arip6Profile[u2Index]->
        u4PeriodicUpdTime = u4PeriodicTime;
    garip6InstanceDatabase[gu4InstanceIndex]->arip6Profile[u2Index]->
        u4TrigDelayTime = u4DelayTime;
    garip6InstanceDatabase[gu4InstanceIndex]->arip6Profile[u2Index]->
        u4RouteAge = u4RouteAge;
    garip6InstanceDatabase[gu4InstanceIndex]->arip6Profile[u2Index]->
        u4GcTime = u4GcTime;
}

/******************************************************************************
 * DESCRIPTION : Routine to delete the information from the given profile entry
 * INPUTS      : u2Index         : The index to the profile table 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/
INT4
Rip6DelFromProfTbl (UINT2 u2Index)
{
    INT4                i4MaxProfs = 0;

    i4MaxProfs = RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                           FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                           u4PreAllocatedUnits);
    if (u2Index == RIP6_INITIALIZE_ZERO)
    {
        /* Can't delete the 0th profile */
        return RIP6_SUCCESS;
    }
    if (u2Index >= i4MaxProfs)
    {
        return RIP6_FAILURE;
    }
    MEMSET (garip6InstanceDatabase[gu4InstanceIndex]->arip6Profile[u2Index],
            RIP6_INITIALIZE_ZERO, sizeof (tRip6Profile));
    garip6InstanceDatabase[gu4InstanceIndex]->arip6Profile[u2Index]->u1Status =
        RIP6_PROFILE_INVALID;
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Routine to add information to the given rip6if entry
 * INPUTS      : u4Index         : The index to the Interface Table
 *               u1Cost          : The cost of the network 
 *               u2ProfIndex     : The profile table index 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/
VOID
Rip6IfInfoAdd (UINT4 u4Index, UINT1 u1Cost, UINT2 u2ProfIndex)
{

    RIP6_GET_IF_ENTRY (u4Index)->u4Index = u4Index;
    RIP6_GET_IF_ENTRY (u4Index)->u1Cost = u1Cost;
    RIP6_GET_IF_ENTRY (u4Index)->u2ProfIndex = u2ProfIndex;
}

/******************************************************************************
 * DESCRIPTION : Routine to delete information from the given rip6if entry
 * INPUTS      : u4Index         : The index to the rip If table 
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       : Called upon getting a control message from IP6 module
 ******************************************************************************/
VOID
Rip6IfInfoDel (UINT4 u4Index)
{

    MEMSET (RIP6_GET_IF_ENTRY (u4Index), RIP6_INITIALIZE_ZERO,
            sizeof (tRip6If));
    RIP6_GET_IF_ENTRY (u4Index)->u4Index = u4Index;
}

/******************************************************************************
 * DESCRIPTION : RIP6 Protocol Shutdown
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     :None
******************************************************************************/

VOID
Rip6Shutdown ()
{
#ifdef IP6_WANTED
    NetIpv6DeRegisterHigherLayerProtocol (IP6_RIP_PROTOID);
#endif

    Rip6SizingMemDeleteMemPools ();
    RIP6_DELETE_IF_RBTREE ();
    RIP6_DELETE_INST_IF_RBTREE ();
    /***   Deleting RIP6 task and Queues    ***/
    /* Deleting the Semaphore */
    if (gRip6SemTaskLockId)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RIP6_TASK_SEM_NAME);
        gRip6SemTaskLockId = 0;
    }
    if (gRip6Rtm6SemLockId)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RIP6_RTM6RT_SEM_NAME);
        gRip6Rtm6SemLockId = 0;
    }
    OsixDeleteTask (RIP6_SELF_NODE, (const UINT1 *) RIP6_TASK_NAME);
    OsixDeleteQ (RIP6_SELF_NODE, (const UINT1 *) RIP6_TASK_CONTROL_QUEUE);
}

/******************************************************************************
 * DESCRIPTION : Registers the mib with SNMP 
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/

#ifdef FUTURE_SNMP_WANTED
INT4
RegisterFSRIP6withFutureSNMP ()
{
    if (SNMP_AGT_RegisterMib
        ((tSNMP_GroupOIDType *) &
         fsrip6_FMAS_GroupOIDTable,
         (tSNMP_BaseOIDType *) &
         fsrip6_FMAS_BaseOIDTable,
         (tSNMP_MIBObjectDescrType
          *) &
         fsrip6_FMAS_MIBObjectTable,
         fsrip6_FMAS_Global_data, (INT4) fsrip6_MAX_OBJECTS) != SNMP_SUCCESS)
    {
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}
#endif /*FUTURE_SNMP_WANTED */

/******************************************************************************
 * DESCRIPTION : Notifies the given data to RIP6 Task
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/
VOID
Rip6DataNotify (INT1 *pDummy)
{
    fd_set              ReadFds;    /* ReadBits */
    INT4                i4RetVal;
    struct timeval      tv;
#ifndef LNXIP4_WANTED
    MEMSET (&ReadFds, 0, sizeof (fd_set));
#endif
    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO (&ReadFds);
    UNUSED_PARAM (pDummy);
    RIP6_INIT_COMPLETE (OSIX_SUCCESS);
    while (gi4Rip6SockId == RIP6_FAILURE)
    {
        OsixDelayTask (HUNDRED);
    }

    OsixDelayTask (FOURHUNDRED);
    while (RIP6_INITIALIZE_ONE)
    {
        FD_SET (gi4Rip6SockId, &ReadFds);
#ifndef LNXIP4_WANTED
        i4RetVal = select (gi4Rip6SockId + RIP6_INITIALIZE_ONE, &ReadFds, NULL,
                           NULL, NULL);
#else
        i4RetVal = select (gi4Rip6SockId + RIP6_INITIALIZE_ONE, &ReadFds, NULL,
                           NULL, &tv);
#endif
        if (i4RetVal > RIP6_FALSE)
        {
            OsixSendEvent (SELF, (const UINT1 *) RIP6_TASK_NAME,
                           RIP6_DATA_EVENT);
        }
/* Delay Task Here, Since the socket is non-blocling, select is also expected to be a non-blocking one.So inorder to schedule the task, we delay for'N' ticks and this task will get scheduled for every 'N' ticks*/
        OsixDelayTask (RIP6_SELECT_TASK_DELAY);
    }
    UNUSED_PARAM (tv);
}

/******************************************************************************
 * DESCRIPTION : Process the Route Redistributed into RIP6
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/
INT4
Rip6ProcessRedisRoutes (VOID)
{
    UINT4               u4Count = 0;
    UINT4               u4InstanceId;
    INT4                i4Status = RIP6_FAILURE;
    INT4                i4Found;
    tRip6RrdRouteInfo  *pCurRtEntry = NULL;
    tRip6RrdRouteInfo  *pNextRtEntry = NULL;
    tRip6RtEntry       *pRtEntry = NULL;
    tRip6RtEntry       *pRip6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    OsixSemTake (gRip6Rtm6SemLockId);

    pNextRtEntry =
        (tRip6RrdRouteInfo *) TMO_SLL_First (&gRip6RedistributionList);

    while (((pCurRtEntry = pNextRtEntry) != NULL) && (u4Count < 1000))
    {
        TMO_SLL_Delete (&gRip6RedistributionList, &(pCurRtEntry->Next));
        pNextRtEntry =
            (tRip6RrdRouteInfo *) TMO_SLL_First (&gRip6RedistributionList);
        if (pCurRtEntry->pRtEntry.u4Index > u4MaxIfs)
        {
            if (MemReleaseMemBlock ((tMemPoolId) i4Rip6RrdRouteInfoId,
                                    (UINT1 *) pCurRtEntry) != RIP6_SUCCESS)
            {
                RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                              "RIP6 : rip6 static add, could not "
                              "release mem to pool \n");
                RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                               "%s mem err: Type = %d  PoolId = "
                               "%d Memptr = %p",
                               ERROR_FATAL_STR, ERR_MEM_RELEASE,
                               RIP6_INITIALIZE_ZERO, i4Rip6RrdRouteInfoId);
            }

            continue;
        }

        u4InstanceId =
            RIP6_GET_INST_IF_MAP_ENTRY (pCurRtEntry->pRtEntry.u4Index)->
            u4InstanceId;

        if (pCurRtEntry->pRtEntry.u2Flag == RIP6_ROUTE_FEASIBLE)
        {
#ifndef RIP6_SINGLE_INSTANCE
            /* Multiple Instance */
            if (((RIP6_GET_INST_IF_MAP_ENTRY (pCurRtEntry->pRtEntry.u4Index)->
                  u4InsIfStatus & RIP6_IFACE_ATTACHED) != RIP6_IFACE_ATTACHED)
                ||
                ((RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                          pCurRtEntry->pRtEntry.u4Index)->
                  u1Status & RIP6_IFACE_UP) != RIP6_IFACE_UP)
                ||
                ((RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                          pCurRtEntry->pRtEntry.u4Index)->
                  u1Status & RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED))
            {
                /* RIP6 is not enabled over this interface. Add this route
                 * to the RB Tree External Route Database */
                i4Status = Rip6RedisRouteAdd (&pCurRtEntry->pRtEntry);
            }
            else
#endif
            {
                /* If single instance, then always add the redistributed
                 * routes to the TRIE. Else if multiple instance, now RIP
                 * is enable over this interface. So add the route to the
                 * instance TRIE */
                pRtEntry = NULL;
                i4Found = Rip6TrieBestEntry (&pCurRtEntry->pRtEntry.dst,
                                             RIP6_STATIC_LOOKUP,
                                             pCurRtEntry->pRtEntry.u1Prefixlen,
                                             u4InstanceId, &pRtEntry,
                                             &pRibNode);

                if (pRtEntry == NULL)
                {
                    if (pCurRtEntry->pRtEntry.i1Proto == RIP6_LOCAL)
                    {

                        i4Found = Rip6TrieBestEntry (&pCurRtEntry->pRtEntry.dst,
                                                     RIP6_DYNAMIC_LOOKUP,
                                                     pCurRtEntry->pRtEntry.
                                                     u1Prefixlen, u4InstanceId,
                                                     &pRtEntry, &pRibNode);
                        if (pRtEntry != NULL)
                        {
                            while (pRtEntry)
                            {
                                Rip6TimerStop (pRtEntry);
                                Rip6SendRtChgNotification (&pRtEntry->dst,
                                                           pRtEntry->
                                                           u1Prefixlen,
                                                           &pRtEntry->nexthop,
                                                           pRtEntry->u1Metric,
                                                           pRtEntry->i1Proto,
                                                           pRtEntry->u4Index,
                                                           120,
                                                           NETIPV6_DELETE_ROUTE);
                                Rip6TrieDeleteEntry (pRtEntry, u4InstanceId);

                                pRtEntry = pRtEntry->pNext;
                            }
                        }
                    }

                    pRtEntry = Rip6RtEntryFill (&pCurRtEntry->pRtEntry.dst,
                                                pCurRtEntry->pRtEntry.
                                                u1Prefixlen,
                                                &pCurRtEntry->pRtEntry.nexthop,
                                                pCurRtEntry->pRtEntry.u1Metric,
                                                pCurRtEntry->pRtEntry.i1Type,
                                                pCurRtEntry->pRtEntry.i1Proto,
                                                pCurRtEntry->pRtEntry.u2RtTag,
                                                pCurRtEntry->pRtEntry.u4Index,
                                                pCurRtEntry->pRtEntry.u1Level);
                    /*if (pRtEntry == NULL)
                       {
                       MEM_FREE (pCurRtEntry);
                       continue;
                       } */
                    if (pRtEntry == NULL)
                    {
                        if (MemReleaseMemBlock
                            ((tMemPoolId) i4Rip6RrdRouteInfoId,
                             (UINT1 *) pCurRtEntry) != RIP6_SUCCESS)
                        {
                            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                          "RIP6 : rip6 static add, could not "
                                          "release mem to pool \n");
                            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                           "%s mem err: Type = %d  PoolId = "
                                           "%d Memptr = %p",
                                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                           RIP6_INITIALIZE_ZERO,
                                           i4Rip6RrdRouteInfoId);
                        }

                        continue;
                    }
                    if (Rip6TrieAddEntry (pRtEntry, u4InstanceId) ==
                        RIP6_FAILURE)
                    {
                        if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                                (UINT1 *) pRtEntry) !=
                            RIP6_SUCCESS)
                        {
                            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                          "RIP6 : rip6 static add, could not "
                                          "release mem to pool \n");
                            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                           "%s mem err: Type = %d  PoolId = "
                                           "%d Memptr = %p",
                                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                           RIP6_INITIALIZE_ZERO, i4Rip6rtId);
                        }
                        pRtEntry = NULL;
                        i4Status = RIP6_FAILURE;
                        if (MemReleaseMemBlock
                            ((tMemPoolId) i4Rip6RrdRouteInfoId,
                             (UINT1 *) pCurRtEntry) != RIP6_SUCCESS)
                        {
                            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                          "RIP6 : rip6 static add, could not "
                                          "release mem to pool \n");
                            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                           "%s mem err: Type = %d  PoolId = "
                                           "%d Memptr = %p",
                                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                           RIP6_INITIALIZE_ZERO,
                                           i4Rip6RrdRouteInfoId);
                        }
                        OsixSemGive (gRip6Rtm6SemLockId);
                        return i4Status;
                    }
                    Rip6CheckTrigUpdateSend (RIP6_CHECK_ALL, pRtEntry->u4Index,
                                             pRtEntry);
                }
                else if (pRtEntry->i1Proto == RIPNG)
                {
                    /* Only the RIPNG Route is present. */
                    pRip6RtEntry = Rip6RtEntryFill (&pCurRtEntry->pRtEntry.dst,
                                                    pCurRtEntry->pRtEntry.
                                                    u1Prefixlen,
                                                    &pCurRtEntry->pRtEntry.
                                                    nexthop,
                                                    pCurRtEntry->pRtEntry.
                                                    u1Metric,
                                                    pCurRtEntry->pRtEntry.
                                                    i1Type,
                                                    pCurRtEntry->pRtEntry.
                                                    i1Proto,
                                                    pCurRtEntry->pRtEntry.
                                                    u2RtTag,
                                                    pCurRtEntry->pRtEntry.
                                                    u4Index,
                                                    pCurRtEntry->pRtEntry.
                                                    u1Level);
                    if (pRip6RtEntry == NULL)
                    {
                        if (MemReleaseMemBlock
                            ((tMemPoolId) i4Rip6RrdRouteInfoId,
                             (UINT1 *) pCurRtEntry) != RIP6_SUCCESS)
                        {
                            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                          "RIP6 : rip6 static add, could not "
                                          "release mem to pool \n");
                            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                           "%s mem err: Type = %d  PoolId = "
                                           "%d Memptr = %p",
                                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                           RIP6_INITIALIZE_ZERO,
                                           i4Rip6RrdRouteInfoId);
                        }

                        continue;
                    }
                    OsixSemGive (gRip6Rtm6SemLockId);
                    Rip6TrieUpdate (pRtEntry, u4InstanceId, pRip6RtEntry);
                    OsixSemGive (gRip6Rtm6SemLockId);
                    Rip6CheckTrigUpdateSend (RIP6_CHECK_ALL,
                                             pRip6RtEntry->u4Index,
                                             pRip6RtEntry);
                }
                else
                {
                    /* NON-RIPNG Route is already present. */
                    pRtEntry->u1Prefixlen = pCurRtEntry->pRtEntry.u1Prefixlen;
                    pRtEntry->u1Metric = pCurRtEntry->pRtEntry.u1Metric;
                    pRtEntry->i1Type = pCurRtEntry->pRtEntry.i1Type;
                    pRtEntry->i1Proto = pCurRtEntry->pRtEntry.i1Proto;
                    pRtEntry->u2RtTag = pCurRtEntry->pRtEntry.u2RtTag;
                    pRtEntry->u4Index = pCurRtEntry->pRtEntry.u4Index;
                    pRtEntry->u1Level = pCurRtEntry->pRtEntry.u1Level;
                    MEMCPY (&pRtEntry->dst, &pCurRtEntry->pRtEntry.dst,
                            sizeof (tIp6Addr));
                    MEMCPY (&pRtEntry->nexthop, &pCurRtEntry->pRtEntry.nexthop,
                            sizeof (tIp6Addr));
                    OsixGetSysTime (&(pRtEntry->u4ChangeTime));
                    Rip6CheckTrigUpdateSend (RIP6_CHECK_ALL, pRtEntry->u4Index,
                                             pRtEntry);

                }
            }
        }
        else if (pCurRtEntry->pRtEntry.u2Flag == RIP6_ROUTE_WITHDRAWN)
        {
#ifndef RIP6_SINGLE_INSTANCE
            /* Multiple Instance */
            if (((RIP6_GET_INST_IF_MAP_ENTRY (pCurRtEntry->pRtEntry.u4Index)->
                  u4InsIfStatus & RIP6_IFACE_ATTACHED) != RIP6_IFACE_ATTACHED)
                ||
                ((RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                          pCurRtEntry->pRtEntry.u4Index)->
                  u1Status & RIP6_IFACE_UP) != RIP6_IFACE_UP)
                ||
                ((RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                          pCurRtEntry->pRtEntry.u4Index)->
                  u1Status & RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED))
            {
                /* RIP6 is not enabled over this interface. Delete this route
                 * from the RB Tree External Route Database */
                i4Status = Rip6RedisRouteDelete (&pCurRtEntry->pRtEntry);
            }
            else
#endif
            {
                i4Status = RIP6_SUCCESS;
                /* If single instance, then always delete the redistributed
                 * routes from the TRIE. Else if multiple instance, now RIP
                 * is enable over this interface. So delete the route from the
                 * instance TRIE */
                i4Found = Rip6TrieBestEntry (&pCurRtEntry->pRtEntry.dst,
                                             RIP6_STATIC_LOOKUP,
                                             pCurRtEntry->pRtEntry.u1Prefixlen,
                                             u4InstanceId, &pRtEntry,
                                             &pRibNode);
                if (pRtEntry != NULL)
                {
                    if (pRtEntry->i1Proto == pCurRtEntry->pRtEntry.i1Proto)
                    {
                        pRtEntry->u1Metric = RIP6_INFINITY;
                        pRtEntry->u1RtFlag = RIP6_INITIALIZE_ONE;
                        if (gu4HoldOnFlag == OSIX_FALSE)

                        {
                            Rip6TmrStart (RIP6_HOLD_ON_TIMER_ID,
                                          gRip6TimerListId,
                                          &(garip6InstanceDatabase
                                            [u4InstanceId]->Rip6RtHold.
                                            HoldTimer), RIP6_HOLD_ON_TIME);
                            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC,
                                          RIP6_NAME,
                                          "RIP6 : Process redist route, setting Hold on flag On\n");
                            gu4HoldOnFlag = OSIX_TRUE;
                        }

                        Rip6CheckTrigUpdateSend (RIP6_CHECK_ALL,
                                                 pRtEntry->u4Index, pRtEntry);
                    }
                }
            }
        }
        if (MemReleaseMemBlock ((tMemPoolId) i4Rip6RrdRouteInfoId,
                                (UINT1 *) pCurRtEntry) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                          "RIP6 : rip6 static add, could not "
                          "release mem to pool \n");
            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = "
                           "%d Memptr = %p",
                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                           RIP6_INITIALIZE_ZERO, i4Rip6RrdRouteInfoId);
        }

    }
    OsixSemGive (gRip6Rtm6SemLockId);

    UNUSED_PARAM (i4Found);
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Adds the given route to the RB External Route Database
 * INPUTS      : pRrdRtEntry - Route to be added.
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/
INT4
Rip6RedisRouteAdd (tRip6RtEntry * pRrdRtEntry)
{
    tRip6RtEntry       *pRtEntry = NULL;

    pRtEntry =
        (tRip6RtEntry *) RBTreeGet (gRip6RBTree, (tRBElem *) pRrdRtEntry);
    if (pRtEntry == NULL)
    {
        pRtEntry = Rip6RtEntryFill (&pRrdRtEntry->dst, pRrdRtEntry->u1Prefixlen,
                                    &pRrdRtEntry->nexthop,
                                    pRrdRtEntry->u1Metric, pRrdRtEntry->i1Type,
                                    pRrdRtEntry->i1Proto, pRrdRtEntry->u2RtTag,
                                    pRrdRtEntry->u4Index, pRrdRtEntry->u1Level);
        if (pRtEntry == NULL)
        {
            return RIP6_FAILURE;
        }

        return Rip6AddNodeToRBTree (gRip6RBTree, pRtEntry);
    }
    else
    {
        pRtEntry->u1Prefixlen = pRrdRtEntry->u1Prefixlen;
        pRtEntry->u1Metric = pRrdRtEntry->u1Metric;
        pRtEntry->i1Type = pRrdRtEntry->i1Type;
        pRtEntry->i1Proto = pRrdRtEntry->i1Proto;
        pRtEntry->u2RtTag = pRrdRtEntry->u2RtTag;
        pRtEntry->u4Index = pRrdRtEntry->u4Index;
        pRtEntry->u1Level = pRrdRtEntry->u1Level;
        MEMCPY (&pRtEntry->dst, &pRrdRtEntry->dst, sizeof (tIp6Addr));
        MEMCPY (&pRtEntry->nexthop, &pRrdRtEntry->nexthop, sizeof (tIp6Addr));
        OsixGetSysTime (&(pRtEntry->u4ChangeTime));
    }
    return RIP6_SUCCESS;
}

#ifndef RIP6_SINGLE_INSTANCE
/******************************************************************************
 * DESCRIPTION : Deletes the given route from the RB External Route Database
 * INPUTS      : pRrdRtEntry - Route to be deleted.
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/
INT4
Rip6RedisRouteDelete (tRip6RtEntry * pRrdRtEntry)
{
    tRip6RtEntry       *pRtEntry = NULL;

    pRtEntry = (tRip6RtEntry *) RBTreeGet (gRip6RBTree,
                                           (tRBElem *) pRrdRtEntry);
    if (pRtEntry != NULL)
    {
        if (RBTreeRemove (gRip6RBTree, (tRBElem *) pRtEntry) == RB_SUCCESS)
        {
            if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                    (UINT1 *) pRtEntry) != RIP6_SUCCESS)
            {
                RIP6_TRC_ARG1 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                               "RIP6 : rip6 add prefix to rt unable to"
                               " release mem to pool id = %d \n", i4Rip6rtId);

                RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                               "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                               ERROR_FATAL_STR, ERR_MEM_RELEASE,
                               RIP6_INITIALIZE_ZERO, i4Rip6rtId);
            }
            pRtEntry = NULL;
            return RIP6_SUCCESS;
        }
        else
        {
            return RIP6_FAILURE;
        }
    }
    return RIP6_SUCCESS;
}
#endif

/******************************************************************************
 * DESCRIPTION : Adds the given route to the RB External Route Database
 * INPUTS      : pRBTree - RB Tree where the route needs to be added.
 *               pRtEntry - Route to be added.
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/
INT4
Rip6AddNodeToRBTree (struct rbtree *pRBTree, tRip6RtEntry * pRtEntry)
{

    if (RBTreeAdd (pRBTree, (tRBElem *) pRtEntry) == RB_FAILURE)
    {
        if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                (UINT1 *) pRtEntry) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                           "RIP6 : rip6 add prefix to rt unable to"
                           " release mem to pool id = %d \n", i4Rip6rtId);

            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                           RIP6_INITIALIZE_ZERO, i4Rip6rtId);
        }
        pRtEntry = NULL;
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Process the RIP6 Enable/Disable event. If RIP6 is enabled over
 *               the interface this routine copies the redistributed routes
 *               reachable over that interface to the corresponding instance
 *               TRIE from the RB External route database. If the RIP6 is
 *               disabled then the redistributed routes are copied back to
 *               RB External Route Database.
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : INT4
 * NOTES       :
 ******************************************************************************/
INT4
Rip6CheckExtRoutes (UINT4 u4Index, INT4 i4Flag)
{
    UINT4               u4InstanceId;
    tRip6RtEntry       *pNextRtEntry = NULL;
    tRip6RtEntry       *pCurRtEntry = NULL;
    tRip6RtEntry       *pRtEntry = NULL;
    VOID               *pNextNode = NULL;
    VOID               *pCurNode = NULL;
    INT4                i4Status = RIP6_SUCCESS;
    INT4                i4Found = RIP6_FAILURE;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;
    if (i4Flag == RIP6_ENABLED)
    {
        pNextRtEntry = (tRip6RtEntry *) RBTreeGetFirst (gRip6RBTree);
        pCurRtEntry = pNextRtEntry;
        while ((pCurRtEntry = pNextRtEntry) != NULL)
        {
            pNextRtEntry = (tRip6RtEntry *)
                RBTreeGetNext (gRip6RBTree, (tRBElem *) pCurRtEntry,
                               Rip6ComapreRoutes);

            if (pCurRtEntry->u4Index == u4Index)
            {
                if (pCurRtEntry->i1Proto == RIP6_LOCAL)
                {

                    i4Found = Rip6TrieBestEntry (&pCurRtEntry->dst,
                                                 RIP6_DYNAMIC_LOOKUP,
                                                 pCurRtEntry->u1Prefixlen,
                                                 u4InstanceId, &pRtEntry,
                                                 &pCurNode);
                    if (i4Found == RIP6_FAILURE)
                    {
                        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                      "RIP6 : Invalid entry !!!\n");
                    }
                    if (pRtEntry != NULL)
                    {
                        while (pRtEntry)
                        {
                            Rip6TimerStop (pRtEntry);
                            Rip6SendRtChgNotification (&pRtEntry->dst,
                                                       pRtEntry->u1Prefixlen,
                                                       &pRtEntry->nexthop,
                                                       pRtEntry->u1Metric,
                                                       pRtEntry->i1Proto,
                                                       pRtEntry->u4Index, 120,
                                                       NETIPV6_DELETE_ROUTE);
                            Rip6TrieDeleteEntry (pRtEntry, u4InstanceId);

                            pRtEntry = pRtEntry->pNext;
                        }
                    }

                }

                pRtEntry = Rip6RtEntryFill (&pCurRtEntry->dst,
                                            pCurRtEntry->u1Prefixlen,
                                            &pCurRtEntry->nexthop,
                                            pCurRtEntry->u1Metric,
                                            pCurRtEntry->i1Type,
                                            pCurRtEntry->i1Proto,
                                            pCurRtEntry->u2RtTag,
                                            pCurRtEntry->u4Index,
                                            pCurRtEntry->u1Level);
                if (pRtEntry == NULL)
                {
                    return RIP6_FAILURE;
                }
                if (Rip6TrieAddEntry (pRtEntry, u4InstanceId) == RIP6_FAILURE)
                {
                    if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                            (UINT1 *) pRtEntry) != RIP6_SUCCESS)
                    {
                        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                      "RIP6 : rip6 static add, could not "
                                      "release mem to pool \n");

                        RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                       "%s mem err: Type = %d  PoolId = %d "
                                       "Memptr = %p",
                                       ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                       RIP6_INITIALIZE_ZERO, i4Rip6rtId);
                    }
                    pRtEntry = NULL;
                    return RIP6_FAILURE;
                }

                if (RBTreeRemove (gRip6RBTree, (tRBElem *) pCurRtEntry)
                    == RB_SUCCESS)
                {
                    if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                            (UINT1 *) pCurRtEntry) !=
                        RIP6_SUCCESS)
                    {
                        RIP6_TRC_ARG1 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                       "RIP6 : rip6 add prefix to rt unable to"
                                       " release mem to pool id = %d \n",
                                       i4Rip6rtId);
                        RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                       "%s mem err: Type = %d  PoolId = %d "
                                       "mptr = %p",
                                       ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                       RIP6_INITIALIZE_ZERO, i4Rip6rtId);
                    }
                    pCurRtEntry = NULL;
                }
            }
        }
    }
    else if (i4Flag == RIP6_DISABLED)
    {
        if (Rip6TrieGetFirstEntry (&pNextRtEntry, &pNextNode, u4InstanceId)
            == RIP6_SUCCESS)
        {
            pCurRtEntry = pNextRtEntry;
            pCurNode = pNextNode;
        }

        while ((pCurRtEntry = pNextRtEntry) != NULL)
        {
            i4Status = RIP6_SUCCESS;
            pCurNode = pNextNode;
            pNextNode = NULL;
            pNextRtEntry = NULL;

            if (Rip6TrieGetNextEntry (&pNextRtEntry, pCurNode, &pNextNode,
                                      u4InstanceId) != RIP6_SUCCESS)
            {
                i4Status = RIP6_FAILURE;
            }

            if ((pCurRtEntry->u4Index == u4Index) &&
                (pCurRtEntry->i1Proto != RIPNG))
            {
                /* Redistributed Route reachable via the same index. */
                Rip6RedisRouteAdd (pCurRtEntry);
                Rip6TrieDeleteEntry (pCurRtEntry, u4InstanceId);
            }

            if (i4Status == RIP6_FAILURE)
            {
                /* No more route is present in the TRIE to process. */
                break;
            }
        }
    }

    UNUSED_PARAM (i4Found);
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Send out the Triggered Responses with metric 16 for the default
 *               routes when the DefRouteAdvt is disabled on an interface.
 *             
 * INPUTS      : u4Index
 * OUTPUTS     : None
 * RETURNS     : None
 * NOTES       :
 ******************************************************************************/
VOID
Rip6DisableDefRouteAdvt (UINT4 u4Index)
{
    tRip6RtEntry       *pRtEntry = NULL;
    tIp6Addr            dstAddr;
    UINT1               u1PrefixLen;
    VOID               *pRibNode = NULL;
    UINT4               u4InstanceId;
    UINT1               u1Metric = 0;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;
    SET_DEFAULT_PREFIX (dstAddr);
    u1PrefixLen = 0;

    Rip6TrieBestEntry (&dstAddr,
                       RIP6_STATIC_LOOKUP,
                       u1PrefixLen, u4InstanceId, &pRtEntry, &pRibNode);

    if (pRtEntry != NULL)
    {
        u1Metric = pRtEntry->u1Metric;
        pRtEntry->u1Metric = RIP6_INFINITY;
        pRtEntry->u1RtFlag = RIP6_INITIALIZE_ONE;
        Rip6CheckTrigUpdateSend (RIP6_CHECK_ALL, pRtEntry->u4Index, pRtEntry);

        pRtEntry->u1Metric = u1Metric;
    }
}

/******************************************************************************
 * DESCRIPTION : RIP6 Semaphore Initialisation
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS      
 ******************************************************************************/
INT4
Rip6TaskSemInit ()
{
    tOsixSemId          SemId = 0;
    tOsixSemId          Rtm6SemId = 0;

    if (OsixCreateSem ((const UINT1 *) RIP6_TASK_SEM_NAME,
                       RIP6_SEM_CREATE_INIT_CNT, 0, &SemId) != OSIX_SUCCESS)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, ALL_FAILURE_TRC, RIP6_NAME,
                      "RIP6MAIN: Rip6Init :Semaphore Creation Failed \n");
        return RIP6_FAILURE;
    }

    if (OsixCreateSem ((const UINT1 *) RIP6_RTM6RT_SEM_NAME,
                       RIP6_SEM_CREATE_INIT_CNT, 0, &Rtm6SemId) != OSIX_SUCCESS)
    {
        OsixDeleteSem (SELF, (CONST UINT1 *) RIP6_TASK_SEM_NAME);
        RIP6_TRC_ARG (RIP6_MOD_TRC, ALL_FAILURE_TRC, RIP6_NAME,
                      "RIP6MAIN: Rip6Init :RTM6 RT Semaphore Creation Failed \n");
        KW_FALSEPOSITIVE_FIX (SemId);
        return RIP6_FAILURE;
    }

    gRip6SemTaskLockId = SemId;
    gRip6Rtm6SemLockId = Rtm6SemId;
    return (RIP6_SUCCESS);
}

/******************************************************************************
 * DESCRIPTION : Function to Lock RIP6 Task  
 * INPUTS      : None
 * OUTPUTS     : None
 * RETURNS     : SNMP_SUCCESS/FAILURE
 * NOTES       :
 ******************************************************************************/
INT4
Rip6Lock (VOID)
{
    if (OsixTakeSem (SELF, (const UINT1 *) RIP6_TASK_SEM_NAME,
                     OSIX_WAIT, 0) != OSIX_SUCCESS)
    {
        RIP6_TRC_ARG1 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                       "RIP6MAIN:Rip6Lock - Sem Take Failed for %s\n",
                       RIP6_TASK_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************
* DESCRIPTION : Function to UnLock RIP6 Task
* INPUTS      : None 
* OUTPUTS     : None
* RETURNS     : SNMP_SUCCESS/SNMP_FAILURE
****************************************************************************/
INT4
Rip6UnLock (VOID)
{
    if (OsixGiveSem (SELF, (const UINT1 *) RIP6_TASK_SEM_NAME) != OSIX_SUCCESS)
    {
        RIP6_TRC_ARG1 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                       "RIP6MAIN:Rip6Unlock - Sem Give Failed for %s\n",
                       RIP6_TASK_SEM_NAME);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/* Function     : Rip6SendRouteMapUpdateMsg                                  */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module, this function posts an event to Rip6     */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : RIP6_SUCCESS if Route Map update Msg Sent to RIP6          */
/*                successfully                                               */
/*                RIP6_FAILURE otherwise                                     */
/*****************************************************************************/
INT4
Rip6SendRouteMapUpdateMsg (UINT1 *pu1RMapName, UINT4 u4Status)
{
    tOsixMsg           *pBuf;
    INT4                i4OutCome;
    UINT4               u4Size;
    UINT1               au1NameBuf[RMAP_MAX_NAME_LEN + 4];

    u4Size = RMAP_MAX_NAME_LEN + sizeof (u4Status);    /* message size is hdr+ size of
                                                     * RouteMap Name+Status */

    if (pu1RMapName == NULL)
    {
        return RIP6_FAILURE;
    }

    /* enshure there is no garbage behind map name */
    MEMSET (au1NameBuf, 0, sizeof (au1NameBuf));
    STRNCPY (au1NameBuf, pu1RMapName, RMAP_MAX_NAME_LEN + 4 - 1);
    au1NameBuf[(sizeof (au1NameBuf) - 1)] = '\0';

    pBuf = CRU_BUF_Allocate_MsgBufChain (u4Size, 0);
    if (pBuf == NULL)
    {
        return RIP6_FAILURE;
    }
    MEMSET (pBuf->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pBuf, "Rip6SndRte");
    /*** copy status to offset 0 ***/
    if ((i4OutCome = CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u4Status, 0,
                                                sizeof (u4Status))) !=
        CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP6_FAILURE;
    }

    /*** copy map name to offset 4 ***/
    if ((i4OutCome =
         CRU_BUF_Copy_OverBufChain (pBuf, au1NameBuf, sizeof (u4Status),
                                    RMAP_MAX_NAME_LEN)) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return RIP6_FAILURE;
    }

    /*** post to Rip6 queue ***/

    if (OsixSendToQ (SELF, (const UINT1 *) RIP6_TASK_CONTROL_QUEUE, pBuf,
                     OSIX_MSG_NORMAL) == OSIX_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                      "RIP6 : In Rip6SendRouteMapUpdateMsg OsixSendTo failed"
                      "while sending RIP6_ROUTEMAP_UPDATE_EVENT \n ");
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buffer    error occurred, Type = %d Bufptr = %p",
                           ERROR_FATAL, ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

    if (OsixSendEvent (SELF, (const UINT1 *) RIP6_TASK_NAME,
                       RIP6_ROUTEMAP_UPDATE_EVENT) == OSIX_FAILURE)
    {
        /* Packet Already posted to Queue. Dont Free the Buffer. */
        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6: In Rip6SendRouteMapUpdateMsg, event posting failed "
                       "to RIP Q = %d Buf = %p\n",
                       RIP6_TASK_CONTROL_QUEUE, pBuf);
        return RIP6_FAILURE;
    }

    return i4OutCome;
}

/*****************************************************************************/
/* Function     : Rip6ProcessRMapHandler                                     */
/*                                                                           */
/* Description  : This function processes Route Map Update Message from      */
/*                Route Map module.                                          */
/*                                                                           */
/* Input        : pu1RMapName   - RouteMap Name                              */
/*                u4Status      - RouteMap status                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None.                                                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
Rip6ProcessRMapHandler (tOsixMsg * pBuf)
{
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    UINT4               u4Status = 0;
    UINT1               u1Status = FILTERNIG_STAT_DEFAULT;

    if (pBuf == NULL)
    {
        return;
    }

    if (CRU_BUF_Copy_FromBufChain
        (pBuf, (UINT1 *) &u4Status, 0, sizeof (u4Status)) != sizeof (u4Status))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    MEMSET (au1RMapName, 0, (RMAP_MAX_NAME_LEN + 4));
    if (CRU_BUF_Copy_FromBufChain (pBuf, au1RMapName, sizeof (u4Status),
                                   RMAP_MAX_NAME_LEN) != RMAP_MAX_NAME_LEN)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return;
    }

    if (u4Status != 0)
    {
        u1Status = FILTERNIG_STAT_ENABLE;
    }
    else
    {
        u1Status = FILTERNIG_STAT_DISABLE;
    }

    if (gpDistributeInFilterRMap != NULL)
    {
        if (STRCMP (gpDistributeInFilterRMap->au1DistInOutFilterRMapName,
                    au1RMapName) == 0)
        {
            gpDistributeInFilterRMap->u1Status = u1Status;
        }
    }

    if (gpDistributeOutFilterRMap != NULL)
    {
        if (STRCMP (gpDistributeOutFilterRMap->au1DistInOutFilterRMapName,
                    au1RMapName) == 0)
        {
            gpDistributeOutFilterRMap->u1Status = u1Status;
        }
    }
    if (gpDistanceFilterRMap != NULL)
    {
        if (STRCMP (gpDistanceFilterRMap->au1DistInOutFilterRMapName,
                    au1RMapName) == 0)
        {
            gpDistanceFilterRMap->u1Status = u1Status;
        }
    }
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
}

/******************************************************************************
 * DESCRIPTION : To add/remove the ipv6 interface address to the 
 *                rip6 route database based on the address status
 *      
 * INPUTS      : The logical interface (u4Index)
 *               The IP6 Address whose status have changed (pIp6Addr)
 *               The Status of the Address (u1AddrStatus)
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS/RIP6_FAILURE
 ******************************************************************************/
INT4
Rip6ProcIp6AddrChg (tIp6Addr * pIp6Addr, UINT1 u1PrefixLen,
                    UINT4 u4Index, UINT1 u1AddrStatus)
{
    UINT4               u4InstanceId = 0;
    INT4                i4RetVal = RIP6_SUCCESS;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {
        /* Interface is not attached with any instance. */
        return RIP6_FAILURE;
    }

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ALLOCATED) !=
        RIP6_IFACE_ALLOCATED)
    {
        /* Interface is not created at the lower layer. */
        return RIP6_FAILURE;
    }

    if (u1AddrStatus == RIP6_ADDR_ADD)
    {
        i4RetVal = Rip6IfIp6AddrEnable (u4Index, pIp6Addr, u1PrefixLen);
    }
    else
    {
        i4RetVal = Rip6IfIp6AddrDisable (u4Index, pIp6Addr, u1PrefixLen);
    }
    return i4RetVal;
}

/******************************************************************************
 * DESCRIPTION : If the rip6 is enabled over the interface, add the address
                  to the rip6 database. Else add it to the Rip6RBTree so that
                  is will be added to the rip6 database when rip6 is enabled 
                  on the interface
 * INPUTS      : The logical interface (u4Index)
 *               The Ipv6 Address (pIp6Addr)
 *               Ip6 Address Prefix Length (u1PrefixLen)
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS/RIP6_FAILURE
 ******************************************************************************/
INT4
Rip6IfIp6AddrEnable (UINT4 u4Index, tIp6Addr * pIp6Addr, UINT1 u1PrefLen)
{
    tRip6RtEntry       *pCurRtEntry = NULL;
    tRip6RtEntry       *pRtEntry = NULL;
    tRip6RtEntry       *pTempRtEntry = NULL;
    tIp6Addr            Ip6NwAddr;
    tIp6Addr            NullAddr;
    VOID               *pCurNode = NULL;
    UINT4               u4InstanceId = 0;
    INT4                i4RetStatus = RIP6_SUCCESS;
    UINT1               u1Metric = 0;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;
    SET_ADDR_UNSPECIFIED (NullAddr);
    if (gRip6Redistribution.u1DefMetric != 0)
    {
        u1Metric = gRip6Redistribution.u1DefMetric;
    }
    else
    {
        u1Metric = RIP6_INITIALIZE_ONE;
    }

    Ip6CopyAddrBits (&Ip6NwAddr, pIp6Addr, u1PrefLen);
    pRtEntry = Rip6RtEntryFill (&Ip6NwAddr, u1PrefLen, &NullAddr,
                                u1Metric, DIRECT, RIP6_LOCAL,
                                RIP6_INITIALIZE_ONE, u4Index,
                                RIP6_INITIALIZE_ZERO);
    if (pRtEntry == NULL)
    {
        return RIP6_FAILURE;
    }

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ENABLED) ==
        RIP6_IFACE_ENABLED)
    {
        /* Verify if the ip6Addr is already present in the rip6 database
         * if not add it */
        i4RetStatus = Rip6TrieBestEntry (&Ip6NwAddr, RIP6_DYNAMIC_LOOKUP,
                                         u1PrefLen, u4InstanceId,
                                         &pCurRtEntry, &pCurNode);
        if ((i4RetStatus == RIP6_SUCCESS))
        {
            /* Local route always has more precedence than ripng learned routes */
            pTempRtEntry = pCurRtEntry;
            while (pTempRtEntry)
            {
                Rip6TimerStop (pTempRtEntry);
                Rip6SendRtChgNotification (&pTempRtEntry->dst,
                                           pTempRtEntry->u1Prefixlen,
                                           &pTempRtEntry->nexthop,
                                           pTempRtEntry->u1Metric,
                                           pTempRtEntry->i1Proto,
                                           pTempRtEntry->u4Index, 120,
                                           NETIPV6_DELETE_ROUTE);

                i4RetStatus = Rip6TrieDeleteEntry (pTempRtEntry, u4InstanceId);
                pTempRtEntry = pTempRtEntry->pNext;
            }
        }

        /* Else the route is not found and
         * Rip6 is enabled on the interface. Add it directly to rip6 database */
        i4RetStatus = Rip6TrieAddEntry (pRtEntry, u4InstanceId);
        if (i4RetStatus == RIP6_FAILURE)
        {
            if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                    (UINT1 *) pRtEntry) != RIP6_SUCCESS)
            {
                RIP6_TRC_ARG (RIP6_MOD_TRC, CONTROL_PLANE_TRC | OS_RESOURCE_TRC,
                              RIP6_NAME,
                              "RIP6 : rip6 connected addr node could not "
                              "release mem to pool \n");

                RIP6_TRC_ARG4 (RIP6_MOD_TRC, OS_RESOURCE_TRC, RIP6_NAME,
                               "%s mem err: Type = %d  PoolId = %d "
                               "Memptr = %p",
                               ERROR_FATAL_STR, ERR_MEM_RELEASE,
                               RIP6_INITIALIZE_ZERO, i4Rip6rtId);
            }
            pRtEntry = NULL;
            return RIP6_FAILURE;
        }
    }
    else
    {
        i4RetStatus = Rip6RedisRouteAdd (pRtEntry);
    }
    return i4RetStatus;
}

/******************************************************************************
 * DESCRIPTION : If the rip6 is enabled over the interface, remove the address
                  from the rip6 database. Else remove it from the Rip6RBTree
 * INPUTS      : The logical interface (u4Index)
 *               The Ipv6 Address (pIp6Addr)
 *               Ip6 Address Prefix Length (u1PrefixLen)
 * OUTPUTS     : None
 * RETURNS     : RIP6_SUCCESS/RIP6_FAILURE
 ******************************************************************************/
INT4
Rip6IfIp6AddrDisable (UINT4 u4Index, tIp6Addr * pIp6Addr, UINT1 u1PrefLen)
{
    tRip6RtEntry       *pNextRtEntry = NULL;
    tRip6RtEntry       *pCurRtEntry = NULL;
    tRip6RtEntry        RtEntry;
    tIp6Addr            Ip6NwAddr;
    VOID               *pNextNode = NULL;
    VOID               *pCurNode = NULL;
    UINT4               u4InstanceId = 0;
    INT4                i4RetStatus = RIP6_FAILURE;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;
    Ip6CopyAddrBits (&Ip6NwAddr, pIp6Addr, u1PrefLen);

    if ((RIP6_GET_IF_ENTRY (u4Index)->u1Status & RIP6_IFACE_ENABLED) ==
        RIP6_IFACE_ENABLED)
    {
        /* Rip6 is enabled on the interface.
         * Remove it directly from rip6 database */
        i4RetStatus = Rip6TrieGetFirstEntry (&pNextRtEntry, &pNextNode,
                                             u4InstanceId);

        while (i4RetStatus == RIP6_SUCCESS)
        {
            pCurNode = pNextNode;
            pCurRtEntry = pNextRtEntry;
            pNextNode = NULL;
            pNextRtEntry = NULL;
            i4RetStatus = Rip6TrieGetNextEntry (&pNextRtEntry, pCurNode,
                                                &pNextNode, u4InstanceId);

            if ((pCurRtEntry->u4Index == u4Index)
                && (pCurRtEntry->i1Proto == RIP6_LOCAL)
                && (MEMCMP (&pCurRtEntry->dst, &Ip6NwAddr,
                            sizeof (tIp6Addr)) == 0)
                && (pCurRtEntry->u1Prefixlen == u1PrefLen))
            {
                i4RetStatus = Rip6TrieDeleteEntry (pCurRtEntry, u4InstanceId);
                break;
            }
        }
    }
    else
    {
        /* Rip6 is disabled on the interface.
         * Remove it from rip6 RBTree */
        MEMCPY (&RtEntry.dst, &Ip6NwAddr, sizeof (tIp6Addr));
        RtEntry.u1Prefixlen = u1PrefLen;
        pCurRtEntry = RBTreeGet (gRip6RBTree, &RtEntry);

        if (pCurRtEntry != NULL)
        {
            if (RBTreeRemove (gRip6RBTree, (tRBElem *) pCurRtEntry)
                == RB_SUCCESS)
            {
                i4RetStatus = RIP6_SUCCESS;
                if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                        (UINT1 *) pCurRtEntry) != RIP6_SUCCESS)
                {
                    RIP6_TRC_ARG (RIP6_MOD_TRC, CONTROL_PLANE_TRC |
                                  OS_RESOURCE_TRC, RIP6_NAME,
                                  "RIP6 : rip6 connected addr node could not"
                                  " release mem to pool \n");

                    RIP6_TRC_ARG4 (RIP6_MOD_TRC, OS_RESOURCE_TRC, RIP6_NAME,
                                   "%s mem err: Type = %d  PoolId = %d "
                                   "Memptr = %p",
                                   ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                   RIP6_INITIALIZE_ZERO, i4Rip6rtId);

                    i4RetStatus = RIP6_FAILURE;
                }
            }
        }
    }

    return i4RetStatus;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6UtilRBTreeCreateStub                         */
/*                                                                           */
/*    Description         : This stub function is called when RIP6_ARRAY_TO_ */
/*                          RBTREE_WANTED is disabled. To avoid compilation  */
/*                          break , condition covers always true/false, this */
/*                          function is added. Check its usage at calling    */
/*                          function.                                        */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : IP6_SUCCESS                                      */
/*****************************************************************************/
INT4
Rip6UtilRBTreeCreateStub ()
{
    return RIP6_SUCCESS;
}

/***************************** END OF FILE **********************************/
