/*******************************************************************
 **** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ****
 **** $Id: rip6io.c,v 1.22.2.1 2018/03/23 13:23:02 siva Exp $
 **********************************************************************/

/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    rip6io.c   
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    RIP6 Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996 
 *
 *    DESCRIPTION                  :    This file contains the processing 
 *                                      of the RIPv6 requests and responses 
 *                                       
 *-----------------------------------------------------------------------------
 */

#include "rip6inc.h"

/*
 * Private Routines' Prototypes 
 ******************************/

PRIVATE INT4 Rip6IsResponseValid PROTO ((UINT1 u1Hlim, UINT4 u4Index,
                                         tIp6Addr * pSrc, tIp6Addr * pDst));
PRIVATE INT4 Rip6ValidateRte PROTO ((tRip6Rte * pRte));
PRIVATE INT4 Rip6IsNextHopRte PROTO ((tRip6Rte * pRip6Rte));

extern INT4         i4Rip6ParamId;

INT1                gai1Buf[IP6_DEFAULT_MTU];

extern UINT1        gu1Rip6Distance;
/******************************************************************************
 * DESCRIPTION : Called when we get a RIP6 packet. This function is called to 
 *               determine the instance, to which the instance belongs, and 
 *               then pass the control to RIP6Input.
 *
 * INPUTS      : The parameters ptr passed from UDP6 module (pUdp6Rip6Params)
 *               The received buffer (pBuf)
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Rip6WrapperInput (tRip6Udp6Params * pUdp6Rip6Params,
                  tCRU_BUF_CHAIN_HEADER * pBuf)
{

    gInstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pUdp6Rip6Params->u4Index)->
        u4InstanceId;

    Rip6Input (pUdp6Rip6Params, pBuf);

}

/******************************************************************************
 * DESCRIPTION : Called when we get a RIP6 packet. Demultiplexes the valid
 *               RIPng packet for request or response processing.
 *
 * INPUTS      : The parameters ptr passed from UDP6 module (pUdp6Rip6Params)
 *               The received buffer (pBuf)
 *
 * OUTPUTS     : None.
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

INT4
Rip6Input (tRip6Udp6Params * pUdp6Rip6Params, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tRip6Hdr           *pRip6Hdr = NULL, rip6Hdr;
    tInstanceDatabase  *pContext = NULL;
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pUdp6Rip6Params->u4Index)->
        u4InstanceId;

    if (u4InstanceId == (UINT4) RIP6_INVALID_INSTANCE)
    {

        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                       "RIP6 : Interface not attached to any Instance  = %d \n",
                       pUdp6Rip6Params->u4Index);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 input, Failure in releasing buffer = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }
    if (pUdp6Rip6Params->u4Len <= RIP6_HDR_LEN)
    {
        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, pUdp6Rip6Params->u4Index)->
           stats.u4Discards);

        /* Erroroneous rip6 packet received */
        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                       "RIP6 : Erroneous rip6 packet received of length = %d \n",
                       pUdp6Rip6Params->u4Len);

        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                       "%s gen err: Type = %d \n", ERROR_FATAL_STR,
                       ERR_GEN_INVALID_VAL);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 input, Failure in releasing buffer = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

    if ((!
         (RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                  pUdp6Rip6Params->u4Index)->
          u1Status & RIP6_IFACE_ENABLED))
        ||
        (!(RIP6_GET_INST_IF_ENTRY (u4InstanceId, pUdp6Rip6Params->u4Index)->
           u1Status & RIP6_IFACE_UP))
        ||
        (IS_ADDR_UNSPECIFIED
         (RIP6_GET_IF_ENTRY (pUdp6Rip6Params->u4Index)->LinkLocalAddr)))
    {

        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                       "RIP6 : Status of If = %d and Global Status = %d\n",
                       RIP6_GET_IF_ENTRY (pUdp6Rip6Params->u4Index)->u1Status,
                       garip6InstanceStatus[u4InstanceId]);

        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, pUdp6Rip6Params->
                                   u4Index)->stats.u4Discards);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 input,Failure in releasing buf = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }
    RIP6_BUF_READ_OFFSET (pBuf) = 0;

    /* Extract RIP6 header from the buffer */
    pRip6Hdr = (tRip6Hdr *) Rip6BufRead (pBuf, (UINT1 *) &rip6Hdr,
                                         (UINT4) RIP6_BUF_READ_OFFSET (pBuf),
                                         (UINT4) sizeof (tRip6Hdr),
                                         RIP6_INITIALIZE_ZERO);

    if (pRip6Hdr == NULL)
    {
        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, pUdp6Rip6Params->
                                   u4Index)->stats.u4Discards);

        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                      "RIP6 : RIP6 input, Got a NULL pRip6Hdr\n");

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_READ, pBuf);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 input,Failure in releasing buf = %p\n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

    RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                   "RIP6 : The RIP6 header has Command = %d Version = %d "
                   "Reserved field = %d \n",
                   pRip6Hdr->u1Command, pRip6Hdr->u1Ver, pRip6Hdr->u2Res);

    ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, pUdp6Rip6Params->u4Index)->
       stats.u4InMessages);

    if (pRip6Hdr->u1Ver != RIP6_VER_1)
    {

        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC | RIP6_BUFFER_TRC,
                       RIP6_NAME,
                       "RIP6 : Got RIP6 packet of different version."
                       "Release buf = %p \n", pBuf);

        /* Update statistics */
        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, pUdp6Rip6Params->
                                   u4Index)->stats.u4OtherVer);
        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                   pUdp6Rip6Params->u4Index)->stats.u4Discards);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 input,Failure in releasing buf = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

    /* Check if reserved fields are zero */
    if (pRip6Hdr->u2Res == RIP6_INITIALIZE_ZERO)
    {
        /* Demultiplex the packet after checking command field */
        switch (pRip6Hdr->u1Command)
        {
            case RIP6_REQUEST:

                Rip6RcvRequest (pUdp6Rip6Params->u2SrcPort,
                                pUdp6Rip6Params->u2DstPort,
                                pUdp6Rip6Params->u4Index,
                                pUdp6Rip6Params->u4Len,
                                &pUdp6Rip6Params->srcAddr, pBuf);

                break;
            case RIP6_RESPONSE:

                pContext = garip6InstanceDatabase[u4InstanceId];
                if (Rip6GetPeerFilterStatus
                    (pContext, &pUdp6Rip6Params->srcAddr) == RIP6_DENY)
                {
                    RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                                   "RIP6 : Peer filter reponse status is denied = %d \n",
                                   pUdp6Rip6Params->u4Index);
                    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) ==
                        RIP6_FAILURE)
                    {
                        RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                       RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                                       RIP6_NAME,
                                       "RIP6 : RIP6 input, Failure in releasing buffer = %p \n",
                                       pBuf);
                        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                       "%s buf err: Type = %d Bufptr = %p",
                                       ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                    }
                    return RIP6_FAILURE;
                }

                if (pUdp6Rip6Params->u2SrcPort != RIP6_PORT)
                {
                    RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                                   "RIP6 : Got a RIP6 resp with src port=%d "
                                   "(nonRIP6 port). BufPtr=%p rel \n",
                                   pUdp6Rip6Params->u2SrcPort, pBuf);

                    ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                               pUdp6Rip6Params->u4Index)->stats.
                       u4Discards);

                    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) ==
                        RIP6_FAILURE)
                    {
                        RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                       RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                                       RIP6_NAME,
                                       "RIP6 : RIP6 input, Failure in releasing buf = %p \n",
                                       pBuf);

                        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                       "%s buf err: Type = %d Bufptr = %p",
                                       ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);

                    }
                    return RIP6_FAILURE;
                }

                Rip6RcvResponse (pUdp6Rip6Params->i4Hlim,
                                 pUdp6Rip6Params->u4Index,
                                 pUdp6Rip6Params->u4Len,
                                 &pUdp6Rip6Params->srcAddr,
                                 &pUdp6Rip6Params->dstAddr, pBuf);
                break;

            default:
                RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                               "RIP6 : Got a RIP6 packet of different Command"
                               "  = %d. Buf ptr released = %p \n",
                               pRip6Hdr->u1Command, pBuf);

                ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                           pUdp6Rip6Params->u4Index)->stats.
                   u4UnknownCmds);
                ++(RIP6_GET_INST_IF_ENTRY
                   (u4InstanceId, pUdp6Rip6Params->u4Index)->stats.u4Discards);

                if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
                {
                    RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                   RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                                   RIP6_NAME,
                                   "RIP6 : RIP6 input,Failure in releasing buf = %p \n",
                                   pBuf);

                    RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                   "%s buf err: Type = %d Bufptr = %p",
                                   ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }
                break;
        }
        return RIP6_SUCCESS;
    }

    RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "RIP6 : Got RIP6 pkt with Reserved field of RIP6Hdr "
                   "not 0.So buf ptr rel=%p\n", pBuf);

    ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, pUdp6Rip6Params->
                               u4Index)->stats.u4Discards);

    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
    {
        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                       RIP6_NAME,
                       "RIP6 : RIP6 input, Failure in releasing buf = %p \n",
                       pBuf);

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_RELEASE, pBuf);
    }
    return RIP6_FAILURE;
}

 /******************************************************************************
 *  Function Name   : Rip6GetPeerFilterStatus
 * 
 * DESCRIPTION : To Verify RIP Peer table filter configured to deny for this peer
 * 
 * INPUTS      : tInstanceDatabase, tIp6Addr
 * 
 * OUTPUTS     : None
 * 
 * RETURNS     : RIP6_ALLOW - if successful in processing the RIP6 request
 *               RIP6_DENY  - otherwise
 * NOTES       :
 *******************************************************************************/

INT4
Rip6GetPeerFilterStatus (tInstanceDatabase * pContext, tIp6Addr * pSrcAddr)
{

    UINT4               u4Counter = 0;

    if (pContext->u4Rip6PeerFlag == RIP6_ALLOW)
    {
        return RIP6_ALLOW;
    }
    for (u4Counter = 0; u4Counter < RIP6_MAX_PEERS; u4Counter++)
    {
        /* Check the RIP Peer filter to allow the packet processing */
        if (MEMCMP
            ((pSrcAddr), &(pContext->aRip6PeerTable[u4Counter].PeerAddr),
             sizeof (tIp6Addr)) == 0)
        {
            return RIP6_DENY;
        }
    }
    return RIP6_ALLOW;

}

/******************************************************************************
 * DESCRIPTION : Called when we get a RIP6 request
 *
 * INPUTS      : The source UDP6 port (u2SrcPort),The dest UDP6 port
 *               (u2DstPort),The logical interface (u4Index),
 *               Length of RIP6 (u4Len),ptr to IPv6 src addr (pSrc),
 *               the buffer (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS - if successful in processing the RIP6 resquest
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/

INT4
Rip6RcvRequest (UINT2 u2SrcPort, UINT2 u2DstPort, UINT4 u4Index, UINT4 u4Len,
                tIp6Addr * pSrc, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT4               u4Routes, u4Count;

    tRip6Hdr            rip6Hdr;
    tRip6Rte           *pRte = NULL, rte;
    tRip6RtEntry       *pRip6RtEntry = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    RIP6_TRC_ARG6 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "RIP6:Rcvd RIP6 req Buf=%p SrcAddr=%s SrcUDP6Port=%d "
                   "DestUDP6Port=%d If=%d Len=%d",
                   pBuf, Ip6PrintAddr (pSrc), u2SrcPort, u2DstPort,
                   u4Index, u4Len);

    RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                  "RIP6 : Received RIP6 request buffer : \n");

    ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4InReq);

    if (u2DstPort != (UINT2) RIP6_PORT)
    {
        /* Update discard statistics for this logical interface */
        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4Discards);

        RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6 : Got RIP6 request with dest port = %d "
                       "(non RIP6 port).Buf = %p relsed \n", u2DstPort, pBuf);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 rcv request, Failure in releasing buf = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }

        return RIP6_FAILURE;
    }

    /* Get the number of routes in the RIPng packet */
    u4Routes = (u4Len - RIP6_HDR_LEN) / RIP6_RTE_LEN;

    if (u4Routes == RIP6_INITIALIZE_ONE)
    {
        /* Extract RTE from the buffer */
        pRte = (tRip6Rte *) Rip6BufRead (pBuf, (UINT1 *) &rte,
                                         RIP6_BUF_READ_OFFSET (pBuf),
                                         sizeof (tRip6Rte),
                                         RIP6_INITIALIZE_ZERO);

        if (pRte == NULL)
        {
            /* Update discard statistics for this logical interface */
            ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
               stats.u4Discards);

            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                          "RIP6 : RIP6 rcv request, Got a NULL pRte\n");
            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_READ, pBuf);

            if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
            {
                RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                               RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                               "RIP6 : RIP6 rcv request, Failure in releasing buf = %p \n",
                               pBuf);

                RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                               "%s buf err: Type = %d Bufptr = %p",
                               ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }

            return RIP6_FAILURE;
        }

        /* Check whether request for full routing table */
        if ((IS_ADDR_UNSPECIFIED (pRte->prefix)) &&
            (pRte->u1PrefixLen == RIP6_INITIALIZE_ZERO) &&
            (pRte->u1Metric == RIP6_INFINITY))
        {
            /*
             * Release the received buffer as we are going to allocate new
             * buffer for sending out a request for a full table
             */
            if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
            {
                RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                               RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                               "RIP6 : RIP6 rcv request, Failure in releasing buffer = %p \n",
                               pBuf);

                RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                               "%s buf err: Type = %d Bufptr = %p",
                               ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }

            if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes ==
                RIP6_INITIALIZE_ZERO)
            {
                /* No routes there in the routing table */
                return RIP6_FAILURE;
            }

            return (Rip6SendResponse (RIP6_FULL_ROUTE_TABLE, u2SrcPort,
                                      u4Index, u4Index, pSrc));
        }
    }

    /* Extract RTEs from the packet  */
    for (u4Count = RIP6_INITIALIZE_ZERO; u4Count < u4Routes; u4Count++)
    {
        /* 
         * Extract RTE from the buffer.If the number of the routes were
         * one then the extract from the buffer would have been done
         * already.So don't do it again.
         */
        if (u4Routes != RIP6_INITIALIZE_ONE)
        {
            /* Extract RTE from the buffer */
            pRte = (tRip6Rte *) Rip6BufRead (pBuf, (UINT1 *) &rte,
                                             RIP6_BUF_READ_OFFSET (pBuf),
                                             sizeof (tRip6Rte),
                                             RIP6_INITIALIZE_ZERO);
            if (pRte == NULL)
            {
                /* Update discard statistics for this logical interface */
                ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                           u4Index)->stats.u4Discards);

                RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                              "RIP6 : RIP6 rcv request, Got a NULL pRte\n");
                RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                               "%s buf err: Type = %d Bufptr = %p",
                               ERROR_FATAL_STR, ERR_BUF_READ, pBuf);

                if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
                {
                    RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                   RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                                   RIP6_NAME,
                                   "RIP6 : RIP6 rcv request, Failure in "
                                   "releasing buffer = %p \n", pBuf);

                    RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                   "%s buf err: Type = %d Bufptr = %p",
                                   ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);

                }

                return RIP6_FAILURE;
            }
        }

        /* Check if this RTE prefix is there in routing table */
        pRibNode = NULL;
        Rip6TrieBestEntry (&(pRte->prefix),
                           (UINT1) garip6InstanceDatabase[u4InstanceId]->
                           u4LookupPreference, pRte->u1PrefixLen,
                           u4InstanceId, &pRip6RtEntry, &pRibNode);

        if (pRip6RtEntry != NULL)
        {
            if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                         pRip6RtEntry->u4Index)->
                 u1Status & RIP6_IFACE_UP) == RIP6_IFACE_UP)
            {
                /* Assign metric from routing table entry to RTE entry's
                 * metric */
                RIP6_PKT_ASSIGN_METRIC (pBuf, u4Count, pRip6RtEntry->u1Metric);
            }
            else
            {
                RIP6_PKT_ASSIGN_METRIC (pBuf, u4Count, (UINT1) RIP6_INFINITY);
            }
        }
        else
        {
            /* Assign the infinity to the RTE entry's metric in the packet */
            RIP6_PKT_ASSIGN_METRIC (pBuf, u4Count, (UINT1) RIP6_INFINITY);
        }
    }

    /* Construct the reply */
    rip6Hdr.u1Command = RIP6_RESPONSE;
    rip6Hdr.u1Ver = RIP6_VER_1;
    rip6Hdr.u2Res = 0;

    /* Put the RIPng header into the buffer */
    if (Rip6BufWrite (pBuf, (UINT1 *) &rip6Hdr, RIP6_INITIALIZE_ZERO,
                      sizeof (tRip6Hdr), RIP6_INITIALIZE_ONE) == RIP6_FAILURE)
    {

        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "RIP6 : RIP6 rcv request unable to do write onto buffer = %p \n",
                       pBuf);

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_WRITE, pBuf);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 rcv request, Failure in releasing buf = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }

        return RIP6_FAILURE;
    }

    /* Send the packet */
    if ((Rip6SendDatagram (RIP6_INITIALIZE_ZERO, u2SrcPort, u4Index,
                           u4Len, pSrc, pBuf)) != RIP6_SUCCESS)
    {
        return RIP6_FAILURE;
    }
    else                        /* Releas the Buffer in case of Rip6SendDatagram Success */
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 put hdr in buf, Failure in releasing buffer = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
            return RIP6_FAILURE;
        }
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called when we receive a RIP6 response
 *
 * INPUTS      : The IPv6 hop limit (u1Hoplimit),
 *               The logical interface (u4Index),
 *               The length of the RIPng portion (u4Len),
 *               The ptr to IPv6 source address (pSrc)
 *               The ptr to IPv6 destination address (pDst)
 *               The received RIPng packet (pBuf)
 * OUTPUTS     : None.
 *
 * RETURNS     : RIP6_SUCCESS - if successful in processing the RIP6 response 
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/

INT4
Rip6RcvResponse (UINT1 u1Hoplimit, UINT4 u4Index, UINT4 u4Len, tIp6Addr * pSrc,
                 tIp6Addr * pDst, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1TrigSend = 0;
    UINT4               u4Rte;
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    RIP6_TRC_ARG6 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "RIP6:rcvd resp Buf=%p HopLim=%d Len=%d If=%d SrcAddr=%s"
                   " DestAddr=%s\n",
                   pBuf, u1Hoplimit, u4Len, u4Index, Ip6PrintAddr (pSrc),
                   Ip6PrintAddr (pDst));

    RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                  "RIP6 : Received RIP6 response buffer : \n");

    ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4InRes);

    /* Check the validity of the response */
    if ((Rip6IsResponseValid (u1Hoplimit, u4Index, pSrc, pDst)) == RIP6_FALSE)
    {
        /* Got an invalid response */

        /* Update discard statistics for this logical interface */
        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4Discards);

        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6 : Invalid RIP6 response Rcvd.So buf ptr = %p released \n",
                       pBuf);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 rcv response, Failure in releasing buffer ptr = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }

        return RIP6_FAILURE;
    }

    /* Get the number of RTEs in the RIPng packet */
    u4Rte = (u4Len - RIP6_HDR_LEN) / RIP6_RTE_LEN;

    if (Rip6ResponseProcessRte (&u1TrigSend, u4Index, u4Rte, pSrc,
                                pBuf) == RIP6_FAILURE)
    {
        return RIP6_FAILURE;
    }

    if (u1TrigSend != RIP6_INITIALIZE_ZERO)
    {
        /* Need to send Trigger update over this instance */
        garip6InstanceDatabase[u4InstanceId]->u1TrigFlag =
            RIP6_TRIGGERED_UPDATE_NEEDED;
        u1TrigSend = 0;
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called to process the RTEs in the RIPng response 
 *
 * INPUTS      : The logical interface (u4Index), 
 *               The maximum number of rtes to process (u4Rte),
 *               The ptr to IPv6 Source address (pSrc) and 
 *               The received buffer (pBuf)
 *
 * OUTPUTS     : The ptr to flag telling whether to send triggered update or not
 *               (pu1TrigSend)
 *
 * RETURNS     : RIP6_SUCCESS - if successful in processing the RIP6 response 
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/

INT4
Rip6ResponseProcessRte (UINT1 *pu1TrigSend, UINT4 u4Index, UINT4 u4Rte,
                        tIp6Addr * pSrc, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               u1MetricNew = 0;
    UINT4               u4Count;
    tIp6Addr           *pNextHop = NULL;
    tIp6Addr            nextHop;
    tRip6Rte           *pRte = NULL, rte;
    tRip6RtEntry       *pRtEntry = NULL, *p_rt_new = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4InstanceId;
    INT4                i4Found;
    UINT1               u1Distance = Rip6FilterRouteSource (pSrc);
    tRip6RtEntry        FiltRtEntry;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    *pu1TrigSend = 0;

    pNextHop = pSrc;

    /* 
     * Get each RTE entry in the packet one by one and put in the
     * routing table based upon checks
     */
    for (u4Count = RIP6_INITIALIZE_ZERO; u4Count < u4Rte; u4Count++)
    {
        i4Found = RIP6_SUCCESS;
        /* Extract RTE from the packet */
        pRte = (tRip6Rte *) Rip6BufRead (pBuf, (UINT1 *) &rte,
                                         RIP6_BUF_READ_OFFSET (pBuf),
                                         (UINT4) (sizeof (tRip6Rte)),
                                         RIP6_INITIALIZE_ZERO);
        if (pRte == NULL)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                          "RIP6 : RIP6 response process rte, Got a NULL pRte\n");

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_READ, pBuf);

            if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
            {
                RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                               RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                               "RIP6 : RIP6 response process rte, Failure "
                               "in releasing buffer ptr = %p \n", pBuf);

                RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                               "%s buf err: Type = %d Bufptr = %p",
                               ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }

            return RIP6_FAILURE;
        }

        FiltRtEntry.dst = pRte->prefix;
        FiltRtEntry.u1Prefixlen = pRte->u1PrefixLen;
        FiltRtEntry.i1Type = INDIRECT;

        if (Rip6ApplyInOutFilter (gpDistributeInFilterRMap, &FiltRtEntry)
            != RIP6_SUCCESS)
        {
            /* Stop processing this route and go to processing next route. */
            continue;
        }

        u1MetricNew = (UINT1) (RIP6_MIN (RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                                 u4Index)->
                                         u1Cost + pRte->u1Metric,
                                         RIP6_INFINITY));

        if (Rip6IsNextHopRte (pRte) == RIP6_SUCCESS)
        {
            /* Check if address is not 0 or non-link-local */
            if (IS_ADDR_UNSPECIFIED (pRte->prefix) != RIP6_FALSE)
            {
                pNextHop = pSrc;
            }
            else if (IS_ADDR_LLOCAL (pRte->prefix) != RIP6_FALSE)
            {
                Ip6AddrCopy (&nextHop, &pRte->prefix);
                pNextHop = &nextHop;
            }
            continue;
        }

        if (Rip6ValidateRte (pRte) != RIP6_SUCCESS)
        {
            continue;
        }
        /*  
         * Check whether the matching DYNAMIC entry there in the  
         * routing table.If there then try to update the existing dynamic
         * route else if there is a matching STATIC route then add to the
         * the leaf of the node containing the matching STATIC route
         */
        i4Found = Rip6TrieBestEntry (&(pRte->prefix), RIP6_DYNAMIC_LOOKUP,
                                     pRte->u1PrefixLen, u4InstanceId,
                                     &pRtEntry, &pRibNode);
        if (i4Found == RIP6_FAILURE)
        {
            /*
             * Since this entry is new it is to be put in the 
             * routing table provided there is space and the new dynamic route's
             * metric is not infinity.
             */
            if (u1MetricNew >= RIP6_INFINITY)
            {
                /* 
                 * Though the route is new, it can't be added as the metric
                 * is infinity.Continue processing with the next rte if any
                 */
                continue;
            }

            pRtEntry = Rip6RtEntryFill (&pRte->prefix,
                                        pRte->u1PrefixLen,
                                        pNextHop, u1MetricNew,
                                        INDIRECT, RIPNG,
                                        (UINT2) OSIX_NTOHS (pRte->u2RtTag),
                                        u4Index, RIP6_INITIALIZE_ZERO);
            if (pRtEntry == NULL)
            {

                if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
                {
                    RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                   RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                                   RIP6_NAME,
                                   "RIP6 : RIP6 response process rte, "
                                   "Failure in releasing buffer ptr = %p \n",
                                   pBuf);

                    RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                   "%s buf err: Type = %d Bufptr = %p\n",
                                   ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);

                }

                return RIP6_FAILURE;
            }
            else
            {
                if (Rip6TrieAddEntry (pRtEntry, u4InstanceId) == RIP6_FAILURE)
                {
                    garip6InstanceDatabase[u4InstanceId]->u4DiscaredRoutes++;

                    if (MemReleaseMemBlock
                        ((tMemPoolId) i4Rip6rtId,
                         (UINT1 *) pRtEntry) != RIP6_SUCCESS)
                    {
                        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC,
                                       RIP6_NAME,
                                       "RIP6 : rip6 resp prcss rte unable "
                                       "to rel mem to the pool of id = %d \n",
                                       i4Rip6rtId);

                        RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                                       "%s mem err: Type = %d  PoolId = %d "
                                       "Memptr = %p\n",
                                       ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                       (UINT1 *) pRtEntry, i4Rip6rtId);

                    }
                    pRtEntry = NULL;

                    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) ==
                        RIP6_FAILURE)
                    {
                        RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                       RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                                       RIP6_NAME,
                                       "RIP6 : RIP6 response process rte "
                                       "Failure in releasing buffer ptr = %p \n",
                                       pBuf);

                        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                       "%s buf err: Type = %d Bufptr = %p\n",
                                       ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                    }
                    return RIP6_FAILURE;
                }

                /* Start the route age timer for this route entry  */
                pRtEntry->rtEntryTimer.u1Id = RIP6_AGE_OUT_TIMER_ID;
                Rip6TimerStart (pRtEntry, RIP6_AGE_OUT_TIMER_ID,
                                RIP6_ROUTE_AGE_INTERVAL (pRtEntry));
                /* Set the flag indicating triggered update be sent */
                *pu1TrigSend = 1;
                /* intimate the Change of Route in Rip6 Route Table to IP6 */
                Rip6SendRtChgNotification (&pRtEntry->dst,
                                           pRtEntry->u1Prefixlen,
                                           &pRtEntry->nexthop,
                                           pRtEntry->u1Metric,
                                           pRtEntry->i1Proto,
                                           pRtEntry->u4Index,
                                           u1Distance, NETIPV6_ADD_ROUTE);

            }
        }
        else if (pRtEntry->i1Proto == RIP6_LOCAL)
        {
            continue;
        }
        else if (pRtEntry->i1Proto != RIPNG)
        {
            /* Found a matching entry which is not dynamic */
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                          "RIP6:recv resp got match with existing static "
                          "entry. Adding the dynamic rt.\n");

            if (u1MetricNew >= RIP6_INFINITY)
            {
                /*
                 * The route can't be added as the metric is infinity.
                 * Continue processing with the next rte if any
                 */
                continue;
            }

            p_rt_new = Rip6RtEntryFill (&pRte->prefix,
                                        pRte->u1PrefixLen,
                                        pNextHop, u1MetricNew,
                                        INDIRECT, RIPNG,
                                        (UINT1) OSIX_NTOHS (pRte->u2RtTag),
                                        u4Index, RIP6_INITIALIZE_ZERO);
            if (p_rt_new == NULL)
            {
                /*
                 * The entry could not be added because of mem 
                 * problem  for allocating for routing entry
                 */
                if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
                {
                    RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                                   RIP6_OS_RES_TRC | RIP6_BUFFER_TRC |
                                   RIP6_ALL_FAIL_TRC, RIP6_NAME,
                                   "RIP6 : RIP6 response process rte Failure ,"
                                   " as a result of allocation of memory for"
                                   " route entry fail:releasing buffer ptr = %p \n",
                                   pBuf);

                    RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                                   "%s buf err: Type = %d Bufptr = %p\n",
                                   ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
                }
                return RIP6_FAILURE;
            }
            else
            {
                if (Rip6TrieUpdate (pRtEntry, u4InstanceId, p_rt_new) ==
                    RIP6_FAILURE)
                {
                    return RIP6_FAILURE;
                }

                p_rt_new->rtEntryTimer.u1Id = RIP6_AGE_OUT_TIMER_ID;
                Rip6TimerStart (p_rt_new, RIP6_AGE_OUT_TIMER_ID,
                                RIP6_ROUTE_AGE_INTERVAL (p_rt_new));
                /* Set flag indicating that triggered update be sent */

                *pu1TrigSend = 1;

                Rip6SendRtChgNotification (&pRte->prefix,
                                           pRte->u1PrefixLen,
                                           pNextHop, u1MetricNew,
                                           RIPNG, u4Index, u1Distance,
                                           NETIPV6_ADD_ROUTE);
            }

        }

        /* Address matched with the existing dynamic entry */

        /* Place where Task Lock can go in */
        else if (Ip6AddrMatch (pSrc, &(pRtEntry->nexthop),
                               IP6_ADDR_SIZE_IN_BITS) == TRUE)
        {
            /* Place where Task UnLock can go in */

            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                          "RIP6:RIP6 rcv resp SrcAddr of RIPng pkt eqls"
                          " existing RtTbl entry's NextHop\n");

            if (pRtEntry->u1Metric != u1MetricNew)
            {
                pRtEntry->u1Metric = u1MetricNew;
                pRtEntry->u1RtFlag = 1;
                OsixGetSysTime (&(pRtEntry->u4ChangeTime));
                /* Set flag indicating that triggered update be sent */
                *pu1TrigSend = 1;
            }
            if (pRtEntry->u4Index != u4Index)
            {
                pRtEntry->u4Index = u4Index;
                pRtEntry->u1RtFlag = 1;
                OsixGetSysTime (&(pRtEntry->u4ChangeTime));
                /* Set flag indicating that triggered update be sent */
                *pu1TrigSend = 1;
            }
            if (pRtEntry->u2RtTag != OSIX_NTOHS (pRte->u2RtTag))
            {
                pRtEntry->u2RtTag = OSIX_NTOHS (pRte->u2RtTag);
                pRtEntry->u1RtFlag = 1;
                OsixGetSysTime (&(pRtEntry->u4ChangeTime));
                /* Set flag indicating that triggered update be sent */
                *pu1TrigSend = 1;
            }

            if (u1MetricNew < RIP6_INFINITY)
            {
                RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                              "RIP6 : RIP6 receive response restart the route "
                              "age timer \n");
                /* Check if the garbage collect is running */
                if (pRtEntry->rtEntryTimer.u1Id == RIP6_GARB_COLLECT_TIMER_ID)
                {
                    /* Stop the garbage collect timer */
                    Rip6TimerStop (pRtEntry);
                    pRtEntry->rtEntryTimer.u1Id = 0;
                }
                OsixGetSysTime (&(pRtEntry->u4ChangeTime));

                /* Initialize route entry's timeout */
                Rip6TimerStart (pRtEntry, RIP6_AGE_OUT_TIMER_ID,
                                RIP6_ROUTE_AGE_INTERVAL (pRtEntry));
            }
            else
            {
                if (pRtEntry->rtEntryTimer.u1Id == RIP6_GARB_COLLECT_TIMER_ID)
                {
                    continue;
                }
                else
                {
                    RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                                   "RIP6:RIP6 rcv resp do garbage collect for"
                                   " Dst = %s Prefixlen = %d \n",
                                   Ip6PrintAddr (&pRtEntry->dst),
                                   pRtEntry->u1Prefixlen);
                    Rip6RouteAgeout (pRtEntry);
                    continue;
                }
            }

            if (*pu1TrigSend == 1)
            {
                pRtEntry->u1Operation = RIP6HA_ADD_ROUTE;
                Rip6RedDbUtilAddTblNode (&gRip6DynInfoList,
                                         &pRtEntry->RouteDbNode);
                Rip6RedSyncDynInfo ();
            }
            /* intimate the Change of Route in Rip6 Route Table to IP6 */
            Rip6SendRtChgNotification (&pRtEntry->dst,
                                       pRtEntry->u1Prefixlen,
                                       &pRtEntry->nexthop,
                                       pRtEntry->u1Metric,
                                       pRtEntry->i1Proto, pRtEntry->u4Index,
                                       u1Distance, NETIPV6_MODIFY_ROUTE);
        }
        else if ((u1MetricNew < pRtEntry->u1Metric) &&
                 (u1MetricNew != RIP6_INFINITY))
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                          "RIP6 : RIP6 receive response metric "
                          "less than matching route entry's metric \n");

            pRtEntry->u1Operation = RIP6HA_DEL_ROUTE;
            Rip6RedDbUtilAddTblNode (&gRip6DynInfoList, &pRtEntry->RouteDbNode);
            Rip6RedSyncDynInfo ();
            /* Next-Hop differs. So Delete the Old route from IPv6 Routing
             * Table */
            Rip6SendRtChgNotification (&pRtEntry->dst,
                                       pRtEntry->u1Prefixlen,
                                       &pRtEntry->nexthop,
                                       pRtEntry->u1Metric,
                                       pRtEntry->i1Proto, pRtEntry->u4Index,
                                       u1Distance, NETIPV6_DELETE_ROUTE);

            pRtEntry->u1Prefixlen = pRte->u1PrefixLen;
            pRtEntry->u1Metric = u1MetricNew;
            pRtEntry->u4Index = u4Index;
            pRtEntry->u1RtFlag = 1;
            pRtEntry->i1Type = INDIRECT;
            pRtEntry->i1Proto = RIPNG;
            pRtEntry->u2RtTag = OSIX_NTOHS (pRte->u2RtTag);
            OsixGetSysTime (&(pRtEntry->u4ChangeTime));

            Ip6AddrCopy (&(pRtEntry->nexthop), pNextHop);

            RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                           "RIP6 : RIP6 rcv resp restart route age timer "
                           "for Dst= %s Prefixlen= %d\n",
                           Ip6PrintAddr (&pRtEntry->dst),
                           pRtEntry->u1Prefixlen);

            /* Initialize the route entry's timeout */
            if (Rip6TimerStop (pRtEntry) == RIP6_SUCCESS)
            {
                Rip6TimerStart (pRtEntry, RIP6_AGE_OUT_TIMER_ID,
                                RIP6_ROUTE_AGE_INTERVAL (pRtEntry));
            }

            pRtEntry->u1Operation = RIP6HA_ADD_ROUTE;
            Rip6RedDbUtilAddTblNode (&gRip6DynInfoList, &pRtEntry->RouteDbNode);
            Rip6RedSyncDynInfo ();
            /* intimate the Change of Route in Rip6 Route Table to IP6 */
            Rip6SendRtChgNotification (&pRtEntry->dst,
                                       pRtEntry->u1Prefixlen,
                                       &pRtEntry->nexthop,
                                       pRtEntry->u1Metric,
                                       pRtEntry->i1Proto, pRtEntry->u4Index,
                                       u1Distance, NETIPV6_MODIFY_ROUTE);

            *pu1TrigSend = 1;
        }
    }

    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
    {
        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                       RIP6_NAME,
                       "RIP6 : RIP6 response process rte, Failure in "
                       "releasing buffer = %p \n", pBuf);

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_RELEASE, pBuf);

        return RIP6_FAILURE;
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Find the appropriate routing table entry
 *
 * INPUTS      : The ptr to IPv6 Destination address (pIp6Dest) and 
 *               The Prefix length(u1PrefLength)
 *               Interface Index (u4Index)               
 *
 * OUTPUTS     : The ptr to flag telling whether to send triggered update or not
 *               (pu1TrigSend)
 *
 * RETURNS     : Pointer to the entry if find is successfull 
 *               NULL - otherwise
 * NOTES       :
 ******************************************************************************/
tRip6RtEntry       *
Rip6RtFindEntry (tIp6Addr * pIp6Dest, UINT1 u1PrefixLen, UINT4 u4Index)
{
    tRip6RtEntry       *pRtDynamic = NULL;
    tRip6RtEntry       *pRtStatic = NULL;
    tRip6RtEntry       *pRtLocal = NULL;

    pRtDynamic = Rip6RouteFindEntry (pIp6Dest, u1PrefixLen, RIPNG, u4Index);
    if (pRtDynamic == NULL)
    {

        pRtStatic = Rip6RouteFindEntry (pIp6Dest, u1PrefixLen, RIP6_STATIC,
                                        u4Index);
        if (pRtStatic == NULL)
        {
            pRtLocal = Rip6RouteFindEntry (pIp6Dest, u1PrefixLen,
                                           RIP6_LOCAL, u4Index);
            if (pRtLocal == NULL)
            {
                return NULL;
            }
            else
                return pRtLocal;
        }
        else
        {
            return pRtStatic;
        }
    }
    else
    {
        return pRtDynamic;
    }
}

/******************************************************************************
 * DESCRIPTION : Called to send a RIP6 response
 *
 * INPUTS      : The Flag telling it is a final update or triggered update
 *               or a regular update (u2RespFlag), The dest UDP6 port
 *               (u2DstPort),The logical interface (u4Index),The ptr
 *               to the IPv6 destination address (pDst)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS - if successful in sending the RIP6  response
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/

INT4
Rip6SendResponse (UINT2 u2RespFlag, UINT2 u2DstPort, UINT4 u4Index,
                  UINT4 u4Combine, tIp6Addr * pDst)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tRip6ScanParam      ScanParams;
    UINT4               u4Len;
    UINT4               u4_rt_counter = 0;
    UINT4               u4MaxNoRte;
    UINT4               u4InstanceId;

    /* Derive the Instance Id from the Interface over which the route is
     * to be advertised. */
    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "RIP6: Send RIP6 resp Dest port = %d If = %d "
                   "Resp flag = %d Dest Addr = %s \n",
                   u2DstPort, u4Index, u2RespFlag, Ip6PrintAddr (pDst));

    /* Get max_number of RTEs that can go in the buffer */
    u4MaxNoRte = MAX_RTE (u4Index);

    /* Allocate the buffer */
    pBuf = Rip6BufAlloc (u4MaxNoRte, u4InstanceId);

    if (pBuf == NULL)
    {

        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                      RIP6_NAME,
                      "RIP6 : Send Response, Failed to allocate "
                      " pbuf for forming the packet.\n");

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_ALLOC, RIP6_INITIALIZE_ZERO);

        return RIP6_FAILURE;
    }

    /* Put the RIP6 hdr into buffer */
    if (Rip6PutHdrInBuf (pBuf, RIP6_RESPONSE) == RIP6_FAILURE)
    {
        return RIP6_FAILURE;
    }

    u4Len = RIP6_HDR_LEN;

    if ((u2RespFlag & RIP6_FINAL_UPDATE) == RIP6_FINAL_UPDATE)
    {
        /* Scan and collect the routes learnt from interface u4Combine
         * and advertise the routes through the interface u4Index. */

        ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_INTF_FINAL;
        ScanParams.u4InstanceId = u4InstanceId;
        ScanParams.u4RecvIndex = u4Combine;
        ScanParams.u4SendIndex = u4Index;
        ScanParams.u2RespFlag = u2RespFlag;
        ScanParams.u2DstPort = u2DstPort;
        ScanParams.u1Horizon = RIP6_NO_HORIZONING;
        ScanParams.pu4RtCounter = &u4_rt_counter;
        ScanParams.pu4Len = &u4Len;
        ScanParams.u4MaxNoRte = u4MaxNoRte;
        ScanParams.pDst = pDst;
        ScanParams.ppBuf = &pBuf;

        if (Rip6TrieScan (&ScanParams) == RIP6_FAILURE)
        {
            return RIP6_FAILURE;
        }
    }
    else
    {
        /* Scan, collect and advt. the routes through the interface u4Index
         * from the corresponding Instance TRIE. */

        ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_INTF_ADVT;
        ScanParams.u4InstanceId = u4InstanceId;
        ScanParams.u4RecvIndex = u4Index;
        ScanParams.u4SendIndex = u4Index;
        ScanParams.u2RespFlag = u2RespFlag;
        ScanParams.u2DstPort = u2DstPort;
        ScanParams.u1Horizon = RIP6_GET_HORIZON (u4Index);
        ScanParams.pu4RtCounter = &u4_rt_counter;
        ScanParams.pu4Len = &u4Len;
        ScanParams.u4MaxNoRte = u4MaxNoRte;
        ScanParams.pDst = pDst;
        ScanParams.ppBuf = &pBuf;
        ScanParams.u4ThrotCnt = RIP6_THROT_MAX_PEROIDIC_ROUTE;

        if (Rip6TrieScan (&ScanParams) == RIP6_FAILURE)
        {
            return RIP6_FAILURE;
        }
    }

    /* Have to send the remaining routes */
    if (u4_rt_counter != 0)
    {
        if (u4_rt_counter != u4MaxNoRte)
        {
            if ((Rip6SendDatagram ((UINT1) u2RespFlag, u2DstPort, u4Index,
                                   u4Len, pDst, pBuf)) == RIP6_FAILURE)
            {
                return RIP6_FAILURE;
            }
        }
    }

    if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
    {
        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                       RIP6_NAME,
                       "RIP6 : RIP6 put hdr in buf, Failure in releasing buffer = %p \n",
                       pBuf);

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_RELEASE, pBuf);
        return RIP6_FAILURE;
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Combines all the routes learnt over the given interface into
 *               the passed buffer and sends the packet
 *
 * INPUTS      : The  flag telling whether it is the final update or
 *               triggered or regular update (u2RespFlag), the horizon
 *               flag telling how the routes will be combined (u1Horizon),
 *               the interface (i2Combine) over which to combine the routes,
 *               interface (u4Index) over which to send,
 *               Dest UDP6 port (u2DstPort), Max no RTE (u4MaxNoOfRte),
 *               Ptr to the dest IPv6 address (pDstAddr) and the ptr to buf
 *               ptr (pBuf)
 *
 * OUTPUTS     : The counter as to how many routes have been put in the
 *               buffer (pu4RouteCounter)
 *
 * RETURNS     : RIP6_SUCCESS - if it is successful in allocating memory for
 *                         new buffer or successful in sending the
 *                         the packet
 *               RIP6_FAILURE - otherwise
 *
 * NOTES       :
 ******************************************************************************/

INT4
Rip6SendRtLearntOverIf (UINT2 u2RespFlag, UINT1 u1Horizon, UINT4 u4Index,
                        UINT4 u4Combine, UINT2 u2DstPort,
                        UINT4 *pu4RouteCounter, UINT4 u4MaxNoOfRte,
                        UINT4 *pu4Len, tIp6Addr * pDstAddr,
                        tCRU_BUF_CHAIN_HEADER ** pBuf,
                        tRip6RtEntry * pCurRt, UINT4 u4InstanceId)
{

    RIP6_TRC_ARG5 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "RIP6:Combining routes over If=%d RespFlag=%d "
                   "Horizon=%d DestPort=%d DestAddr=%s",
                   u4Combine, u2RespFlag, u1Horizon, u2DstPort,
                   Ip6PrintAddr (pDstAddr));

    RIP6_TRC_ARG5 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "Sending over If=%d Route counter=%d Max no of"
                   " RTE= %d Len=%d BufPtr=%p\n",
                   u4Index, *pu4RouteCounter, u4MaxNoOfRte, *pu4Len, **pBuf);

    if (Rip6IsNetFilterEnabled (u4InstanceId, &pCurRt->dst) == RIP6_SUCCESS)
    {
        /* No need to advt the route */
        return RIP6_SUCCESS;
    }

    if (Rip6ApplyInOutFilter (gpDistributeOutFilterRMap, pCurRt)
        != RIP6_SUCCESS)
    {
        /* Stop processing this route. */
        return RIP6_SUCCESS;
    }

    if (IS_ADDR_LLOCAL (pCurRt->dst) != RIP6_FALSE)
    {
        /* No need to advt the route */
        return RIP6_SUCCESS;
    }

    if ((((u2RespFlag & RIP6_FINAL_UPDATE) == RIP6_FINAL_UPDATE) &&
         ((u2RespFlag & RIP6_IFACE_FINAL) == RIP6_IFACE_FINAL) &&
         ((u2RespFlag & RIP6_SEND_DYNAMIC) == RIP6_SEND_DYNAMIC)) ||
        (((u2RespFlag & RIP6_FINAL_UPDATE) == RIP6_FINAL_UPDATE) &&
         ((u2RespFlag & RIP6_SEND_DYNAMIC) == RIP6_SEND_DYNAMIC)))
    {
        if (pCurRt->i1Proto != RIPNG)
        {
            /* No need to advt the route */
            return RIP6_SUCCESS;
        }
    }

    /*
     * If the triggered update is to be sent take those routes which have
     * route change flag set indicating that
     */

    if (u2RespFlag == RIP6_TRIG_UPDATE)
    {
        if ((pCurRt->u1RtFlag == RIP6_FALSE) ||
            ((pCurRt->i1Proto == RIPNG) && (pCurRt->u4Index == u4Index)))
        {
            /* No need to advt the route */
            return RIP6_SUCCESS;
        }
    }

    /* In case of SPLIT HORIZON, update all dynamic routes except
     * the routes learnt over the same interface */

    if ((u1Horizon == RIP6_SPLIT_HORIZONING) && (pCurRt->i1Proto == RIPNG))
    {
        if (pCurRt->u4Index == u4Index)
        {
            /* No need to advt the route */
            return RIP6_SUCCESS;
        }
    }

    /*
     * The routes with the interface down are sent as with
     * metric infinity
     */

    /* Interface down */
    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Combine)->
         u1Status & RIP6_IFACE_UP) != RIP6_IFACE_UP)
    {
        u1Horizon = RIP6_POISON_REVERSE;
    }

    if (Rip6PutRteInBuf (*pBuf, u4Index, u1Horizon, u2RespFlag, pCurRt) ==
        RIP6_FAILURE)
    {
        return RIP6_FAILURE;
    }

    *pu4Len = *pu4Len + RIP6_RTE_LEN;

    /* Increment route counter after putting the route into the buffer */

    ++(*pu4RouteCounter);

    if (*pu4RouteCounter == u4MaxNoOfRte)
    {
        if (Rip6SendDatagram ((UINT1) u2RespFlag, u2DstPort,
                              u4Index, *pu4Len, pDstAddr,
                              *pBuf) != RIP6_SUCCESS)
        {
            return RIP6_FAILURE;
        }

        *pu4RouteCounter = 0;

        if (Rip6PutHdrInBuf (*pBuf, RIP6_RESPONSE) == RIP6_FAILURE)
        {
            return RIP6_FAILURE;
        }

        *pu4Len = RIP6_HDR_LEN;
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called to send out a RIP6 request for full routing table
 *               on a destination RIP6 ALL ROUTERS MULTICAST address over
 *               all the valid logical interfaces
 * 
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

INT4
Rip6SendRequest (UINT4 u4Index)
{
    UINT1               u1Copy = 1;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4InstanceId;
    tIp6Addr            dstAddr;
    tRip6Rte           *pRip6Rte = NULL, rip6_rte;
    UINT4               u4OutResponse;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    /* Allocate a buffer of size one RTE including */
    pBuf = Rip6BufAlloc (RIP6_INITIALIZE_ONE, u4InstanceId);

    if (pBuf == NULL)
    {

        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                      "RIP6 : Send Request, No memory to allocate buffer \n");

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_ALLOC, RIP6_INITIALIZE_ZERO);

        return RIP6_FAILURE;
    }

    /* Put the RIP6 hdr into the buffer */
    if (Rip6PutHdrInBuf (pBuf, RIP6_REQUEST) == RIP6_FAILURE)
    {
        return RIP6_FAILURE;
    }

    /* Put the RIP6 RTE into the buffer */
    pRip6Rte = (tRip6Rte *) & rip6_rte;

    MEMSET (pRip6Rte, RIP6_INITIALIZE_ZERO, sizeof (tRip6Rte));
    pRip6Rte->u1Metric = RIP6_INFINITY;

    if (Rip6BufWrite
        (pBuf, (UINT1 *) pRip6Rte, RIP6_BUF_WRITE_OFFSET (pBuf),
         sizeof (tRip6Rte), u1Copy) == RIP6_FAILURE)
    {

        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "RIP6 : RIP6 send request unable to do write onto buf = %p\n",
                       pBuf);

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p",
                       ERROR_FATAL_STR, ERR_BUF_WRITE, pBuf);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "RIP6 : RIP6 send request, Failure in releasing buffer ptr = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }

        return RIP6_FAILURE;
    }

    SET_ALL_RIP_ROUTERS (dstAddr);

    /* 
     * Send the request over each of the interface on which RIPng is 
     * enabled and which is up
     */

    if (Rip6SendDatagram (RIP6_INITIALIZE_ZERO, RIP6_PORT, u4Index,
                          RIP6_HDR_LEN + RIP6_RTE_LEN, &dstAddr,
                          pBuf) == RIP6_FAILURE)
    {
        return RIP6_FAILURE;
    }
    else
    {
        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4OutReq);

        u4OutResponse = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                u4Index)->stats.u4OutRes;

        if (u4OutResponse > RIP6_INITIALIZE_ZERO)
        {
            --(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4OutRes);
        }

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {

            RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                           RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 send request, Failure in "
                           "releasing buffer ptr = %p \n", pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p",
                           ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);

            return RIP6_FAILURE;
        }
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called to send a RIP6 packet, request or response 
 *
 * INPUTS      : The Flag telling it is a final update or triggered update
 *               or a regular update (u1SendFlag), The Dest UDP6 port
 *               (u2DstPort), The logical interface (u4Index), 
 *               Len (u4Len), The ptr to the IPv6 destination address (pDst),
 *               buf ptr (pBuf)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS - if successful in sending the RIP6  response
 *               RIP6_FAILURE - otherwise
 * NOTES       :
 ******************************************************************************/

INT4
Rip6SendDatagram (UINT1 u1SendFlag, UINT2 u2DstPort, UINT4 u4Index, UINT4 u4Len,
                  tIp6Addr * pDstAddr, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Addr           *pSrcAddr = NULL;
    tIp6Addr            SrcAddress;
    struct sockaddr_in6 PeerAddr;
    UINT4               u4Rip6DataLen;
    struct msghdr       Ip6MsgHdr;
    struct in6_pktinfo *pIn6Pktinfo = NULL;
    UINT4               u4InstanceId;
    INT4                i4ReturnValue = RIP6_SUCCESS;
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4McHopLimit = IP6_MAX_HLIM;
    INT4                i4McLoop = FALSE;
    struct cmsghdr      CmsgInfo;
#else
    tNetIpv6IfInfo      NetIpv6IfInfo;
    UINT1               CmsgInfo[112];
    struct iovec        Iov;
    struct cmsghdr     *pCmsgInfo;
#endif

    if (RIP6_GET_NODE_STATUS () != RM_ACTIVE)
    {
        /* Packet send done only in Active Node */
        return RIP6_SUCCESS;
    }

    pSrcAddr = &SrcAddress;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    RIP6_TRC_ARG6 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "RIP6:RIP6 send Buf=%p Send flag=%d DstUDP6Port=%d "
                   "If=%d DestAddr=%s Len=%d\n",
                   pBuf, u1SendFlag, u2DstPort, u4Index,
                   Ip6PrintAddr (pDstAddr), u4Len);

    /* 
     * The check below is made because due to the configuring of the
     * SPLIT-HORIZONING we have routes in the routing table,still
     * we won't be putting in the buffer.So though the case of having
     * no routes is handled at the start of the send response path
     * the below check has to be at end.
     */
    if (u4Len <= RIP6_HDR_LEN)
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 send datagram, Failure in "
                           "releasing buf = %p \n", pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);

        }

        return RIP6_FAILURE;
    }

    if (u4Len > IP6_DEFAULT_MTU)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                      "Too Big Rip6 Packet");

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 put hdr in buf, Failure in releasing "
                           "buffer = %p \n", pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

    /* Place where Task Lock can go */
    if (u2DstPort == RIP6_PORT)
    {

        /* Set src addr as link local addr for this logical interface */
        pSrcAddr = Rip6GetLlocalAddr (u4Index);
        if (pSrcAddr == NULL)
        {
            RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                           "RIP6 :SendDgram, No LLocalAddr for this logIf = %d."
                           "BufPtr rel=%p\n", u4Index, pBuf);

            ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
               stats.u4Discards);

            if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
            {
                RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                               RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                               "RIP6 : RIP6 send datagram, Failure in releasing"
                               "buffer ptr = %p \n", pBuf);

                RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                               "%s buf err: Type = %d Bufptr = %p",
                               ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }

            return RIP6_FAILURE;
        }
    }
    else
    {
        /* Set src addr as global unicast addr for this logical interface */
        i4ReturnValue = Rip6GetGlobalAddr (u4Index, pDstAddr, pSrcAddr);
        if (i4ReturnValue == RIP6_FAILURE)
        {
            RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                           "RIP6 :Send Dgram, No Gbl ucast addr for this "
                           "logical If=%d .BufPtr Rel=%p\n", u4Index, pBuf);
            ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
               stats.u4Discards);

            if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
            {
                RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                               RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                               "RIP6 : RIP6 send datagram, Failure in "
                               "releasing buffer ptr = %p \n", pBuf);

                RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                               "%s buf err: Type = %d Bufptr = %p",
                               ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
            }

            return RIP6_FAILURE;
        }
    }

    PeerAddr.sin6_family = AF_INET6;
    PeerAddr.sin6_port = OSIX_HTONS (u2DstPort);
#ifdef BSDCOMP_SLI_WANTED
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    i4ReturnValue = NetIpv6GetIfInfo (u4Index, &NetIpv6IfInfo);
    if (i4ReturnValue == NETIPV6_FAILURE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                      "No info present for the index\n");
        return RIP6_FAILURE;
    }
    PeerAddr.sin6_scope_id = NetIpv6IfInfo.u4IpPort;
#else
    PeerAddr.sin6_scope_id = u4Index;
#endif

    Ip6AddrCopy ((tIp6Addr *) (VOID *) &PeerAddr.sin6_addr.s6_addr, pDstAddr);

#ifndef BSDCOMP_SLI_WANTED
    u4Rip6DataLen = CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &gai1Buf,
                                               RIP6_INITIALIZE_ZERO, u4Len);

    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo = (struct in6_pktinfo *) (VOID *)
        CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    if (pIn6Pktinfo == NULL)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                      "pIn6Pktinfo is NULL\n");
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                           RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                           "RIP6 : RIP6 send datagram, Failure in releasing "
                           "buffer ptr = %p \n", pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p",
                           ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

    pIn6Pktinfo->ipi6_ifindex = u4Index;

    Ip6AddrCopy (((tIp6Addr *) (VOID *)
                  &pIn6Pktinfo->ipi6_addr.s6_addr), pSrcAddr);

    sendmsg (gi4Rip6SockId, &Ip6MsgHdr, RIP6_INITIALIZE_ZERO);

    if (setsockopt (gi4Rip6SockId, IPPROTO_IPV6, IPV6_MULTICAST_HOPS,
                    &i4McHopLimit, sizeof (INT4)) < 0)
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                           RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                           "RIP6 : RIP6 send datagram, Failure in releasing "
                           "buffer ptr = %p \n", pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p",
                           ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }
    if (setsockopt (gi4Rip6SockId, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                    &i4McLoop, sizeof (INT4)) < 0)
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                           RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                           "RIP6 : RIP6 send datagram, Failure in releasing "
                           "buffer ptr = %p \n", pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p",
                           ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }
    if (sendto (gi4Rip6SockId, &gai1Buf, u4Rip6DataLen,
                RIP6_INITIALIZE_ZERO, (struct sockaddr *) &PeerAddr,
                sizeof (struct sockaddr)) < RIP6_INITIALIZE_ZERO)
    {
        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC,
                           RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC, RIP6_NAME,
                           "RIP6 : RIP6 send datagram, Failure in releasing "
                           "buffer ptr = %p \n", pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p",
                           ERROR_FATAL_STR, ERR_BUF_RELEASE, pBuf);
        }
        return RIP6_FAILURE;
    }

#else

    u4Rip6DataLen =
        CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &gai1Buf, 0, u4Len);

    pCmsgInfo = (struct cmsghdr *) (VOID *) CmsgInfo;
    pCmsgInfo->cmsg_level = IPPROTO_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    /* EVALRIPNG cc below :: changed from sizeof (Cmsginfo) */
    pCmsgInfo->cmsg_len = CMSG_LEN (sizeof (struct in6_pktinfo));

    /* EVALRIPNG cc */
    Iov.iov_base = gai1Buf;
    Iov.iov_len = u4Rip6DataLen;

    Ip6MsgHdr.msg_name = (VOID *) &PeerAddr;
    Ip6MsgHdr.msg_namelen = sizeof (struct sockaddr_in6);
    Ip6MsgHdr.msg_iov = &Iov;
    Ip6MsgHdr.msg_iovlen = 1;
    Ip6MsgHdr.msg_controllen = CMSG_SPACE (sizeof (struct in6_pktinfo));

    /* EVALRIPNG end */
    Ip6MsgHdr.msg_control = (VOID *) &CmsgInfo;

    pIn6Pktinfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&Ip6MsgHdr));

    pIn6Pktinfo->ipi6_ifindex = PeerAddr.sin6_scope_id;

    Ip6AddrCopy (((tIp6Addr *) (VOID *) &pIn6Pktinfo->ipi6_addr.s6_addr),
                 pSrcAddr);

    /* EVALRIPNG :: using a single call */
    if (sendmsg (gi4Rip6SockId, &Ip6MsgHdr, 0) < 0)
    {
        return RIP6_FAILURE;
    }

#endif
    /* Increment out packets for this logical interface */
    ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4OutMessages);
    ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4OutRes);

    /* Reset the Trig upd needed flag */
    if ((u1SendFlag == RIP6_TRIG_UPDATE) ||
        (u1SendFlag == RIP6_PERIODIC_UPDATE))
    {
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag &=
            ~RIP6_TRIGGERED_UPDATE_NEEDED;
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->u1TrigFlag |=
            RIP6_TRIGGERED_UPDATE_NOT_NEEDED;
    }

    if (u1SendFlag == RIP6_TRIG_UPDATE)
    {
        /* Increment triggered updates sent for this logical interface */
        ++(RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->stats.u4TrigUpdates);
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called to check whether we can send triggered update after
 *               processing response message from RIPNG socket.
 *
 * INPUTS      : Instance Id whose routes should be verified to check
 *               for Triggered update. 
 *               
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
INT4
Rip6CheckTrigUpdateSendForResponseMsg (UINT4 u4InstanceId,
                                       UINT1 u1BulkDeletionFlag)
{
    tRip6ScanParam      ScanParams;
    UINT4               u4Count;
    UINT1               u1TrigTimerRunning = 0;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        /* Instance Status is DOWN. Reset the Route Change status for all
         * the route in the instance. */
        ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_TRIG_CLEAR;
        ScanParams.u4InstanceId = u4InstanceId;
        Rip6TrieScan (&ScanParams);
        return RIP6_FAILURE;
    }

    for (u4Count = RIP6_INITIALIZE_ONE; u4Count <= u4MaxIfs; u4Count++)
    {
        if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count) == NULL)
        {
            continue;
        }

        /* Check interfaces on which to send a triggered update */
        if (((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1Status &
              RIP6_IFACE_ALLOCATED) == RIP6_IFACE_ALLOCATED) &&
            ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1Status &
              RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED) &&
            ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1Status &
              RIP6_IFACE_UP) == RIP6_IFACE_UP))
        {
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1TrigFlag
                &= ~RIP6_TRIGGERED_UPDATE_NOT_NEEDED;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1TrigFlag
                |= RIP6_TRIGGERED_UPDATE_NEEDED;

            if (Rip6SendUpdateOrNot (NULL, u4Count, u1BulkDeletionFlag) !=
                RIP6_SUCCESS)
            {
                /*
                 * Set flag telling that a triggered delay timer expiry will
                 * send the trig update and so the route change flag has to be 
                 * preserved till then
                 */
                u1TrigTimerRunning = 1;
            }
        }
    }

    /*
     * Reset the route change flag if the trig delay timer is not running.
     * After the expiry of the trig delay timer the route flag will be reset
     */
    if (u1TrigTimerRunning == RIP6_FALSE)
    {
        ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_TRIG_CLEAR;
        ScanParams.u4InstanceId = u4InstanceId;
        Rip6TrieScan (&ScanParams);
        u1TrigTimerRunning = 0;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Called to check whether we can send triggered update
 *
 * INPUTS      : The flag (u1CheckFlag) telling whether to check 
 *               the sending of the triggered update over the interface 
 *               (u4Index)
 *               
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

INT4
Rip6CheckTrigUpdateSend (UINT1 u1CheckFlag, UINT4 u4Index,
                         tRip6RtEntry * pRtEntry)
{
    tRip6ScanParam      ScanParams;
    UINT4               u4InstanceId;
    UINT4               u4Count;
    UINT1               u1TrigTimerRunning = 0;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        /* Instance Status is DOWN. Reset the Route Change status for all
         * the route in the instance. */
        ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_TRIG_CLEAR;
        ScanParams.u4InstanceId = u4InstanceId;
        Rip6TrieScan (&ScanParams);
        return RIP6_FAILURE;
    }

    for (u4Count = RIP6_INITIALIZE_ONE; u4Count <= u4MaxIfs; u4Count++)
    {
        if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count) == NULL)
        {
            continue;
        }
        if ((u1CheckFlag != RIP6_CHECK_ALL) && (u4Count == u4Index))
        {
            continue;
        }
        /* Check interfaces on which to send a triggered update */
        if (((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1Status &
              RIP6_IFACE_ALLOCATED) == RIP6_IFACE_ALLOCATED) &&
            ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1Status &
              RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED) &&
            ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1Status &
              RIP6_IFACE_UP) == RIP6_IFACE_UP))
        {
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1TrigFlag
                &= ~RIP6_TRIGGERED_UPDATE_NOT_NEEDED;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->u1TrigFlag
                |= RIP6_TRIGGERED_UPDATE_NEEDED;
            if (Rip6SendUpdateOrNot (pRtEntry, u4Count,
                                     (UINT1) ~RIP6_BULK_DELETE) != RIP6_SUCCESS)
            {
                /*
                 * Set flag telling that a triggered delay timer expiry will
                 * send the trig update and so the route change flag has to be 
                 * preserved till then
                 */
                u1TrigTimerRunning = 1;
            }
        }
    }

    /*
     * Reset the route change flag if the trig delay timer is not running.
     * After the expiry of the trig delay timer the route flag will be reset
     */
    if (u1TrigTimerRunning == RIP6_FALSE)
    {
        if (pRtEntry == NULL)
        {
            ScanParams.u1ScanCmd = RIP6_TRIE_SCAN_TRIG_CLEAR;
            ScanParams.u4InstanceId = u4InstanceId;
            Rip6TrieScan (&ScanParams);
        }
        else
        {
            /* Reset the route change flag only for this entry. */
            pRtEntry->u1RtFlag = 0;
        }
        u1TrigTimerRunning = 0;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Sees whether to send triggered update or not on the 
 *               specified interface .If yes then sends it.
 *
 * INPUTS      : The interface (u4Interface) on which to do the check
 *
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS - if it is successful in sending the triggered update
 *                         or a regular update is due shortly.
 *               RIP6_FAILURE - if it is fails in sending the triggered update
 *
 * NOTES       :
 ******************************************************************************/
INT4
Rip6SendUpdateOrNot (tRip6RtEntry * pRtEntry, UINT4 u4Interface,
                     UINT1 u1BulkDeletionFlag)
{
    UINT4               u4Diff, u4CurrentTime;

    tIp6Addr            dstAddr;
    UINT4               u4InstanceId;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4Len;
    UINT1               u1Horizon;
    tRip6RtEntry       *pBestRtEntry;
    VOID               *pRibNode = NULL;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Interface)->u4InstanceId;

    RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                   "RIP6 : Check if trig update to send over If= %d \n",
                   u4Interface);

    OsixGetSysTime (&u4CurrentTime);

    if ((pRtEntry != NULL) && (IS_ADDR_UNSPECIFIED (pRtEntry->dst)))
    {
        if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
            u4DefRouteAdvt == RIP6_FALSE)
        {
            /* Default route advertisement not enabled.So need not
             * send trigerred update for def route over that interface. */
            return RIP6_SUCCESS;
        }
    }

    if ((pRtEntry == NULL) || (pRtEntry->u1Metric != RIP6_INFINITY))
    {
        if ((u1BulkDeletionFlag & RIP6_BULK_DELETE) != RIP6_BULK_DELETE)
        {

            if (u4CurrentTime <=
                RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                        u4Interface)->u4NextRegularUpdate)
            {
                u4Diff =
                    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
                    u4NextRegularUpdate - u4CurrentTime;
            }
            else
            {
                u4Diff = RIP6_GET_DIFF_TIME (u4CurrentTime,
                                             RIP6_GET_INST_IF_ENTRY
                                             (u4InstanceId, u4Interface)->
                                             u4NextRegularUpdate);
            }
            if (u4Diff < RIP6_DELTA_TIME)
            {

                RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                               "RIP6:next regular updte sent at %d current time %d "
                               "So trig updte is not sent\n",
                               RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                       u4Interface)->
                               u4NextRegularUpdate, u4CurrentTime);

                /* Since the regular update is due don't send the triggered update */
                RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
                    u1TrigFlag &= ~RIP6_TRIGGERED_UPDATE_NEEDED;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
                    u1TrigFlag |= RIP6_TRIGGERED_UPDATE_NOT_NEEDED;

                return RIP6_SUCCESS;
            }
            /* If triggered delay is just then don't send a new triggered update now */
            else if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
                      u1TrigFlag & RIP6_TRIGGERED_DELAY_RUNNING) ==
                     RIP6_TRIGGERED_DELAY_RUNNING)
            {

                RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                              "RIP6 : Triggered delay just went So triggered update "
                              "is not sent\n");

                /* 
                 * Since some triggered update went just now so don't send
                 * another soon but set the flag and after the end of this
                 * delay interval send the update
                 */
                RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
                    u1TrigFlag &= ~RIP6_TRIGGERED_UPDATE_NOT_NEEDED;
                RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
                    u1TrigFlag |= RIP6_TRIGGERED_UPDATE_NEEDED;

                return RIP6_FAILURE;
            }
        }

    }
    /* Set the all rip_routers multicast address -- 0xff02::9 */
    SET_ALL_RIP_ROUTERS (dstAddr);
    if (pRtEntry != NULL)
    {

        if (Rip6IsNetFilterEnabled (u4InstanceId, &pRtEntry->dst) ==
            RIP6_SUCCESS)
        {
            /* No need to advt the route */
            return RIP6_SUCCESS;
        }

        if (Rip6ApplyInOutFilter (gpDistributeOutFilterRMap, pRtEntry)
            != RIP6_SUCCESS)
        {
            /* Stop processing this route and go to the next route. */
            return RIP6_SUCCESS;
        }

        /* Get max_number of RTEs that can go in the buffer 
           u4MaxNoRte = MAX_RTE (u4Index); */

        /* Allocate the buffer */
        pBuf = Rip6BufAlloc (1, u4InstanceId);

        if (pBuf == NULL)
        {

            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                          RIP6_NAME,
                          "RIP6 : Send Response, Failed to allocate "
                          " pbuf for forming the packet.\n");

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_ALLOC, RIP6_INITIALIZE_ZERO);

            return RIP6_FAILURE;
        }

        /* Put the RIP6 hdr into buffer */
        if (Rip6PutHdrInBuf (pBuf, RIP6_RESPONSE) == RIP6_FAILURE)
        {
            return RIP6_FAILURE;
        }

        u4Len = RIP6_HDR_LEN;

        u1Horizon = RIP6_GET_HORIZON (u4Interface);

        if (pRtEntry->u1Metric == RIP6_INFINITY)    /* In case of Route Deletion Trig Update */
        {
            pBestRtEntry = pRtEntry;
            Rip6TrieBestEntry (&pRtEntry->dst,
                               RIP6_BEST_METRIC_LOOKUP,
                               pRtEntry->u1Prefixlen,
                               u4InstanceId, &pBestRtEntry, &pRibNode);

            if ((pBestRtEntry != NULL) && (pBestRtEntry != pRtEntry))
            {
                if ((u4Interface == pBestRtEntry->u4Index))
                {
                    if (Rip6PutRteInBuf
                        (pBuf, u4Interface, u1Horizon, RIP6_TRIG_UPDATE,
                         pRtEntry) == RIP6_FAILURE)
                    {
                        return RIP6_FAILURE;
                    }
                }
                else            /* The sending interface is not the one from which the alt. route was learnt */
                {
                    if (Rip6PutRteInBuf
                        (pBuf, u4Interface, u1Horizon, RIP6_TRIG_UPDATE,
                         pBestRtEntry) == RIP6_FAILURE)
                    {
                        return RIP6_FAILURE;
                    }
                }
            }
            else                /* No alternate route found */
            {
                if (Rip6PutRteInBuf
                    (pBuf, u4Interface, u1Horizon, RIP6_TRIG_UPDATE,
                     pRtEntry) == RIP6_FAILURE)
                {
                    return RIP6_FAILURE;
                }
            }
        }
        else
        {
            if (Rip6PutRteInBuf
                (pBuf, u4Interface, u1Horizon, RIP6_TRIG_UPDATE,
                 pRtEntry) == RIP6_FAILURE)
            {
                return RIP6_FAILURE;
            }
        }

        u4Len = u4Len + RIP6_RTE_LEN;
        if ((Rip6SendDatagram ((UINT1) RIP6_TRIG_UPDATE, RIP6_PORT, u4Interface,
                               u4Len, &dstAddr, pBuf)) == RIP6_FAILURE)
        {
            return RIP6_FAILURE;
        }

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 put hdr in buf, Failure in releasing buffer = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
            return RIP6_FAILURE;
        }
    }
    else
    {
        if (Rip6SendResponse (RIP6_TRIG_UPDATE, RIP6_PORT, u4Interface,
                              u4Interface, &dstAddr) == RIP6_FAILURE)
        {

            return RIP6_FAILURE;
        }
    }
    /*
     * Start the triggered delay timer only if the interface is UP and RIPNG 
     * is ENABLED on it
     */
    if (((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
          u1Status & RIP6_IFACE_ALLOCATED) == RIP6_IFACE_ALLOCATED) &&
        ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
          u1Status & RIP6_IFACE_UP) == RIP6_IFACE_UP) &&
        ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
          u1Status & RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED))
    {
        /*
         * Triggered update was sent and the interface was RIPng enabled and 
         * UP,so delay timer started
         */
        if ((pRtEntry != NULL) && (pRtEntry->u1Metric == RIP6_INFINITY))
        {
            if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->
                trigDelayTimer.u1Id != RIP6_INITIALIZE_ZERO)
            {
                Rip6TmrStop (RIP6_TRIG_TIMER_ID,
                             gRip6IntfTimerListId,
                             &(RIP6_DELAY_TIMER_NODE (u4Interface)));
            }
        }

        Rip6IfTimerStart (RIP6_TRIG_TIMER_ID, u4Interface);
    }

    /*
     * Set the flag telling that the triggered delay timer running
     * for this logical interface
     */
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->u1TrigFlag &=
        ~RIP6_TRIGGERED_DELAY_NOT_RUNNING;
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Interface)->u1TrigFlag |=
        RIP6_TRIGGERED_DELAY_RUNNING;

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Checks the validity of the RIPng responses 
 *
 * INPUTS      : u1Hlim  : The Ipv6 Pkt Hop limit
 *               u4Index : The interface on which to do the check
 *               pSrc    : The IPv6 source address 
 *               pDst    : The IPv6 destination address 
 * OUTPUTS     : None
 *
 * RETURNS     : TRUE or FALSE
 *
 * NOTES       :
 ******************************************************************************/
PRIVATE INT4
Rip6IsResponseValid (UINT1 u1Hlim, UINT4 u4Index, tIp6Addr * pSrc,
                     tIp6Addr * pDst)
{
    UINT1               u1Type;
    UINT4               u4InstanceId;
    UINT4               u4IndexRcvd, u4IndexSent = RIP6_INITIALIZE_ZERO;

    u1Type = Ip6AddrType (pSrc);
    UNUSED_PARAM (u1Type);
    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;
    u4IndexRcvd = u4Index;
    /* 
     * Check if the source address is link local 
     */
    if (IS_ADDR_LLOCAL (*pSrc) == RIP6_FALSE)
    {
        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6:sndrOfRIPngRespDidnt putLlocalSrcAddr "
                       "RespCameOnIf=%d SrcAddr=%s DesAddr=%s",
                       u4IndexRcvd, Ip6ErrPrintAddr (ERROR_MINOR, pSrc),
                       Ip6ErrPrintAddr (ERROR_MINOR, pDst));

        return FALSE;
    }
    else
    {

        /* 
         * Place where Task Lock can go in 
         */
        if (Rip6IsOurAddr (pSrc, &u4IndexSent) == RIP6_SUCCESS)
        {
            if (u4InstanceId == RIP6_GET_INST_IF_MAP_ENTRY (u4IndexSent)->
                u4InstanceId)
            {
                RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                               "RIP6:GotRIPngResp Gen by us.Resp came onIf=%d"
                               " SrcAddr=%s DestAddr=%s \n",
                               u4IndexSent, Ip6ErrPrintAddr (ERROR_MINOR, pSrc),
                               Ip6ErrPrintAddr (ERROR_MINOR, pDst));
                return FALSE;
            }
        }
        /* 
         * Place where Task UnLock can go in 
         */
        if (Rip6IsTrustedPeer (u4InstanceId, pSrc) != RIP6_SUCCESS)
        {
            return FALSE;
        }

        /* 
         * Check whether we got a periodic/triggered update from a neighbour 
         */
        if (u1Hlim != IP6_MAX_HLIM)
        {
            if (IS_ADDR_MULTI (*pDst) != RIP6_FALSE)
            {
                RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                              "RIP6 periodic/trig update is not from a"
                              " neighbour.Response came on ");

                RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                               " If = %d Src address = %s Dest Addr = %s \n",
                               u4IndexRcvd, Ip6ErrPrintAddr (ERROR_MINOR, pSrc),
                               Ip6ErrPrintAddr (ERROR_MINOR, pDst));

                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }
        return TRUE;
    }
}

/******************************************************************************
 * DESCRIPTION : Checks the validity of the RIPng RTE in the packet 
 *
 * INPUTS      : pRte  : Pointer to the route entry to be validated. 
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS or RIP6_FAILURE.
 *
 * NOTES       :
 ******************************************************************************/
PRIVATE INT4
Rip6ValidateRte (pRte)
     tRip6Rte           *pRte;
{
    if ((!(IS_ADDR_MULTI (pRte->prefix))) &&
        (!(IS_ADDR_LLOCAL (pRte->prefix))) &&
        (pRte->u1PrefixLen <= IP6_ADDR_MAX_PREFIX) &&
        (pRte->u1Metric >= RIP6_MIN_METRIC) &&
        (pRte->u1Metric <= RIP6_MAX_METRIC))
    {
        return RIP6_SUCCESS;
    }

    RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                  "RIP6 : Got an invalid RTE\n");

    return RIP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Checks whether the  RIPng RTE is the next-hop RTE 
 *
 * INPUTS      : pRip6Rte  : Pointer to the Next hop entry to be validated. 
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS or RIP6_FAILURE.
 *
 * NOTES       :
 ******************************************************************************/
PRIVATE INT4
Rip6IsNextHopRte (pRip6Rte)
     tRip6Rte           *pRip6Rte;
{
    if ((pRip6Rte->u1Metric == RIP6_NEXTHOP_RTE_METRIC) &&
        (pRip6Rte->u2RtTag == RIP6_INITIALIZE_ZERO) &&
        (pRip6Rte->u1PrefixLen == RIP6_INITIALIZE_ZERO))
    {
        /* This is a next hop RTE */
        return RIP6_SUCCESS;
    }

    return RIP6_FAILURE;
}

/******************************************************************************
 * DESCRIPTION : Allocate a buffer of size including u4MaxNoRte RTEs 
 *
 * INPUTS      : u4MaxNoRte: Max number of route entries. 
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : Pointer to Cru buffer on RIP6_SUCCESS or NULL on RIP6_FAILURE.
 *
 * NOTES       :
 ******************************************************************************/
tCRU_BUF_CHAIN_HEADER *
Rip6BufAlloc (u4MaxNoRte, u4InstanceId)
     UINT4               u4MaxNoRte;
     UINT4               u4InstanceId;
{
    UINT4               u4Rip6Size, u4_rip6_woffset;

    /* Determine the offset at which to start its header */
    u4_rip6_woffset = IP6_HDR_LEN + UDP6_HDR_LEN;

    /* The size of data which will be formed by RIP6 module */
    if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes > u4MaxNoRte)
    {
        u4Rip6Size = (UINT4) (RIP6_HDR_LEN + u4MaxNoRte * sizeof (tRip6Rte));
    }
    else
    {
        u4Rip6Size = (UINT4) (RIP6_HDR_LEN +
                              (garip6InstanceDatabase[u4InstanceId]->
                               u4RtTableRoutes) * sizeof (tRip6Rte));
    }

    /* Allocate buffer */
    return (CRU_BUF_Allocate_MsgBufChain
            (u4Rip6Size + u4_rip6_woffset, u4_rip6_woffset));
}

/******************************************************************************
 * DESCRIPTION : Put the RIP6 hdr into the given buffer
 *
 * INPUTS      : pBuf   : Pointer to the message buffer to be filled. 
 *               u1Flag : Command in the rip6 header.
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS or RIP6_FAILURE.
 *
 * NOTES       :
 ******************************************************************************/
INT4
Rip6PutHdrInBuf (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Flag)
{
    UINT1               u1Copy = 1;
    tRip6Hdr           *pRip6Hdr = NULL, rip6Hdr;

    pRip6Hdr = (tRip6Hdr *) & rip6Hdr;
    if (u1Flag == RIP6_RESPONSE)
    {
        pRip6Hdr->u1Command = RIP6_RESPONSE;
    }
    else
    {
        pRip6Hdr->u1Command = RIP6_REQUEST;
    }
    pRip6Hdr->u1Ver = RIP6_VER_1;
    pRip6Hdr->u2Res = 0;

    RIP6_BUF_WRITE_OFFSET (pBuf) = 0;
    if (Rip6BufWrite
        (pBuf, (UINT1 *) pRip6Hdr, RIP6_BUF_WRITE_OFFSET (pBuf),
         sizeof (tRip6Hdr), u1Copy) == RIP6_FAILURE)
    {

        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "RIP6 : RIP6 put hdr in buf,unable to do write onto buffer = %d \n",
                       pBuf);

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_WRITE, pBuf);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 put hdr in buf, Failure in releasing buffer = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }

        return RIP6_FAILURE;
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Put the RTE in  the buffer
 *
 * INPUTS      : pBuf      : Pointer to the message buffer to be filled. 
 *               u4Index   : Interface index on which response is to be sent.
 *               u1Horizon : flag notifying what horizon to be applied.
 *               u2RespFlag: Flag identifying whether it is a final update.
 *               pRtEntry  : Pointer to the route entry. 
 * 
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS or RIP6_FAILURE.
 *
 * NOTES       :
 ******************************************************************************/
INT4
Rip6PutRteInBuf (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4Index, UINT1 u1Horizon,
                 UINT2 u2RespFlag, tRip6RtEntry * pRtEntry)
{

    UINT1               u1Copy = 1;
    tRip6Rte            rip6_rte, *pRip6Rte = NULL;

    /* Get pointer to write to the buffer */
    pRip6Rte = (tRip6Rte *) & rip6_rte;
    Ip6AddrCopy (&pRip6Rte->prefix, &pRtEntry->dst);
    pRip6Rte->u2RtTag = OSIX_HTONS (pRtEntry->u2RtTag);
    pRip6Rte->u1PrefixLen = pRtEntry->u1Prefixlen;
    pRip6Rte->u1Metric = pRtEntry->u1Metric;

    if (u1Horizon == RIP6_POISON_REVERSE)
    {
        if ((pRtEntry->i1Proto == RIPNG) && (pRtEntry->u4Index == u4Index))
        {
            pRip6Rte->u1Metric = RIP6_INFINITY;
        }
    }
    else
    {
        pRip6Rte->u1Metric = pRtEntry->u1Metric;
    }

    /*
     * If this is the final update to go whatever the case the metric is
     * always going to be RIP6_INFINITY because no garbage is run
     */

    if ((u2RespFlag & RIP6_FINAL_UPDATE) == RIP6_FINAL_UPDATE)
    {
        pRip6Rte->u1Metric = RIP6_INFINITY;
    }

    /* Check if the response is for specific request */
    if (u2RespFlag == RIP6_INITIALIZE_ZERO)
    {
        pRip6Rte->u1Metric = pRtEntry->u1Metric;
    }

    /* Put the values into the buffer */
    if (Rip6BufWrite
        (pBuf, (UINT1 *) pRip6Rte, RIP6_BUF_WRITE_OFFSET (pBuf),
         sizeof (tRip6Rte), u1Copy) == RIP6_FAILURE)
    {

        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "RIP6 : RIP6 put rte in buf, unable to do write onto buffer = %p\n",
                       pBuf);

        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                       ERR_BUF_WRITE, pBuf);

        if (CRU_BUF_Release_MsgBufChain (pBuf, FALSE) == RIP6_FAILURE)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_BUFFER_TRC | RIP6_ALL_FAIL_TRC,
                           RIP6_NAME,
                           "RIP6 : RIP6 put rte in buf, Failure in releasing buffer ptr = %p \n",
                           pBuf);

            RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "%s buf err: Type = %d Bufptr = %p", ERROR_FATAL_STR,
                           ERR_BUF_RELEASE, pBuf);
        }

        return RIP6_FAILURE;
    }

    return RIP6_SUCCESS;
}

INT4
Rip6CheckRouteInRbTree (tIp6Addr * pAddr, UINT1 u1PrefixLen)
{
    tRip6RtEntry        RtEntry;
    tRip6RtEntry       *pRtEntry = NULL;

    MEMSET (&RtEntry, 0, sizeof (tRip6RtEntry));
    RtEntry.u1Prefixlen = u1PrefixLen;
    Ip6AddrCopy (&RtEntry.dst, pAddr);

    pRtEntry = (tRip6RtEntry *) RBTreeGet (gRip6RBTree, (tRBElem *) & RtEntry);
    if (pRtEntry != NULL)
    {
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/**************************************************************************/
/* Function Name : Rip6FilterouteSource                                   */
/*                                                                        */
/* Description   : Apply route map, return distance (or default distance) */
/*                 for the route                                          */
/* Inputs        : 1. pSrcAddr - source IPv6 address                      */
/*                                                                        */
/* Outputs       : none                                                   */
/*                                                                        */
/* Return values : Distance for the route                                 */
/**************************************************************************/
UINT1
Rip6FilterRouteSource (tIp6Addr * pSrcAddr)
{
    UINT1               u1Distance = gu1Rip6Distance;

#ifdef ROUTEMAP_WANTED
    if (gpDistanceFilterRMap != NULL
        && gpDistanceFilterRMap->u1Status == FILTERNIG_STAT_ENABLE)
    {
        tRtMapInfo          MapInfo;
        MEMSET (&MapInfo, 0, sizeof (MapInfo));
        IPVX_ADDR_INIT_FROMV6 (MapInfo.SrcXAddr, pSrcAddr);
        if (RMapApplyRule2
            (&MapInfo,
             gpDistanceFilterRMap->au1DistInOutFilterRMapName) ==
            RMAP_ENTRY_MATCHED)
        {
            u1Distance = gpDistanceFilterRMap->u1RMapDistance;
        }
    }
#else
    UNUSED_PARAM (pSrcAddr);
#endif
    return u1Distance;
}
