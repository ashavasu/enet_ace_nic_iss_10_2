/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6mbsm.c,v 1.6 2014/04/10 13:23:04 siva Exp $
 *
 *******************************************************************/
#ifdef MBSM_WANTED
#include "rip6inc.h"

INT4 Rip6NpMbsmInitProtocol PROTO ((tMbsmSlotInfo *));

/*****************************************************************************/
/*                                                                           */
/* Function     : Rip6FwdMbsmUpdateCardStatus                                */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the line card          */
/*                change status to the RIP6 task                             */
/*                                                                           */
/* Input        : pProtoMsg - Contains the slot and port information         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

INT4
Rip6FwdMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, UINT1 u1Cmd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    pBuf = CRU_BUF_Allocate_MsgBufChain ((sizeof (tMbsmProtoMsg)), 0);
    if (pBuf == NULL)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RIP6, "CRU Buffer Allocation Failed\n");
        return MBSM_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pProtoMsg, 0,
                                   sizeof (tMbsmProtoMsg)) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RIP6, "Copy Over CRU Buffer Failed\n");
        return MBSM_FAILURE;
    }

    CRU_BUF_Set_SourceModuleId (pBuf, u1Cmd);

    if (OsixSendToQ (SELF,
                     (const UINT1 *) RIP6_TASK_CONTROL_QUEUE,
                     pBuf, OSIX_MSG_NORMAL) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RIP6, "Send To IP6 Q Failed\n");
        return MBSM_FAILURE;
    }

    OsixSendEvent (SELF, (const UINT1 *) RIP6_TASK_NAME, RIP6_CONTROL_EVENT);

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rip6MbsmUpdateLCStatus                                     */
/*                                                                           */
/* Description  : Initialise the line card, based on the line card status    */
/*                received                                                   */
/*                                                                           */
/* Input        : pBuf     - Buffer containing the protocol message info.    */
/*                u1Cmd    - Line card status (UP/DOWN)                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
Rip6MbsmUpdateLCStatus (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmProtoAckMsg    ProtoAckMsg;

    MEMSET (&ProtoMsg, 0, sizeof (tMbsmProtoMsg));
    MEMSET (&ProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));

    if ((CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
                                    sizeof (tMbsmProtoMsg))) == CFA_FAILURE)
    {
        MBSM_DBG (MBSM_PROTO_TRC, MBSM_RIP6, "Copy From CRU Buffer Failed\n");
        i4RetStatus = MBSM_FAILURE;
    }

    if (i4RetStatus != MBSM_FAILURE)
    {
        pSlotInfo = &(ProtoMsg.MbsmSlotInfo);

        if (u1Cmd == MBSM_MSG_CARD_INSERT)
        {
            if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
            {
                if ((Rip6NpMbsmInitProtocol (pSlotInfo)) != MBSM_SUCCESS)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
            }
        }
        else
        {
            MBSM_DBG (MBSM_PROTO_TRC, MBSM_RIP6,
                      "MBSM_CARD_REMOVE - No Support\n");
            i4RetStatus = MBSM_SUCCESS;
        }
        ProtoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
        ProtoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
    }

    ProtoAckMsg.i4RetStatus = i4RetStatus;

    MbsmSendAckFromProto (&ProtoAckMsg);

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : Rip6NpMbsmInitProtocol                                     */
/*                                                                           */
/* Description  : Initialise the RIP6 protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

INT4
Rip6NpMbsmInitProtocol (tMbsmSlotInfo * pSlotInfo)
{
    if (OSIX_FALSE == MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
    {
        if (Rip6FsNpMbsmRip6Init (pSlotInfo) != FNP_SUCCESS)
        {
            return MBSM_FAILURE;
        }
    }
    return MBSM_SUCCESS;
}
#endif
