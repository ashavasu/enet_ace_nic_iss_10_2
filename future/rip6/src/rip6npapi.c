
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6npapi.c,v 1.1 2014/04/10 13:26:05 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of RIP6 module.
 ******************************************************************************/

#ifndef __RIP6_NPAPI_C__
#define __RIP6_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : Rip6FsNpRip6Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpRip6Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpRip6Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rip6FsNpRip6Init ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_RIP6_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    return (NpUtilHwProgram (&FsHwNp));
}

#ifdef MBSM_WANTED
/***************************************************************************
 *                                                                          
 *    Function Name       : Rip6FsNpMbsmRip6Init                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpMbsmRip6Init
 *                                                                          
 *    Input(s)            : Arguments of FsNpMbsmRip6Init
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
Rip6FsNpMbsmRip6Init (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tIpv6NpModInfo     *pIpv6NpModInfo = NULL;
    tIpv6NpWrFsNpMbsmRip6Init *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IPV6_MOD,    /* Module ID */
                         FS_NP_MBSM_RIP6_INIT,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpv6NpModInfo = &(FsHwNp.Ipv6NpModInfo);
    pEntry = &pIpv6NpModInfo->Ipv6NpFsNpMbsmRip6Init;

    pEntry->pSlotInfo = pSlotInfo;

    return (NpUtilHwProgram (&FsHwNp));
}
#endif /* MBSM_WANTED */
#endif /* __RIP6_NPAPI_C__ */
