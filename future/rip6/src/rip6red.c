/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6red.c,v 1.11 2017/12/05 09:37:48 siva Exp $
 *
 * Description: This file contains RIP6 Redundancy related 
 *              routines
 *           
 *******************************************************************/
#ifndef __RIP6RED_C
#define __RIP6RED_C

#include "rip6inc.h"

#ifdef L2RED_WANTED
/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize rip6 dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedDynDataDescInit (VOID)
{
    tDbDataDescInfo    *pRip6RtDynDataDesc = NULL;

    pRip6RtDynDataDesc = &(gaRip6DynDataDescList[RIP6_RT_DYN_INFO]);

    pRip6RtDynDataDesc->pDbDataHandleFunction = NULL;

    /* Assumption:
     * All dynamic info in data structure are placed after the DbNode.

     * Size of the dynamic info =
     * Total size of structure - offset of first dynamic info
     */

    pRip6RtDynDataDesc->u4DbDataSize =
        sizeof (tRip6RtEntry) - FSAP_OFFSETOF (tRip6RtEntry, dst);

    pRip6RtDynDataDesc->pDbDataOffsetTbl = gaRip6RtOffsetTbl;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Rip6 descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedDescrTblInit (VOID)
{
    tDbDescrParams      Rip6DescrParams;
    MEMSET (&Rip6DescrParams, RIP6_INITIALIZE_ZERO, sizeof (tDbDescrParams));

    Rip6DescrParams.u4ModuleId = RM_RIP6_APP_ID;

    Rip6DescrParams.pDbDataDescList = gaRip6DynDataDescList;

    DbUtilTblInit (&gRip6DynInfoList, &Rip6DescrParams);

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Rip6 route db node.*/
/*                                                                           */
/*    Input(s)            : pRip6DbTblNode - pointer to Rip6 Db Entry.       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedDbNodeInit (tDbTblNode * pDBNode, UINT4 u4Type)
{
    DbUtilNodeInit (pDBNode, u4Type);

    return;
}

/************************************************************************
 *  Function Name   : Rip6RedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the RIP6 module while
 *                    task initialisation. It initialises the redundancy     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
Rip6RedInitGlobalInfo (VOID)
{
    tRmRegParams        RmRegParams;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedInitGlobalInfo\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedInitGlobalInfo"));

    /* Create the RM Packet arrival Q */

    if (OsixQueCrt ((UINT1 *) RIP6_RM_PKT_QUEUE, OSIX_MAX_Q_MSG_LEN,
                    RIP6_RM_QUE_DEPTH, &gRip6RmPktQId) != OSIX_SUCCESS)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "INIT: Queue Creation Failed\n");
        return OSIX_FAILURE;
    }

    MEMSET (&RmRegParams, RIP6_INITIALIZE_ZERO, sizeof (tRmRegParams));

    RmRegParams.u4EntId = RM_RIP6_APP_ID;
    RmRegParams.pFnRcvPkt = Rip6RedRmCallBack;

    /* Registering RIP6 with RM */
    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {

        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedInitGlobalInfo: Registration with RM failed\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedInitGlobalInfo: Registration with RM failed"));
        return OSIX_FAILURE;
    }

    RIP6_GET_NODE_STATUS () = RM_INIT;
    RIP6_NUM_STANDBY_NODES () = 0;
    gRip6RedGblInfo.bBulkReqRcvd = OSIX_FALSE;

    /* BulkReqRcvd is set as FALSE initially. When bulk request is received
     * the flag is set to true. This is checked for sending bulk update
     * message to the standby node */

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedInitGlobalInfo"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedInitGlobalInfo\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : Rip6RedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the RIP6 module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register RIP6 with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
Rip6RedDeInitGlobalInfo (VOID)
{
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedDeInitGlobalInfo\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedDeInitGlobalInfo"));

    /* Deregister from SYSLOG */

    if (gRip6RmPktQId != NULL)
    {
        OsixQueDel (gRip6RmPktQId);
        gRip6RmPktQId = IP_ZERO;
    }

    if (RmDeRegisterProtocols (RM_RIP6_APP_ID) == RM_FAILURE)
    {
        SYS_LOG_DEREGISTER (gu4Rip6SysLogId);

        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedDeInitGlobalInfo:"
                      "De-Registration with RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedDeInitGlobalInfo:"
                      "De-Registration with RM failed"));
        return OSIX_FAILURE;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedDeInitGlobalInfo"));
    SYS_LOG_DEREGISTER (gu4Rip6SysLogId);
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedDeInitGlobalInfo\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to RIP6        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rip6RedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tRip6RmMsg         *pMsg = NULL;
    UINT1               u1MsgProcessFlag = RIP6_PROCESS_ONE_MSG;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedRmCallBack\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedRmCallBack"));

    /* Callback function for RM events. The event and the message is sent as 
     * parameters to this function */

    if ((u1Event != RM_MESSAGE) &&
        (u1Event != GO_ACTIVE) &&
        (u1Event != GO_STANDBY) &&
        (u1Event != RM_STANDBY_UP) &&
        (u1Event != RM_STANDBY_DOWN) &&
        (u1Event != RIP6_INITIATE_BULK_UPDATES) &&
        (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedRmCallBack:This event is not"
                      "associated with RM \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedRmCallBack:This event is not"
                      "associated with RM"));
        return OSIX_FAILURE;
    }

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Queue message associated with the event is not present */
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedRmCallBack: Queue Message associated with the event "
                      "is not sent by RM \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedRmCallBack: Queue Message associated with the "
                      "event is not sent by RM"));
        return OSIX_FAILURE;
    }

    pMsg = (tRip6RmMsg *) MemAllocMemBlk ((tMemPoolId) gRip6RMPoolId);
    if (pMsg == NULL)
    {
        RmApiSetNoLock ();
        Rip6Lock ();
        Rip6RedHandleRmEvents (u1MsgProcessFlag);
        Rip6UnLock ();
        RmApiSetLock ();

        /*It is assumed that memory is available 
         * so allocating again */
        pMsg = (tRip6RmMsg *) MemAllocMemBlk ((tMemPoolId) gRip6RMPoolId);
    }

    if (pMsg != NULL)
    {
        MEMSET (pMsg, 0, sizeof (tRip6RmMsg));

        pMsg->RmCtrlMsg.pData = pData;
        pMsg->RmCtrlMsg.u1Event = u1Event;
        pMsg->RmCtrlMsg.u2DataLen = u2DataLen;

        /* Send the message associated with the 
         * event to RIP6 module in a queue */
        if (OsixQueSend (gRip6RmPktQId,
                         (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
        {
            /* Send queue will not fail, as we processed the queue message
             * and the queue is free.
             * Even then if the queue send fails, it is critical.
             * It is reported to the syslog manager.
             */
            if (MemReleaseMemBlock ((tMemPoolId) gRip6RMPoolId, (UINT1 *) pMsg)
                != RIP6_SUCCESS)
            {
                RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                              "RIP6 : rip6 static add, could not release mem to pool \n");
                RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                               "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                               ERROR_FATAL_STR, ERR_MEM_RELEASE,
                               RIP6_INITIALIZE_ZERO, i4Rip6rtId);
            }
            if (u1Event == RM_MESSAGE)
            {
                /* RM CRU Buffer Memory Release */
                RM_FREE (pData);
            }
            else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
            {
                /* Call RM API to release memory of data of these events */
                Rip6RedRmReleaseMemoryForMsg ((UINT1 *) pData);
            }

            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                          "Rip6RedRmCallBack: Q send failure\r \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                          "Rip6RedRmCallBack: Q send failure"));
            return OSIX_FAILURE;
        }
    }

    /* Post a event to RIP6 to process RM events */
    OsixSendEvent (RIP6_TASK_NODE_ID, (const UINT1 *) RIP6_TASK_NAME,
                   RIP6_RM_PKT_EVENT);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedRmCallBack"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedRmCallBack\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : Rip6RedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the RIP6 module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the RIP6 Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
Rip6RedHandleRmEvents (UINT1 u1MsgProcessFlag)
{
    tRip6RmMsg         *pMsg = NULL;
    tRmNodeInfo        *pData = NULL;
    tRmProtoAck         ProtoAck;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4SeqNum = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedHandleRmEvents\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedHandleRmEvents"));

    MEMSET (&ProtoAck, RIP6_INITIALIZE_ZERO, sizeof (tRmProtoAck));
    MEMSET (&ProtoEvt, RIP6_INITIALIZE_ZERO, sizeof (tRmProtoEvt));

    do
    {
        if (OsixQueRecv (gRip6RmPktQId,
                         (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN,
                         OSIX_NO_WAIT) == OSIX_FAILURE)
        {
            break;
        }

        switch (pMsg->RmCtrlMsg.u1Event)
        {
            case GO_ACTIVE:

                RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                              "Rip6RedHandleRmEvents:Received GO_ACTIVE event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                              "Rip6RedHandleRmEvents: Received GO_ACTIVE"
                              " event"));
                Rip6RedHandleGoActive ();
                break;

            case GO_STANDBY:
                RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                              "Rip6RedHandleRmEvents:Received GO_STANDBY event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                              "Rip6RedHandleRmEvents:Received GO_STANDBY event"));
                Rip6RedHandleGoStandby (pMsg);
                /* pMsg is passed as a argument to GoStandby function and
                 * it is released there. If the transformation is from active 
                 * to standby then there is no need for releasing the mempool
                 * as we are calling DeInit function. This function clears
                 * all the mempools, so it is returned here instead of break.*/
                return;

            case RM_STANDBY_UP:
                RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                              "Rip6RedHandleRmEvents: Received RM_STANDBY_UP"
                              " event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                              "Rip6RedHandleRmEvents: Received RM_STANDBY_UP"
                              " event"));
                /* Standby up event, number of standby node is updated. 
                 * BulkReqRcvd flag is checked, if it is true, then Bulk update
                 * message is sent to the standby node and the flag is reset */

                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                RIP6_NUM_STANDBY_NODES () = pData->u1NumStandby;
                Rip6RedRmReleaseMemoryForMsg ((UINT1 *) pData);

                if (RIP6_RM_BULK_REQ_RCVD () == OSIX_TRUE)
                {
                    RIP6_RM_BULK_REQ_RCVD () = OSIX_FALSE;
                    gRip6RedGblInfo.u1BulkUpdStatus = RIP6_HA_UPD_NOT_STARTED;
                    Rip6RedSendBulkUpdMsg ();
                }
                break;

            case RM_STANDBY_DOWN:
                RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                              "Rip6RedHandleRmEvents: Received RM_STANDBY_DOWN"
                              "event \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                              "Rip6RedHandleRmEvents: Received RM_STANDBY_DOWN"
                              "event"));
                /* Standby down event, number of standby nodes is updated */
                pData = (tRmNodeInfo *) pMsg->RmCtrlMsg.pData;
                RIP6_NUM_STANDBY_NODES () = pData->u1NumStandby;
                Rip6RedRmReleaseMemoryForMsg ((UINT1 *) pData);
                break;

            case RM_MESSAGE:
                /* Read the sequence number from the RM Header */
                RM_PKT_GET_SEQNUM (pMsg->RmCtrlMsg.pData, &u4SeqNum);
                /* Remove the RM Header */
                RM_STRIP_OFF_RM_HDR (pMsg->RmCtrlMsg.pData,
                                     pMsg->RmCtrlMsg.u2DataLen);
                RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                              "Rip6RedHandleRmEvents:"
                              "Received RM_MESSAGE event\r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                              "Rip6RedHandleRmEvents:"
                              "Received RM_MESSAGE event"));
                ProtoAck.u4AppId = RM_RIP6_APP_ID;
                ProtoAck.u4SeqNumber = u4SeqNum;

                if (gRip6RedGblInfo.u1NodeStatus == RM_ACTIVE)
                {
                    /* Process the message at active */
                    Rip6RedProcessPeerMsgAtActive (pMsg->RmCtrlMsg.pData,
                                                   pMsg->RmCtrlMsg.u2DataLen);
                }
                else if (gRip6RedGblInfo.u1NodeStatus == RM_STANDBY)
                {
                    /* Process the message at standby */
                    Rip6RedProcessPeerMsgAtStandby (pMsg->RmCtrlMsg.pData,
                                                    pMsg->RmCtrlMsg.u2DataLen);
                }
                else
                {
                    /* Message is received at the idle node so ignore */
                    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                  "Rip6RedHandleRmEvents: Sync-up message received"
                                  " at Idle Node!!!!\r\n");
                    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                                  "Rip6RedHandleRmEvents: Sync-up message "
                                  "received at Idle Node"));
                }

                RM_FREE (pMsg->RmCtrlMsg.pData);
                RmApiSendProtoAckToRM (&ProtoAck);
                break;

            case RM_CONFIG_RESTORE_COMPLETE:
                /* Config restore complete event is sent by the RM on completing
                 * the static configurations. If the node status is init, 
                 * and the rm state is standby, then the RIP6 status is changed 
                 * from idle to standby */
                if (gRip6RedGblInfo.u1NodeStatus == RM_INIT)
                {
                    if (RIP6_GET_RMNODE_STATUS () == RM_STANDBY)
                    {
                        Rip6RedHandleIdleToStandby ();

                        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

                        RmApiHandleProtocolEvent (&ProtoEvt);
                    }
                }
                break;

            case RIP6_INITIATE_BULK_UPDATES:
                /* RIP6 Initiate bulk update is sent by RM to the standby node 
                 * to send a bulk update request to the active node */
                RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                              "Rip6RedHandleRmEvents: Received "
                              "RIP6_INITIATE_BULK_UPDATES \r\n");
                SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                              "Rip6RedHandleRmEvents: Received "
                              "RIP6_INITIATE_BULK_UPDATES"));
                Rip6RedSendBulkReqMsg ();
                break;

            default:
                RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                              "Rip6RedHandleRmEvents:"
                              "Invalid RM event received\r\n");
                SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                              "Rip6RedHandleRmEvents:"
                              "Invalid RM event received"));
                break;

        }
        if (MemReleaseMemBlock (gRip6RMPoolId, (UINT1 *) pMsg) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                          "RIP6 : rip6 static add, could not release mem to pool \n");
            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                           RIP6_INITIALIZE_ZERO, i4Rip6rtId);
        }
    }
    while (u1MsgProcessFlag);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedHandleRmEvents"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedHandleRmEvents\r \n");
    return;
}

/************************************************************************
 * Function Name      : Rip6RedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the RIP6 upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
Rip6RedHandleGoActive (VOID)
{
    tRmProtoEvt         ProtoEvt;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedHandleGoActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedHandleGoActive"));

    ProtoEvt.u4AppId = RM_RIP6_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to not started. */
    gRip6RedGblInfo.u1BulkUpdStatus = RIP6_HA_UPD_NOT_STARTED;

    if (RIP6_GET_NODE_STATUS () == RM_ACTIVE)
    {
        /* Go active received by the active node, so ignore */
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedHandleGoActive: GO_ACTIVE event reached when node "
                      "is already active \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedHandleGoActive: GO_ACTIVE event reached when "
                      "node is already active"));
        return;
    }
    else if (RIP6_GET_NODE_STATUS () == RM_INIT)
    {
        /* Go active received by idle node, so state changed to active */
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "Rip6RedHandleGoActive: Idle to Active transition...\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedHandleGoActive: Idle to Active transition"));

        Rip6RedHandleIdleToActive ();
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (RIP6_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go active received by standby node, 
         * Do hardware audit, and start the timers for all the arp entries 
         * and change the state to active */
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "Rip6RedHandleGoActive: Standby to Active transition.\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedHandleGoActive: Standby to Active transition"));
        Rip6RedHandleStandbyToActive ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
    }
    if (RIP6_RM_BULK_REQ_RCVD () == OSIX_TRUE)
    {
        /* If bulk update req received flag is true, send the bulk updates */
        RIP6_RM_BULK_REQ_RCVD () = OSIX_FALSE;
        gRip6RedGblInfo.u1BulkUpdStatus = RIP6_HA_UPD_NOT_STARTED;
        Rip6RedSendBulkUpdMsg ();
    }
    RmApiHandleProtocolEvent (&ProtoEvt);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedHandleGoActive"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedHandleGoActive\r \n");
    return;
}

/************************************************************************
 * Function Name      : Rip6RedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the RIP6 upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
Rip6RedHandleGoStandby (tRip6RmMsg * pMsg)
{
    tRmProtoEvt         ProtoEvt;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedHandleGoStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedHandleGoStandby"));

    ProtoEvt.u4AppId = RM_RIP6_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    /* Bulk update status is set to Update not started */
    if (MemReleaseMemBlock (gRip6RMPoolId, (UINT1 *) pMsg) != RIP6_SUCCESS)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "RIP6 : rip6 static add, could not release mem to pool \n");
        RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                       "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                       ERROR_FATAL_STR, ERR_MEM_RELEASE,
                       RIP6_INITIALIZE_ZERO, i4Rip6rtId);
    }
    gRip6RedGblInfo.u1BulkUpdStatus = RIP6_HA_UPD_NOT_STARTED;

    if (RIP6_GET_NODE_STATUS () == RM_STANDBY)
    {
        /* Go standby received by standby node. So ignore */
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "Rip6RedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already in standby \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already in standby"));
        return;
    }
    else if (RIP6_GET_NODE_STATUS () == RM_INIT)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "Rip6RedHandleGoStandby: GO_STANDBY event reached when node "
                      "is already idle \r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedHandleGoStandby: GO_STANDBY event reached when "
                      "node is already idle"));

        /* GO_STANDBY event is not processed here. It is done when
         * CONFIG_RESTORE_COMPLETE event is received. Since static bulk
         * update will be completed only by then, the acknowledgement can be
         * sent during CONFIG_RESTORE_COMPLETE handling, which will trigger RM
         * to send dynamic bulk update event to modules.
         */

        return;
    }
    else
    {
        /* Go standby received by active event. Clear all the dynamic data and
         * populate again by bulk updates */
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "Rip6RedHandleGoStandby: Active to Standby transition..\r\n");
        SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedHandleGoStandby: Active to Standby transition"));
        Rip6RedHandleActiveToStandby ();
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedHandleGoStandby"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedHandleGoStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedHandleIdleToActive (VOID)
{
    UINT4               u4InstId = 0;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedHandleIdleToActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedHandleIdleToActive"));

    /* Node status is set to active and the number of standby nodes 
     * are updated */
    RIP6_GET_NODE_STATUS () = RM_ACTIVE;
    RIP6_RM_GET_NUM_STANDBY_NODES_UP ();

    Rip6TmrStart (RIP6_Regular_TIMER_ID, gRip6TimerListId,
                  &(garip6InstanceDatabase[u4InstId]->Rip6RtGlobal.timer),
                  RIP6_INITIALIZE_TWENTYONE);

    Rip6TmrStart (RIP6_1S_TIMER_ID, gRip6RedisTmrListId,
                  &(garip6InstanceDatabase[u4InstId]->Rip6RrdGlobal.timer),
                  RIP6_INITIALIZE_ONE);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedHandleIdleToActive"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedHandleIdleToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedHandleIdleToStandby (VOID)
{
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedHandleIdleToStandby\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedHandleIdleToStandby"));

    /* the node status is set to standby and no of standby nodes is set to 0 */
    RIP6_GET_NODE_STATUS () = RM_STANDBY;
    RIP6_NUM_STANDBY_NODES () = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Rip6RedHandleIdleToStandby: "
                  "Node Status Idle to Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Rip6RedHandleIdleToStandby: "
                  "Node Status Idle to Standby"));

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedHandleIdleToStandby"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedHandleIdleToStandby\r \n");

    return;
}

/************************************************************************/
/* Function Name      : Rip6RedStartTimers                                 */
/*                                                                      */
/* Description        : This will start all the necessary timers        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP6_SUCCESS/RIP6_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rip6RedStartTimers ()
{
    tRip6RtEntry       *pRtEntry = NULL;
    tRip6RtEntry       *pNextRtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    tIp6Addr            dstAddr, null_addr;
    UINT4               u4MaxIfs = RIP6_INITIALIZE_ZERO;
    UINT4               u4MaxCxts = RIP6_INITIALIZE_ZERO;
    UINT4               u4Index = RIP6_INITIALIZE_ZERO;
    UINT4               u4InstanceId = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedStartTimers\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedStartTimers"));

    u4MaxCxts = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                          FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                          u4PreAllocatedUnits);

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    /* Scan through all instance & start the corresponding 
     * interface timers & route timers */
    for (u4InstanceId = RIP6_INITIALIZE_ZERO;
         u4InstanceId < u4MaxCxts; u4InstanceId++)
    {
        if (garip6InstanceDatabase[u4InstanceId] == NULL)
        {
            continue;
        }

        if (garip6InstanceStatus[u4InstanceId] == RIP6_INST_UP)
        {
            Rip6TmrStart (RIP6_Regular_TIMER_ID, gRip6TimerListId,
                          &(garip6InstanceDatabase[u4InstanceId]->Rip6RtGlobal.
                            timer), RIP6_INITIALIZE_TWENTYONE);

            Rip6TmrStart (RIP6_1S_TIMER_ID, gRip6RedisTmrListId,
                          &(garip6InstanceDatabase[u4InstanceId]->Rip6RrdGlobal.
                            timer), RIP6_INITIALIZE_ONE);
        }
        /*Scan through all RIP interfaces & 
         * start the respective timers */
        for (u4Index = RIP6_INITIALIZE_ZERO; u4Index <= u4MaxIfs; u4Index++)
        {
            if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index) == NULL)
            {
                continue;
            }
            /*Check whether the interface is RIP enabled */
            if (((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                  u1Status & RIP6_IFACE_ALLOCATED) == RIP6_IFACE_ALLOCATED)
                &&
                ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                  u1Status & RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED)
                &&
                ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                  u1Status & RIP6_IFACE_UP) == RIP6_IFACE_UP))
            {
                RIP6_TRC_ARG2 (RIP6_MOD_TRC,
                               RIP6_DATA_PATH_TRC, RIP6_NAME,
                               "RIP6 : Starting Update timer on If =%dDuration=%d \n",
                               u4Index,
                               garip6InstanceDatabase[u4InstanceId]->
                               arip6Profile[RIP6_GET_INST_IF_ENTRY
                                            (u4InstanceId,
                                             u4Index)->u2ProfIndex]->
                               u4PeriodicUpdTime);

                (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs)++;

                if (garip6InstanceDatabase[u4InstanceId]->u4Rip6EnabledIfs ==
                    RIP6_INITIALIZE_ONE)
                {
                    SET_ADDR_UNSPECIFIED (null_addr);
                    UNUSED_PARAM (null_addr);
                }

                /*Send Initial RIP request */
                Rip6SendRequest (u4Index);

                if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes !=
                    RIP6_INITIALIZE_ZERO)
                {
                    SET_ALL_RIP_ROUTERS (dstAddr);
                    MEMSET (&RIP6_GET_IF_ENTRY (u4Index)->
                            PeriodicThrotInitPrefix, RIP6_INITIALIZE_ZERO,
                            sizeof (tIp6Addr));
                    RIP6_GET_IF_ENTRY (u4Index)->u1ThrotInitPrefixLen =
                        RIP6_INITIALIZE_ZERO;
                    RIP6_GET_IF_ENTRY (u4Index)->u1ThrotFlag =
                        RIP6_THROT_NOT_IN_PROGRESS;
                    /*Send periodic update */
                    Rip6SendResponse (RIP6_PERIODIC_UPDATE,
                                      RIP6_PORT, u4Index, u4Index, &dstAddr);
                }
                /*Start the update timer */
                Rip6IfTimerStart (RIP6_UPDATE_TIMER_ID, u4Index);
                Rip6IfTimerStart (RIP6_TRIG_TIMER_ID, u4Index);
            }
        }
        /* Scan through all the routes & start the Route rimers */
        Rip6TrieGetFirstEntry (&pNextRtEntry, &pRibNode, u4InstanceId);

        while ((pRtEntry = pNextRtEntry) != NULL)
        {
            Rip6TrieGetNextEntry (&pNextRtEntry, pRibNode,
                                  &pNextRibNode, u4InstanceId);

            while (pRtEntry)
            {
                if (pRtEntry->u1Metric < RIP6_INFINITY)
                {
                    /* Start Age out timer */
                    Rip6TimerStart (pRtEntry, RIP6_AGE_OUT_TIMER_ID,
                                    RIP6_ROUTE_AGE_INTERVAL (pRtEntry));
                }
                else
                {
                    /* Start the garbage collection timer */
                    Rip6TimerStart (pRtEntry, RIP6_GARB_COLLECT_TIMER_ID,
                                    RIP6_GARBAGE_COLLECT_INTERVAL (pRtEntry->
                                                                   u4Index));
                }

                pRtEntry = pRtEntry->pNext;
            }

            pRibNode = pNextRibNode;
            pNextRibNode = NULL;
        }
    }

    return RIP6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedNotifyRestartRTM                         */
/*                                                                      */
/* Description        : This function will notify RTM to mark all RIP6  */
/*                      routes as STALE. It will send an event to RTM   */
/*                      for this. On receiving the event, RTM will      */
/*                      mark all the routes as stale and start a timer  */
/*                      for Timeout value. On the expiry of this timer  */
/*                      if there are any stale routes still present     */
/*                      those routes are deleted from the RTM and also  */
/*                      from the hardware.                              */
/*                                                                      */
/* Input(s)           : u4CxtId - Context ID                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP6_SUCCESS/RIP6_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rip6RedNotifyRestartRTM (UINT4 u4CxtId)
{
    tOsixMsg           *pRtm6Msg = NULL;
    tRtm6MsgHdr        *pRtm6MsgHdr = NULL;
    UINT4               u4RestartTime = RIP6_DFLT_ROUTE_AGE;

    /* Notifies to RTMv4 about RIP6 restart with restart interval */
    if ((pRtm6Msg = CRU_BUF_Allocate_MsgBufChain
         ((sizeof (tRtm6MsgHdr) + sizeof (UINT4)), 0)) == NULL)

    {
        return RIP6_FAILURE;
    }
    MEMSET (pRtm6Msg->ModuleData.au1ModuleInfo, 0, CRU_BUF_NAME_LEN);
    CRU_BUF_UPDATE_MODULE_INFO (pRtm6Msg, "Rip6NtfyRTM");
    pRtm6MsgHdr = (tRtm6MsgHdr *) IP6_GET_MODULE_DATA_PTR (pRtm6Msg);

    /* fill the message header */
    pRtm6MsgHdr->u1MessageType = RTM6_GR_NOTIFY_MSG;
    pRtm6MsgHdr->RegnId.u2ProtoId = (UINT2) RIPNG_ID;
    pRtm6MsgHdr->RegnId.u4ContextId = u4CxtId;
    pRtm6MsgHdr->u2MsgLen = sizeof (UINT2) + sizeof (UINT4);

    if (IP6_COPY_TO_BUF (pRtm6Msg, &u4RestartTime,
                         RIP6_INITIALIZE_ZERO,
                         sizeof (UINT4)) == IP_BUF_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pRtm6Msg, FALSE);
        return RIP6_FAILURE;
    }

    /* Send the buffer to RTM */
    if (RpsEnqueuePktToRtm6 (pRtm6Msg) != IP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pRtm6Msg, 0);
        return RIP6_FAILURE;
    }
    return RIP6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedHandleStandbyToActive (VOID)
{
    UINT4               u4MaxCxts = RIP6_INITIALIZE_ZERO;
    UINT4               u4InstanceId = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedHandleStandbyToActive\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedHandleStandbyToActive"));

    RIP6_GET_NODE_STATUS () = RM_ACTIVE;
    RIP6_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Timers to be started, check the dynamic arp entry. 
     * If in pending/ageout state, then start the arp age out 
     * timer and send the arp request. If in dynamic state,
     * start the arp cache timer. */

    u4MaxCxts = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                          FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                          u4PreAllocatedUnits);

    /* Scan through the TRIE instances to find the corresponding routes and
     ** notify the RTM. */
    for (u4InstanceId = RIP6_INITIALIZE_ZERO; u4InstanceId < u4MaxCxts;
         u4InstanceId++)
    {
        if (garip6InstanceDatabase[u4InstanceId] == NULL)
        {
            continue;
        }
        if (Rip6RedNotifyRestartRTM (u4InstanceId) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG1 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                           "Failed to notify RTM for instance-%d \r \n",
                           u4InstanceId);

        }
    }

    OsixSendEvent (RIP6_TASK_NODE_ID, (const UINT1 *) RIP6_TASK_NAME,
                   RIP6_RM_START_TIMER_EVENT);

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Rip6RedHandleStandbyToActive: Node Status Standby to "
                  "Active\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Rip6RedHandleStandbyToActive:"
                  " Node Status Standby to Active"));
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedHandleStandbyToActive"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedHandleStandbyToActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedHandleActiveToStandby (VOID)
{
    UINT4               u4MaxCxts = RIP6_INITIALIZE_ZERO;
    UINT4               u4MaxIfs = RIP6_INITIALIZE_ZERO;
    UINT4               u4Index = RIP6_INITIALIZE_ZERO;
    UINT4               u4InstanceId = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedHandleActiveToStandby\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedHandleActiveToStandby"));

    /*update the statistics */
    RIP6_GET_NODE_STATUS () = RM_STANDBY;
    RIP6_RM_GET_NUM_STANDBY_NODES_UP ();

    /* Stop the running timers in the active node. Check whether any timers are 
     * running, if running, stop those timers. If there are any messages in the 
     * queue, discard those messages. Delete all the dynamically learnt entries
     * and flush those memories. These entries will be learnt from the new
     * active node through bulk updated */

    u4MaxCxts = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                          FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                          u4PreAllocatedUnits);

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    /* Scan through the TRIE instances to find the corresponding routes and
     ** delete the routes. */
    for (u4InstanceId = RIP6_INITIALIZE_ZERO; u4InstanceId < u4MaxCxts;
         u4InstanceId++)
    {
        if (garip6InstanceDatabase[u4InstanceId] == NULL)
        {
            continue;
        }

        /* Clear the interface statistics */
        for (u4Index = RIP6_INITIALIZE_ZERO; u4Index <= u4MaxIfs; u4Index++)
        {
            if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index) == NULL)
            {
                continue;
            }

            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4InMessages = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4InReq = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4InRes = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4UnknownCmds = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4OtherVer = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4Discards = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4OutMessages = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4OutReq = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4OutRes = RIP6_INITIALIZE_ZERO;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Index)->
                stats.u4TrigUpdates = RIP6_INITIALIZE_ZERO;
        }

        Rip6MIPurgeAll (u4InstanceId);
    }

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Rip6RedHandleActiveToStandby: Node Status Active to "
                  "Standby\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Rip6RedHandleActiveToStandby: "
                  "Node Status Active to Standby"));

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedHandleActiveToStandby"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedHandleActiveToStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = RIP6_INITIALIZE_ZERO;
    UINT2               u2Length = RIP6_INITIALIZE_ZERO;
    UINT1               u1MsgType = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedProcessPeerMsgAtActive\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedProcessPeerMsgAtActive"));
    ProtoEvt.u4AppId = RM_RIP6_APP_ID;

    RIP6_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);
    RIP6_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

    if (u4OffSet != u2DataLen)
    {
        /* Currently, the only RM packet expected to be processed at
         * active node is Bulk Request message which has only Type and
         * length. Hence this validation is done.
         */
        ProtoEvt.u4Error = RM_PROCESS_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u2Length != RIP6_RED_BULK_REQ_MSG_SIZE)
    {
        ProtoEvt.u4Error = RM_PROCESS_FAIL;

        RmApiHandleProtocolEvent (&ProtoEvt);
    }

    if (u1MsgType == RIP6_RED_BULK_REQ_MESSAGE)
    {
        gRip6RedGblInfo.u1BulkUpdStatus = RIP6_HA_UPD_NOT_STARTED;
        if (!RIP6_IS_STANDBY_UP ())
        {
            /* This is a special case, where bulk request msg from
             * standby is coming before RM_STANDBY_UP. So no need to
             * process the bulk request now. Bulk updates will be send
             * on RM_STANDBY_UP event.
             */
            gRip6RedGblInfo.bBulkReqRcvd = OSIX_TRUE;

            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                          "Rip6RedProcessPeerMsgAtActive:Bulk request message "
                          "before RM_STANDBY_UP\r \n");
            SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                          "Rip6RedProcessPeerMsgAtActive:Bulk request message "
                          "before RM_STANDBY_UP"));
            return;
        }
        Rip6RedSendBulkUpdMsg ();
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedProcessPeerMsgAtActive"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedProcessPeerMsgAtActive\r \n");
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = RIP6_INITIALIZE_ZERO;
    UINT2               u2Length = RIP6_INITIALIZE_ZERO;
    UINT2               u2ExtractMsgLen = RIP6_INITIALIZE_ZERO;
    UINT2               u2MinLen = RIP6_INITIALIZE_ZERO;
    UINT1               u1MsgType = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedProcessPeerMsgAtStandby\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedProcessPeerMsgAtStandby"));
    ProtoEvt.u4AppId = RM_RIP6_APP_ID;
    u2MinLen = RIP6_RED_TYPE_FIELD_SIZE + RIP6_RED_LEN_FIELD_SIZE;

    while (u4OffSet <= u2DataLen)
    {
        u2ExtractMsgLen = (UINT2) u4OffSet;
        RIP6_RM_GET_1_BYTE (pMsg, &u4OffSet, u1MsgType);

        RIP6_RM_GET_2_BYTE (pMsg, &u4OffSet, u2Length);

        if (u2Length < u2MinLen)
        {
            /* The Length field in the RM packet is less than minimum
             * number of bytes, which is MessageType + Length.
             */
            u4OffSet += u2Length;
            continue;
        }
        /* If extracting information upto the length present in the length
         * field goes beyond the total length, means length is not corrent
         * discard the remaining information.
         */
        u2ExtractMsgLen = (UINT2) (u2ExtractMsgLen + u2Length);

        if (u2ExtractMsgLen > u2DataLen)
        {
            /* If there is a length mismatch between the message and the given 
             * length, ignore the message and send error to RM */
            ProtoEvt.u4Error = RM_PROCESS_FAIL;

            RmApiHandleProtocolEvent (&ProtoEvt);
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                          "Rip6RedProcessPeerMsgAtStandby: "
                          "RM_PROCESS Failure\r \n");
            SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                          "Rip6RedProcessPeerMsgAtStandby: "
                          "RM_PROCESS Failure"));
            return;
        }

        switch (u1MsgType)
        {
            case RIP6_RED_BULK_UPD_TAIL_MESSAGE:
                Rip6RedProcessBulkTailMsg (pMsg, &u4OffSet);
                break;
            case RIP6_RT_DYN_INFO:
                Rip6RedProcessDynamicRtInfo (pMsg, &u4OffSet);
                break;
            default:
                break;
        }
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedProcessPeerMsgAtStandby"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedProcessPeerMsgAtStandby\r \n");
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the RIP6 module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rip6RedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedRmReleaseMemoryForMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedRmReleaseMemoryForMsg"));
    if (RmReleaseMemoryForMsg (pu1Block) == OSIX_FAILURE)
    {

        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedRmReleaseMemoryForMsg:Failure in releasing allocated"
                      " memory\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedRmReleaseMemoryForMsg:Failure in releasing "
                      "allocated memory"));
        return OSIX_FAILURE;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedRmReleaseMemoryForMsg"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedRmReleaseMemoryForMsg\r \n");

    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rip6RedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UINT4               u4RetVal = RIP6_INITIALIZE_ZERO;
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedSendMsgToRm\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedSendMsgToRm"));
    u4RetVal = RmEnqMsgToRmFromAppl (pMsg, u2Length,
                                     RM_RIP6_APP_ID, RM_RIP6_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        RM_FREE (pMsg);
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "Rip6RedSendMsgToRm:Freememory due to message send "
                      "failure\r \n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedSendMsgToRm:Freememory due to message send"
                      " failure"));
        return OSIX_FAILURE;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedSendMsgToRm"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedSendMsgToRm\r \n");
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedSendBulkReqMsg (VOID)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg             *pMsg = NULL;
    UINT4               u4OffSet = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedSendBulkReqMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedSendBulkReqMsg"));

    ProtoEvt.u4AppId = RM_RIP6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /*
     *    RIP6 Bulk Request message
     *
     *    <- 1 byte ->|<- 2 bytes->|
     *    --------------------------
     *    | Msg. Type |  Length    |
     *    |           |            |
     *    |-------------------------
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RIP6_RED_BULK_REQ_MSG_SIZE)) == NULL)
    {
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedSendBulkReqMsg: RM Memory allocation failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedSendBulkReqMsg: RM Memory allocation failed"));
        return;
    }

    RIP6_RM_PUT_1_BYTE (pMsg, &u4OffSet, RIP6_RED_BULK_REQ_MESSAGE);
    RIP6_RM_PUT_2_BYTE (pMsg, &u4OffSet, RIP6_RED_BULK_REQ_MSG_SIZE);

    if (Rip6RedSendMsgToRm (pMsg, (UINT2) u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedSendBulkReqMsg: Send message to RM failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedSendBulkReqMsg: Send message to RM failed"));
        return;
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedSendBulkReqMsg"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedSendBulkReqMsg\r \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pRip6DataDesc - This is Rip6 sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pRip6DbNode - This is db node defined in the RIP6*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedDbUtilAddTblNode (tDbTblDescriptor * pRip6DataDesc,
                         tDbTblNode * pRip6DbNode)
{
    if ((RIP6_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gRip6RedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }
    DbUtilAddTblNode (pRip6DataDesc, pRip6DbNode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate RIP6 dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedSyncDynInfo (VOID)
{
    if ((RIP6_IS_STANDBY_UP () == OSIX_FALSE) ||
        (gRip6RedGblInfo.u1NodeStatus != RM_ACTIVE))
    {
        return;
    }
    DbUtilSyncModuleNodes (&gRip6DynInfoList);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedAddAllRouteNodeInDbTbl                    */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedAddAllRouteNodeInDbTbl (VOID)
{
    tRip6RtMarker      *pRip6RtMarker = NULL;
    tInputParams        Inparams;
    tRip6RtEntry       *pRtEntry = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4RtCount = RIP6_INITIALIZE_ZERO;
    UINT4               u4InstanceId = RIP6_INITIALIZE_ZERO;
    UINT4               u4IfIndex = RIP6_INITIALIZE_ZERO;
    UINT1               u1Key[RIP6_IP6ADDR_LEN];
    tIp6Addr            AddrMask;
    tIp6Addr            TempAddr;
    UINT4               u4MaxCxts = RIP6_INITIALIZE_ZERO;
    INT4                i4OutCome = TRIE_SUCCESS;

    MEMSET ((UINT1 *) &AddrMask, IP6_UINT1_ALL_ONE, sizeof (tIp6Addr));
    MEMSET ((UINT1 *) &TempAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Inparams, 0, sizeof (tInputParams));

    pRip6RtMarker = &(gRip6RedRtMarker);

    gRip6RedGblInfo.u1BulkUpdStatus = RIP6_HA_UPD_IN_PROGRESS;

    u4MaxCxts = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                          FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                          u4PreAllocatedUnits);

    /* Scan through the TRIE instances to find the corresponding routes and
     ** add the RIPng routes to DB list. */
    for (u4InstanceId = pRip6RtMarker->u4InstanceId; u4InstanceId < u4MaxCxts;
         u4InstanceId++)
    {
        if ((garip6InstanceDatabase[u4InstanceId] == NULL) ||
            (garip6InstanceDatabase[u4InstanceId]->prip6RtTable == NULL))
        {
            continue;
        }

        if ((pRip6RtMarker->u1PrefixLen == RIP6_INITIALIZE_ZERO) &&
            (!(MEMCMP (&(pRip6RtMarker->DestAddr), &TempAddr,
                       sizeof (tIp6Addr)))))
        {
            if (Rip6TrieGetFirstEntry (&pRtEntry,
                                       &pRibNode, u4InstanceId) != RIP6_SUCCESS)
            {
                continue;
            }

            if (pRtEntry != NULL)
            {
                if (u4RtCount < MAX_RIP6_SYNCUP_ROUTES)
                {
                    Rip6RedDbUtilAddTblNode (&gRip6DynInfoList,
                                             &pRtEntry->RouteDbNode);

                    u4RtCount = u4RtCount + 1;
                    pRip6RtMarker->u4IfIndex = pRtEntry->u4Index;
                    pRip6RtMarker->u4InstanceId = u4InstanceId;
                    MEMCPY (&(pRip6RtMarker->DestAddr), &(pRtEntry->dst),
                            sizeof (tIp6Addr));
                    pRip6RtMarker->u1PrefixLen = pRtEntry->u1Prefixlen;
                }
                else
                {
                    Rip6RedSyncDynInfo ();
                    OsixSendEvent (RIP6_TASK_NODE_ID,
                                   (const UINT1 *) RIP6_TASK_NAME,
                                   RIP6_RM_PEND_RT_SYNC_EVENT);
                    return;
                }
            }
        }

        Inparams.pRoot = garip6InstanceDatabase[u4InstanceId]->prip6RtTable;
        Inparams.i1AppId = (RIPNG_ID - 1);
        Inparams.u1PrefixLen = pRip6RtMarker->u1PrefixLen;
        Inparams.pLeafNode = NULL;
        Inparams.Key.pKey = u1Key;
        MEMSET (Inparams.Key.pKey, RIP6_INITIALIZE_ZERO, RIP6_IP6ADDR_LEN);
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) Inparams.Key.pKey,
                         &(pRip6RtMarker->DestAddr),
                         (INT4) pRip6RtMarker->u1PrefixLen);
        Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (Inparams.Key.pKey +
                                                IP6_ADDR_SIZE), &AddrMask,
                         (INT4) pRip6RtMarker->u1PrefixLen);

        i4OutCome = TRIE_SUCCESS;
        while (i4OutCome == TRIE_SUCCESS)
        {
            i4OutCome = TrieGetNextNode (&Inparams, pRibNode,
                                         (VOID *) &pRtEntry, &pRibNode);
            if (i4OutCome == TRIE_FAILURE)
            {
                pRibNode = NULL;
                break;
            }

            pRtEntry = (tRip6RtEntry *) pRtEntry;
            if (pRtEntry != NULL)
            {
                if (u4RtCount < MAX_RIP6_SYNCUP_ROUTES)
                {
                    Rip6RedDbUtilAddTblNode (&gRip6DynInfoList,
                                             &pRtEntry->RouteDbNode);

                    u4RtCount = u4RtCount + 1;
                    pRip6RtMarker->u4InstanceId = u4InstanceId;
                    pRip6RtMarker->u4IfIndex = u4IfIndex;
                    MEMCPY (&(pRip6RtMarker->DestAddr), &(pRtEntry->dst),
                            sizeof (tIp6Addr));
                    pRip6RtMarker->u1PrefixLen = pRtEntry->u1Prefixlen;
                }
                else
                {
                    Rip6RedSyncDynInfo ();
                    OsixSendEvent (RIP6_TASK_NODE_ID,
                                   (const UINT1 *) RIP6_TASK_NAME,
                                   RIP6_RM_PEND_RT_SYNC_EVENT);
                    return;
                }
                Inparams.u1PrefixLen = pRtEntry->u1Prefixlen;
                Ip6CopyAddrBits ((tIp6Addr *) (VOID *) u1Key, &(pRtEntry->dst),
                                 (INT4) pRtEntry->u1Prefixlen);
                Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (u1Key + IP6_ADDR_SIZE),
                                 &AddrMask, (INT4) pRtEntry->u1Prefixlen);
                Inparams.Key.pKey = u1Key;
                Inparams.pLeafNode = NULL;
            }
        }
    }

    if (u4RtCount != RIP6_INITIALIZE_ZERO)
    {
#ifdef ISS_TEST_WANTED
        if (gi4Rip6BulkUpdTestStatus == RIP6_INITIALIZE_TWO)
        {
            /* CRASHING for HA test purpose */
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                          "Rip6RedAddAllRouteNodeInDbTbl:EXE Crashing \r\n");
        }
#endif
        Rip6RedSyncDynInfo ();
    }

    gRip6RedGblInfo.u1BulkUpdStatus = RIP6_HA_UPD_COMPLETED;
    Rip6RedSendBulkUpdTailMsg ();

    return;
}

/************************************************************************/
/* Function Name      : Rip6RedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedSendBulkUpdMsg (VOID)
{
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedSendBulkUpdMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedSendBulkUpdMsg"));

    if (RIP6_IS_STANDBY_UP () == OSIX_FALSE)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedSendBulkUpMsg:RIP6 Standby up failure \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedSendBulkUpMsg:RIP6 stand by up failure"));
        return;
    }

    if (gRip6RedGblInfo.u1BulkUpdStatus == RIP6_HA_UPD_NOT_STARTED)
    {
        /* If bulk update is not started, reset the marker and set the status
         * to in progress */
        MEMSET (&gRip6RedRtMarker, 0, sizeof (tRip6RtMarker));
        Rip6RedAddAllRouteNodeInDbTbl ();
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedSendBulkUpdMsg"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedSendBulkUpdMsg\r \n");
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the RIP6 offers an IP address         */
/*                      to the RIP6 client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pRip6BindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
Rip6RedSendBulkUpdTailMsg (VOID)
{
    tRmMsg             *pMsg = NULL;
    tRmProtoEvt         ProtoEvt;
    UINT4               u4OffSet = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedSendBulkUpdTailMsg\r \n");

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedSendBulkUpdTailMsg"));
    ProtoEvt.u4AppId = RM_RIP6_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;

    /* Form a bulk update tail message.

     *        <------------1 Byte----------><---2 Byte--->
     *****************************************************
     *        *                             *            *
     * RM Hdr * RIP6_RED_BULK_UPD_TAIL_MSG * Msg Length *
     *        *                             *            *
     *****************************************************

     * The RM Hdr shall be included by RM.
     */

    if ((pMsg = RM_ALLOC_TX_BUF (RIP6_RED_BULK_UPD_TAIL_MSG_SIZE)) == NULL)
    {
        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "Rip6RedSendBulkUpdTailMsg: RM Memory allocation"
                      " failed\r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedSendBulkUpdTailMsg: RM Memory allocation "
                      "failed"));
        ProtoEvt.u4Error = RM_MEMALLOC_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        return;
    }

    u4OffSet = RIP6_INITIALIZE_ZERO;

    RIP6_RM_PUT_1_BYTE (pMsg, &u4OffSet, RIP6_RED_BULK_UPD_TAIL_MESSAGE);
    RIP6_RM_PUT_2_BYTE (pMsg, &u4OffSet, RIP6_RED_BULK_UPD_TAIL_MSG_SIZE);

    /* This routine sends the message to RM and in case of failure
     * releases the RM buffer memory
     */
    if (Rip6RedSendMsgToRm (pMsg, (UINT2) u4OffSet) != OSIX_SUCCESS)
    {
        ProtoEvt.u4Error = RM_SENDTO_FAIL;
        RmApiHandleProtocolEvent (&ProtoEvt);
        RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                      "Rip6RedSendBulkUpdTailMsg:Send message to RM failed \r\n");
        SYS_LOG_MSG ((SYSLOG_ERROR_LEVEL, gu4Rip6SysLogId,
                      "Rip6RedSendBulkUpdTailMsg:Send message to RM failed"));
        return;
    }
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedSendBulkUpdTailMsg"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedSendBulkUpdTailMsg\r \n");
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_RIP6_APP_ID;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedProcessBulkTailMsg\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedProcessBulkTailMsg"));

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Rip6RedProcessBulkTailMsg: Bulk Update Tail Message received"
                  " at Standby node.\r\n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Rip6RedProcessBulkTailMsg: Bulk Update Tail Message"
                  " received at Standby node"));

    ProtoEvt.u4Error = RM_NONE;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;

    RmApiHandleProtocolEvent (&ProtoEvt);

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedProcessBulkTailMsg"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedProcessBulkTailMsg\r \n");

    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedProcessDynamicRtInfo                      */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the route information   */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rip6RedProcessDynamicRtInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    tRip6RtEntry        TempRtInfo;
    tRip6RtEntry       *pRtEntry = NULL;
    VOID               *pRibNode = NULL;
    UINT4               u4InstanceId = RIP6_INITIALIZE_ZERO;
    INT4                i4Found = RIP6_FAILURE;
    UINT1               u1Distance = RIP6_INITIALIZE_ZERO;

    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Entering Rip6RedProcessDynamicRtInfo\r \n");
    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Entering Rip6RedProcessDynamicRtInfo"));

    MEMSET (&TempRtInfo, RIP6_INITIALIZE_ZERO, sizeof (tRip6RtEntry));

    RIP6_RM_GET_N_BYTE (pMsg, pu4OffSet, &(TempRtInfo.dst), sizeof (tIp6Addr));
    RIP6_RM_GET_N_BYTE (pMsg, pu4OffSet, &(TempRtInfo.nexthop),
                        sizeof (tIp6Addr));
    RIP6_RM_GET_4_BYTE (pMsg, pu4OffSet, TempRtInfo.u4Index);
    RIP6_RM_GET_2_BYTE (pMsg, pu4OffSet, TempRtInfo.u2RtTag);
    RIP6_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.u1Preference);
    RIP6_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.u1RtFlag);
    RIP6_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.u1Metric);
    RIP6_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.u1Prefixlen);
    RIP6_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.u1Operation);
    RIP6_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.i1Proto);
    RIP6_RM_GET_1_BYTE (pMsg, pu4OffSet, TempRtInfo.i1Type);
    RIP6_RM_GET_N_BYTE (pMsg, pu4OffSet, TempRtInfo.au1Pad,
                        RIP6_INITIALIZE_THREE);

    if (RIP6_GET_INST_IF_MAP_ENTRY (TempRtInfo.u4Index) == NULL)
    {
        return;
    }

    u4InstanceId =
        RIP6_GET_INST_IF_MAP_ENTRY (TempRtInfo.u4Index)->u4InstanceId;

    /*  
     * Check whether the matching DYNAMIC entry there in the  
     * routing table.If there then try to update the existing dynamic
     * route else if there is a matching STATIC route then add to the
     * the leaf of the node containing the matching STATIC route
     */
    i4Found = Rip6TrieBestEntry (&(TempRtInfo.dst), RIP6_DYNAMIC_LOOKUP,
                                 TempRtInfo.u1Prefixlen, u4InstanceId,
                                 &pRtEntry, &pRibNode);

    if (TempRtInfo.u1Operation == RIP6HA_ADD_ROUTE)
    {
        if (i4Found == RIP6_FAILURE)
        {
            /*
             * Since this entry is new it is to be put in the 
             * routing table provided there is space and the new dynamic route's
             * metric is not infinity.
             */

            pRtEntry = Rip6RtEntryFill (&(TempRtInfo.dst),
                                        TempRtInfo.u1Prefixlen,
                                        &(TempRtInfo.nexthop),
                                        TempRtInfo.u1Metric,
                                        INDIRECT, TempRtInfo.i1Proto,
                                        TempRtInfo.u2RtTag, TempRtInfo.u4Index,
                                        RIP6_INITIALIZE_ZERO);
            if (pRtEntry != NULL)
            {
                if (Rip6TrieAddEntry (pRtEntry, u4InstanceId) == RIP6_FAILURE)
                {
                    garip6InstanceDatabase[u4InstanceId]->u4DiscaredRoutes++;

                    if (MemReleaseMemBlock
                        ((tMemPoolId) i4Rip6rtId,
                         (UINT1 *) pRtEntry) != RIP6_SUCCESS)
                    {
                        RIP6_TRC_ARG1 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC,
                                       RIP6_NAME,
                                       "RIP6 : rip6 resp prcss rte unable "
                                       "to rel mem to the pool of id = %d \n",
                                       i4Rip6rtId);

                        RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_MGMT_TRC, RIP6_NAME,
                                       "%s mem err: Type = %d  PoolId = %d "
                                       "Memptr = %p\n",
                                       ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                       (UINT1 *) pRtEntry, i4Rip6rtId);

                    }
                    pRtEntry = NULL;

                    return;
                }

                if (pRtEntry->u1Metric != RIP6_INFINITY)
                {
                    u1Distance = Rip6FilterRouteSource (&(pRtEntry->nexthop));
                    Rip6SendRtChgNotification (&pRtEntry->dst,
                                               pRtEntry->u1Prefixlen,
                                               &pRtEntry->nexthop,
                                               pRtEntry->u1Metric,
                                               pRtEntry->i1Proto,
                                               pRtEntry->u4Index,
                                               u1Distance, NETIPV6_ADD_ROUTE);
                }

            }
        }
        else
        {
            if (pRtEntry != NULL)
            {
                pRtEntry->u1Metric = TempRtInfo.u1Metric;
                u1Distance = Rip6FilterRouteSource (&(pRtEntry->nexthop));
                if (pRtEntry->u1Metric != RIP6_INFINITY)
                {
                    Rip6SendRtChgNotification (&pRtEntry->dst,
                                               pRtEntry->u1Prefixlen,
                                               &pRtEntry->nexthop,
                                               pRtEntry->u1Metric,
                                               pRtEntry->i1Proto,
                                               pRtEntry->u4Index,
                                               u1Distance,
                                               NETIPV6_MODIFY_ROUTE);
                }
                else
                {
                    Rip6SendRtChgNotification (&pRtEntry->dst,
                                               pRtEntry->u1Prefixlen,
                                               &pRtEntry->nexthop,
                                               pRtEntry->u1Metric,
                                               pRtEntry->i1Proto,
                                               pRtEntry->u4Index,
                                               u1Distance,
                                               NETIPV6_DELETE_ROUTE);
                }
            }
        }
    }
    else if (TempRtInfo.u1Operation == RIP6HA_DEL_ROUTE)
    {
        if (pRtEntry != NULL)
        {
            u1Distance = Rip6FilterRouteSource (&(pRtEntry->nexthop));
            Rip6SendRtChgNotification (&pRtEntry->dst,
                                       pRtEntry->u1Prefixlen,
                                       &pRtEntry->nexthop,
                                       pRtEntry->u1Metric,
                                       pRtEntry->i1Proto,
                                       pRtEntry->u4Index,
                                       u1Distance, NETIPV6_DELETE_ROUTE);
            Rip6TrieDeleteEntry (pRtEntry, u4InstanceId);
        }
        else
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                          "Route entry not present\r \n");
            return;
        }
    }

    SYS_LOG_MSG ((SYSLOG_INFO_LEVEL, gu4Rip6SysLogId,
                  "Exiting Rip6RedProcessDynamicRtInfo"));
    RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                  "Exiting Rip6RedProcessDynamicRtInfo\r \n");
    return;
}

#else

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedDynDataDescInit                          */
/*                                                                           */
/*    Description         : This function will initialize rip6 dynamic info  */
/*                          data descriptor.                                 */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedDynDataDescInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedDescrTblInit                             */
/*                                                                           */
/*    Description         : This function will initialize Rip6 descriptor    */
/*                          table.                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedDescrTblInit (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedDbNodeInit                                */
/*                                                                           */
/*    Description         : This function will initialize Rip6 route db node.*/
/*                                                                           */
/*    Input(s)            : pRip6DbTblNode - pointer to Rip6 Db Entry.       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedDbNodeInit (tDbTblNode * pDBNode, UINT4 u4Type)
{
    UNUSED_PARAM (pDBNode);
    UNUSED_PARAM (u4Type);
    return;
}

/************************************************************************
 *  Function Name   : Rip6RedInitGlobalInfo                            
 *                                                                    
 *  Description     : This function is invoked by the RIP6 module while
 *                    task initialisation. It initialises the redundancy     
 *                    global variables.
 *                                                                    
 *  Input(s)        : None.                                           
 *                                                                    
 *  Output(s)       : None.                                           
 *                                                                    
 *  Returns         : OSIX_SUCCESS / OSIX_FAILURE                     
 ************************************************************************/

PUBLIC INT4
Rip6RedInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : Rip6RedDeInitGlobalInfo                        
 *                                                                    
 * Description        : This function is invoked by the RIP6 module    
 *                      during module shutdown and this function      
 *                      deinitializes the redundancy global variables 
 *                      and de-register RIP6 with RM.            
 *                                                                    
 * Input(s)           : None                                          
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : OSIX_SUCCESS / OSIX_FAILURE                   
 ************************************************************************/

INT4
Rip6RedDeInitGlobalInfo (VOID)
{
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedRmCallBack                                */
/*                                                                      */
/* Description        : This function will be invoked by the RM module  */
/*                      to enque events and post messages to RIP6        */
/*                                                                      */
/* Input(s)           : u1Event   - Event type given by RM module       */
/*                      pData     - RM Message to enqueue               */
/*                      u2DataLen - Message size                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rip6RedRmCallBack (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    UNUSED_PARAM (u1Event);
    UNUSED_PARAM (pData);
    UNUSED_PARAM (u2DataLen);
    return OSIX_SUCCESS;
}

/************************************************************************
 * Function Name      : Rip6RedHandleRmEvents                          
 *                                                                    
 * Description        : This function is invoked by the RIP6 module to 
 *                      process all the events and messages posted by 
 *                      the RM module                                 
 *                                                                    
 * Input(s)           : pMsg -- Pointer to the RIP6 Q Msg       
 *                                                                    
 * Output(s)          : None                                          
 *                                                                    
 * Returns            : None                                          
 ************************************************************************/

PUBLIC VOID
Rip6RedHandleRmEvents (UINT1 u1MsgProcessFlag)
{
    UNUSED_PARAM (u1MsgProcessFlag);
    return;
}

/************************************************************************
 * Function Name      : Rip6RedHandleGoActive                          
 *                                                                      
 * Description        : This function is invoked by the RIP6 upon
 *                      receiving the GO_ACTIVE indication from RM      
 *                      module. And this function responds to RM with an
 *                      acknowledgement.                                
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
Rip6RedHandleGoActive (VOID)
{
    return;
}

/************************************************************************
 * Function Name      : Rip6RedHandleGoStandby                         
 *                                                                    
 * Description        : This function is invoked by the RIP6 upon
 *                      receiving the GO_STANBY indication from RM    
 *                      module. And this function responds to RM module 
 *                      with an acknowledgement.                        
 *                                                                      
 * Input(s)           : None                                            
 *                                                                      
 * Output(s)          : None                                            
 *                                                                      
 * Returns            : None                                            
 ************************************************************************/

PUBLIC VOID
Rip6RedHandleGoStandby (tRip6RmMsg * pMsg)
{
    UNUSED_PARAM (pMsg);
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedHandleIdleToActive                       */
/*                                                                      */
/* Description        : This function updates the node status to ACTIVE */
/*                      on transition from Idle to Active and also      */
/*                      updates the number of Standby node count.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedHandleIdleToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedHandleIdleToStandby                      */
/*                                                                      */
/* Description        : This function updates the node status to STANDBY*/
/*                      on transition from Idle to Standby and also     */
/*                      updates the number of Standby node count to 0   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedHandleIdleToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedStartTimers                              */
/*                                                                      */
/* Description        : This will start all the necessary timers        */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP6_SUCCESS/RIP6_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rip6RedStartTimers ()
{
    return RIP6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedNotifyRestartRTM                         */
/*                                                                      */
/* Description        : This function will notify RTM to mark all RIP6  */
/*                      routes as STALE. It will send an event to RTM   */
/*                      for this. On receiving the event, RTM will      */
/*                      mark all the routes as stale and start a timer  */
/*                      for Timeout value. On the expiry of this timer  */
/*                      if there are any stale routes still present     */
/*                      those routes are deleted from the RTM and also  */
/*                      from the hardware.                              */
/*                                                                      */
/* Input(s)           : u4CxtId - Context ID                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : RIP6_SUCCESS/RIP6_FAILURE                       */
/************************************************************************/

PUBLIC INT4
Rip6RedNotifyRestartRTM (UINT4 u4CxtId)
{
    UNUSED_PARAM (u4CxtId);
    return RIP6_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedHandleStandbyToActive                     */
/*                                                                      */
/* Description        : This function handles the Standby node to Active*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion.              */
/*                      1.Update the Node Status and Standby node count.*/
/*                      2.Start the timers and enable the module.       */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedHandleStandbyToActive (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedHandleActiveToStandby                     */
/*                                                                      */
/* Description        : This function handles the Active node to Standby*/
/*                      node transistion. The following action are      */
/*                      performed during this transistion               */
/*                      1.Update the Node Status and Standby node count */
/*                      2.Disable and Enable the module                 */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedHandleActiveToStandby (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedProcessPeerMsgAtActive                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Standby) at Active. The messages that are */
/*                      handled in Active node are                      */
/*                      1. RM_BULK_UPDT_REQ_MSG                         */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedProcessPeerMsgAtActive (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedProcessPeerMsgAtStandby                   */
/*                                                                      */
/* Description        : This routine handles messages from the other    */
/*                      node (Active) at standby. The messages that are */
/*                      handled in Standby node are                     */
/*                      1. RM_BULK_UPDT_TAIL_MSG                        */
/*                      2. Dynamic sync-up messages                     */
/*                      3. Dynamic bulk messages                        */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding message           */
/*                      u2DataLen - Length of data in buffer.           */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedProcessPeerMsgAtStandby (tRmMsg * pMsg, UINT2 u2DataLen)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2DataLen);
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedRmReleaseMemoryForMsg                     */
/*                                                                      */
/* Description        : This function is invoked by the RIP6 module to   */
/*                      release the allocated memory by calling the RM  */
/*                      module.                                         */
/*                                                                      */
/* Input(s)           : pu1Block -- Memory Block                        */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rip6RedRmReleaseMemoryForMsg (UINT1 *pu1Block)
{
    UNUSED_PARAM (pu1Block);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedSendMsgToRm                              */
/*                                                                      */
/* Description        : This routine enqueues the Message to RM. If the */
/*                      sending fails, it frees the memory.             */
/*                                                                      */
/* Input(s)           : pMsg - RM Message Data Buffer                   */
/*                      u2Length - Length of the message                */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

PUBLIC INT4
Rip6RedSendMsgToRm (tRmMsg * pMsg, UINT2 u2Length)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u2Length);
    return OSIX_SUCCESS;
}

/************************************************************************/
/* Function Name      : Rip6RedSendBulkReqMsg                           */
/*                                                                      */
/* Description        : This function sends the bulk request message    */
/*                      from standby node to Active node to initiate the*/
/*                      bulk update process.                            */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedSendBulkReqMsg (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedDbUtilAddTblNode                          */
/*                                                                           */
/*    Description         : This function will be use to add a dynamic info  */
/*                          entry into database table.                       */
/*                                                                           */
/*    Input(s)            : pRip6DataDesc - This is Rip6 sepcific data desri-*/
/*                                          ptor info that will be used by   */
/*                                          data base machanism to sync dyna-*/
/*                                          mic info.                        */
/*                          pRip6DbNode - This is db node defined in the RIP6*/
/*                                        to hold the dynamic info.          */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedDbUtilAddTblNode (tDbTblDescriptor * pRip6DataDesc,
                         tDbTblNode * pRip6DbNode)
{
    UNUSED_PARAM (pRip6DataDesc);
    UNUSED_PARAM (pRip6DbNode);
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedSyncDynInfo                               */
/*                                                                           */
/*    Description         : This function will initiate RIP6 dynamic info    */
/*                          syncup in active node.                           */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedSyncDynInfo (VOID)
{
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : Rip6RedAddAllRouteNodeInDbTbl                    */
/*                                                                           */
/*    Description         : This function will initiate addition of all dyna */
/*                          mic info that needs to be syncup to standby node.*/
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None.                                            */
/*****************************************************************************/

PUBLIC VOID
Rip6RedAddAllRouteNodeInDbTbl (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedSendBulkUpdMsg                            */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                                                                      */
/* Input(s)           : None                                            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedSendBulkUpdMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedSendBulkUpdTailMsg                        */
/*                                                                      */
/* Description        : This function sends the binding table dynamic   */
/*                      sync up message to Standby node from the Active */
/*                      node, when the RIP6 offers an IP address         */
/*                      to the RIP6 client and dynamically update the    */
/*                      binding table entries.                          */
/*                                                                      */
/* Input(s)           : pRip6BindingInfo - binding entry to be synced up */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : OSIX_SUCCESS / OSIX_FAILURE                     */
/************************************************************************/

VOID
Rip6RedSendBulkUpdTailMsg (VOID)
{
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedProcessBulkTailMsg                        */
/*                                                                      */
/* Description        : This function process the bulk update tail      */
/*                      message and send bulk updates                   */
/*                                                                      */
/* Input(s)           : pMsg - RM Data buffer holding messges           */
/*                      u2DataLen - Length of data in buffer            */
/*                                                                      */
/* Output(s)          : None                                            */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

PUBLIC VOID
Rip6RedProcessBulkTailMsg (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

/************************************************************************/
/* Function Name      : Rip6RedProcessDynamicRtInfo                      */
/*                                                                      */
/* Description        : This function decodes the sync-up message from  */
/*                      Active node and updates the route information   */
/*                      at the Standby node.                            */
/*                                                                      */
/* Input(s)           : pMsg - RM Sync-up message                       */
/*                      u2RemMsgLen - Length of value of Message        */
/*                                                                      */
/* Output(s)          : u4OffSet - OffSet value                         */
/*                                                                      */
/* Returns            : None                                            */
/************************************************************************/

VOID
Rip6RedProcessDynamicRtInfo (tRmMsg * pMsg, UINT4 *pu4OffSet)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (pu4OffSet);
    return;
}

#endif

#endif
