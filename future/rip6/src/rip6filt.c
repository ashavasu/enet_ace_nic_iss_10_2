/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6filt.c,v 1.4 2009/11/17 09:00:18 prabuc Exp $
 *
 * Description:Contains routines for handling peer and advertisement 
 * filters
 *******************************************************************/

#include "rip6inc.h"
#include "rip6filt.h"

INT4
Rip6AddPeerEntry (UINT4 u4InstanceId, tIp6Addr * pSrc)
{
    UINT4               u4Index;

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_PEERS; u4Index++)
    {
        if (IS_ADDR_UNSPECIFIED (garip6InstanceDatabase[u4InstanceId]->
                                 aRip6PeerTable[u4Index].PeerAddr))
        {
            /* Found a free entry */
            Ip6AddrCopy (&(garip6InstanceDatabase[u4InstanceId]->
                           aRip6PeerTable[u4Index].PeerAddr), pSrc);
            return RIP6_SUCCESS;
        }
    }

    /* no sufficient resources */
    return RIP6_FAILURE;
}

INT4
Rip6DeletePeerEntry (UINT4 u4InstanceId, tIp6Addr * pPeerAddr)
{
    UINT4               u4Index = RIP6_INITIALIZE_ZERO;

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_PEERS; u4Index++)
    {
        if (MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6PeerTable[u4Index].PeerAddr), pPeerAddr,
                    sizeof (tIp6Addr)) == RIP6_INITIALIZE_ZERO)
        {
            /* Entry found */

            MEMSET (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6PeerTable[u4Index]), RIP6_INITIALIZE_ZERO,
                    sizeof (tRip6PeerEntry));
            return RIP6_SUCCESS;
        }
    }

    /* Entry not found */
    return RIP6_FAILURE;
}

INT4
Rip6IsTrustedPeer (UINT4 u4InstanceId, tIp6Addr * pSrc)
{
    UINT4               u4Index = RIP6_INITIALIZE_ZERO;

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_PEERS; u4Index++)
    {
        if (MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6PeerTable[u4Index].PeerAddr), pSrc,
                    sizeof (tIp6Addr)) == RIP6_INITIALIZE_ZERO)
        {
            return RIP6_FAILURE;
        }
    }
    return RIP6_SUCCESS;
}

INT4
Rip6AddAdvFilterEntry (UINT4 u4InstanceId, tIp6Addr * pNetAddr)
{
    UINT4               u4Index = RIP6_INITIALIZE_ZERO;

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_ADV_FILTERS;
         u4Index++)
    {
        if (IS_ADDR_UNSPECIFIED
            (garip6InstanceDatabase[u4InstanceId]->aRip6AdvFltTable[u4Index].
             NetAddr))
        {
            Ip6AddrCopy (&(garip6InstanceDatabase[u4InstanceId]->
                           aRip6AdvFltTable[u4Index].NetAddr), pNetAddr);
            return RIP6_SUCCESS;
        }
    }

    /* No sufficient resources */

    return RIP6_FAILURE;
}

INT4
Rip6DeleteAdvFilterEntry (UINT4 u4InstanceId, tIp6Addr * pNetAddr)
{
    UINT4               u4Index = RIP6_INITIALIZE_ZERO;

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_ADV_FILTERS;
         u4Index++)
    {
        if (MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6AdvFltTable[u4Index].NetAddr), pNetAddr,
                    sizeof (tIp6Addr)) == RIP6_INITIALIZE_ZERO)
        {
            /* Entry found */
            MEMSET (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6AdvFltTable[u4Index]), RIP6_INITIALIZE_ZERO,
                    sizeof (tRip6AdvFltEntry));
            return RIP6_SUCCESS;
        }
    }

    /* Entry not found */
    return RIP6_FAILURE;
}

/*
*  returns true if the filter is enabled and the network number 
*  is present in the list. returns false if the filter is disabled 
*  or the network number is not present in the list.
*/
INT4
Rip6IsNetFilterEnabled (UINT4 u4InstanceId, tIp6Addr * pNetAddr)
{
    UINT4               u4Index = RIP6_INITIALIZE_ZERO;

    if (garip6InstanceDatabase[u4InstanceId]->u4Rip6AdvFltFlag == RIP6_DISABLE)
    {
        return RIP6_FAILURE;
    }
    else
    {
        for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_ADV_FILTERS;
             u4Index++)
        {
            if (MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                          aRip6AdvFltTable[u4Index].NetAddr), pNetAddr,
                        sizeof (tIp6Addr)) == RIP6_INITIALIZE_ZERO)
            {
                /* Entry found */
                return RIP6_SUCCESS;
            }
        }

        /* Entry not found */
        return RIP6_FAILURE;
    }
}

/**************************************************************************/
/*   Function Name   : Rip6ApplyInOutFilter                               */
/*   Description     : This function will check whether the route         */
/*                     can be added or dropped.                           */
/*                     If route is permitted then returned                */
/*                     RIP_SUCCESS else return RIP_FAILURE.               */
/*                                                                        */
/*   Input(s)        : pFilterRMap  - route map data                      */
/*                     pRtInfo      - ptr to the routing update           */
/*                                                                        */
/*   Output(s)       : None.                                              */
/*                                                                        */
/*   Return Value    : RIP6_FAILURE / RIP6_SUCCESS                        */
/**************************************************************************/
INT1
Rip6ApplyInOutFilter (tFilteringRMap * pFilterRMap, tRip6RtEntry * pRtInfo)
{
#ifdef ROUTEMAP_WANTED
    tRtMapInfo          RtInfoIn;
    tRtMapInfo          RtInfoOut;

    if ((pFilterRMap) == NULL || (pRtInfo == NULL))
    {
        return RIP6_SUCCESS;
    }

/*  If status of route map is disable then this route should not be droped */
    if (pFilterRMap->u1Status == FILTERNIG_STAT_DISABLE)
    {
        return RIP6_SUCCESS;
    }

    MEMSET (&RtInfoIn, 0, sizeof (tRtMapInfo));
    MEMSET (&RtInfoOut, 0, sizeof (tRtMapInfo));

    IPVX_ADDR_INIT_FROMV6 (RtInfoIn.DstXAddr,
                           pRtInfo->dst.ip6_addr_u.u4WordAddr);
    RtInfoIn.u2DstPrefixLen = (UINT2) pRtInfo->u1Prefixlen;
    RtInfoIn.i2RouteType = (INT2) pRtInfo->i1Type;

/*  Ignore modification of route attributes by route map */

    if (RMAP_ROUTE_DENY != RMapApplyRule (&RtInfoIn, &RtInfoOut,
                                          pFilterRMap->
                                          au1DistInOutFilterRMapName))
    {
        return RIP6_SUCCESS;
    }
    else
    {
        return RIP6_FAILURE;
    }
#else
    UNUSED_PARAM (pFilterRMap);
    UNUSED_PARAM (pRtInfo);
    return RIP6_SUCCESS;
#endif
}
