/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: rip6buf.c,v 1.7 2014/03/14 12:56:53 siva Exp $
 *
 ********************************************************************/

#include "rip6inc.h"

/******************************************************************************
 * DESCRIPTION : This routine will return a pointer to data for reading at the 
 *               requested offset within the buffer from the first valid byte 
 *               in the buffer. If the data in the buffer is NOT contiguous 
 *               from the specified offset for the specified size OR the caller 
 *               wants a copy of data, the routine will copy the requested 
 *               number of bytes into the data pointer given and return a 
 *               pointer to this copied data. Otherwise, the routine will just 
 *               return a pointer into the buffer.
 *
 * INPUTS      : buffer pointer (pBuf), data pointer (pData),
 *               offset from where to read (u4Offset), 
 *               number of bytes to read (u4Size) and 
 *               whether the data should be compulsorily copied (u1Copy).
 *
 * OUTPUTS     : None (copy could have taken place into 'pData' but this
 *               is not known to the caller)
 *
 * RETURNS     : Void pointer to the data to be written - caller should
 *               cast it to desired type
 *
 * NOTES       :
 ******************************************************************************/

VOID               *
Rip6BufRead (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Offset,
             UINT4 u4Size, UINT1 u1Copy)
{

    UINT1              *pStart = NULL;

    RIP6_TRC_ARG5 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "RIP6BUF: Reading from buf = %p data = %p"
                   " offset = %d size = %d copy = %d\n",
                   pBuf, pData, u4Offset, u4Size, u1Copy);

    /*
     * check if a copy is requested
     */

    if (u1Copy != RIP6_INITIALIZE_ZERO)
    {
        if (CRU_BUF_Copy_FromBufChain (pBuf, pData, u4Offset,
                                       u4Size) == CRU_FAILURE)
        {
            RIP6_TRC_ARG5 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "RIP6BUF:CRU_BUF_Copy_FromBufChain fail,"
                           " buf=%p data=%p offset=%d size=%d copy=%d",
                           pBuf, pData, u4Offset, u4Size, u1Copy);
            return (NULL);
        }
        RIP6_BUF_READ_OFFSET (pBuf) = RIP6_BUF_READ_OFFSET (pBuf) + u4Size;
        return ((VOID *) pData);
    }

    pStart = CRU_BUF_Get_DataPtr_IfLinear (pBuf, u4Offset, u4Size);
    if (pStart == NULL)
    {
        RIP6_TRC_ARG5 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "RIP6BUF:CRU_BUF_Read_FromBufChain fail,"
                       " buf=%p data=%p offset=%d size=%d copy=%d",
                       pBuf, pData, u4Offset, u4Size, u1Copy);

        if (CRU_BUF_Copy_FromBufChain (pBuf, pData, u4Offset,
                                       u4Size) == CRU_FAILURE)
        {
            RIP6_TRC_ARG5 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                           "RIP6BUF: CRU_BUF_Copy_FromBufChain fail,"
                           " buf=%p data=%p offset=%d size=%d cp=%d",
                           pBuf, pData, u4Offset, u4Size, u1Copy);
            return (NULL);
        }
        RIP6_BUF_READ_OFFSET (pBuf) = RIP6_BUF_READ_OFFSET (pBuf) + u4Size;
        return ((VOID *) pData);
    }

    RIP6_BUF_READ_OFFSET (pBuf) = RIP6_BUF_READ_OFFSET (pBuf) + u4Size;
    RIP6_TRC_ARG2 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                   "RIP6BUF: Returning pointer = %p local data = %p\n", pStart,
                   pData);

    return ((VOID *) pStart);
}

/******************************************************************************
 * DESCRIPTION : This routine will write the passed data into the buffer at
 *               the specified offset if copy is required.  This routine is
 *               used in conjunction with ip6_buf_wptr().
 *
 * INPUTS      : buffer pointer (pBuf), data pointer (pData),
 *               offset from where to write (u4Offset),
 *               number of bytes to write (u4Size) and
 *               whether copy into buffer is really required or not (u1Copy)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS, upon successful completion
 *               RIP6_FAILURE, upon failure to copy
 *
 * NOTES       :
 ******************************************************************************/

INT4
Rip6BufWrite (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pData, UINT4 u4Offset,
              UINT4 u4Size, UINT1 u1Copy)
{

    RIP6_TRC_ARG5 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                   "RIP6BUF: In bufwrite Bufptr = %p data = %p offset = "
                   "%d size = %d Copy = %d\n",
                   pBuf, pData, u4Offset, u4Size, u1Copy);

    if (u1Copy == RIP6_INITIALIZE_ZERO)
    {
        return (RIP6_SUCCESS);
    }

    /* 
     * we need to copy from pData into the buffer, so call the CRU library
     * routine
     */

    if (CRU_BUF_Copy_OverBufChain (pBuf, pData, u4Offset, u4Size) ==
        CRU_FAILURE)
    {
        RIP6_TRC_ARG4 (RIP6_MOD_TRC, RIP6_BUFFER_TRC, RIP6_NAME,
                       "RIP6BUF: CRU_BUF_Copy_OverBufChain fail Write,"
                       " buf=%p data=%p offset=%d size=%d",
                       pBuf, pData, u4Offset, u4Size);

        return (RIP6_FAILURE);
    }

    RIP6_BUF_WRITE_OFFSET (pBuf) = RIP6_BUF_WRITE_OFFSET (pBuf) + u4Size;
    return (RIP6_SUCCESS);

}

/******************************************************************************
 * DESCRIPTION : This routine will start a timer of specified duration
 *               by invoking the library routine.
 *
 * INPUTS      : Timer Id (u1TimerId), List in which to add the timer
 *               (p_timer_list), the timer node structure which is part
 *               of the data structure (pAppTimer) and the timer duration
 *               (u4Duration)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Rip6TmrStart (UINT1 u1TimerId, tTimerListId timerListId,
              tTmrAppTimer * pAppTimer, UINT4 u4Duration)
{
    if (RIP6_GET_NODE_STATUS () == RM_ACTIVE)
    {
        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_DATA_PATH_TRC, RIP6_NAME,
                       "RIP6LIB: Starting timer Id = %d Duration = %d"
                       " Timer Node = %p\n", u1TimerId, u4Duration, pAppTimer);

        ((tRip6RtTimer *) pAppTimer)->u1Id = u1TimerId;
        TmrStartTimer (timerListId, pAppTimer,
                       SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4Duration);
    }
}

/******************************************************************************
*
 * DESCRIPTION : This routine will stop a timer by invoking the library routine.
 *
 * INPUTS      : Timer Id (u1TimerId), List in which the timer is present
 *               (p_timer_list) and the timer node structure (pAppTimer)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : NONE
 *
 * NOTES       :
 ******************************************************************************/
VOID
Rip6TmrStop (UINT1 u1TimerId, tTimerListId timerListId,
             tTmrAppTimer * pAppTimer)
{
    /* To handle warnings in case of Trace is not defined */
    UNUSED_PARAM (u1TimerId);
    RIP6_TRC_ARG2 (RIP6_MOD_TRC, CONTROL_PLANE_TRC, RIP6_NAME,
                   "RIP6LIB: Stopping timer Id = %d Timer Node = %p\n",
                   u1TimerId, pAppTimer);
    if (TmrStopTimer (timerListId, pAppTimer) != TMR_SUCCESS)
    {
        RIP6_TRC_ARG3 (RIP6_MOD_TRC, RIP6_CTRL_PLANE_TRC, RIP6_NAME,
                       "RIP6LIB: Unlinking of timer Id = %d Node = %p "
                       "list = %p failed\n", u1TimerId, pAppTimer, timerListId);
    }
    ((tRip6RtTimer *) pAppTimer)->u1Id = 0;
}

UINT4
Rip6TimerStart (tRip6RtEntry * pRtEntry, UINT1 u1TimerId, UINT4 u4Duration)
{
    UINT4               u4InstanceId;
    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRtEntry->u4Index)->u4InstanceId;
    UNUSED_PARAM (u4InstanceId);
    pRtEntry->rtEntryTimer.u1Id = u1TimerId;
    OsixGetSysTime (&(pRtEntry->u4ChangeTime));
    pRtEntry->rtEntryTimer.u4RemainingTime =
        pRtEntry->u4ChangeTime + (u4Duration * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    return RIP6_SUCCESS;
}

UINT4
Rip6TimerStop (tRip6RtEntry * pRtEntry)
{
    pRtEntry->rtEntryTimer.u1Id = 0;
    pRtEntry->rtEntryTimer.u4RemainingTime = 0;
    return RIP6_SUCCESS;
}

UINT4
Rip6TimerUpdate (tRip6RtEntry * pRtEntry, UINT1 u1TimerId, UINT4 u4Duration)
{
    UINT4               u4InstanceId;
    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRtEntry->u4Index)->u4InstanceId;
    UNUSED_PARAM (u4InstanceId);
    pRtEntry->rtEntryTimer.u1Id = u1TimerId;
    OsixGetSysTime (&(pRtEntry->u4ChangeTime));
    pRtEntry->rtEntryTimer.u4RemainingTime =
        pRtEntry->u4ChangeTime + (u4Duration * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    return RIP6_SUCCESS;
}

INT4
TmrRip6MIRemainingTime (tRip6RtEntry * pRtEntry, UINT4 *pu4RemainingTime)
{
    UINT4               u4temptime;
    OsixGetSysTime (&(u4temptime));
    if (pRtEntry->rtEntryTimer.u4RemainingTime > u4temptime)
    {
        *pu4RemainingTime = pRtEntry->rtEntryTimer.u4RemainingTime - u4temptime;
        return RIP6_SUCCESS;
    }
    else
    {
        return RIP6_FAILURE;
    }
}
