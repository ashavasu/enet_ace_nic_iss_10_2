/********************************************************************
 *  Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 *  $Id: rip6rt.c,v 1.11 2017/02/21 14:06:43 siva Exp $
 *
 *  Description: This file contains the routines
 *               for handling routing table processing
 *  ************************************************************/
/*-----------------------------------------------------------------------------
 *    FILE NAME                    :    rip6rt.c   
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    IP6 RIP6 Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996 
 *
 *    DESCRIPTION                  :    This file contains the routines
 *                                      for handling routing table processing 
 *                                      
 *-----------------------------------------------------------------------------
 */

#include "rip6inc.h"
extern UINT1 gu1Rip6Distance;

/* 
 * Private Routines
 ******************/

/******************************************************************************
 * DESCRIPTION : To allocate a route entry and fill it
 *
 * INPUTS      : The ptr to Dest IPv6 addr (pDest), the prefix length
 *               (u1Prefixlen),ptr to Next Hop IPv6 address (pNexthop),
 *               metric (u1Metric),type of route (i1Type),the protocol of 
 *               the route (i1Proto),the tag (u2Tag), the logical interface
 *               (u4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : The routing entry ptr if successful in allocating memory
 *               and filling the route into it else a NULL
 *
 * NOTES       :
 ******************************************************************************/

tRip6RtEntry       *
Rip6RtEntryFill (tIp6Addr * pDest, UINT1 u1Prefixlen, tIp6Addr * pNexthop,
                 UINT1 u1Metric, INT1 i1Type, INT1 i1Proto, UINT2 u2Tag,
                 UINT4 u4Index, UINT1 u1Level)
{
    tRip6RtEntry       *pRt = NULL;
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    RIP6_TRC_ARG6 (RIP6_MOD_TRC, DATA_PATH_TRC, RIP6_NAME,
                   "RIP6FilRtEntryDstAdr=%sPfxlen=%dNxtHopAdr=%sMetrc=%dType=%dProto=%d",
                   Ip6PrintAddr (pDest), u1Prefixlen,
                   Ip6PrintAddr (pNexthop), u1Metric, i1Type, i1Proto);

    RIP6_TRC_ARG2 (RIP6_MOD_TRC, DATA_PATH_TRC, RIP6_NAME, "Tag=%dIndx=%d",
                   u2Tag, u4Index);

    /* Allocate routing entry from the pool of CRU buffers */
    if (NULL ==
        (pRt = (tRip6RtEntry *) MemAllocMemBlk ((tMemPoolId) i4Rip6rtId)))
    {
        garip6InstanceDatabase[u4InstanceId]->u4DiscaredRoutes++;

        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                      "RIP6 : RIP6 route fill, No memory to allocate "
                      "for the routing info structure\n");

        RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                       "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                       ERROR_FATAL_STR, ERR_MEM_GET, RIP6_INITIALIZE_ZERO,
                       i4Rip6rtId);

        return NULL;
    }

    MEMSET (pRt, RIP6_INITIALIZE_ZERO, sizeof (tRip6RtEntry));

    Rip6RedDbNodeInit (&(pRt->RouteDbNode), RIP6_RT_DYN_INFO);
    /* Fill in the routing table entry */

    MEMCPY (&(pRt->dst), pDest, sizeof (tIp6Addr));

    pRt->u1Prefixlen = u1Prefixlen;
    if (pNexthop != NULL)
    {
        MEMCPY (&(pRt->nexthop), pNexthop, sizeof (tIp6Addr));
    }

    pRt->u1Metric = u1Metric;
    pRt->i1Type = i1Type;
    pRt->i1Proto = i1Proto;
    pRt->u4Index = u4Index;
    pRt->u2RtTag = u2Tag;
    pRt->u1RtFlag = RIP6_INITIALIZE_ONE;
    pRt->i1AddrCnt = RIP6_INITIALIZE_ONE;
    pRt->u1Operation = RIP6HA_ADD_ROUTE;
    pRt->u1Level = u1Level;

    OsixGetSysTime (&(pRt->u4ChangeTime));

    return (pRt);
}

/******************************************************************************
 * DESCRIPTION : This routine handles the processing of routes reachable via
 *               the interface going down. This function garbage collect
 *               dynamic routes learnt through this iface and removes the
 *               non-RIPNG routes. Triggered Update is sent if required.
 *
 * INPUTS      : The logical interface (u4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Rip6AgeoutOverIf (UINT4 u4Index)
{
    UINT1               u1TrigSend = RIP6_INITIALIZE_ZERO;
    tRip6RtEntry       *pRtEntry = NULL;
    tRip6RtEntry       *pNextRtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    RIP6_TRC_ARG1 (RIP6_MOD_TRC, CONTROL_PLANE_TRC, RIP6_NAME,
                   "RIP6 : Delete routes on If = %d \n", u4Index);

    Rip6TrieGetFirstEntry (&pNextRtEntry, &pRibNode, u4InstanceId);

    while ((pRtEntry = pNextRtEntry) != NULL)
    {
        /* Get the Next Route entry. */
        Rip6TrieGetNextEntry (&pNextRtEntry, pRibNode, &pNextRibNode,
                              u4InstanceId);

        while (pRtEntry)
        {
            if (pRtEntry->u4Index == u4Index)
            {
                /* Process the current route. */
                if (pRtEntry->i1Proto == RIPNG)
                {
                    Rip6RouteAgeout (pRtEntry);
                    u1TrigSend = RIP6_INITIALIZE_ONE;
                }
                else
                {
                    pRtEntry->u1RtFlag = RIP6_INITIALIZE_ONE;
                    pRtEntry->u1Metric = RIP6_INFINITY;
                    Rip6CheckTrigUpdateSend (RIP6_CHECK_ALL, pRtEntry->u4Index,
                                             pRtEntry);
                    Rip6TrieDeleteEntry (pRtEntry, u4InstanceId);
                }
            }

            pRtEntry = pRtEntry->pNext;
        }

        /* Update Next Node */
        pRibNode = pNextRibNode;
        pNextRibNode = NULL;
    }

    if (u1TrigSend != RIP6_FALSE)
    {
        Rip6CheckTrigUpdateSend ((UINT1) (~RIP6_CHECK_ALL), u4Index, NULL);
        u1TrigSend = RIP6_INITIALIZE_ZERO;
    }
}

/******************************************************************************
 * DESCRIPTION : Does the garbage collect on the route 
 *
 * INPUTS      : The routing table entry ptr (pRtEntry)
 *
 * OUTPUTS     : None 
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Rip6RouteAgeout (pRtEntry)
     tRip6RtEntry       *pRtEntry;
{

    RIP6_TRC_ARG2 (RIP6_MOD_TRC, CONTROL_PLANE_TRC, RIP6_NAME,
                   "RIP6 : To Garbage collect route, Addr = %s Prefixlen = %d\n",
                   Ip6PrintAddr (&pRtEntry->dst), pRtEntry->u1Prefixlen);

    if (pRtEntry->i1Proto == RIPNG)
    {
        pRtEntry->u1Metric = RIP6_INFINITY;
        pRtEntry->u1RtFlag = RIP6_INITIALIZE_ONE;

        if (pRtEntry->rtEntryTimer.u1Id != RIP6_INITIALIZE_ZERO)
        {
            Rip6TimerStop (pRtEntry);
        }

        /* Start the garbage collect timer */
        pRtEntry->rtEntryTimer.u1Id = RIP6_GARB_COLLECT_TIMER_ID;

        Rip6TimerStart (pRtEntry, RIP6_GARB_COLLECT_TIMER_ID,
                        RIP6_GARBAGE_COLLECT_INTERVAL (pRtEntry->u4Index));

        pRtEntry->u1Operation = RIP6HA_ADD_ROUTE;
        Rip6RedDbUtilAddTblNode (&gRip6DynInfoList, &pRtEntry->RouteDbNode);
        Rip6RedSyncDynInfo ();
        /* METRIC modified. Update IPv6 Routing Table */
        Rip6SendRtChgNotification (&pRtEntry->dst, pRtEntry->u1Prefixlen,
                                   &pRtEntry->nexthop, pRtEntry->u1Metric,
                                   pRtEntry->i1Proto, pRtEntry->u4Index,
                                   pRtEntry->u1Preference,
                                   NETIPV6_DELETE_ROUTE);
    }
}

/******************************************************************************
 * DESCRIPTION : Purges routes learnt over this interface. Non-dynamic also
 *               depending upon u1Purge flag.
 *
 * INPUTS      : The purge flag (u1Purge), The logical interface (u4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

INT4
Rip6PurgeRtLearnt (UINT1 u1Purge, UINT4 u4Index)
{
    tRip6RtEntry       *pCurrRt = NULL;
    tRip6RtEntry       *pNextRt = NULL;
    tRip6RtEntry       *pNextRtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    UINT4               u4InstanceId;
    UINT1               u1NextMetric = 0;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    RIP6_TRC_ARG1 (RIP6_MOD_TRC, DATA_PATH_TRC, RIP6_NAME,
                   "RIP6 : Purge Routes learnt on If = %d \n", u4Index);

    Rip6TrieGetFirstEntry (&pNextRtEntry, &pRibNode, u4InstanceId);

    while ((pCurrRt = pNextRtEntry) != NULL)
    {
        /* Get the Next Route entry. */
        Rip6TrieGetNextEntry (&pNextRtEntry, pRibNode, &pNextRibNode,
                              u4InstanceId);

        while (pCurrRt)
        {
            pNextRt = pCurrRt->pNext;
            if (pCurrRt->u4Index == u4Index)
            {
                if (u1Purge != RIP6_MI_PURGE_ALL)
                {
                    if ((pCurrRt->i1Proto != RIPNG) &&
                        (u1Purge == RIP6_PURGE_DYNAMIC))
                    {
                        pCurrRt = pNextRt;
                        pNextRt = NULL;
                        break;
                    }
                }

                /* intimate change of route in Rip6 Route Table to IP6 */
                Rip6SendRtChgNotification (&pCurrRt->dst, pCurrRt->u1Prefixlen,
                                           &pCurrRt->nexthop, pCurrRt->u1Metric,
                                           pCurrRt->i1Proto, pCurrRt->u4Index,
                                           pCurrRt->u1Preference,
                                           NETIPV6_DELETE_ROUTE);

                if (Rip6TrieDeleteEntry (pCurrRt, u4InstanceId) == RIP6_FAILURE)
                {
                    return RIP6_FAILURE;
                }
                if (Rip6CheckEcmpRoutePresentInDb (&pCurrRt->dst, pCurrRt->u1Prefixlen,
                                           pCurrRt->u1Metric,&u1NextMetric) == RIP6_FAILURE)
                {

                /*check whether we have route for the dst with higher metric in the DB
                 * if so we need add route */
                    if (u1NextMetric != 0)
                    {

                    Rip6TriggerRouteChangeforEcmpRoute (&pCurrRt->dst, pCurrRt->u1Prefixlen,
                                                        u1NextMetric);
                   }
                }
            }

            pCurrRt = pNextRt;
            pNextRt = NULL;
        }
        /* Update Next Node */
        pRibNode = pNextRibNode;
        pNextRibNode = NULL;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Purges all routes over all interface. 
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Rip6MIPurgeAll (UINT4 u4InstId)
{
    UINT4               u4Count;
    UINT4               u4InstanceId;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    for (u4Count = RIP6_INITIALIZE_ONE; u4Count <= u4MaxIfs; u4Count++)
    {
        u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Count)->u4InstanceId;
        if (u4InstanceId != u4InstId)
        {
            continue;
        }

        if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count) == NULL)
        {
            continue;
        }

        Rip6PurgeRtLearnt (RIP6_MI_PURGE_ALL, u4Count);
    }
}

/******************************************************************************
 * DESCRIPTION : Purges routes dynamic routes over this interface. 
 *
 * INPUTS      : None
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

VOID
Rip6PurgeAllDynamic ()
{
    UINT2               u2Count;
    UINT4               u4InstanceId;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    for (u2Count = RIP6_INITIALIZE_ONE;
         (u2Count <= u4MaxIfs && u2Count < MAX_RIP6_IFACES_LIMIT + 2);
         u2Count++)
    {
        u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u2Count)->u4InstanceId;
        if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u2Count) == NULL)

        {
            continue;
        }

        Rip6PurgeRtLearnt (RIP6_PURGE_DYNAMIC, u2Count);
    }
}

/******************************************************************************
 * DESCRIPTION : Sends out the routing table information learnt over this
 *               interface. Static or dynamic only depends on the flag(i2_send
 *               _flag). No timers are started after this regular update.
 *
 * INPUTS      : The logical interface (u4Index) and flag (i2SendFlag)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

INT4
Rip6SendRtOnIf (UINT4 u4Index, INT2 i2SendFlag)
{
    tIp6Addr            dstAddr;
    UINT4               u4InstanceId;
    UINT4               u4Count;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->u4InstanceId;

    if (garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes
        == RIP6_INITIALIZE_ZERO)
    {
        /* No routes there in the routing table */
        return RIP6_FAILURE;
    }

    SET_ALL_RIP_ROUTERS (dstAddr);

    for (u4Count = RIP6_INITIALIZE_ONE; u4Count <= u4MaxIfs; u4Count++)
    {
        if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count) == NULL)
        {
            continue;
        }

        if (u4Index == u4Count)
        {
            /* Need to advertise all the routes with metric as INFINITY
             * over the interface going down. */
            Rip6SendResponse ((UINT1) (RIP6_FINAL_UPDATE | RIP6_SEND_ALL),
                              RIP6_PORT, u4Count, u4Index, &dstAddr);
        }
        else if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->
                  u1Status & RIP6_IFACE_ALLOCATED)
                 && (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->
                     u1Status & RIP6_IFACE_ENABLED)
                 && (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4Count)->
                     u1Status & RIP6_IFACE_UP)
                 && (RIP6_GET_INST_IF_MAP_ENTRY (u4Index)->
                     u4InsIfStatus & RIP6_IFACE_ATTACHED))
        {
            /* Need to advertise the dynamic/all routes learnt over the interface
             * going down, with metric as INFINITY to all active interfaces. */
            Rip6SendResponse ((UINT1) (RIP6_FINAL_UPDATE | RIP6_IFACE_FINAL |
                                       i2SendFlag), RIP6_PORT, u4Count, u4Index,
                              &dstAddr);
        }
    }

    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : To do the release of the route entry
 *
 * INPUTS      : pRt : pointer to the route entry
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/
VOID
Rip6RtRelease (pRt)
     tRip6RtEntry       *pRt;
{
    UINT4               u4InstanceId;

    u4InstanceId = RIP6_GET_INST_IF_MAP_ENTRY (pRt->u4Index)->u4InstanceId;
    UNUSED_PARAM (u4InstanceId);

    /* 
     * Return the memory to the CRU buffer pool from which the routing
     * entries are allocated only if the entry is not pointed to by 
     * many entries
     */

    --(pRt->i1Refcnt);

    if (pRt->i1Refcnt == RIP6_INITIALIZE_ZERO)
    {

        if (pRt->i1Proto == RIPNG)
        {
            if (pRt->u1Metric != RIP6_INFINITY)
            {
                Rip6TimerStop (pRt);
            }
            else
            {
                Rip6TimerStop (pRt);
            }
            pRt->rtEntryTimer.u1Id = 0;

        }

    }
}

/******************************************************************************
 * DESCRIPTION : Removes routes learnt over this interface. 
 *
 * INPUTS      : The logical interface (u4Index)
 *
 * OUTPUTS     : None
 *
 * RETURNS     : None
 *
 * NOTES       :
 ******************************************************************************/

INT4
Rip6RemoveAllRtLearnt (VOID)
{
    tRip6RtEntry       *pCurrRt = NULL;
    tRip6RtEntry       *pNextRt = NULL;
    tRip6RtEntry       *pNextRtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;

    Rip6TrieGetFirstEntry (&pNextRtEntry, &pRibNode, RIP6_DEFAULT_INSTANCE);

    while ((pCurrRt = pNextRtEntry) != NULL)
    {
        /* Get the Next Route entry. */
        Rip6TrieGetNextEntry (&pNextRtEntry, pRibNode, &pNextRibNode,
                              RIP6_DEFAULT_INSTANCE);

        while (pCurrRt)
        {
            pNextRt = pCurrRt->pNext;

            if (Rip6TrieDeleteEntry (pCurrRt, RIP6_DEFAULT_INSTANCE) ==
                RIP6_FAILURE)
            {
                return RIP6_FAILURE;
            }

            pCurrRt = pNextRt;
            pNextRt = NULL;
        }
        /* Update Next Node */
        pRibNode = pNextRibNode;
        pNextRibNode = NULL;
    }
    return RIP6_SUCCESS;
}

/******************************************************************************
 * DESCRIPTION : Check whether ECMP path for the given destination is present or
 *               not. if no, it will return the next highest metric
 *
 * INPUTS      : pDestination - dst address,u1Prefixlen- prefix length
 *               u1Metric - current metric
 *
 * OUTPUTS     : pu1NextMetric - next best metric
 *
 * RETURNS     : RIP6_SUCCESS/RIP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4 Rip6CheckEcmpRoutePresentInDb (tIp6Addr * pDestination, UINT1 u1Prefixlen,
                                 UINT1 u1Metric, UINT1  *pu1NextMetric)
{
    tRip6RtEntry       *pCurrRt = NULL;
    tRip6RtEntry       *pNextRt = NULL;
    tRip6RtEntry       *pNextRtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    UINT1               u1PrevMetric = 0xff;
    INT4                i4RetVal = 0;

    i4RetVal = Rip6TrieGetFirstEntry (&pNextRtEntry, &pRibNode, RIP6_DEFAULT_INSTANCE);

    while ((pCurrRt = pNextRtEntry) != NULL)
    {
         if ((i4RetVal == RIP6_SUCCESS) && (MEMCMP (&pNextRtEntry->dst,
                                                     pDestination,IP6_ADDR_SIZE) == 0)
              && (pNextRtEntry->u1Prefixlen == u1Prefixlen))
         {
              pCurrRt = pNextRtEntry;
              while (pCurrRt != NULL)
              {
                  pNextRt = pCurrRt->pNext;
                  if (pCurrRt->u1Metric == u1Metric)
                  {
                      /* here we have ecmp path alive no need to do anything*/
                      return RIP6_SUCCESS;
                  }
        
                  if (pCurrRt->u1Metric < u1PrevMetric)
                  {
                      u1PrevMetric = pCurrRt->u1Metric;
                      *pu1NextMetric = u1PrevMetric;
                  }
                  /*Validate whether to update route or not*/
                  pCurrRt = pNextRt;
                  pNextRt = NULL;
              }
              if ((u1PrevMetric != 0xff) && (u1PrevMetric > u1Metric))
              {
              /* Give failure, to handle the other metric routes */
              return RIP6_FAILURE;
              }
              else
              {
                  /* no need to take action during this deletion
                   * because its either last node, or its a piece from
                   * existing ecmp
                   */
                  return RIP6_SUCCESS;
              }
         }
        /* Get the Next Route entry. */
        i4RetVal = Rip6TrieGetNextEntry (&pNextRtEntry, pRibNode, &pNextRibNode,
                                         RIP6_DEFAULT_INSTANCE);
        pRibNode = pNextRibNode;
    }

    return RIP6_FAILURE;
}
/******************************************************************************
 * DESCRIPTION : Trigger the next route to RTM for the given metric for addition
 *
 * INPUTS      : pDest - dest address, u1Prefixlen - prefix length
 *               u1Metric - metric for which route to be triggered
 *
 * OUTPUTS     : None
 *
 * RETURNS     : RIP6_SUCCESS/ RIP6_FAILURE
 *
 * NOTES       :
 ******************************************************************************/
INT4 Rip6TriggerRouteChangeforEcmpRoute (tIp6Addr * pDest, UINT1 u1Prefixlen,
                                         UINT1 u1Metric)
{
    tRip6RtEntry       *pCurrRt = NULL;
    tRip6RtEntry       *pNextRt = NULL;
    tRip6RtEntry       *pNextRtEntry = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;

    Rip6TrieGetFirstEntry (&pNextRtEntry, &pRibNode, RIP6_DEFAULT_INSTANCE);

    while ((pCurrRt = pNextRtEntry) != NULL)
    {
        while (pCurrRt)
        {
            pNextRt = pCurrRt->pNext;

            if((MEMCMP (&pCurrRt->dst,pDest,IP6_ADDR_SIZE) == 0) &&
               (pCurrRt->u1Metric == u1Metric) && (pCurrRt->u1Prefixlen == u1Prefixlen))
            {
                Rip6SendRtChgNotification (&pCurrRt->dst,
                                           pCurrRt->u1Prefixlen,
                                           &pCurrRt->nexthop,
                                           pCurrRt->u1Metric,
                                           pCurrRt->i1Proto,
                                           pCurrRt->u4Index,
                                           gu1Rip6Distance, NETIPV6_ADD_ROUTE);

            }

            pCurrRt = pNextRt;
            pNextRt = NULL;
        }
        /* Get the Next Route entry. */
        Rip6TrieGetNextEntry (&pNextRtEntry, pRibNode, &pNextRibNode,
                              RIP6_DEFAULT_INSTANCE);
        /* Update Next Node */
        pRibNode = pNextRibNode;
    }
    return RIP6_SUCCESS;

}
