/*******************************************************************
 *** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ***
 *** $Id: rip6cli.c,v 1.32 2018/02/02 09:47:57 siva Exp $
 *********************************************************************/

/* SOURCE FILE HEADER :
 *
 *  ---------------------------------------------------------------------------
 * |  FILE NAME             : rip6cli.c                                        |
 * |                                                                           |
 * |  PRINCIPAL AUTHOR      : Aricent Inc.                              |
 * |                                                                           |
 * |  SUBSYSTEM NAME        : CLI                                              |
 * |                                                                           |
 * |  MODULE NAME           : RIP6 configuration                               |
 * |                                                                           |
 * |  LANGUAGE              : C                                                |
 * |                                                                           |
 * |  TARGET ENVIRONMENT    :                                                  |
 * |                                                                           |
 * |  DATE OF FIRST RELEASE :                                                  |
 * |                                                                           |
 * |  DESCRIPTION           : Action routines for set/get objects in           |
 * |                          fsrip6.mib                                      |
 *  ---------------------------------------------------------------------------
 *
 */
#ifndef __RIP6CLI_C__
#define __RIP6CLI_C__
#include "rip6inc.h"
#include "cli.h"
#include "rip6cli.h"
#include "rip6cliproto.h"
#include "extern.h"
#include "fsrip6cli.h"
#include "fsrip6wr.h"

UINT4               gu4Rip6AdminStatus;
extern UINT1        gu1Rip6Distance;
/*********************************************************************
*  Function Name : cli_process_rip6_cmd() 
*  Description   : This function processes the RIP6 Related Commands ,
*                  Pre-validates the Inputs before sending to the
*                  RIP6CLI Module , Fills up the RIP6 Config Params
*                  Structure with the input values given by the User,
*                  fills up the Command in the Input Message, and
*                  calls RIP6CliCmdActionRoutine() for further Processing of
*                  the User Commands/Inputs.
*  Input(s)      : UserInputs/Command in va_alist. 
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE.
*********************************************************************/

INT4
cli_process_rip6_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[RIP6_CLI_MAX_ARGS];
    UINT4               u4ErrorCode;
    INT1                argno = 0;
    INT4                i4IfIndex = 0;
    UINT1               u1PeerAddr[16];
    INT4                i4RetStatus = CLI_SUCCESS;
    tIp6Addr           *tempIp6addr;

    va_start (ap, u4Command);

    /* third argument is always InterfaceName/Index */
    i4IfIndex = va_arg (ap, INT4);

    /* Walk through the rest of the arguments and store in args array. 
     * Store RIP6_CLI_MAX_ARGS arguements at the max. 
     */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == RIP6_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);
    CliRegisterLock (CliHandle, Rip6Lock, Rip6UnLock);
    if (RIP6_TASK_LOCK () == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%RIP6 Locking Failed, Command Not Executed\r\n");

        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    switch (u4Command)
    {

        case CLI_RIP6_DEBUG:
            i4RetStatus = Rip6Debug (CliHandle, CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RIP6_SHOW_RIP:
            if (CLI_PTR_TO_U4 (args[0]) == CLI_RIP6_ROUTE_SHOW)
                i4RetStatus = Rip6ShowRoute (CliHandle);
            if (CLI_PTR_TO_U4 (args[0]) == CLI_RIP6_PROFILE_SHOW)
                i4RetStatus = Rip6ProfileShow (CliHandle);
            break;

        case CLI_RIP6_IF_STATS_SHOW:
            i4RetStatus = Rip6IfStatsShow (CliHandle);
            break;

        case CLI_RIP6_PEERADV_TABLE_SHOW:
            i4RetStatus = Rip6FilterTableShow (CliHandle);
            break;

        case CLI_RIP6_ROUTER:
            i4RetStatus = Rip6EnableRouter (CliHandle);
            break;

        case CLI_RIP6_NO_ROUTER:
            i4RetStatus = Rip6DisableRouter (CliHandle);
            break;

        case CLI_RIP6_SPLIT_HORIZON:
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = Rip6EnableSplitHorizon (CliHandle, i4IfIndex);
            break;

        case CLI_RIP6_NO_SPLIT_HORIZON:
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = Rip6EnableNoHorizon (CliHandle, i4IfIndex);
            break;

        case CLI_RIP6_IF:

            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = Rip6EnableDisableRip6If (CliHandle,
                                                   i4IfIndex,
                                                   (UINT1) (CLI_PTR_TO_I4
                                                            (args[0])));
            break;

        case CLI_RIP6_POISON_REVERSE:
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = Rip6EnableDisablePoisonReverse (CliHandle, i4IfIndex);
            break;

        case CLI_RIP6_DEFROUTE:
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = Rip6EnableDisableDefRouteAdvt (CliHandle,
                                                         i4IfIndex,
                                                         (UINT1)
                                                         CLI_PTR_TO_U4 (args
                                                                        [0]));
            break;

        case CLI_RIP6_METRIC:
            i4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = Rip6Metric (CliHandle,
                                      i4IfIndex, *(INT4 *) (args[0]));
            break;

        case CLI_RIP6_REDISTRIBUTE:
            /* args[1] is metric for redistributed route */

            i4RetStatus = Rip6Redistribute (CliHandle,
                                            CLI_PTR_TO_U4 (args[0]),
                                            CLI_PTR_TO_I4 (args[1]),
                                            (UINT1) CLI_PTR_TO_U4 (args[2]),
                                            (UINT1 *) args[3]);
            break;

        case CLI_RIP6_PEER_FILTER_SET:
            i4RetStatus = RIP6AddOrDelPeerFilter (CliHandle,
                                                  CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RIP6_DELAY_TRIG_INTERVAL_SET:
            i4RetStatus = RIP6ProfileTrigDelayTime (CliHandle,
                                                    CLI_PTR_TO_U4 (args[0]));
            break;

        case CLI_RIP6_PEER_FLTR_STATUS_SHOW:
            i4RetStatus = ShowRIP6PeerFilterStatus (CliHandle);
            break;

        case CLI_RIP6_TRIG_UPDATE_INTERVAL_SHOW:
            i4RetStatus = RIP6TrigUpdateIntervalShow (CliHandle);
            break;

        case CLI_RIP6_FILTER:
            if (args[0] == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            tempIp6addr = str_to_ip6addr ((UINT1 *) args[0]);
            if (tempIp6addr == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMCPY (u1PeerAddr, tempIp6addr, RIP6_ADDR_BYTES);
            i4RetStatus = Rip6AddDelFilterAddr (CliHandle, u1PeerAddr,
                                                (UINT1) CLI_PTR_TO_U4 (args[1]),
                                                (UINT1)
                                                CLI_PTR_TO_U4 (args[2]));
            break;

        case CLI_RIP6_DISTRIBUTE_LIST:
            i4RetStatus = Rip6CliSetDistribute (CliHandle, (UINT1 *) args[0],
                                                (UINT1) CLI_PTR_TO_U4 (args[1]),
                                                (UINT1)
                                                CLI_PTR_TO_U4 (args[2]));
            break;

        case CLI_RIP6_ROUTE_DISTANCE:
        {
            /* args[0] - distance value (1-255)
             * args[1] - route map name (optional)
             */
            i4RetStatus =
                Rip6SetRouteDistance (CliHandle, *(INT4 *) args[0], (UINT1 *)
                                      args[1]);
        }
            break;

        case CLI_RIP6_NO_ROUTE_DISTANCE:
        {
            /* args[0] - route map name (optional)
             */
            i4RetStatus = Rip6SetNoRouteDistance (CliHandle, (UINT1 *) args[0]);
        }
            break;

        default:
            CliPrintf (CliHandle, "\r% Invalid Command\r\n");
            i4RetStatus = CLI_FAILURE;
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrorCode) == CLI_SUCCESS))
    {
        if ((u4ErrorCode > 0) && (u4ErrorCode < CLI_RIP6_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", Rip6CliErrString[u4ErrorCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    RIP6_TASK_UNLOCK ();
    CliUnRegisterLock (CliHandle);

    return i4RetStatus;

}

/***************************************************************************/
/*   Function Name : Rip6EnableRouter                                       */
/*                                                                         */
/*   Description   : To enter the router configuration mode                 */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
Rip6EnableRouter (tCliHandle CliHandle)
{
    UINT1               au1IfName[16];
    INT4                i4GetVal = 0;
    UNUSED_PARAM (CliHandle);

#ifdef IP6_WANTED
    /* Check whether IP forwarding is enabled */
    nmhGetFsMIStdIpv6IpForwarding (VCM_DEFAULT_CONTEXT, &i4GetVal);
#endif
    if (i4GetVal != RIP6_INITIALIZE_ONE)
    {
        CLI_SET_ERR (CLI_RIP6_IP6_FORWAD_DIS);
        return (CLI_FAILURE);
    }

    /* Enter the router configuration mode */

    SPRINTF ((CHR1 *) au1IfName, "%s", CLI_RIP6_ROUTER_MODE);

    /* Return the router configuration prompt */
    CliChangePath ((CHR1 *) au1IfName);
    gu4Rip6AdminStatus = RIP6_TRUE;
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : Rip6DisableRouter                                      */
/*                                                                         */
/*   Description   :To disable RIP6 on all the interfaces                   */
/*                                                                         */
/*   Input(s)      :  1. CliHandle - CLI context ID                        */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
Rip6DisableRouter (tCliHandle CliHandle)
{
    INT4                i4Rip6IfIndex;
    UINT4               u4ErrorCode;
    INT4                i4Rip6PrevIfIndex;

    if (nmhGetFirstIndexFsMIrip6RipIfTable (0, &i4Rip6PrevIfIndex)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP6_INSTANCE_DOWN);
        return CLI_FAILURE;
    }
    do
    {
        i4Rip6IfIndex = i4Rip6PrevIfIndex;
        if (nmhTestv2FsMIrip6RipIfProtocolEnable (&u4ErrorCode,
                                                  0,
                                                  i4Rip6IfIndex,
                                                  CLI_RIP6_DISABLE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetFsMIrip6RipIfProtocolEnable (0,
                                               i4Rip6IfIndex,
                                               CLI_RIP6_DISABLE) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;

        }

    }
    while (nmhGetNextIndexFsMIrip6RipIfTable (0, i4Rip6IfIndex,
                                              &i4Rip6PrevIfIndex) ==
           SNMP_SUCCESS);
    Rip6RemoveAllRtLearnt ();
    gu4Rip6AdminStatus = RIP6_FALSE;
    return (CLI_SUCCESS);
}

/*********************************************************************
*  Function Name : Rip6EnableDisableRip6If
*  Description   : Configuration of Ripng Over Specific Interface
*  Input(s)      :  1. CliHandle - CLI context ID
*                   2. u4Rip6IfIndex - Rip6 Interface 
*                   3. u1Status - Rip6 Enable/Disable 
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE.
*********************************************************************/
INT4
Rip6EnableDisableRip6If (tCliHandle CliHandle, UINT4 u4Rip6IfIndex,
                         UINT1 u1Status)
{
    UINT4               u4ErrorCode, u4InstId;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((u4Rip6IfIndex < RIP6_INITIALIZE_ONE) || (u4Rip6IfIndex > u4MaxIfs))
    {
        CLI_SET_ERR (CLI_RIP6_INV_RIP6IF);
        return CLI_FAILURE;
    }

    u4InstId = 0;

    if (nmhTestv2FsMIrip6RipIfProtocolEnable (&u4ErrorCode,
                                              u4InstId,
                                              u4Rip6IfIndex,
                                              u1Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIrip6RipIfProtocolEnable (u4InstId,
                                           u4Rip6IfIndex,
                                           u1Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6EnablePoisonReverse
*  Description   : Configuration of PoisonReverse Over Specific 
*                   Interface
*  Input(s)      :  1. CliHandle - CLI context ID
*                   2. u4Rip6IfIndex - Rip6 Interface
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE. 
*********************************************************************/
INT4
Rip6EnableDisablePoisonReverse (tCliHandle CliHandle, UINT4 u4Rip6IfIndex)
{
    UINT4               u4ErrorCode;
    UINT2               u2ProfIndex;
    UINT4               u4InstId;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((u4Rip6IfIndex < RIP6_INITIALIZE_ONE) || (u4Rip6IfIndex > u4MaxIfs))
    {
        CLI_SET_ERR (CLI_RIP6_INV_RIP6IF);
        return CLI_FAILURE;
    }

    u4InstId = 0;

    if (Rip6IfEntryExists (u4Rip6IfIndex) == RIP6_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP6_INV_ENTRY);
        return CLI_FAILURE;
    }

    u2ProfIndex = RIP6_GET_INST_IF_ENTRY (u4InstId, u4Rip6IfIndex)->u2ProfIndex;

    if (nmhTestv2FsMIrip6RipProfileHorizon (&u4ErrorCode, u4InstId,
                                            u2ProfIndex,
                                            RIP6_POISON_REVERSE) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIrip6RipProfileHorizon (u4InstId, u2ProfIndex,
                                         RIP6_POISON_REVERSE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6EnableSplitHorizon
*  Description   : Configuration of Split horizon Over Specific 
*                  Interface
*  Input(s)      :  1. CliHandle - CLI context ID
*                   2. u4Rip6IfIndex - Rip6 Interface
*  Output(s)     : CLI_SUCCESS/CLI_FAILURE.
*  Return Values : None.
*********************************************************************/
INT4
Rip6EnableSplitHorizon (tCliHandle CliHandle, UINT4 u4Rip6IfIndex)
{
    UINT4               u4ErrorCode, u4InstId;
    UINT2               u2ProfIndex;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((u4Rip6IfIndex < RIP6_INITIALIZE_ONE) || (u4Rip6IfIndex > u4MaxIfs))
    {
        CLI_SET_ERR (CLI_RIP6_INV_RIP6IF);
        return CLI_FAILURE;
    }

    u4InstId = 0;

    if (Rip6IfEntryExists (u4Rip6IfIndex) == RIP6_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP6_INV_ENTRY);
        return CLI_FAILURE;
    }

    u2ProfIndex = RIP6_GET_INST_IF_ENTRY (u4InstId, u4Rip6IfIndex)->u2ProfIndex;

    if (nmhTestv2FsMIrip6RipProfileHorizon
        (&u4ErrorCode, u4InstId, u2ProfIndex, RIP6_SPLIT_HORIZONING) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIrip6RipProfileHorizon (u4InstId, u2ProfIndex,
                                         RIP6_SPLIT_HORIZONING) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6EnableNoHorizon
*  Description   : Configuration of no horizon Over Specific Interface
*  Input(s)      :  1. CliHandle - CLI context ID
*                   2. u4Rip6IfIndex - Rip6 Interface
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE.
*********************************************************************/

INT4
Rip6EnableNoHorizon (tCliHandle CliHandle, UINT4 u4Rip6IfIndex)
{
    UINT4               u4ErrorCode, u4InstId;
    UINT2               u2ProfIndex;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((u4Rip6IfIndex < RIP6_INITIALIZE_ONE) || (u4Rip6IfIndex > u4MaxIfs))
    {
        CLI_SET_ERR (CLI_RIP6_INV_RIP6IF);
        return CLI_FAILURE;
    }

    u4InstId = 0;

    if (Rip6IfEntryExists (u4Rip6IfIndex) == RIP6_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP6_INV_ENTRY);
        return CLI_FAILURE;
    }

    u2ProfIndex = RIP6_GET_INST_IF_ENTRY (u4InstId, u4Rip6IfIndex)->u2ProfIndex;

    if (nmhTestv2FsMIrip6RipProfileHorizon
        (&u4ErrorCode, u4InstId, u2ProfIndex, RIP6_NO_HORIZONING) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsMIrip6RipProfileHorizon (u4InstId, u2ProfIndex,
                                         RIP6_NO_HORIZONING) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6EnableDisableDefRouteAdvt
*  Description   : Enable/Disable Default Route Advt Over Specific RIPng Interface
*  Input(s)      :  1. CliHandle - CLI context ID
*                   2. u4Rip6IfIndex - Rip6 Interface
*                   3. u1Status - Enable/Disable
*  Output(s)     : None. 
*  Return Values : CLI_FAILURE/CLI_SUCCESS.
*********************************************************************/
INT4
Rip6EnableDisableDefRouteAdvt (tCliHandle CliHandle, UINT4 u4Rip6IfIndex,
                               UINT1 u1Status)
{
    UINT4               u4ErrorCode, u4InstId;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((u4Rip6IfIndex < RIP6_INITIALIZE_ONE) || (u4Rip6IfIndex > u4MaxIfs))
    {
        CLI_SET_ERR (CLI_RIP6_INV_RIP6IF);
        return CLI_FAILURE;
    }

    u4InstId = 0;

    if (nmhTestv2FsMIrip6RipIfDefRouteAdvt (&u4ErrorCode,
                                            u4InstId,
                                            u4Rip6IfIndex,
                                            u1Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMIrip6RipIfDefRouteAdvt (u4InstId,
                                         u4Rip6IfIndex,
                                         u1Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;

    }

    return CLI_SUCCESS;
}

/*********************************************************************
*   Function Name : Rip6Metric                                
*   Description   : Setting Route Propagate for static and dynamic 
*                   Routes 
*  Input(s)      :  1. CliHandle - CLI context ID
*                   2. u4Rip6IfIndex - Rip6 Interface
*                   3. i4Metric - Metric
*   Output(s)     : None.                                                   
*   Return Values : CLI_FAILURE/CLI_SUCCESS.                                             
* *******************************************************************/
INT4
Rip6Metric (tCliHandle CliHandle, UINT4 u4Rip6IfIndex, INT4 i4Metric)
{
    UINT4               u4InstId;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((u4Rip6IfIndex < RIP6_INITIALIZE_ONE) || (u4Rip6IfIndex > u4MaxIfs))
    {
        CLI_SET_ERR (CLI_RIP6_INV_RIP6IF);
        return CLI_FAILURE;
    }

    u4InstId = 0;

    if (i4Metric < RIP6_INITIALIZE_ONE)
    {
        CLI_SET_ERR (CLI_RIP6_INV_METRIC);
        return CLI_FAILURE;
    }
    if (i4Metric > RIP6_MAX_COST)
    {
        CLI_SET_ERR (CLI_RIP6_INV_METRIC);
        return CLI_FAILURE;
    }
    if (nmhSetFsMIrip6RipIfCost (u4InstId, u4Rip6IfIndex, i4Metric)
        == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/********************************************************************
*   Function Name : Rip6Redistribute
*   Description   : Enable the Redistribution non-RIP routes into
*                   RIP Domain.
*  Input(s)      :  1. CliHandle - CLI context ID
*                   2. u4ProtoMask - Protocol identifier
                    3. i1DefMetric - default metric in redistributed routes.
*                   4. u1Value - Enable/Disable
*   Output(s)     : None                                                  
*   Return Values : CLI_FAILURE or CLI_SUCCESS .                                
********************************************************************/
INT4
Rip6Redistribute (tCliHandle CliHandle, UINT4 u4ProtoMask,
                  INT4 i4DefMetric, UINT1 u1Value, UINT1 *pu1RouteMapName)
{
    INT4                i4RetVal;
    UNUSED_PARAM (CliHandle);

    i4RetVal = Rip6ConfigRedistribute ((INT4) u4ProtoMask,
                                       u1Value, pu1RouteMapName);

    if (i4RetVal == RIP6_PROTO_REDIS_NOT_SUPPORTED)
    {
        return CLI_FAILURE;
    }

    if (i4RetVal == SNMP_SUCCESS)
    {
        if (u1Value == CLI_RIP6_ENABLE)
        {
            i4RetVal = Rip6ConfigRedisDefMetric (i4DefMetric);

            if (i4RetVal == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
            else
            {
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/********************************************************************
*  Function Name  : RIP6AddOrDelPeerFilter  
*  Description    : determines whether to allow or deny responses 
*              from Peers configured in Peer Table. 
*  Input(s)       : CliHandle   -  CLI context ID
*                   u4ErrorCode -  Error code. 
*                   u4Status    -  Enable/Disable.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS/CLI_FAILURE.                                        
********************************************************************/
INT4
RIP6AddOrDelPeerFilter (tCliHandle CliHandle, UINT4 u4Status)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2Fsrip6PeerFilter (&u4ErrorCode, u4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    if (nmhSetFsrip6PeerFilter (u4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/********************************************************************
*  Function Name  : ShowRIP6PeerFilterStatus
*  Description    : Displays the status of the Peer filter
*  Input(s)       : CliHandle   -  CLI context ID
*  Output(s)      : None.                                                  
*  Return Values  : CLI_SUCCESS/CLI_FAILURE.                                        
********************************************************************/
INT4
ShowRIP6PeerFilterStatus (tCliHandle CliHandle)
{
    INT4                i4Status = 0;

    nmhGetFsrip6PeerFilter (&i4Status);

    if (i4Status == 1)
    {
        CliPrintf (CliHandle, "Peer Filter Status : Enabled\r\n");
    }
    else if (i4Status == 2)
    {
        CliPrintf (CliHandle, "Peer Filter Status : Disabled\r\n");
    }
    return CLI_SUCCESS;
}

/********************************************************************
*  Function Name  : RIP6TrigUpdateIntervalShow
*  Description    : Displays the time interval in seconds by which 
*                  further triggered updates are delayed after one 
*              triggered update is sent.
*  Input(s)       : CliHandle   -  CLI context ID
*
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS/CLI_FAILURE.                                        
********************************************************************/
INT4
RIP6TrigUpdateIntervalShow (tCliHandle CliHandle)
{
    INT4                i4Interval = 0;

    if (nmhGetFsrip6RipProfileTrigDelayTime (gu4InstanceIndex,
                                             &i4Interval) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "Triggered Delay Time : %d\r\n", i4Interval);
        return CLI_SUCCESS;
    }
    return CLI_FAILURE;
}

/********************************************************************
*  Function Name  : RIP6ProfileTrigDelayTime 
*  Description    : "Indicates the time interval in seconds by which further
*                   triggered updates are delayed after one triggered update
*                                    is sent." 
*  Input(s)       : CliHandle         -  CLI context ID
*                   u4TrigDelayTimer  -  Delay time interval.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS/CLI_FAILURE.                                        
********************************************************************/
INT4
RIP6ProfileTrigDelayTime (tCliHandle CliHandle, UINT4 u4TrigDelayTimer)
{
    UINT4               u4ErrCode = 0;

    if (nmhTestv2Fsrip6RipProfileTrigDelayTime (&u4ErrCode,
                                                gu4InstanceIndex,
                                                u4TrigDelayTimer) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsrip6RipProfileTrigDelayTime (gu4InstanceIndex,
                                             u4TrigDelayTimer) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/********************************************************************
*  Function Name  : Rip6AddDelFilterAddr
*  Description    : Filter Network in Routing Updates received or sent. 
*  Input(s)       : 1. CliHandle   -  CLI context ID
*                   2. pu1Ip6Addr  -  Ipv6 Network Address  
*                   3. u1Fltr      -  in/out. 
*                   4. u1Status    -  Enable/Disable.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS/CLI_FAILURE.                                        
********************************************************************/

INT4
Rip6AddDelFilterAddr (tCliHandle CliHandle, UINT1 *pu1Ip6Addr,
                      UINT1 u1Fltr, UINT1 u1Status)
{
    UINT4               u4ErrorCode;
    tSNMP_OCTET_STRING_TYPE Addr;
    tSNMP_OCTET_STRING_TYPE FirstPeerAddr;
    tSNMP_OCTET_STRING_TYPE FirstAdvAddr;
    UINT1               u1TempAddr[16];
    UINT1               au1FirstPeerAddr[RIP6_INITIALIZE_SIXTEEN];
    UINT1               au1FirstAdvAddr[RIP6_INITIALIZE_SIXTEEN];

    Addr.pu1_OctetList = &u1TempAddr[0];
    MEMCPY (Addr.pu1_OctetList, pu1Ip6Addr, RIP6_ADDR_BYTES);
    Addr.i4_Length = RIP6_ADDR_BYTES;

    MEMSET (&au1FirstPeerAddr, RIP6_INITIALIZE_ZERO, RIP6_INITIALIZE_SIXTEEN);
    MEMSET (&au1FirstAdvAddr, RIP6_INITIALIZE_ZERO, RIP6_INITIALIZE_SIXTEEN);

    FirstPeerAddr.pu1_OctetList = au1FirstPeerAddr;
    FirstPeerAddr.i4_Length = RIP6_INITIALIZE_SIXTEEN;

    FirstAdvAddr.pu1_OctetList = au1FirstAdvAddr;
    FirstAdvAddr.i4_Length = RIP6_INITIALIZE_SIXTEEN;

    if (u1Fltr == RIP6_PEER_FILTER)
    {
        if (u1Status == CLI_RIP6_ENABLE)
        {
            if (nmhGetFirstIndexFsrip6RipPeerTable (&FirstPeerAddr)
                == SNMP_FAILURE)
            {

                if (nmhTestv2Fsrip6PeerFilter (&u4ErrorCode,
                                               CLI_RIP6_ENABLE) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsrip6PeerFilter (CLI_RIP6_ENABLE);
            }
        }

        if (nmhValidateIndexInstanceFsrip6RipPeerTable (&Addr) == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RIP6_INV_PEER_ADDR);
            return CLI_FAILURE;
        }

        if (nmhTestv2Fsrip6RipPeerEntryStatus (&u4ErrorCode,
                                               &Addr, u1Status) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsrip6RipPeerEntryStatus (&Addr, u1Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (u1Status == CLI_RIP6_DISABLE)
        {
            if (nmhGetFirstIndexFsrip6RipPeerTable (&FirstPeerAddr)
                == SNMP_FAILURE)
            {

                if (nmhTestv2Fsrip6PeerFilter (&u4ErrorCode,
                                               CLI_RIP6_DISABLE)
                    == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsrip6PeerFilter (CLI_RIP6_DISABLE);
            }
        }
    }

    if (u1Fltr == RIP6_ADV_FILTER)
    {

        if (u1Status == CLI_RIP6_ENABLE)
        {
            if (nmhGetFirstIndexFsrip6RipAdvFilterTable (&FirstAdvAddr)
                == SNMP_FAILURE)
            {

                if (nmhTestv2Fsrip6AdvFilter (&u4ErrorCode,
                                              CLI_RIP6_ENABLE) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsrip6AdvFilter (CLI_RIP6_ENABLE);
            }
        }

        if (nmhValidateIndexInstanceFsrip6RipAdvFilterTable (&Addr)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (CLI_RIP6_INV_ADV_ADDR);
            return CLI_FAILURE;
        }

        if (nmhTestv2Fsrip6RipAdvFilterStatus (&u4ErrorCode,
                                               &Addr, u1Status) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhSetFsrip6RipAdvFilterStatus (&Addr, u1Status) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

        if (u1Status == CLI_RIP6_DISABLE)
        {
            if (nmhGetFirstIndexFsrip6RipAdvFilterTable (&FirstAdvAddr)
                == SNMP_FAILURE)
            {

                if (nmhTestv2Fsrip6AdvFilter (&u4ErrorCode,
                                              CLI_RIP6_DISABLE) == SNMP_FAILURE)
                {
                    return CLI_FAILURE;
                }

                nmhSetFsrip6AdvFilter (CLI_RIP6_DISABLE);
            }
        }

    }
    return CLI_SUCCESS;
}

/********************************************************************
*  Function Name  : Rip6CliSetDistribute
*  Description    : This Routine Enable or Disable route map filtering.
*  Input(s)       : 1. CliHandle        -  CLI context ID
*                   2. pu1RouteMapName  -  Route map name
*                   3. u4Seqno          -  Sequence number
*                   4. u1Fltr           -  in/out.
*                   5. u1Status         -  Enable/Disable.
*   Output(s)     : None.
*   Return Values : CLI_SUCCESS/CLI_FAILURE.
********************************************************************/
INT4
Rip6CliSetDistribute (tCliHandle CliHandle, UINT1 *pu1RMapName,
                      UINT1 u1Fltr, UINT1 u1Status)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = CLI_RIP6_RMAP_ASSOC_FAILED;
    UINT1               u1FilterType;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);

    if (u1Fltr == CLI_RIP6_IN_FILTER)
    {
        u1FilterType = FILTERING_TYPE_DISTRIB_IN;
    }
    else if (u1Fltr == CLI_RIP6_OUT_FILTER)
    {
        u1FilterType = FILTERING_TYPE_DISTRIB_OUT;
    }
    else
    {
        CLI_SET_ERR (u4CliError);
        return (CLI_FAILURE);
    }

    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);

        if (CLI_RIP6_ENABLE == u1Status)
        {
            if (nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                        u1FilterType,
                                                        &i4RowStatus) !=
                SNMP_SUCCESS)
            {
                if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, u1FilterType,
                     CREATE_AND_WAIT) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                                u1FilterType,
                                                                CREATE_AND_WAIT)
                        == SNMP_SUCCESS)
                    {
                        u4ErrorCode = SNMP_ERR_NO_ERROR;
                    }
                }
            }
            else
            {
                u4ErrorCode = SNMP_ERR_NO_ERROR;
            }

            if (SNMP_ERR_NO_ERROR == u4ErrorCode)
            {
                if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, u1FilterType,
                     ACTIVE) == SNMP_SUCCESS)
                {
                    if (nmhSetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                                u1FilterType,
                                                                ACTIVE) ==
                        SNMP_SUCCESS)
                    {
                        return CLI_SUCCESS;
                    }
                }
            }
        }
        else if (CLI_RIP6_DISABLE == u1Status)
        {
            if (nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                        u1FilterType,
                                                        &i4RowStatus) ==
                SNMP_SUCCESS)
            {
                if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                    (&u4ErrorCode, &RouteMapName, u1FilterType,
                     DESTROY) == SNMP_SUCCESS)
                {
                    nmhSetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                            u1FilterType,
                                                            DESTROY);
                    return CLI_SUCCESS;
                }
            }
        }
    }

    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/*********************************************************************
*   Function Name : Rip6Debug                                
*   Description   : Enabling RIP6 Debug Trace 
*   Input(s)      : 1. CliHandle - Cli Context ID.
*                   2. u4DbgFlag - with the Command,followed by the user 
*                      inputs.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS                                            
* *******************************************************************/

INT4
Rip6Debug (tCliHandle CliHandle, UINT4 u4DbgFlag)
{

    switch (u4DbgFlag)
    {
        case RIP6_DATA_PATH:
            gu4Rip6DbgMap = RIP6_DATA_PATH_TRC;
            break;
        case RIP6_CONTROL_PLANE:
            gu4Rip6DbgMap = RIP6_CTRL_PLANE_TRC;
            break;
        case RIP6_ALL_TRC:
            gu4Rip6DbgMap = RIP6_DATA_CTRL_TRC;
            break;
        case RIP6_NO_TRC:
            gu4Rip6DbgMap = RIP6_NOT_TRC;
            break;
        default:
            CliPrintf (CliHandle, "%%Unable to Set Trace\r\n");
            break;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*   Function Name : Rip6FilterTableShow                       
*   Description   : Displaying the Filter Table. 
*   Input(s)      : CliHandle - Cli Context ID.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS/CLI_FAILURE.                               
* *******************************************************************/

INT4
Rip6FilterTableShow (tCliHandle CliHandle)
{
    INT1                i1ReachedEOTable = 0;
    INT1                i1PeerFilterEmpty = 0;
    INT1                i1AdvFilterEmpty = 0;
    tSNMP_OCTET_STRING_TYPE Rip6PeerAddr;
    tSNMP_OCTET_STRING_TYPE Rip6NextPeerAddr;
    INT4                i4InstanceId = 0;
    tSNMP_OCTET_STRING_TYPE Rip6AdvAddr;
    tSNMP_OCTET_STRING_TYPE Rip6NextAdvAddr;
    UINT1               u1TempAddr[16];
    UINT1               u1TempNextAddr[16];
    Rip6PeerAddr.pu1_OctetList = &u1TempAddr[0];
    Rip6PeerAddr.i4_Length = 16;

    Rip6NextPeerAddr.pu1_OctetList = &u1TempNextAddr[0];
    Rip6NextPeerAddr.i4_Length = 16;

    if (nmhGetFirstIndexFsMIrip6RipPeerTable (i4InstanceId,
                                              &Rip6PeerAddr) == SNMP_SUCCESS)
    {

        CliPrintf (CliHandle, "             Filter Address                    "
                   "FilterType\r\n");
        CliPrintf (CliHandle, "             **************                    "
                   "**********\r\n\r\n");

        do
        {
            if (nmhGetNextIndexFsMIrip6RipPeerTable ((UINT4) i4InstanceId,
                                                     &Rip6PeerAddr,
                                                     &Rip6NextPeerAddr) ==
                SNMP_SUCCESS)
            {

                CliPrintf (CliHandle, "             %-35s      IN  \r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) Rip6PeerAddr.
                                         pu1_OctetList));

                MEMCPY (Rip6PeerAddr.pu1_OctetList,
                        Rip6NextPeerAddr.pu1_OctetList, RIP6_ADDR_BYTES);
            }
            else
            {
                CliPrintf (CliHandle, "             %-35s      IN  \r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) Rip6PeerAddr.
                                         pu1_OctetList));

                i1ReachedEOTable = 1;
            }
        }
        while (i1ReachedEOTable != RIP6_INITIALIZE_ONE);

    }
    else
    {
        i1PeerFilterEmpty = RIP6_INITIALIZE_ONE;
    }

    i1ReachedEOTable = 0;
    Rip6AdvAddr.pu1_OctetList = &u1TempAddr[0];
    Rip6AdvAddr.i4_Length = 16;
    Rip6NextAdvAddr.pu1_OctetList = &u1TempNextAddr[0];
    Rip6NextAdvAddr.i4_Length = 16;

    if (nmhGetFirstIndexFsMIrip6RipAdvFilterTable (i4InstanceId,
                                                   &Rip6AdvAddr) ==
        SNMP_SUCCESS)
    {

        if (i1PeerFilterEmpty == RIP6_INITIALIZE_ONE)
        {
            CliPrintf (CliHandle, "             Filter Address               "
                       "     FilterType\r\n");
            CliPrintf (CliHandle, "             **************               "
                       "     **********\n\r\n");
        }

        do
        {
            if (nmhGetNextIndexFsMIrip6RipAdvFilterTable ((UINT4) i4InstanceId,
                                                          &Rip6AdvAddr,
                                                          &Rip6NextAdvAddr) ==
                SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "             %-35s      OUT  \r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) Rip6AdvAddr.
                                         pu1_OctetList));

                MEMCPY (Rip6AdvAddr.pu1_OctetList,
                        Rip6NextAdvAddr.pu1_OctetList, RIP6_ADDR_BYTES);
            }
            else
            {
                CliPrintf (CliHandle, "             %-35s      OUT  \r\n",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) Rip6AdvAddr.
                                         pu1_OctetList));

                i1ReachedEOTable = 1;
            }
        }
        while (i1ReachedEOTable != RIP6_INITIALIZE_ONE);
    }
    else
    {
        i1AdvFilterEmpty = RIP6_INITIALIZE_ONE;
    }

    if (i1AdvFilterEmpty == RIP6_INITIALIZE_ONE &&
        i1PeerFilterEmpty == RIP6_INITIALIZE_ONE)
    {
        CLI_SET_ERR (CLI_RIP6_FILTER_TBL_EMPTY);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;

}

/*********************************************************************
*  Function Name : Rip6IfStatsShow
*  Description   : Display the Rip6 Interface Statictics 
*  Input(s)      : CliHandle - Cli Context ID.
*  Output(s)     : None. 
*  Return Values : CLI_FAILURE/CLI_SUCCESS.
*********************************************************************/

INT4
Rip6IfStatsShow (tCliHandle CliHandle)
{
    INT4                i4Rip6IfStatsIndex;
    INT4                i4Rip6IfStatsNextIndex;
    INT4                i4Rip6IfProtocolEnable;
    INT1                i1ReachedEOTable = 0;
    INT1               *pi1IfName;
    UINT4               u4InstanceId;
    UINT4               u4InMessages = 0;
    UINT4               u4InReq = 0;
    UINT4               u4InRes = 0;
    UINT4               u4UnknownCmds = 0;
    UINT4               u4OtherVer = 0;
    UINT4               u4Discards = 0;
    UINT4               u4OutMessages = 0;
    UINT4               u4OutReq = 0;
    UINT4               u4OutRes = 0;
    UINT4               u4TrigUpdates = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    u4InstanceId = 0;

    pi1IfName = (INT1 *) &au1IfName[0];

    if (nmhGetFirstIndexFsMIrip6RipIfTable (u4InstanceId, &i4Rip6IfStatsIndex)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP6_INSTANCE_DOWN);
        return CLI_FAILURE;
    }

    if (nmhGetFsMIrip6RipIfProtocolEnable (u4InstanceId, i4Rip6IfStatsIndex,
                                           &i4Rip6IfProtocolEnable) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP6_NO_IF);
        return CLI_FAILURE;
    }

    u4InMessages =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4InMessages;
    u4InReq =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4InReq;
    u4InRes =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4InRes;
    u4UnknownCmds =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4UnknownCmds;
    u4OtherVer =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4OtherVer;
    u4Discards =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4Discards;
    u4OutMessages =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4OutMessages;
    u4OutReq =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4OutReq;
    u4OutRes =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4OutRes;
    u4TrigUpdates =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                i4Rip6IfStatsIndex)->stats.u4TrigUpdates;
    CfaCliGetIfName (i4Rip6IfStatsIndex, pi1IfName);

    if (i4Rip6IfProtocolEnable == RIP6_ENABLED)
    {
        CliPrintf (CliHandle, "        Interface Index     %s\r\n", pi1IfName);

        CliPrintf (CliHandle, "        *****************    ***\r\n");

        CliPrintf (CliHandle, "Rcvd  :\r\n");

        CliPrintf (CliHandle, "Messages         %d    Requests      %d "
                   "  Responses      %d\r\n", u4InMessages, u4InReq, u4InRes);

        CliPrintf (CliHandle, "UnknownCommds    %d    OtherVer    "
                   "  %d   Discards       %d\r\n", u4UnknownCmds, u4OtherVer,
                   u4Discards);
        CliPrintf (CliHandle, "Sent  :\r\n");
        CliPrintf (CliHandle, "Messages         %d    Requests   "
                   "   %d   Responses      %d\r\n", u4OutMessages, u4OutReq,
                   u4OutRes);

        CliPrintf (CliHandle, "Trigger Updates  %d\r\n", u4TrigUpdates);
    }

    do
    {
        if (nmhGetNextIndexFsMIrip6RipIfTable (u4InstanceId, i4Rip6IfStatsIndex,
                                               &i4Rip6IfStatsNextIndex) ==
            SNMP_SUCCESS)
        {

            if (nmhGetFsMIrip6RipIfProtocolEnable (u4InstanceId,
                                                   i4Rip6IfStatsNextIndex,
                                                   &i4Rip6IfProtocolEnable) ==
                SNMP_SUCCESS)
            {

                u4InMessages = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4InMessages;
                u4InReq = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4InReq;
                u4InRes = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4InRes;
                u4UnknownCmds = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4UnknownCmds;
                u4OtherVer = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4OtherVer;
                u4Discards = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4Discards;
                u4OutMessages = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4OutMessages;
                u4OutReq = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4OutReq;
                u4OutRes = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4OutRes;
                u4TrigUpdates = RIP6_GET_INST_IF_ENTRY
                    (u4InstanceId, i4Rip6IfStatsNextIndex)->stats.u4TrigUpdates;
                CfaCliGetIfName (i4Rip6IfStatsNextIndex, pi1IfName);

                if (i4Rip6IfProtocolEnable == RIP6_ENABLED)
                {
                    CliPrintf (CliHandle, "\n    Interface Index     %s  \r\n",
                               pi1IfName);

                    CliPrintf (CliHandle, "    *****************    ***\r\n");

                    CliPrintf (CliHandle, "Rcvd  :\r\n");

                    CliPrintf (CliHandle, "Messages         %d  "
                               "  Requests       %d   Responses     %d\r\n",
                               u4InMessages, u4InReq, u4InRes);

                    CliPrintf (CliHandle, "UnknownCommds    %d    OtherVer "
                               "      %d   Discards      %d\r\n",
                               u4UnknownCmds, u4OtherVer, u4Discards);

                    CliPrintf (CliHandle, "Sent  :\r\n");

                    CliPrintf (CliHandle, "Messages         %d    Requests "
                               "      %d   Responses     %d\r\n",
                               u4OutMessages, u4OutReq, u4OutRes);

                    CliPrintf (CliHandle, "Trigger Updates  %d\r\n",
                               u4TrigUpdates);
                }
            }
            i4Rip6IfStatsIndex = i4Rip6IfStatsNextIndex;

        }
        else
        {
            i1ReachedEOTable = 1;
        }
    }
    while (i1ReachedEOTable != RIP6_INITIALIZE_ONE);
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6ShowGetRoute
*  Description   : Retrives the Route Entries to be displayed. 
*  Input(s)      : pu1Buffer - non-NULL buffer, where the routes are returned.
*                              This buffer must be big enough to hold
*                              atleast one route.
*                  u4BufLen - Length of pu1Buffer
*                  pCookie  - information about route to be fetched are
*                             stored.
*  Output(s)     : pCookie  - information about next route to be fetched are
*                             stored.
*  Return Values : RIP6_SUCCESS/RIP6_FAILURE
*********************************************************************/
PRIVATE INT4
Rip6ShowGetRoute (UINT1 *pu1Buffer, UINT4 u4BufLen,
                  tShowRip6RtCookie * pCookie, UINT4 u4InstanceId)
{
    tShowRip6Route     *pShowRip6Route = NULL;
    tShowRip6Rt        *pShowRip6Rt = NULL;
    tRip6RtEntry       *pRip6Route = NULL;
    tRip6RtEntry       *pNextRt = NULL;
    tRip6RtEntry       *pNextProtoRt = NULL;
    tRip6RtEntry        Rip6RtEntry;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;

    UINT4               u4NoOfRoute = 0;

    pShowRip6Rt = (tShowRip6Rt *) (VOID *) pu1Buffer;
    pShowRip6Route = &pShowRip6Rt->aRoutes[0];

    MEMSET (pu1Buffer, 0, u4BufLen);

    /* check for valid u4Cookie; if cookie is invalid, then this is a fresh
     * call to Ip6ShowGetRoute - start from the begining. If cookie is
     * valid, then use to cookie to continue from where we left last time
     */
    if (pCookie->i1Protocol != 0)
    {
        /* valid cookie. Get the Route matching the Cookie information */
        MEMSET (&Rip6RtEntry, 0, sizeof (tRip6RtEntry));
        Ip6AddrCopy (&Rip6RtEntry.dst, &pCookie->PrefixAddr);
        Rip6RtEntry.u1Prefixlen = pCookie->u1PrefixLen;
        Rip6RtEntry.i1Proto = 0;

        if (pCookie->i1Protocol == RIPNG)
        {
            Rip6TrieBestEntry (&Rip6RtEntry.dst,
                               RIP6_DYNAMIC_LOOKUP,
                               Rip6RtEntry.u1Prefixlen,
                               u4InstanceId, &pRip6Route, &pRibNode);
        }
        else
        {
            Rip6TrieBestEntry (&Rip6RtEntry.dst,
                               RIP6_STATIC_LOOKUP,
                               Rip6RtEntry.u1Prefixlen,
                               u4InstanceId, &pRip6Route, &pRibNode);
        }
        if (pRip6Route == NULL)
        {
            /* Given route is not present. */
            CLI_SET_ERR (CLI_RIP6_ROUTE_EMPTY);
            return RIP6_FAILURE;
        }
    }
    else
    {
        /* invalid cookie; get the first route entry */
        if (Rip6TrieGetFirstEntry (&pRip6Route, &pRibNode, u4InstanceId) ==
            RIP6_FAILURE)
        {
            /* No IPV6 Route present. */
            MEMSET (pCookie, 0, sizeof (tShowRip6RtCookie));
            CLI_SET_ERR (CLI_RIP6_ROUTE_EMPTY);
            return RIP6_FAILURE;
        }
    }

    /* if u4NeigExist flag is set to 1 then neighbors exist in the database;
     * else no neighbors exist in the database - just return.
     */
    if (pRip6Route)
    {
        /* calculate the number of neighbors that could be accomodated into this
         * buffer
         */
        u4NoOfRoute = (u4BufLen - ((sizeof (tShowRip6Rt)) -
                                   (sizeof (tShowRip6Route)))) /
            (sizeof (tShowRip6Route));

        do
        {
            pNextProtoRt = pRip6Route->pNext;
            while (pRip6Route != NULL)
            {
                if (!u4NoOfRoute)
                {
                    /* we have run out of buffer; but we have more routes in the
                     * database to be returned. Hence set the cookie.
                     */
                    Ip6AddrCopy (&pCookie->PrefixAddr, &pRip6Route->dst);
                    Ip6AddrCopy (&pCookie->NextHop, &pRip6Route->nexthop);
                    pCookie->u1PrefixLen = pRip6Route->u1Prefixlen;
                    pCookie->i1Protocol = pRip6Route->i1Proto;
                    return RIP6_SUCCESS;
                }
                /* Store the route. */
                Ip6AddrCopy (&pShowRip6Route->PrefixAddr, &pRip6Route->dst);
                Ip6AddrCopy (&pShowRip6Route->NextHop, &pRip6Route->nexthop);
                pShowRip6Route->u1PrefixLen = pRip6Route->u1Prefixlen;
                pShowRip6Route->i1Protocol = pRip6Route->i1Proto;
                pShowRip6Route->u4IfIndex = pRip6Route->u4Index;
                pShowRip6Route->u1Metric = pRip6Route->u1Metric;
                pShowRip6Rt->u4NoOfRoutes++;
                u4NoOfRoute--;
                if (u4NoOfRoute)
                {
                    /* Increment the pShowRip6Route pointer to store the next
                     * R/oute information. */
                    pShowRip6Route++;
                }
                if (pNextProtoRt != NULL)
                {
                    pRip6Route = pNextProtoRt;
                    pNextProtoRt = pRip6Route->pNext;
                }
                else
                {
                    /* No more route for this prefix. */
                    break;
                }
            }

            if (Rip6TrieGetNextEntry
                (&pNextRt, pRibNode, &pNextRibNode,
                 u4InstanceId) == RIP6_FAILURE)
            {
                /* No more route present. Set the cookie to indicate it */
                MEMSET (pCookie, 0, sizeof (tShowRip6RtCookie));
                break;
            }
            pRip6Route = pNextRt;
            pRibNode = pNextRibNode;
            pNextRt = NULL;
            pNextRibNode = NULL;
        }
        while (pRip6Route != NULL);
    }
    return RIP6_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6RouteShow
*  Description   : Route Table Entries to be displayed. 
*  Input(s)      : CliHandle - Cli Context ID.
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE.
*********************************************************************/
INT4
Rip6RouteShow (tCliHandle CliHandle)
{
    UINT4               u4InstanceId;

    tShowRip6RtCookie   Rip6RtCookie;
    tShowRip6Rt        *pShowRip6Rt;
    tShowRip6Route     *pShowRip6Route;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    UINT1               au1RouteInfo[sizeof (tShowRip6Rt) +
                                     ((RIP6_SHOW_MAX_ROUTE -
                                       1) * sizeof (tShowRip6Route)) +
                                     (2 * sizeof (UINT4))];
    INT1               *pi1IfName;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4BufSize = 0;
    INT4                i4TotalRouteCnt = 0;
    INT4                i4RouteAge;
    UINT1               u1TempAddr[16];

    const CHR1         *au1Proto[] = {
        NULL,
        "other",
        "local",
        "static",
        "ndisc",
        "ripng",
        "ospf",
        "bgp",
        "irdp"
    };

    DestAddr.pu1_OctetList = &u1TempAddr[0];
    DestAddr.i4_Length = RIP6_ADDR_BYTES;

    /* Initialize the cookie */
    MEMSET (&Rip6RtCookie, 0, sizeof (tShowRip6RtCookie));

    /* Allocate Memory for Route Display. */
    u4BufSize = sizeof (tShowRip6Rt) +
        ((RIP6_SHOW_MAX_ROUTE - 1) * sizeof (tShowRip6Route));

    pi1IfName = (INT1 *) &au1IfName[0];

    u4InstanceId = 0;

    CliPrintf (CliHandle, "\nRIP local RIB \r\n");

    nmhGetFsrip6RouteCount (&i4TotalRouteCnt);

    CliPrintf (CliHandle, "\nNo of RIP6 Route Entries %d\r\n", i4TotalRouteCnt);

    for (;;)
    {
        MEMSET (au1RouteInfo, 0, u4BufSize);

        if (Rip6ShowGetRoute
            (au1RouteInfo, u4BufSize, &Rip6RtCookie,
             u4InstanceId) == RIP6_FAILURE)
        {
            /* Error in retrieving the routes. */
            return CLI_FAILURE;
        }

        pShowRip6Rt = (tShowRip6Rt *) (VOID *) au1RouteInfo;
        pShowRip6Route = &pShowRip6Rt->aRoutes[0];

        /* Display the Route info */
        while (pShowRip6Rt->u4NoOfRoutes != 0)
        {
            CliPrintf (CliHandle, "%s/%d, ",
                       Ip6PrintNtop ((tIp6Addr *) &
                                     (pShowRip6Route->PrefixAddr)),
                       pShowRip6Route->u1PrefixLen);

            CliPrintf (CliHandle, " metric %d,", pShowRip6Route->u1Metric);

            CliPrintf (CliHandle, " %s\r\n ",
                       au1Proto[pShowRip6Route->i1Protocol]);

            CfaCliGetIfName (pShowRip6Route->u4IfIndex, pi1IfName);

            CliPrintf (CliHandle, "     %s/%s, ", pi1IfName,
                       Ip6PrintNtop ((tIp6Addr *) & (pShowRip6Route->NextHop)));

            Ip6AddrCopy ((tIp6Addr *) (VOID *) DestAddr.pu1_OctetList,
                         (tIp6Addr *) (VOID *) &(pShowRip6Route->PrefixAddr));

            nmhGetFsMIrip6RipRouteAge (u4InstanceId,
                                       &DestAddr,
                                       (INT4) pShowRip6Route->u1PrefixLen,
                                       (INT4) pShowRip6Route->i1Protocol,
                                       (INT4) pShowRip6Route->u4IfIndex,
                                       &i4RouteAge);

            if (pShowRip6Route->i1Protocol != RIP6_LOCAL)
            {
                if (pShowRip6Route->u1Metric != RIP6_INFINITY)
                {
                    i4RouteAge =
                        (RIP6_DFLT_ROUTE_AGE - (i4RouteAge / OSIX_TPS));
                    CliPrintf (CliHandle, "expires in %d secs \r\n",
                               i4RouteAge);
                }
                else
                {
                    i4RouteAge = (RIP6_DFLT_GARBAGE_COLLECT_TIME -
                                  (i4RouteAge / OSIX_TPS));
                    CliPrintf (CliHandle, "expired, [advertise %d ]\r\n ",
                               i4RouteAge);
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }

            /* Go to Next Route */
            pShowRip6Rt->u4NoOfRoutes--;
            if (pShowRip6Rt->u4NoOfRoutes)
            {
                pShowRip6Route++;
            }
        }
        /* Check Cookie for to find more route still exist or over. */
        if (Rip6RtCookie.i1Protocol == 0)
        {
            break;
        }
        continue;
    }
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6ProfileShow
*  Description   : Display the Rip6 Profile Table 
*  Input(s)      : CliHandle - Cli Context ID.
*  Output(s)     : None. 
*  Return Values : CLI_SUCCESS/CLI_FAILURE.
*********************************************************************/
INT4
Rip6ProfileShow (tCliHandle CliHandle)
{
    UINT2               u2i;
    UINT4               u4InstanceId;
    UINT4               u4IfIndex;
    INT4                i4Rip6ProfileFirstIndex;
    INT4                i4Rip6ProfileStatus;
    INT4                i4Rip6ProfileHorizon;
    INT4                i4Rip6ProfileTrigtime;
    INT4                i4Rip6ProfileUpTime;
    INT4                i4Rip6ProfileRouteAge;
    INT4                i4Rip6ProfileGCTime;
    INT4                i4Rip6ProfileNextGCTime;
    INT4                i4Rip6ProfileNextStatus;
    INT4                i4Rip6ProfileNextHorizon;
    INT4                i4Rip6ProfileNextTrigtime;
    INT4                i4Rip6ProfileNextUpTime;
    INT4                i4Rip6ProfileNextRouteAge;
    INT4                i4Rip6ProfileNextIndex;
    INT4                i4Rip6Redistribute;
    INT4                i4Rip6RRDAdminStatus;
    INT1                i1ReachedEOTable = 0;
    INT1               *pi1IfName;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    tIp6Addr            rip6McastAddr;
    INT4                i4OperStatus;

    INT4                i4Rip6IfStatsIndex;
    INT4                i4Rip6IfProtocolEnable;
    INT1                i1Rip6Enable = 0;
    INT4                i4Rip6IfStatsNextIndex = 0;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    u4InstanceId = 0;

    pi1IfName = (INT1 *) &au1IfName[0];

    SET_ALL_RIP_ROUTERS (rip6McastAddr);

    /*Validation to check whether Rip6 enabled in any of the interface */

    if (nmhGetFirstIndexFsMIrip6RipIfTable (u4InstanceId, &i4Rip6IfStatsIndex)
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP6_INSTANCE_DOWN);
        return CLI_FAILURE;
    }

    if (nmhGetFsMIrip6RipIfProtocolEnable (u4InstanceId, i4Rip6IfStatsIndex,
                                           &i4Rip6IfProtocolEnable) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (CLI_RIP6_NO_IF);
        return CLI_FAILURE;
    }
    if (i4Rip6IfProtocolEnable == RIP6_ENABLED)
    {
        i1Rip6Enable = 1;
    }
    else
    {
        do
        {
            if (nmhGetNextIndexFsMIrip6RipIfTable
                (u4InstanceId, i4Rip6IfStatsIndex,
                 &i4Rip6IfStatsNextIndex) == SNMP_SUCCESS)
            {

                if (nmhGetFsMIrip6RipIfProtocolEnable (u4InstanceId,
                                                       i4Rip6IfStatsNextIndex,
                                                       &i4Rip6IfProtocolEnable)
                    == SNMP_SUCCESS)
                {
                    if (i4Rip6IfProtocolEnable == RIP6_ENABLED)
                    {
                        i1Rip6Enable = 1;
                        break;
                    }
                }
                i4Rip6IfStatsIndex = i4Rip6IfStatsNextIndex;
            }
            else
            {
                i1ReachedEOTable = 1;
            }
        }
        while (i1ReachedEOTable != RIP6_INITIALIZE_ONE);
    }
    if (i1Rip6Enable == 0)
    {
        CLI_SET_ERR (CLI_RIP6_NO_IF);
        return CLI_FAILURE;
    }

    if (nmhGetFirstIndexFsMIrip6RipProfileTable (u4InstanceId,
                                                 &i4Rip6ProfileFirstIndex) ==
        SNMP_FAILURE)
    {

        CLI_SET_ERR (CLI_RIP6_PROFILE_TBL_EMPTY);
        return CLI_FAILURE;

    }

    nmhGetFsMIrip6RipProfileStatus (u4InstanceId, i4Rip6ProfileFirstIndex,
                                    &i4Rip6ProfileStatus);

    nmhGetFsMIrip6RipProfileHorizon (u4InstanceId, i4Rip6ProfileFirstIndex,
                                     &i4Rip6ProfileHorizon);

    nmhGetFsMIrip6RipProfilePeriodicUpdTime (u4InstanceId,
                                             i4Rip6ProfileFirstIndex,
                                             &i4Rip6ProfileUpTime);

    nmhGetFsMIrip6RipProfileTrigDelayTime (u4InstanceId,
                                           i4Rip6ProfileFirstIndex,
                                           &i4Rip6ProfileTrigtime);

    nmhGetFsMIrip6RipProfileRouteAge (u4InstanceId, i4Rip6ProfileFirstIndex,
                                      &i4Rip6ProfileRouteAge);

    nmhGetFsMIrip6RipProfileGarbageCollectTime (u4InstanceId,
                                                i4Rip6ProfileFirstIndex,
                                                &i4Rip6ProfileGCTime);

    nmhGetFsrip6RRDProtoMaskForEnable (&i4Rip6Redistribute);

    nmhGetFsRip6RRDAdminStatus (&i4Rip6RRDAdminStatus);

    CliPrintf (CliHandle, "\nRIP ");
    CliPrintf (CliHandle, " port %d,", RIP6_PORT);
    CliPrintf (CliHandle, " multicast-group %s,",
               Ip6PrintNtop (&rip6McastAddr));
    CliPrintf (CliHandle, "Maximum paths is %d\r\n", RIP6_INFINITY);

    CliPrintf (CliHandle, "Updates every %d seconds;", i4Rip6ProfileUpTime);
    CliPrintf (CliHandle, " expire after %d\r\n", i4Rip6ProfileRouteAge);
    CliPrintf (CliHandle, "Garbage Collect after  %d seconds\r\n",
               i4Rip6ProfileGCTime);

    if (i4Rip6ProfileHorizon == RIP6_INITIALIZE_ONE)
    {
        CliPrintf (CliHandle, "Split Horizon is off;\r\n");
    }
    else if (i4Rip6ProfileHorizon == RIP6_INITIALIZE_TWO)
    {
        CliPrintf (CliHandle, "Split Horizon is on;\r\n");
    }
    else if (i4Rip6ProfileHorizon == RIP6_INITIALIZE_THREE)
    {
        CliPrintf (CliHandle, "Poison Reverse is on\r\n");
    }

    CliPrintf (CliHandle, "Interface:\r\n");

    for (u2i = RIP6_INITIALIZE_ONE; u2i <= u4MaxIfs; u2i++)
    {
        i4OperStatus = 0;
        nmhGetFsMIrip6RipIfOperStatus (0, u2i, &i4OperStatus);
        if (i4OperStatus == IP6_RIP_ENABLED_UP)
        {
            if (RIP6_GET_IF_ENTRY (u2i)->u2ProfIndex == i4Rip6ProfileFirstIndex)
            {
                u4IfIndex = RIP6_GET_IF_ENTRY (u2i)->u4Index;
                CfaCliGetIfName (u4IfIndex, pi1IfName);
                CliPrintf (CliHandle, "%s\r\n", pi1IfName);
            }
        }
    }

    CliPrintf (CliHandle, "Redistribution:\r\n");

    if (i4Rip6RRDAdminStatus == RIP6_RRD_ENABLE)
    {

        if ((i4Rip6Redistribute & RIP6_IMPORT_DIRECT) == RIP6_IMPORT_DIRECT)
        {
            CliPrintf (CliHandle, " Connected,");
        }
        if ((i4Rip6Redistribute & RIP6_IMPORT_STATIC) == RIP6_IMPORT_STATIC)
        {
            CliPrintf (CliHandle, " Static,");
        }
        if ((i4Rip6Redistribute & RIP6_IMPORT_BGP) == RIP6_IMPORT_BGP)
        {
            CliPrintf (CliHandle, " Bgp,");
        }
        if ((i4Rip6Redistribute & RIP6_IMPORT_OSPF) == RIP6_IMPORT_OSPF)
        {
            CliPrintf (CliHandle, " Ospf,");
        }
#ifdef ISIS_WANTED
        if ((i4Rip6Redistribute & RIP6_IMPORT_ISISL1L2))
        {
            if ((i4Rip6Redistribute & RIP6_IMPORT_ISISL1L2) ==
                RIP6_IMPORT_ISISL1L2)
            {
                CliPrintf (CliHandle, " isis level-1-2, ");
            }
            else if ((i4Rip6Redistribute & RIP6_IMPORT_ISISL1L2) ==
                     RIP6_IMPORT_ISISL1)
            {
                CliPrintf (CliHandle, " isis level-1,");
            }
            else if ((i4Rip6Redistribute & RIP6_IMPORT_ISISL1L2) ==
                     RIP6_IMPORT_ISISL2)
            {
                CliPrintf (CliHandle, " isis level-2,");
            }
        }
#endif
        CliPrintf (CliHandle, "Routes  Redistribution is enabled. \r\n");

    }
    else
    {
        CliPrintf (CliHandle, "None \r\n");
    }

    do
    {
        if (nmhGetNextIndexFsMIrip6RipProfileTable (u4InstanceId,
                                                    i4Rip6ProfileFirstIndex,
                                                    &i4Rip6ProfileNextIndex) ==
            SNMP_SUCCESS)
        {

            nmhGetFsMIrip6RipProfileStatus (u4InstanceId,
                                            i4Rip6ProfileNextIndex,
                                            &i4Rip6ProfileNextStatus);

            nmhGetFsMIrip6RipProfileHorizon (u4InstanceId,
                                             i4Rip6ProfileNextIndex,
                                             &i4Rip6ProfileNextHorizon);

            nmhGetFsMIrip6RipProfilePeriodicUpdTime (u4InstanceId,
                                                     i4Rip6ProfileNextIndex,
                                                     &i4Rip6ProfileNextUpTime);

            nmhGetFsMIrip6RipProfileTrigDelayTime (u4InstanceId,
                                                   i4Rip6ProfileNextIndex,
                                                   &i4Rip6ProfileNextTrigtime);

            nmhGetFsMIrip6RipProfileRouteAge (u4InstanceId,
                                              i4Rip6ProfileNextIndex,
                                              &i4Rip6ProfileNextRouteAge);

            nmhGetFsMIrip6RipProfileGarbageCollectTime (u4InstanceId,
                                                        i4Rip6ProfileNextIndex,
                                                        &i4Rip6ProfileNextGCTime);

            CliPrintf (CliHandle, "\nRIP ");
            CliPrintf (CliHandle, " port %d, ", RIP6_PORT);
            CliPrintf (CliHandle, "multicast-group %s,",
                       Ip6PrintNtop (&rip6McastAddr));

            CliPrintf (CliHandle, "Maximum paths is %d\r\n", RIP6_INFINITY);

            CliPrintf (CliHandle, "Updates every  %d seconds;",
                       i4Rip6ProfileNextUpTime);

            CliPrintf (CliHandle, "Expire after %d;\r\n",
                       i4Rip6ProfileNextRouteAge);

            CliPrintf (CliHandle, "Garbage Collect after  %d seconds\r\n",
                       i4Rip6ProfileNextGCTime);

            if (i4Rip6ProfileNextHorizon == RIP6_INITIALIZE_ONE)
            {
                CliPrintf (CliHandle, "Split Horizon is off;\r\n");
            }
            else if (i4Rip6ProfileNextHorizon == RIP6_INITIALIZE_TWO)
            {
                CliPrintf (CliHandle, "Split Horizon is on;\r\n");
            }
            else if (i4Rip6ProfileNextHorizon == RIP6_INITIALIZE_THREE)
            {
                CliPrintf (CliHandle, "Poison Reverse is on\r\n");
            }

            CliPrintf (CliHandle, "Interface:\r\n");

            for (u2i = RIP6_INITIALIZE_ONE; u2i <= u4MaxIfs; u2i++)
            {
                i4OperStatus = 0;
                nmhGetFsMIrip6RipIfOperStatus (0, u2i, &i4OperStatus);
                if (i4OperStatus == IP6_RIP_ENABLED_UP)
                {
                    if (RIP6_GET_IF_ENTRY (u2i)->u2ProfIndex ==
                        i4Rip6ProfileNextIndex)
                    {
                        u4IfIndex = RIP6_GET_IF_ENTRY (u2i)->u4Index;
                        CfaCliGetIfName (u4IfIndex, pi1IfName);
                        CliPrintf (CliHandle, "%s\r\n", pi1IfName);
                    }
                }

            }

            CliPrintf (CliHandle, "Redistribution:\r\n");

            if (i4Rip6RRDAdminStatus == RIP6_RRD_ENABLE)
            {

                if ((i4Rip6Redistribute & RIP6_IMPORT_DIRECT) ==
                    RIP6_IMPORT_DIRECT)
                {
                    CliPrintf (CliHandle, " Connected,");
                }
                if ((i4Rip6Redistribute & RIP6_IMPORT_STATIC) ==
                    RIP6_IMPORT_STATIC)
                {
                    CliPrintf (CliHandle, " Static,");
                }
                if ((i4Rip6Redistribute & RIP6_IMPORT_BGP) == RIP6_IMPORT_BGP)
                {
                    CliPrintf (CliHandle, " Bgp,");
                }
                if ((i4Rip6Redistribute & RIP6_IMPORT_OSPF) == RIP6_IMPORT_OSPF)
                {
                    CliPrintf (CliHandle, " Ospf,");
                }

                CliPrintf (CliHandle, "Routes Redistribution is enabled. \r\n");

            }
            else
            {
                CliPrintf (CliHandle, "None \r\n");
            }

            i4Rip6ProfileFirstIndex = i4Rip6ProfileNextIndex;

        }
        else
        {
            i1ReachedEOTable = 1;
        }
    }
    while (i1ReachedEOTable != RIP6_INITIALIZE_ONE);
    return CLI_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6ConfigRedistribute
*  Description   : Configuring redistribution of routes from a specific 
*                  protocol.
*  Input(s)      : 1. i4RedisProto - Protocol identifier 
*                  2. u1Set - Enable/Disable.
*  Output(s)     : None. 
*  Return Values : RIP6_SUCCESS.
*********************************************************************/
INT4
Rip6ConfigRedistribute (INT4 i4RedisProto, UINT1 u1Set, UINT1 *pu1RouteMapName)
{
    tSNMP_OCTET_STRING_TYPE RouteMapName;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

    UINT4               u4ErrCode;
    INT4                i4CurrRedisVal = 0;
    INT1                i1Status;

    if (u1Set == RIP6_RRD_ENABLE)
    {
        if (pu1RouteMapName != NULL)
        {
            RouteMapName.pu1_OctetList = au1RMapName;
            RouteMapName.i4_Length = STRLEN (pu1RouteMapName);
            MEMCPY (RouteMapName.pu1_OctetList, pu1RouteMapName,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;

            if (nmhTestv2Fsrip6RRDRouteMapName (&u4ErrCode,
                                                &RouteMapName) == SNMP_FAILURE)
            {
                return RIP6_PROTO_REDIS_NOT_SUPPORTED;
            }

            if (nmhSetFsrip6RRDRouteMapName (&RouteMapName) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP6_INV_ASSOC_RMAP);
                return RIP6_PROTO_REDIS_NOT_SUPPORTED;
            }
        }

        nmhGetFsrip6RRDProtoMaskForEnable (&i4CurrRedisVal);
        i4RedisProto |= i4CurrRedisVal;
        i1Status = nmhTestv2Fsrip6RRDProtoMaskForEnable (&u4ErrCode,
                                                         i4RedisProto);
        if (i1Status == SNMP_FAILURE)
        {
            return RIP6_PROTO_REDIS_NOT_SUPPORTED;
        }

        return nmhSetFsrip6RRDProtoMaskForEnable (i4RedisProto);
    }
    else if (u1Set == RIP6_RRD_DISABLE)
    {
        if (pu1RouteMapName != NULL)
        {
            RouteMapName.pu1_OctetList = au1RMapName;
            RouteMapName.i4_Length = STRLEN (pu1RouteMapName);
            MEMCPY (RouteMapName.pu1_OctetList, pu1RouteMapName,
                    RouteMapName.i4_Length);
            RouteMapName.pu1_OctetList[RouteMapName.i4_Length] = 0;

            if (nmhTestv2Fsrip6RRDRouteMapName (&u4ErrCode,
                                                &RouteMapName) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP6_INV_DISASSOC_RMAP);
                return RIP6_PROTO_REDIS_NOT_SUPPORTED;
            }
            RouteMapName.i4_Length = 0;
            if (nmhSetFsrip6RRDRouteMapName (&RouteMapName) == SNMP_FAILURE)
            {
                CLI_SET_ERR (CLI_RIP6_INV_DISASSOC_RMAP);
                return RIP6_PROTO_REDIS_NOT_SUPPORTED;
            }
        }
        nmhGetFsrip6RRDSrcProtoMaskForDisable (&i4CurrRedisVal);
        i4RedisProto |= i4CurrRedisVal;
        i1Status = nmhTestv2Fsrip6RRDSrcProtoMaskForDisable (&u4ErrCode,
                                                             i4RedisProto);
        if (i1Status == SNMP_FAILURE)
        {
            return RIP6_PROTO_REDIS_NOT_SUPPORTED;
        }

        return nmhSetFsrip6RRDSrcProtoMaskForDisable (i4RedisProto);
    }
    return RIP6_SUCCESS;
}

/*********************************************************************
*  Function Name : Rip6ConfigRedisDefMetric
*  Description   : Configuring Default Metric for redistributed routes
*  Input(s)      : 1. i4RedisMetric- metric 
*  Output(s)     : None. 
*  Return Values : None.
*********************************************************************/
INT4
Rip6ConfigRedisDefMetric (INT4 i4RedisMetric)
{
    UINT4               u4ErrCode;
    INT1                i1Status;

    i1Status = nmhTestv2Fsrip6RRDRouteDefMetric (&u4ErrCode, i4RedisMetric);
    if (i1Status == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return nmhSetFsrip6RRDRouteDefMetric (i4RedisMetric);
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : Rip6GetRouterCfgPrompt                              */
/*                                                                           */
/*     DESCRIPTION      : This function validates the given pi1ModeName      */
/*                        and returns the router configuration prompt in     */
/*                        pi1DispStr if valid                                */
/*                        Returns TRUE if given pi1ModeName is valid.        */
/*                        Returns FALSE if the given pi1ModeName is not valid*/
/*                        pi1ModeName is NULL to display the mode tree with  */
/*                        mode name and prompt string.                       */
/*                                                                           */
/*     INPUT            : pi1ModeName- Mode Name                             */
/*                                                                           */
/*     OUTPUT           : pi1DispStr- DIsplay string                         */
/*                                                                           */
/*     RETURNS          : True/False                                         */
/*                                                                           */
/*****************************************************************************/
INT1
Rip6GetRouterCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len;

    if (!pi1DispStr)
    {
        return FALSE;
    }

    /* NULL is passed to return "config-router" as the prompt
     * for the mode  */
    if (pi1ModeName == NULL)
    {
        STRNCPY (pi1DispStr, "config-router", STRLEN ("config-router"));
        pi1DispStr[STRLEN ("config-router")] = '\0';
        return TRUE;
    }

    u4Len = STRLEN (CLI_RIP6_ROUTER_MODE);
    if (STRNCMP (pi1ModeName, CLI_RIP6_ROUTER_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, "(config-router)#", STRLEN ("(config-router)#"));
    pi1DispStr[STRLEN ("(config-router)#")] = '\0';
    return TRUE;
}

/*********************************************************************
*   Function Name : Rip6ShowRunningConfig                       
*   Description   : Displays current RIP6 configuration details
*   Input(s)      : CliHandle - Cli Context ID.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS.                               
* *******************************************************************/
INT4
Rip6ShowRunningConfig (tCliHandle CliHandle, UINT4 u4Module)
{

    if (u4Module == ISS_RIP6_SHOW_RUNNING_CONFIG)
    {
        Rip6ShowRunningConfigInterface (CliHandle);
    }
    Rip6ShowRunningConfigGlobal (CliHandle);
    return (CLI_SUCCESS);
}

/*********************************************************************
*   Function Name : Rip6ShowRunningConfigGlobal                       
*   Description   : Displays current RIP6 global configurations
*   Input(s)      : CliHandle - Cli Context ID.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS.                               
********************************************************************/
INT4
Rip6ShowRunningConfigGlobal (tCliHandle CliHandle)
{
    UINT1               u1TempAddr[RIP6_ADDR_BYTES];
    UINT1               u1TempNextAddr[RIP6_ADDR_BYTES];
    UINT1               u1Flag = 0;
    INT1                i1ReachedEOTable = 0;
    UINT4               u4InstanceId = 0;
    INT4                i4Rip6RRDAdminStatus;
    INT4                i4Rip6Redistribute;
    INT4                i4RetVal;
    INT4                i4RedisMetric;
    INT1                i1OutCome;
    INT4                i4FilterType;
    INT4                i4NextFilterType;
    INT4                i4Value = 0;
    UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];
    INT4                i4Distance = 0;
    UINT1               au1NextRMapName[RMAP_MAX_NAME_LEN + 4];
    INT4                i4Interval = 0;
    tSNMP_OCTET_STRING_TYPE Rip6PeerAddr;
    tSNMP_OCTET_STRING_TYPE Rip6NextPeerAddr;
    tSNMP_OCTET_STRING_TYPE Rip6AdvAddr;
    tSNMP_OCTET_STRING_TYPE Rip6NextAdvAddr;
    tSNMP_OCTET_STRING_TYPE NextRouteMapName;
    tSNMP_OCTET_STRING_TYPE RouteMapName;

    CliRegisterLock (CliHandle, Rip6Lock, Rip6UnLock);
    RIP6_TASK_LOCK ();
    nmhGetFsrip6RipProfileTrigDelayTime ((INT4) gu4InstanceIndex, &i4Interval);
    if (i4Interval != IP6_RIP_PROF_DFL_TDELAY_TIME)
    {
        CliPrintf (CliHandle, "ipv6 rip peer triggered-updated-interval %d\r\n",
                   i4Interval);
    }

    if (gu4Rip6AdminStatus == RIP6_TRUE)
    {
        CliPrintf (CliHandle, "ipv6 router rip\r\n");
    }
    nmhGetFsRip6RRDAdminStatus (&i4Rip6RRDAdminStatus);
    if (i4Rip6RRDAdminStatus == RIP6_RRD_ENABLE)
    {
        nmhGetFsrip6RRDProtoMaskForEnable (&i4Rip6Redistribute);
        if ((i4Rip6Redistribute & RIP6_IMPORT_DIRECT) == RIP6_IMPORT_DIRECT)
        {

            if ((u1Flag == 0) && (gu4Rip6AdminStatus != RIP6_TRUE))
            {
                CliPrintf (CliHandle, "ipv6 router rip\r\n");
            }
            CliPrintf (CliHandle, "  redistribute connected");
            u1Flag = 1;
            nmhGetFsrip6RRDRouteDefMetric (&i4RedisMetric);
            if (i4RedisMetric != RIP6_DFLT_COST)
            {
                CliPrintf (CliHandle, " metric %d\r\n", i4RedisMetric);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
        if ((i4Rip6Redistribute & RIP6_IMPORT_STATIC) == RIP6_IMPORT_STATIC)
        {
            if ((u1Flag == 0) && (gu4Rip6AdminStatus != RIP6_TRUE))
            {
                CliPrintf (CliHandle, "ipv6 router rip\r\n");
            }
            CliPrintf (CliHandle, "redistribute static");
            u1Flag = 1;
            nmhGetFsrip6RRDRouteDefMetric (&i4RedisMetric);
            if (i4RedisMetric != RIP6_DFLT_COST)
            {
                CliPrintf (CliHandle, " metric %d\r\n", i4RedisMetric);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
        if ((i4Rip6Redistribute & RIP6_IMPORT_OSPF) == RIP6_IMPORT_OSPF)
        {
            if (u1Flag == 0)
            {
                CliPrintf (CliHandle, "ipv6 router rip\r\n");
            }
            CliPrintf (CliHandle, "redistribute ospf");
            u1Flag = 1;
            nmhGetFsrip6RRDRouteDefMetric (&i4RedisMetric);
            if (i4RedisMetric != RIP6_DFLT_COST)
            {
                CliPrintf (CliHandle, " metric %d\r\n", i4RedisMetric);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
        if ((i4Rip6Redistribute & RIP6_IMPORT_ISISL1L2))
        {
            if (u1Flag == 0)
            {
                CliPrintf (CliHandle, "ipv6 router rip\r\n");
            }
#ifdef ISIS_WANTED
            if ((i4Rip6Redistribute & RIP6_IMPORT_ISISL1L2) ==
                RIP6_IMPORT_ISISL1L2)
            {
                CliPrintf (CliHandle, "redistribute isis level-1-2");
            }
            else if ((i4Rip6Redistribute & RIP6_IMPORT_ISISL1L2) ==
                     RIP6_IMPORT_ISISL1)
            {
                CliPrintf (CliHandle, "redistribute isis level-1");
            }
            else if ((i4Rip6Redistribute & RIP6_IMPORT_ISISL1L2) ==
                     RIP6_IMPORT_ISISL2)
            {
                CliPrintf (CliHandle, "redistribute isis level-2");
            }
#endif
            u1Flag = 1;
            nmhGetFsrip6RRDRouteDefMetric (&i4RedisMetric);
            if (i4RedisMetric != RIP6_DFLT_COST)
            {
                CliPrintf (CliHandle, " metric %d\r\n", i4RedisMetric);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }
        }
    }
    if (nmhGetFsMIrip6PeerFilter (u4InstanceId, &i4RetVal) != SNMP_FAILURE)
    {
        if (i4RetVal != RIP6_DENY)
        {
            Rip6PeerAddr.i4_Length = 16;
            Rip6PeerAddr.pu1_OctetList = &u1TempAddr[0];
            Rip6NextPeerAddr.i4_Length = 16;
            Rip6NextPeerAddr.pu1_OctetList = &u1TempNextAddr[0];
            if (nmhGetFirstIndexFsMIrip6RipPeerTable (u4InstanceId,
                                                      &Rip6PeerAddr)
                == SNMP_SUCCESS)
            {
                if (u1Flag == 0)
                {
                    CliPrintf (CliHandle, "ipv6 router rip\r\n");
                    u1Flag = 1;
                }

                do
                {
                    if (nmhGetNextIndexFsMIrip6RipPeerTable (u4InstanceId,
                                                             &Rip6PeerAddr,
                                                             &Rip6NextPeerAddr)
                        == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle, "distribute prefix %s in\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 Rip6PeerAddr.pu1_OctetList));

                        MEMCPY (Rip6PeerAddr.pu1_OctetList,
                                Rip6NextPeerAddr.pu1_OctetList,
                                RIP6_ADDR_BYTES);
                        u1Flag = 1;
                    }
                    else
                    {
                        CliPrintf (CliHandle, "distribute prefix %s in\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 Rip6PeerAddr.pu1_OctetList));

                        u1Flag = 1;
                        i1ReachedEOTable = 1;
                    }
                }
                while (i1ReachedEOTable != RIP6_INITIALIZE_ONE);

            }
        }
    }

    if (nmhGetFsMIrip6AdvFilter (u4InstanceId, &i4RetVal) != SNMP_FAILURE)
    {
        if (i4RetVal != RIP6_DISABLE)
        {
            i1ReachedEOTable = 0;
            Rip6AdvAddr.pu1_OctetList = &u1TempAddr[0];
            Rip6AdvAddr.i4_Length = 16;
            Rip6NextAdvAddr.pu1_OctetList = &u1TempNextAddr[0];
            Rip6NextAdvAddr.i4_Length = 16;

            if (nmhGetFirstIndexFsMIrip6RipAdvFilterTable (u4InstanceId,
                                                           &Rip6AdvAddr)
                == SNMP_SUCCESS)
            {

                if (u1Flag == 0)
                {
                    CliPrintf (CliHandle, "ipv6 router rip\n");
                    u1Flag = 1;
                }

                do
                {
                    if (nmhGetNextIndexFsMIrip6RipAdvFilterTable (u4InstanceId,
                                                                  &Rip6AdvAddr,
                                                                  &Rip6NextAdvAddr)
                        == SNMP_SUCCESS)
                    {
                        CliPrintf (CliHandle,
                                   "distribute prefix %s out\r\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 Rip6AdvAddr.pu1_OctetList));

                        MEMCPY (Rip6AdvAddr.pu1_OctetList,
                                Rip6NextAdvAddr.pu1_OctetList, RIP6_ADDR_BYTES);
                        u1Flag = 1;
                    }
                    else
                    {
                        CliPrintf (CliHandle, "distribute prefix %s out\r\n",
                                   Ip6PrintNtop ((tIp6Addr *) (VOID *)
                                                 Rip6AdvAddr.pu1_OctetList));

                        u1Flag = 1;
                        i1ReachedEOTable = 1;
                    }
                }
                while (i1ReachedEOTable != RIP6_INITIALIZE_ONE);
            }
        }
    }

    /*Default distance */

    if (nmhGetFsRip6PreferenceValue (&i4Distance) == SNMP_SUCCESS)
    {
        if (i4Distance != IP6_PREFERENCE_RIP)
        {
            if (u1Flag == 0)
            {
                CliPrintf (CliHandle, "ipv6 router rip\n");
                u1Flag = 1;
            }
            CliPrintf (CliHandle, "distance %d\r\n", i4Distance);
        }
    }

    /* Distance, Distribute in/out filters */

    MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
    RouteMapName.pu1_OctetList = au1RMapName;

    i1OutCome =
        nmhGetFirstIndexFsRip6DistInOutRouteMapTable (&RouteMapName,
                                                      &i4FilterType);
    while (i1OutCome != SNMP_FAILURE)
    {
        if (u1Flag == 0)
        {
            CliPrintf (CliHandle, "ipv6 router rip\n");
            u1Flag = 1;
        }
        switch (i4FilterType)
        {
            case FILTERING_TYPE_DISTANCE:
                if (nmhGetFsRip6DistInOutRouteMapValue
                    (&RouteMapName, i4FilterType, &i4Value) == SNMP_SUCCESS)
                {
                    CliPrintf (CliHandle, "distance %d route-map %s\r\n",
                               i4Value, RouteMapName.pu1_OctetList);
                }
                break;
            case FILTERING_TYPE_DISTRIB_IN:
                CliPrintf (CliHandle, "distribute-list route-map %s in\r\n",
                           RouteMapName.pu1_OctetList);
                break;
            case FILTERING_TYPE_DISTRIB_OUT:
                CliPrintf (CliHandle, "distribute-list route-map %s out\r\n",
                           RouteMapName.pu1_OctetList);
                break;
            default:
                break;
        }

        MEMSET (&NextRouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1NextRMapName, 0, RMAP_MAX_NAME_LEN + 4);
        NextRouteMapName.pu1_OctetList = au1NextRMapName;

        i1OutCome =
            nmhGetNextIndexFsRip6DistInOutRouteMapTable (&RouteMapName,
                                                         &NextRouteMapName,
                                                         i4FilterType,
                                                         &i4NextFilterType);

        i4FilterType = i4NextFilterType;

        MEMCPY (RouteMapName.pu1_OctetList, NextRouteMapName.pu1_OctetList,
                RMAP_MAX_NAME_LEN + 4);
        RouteMapName.i4_Length = NextRouteMapName.i4_Length;
    }

    if (u1Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
        CliPrintf (CliHandle, "!\r\n");
    }

    RIP6_TASK_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);
}

/*********************************************************************
*   Function Name : Rip6ShowRunningConfigInterface                       
*   Description   : Displays current RIP6 interface configurations
*   Input(s)      : CliHandle - Cli Context ID.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS.                               
* *******************************************************************/
INT4
Rip6ShowRunningConfigInterface (tCliHandle CliHandle)
{
    UINT4               u4Flag;
    INT4                i4IfIndex;
    INT4                i4MaxIfs = 0;

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    u4Flag = 0;

    for (i4IfIndex = RIP6_INITIALIZE_ONE; i4IfIndex <= i4MaxIfs; i4IfIndex++)
    {
        u4Flag = 1;
        Rip6ShowRunningConfigInterfaceDetails (CliHandle, i4IfIndex, u4Flag);
    }

    return (CLI_SUCCESS);

}

/*********************************************************************
*   Function Name : Rip6ShowRunningConfigInterfaceDetails                       
*   Description   : Displays current RIP6 interface configurations
*   Input(s)      : CliHandle - Cli Context ID.
*   Output(s)     : None.                                                  
*   Return Values : CLI_SUCCESS.                               
********************************************************************/
INT4
Rip6ShowRunningConfigInterfaceDetails (tCliHandle CliHandle,
                                       INT4 i4IfIndex, UINT4 u4Flag)
{
    UINT1               au1NameStr[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1Flag;
    UINT4               u4InstanceId;
    INT4                i4Rip6ProfileHorizon = 0;
    INT4                i4Rip6ProfileStatus = 0;
    INT4                i4RetVal;

    u4InstanceId = 0;
    u1Flag = 0;

    CliRegisterLock (CliHandle, Rip6Lock, Rip6UnLock);
    RIP6_TASK_LOCK ();

    if (nmhValidateIndexInstanceFsMIrip6RipIfTable (u4InstanceId, i4IfIndex)
        != SNMP_FAILURE)
    {
        if (nmhGetFsMIrip6RipIfProfileIndex (u4InstanceId, i4IfIndex,
                                             &i4RetVal) != SNMP_FAILURE)
        {
            if (nmhValidateIndexInstanceFsMIrip6RipProfileTable (u4InstanceId,
                                                                 i4RetVal)
                != SNMP_FAILURE)
            {
                /*To check the profile status is valid */
                nmhGetFsMIrip6RipProfileStatus (u4InstanceId,
                                                i4RetVal, &i4Rip6ProfileStatus);
                if (i4Rip6ProfileStatus == IP6_RIP_PROFILE_VALID)
                {
                    nmhGetFsMIrip6RipProfileHorizon (u4InstanceId,
                                                     i4RetVal,
                                                     &i4Rip6ProfileHorizon);

                    if (i4Rip6ProfileHorizon == RIP6_INITIALIZE_ONE)
                    {

                        if (u4Flag == 1 && u1Flag == 0)
                        {
                            CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                                 (INT1 *) au1NameStr);
                            CliPrintf (CliHandle,
                                       "interface %s\r\n", au1NameStr);
                            u1Flag = 1;
                        }
                        CliPrintf (CliHandle, "no ipv6 split-horizon\r\n");
                    }
                    if (i4Rip6ProfileHorizon == RIP6_INITIALIZE_TWO)
                    {

                        if (u4Flag == 1 && u1Flag == 0)
                        {
                            CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                                 (INT1 *) au1NameStr);
                            CliPrintf (CliHandle,
                                       "interface %s\r\n", au1NameStr);
                            u1Flag = 1;
                        }
                        CliPrintf (CliHandle, "ipv6 split-horizon\r\n");
                    }
                }
            }

            if (nmhGetFsMIrip6RipIfProtocolEnable (u4InstanceId, i4IfIndex,
                                                   &i4RetVal) != SNMP_FAILURE)
            {
                if (i4RetVal != CLI_RIP6_DISABLE)
                {
                    if (u4Flag == 1 && u1Flag == 0)
                    {
                        CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                             (INT1 *) au1NameStr);
                        CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
                        u1Flag = 1;
                    }
                    CliPrintf (CliHandle, "ipv6 rip enable\r\n");
                    CliPrintf (CliHandle, "!\r\n");
                }
            }
            nmhGetFsMIrip6RipIfDefRouteAdvt (u4InstanceId, i4IfIndex,
                                             &i4RetVal);

            if (i4RetVal != RIP6_FALSE)
            {
                if (u4Flag == 1 && u1Flag == 0)
                {
                    CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                         (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
                    u1Flag = 1;
                }
                CliPrintf (CliHandle,
                           "ipv6 rip default-information originate\r\n");
            }

            nmhGetFsMIrip6RipIfCost (u4InstanceId, i4IfIndex, &i4RetVal);

            if (i4RetVal != 1)
            {
                if (u4Flag == 1 && u1Flag == 0)
                {
                    CfaCliConfGetIfName ((UINT4) i4IfIndex,
                                         (INT1 *) au1NameStr);
                    CliPrintf (CliHandle, "interface %s\r\n", au1NameStr);
                    u1Flag = 1;
                }
                CliPrintf (CliHandle, "ipv6 rip metric-offset %d\r\n",
                           i4RetVal);
            }
        }
    }
    if (u4Flag == 1 && u1Flag == 1)
    {
        CliPrintf (CliHandle, "!\r\n");
    }
    RIP6_TASK_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return (CLI_SUCCESS);
}

/***************************************************************************/
/*   Function Name : Ripi6SetRouteDistance                                 */
/*                                                                         */
/*   Description   : Sets route distance                                   */
/*                                                                         */
/*   Input(s)      : 1. CliHandle                                          */
/*                   2. i4Distance  - Distance value                       */
/*                   3. pu1RMapName - route map name                       */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
Rip6SetRouteDistance (tCliHandle CliHandle, INT4 i4Distance, UINT1 *pu1RMapName)
{
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    UINT4               u4CliError = CLI_RIP6_INV_ASSOC_RMAP;
    INT4                i4RowStatus;

    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);

        if (nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                    FILTERING_TYPE_DISTANCE,
                                                    &i4RowStatus) !=
            SNMP_SUCCESS)
        {
            if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 CREATE_AND_WAIT) == SNMP_SUCCESS)
            {
                if (nmhSetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                            FILTERING_TYPE_DISTANCE,
                                                            CREATE_AND_WAIT) ==
                    SNMP_SUCCESS)
                {
                    u4ErrorCode = SNMP_ERR_NO_ERROR;
                }
            }
            else
            {
                u4CliError = CLI_RIP6_RMAP_ASSOC_FAILED;
            }
        }
        else
        {
            u4ErrorCode = SNMP_ERR_NO_ERROR;
        }

        if (SNMP_ERR_NO_ERROR == u4ErrorCode)
        {
            if (nmhTestv2FsRip6DistInOutRouteMapValue
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 i4Distance) == SNMP_SUCCESS)
            {
                if (nmhSetFsRip6DistInOutRouteMapValue (&RouteMapName,
                                                        FILTERING_TYPE_DISTANCE,
                                                        i4Distance) ==
                    SNMP_SUCCESS)
                {
                    if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                        (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                         ACTIVE) == SNMP_SUCCESS)
                    {
                        nmhSetFsRip6DistInOutRouteMapRowStatus
                            (&RouteMapName, FILTERING_TYPE_DISTANCE, ACTIVE);

                        return CLI_SUCCESS;
                    }
                }
            }
        }
    }
    else
    {
        if (nmhTestv2FsRip6PreferenceValue (&u4ErrorCode, i4Distance) ==
            SNMP_SUCCESS)
        {
            if (nmhSetFsRip6PreferenceValue (i4Distance) == SNMP_SUCCESS)
            {
                return CLI_SUCCESS;
            }
        }
        u4CliError = CLI_RIP6_INV_VALUE;
    }
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/***************************************************************************/
/*   Function Name : Rip6SetNoRouteDistance                                */
/*                                                                         */
/*   Description   : Unset route distance                                  */
/*                                                                         */
/*   Input(s)      : 1. CliHandle                                          */
/*                   2. pu1RMapName - route map name                       */
/*                                                                         */
/*   Output(s)     : None                                                  */
/*                                                                         */
/*   Return Values : CLI_SUCCESS / CLI_FAILURE                             */
/***************************************************************************/

INT4
Rip6SetNoRouteDistance (tCliHandle CliHandle, UINT1 *pu1RMapName)
{
    UINT4               u4CliError = CLI_RIP6_INV_DISASSOC_RMAP;
    UINT4               u4ErrorCode = SNMP_ERR_NO_CREATION;
    INT4                i4RowStatus;
    UNUSED_PARAM (CliHandle);
    if (pu1RMapName != NULL)
    {
        tSNMP_OCTET_STRING_TYPE RouteMapName;
        UINT1               au1RMapName[RMAP_MAX_NAME_LEN + 4];

        MEMSET (&RouteMapName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
        MEMSET (au1RMapName, 0, RMAP_MAX_NAME_LEN + 4);
        RouteMapName.pu1_OctetList = au1RMapName;

        RouteMapName.i4_Length = STRLEN (pu1RMapName);
        MEMCPY (RouteMapName.pu1_OctetList, pu1RMapName,
                RouteMapName.i4_Length);
        if (nmhGetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                    FILTERING_TYPE_DISTANCE,
                                                    &i4RowStatus) ==
            SNMP_SUCCESS)
        {
            if (nmhTestv2FsRip6DistInOutRouteMapRowStatus
                (&u4ErrorCode, &RouteMapName, FILTERING_TYPE_DISTANCE,
                 DESTROY) == SNMP_SUCCESS)
            {
                if (nmhSetFsRip6DistInOutRouteMapRowStatus (&RouteMapName,
                                                            FILTERING_TYPE_DISTANCE,
                                                            DESTROY) ==
                    SNMP_SUCCESS)
                {
                    return CLI_SUCCESS;
                }
            }
        }
        else
        {
            u4CliError = CLI_RIP6_INV_ASSOC_RMAP;
        }
    }
    else
    {                            /* 0 distance will set default value */
        if (nmhTestv2FsRip6PreferenceValue (&u4ErrorCode, 0) == SNMP_SUCCESS)
        {
            nmhSetFsRip6PreferenceValue (0);

            return CLI_SUCCESS;

        }
    }
    u4CliError = CLI_RIP6_INV_VALUE;
    CLI_SET_ERR (u4CliError);
    return (CLI_FAILURE);
}

/*********************************************************************
 *** *   Function Name : IssRip6ShowDebugging
 *** *   Description   : Displays current RIP6 Trace level configurations
 *** *   Input(s)      : CliHandle - Cli Context ID.
 *** *   Output(s)     : None.
 *** *   Return Values : None.
 **********************************************************************/
VOID
IssRip6ShowDebugging (tCliHandle CliHandle)
{

    if (gu4Rip6DbgMap != 0)
    {
        if (gu4Rip6DbgMap == RIP6_DATA_PATH_TRC)
        {
            CliPrintf (CliHandle, "\r\n  RIP6 data plane debugging is on");
        }
        if (gu4Rip6DbgMap == RIP6_CTRL_PLANE_TRC)
        {
            CliPrintf (CliHandle, "\r\n  RIP6 control plane debugging is on");
        }
        if (gu4Rip6DbgMap == RIP6_DATA_CTRL_TRC)
        {
            CliPrintf (CliHandle,
                       "\r\n  RIP6 control plane,data path debugging is on");
        }
    }
    else
    {
        UNUSED_PARAM (CliHandle);
    }
    return;
}

/*********************************************************************
*  Function Name : Rip6ShowRoute
*  Description   : Route Table Entries to be displayed.
*  Input(s)      : CliHandle - Cli Context ID.
*  Output(s)     : None.
*  Return Values : CLI_SUCCESS/CLI_FAILURE.
*********************************************************************/

INT4
Rip6ShowRoute (tCliHandle CliHandle)
{
    tRip6RtEntry       *pRip6Route = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    UINT4               u4InstanceId = 0;
    INT4                i4TotalRouteCnt = 0;
    INT1               *pi1IfName = NULL;
    tSNMP_OCTET_STRING_TYPE DestAddr;
    INT4                i4RouteAge = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    const CHR1         *au1Proto[] = {
        NULL,
        "other",
        "local",
        "static",
        "ndisc",
        "ripng",
        "ospf",
        "bgp",
        "irdp"
    };
    UINT1               u1TempAddr[16];

    MEMSET (&DestAddr, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    pi1IfName = (INT1 *) &au1IfName[0];
    u4InstanceId = 0;
    CliPrintf (CliHandle, "\nRIP local RIB \r\n");

    nmhGetFsrip6RouteCount (&i4TotalRouteCnt);

    CliPrintf (CliHandle, "\nNo of RIP6 Route Entries %d\r\n", i4TotalRouteCnt);

    DestAddr.pu1_OctetList = &u1TempAddr[0];
    DestAddr.i4_Length = RIP6_ADDR_BYTES;

    Rip6TrieGetFirstEntry (&pRip6Route, &pRibNode, 0);
    if (pRip6Route != NULL)
    {
        CliPrintf (CliHandle, "%s/%d, ",
                   Ip6PrintNtop ((tIp6Addr *) &
                                 (pRip6Route->dst)), (pRip6Route->u1Prefixlen));
        CliPrintf (CliHandle, " metric %d,", pRip6Route->u1Metric);

        CliPrintf (CliHandle, " %s\r\n ", au1Proto[pRip6Route->i1Proto]);

        CfaCliGetIfName (pRip6Route->u4Index, pi1IfName);

        CliPrintf (CliHandle, "     %s/%s, ", pi1IfName,
                   Ip6PrintNtop ((tIp6Addr *) & (pRip6Route->nexthop)));
        Ip6AddrCopy ((tIp6Addr *) (VOID *) DestAddr.pu1_OctetList,
                     (tIp6Addr *) (VOID *) &(pRip6Route->dst));

        nmhGetFsMIrip6RipRouteAge (u4InstanceId,
                                   &DestAddr,
                                   (INT4) pRip6Route->u1Prefixlen,
                                   (INT4) pRip6Route->i1Proto,
                                   (INT4) pRip6Route->u4Index, &i4RouteAge);

        if (pRip6Route->i1Proto != RIP6_LOCAL)
        {
            if (pRip6Route->u1Metric != RIP6_INFINITY)
            {
                i4RouteAge = (RIP6_DFLT_ROUTE_AGE - (i4RouteAge / OSIX_TPS));
                CliPrintf (CliHandle, "expires in %d secs \r\n", i4RouteAge);
            }
            else
            {
                i4RouteAge = (RIP6_DFLT_GARBAGE_COLLECT_TIME -
                              (i4RouteAge / OSIX_TPS));
                CliPrintf (CliHandle, "expired, [advertise %d ]\r\n ",
                           i4RouteAge);
            }
        }
        else
        {
            CliPrintf (CliHandle, "\r\n");
        }

        while (pRip6Route->pNext)
        {
            pRip6Route = pRip6Route->pNext;
            CliPrintf (CliHandle, "%s/%d, ",
                       Ip6PrintNtop ((tIp6Addr *) &
                                     (pRip6Route->dst)),
                       pRip6Route->u1Prefixlen);
            CliPrintf (CliHandle, " metric %d,", pRip6Route->u1Metric);

            CliPrintf (CliHandle, " %s\r\n ", au1Proto[(pRip6Route->i1Proto)]);

            CfaCliGetIfName ((pRip6Route->u4Index), pi1IfName);

            CliPrintf (CliHandle, "     %s/%s, ", pi1IfName,
                       Ip6PrintNtop ((tIp6Addr *) & (pRip6Route->nexthop)));

            Ip6AddrCopy ((tIp6Addr *) (VOID *) DestAddr.pu1_OctetList,
                         (tIp6Addr *) (VOID *) &(pRip6Route->dst));

            nmhGetFsMIrip6RipRouteAge (u4InstanceId,
                                       &DestAddr,
                                       (INT4) pRip6Route->u1Prefixlen,
                                       (INT4) pRip6Route->i1Proto,
                                       (INT4) pRip6Route->u4Index, &i4RouteAge);
            if (pRip6Route->i1Proto != RIP6_LOCAL)
            {
                if (pRip6Route->u1Metric != RIP6_INFINITY)
                {
                    i4RouteAge =
                        (RIP6_DFLT_ROUTE_AGE - (i4RouteAge / OSIX_TPS));
                    CliPrintf (CliHandle, "expires in %d secs \r\n",
                               i4RouteAge);
                }
                else
                {
                    i4RouteAge = (RIP6_DFLT_GARBAGE_COLLECT_TIME -
                                  (i4RouteAge / OSIX_TPS));
                    CliPrintf (CliHandle, "expired, [advertise %d ]\r\n ",
                               i4RouteAge);
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }

        }

        while ((Rip6TrieGetNextEntry (&pRip6Route, pRibNode, &pNextRibNode, 0)
                == RIP6_SUCCESS))
        {
            CliPrintf (CliHandle, "%s/%d, ",
                       Ip6PrintNtop ((tIp6Addr *) &
                                     (pRip6Route->dst)),
                       pRip6Route->u1Prefixlen);
            CliPrintf (CliHandle, " metric %d,", pRip6Route->u1Metric);

            CliPrintf (CliHandle, " %s\r\n ", au1Proto[(pRip6Route->i1Proto)]);

            CfaCliGetIfName ((pRip6Route->u4Index), pi1IfName);

            CliPrintf (CliHandle, "     %s/%s, ", pi1IfName,
                       Ip6PrintNtop ((tIp6Addr *) & (pRip6Route->nexthop)));

            Ip6AddrCopy ((tIp6Addr *) (VOID *) DestAddr.pu1_OctetList,
                         (tIp6Addr *) (VOID *) &(pRip6Route->dst));

            nmhGetFsMIrip6RipRouteAge (u4InstanceId,
                                       &DestAddr,
                                       (INT4) pRip6Route->u1Prefixlen,
                                       (INT4) pRip6Route->i1Proto,
                                       (INT4) pRip6Route->u4Index, &i4RouteAge);

            if (pRip6Route->i1Proto != RIP6_LOCAL)
            {
                if (pRip6Route->u1Metric != RIP6_INFINITY)
                {
                    i4RouteAge =
                        (RIP6_DFLT_ROUTE_AGE - (i4RouteAge / OSIX_TPS));
                    CliPrintf (CliHandle, "expires in %d secs \r\n",
                               i4RouteAge);
                }
                else
                {
                    i4RouteAge = (RIP6_DFLT_GARBAGE_COLLECT_TIME -
                                  (i4RouteAge / OSIX_TPS));
                    CliPrintf (CliHandle, "expired, [advertise %d ]\r\n ",
                               i4RouteAge);
                }
            }
            else
            {
                CliPrintf (CliHandle, "\r\n");
            }

            while (pRip6Route->pNext)
            {
                pRip6Route = pRip6Route->pNext;
                CliPrintf (CliHandle, "%s/%d, ",
                           Ip6PrintNtop ((tIp6Addr *) &
                                         (pRip6Route->dst)),
                           pRip6Route->u1Prefixlen);
                CliPrintf (CliHandle, " metric %d,", pRip6Route->u1Metric);

                CliPrintf (CliHandle, " %s\r\n ",
                           au1Proto[(pRip6Route->i1Proto)]);

                CfaCliGetIfName ((pRip6Route->u4Index), pi1IfName);

                CliPrintf (CliHandle, "     %s/%s, ", pi1IfName,
                           Ip6PrintNtop ((tIp6Addr *) & (pRip6Route->nexthop)));

                Ip6AddrCopy ((tIp6Addr *) (VOID *) DestAddr.pu1_OctetList,
                             (tIp6Addr *) (VOID *) &(pRip6Route->dst));

                nmhGetFsMIrip6RipRouteAge (u4InstanceId,
                                           &DestAddr,
                                           (INT4) pRip6Route->u1Prefixlen,
                                           (INT4) pRip6Route->i1Proto,
                                           (INT4) pRip6Route->u4Index,
                                           &i4RouteAge);

                if (pRip6Route->i1Proto != RIP6_LOCAL)
                {
                    if (pRip6Route->u1Metric != RIP6_INFINITY)
                    {
                        i4RouteAge =
                            (RIP6_DFLT_ROUTE_AGE - (i4RouteAge / OSIX_TPS));
                        CliPrintf (CliHandle, "expires in %d secs \r\n",
                                   i4RouteAge);
                    }
                    else
                    {
                        i4RouteAge = (RIP6_DFLT_GARBAGE_COLLECT_TIME -
                                      (i4RouteAge / OSIX_TPS));
                        CliPrintf (CliHandle, "expired, [advertise %d ]\r\n ",
                                   i4RouteAge);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, "\r\n");
                }

            }
            pRibNode = pNextRibNode;

        }
    }
    return RIP6_SUCCESS;
}

#endif /* __RIP6CLI_C__ */
