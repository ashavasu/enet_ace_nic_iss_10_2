
/* $Id: fsrip6mid.c,v 1.4 2013/06/23 13:34:05 siva Exp $*/
# include  "include.h"
# include  "fsrip6mid.h"
# include  "fsrip6low.h"
# include  "fsrip6con.h"
# include  "fsrip6ogi.h"
# include  "extern.h"
# include  "midconst.h"
# include  "fsrip6cli.h"

/****************************************************************************
 Function   : fsrip6ScalarsGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrip6ScalarsGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Scalar get routine. */

    UINT1               i1_ret_val = FALSE;
    INT4                LEN_fsrip6Scalars_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  This Variable is declared for being used in the
     *  FOR Loop for extracting Indices from OID Given.
     */

/*** DECLARATION_END ***/

    LEN_fsrip6Scalars_INDEX = p_in_db->u4_Length;

    /*  Incrementing the Length for the Extract of Scalar Tables. */
    LEN_fsrip6Scalars_INDEX++;
    if (u1_search_type == SNMP_SEARCH_TYPE_EXACT)
    {
        if ((LEN_fsrip6Scalars_INDEX != (INT4) p_incoming->u4_Length)
            || (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0))
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    else
    {
        /*  Get Next Operation on the Scalar Variable.  */
        if ((INT4) p_incoming->u4_Length >= LEN_fsrip6Scalars_INDEX)
        {
            return ((tSNMP_VAR_BIND *) NULL);
        }
    }
    switch (u1_arg)
    {
        case FSRIP6ROUTEPREFERENCE:
        {
            i1_ret_val = nmhGetFsrip6RoutePreference (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6ROUTEPROPAGATE:
        {
            i1_ret_val = nmhGetFsrip6RoutePropagate (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6GLOBALDEBUG:
        {
            i1_ret_val = nmhGetFsrip6GlobalDebug (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6GLOBALINSTANCEINDEX:
        {
            i1_ret_val = nmhGetFsrip6GlobalInstanceIndex (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6PEERFILTER:
        {
            i1_ret_val = nmhGetFsrip6PeerFilter (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6ADVFILTER:
        {
            i1_ret_val = nmhGetFsrip6AdvFilter (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6ROUTECOUNT:
        {
            i1_ret_val = nmhGetFsrip6RouteCount (&i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    /* Incrementing the Length of the p_in_db. */
    p_in_db->u4_Length++;
    /* Adding the .0 to the p_in_db for scalar Objects. */
    p_in_db->pu4_OidList[p_in_db->u4_Length - 1] = ZERO;

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrip6ScalarsSet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrip6ScalarsSet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                  UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRIP6ROUTEPREFERENCE:
        {
            i1_ret_val = nmhSetFsrip6RoutePreference (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6ROUTEPROPAGATE:
        {
            i1_ret_val = nmhSetFsrip6RoutePropagate (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6GLOBALDEBUG:
        {
            i1_ret_val = nmhSetFsrip6GlobalDebug (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6GLOBALINSTANCEINDEX:
        {
            i1_ret_val =
                nmhSetFsrip6GlobalInstanceIndex (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6PEERFILTER:
        {
            i1_ret_val = nmhSetFsrip6PeerFilter (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6ADVFILTER:
        {
            i1_ret_val = nmhSetFsrip6AdvFilter (p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrip6ScalarsTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrip6ScalarsTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                   UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
    }
    if (p_incoming->pu4_OidList[p_incoming->u4_Length - 1] != 0)
    {
        return (SNMP_ERR_GEN_ERR);
    }
    switch (u1_arg)
    {

        case FSRIP6ROUTEPREFERENCE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RoutePreference (&u4ErrorCode,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6ROUTEPROPAGATE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RoutePropagate (&u4ErrorCode,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6GLOBALDEBUG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6GlobalDebug (&u4ErrorCode,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6GLOBALINSTANCEINDEX:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6GlobalInstanceIndex (&u4ErrorCode,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6PEERFILTER:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6PeerFilter (&u4ErrorCode,
                                           p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6ADVFILTER:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6AdvFilter (&u4ErrorCode, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsrip6InstanceEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrip6InstanceEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsrip6InstanceTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsrip6InstanceIndex = FALSE;
    INT4                i4_next_fsrip6InstanceIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsrip6InstanceTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsrip6InstanceTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsrip6InstanceIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsrip6InstanceTable
                     (i4_fsrip6InstanceIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsrip6InstanceIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsrip6InstanceTable
                     (&i4_fsrip6InstanceIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsrip6InstanceIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsrip6InstanceIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsrip6InstanceTable (i4_fsrip6InstanceIndex,
                                                         &i4_next_fsrip6InstanceIndex))
                    == SNMP_SUCCESS)
                {
                    i4_fsrip6InstanceIndex = i4_next_fsrip6InstanceIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsrip6InstanceIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRIP6INSTANCEINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsrip6InstanceIndex;
            }
            else
            {
                i4_return_val = i4_next_fsrip6InstanceIndex;
            }
            break;
        }
        case FSRIP6INSTANCESTATUS:
        {
            i1_ret_val =
                nmhGetFsrip6InstanceStatus (i4_fsrip6InstanceIndex,
                                            &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrip6InstanceEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrip6InstanceEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsrip6InstanceIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsrip6InstanceIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRIP6INSTANCESTATUS:
        {
            i1_ret_val =
                nmhSetFsrip6InstanceStatus (i4_fsrip6InstanceIndex,
                                            p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRIP6INSTANCEINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrip6InstanceEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrip6InstanceEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsrip6InstanceIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsrip6InstanceIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsrip6InstanceTable(i4_fsrip6InstanceIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRIP6INSTANCESTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6InstanceStatus (&u4ErrorCode,
                                               i4_fsrip6InstanceIndex,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRIP6INSTANCEINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsrip6InstIfMapEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrip6InstIfMapEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsrip6InstIfMapTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsrip6IfIndex = FALSE;
    INT4                i4_next_fsrip6IfIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsrip6InstIfMapTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsrip6InstIfMapTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsrip6IfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsrip6InstIfMapTable
                     (i4_fsrip6IfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsrip6IfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsrip6InstIfMapTable (&i4_fsrip6IfIndex))
                    == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsrip6IfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsrip6IfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsrip6InstIfMapTable (i4_fsrip6IfIndex,
                                                          &i4_next_fsrip6IfIndex))
                    == SNMP_SUCCESS)
                {
                    i4_fsrip6IfIndex = i4_next_fsrip6IfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsrip6IfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRIP6IFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsrip6IfIndex;
            }
            else
            {
                i4_return_val = i4_next_fsrip6IfIndex;
            }
            break;
        }
        case FSRIP6INSTIFMAPINSTID:
        {
            i1_ret_val =
                nmhGetFsrip6InstIfMapInstId (i4_fsrip6IfIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6INSTIFMAPIFATCHSTATUS:
        {
            i1_ret_val =
                nmhGetFsrip6InstIfMapIfAtchStatus (i4_fsrip6IfIndex,
                                                   &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrip6InstIfMapEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrip6InstIfMapEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsrip6IfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsrip6IfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRIP6INSTIFMAPIFATCHSTATUS:
        {
            i1_ret_val =
                nmhSetFsrip6InstIfMapIfAtchStatus (i4_fsrip6IfIndex,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRIP6IFINDEX:
            /*  Read Only Variables. */
        case FSRIP6INSTIFMAPINSTID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrip6InstIfMapEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrip6InstIfMapEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsrip6IfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsrip6IfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsrip6InstIfMapTable(i4_fsrip6IfIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRIP6INSTIFMAPIFATCHSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6InstIfMapIfAtchStatus (&u4ErrorCode,
                                                      i4_fsrip6IfIndex,
                                                      p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRIP6IFINDEX:
        case FSRIP6INSTIFMAPINSTID:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsrip6RipIfEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrip6RipIfEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsrip6RipIfTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsrip6RipIfIndex = FALSE;
    INT4                i4_next_fsrip6RipIfIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsrip6RipIfTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsrip6RipIfTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsrip6RipIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsrip6RipIfTable
                     (i4_fsrip6RipIfIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsrip6RipIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsrip6RipIfTable (&i4_fsrip6RipIfIndex)) ==
                    SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsrip6RipIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsrip6RipIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsrip6RipIfTable (i4_fsrip6RipIfIndex,
                                                      &i4_next_fsrip6RipIfIndex))
                    == SNMP_SUCCESS)
                {
                    i4_fsrip6RipIfIndex = i4_next_fsrip6RipIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsrip6RipIfIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRIP6RIPIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsrip6RipIfIndex;
            }
            else
            {
                i4_return_val = i4_next_fsrip6RipIfIndex;
            }
            break;
        }
        case FSRIP6RIPIFPROFILEINDEX:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfProfileIndex (i4_fsrip6RipIfIndex,
                                               &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFCOST:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfCost (i4_fsrip6RipIfIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFOPERSTATUS:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfOperStatus (i4_fsrip6RipIfIndex,
                                             &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFPROTOCOLENABLE:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfProtocolEnable (i4_fsrip6RipIfIndex,
                                                 &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFINMESSAGES:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfInMessages (i4_fsrip6RipIfIndex,
                                             &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFINREQUESTS:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfInRequests (i4_fsrip6RipIfIndex,
                                             &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFINRESPONSES:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfInResponses (i4_fsrip6RipIfIndex,
                                              &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFUNKNOWNCMDS:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfUnknownCmds (i4_fsrip6RipIfIndex,
                                              &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFINOTHERVER:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfInOtherVer (i4_fsrip6RipIfIndex,
                                             &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFINDISCARDS:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfInDiscards (i4_fsrip6RipIfIndex,
                                             &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFOUTMESSAGES:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfOutMessages (i4_fsrip6RipIfIndex,
                                              &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFOUTREQUESTS:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfOutRequests (i4_fsrip6RipIfIndex,
                                              &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFOUTRESPONSES:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfOutResponses (i4_fsrip6RipIfIndex,
                                               &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPIFOUTTRIGUPDATES:
        {
            i1_ret_val =
                nmhGetFsrip6RipIfOutTrigUpdates (i4_fsrip6RipIfIndex,
                                                 &u4_counter_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_COUNTER32;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrip6RipIfEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrip6RipIfEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                     UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsrip6RipIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsrip6RipIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRIP6RIPIFPROFILEINDEX:
        {
            i1_ret_val =
                nmhSetFsrip6RipIfProfileIndex (i4_fsrip6RipIfIndex,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPIFCOST:
        {
            i1_ret_val =
                nmhSetFsrip6RipIfCost (i4_fsrip6RipIfIndex,
                                       p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPIFPROTOCOLENABLE:
        {
            i1_ret_val =
                nmhSetFsrip6RipIfProtocolEnable (i4_fsrip6RipIfIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRIP6RIPIFINDEX:
            /*  Read Only Variables. */
        case FSRIP6RIPIFOPERSTATUS:
            /*  Read Only Variables. */
        case FSRIP6RIPIFINMESSAGES:
            /*  Read Only Variables. */
        case FSRIP6RIPIFINREQUESTS:
            /*  Read Only Variables. */
        case FSRIP6RIPIFINRESPONSES:
            /*  Read Only Variables. */
        case FSRIP6RIPIFUNKNOWNCMDS:
            /*  Read Only Variables. */
        case FSRIP6RIPIFINOTHERVER:
            /*  Read Only Variables. */
        case FSRIP6RIPIFINDISCARDS:
            /*  Read Only Variables. */
        case FSRIP6RIPIFOUTMESSAGES:
            /*  Read Only Variables. */
        case FSRIP6RIPIFOUTREQUESTS:
            /*  Read Only Variables. */
        case FSRIP6RIPIFOUTRESPONSES:
            /*  Read Only Variables. */
        case FSRIP6RIPIFOUTTRIGUPDATES:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrip6RipIfEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrip6RipIfEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                      UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsrip6RipIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsrip6RipIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsrip6RipIfTable(i4_fsrip6RipIfIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRIP6RIPIFPROFILEINDEX:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipIfProfileIndex (&u4ErrorCode,
                                                  i4_fsrip6RipIfIndex,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPIFCOST:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipIfCost (&u4ErrorCode, i4_fsrip6RipIfIndex,
                                          p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPIFPROTOCOLENABLE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipIfProtocolEnable (&u4ErrorCode,
                                                    i4_fsrip6RipIfIndex,
                                                    p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRIP6RIPIFINDEX:
        case FSRIP6RIPIFOPERSTATUS:
        case FSRIP6RIPIFINMESSAGES:
        case FSRIP6RIPIFINREQUESTS:
        case FSRIP6RIPIFINRESPONSES:
        case FSRIP6RIPIFUNKNOWNCMDS:
        case FSRIP6RIPIFINOTHERVER:
        case FSRIP6RIPIFINDISCARDS:
        case FSRIP6RIPIFOUTMESSAGES:
        case FSRIP6RIPIFOUTREQUESTS:
        case FSRIP6RIPIFOUTRESPONSES:
        case FSRIP6RIPIFOUTTRIGUPDATES:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsrip6RipProfileEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrip6RipProfileEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsrip6RipProfileTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_fsrip6RipProfileIndex = FALSE;
    INT4                i4_next_fsrip6RipProfileIndex = FALSE;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsrip6RipProfileTable_INDEX = p_in_db->u4_Length + INTEGER_LEN;

            if (LEN_fsrip6RipProfileTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /* Extracting The Integer Index. */
                i4_fsrip6RipProfileIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsrip6RipProfileTable
                     (i4_fsrip6RipProfileIndex)) != SNMP_SUCCESS)
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsrip6RipProfileIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsrip6RipProfileTable
                     (&i4_fsrip6RipProfileIndex)) == SNMP_SUCCESS)
                {
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsrip6RipProfileIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsrip6RipProfileIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsrip6RipProfileTable
                     (i4_fsrip6RipProfileIndex,
                      &i4_next_fsrip6RipProfileIndex)) == SNMP_SUCCESS)
                {
                    i4_fsrip6RipProfileIndex = i4_next_fsrip6RipProfileIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsrip6RipProfileIndex;
                }
                else
                {
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRIP6RIPPROFILEINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsrip6RipProfileIndex;
            }
            else
            {
                i4_return_val = i4_next_fsrip6RipProfileIndex;
            }
            break;
        }
        case FSRIP6RIPPROFILESTATUS:
        {
            i1_ret_val =
                nmhGetFsrip6RipProfileStatus (i4_fsrip6RipProfileIndex,
                                              &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPPROFILEHORIZON:
        {
            i1_ret_val =
                nmhGetFsrip6RipProfileHorizon (i4_fsrip6RipProfileIndex,
                                               &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPPROFILEPERIODICUPDTIME:
        {
            i1_ret_val =
                nmhGetFsrip6RipProfilePeriodicUpdTime (i4_fsrip6RipProfileIndex,
                                                       &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPPROFILETRIGDELAYTIME:
        {
            i1_ret_val =
                nmhGetFsrip6RipProfileTrigDelayTime (i4_fsrip6RipProfileIndex,
                                                     &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPPROFILEROUTEAGE:
        {
            i1_ret_val =
                nmhGetFsrip6RipProfileRouteAge (i4_fsrip6RipProfileIndex,
                                                &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPPROFILEGARBAGECOLLECTTIME:
        {
            i1_ret_val =
                nmhGetFsrip6RipProfileGarbageCollectTime
                (i4_fsrip6RipProfileIndex, &i4_return_val);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrip6RipProfileEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrip6RipProfileEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                          UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_fsrip6RipProfileIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;
        /* Extracting The Integer Index. */
        i4_fsrip6RipProfileIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRIP6RIPPROFILESTATUS:
        {
            i1_ret_val =
                nmhSetFsrip6RipProfileStatus (i4_fsrip6RipProfileIndex,
                                              p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPPROFILEHORIZON:
        {
            i1_ret_val =
                nmhSetFsrip6RipProfileHorizon (i4_fsrip6RipProfileIndex,
                                               p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPPROFILEPERIODICUPDTIME:
        {
            i1_ret_val =
                nmhSetFsrip6RipProfilePeriodicUpdTime (i4_fsrip6RipProfileIndex,
                                                       p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPPROFILETRIGDELAYTIME:
        {
            i1_ret_val =
                nmhSetFsrip6RipProfileTrigDelayTime (i4_fsrip6RipProfileIndex,
                                                     p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPPROFILEROUTEAGE:
        {
            i1_ret_val =
                nmhSetFsrip6RipProfileRouteAge (i4_fsrip6RipProfileIndex,
                                                p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPPROFILEGARBAGECOLLECTTIME:
        {
            i1_ret_val =
                nmhSetFsrip6RipProfileGarbageCollectTime
                (i4_fsrip6RipProfileIndex, p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRIP6RIPPROFILEINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrip6RipProfileEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrip6RipProfileEntryTest (tSNMP_OID_TYPE * p_in_db,
                           tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                           tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_fsrip6RipProfileIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /* Extracting The Integer Index. */
        i4_fsrip6RipProfileIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsrip6RipProfileTable(i4_fsrip6RipProfileIndex)) != SNMP_SUCCESS) {
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRIP6RIPPROFILESTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipProfileStatus (&u4ErrorCode,
                                                 i4_fsrip6RipProfileIndex,
                                                 p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPPROFILEHORIZON:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipProfileHorizon (&u4ErrorCode,
                                                  i4_fsrip6RipProfileIndex,
                                                  p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPPROFILEPERIODICUPDTIME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipProfilePeriodicUpdTime (&u4ErrorCode,
                                                          i4_fsrip6RipProfileIndex,
                                                          p_value->
                                                          i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPPROFILETRIGDELAYTIME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipProfileTrigDelayTime (&u4ErrorCode,
                                                        i4_fsrip6RipProfileIndex,
                                                        p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPPROFILEROUTEAGE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipProfileRouteAge (&u4ErrorCode,
                                                   i4_fsrip6RipProfileIndex,
                                                   p_value->i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPPROFILEGARBAGECOLLECTTIME:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipProfileGarbageCollectTime (&u4ErrorCode,
                                                             i4_fsrip6RipProfileIndex,
                                                             p_value->
                                                             i4_SLongValue);

            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRIP6RIPPROFILEINDEX:
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsrip6RipRouteEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrip6RipRouteEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsrip6RipRouteTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipRouteDest = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipRouteDest = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_next_fsrip6RipRouteDest = NULL;

    INT4                i4_fsrip6RipRoutePfxLength = FALSE;
    INT4                i4_next_fsrip6RipRoutePfxLength = FALSE;

    INT4                i4_fsrip6RipRouteProtocol = FALSE;
    INT4                i4_next_fsrip6RipRouteProtocol = FALSE;

    INT4                i4_fsrip6RipRouteIfIndex = FALSE;
    INT4                i4_next_fsrip6RipRouteIfIndex = FALSE;

    tSNMP_OCTET_STRING_TYPE *poctet_retval_fsrip6RipRouteNextHop = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_len_Octet_index_fsrip6RipRouteDest =
                p_incoming->pu4_OidList[i4_size_offset++];
            i4_size_offset += i4_len_Octet_index_fsrip6RipRouteDest;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;
            i4_size_offset += INTEGER_LEN;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsrip6RipRouteTable_INDEX =
                p_in_db->u4_Length + i4_len_Octet_index_fsrip6RipRouteDest +
                LEN_OF_VARIABLE_LEN_INDEX + INTEGER_LEN + INTEGER_LEN +
                INTEGER_LEN;

            if (LEN_fsrip6RipRouteTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  Allocating Memory for the Get Exact Octet String Index. */
                poctet_fsrip6RipRouteDest =
                    (tSNMP_OCTET_STRING_TYPE *)
                    allocmem_octetstring
                    (i4_len_Octet_index_fsrip6RipRouteDest);
                if ((poctet_fsrip6RipRouteDest == NULL))
                {
                    free_octetstring (poctet_fsrip6RipRouteDest);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*
                 *  This is to Increment the Array Pointer by one Which
                 *  Contains the Length of the Index.
                 */
                i4_offset++;
                /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                for (i4_count = FALSE;
                     i4_count < i4_len_Octet_index_fsrip6RipRouteDest;
                     i4_count++, i4_offset++)
                    poctet_fsrip6RipRouteDest->pu1_OctetList[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /* Extracting The Integer Index. */
                i4_fsrip6RipRoutePfxLength =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_fsrip6RipRouteProtocol =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /* Extracting The Integer Index. */
                i4_fsrip6RipRouteIfIndex =
                    (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                   i4_offset];
                i4_offset++;

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsrip6RipRouteTable
                     (poctet_fsrip6RipRouteDest, i4_fsrip6RipRoutePfxLength,
                      i4_fsrip6RipRouteProtocol,
                      i4_fsrip6RipRouteIfIndex)) != SNMP_SUCCESS)
                {
                    free_octetstring (poctet_fsrip6RipRouteDest);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                /*  Storing the Length of the Octet String Get Exact.  */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    poctet_fsrip6RipRouteDest->i4_Length;
                /*  FOR Loop for storing the value from get first to p_in_db. */
                for (i4_count = FALSE;
                     i4_count < poctet_fsrip6RipRouteDest->i4_Length;
                     i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) poctet_fsrip6RipRouteDest->
                        pu1_OctetList[i4_count];

                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsrip6RipRoutePfxLength;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsrip6RipRouteProtocol;
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    (UINT4) i4_fsrip6RipRouteIfIndex;
                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Allocating Memory for the Get First Octet String Index. */
                poctet_fsrip6RipRouteDest =
                    (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
                if ((poctet_fsrip6RipRouteDest == NULL))
                {
                    free_octetstring (poctet_fsrip6RipRouteDest);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsrip6RipRouteTable
                     (poctet_fsrip6RipRouteDest, &i4_fsrip6RipRoutePfxLength,
                      &i4_fsrip6RipRouteProtocol,
                      &i4_fsrip6RipRouteIfIndex)) == SNMP_SUCCESS)
                {
                    /*  Storing the Length. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_fsrip6RipRouteDest->i4_Length;
                    /*  FOR Loop for storing the value from get first to p_in_db. */
                    for (i4_count = FALSE;
                         i4_count < poctet_fsrip6RipRouteDest->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_fsrip6RipRouteDest->
                            pu1_OctetList[i4_count];

                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsrip6RipRoutePfxLength;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsrip6RipRouteProtocol;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_fsrip6RipRouteIfIndex;
                }
                else
                {
                    free_octetstring (poctet_fsrip6RipRouteDest);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_len_Octet_index_fsrip6RipRouteDest =
                        p_incoming->pu4_OidList[i4_partial_index_len++];
                    i4_partial_index_len +=
                        i4_len_Octet_index_fsrip6RipRouteDest;
                    /*  Allocating Memory for The Octet Str Get Next Index. */
                    poctet_fsrip6RipRouteDest =
                        (tSNMP_OCTET_STRING_TYPE *)
                        allocmem_octetstring
                        (i4_len_Octet_index_fsrip6RipRouteDest);
                    poctet_next_fsrip6RipRouteDest =
                        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);

                    /*  Checking for the malloc failure.  */
                    if ((poctet_fsrip6RipRouteDest == NULL)
                        || (poctet_next_fsrip6RipRouteDest == NULL))
                    {
                        /*  Freeing the Current Index. */
                        free_octetstring (poctet_fsrip6RipRouteDest);
                        free_octetstring (poctet_next_fsrip6RipRouteDest);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }

                    /*
                     *  This is to Increment the Array Pointer by one Which
                     *  Contains the Length of the Index.
                     */
                    i4_offset++;
                    /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                    for (i4_count = FALSE;
                         i4_count < i4_len_Octet_index_fsrip6RipRouteDest;
                         i4_count++, i4_offset++)
                        poctet_fsrip6RipRouteDest->pu1_OctetList[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                }
                else
                {
                    /*
                     *  Memory Allocation of the (Partial) Index of type OID
                     *  and Octet string which is not given by the Manager.
                     */
                    poctet_fsrip6RipRouteDest = allocmem_octetstring (17);
                    poctet_next_fsrip6RipRouteDest = allocmem_octetstring (17);
                    if ((poctet_fsrip6RipRouteDest == NULL)
                        || (poctet_next_fsrip6RipRouteDest == NULL))
                    {
                        free_octetstring (poctet_fsrip6RipRouteDest);
                        free_octetstring (poctet_next_fsrip6RipRouteDest);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }
                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsrip6RipRoutePfxLength =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsrip6RipRouteProtocol =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }
                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_partial_index_len += INTEGER_LEN;
                    /* Extracting The Integer Index. */
                    i4_fsrip6RipRouteIfIndex =
                        (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                       i4_offset];
                    i4_offset++;

                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsrip6RipRouteTable
                     (poctet_fsrip6RipRouteDest, poctet_next_fsrip6RipRouteDest,
                      i4_fsrip6RipRoutePfxLength,
                      &i4_next_fsrip6RipRoutePfxLength,
                      i4_fsrip6RipRouteProtocol,
                      &i4_next_fsrip6RipRouteProtocol, i4_fsrip6RipRouteIfIndex,
                      &i4_next_fsrip6RipRouteIfIndex)) == SNMP_SUCCESS)
                {
                    /*  Storing the Value of the Len of Octet Str in p_in_db. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_next_fsrip6RipRouteDest->i4_Length;
                    for (i4_count = FALSE;
                         i4_count < poctet_next_fsrip6RipRouteDest->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_next_fsrip6RipRouteDest->
                            pu1_OctetList[i4_count];
                    free_octetstring (poctet_fsrip6RipRouteDest);
                    poctet_fsrip6RipRouteDest = poctet_next_fsrip6RipRouteDest;
                    i4_fsrip6RipRoutePfxLength =
                        i4_next_fsrip6RipRoutePfxLength;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsrip6RipRoutePfxLength;
                    i4_fsrip6RipRouteProtocol = i4_next_fsrip6RipRouteProtocol;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsrip6RipRouteProtocol;
                    i4_fsrip6RipRouteIfIndex = i4_next_fsrip6RipRouteIfIndex;
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) i4_next_fsrip6RipRouteIfIndex;
                }
                else
                {
                    free_octetstring (poctet_fsrip6RipRouteDest);
                    free_octetstring (poctet_next_fsrip6RipRouteDest);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRIP6RIPROUTEDEST:
        {
            i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                poctet_string = poctet_fsrip6RipRouteDest;
            }
            else
            {
                poctet_string = poctet_next_fsrip6RipRouteDest;
            }
            break;
        }
        case FSRIP6RIPROUTEPFXLENGTH:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsrip6RipRoutePfxLength;
            }
            else
            {
                i4_return_val = i4_next_fsrip6RipRoutePfxLength;
            }
            free_octetstring (poctet_fsrip6RipRouteDest);
            break;
        }
        case FSRIP6RIPROUTEPROTOCOL:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsrip6RipRouteProtocol;
            }
            else
            {
                i4_return_val = i4_next_fsrip6RipRouteProtocol;
            }
            free_octetstring (poctet_fsrip6RipRouteDest);
            break;
        }
        case FSRIP6RIPROUTEIFINDEX:
        {
            i2_type = SNMP_DATA_TYPE_INTEGER;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                i4_return_val = i4_fsrip6RipRouteIfIndex;
            }
            else
            {
                i4_return_val = i4_next_fsrip6RipRouteIfIndex;
            }
            free_octetstring (poctet_fsrip6RipRouteDest);
            break;
        }
        case FSRIP6RIPROUTENEXTHOP:
        {
            poctet_retval_fsrip6RipRouteNextHop =
                (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
            if (poctet_retval_fsrip6RipRouteNextHop == NULL)
            {
                free_octetstring (poctet_fsrip6RipRouteDest);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            i1_ret_val =
                nmhGetFsrip6RipRouteNextHop (poctet_fsrip6RipRouteDest,
                                             i4_fsrip6RipRoutePfxLength,
                                             i4_fsrip6RipRouteProtocol,
                                             i4_fsrip6RipRouteIfIndex,
                                             poctet_retval_fsrip6RipRouteNextHop);
            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

                /*  Assigning the Octet Str Ptr Got From Low Level Rtns. */
                poctet_string = poctet_retval_fsrip6RipRouteNextHop;
            }
            else
            {
                free_octetstring (poctet_retval_fsrip6RipRouteNextHop);
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPROUTEMETRIC:
        {
            i1_ret_val =
                nmhGetFsrip6RipRouteMetric (poctet_fsrip6RipRouteDest,
                                            i4_fsrip6RipRoutePfxLength,
                                            i4_fsrip6RipRouteProtocol,
                                            i4_fsrip6RipRouteIfIndex,
                                            &i4_return_val);
            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPROUTETYPE:
        {
            i1_ret_val =
                nmhGetFsrip6RipRouteType (poctet_fsrip6RipRouteDest,
                                          i4_fsrip6RipRoutePfxLength,
                                          i4_fsrip6RipRouteProtocol,
                                          i4_fsrip6RipRouteIfIndex,
                                          &i4_return_val);
            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPROUTETAG:
        {
            i1_ret_val =
                nmhGetFsrip6RipRouteTag (poctet_fsrip6RipRouteDest,
                                         i4_fsrip6RipRoutePfxLength,
                                         i4_fsrip6RipRouteProtocol,
                                         i4_fsrip6RipRouteIfIndex,
                                         &i4_return_val);
            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case FSRIP6RIPROUTEAGE:
        {
            i1_ret_val =
                nmhGetFsrip6RipRouteAge (poctet_fsrip6RipRouteDest,
                                         i4_fsrip6RipRoutePfxLength,
                                         i4_fsrip6RipRouteProtocol,
                                         i4_fsrip6RipRouteIfIndex,
                                         &i4_return_val);
            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            free_octetstring (poctet_fsrip6RipRouteDest);
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrip6RipRouteEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrip6RipRouteEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipRouteDest = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipRouteDest = NULL;

    INT4                i4_fsrip6RipRoutePfxLength = FALSE;

    INT4                i4_fsrip6RipRouteProtocol = FALSE;

    INT4                i4_fsrip6RipRouteIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_fsrip6RipRouteDest =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_fsrip6RipRouteDest;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_fsrip6RipRouteDest =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_fsrip6RipRouteDest);
        if ((poctet_fsrip6RipRouteDest == NULL))
        {
            free_octetstring (poctet_fsrip6RipRouteDest);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_fsrip6RipRouteDest;
             i4_count++, i4_offset++)
            poctet_fsrip6RipRouteDest->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_fsrip6RipRoutePfxLength =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsrip6RipRouteProtocol =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsrip6RipRouteIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRIP6RIPROUTENEXTHOP:
        {
            i1_ret_val =
                nmhSetFsrip6RipRouteNextHop (poctet_fsrip6RipRouteDest,
                                             i4_fsrip6RipRoutePfxLength,
                                             i4_fsrip6RipRouteProtocol,
                                             i4_fsrip6RipRouteIfIndex,
                                             p_value->pOctetStrValue);

            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPROUTEMETRIC:
        {
            i1_ret_val =
                nmhSetFsrip6RipRouteMetric (poctet_fsrip6RipRouteDest,
                                            i4_fsrip6RipRoutePfxLength,
                                            i4_fsrip6RipRouteProtocol,
                                            i4_fsrip6RipRouteIfIndex,
                                            p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPROUTETYPE:
        {
            i1_ret_val =
                nmhSetFsrip6RipRouteType (poctet_fsrip6RipRouteDest,
                                          i4_fsrip6RipRoutePfxLength,
                                          i4_fsrip6RipRouteProtocol,
                                          i4_fsrip6RipRouteIfIndex,
                                          p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
        case FSRIP6RIPROUTETAG:
        {
            i1_ret_val =
                nmhSetFsrip6RipRouteTag (poctet_fsrip6RipRouteDest,
                                         i4_fsrip6RipRoutePfxLength,
                                         i4_fsrip6RipRouteProtocol,
                                         i4_fsrip6RipRouteIfIndex,
                                         p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRIP6RIPROUTEDEST:
            /*  Read Only Variables. */
        case FSRIP6RIPROUTEPFXLENGTH:
            /*  Read Only Variables. */
        case FSRIP6RIPROUTEPROTOCOL:
            /*  Read Only Variables. */
        case FSRIP6RIPROUTEIFINDEX:
            /*  Read Only Variables. */
        case FSRIP6RIPROUTEAGE:
            free_octetstring (poctet_fsrip6RipRouteDest);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_fsrip6RipRouteDest);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrip6RipRouteEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrip6RipRouteEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                         UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipRouteDest = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipRouteDest = NULL;

    INT4                i4_fsrip6RipRoutePfxLength = FALSE;

    INT4                i4_fsrip6RipRouteProtocol = FALSE;

    INT4                i4_fsrip6RipRouteIfIndex = FALSE;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_fsrip6RipRouteDest =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_fsrip6RipRouteDest;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;
        i4_size_offset += INTEGER_LEN;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_fsrip6RipRouteDest =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_fsrip6RipRouteDest);
        if ((poctet_fsrip6RipRouteDest == NULL))
        {
            free_octetstring (poctet_fsrip6RipRouteDest);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_fsrip6RipRouteDest;
             i4_count++, i4_offset++)
            poctet_fsrip6RipRouteDest->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

        /* Extracting The Integer Index. */
        i4_fsrip6RipRoutePfxLength =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsrip6RipRouteProtocol =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

        /* Extracting The Integer Index. */
        i4_fsrip6RipRouteIfIndex =
            (INT4) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];
        i4_offset++;

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsrip6RipRouteTable(poctet_fsrip6RipRouteDest , i4_fsrip6RipRoutePfxLength , i4_fsrip6RipRouteProtocol , i4_fsrip6RipRouteIfIndex)) != SNMP_SUCCESS) {
       free_octetstring(poctet_fsrip6RipRouteDest);
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRIP6RIPROUTENEXTHOP:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_OCTET_PRIM)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipRouteNextHop (&u4ErrorCode,
                                                poctet_fsrip6RipRouteDest,
                                                i4_fsrip6RipRoutePfxLength,
                                                i4_fsrip6RipRouteProtocol,
                                                i4_fsrip6RipRouteIfIndex,
                                                p_value->pOctetStrValue);

            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPROUTEMETRIC:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipRouteMetric (&u4ErrorCode,
                                               poctet_fsrip6RipRouteDest,
                                               i4_fsrip6RipRoutePfxLength,
                                               i4_fsrip6RipRouteProtocol,
                                               i4_fsrip6RipRouteIfIndex,
                                               p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPROUTETYPE:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipRouteType (&u4ErrorCode,
                                             poctet_fsrip6RipRouteDest,
                                             i4_fsrip6RipRoutePfxLength,
                                             i4_fsrip6RipRouteProtocol,
                                             i4_fsrip6RipRouteIfIndex,
                                             p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }

        case FSRIP6RIPROUTETAG:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipRouteTag (&u4ErrorCode,
                                            poctet_fsrip6RipRouteDest,
                                            i4_fsrip6RipRoutePfxLength,
                                            i4_fsrip6RipRouteProtocol,
                                            i4_fsrip6RipRouteIfIndex,
                                            p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipRouteDest);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRIP6RIPROUTEDEST:
        case FSRIP6RIPROUTEPFXLENGTH:
        case FSRIP6RIPROUTEPROTOCOL:
        case FSRIP6RIPROUTEIFINDEX:
        case FSRIP6RIPROUTEAGE:
            free_octetstring (poctet_fsrip6RipRouteDest);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_fsrip6RipRouteDest);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsrip6RipPeerEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrip6RipPeerEntryGet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsrip6RipPeerTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipPeerAddr = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipPeerAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_next_fsrip6RipPeerAddr = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_len_Octet_index_fsrip6RipPeerAddr =
                p_incoming->pu4_OidList[i4_size_offset++];
            i4_size_offset += i4_len_Octet_index_fsrip6RipPeerAddr;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsrip6RipPeerTable_INDEX =
                p_in_db->u4_Length + i4_len_Octet_index_fsrip6RipPeerAddr +
                LEN_OF_VARIABLE_LEN_INDEX;

            if (LEN_fsrip6RipPeerTable_INDEX == (INT4) p_incoming->u4_Length)
            {
                /*  Allocating Memory for the Get Exact Octet String Index. */
                poctet_fsrip6RipPeerAddr =
                    (tSNMP_OCTET_STRING_TYPE *)
                    allocmem_octetstring (i4_len_Octet_index_fsrip6RipPeerAddr);
                if ((poctet_fsrip6RipPeerAddr == NULL))
                {
                    free_octetstring (poctet_fsrip6RipPeerAddr);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*
                 *  This is to Increment the Array Pointer by one Which
                 *  Contains the Length of the Index.
                 */
                i4_offset++;
                /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                for (i4_count = FALSE;
                     i4_count < i4_len_Octet_index_fsrip6RipPeerAddr;
                     i4_count++, i4_offset++)
                    poctet_fsrip6RipPeerAddr->pu1_OctetList[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsrip6RipPeerTable
                     (poctet_fsrip6RipPeerAddr)) != SNMP_SUCCESS)
                {
                    free_octetstring (poctet_fsrip6RipPeerAddr);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                /*  Storing the Length of the Octet String Get Exact.  */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    poctet_fsrip6RipPeerAddr->i4_Length;
                /*  FOR Loop for storing the value from get first to p_in_db. */
                for (i4_count = FALSE;
                     i4_count < poctet_fsrip6RipPeerAddr->i4_Length; i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) poctet_fsrip6RipPeerAddr->
                        pu1_OctetList[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Allocating Memory for the Get First Octet String Index. */
                poctet_fsrip6RipPeerAddr =
                    (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
                if ((poctet_fsrip6RipPeerAddr == NULL))
                {
                    free_octetstring (poctet_fsrip6RipPeerAddr);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsrip6RipPeerTable
                     (poctet_fsrip6RipPeerAddr)) == SNMP_SUCCESS)
                {
                    /*  Storing the Length. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_fsrip6RipPeerAddr->i4_Length;
                    /*  FOR Loop for storing the value from get first to p_in_db. */
                    for (i4_count = FALSE;
                         i4_count < poctet_fsrip6RipPeerAddr->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_fsrip6RipPeerAddr->
                            pu1_OctetList[i4_count];

                }
                else
                {
                    free_octetstring (poctet_fsrip6RipPeerAddr);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_len_Octet_index_fsrip6RipPeerAddr =
                        p_incoming->pu4_OidList[i4_partial_index_len++];
                    i4_partial_index_len +=
                        i4_len_Octet_index_fsrip6RipPeerAddr;
                    /*  Allocating Memory for The Octet Str Get Next Index. */
                    poctet_fsrip6RipPeerAddr =
                        (tSNMP_OCTET_STRING_TYPE *)
                        allocmem_octetstring
                        (i4_len_Octet_index_fsrip6RipPeerAddr);
                    poctet_next_fsrip6RipPeerAddr =
                        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);

                    /*  Checking for the malloc failure.  */
                    if ((poctet_fsrip6RipPeerAddr == NULL)
                        || (poctet_next_fsrip6RipPeerAddr == NULL))
                    {
                        /*  Freeing the Current Index. */
                        free_octetstring (poctet_fsrip6RipPeerAddr);
                        free_octetstring (poctet_next_fsrip6RipPeerAddr);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }

                    /*
                     *  This is to Increment the Array Pointer by one Which
                     *  Contains the Length of the Index.
                     */
                    i4_offset++;
                    /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                    for (i4_count = FALSE;
                         i4_count < i4_len_Octet_index_fsrip6RipPeerAddr;
                         i4_count++, i4_offset++)
                        poctet_fsrip6RipPeerAddr->pu1_OctetList[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                }
                else
                {
                    /*
                     *  Memory Allocation of the (Partial) Index of type OID
                     *  and Octet string which is not given by the Manager.
                     */
                    poctet_fsrip6RipPeerAddr = allocmem_octetstring (17);
                    poctet_next_fsrip6RipPeerAddr = allocmem_octetstring (17);
                    if ((poctet_fsrip6RipPeerAddr == NULL)
                        || (poctet_next_fsrip6RipPeerAddr == NULL))
                    {
                        free_octetstring (poctet_fsrip6RipPeerAddr);
                        free_octetstring (poctet_next_fsrip6RipPeerAddr);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }
                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsrip6RipPeerTable
                     (poctet_fsrip6RipPeerAddr,
                      poctet_next_fsrip6RipPeerAddr)) == SNMP_SUCCESS)
                {
                    /*  Storing the Value of the Len of Octet Str in p_in_db. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_next_fsrip6RipPeerAddr->i4_Length;
                    for (i4_count = FALSE;
                         i4_count < poctet_next_fsrip6RipPeerAddr->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_next_fsrip6RipPeerAddr->
                            pu1_OctetList[i4_count];
                    free_octetstring (poctet_fsrip6RipPeerAddr);
                    poctet_fsrip6RipPeerAddr = poctet_next_fsrip6RipPeerAddr;
                }
                else
                {
                    free_octetstring (poctet_fsrip6RipPeerAddr);
                    free_octetstring (poctet_next_fsrip6RipPeerAddr);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRIP6RIPPEERADDR:
        {
            i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                poctet_string = poctet_fsrip6RipPeerAddr;
            }
            else
            {
                poctet_string = poctet_next_fsrip6RipPeerAddr;
            }
            break;
        }
        case FSRIP6RIPPEERENTRYSTATUS:
        {
            i1_ret_val =
                nmhGetFsrip6RipPeerEntryStatus (poctet_fsrip6RipPeerAddr,
                                                &i4_return_val);
            free_octetstring (poctet_fsrip6RipPeerAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            free_octetstring (poctet_fsrip6RipPeerAddr);
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrip6RipPeerEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrip6RipPeerEntrySet (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                       UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipPeerAddr = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipPeerAddr = NULL;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_fsrip6RipPeerAddr =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_fsrip6RipPeerAddr;
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_fsrip6RipPeerAddr =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_fsrip6RipPeerAddr);
        if ((poctet_fsrip6RipPeerAddr == NULL))
        {
            free_octetstring (poctet_fsrip6RipPeerAddr);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_fsrip6RipPeerAddr;
             i4_count++, i4_offset++)
            poctet_fsrip6RipPeerAddr->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRIP6RIPPEERENTRYSTATUS:
        {
            i1_ret_val =
                nmhSetFsrip6RipPeerEntryStatus (poctet_fsrip6RipPeerAddr,
                                                p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipPeerAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRIP6RIPPEERADDR:
            free_octetstring (poctet_fsrip6RipPeerAddr);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_fsrip6RipPeerAddr);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrip6RipPeerEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrip6RipPeerEntryTest (tSNMP_OID_TYPE * p_in_db, tSNMP_OID_TYPE * p_incoming,
                        UINT1 u1_arg, tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipPeerAddr = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipPeerAddr = NULL;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_fsrip6RipPeerAddr =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_fsrip6RipPeerAddr;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_fsrip6RipPeerAddr =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_fsrip6RipPeerAddr);
        if ((poctet_fsrip6RipPeerAddr == NULL))
        {
            free_octetstring (poctet_fsrip6RipPeerAddr);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE; i4_count < i4_len_Octet_index_fsrip6RipPeerAddr;
             i4_count++, i4_offset++)
            poctet_fsrip6RipPeerAddr->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsrip6RipPeerTable(poctet_fsrip6RipPeerAddr)) != SNMP_SUCCESS) {
       free_octetstring(poctet_fsrip6RipPeerAddr);
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRIP6RIPPEERENTRYSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipPeerEntryStatus (&u4ErrorCode,
                                                   poctet_fsrip6RipPeerAddr,
                                                   p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipPeerAddr);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRIP6RIPPEERADDR:
            free_octetstring (poctet_fsrip6RipPeerAddr);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_fsrip6RipPeerAddr);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */

/****************************************************************************
 Function   : fsrip6RipAdvFilterEntryGet
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db       : The OID as formed by the SNMP Agent
                              from the static MIB database.
              p_incoming    : The OID as it is send by the manager
                              in the SNMP PDU.
              u1_arg        : The position of the variable in the MIB group.
              u1_search_type: Indicates whether this routine is called
                              as a GET or GETNEXT.
 Output     : The actual OID for which the operation is performed is
              returned in p_in_db.
 Returns    : Returns a pointer to tSNMP_VAR_BIND structure, with either
              the corresponding value, or NULL if the GET/GETNEXT
              operation could not be performed successfully.
****************************************************************************/

/* Prototype declarations for get_routine. */
tSNMP_VAR_BIND     *
fsrip6RipAdvFilterEntryGet (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            UINT1 u1_search_type)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for get routine . */
    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_partial_index_len = FALSE;
    INT4                i4_partial_index_flag = TRUE;
    INT4                i4_size_offset = FALSE;
    INT4                LEN_fsrip6RipAdvFilterTable_INDEX;

    /*  The Declaration of the Vars which are passed to FormVarbind Fn. */
    INT4                i4_return_val = FALSE;
    UINT4               u4_counter_val = FALSE;
    tSNMP_COUNTER64_TYPE u8_counter_val = { 0, 0 };
    INT2                i2_type;

    /*
     *  The Declaration of the Octet String Array which is used for
     *  conversion and The Pointer of tSNMP_OCTET_STRING_TYPE.
     */
    tSNMP_OCTET_STRING_TYPE *poctet_string = NULL;
    tSNMP_OID_TYPE     *pOidValue = NULL;

    /*
     *  These Variable are declared for being used in the
     *  FOR Loop for extracting Indices from the given OID.
     */
    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipAdvFilterAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipAdvFilterAddress = NULL;
    tSNMP_OCTET_STRING_TYPE *poctet_next_fsrip6RipAdvFilterAddress = NULL;

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    switch (u1_search_type)
    {

        case SNMP_SEARCH_TYPE_EXACT:
        {
            /*
             *  Initializing the i4_size_offset Variable to the Length of the
             *  OID of the Object i.e the Length present in the p_in_db.
             */
            i4_size_offset = p_in_db->u4_Length;
            i4_len_Octet_index_fsrip6RipAdvFilterAddress =
                p_incoming->pu4_OidList[i4_size_offset++];
            i4_size_offset += i4_len_Octet_index_fsrip6RipAdvFilterAddress;

            if (p_incoming->u4_Length != (UINT4) i4_size_offset)
            {
                return (NULL);
            }
            /*
             *  Finding the length of the Index for extracting
             *  Index from the structure given by the Manager.
             */
            /*
             *  If LEN_OF_VARIABLE_LEN_INDEX is Present refer Header File
             *  where it is Defined for Explanation of the Variable.
             */
            LEN_fsrip6RipAdvFilterTable_INDEX =
                p_in_db->u4_Length +
                i4_len_Octet_index_fsrip6RipAdvFilterAddress +
                LEN_OF_VARIABLE_LEN_INDEX;

            if (LEN_fsrip6RipAdvFilterTable_INDEX ==
                (INT4) p_incoming->u4_Length)
            {
                /*  Allocating Memory for the Get Exact Octet String Index. */
                poctet_fsrip6RipAdvFilterAddress =
                    (tSNMP_OCTET_STRING_TYPE *)
                    allocmem_octetstring
                    (i4_len_Octet_index_fsrip6RipAdvFilterAddress);
                if ((poctet_fsrip6RipAdvFilterAddress == NULL))
                {
                    free_octetstring (poctet_fsrip6RipAdvFilterAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*
                 *  This is to Increment the Array Pointer by one Which
                 *  Contains the Length of the Index.
                 */
                i4_offset++;
                /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                for (i4_count = FALSE;
                     i4_count < i4_len_Octet_index_fsrip6RipAdvFilterAddress;
                     i4_count++, i4_offset++)
                    poctet_fsrip6RipAdvFilterAddress->pu1_OctetList[i4_count] =
                        (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                        i4_offset];

                /*  Low Level Routine Which Validates the Indices. */
                if ((i1_ret_val =
                     nmhValidateIndexInstanceFsrip6RipAdvFilterTable
                     (poctet_fsrip6RipAdvFilterAddress)) != SNMP_SUCCESS)
                {
                    free_octetstring (poctet_fsrip6RipAdvFilterAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Storing the Extracted Index in p_in_db. */
                /*  Storing the Length of the Octet String Get Exact.  */
                p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                    poctet_fsrip6RipAdvFilterAddress->i4_Length;
                /*  FOR Loop for storing the value from get first to p_in_db. */
                for (i4_count = FALSE;
                     i4_count < poctet_fsrip6RipAdvFilterAddress->i4_Length;
                     i4_count++)
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        (UINT4) poctet_fsrip6RipAdvFilterAddress->
                        pu1_OctetList[i4_count];

                i4_partial_index_flag = FALSE;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        case SNMP_SEARCH_TYPE_NEXT:
        {
            /*  The Manager Has Not Given the Indices. */
            if (p_incoming->u4_Length <= p_in_db->u4_Length)
            {
                /*  Allocating Memory for the Get First Octet String Index. */
                poctet_fsrip6RipAdvFilterAddress =
                    (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);
                if ((poctet_fsrip6RipAdvFilterAddress == NULL))
                {
                    free_octetstring (poctet_fsrip6RipAdvFilterAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
                /*  Flag is Set to Indicate Low Level Get First Operation. */
                i4_partial_index_flag = FALSE;
                if ((i1_ret_val
                     =
                     nmhGetFirstIndexFsrip6RipAdvFilterTable
                     (poctet_fsrip6RipAdvFilterAddress)) == SNMP_SUCCESS)
                {
                    /*  Storing the Length. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_fsrip6RipAdvFilterAddress->i4_Length;
                    /*  FOR Loop for storing the value from get first to p_in_db. */
                    for (i4_count = FALSE;
                         i4_count < poctet_fsrip6RipAdvFilterAddress->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_fsrip6RipAdvFilterAddress->
                            pu1_OctetList[i4_count];

                }
                else
                {
                    free_octetstring (poctet_fsrip6RipAdvFilterAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else if (p_incoming->u4_Length > p_in_db->u4_Length)
            {
                /*  Flag is Set to Indicate Get First Operation. */
                i4_partial_index_flag = TRUE;

                /*  Initializing the Partial Index Length. */
                i4_partial_index_len = p_in_db->u4_Length;

                if ((INT4) p_incoming->u4_Length > i4_partial_index_len)
                {
                    /*  Adding the Len of Index to i4_partial_index_len Var. */
                    i4_len_Octet_index_fsrip6RipAdvFilterAddress =
                        p_incoming->pu4_OidList[i4_partial_index_len++];
                    i4_partial_index_len +=
                        i4_len_Octet_index_fsrip6RipAdvFilterAddress;
                    /*  Allocating Memory for The Octet Str Get Next Index. */
                    poctet_fsrip6RipAdvFilterAddress =
                        (tSNMP_OCTET_STRING_TYPE *)
                        allocmem_octetstring
                        (i4_len_Octet_index_fsrip6RipAdvFilterAddress);
                    poctet_next_fsrip6RipAdvFilterAddress =
                        (tSNMP_OCTET_STRING_TYPE *) allocmem_octetstring (17);

                    /*  Checking for the malloc failure.  */
                    if ((poctet_fsrip6RipAdvFilterAddress == NULL)
                        || (poctet_next_fsrip6RipAdvFilterAddress == NULL))
                    {
                        /*  Freeing the Current Index. */
                        free_octetstring (poctet_fsrip6RipAdvFilterAddress);
                        free_octetstring
                            (poctet_next_fsrip6RipAdvFilterAddress);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }

                    /*
                     *  This is to Increment the Array Pointer by one Which
                     *  Contains the Length of the Index.
                     */
                    i4_offset++;
                    /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
                    for (i4_count = FALSE;
                         i4_count <
                         i4_len_Octet_index_fsrip6RipAdvFilterAddress;
                         i4_count++, i4_offset++)
                        poctet_fsrip6RipAdvFilterAddress->
                            pu1_OctetList[i4_count] =
                            (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length +
                                                            i4_offset];

                }
                else
                {
                    /*
                     *  Memory Allocation of the (Partial) Index of type OID
                     *  and Octet string which is not given by the Manager.
                     */
                    poctet_fsrip6RipAdvFilterAddress =
                        allocmem_octetstring (17);
                    poctet_next_fsrip6RipAdvFilterAddress =
                        allocmem_octetstring (17);
                    if ((poctet_fsrip6RipAdvFilterAddress == NULL)
                        || (poctet_next_fsrip6RipAdvFilterAddress == NULL))
                    {
                        free_octetstring (poctet_fsrip6RipAdvFilterAddress);
                        free_octetstring
                            (poctet_next_fsrip6RipAdvFilterAddress);
                        return ((tSNMP_VAR_BIND *) NULL);
                    }
                }

                /*
                 *  Get the value of the variable with the new index
                 *  By Calling the Low Level GET_NEXT routine.
                 */
                if ((i1_ret_val
                     =
                     nmhGetNextIndexFsrip6RipAdvFilterTable
                     (poctet_fsrip6RipAdvFilterAddress,
                      poctet_next_fsrip6RipAdvFilterAddress)) == SNMP_SUCCESS)
                {
                    /*  Storing the Value of the Len of Octet Str in p_in_db. */
                    p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                        poctet_next_fsrip6RipAdvFilterAddress->i4_Length;
                    for (i4_count = FALSE;
                         i4_count <
                         poctet_next_fsrip6RipAdvFilterAddress->i4_Length;
                         i4_count++)
                        p_in_db->pu4_OidList[p_in_db->u4_Length++] =
                            (UINT4) poctet_next_fsrip6RipAdvFilterAddress->
                            pu1_OctetList[i4_count];
                    free_octetstring (poctet_fsrip6RipAdvFilterAddress);
                    poctet_fsrip6RipAdvFilterAddress =
                        poctet_next_fsrip6RipAdvFilterAddress;
                }
                else
                {
                    free_octetstring (poctet_fsrip6RipAdvFilterAddress);
                    free_octetstring (poctet_next_fsrip6RipAdvFilterAddress);
                    return ((tSNMP_VAR_BIND *) NULL);
                }
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            return ((tSNMP_VAR_BIND *) NULL);

    }                            /* End of SWITCH statement. */

    /*
     *  The SWITCH CASE statement for calling the Low Level fn
     *  for all objects Low Level Routines for all Objects.
     */
    switch (u1_arg)
    {
        case FSRIP6RIPADVFILTERADDRESS:
        {
            i2_type = SNMP_DATA_TYPE_OCTET_PRIM;

            /*
             *  This is for the Indices Which are not accessible
             *  They are not Passed to the Low Level Routine but
             *  Extracted in the Midlevel function and Returned.
             */
            if ((u1_search_type == SNMP_SEARCH_TYPE_EXACT)
                || (i4_partial_index_flag == FALSE))
            {
                poctet_string = poctet_fsrip6RipAdvFilterAddress;
            }
            else
            {
                poctet_string = poctet_next_fsrip6RipAdvFilterAddress;
            }
            break;
        }
        case FSRIP6RIPADVFILTERSTATUS:
        {
            i1_ret_val =
                nmhGetFsrip6RipAdvFilterStatus
                (poctet_fsrip6RipAdvFilterAddress, &i4_return_val);
            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                i2_type = SNMP_DATA_TYPE_INTEGER;
            }
            else
            {
                return ((tSNMP_VAR_BIND *) NULL);
            }
            break;
        }
        default:
            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            return ((tSNMP_VAR_BIND *) NULL);
    }                            /* End Of SWITCH Case for All Objects. */

    return (SNMP_AGT_FormVarBind
            (p_in_db, i2_type, u4_counter_val, i4_return_val, poctet_string,
             pOidValue, u8_counter_val));

}                                /*   THE GET FUNCTION GETS OVER . */

/****************************************************************************
 Function   : fsrip6RipAdvFilterEntrySet
 Description: This routine returns the value of the requestedMIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Output     : The actual OID for which the operation is
              performed is returned in p_in_db.
 Returns    : Returns one of the following error codes
           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for set_routine. */
INT4
fsrip6RipAdvFilterEntrySet (tSNMP_OID_TYPE * p_in_db,
                            tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                            tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for set routine */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    /*
     *  This Variable is used to extract the Value in The
     *  MULTI DATA TYPE which is sent by the Manager.
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipAdvFilterAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipAdvFilterAddress = NULL;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_fsrip6RipAdvFilterAddress =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_fsrip6RipAdvFilterAddress;
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_fsrip6RipAdvFilterAddress =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_fsrip6RipAdvFilterAddress);
        if ((poctet_fsrip6RipAdvFilterAddress == NULL))
        {
            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE;
             i4_count < i4_len_Octet_index_fsrip6RipAdvFilterAddress;
             i4_count++, i4_offset++)
            poctet_fsrip6RipAdvFilterAddress->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

    }                            /* End Of Else Condition. */
    switch (u1_arg)
    {
        case FSRIP6RIPADVFILTERSTATUS:
        {
            i1_ret_val =
                nmhSetFsrip6RipAdvFilterStatus
                (poctet_fsrip6RipAdvFilterAddress, p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                return (SNMP_ERR_GEN_ERR);
            }
            break;
        }
            /*  Read Only Variables. */
        case FSRIP6RIPADVFILTERADDRESS:
            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*   THE SET ROUTINES GET OVER. */

/****************************************************************************
 Function   : fsrip6RipAdvFilterEntryTest
 Description: This routine returns the value of the requested MIB variable.
 Input      : p_in_db   : The OID as formed by the SNMP Agent
                          from the static MIB database.
              p_incoming: The OID as it is send by the manager
                          in the SNMP PDU.
              u1_arg    : The position of the variable in the MIB group.
              p_value   : Points to MultiDataType of the variable
                          specified by p_incoming.
 Returns    : Returns one of the following error codes

           SNMP_ERR_INCONSISTNT_NAME
           SNMP_ERR_INCONSISTNT_VALUE
           SNMP_ERR_WRONG_VALUE
           SNMP_ERR_WRONG_LENGTH
           SNMP_ERR_NOT_WRITABLE
           SNMP_ERR_NO_CREATION
           SNMP_ERR_WRONG_TYPE
           SNMP_ERR_GEN_ERR
****************************************************************************/

/* Prototype declarations for test_routine */

INT4
fsrip6RipAdvFilterEntryTest (tSNMP_OID_TYPE * p_in_db,
                             tSNMP_OID_TYPE * p_incoming, UINT1 u1_arg,
                             tSNMP_MULTI_DATA_TYPE * p_value)
{

/*** DECLARATION_BEGIN ***/

    /* Declarations for Test routine . */

    UINT1               i1_ret_val = FALSE;
    INT4                i4_offset = FALSE;
    INT4                i4_size_offset = FALSE;
    UINT4               u4ErrorCode = 0;
    /*
     *  This Variable is used to extract the Value in The
     *  Multi Data Type which is sent by the Manager
     */

    INT4                i4_count = FALSE;
    /*
     *  The tSNMP_OCTET_STRING_TYPE Which Store
     *  the Length and the Octet String.
     */
    INT4                i4_len_Octet_index_fsrip6RipAdvFilterAddress = FALSE;
    tSNMP_OCTET_STRING_TYPE *poctet_fsrip6RipAdvFilterAddress = NULL;

    /*
     *  The Declaration of variables which are used to extract
     *  the Values From the MULTI DATA TYPE sent by the Manager.
     */

/*** DECLARATION_END ***/

    i4_offset = FALSE;
    if (p_incoming->u4_Length <= p_in_db->u4_Length)
    {
        return (SNMP_ERR_WRONG_LENGTH);
    }
    else
    {
        /*
         *  Initializing the i4_size_offset Variable to the Length of the
         *  OID of the Object i.e the Length present in the p_in_db.
         */
        i4_size_offset = p_in_db->u4_Length;
        i4_len_Octet_index_fsrip6RipAdvFilterAddress =
            p_incoming->pu4_OidList[i4_size_offset++];
        i4_size_offset += i4_len_Octet_index_fsrip6RipAdvFilterAddress;

        if (p_incoming->u4_Length != (UINT4) i4_size_offset)
        {
            return (SNMP_ERR_WRONG_LENGTH);
        }
        /*  Allocating Memory for the Get Exact Octet String Index. */
        poctet_fsrip6RipAdvFilterAddress =
            (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (i4_len_Octet_index_fsrip6RipAdvFilterAddress);
        if ((poctet_fsrip6RipAdvFilterAddress == NULL))
        {
            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            return (SNMP_ERR_GEN_ERR);
        }
        /*
         *  This is to Increment the Array Pointer by one Which
         *  Contains the Length of the Index.
         */
        i4_offset++;
        /*  The FOR LOOP for extracting Octet_str Index from the Given OID. */
        for (i4_count = FALSE;
             i4_count < i4_len_Octet_index_fsrip6RipAdvFilterAddress;
             i4_count++, i4_offset++)
            poctet_fsrip6RipAdvFilterAddress->pu1_OctetList[i4_count] =
                (UINT1) p_incoming->pu4_OidList[p_in_db->u4_Length + i4_offset];

    }
    /*  Low Level Routine Which Validates the Indices. */
    /* COMMENT FOLLOWING CHECK TO ENABLE ROW CREATION 

       if((i1_ret_val = nmhValidateIndexInstanceFsrip6RipAdvFilterTable(poctet_fsrip6RipAdvFilterAddress)) != SNMP_SUCCESS) {
       free_octetstring(poctet_fsrip6RipAdvFilterAddress);
       return SNMP_ERR_INCONSISTENT_NAME;
       } */
    switch (u1_arg)
    {

        case FSRIP6RIPADVFILTERSTATUS:
        {
            if (p_value->i2_DataType != SNMP_DATA_TYPE_INTEGER)
            {
                return (SNMP_ERR_WRONG_TYPE);
            }

            i1_ret_val =
                nmhTestv2Fsrip6RipAdvFilterStatus (&u4ErrorCode,
                                                   poctet_fsrip6RipAdvFilterAddress,
                                                   p_value->i4_SLongValue);

            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            if (i1_ret_val == SNMP_SUCCESS)
            {
                return (SNMP_ERR_NO_ERROR);
            }
            else
            {
                switch (u4ErrorCode)
                {
                    case SNMP_ERR_WRONG_LENGTH:
                    case SNMP_ERR_WRONG_VALUE:
                    case SNMP_ERR_NO_CREATION:
                    case SNMP_ERR_INCONSISTENT_VALUE:
                    case SNMP_ERR_INCONSISTENT_NAME:
                        return (u4ErrorCode);
                    default:
                        return (SNMP_ERR_WRONG_VALUE);
                }
            }
            break;
        }
            /*  Read Only Variables */

        case FSRIP6RIPADVFILTERADDRESS:
            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            return (SNMP_ERR_NOT_WRITABLE);

        default:
            free_octetstring (poctet_fsrip6RipAdvFilterAddress);
            return (SNMP_ERR_INCONSISTENT_NAME);
    }                            /* End of Switch */
}                                /*  THE TEST ROUTINES ARE OVER . */
