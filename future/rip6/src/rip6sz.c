/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6sz.c,v 1.4 2013/11/29 11:04:15 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _RIP6SZ_C
#include "rip6inc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
Rip6SizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RIP6_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsRIP6SizingParams[i4SizingId].u4StructSize,
                              FsRIP6SizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(RIP6MemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            Rip6SizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
Rip6SzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRIP6SizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RIP6MemPoolIds);
    return OSIX_SUCCESS;
}

VOID
Rip6SizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RIP6_MAX_SIZING_ID; i4SizingId++)
    {
        if (RIP6MemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RIP6MemPoolIds[i4SizingId]);
            RIP6MemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
