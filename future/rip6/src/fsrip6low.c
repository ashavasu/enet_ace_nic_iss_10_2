/* $Id: fsrip6low.c,v 1.18 2017/02/21 14:06:43 siva Exp $*/

#include "rip6inc.h"
#include "rip6cli.h"
#include "rmgr.h"

extern UINT4        Fsrip6RipIfProtocolEnable[12];
extern UINT4        Fsrip6RRDSrcProtoMaskForDisable[10];
extern UINT4        Fsrip6PeerFilter[10];
extern UINT4        Fsrip6AdvFilter[10];
extern UINT4        Fsrip6RipIfCost[12];
extern UINT4        Fsrip6RipIfDefRouteAdvt[12];
extern UINT4        Fsrip6RipPeerEntryStatus[12];
extern UINT4        Fsrip6RipAdvFilterStatus[12];
extern UINT4        Fsrip6RipProfileHorizon[12];

#ifdef ROUTEMAP_WANTED
PRIVATE tFilteringRMap gaDistanceFiltName[1];
PRIVATE tFilteringRMap gaDistributeOutFiltName[1];
PRIVATE tFilteringRMap gaDistributeInFiltName[1];
#endif

/* GET routines for SCALARS in ripng.mib*/

/****************************************************************************
 Function    :  nmhGetFsrip6RoutePreference
 Input       :  The Indices

                The Object
                retValIpv6RoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
extern UINT1        gu1Rip6Distance;

INT1
nmhGetFsrip6RoutePreference (INT4 *pi4RetValIpv6RoutePreference)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhGetFsMIrip6RoutePreference (gu4InstanceIndex,
                                           pi4RetValIpv6RoutePreference) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsrip6GlobalDebug
 Input       :  The Indices

                The Object
                retValFsrip6GlobalDebug
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6GlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6GlobalDebug (INT4 *pi4RetValFsrip6GlobalDebug)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValFsrip6GlobalDebug = (INT4)gu4Rip6DbgMap;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsrip6GlobalInstanceIndex
 Input       :  NONE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6GlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6GlobalInstanceIndex (INT4 *pi4RetValFsrip6InstanceIndex)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    *pi4RetValFsrip6InstanceIndex = (INT4)gu4InstanceIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsrip6PeerFilter
 Input       :  The Indices

                The Object 
                retValFsrip6PeerFilter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrip6PeerFilter (INT4 *pi4RetValFsrip6PeerFilter)
{
    if (nmhGetFsMIrip6PeerFilter (gu4InstanceIndex, pi4RetValFsrip6PeerFilter)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6AdvFilter
 Input       :  The Indices

                The Object 
                retValFsrip6AdvFilter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrip6AdvFilter (INT4 *pi4RetValFsrip6AdvFilter)
{
    if (nmhGetFsMIrip6AdvFilter (gu4InstanceIndex, pi4RetValFsrip6AdvFilter)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 *  Function    :  nmhGetFsRip6RRDAdminStatus
 *  Input       :  The Indices
 *
 *                 The Object
 *                     retValFsRip6RRDAdminStatus
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsRip6RRDAdminStatus (INT4 *pi4RetValFsRip6RRDAdminStatus)
{
    *pi4RetValFsRip6RRDAdminStatus = (INT4)gRip6Redistribution.u4RRDAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsrip6RRDProtoMaskForEnable
 *  Input       :  The Indices
 *
 *                   The Object
 *                       retValFsrip6RRDProtoMaskForEnable
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsrip6RRDProtoMaskForEnable (INT4 *pi4RetValFsrip6RRDProtoMaskForEnable)
{
    *pi4RetValFsrip6RRDProtoMaskForEnable = (INT4)gRip6Redistribution.u4ProtoMask;
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    :  nmhGetFsrip6RRDSrcProtoMaskForDisable
 *  Input       :  The Indices
 *
 *                 The Object
 *                     retValFsrip6RRDSrcProtoMaskForDisable
 *  Output      :  The Get Low Lev Routine Take the Indices &
 *                 store the Value requested in the Return val.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhGetFsrip6RRDSrcProtoMaskForDisable (INT4
                                       *pi4RetValFsrip6RRDSrcProtoMaskForDisable)
{
    *pi4RetValFsrip6RRDSrcProtoMaskForDisable = RIP6_RRD_DISABLE_MASK_VALUE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsrip6RRDRouteDefMetric
 Input       :  The Indices

                The Object 
                retValFsrip6RRDRouteDefMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrip6RRDRouteDefMetric (INT4 *pi4RetValFsrip6RRDRouteDefMetric)
{
    *pi4RetValFsrip6RRDRouteDefMetric = gRip6Redistribution.u1DefMetric;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsrip6RRDRouteMapName
 Input       :  The Indices

                The Object
                retValFsrip6RRDRouteMapName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrip6RRDRouteMapName (tSNMP_OCTET_STRING_TYPE *
                             pRetValFsrip6RRDRouteMapName)
{
#ifdef RRD_WANTED

    pRetValFsrip6RRDRouteMapName->i4_Length =
        STRLEN ((UINT1 *) gRip6Redistribution.au1RMapName);

    MEMCPY ((UINT1 *) pRetValFsrip6RRDRouteMapName->pu1_OctetList,
            (UINT1 *) gRip6Redistribution.au1RMapName,
            pRetValFsrip6RRDRouteMapName->i4_Length);
#else

    pRetValFsrip6RRDRouteMapName->i4_Length = 0;
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsrip6RouteCount
 Input       :  The Indices

                The Object 
                retValFsrip6RouteCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrip6RouteCount (INT4 *pi4RetValFsrip6RouteCount)
{
    if (nmhGetFsMIrip6RouteCount (gu4InstanceIndex, pi4RetValFsrip6RouteCount)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* SET routines for SCALARS in ripng.mib*/

/****************************************************************************
 Function    :  nmhSetFsrip6RoutePreference
 Input       :  The Indices

                The Object 
                setValIpv6RoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RoutePreference (INT4 i4SetValIpv6RoutePreference)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePreference = %d\n", 
          i4SetValIpv6RoutePreference); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhSetFsMIrip6RoutePreference (gu4InstanceIndex,
                                           i4SetValIpv6RoutePreference) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6GlobalDebug
 Input       :  The Indices

                The Object
                setValFsrip6GlobalDebug
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6GlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6GlobalDebug (INT4 i4SetValFsrip6GlobalDebug)
{

   /*** $$TRACE_LOG (ENTRY, "Fsrip6GlobalDebug = %d\n", 
              i4SetValFsrip6GlobalDebug); ***/
    gu4Rip6DbgMap = (UINT4)i4SetValFsrip6GlobalDebug;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6InstanceIndex
 Input       :  NONE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6GlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6GlobalInstanceIndex (UINT4 u4SetValFsrip6InstanceIndex)
{
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    gu4InstanceIndex = u4SetValFsrip6InstanceIndex;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6PeerFilter
 Input       :  The Indices

                The Object 
                setValFsrip6PeerFilter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrip6PeerFilter (INT4 i4SetValFsrip6PeerFilter)
{
    if (nmhSetFsMIrip6PeerFilter (gu4InstanceIndex, i4SetValFsrip6PeerFilter)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6AdvFilter
 Input       :  The Indices

                The Object 
                setValFsrip6AdvFilter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrip6AdvFilter (INT4 i4SetValFsrip6AdvFilter)
{
    if (nmhSetFsMIrip6AdvFilter (gu4InstanceIndex, i4SetValFsrip6AdvFilter)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 *  Function    :  nmhSetFsRip6RRDAdminStatus
 *  Input       :  The Indices
 *
 *                 The Object
 *                  setValFsRip6RRDAdminStatus
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsRip6RRDAdminStatus (INT4 i4SetValFsRip6RRDAdminStatus)
{

    if (i4SetValFsRip6RRDAdminStatus == RIP6_RRD_ENABLE)
    {
        if (gRip6Redistribution.u4RRDAdminStatus != RIP6_RRD_ENABLE)
        {
            gRip6Redistribution.u4RRDAdminStatus = RIP6_RRD_ENABLE;
        }
        if (gRip6Redistribution.u4ProtoMask != 0)
        {
            nmhSetFsrip6RRDProtoMaskForEnable (gRip6Redistribution.u4ProtoMask);
        }
        return SNMP_SUCCESS;
    }
    else if (i4SetValFsRip6RRDAdminStatus == RIP6_RRD_DISABLE)
    {
        if (gRip6Redistribution.u4RRDAdminStatus != RIP6_RRD_DISABLE)
        {
            gRip6Redistribution.u4RRDAdminStatus = RIP6_RRD_DISABLE;

            if (gRip6Redistribution.u4ProtoMask != 0)
            {
                nmhSetFsrip6RRDSrcProtoMaskForDisable
                    (gRip6Redistribution.u4ProtoMask);

                return ((INT1) Rip6SendMsgToRTMQueue
                        (gRip6Redistribution.u4ProtoMask,
                         RIP6_RTM_REDIS_DISABLE,
                         gRip6Redistribution.au1RMapName));
            }
        }
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 *  Function    :  nmhSetFsrip6RRDProtoMaskForEnable
 *  Input       :  The Indices
 *
 *                  The Object
 *                     setValFsrip6RRDProtoMaskForEnable
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsrip6RRDProtoMaskForEnable (INT4 i4SetValFsrip6RRDProtoMaskForEnable)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
    INT4                i4NewProtoMask = 0;
    INT4                i4Status;
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (gRip6Redistribution.u4RRDAdminStatus != RIP6_RRD_ENABLE)
    {
        gRip6Redistribution.u4ProtoMask |= i4SetValFsrip6RRDProtoMaskForEnable;
        i4Status = SNMP_SUCCESS;
    }
    else
    {
        i4NewProtoMask =
            gRip6Redistribution.
            u4ProtoMask ^ i4SetValFsrip6RRDProtoMaskForEnable;
        gRip6Redistribution.u4ProtoMask |=
            (UINT4) i4SetValFsrip6RRDProtoMaskForEnable;
        i4Status =
            Rip6SendMsgToRTMQueue ((UINT4) i4NewProtoMask,
                                   RIP6_RTM_REDIS_ENABLE,
                                   gRip6Redistribution.au1RMapName);
    }
    if (i4Status == SNMP_SUCCESS)
    {
        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RRDSrcProtoMaskForDisable,
                              u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                              0, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i", RIP6_RRD_DISABLE_MASK_VALUE));
    }

    return (INT1) i4Status;
}

/****************************************************************************
 *  Function    :  nmhSetFsrip6RRDSrcProtoMaskForDisable
 *  Input       :  The Indices
 *
 *                 The Object
 *                    setValFsrip6RRDSrcProtoMaskForDisable
 *  Output      :  The Set Low Lev Routine Take the Indices &
 *                 Sets the Value accordingly.
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhSetFsrip6RRDSrcProtoMaskForDisable (INT4
                                       i4SetValFsrip6RRDSrcProtoMaskForDisable)
{

    INT4                i4Status;
    INT4                i4NewProtoMask = 0;

    if (gRip6Redistribution.u4RRDAdminStatus != RIP6_RRD_ENABLE)
    {
        gRip6Redistribution.u4ProtoMask &=
            (~i4SetValFsrip6RRDSrcProtoMaskForDisable);
        return SNMP_SUCCESS;
    }
    i4NewProtoMask =
        RIP6_RRD_DISABLE_MASK_VALUE ^ i4SetValFsrip6RRDSrcProtoMaskForDisable;
    gRip6Redistribution.u4ProtoMask &=
        (~i4SetValFsrip6RRDSrcProtoMaskForDisable);

    Rip6DeleteRedisRoutes (i4NewProtoMask);
    i4Status =
        Rip6SendMsgToRTMQueue ((UINT4) i4NewProtoMask,
                               RIP6_RTM_REDIS_DISABLE,
                               gRip6Redistribution.au1RMapName);
    return (INT1) i4Status;
}

/****************************************************************************
 Function    :  nmhSetFsrip6RRDRouteDefMetric
 Input       :  The Indices

                The Object 
                setValFsrip6RRDRouteDefMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrip6RRDRouteDefMetric (INT4 i4SetValFsrip6RRDRouteDefMetric)
{
    gRip6Redistribution.u1DefMetric = (UINT1) i4SetValFsrip6RRDRouteDefMetric;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsrip6RRDRouteMapName
 Input       :  The Indices

                The Object
                setValFsrip6RRDRouteMapName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrip6RRDRouteMapName (tSNMP_OCTET_STRING_TYPE *
                             pSetValFsrip6RRDRouteMapName)
{
    tRip6Redistribution *pRip6Redistribution = &gRip6Redistribution;

    if (pSetValFsrip6RRDRouteMapName->i4_Length > RMAP_MAX_NAME_LEN + 1)
    {
        return SNMP_FAILURE;
    }
    if (pRip6Redistribution->u4RRDAdminStatus != RIP6_RRD_ENABLE)
    {
        return SNMP_FAILURE;
    }

    if (pSetValFsrip6RRDRouteMapName->i4_Length != 0)
    {
        if (STRLEN (pRip6Redistribution->au1RMapName) == 0)
        {
            MEMCPY (pRip6Redistribution->au1RMapName,
                    pSetValFsrip6RRDRouteMapName->pu1_OctetList,
                    pSetValFsrip6RRDRouteMapName->i4_Length);
        }
        else if (MEMCMP (pRip6Redistribution->au1RMapName,
                         pSetValFsrip6RRDRouteMapName->pu1_OctetList,
                         pSetValFsrip6RRDRouteMapName->i4_Length) != 0)
        {
            /* Check for existence of Route Map with different Name.
             * If So, return failure, */
            return SNMP_FAILURE;
        }
    }
    else
    {
        /* Resetting the Route Map Name Associated */
        MEMSET (pRip6Redistribution->au1RMapName, 0, RMAP_MAX_NAME_LEN);
    }

    return SNMP_SUCCESS;
}

/* TEST routines for SCALARS in ripng.mib*/

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RoutePreference
 Input       :  The Indices

                The Object 
                testValIpv6RoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RoutePreference (UINT4 *pu4ErrorCode,
                                INT4 i4TestValIpv6RoutePreference)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePreference = %d\n", 
                            i4TestValIpv6RoutePreference); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RoutePreference (pu4ErrorCode,
                                              gu4InstanceIndex,
                                              i4TestValIpv6RoutePreference)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6GlobalDebug
 Input       :  The Indices

                The Object
                testValFsrip6GlobalDebug
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6GlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6GlobalDebug (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsrip6GlobalDebug)
{
   /*** $$TRACE_LOG (ENTRY, "Fsrip6GlobalDebug = %d\n", 
          i4TestValFsrip6GlobalDebug); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if ((i4TestValFsrip6GlobalDebug < RIP6_INITIALIZE_ZERO) ||
        (i4TestValFsrip6GlobalDebug > 255))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6InstanceIndex
 Input       :  NONE
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6GlobalDebug ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6GlobalInstanceIndex (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsrip6InstanceIndex)
{
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                         FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                         u4PreAllocatedUnits);
#ifdef RIP6_SINGLE_INSTANCE
    UNUSED_PARAM (i4TestValFsrip6InstanceIndex);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    UNUSED_PARAM (i4MaxCxt);
    return SNMP_FAILURE;
#else
    if ((i4TestValFsrip6InstanceIndex >= i4MaxCxt) ||
        (i4TestValFsrip6InstanceIndex < RIP6_INITIALIZE_ZERO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#endif
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

 /****************************************************************************
 Function    :  nmhTestv2Fsrip6PeerFilter
 Input       :  The Indices

                The Object 
                testValFsrip6PeerFilter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsrip6PeerFilter (UINT4 *pu4ErrorCode, INT4 i4TestValFsrip6PeerFilter)
{
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6PeerFilter (pu4ErrorCode, gu4InstanceIndex,
                                         i4TestValFsrip6PeerFilter) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6AdvFilter
 Input       :  The Indices

                The Object 
                testValFsrip6AdvFilter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsrip6AdvFilter (UINT4 *pu4ErrorCode, INT4 i4TestValFsrip6AdvFilter)
{
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6AdvFilter (pu4ErrorCode, gu4InstanceIndex,
                                        i4TestValFsrip6AdvFilter) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 *  Function    :  nmhTestv2FsRip6RRDAdminStatus
 *  Input       :  The Indices
 *
 *                   The Object
 *                      testValFsRip6RRDAdminStatus
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *                 Error Codes :  The following error codes are to be returned
 *                                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2FsRip6RRDAdminStatus (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsRip6RRDAdminStatus)
{
    switch (i4TestValFsRip6RRDAdminStatus)
    {
        case RIP6_RRD_ENABLE:
        case RIP6_RRD_DISABLE:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 *  Function    :  nmhTestv2Fsrip6RRDProtoMaskForEnable 
 *  Input       :  The Indices
 *
 *                   The Object
 *                     testValFsrip6RRDProtoMaskForEnable 
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *                 Error Codes :  The following error codes are to be returned
 *                                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2Fsrip6RRDProtoMaskForEnable (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsrip6RRDProtoMaskForEnable)
{
    INT4                i4Value = RIP6_ALL_PROTO_MASK;

    if (i4TestValFsrip6RRDProtoMaskForEnable & ~(i4Value))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_RRD_PROTOMASK);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 *  Function    : nmhTestv2Fsrip6RRDSrcProtoMaskForDisable 
 *  Input       :  The Indices
 *
 *                   The Object
 *                    testValFsrip6RRDSrcProtoMaskForDisable 
 *  Output      :  The Test Low Lev Routine Take the Indices &
 *                 Test whether that Value is Valid Input for Set.
 *                 Stores the value of error code in the Return val
 *                 Error Codes :  The following error codes are to be returned
 *                                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
 *                                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 *  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 *****************************************************************************/
INT1
nmhTestv2Fsrip6RRDSrcProtoMaskForDisable (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValFsrip6RRDSrcProtoMaskForDisable)
{
    INT4                i4Value = RIP6_ALL_PROTO_MASK;

    if ((i4TestValFsrip6RRDSrcProtoMaskForDisable == 0) ||
        (i4TestValFsrip6RRDSrcProtoMaskForDisable & ~(i4Value)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RRDRouteDefMetric
 Input       :  The Indices

                The Object 
                testValFsrip6RRDRouteDefMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsrip6RRDRouteDefMetric (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsrip6RRDRouteDefMetric)
{
    if ((i4TestValFsrip6RRDRouteDefMetric < 0) ||
        (i4TestValFsrip6RRDRouteDefMetric > RIP6_INFINITY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_METRIC);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RRDRouteMapName
 Input       :  The Indices

                The Object
                testValFsrip6RRDRouteMapName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsrip6RRDRouteMapName (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsrip6RRDRouteMapName)
{
    tRip6Redistribution *pRip6Redistribution = &gRip6Redistribution;

    if (pTestValFsrip6RRDRouteMapName->i4_Length > RMAP_MAX_NAME_LEN + 1)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if (pRip6Redistribution->u4RRDAdminStatus != RIP6_RRD_ENABLE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_ASSOC_RMAP);
        return SNMP_FAILURE;
    }

    if (pTestValFsrip6RRDRouteMapName->i4_Length != 0)
    {
        if (STRLEN (pRip6Redistribution->au1RMapName) != 0)
        {
            /* Check for existence of Route Map with different Name.
             * If So, return failure, */
            if (STRLEN (pRip6Redistribution->au1RMapName) !=
                (UINT4) pTestValFsrip6RRDRouteMapName->i4_Length)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_RIP6_RMAP_ASSOC_FAILED);
                return SNMP_FAILURE;
            }
            if (MEMCMP (pRip6Redistribution->au1RMapName,
                        pTestValFsrip6RRDRouteMapName->pu1_OctetList,
                        pTestValFsrip6RRDRouteMapName->i4_Length) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (CLI_RIP6_RMAP_ASSOC_FAILED);
                return SNMP_FAILURE;
            }
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RoutePreference
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RoutePreference (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsrip6GlobalDebug
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6GlobalDebug (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsrip6GlobalInstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6GlobalInstanceIndex (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsrip6PeerFilter
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6PeerFilter (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsrip6AdvFilter
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6AdvFilter (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip6RRDAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip6RRDAdminStatus (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RRDProtoMaskForEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RRDProtoMaskForEnable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RRDSrcProtoMaskForDisable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RRDSrcProtoMaskForDisable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RRDRouteDefMetric
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
               check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RRDRouteDefMetric (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RRDRouteMapName
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RRDRouteMapName (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* end of SET , GET & TEST routines for SCALARS in ripng.mib */

/* LOW LEVEL Routines for Table : Ipv6RipInstanceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrip6InstanceTable
 Input       :  The Indices
                Ipv6RipInstIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsrip6InstanceTable***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsrip6InstanceTable (INT4 i4Ipv6RipInstIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipInstIndex= %d\n", i4Ipv6RipInstIndex); ***/
    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                         FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                         u4PreAllocatedUnits);
#ifdef RIP6_SINGLE_INSTANCE
    UNUSED_PARAM (i4MaxCxt);
    if (i4Ipv6RipInstIndex != RIP6_DEFAULT_INSTANCE)
#else
    if ((i4Ipv6RipInstIndex < RIP6_INITIALIZE_ZERO) ||
        (i4Ipv6RipInstIndex >= i4MaxCxt))
#endif
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6InstanceTable
 Input       :  The Indices
                Ipv6RipInstIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipInstanceTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsrip6InstanceTable (INT4 *pi4Ipv6RipInstIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipInstIndex= %d\n", 
         *pi4Ipv6RipInstIndex); ***/
    *pi4Ipv6RipInstIndex = 0;

    if (garip6InstanceStatus[RIP6_INITIALIZE_ZERO] == RIP6_INST_UP)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6InstanceTable
 Input       :  The Indices
                Ipv6RipInstanceIndex
                nextIpv6RipInstanceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipInstanceTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsrip6InstanceTable (INT4 i4Ipv6RipInstIndex,
                                    INT4 *pi4NextIpv6RipInstIndex)
{
   /*** $$TRACE_LOG (ENTRY, "i4Ipv6RipInstanceIndex= %d\n", 
        *pi4NextIpv6RipInstanceIndex); ***/
    UINT4               u4I = RIP6_INITIALIZE_ZERO;
    UINT4               u4MaxCxt = 0;

    u4MaxCxt = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                         FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                         u4PreAllocatedUnits);

    if (i4Ipv6RipInstIndex < 0)
    {
        UNUSED_PARAM (pi4NextIpv6RipInstIndex);
        return SNMP_FAILURE;
    }
    for (u4I = (UINT4) (i4Ipv6RipInstIndex + RIP6_INITIALIZE_ONE);
         u4I < u4MaxCxt; u4I++)
    {
        if ((garip6InstanceStatus[u4I] == RIP6_INST_UP))
        {
            *pi4NextIpv6RipInstIndex = (INT4) u4I;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrip6InstanceStatus
 Input       :  The Indices
                Ipv6RipInstIndex

                The Object 
                retValIpv6RipInstanceStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6InstanceStatus (INT4 i4Ipv6RipInstIndex,
                            INT4 *pi4RetValIpv6RipInstanceStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    *pi4RetValIpv6RipInstanceStatus =
        (INT4) garip6InstanceStatus[i4Ipv6RipInstIndex];
    return SNMP_SUCCESS;
}

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrip6InstanceStatus
 Input       :  The Indices
                i4Ipv6RipInstIndex

                The Object 
                i4SetValIpv6RipInstanceStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6InstanceStatus***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhSetFsrip6InstanceStatus (INT4 i4Ipv6RipInstIndex,
                            INT4 i4SetValIpv6RipInstanceStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (i4Ipv6RipInstIndex == RIP6_INITIALIZE_ZERO)
    {
        return SNMP_FAILURE;    /*Should not manipulate default Instance(0) */
    }

    if (garip6InstanceStatus[i4Ipv6RipInstIndex] ==
        (UINT1) i4SetValIpv6RipInstanceStatus)
        /*Trying to set the status to existing status should fail */
    {
        return SNMP_FAILURE;
    }
    if (i4SetValIpv6RipInstanceStatus == (INT4) RIP6_INST_UP)
    {
        if (Rip6InstanceCreation ((UINT4) i4Ipv6RipInstIndex) == RIP6_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (Rip6InstanceDeletion ((UINT4) i4Ipv6RipInstIndex) == RIP6_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
}

/* Low Level TEST Routine for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsrip6InstanceStatus
 Input       :  The Indices
                Ipv6RipInstIndex

                The Object 
                testValIpv6RipInstStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipIfProfileIndex***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/

INT1
nmhTestv2Fsrip6InstanceStatus (UINT4 *pu4ErrorCode, INT4 i4Ipv6RipInstIndex,
                               INT4 i4TestValIpv6RipInstanceStatus)
{

    INT4                i4MaxCxt = 0;

    i4MaxCxt = RIP6_MIN (MAX_RIP6_CONTEXTS_LIMIT,
                         FsRIP6SizingParams[MAX_RIP6_INSTANCE_SIZING_ID].
                         u4PreAllocatedUnits);
#ifdef RIP6_SINGLE_INSTANCE
    UNUSED_PARAM (i4MaxCxt);
    if (i4Ipv6RipInstIndex != RIP6_DEFAULT_INSTANCE)
#else
    if ((i4Ipv6RipInstIndex < RIP6_INITIALIZE_ZERO) ||
        (i4Ipv6RipInstIndex >= i4MaxCxt))
#endif
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIpv6RipInstanceStatus != RIP6_INST_UP) &&
        (i4TestValIpv6RipInstanceStatus != RIP6_INST_DOWN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsrip6InstanceTable
 Input       :  The Indices
                Fsrip6InstanceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6InstanceTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*LOW LEVEL Routines for Table : Ipv6RipInstIfMapTable*/

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrip6RipInstIfMapTable
 Input       :  The Indices
                Ipv6RipIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsrip6IsntIfMapTable***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsrip6InstIfMapTable (INT4 i4Ipv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhValidateIndexInstanceFsMIrip6InstIfMapTable (gu4InstanceIndex,
                                                        i4Ipv6RipIfIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6InstIfMapTable
 Input       :  NONE
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipInstIfMapTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsrip6InstIfMapTable (INT4 *pi4Ipv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", *pi4Ipv6RipIfIndex); ***/
    if (nmhGetFirstIndexFsMIrip6InstIfMapTable (gu4InstanceIndex,
                                                pi4Ipv6RipIfIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6InstIfMapTable
 Input       :  The Indices
                Ipv6RipIfIndex
                nextIpv6RipIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipInstIfMapTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsrip6InstIfMapTable (INT4 i4Ipv6RipIfIndex,
                                     INT4 *pi4NextIpv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "i4Ipv6RipIfIndex= %d\n", 
        *pi4NextIpv6RipIfIndex); ***/
    if (nmhGetNextIndexFsMIrip6InstIfMapTable (gu4InstanceIndex,
                                               i4Ipv6RipIfIndex,
                                               pi4NextIpv6RipIfIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6InstIfMapInstId
 Input       :  The Indices
                Ipv6RipIfIndex

 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6InstIfMapInstId (INT4 i4Ipv6RipIfIndex,
                             INT4 *pi4RetValIpv6RipInstIfMapInstId)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6InstIfMapInstId (gu4InstanceIndex,
                                       i4Ipv6RipIfIndex,
                                       pi4RetValIpv6RipInstIfMapInstId)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

/****************************************************************************
 Function    :  nmhGetFsrip6InstIfMapInsIfStatus
 Input       :  The Indices
                Ipv6RipIfIndex

 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6InstIfMapInstIfStatus***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6InstIfMapIfAtchStatus (INT4 i4Ipv6RipIfIndex,
                                   INT4 *pi4RetValIpv6RipInstIfMapInsIfStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6InstIfMapIfAtchStatus (gu4InstanceIndex,
                                             i4Ipv6RipIfIndex,
                                             pi4RetValIpv6RipInstIfMapInsIfStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

/****************************************************************************
 Function    :  nmhSetFsrip6InstIfMapInsIfStatus
 Input       :  The Indices
                Ipv6RipIfIndex
        The Value to be set
                Ipv6RipInstIfMapInsIfStatus
 Output      :  NONE
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6InstIfMapInstIfStatus***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6InstIfMapIfAtchStatus (INT4 i4Ipv6RipIfIndex,
                                   INT4 i4SetValIpv6RipInstIfMapIfAtchStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhSetFsMIrip6InstIfMapIfAtchStatus (gu4InstanceIndex,
                                                 i4Ipv6RipIfIndex,
                                                 i4SetValIpv6RipInstIfMapIfAtchStatus)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

}

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
/****************************************************************************
 Function    :  nmhTestv2Fsrip6InstIfMapIfAtchStatus
 Input       :  The Indices
                Ipv6RipIfIndex

    nj
    The Object 
                testValIpv6RipInstIfMapIfAtchStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6InstIfMapIfAtchStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4Ipv6RipIfIndex,
                                      INT4
                                      i4TestValIpv6RipInstIfMapIfAtchStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhTestv2FsMIrip6InstIfMapIfAtchStatus (pu4ErrorCode,
                                                gu4InstanceIndex,
                                                i4Ipv6RipIfIndex,
                                                i4TestValIpv6RipInstIfMapIfAtchStatus)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsrip6InstIfMapTable
 Input       :  The Indices
                Fsrip6IfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6InstIfMapTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6RipIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrip6RipIfTable
 Input       :  The Indices
                Ipv6RipIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsrip6RipIfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsrip6RipIfTable (INT4 i4Ipv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhValidateIndexInstanceFsMIrip6RipIfTable (gu4InstanceIndex,
                                                        i4Ipv6RipIfIndex) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6RipIfTable
 Input       :  The Indices
                Ipv6RipIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipIfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsrip6RipIfTable (INT4 *pi4Ipv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", *pi4Ipv6RipIfIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhGetFirstIndexFsMIrip6RipIfTable (gu4InstanceIndex,
                                                pi4Ipv6RipIfIndex) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6RipIfTable
 Input       :  The Indices
                Ipv6RipIfIndex
                nextIpv6RipIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipIfTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsrip6RipIfTable (INT4 i4Ipv6RipIfIndex,
                                 INT4 *pi4NextIpv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", *pi4Ipv6RipIfIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhGetNextIndexFsMIrip6RipIfTable (gu4InstanceIndex,
                                               i4Ipv6RipIfIndex,
                                               pi4NextIpv6RipIfIndex) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfProfileIndex
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfProfileIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfProfileIndex (INT4 i4Ipv6RipIfIndex,
                               INT4 *pi4RetValIpv6RipIfProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfProfileIndex (gu4InstanceIndex,
                                         i4Ipv6RipIfIndex,
                                         pi4RetValIpv6RipIfProfileIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfCost
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfCost (INT4 i4Ipv6RipIfIndex, INT4 *pi4RetValIpv6RipIfCost)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfCost (gu4InstanceIndex,
                                 i4Ipv6RipIfIndex,
                                 pi4RetValIpv6RipIfCost) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfOperStatus
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfOperStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfOperStatus (INT4 i4Ipv6RipIfIndex,
                             INT4 *pi4RetValIpv6RipIfOperStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfOperStatus (gu4InstanceIndex,
                                       i4Ipv6RipIfIndex,
                                       pi4RetValIpv6RipIfOperStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfProtocolEnable
 Input       :  The Indices
                Fsrip6RipIfIndex

                The Object
                retValFsrip6RipIfProtocolEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfProtocolEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfProtocolEnable (INT4 i4Fsrip6RipIfIndex,
                                 INT4 *pi4RetValFsrip6RipIfProtocolEnable)
{
    /*** $$trace_log (entry, "fsrip6ripifindex = %d\n", i4fsrip6ripifindex); 
      $$trace_log (exit," snmp success\n"); ***/
    if (nmhGetFsMIrip6RipIfProtocolEnable (gu4InstanceIndex,
                                           i4Fsrip6RipIfIndex,
                                           pi4RetValFsrip6RipIfProtocolEnable)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 function    :  nmhGetFsrip6RipIfInMessages
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfInMessages ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfInMessages (INT4 i4Ipv6RipIfIndex,
                             UINT4 *pu4RetValIpv6RipIfInMessages)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfInMessages (gu4InstanceIndex,
                                       i4Ipv6RipIfIndex,
                                       pu4RetValIpv6RipIfInMessages) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfInRequests
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfInRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfInRequests (INT4 i4Ipv6RipIfIndex,
                             UINT4 *pu4RetValIpv6RipIfInRequests)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfInRequests (gu4InstanceIndex,
                                       i4Ipv6RipIfIndex,
                                       pu4RetValIpv6RipIfInRequests) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfInResponses
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfInResponses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfInResponses (INT4 i4Ipv6RipIfIndex,
                              UINT4 *pu4RetValIpv6RipIfInResponses)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfInResponses (gu4InstanceIndex,
                                        i4Ipv6RipIfIndex,
                                        pu4RetValIpv6RipIfInResponses) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfUnknownCmds
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfUnknownCmds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfUnknownCmds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfUnknownCmds (INT4 i4Ipv6RipIfIndex,
                              UINT4 *pu4RetValIpv6RipIfUnknownCmds)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfUnknownCmds (gu4InstanceIndex,
                                        i4Ipv6RipIfIndex,
                                        pu4RetValIpv6RipIfUnknownCmds) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfInOtherVer
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInOtherVer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfInOtherVer ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfInOtherVer (INT4 i4Ipv6RipIfIndex,
                             UINT4 *pu4RetValIpv6RipIfInOtherVer)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfInOtherVer (gu4InstanceIndex, i4Ipv6RipIfIndex,
                                       pu4RetValIpv6RipIfInOtherVer) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfInDiscards
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfInDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfInDiscards (INT4 i4Ipv6RipIfIndex,
                             UINT4 *pu4RetValIpv6RipIfInDiscards)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfInDiscards (gu4InstanceIndex,
                                       i4Ipv6RipIfIndex,
                                       pu4RetValIpv6RipIfInDiscards) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfOutMessages
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOutMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfOutMessages ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfOutMessages (INT4 i4Ipv6RipIfIndex,
                              UINT4 *pu4RetValIpv6RipIfOutMessages)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfOutMessages (gu4InstanceIndex,
                                        i4Ipv6RipIfIndex,
                                        pu4RetValIpv6RipIfOutMessages) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfOutRequests
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfOutRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfOutRequests (INT4 i4Ipv6RipIfIndex,
                              UINT4 *pu4RetValIpv6RipIfOutRequests)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfOutRequests (gu4InstanceIndex,
                                        i4Ipv6RipIfIndex,
                                        pu4RetValIpv6RipIfOutRequests) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfOutResponses
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOutResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfOutResponses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfOutResponses (INT4 i4Ipv6RipIfIndex,
                               UINT4 *pu4RetValIpv6RipIfOutResponses)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfOutResponses (gu4InstanceIndex,
                                         i4Ipv6RipIfIndex,
                                         pu4RetValIpv6RipIfOutResponses) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipIfOutTrigUpdates
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOutTrigUpdates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipIfOutTrigUpdates ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipIfOutTrigUpdates (INT4 i4Ipv6RipIfIndex,
                                 UINT4 *pu4RetValIpv6RipIfOutTrigUpdates)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (nmhGetFsMIrip6RipIfOutTrigUpdates (gu4InstanceIndex,
                                           i4Ipv6RipIfIndex,
                                           pu4RetValIpv6RipIfOutTrigUpdates) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
  Function    :  nmhGetFsrip6RipIfDefRouteAdvt
  Input       :  The Indices
                 Fsrip6RipIfIndex
 
                 The Object
                 retValFsrip6RipIfDefRouteAdvt
    
  Output      :  The Get Low Lev Routine Take the Indices &/
                 store the Value requested in the Return val.
 
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrip6RipIfDefRouteAdvt (INT4 i4Fsrip6RipIfIndex,
                               INT4 *pi4RetValFsrip6RipIfDefRouteAdvt)
{
    if (nmhGetFsMIrip6RipIfDefRouteAdvt (gu4InstanceIndex,
                                         i4Fsrip6RipIfIndex,
                                         pi4RetValFsrip6RipIfDefRouteAdvt) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrip6RipIfProfileIndex
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                setValIpv6RipIfProfileIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipIfProfileIndex (INT4 i4Ipv6RipIfIndex,
                               INT4 i4SetValIpv6RipIfProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfProfileIndex = %d\n", 
         i4SetValIpv6RipIfProfileIndex); ***/
    if (nmhSetFsMIrip6RipIfProfileIndex (gu4InstanceIndex,
                                         i4Ipv6RipIfIndex,
                                         i4SetValIpv6RipIfProfileIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6RipIfCost
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                setValIpv6RipIfCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipIfCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipIfCost (INT4 i4Ipv6RipIfIndex, INT4 i4SetValIpv6RipIfCost)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfCost = %d\n", i4SetValIpv6RipIfCost); ***/
    if (nmhSetFsMIrip6RipIfCost (gu4InstanceIndex,
                                 i4Ipv6RipIfIndex,
                                 i4SetValIpv6RipIfCost) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6RipIfProtocolEnable
 Input       :  The Indices
                Fsrip6RipIfIndex

                The Object
                setValFsrip6RipIfProtocolEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipIfProtocolEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipIfProtocolEnable (INT4 i4Fsrip6RipIfIndex,
                                 INT4 i4SetValFsrip6RipIfProtocolEnable)
{
  /*** $$TRACE_LOG (ENTRY, "Fsrip6RipIfIndex = %d\n", i4Fsrip6RipIfIndex); 
   $$TRACE_LOG (ENTRY, "Fsrip6RipIfProtocolEnable = %d\n", 
         i4SetValFsrip6RipIfProtocolEnable); ***/
    if (nmhSetFsMIrip6RipIfProtocolEnable (gu4InstanceIndex,
                                           i4Fsrip6RipIfIndex,
                                           i4SetValFsrip6RipIfProtocolEnable) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

}

/****************************************************************************
  Function    :  nmhSetFsrip6RipIfDefRouteAdvt
  Input       :  The Indices
                 Fsrip6RipIfIndex

                 The Object
                 setValFsrip6RipIfDefRouteAdvt

  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrip6RipIfDefRouteAdvt (INT4 i4Fsrip6RipIfIndex,
                               INT4 i4SetValFsrip6RipIfDefRouteAdvt)
{

    if (nmhSetFsMIrip6RipIfDefRouteAdvt (gu4InstanceIndex,
                                         i4Fsrip6RipIfIndex,
                                         i4SetValFsrip6RipIfDefRouteAdvt) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipIfProfileIndex
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                testValIpv6RipIfProfileIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipIfProfileIndex (UINT4 *pu4ErrorCode, INT4 i4Ipv6RipIfIndex,
                                  INT4 i4TestValIpv6RipIfProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfProfileIndex = %d\n", 
          i4TestValIpv6RipIfProfileIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipIfProfileIndex (pu4ErrorCode,
                                                gu4InstanceIndex,
                                                i4Ipv6RipIfIndex,
                                                i4TestValIpv6RipIfProfileIndex)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipIfCost
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                testValIpv6RipIfCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipIfCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipIfCost (UINT4 *pu4ErrorCode, INT4 i4Ipv6RipIfIndex,
                          INT4 i4TestValIpv6RipIfCost)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfCost = %d\n", 
          i4TestValIpv6RipIfCost); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipIfCost (pu4ErrorCode,
                                        gu4InstanceIndex,
                                        i4Ipv6RipIfIndex,
                                        i4TestValIpv6RipIfCost) == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipIfProtocolEnable
 Input       :  The Indices
                Fsrip6RipIfIndex

                The Object
                testValFsrip6RipIfProtocolEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipIfProtocolEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipIfProtocolEnable (UINT4 *pu4ErrorCode,
                                    INT4 i4Fsrip6RipIfIndex,
                                    INT4 i4TestValFsrip6RipIfProtocolEnable)
{
/*** $$TRACE_LOG (ENTRY, "Fsrip6RipIfIndex = %d\n", i4Fsrip6RipIfIndex); 
  $$TRACE_LOG (ENTRY, "Fsrip6RipIfProtocolEnable = %d\n", 
           i4TestValFsrip6RipIfProtocolEnable); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipIfProtocolEnable (pu4ErrorCode,
                                                  gu4InstanceIndex,
                                                  i4Fsrip6RipIfIndex,
                                                  i4TestValFsrip6RipIfProtocolEnable)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
   Function    :  nmhTestv2Fsrip6RipIfDefRouteAdvt
   Input       :  The Indices
                  Fsrip6RipIfIndex

                  The Object
                  testValFsrip6RipIfDefRouteAdvt
   Output      :  The Test Low Lev Routine Take the Indices &
                  Test whether that Value is Valid Input for Set.
                  Stores the value of error code in the Return val
                                                                                  Error Codes :  The following error codes are to be returned
                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsrip6RipIfDefRouteAdvt (UINT4 *pu4ErrorCode, INT4 i4Fsrip6RipIfIndex,
                                  INT4 i4TestValFsrip6RipIfDefRouteAdvt)
{

    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipIfDefRouteAdvt (pu4ErrorCode,
                                                gu4InstanceIndex,
                                                i4Fsrip6RipIfIndex,
                                                i4TestValFsrip6RipIfDefRouteAdvt)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RipIfTable
 Input       :  The Indices
                Fsrip6RipIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RipIfTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6RipProfileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrip6RipProfileTable
 Input       :  The Indices
                Ipv6RipProfileIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsrip6RipProfileTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsrip6RipProfileTable (INT4 i4Ipv6RipProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
          i4Ipv6RipProfileIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhValidateIndexInstanceFsMIrip6RipProfileTable (gu4InstanceIndex,
                                                             i4Ipv6RipProfileIndex)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6RipProfileTable
 Input       :  The Indices
                Ipv6RipProfileIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipProfileTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsrip6RipProfileTable (INT4 *pi4Ipv6RipProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
          *pi4Ipv6RipProfileIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhGetFirstIndexFsMIrip6RipProfileTable (gu4InstanceIndex,
                                                     pi4Ipv6RipProfileIndex) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6RipProfileTable
 Input       :  The Indices
                Ipv6RipProfileIndex
                nextIpv6RipProfileIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipProfileTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsrip6RipProfileTable (INT4 i4Ipv6RipProfileIndex,
                                      INT4 *pi4NextIpv6RipProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
          *pi4Ipv6RipProfileIndex); ***/
    if (nmhGetNextIndexFsMIrip6RipProfileTable (gu4InstanceIndex,
                                                i4Ipv6RipProfileIndex,
                                                pi4NextIpv6RipProfileIndex) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrip6RipProfileStatus
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipProfileStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipProfileStatus (INT4 i4Ipv6RipProfileIndex,
                              INT4 *pi4RetValIpv6RipProfileStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
          i4Ipv6RipProfileIndex); ***/
    if (nmhGetFsMIrip6RipProfileStatus (gu4InstanceIndex,
                                        i4Ipv6RipProfileIndex,
                                        pi4RetValIpv6RipProfileStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipProfileHorizon
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileHorizon
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipProfileHorizon ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipProfileHorizon (INT4 i4Ipv6RipProfileIndex,
                               INT4 *pi4RetValIpv6RipProfileHorizon)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n",
          i4Ipv6RipProfileIndex); ***/
    if (nmhGetFsMIrip6RipProfileHorizon (gu4InstanceIndex,
                                         i4Ipv6RipProfileIndex,
                                         pi4RetValIpv6RipProfileHorizon) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipProfilePeriodicUpdTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfilePeriodicUpdTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipProfilePeriodicUpdTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipProfilePeriodicUpdTime (INT4 i4Ipv6RipProfileIndex,
                                       INT4
                                       *pi4RetValIpv6RipProfilePeriodicUpdTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
         i4Ipv6RipProfileIndex); ***/
    if (nmhGetFsMIrip6RipProfilePeriodicUpdTime (gu4InstanceIndex,
                                                 i4Ipv6RipProfileIndex,
                                                 pi4RetValIpv6RipProfilePeriodicUpdTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipProfileTrigDelayTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileTrigDelayTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipProfileTrigDelayTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipProfileTrigDelayTime (INT4 i4Ipv6RipProfileIndex,
                                     INT4 *pi4RetValIpv6RipProfileTrigDelayTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                 i4Ipv6RipProfileIndex); ***/
    if (nmhGetFsMIrip6RipProfileTrigDelayTime (gu4InstanceIndex,
                                               i4Ipv6RipProfileIndex,
                                               pi4RetValIpv6RipProfileTrigDelayTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipProfileRouteAge
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipProfileRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipProfileRouteAge (INT4 i4Ipv6RipProfileIndex,
                                INT4 *pi4RetValIpv6RipProfileRouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
               i4Ipv6RipProfileIndex); ***/
    if (nmhGetFsMIrip6RipProfileRouteAge (gu4InstanceIndex,
                                          i4Ipv6RipProfileIndex,
                                          pi4RetValIpv6RipProfileRouteAge) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipProfileGarbageCollectTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileGarbageCollectTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipProfileGarbageCollectTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipProfileGarbageCollectTime (INT4 i4Ipv6RipProfileIndex,
                                          INT4
                                          *pi4RetValIpv6RipProfileGarbageCollectTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
    if (nmhGetFsMIrip6RipProfileGarbageCollectTime (gu4InstanceIndex,
                                                    i4Ipv6RipProfileIndex,
                                                    pi4RetValIpv6RipProfileGarbageCollectTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrip6RipProfileStatus
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipProfileStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipProfileStatus (INT4 i4Ipv6RipProfileIndex,
                              INT4 i4SetValIpv6RipProfileStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileStatus = %d\n", 
            i4SetValIpv6RipProfileStatus); ***/
    if (nmhSetFsMIrip6RipProfileStatus (gu4InstanceIndex,
                                        i4Ipv6RipProfileIndex,
                                        i4SetValIpv6RipProfileStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6RipProfileHorizon
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileHorizon
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipProfileHorizon ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipProfileHorizon (INT4 i4Ipv6RipProfileIndex,
                               INT4 i4SetValIpv6RipProfileHorizon)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileHorizon = %d\n", 
            i4SetValIpv6RipProfileHorizon); ***/
    if (nmhSetFsMIrip6RipProfileHorizon (gu4InstanceIndex,
                                         i4Ipv6RipProfileIndex,
                                         i4SetValIpv6RipProfileHorizon) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6RipProfilePeriodicUpdTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfilePeriodicUpdTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipProfilePeriodicUpdTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipProfilePeriodicUpdTime (INT4 i4Ipv6RipProfileIndex,
                                       INT4
                                       i4SetValIpv6RipProfilePeriodicUpdTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
          i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfilePeriodicUpdTime = %d\n", 
          i4SetValIpv6RipProfilePeriodicUpdTime); ***/
    if (nmhSetFsMIrip6RipProfilePeriodicUpdTime (gu4InstanceIndex,
                                                 i4Ipv6RipProfileIndex,
                                                 i4SetValIpv6RipProfilePeriodicUpdTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6RipProfileTrigDelayTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileTrigDelayTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipProfileTrigDelayTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipProfileTrigDelayTime (INT4 i4Ipv6RipProfileIndex,
                                     INT4 i4SetValIpv6RipProfileTrigDelayTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
             i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileTrigDelayTime = %d\n", 
             i4SetValIpv6RipProfileTrigDelayTime); ***/
    if (nmhSetFsMIrip6RipProfileTrigDelayTime (gu4InstanceIndex,
                                               i4Ipv6RipProfileIndex,
                                               i4SetValIpv6RipProfileTrigDelayTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6RipProfileRouteAge
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileRouteAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipProfileRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipProfileRouteAge (INT4 i4Ipv6RipProfileIndex,
                                INT4 i4SetValIpv6RipProfileRouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                 i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileRouteAge = %d\n", 
                 i4SetValIpv6RipProfileRouteAge); ***/
    if (nmhSetFsMIrip6RipProfileRouteAge (gu4InstanceIndex,
                                          i4Ipv6RipProfileIndex,
                                          i4SetValIpv6RipProfileRouteAge) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6RipProfileGarbageCollectTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileGarbageCollectTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipProfileGarbageCollectTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipProfileGarbageCollectTime (INT4 i4Ipv6RipProfileIndex,
                                          INT4
                                          i4SetValIpv6RipProfileGarbageCollectTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
          i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileGarbageCollectTime = %d\n", 
          i4SetValIpv6RipProfileGarbageCollectTime); ***/
    if (nmhSetFsMIrip6RipProfileGarbageCollectTime (gu4InstanceIndex,
                                                    i4Ipv6RipProfileIndex,
                                                    i4SetValIpv6RipProfileGarbageCollectTime)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipProfileStatus
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipProfileStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipProfileStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4Ipv6RipProfileIndex,
                                 INT4 i4TestValIpv6RipProfileStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                                     i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileStatus = %d\n", 
                                     i4TestValIpv6RipProfileStatus); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipProfileStatus (pu4ErrorCode,
                                               gu4InstanceIndex,
                                               i4Ipv6RipProfileIndex,
                                               i4TestValIpv6RipProfileStatus) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipProfileHorizon
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileHorizon
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipProfileHorizon ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipProfileHorizon (UINT4 *pu4ErrorCode,
                                  INT4 i4Ipv6RipProfileIndex,
                                  INT4 i4TestValIpv6RipProfileHorizon)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileHorizon = %d\n", 
                i4TestValIpv6RipProfileHorizon); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipProfileHorizon (pu4ErrorCode,
                                                gu4InstanceIndex,
                                                i4Ipv6RipProfileIndex,
                                                i4TestValIpv6RipProfileHorizon)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipProfilePeriodicUpdTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfilePeriodicUpdTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipProfilePeriodicUpdTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipProfilePeriodicUpdTime (UINT4 *pu4ErrorCode,
                                          INT4 i4Ipv6RipProfileIndex,
                                          INT4
                                          i4TestValIpv6RipProfilePeriodicUpdTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfilePeriodicUpdTime = %d\n", 
                i4TestValIpv6RipProfilePeriodicUpdTime); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipProfilePeriodicUpdTime (pu4ErrorCode,
                                                        gu4InstanceIndex,
                                                        i4Ipv6RipProfileIndex,
                                                        i4TestValIpv6RipProfilePeriodicUpdTime)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipProfileTrigDelayTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileTrigDelayTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipProfileTrigDelayTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipProfileTrigDelayTime (UINT4 *pu4ErrorCode,
                                        INT4 i4Ipv6RipProfileIndex,
                                        INT4
                                        i4TestValIpv6RipProfileTrigDelayTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                    i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileTrigDelayTime = %d\n", 
                i4TestValIpv6RipProfileTrigDelayTime); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipProfileTrigDelayTime (pu4ErrorCode,
                                                      gu4InstanceIndex,
                                                      i4Ipv6RipProfileIndex,
                                                      i4TestValIpv6RipProfileTrigDelayTime)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipProfileRouteAge
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileRouteAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipProfileRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipProfileRouteAge (UINT4 *pu4ErrorCode,
                                   INT4 i4Ipv6RipProfileIndex,
                                   INT4 i4TestValIpv6RipProfileRouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                    i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileRouteAge = %d\n", 
                             i4TestValIpv6RipProfileRouteAge); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipProfileRouteAge (pu4ErrorCode,
                                                 gu4InstanceIndex,
                                                 i4Ipv6RipProfileIndex,
                                                 i4TestValIpv6RipProfileRouteAge)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipProfileGarbageCollectTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileGarbageCollectTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipProfileGarbageCollectTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipProfileGarbageCollectTime (UINT4 *pu4ErrorCode,
                                             INT4 i4Ipv6RipProfileIndex,
                                             INT4
                                             i4TestValIpv6RipProfileGarbageCollectTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                    i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileGarbageCollectTime = %d\n", 
                i4TestValIpv6RipProfileGarbageCollectTime); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipProfileGarbageCollectTime (pu4ErrorCode,
                                                           gu4InstanceIndex,
                                                           i4Ipv6RipProfileIndex,
                                                           i4TestValIpv6RipProfileGarbageCollectTime)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RipProfileTable
 Input       :  The Indices
                Fsrip6RipProfileIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RipProfileTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Ipv6RipRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrip6RipRouteTable
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsrip6RipRouteTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsrip6RipRouteTable (tSNMP_OCTET_STRING_TYPE *
                                             pIpv6RipRouteDest,
                                             INT4 i4Ipv6RipRoutePfxLength,
                                             INT4 i4Ipv6RipRouteProtocol,
                                             INT4 i4Ipv6RipRouteIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhValidateIndexInstanceFsMIrip6RipRouteTable (gu4InstanceIndex,
                                                           pIpv6RipRouteDest,
                                                           i4Ipv6RipRoutePfxLength,
                                                           i4Ipv6RipRouteProtocol,
                                                           i4Ipv6RipRouteIfIndex)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6RipRouteTable
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipRouteTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsrip6RipRouteTable (tSNMP_OCTET_STRING_TYPE *
                                     pIpv6RipRouteDest,
                                     INT4 *pi4Ipv6RipRoutePfxLength,
                                     INT4 *pi4Ipv6RipRouteProtocol,
                                     INT4 *pi4Ipv6RipRouteIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            *pi4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            *pi4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            *pi4Ipv6RipRouteIfIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhGetFirstIndexFsMIrip6RipRouteTable (gu4InstanceIndex,
                                                   pIpv6RipRouteDest,
                                                   pi4Ipv6RipRoutePfxLength,
                                                   pi4Ipv6RipRouteProtocol,
                                                   pi4Ipv6RipRouteIfIndex) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6RipRouteTable
 Input       :  The Indices
                Ipv6RipRouteDest
                nextIpv6RipRouteDest
                Ipv6RipRoutePfxLength
                nextIpv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                nextIpv6RipRouteProtocol
                Ipv6RipRouteIfIndex
                nextIpv6RipRouteIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipRouteTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsrip6RipRouteTable (tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextIpv6RipRouteDest,
                                    INT4 i4Ipv6RipRoutePfxLength,
                                    INT4 *pi4NextIpv6RipRoutePfxLength,
                                    INT4 i4Ipv6RipRouteProtocol,
                                    INT4 *pi4NextIpv6RipRouteProtocol,
                                    INT4 i4Ipv6RipRouteIfIndex,
                                    INT4 *pi4NextIpv6RipRouteIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
                        *pi4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            *pi4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            *pi4Ipv6RipRouteIfIndex); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhGetNextIndexFsMIrip6RipRouteTable (gu4InstanceIndex,
                                                  pIpv6RipRouteDest,
                                                  pNextIpv6RipRouteDest,
                                                  i4Ipv6RipRoutePfxLength,
                                                  pi4NextIpv6RipRoutePfxLength,
                                                  i4Ipv6RipRouteProtocol,
                                                  pi4NextIpv6RipRouteProtocol,
                                                  i4Ipv6RipRouteIfIndex,
                                                  pi4NextIpv6RipRouteIfIndex) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrip6RipRouteNextHop
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                retValIpv6RipRouteNextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipRouteNextHop ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipRouteNextHop (tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                             INT4 i4Ipv6RipRoutePfxLength,
                             INT4 i4Ipv6RipRouteProtocol,
                             INT4 i4Ipv6RipRouteIfIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValIpv6RipRouteNextHop)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
    if (nmhGetFsMIrip6RipRouteNextHop (gu4InstanceIndex,
                                       pIpv6RipRouteDest,
                                       i4Ipv6RipRoutePfxLength,
                                       i4Ipv6RipRouteProtocol,
                                       i4Ipv6RipRouteIfIndex,
                                       pRetValIpv6RipRouteNextHop) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipRouteMetric
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                retValIpv6RipRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipRouteMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipRouteMetric (tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                            INT4 i4Ipv6RipRoutePfxLength,
                            INT4 i4Ipv6RipRouteProtocol,
                            INT4 i4Ipv6RipRouteIfIndex,
                            INT4 *pi4RetValIpv6RipRouteMetric)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
    if (nmhGetFsMIrip6RipRouteMetric (gu4InstanceIndex,
                                      pIpv6RipRouteDest,
                                      i4Ipv6RipRoutePfxLength,
                                      i4Ipv6RipRouteProtocol,
                                      i4Ipv6RipRouteIfIndex,
                                      pi4RetValIpv6RipRouteMetric) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipRouteTag
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                retValIpv6RipRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipRouteTag (tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                         INT4 i4Ipv6RipRoutePfxLength,
                         INT4 i4Ipv6RipRouteProtocol,
                         INT4 i4Ipv6RipRouteIfIndex,
                         INT4 *pi4RetValIpv6RipRouteTag)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
    if (nmhGetFsMIrip6RipRouteTag (gu4InstanceIndex,
                                   pIpv6RipRouteDest,
                                   i4Ipv6RipRoutePfxLength,
                                   i4Ipv6RipRouteProtocol,
                                   i4Ipv6RipRouteIfIndex,
                                   pi4RetValIpv6RipRouteTag) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsrip6RipRouteAge
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                retValIpv6RipRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsrip6RipRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsrip6RipRouteAge (tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                         INT4 i4Ipv6RipRoutePfxLength,
                         INT4 i4Ipv6RipRouteProtocol,
                         INT4 i4Ipv6RipRouteIfIndex,
                         INT4 *pi4RetValIpv6RipRouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
    if (nmhGetFsMIrip6RipRouteAge (gu4InstanceIndex,
                                   pIpv6RipRouteDest,
                                   i4Ipv6RipRoutePfxLength,
                                   i4Ipv6RipRouteProtocol,
                                   i4Ipv6RipRouteIfIndex,
                                   pi4RetValIpv6RipRouteAge) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsrip6RipRouteMetric
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                setValIpv6RipRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipRouteMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipRouteMetric (tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                            INT4 i4Ipv6RipRoutePfxLength,
                            INT4 i4Ipv6RipRouteProtocol,
                            INT4 i4Ipv6RipRouteIfIndex,
                            INT4 i4SetValIpv6RipRouteMetric)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
    if (nmhSetFsMIrip6RipRouteMetric (gu4InstanceIndex,
                                      pIpv6RipRouteDest,
                                      i4Ipv6RipRoutePfxLength,
                                      i4Ipv6RipRouteProtocol,
                                      i4Ipv6RipRouteIfIndex,
                                      i4SetValIpv6RipRouteMetric) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteMetric = %d\n", 
            i4SetValIpv6RipRouteMetric); ***/

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsrip6RipRouteTag
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                setValIpv6RipRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsrip6RipRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsrip6RipRouteTag (tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                         INT4 i4Ipv6RipRoutePfxLength,
                         INT4 i4Ipv6RipRouteProtocol,
                         INT4 i4Ipv6RipRouteIfIndex,
                         INT4 i4SetValIpv6RipRouteTag)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteTag = %d\n", 
            i4SetValIpv6RipRouteTag); ***/
    if (nmhSetFsMIrip6RipRouteTag (gu4InstanceIndex,
                                   pIpv6RipRouteDest,
                                   i4Ipv6RipRoutePfxLength,
                                   i4Ipv6RipRouteProtocol,
                                   i4Ipv6RipRouteIfIndex,
                                   i4SetValIpv6RipRouteTag) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipRouteMetric
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                testValIpv6RipRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipRouteMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipRouteMetric (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                               INT4 i4Ipv6RipRoutePfxLength,
                               INT4 i4Ipv6RipRouteProtocol,
                               INT4 i4Ipv6RipRouteIfIndex,
                               INT4 i4TestValIpv6RipRouteMetric)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteMetric = %d\n", 
            i4TestValIpv6RipRouteMetric); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipRouteMetric (pu4ErrorCode,
                                             gu4InstanceIndex,
                                             pIpv6RipRouteDest,
                                             i4Ipv6RipRoutePfxLength,
                                             i4Ipv6RipRouteProtocol,
                                             i4Ipv6RipRouteIfIndex,
                                             i4TestValIpv6RipRouteMetric) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipRouteTag
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                testValIpv6RipRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2Fsrip6RipRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2Fsrip6RipRouteTag (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                            INT4 i4Ipv6RipRoutePfxLength,
                            INT4 i4Ipv6RipRouteProtocol,
                            INT4 i4Ipv6RipRouteIfIndex,
                            INT4 i4TestValIpv6RipRouteTag)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteTag = %d\n", 
            i4TestValIpv6RipRouteTag); ***/
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipRouteTag (pu4ErrorCode,
                                          gu4InstanceIndex,
                                          pIpv6RipRouteDest,
                                          i4Ipv6RipRoutePfxLength,
                                          i4Ipv6RipRouteProtocol,
                                          i4Ipv6RipRouteIfIndex,
                                          i4TestValIpv6RipRouteTag) ==
            SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RipRouteTable
 Input       :  The Indices
                Fsrip6RipRouteDest
                Fsrip6RipRoutePfxLength
                Fsrip6RipRouteProtocol
                Fsrip6RipRouteIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RipRouteTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrip6RipPeerTable
 Input       :  The Indices
                Fsrip6RipPeerAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrip6RipPeerTable (tSNMP_OCTET_STRING_TYPE *
                                            pFsrip6RipPeerAddr)
{
    if (gu4InstanceIndex == (UINT4) RIP6_INVALID_INSTANCE)
    {
        return SNMP_FAILURE;
    }
    else if (nmhValidateIndexInstanceFsMIrip6RipPeerTable (gu4InstanceIndex,
                                                           pFsrip6RipPeerAddr)
             == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6RipPeerTable
 Input       :  The Indices
                Fsrip6RipPeerAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrip6RipPeerTable (tSNMP_OCTET_STRING_TYPE *
                                    pFsrip6RipPeerAddr)
{
    if (gu4InstanceIndex == (UINT4) RIP6_INVALID_INSTANCE)
    {
        return SNMP_FAILURE;
    }
    else if (nmhGetFirstIndexFsMIrip6RipPeerTable (gu4InstanceIndex,
                                                   pFsrip6RipPeerAddr) ==
             SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6RipPeerTable
 Input       :  The Indices
                Fsrip6RipPeerAddr
                nextFsrip6RipPeerAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsrip6RipPeerTable (tSNMP_OCTET_STRING_TYPE *
                                   pFsrip6RipPeerAddr,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextFsrip6RipPeerAddr)
{
    if (nmhGetNextIndexFsMIrip6RipPeerTable (gu4InstanceIndex,
                                             pFsrip6RipPeerAddr,
                                             pNextFsrip6RipPeerAddr) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrip6RipPeerEntryStatus
 Input       :  The Indices
                Fsrip6RipPeerAddr

                The Object 
                retValFsrip6RipPeerEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrip6RipPeerEntryStatus (tSNMP_OCTET_STRING_TYPE *
                                pFsrip6RipPeerAddr,
                                INT4 *pi4RetValFsrip6RipPeerEntryStatus)
{
    if (nmhGetFsMIrip6RipPeerEntryStatus (gu4InstanceIndex,
                                          pFsrip6RipPeerAddr,
                                          pi4RetValFsrip6RipPeerEntryStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrip6RipPeerEntryStatus
 Input       :  The Indices
                Fsrip6RipPeerAddr

                The Object 
                setValFsrip6RipPeerEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrip6RipPeerEntryStatus (tSNMP_OCTET_STRING_TYPE *
                                pFsrip6RipPeerAddr,
                                INT4 i4SetValFsrip6RipPeerEntryStatus)
{
    if (nmhSetFsMIrip6RipPeerEntryStatus (gu4InstanceIndex,
                                          pFsrip6RipPeerAddr,
                                          i4SetValFsrip6RipPeerEntryStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipPeerEntryStatus
 Input       :  The Indices
                Fsrip6RipPeerAddr

                The Object 
                testValFsrip6RipPeerEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsrip6RipPeerEntryStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsrip6RipPeerAddr,
                                   INT4 i4TestValFsrip6RipPeerEntryStatus)
{
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipPeerEntryStatus (pu4ErrorCode, gu4InstanceIndex,
                                                 pFsrip6RipPeerAddr,
                                                 i4TestValFsrip6RipPeerEntryStatus)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RipPeerTable
 Input       :  The Indices
                Fsrip6RipPeerAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RipPeerTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsrip6RipAdvFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrip6RipAdvFilterTable
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsrip6RipAdvFilterTable (tSNMP_OCTET_STRING_TYPE *
                                                 pFsrip6RipAdvFilterAddress)
{
    if (gu4InstanceIndex == (UINT4) RIP6_INVALID_INSTANCE)
    {
        return SNMP_FAILURE;
    }
    else if (nmhValidateIndexInstanceFsMIrip6RipAdvFilterTable
             (gu4InstanceIndex, pFsrip6RipAdvFilterAddress) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6RipAdvFilterTable
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsrip6RipAdvFilterTable (tSNMP_OCTET_STRING_TYPE *
                                         pFsrip6RipAdvFilterAddress)
{
    if (gu4InstanceIndex == (UINT4) RIP6_INVALID_INSTANCE)
    {
        return SNMP_FAILURE;
    }
    else if (nmhGetFirstIndexFsMIrip6RipAdvFilterTable (gu4InstanceIndex,
                                                        pFsrip6RipAdvFilterAddress)
             == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6RipAdvFilterTable
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress
                nextFsrip6RipAdvFilterAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsrip6RipAdvFilterTable (tSNMP_OCTET_STRING_TYPE *
                                        pFsrip6RipAdvFilterAddress,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsrip6RipAdvFilterAddress)
{
    if (nmhGetNextIndexFsMIrip6RipAdvFilterTable (gu4InstanceIndex,
                                                  pFsrip6RipAdvFilterAddress,
                                                  pNextFsrip6RipAdvFilterAddress)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrip6RipAdvFilterStatus
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress

                The Object 
                retValFsrip6RipAdvFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsrip6RipAdvFilterStatus (tSNMP_OCTET_STRING_TYPE *
                                pFsrip6RipAdvFilterAddress,
                                INT4 *pi4RetValFsrip6RipAdvFilterStatus)
{
    if (nmhGetFsMIrip6RipAdvFilterStatus (gu4InstanceIndex,
                                          pFsrip6RipAdvFilterAddress,
                                          pi4RetValFsrip6RipAdvFilterStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrip6RipAdvFilterStatus
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress

                The Object 
                setValFsrip6RipAdvFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsrip6RipAdvFilterStatus (tSNMP_OCTET_STRING_TYPE *
                                pFsrip6RipAdvFilterAddress,
                                INT4 i4SetValFsrip6RipAdvFilterStatus)
{
    if (nmhSetFsMIrip6RipAdvFilterStatus (gu4InstanceIndex,
                                          pFsrip6RipAdvFilterAddress,
                                          i4SetValFsrip6RipAdvFilterStatus) ==
        SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipAdvFilterStatus
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress

                The Object 
                testValFsrip6RipAdvFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2Fsrip6RipAdvFilterStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsrip6RipAdvFilterAddress,
                                   INT4 i4TestValFsrip6RipAdvFilterStatus)
{
    if (gu4InstanceIndex != (UINT4) RIP6_INVALID_INSTANCE)
    {
        if (nmhTestv2FsMIrip6RipAdvFilterStatus (pu4ErrorCode, gu4InstanceIndex,
                                                 pFsrip6RipAdvFilterAddress,
                                                 i4TestValFsrip6RipAdvFilterStatus)
            == SNMP_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2Fsrip6RipAdvFilterTable
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2Fsrip6RipAdvFilterTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/******************************************************************************
All the nmh routines below are with InstanceIndex passed as an argument, these will be used if user can have a middle level routine that is able to pass instance to the nmh routines.
******************************************************************************/

/**Get routines for scalars of fsrip6.mib**/

/****************************************************************************
 Function    :  nmhGetFsMIrip6RoutePreference
 Input       :  The Indices

                The Object
                retValIpv6RoutePreference
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RoutePreference (UINT4 u4InstId,
                               INT4 *pi4RetValIpv6RoutePreference)
{
    if (garip6InstanceStatus[u4InstId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6RoutePreference =
        garip6InstanceDatabase[u4InstId]->u4LookupPreference;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

}

/****************************************************************************
 Function    :  nmhGetFsMIrip6PeerFilter
 Input       :  The Indices

                The Object 
                retValFsrip6PeerFilter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIrip6PeerFilter (UINT4 u4InstanceId, INT4 *pi4RetValFsrip6PeerFilter)
{
    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsrip6PeerFilter =
        garip6InstanceDatabase[u4InstanceId]->u4Rip6PeerFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6AdvFilter
 Input       :  The Indices

                The Object 
                retValFsrip6AdvFilter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIrip6AdvFilter (UINT4 u4InstanceId, INT4 *pi4RetValFsrip6AdvFilter)
{
    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsrip6AdvFilter =
        garip6InstanceDatabase[u4InstanceId]->u4Rip6AdvFltFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RouteCount
 Input       :  The Indices

                The Object 
                retValFsrip6RouteCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIrip6RouteCount (UINT4 u4InstanceId, INT4 *pi4RetValFsrip6RouteCount)
{
    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsrip6RouteCount =
        (INT4) garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes;
    return SNMP_SUCCESS;
}

/* SET routines for SCALARS in ripng.mib*/

/****************************************************************************
 Function    :  nmhSetFsMIrip6RoutePreference
 Input       :  The Indices

                The Object 
                setValIpv6RoutePreference
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RoutePreference (UINT4 u4InstId, INT4 i4SetValIpv6RoutePreference)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePreference = %d\n", 
                              i4SetValIpv6RoutePreference); ***/
    if (garip6InstanceStatus[u4InstId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    if (i4SetValIpv6RoutePreference == IP6_ROUTE_PREFERENCE_STATIC)
    {
        Rip6SetStaticLookup (u4InstId);
    }
    else if (i4SetValIpv6RoutePreference == IP6_ROUTE_PREFERENCE_DYNAMIC)
    {
        Rip6SetDynamicLookup (u4InstId);
    }
    else if (i4SetValIpv6RoutePreference == IP6_ROUTE_PREFERENCE_BEST_METRIC)
    {
        Rip6SetBestMetricLookup (u4InstId);
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6PeerFilter
 Input       :  The Indices

                The Object 
                setValFsrip6PeerFilter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIrip6PeerFilter (UINT4 u4InstanceId, INT4 i4SetValFsrip6PeerFilter)
{
    garip6InstanceDatabase[u4InstanceId]->u4Rip6PeerFlag =
        i4SetValFsrip6PeerFilter;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6AdvFilter
 Input       :  The Indices

                The Object 
                setValFsrip6AdvFilter
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIrip6AdvFilter (UINT4 u4InstanceId, INT4 i4SetValFsrip6AdvFilter)
{
    garip6InstanceDatabase[u4InstanceId]->u4Rip6AdvFltFlag =
        i4SetValFsrip6AdvFilter;
    return SNMP_SUCCESS;
}

/* TEST routines for SCALARS in ripng.mib*/

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RoutePreference
 Input       :  The Indices

                The Object 
                testValIpv6RoutePreference
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RoutePreference ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RoutePreference (UINT4 *pu4ErrorCode, UINT4 u4InstId,
                                  INT4 i4TestValIpv6RoutePreference)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RoutePreference = %d\n", 
        i4TestValIpv6RoutePreference); ***/
    UNUSED_PARAM (u4InstId);

    if ((i4TestValIpv6RoutePreference != IP6_ROUTE_PREFERENCE_STATIC) &&
        (i4TestValIpv6RoutePreference != IP6_ROUTE_PREFERENCE_DYNAMIC) &&
        (i4TestValIpv6RoutePreference != IP6_ROUTE_PREFERENCE_BEST_METRIC))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6PeerFilter
 Input       :  The Indices

                The Object 
                testValFsrip6PeerFilter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIrip6PeerFilter (UINT4 *pu4ErrorCode, UINT4 u4InstanceId,
                             INT4 i4TestValFsrip6PeerFilter)
{
    UNUSED_PARAM (u4InstanceId);
    if ((i4TestValFsrip6PeerFilter != RIP6_ALLOW)
        && (i4TestValFsrip6PeerFilter != RIP6_DENY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_PEER_FLAG);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6AdvFilter
 Input       :  The Indices

                The Object 
                testValFsrip6AdvFilter
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIrip6AdvFilter (UINT4 *pu4ErrorCode, UINT4 u4InstanceId,
                            INT4 i4TestValFsrip6AdvFilter)
{
    UNUSED_PARAM (u4InstanceId);
    if ((i4TestValFsrip6AdvFilter != RIP6_ENABLE) &&
        (i4TestValFsrip6AdvFilter != RIP6_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_ADV_FLAG);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* end of SET , GET & TEST routines for SCALARS in fsrip6.mib */

/*LOW LEVEL Routines for Table : Ipv6RipInstIfMapTable*/

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIrip6RipInstIfMapTable
 Input       :  The Indices
                Ipv6RipIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = 
                    nmhValidateIndexInstanceFsMIrip6RipInstIfMapTable***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsMIrip6InstIfMapTable (UINT4 u4InstanceId,
                                                INT4 i4Ipv6RipIfIndex)
{
    INT4                i4MaxIfs = 0;

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    UNUSED_PARAM (u4InstanceId);
#ifdef RIP6_SINGLE_INSTANCE
    if (u4InstanceId != RIP6_DEFAULT_INSTANCE)
    {
        return SNMP_FAILURE;
    }
#endif
    if ((i4Ipv6RipIfIndex > i4MaxIfs) ||
        (i4Ipv6RipIfIndex < RIP6_INITIALIZE_ONE))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIrip6InstIfMapTable
 Input       :  NONE
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipInstIfMapTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsMIrip6InstIfMapTable (UINT4 u4InstanceId,
                                        INT4 *pi4Ipv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", *pi4Ipv6RipIfIndex); ***/

    *pi4Ipv6RipIfIndex = 1;

    if (RIP6_GET_INST_IF_MAP_ENTRY (RIP6_INITIALIZE_ONE)->u4InsIfStatus ==
        RIP6_IFACE_ATTACHED)
    {
        return SNMP_SUCCESS;
    }
    return (nmhGetNextIndexFsMIrip6InstIfMapTable
            (u4InstanceId, *pi4Ipv6RipIfIndex, pi4Ipv6RipIfIndex));

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIrip6InstIfMapTable
 Input       :  The Indices
                Ipv6RipIfIndex
                nextIpv6RipIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipInstIfMapTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsMIrip6InstIfMapTable (UINT4 u4InstanceId,
                                       INT4 i4Ipv6RipIfIndex,
                                       INT4 *pi4NextIpv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "i4Ipv6RipIfIndex= %d\n", 
                        *pi4NextIpv6RipIfIndex); ***/
    UINT4               u4I = RIP6_INITIALIZE_ZERO;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    UNUSED_PARAM (u4InstanceId);

    if (i4Ipv6RipIfIndex < 0)
        return SNMP_FAILURE;

    for (u4I = (UINT4) (i4Ipv6RipIfIndex + RIP6_INITIALIZE_ONE);
         u4I <= u4MaxIfs; u4I++)

    {
        if ((RIP6_GET_INST_IF_MAP_ENTRY (u4I)->u4InsIfStatus
             == RIP6_IFACE_ATTACHED))
        {
            *pi4NextIpv6RipIfIndex = (INT4) u4I;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6InstIfMapInstId
 Input       :  The Indices
                Ipv6RipIfIndex

 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6InstIfMapInstId (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                               INT4 *pi4RetValIpv6RipInstIfMapInstId)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    UNUSED_PARAM (u4InstanceId);
    *pi4RetValIpv6RipInstIfMapInstId =
        (INT4) RIP6_GET_INST_IF_MAP_ENTRY ((UINT4) i4Ipv6RipIfIndex)->
        u4InstanceId;
    return SNMP_SUCCESS;
}

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

/****************************************************************************
 Function    :  nmhGetFsMIrip6InstIfMapInsIfStatus
 Input       :  The Indices
                Ipv6RipIfIndex

 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6InstIfMapInstIfStatus***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6InstIfMapIfAtchStatus (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                     INT4 *pi4RetValIpv6RipInstIfMapInsIfStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    UNUSED_PARAM (u4InstanceId);

    *pi4RetValIpv6RipInstIfMapInsIfStatus =
        (INT4) RIP6_GET_INST_IF_MAP_ENTRY ((UINT4) i4Ipv6RipIfIndex)->
        u4InsIfStatus;
    return SNMP_SUCCESS;
}

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/

/****************************************************************************
 Function    :  nmhSetFsMIrip6InstIfMapInsIfStatus
 Input       :  The Indices
                Ipv6RipIfIndex
        The Value to be set
                Ipv6RipInstIfMapInsIfStatus
 Output      :  NONE
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6InstIfMapInstIfStatus***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6InstIfMapIfAtchStatus (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                     INT4 i4SetValIpv6RipInstIfMapIfAtchStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;    /*Check whether Instance is Created or not */
    }

    if (i4Ipv6RipIfIndex == RIP6_INITIALIZE_ONE)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValIpv6RipInstIfMapIfAtchStatus ==
        (INT4) RIP6_GET_INST_IF_MAP_ENTRY ((UINT4) i4Ipv6RipIfIndex)->
        u4InsIfStatus)
    {
        return SNMP_FAILURE;    /*Should not set to existing status */
    }
    else if (i4SetValIpv6RipInstIfMapIfAtchStatus == RIP6_IFACE_ATTACHED)
    {
        if (Rip6InterfaceInstanceAttach (u4InstanceId, i4Ipv6RipIfIndex)
            == RIP6_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else if (i4SetValIpv6RipInstIfMapIfAtchStatus == RIP6_IFACE_DETACHED)
    {
        if (u4InstanceId !=
            RIP6_GET_INST_IF_MAP_ENTRY ((UINT4) i4Ipv6RipIfIndex)->u4InstanceId)
        {
            return SNMP_FAILURE;
        }

        if (Rip6InterfaceInstanceDetach (i4Ipv6RipIfIndex) == RIP6_SUCCESS)
        {
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        return SNMP_FAILURE;
    }
}

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
/****************************************************************************
 Function    :  nmhTestv2FsMIrip6InstIfMapIfAtchStatus
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                testValIpv6RipInstIfMapIfAtchStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6InstIfMapIfAtchStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4InstanceId,
                                        INT4 i4Ipv6RipIfIndex,
                                        INT4
                                        i4TestValIpv6RipInstIfMapIfAtchStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    INT4                i4MaxIfs = 0;

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    UNUSED_PARAM (u4InstanceId);

#ifdef RIP6_SINGLE_INSTANCE
    UNUSED_PARAM (i4MaxIfs);
    UNUSED_PARAM (i4Ipv6RipIfIndex);
    UNUSED_PARAM (i4TestValIpv6RipInstIfMapIfAtchStatus);
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
    return SNMP_FAILURE;
#else
    if ((i4Ipv6RipIfIndex > i4MaxIfs) ||
        (i4Ipv6RipIfIndex < RIP6_INITIALIZE_ONE))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIpv6RipInstIfMapIfAtchStatus != RIP6_IFACE_ATTACHED) &&
        (i4TestValIpv6RipInstIfMapIfAtchStatus != RIP6_IFACE_DETACHED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#endif

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table : Ipv6RipIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIrip6RipIfTable
 Input       :  The Indices
                Ipv6RipIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsMIrip6RipIfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsMIrip6RipIfTable (UINT4 u4InstanceId,
                                            INT4 i4Ipv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    INT4                i4MaxIfs = 0;

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    if ((i4Ipv6RipIfIndex > i4MaxIfs)
        || (i4Ipv6RipIfIndex < RIP6_INITIALIZE_ONE))
    {
        return SNMP_FAILURE;
    }
    if (u4InstanceId !=
        RIP6_GET_INST_IF_MAP_ENTRY ((UINT4) i4Ipv6RipIfIndex)->u4InstanceId)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIrip6RipIfTable
 Input       :  The Indices
                Ipv6RipIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipIfTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsMIrip6RipIfTable (UINT4 u4InstanceId, INT4 *pi4Ipv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", *pi4Ipv6RipIfIndex); ***/
    if (garip6InstanceStatus[u4InstanceId] == RIP6_INST_DOWN)
    {
        return SNMP_FAILURE;
    }
    *pi4Ipv6RipIfIndex = 0;

    if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, *pi4Ipv6RipIfIndex) != NULL)
    {
        if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, *pi4Ipv6RipIfIndex)->
             u1Status & RIP6_IFACE_ALLOCATED) == RIP6_IFACE_ALLOCATED)
        {
            return SNMP_SUCCESS;
        }
    }
    return (nmhGetNextIndexFsMIrip6RipIfTable
            (u4InstanceId, *pi4Ipv6RipIfIndex, pi4Ipv6RipIfIndex));

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIrip6RipIfTable
 Input       :  The Indices
                Ipv6RipIfIndex
                nextIpv6RipIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipIfTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsMIrip6RipIfTable (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                   INT4 *pi4NextIpv6RipIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", *pi4Ipv6RipIfIndex); ***/
    UINT4               u4I = RIP6_INITIALIZE_ZERO;
    UINT4               u4MaxIfs = 0;

    u4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if (i4Ipv6RipIfIndex < 0)
        return SNMP_FAILURE;

    for (u4I = i4Ipv6RipIfIndex + RIP6_INITIALIZE_ONE; u4I <= u4MaxIfs; u4I++)
    {
        if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4I) != NULL)
        {
            if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, u4I)->u1Status &
                 RIP6_IFACE_ALLOCATED) == RIP6_IFACE_ALLOCATED)
            {
                (*pi4NextIpv6RipIfIndex) = u4I;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfProfileIndex
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfProfileIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfProfileIndex (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                 INT4 *pi4RetValIpv6RipIfProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT4) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6RipIfProfileIndex = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                              i4Ipv6RipIfIndex)->
        u2ProfIndex;

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfCost
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfCost
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfCost (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                         INT4 *pi4RetValIpv6RipIfCost)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValIpv6RipIfCost = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                      i4Ipv6RipIfIndex)->u1Cost;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfOperStatus
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfOperStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfOperStatus (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                               INT4 *pi4RetValIpv6RipIfOperStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
         u1Status & RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED)
    {
        if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
             u1Status & RIP6_IFACE_UP) == RIP6_IFACE_UP)
        {
            *pi4RetValIpv6RipIfOperStatus = IP6_RIP_ENABLED_UP;
            return SNMP_SUCCESS;
        }
        else
        {
            *pi4RetValIpv6RipIfOperStatus = IP6_RIP_ENABLED_DOWN;
            return SNMP_SUCCESS;
        }
    }
    if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
         u1Status & RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED)
    {
        if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
             u1Status & RIP6_IFACE_UP) == RIP6_IFACE_UP)
        {
            *pi4RetValIpv6RipIfOperStatus = IP6_RIP_DISABLED_UP;
            return SNMP_SUCCESS;
        }
        else
        {
            *pi4RetValIpv6RipIfOperStatus = IP6_RIP_DISABLED_DOWN;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfProtocolEnable
 Input       :  The Indices
                Fsrip6RipIfIndex

                The Object
                retValFsrip6RipIfProtocolEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfProtocolEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfProtocolEnable (UINT4 u4InstanceId, INT4 i4Fsrip6RipIfIndex,
                                   INT4 *pi4RetValFsrip6RipIfProtocolEnable)
{
        /*** $$TRACE_LOG (ENTRY, "Fsrip6RipIfIndex = %d\n", i4Fsrip6RipIfIndex);
          $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    if (Rip6IfEntryExists (i4Fsrip6RipIfIndex) == RIP6_SUCCESS)
    {
        if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Fsrip6RipIfIndex)->
             u1Status & RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED)
        {
            *pi4RetValFsrip6RipIfProtocolEnable = RIP6_ENABLED;
            return SNMP_SUCCESS;
        }

        if (!
            ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Fsrip6RipIfIndex)->
              u1Status & RIP6_IFACE_ENABLED) == RIP6_IFACE_ENABLED))
        {
            *pi4RetValFsrip6RipIfProtocolEnable = RIP6_DISABLED;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
  Function    :  nmhGetFsMIrip6RipIfDefRouteAdvt
  Input       :  The Indices
                 Fsrip6RipIfIndex
 
                 The Object
                 retValFsrip6RipIfDefRouteAdvt
    
  Output      :  The Get Low Lev Routine Take the Indices &/
                 store the Value requested in the Return val.
 
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIrip6RipIfDefRouteAdvt (UINT4 u4InstanceId, INT4 i4Fsrip6RipIfIndex,
                                 INT4 *pi4RetValFsrip6RipIfDefRouteAdvt)
{
    if (Rip6IfEntryExists ((UINT4) i4Fsrip6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsrip6RipIfDefRouteAdvt =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Fsrip6RipIfIndex)->
        u4DefRouteAdvt;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfInMessages
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfInMessages ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfInMessages (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                               UINT4 *pu4RetValIpv6RipIfInMessages)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfInMessages =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
        stats.u4InMessages;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfInRequests
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfInRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfInRequests (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                               UINT4 *pu4RetValIpv6RipIfInRequests)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfInRequests = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                            i4Ipv6RipIfIndex)->
        stats.u4InReq;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfInResponses
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfInResponses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfInResponses (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                UINT4 *pu4RetValIpv6RipIfInResponses)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfInResponses = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                             i4Ipv6RipIfIndex)->
        stats.u4InRes;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfUnknownCmds
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfUnknownCmds
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfUnknownCmds ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfUnknownCmds (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                UINT4 *pu4RetValIpv6RipIfUnknownCmds)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfUnknownCmds =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
        stats.u4UnknownCmds;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfInOtherVer
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInOtherVer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfInOtherVer ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfInOtherVer (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                               UINT4 *pu4RetValIpv6RipIfInOtherVer)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfInOtherVer = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                            i4Ipv6RipIfIndex)->
        stats.u4OtherVer;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfInDiscards
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfInDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfInDiscards ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfInDiscards (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                               UINT4 *pu4RetValIpv6RipIfInDiscards)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/

    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfInDiscards = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                            i4Ipv6RipIfIndex)->
        stats.u4Discards;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfOutMessages
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOutMessages
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfOutMessages ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfOutMessages (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                UINT4 *pu4RetValIpv6RipIfOutMessages)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/

    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfOutMessages =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
        stats.u4OutMessages;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfOutRequests
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOutRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfOutRequests ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfOutRequests (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                UINT4 *pu4RetValIpv6RipIfOutRequests)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfOutRequests = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                             i4Ipv6RipIfIndex)->
        stats.u4OutReq;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfOutResponses
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOutResponses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfOutResponses ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfOutResponses (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                 UINT4 *pu4RetValIpv6RipIfOutResponses)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfOutResponses = RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                                              i4Ipv6RipIfIndex)->
        stats.u4OutRes;
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipIfOutTrigUpdates
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                retValIpv6RipIfOutTrigUpdates
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipIfOutTrigUpdates ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipIfOutTrigUpdates (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                   UINT4 *pu4RetValIpv6RipIfOutTrigUpdates)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValIpv6RipIfOutTrigUpdates =
        RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
        stats.u4TrigUpdates;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipIfProfileIndex
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                setValIpv6RipIfProfileIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipIfProfileIndex (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                 INT4 i4SetValIpv6RipIfProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfProfileIndex = %d\n", 
            i4SetValIpv6RipIfProfileIndex); ***/

    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == FAILURE)
    {
        if (garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4SetValIpv6RipIfProfileIndex]->u1Status ==
            IP6_RIP_PROFILE_VALID)
        {
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
                u2ProfIndex = (UINT2) i4SetValIpv6RipIfProfileIndex;
            RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
                u1Status = RIP6_IFACE_ALLOCATED;
            return SNMP_SUCCESS;
        }
        else
            return SNMP_FAILURE;
    }

    RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->
        u2ProfIndex = (UINT2) i4SetValIpv6RipIfProfileIndex;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipIfCost
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                setValIpv6RipIfCost
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipIfCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipIfCost (UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                         INT4 i4SetValIpv6RipIfCost)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfCost = %d\n", i4SetValIpv6RipIfCost); ***/

    if (Rip6IfEntryExists ((UINT2) i4Ipv6RipIfIndex) == RIP6_FAILURE)
    {
        return SNMP_FAILURE;
    }
    RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Ipv6RipIfIndex)->u1Cost =
        (UINT1) i4SetValIpv6RipIfCost;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RipIfCost,
                          u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                          1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4Ipv6RipIfIndex,
                      i4SetValIpv6RipIfCost));

    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipIfProtocolEnable
 Input       :  The Indices
                Fsrip6RipIfIndex

                The Object
                setValFsrip6RipIfProtocolEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipIfProtocolEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipIfProtocolEnable (UINT4 u4InstanceId, INT4 i4Fsrip6RipIfIndex,
                                   INT4 i4SetValFsrip6RipIfProtocolEnable)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
              /*** $$TRACE_LOG (ENTRY, "Fsrip6RipIfIndex = %d\n", 
                i4Fsrip6RipIfIndex); 
               $$TRACE_LOG (ENTRY, "Fsrip6RipIfProtocolEnable = %d\n"
           , i4SetValFsrip6RipIfProtocolEnable);
   *** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    UNUSED_PARAM (u4InstanceId);
    if (Rip6IfEntryExists (i4Fsrip6RipIfIndex) == RIP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsrip6RipIfProtocolEnable == RIP6_ENABLED)
    {
        Rip6IfEnable ((UINT4) i4Fsrip6RipIfIndex);
    }
    else if (i4SetValFsrip6RipIfProtocolEnable == RIP6_DISABLED)
    {
        Rip6IfDisable ((UINT4) i4Fsrip6RipIfIndex);

        gRip6Redistribution.u4ProtoMask = 0;

        Rip6SendMsgToRTMQueue ((RIP6_IMPORT_STATIC  | RIP6_IMPORT_OSPF | RIP6_IMPORT_DIRECT),
                               RIP6_RTM_REDIS_DISABLE,
                               gRip6Redistribution.au1RMapName);
    }

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RipIfProtocolEnable,
                          u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                          1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4Fsrip6RipIfIndex,
                      i4SetValFsrip6RipIfProtocolEnable));
    return (SNMP_SUCCESS);

}

/****************************************************************************
  Function    :  nmhSetFsMIrip6RipIfDefRouteAdvt
  Input       :  The Indices
                 Fsrip6RipIfIndex

                 The Object
                 setValFsrip6RipIfDefRouteAdvt

  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIrip6RipIfDefRouteAdvt (UINT4 u4InstanceId, INT4 i4Fsrip6RipIfIndex,
                                 INT4 i4SetValFsrip6RipIfDefRouteAdvt)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (Rip6IfEntryExists ((UINT4) i4Fsrip6RipIfIndex) == FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsrip6RipIfDefRouteAdvt == RIP6_FALSE)
    {
        Rip6DisableDefRouteAdvt (i4Fsrip6RipIfIndex);
    }

    RIP6_GET_INST_IF_ENTRY (u4InstanceId, i4Fsrip6RipIfIndex)->
        u4DefRouteAdvt = (UINT4) i4SetValFsrip6RipIfDefRouteAdvt;

    RM_GET_SEQ_NUM (&u4SeqNum);
    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RipIfDefRouteAdvt,
                          u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                          1, SNMP_SUCCESS);
    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4Fsrip6RipIfIndex,
                      i4SetValFsrip6RipIfDefRouteAdvt));

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipIfProfileIndex
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                testValIpv6RipIfProfileIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipIfProfileIndex ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipIfProfileIndex (UINT4 *pu4ErrorCode,
                                    UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                                    INT4 i4TestValIpv6RipIfProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfProfileIndex = %d\n", 
        i4TestValIpv6RipIfProfileIndex); ***/
    INT4                i4MaxIfs = 0;
    INT4                i4MaxProfs = 0;

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    i4MaxProfs =
        RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                  FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                  u4PreAllocatedUnits);

    if ((i4Ipv6RipIfIndex < RIP6_INITIALIZE_ONE) ||
        (i4Ipv6RipIfIndex > i4MaxIfs))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValIpv6RipIfProfileIndex >= RIP6_INITIALIZE_ZERO) &&
        (i4TestValIpv6RipIfProfileIndex < i4MaxProfs))
    {
        if (garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4TestValIpv6RipIfProfileIndex]->u1Status ==
            IP6_RIP_PROFILE_INVALID)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return (SNMP_FAILURE);
        }

        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipIfCost
 Input       :  The Indices
                Ipv6RipIfIndex

                The Object 
                testValIpv6RipIfCost
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipIfCost ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipIfCost (UINT4 *pu4ErrorCode,
                            UINT4 u4InstanceId, INT4 i4Ipv6RipIfIndex,
                            INT4 i4TestValIpv6RipIfCost)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfIndex = %d\n", i4Ipv6RipIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipIfCost = %d\n", 
                    i4TestValIpv6RipIfCost); ***/
    INT4                i4MaxIfs = 0;

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    UNUSED_PARAM (u4InstanceId);
    if ((i4Ipv6RipIfIndex < RIP6_INITIALIZE_ONE) ||
        (i4Ipv6RipIfIndex > i4MaxIfs))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return (SNMP_FAILURE);
    }
    if ((i4TestValIpv6RipIfCost >= RIP6_INITIALIZE_ONE) &&
        (i4TestValIpv6RipIfCost <= RIP6_MAX_COST))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipIfProtocolEnable
 Input       :  The Indices
                Fsrip6RipIfIndex

                The Object
                testValFsrip6RipIfProtocolEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipIfProtocolEnable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipIfProtocolEnable (UINT4 *pu4ErrorCode,
                                      UINT4 u4InstanceId,
                                      INT4 i4Fsrip6RipIfIndex,
                                      INT4 i4TestValFsrip6RipIfProtocolEnable)
{
     /*** $$TRACE_LOG (ENTRY, "Fsrip6RipIfIndex = %d\n", i4Fsrip6RipIfIndex);
        * $$TRACE_LOG (ENTRY, "Fsrip6RipIfProtocolEnable = %d\n", 
        * i4TestValFsrip6RipIfProtocolEnable);
        * $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
    UNUSED_PARAM (u4InstanceId);
    if (Rip6IfEntryExists (i4Fsrip6RipIfIndex) == RIP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RIP6_INV_ENTRY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsrip6RipIfProtocolEnable != RIP6_ENABLED) &&
        (i4TestValFsrip6RipIfProtocolEnable != RIP6_DISABLED))
    {
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (CLI_RIP6_INV_VALUE);
            return (SNMP_FAILURE);
        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
   Function    :  nmhTestv2FsMIrip6RipIfDefRouteAdvt
   Input       :  The Indices
                  Fsrip6RipIfIndex

                  The Object
                  testValFsrip6RipIfDefRouteAdvt
   Output      :  The Test Low Lev Routine Take the Indices &
                  Test whether that Value is Valid Input for Set.
                  Stores the value of error code in the Return val
                                                                                  Error Codes :  The following error codes are to be returned
                  SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                  SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
   Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIrip6RipIfDefRouteAdvt (UINT4 *pu4ErrorCode, UINT4 u4InstanceId,
                                    INT4 i4Fsrip6RipIfIndex,
                                    INT4 i4TestValFsrip6RipIfDefRouteAdvt)
{

    UNUSED_PARAM (u4InstanceId);

    if (Rip6IfEntryExists (i4Fsrip6RipIfIndex) == RIP6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RIP6_INV_RIP6IF);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsrip6RipIfDefRouteAdvt != RIP6_TRUE) &&
        (i4TestValFsrip6RipIfDefRouteAdvt != RIP6_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_DEF_ROUTEADV);
        return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : Ipv6RipProfileTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIrip6RipProfileTable
 Input       :  The Indices
                Ipv6RipProfileIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = 
        nmhValidateIndexInstanceFsMIrip6RipProfileTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsMIrip6RipProfileTable (UINT4 u4InstanceId,
                                                 INT4 i4Ipv6RipProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
    INT4                i4MaxProfs = 0;

    i4MaxProfs = RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                           FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                           u4PreAllocatedUnits);
    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    if (i4Ipv6RipProfileIndex < i4MaxProfs)
    {
        if (garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex] != NULL)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIrip6RipProfileTable
 Input       :  The Indices
                Ipv6RipProfileIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipProfileTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsMIrip6RipProfileTable (UINT4 u4InstanceId,
                                         INT4 *pi4Ipv6RipProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            *pi4Ipv6RipProfileIndex); ***/
    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    *pi4Ipv6RipProfileIndex = 0;
    if (garip6InstanceDatabase[u4InstanceId]->
        arip6Profile[*pi4Ipv6RipProfileIndex]->u1Status ==
        IP6_RIP_PROFILE_VALID)
    {
        return (SNMP_SUCCESS);
    }
    if (Rip6ProfileGetNextIndex (u4InstanceId, (UINT2 *) pi4Ipv6RipProfileIndex)
        == SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIrip6RipProfileTable
 Input       :  The Indices
                Ipv6RipProfileIndex
                nextIpv6RipProfileIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipProfileTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsMIrip6RipProfileTable (UINT4 u4InstanceId,
                                        INT4 i4Ipv6RipProfileIndex,
                                        INT4 *pi4NextIpv6RipProfileIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            *pi4Ipv6RipProfileIndex); ***/
    UINT1               u1I = RIP6_INITIALIZE_ZERO;
    INT4                i4MaxProfs = 0;

    i4MaxProfs = RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                           FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                           u4PreAllocatedUnits);

    if (i4Ipv6RipProfileIndex < 0)
        return SNMP_FAILURE;

    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    for (u1I = (UINT1) (i4Ipv6RipProfileIndex + RIP6_INITIALIZE_ONE);
         u1I < (UINT1) i4MaxProfs; u1I++)
    {
        if (garip6InstanceDatabase[u4InstanceId]->arip6Profile[u1I]->
            u1Status == IP6_RIP_PROFILE_VALID)
        {
            (*pi4NextIpv6RipProfileIndex) = u1I;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipProfileStatus
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipProfileStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipProfileStatus (UINT4 u4InstanceId,
                                INT4 i4Ipv6RipProfileIndex,
                                INT4 *pi4RetValIpv6RipProfileStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                        i4Ipv6RipProfileIndex); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2) i4Ipv6RipProfileIndex)
        == RIP6_SUCCESS)
    {
        *pi4RetValIpv6RipProfileStatus =
            (INT4) garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u1Status;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipProfileHorizon
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileHorizon
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipProfileHorizon ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipProfileHorizon (UINT4 u4InstanceId,
                                 INT4 i4Ipv6RipProfileIndex,
                                 INT4 *pi4RetValIpv6RipProfileHorizon)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                i4Ipv6RipProfileIndex); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2)
                                i4Ipv6RipProfileIndex) == RIP6_SUCCESS)
    {
        *pi4RetValIpv6RipProfileHorizon =
            (INT4) garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u1Horizon;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipProfilePeriodicUpdTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfilePeriodicUpdTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipProfilePeriodicUpdTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipProfilePeriodicUpdTime (UINT4 u4InstanceId,
                                         INT4 i4Ipv6RipProfileIndex,
                                         INT4
                                         *pi4RetValIpv6RipProfilePeriodicUpdTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                    i4Ipv6RipProfileIndex); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2) i4Ipv6RipProfileIndex)
        == RIP6_SUCCESS)
    {
        *pi4RetValIpv6RipProfilePeriodicUpdTime =
            (INT4) garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u4PeriodicUpdTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipProfileTrigDelayTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileTrigDelayTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipProfileTrigDelayTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipProfileTrigDelayTime (UINT4 u4InstanceId,
                                       INT4 i4Ipv6RipProfileIndex,
                                       INT4
                                       *pi4RetValIpv6RipProfileTrigDelayTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                i4Ipv6RipProfileIndex); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2)
                                i4Ipv6RipProfileIndex) == RIP6_SUCCESS)
    {
        *pi4RetValIpv6RipProfileTrigDelayTime =
            (INT4) garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u4TrigDelayTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipProfileRouteAge
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipProfileRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipProfileRouteAge (UINT4 u4InstanceId,
                                  INT4 i4Ipv6RipProfileIndex,
                                  INT4 *pi4RetValIpv6RipProfileRouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n",
                     i4Ipv6RipProfileIndex); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2) i4Ipv6RipProfileIndex)
        == RIP6_SUCCESS)
    {
        *pi4RetValIpv6RipProfileRouteAge =
            (INT4) garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u4RouteAge;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipProfileGarbageCollectTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                retValIpv6RipProfileGarbageCollectTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipProfileGarbageCollectTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipProfileGarbageCollectTime (UINT4 u4InstanceId,
                                            INT4 i4Ipv6RipProfileIndex,
                                            INT4
                                            *pi4RetValIpv6RipProfileGarbageCollectTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                        i4Ipv6RipProfileIndex); ***/
    if (Rip6ProfileEntryExists (u4InstanceId,
                                (UINT2) i4Ipv6RipProfileIndex) == RIP6_SUCCESS)
    {
        *pi4RetValIpv6RipProfileGarbageCollectTime =
            (INT4) garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u4GcTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipProfileStatus
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipProfileStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipProfileStatus (UINT4 u4InstanceId, INT4 i4Ipv6RipProfileIndex,
                                INT4 i4SetValIpv6RipProfileStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileStatus = %d\n", 
                i4SetValIpv6RipProfileStatus); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2) i4Ipv6RipProfileIndex)
        == RIP6_SUCCESS)
    {
        garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u1Status =
            (UINT1) i4SetValIpv6RipProfileStatus;
        return SNMP_SUCCESS;
    }
    /* Profile entry does not exist */
    if (i4SetValIpv6RipProfileStatus == IP6_RIP_PROFILE_VALID)
    {
        /* create a profile index */
        garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u1Status =
            IP6_RIP_PROFILE_VALID;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipProfileHorizon
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileHorizon
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipProfileHorizon ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipProfileHorizon (UINT4 u4InstanceId, INT4 i4Ipv6RipProfileIndex,
                                 INT4 i4SetValIpv6RipProfileHorizon)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileHorizon = %d\n", 
            i4SetValIpv6RipProfileHorizon); ***/
    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2) i4Ipv6RipProfileIndex)
        == RIP6_SUCCESS)
    {
        garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u1Horizon =
            (UINT1) i4SetValIpv6RipProfileHorizon;

        RM_GET_SEQ_NUM (&u4SeqNum);
        SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RipProfileHorizon,
                              u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                              1, SNMP_SUCCESS);
        SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%i %i", i4Ipv6RipProfileIndex,
                          i4SetValIpv6RipProfileHorizon));

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipProfilePeriodicUpdTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfilePeriodicUpdTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipProfilePeriodicUpdTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipProfilePeriodicUpdTime (UINT4 u4InstanceId,
                                         INT4 i4Ipv6RipProfileIndex,
                                         INT4
                                         i4SetValIpv6RipProfilePeriodicUpdTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfilePeriodicUpdTime = %d\n", 
            i4SetValIpv6RipProfilePeriodicUpdTime); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2) i4Ipv6RipProfileIndex)
        == RIP6_SUCCESS)
    {
        garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u4PeriodicUpdTime =
            (UINT4) i4SetValIpv6RipProfilePeriodicUpdTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipProfileTrigDelayTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileTrigDelayTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipProfileTrigDelayTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipProfileTrigDelayTime (UINT4 u4InstanceId,
                                       INT4 i4Ipv6RipProfileIndex,
                                       INT4 i4SetValIpv6RipProfileTrigDelayTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileTrigDelayTime = %d\n", 
                i4SetValIpv6RipProfileTrigDelayTime); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2) i4Ipv6RipProfileIndex)
        == RIP6_SUCCESS)
    {
        garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u4TrigDelayTime =
            (UINT4) i4SetValIpv6RipProfileTrigDelayTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipProfileRouteAge
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileRouteAge
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipProfileRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipProfileRouteAge (UINT4 u4InstanceId,
                                  INT4 i4Ipv6RipProfileIndex,
                                  INT4 i4SetValIpv6RipProfileRouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileRouteAge = %d\n", 
                i4SetValIpv6RipProfileRouteAge); ***/
    if (Rip6ProfileEntryExists (u4InstanceId,
                                (UINT2) i4Ipv6RipProfileIndex) == RIP6_SUCCESS)
    {
        garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u4RouteAge =
            (UINT4) i4SetValIpv6RipProfileRouteAge;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipProfileGarbageCollectTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                setValIpv6RipProfileGarbageCollectTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipProfileGarbageCollectTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipProfileGarbageCollectTime (UINT4 u4InstanceId,
                                            INT4 i4Ipv6RipProfileIndex,
                                            INT4
                                            i4SetValIpv6RipProfileGarbageCollectTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
                i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileGarbageCollectTime = %d\n", 
                i4SetValIpv6RipProfileGarbageCollectTime); ***/
    if (Rip6ProfileEntryExists (u4InstanceId, (UINT2) i4Ipv6RipProfileIndex)
        == RIP6_SUCCESS)
    {
        garip6InstanceDatabase[u4InstanceId]->
            arip6Profile[i4Ipv6RipProfileIndex]->u4GcTime =
            (UINT4) i4SetValIpv6RipProfileGarbageCollectTime;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipProfileStatus
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipProfileStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipProfileStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4InstanceId,
                                   INT4 i4Ipv6RipProfileIndex,
                                   INT4 i4TestValIpv6RipProfileStatus)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileStatus = %d\n", 
            i4TestValIpv6RipProfileStatus); ***/
    INT2                i2Index = RIP6_INITIALIZE_ZERO;
    INT4                i4MaxIfs = 0;
    INT4                i4MaxProfs = 0;

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);
    i4MaxProfs =
        RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                  FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                  u4PreAllocatedUnits);

    if ((i4Ipv6RipProfileIndex < RIP6_INITIALIZE_ZERO) ||
        (i4Ipv6RipProfileIndex >= i4MaxProfs))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (i4Ipv6RipProfileIndex == RIP6_INITIALIZE_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if ((i4TestValIpv6RipProfileStatus != IP6_RIP_PROFILE_VALID) &&
        (i4TestValIpv6RipProfileStatus != IP6_RIP_PROFILE_INVALID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    for (i2Index = RIP6_INITIALIZE_ONE; i2Index <= i4MaxIfs; i2Index++)
    {
        if (RIP6_GET_INST_IF_ENTRY (u4InstanceId, i2Index) == NULL)
        {
            continue;
        }
        if ((RIP6_GET_INST_IF_ENTRY (u4InstanceId, i2Index)->
             u2ProfIndex == i4Ipv6RipProfileIndex) &&
            (i4TestValIpv6RipProfileStatus == IP6_RIP_PROFILE_INVALID))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipProfileHorizon
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileHorizon
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipProfileHorizon ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipProfileHorizon (UINT4 *pu4ErrorCode,
                                    UINT4 u4InstanceId,
                                    INT4 i4Ipv6RipProfileIndex,
                                    INT4 i4TestValIpv6RipProfileHorizon)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileHorizon = %d\n", 
            i4TestValIpv6RipProfileHorizon); ***/
    UNUSED_PARAM (i4Ipv6RipProfileIndex);
    UNUSED_PARAM (u4InstanceId);
    if ((i4TestValIpv6RipProfileHorizon != IP6_RIP_NO_HORIZON) &&
        (i4TestValIpv6RipProfileHorizon != IP6_RIP_SPLIT_HORIZON) &&
        (i4TestValIpv6RipProfileHorizon != IP6_RIP_POISON_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipProfilePeriodicUpdTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfilePeriodicUpdTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipProfilePeriodicUpdTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipProfilePeriodicUpdTime (UINT4 *pu4ErrorCode,
                                            UINT4 u4InstanceId,
                                            INT4 i4Ipv6RipProfileIndex,
                                            INT4
                                            i4TestValIpv6RipProfilePeriodicUpdTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfilePeriodicUpdTime = %d\n", 
            i4TestValIpv6RipProfilePeriodicUpdTime); ***/
    UNUSED_PARAM (i4Ipv6RipProfileIndex);
    UNUSED_PARAM (u4InstanceId);
    if ((i4TestValIpv6RipProfilePeriodicUpdTime >=
         IP6_RIP_PROF_MIN_UDPATE_TIME) &&
        (i4TestValIpv6RipProfilePeriodicUpdTime <=
         IP6_RIP_PROF_MAX_UDPATE_TIME))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipProfileTrigDelayTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileTrigDelayTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipProfileTrigDelayTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipProfileTrigDelayTime (UINT4 *pu4ErrorCode,
                                          UINT4 u4InstanceId,
                                          INT4 i4Ipv6RipProfileIndex,
                                          INT4
                                          i4TestValIpv6RipProfileTrigDelayTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileTrigDelayTime = %d\n", 
            i4TestValIpv6RipProfileTrigDelayTime); ***/
    UNUSED_PARAM (i4Ipv6RipProfileIndex);
    UNUSED_PARAM (u4InstanceId);
    if ((i4TestValIpv6RipProfileTrigDelayTime >= IP6_RIP_PROF_MIN_TDELAY_TIME)
        && (i4TestValIpv6RipProfileTrigDelayTime <=
            IP6_RIP_PROF_MAX_TDELAY_TIME))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipProfileRouteAge
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileRouteAge
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipProfileRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipProfileRouteAge (UINT4 *pu4ErrorCode,
                                     UINT4 u4InstanceId,
                                     INT4 i4Ipv6RipProfileIndex,
                                     INT4 i4TestValIpv6RipProfileRouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileRouteAge = %d\n", 
            i4TestValIpv6RipProfileRouteAge); ***/
    UNUSED_PARAM (i4Ipv6RipProfileIndex);
    UNUSED_PARAM (u4InstanceId);
    if ((i4TestValIpv6RipProfileRouteAge >= IP6_RIP_PROF_MIN_ROUTE_AGE) &&
        (i4TestValIpv6RipProfileRouteAge <= IP6_RIP_PROF_MAX_ROUTE_AGE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipProfileGarbageCollectTime
 Input       :  The Indices
                Ipv6RipProfileIndex

                The Object 
                testValIpv6RipProfileGarbageCollectTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipProfileGarbageCollectTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipProfileGarbageCollectTime (UINT4 *pu4ErrorCode,
                                               UINT4 u4InstanceId,
                                               INT4 i4Ipv6RipProfileIndex,
                                               INT4
                                               i4TestValIpv6RipProfileGarbageCollectTime)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileIndex = %d\n", 
            i4Ipv6RipProfileIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipProfileGarbageCollectTime = %d\n", 
            i4TestValIpv6RipProfileGarbageCollectTime); ***/
    UNUSED_PARAM (i4Ipv6RipProfileIndex);
    UNUSED_PARAM (u4InstanceId);
    if ((i4TestValIpv6RipProfileGarbageCollectTime >=
         IP6_RIP_PROF_MIN_GARBAGE_TIME) &&
        (i4TestValIpv6RipProfileGarbageCollectTime <=
         IP6_RIP_PROF_MAX_GARBAGE_TIME))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* LOW LEVEL Routines for Table : Ipv6RipRouteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIrip6RipRouteTable
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhValidateIndexInstanceFsMIrip6RipRouteTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhValidateIndexInstanceFsMIrip6RipRouteTable (UINT4 u4InstanceId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pIpv6RipRouteDest,
                                               INT4 i4Ipv6RipRoutePfxLength,
                                               INT4 i4Ipv6RipRouteProtocol,
                                               INT4 i4Ipv6RipRouteIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
                i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
                i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
                i4Ipv6RipRouteIfIndex); ***/
    tIp6Addr            ip6Addr;
    INT4                i4MaxIfs = 0;

    i4MaxIfs = RIP6_MIN (MAX_RIP6_IFACES_LIMIT + 1,
                         FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].
                         u4PreAllocatedUnits);

    if ((i4Ipv6RipRouteIfIndex < RIP6_INITIALIZE_ONE) ||
        (i4Ipv6RipRouteIfIndex > i4MaxIfs))
    {
        return SNMP_FAILURE;
    }

    if (u4InstanceId !=
        RIP6_GET_INST_IF_MAP_ENTRY ((UINT4) i4Ipv6RipRouteIfIndex)->
        u4InstanceId)
    {
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList);

    if ((IS_ADDR_LLOCAL (ip6Addr)) ||
        (IS_ADDR_MULTI (ip6Addr)) ||
        (i4Ipv6RipRoutePfxLength < RIP6_INITIALIZE_ONE) ||
        (i4Ipv6RipRoutePfxLength > RIP6_MAX_PRFX_LEN) ||
        (i4Ipv6RipRouteProtocol < IP6_OTHER_PROTOID) ||
        (i4Ipv6RipRouteProtocol > IP6_IDRP_PROTOID))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMIrip6RipRouteTable
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

/*** $$TRACE_PROCEDURE_NAME = nmhGetFirstIpv6RipRouteTable ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFirstIndexFsMIrip6RipRouteTable (UINT4 u4InstanceId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pIpv6RipRouteDest,
                                       INT4 *pi4Ipv6RipRoutePfxLength,
                                       INT4 *pi4Ipv6RipRouteProtocol,
                                       INT4 *pi4Ipv6RipRouteIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n",
            *pi4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n",
            *pi4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            *pi4Ipv6RipRouteIfIndex); ***/

    tRip6RtEntry       *pRtTmp = NULL;
    VOID               *pRibNode = NULL;

    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    if (Rip6TrieGetFirstEntry (&pRtTmp, &pRibNode, u4InstanceId) ==
        RIP6_FAILURE)
    {
        /* No route is present. */
        return SNMP_FAILURE;
    }

    *pi4Ipv6RipRoutePfxLength = pRtTmp->u1Prefixlen;
    *pi4Ipv6RipRouteProtocol = pRtTmp->i1Proto;
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList,
                 &pRtTmp->dst);
    pIpv6RipRouteDest->i4_Length = 16;
    *pi4Ipv6RipRouteIfIndex = pRtTmp->u4Index;
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMIrip6RipRouteTable
 Input       :  The Indices
                Ipv6RipRouteDest
                nextIpv6RipRouteDest
                Ipv6RipRoutePfxLength
                nextIpv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                nextIpv6RipRouteProtocol
                Ipv6RipRouteIfIndex
                nextIpv6RipRouteIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
 /*** $$TRACE_PROCEDURE_NAME = nmhGetNextIpv6RipRouteTable ***/
 /*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetNextIndexFsMIrip6RipRouteTable (UINT4 u4InstanceId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pIpv6RipRouteDest,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextIpv6RipRouteDest,
                                      INT4 i4Ipv6RipRoutePfxLength,
                                      INT4 *pi4NextIpv6RipRoutePfxLength,
                                      INT4 i4Ipv6RipRouteProtocol,
                                      INT4 *pi4NextIpv6RipRouteProtocol,
                                      INT4 i4Ipv6RipRouteIfIndex,
                                      INT4 *pi4NextIpv6RipRouteIfIndex)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            *pi4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            *pi4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            *pi4Ipv6RipRouteIfIndex); ***/

    tRip6RtEntry       *pRtEntry = NULL;
    tRip6RtEntry       *pRtTmp = NULL;
    VOID               *pRibNode = NULL;
    VOID               *pNextRibNode = NULL;
    tIp6Addr            ip6Addr;

    UNUSED_PARAM (i4Ipv6RipRouteIfIndex);

    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList);

    if (Rip6TrieBestEntry (&ip6Addr, RIP6_DYNAMIC_LOOKUP, (UINT1) i4Ipv6RipRoutePfxLength,
                           u4InstanceId, &pRtTmp, &pRibNode) == RIP6_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pRtEntry = pRtTmp;
    if(i4Ipv6RipRouteProtocol == RIP6_LOCAL)
    {
        /* Get the next prefix from the Trie */
        if (Rip6TrieGetNextEntry (&pRtEntry, pRibNode, &pNextRibNode,
                                  u4InstanceId) == RIP6_FAILURE)
        {
            return SNMP_FAILURE;
    }

    if (pRtEntry == NULL)
    {
            return SNMP_FAILURE;
        }
    }

    else
    {
        while (1)
        {
            if (pRtEntry == NULL)
            {
                return SNMP_FAILURE;
            }
            if (pRtEntry->u4Index == (UINT4)i4Ipv6RipRouteIfIndex)
            {
                pRtTmp = pRtEntry;
                pRtEntry = pRtEntry->pNext;
                if (pRtEntry == NULL)
                {
                    if (Rip6TrieGetNextEntry (&pRtTmp, pRibNode, &pNextRibNode,
                                  u4InstanceId) == RIP6_FAILURE)
        {
            return SNMP_FAILURE;
        }
                    pRtEntry = pRtTmp;
                    break;
                }
                break;
            }
            pRtEntry = pRtEntry->pNext;
    }
    }

    if (pRtEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextIpv6RipRouteDest->pu1_OctetList,
                 &pRtEntry->dst);


    *pi4NextIpv6RipRoutePfxLength = pRtEntry->u1Prefixlen;
    *pi4NextIpv6RipRouteProtocol = pRtEntry->i1Proto;
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextIpv6RipRouteDest->pu1_OctetList,
                 &pRtEntry->dst);
    pNextIpv6RipRouteDest->i4_Length = 16;
    *pi4NextIpv6RipRouteIfIndex = pRtEntry->u4Index;


    return SNMP_SUCCESS;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipRouteNextHop
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                retValIpv6RipRouteNextHop
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipRouteNextHop ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipRouteNextHop (UINT4 u4InstanceId,
                               tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                               INT4 i4Ipv6RipRoutePfxLength,
                               INT4 i4Ipv6RipRouteProtocol,
                               INT4 i4Ipv6RipRouteIfIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValIpv6RipRouteNextHop)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/

    tRip6RtEntry       *pRt = NULL;
    tIp6Addr            ip6Addr;

    UNUSED_PARAM (u4InstanceId);
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList);

    pRetValIpv6RipRouteNextHop->i4_Length = 16;

    pRt = Rip6RouteFindEntry (&ip6Addr, (UINT1) i4Ipv6RipRoutePfxLength,
                              (INT1) i4Ipv6RipRouteProtocol,
                              (UINT4) i4Ipv6RipRouteIfIndex);
    if (pRt == NULL)
    {
        return SNMP_FAILURE;
    }
    Ip6AddrCopy ((tIp6Addr *) (VOID *) pRetValIpv6RipRouteNextHop->
                 pu1_OctetList, &pRt->nexthop);
    return SNMP_SUCCESS;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipRouteMetric
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                retValIpv6RipRouteMetric
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipRouteMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipRouteMetric (UINT4 u4InstanceId,
                              tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                              INT4 i4Ipv6RipRoutePfxLength,
                              INT4 i4Ipv6RipRouteProtocol,
                              INT4 i4Ipv6RipRouteIfIndex,
                              INT4 *pi4RetValIpv6RipRouteMetric)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/

    tRip6RtEntry       *pRt = NULL;
    tIp6Addr            ip6Addr;
    UNUSED_PARAM (u4InstanceId);
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList);
    pRt =
        Rip6RouteFindEntry (&ip6Addr, (UINT1) i4Ipv6RipRoutePfxLength,
                            (INT1) i4Ipv6RipRouteProtocol,
                            (UINT4) i4Ipv6RipRouteIfIndex);
    if (pRt != NULL)
    {
        *pi4RetValIpv6RipRouteMetric = (INT4) pRt->u1Metric;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipRouteTag
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                retValIpv6RipRouteTag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipRouteTag (UINT4 u4InstanceId,
                           tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                           INT4 i4Ipv6RipRoutePfxLength,
                           INT4 i4Ipv6RipRouteProtocol,
                           INT4 i4Ipv6RipRouteIfIndex,
                           INT4 *pi4RetValIpv6RipRouteTag)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/

    tRip6RtEntry       *pRt = NULL;
    tIp6Addr            ip6Addr;
    UNUSED_PARAM (u4InstanceId);
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList);
    pRt =
        Rip6RouteFindEntry (&ip6Addr, (UINT1) i4Ipv6RipRoutePfxLength,
                            (INT1) i4Ipv6RipRouteProtocol,
                            (UINT4) i4Ipv6RipRouteIfIndex);
    if (pRt != NULL)
    {
        *pi4RetValIpv6RipRouteTag = (INT4) pRt->u2RtTag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhGetFsMIrip6RipRouteAge
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                retValIpv6RipRouteAge
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetFsMIrip6RipRouteAge ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetFsMIrip6RipRouteAge (UINT4 u4InstanceId,
                           tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                           INT4 i4Ipv6RipRoutePfxLength,
                           INT4 i4Ipv6RipRouteProtocol,
                           INT4 i4Ipv6RipRouteIfIndex,
                           INT4 *pi4RetValIpv6RipRouteAge)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/

    tRip6RtEntry       *pRt = NULL;
    UINT4               SysTime;
    tIp6Addr            ip6Addr;
    UNUSED_PARAM (u4InstanceId);
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList);
    pRt =
        Rip6RouteFindEntry (&ip6Addr, i4Ipv6RipRoutePfxLength,
                            (INT1) i4Ipv6RipRouteProtocol,
                            (UINT4) i4Ipv6RipRouteIfIndex);
    /*pRt = Rip6RouteFindEntry (&ip6Addr, i4Ipv6RipRoutePfxLength,
       (INT1) i4Ipv6RipRouteProtocol,
       (UINT4) i4Ipv6RipRouteIfIndex); */
    if (pRt != NULL)
    {
        if (pRt->i1Proto != RIPNG)
        {
            *pi4RetValIpv6RipRouteAge = 0;
            return SNMP_SUCCESS;
        }

        OsixGetSysTime (&SysTime);
        *pi4RetValIpv6RipRouteAge = (SysTime - pRt->u4ChangeTime);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsMIrip6RipRouteMetric
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                setValIpv6RipRouteMetric
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipRouteMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipRouteMetric (UINT4 u4InstanceId,
                              tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                              INT4 i4Ipv6RipRoutePfxLength,
                              INT4 i4Ipv6RipRouteProtocol,
                              INT4 i4Ipv6RipRouteIfIndex,
                              INT4 i4SetValIpv6RipRouteMetric)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
                i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteMetric = %d\n", 
            i4SetValIpv6RipRouteMetric); ***/

    tRip6RtEntry       *pRtEntry = NULL;
    tIp6Addr            ip6Addr;

    UNUSED_PARAM (u4InstanceId);
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList);
    pRtEntry =
        Rip6RouteFindEntry (&ip6Addr, (UINT1) i4Ipv6RipRoutePfxLength,
                            (INT1) i4Ipv6RipRouteProtocol,
                            (UINT4) i4Ipv6RipRouteIfIndex);
    if (pRtEntry != NULL)
    {
        pRtEntry->u1Metric = (UINT1) i4SetValIpv6RipRouteMetric;

        Rip6SendRtChgNotification (&pRtEntry->dst, pRtEntry->u1Prefixlen,
                                   &pRtEntry->nexthop, pRtEntry->u1Metric,
                                   pRtEntry->i1Proto, pRtEntry->u4Index,
                                   pRtEntry->u1Preference,
                                   NETIPV6_MODIFY_ROUTE);

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhSetFsMIrip6RipRouteTag
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                setValIpv6RipRouteTag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhSetFsMIrip6RipRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhSetFsMIrip6RipRouteTag (UINT4 u4InstanceId,
                           tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                           INT4 i4Ipv6RipRoutePfxLength,
                           INT4 i4Ipv6RipRouteProtocol,
                           INT4 i4Ipv6RipRouteIfIndex,
                           INT4 i4SetValIpv6RipRouteTag)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteTag = %d\n", 
            i4SetValIpv6RipRouteTag); ***/

    tRip6RtEntry       *pRt = NULL;
    tIp6Addr            ip6Addr;
    UNUSED_PARAM (u4InstanceId);
    Ip6AddrCopy (&ip6Addr,
                 (tIp6Addr *) (VOID *) pIpv6RipRouteDest->pu1_OctetList);
    pRt =
        Rip6RouteFindEntry (&ip6Addr, (UINT1) i4Ipv6RipRoutePfxLength,
                            (INT1) i4Ipv6RipRouteProtocol,
                            (UINT4) i4Ipv6RipRouteIfIndex);
    if (pRt != NULL)
    {
        pRt->u2RtTag = (UINT2) i4SetValIpv6RipRouteTag;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipRouteMetric
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                testValIpv6RipRouteMetric
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipRouteMetric ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipRouteMetric (UINT4 *pu4ErrorCode,
                                 UINT4 u4InstanceId,
                                 tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                                 INT4 i4Ipv6RipRoutePfxLength,
                                 INT4 i4Ipv6RipRouteProtocol,
                                 INT4 i4Ipv6RipRouteIfIndex,
                                 INT4 i4TestValIpv6RipRouteMetric)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
            i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
            i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
            i4Ipv6RipRouteIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteMetric = %d\n", 
            i4TestValIpv6RipRouteMetric); ***/
    UNUSED_PARAM (u4InstanceId);
    UNUSED_PARAM (pIpv6RipRouteDest);
    UNUSED_PARAM (i4Ipv6RipRoutePfxLength);
    UNUSED_PARAM (i4Ipv6RipRouteProtocol);
    UNUSED_PARAM (i4Ipv6RipRouteIfIndex);
    if ((i4TestValIpv6RipRouteMetric >= IP6_RIP_ROUTE_MIN_METRIC) &&
        (i4TestValIpv6RipRouteMetric <= IP6_RIP_ROUTE_MAX_METRIC))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhTestv2FsMIrip6RipRouteTag
 Input       :  The Indices
                Ipv6RipRouteDest
                Ipv6RipRoutePfxLength
                Ipv6RipRouteProtocol
                Ipv6RipRouteIfIndex

                The Object 
                testValIpv6RipRouteTag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMIrip6RipRouteTag ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMIrip6RipRouteTag (UINT4 *pu4ErrorCode,
                              UINT4 u4InstanceId,
                              tSNMP_OCTET_STRING_TYPE * pIpv6RipRouteDest,
                              INT4 i4Ipv6RipRoutePfxLength,
                              INT4 i4Ipv6RipRouteProtocol,
                              INT4 i4Ipv6RipRouteIfIndex,
                              INT4 i4TestValIpv6RipRouteTag)
{
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRoutePfxLength = %d\n", 
                i4Ipv6RipRoutePfxLength); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteProtocol = %d\n", 
                i4Ipv6RipRouteProtocol); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteIfIndex = %d\n", 
                i4Ipv6RipRouteIfIndex); ***/
   /*** $$TRACE_LOG (ENTRY, "Ipv6RipRouteTag = %d\n", 
                i4TestValIpv6RipRouteTag); ***/

    UNUSED_PARAM (pIpv6RipRouteDest);
    UNUSED_PARAM (i4Ipv6RipRoutePfxLength);
    UNUSED_PARAM (i4Ipv6RipRouteProtocol);
    UNUSED_PARAM (i4Ipv6RipRouteIfIndex);
    UNUSED_PARAM (u4InstanceId);
    if ((i4TestValIpv6RipRouteTag == RIP6_INTERNAL) ||
        (i4TestValIpv6RipRouteTag == RIP6_EXTERNAL))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

   /*** $$TRACE_LOG (EXIT," SNMP Success\n"); ***/
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMIrip6RipPeerTable
 Input       :  The Indices
                Fsrip6RipPeerAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIrip6RipPeerTable (UINT4 u4InstanceId,
                                              tSNMP_OCTET_STRING_TYPE *
                                              pFsrip6RipPeerAddr)
{
    tIp6Addr            Ip6Addr;

    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }

    Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                 pFsrip6RipPeerAddr->pu1_OctetList);

    if ((!(IS_ADDR_LLOCAL (Ip6Addr))) || (IS_ADDR_MULTI (Ip6Addr)) ||
        (IS_ADDR_UNSPECIFIED (Ip6Addr)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6RipPeerTable
 Input       :  The Indices
                Fsrip6RipPeerAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIrip6RipPeerTable (UINT4 u4InstanceId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pFsrip6RipPeerAddr)
{
    UINT4               u4Index;
    tIp6Addr            tempAddr;

    MEMSET (&tempAddr, 0, sizeof (tIp6Addr));

    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_PEERS; u4Index++)
    {
        if (!IS_ADDR_UNSPECIFIED (garip6InstanceDatabase[u4InstanceId]->
                                  aRip6PeerTable[u4Index].PeerAddr))
        {
            if (IS_ADDR_UNSPECIFIED (tempAddr))
            {
                Ip6AddrCopy (&tempAddr,
                             &(garip6InstanceDatabase[u4InstanceId]->
                               aRip6PeerTable[u4Index].PeerAddr));
            }
            else
            {
                if (Ip6IsAddrGreater (&tempAddr,
                                      &garip6InstanceDatabase[u4InstanceId]->
                                      aRip6PeerTable[u4Index].PeerAddr) ==
                    FAILURE)
                {
                    Ip6AddrCopy (&tempAddr,
                                 &(garip6InstanceDatabase[u4InstanceId]->
                                   aRip6PeerTable[u4Index].PeerAddr));
                }
            }
        }
    }

    if (!IS_ADDR_UNSPECIFIED (tempAddr))
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *)
                     pFsrip6RipPeerAddr->pu1_OctetList, &tempAddr);
        pFsrip6RipPeerAddr->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6RipPeerTable
 Input       :  The Indices
                Fsrip6RipPeerAddr
                nextFsrip6RipPeerAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIrip6RipPeerTable (UINT4 u4InstanceId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsrip6RipPeerAddr,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextFsrip6RipPeerAddr)
{
    UINT4               u4Index;
    tIp6Addr            tempAddr;

    MEMSET (&tempAddr, 0, sizeof (tIp6Addr));

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_PEERS; u4Index++)
    {

        if ((MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                       aRip6PeerTable[u4Index].PeerAddr),
                     pFsrip6RipPeerAddr->pu1_OctetList,
                     sizeof (tIp6Addr)) != RIP6_INITIALIZE_ZERO)
            && (!IS_ADDR_UNSPECIFIED (garip6InstanceDatabase[u4InstanceId]->
                                      aRip6PeerTable[u4Index].PeerAddr)))
        {
            if (Ip6IsAddrGreater
                ((tIp6Addr *) (VOID *) pFsrip6RipPeerAddr->pu1_OctetList,
                 &garip6InstanceDatabase[u4InstanceId]->aRip6PeerTable[u4Index].
                 PeerAddr) == SUCCESS)
            {
                if (IS_ADDR_UNSPECIFIED (tempAddr))
                {
                    Ip6AddrCopy (&tempAddr,
                                 &(garip6InstanceDatabase[u4InstanceId]->
                                   aRip6PeerTable[u4Index].PeerAddr));
                }
                else
                {
                    if (Ip6IsAddrGreater (&tempAddr,
                                          &garip6InstanceDatabase
                                          [u4InstanceId]->
                                          aRip6PeerTable[u4Index].PeerAddr) ==
                        FAILURE)
                    {
                        Ip6AddrCopy (&tempAddr,
                                     &(garip6InstanceDatabase[u4InstanceId]->
                                       aRip6PeerTable[u4Index].PeerAddr));
                    }
                }
            }
        }
    }
    if (!IS_ADDR_UNSPECIFIED (tempAddr))
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *) pNextFsrip6RipPeerAddr->
                     pu1_OctetList, &tempAddr);
        pNextFsrip6RipPeerAddr->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrip6RipPeerEntryStatus
 Input       :  The Indices
                Fsrip6RipPeerAddr

                The Object 
                retValFsrip6RipPeerEntryStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIrip6RipPeerEntryStatus (UINT4 u4InstanceId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsrip6RipPeerAddr,
                                  INT4 *pi4RetValFsrip6RipPeerEntryStatus)
{
    UINT4               u4Index;

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_PEERS; u4Index++)
    {
        if (MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6PeerTable[u4Index].PeerAddr),
                    pFsrip6RipPeerAddr->pu1_OctetList, sizeof (tIp6Addr))
            == RIP6_INITIALIZE_ZERO)
        {
            *pi4RetValFsrip6RipPeerEntryStatus = RIP6_VALID;
            return SNMP_SUCCESS;
        }
    }

    /* Entry is not found */
    *pi4RetValFsrip6RipPeerEntryStatus = RIP6_INVALID;
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrip6RipPeerEntryStatus
 Input       :  The Indices
                Fsrip6RipPeerAddr

                The Object 
                setValFsrip6RipPeerEntryStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIrip6RipPeerEntryStatus (UINT4 u4InstanceId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsrip6RipPeerAddr,
                                  INT4 i4SetValFsrip6RipPeerEntryStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (i4SetValFsrip6RipPeerEntryStatus == RIP6_VALID)
    {
        if (Rip6AddPeerEntry (u4InstanceId,
                              (tIp6Addr *) (VOID *)
                              pFsrip6RipPeerAddr->pu1_OctetList)
            == RIP6_SUCCESS)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RipPeerEntryStatus,
                                  u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                                  1, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsrip6RipPeerAddr,
                              i4SetValFsrip6RipPeerEntryStatus));
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (Rip6DeletePeerEntry (u4InstanceId,
                                 (tIp6Addr *) (VOID *)
                                 pFsrip6RipPeerAddr->pu1_OctetList)
            == RIP6_SUCCESS)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RipPeerEntryStatus,
                                  u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                                  1, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i", pFsrip6RipPeerAddr,
                              i4SetValFsrip6RipPeerEntryStatus));
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipPeerEntryStatus
 Input       :  The Indices
                Fsrip6RipPeerAddr

                The Object 
                testValFsrip6RipPeerEntryStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIrip6RipPeerEntryStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4InstanceId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsrip6RipPeerAddr,
                                     INT4 i4TestValFsrip6RipPeerEntryStatus)
{
    UINT4               u4Index;
    tIp6Addr            Ip6Addr;

    Ip6AddrCopy (&Ip6Addr, (tIp6Addr *) (VOID *)
                 pFsrip6RipPeerAddr->pu1_OctetList);

    if ((i4TestValFsrip6RipPeerEntryStatus != RIP6_VALID) &&
        (i4TestValFsrip6RipPeerEntryStatus != RIP6_INVALID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_PEER_STATUS);
        return SNMP_FAILURE;
    }

    if ((!(IS_ADDR_LLOCAL (Ip6Addr))) || (IS_ADDR_MULTI (Ip6Addr)) ||
        (IS_ADDR_UNSPECIFIED (Ip6Addr)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RIP6_INV_PEER_ADDR);
        return SNMP_FAILURE;
    }

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_PEERS; u4Index++)
    {
        if (MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6PeerTable[u4Index].PeerAddr),
                    pFsrip6RipPeerAddr->pu1_OctetList,
                    sizeof (tIp6Addr)) == RIP6_INITIALIZE_ZERO)
        {
            /* Entry exists already  */
            if (i4TestValFsrip6RipPeerEntryStatus == RIP6_VALID)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_RIP6_INV_PEER_STATUS);
                return SNMP_FAILURE;
            }

            /*sivarka -add */
            /* The entry is present & is not 
             * set to RIP6_VALID so return SUCCESS */
            return SNMP_SUCCESS;
        }
    }

    /* Entry not found */
    if (i4TestValFsrip6RipPeerEntryStatus == RIP6_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RIP6_INV_PEER_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : Fsrip6RipAdvFilterTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsrip6RipAdvFilterTable
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMIrip6RipAdvFilterTable (UINT4 u4InstanceId,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsrip6RipAdvFilterAddress)
{
    tIp6Addr            Ip6Addr;

    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsrip6RipAdvFilterAddress->
                 pu1_OctetList);

    if ((IS_ADDR_LLOCAL (Ip6Addr)) || (IS_ADDR_MULTI (Ip6Addr)) ||
        (IS_ADDR_UNSPECIFIED (Ip6Addr)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsrip6RipAdvFilterTable
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMIrip6RipAdvFilterTable (UINT4 u4InstanceId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsrip6RipAdvFilterAddress)
{
    UINT4               u4Index;
    tIp6Addr            tempAddr;

    MEMSET (&tempAddr, 0, sizeof (tIp6Addr));

    if (garip6InstanceStatus[u4InstanceId] != RIP6_INST_UP)
    {
        return SNMP_FAILURE;
    }
    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_ADV_FILTERS;
         u4Index++)
    {
        if (!IS_ADDR_UNSPECIFIED (garip6InstanceDatabase[u4InstanceId]->
                                  aRip6AdvFltTable[u4Index].NetAddr))
        {

            if (IS_ADDR_UNSPECIFIED (tempAddr))
            {
                Ip6AddrCopy (&tempAddr,
                             &(garip6InstanceDatabase[u4InstanceId]->
                               aRip6AdvFltTable[u4Index].NetAddr));
            }
            else
            {
                if (Ip6IsAddrGreater (&tempAddr,
                                      &garip6InstanceDatabase[u4InstanceId]->
                                      aRip6AdvFltTable[u4Index].NetAddr) ==
                    FAILURE)
                {
                    Ip6AddrCopy (&tempAddr,
                                 &(garip6InstanceDatabase[u4InstanceId]->
                                   aRip6AdvFltTable[u4Index].NetAddr));
                }
            }
        }
    }

    if (!IS_ADDR_UNSPECIFIED (tempAddr))
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *)
                     pFsrip6RipAdvFilterAddress->pu1_OctetList, &tempAddr);
        pFsrip6RipAdvFilterAddress->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsrip6RipAdvFilterTable
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress
                nextFsrip6RipAdvFilterAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMIrip6RipAdvFilterTable (UINT4 u4InstanceId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsrip6RipAdvFilterAddress,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsrip6RipAdvFilterAddress)
{
    UINT4               u4Index;
    tIp6Addr            tempAddr;

    MEMSET (&tempAddr, 0, sizeof (tIp6Addr));

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_ADV_FILTERS;
         u4Index++)
    {
        if ((MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                       aRip6AdvFltTable[u4Index].NetAddr),
                     pFsrip6RipAdvFilterAddress->pu1_OctetList,
                     sizeof (tIp6Addr)) != RIP6_INITIALIZE_ZERO)
            && (!IS_ADDR_UNSPECIFIED (garip6InstanceDatabase[u4InstanceId]->
                                      aRip6AdvFltTable[u4Index].NetAddr)))
        {
            if (Ip6IsAddrGreater
                ((tIp6Addr *) (VOID *) pFsrip6RipAdvFilterAddress->
                 pu1_OctetList,
                 &garip6InstanceDatabase[u4InstanceId]->
                 aRip6AdvFltTable[u4Index].NetAddr) == SUCCESS)
            {
                if (IS_ADDR_UNSPECIFIED (tempAddr))
                {
                    Ip6AddrCopy (&tempAddr,
                                 &(garip6InstanceDatabase[u4InstanceId]->
                                   aRip6AdvFltTable[u4Index].NetAddr));
                }
                else
                {
                    if (Ip6IsAddrGreater
                        (&tempAddr,
                         &garip6InstanceDatabase[u4InstanceId]->
                         aRip6AdvFltTable[u4Index].NetAddr) == FAILURE)
                    {
                        Ip6AddrCopy (&tempAddr,
                                     &(garip6InstanceDatabase[u4InstanceId]->
                                       aRip6AdvFltTable[u4Index].NetAddr));
                    }
                }
            }
        }
    }
    if (!IS_ADDR_UNSPECIFIED (tempAddr))
    {
        Ip6AddrCopy ((tIp6Addr *) (VOID *)
                     pNextFsrip6RipAdvFilterAddress->pu1_OctetList, &tempAddr);
        pNextFsrip6RipAdvFilterAddress->i4_Length = sizeof (tIp6Addr);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsrip6RipAdvFilterStatus
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress

                The Object 
                retValFsrip6RipAdvFilterStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMIrip6RipAdvFilterStatus (UINT4 u4InstanceId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsrip6RipAdvFilterAddress,
                                  INT4 *pi4RetValFsrip6RipAdvFilterStatus)
{
    UINT4               u4Index;

    for (u4Index = RIP6_INITIALIZE_ZERO; u4Index < RIP6_MAX_ADV_FILTERS;
         u4Index++)
    {
        if (MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6AdvFltTable[u4Index].NetAddr),
                    pFsrip6RipAdvFilterAddress->pu1_OctetList,
                    sizeof (tIp6Addr)) == RIP6_INITIALIZE_ZERO)
        {
            /* Entry found */
            *pi4RetValFsrip6RipAdvFilterStatus = RIP6_VALID;
            return SNMP_SUCCESS;
        }
    }

    /* Entry not found */
    *pi4RetValFsrip6RipAdvFilterStatus = RIP6_INVALID;
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsrip6RipAdvFilterStatus
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress

                The Object 
                setValFsrip6RipAdvFilterStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMIrip6RipAdvFilterStatus (UINT4 u4InstanceId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pFsrip6RipAdvFilterAddress,
                                  INT4 i4SetValFsrip6RipAdvFilterStatus)
{
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UINT4               u4SeqNum = 0;

    MEMSET (&SnmpNotifyInfo, 0, sizeof (tSnmpNotifyInfo));
    if (i4SetValFsrip6RipAdvFilterStatus == RIP6_VALID)
    {
        if (Rip6AddAdvFilterEntry
            (u4InstanceId, (tIp6Addr *) (VOID *) pFsrip6RipAdvFilterAddress->
             pu1_OctetList) == RIP6_SUCCESS)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RipAdvFilterStatus,
                                  u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                                  1, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i",
                              pFsrip6RipAdvFilterAddress,
                              i4SetValFsrip6RipAdvFilterStatus));
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (Rip6DeleteAdvFilterEntry
            (u4InstanceId, (tIp6Addr *) (VOID *) pFsrip6RipAdvFilterAddress->
             pu1_OctetList) == RIP6_SUCCESS)
        {
            RM_GET_SEQ_NUM (&u4SeqNum);
            SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, Fsrip6RipAdvFilterStatus,
                                  u4SeqNum, FALSE, Rip6Lock, Rip6UnLock,
                                  1, SNMP_SUCCESS);
            SNMP_NOTIFY_CFG ((SnmpNotifyInfo, "%s %i",
                              pFsrip6RipAdvFilterAddress,
                              i4SetValFsrip6RipAdvFilterStatus));
            return SNMP_SUCCESS;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2Fsrip6RipAdvFilterStatus
 Input       :  The Indices
                Fsrip6RipAdvFilterAddress

                The Object 
                testValFsrip6RipAdvFilterStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMIrip6RipAdvFilterStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4InstanceId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsrip6RipAdvFilterAddress,
                                     INT4 i4TestValFsrip6RipAdvFilterStatus)
{
    UINT4               u4Index;
    tIp6Addr            Ip6Addr;

    Ip6AddrCopy (&Ip6Addr,
                 (tIp6Addr *) (VOID *) pFsrip6RipAdvFilterAddress->
                 pu1_OctetList);

    if ((i4TestValFsrip6RipAdvFilterStatus != RIP6_VALID) &&
        (i4TestValFsrip6RipAdvFilterStatus != RIP6_INVALID))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_RIP6_INV_ADV_STATUS);
        return SNMP_FAILURE;
    }

    if ((IS_ADDR_LLOCAL (Ip6Addr)) || (IS_ADDR_MULTI (Ip6Addr)) ||
        (IS_ADDR_UNSPECIFIED (Ip6Addr)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RIP6_INV_ADV_ADDR);
        return SNMP_FAILURE;
    }

    for (u4Index = RIP6_INITIALIZE_ZERO;
         u4Index < RIP6_MAX_ADV_FILTERS; u4Index++)
    {
        if (MEMCMP (&(garip6InstanceDatabase[u4InstanceId]->
                      aRip6AdvFltTable[u4Index].NetAddr),
                    pFsrip6RipAdvFilterAddress->pu1_OctetList,
                    sizeof (tIp6Addr)) == RIP6_INITIALIZE_ZERO)
        {
            /* Entry exists already  */
            if (i4TestValFsrip6RipAdvFilterStatus == RIP6_VALID)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CLI_SET_ERR (CLI_RIP6_INV_ADV_STATUS);
                return SNMP_FAILURE;
            }
            /* sivarka -add */
            /* The entry is present & is not 
             * set to RIP6_VALID so return SUCCESS */
            return SNMP_SUCCESS;

        }
    }

    /* Entry not found */
    if (i4TestValFsrip6RipAdvFilterStatus == RIP6_INVALID)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CLI_SET_ERR (CLI_RIP6_INV_ADV_STATUS);
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsRip6DistInOutRouteMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsRip6DistInOutRouteMapTable
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsRip6DistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                                      pFsRip6DistInOutRouteMapName,
                                                      INT4
                                                      i4FsRip6DistInOutRouteMapType)
{
#ifdef ROUTEMAP_WANTED
    if (pFsRip6DistInOutRouteMapName == NULL
        || pFsRip6DistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }
    if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName
            (gpDistanceFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName
            (gpDistributeInFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_OUT)
    {
        if (CmpFilterRMapName
            (gpDistributeOutFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
#else
    UNUSED_PARAM (pFsRip6DistInOutRouteMapName);
    UNUSED_PARAM (i4FsRip6DistInOutRouteMapType);
#endif
    return SNMP_FAILURE;
}

/*Filtering nmh processing utils*/
INT4                GetFlterTypeByFlterPtrRip6 (tFilteringRMap * pFltr1);
INT4                FilteringRMapCompareRip6 (tFilteringRMap * pFltr1,
                                              tFilteringRMap * pFltr2);
void                FilteringRMapSortByIndexRip6 (tFilteringRMap * ppFiltr[]);

#define MAX_FILTER_AMOUNT 3

/****************************************************************************
 Function    :  GetFlterTypeByFlterPtrRip6
 Input       :  pFltr1 filter to search
 Output      :  none
 Returns     :  positive fisrt bigger, negative second one bigger
****************************************************************************/
INT4
GetFlterTypeByFlterPtrRip6 (tFilteringRMap * pFltr1)
{
    if (gpDistanceFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTANCE;
    }
    if (gpDistributeInFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_IN;
    }
    if (gpDistributeOutFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_OUT;
    }
    return 0;
}

/****************************************************************************
 Function    :  FilteringRMapCompareRip6
 Input       :  compare filters by OID names
 Output      :  none
 Returns     :  positive fisrt bigger, negative second one bigger
****************************************************************************/
INT4
FilteringRMapCompareRip6 (tFilteringRMap * pFltr1, tFilteringRMap * pFltr2)
{
#define MAX_VALUE 256
#define EQ_VALUE 0

    INT4                i4CmpResult = EQ_VALUE;

    if ((NULL == pFltr1) && (NULL == pFltr2))
    {
        return EQ_VALUE;
    }
    if (NULL == pFltr1)
    {
        return MAX_VALUE;
    }
    if (NULL == pFltr2)
    {
        return -MAX_VALUE;
    }

    i4CmpResult =
        STRLEN (pFltr1->au1DistInOutFilterRMapName) -
        STRLEN (pFltr2->au1DistInOutFilterRMapName);
    if (i4CmpResult != 0)
    {
        return i4CmpResult;
    }
    i4CmpResult =
        STRCMP (pFltr1->au1DistInOutFilterRMapName,
                pFltr2->au1DistInOutFilterRMapName);
    if (i4CmpResult != 0)
    {
        return i4CmpResult;
    }

    if (gpDistributeOutFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_OUT;
    }
    if (gpDistributeOutFilterRMap == pFltr2)
    {
        return -FILTERING_TYPE_DISTRIB_OUT;
    }

    if (gpDistributeInFilterRMap == pFltr1)
    {
        return FILTERING_TYPE_DISTRIB_IN;
    }
    if (gpDistributeInFilterRMap == pFltr2)
    {
        return -FILTERING_TYPE_DISTRIB_IN;
    }

    if (gpDistanceFilterRMap == pFltr1)
    {
        return -FILTERING_TYPE_DISTANCE;
    }

    return i4CmpResult;
}

/****************************************************************************
 Function    :  FilteringRMapSortByIndexRip6
 Input       :  tFilteringRMap pointer to array of 3 tFilteringRMap elements
 Output      :  sorted array
 Returns     :  none
****************************************************************************/
void
FilteringRMapSortByIndexRip6 (tFilteringRMap * ppFiltr[])
{
    BOOL1               b1UpdatedArray = FALSE;
    tFilteringRMap     *pExFilteringRMap = NULL;
    INT4                i4Idx;

    ppFiltr[0] = gpDistanceFilterRMap;
    ppFiltr[1] = gpDistributeInFilterRMap;
    ppFiltr[2] = gpDistributeOutFilterRMap;

    do
    {
        b1UpdatedArray = FALSE;
        for (i4Idx = 0; i4Idx < MAX_FILTER_AMOUNT - 1; i4Idx++)
            if (FilteringRMapCompareRip6 (ppFiltr[i4Idx], ppFiltr[i4Idx + 1]) >
                0)
            {
                pExFilteringRMap = ppFiltr[i4Idx + 1];
                ppFiltr[i4Idx + 1] = ppFiltr[i4Idx];
                ppFiltr[i4Idx] = pExFilteringRMap;
                b1UpdatedArray = TRUE;
            }
    }
    while (TRUE == b1UpdatedArray);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsRip6DistInOutRouteMapTable
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsRip6DistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                              pFsRip6DistInOutRouteMapName,
                                              INT4
                                              *pi4FsRip6DistInOutRouteMapType)
{
    tFilteringRMap     *FilteringRMapArray[3];
    FilteringRMapSortByIndexRip6 (FilteringRMapArray);

    if (NULL != FilteringRMapArray[0])
    {
        *pi4FsRip6DistInOutRouteMapType =
            GetFlterTypeByFlterPtrRip6 (FilteringRMapArray[0]);

        pFsRip6DistInOutRouteMapName->i4_Length =
            STRLEN (FilteringRMapArray[0]->au1DistInOutFilterRMapName);
        MEMCPY (pFsRip6DistInOutRouteMapName->pu1_OctetList,
                FilteringRMapArray[0]->au1DistInOutFilterRMapName,
                pFsRip6DistInOutRouteMapName->i4_Length);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsRip6DistInOutRouteMapTable
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                nextFsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType
                nextFsRip6DistInOutRouteMapType
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsRip6DistInOutRouteMapTable (tSNMP_OCTET_STRING_TYPE *
                                             pFsRip6DistInOutRouteMapName,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pNextFsRip6DistInOutRouteMapName,
                                             INT4 i4FsRip6DistInOutRouteMapType,
                                             INT4
                                             *pi4NextFsRip6DistInOutRouteMapType)
{
    INT4                i4Idx;
    tFilteringRMap     *FilteringRMapArray[3];
    FilteringRMapSortByIndexRip6 (FilteringRMapArray);

    for (i4Idx = 0; i4Idx < MAX_FILTER_AMOUNT - 1; i4Idx++)
    {
        if ((NULL != FilteringRMapArray[i4Idx])
            && (NULL != FilteringRMapArray[i4Idx + 1])
            && (0 ==
                MEMCMP (pFsRip6DistInOutRouteMapName->pu1_OctetList,
                        FilteringRMapArray[i4Idx]->au1DistInOutFilterRMapName,
                        pFsRip6DistInOutRouteMapName->i4_Length))
            && (i4FsRip6DistInOutRouteMapType ==
                GetFlterTypeByFlterPtrRip6 (FilteringRMapArray[i4Idx])))
        {
            pNextFsRip6DistInOutRouteMapName->i4_Length =
                STRLEN (FilteringRMapArray[i4Idx + 1]->
                        au1DistInOutFilterRMapName);
            MEMCPY (pNextFsRip6DistInOutRouteMapName->pu1_OctetList,
                    FilteringRMapArray[i4Idx + 1]->au1DistInOutFilterRMapName,
                    pNextFsRip6DistInOutRouteMapName->i4_Length);
            *pi4NextFsRip6DistInOutRouteMapType =
                GetFlterTypeByFlterPtrRip6 (FilteringRMapArray[i4Idx + 1]);
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRip6DistInOutRouteMapValue
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType

                The Object 
                retValFsRip6DistInOutRouteMapValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip6DistInOutRouteMapValue (tSNMP_OCTET_STRING_TYPE *
                                    pFsRip6DistInOutRouteMapName,
                                    INT4 i4FsRip6DistInOutRouteMapType,
                                    INT4 *pi4RetValFsRip6DistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    if (pFsRip6DistInOutRouteMapName == NULL
        || pFsRip6DistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }
    if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE
        && CmpFilterRMapName (gpDistanceFilterRMap,
                              pFsRip6DistInOutRouteMapName) == 0)
    {
        *pi4RetValFsRip6DistInOutRouteMapValue =
            gpDistanceFilterRMap->u1RMapDistance;
    }
    else
    {
        *pi4RetValFsRip6DistInOutRouteMapValue = 0;
    }
#else
    UNUSED_PARAM (pFsRip6DistInOutRouteMapName);
    UNUSED_PARAM (i4FsRip6DistInOutRouteMapType);
    UNUSED_PARAM (pi4RetValFsRip6DistInOutRouteMapValue);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip6DistInOutRouteMapRowStatus
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType

                The Object 
                retValFsRip6DistInOutRouteMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip6DistInOutRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE *
                                        pFsRip6DistInOutRouteMapName,
                                        INT4 i4FsRip6DistInOutRouteMapType,
                                        INT4
                                        *pi4RetValFsRip6DistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    if (pFsRip6DistInOutRouteMapName == NULL
        || pFsRip6DistInOutRouteMapName->i4_Length <= 0)
    {
        return SNMP_FAILURE;
    }
    if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName
            (gpDistanceFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            *pi4RetValFsRip6DistInOutRouteMapRowStatus =
                gpDistanceFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName
            (gpDistributeInFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            *pi4RetValFsRip6DistInOutRouteMapRowStatus =
                gpDistributeInFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_OUT)
    {
        if (CmpFilterRMapName
            (gpDistributeOutFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            *pi4RetValFsRip6DistInOutRouteMapRowStatus =
                gpDistributeOutFilterRMap->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
#else
    UNUSED_PARAM (pFsRip6DistInOutRouteMapName);
    UNUSED_PARAM (i4FsRip6DistInOutRouteMapType);
    UNUSED_PARAM (pi4RetValFsRip6DistInOutRouteMapRowStatus);
    return SNMP_SUCCESS;
#endif
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRip6DistInOutRouteMapValue
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType

                The Object 
                setValFsRip6DistInOutRouteMapValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip6DistInOutRouteMapValue (tSNMP_OCTET_STRING_TYPE *
                                    pFsRip6DistInOutRouteMapName,
                                    INT4 i4FsRip6DistInOutRouteMapType,
                                    INT4 i4SetValFsRip6DistInOutRouteMapValue)
{
    UNUSED_PARAM (pFsRip6DistInOutRouteMapName);
#ifdef ROUTEMAP_WANTED
    if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        gpDistanceFilterRMap->u1RMapDistance =
            i4SetValFsRip6DistInOutRouteMapValue;
    }
#else
    UNUSED_PARAM (i4FsRip6DistInOutRouteMapType);
    UNUSED_PARAM (i4SetValFsRip6DistInOutRouteMapValue);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip6DistInOutRouteMapRowStatus
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType

                The Object 
                setValFsRip6DistInOutRouteMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip6DistInOutRouteMapRowStatus (tSNMP_OCTET_STRING_TYPE *
                                        pFsRip6DistInOutRouteMapName,
                                        INT4 i4FsRip6DistInOutRouteMapType,
                                        INT4
                                        i4SetValFsRip6DistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED
    switch (i4SetValFsRip6DistInOutRouteMapRowStatus)
    {
        case CREATE_AND_WAIT:
        {
            tFilteringRMap      Filter;
            UINT4               u4Status = FILTERNIG_STAT_DEFAULT;

            MEMSET (&Filter, 0, sizeof (tFilteringRMap));
            Filter.u1RowStatus = i4SetValFsRip6DistInOutRouteMapRowStatus;
            MEMCPY (Filter.au1DistInOutFilterRMapName,
                    pFsRip6DistInOutRouteMapName->pu1_OctetList,
                    pFsRip6DistInOutRouteMapName->i4_Length);

            u4Status =
                RMapGetInitialMapStatus (pFsRip6DistInOutRouteMapName->
                                         pu1_OctetList);
            if (u4Status != 0)
            {
                Filter.u1Status = FILTERNIG_STAT_ENABLE;
            }
            else
            {
                Filter.u1Status = FILTERNIG_STAT_DISABLE;
            }

            if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
            {
                gpDistanceFilterRMap = gaDistanceFiltName;
                MEMSET (gpDistanceFilterRMap, 0, sizeof (tFilteringRMap));
                MEMCPY (gpDistanceFilterRMap, &(Filter),
                        sizeof (tFilteringRMap));

            }
            else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
            {
                gpDistributeInFilterRMap = gaDistributeInFiltName;
                MEMSET (gpDistributeInFilterRMap, 0, sizeof (tFilteringRMap));
                MEMCPY (gpDistributeInFilterRMap, &(Filter),
                        sizeof (tFilteringRMap));

            }
            else if (i4FsRip6DistInOutRouteMapType ==
                     FILTERING_TYPE_DISTRIB_OUT)
            {
                gpDistributeOutFilterRMap = gaDistributeOutFiltName;
                MEMSET (gpDistributeOutFilterRMap, 0, sizeof (tFilteringRMap));
                MEMCPY (gpDistributeOutFilterRMap, &(Filter),
                        sizeof (tFilteringRMap));

            }

        }
            break;
        case DESTROY:
            if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
            {
                gpDistanceFilterRMap = NULL;
            }
            else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
            {
                gpDistributeInFilterRMap = NULL;
            }
            else if (i4FsRip6DistInOutRouteMapType ==
                     FILTERING_TYPE_DISTRIB_OUT)
            {
                gpDistributeOutFilterRMap = NULL;
            }
            break;
        default:
            if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
            {
                gpDistanceFilterRMap->u1RowStatus =
                    i4SetValFsRip6DistInOutRouteMapRowStatus;
            }
            else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
            {
                gpDistributeInFilterRMap->u1RowStatus =
                    i4SetValFsRip6DistInOutRouteMapRowStatus;
            }
            else if (i4FsRip6DistInOutRouteMapType ==
                     FILTERING_TYPE_DISTRIB_OUT)
            {
                gpDistributeOutFilterRMap->u1RowStatus =
                    i4SetValFsRip6DistInOutRouteMapRowStatus;
            }
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pFsRip6DistInOutRouteMapName);
    UNUSED_PARAM (i4FsRip6DistInOutRouteMapType);
    UNUSED_PARAM (i4SetValFsRip6DistInOutRouteMapRowStatus);
    return SNMP_FAILURE;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRip6DistInOutRouteMapValue
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType

                The Object 
                testValFsRip6DistInOutRouteMapValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip6DistInOutRouteMapValue (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsRip6DistInOutRouteMapName,
                                       INT4 i4FsRip6DistInOutRouteMapType,
                                       INT4
                                       i4TestValFsRip6DistInOutRouteMapValue)
{
#ifdef ROUTEMAP_WANTED
    if (i4TestValFsRip6DistInOutRouteMapValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (pFsRip6DistInOutRouteMapName == NULL
        || pFsRip6DistInOutRouteMapName->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)
    {
        if (CmpFilterRMapName
            (gpDistanceFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (CmpFilterRMapName
            (gpDistributeInFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_OUT)
    {
        if (CmpFilterRMapName
            (gpDistributeOutFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
        {
            return SNMP_SUCCESS;
        }
    }
    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsRip6DistInOutRouteMapName);
    UNUSED_PARAM (i4FsRip6DistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFsRip6DistInOutRouteMapValue);
#endif
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsRip6DistInOutRouteMapRowStatus
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType

                The Object 
                testValFsRip6DistInOutRouteMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip6DistInOutRouteMapRowStatus (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsRip6DistInOutRouteMapName,
                                           INT4 i4FsRip6DistInOutRouteMapType,
                                           INT4
                                           i4TestValFsRip6DistInOutRouteMapRowStatus)
{
#ifdef ROUTEMAP_WANTED

    INT1                i1Exists = 0;
    INT1                i1Match = 0;
    if (pFsRip6DistInOutRouteMapName == NULL
        || pFsRip6DistInOutRouteMapName->i4_Length <= 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTANCE)

    {
        if (gpDistanceFilterRMap != NULL)
        {
            i1Exists = 1;
            if (CmpFilterRMapName
                (gpDistanceFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_IN)
    {
        if (gpDistributeInFilterRMap != NULL)
        {
            i1Exists = 1;
            if (CmpFilterRMapName
                (gpDistributeInFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else if (i4FsRip6DistInOutRouteMapType == FILTERING_TYPE_DISTRIB_OUT)
    {
        if (gpDistributeOutFilterRMap != NULL)
        {
            i1Exists = 1;
            if (CmpFilterRMapName
                (gpDistributeOutFilterRMap, pFsRip6DistInOutRouteMapName) == 0)
            {
                i1Match = 1;
            }
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsRip6DistInOutRouteMapRowStatus)
    {
        case ACTIVE:
        case DESTROY:
            if (i1Match)
            {
                return SNMP_SUCCESS;
            }
            else if (i1Exists)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            }
            else
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            }
            break;
        case CREATE_AND_WAIT:
            if (!i1Exists)
            {
                return SNMP_SUCCESS;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pFsRip6DistInOutRouteMapName);
    UNUSED_PARAM (i4FsRip6DistInOutRouteMapType);
    UNUSED_PARAM (i4TestValFsRip6DistInOutRouteMapRowStatus);
#endif
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRip6DistInOutRouteMapTable
 Input       :  The Indices
                FsRip6DistInOutRouteMapName
                FsRip6DistInOutRouteMapType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip6DistInOutRouteMapTable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip6PreferenceValue
 Input       :  The Indices

                The Object 
                retValFsRip6PreferenceValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip6PreferenceValue (INT4 *pi4RetValFsRip6PreferenceValue)
{
    *pi4RetValFsRip6PreferenceValue = gu1Rip6Distance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip6PreferenceValue
 Input       :  The Indices

                The Object 
                setValFsRip6PreferenceValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip6PreferenceValue (INT4 i4SetValFsRip6PreferenceValue)
{
    if (i4SetValFsRip6PreferenceValue == 0)
    {
        gu1Rip6Distance = IP6_PREFERENCE_RIP;
    }
    else
    {
        gu1Rip6Distance = i4SetValFsRip6PreferenceValue;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip6PreferenceValue
 Input       :  The Indices

                The Object 
                testValFsRip6PreferenceValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip6PreferenceValue (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsRip6PreferenceValue)
{
    if (i4TestValFsRip6PreferenceValue & 0xFFFFFF00)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip6PreferenceValue
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip6PreferenceValue (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsRip6TestBulkUpd
 Input       :  The Indices

                The Object 
                retValFsRip6TestBulkUpd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip6TestBulkUpd (INT4 *pi4RetValFsRip6TestBulkUpd)
{
    *pi4RetValFsRip6TestBulkUpd = gi4Rip6BulkUpdTestStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsRip6TestDynamicUpd
 Input       :  The Indices

                The Object 
                retValFsRip6TestDynamicUpd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsRip6TestDynamicUpd (INT4 *pi4RetValFsRip6TestDynamicUpd)
{
    *pi4RetValFsRip6TestDynamicUpd = gi4Rip6DynUpdTestStatus;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsRip6TestBulkUpd
 Input       :  The Indices

                The Object 
                setValFsRip6TestBulkUpd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip6TestBulkUpd (INT4 i4SetValFsRip6TestBulkUpd)
{
    gi4Rip6BulkUpdTestStatus = i4SetValFsRip6TestBulkUpd;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsRip6TestDynamicUpd
 Input       :  The Indices

                The Object 
                setValFsRip6TestDynamicUpd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsRip6TestDynamicUpd (INT4 i4SetValFsRip6TestDynamicUpd)
{
    gi4Rip6DynUpdTestStatus = i4SetValFsRip6TestDynamicUpd;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsRip6TestBulkUpd
 Input       :  The Indices

                The Object 
                testValFsRip6TestBulkUpd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip6TestBulkUpd (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsRip6TestBulkUpd)
{
    if ((i4TestValFsRip6TestBulkUpd != RIP6_INITIALIZE_ONE) &&
        (i4TestValFsRip6TestBulkUpd != RIP6_INITIALIZE_TWO))
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsRip6TestDynamicUpd
 Input       :  The Indices

                The Object 
                testValFsRip6TestDynamicUpd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsRip6TestDynamicUpd (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsRip6TestDynamicUpd)
{
    if ((i4TestValFsRip6TestDynamicUpd != RIP6_INITIALIZE_ONE) &&
        (i4TestValFsRip6TestDynamicUpd != RIP6_INITIALIZE_TWO))
    {
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (pu4ErrorCode);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsRip6TestBulkUpd
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip6TestBulkUpd (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsRip6TestDynamicUpd
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsRip6TestDynamicUpd (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
