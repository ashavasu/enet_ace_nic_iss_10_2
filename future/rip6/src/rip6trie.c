
/* $Id: rip6trie.c,v 1.14.2.1 2018/03/23 13:23:02 siva Exp $*/
#include "rip6inc.h"

struct TrieAppFns   Rip6TrieLibFns = {
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams, VOID **ppAppPtr,
              VOID *pAppSpecInfo)) Rip6TriecbAddEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr, VOID *pOutputParams,
              VOID *pNxtHop, tKey Key)) Rip6TriecbDeleteEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr)) Rip6TrieLookupExactEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams, VOID *pAppPtr,
              UINT2 u2KeySize, tKey Key)) NULL,
    (VOID *(*)(tInputParams * pInputParams, void (*AppSpecScanFunc) (VOID *),
               VOID *pScanOutParams)) Rip6TrieCbInitScanCtx,
    (VOID (*)(VOID *pScanCtx)) Rip6TrieCbDeInitScanCtx,
    (INT4 (*)(VOID *pScanCtx, VOID **pAppPtr, tKey Key)) Rip6TrieCbScan,
    (INT4 (*)(VOID *pScanCtx, VOID *pAppPtr, tKey Key)) NULL,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams, VOID **ppAppPtr,
              void *pAppSpecInfo, UINT4 u4NewMetric)) Rip6TriecbUpdate,
    (VOID *(*)(tInputParams * pInputParams, void (*AppSpecDelFunc) (VOID *),
               VOID *pDeleteOutParams)) Rip6TrieDelCb,
    (VOID (*)(VOID *pDelCtx)) Rip6TrieStopTimer,
    (INT4 (*)(VOID *pDelCtx, VOID **ppAppPtr, tKey Key)) NULL,
    (INT4 (*)(tInputParams * pInParms, VOID *pOutParms)) NULL,
    (INT4 (*)(UINT2 u2KeySize, tInputParams * pInputParams,
              VOID *pOutputParams, VOID *pAppPtr, tKey Key))
        Rip6TriecbBestEntry,
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams, VOID *pAppPtr,
              tKey Key)) NULL,
    (INT4 (*)
     (tInputParams * pInputParams, VOID *pOutputParams, VOID *AppSpecPtr,
      UINT2 u2KeySize, tKey Key)) NULL
};

extern UINT4        gu4HoldOnFlag;

/*****************************************************************************/
/* Function Name : Rip6TrieCreate                                            */
/* Description   : This function calls TRIE specific API to create TRIE      */
/* Input(s)      : pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be added            */
/* Output(s)     : None.                                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/
INT4
Rip6TrieCreate (UINT4 u4Instance)
{
    tTrieCrtParams      CreateParams;
    UINT4               u4MaxRts = 0;

    CreateParams.u2KeySize = RIP6_IP6ADDR_LEN;
    CreateParams.u4Type = u4Instance + 1;
    CreateParams.u1AppId = u4Instance + 1;
    u4MaxRts =
        FsRIP6SizingParams[MAX_RIP6_ROUTE_ENTRIES_SIZING_ID].
        u4PreAllocatedUnits;
    CreateParams.u4NumRadixNodes = u4MaxRts;
    CreateParams.u4NumLeafNodes = u4MaxRts;
    CreateParams.u4NoofRoutes = u4MaxRts;
    CreateParams.AppFns = &(Rip6TrieLibFns);
    CreateParams.bPoolPerInst = OSIX_FALSE;
    CreateParams.bSemPerInst = OSIX_FALSE;
    CreateParams.bValidateType = OSIX_FALSE;
    garip6InstanceDatabase[u4Instance]->prip6RtTable =
        TrieCreateInstance (&(CreateParams));
    if (garip6InstanceDatabase[u4Instance]->prip6RtTable == NULL)
    {
        return RIP6_FAILURE;
    }

    return RIP6_SUCCESS;
}

VOID
Rip6TrieStopTimer (tDeleteOutParams * pDelParams)
{
    UINT4               u4InstId;
    UINT4               u4Loop;

    u4InstId = PTR_TO_U4 (pDelParams->pAppSpecInfo);

    Rip6TmrStop (RIP6_Regular_TIMER_ID, gRip6TimerListId,
                 &(garip6InstanceDatabase[u4InstId]->Rip6RtGlobal.timer));

    Rip6TmrStop (RIP6_HOLD_ON_TIMER_ID, gRip6TimerListId,
                 &(garip6InstanceDatabase[u4InstId]->Rip6RtHold.HoldTimer));

    Rip6TmrStop (RIP6_1S_TIMER_ID, gRip6RedisTmrListId,
                 &(garip6InstanceDatabase[u4InstId]->Rip6RrdGlobal.timer));

    for (u4Loop = RIP6_INITIALIZE_ZERO;
         u4Loop < pDelParams->u4NumEntries; u4Loop++)
    {
        if (RIP6_GET_INST_IF_MAP_ENTRY (u4Loop)->u4InstanceId == u4InstId)
        {
            Rip6InterfaceInstanceDetach (u4Loop);
        }
    }

}

VOID
Rip6TrieIfDelete (VOID *pParams)
{
    tDeleteOutParams   *pDelParams = (tDeleteOutParams *) pParams;
    UINT4               u4Loop1;
    UINT4               u4InstId;
    UINT4               u4MaxProfs = 0;

    u4MaxProfs = RIP6_MIN (MAX_RIP6_PROFILES_LIMIT,
                           FsRIP6SizingParams[MAX_RIP6_PROFILES_SIZING_ID].
                           u4PreAllocatedUnits);

    u4InstId = PTR_TO_U4 (pDelParams->pAppSpecInfo);

    /* Profile table deletion for the instance */
    for (u4Loop1 = RIP6_INITIALIZE_ZERO; u4Loop1 < u4MaxProfs; u4Loop1++)
    {
        if (MemReleaseMemBlock ((tMemPoolId) i4Rip6ProfilePoolId,
                                (UINT1 *) garip6InstanceDatabase[u4InstId]->
                                arip6Profile[u4Loop1]) != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                          "RIP6 : rip6 static add, could not release mem to pool \n");
            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                           RIP6_INITIALIZE_ZERO, i4Rip6rtId);
        }
    }
}

VOID               *
Rip6TrieDelCb (tInputParams * pInparams, VOID (*AppSpecDelFunc) (VOID *),
               tDeleteOutParams * pDelParams)
{
    UNUSED_PARAM (pInparams);
    (*AppSpecDelFunc) (pDelParams);

    return (VOID *) pDelParams;
}

/*****************************************************************************/
/* Function Name : Rip6TrieDelete                                            */
/* Description   : This function calls TRIE specific API to Delete TRIE      */
/* Input(s)      : u4InstanceId - The instance id of the RIP                 */
/*                                                                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/

INT4
Rip6TrieDelete (UINT4 u4InstanceId)
{
    tInputParams        Inparams;
    tDeleteOutParams    DelParams;
    INT4                i4Status = 0;

    MEMSET (&Inparams, 0, sizeof (tInputParams));
    MEMSET (&DelParams, 0, sizeof (tDeleteOutParams));

    if (garip6InstanceDatabase[u4InstanceId]->prip6RtTable == NULL)
    {
        return RIP6_FAILURE;
    }

    Inparams.pRoot = garip6InstanceDatabase[u4InstanceId]->prip6RtTable;
    Inparams.i1AppId = (INT1) (u4InstanceId + 1);

    DelParams.pAppSpecInfo = (void *) ((FS_ULONG) u4InstanceId);
    DelParams.u4NumEntries =
        FsRIP6SizingParams[MAX_RIP6_IFACES_SIZING_ID].u4PreAllocatedUnits;

    i4Status = TrieDel (&Inparams, Rip6TrieIfDelete, (VOID *) &DelParams);

    return ((i4Status == TRIE_SUCCESS) ? RIP6_SUCCESS : RIP6_FAILURE);
}

/*****************************************************************************/
/* Function Name : Rip6TrieBestEntry                         */
/* Description   : This function searches for a route profile                */
/*                 entry in RIB (Best Match)                                 */
/* Description   : This function calls TRIE specific API to search a route   */
/*                 profile entry from RIB                                    */
/* Input(s)      :                                                           */
/*                                                                           */
/* Output(s)     : None.                                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/

INT4
Rip6TrieBestEntry (tIp6Addr * pLookUpAddr, UINT1 u1Preference,
                   UINT1 u1PrefixLen, UINT4 u4InstanceId,
                   tRip6RtEntry ** ppRtEntry, VOID **ppNode)
{

    tInputParams        Inparams;
    tOutputParams       Outparams;
    UINT1               u1Key[RIP6_IP6ADDR_LEN];
    tIp6Addr            AddrMask;

    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    MEMSET (&Inparams, 0, sizeof (tInputParams));
    MEMSET (&Outparams, 0, sizeof (tOutputParams));

    Inparams.pRoot = garip6InstanceDatabase[u4InstanceId]->prip6RtTable;
    Inparams.i1AppId = (INT1) (u4InstanceId + 1);
    Inparams.u1Reserved1 = u1Preference;
    Inparams.u1PrefixLen = u1PrefixLen;

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) u1Key, pLookUpAddr, u1PrefixLen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (u1Key + IP6_ADDR_SIZE), &AddrMask,
                     u1PrefixLen);

    Inparams.Key.pKey = u1Key;

    if (TrieSearch (&Inparams, &Outparams, ppNode) == TRIE_FAILURE)
    {
        return RIP6_FAILURE;
    }

    *ppRtEntry = Outparams.pAppSpecInfo;
    return RIP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rip6TrieLookupExactEntry                                  */
/* Description   : This is a call back function to search for a route profile*/
/*                 entry in RIB (Exact Match)                                */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pOutputParams - void pointer (used by RIP)                */
/*                 ppAppSpecInfo - address of the Application (RIP) specific */
/*                                 info present in TRIE leaf node            */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/

INT4
Rip6TrieLookupExactEntry (tInputParams * pInputParams, tRip6TrieOutParams *
                          pOutParams, tRip6RtEntry * pAppSpecInfo)
{

    UINT1               u1NonPreferredRoute = 0, u1_none = 0;
    tRip6RtEntry       *pCurrEntry = NULL, *p_best_entry = NULL;
    tRip6RtEntry       *pPrevEntry = NULL;
    UINT4               u4InstanceId = pInputParams->i1AppId - 1;

    switch (pInputParams->u1Reserved1)
    {
        case RIP6_DYNAMIC_LOOKUP:
            for (pCurrEntry = pAppSpecInfo; pCurrEntry != NULL;
                 pCurrEntry = pCurrEntry->pNext)
            {
                if (pCurrEntry->u1Metric == RIP6_INFINITY)
                {
                    if (pCurrEntry->i1Proto == RIPNG)
                    {
                        pOutParams->pRtProfile = pCurrEntry;
                        return TRIE_SUCCESS;
                    }

                    if (pCurrEntry->pNext == NULL)
                    {
                        p_best_entry = pCurrEntry;
                    }
                    u1_none = RIP6_INITIALIZE_ONE;
                    continue;
                }

                u1_none = RIP6_INITIALIZE_ZERO;
                if (pCurrEntry->i1Proto == RIPNG)
                {
                    pOutParams->pRtProfile = pCurrEntry;
                    return TRIE_SUCCESS;
                }

                if (pCurrEntry != NULL)
                {
                    pPrevEntry = pCurrEntry;
                }
            }
            u1NonPreferredRoute = RIP6_INITIALIZE_ONE;
            break;

        case RIP6_STATIC_LOOKUP:
            for (pCurrEntry = pAppSpecInfo; pCurrEntry != NULL;
                 pCurrEntry = pCurrEntry->pNext)
            {
                if (pCurrEntry->u1Metric == RIP6_INFINITY)
                {
                    if (pCurrEntry != NULL)
                    {
                        pPrevEntry = pCurrEntry;
                    }
                    u1_none = RIP6_INITIALIZE_ONE;
                    continue;
                }

                u1_none = RIP6_INITIALIZE_ZERO;
                if (pCurrEntry->i1Proto != RIPNG)
                {
                    pOutParams->pRtProfile = pCurrEntry;
                    return TRIE_SUCCESS;
                }

                if (pCurrEntry != NULL)
                {
                    pPrevEntry = pCurrEntry;
                }
            }
            u1NonPreferredRoute = RIP6_INITIALIZE_ONE;
            break;

        case RIP6_BEST_METRIC_LOOKUP:
            pCurrEntry = pAppSpecInfo;
            p_best_entry = NULL;

            while (pCurrEntry != NULL)
            {
                if (!(RIP6_GET_INST_IF_ENTRY (u4InstanceId,
                                              pCurrEntry->u4Index)->
                      u1Status & RIP6_IFACE_UP))
                {
                    u1_none = RIP6_INITIALIZE_ONE;
                    pCurrEntry = pCurrEntry->pNext;
                    continue;
                }

                if (pCurrEntry->u1Metric == RIP6_INFINITY)
                {
                    u1_none = RIP6_INITIALIZE_ONE;
                    if ((!p_best_entry) ||
                        (p_best_entry->u1Metric > pCurrEntry->u1Metric))
                    {
                        p_best_entry = pCurrEntry;
                    }
                    pCurrEntry = pCurrEntry->pNext;
                    continue;
                }

                u1_none = RIP6_INITIALIZE_ZERO;
                if ((!p_best_entry) ||
                    (p_best_entry->u1Metric > pCurrEntry->u1Metric))
                {
                    p_best_entry = pCurrEntry;
                }

                pCurrEntry = pCurrEntry->pNext;

            }

            if (p_best_entry == NULL)
            {
                /* No Best route Present */
                pOutParams->pRtProfile = NULL;
                return TRIE_FAILURE;
            }
            else
            {
                pOutParams->pRtProfile = p_best_entry;
                return TRIE_SUCCESS;
            }

        default:
            break;
    }
    if ((u1NonPreferredRoute != RIP6_FALSE) && (u1_none == RIP6_FALSE))
    {
        pOutParams->pRtProfile = pPrevEntry;
        return TRIE_SUCCESS;
    }

    pOutParams->pRtProfile = NULL;
    return TRIE_FAILURE;
}

/*****************************************************************************/
/* Function Name : Rip6TriecbBestEntry                                       */
/* Description   : This is a call back function to search for a route profile*/
/*                 entry in RIB (Best Match)                                 */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pOutputParams - void pointer (used by RIP)                */
/*                 ppAppSpecInfo - address of the Application (RIP) specific */
/*                                 info present in TRIE leaf node            */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/

INT4
Rip6TriecbBestEntry (UINT2 u2KeySize, tInputParams * pInputParams,
                     tRip6TrieOutParams * pOutParams,
                     tRip6RtEntry * pAppSpecInfo, tKey Key)
{

    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);

    if (pInputParams->u1PrefixLen != pAppSpecInfo->u1Prefixlen)
    {
        return TRIE_FAILURE;
    }
    return (Rip6TrieLookupExactEntry (pInputParams, pOutParams, pAppSpecInfo));
}

/*****************************************************************************/
/* Function Name : Rip6TrieAddEntry                                          */
/* Description   : This function calls TRIE specific API to add a route      */
/*                 profile entry into RIB                                    */
/* Input(s)      : pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be added            */
/* Output(s)     : None.                                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/

INT4
Rip6TrieAddEntry (tRip6RtEntry * pRtEntry, UINT4 u4InstanceId)
{
    tInputParams        Inparams;
    tOutputParams       Outparams;
    UINT1               u1Key[RIP6_IP6ADDR_LEN];
    tIp6Addr            AddrMask;

    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    Inparams.pRoot = garip6InstanceDatabase[u4InstanceId]->prip6RtTable;
    Inparams.i1AppId = (INT1) (u4InstanceId + 1);
    Inparams.u1PrefixLen = pRtEntry->u1Prefixlen;

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) u1Key,
                     &pRtEntry->dst, pRtEntry->u1Prefixlen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (u1Key + IP6_ADDR_SIZE), &AddrMask,
                     pRtEntry->u1Prefixlen);
    Inparams.Key.pKey = u1Key;

    if (TrieAdd (&Inparams, pRtEntry, &Outparams) == TRIE_FAILURE)
    {
        return RIP6_FAILURE;
    }

    ++pRtEntry->i1Refcnt;

    if (pRtEntry->i1Proto == RIPNG)
    {
        pRtEntry->u1Operation = RIP6HA_ADD_ROUTE;
#ifdef ISS_TEST_WANTED
        if (gi4Rip6DynUpdTestStatus == RIP6_INITIALIZE_TWO)
        {
            /* Skipping the dynamic update for HA test purpose */
            RIP6_TRC_ARG (RIP6_MOD_TRC, RIP6_ALL_FAIL_TRC, RIP6_NAME,
                          "Rip6TrieAddEntry:Skipping the dynamic update \r\n");
            return RIP6_SUCCESS;
        }
#endif
        Rip6RedDbUtilAddTblNode (&gRip6DynInfoList, &pRtEntry->RouteDbNode);
        Rip6RedSyncDynInfo ();
    }

    ++(garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes);
    return RIP6_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rip6TrieCbAddEntry                                        */
/* Description   : This is a call back function to add a route profile entry */
/*                 into RIB                                                  */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pOutputParams - void pointer (used by RIP6)                */
/*                 ppAppSpecInfo - address of the Application (RIP6) specific */
/*                                 info present in TRIE leaf node            */
/*                 pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be added            */
/* Output(s)     : ppAppSpecInfo - address of the updated application        */
/*                                 specific info pointer                     */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rip6TriecbAddEntry (VOID *pInputParams, VOID *pOutputParams,
                    tRip6RtEntry ** ppAppSpecInfo,
                    tRip6RtEntry * pNewAppSpecInfo)
{
    tRip6RtEntry       *pPrevRoute = NULL;

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);

    pNewAppSpecInfo->pNext = NULL;

    if ((*ppAppSpecInfo) == NULL)
    {
        (*ppAppSpecInfo) = pNewAppSpecInfo;
    }
    else
    {
        /* Add the new route to the end. */
        pPrevRoute = *ppAppSpecInfo;
        while (pPrevRoute->pNext != NULL)
        {
            pPrevRoute = pPrevRoute->pNext;
        }
        pPrevRoute->pNext = pNewAppSpecInfo;
    }

    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rip6TrieDeleteEntry                                       */
/* Description   : This function calls TRIE specific API to delete a route   */
/*                 profile entry from RIB                                    */
/* Input(s)      : pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be deleted          */
/* Output(s)     : None.                                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/

INT4
Rip6TrieDeleteEntry (tRip6RtEntry * pRtEntry, UINT4 u4InstanceId)
{
    tInputParams        Inparams;
    tOutputParams       Outparams;
    UINT1               u1Key[RIP6_IP6ADDR_LEN];
    tIp6Addr            AddrMask;

    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    Inparams.pRoot = garip6InstanceDatabase[u4InstanceId]->prip6RtTable;
    Inparams.i1AppId = (INT1) (u4InstanceId + 1);
    Inparams.u1PrefixLen = pRtEntry->u1Prefixlen;

    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) u1Key, &pRtEntry->dst,
                     pRtEntry->u1Prefixlen);
    Ip6CopyAddrBits ((tIp6Addr *) (VOID *) (u1Key + IP6_ADDR_SIZE), &AddrMask,
                     pRtEntry->u1Prefixlen);

    Inparams.Key.pKey = u1Key;

    if (TrieRemove (&Inparams, &Outparams, pRtEntry) == TRIE_FAILURE)
    {
        return RIP6_FAILURE;
    }

    if (pRtEntry->i1Proto == RIPNG)
    {
        pRtEntry->u1Operation = RIP6HA_DEL_ROUTE;
        Rip6RedDbUtilAddTblNode (&gRip6DynInfoList, &pRtEntry->RouteDbNode);
        Rip6RedSyncDynInfo ();
    }

    --(garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes);

    return RIP6_SUCCESS;

}

/*****************************************************************************/
/* Function Name : Rip6TriecbDeleteEntry                     */
/* Description   : This is a call back function to delete a route profile    */
/*                 entry from RIB                                            */
/*                 (Registered with TRIE library during TRIE creation)       */
/* Input(s)      : pInputParams - void pointer used by TRIE                  */
/*                 pOutputParams - void pointer (used by RIP6)               */
/*                 ppAppSpecInfo - address of the Application (RIP6) specific*/
/*                                 info present in TRIE leaf node            */
/*                 pNewAppSpecInfo - pointer to the new application specific */
/*                                 info that is going to be added            */
/* Output(s)     : ppAppSpecInfo - address of the updated application        */
/*                                 specific info pointer                     */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rip6TriecbDeleteEntry (tInputParams * pInputParams, tRip6RtEntry ** pAppspecPtr,
                       VOID *pOutParams, tRip6RtEntry * pRtEntry, tKey Key)
{
    tRip6RtEntry       *pPrevRoute = NULL;
    tRip6RtEntry       *pRoute = NULL;

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutParams);
    UNUSED_PARAM (Key);

    pRoute = *pAppspecPtr;
    while ((pRoute != NULL) && (pRoute != pRtEntry))
    {
        pPrevRoute = pRoute;
        pRoute = pRoute->pNext;
    }

    /* Update the TRIE Node AppSpec Pointer */
    if (pPrevRoute == NULL)
    {
        /* Route to be deleted is the first route */
        *pAppspecPtr = pRtEntry->pNext;
    }
    else
    {
        pPrevRoute->pNext = pRtEntry->pNext;
    }

    Rip6RtRelease (pRtEntry);
    if (pRtEntry->i1Refcnt == RIP6_INITIALIZE_ZERO)
    {
        if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId, (UINT1 *) pRtEntry)
            != RIP6_SUCCESS)
        {
            RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                          "RIP6 : rip6 static add, could not release mem to pool \n");
            RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                           "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                           ERROR_FATAL_STR, ERR_MEM_RELEASE,
                           RIP6_INITIALIZE_ZERO, i4Rip6rtId);
        }
        pRtEntry = NULL;

    }

    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rip6TrieUpdate                                            */
/* Description   : This function updates the leaf node of the TRIE           */
/*                 This function is called to add the route the node which   */
/*                 already has a route                                       */
/* Input(s)      : pNewRtEntry     - pointer to the new application specific */
/*                                   info that is going to be added          */
/* Output(s)     : None.                                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/

INT4
Rip6TrieUpdate (tRip6RtEntry * pPrevRtEntry, UINT4 u4InstanceId,
                tRip6RtEntry * pNewRtEntry)
{
    tRip6RtEntry       *pTempPrevRtEntry = NULL;
    tRip6RtEntry       *pTempPrevRtEntry1 = NULL;
    UINT1               u1Distance = 120;
    UINT1               u1NewNodeAdded = OSIX_FALSE;

    UNUSED_PARAM (u4InstanceId);
    pNewRtEntry->pNext = NULL;
    pTempPrevRtEntry = pPrevRtEntry;
    while (pTempPrevRtEntry)
    {
        printf ("Inside While pTempPrevRtEntry %p \n", pTempPrevRtEntry);
        pTempPrevRtEntry1 = pTempPrevRtEntry;
        if ((MEMCMP (&pNewRtEntry->dst, &pTempPrevRtEntry->dst, IP6_ADDR_SIZE)
             == 0)
            &&
            (MEMCMP
             (&pNewRtEntry->nexthop, &pTempPrevRtEntry->nexthop,
              IP6_ADDR_SIZE) == 0)
            && (pTempPrevRtEntry->u1Metric == pNewRtEntry->u1Metric))
        {
            /* update is not required. so just refresh timer and return failure,
             * to free up the allocated node earlier
             */
            OsixGetSysTime (&(pTempPrevRtEntry->u4ChangeTime));
            pTempPrevRtEntry->rtEntryTimer.u1Id = RIP6_AGE_OUT_TIMER_ID;
            Rip6TimerStart (pTempPrevRtEntry, RIP6_AGE_OUT_TIMER_ID,
                            RIP6_ROUTE_AGE_INTERVAL (pTempPrevRtEntry));
            return RIP6_FAILURE;
        }
        else if ((MEMCMP
                  (&pNewRtEntry->dst, &pTempPrevRtEntry->dst,
                   IP6_ADDR_SIZE) == 0)
                 &&
                 (MEMCMP
                  (&pNewRtEntry->nexthop, &pTempPrevRtEntry->nexthop,
                   IP6_ADDR_SIZE) == 0)
                 && (pTempPrevRtEntry->u4Index == pNewRtEntry->u4Index)
                 && (pTempPrevRtEntry->u1Metric == RIP6_INFINITY)
                 && (gu4HoldOnFlag == OSIX_FALSE))
        {
            pTempPrevRtEntry->u1Metric = pNewRtEntry->u1Metric;
            Rip6TimerStop (pTempPrevRtEntry);
            pTempPrevRtEntry->rtEntryTimer.u1Id = 0;
            OsixGetSysTime (&(pTempPrevRtEntry->u4ChangeTime));
            pTempPrevRtEntry->rtEntryTimer.u1Id = RIP6_AGE_OUT_TIMER_ID;
            Rip6TimerStart (pTempPrevRtEntry, RIP6_AGE_OUT_TIMER_ID,
                            RIP6_ROUTE_AGE_INTERVAL (pTempPrevRtEntry));
            Rip6SendRtChgNotification (&pTempPrevRtEntry->dst,
                                       pTempPrevRtEntry->u1Prefixlen,
                                       &pTempPrevRtEntry->nexthop,
                                       pTempPrevRtEntry->u1Metric,
                                       pTempPrevRtEntry->i1Proto,
                                       pTempPrevRtEntry->u4Index,
                                       u1Distance, NETIPV6_ADD_ROUTE);
            return RIP6_FAILURE;
        }
        else if ((MEMCMP
                  (&pNewRtEntry->dst, &pTempPrevRtEntry->dst,
                   IP6_ADDR_SIZE) == 0)
                 && (pTempPrevRtEntry->u1Metric > pNewRtEntry->u1Metric)
                 && (pTempPrevRtEntry->u1Metric != RIP6_INFINITY))
        {
            Rip6TimerStop (pTempPrevRtEntry);
            Rip6SendRtChgNotification (&pTempPrevRtEntry->dst,
                                       pTempPrevRtEntry->u1Prefixlen,
                                       &pTempPrevRtEntry->nexthop,
                                       pTempPrevRtEntry->u1Metric,
                                       pTempPrevRtEntry->i1Proto,
                                       pTempPrevRtEntry->u4Index,
                                       u1Distance, NETIPV6_DELETE_ROUTE);
            Rip6TrieDeleteEntry (pTempPrevRtEntry, RIP6_DEFAULT_INSTANCE);
        }

        pTempPrevRtEntry = pTempPrevRtEntry->pNext;
    }

    /*Since pTempPrevRtEntry is always NULL when the control comes here , if check is not required */
    pPrevRtEntry = pTempPrevRtEntry1;

    if ((pPrevRtEntry->pNext == NULL) && (gu4HoldOnFlag == OSIX_FALSE))
    {
        pPrevRtEntry->pNext = pNewRtEntry;
        u1NewNodeAdded = OSIX_TRUE;
    }
    else if (pPrevRtEntry->pNext != NULL)
    {
        /*need to check the duplicate */
        if ((MEMCMP
             (&pNewRtEntry->dst, &pPrevRtEntry->pNext->dst, IP6_ADDR_SIZE) == 0)
            &&
            (MEMCMP
             (&pNewRtEntry->nexthop, &pPrevRtEntry->pNext->nexthop,
              IP6_ADDR_SIZE) == 0)
            && (pPrevRtEntry->pNext->u1Metric == pNewRtEntry->u1Metric))
        {
            /* update is not required. so just refresh timer and return failure,
             * to free up the allocated node earlier
             */
            OsixGetSysTime (&(pPrevRtEntry->pNext->u4ChangeTime));
            pPrevRtEntry->pNext->rtEntryTimer.u1Id = RIP6_AGE_OUT_TIMER_ID;
            Rip6TimerStart (pPrevRtEntry->pNext, RIP6_AGE_OUT_TIMER_ID,
                            RIP6_ROUTE_AGE_INTERVAL (pPrevRtEntry->pNext));
            return RIP6_FAILURE;
        }
        else if ((MEMCMP
                  (&pNewRtEntry->dst, &pPrevRtEntry->pNext->dst,
                   IP6_ADDR_SIZE) == 0)
                 &&
                 (MEMCMP
                  (&pNewRtEntry->nexthop, &pPrevRtEntry->pNext->nexthop,
                   IP6_ADDR_SIZE) == 0)
                 && (pPrevRtEntry->pNext->u1Metric == RIP6_INFINITY)
                 && (gu4HoldOnFlag == OSIX_FALSE))
        {
            pPrevRtEntry->pNext->u1Metric = pNewRtEntry->u1Metric;
            Rip6TimerStop (pPrevRtEntry->pNext);
            pPrevRtEntry->pNext->rtEntryTimer.u1Id = 0;
            OsixGetSysTime (&(pPrevRtEntry->pNext->u4ChangeTime));
            pPrevRtEntry->pNext->rtEntryTimer.u1Id = RIP6_AGE_OUT_TIMER_ID;
            Rip6TimerStart (pPrevRtEntry->pNext, RIP6_AGE_OUT_TIMER_ID,
                            RIP6_ROUTE_AGE_INTERVAL (pPrevRtEntry->pNext));
            Rip6SendRtChgNotification (&pPrevRtEntry->pNext->dst,
                                       pPrevRtEntry->pNext->u1Prefixlen,
                                       &pPrevRtEntry->pNext->nexthop,
                                       pPrevRtEntry->pNext->u1Metric,
                                       pPrevRtEntry->pNext->i1Proto,
                                       pPrevRtEntry->pNext->u4Index,
                                       u1Distance, NETIPV6_ADD_ROUTE);
            return RIP6_FAILURE;
        }
        else if ((MEMCMP
                  (&pNewRtEntry->dst, &pPrevRtEntry->pNext->dst,
                   IP6_ADDR_SIZE) == 0)
                 && (pPrevRtEntry->pNext->u1Metric > pNewRtEntry->u1Metric)
                 && (pPrevRtEntry->pNext->u1Metric != RIP6_INFINITY))
        {
            Rip6TimerStop (pPrevRtEntry->pNext);
            Rip6SendRtChgNotification (&pPrevRtEntry->pNext->dst,
                                       pPrevRtEntry->pNext->u1Prefixlen,
                                       &pPrevRtEntry->pNext->nexthop,
                                       pPrevRtEntry->pNext->u1Metric,
                                       pPrevRtEntry->pNext->i1Proto,
                                       pPrevRtEntry->pNext->u4Index,
                                       u1Distance, NETIPV6_DELETE_ROUTE);
            Rip6TrieDeleteEntry (pPrevRtEntry->pNext, RIP6_DEFAULT_INSTANCE);

        }
        if (gu4HoldOnFlag == OSIX_FALSE)
        {
            pPrevRtEntry->pNext->pNext = pNewRtEntry;
            u1NewNodeAdded = OSIX_TRUE;

        }
    }
    if (u1NewNodeAdded == OSIX_TRUE)
    {
        ++(pNewRtEntry->i1Refcnt);
        ++(garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes);
        return RIP6_SUCCESS;
    }
    else
    {
        return RIP6_FAILURE;
    }

}

INT4
Rip6TriecbUpdate (tInputParams * pInputParams, VOID *pOutParams,
                  tRip6RtEntry ** pLeafEntry, tRip6RtEntry * pAppSpecInfo,
                  UINT4 u4NewMetric)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutParams);
    UNUSED_PARAM (u4NewMetric);

    pAppSpecInfo->pNext = *pLeafEntry;
    *pLeafEntry = pAppSpecInfo;

    return TRIE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Rip6TrieGetFirstEntry                                     */
/* Description   : This function gets the first route from the TRIE of the   */
/*                 given instance.                                           */
/* Input(s)      : u4InstanceId    - Instance id whose first route should be */
/*                                   retrieved.                              */
/* Output(s)     : pRtEntry  -  Pointer to the First route entry or NULL     */
/*                 ppNode    -  Pointer to the Node in which the route is    */
/*                              present.                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/
INT4
Rip6TrieGetFirstEntry (tRip6RtEntry ** pRtEntry, VOID **ppNode,
                       UINT4 u4InstanceId)
{
    tInputParams        Inparams;
    INT4                i4Status;
    VOID               *pTempPtr = NULL;

    MEMSET (&Inparams, 0, sizeof (tInputParams));
    *pRtEntry = NULL;
    Inparams.pRoot = garip6InstanceDatabase[u4InstanceId]->prip6RtTable;
    Inparams.i1AppId = (INT1) (u4InstanceId + 1);

    i4Status = TrieGetFirstNode (&Inparams, &pTempPtr, ppNode);
    *pRtEntry = (tRip6RtEntry *) pTempPtr;
    return ((i4Status == TRIE_FAILURE) ? RIP6_FAILURE : RIP6_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Rip6TrieGetNextEntry                                      */
/* Description   : This function gets the next route from the TRIE of the    */
/*                 given instance.                                           */
/* Input(s)      : u4InstanceId    - Instance id whose first route should be */
/*                                   retrieved.                              */
/*                 pCurrEntry      - Pointer to the route whose next route   */
/*                                   should be retrieved.                    */
/* Output(s)     : pRtEntry  -  Pointer to the Next route entry or NULL      */
/*                 ppNode    -  Pointer to the Node in which the route is    */
/*                              present.                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/
INT4
Rip6TrieGetNextEntry (tRip6RtEntry ** pRtEntry, VOID *pCurrNode, VOID **ppNode,
                      UINT4 u4InstanceId)
{
    tInputParams        Inparams;
    INT4                i4Status;

    VOID               *pTempPtr = NULL;

    MEMSET (&Inparams, 0, sizeof (tInputParams));
    *pRtEntry = NULL;
    Inparams.pRoot = garip6InstanceDatabase[u4InstanceId]->prip6RtTable;
    Inparams.i1AppId = (INT1) (u4InstanceId + 1);

    i4Status = TrieGetNextNode (&Inparams, pCurrNode, &pTempPtr, ppNode);
    *pRtEntry = (tRip6RtEntry *) pTempPtr;
    return ((i4Status == TRIE_FAILURE) ? RIP6_FAILURE : RIP6_SUCCESS);
}

/*****************************************************************************/
/* Function Name : Rip6TrieScan                                              */
/* Description   : This function scans through all the nodes in the trie and */
/*                 will update the application spec pointer as required.     */
/* Input(s)      : pScanParams - Parameter describing operation to be        */
/*                               performed during scanning.                  */
/* Output(s)     : None.                                                     */
/* Return(s)     : RIP6_SUCCESS/RIP6_FAILURE.                                */
/*****************************************************************************/
INT4
Rip6TrieScan (tRip6ScanParam * pScanParams)
{
    tInputParams        Inparams;

    MEMSET (&Inparams, 0, sizeof (tInputParams));
    Inparams.pRoot =
        garip6InstanceDatabase[pScanParams->u4InstanceId]->prip6RtTable;
    Inparams.i1AppId = (INT1) (pScanParams->u4InstanceId + 1);

    if (TrieWalk (&Inparams, NULL, pScanParams) == TRIE_FAILURE)
    {
        /* Failure while scanning the TRIE */
        return RIP6_FAILURE;
    }

    return RIP6_SUCCESS;

}

/*****************************************************************************/
/* Function Name : Rip6TrieCbInitScanCtx                                     */
/* Description   : This function is the callback function by Trie Scan to    */
/*                 initialise the required parameters.                       */
/* Input(s)      : pInputParameter - Input parameter passed                  */
/*                 AppSpecScanFunc - Function pointer to function that can   */
/*                                   be used to process the given value.     */
/* Output(s)     : pScanOutParams - Updated scan parameters that shall be    */
/*                                  used by Trie Scan routine.               */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
VOID               *
Rip6TrieCbInitScanCtx (tInputParams * pInputParams,
                       VOID (*AppSpecScanFunc) (VOID *), VOID *pScanOutParams)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (AppSpecScanFunc);

    return (pScanOutParams);
}

/*****************************************************************************/
/* Function Name : Rip6TrieCbDeInitScanCtx                                   */
/* Description   : This function is the callback function by Trie Scan to    */
/*                 deinitialise the required parameters.                     */
/* Input(s)      : pScanCtx        - Input parameter to be deinitialised     */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
VOID
Rip6TrieCbDeInitScanCtx (VOID *pScanCtx)
{
    UNUSED_PARAM (pScanCtx);
    return;
}

/*****************************************************************************/
/* Function Name : Rip6TrieCbScan                                            */
/* Description   : This function is the callback function by Trie Scan to    */
/*                 process application specific data present in each node.   */
/* Input(s)      : pScanCtx        - Input parameter based on which the      */
/*                                   entries are handled.                    */
/*                 pAppPtr         - Trie's nodes Application Pointer to be  */
/*                                   handled.                                */
/*                 Key             - Trie Node's Key                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : TRIE_SUCCESS/TRIE_FAILURE.                                */
/*****************************************************************************/
INT4
Rip6TrieCbScan (VOID *pScanCtx, VOID **ppAppPtr, tKey Key)
{
    tRip6ScanParam     *pScanParams = (tRip6ScanParam *) pScanCtx;
    tRip6RtEntry       *pRtEntry = *(tRip6RtEntry **) ppAppPtr;
    tRip6RtEntry       *pPrevRoute = NULL;
    tRip6RtEntry       *pBestRt = NULL;
    INT4                i4RouteProtoMask = 0;
    INT4                i4Result = RIP6_FAILURE;

    UINT4               u4InstanceId = 0;
    UNUSED_PARAM (Key);

    if (pScanParams->u1ScanCmd == RIP6_TRIE_SCAN_TRIG_CLEAR)
    {
        /* Need to Reset the Trigger update flag */
        while (pRtEntry)
        {
            pRtEntry->u1RtFlag = 0;
            pRtEntry = pRtEntry->pNext;
        }
    }
    else if (pScanParams->u1ScanCmd == RIP6_TRIE_SCAN_INTF_FINAL)
    {
        /* Get the Best Route. */
        pBestRt = NULL;

        while (pRtEntry)
        {
            if ((pBestRt == NULL) || (pBestRt->u1Metric > pRtEntry->u1Metric))
            {
                pBestRt = pRtEntry;
            }
            pRtEntry = pRtEntry->pNext;
        }

        if (pBestRt == NULL)
        {
            return TRIE_FAILURE;
        }
        if ((pScanParams->u2RespFlag & RIP6_IFACE_FINAL) == RIP6_IFACE_FINAL)
        {
            if (pBestRt->u4Index != pScanParams->u4RecvIndex)
            {
                /* Route is not learnt from the required interface. */
                /* Go to next route. */
                return TRIE_SUCCESS;
            }
        }

        if (IS_ADDR_UNSPECIFIED (pBestRt->dst))
        {
            if (RIP6_GET_INST_IF_ENTRY (pScanParams->u4InstanceId,
                                        pScanParams->u4SendIndex)->
                u4DefRouteAdvt == RIP6_FALSE)
            {
                /* Default route advertisement not enabled. Process
                 * next route. */
                return TRIE_SUCCESS;
            }
        }

        if (Rip6SendRtLearntOverIf (pScanParams->u2RespFlag,
                                    pScanParams->u1Horizon,
                                    pScanParams->u4SendIndex,
                                    pScanParams->u4RecvIndex,
                                    pScanParams->u2DstPort,
                                    pScanParams->pu4RtCounter,
                                    pScanParams->u4MaxNoRte,
                                    pScanParams->pu4Len,
                                    pScanParams->pDst,
                                    pScanParams->ppBuf, pBestRt,
                                    pScanParams->u4InstanceId) != RIP6_SUCCESS)
        {
            return TRIE_FAILURE;
        }
    }
    else if (pScanParams->u1ScanCmd == RIP6_TRIE_SCAN_INTF_ADVT)
    {
        /* Get the Best Route. */
        pBestRt = NULL;

        while (pRtEntry)
        {
            if ((pBestRt == NULL) || (pBestRt->u1Metric > pRtEntry->u1Metric))
            {
                pBestRt = pRtEntry;
            }
            pRtEntry = pRtEntry->pNext;
        }
        if (pBestRt == NULL)
        {
            return TRIE_FAILURE;
        }

        if (IS_ADDR_UNSPECIFIED (pBestRt->dst))
        {
            if (RIP6_GET_INST_IF_ENTRY (pScanParams->u4InstanceId,
                                        pScanParams->u4SendIndex)->
                u4DefRouteAdvt == RIP6_FALSE)
            {
                /* Default route advertisement not enabled. Process
                 * next route. */
                return TRIE_SUCCESS;
            }
        }

        if (pScanParams->u2RespFlag & RIP6_PERIODIC_UPDATE)
        {
            if (pScanParams->u4ThrotCnt == 0)
            {
                /* Have completed processing MAX allowable routes for this
                 * tick. Remaining routes will be processed in the next
                 * tick. Store the next route to be processed in the
                 * Interface InitPrefix variable and return. */
                Ip6AddrCopy
                    (&RIP6_GET_IF_ENTRY (pScanParams->u4SendIndex)->
                     PeriodicThrotInitPrefix, &pBestRt->dst);
                RIP6_GET_IF_ENTRY (pScanParams->u4SendIndex)->
                    u1ThrotInitPrefixLen = pBestRt->u1Prefixlen;
                RIP6_GET_IF_ENTRY (pScanParams->u4SendIndex)->u1ThrotFlag =
                    RIP6_THROT_IN_PROGRESS;

                /* TRIE Scan needs to be terminated over here. So sending 
                 * TRIE_FAILURE. Caller needs to differentiate the actual
                 * reason of failure by looking into the THROT_COUNT Value
                 */
                return TRIE_FAILURE;
            }
            else if ((RIP6_GET_IF_ENTRY (pScanParams->u4SendIndex)->
                      u1ThrotFlag == RIP6_THROT_IN_PROGRESS)
                     &&
                     (Ip6AddrMatch
                      (&pBestRt->dst,
                       &RIP6_GET_IF_ENTRY (pScanParams->u4SendIndex)->
                       PeriodicThrotInitPrefix,
                       RIP6_GET_IF_ENTRY (pScanParams->u4SendIndex)->
                       u1ThrotInitPrefixLen) == FALSE)
                     &&
                     (Ip6IsAddrGreater
                      (&pBestRt->dst,
                       &RIP6_GET_IF_ENTRY (pScanParams->u4SendIndex)->
                       PeriodicThrotInitPrefix) == IP6_SUCCESS))
            {
                /* Route is less than the route to be processed. 
                 * So go to next route. */
                return TRIE_SUCCESS;
            }

            /* The Best route needs to be Send out over the interface */
            RIP6_GET_IF_ENTRY (pScanParams->u4SendIndex)->u1ThrotFlag =
                RIP6_THROT_NOT_IN_PROGRESS;
            pScanParams->u4ThrotCnt--;
        }

        if (Rip6SendRtLearntOverIf (pScanParams->u2RespFlag,
                                    pScanParams->u1Horizon,
                                    pScanParams->u4SendIndex,
                                    pScanParams->u4RecvIndex,
                                    pScanParams->u2DstPort,
                                    pScanParams->pu4RtCounter,
                                    pScanParams->u4MaxNoRte,
                                    pScanParams->pu4Len,
                                    pScanParams->pDst,
                                    pScanParams->ppBuf, pBestRt,
                                    pScanParams->u4InstanceId) != RIP6_SUCCESS)
        {
            return TRIE_FAILURE;
        }
    }
    else if (pScanParams->u1ScanCmd == RIP6_TRIE_SCAN_REDIS_METRIC_UPD)
    {
        while (pRtEntry != NULL)
        {
            RTM6_SET_BIT (i4RouteProtoMask, pRtEntry->i1Proto);
            if (pRtEntry->i1Proto == ISIS6_ID)
            {
                if ((pRtEntry->u1Level == IP_ISIS_LEVEL1) &&
                    (pScanParams->i4ProtoMask & RTM6_ISISL1_MASK))
                {
                    i4Result = RIP6_TRUE;
                }
                else if ((pRtEntry->u1Level == IP_ISIS_LEVEL2) &&
                         (pScanParams->i4ProtoMask & RTM6_ISISL2_MASK))
                {
                    i4Result = RIP6_TRUE;
                }
            }
            else if (pScanParams->i4ProtoMask & i4RouteProtoMask)
            {
                if ((RIP6_GET_IF_ENTRY (pRtEntry->u4Index)->
                     u1Status & RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED)
                {

                    i4Result = RIP6_TRUE;
                }
            }
            if (i4Result == RIP6_TRUE)
            {
                /* Delete the current route from the TRIE */
                pRtEntry->u1Metric = RIP6_INFINITY;
                pRtEntry->u1RtFlag = RIP6_INITIALIZE_ONE;
                break;
            }
            pRtEntry = pRtEntry->pNext;
        }
    }
    else if (pScanParams->u1ScanCmd == RIP6_TRIE_SCAN_REDIS_ROUTE_DEL)
    {
        pPrevRoute = NULL;
        while (pRtEntry != NULL)
        {
            RTM6_SET_BIT (i4RouteProtoMask, pRtEntry->i1Proto);
            if (pRtEntry->i1Proto == ISIS6_ID)
            {
                if ((pRtEntry->u1Level == IP_ISIS_LEVEL1) &&
                    (pScanParams->i4ProtoMask & RTM6_ISISL1_MASK))
                {
                    i4Result = RIP6_TRUE;
                }
                else if ((pRtEntry->u1Level == IP_ISIS_LEVEL2) &&
                         (pScanParams->i4ProtoMask & RTM6_ISISL2_MASK))
                {
                    i4Result = RIP6_TRUE;
                }
            }
            else if (pScanParams->i4ProtoMask & i4RouteProtoMask)
            {
                if ((RIP6_GET_IF_ENTRY (pRtEntry->u4Index)->
                     u1Status & RIP6_IFACE_ENABLED) != RIP6_IFACE_ENABLED)
                {
                    i4Result = RIP6_TRUE;
                }
            }
            if (i4Result == RIP6_TRUE)
            {
                /* Delete the current route from the TRIE */
                if (pPrevRoute == NULL)
                {
                    /* Route to be deleted is the first route */
                    *(tRip6RtEntry **) ppAppPtr = pRtEntry->pNext;
                }
                else
                {
                    pPrevRoute->pNext = pRtEntry->pNext;
                }
                u4InstanceId =
                    RIP6_GET_INST_IF_MAP_ENTRY (pRtEntry->u4Index)->
                    u4InstanceId;
                --(garip6InstanceDatabase[u4InstanceId]->u4RtTableRoutes);
                Rip6SendRtChgNotification (&pRtEntry->dst,
                                           pRtEntry->u1Prefixlen,
                                           &pRtEntry->nexthop,
                                           pRtEntry->u1Metric,
                                           pRtEntry->i1Proto, pRtEntry->u4Index,
                                           120, NETIPV6_DELETE_ROUTE);

                Rip6RtRelease (pRtEntry);
                if (pRtEntry->i1Refcnt == RIP6_INITIALIZE_ZERO)
                {
                    if (MemReleaseMemBlock ((tMemPoolId) i4Rip6rtId,
                                            (UINT1 *) pRtEntry) != RIP6_SUCCESS)
                    {
                        RIP6_TRC_ARG (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                      "RIP6 : rip6 redis route could not "
                                      "release mem to pool \n");
                        RIP6_TRC_ARG4 (RIP6_MOD_TRC, MGMT_TRC, RIP6_NAME,
                                       "%s mem err: Type = %d  PoolId = %d Memptr = %p",
                                       ERROR_FATAL_STR, ERR_MEM_RELEASE,
                                       RIP6_INITIALIZE_ZERO, i4Rip6rtId);
                    }
                    pRtEntry = NULL;
                }
                break;
            }
            pPrevRoute = pRtEntry;
            pRtEntry = pRtEntry->pNext;
        }
    }

    return TRIE_SUCCESS;
}
