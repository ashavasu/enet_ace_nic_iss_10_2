/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6filt.h,v 1.3 2009/06/04 04:31:01 premap-iss Exp $
 *
 * Description:Contains routines for handling peer and advertisement 
 * filters
 *******************************************************************/

#ifndef _RIP6PEER_H_
#define _RIP6PEER_H_

#define RIP6_VALID           1
#define RIP6_INVALID         2

#define RIP6_ALLOW           1
#define RIP6_DENY            2

#define RIP6_ENABLE          1
#define RIP6_DISABLE         2

#endif
