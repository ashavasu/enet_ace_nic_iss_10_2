/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6red.h,v 1.1 2013/06/23 13:18:51 siva Exp $
 *
 * Description: This file contains all macro definitions and 
 *              function prototypes for RIP6 Server module.
 *              
 *******************************************************************/
#ifndef __RIP6_RED_H
#define __RIP6_RED_H


/* Represents the message types encoded in the update messages */
typedef enum {
    RIP6_RED_BULK_REQ_MESSAGE      = RM_BULK_UPDT_REQ_MSG,
    RIP6_RED_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
    RIP6_RT_DYN_INFO,
              /* Structure enum will be used by db table and also standby node
               * to identify sync info type as RIP6 Route table info. */
    RIP6_MAX_DYN_INFO_TYPE
             /* Number of structures used for dynamic info sync up through DB
              * mechanism. */
}eRip6RedRmMsgType;

typedef enum{
    RIP6_HA_UPD_NOT_STARTED = 1,/* 1 */
    RIP6_HA_UPD_IN_PROGRESS,    /* 2 */
    RIP6_HA_UPD_COMPLETED,      /* 3 */
    RIP6_HA_UPD_ABORTED,        /* 4 */
    RIP6_HA_MAX_BLK_UPD_STATUS
} eRip6HaBulkUpdStatus;

/* Macro Definitions for RIP6 Server Redundancy */

#define MAX_RIP6_SYNCUP_ROUTES      100
#define RIP6_INITIATE_BULK_UPDATES    L2_INITIATE_BULK_UPDATES

#define RIP6_RM_GET_NUM_STANDBY_NODES_UP() \
          gRip6RedGblInfo.u1NumPeersUp = RmGetStandbyNodeCount ()

#define RIP6_NUM_STANDBY_NODES() gRip6RedGblInfo.u1NumPeersUp

#define RIP6_RM_BULK_REQ_RCVD() gRip6RedGblInfo.bBulkReqRcvd

#define RIP6_RED_MSG_SIZE(pInfo) \
        ((pInfo->u1Action == RIP6_RED_ADD_CACHE) ?  \
        RIP6_RED_DYN_INFO_SIZE : 4 + 4)

#define RIP6_IS_STANDBY_UP() \
          ((gRip6RedGblInfo.u1NumPeersUp > 0) ? OSIX_TRUE : OSIX_FALSE)

/* RM wanted */

#define RIP6_RM_PUT_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_DATA_ASSIGN_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define RIP6_RM_PUT_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_DATA_ASSIGN_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define RIP6_RM_PUT_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_DATA_ASSIGN_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define RIP6_RM_PUT_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
    RM_COPY_TO_OFFSET (pMsg, psrc, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size;\
}while (0)

#define RIP6_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
        RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
                *(pu4Offset) += 1;\
}while (0)

#define RIP6_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
        RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
                *(pu4Offset) += 2;\
}while (0)

#define RIP6_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
        RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
                *(pu4Offset) += 4;\
}while (0)

#define RIP6_RM_GET_N_BYTE(pMsg, pu4Offset, psrc, u4Size) \
do { \
        RM_GET_DATA_N_BYTE (pMsg, psrc, *(pu4Offset), u4Size); \
                *(pu4Offset) += u4Size;\
}while (0)

#define RIP6_RED_MAX_MSG_SIZE        1500
#define RIP6_RED_TYPE_FIELD_SIZE     1
#define RIP6_RED_LEN_FIELD_SIZE      2
#define RIP6_RED_MIM_MSG_SIZE        (1 + 2 + 2)
#define RIP6_RED_DYN_INFO_SIZE       (6 + 4 + 2 + 2 + 1 + 1)

#define RIP6_RED_BULK_UPD_TAIL_MSG_SIZE       3
#define RIP6_RED_BULK_REQ_MSG_SIZE            3

#define RIP6_RED_BULQ_REQ_SIZE       3

/* Function prototypes for RIP6 Redundancy */

#endif /* __RIP6_RED_H */
