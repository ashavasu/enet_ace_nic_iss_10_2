/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrip6db.h,v 1.5 2013/07/01 12:55:56 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRIP6DB_H
#define _FSRIP6DB_H

UINT1 Fsrip6InstanceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsrip6InstIfMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsrip6RipIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsrip6RipProfileTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsrip6RipRouteTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 Fsrip6RipPeerTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 Fsrip6RipAdvFilterTableINDEX [] = {SNMP_FIXED_LENGTH_OCTET_STRING ,16};
UINT1 FsRip6DistInOutRouteMapTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};

UINT4 fsrip6 [] ={1,3,6,1,4,1,2076,3};
tSNMP_OID_TYPE fsrip6OID = {8, fsrip6};


UINT4 Fsrip6RoutePreference [ ] ={1,3,6,1,4,1,2076,3,1,1};
UINT4 Fsrip6GlobalDebug [ ] ={1,3,6,1,4,1,2076,3,1,2};
UINT4 Fsrip6GlobalInstanceIndex [ ] ={1,3,6,1,4,1,2076,3,1,3};
UINT4 Fsrip6PeerFilter [ ] ={1,3,6,1,4,1,2076,3,1,4};
UINT4 Fsrip6AdvFilter [ ] ={1,3,6,1,4,1,2076,3,1,5};
UINT4 FsRip6RRDAdminStatus [ ] ={1,3,6,1,4,1,2076,3,1,6};
UINT4 Fsrip6RRDProtoMaskForEnable [ ] ={1,3,6,1,4,1,2076,3,1,7};
UINT4 Fsrip6RRDSrcProtoMaskForDisable [ ] ={1,3,6,1,4,1,2076,3,1,8};
UINT4 Fsrip6RRDRouteDefMetric [ ] ={1,3,6,1,4,1,2076,3,1,9};
UINT4 Fsrip6RRDRouteMapName [ ] ={1,3,6,1,4,1,2076,3,1,10};
UINT4 Fsrip6RouteCount [ ] ={1,3,6,1,4,1,2076,3,1,11};
UINT4 Fsrip6InstanceIndex [ ] ={1,3,6,1,4,1,2076,3,2,1,1,1};
UINT4 Fsrip6InstanceStatus [ ] ={1,3,6,1,4,1,2076,3,2,1,1,2};
UINT4 Fsrip6IfIndex [ ] ={1,3,6,1,4,1,2076,3,2,2,1,1};
UINT4 Fsrip6InstIfMapInstId [ ] ={1,3,6,1,4,1,2076,3,2,2,1,2};
UINT4 Fsrip6InstIfMapIfAtchStatus [ ] ={1,3,6,1,4,1,2076,3,2,2,1,3};
UINT4 Fsrip6RipIfIndex [ ] ={1,3,6,1,4,1,2076,3,2,3,1,1};
UINT4 Fsrip6RipIfProfileIndex [ ] ={1,3,6,1,4,1,2076,3,2,3,1,2};
UINT4 Fsrip6RipIfCost [ ] ={1,3,6,1,4,1,2076,3,2,3,1,3};
UINT4 Fsrip6RipIfOperStatus [ ] ={1,3,6,1,4,1,2076,3,2,3,1,4};
UINT4 Fsrip6RipIfProtocolEnable [ ] ={1,3,6,1,4,1,2076,3,2,3,1,5};
UINT4 Fsrip6RipIfInMessages [ ] ={1,3,6,1,4,1,2076,3,2,3,1,6};
UINT4 Fsrip6RipIfInRequests [ ] ={1,3,6,1,4,1,2076,3,2,3,1,7};
UINT4 Fsrip6RipIfInResponses [ ] ={1,3,6,1,4,1,2076,3,2,3,1,8};
UINT4 Fsrip6RipIfUnknownCmds [ ] ={1,3,6,1,4,1,2076,3,2,3,1,9};
UINT4 Fsrip6RipIfInOtherVer [ ] ={1,3,6,1,4,1,2076,3,2,3,1,10};
UINT4 Fsrip6RipIfInDiscards [ ] ={1,3,6,1,4,1,2076,3,2,3,1,11};
UINT4 Fsrip6RipIfOutMessages [ ] ={1,3,6,1,4,1,2076,3,2,3,1,12};
UINT4 Fsrip6RipIfOutRequests [ ] ={1,3,6,1,4,1,2076,3,2,3,1,13};
UINT4 Fsrip6RipIfOutResponses [ ] ={1,3,6,1,4,1,2076,3,2,3,1,14};
UINT4 Fsrip6RipIfOutTrigUpdates [ ] ={1,3,6,1,4,1,2076,3,2,3,1,15};
UINT4 Fsrip6RipIfDefRouteAdvt [ ] ={1,3,6,1,4,1,2076,3,2,3,1,16};
UINT4 Fsrip6RipProfileIndex [ ] ={1,3,6,1,4,1,2076,3,2,4,1,1};
UINT4 Fsrip6RipProfileStatus [ ] ={1,3,6,1,4,1,2076,3,2,4,1,2};
UINT4 Fsrip6RipProfileHorizon [ ] ={1,3,6,1,4,1,2076,3,2,4,1,3};
UINT4 Fsrip6RipProfilePeriodicUpdTime [ ] ={1,3,6,1,4,1,2076,3,2,4,1,4};
UINT4 Fsrip6RipProfileTrigDelayTime [ ] ={1,3,6,1,4,1,2076,3,2,4,1,5};
UINT4 Fsrip6RipProfileRouteAge [ ] ={1,3,6,1,4,1,2076,3,2,4,1,6};
UINT4 Fsrip6RipProfileGarbageCollectTime [ ] ={1,3,6,1,4,1,2076,3,2,4,1,7};
UINT4 Fsrip6RipRouteDest [ ] ={1,3,6,1,4,1,2076,3,2,5,1,1};
UINT4 Fsrip6RipRoutePfxLength [ ] ={1,3,6,1,4,1,2076,3,2,5,1,2};
UINT4 Fsrip6RipRouteProtocol [ ] ={1,3,6,1,4,1,2076,3,2,5,1,3};
UINT4 Fsrip6RipRouteIfIndex [ ] ={1,3,6,1,4,1,2076,3,2,5,1,4};
UINT4 Fsrip6RipRouteNextHop [ ] ={1,3,6,1,4,1,2076,3,2,5,1,5};
UINT4 Fsrip6RipRouteMetric [ ] ={1,3,6,1,4,1,2076,3,2,5,1,6};
UINT4 Fsrip6RipRouteTag [ ] ={1,3,6,1,4,1,2076,3,2,5,1,7};
UINT4 Fsrip6RipRouteAge [ ] ={1,3,6,1,4,1,2076,3,2,5,1,8};
UINT4 Fsrip6RipPeerAddr [ ] ={1,3,6,1,4,1,2076,3,2,6,1,1};
UINT4 Fsrip6RipPeerEntryStatus [ ] ={1,3,6,1,4,1,2076,3,2,6,1,2};
UINT4 Fsrip6RipAdvFilterAddress [ ] ={1,3,6,1,4,1,2076,3,2,7,1,1};
UINT4 Fsrip6RipAdvFilterStatus [ ] ={1,3,6,1,4,1,2076,3,2,7,1,2};
UINT4 FsRip6DistInOutRouteMapName [ ] ={1,3,6,1,4,1,2076,3,3,1,1,1};
UINT4 FsRip6DistInOutRouteMapType [ ] ={1,3,6,1,4,1,2076,3,3,1,1,3};
UINT4 FsRip6DistInOutRouteMapValue [ ] ={1,3,6,1,4,1,2076,3,3,1,1,4};
UINT4 FsRip6DistInOutRouteMapRowStatus [ ] ={1,3,6,1,4,1,2076,3,3,1,1,5};
UINT4 FsRip6PreferenceValue [ ] ={1,3,6,1,4,1,2076,3,4,1};
UINT4 FsRip6TestBulkUpd [ ] ={1,3,6,1,4,1,2076,3,5,1};
UINT4 FsRip6TestDynamicUpd [ ] ={1,3,6,1,4,1,2076,3,5,2};




tMbDbEntry fsrip6MibEntry[]= {

{{10,Fsrip6RoutePreference}, NULL, Fsrip6RoutePreferenceGet, Fsrip6RoutePreferenceSet, Fsrip6RoutePreferenceTest, Fsrip6RoutePreferenceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{10,Fsrip6GlobalDebug}, NULL, Fsrip6GlobalDebugGet, Fsrip6GlobalDebugSet, Fsrip6GlobalDebugTest, Fsrip6GlobalDebugDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsrip6GlobalInstanceIndex}, NULL, Fsrip6GlobalInstanceIndexGet, Fsrip6GlobalInstanceIndexSet, Fsrip6GlobalInstanceIndexTest, Fsrip6GlobalInstanceIndexDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsrip6PeerFilter}, NULL, Fsrip6PeerFilterGet, Fsrip6PeerFilterSet, Fsrip6PeerFilterTest, Fsrip6PeerFilterDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,Fsrip6AdvFilter}, NULL, Fsrip6AdvFilterGet, Fsrip6AdvFilterSet, Fsrip6AdvFilterTest, Fsrip6AdvFilterDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{10,FsRip6RRDAdminStatus}, NULL, FsRip6RRDAdminStatusGet, FsRip6RRDAdminStatusSet, FsRip6RRDAdminStatusTest, FsRip6RRDAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{10,Fsrip6RRDProtoMaskForEnable}, NULL, Fsrip6RRDProtoMaskForEnableGet, Fsrip6RRDProtoMaskForEnableSet, Fsrip6RRDProtoMaskForEnableTest, Fsrip6RRDProtoMaskForEnableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsrip6RRDSrcProtoMaskForDisable}, NULL, Fsrip6RRDSrcProtoMaskForDisableGet, Fsrip6RRDSrcProtoMaskForDisableSet, Fsrip6RRDSrcProtoMaskForDisableTest, Fsrip6RRDSrcProtoMaskForDisableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsrip6RRDRouteDefMetric}, NULL, Fsrip6RRDRouteDefMetricGet, Fsrip6RRDRouteDefMetricSet, Fsrip6RRDRouteDefMetricTest, Fsrip6RRDRouteDefMetricDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,Fsrip6RRDRouteMapName}, NULL, Fsrip6RRDRouteMapNameGet, Fsrip6RRDRouteMapNameSet, Fsrip6RRDRouteMapNameTest, Fsrip6RRDRouteMapNameDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,Fsrip6RouteCount}, NULL, Fsrip6RouteCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,Fsrip6InstanceIndex}, GetNextIndexFsrip6InstanceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsrip6InstanceTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6InstanceStatus}, GetNextIndexFsrip6InstanceTable, Fsrip6InstanceStatusGet, Fsrip6InstanceStatusSet, Fsrip6InstanceStatusTest, Fsrip6InstanceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsrip6InstanceTableINDEX, 1, 0, 0, "1"},

{{12,Fsrip6IfIndex}, GetNextIndexFsrip6InstIfMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsrip6InstIfMapTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6InstIfMapInstId}, GetNextIndexFsrip6InstIfMapTable, Fsrip6InstIfMapInstIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsrip6InstIfMapTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6InstIfMapIfAtchStatus}, GetNextIndexFsrip6InstIfMapTable, Fsrip6InstIfMapIfAtchStatusGet, Fsrip6InstIfMapIfAtchStatusSet, Fsrip6InstIfMapIfAtchStatusTest, Fsrip6InstIfMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsrip6InstIfMapTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfIndex}, GetNextIndexFsrip6RipIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfProfileIndex}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfProfileIndexGet, Fsrip6RipIfProfileIndexSet, Fsrip6RipIfProfileIndexTest, Fsrip6RipIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsrip6RipIfTableINDEX, 1, 0, 0, "0"},

{{12,Fsrip6RipIfCost}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfCostGet, Fsrip6RipIfCostSet, Fsrip6RipIfCostTest, Fsrip6RipIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsrip6RipIfTableINDEX, 1, 0, 0, "1"},

{{12,Fsrip6RipIfOperStatus}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfProtocolEnable}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfProtocolEnableGet, Fsrip6RipIfProtocolEnableSet, Fsrip6RipIfProtocolEnableTest, Fsrip6RipIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsrip6RipIfTableINDEX, 1, 0, 0, "2"},

{{12,Fsrip6RipIfInMessages}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfInMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfInRequests}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfInRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfInResponses}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfInResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfUnknownCmds}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfUnknownCmdsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfInOtherVer}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfInOtherVerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfInDiscards}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfInDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfOutMessages}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfOutMessagesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfOutRequests}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfOutRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfOutResponses}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfOutResponsesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfOutTrigUpdates}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfOutTrigUpdatesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, Fsrip6RipIfTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipIfDefRouteAdvt}, GetNextIndexFsrip6RipIfTable, Fsrip6RipIfDefRouteAdvtGet, Fsrip6RipIfDefRouteAdvtSet, Fsrip6RipIfDefRouteAdvtTest, Fsrip6RipIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsrip6RipIfTableINDEX, 1, 0, 0, "0"},

{{12,Fsrip6RipProfileIndex}, GetNextIndexFsrip6RipProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsrip6RipProfileTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipProfileStatus}, GetNextIndexFsrip6RipProfileTable, Fsrip6RipProfileStatusGet, Fsrip6RipProfileStatusSet, Fsrip6RipProfileStatusTest, Fsrip6RipProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsrip6RipProfileTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipProfileHorizon}, GetNextIndexFsrip6RipProfileTable, Fsrip6RipProfileHorizonGet, Fsrip6RipProfileHorizonSet, Fsrip6RipProfileHorizonTest, Fsrip6RipProfileTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsrip6RipProfileTableINDEX, 1, 0, 0, "3"},

{{12,Fsrip6RipProfilePeriodicUpdTime}, GetNextIndexFsrip6RipProfileTable, Fsrip6RipProfilePeriodicUpdTimeGet, Fsrip6RipProfilePeriodicUpdTimeSet, Fsrip6RipProfilePeriodicUpdTimeTest, Fsrip6RipProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsrip6RipProfileTableINDEX, 1, 0, 0, "30"},

{{12,Fsrip6RipProfileTrigDelayTime}, GetNextIndexFsrip6RipProfileTable, Fsrip6RipProfileTrigDelayTimeGet, Fsrip6RipProfileTrigDelayTimeSet, Fsrip6RipProfileTrigDelayTimeTest, Fsrip6RipProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsrip6RipProfileTableINDEX, 1, 0, 0, "5"},

{{12,Fsrip6RipProfileRouteAge}, GetNextIndexFsrip6RipProfileTable, Fsrip6RipProfileRouteAgeGet, Fsrip6RipProfileRouteAgeSet, Fsrip6RipProfileRouteAgeTest, Fsrip6RipProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsrip6RipProfileTableINDEX, 1, 0, 0, "180"},

{{12,Fsrip6RipProfileGarbageCollectTime}, GetNextIndexFsrip6RipProfileTable, Fsrip6RipProfileGarbageCollectTimeGet, Fsrip6RipProfileGarbageCollectTimeSet, Fsrip6RipProfileGarbageCollectTimeTest, Fsrip6RipProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsrip6RipProfileTableINDEX, 1, 0, 0, "120"},

{{12,Fsrip6RipRouteDest}, GetNextIndexFsrip6RipRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsrip6RipRouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsrip6RipRoutePfxLength}, GetNextIndexFsrip6RipRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsrip6RipRouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsrip6RipRouteProtocol}, GetNextIndexFsrip6RipRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, Fsrip6RipRouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsrip6RipRouteIfIndex}, GetNextIndexFsrip6RipRouteTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, Fsrip6RipRouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsrip6RipRouteNextHop}, GetNextIndexFsrip6RipRouteTable, Fsrip6RipRouteNextHopGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, Fsrip6RipRouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsrip6RipRouteMetric}, GetNextIndexFsrip6RipRouteTable, Fsrip6RipRouteMetricGet, Fsrip6RipRouteMetricSet, Fsrip6RipRouteMetricTest, Fsrip6RipRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsrip6RipRouteTableINDEX, 4, 0, 0, "1"},

{{12,Fsrip6RipRouteTag}, GetNextIndexFsrip6RipRouteTable, Fsrip6RipRouteTagGet, Fsrip6RipRouteTagSet, Fsrip6RipRouteTagTest, Fsrip6RipRouteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, Fsrip6RipRouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsrip6RipRouteAge}, GetNextIndexFsrip6RipRouteTable, Fsrip6RipRouteAgeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, Fsrip6RipRouteTableINDEX, 4, 0, 0, NULL},

{{12,Fsrip6RipPeerAddr}, GetNextIndexFsrip6RipPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsrip6RipPeerTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipPeerEntryStatus}, GetNextIndexFsrip6RipPeerTable, Fsrip6RipPeerEntryStatusGet, Fsrip6RipPeerEntryStatusSet, Fsrip6RipPeerEntryStatusTest, Fsrip6RipPeerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsrip6RipPeerTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipAdvFilterAddress}, GetNextIndexFsrip6RipAdvFilterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, Fsrip6RipAdvFilterTableINDEX, 1, 0, 0, NULL},

{{12,Fsrip6RipAdvFilterStatus}, GetNextIndexFsrip6RipAdvFilterTable, Fsrip6RipAdvFilterStatusGet, Fsrip6RipAdvFilterStatusSet, Fsrip6RipAdvFilterStatusTest, Fsrip6RipAdvFilterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, Fsrip6RipAdvFilterTableINDEX, 1, 0, 0, NULL},

{{12,FsRip6DistInOutRouteMapName}, GetNextIndexFsRip6DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsRip6DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsRip6DistInOutRouteMapType}, GetNextIndexFsRip6DistInOutRouteMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsRip6DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsRip6DistInOutRouteMapValue}, GetNextIndexFsRip6DistInOutRouteMapTable, FsRip6DistInOutRouteMapValueGet, FsRip6DistInOutRouteMapValueSet, FsRip6DistInOutRouteMapValueTest, FsRip6DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsRip6DistInOutRouteMapTableINDEX, 2, 0, 0, NULL},

{{12,FsRip6DistInOutRouteMapRowStatus}, GetNextIndexFsRip6DistInOutRouteMapTable, FsRip6DistInOutRouteMapRowStatusGet, FsRip6DistInOutRouteMapRowStatusSet, FsRip6DistInOutRouteMapRowStatusTest, FsRip6DistInOutRouteMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsRip6DistInOutRouteMapTableINDEX, 2, 0, 1, NULL},

{{10,FsRip6PreferenceValue}, NULL, FsRip6PreferenceValueGet, FsRip6PreferenceValueSet, FsRip6PreferenceValueTest, FsRip6PreferenceValueDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{10,FsRip6TestBulkUpd}, NULL, FsRip6TestBulkUpdGet, FsRip6TestBulkUpdSet, FsRip6TestBulkUpdTest, FsRip6TestBulkUpdDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsRip6TestDynamicUpd}, NULL, FsRip6TestDynamicUpdGet, FsRip6TestDynamicUpdSet, FsRip6TestDynamicUpdTest, FsRip6TestDynamicUpdDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsrip6Entry = { 58, fsrip6MibEntry };

#endif /* _FSRIP6DB_H */

