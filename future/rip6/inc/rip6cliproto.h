/*******************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: rip6cliproto.h,v 1.9 2017/02/21 14:06:42 siva Exp $
 *********************************************************************/

#include "rip6inc.h"
INT4 Rip6EnableRouter PROTO ((tCliHandle));
INT4 Rip6DisableRouter PROTO ((tCliHandle));
INT4 Rip6EnableDisableRip6If PROTO ((tCliHandle, UINT4 , UINT1));
INT4 Rip6EnableDisablePoisonReverse PROTO ((tCliHandle,UINT4)); 
INT4 Rip6EnableSplitHorizon PROTO ((tCliHandle, UINT4));
INT4 Rip6EnableNoHorizon PROTO ((tCliHandle, UINT4));
INT4 Rip6EnableDisableDefRouteAdvt PROTO ((tCliHandle, UINT4, UINT1));
INT4 Rip6Metric PROTO ((tCliHandle, UINT4, INT4));
INT4 Rip6Redistribute PROTO ((tCliHandle, UINT4, INT4, UINT1, UINT1 *));

INT4 RIP6AddOrDelPeerFilter PROTO ((tCliHandle, UINT4));
INT4 ShowRIP6PeerFilterStatus PROTO ((tCliHandle));
INT4 RIP6TrigUpdateIntervalShow PROTO ((tCliHandle));
INT4 RIP6ProfileTrigDelayTime PROTO ((tCliHandle, UINT4));

INT4 Rip6AddDelFilterAddr PROTO ((tCliHandle, UINT1 *,UINT1, UINT1));
INT4 Rip6CliSetDistribute PROTO ((tCliHandle, UINT1 *, UINT1, UINT1));
INT4 Rip6Debug PROTO ((tCliHandle,UINT4));
INT4 Rip6FilterTableShow PROTO ((tCliHandle));
INT4 Rip6IfStatsShow PROTO ((tCliHandle));
INT4 Rip6RouteShow PROTO ((tCliHandle));
INT4 Rip6ShowRoute PROTO ((tCliHandle));
INT4 Rip6ProfileShow PROTO ((tCliHandle));
INT4 Rip6ConfigRedistribute PROTO ((INT4, UINT1, UINT1 *));
INT4 Rip6ConfigRedisDefMetric PROTO ((INT4));
INT4 Rip6ShowRunningConfig PROTO ((tCliHandle,UINT4));
INT4 Rip6ShowRunningConfigGlobal PROTO ((tCliHandle));
INT4 Rip6ShowRunningConfigInterface PROTO ((tCliHandle));
INT4 Rip6ShowRunningConfigInterfaceDetails PROTO ((tCliHandle,INT4,UINT4));
INT4 Rip6SetRouteDistance PROTO ((tCliHandle, INT4, UINT1 *));
INT4 Rip6SetNoRouteDistance PROTO ((tCliHandle, UINT1 *));
VOID IssRip6ShowDebugging PROTO ((tCliHandle));


INT1 nmhTestv2IfMainRowStatus PROTO((UINT4 *, INT4 ,INT4 ));
INT1 nmhSetIfMainRowStatus PROTO((INT4 , INT4 ));
INT1 nmhTestv2IfMainType PROTO((UINT4 *, INT4 ,INT4 ));
INT1 nmhSetIfMainType PROTO((INT4 , INT4 ));
INT1 nmhSetIfAlias PROTO((INT4 , tSNMP_OCTET_STRING_TYPE * ));
INT1 nmhSetIfMainAdminStatus PROTO((INT4 , INT4 ));
INT1 nmhTestv2IfIpAddr PROTO((UINT4 *, INT4 , UINT4 ));
INT1 nmhSetIfIpAddr PROTO((INT4 i4IfMainIndex, UINT4 u4SetValIfIpAddr));
INT1 nmhGetFsMIStdIpv6IpForwarding PROTO ((INT4 i4FsMIStdIpContextId,
                                           INT4 *pi4RetValFsMIStdIpv6IpForwarding));

extern tIp6Addr * CliStrToIp6Addr (UINT1 *);


