#ifndef _RIP6_H_
#define _RIP6_H_
/* $Id: rip6.h,v 1.17 2017/02/21 14:06:42 siva Exp $*/
/*
 *-----------------------------------------------------------------------------
 *    FILE NAME                    :    rip6.h
 *
 *    PRINCIPAL AUTHOR             :    Kaushik Biswas 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    RIP6 Module
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains constants, macros
 *                                      and typedefs which are
 *                                      common to the RIP6 module .
 *
 *-----------------------------------------------------------------------------
 */
/* RIP6 - Sizing Related macros */
#define RIP6_MAX_PEERS                    10
#define RIP6_MAX_ADV_FILTERS              10

/* Throttling parameter value that specifies the max
 * number of routes in a Trie Instances to be processed
 * during each periodic update. */
#define RIP6_THROT_MAX_PEROIDIC_ROUTE     2300
#define RIP6_THROT_INIT_DELAY             1

/* RIP6 Socket RCVBUF/SNDBUF depth */
#define RIP6_SOCK_BUF_SIZE                15000

/**************************** GLOBAL VARIABLE *********************************/
#define RIP6_TRUE                   1
#define RIP6_FALSE                  0
#define RIP6_LOOP_INIT              1
#define RIP6_INITIALIZE_ZERO        0
#define RIP6_INITIALIZE_ONE         1
#define RIP6_INITIALIZE_TWO         2
#define RIP6_INITIALIZE_THREE       3
#define RIP6_INITIALIZE_FOUR        4
#define RIP6_INITIALIZE_FIVE        5
#define RIP6_INITIALIZE_SIX         6
#define RIP6_INITIALIZE_SEVEN       7
#define RIP6_INITIALIZE_TEN         10
#define RIP6_MAX_COST               15
#define RIP6_MAX_PRFX_LEN           128
#define RIP6_INITIALIZE_SIXTEEN     16
#define RIP6_INITIALIZE_TWENTYONE   21
#define RIP6_INITIALIZE_FOURTY      40
#define HUNDRED                     100
#define FOURHUNDRED                 400
#define RIP6_IP6ADDR_LEN            32
#define RIP6_RTM_REDIS_ENABLE       RTM6_REDISTRIBUTE_ENABLE_MESSAGE 
#define RIP6_RTM_REDIS_DISABLE      RTM6_REDISTRIBUTE_DISABLE_MESSAGE 

#define SET_PROTO_MASK              0x80
/* Administrative Status */
#define  RIP6_ADMIN_UP       0x01
#define  RIP6_ADMIN_DOWN     0x02
#define  RIP6_ADMIN_INVALID  0x03
#define  RIP6_ADMIN_VALID    0x04


/**************************** DATA STRUCTURES *********************************/

#define RIP6_IP6ADDR_MEM_ALLOC    MEM_CALLOC((sizeof(UINT2) * RIP6_IP6ADDR_PREFIX_LEN), \
                                  RIP6_INITIALIZE_ONE, UINT1)


#define RIP6_INIT_COMPLETE(u4Status)     lrInitComplete(u4Status)
typedef struct PEER_ENTRY
{
    tIp6Addr PeerAddr;
}
tRip6PeerEntry;

typedef UINT1 tRip6RtKey[20];

/* Advertisement filter entry */


typedef struct ADV_FILTER
{
    tIp6Addr NetAddr;
}
tRip6AdvFltEntry;

/* 
 * The Instance Interface Mapping structure
 */
 
typedef struct 
{
UINT4       u4IfIndex;      /* The interface index */
UINT4       u4InstanceId;   /* Instance ID */
UINT4       u4InsIfStatus;  /* Instance Insterface attachment status */
}
tInstanceInterfaceMap;

/*
 * The RIPng timer structure
 */
typedef struct
{
    tTmrAppTimer        timer;
    UINT1               u1Id;   /*To demultiplex between handlers*/           
    UINT1               au1Reserved[3]; /* For Padding */
}
tRip6RtTimer;


typedef struct
{
    UINT4               u4RemainingTime;
    UINT1               u1Id;              
    UINT1               au1Reserved[3];   
}
tRip6RtMITimer;

/* 
 * The routing table entry structure
 */
typedef struct rip6_entry
{
    tRip6RtMITimer    rtEntryTimer;  /* For route timeouts and 
                                      * for garbage collection 
                                      */
    tTMO_SLL_NODE     link;          /* Link into the list of routes 
                                      * learnt through
                                      * the same logical interface
                                      */
    struct rip6_entry  *pNext;       /* The next route entry on the same
                                      * leaf
                                      */
    UINT4             u4ChangeTime;  /* The time of change of this entry */
    UINT2             u2Flag;
    INT1              i1Refcnt;      /* To tell the No. of places this
                                      * entry is attached in the tree
                                      */
    INT1              i1AddrCnt;     /* To tell the No. of Direct routes
                                      * with same prefix. 
                                      */
    UINT1             u1Level;         /*ISIS Level */
    UINT1             au1Pad1[3];     /* Padding */
    /* The dynamic entries that are to be synced should be added after the
     *  * RouteDbNode. And both the halves should be padded accordingly */
    tDbTblNode        RouteDbNode;
    tIp6Addr          dst;           /* Destination address prefix */
    tIp6Addr          nexthop;       /* NextHop Address */
    UINT4             u4Index;       /* The index to the interface table
                                      * telling on which logical interface 
                                      * the route was learnt 
                                      */
    UINT2             u2RtTag;       /* To tell whether the route is learnt
                                      * INTERNALLY or EXTERNALLY
                                      */
    UINT1              u1Preference; /* Contains preference value */
    UINT1             u1RtFlag;      /* To indicate change of route */  
    UINT1             u1Metric;      /* No. of hops to reach the dest */
    UINT1             u1Prefixlen;   /* No of significant bits in the prefix */
    UINT1             u1Operation;   /* Route Operation ADD/DELETE */
    INT1              i1Proto;       /*  STATIC or RIPNG or LOCAL */
    INT1              i1Type;        /* How is the route learnt ?
                                      * DIRECT / INDIRECT means
                                      */
    UINT1             au1Pad[3];     /* Padding */
}
tRip6RtEntry;

typedef struct rip6_RrdGlobal
{
   tTmrAppTimer       timer;
   UINT1              u1Id;
   UINT1              au1Reserved[3];
   UINT4              u4InstanceId;
}tRip6RrdGlobal;

typedef struct rip6_RtGlobal
{
    tTmrAppTimer        timer;
    UINT1               u1Id;   
    UINT1               au1Reserved[3];  
    UINT4               u4InstanceId;
}
tRip6RtGlobal;

typedef struct rip6_RtHold
{
    tTmrAppTimer        HoldTimer;
    UINT1               u1Id;
    UINT1               au1Reserved[3];
    UINT4               u4InstanceId;
}
tRip6RtHold;


/* 
 * The RIPv6 Interface statistics
 */
typedef struct
{

    UINT4               u4InMessages; /* RIPng packets received on 
                                       * this interface
                                       */
    UINT4               u4OutMessages;/* RIPng packets sent on this
                                       * interface
                                       */
    UINT4               u4Discards;   /* Total discarded RIPng packets
                                       * on this interface
                                       */
    UINT4               u4InReq;      /* Total RIPng requests received
                                       * on this interface
                                       */
    UINT4               u4InRes;      /* Total RIPng responses obtained
                                       * on this interface
                                       */
    UINT4               u4OutRes;     /* Total RIPng requests sent
                                       * on this interface
                                       */
    UINT4               u4OutReq;     /* Total RIPng responses sent
                                       * on this interface
                                       */
    UINT4               u4UnknownCmds;/* Number of unknown commands
                                       * received  on this interface
                                       */
    UINT4               u4OtherVer;   /* Other version packets received
                                       * on this interface
                                       */
    UINT4               u4TrigUpdates;/* Total trig-updates on this
                                       * interface
                                       */
}
tRip6IfStats;

/* 
 * The structure for storing the interface related RIPng parameters
 */
typedef struct
{

    UINT1               u1Status;    /* To indicate whether the 
                                      * entry is UP/DOWN/ENABLED/
                                      * DISABLED/ALLOCATED/FREE
                                      */
    UINT1               u1TrigFlag;  /* Used as a flag to tell that
                                      * triggered update be sent on
                                      * this interface and also as
                                      * a flag to tell that 
                                      * triggered  update timer 
                                      * is running on this
                                      * interface
                                      */
    UINT2               u2ProfIndex; /* The index to the rip6 
                                      * profile table.
                                      */
    UINT1               u1Cost;      /* Cost of the network on 
                                      * which the packet arrived.
                                      */
    UINT1               u1ThrotInitPrefixLen;   /* PrefixLen of the Route Prefix
                                                 * from which the Trie Scan
                                                 * for Periodic Response should 
                                                 * start. */
    UINT1               u1ThrotFlag;            /* Flag indicating whether
                                                 * Throttling of periodic
                                                 * response send is in progress
                                                 * for this interface or not
                                                 */
    /* Values taken for u1ThrotFlag */
#define     RIP6_THROT_NOT_IN_PROGRESS       0
#define     RIP6_THROT_IN_PROGRESS           1
    UINT1               au1Reserved[1];   /* For Alignment */
    UINT4               u4Index;     /* The logical interface 
                                      * index
                                      */
    UINT4               u4NextRegularUpdate;    /* The time at which the next
                                                 * regular update is due
                                                 */
    UINT4               u4DefRouteAdvt;         /* Default route advertisement
       * policy for this interface */
    tIp6Addr            PeriodicThrotInitPrefix;/* Prefix from which the Trie Scan
                                                 * for Periodic Response should
                                                 * start. */
    tRip6RtTimer        periodicTimer;          /* Periodic update timer      */
    tRip6RtTimer        trigDelayTimer;         /* Triggered delay timer      */
    tRip6IfStats        stats;
    tIp6Addr            LinkLocalAddr;          /* Interface's Link-Local Address
                                                 * that will be used as Source
                                                 * Address in RIP6 packets. */
}
tRip6If;

/*
 * The RIPv6 Profile Table entry structure
 */
typedef struct
{

    UINT1               u1Status;               /* To tell whether the entry
                                                 * is allocated or free
                                                 */
    UINT1               u1Horizon;              /* To tell whether to use
                                                 * no-horizoning,
                                                 * Split-horizoning
                                                 * or poison reversing.
                                                 */
    UINT2               u2Reserved;              /*  For Padding  */
    UINT4               u4PeriodicUpdTime;      /* The periodicity at which 
                                                 * the regular RIPng updates 
                                                 * are to be sent
                                                 */
    UINT4               u4TrigDelayTime;        /* The interval by which the
                                                 * triggered update is delayed
                                                 */
    UINT4               u4RouteAge;             /* The interval after which 
                                                 * the route has to age out
                                                 */
    UINT4               u4GcTime;               /* The garbage collect time 
                                                 * after which the route has 
                                                 * to be purged from the 
                                                 * route table
                                                 */
}
tRip6Profile;

typedef struct 
{
    tTMO_SLL_NODE  Next;
    tRip6RtEntry   pRtEntry;
}tRip6RrdRouteInfo;

/*
* The Instance Database structure 
 */

typedef struct 
{
tRadixNodeHead     *prip6RtTable;
#ifndef RIP6_ARRAY_TO_RBTREE_WANTED
tRip6If            *arip6If[MAX_RIP6_IFACES_LIMIT +2];   /* The RIP6 interface table */
#endif
tRip6Profile       *arip6Profile[MAX_RIP6_PROFILES_LIMIT];
                                                /* The RIP6 profile table */
tRip6PeerEntry      aRip6PeerTable[RIP6_MAX_PEERS];
                                                /* Neighbors of this RIPNG
                                                 * router. */
tRip6AdvFltEntry    aRip6AdvFltTable[RIP6_MAX_ADV_FILTERS];
                                                /* Filter Table for filtering
                                                 * out routes to Neighbors. */
UINT4               u4RtTableRoutes;            /* No. of routes in the Routing
                                                   table */
UINT4               u4LookupPreference;         /* flag telling how to lookup */
UINT4               u4Rip6EnabledIfs;           /* No. of intefaces on which RIP6
                                                 * pkts can be received and sent 
                                                 * to */
UINT4               u4DiscaredRoutes;           /* No. of discarded routes */
UINT4               u4Rip6PeerFlag;             /* Flag to set the peer list as 
                                                 * allow or filter table.*/
UINT4               u4Rip6AdvFltFlag;           /* Flag to enable or disable
                                                 * filtering */
UINT1               u1TrigFlag;                 /* Flag indicating whether Trigger
                                                 * updation needs to be done or
                                                 * not after processing the
                                                 * received route response
                                                 * message. */
UINT1               u1ThrotInitPrefixLen;       /* PrefixLen of the Route Prefix
                                                 * from which the Trie Scan
                                                 * for Periodic Response should 
                                                 * start. */
UINT1               u1Pad[2];                   /* Reserved */
tRip6RtGlobal       Rip6RtGlobal;               /* Global Timer */
tRip6RtHold         Rip6RtHold;                 /* Hold Timer */
tRip6RrdGlobal      Rip6RrdGlobal;              /* Route Redistribution Timer */
}
tInstanceDatabase;
 

/* The RIPv6 packet header
 */
typedef struct
{

    UINT1               u1Command;              /* REQUEST or RESPONSE      */
    UINT1               u1Ver;                  /* For us this is version 1 */
    UINT2               u2Res;                  /* 2 bytes - must be 0      */
}
tRip6Hdr;

/*
 * The RIPv6 packet RTE structure
 */
typedef struct
{

    tIp6Addr           prefix;                 /* The IPv6 address prefix stored
                                                * in n/w byte order
                                                */
    UINT2              u2RtTag;                /* Attribute assigned to a route
                                                * whether -- internally or
                                                * externally learnt
                                                */
    UINT1              u1PrefixLen;            /* The no of the significant bits
                                                * in the prefix
                                                */
    UINT1              u1Metric;

}
tRip6Rte;

/*
 * Structure for exchanging interface and address related control information
 * between RIP6 and IP6 tasks
 */
typedef struct _RIP6_IP6_PARAMETERS
{

#define  RIP6_INTF_STATUS_CHANGE    1
#define  RIP6_FWD_STATUS_CHANGE     2
#define  RIP6_ADDR_STATUS_CHANGE    3
    UINT4       u4Command;
#define  RIP6_IF_CREATE   1
#define  RIP6_IF_DELETE   2
#define  RIP6_IF_UP       3
#define  RIP6_IF_DOWN     4
#define  RIP6_INST_UP     1
#define  RIP6_INST_DOWN   0
#define  RIP6_IF_ENABLE   5
#define  RIP6_IF_DISABLE  6
#define  RIP6_ROUTE_ADD   7
#define  RIP6_ROUTE_DEL   8
#define  RIP6_ROUTE_MODIFY   9
#define  RIP6_ADDR_ADD   10
#define  RIP6_ADDR_DEL   11
    UINT1       u1Type;       /* Type of indication */
    UINT1       u1Stat;       /* delete or create */
    UINT1       u1Prefixlen;  /* Prefix length of Address */
    UINT1       u1Admin;
    UINT4       u4Index;      /* Index for the interface entry */
    tIp6Addr  addr;          /* IPv6 address */
}
tRip6Ip6Params;

/*
 * Structure for exchanging RIP message parameters
 */

typedef struct _RIP6_UDP6_PARAMETERS
{

    INT4        i4Hlim;      /* Hop Limit of the packet */
    UINT4       u4Index;     /* Index of the interface over which
                               * it is to be sent */
    UINT2       u2SrcPort;  /* Source port number over which packet
                               * came */
    UINT2       u2DstPort;  /* Destination port number over which
                               * packet is to be sent */
    UINT4       u4Len;       /* Length of application data */
    tIp6Addr  srcAddr;     /* Source address in the packet */
    tIp6Addr  dstAddr;     /* Destination address in the packet */

}
tRip6Udp6Params;


typedef struct Rip6Redistribution
{
   UINT4       u4RRDAdminStatus;
   UINT4       u4ProtoMask;
   UINT4       u4RouteId;
   UINT4       u4MaxMsgSize;
   UINT2       u2ASNumber;
   UINT1       u1DefMetric;
   UINT1       u1Pad;
   UINT1       au1RMapName[RMAP_MAX_NAME_LEN + 4];
} tRip6Redistribution;

typedef struct Rip6TrieScanParam
{
    tCRU_BUF_CHAIN_HEADER **ppBuf;
    tIp6Addr               *pDst;
    UINT4                  *pu4RtCounter;
    UINT4                  *pu4Len;
    UINT4                   u4ThrotCnt;
    UINT4                   u4InstanceId;
    UINT4                   u4RecvIndex;
    UINT4                   u4SendIndex;
    UINT4                   u4MaxNoRte;
    INT4                    i4ProtoMask;
    UINT2                   u2RespFlag;
    UINT2                   u2DstPort;
    UINT1                   u1Horizon;
    UINT1                   u1ScanCmd;
/* Macros for Scan Cmd */
#define  RIP6_TRIE_SCAN_TRIG_CLEAR           1
#define  RIP6_TRIE_SCAN_INTF_FINAL           2
#define  RIP6_TRIE_SCAN_INTF_ADVT            3
#define  RIP6_TRIE_SCAN_REDIS_METRIC_UPD     4
#define  RIP6_TRIE_SCAN_REDIS_ROUTE_DEL      5
    UINT1                   au1Pad[2];
} tRip6ScanParam;


typedef struct Rip6Msg
{
    UINT1 au1Rip6Buf[IP6_DEFAULT_MTU];
}tRip6Buf;

/* Structure for RIP6 HA */

typedef struct
{
    tIp6Addr    DestAddr;
    UINT4       u4InstanceId;
    UINT4       u4IfIndex;
    UINT1       u1PrefixLen;
    UINT1       au1Pad[3];
}tRip6RtMarker;

typedef struct _Rip6RmCtrlMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Pad[1];
}tRip6RmCtrlMsg;

typedef struct _Rip6RmMsg
{
     tRip6RmCtrlMsg RmCtrlMsg; /* Control message from RM */
} tRip6RmMsg;

typedef struct _sRip6RedGblInfo{
    UINT1 u1BulkUpdStatus; /* bulk update status
                            * RIP_HA_UPD_NOT_STARTED 0
                            * RIP_HA_UPD_COMPLETED 1
                            * RIP_HA_UPD_IN_PROGRESS 2,
                            * RIP_HA_UPD_ABORTED 3 */
    UINT1 u1NodeStatus;     /* Node status(RM_INIT/RM_ACTIVE/RM_STANDBY). */
    UINT1 u1NumPeersUp;     /* Indicates number of standby nodes
                                            that are up. */
    UINT1 bBulkReqRcvd;     /* To check whether bulk request recieved
                               from standby before RM_STANDBY_UP event. */
}tRip6RedGblInfo;

/**************************** MACRO DEFINITIONS *******************************/
/*
 * Task-related constants: Task name, priority, queues, events
 */

#define  RIP6_TASK_NAME                   "RIP6"
#define  RIP6_TASK_CONTROL_QUEUE          "RPQ0"
#define  RIP6_TASK_RIP_INPUT_QUEUE        "RPQ1"
#define  RIP6_RM_PKT_QUEUE                "RIP6RMQ"
#define  RIP6_TASK_NODE_ID                SELF
#define  RIP6_TASK_PRIORITY               30
#define  RIP6_RM_QUE_DEPTH                10
#define  RIP6_DEFAULT_INSTANCE            0
#define  RIP6_SELECT_TASK_DELAY           10
                                           /* This delay is used in RR6 task to schedule it every 
                                            * "RIP6_SELECT_TASK DELAY" Ticks - Setting this value
                                            * to schedule Select task every 10 ticks. */

#define  RIP6_YES                         1
#define  RIP6_NO                          2
#define  RIP6_SELF_NODE                   0
#define  RIP6_IFACE_ATTACHED              1
#define  RIP6_IFACE_DETACHED              0
#define  RIP6_INVALID_INSTANCE            -1
#define  RIP6_MAX_STACK_NODE              40

#define  RIP6_CONTROL_EVENT               (0x00000020)
#define  RIP6_DATA_EVENT                  (0x00000040)
#define  RIP6_TIMER0_EVENT                (0x00000080)
#define  RIP6_TIMER1_EVENT                (0x00000200)
#define  RIP6_1S_TIMER_EVENT              (0x00000004)
#define  RIP6_ROUTEMAP_UPDATE_EVENT       (0x00000008)
#define  RIP6_RM_PKT_EVENT                (0x00000400)
#define  RIP6_RM_PEND_RT_SYNC_EVENT       (0x00000800)
#define  RIP6_RM_START_TIMER_EVENT        (0x00001000)

#define  RIP6_TREE_ROOT                      0x01
#define  RIP6_TREE_BACKTRACK                 0x02
#define  RIP6_TREE_NO_BACKTRACK              0x04

#define  RIP6_PERIODIC_UPDATE                0x0001
#define  RIP6_TRIG_UPDATE                    0x0002
#define  RIP6_FINAL_UPDATE                   0x0004
#define  RIP6_IFACE_FINAL                    0x0010
#define  RIP6_SEND_DYNAMIC                   0x0020
#define  RIP6_SEND_ALL                       0x0040
#define  RIP6_FULL_ROUTE_TABLE               0x0100

#define  RIP6_PURGE_DYNAMIC                  1

#define  RIP6_CHECK_ALL                      1
#define  RIP6_BULK_DELETE                    1
#define  RIP6_NEXTHOP_RTE_METRIC             0xff

#define RIP6_Q_DEPTH_DEF                     8000

#define  RIP6_DELTA_TIME                     500
#define  RIP6_INFINITY                       16
#define  RIP6_MIN_METRIC                     1
#define  RIP6_MAX_METRIC                     16
#define  RIP6_DFLT_COST                      1
#define  RIP6_DFLT_HORIZON                   3
#define  RIP6_DFLT_PERIODIC_UPD_TIME         30
#define  RIP6_DFLT_TRIG_DELAY_TIME           5
#define  RIP6_DFLT_ROUTE_AGE                 180
#define  RIP6_DFLT_GARBAGE_COLLECT_TIME      120
#define  RIP6_HOLD_ON_TIME                   50
#define  RIP6_STAGGER_ROUTE                  30

#define  IP6_ROUTING_PROTO_RIPNG             1
#define  IP6_ROUTING_PROTO_NONE              2

/*
 * Some constants related to NetMgmt 
 */
#define  RIP6_NO_HORIZONING                  1  
#define  RIP6_SPLIT_HORIZONING               2 
#define  RIP6_POISON_REVERSE                 3 

/* Protocol Id */
#define  RIP6_OTHERS                         1
#define  RIP6_LOCAL                          2
#define  RIP6_STATIC                         3
#define  RIPNG                               5

#define  DIRECT                              3  
#define  INDIRECT                            4 
#define  RIP6_MI_PURGE_ALL                   111

    /* This number doesn't have any significance, I just needed some number
     * so used it, if you find it conflicting, change it*/

/* 
 * Values used to set from the NetMgmt for setting the status of RIP6 to
 * UP,decide upon the type of lookup and  decide whether to propogate the
 * the static routes or not
 */
#define RIP6_ENABLED                         IP6_ROUTING_PROTO_RIPNG
#define RIP6_DISABLED                        IP6_ROUTING_PROTO_NONE

#define RIP6_STATIC_LOOKUP                   1 
#define RIP6_DYNAMIC_LOOKUP                  2 
#define RIP6_BEST_METRIC_LOOKUP              3

/*
 * Packet related constants
 */
#define RIP6_VER_1                           1
#define RIP6_REQUEST                         1
#define RIP6_RESPONSE                        2
#define RIP6_METRIC_OFFSET                   19
#define RIP6_HDR_LEN                         (sizeof(tRip6Hdr))
#define RIP6_RTE_LEN                         (sizeof(tRip6Rte))
#define IP6_HDR_LEN                          40
#define UDP6_HDR_LEN                         8
#define RIP6_INTERNAL                        0
#define RIP6_EXTERNAL                        1
#define RIP6_MI_MAX_PREFIX_LEN               128
#define IP6_ROUTE_TYPE_DIRECT                3
#define IP6_ROUTE_TYPE_INDIRECT              4
 
#define RIP6_PORT                            521

/* Values used to indicate the status of the RIP6_IF entry */
#define RIP6_IFACE_ALLOCATED                 0x01   /* Indicates the interface
                                                     * is created at the lower
                                                     * layer.
                                                     */
#define RIP6_IFACE_UP                        0x02   /* Indicates that at the
                                                     * lower layer the
                                                     * interface is active
                                                     */
#define RIP6_IFACE_ENABLED                   0x04   /* Indiacates that RIP6
                                                     * is enabled over this
                                                     * interface.
                                                     */

#define  RIP6_AGE_OUT_TIMER_ID               1
#define  RIP6_GARB_COLLECT_TIMER_ID          2
#define  RIP6_UPDATE_TIMER_ID                3
#define  RIP6_TRIG_TIMER_ID                  4
#define  RIP6_PKT_RECV_TIMER_ID              5
#define  RIP6_Regular_TIMER_ID               6
#define  RIP6_1S_TIMER_ID                    7
#define  RIP6_HOLD_ON_TIMER_ID               8
#define  RIP6_BUF_READ_OFFSET(buf)   CB_READ_OFFSET(buf)
#define  RIP6_BUF_WRITE_OFFSET(buf)  CB_WRITE_OFFSET(buf)
#define  RIP6_BUF_DATA_PTR(pBuf)     CRU_BUF_GetDataPtr(CRU_BUF_GetFirstDataDesc(pBuf))
#define  SET_ALL_RIP_ROUTERS(a) {(a).u4_addr[0] = CRU_HTONL(0xff020000);\
                                (a).u4_addr[1] = 0;\
                                (a).u4_addr[2] = 0;\
                                (a).u4_addr[3] = CRU_HTONL(9);}
#define  SET_DEFAULT_PREFIX(a) {(a).u4_addr[0] = 0;\
                                (a).u4_addr[1] = 0;\
                                (a).u4_addr[2] = 0;\
                                (a).u4_addr[3] = 0;}
                                
/* Values used to indicate the status of RIP6 profile table entry */
#define RIP6_PROFILE_UP                      1
#define RIP6_PROFILE_INVALID                 2

#define RIP6_RT_INDEX(pRt)      (pRt)->i2Index
#define RIP6_RT_TYPE(pRt)       (pRt)->i1Type
#define RIP6_RT_NDPTR(pRt)      (pRt)->pNdCache
#define RIP6_RT_NHPTR(pRt)      &((pRt)->nexthop)

/* Macro for Getting RIP6 sizing parameters */                                
                                
/* Definition Related with Semaphore locks. */
#define  RIP6_TASK_SEM_NAME              "RP6L"
#define  RIP6_RTM6RT_SEM_NAME            "RRTL"
#define  RIP6_SEM_CREATE_INIT_CNT        1

/*
 * Macros related to timer processing
 */

#define IS_GARBAGE_COLLECT_RUNNING(pRtEntry, pu4RemainingTime) \
        (pRtEntry->rtEntryTimer.u1Id == RIP6_GARB_COLLECT_TIMER_ID) ? \
        (RIP6_SUCCESS) : (RIP6_FAILURE)
#define RIP6_GET_RT_FROM_TIMER(x) \
      (tRip6RtEntry *)((UINT1 *)(x) - RIP6_OFFSET(tRip6RtEntry,rtEntryTimer))

#define RIP6IF_FROM_TIMER(x,y) \
      (tRip6If *)(VOID *)((UINT1 *)(x) - RIP6_OFFSET(tRip6If,y))

#define RIP6_DELAY_TIMER_NODE(i2Index) \
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,i2Index)->trigDelayTimer.timer
#define RIP6_UPDATE_TIMER_NODE(i2Index) \
        RIP6_GET_INST_IF_ENTRY (u4InstanceId,i2Index)->periodicTimer.timer
#define RIP6_ROUTE_TIMER_NODE(pRtEntry) \
        pRtEntry->rtEntryTimer.timer

#define RIP6_TRIG_DELAY_INTERVAL(i2Interface) \
        garip6InstanceDatabase[u4InstanceId]->arip6Profile[RIP6_GET_INST_IF_ENTRY (u4InstanceId,i2Interface)->u2ProfIndex]->u4TrigDelayTime

#define RIP6_UPDATE_TIMER_INTERVAL(i2Interface) \
        garip6InstanceDatabase[u4InstanceId]->arip6Profile[RIP6_GET_INST_IF_ENTRY (u4InstanceId,i2Interface)->u2ProfIndex]->u4PeriodicUpdTime

#define RIP6_ROUTE_AGE_INTERVAL(pRt) \
        garip6InstanceDatabase[u4InstanceId]->arip6Profile[RIP6_GET_INST_IF_ENTRY (u4InstanceId,pRt->u4Index)->u2ProfIndex]->u4RouteAge

#define RIP6_GARBAGE_COLLECT_INTERVAL(i2Interface) \
        garip6InstanceDatabase[gInstanceId]->arip6Profile[RIP6_GET_INST_IF_ENTRY (gInstanceId,i2Interface)->u2ProfIndex]->u4GcTime

#define RIP6_MAX_U4       0xffffffff

#define RIP6_GET_DIFF_TIME(u4CurrentTime,u4NextRegularUpdate) \
              (u4CurrentTime > u4NextRegularUpdate) ? \
              (((RIP6_MAX_U4 - u4CurrentTime + 1) + u4NextRegularUpdate + \
                RIP6_MAX_U4 + 1) - u4CurrentTime) :\
                (u4NextRegularUpdate - u4CurrentTime)

#define RIP6_RT_ENTRY_FROM_SLL(sll) \
        (tRip6RtEntry *)\
        ((UINT1 *)(sll) - RIP6_OFFSET(tRip6RtEntry, link))

#define RIP6_RT_SLL_Next(pList,pNode,pSll_prev) \
        (((pNode) == NULL) ? TMO_SLL_First(pList) : \
        (((pNode)->pNext == &(pList)->Head) ? NULL : pSll_prev))

#define RIP6_GET_HORIZON(i2Index) \
        garip6InstanceDatabase[u4InstanceId]->arip6Profile[RIP6_GET_INST_IF_ENTRY (u4InstanceId,i2Index)->u2ProfIndex]->u1Horizon

#define RIP6_PKT_ASSIGN_METRIC(pBuf,i,u1Metric) \
    ASSIGN_1_BYTE (pBuf,(UINT4)(RIP6_HDR_LEN + (i * RIP6_RTE_LEN) + \
                          RIP6_METRIC_OFFSET), u1Metric)
/* Place where Task Lock can go in */
#define MAX_RTE(i2Index)\
        (UINT4) ((Rip6GetIfMtu (i2Index) - \
        ( 2 * IP6_HDR_LEN) - UDP6_HDR_LEN - RIP6_HDR_LEN) / RIP6_RTE_LEN)

/* Place where Task UnLock can go in */

#define RIP6_MIN(a,b)   (a < b) ? a : b
#define RIP6_OFFSET(x,y)            FSAP_OFFSETOF(x,y)


#define RIP6_PROTO_REDIS_NOT_SUPPORTED 2
#define RIP6_RRD_DISABLE_MASK_VALUE \
          (RIP6_ALL_PROTO_MASK & \
           (~(gRip6Redistribution.u4ProtoMask)))
#define RIP6_RRD_ENABLE         1
#define RIP6_RRD_DISABLE        2

#define RIP6_ROUTE_WITHDRAWN    1
#define RIP6_ROUTE_FEASIBLE     0

#define RIP6_REDISTRIBUTION_ID  9


#ifdef L2RED_WANTED
#define RIP6_GET_NODE_STATUS()  gRip6RedGblInfo.u1NodeStatus
#else
#define RIP6_GET_NODE_STATUS()  RM_ACTIVE
#endif

#ifdef L2RED_WANTED
#define RIP6_GET_RMNODE_STATUS()  RmGetNodeState ()
#else
#define RIP6_GET_RMNODE_STATUS()  RM_ACTIVE
#endif

#define RIP6HA_ADD_ROUTE   1
#define RIP6HA_DEL_ROUTE   2

#define RIP6_PROCESS_ONE_MSG    0
#define RIP6_PROCESS_ALL_MSG    1

/***********************EXTERN DECLARATIONS************************************/
extern tRip6RedGblInfo  gRip6RedGblInfo;
extern tDbTblDescriptor gRip6DynInfoList;
extern tDbDataDescInfo   gaRip6DynDataDescList[];
extern tDbOffsetTemplate gaRip6RtOffsetTbl[];
extern tRip6RtMarker    gRip6RedRtMarker;
extern tOsixQId         gRip6RmPktQId;
extern UINT4            gu4Rip6SysLogId;
extern INT4             gi4Rip6SockId;
extern UINT4            gInstanceId;
extern UINT4            gu4InstanceIndex;
extern INT4             i4Rip6rtId;         /* ID for the pool of route entries  */
extern INT4             i4Rip6ProfilePoolId;
extern tMemPoolId       gRip6RMPoolId;
extern INT4             i4Rip6RrdRouteInfoId;
extern INT4             i4Rip6IfInfoPoolId;
extern INT4             i4Rip6InstIfPoolId;
#ifdef RIP6_ARRAY_TO_RBTREE_WANTED
extern tRBTree          gRip6IfInfoTbl;     /* It is added instead of garip6If and
                                                garip6InstanceInterfaceMap */
extern tRBTree          gRip6InstIfTbl;     /* It is added instead of arip6If in
                                               tInstanceDatabase */ 
#else
extern tRip6If          *garip6If[];          /* The global RIPng Interface table */       
extern tInstanceInterfaceMap *garip6InstanceInterfaceMap[];  /* The RIP6 Instance Interface Mapping table */
#endif
extern tInstanceDatabase *garip6InstanceDatabase[]; /* The RIP6 Instance Database Table */
extern UINT1     garip6InstanceStatus[];  /* Instance status   */
extern tTimerListId  gRip6TimerListId;
extern tTimerListId  gRip6IntfTimerListId;
extern tRip6Redistribution gRip6Redistribution;
extern tFilteringRMap* gpDistributeInFilterRMap;    /* Config for Distribute in filtering feature */
extern tFilteringRMap* gpDistributeOutFilterRMap;   /* Config for Distribute out filtering feature */
extern tFilteringRMap* gpDistanceFilterRMap;        /* Config for Distance filtering feature */
extern tTimerListId        gRip6RedisTmrListId;
extern tTMO_SLL     gRip6RedistributionList;
extern struct rbtree      *gRip6RBTree;
extern INT4 gi4Rip6BulkUpdTestStatus;
extern INT4 gi4Rip6DynUpdTestStatus;
extern tOsixSemId          gRip6Rtm6SemLockId;

/* The following data structure is used only in case of
 * RIP6_ARRAY_TO_RBTREE_WANTED. */
typedef struct __Rip6IfInfoNode {
    tRBNodeEmbd            RBNode;     /* Node to be added in gRip6IfInfoTbl*/
    UINT4                  u4Index;    /* Index of RBTree */
    tRip6If                *pRip6If;   /* Pointer to tRip6If */
    tInstanceInterfaceMap  *pRip6InstIfaceMap; /* Pointer to ttInstanceInterfaceMap */
}tRip6IfInfoNode;

/* The following data structure is used only in case of
 * RIP6_ARRAY_TO_RBTREE_WANTED. */
typedef struct __Rip6InstIfNode {
    tRBNodeEmbd            RBNode;     /* Node to be added in gRip6InstIfTbl */
    UINT4                  u4Instance; /* Primary index of RBTree */
    UINT4                  u4Index;    /* Secondary index of RBTree */
    tRip6If                *pRip6If;   /* Pointer to tRip6If */
}tRip6InstIfNode;

#define RIP6_RBTREE_KEY_LESSER                  (-1)
#define RIP6_RBTREE_KEY_GREATER                 (1)
#define RIP6_RBTREE_KEY_EQUAL                   (0)

#ifdef RIP6_ARRAY_TO_RBTREE_WANTED

#define RIP6_MAX_IF_INFO_ENTRIES            (MAX_RIP6_IFACES_LIMIT + 2)
#define RIP6_MAX_INST_IF_ENTRIES            ((MAX_RIP6_IFACES_LIMIT + 2) *\
                                              MAX_RIP6_INSTANCE)
#define RIP6_CREATE_IF_RBTREE()             Rip6CreateRip6IfInfoTbl()
#define RIP6_DELETE_IF_RBTREE()             Rip6DeleteRip6IfInfoTbl()
#define RIP6_GET_IF_ENTRY(u4Index)                                             \
        Rip6GetIfInRip6IfInfoTbl(u4Index)
#define RIP6_SET_IF_ENTRY(u4Index,pEntry)                                      \
        Rip6SetIfInRip6IfInfoTbl(u4Index,pEntry)
#define RIP6_GET_INST_IF_MAP_ENTRY(u4Index)                                    \
        Rip6GetInstIfaceMapInRip6IfInfoTbl(u4Index)
#define RIP6_SET_INST_IF_MAP_ENTRY(u4Index,pEntry)                             \
        Rip6SetInstIfaceMapInRip6IfInfoTbl(u4Index,pEntry)
#define RIP6_CREATE_INST_IF_RBTREE()             Rip6CreateRip6InstIfTbl()
#define RIP6_DELETE_INST_IF_RBTREE()             Rip6DeleteRip6InstIfTbl()
#define RIP6_GET_INST_IF_ENTRY(u4Inst,u4Index)                                 \
        Rip6GetIfEntryInRip6InstIfTbl(u4Inst,(UINT4)u4Index)
#define RIP6_SET_INST_IF_ENTRY(u4Inst,u4Index,pEntry)                          \
        Rip6SetIfEntryInRip6InstIfTbl(u4Inst,u4Index,pEntry)

#else /* RIP6_ARRAY_TO_RBTREE_WANTED */

#define RIP6_MAX_IF_INFO_ENTRIES            1
#define RIP6_MAX_INST_IF_ENTRIES            1
#define RIP6_CREATE_IF_RBTREE()             Rip6UtilRBTreeCreateStub()
#define RIP6_DELETE_IF_RBTREE() 
#define RIP6_GET_IF_ENTRY(u4Index)          garip6If[u4Index]
#define RIP6_SET_IF_ENTRY(u4Index,pEntry)   garip6If[u4Index] = pEntry
#define RIP6_GET_INST_IF_MAP_ENTRY(u4Index) garip6InstanceInterfaceMap[u4Index]
#define RIP6_SET_INST_IF_MAP_ENTRY(u4Index,pEntry)                             \
        garip6InstanceInterfaceMap[u4Index] = pEntry
#define RIP6_CREATE_INST_IF_RBTREE()        Rip6UtilRBTreeCreateStub()
#define RIP6_DELETE_INST_IF_RBTREE()     
#define RIP6_GET_INST_IF_ENTRY(u4Inst,u4Index)                                 \
        garip6InstanceDatabase[u4Inst]->arip6If[u4Index]
#define RIP6_SET_INST_IF_ENTRY(u4Inst,u4Index,pEntry)                          \
        garip6InstanceDatabase[u4Inst]->arip6If[u4Index] = pEntry
#endif /* RIP6_ARRAY_TO_RBTREE_WANTED */

#endif
