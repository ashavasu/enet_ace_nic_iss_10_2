
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrip6low.h,v 1.5 2013/07/01 12:55:56 siva Exp $
*
* Description: 
*********************************************************************/
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrip6RoutePreference ARG_LIST((INT4 *));

INT1
nmhGetFsMIrip6RoutePreference ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsrip6GlobalDebug ARG_LIST((INT4 *));

INT1
nmhGetFsrip6GlobalInstanceIndex ARG_LIST((INT4 *));

INT1
nmhGetFsrip6PeerFilter ARG_LIST((INT4 *));
INT1
nmhGetFsMIrip6PeerFilter ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsrip6AdvFilter ARG_LIST((INT4 *));
INT1
nmhGetFsMIrip6AdvFilter ARG_LIST((UINT4, INT4 *));

INT1
nmhGetFsRip6RRDAdminStatus ARG_LIST((INT4 *));

INT1
nmhGetFsrip6RRDProtoMaskForEnable ARG_LIST((INT4 *));

INT1
nmhGetFsrip6RRDSrcProtoMaskForDisable ARG_LIST((INT4 *));

INT1
nmhGetFsrip6RRDRouteDefMetric ARG_LIST((INT4 *));

INT1
nmhGetFsrip6RRDRouteMapName ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsrip6RouteCount ARG_LIST((INT4 *));

INT1
nmhGetFsMIrip6RouteCount ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrip6RoutePreference ARG_LIST((INT4 ));

INT1
nmhSetFsMIrip6RoutePreference ARG_LIST((UINT4 ,INT4 ));

INT1
nmhSetFsrip6GlobalDebug ARG_LIST((INT4 ));

INT1
nmhSetFsrip6GlobalInstanceIndex ARG_LIST((UINT4));

INT1
nmhSetFsrip6PeerFilter ARG_LIST((INT4));
INT1
nmhSetFsMIrip6PeerFilter ARG_LIST((UINT4 ,INT4));

INT1
nmhSetFsrip6AdvFilter ARG_LIST((INT4));
INT1
nmhSetFsMIrip6AdvFilter ARG_LIST((UINT4, INT4));

INT1
nmhSetFsRip6RRDAdminStatus ARG_LIST((INT4 ));

INT1
nmhSetFsrip6RRDProtoMaskForEnable ARG_LIST((INT4 ));

INT1
nmhSetFsrip6RRDSrcProtoMaskForDisable ARG_LIST((INT4 ));

INT1
nmhSetFsrip6RRDRouteDefMetric ARG_LIST((INT4 ));

INT1
nmhSetFsrip6RRDRouteMapName ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsrip6RoutePreference ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhTestv2FsMIrip6RoutePreference ARG_LIST((UINT4 *  ,UINT4 ,INT4 ));

INT1
nmhTestv2Fsrip6GlobalDebug ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsrip6GlobalInstanceIndex ARG_LIST((UINT4 * ,INT4));

INT1
nmhTestv2Fsrip6PeerFilter ARG_LIST((UINT4 *, INT4));
INT1
nmhTestv2FsMIrip6PeerFilter ARG_LIST((UINT4 *, UINT4, INT4));

INT1
nmhTestv2Fsrip6AdvFilter ARG_LIST((UINT4 *, INT4));
INT1
nmhTestv2FsMIrip6AdvFilter ARG_LIST((UINT4 *, UINT4, INT4));

INT1
nmhTestv2FsRip6RRDAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsrip6RRDProtoMaskForEnable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsrip6RRDSrcProtoMaskForDisable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsrip6RRDRouteDefMetric ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2Fsrip6RRDRouteMapName ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsrip6RoutePreference ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsrip6GlobalDebug ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsrip6GlobalInstanceIndex ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsrip6PeerFilter ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsrip6AdvFilter ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip6RRDAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsrip6RRDProtoMaskForEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsrip6RRDSrcProtoMaskForDisable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsrip6RRDRouteDefMetric ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2Fsrip6RRDRouteMapName ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsrip6InstanceTable. */
INT1
nmhValidateIndexInstanceFsrip6InstanceTable ARG_LIST((INT4));

/* Proto Type for Low Level GET FIRST fn for Fsrip6InstanceTable  */

INT1
nmhGetFirstIndexFsrip6InstanceTable ARG_LIST ((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrip6InstanceTable ARG_LIST ((INT4 ,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrip6InstanceStatus ARG_LIST ((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrip6InstanceStatus ARG_LIST ((INT4 ,INT4));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsrip6InstanceStatus ARG_LIST ((UINT4 *,INT4 ,INT4));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsrip6InstanceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsrip6InstIfMapTable. */
INT1
nmhValidateIndexInstanceFsrip6InstIfMapTable ARG_LIST ((INT4));
INT1
nmhValidateIndexInstanceFsMIrip6InstIfMapTable ARG_LIST ((UINT4 ,INT4));
/* Proto Type for Low Level GET FIRST fn for Fsrip6InstIfMapTable  */

INT1
nmhGetFirstIndexFsrip6InstIfMapTable ARG_LIST ((INT4 *));
INT1
nmhGetFirstIndexFsMIrip6InstIfMapTable ARG_LIST ((UINT4 ,INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrip6InstIfMapTable ARG_LIST ((INT4 ,INT4 *));
INT1
nmhGetNextIndexFsMIrip6InstIfMapTable ARG_LIST ((UINT4 ,INT4 ,INT4 *));
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrip6InstIfMapInstId ARG_LIST ((INT4 ,INT4 *));
INT1
nmhGetFsMIrip6InstIfMapInstId ARG_LIST ((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsrip6InstIfMapIfAtchStatus ARG_LIST ((INT4 ,INT4 *));
INT1
nmhGetFsMIrip6InstIfMapIfAtchStatus ARG_LIST ((UINT4 ,INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrip6InstIfMapIfAtchStatus ARG_LIST ((INT4 ,INT4));
INT1
nmhSetFsMIrip6InstIfMapIfAtchStatus ARG_LIST ((UINT4 ,INT4 ,INT4));
/* Proto type for Low Level TEST Routine All Objects.  */
INT1
nmhTestv2Fsrip6InstIfMapIfAtchStatus ARG_LIST ((UINT4 *,INT4 ,INT4));
INT1
nmhTestv2FsMIrip6InstIfMapIfAtchStatus ARG_LIST ((UINT4 *,UINT4 ,INT4 ,INT4));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsrip6InstIfMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsrip6RipIfTable. */
INT1
nmhValidateIndexInstanceFsrip6RipIfTable ARG_LIST((INT4 ));
INT1
nmhValidateIndexInstanceFsMIrip6RipIfTable ARG_LIST((UINT4 ,INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsrip6RipIfTable  */

INT1
nmhGetFirstIndexFsrip6RipIfTable ARG_LIST((INT4 *));
INT1
nmhGetFirstIndexFsMIrip6RipIfTable ARG_LIST((UINT4 ,INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrip6RipIfTable ARG_LIST((INT4 , INT4 *));
INT1
nmhGetNextIndexFsMIrip6RipIfTable ARG_LIST((UINT4 ,INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrip6RipIfProfileIndex ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetFsMIrip6RipIfProfileIndex ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsrip6RipIfCost ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetFsMIrip6RipIfCost ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsrip6RipIfOperStatus ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetFsMIrip6RipIfOperStatus ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsrip6RipIfProtocolEnable ARG_LIST((INT4 ,INT4 *));
INT1
nmhGetFsMIrip6RipIfProtocolEnable ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsrip6RipIfInMessages ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfInMessages ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfInRequests ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfInRequests ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfInResponses ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfInResponses ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfUnknownCmds ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfUnknownCmds ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfInOtherVer ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfInOtherVer ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfInDiscards ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfInDiscards ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfOutMessages ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfOutMessages ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfOutRequests ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfOutRequests ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfOutResponses ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfOutResponses ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfOutTrigUpdates ARG_LIST((INT4 ,UINT4 *));
INT1
nmhGetFsMIrip6RipIfOutTrigUpdates ARG_LIST((UINT4 ,INT4 ,UINT4 *));

INT1
nmhGetFsrip6RipIfDefRouteAdvt ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipIfDefRouteAdvt ARG_LIST((UINT4 ,INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrip6RipIfProfileIndex ARG_LIST((INT4  ,INT4 ));
INT1
nmhSetFsMIrip6RipIfProfileIndex ARG_LIST((UINT4 ,INT4  ,INT4 ));

INT1
nmhSetFsrip6RipIfCost ARG_LIST((INT4  ,INT4 ));
INT1
nmhSetFsMIrip6RipIfCost ARG_LIST((UINT4 ,INT4  ,INT4 ));

INT1
nmhSetFsrip6RipIfProtocolEnable ARG_LIST((INT4  ,INT4 ));
INT1
nmhSetFsMIrip6RipIfProtocolEnable ARG_LIST((UINT4 ,INT4  ,INT4 ));

INT1
nmhSetFsrip6RipIfDefRouteAdvt ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipIfDefRouteAdvt ARG_LIST((UINT4  ,INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsrip6RipIfProfileIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipIfCost ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipIfProtocolEnable ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipIfProfileIndex ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipIfCost ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipIfProtocolEnable ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipIfDefRouteAdvt ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipIfDefRouteAdvt ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsrip6RipIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsrip6RipProfileTable. */
INT1
nmhValidateIndexInstanceFsrip6RipProfileTable ARG_LIST((INT4 ));
INT1
nmhValidateIndexInstanceFsMIrip6RipProfileTable ARG_LIST((UINT4 ,INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsrip6RipProfileTable  */

INT1
nmhGetFirstIndexFsrip6RipProfileTable ARG_LIST((INT4 *));
INT1
nmhGetFirstIndexFsMIrip6RipProfileTable ARG_LIST((UINT4 ,INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrip6RipProfileTable ARG_LIST((INT4 , INT4 *));
INT1
nmhGetNextIndexFsMIrip6RipProfileTable ARG_LIST((UINT4 ,INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrip6RipProfileStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsrip6RipProfileHorizon ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsrip6RipProfilePeriodicUpdTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsrip6RipProfileTrigDelayTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsrip6RipProfileRouteAge ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsrip6RipProfileGarbageCollectTime ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipProfileStatus ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipProfileHorizon ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipProfilePeriodicUpdTime ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipProfileTrigDelayTime ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipProfileRouteAge ARG_LIST((UINT4 ,INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipProfileGarbageCollectTime ARG_LIST((UINT4 ,INT4 ,INT4 *));
/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrip6RipProfileStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsrip6RipProfileHorizon ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsrip6RipProfilePeriodicUpdTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsrip6RipProfileTrigDelayTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsrip6RipProfileRouteAge ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsrip6RipProfileGarbageCollectTime ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipProfileStatus ARG_LIST((UINT4 ,INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipProfileHorizon ARG_LIST((UINT4 ,INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipProfilePeriodicUpdTime ARG_LIST((UINT4 ,INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipProfileTrigDelayTime ARG_LIST((UINT4 ,INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipProfileRouteAge ARG_LIST((UINT4 ,INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipProfileGarbageCollectTime ARG_LIST((UINT4 ,INT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsrip6RipProfileStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipProfileHorizon ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipProfilePeriodicUpdTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipProfileTrigDelayTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipProfileRouteAge ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipProfileGarbageCollectTime ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipProfileStatus ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipProfileHorizon ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipProfilePeriodicUpdTime ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipProfileTrigDelayTime ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipProfileRouteAge ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipProfileGarbageCollectTime ARG_LIST((UINT4 *  ,UINT4 ,INT4  ,INT4 ));
INT1
nmhDepv2Fsrip6RipProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsrip6RipRouteTable. */
INT1
nmhValidateIndexInstanceFsrip6RipRouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ));
INT1
nmhValidateIndexInstanceFsMIrip6RipRouteTable ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for Fsrip6RipRouteTable  */

INT1
nmhGetFirstIndexFsrip6RipRouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 *));
INT1
nmhGetFirstIndexFsMIrip6RipRouteTable ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsrip6RipRouteTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));
INT1
nmhGetNextIndexFsMIrip6RipRouteTable ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsrip6RipRouteNextHop ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsrip6RipRouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsrip6RipRouteTag ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsrip6RipRouteAge ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipRouteNextHop ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMIrip6RipRouteMetric ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipRouteTag ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetFsMIrip6RipRouteAge ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsrip6RipRouteMetric ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsrip6RipRouteTag ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipRouteMetric ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetFsMIrip6RipRouteTag ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2Fsrip6RipRouteMetric ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2Fsrip6RipRouteTag ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipRouteMetric ARG_LIST((UINT4 *  ,UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsMIrip6RipRouteTag ARG_LIST((UINT4 *  ,UINT4 ,tSNMP_OCTET_STRING_TYPE * , INT4  , INT4  , INT4  ,INT4 ));
INT1
nmhDepv2Fsrip6RipRouteTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsrip6RipPeerTable. */
INT1
nmhValidateIndexInstanceFsrip6RipPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
INT1
nmhValidateIndexInstanceFsMIrip6RipPeerTable ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFirstIndexFsrip6RipPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
INT1
nmhGetFirstIndexFsMIrip6RipPeerTable ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetNextIndexFsrip6RipPeerTable ARG_LIST(( tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
INT1
nmhGetNextIndexFsMIrip6RipPeerTable ARG_LIST((UINT4, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsrip6RipPeerEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4 *));
INT1
nmhGetFsMIrip6RipPeerEntryStatus ARG_LIST((UINT4, tSNMP_OCTET_STRING_TYPE *, INT4 *));

INT1
nmhSetFsrip6RipPeerEntryStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4));
INT1
nmhSetFsMIrip6RipPeerEntryStatus ARG_LIST((UINT4, tSNMP_OCTET_STRING_TYPE *, INT4));

INT1
nmhTestv2Fsrip6RipPeerEntryStatus ARG_LIST((UINT4 *, tSNMP_OCTET_STRING_TYPE *, INT4));
INT1
nmhTestv2FsMIrip6RipPeerEntryStatus ARG_LIST((UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE *, INT4));

INT1
nmhDepv2Fsrip6RipPeerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for Fsrip6RipAdvFilterTable. */
INT1
nmhValidateIndexInstanceFsrip6RipAdvFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
INT1
nmhValidateIndexInstanceFsMIrip6RipAdvFilterTable ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFirstIndexFsrip6RipAdvFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));
INT1
nmhGetFirstIndexFsMIrip6RipAdvFilterTable ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetNextIndexFsrip6RipAdvFilterTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));
INT1
nmhGetNextIndexFsMIrip6RipAdvFilterTable ARG_LIST((UINT4, tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetFsrip6RipAdvFilterStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4 *));
INT1
nmhGetFsMIrip6RipAdvFilterStatus ARG_LIST((UINT4, tSNMP_OCTET_STRING_TYPE *, INT4 *));

INT1
nmhSetFsrip6RipAdvFilterStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *, INT4));
INT1
nmhSetFsMIrip6RipAdvFilterStatus ARG_LIST((UINT4, tSNMP_OCTET_STRING_TYPE *, INT4));

INT1
nmhTestv2Fsrip6RipAdvFilterStatus ARG_LIST((UINT4 *, tSNMP_OCTET_STRING_TYPE * , INT4));
INT1
nmhTestv2FsMIrip6RipAdvFilterStatus ARG_LIST((UINT4 *, UINT4, tSNMP_OCTET_STRING_TYPE * , INT4));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2Fsrip6RipAdvFilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsRip6DistInOutRouteMapTable. */
INT1
nmhValidateIndexInstanceFsRip6DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsRip6DistInOutRouteMapTable  */

INT1
nmhGetFirstIndexFsRip6DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsRip6DistInOutRouteMapTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip6DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

INT1
nmhGetFsRip6DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRip6DistInOutRouteMapValue ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhSetFsRip6DistInOutRouteMapRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRip6DistInOutRouteMapValue ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

INT1
nmhTestv2FsRip6DistInOutRouteMapRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRip6DistInOutRouteMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhGetFsRip6PreferenceValue ARG_LIST((INT4 *));
INT1
nmhSetFsRip6PreferenceValue ARG_LIST((INT4 ));
INT1
nmhTestv2FsRip6PreferenceValue ARG_LIST((UINT4 *  ,INT4 ));
INT1
nmhDepv2FsRip6PreferenceValue ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsRip6TestBulkUpd ARG_LIST((INT4 *));

INT1
nmhGetFsRip6TestDynamicUpd ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsRip6TestBulkUpd ARG_LIST((INT4 ));

INT1
nmhSetFsRip6TestDynamicUpd ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsRip6TestBulkUpd ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsRip6TestDynamicUpd ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsRip6TestBulkUpd ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsRip6TestDynamicUpd ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

