/*
 * Automatically generated by FuturePostmosy
 * Do not Edit!!!
 */

#ifndef _SNMP_MIB_H
#define _SNMP_MIB_H

/* SNMP-MIB translation table. */

static struct MIB_OID orig_mib_oid_table[] = {
"ccitt",        "0",
"iso",        "1",
"lldpExtensions",        "1.0.8802.1.1.2.1.5",
"org",        "1.3",
"dod",        "1.3.6",
"internet",        "1.3.6.1",
"directory",        "1.3.6.1.1",
"mgmt",        "1.3.6.1.2",
"mib-2",        "1.3.6.1.2.1",
"transmission",        "1.3.6.1.2.1.10",
"mplsStdMIB",        "1.3.6.1.2.1.10.166",
"dot1dBridge",        "1.3.6.1.2.1.17",
"dot1dStp",        "1.3.6.1.2.1.17.2",
"dot1dTp",        "1.3.6.1.2.1.17.4",
"vrrpOperEntry",        "1.3.6.1.2.1.18.1.3.1",
"experimental",        "1.3.6.1.3",
"private",        "1.3.6.1.4",
"enterprises",        "1.3.6.1.4.1",
"fsrip6",        "1.3.6.1.4.1.2076.3",
"fsrip6Scalars",        "1.3.6.1.4.1.2076.3.1",
"fsrip6RoutePreference",        "1.3.6.1.4.1.2076.3.1.1",
"fsrip6GlobalDebug",        "1.3.6.1.4.1.2076.3.1.2",
"fsrip6GlobalInstanceIndex",        "1.3.6.1.4.1.2076.3.1.3",
"fsrip6PeerFilter",        "1.3.6.1.4.1.2076.3.1.4",
"fsrip6AdvFilter",        "1.3.6.1.4.1.2076.3.1.5",
"fsRip6RRDAdminStatus",        "1.3.6.1.4.1.2076.3.1.6",
"fsrip6RRDProtoMaskForEnable",        "1.3.6.1.4.1.2076.3.1.7",
"fsrip6RRDSrcProtoMaskForDisable",        "1.3.6.1.4.1.2076.3.1.8",
"fsrip6RRDRouteDefMetric",        "1.3.6.1.4.1.2076.3.1.9",
"fsrip6RRDRouteMapName",        "1.3.6.1.4.1.2076.3.1.10",
"fsrip6Tables",        "1.3.6.1.4.1.2076.3.2",
"fsrip6InstanceTable",        "1.3.6.1.4.1.2076.3.2.1",
"fsrip6InstanceEntry",        "1.3.6.1.4.1.2076.3.2.1.1",
"fsrip6InstanceIndex",        "1.3.6.1.4.1.2076.3.2.1.1.1",
"fsrip6InstanceStatus",        "1.3.6.1.4.1.2076.3.2.1.1.2",
"fsrip6InstIfMapTable",        "1.3.6.1.4.1.2076.3.2.2",
"fsrip6InstIfMapEntry",        "1.3.6.1.4.1.2076.3.2.2.1",
"fsrip6IfIndex",        "1.3.6.1.4.1.2076.3.2.2.1.1",
"fsrip6InstIfMapInstId",        "1.3.6.1.4.1.2076.3.2.2.1.2",
"fsrip6InstIfMapIfAtchStatus",        "1.3.6.1.4.1.2076.3.2.2.1.3",
"fsrip6RipIfTable",        "1.3.6.1.4.1.2076.3.2.3",
"fsrip6RipIfEntry",        "1.3.6.1.4.1.2076.3.2.3.1",
"fsrip6RipIfIndex",        "1.3.6.1.4.1.2076.3.2.3.1.1",
"fsrip6RipIfProfileIndex",        "1.3.6.1.4.1.2076.3.2.3.1.2",
"fsrip6RipIfCost",        "1.3.6.1.4.1.2076.3.2.3.1.3",
"fsrip6RipIfOperStatus",        "1.3.6.1.4.1.2076.3.2.3.1.4",
"fsrip6RipIfProtocolEnable",        "1.3.6.1.4.1.2076.3.2.3.1.5",
"fsrip6RipIfInMessages",        "1.3.6.1.4.1.2076.3.2.3.1.6",
"fsrip6RipIfInRequests",        "1.3.6.1.4.1.2076.3.2.3.1.7",
"fsrip6RipIfInResponses",        "1.3.6.1.4.1.2076.3.2.3.1.8",
"fsrip6RipIfUnknownCmds",        "1.3.6.1.4.1.2076.3.2.3.1.9",
"fsrip6RipIfInOtherVer",        "1.3.6.1.4.1.2076.3.2.3.1.10",
"fsrip6RipIfInDiscards",        "1.3.6.1.4.1.2076.3.2.3.1.11",
"fsrip6RipIfOutMessages",        "1.3.6.1.4.1.2076.3.2.3.1.12",
"fsrip6RipIfOutRequests",        "1.3.6.1.4.1.2076.3.2.3.1.13",
"fsrip6RipIfOutResponses",        "1.3.6.1.4.1.2076.3.2.3.1.14",
"fsrip6RipIfOutTrigUpdates",        "1.3.6.1.4.1.2076.3.2.3.1.15",
"fsrip6RipIfDefRouteAdvt",        "1.3.6.1.4.1.2076.3.2.3.1.16",
"fsrip6RipProfileTable",        "1.3.6.1.4.1.2076.3.2.4",
"fsrip6RipProfileEntry",        "1.3.6.1.4.1.2076.3.2.4.1",
"fsrip6RipProfileIndex",        "1.3.6.1.4.1.2076.3.2.4.1.1",
"fsrip6RipProfileStatus",        "1.3.6.1.4.1.2076.3.2.4.1.2",
"fsrip6RipProfileHorizon",        "1.3.6.1.4.1.2076.3.2.4.1.3",
"fsrip6RipProfilePeriodicUpdTime",        "1.3.6.1.4.1.2076.3.2.4.1.4",
"fsrip6RipProfileTrigDelayTime",        "1.3.6.1.4.1.2076.3.2.4.1.5",
"fsrip6RipProfileRouteAge",        "1.3.6.1.4.1.2076.3.2.4.1.6",
"fsrip6RipProfileGarbageCollectTime",        "1.3.6.1.4.1.2076.3.2.4.1.7",
"fsrip6RipRouteTable",        "1.3.6.1.4.1.2076.3.2.5",
"fsrip6RipRouteEntry",        "1.3.6.1.4.1.2076.3.2.5.1",
"fsrip6RipRouteDest",        "1.3.6.1.4.1.2076.3.2.5.1.1",
"fsrip6RipRoutePfxLength",        "1.3.6.1.4.1.2076.3.2.5.1.2",
"fsrip6RipRouteProtocol",        "1.3.6.1.4.1.2076.3.2.5.1.3",
"fsrip6RipRouteIfIndex",        "1.3.6.1.4.1.2076.3.2.5.1.4",
"fsrip6RipRouteNextHop",        "1.3.6.1.4.1.2076.3.2.5.1.5",
"fsrip6RipRouteMetric",        "1.3.6.1.4.1.2076.3.2.5.1.6",
"fsrip6RipRouteTag",        "1.3.6.1.4.1.2076.3.2.5.1.7",
"fsrip6RipRouteAge",        "1.3.6.1.4.1.2076.3.2.5.1.8",
"fsrip6RipPeerTable",        "1.3.6.1.4.1.2076.3.2.6",
"fsrip6RipPeerEntry",        "1.3.6.1.4.1.2076.3.2.6.1",
"fsrip6RipPeerAddr",        "1.3.6.1.4.1.2076.3.2.6.1.1",
"fsrip6RipPeerEntryStatus",        "1.3.6.1.4.1.2076.3.2.6.1.2",
"fsrip6RipAdvFilterTable",        "1.3.6.1.4.1.2076.3.2.7",
"fsrip6RipAdvFilterEntry",        "1.3.6.1.4.1.2076.3.2.7.1",
"fsrip6RipAdvFilterAddress",        "1.3.6.1.4.1.2076.3.2.7.1.1",
"fsrip6RipAdvFilterStatus",        "1.3.6.1.4.1.2076.3.2.7.1.2",
"fsrip6DistInOutRouteMap",        "1.3.6.1.4.1.2076.3.3",
"fsRip6DistInOutRouteMapTable",        "1.3.6.1.4.1.2076.3.3.1",
"fsRip6DistInOutRouteMapEntry",        "1.3.6.1.4.1.2076.3.3.1.1",
"fsRip6DistInOutRouteMapName",        "1.3.6.1.4.1.2076.3.3.1.1.1",
"fsRip6DistInOutRouteMapType",        "1.3.6.1.4.1.2076.3.3.1.1.3",
"fsRip6DistInOutRouteMapValue",        "1.3.6.1.4.1.2076.3.3.1.1.4",
"fsRip6DistInOutRouteMapRowStatus",        "1.3.6.1.4.1.2076.3.3.1.1.5",
"issExt",        "1.3.6.1.4.1.2076.81.8",
"fsDot1dBridge",        "1.3.6.1.4.1.2076.116",
"fsDot1dStp",        "1.3.6.1.4.1.2076.116.2",
"fsDot1dTp",        "1.3.6.1.4.1.2076.116.4",
"security",        "1.3.6.1.5",
"snmpV2",        "1.3.6.1.6",
"snmpDomains",        "1.3.6.1.6.1",
"snmpProxys",        "1.3.6.1.6.2",
"snmpModules",        "1.3.6.1.6.3",
"snmpAuthProtocols",        "1.3.6.1.6.3.10.1.1",
"snmpPrivProtocols",        "1.3.6.1.6.3.10.1.2",
"ieee802dot1mibs",        "1.111.2.802.1",
"joint-iso-ccitt",        "2",
0,0
};

#endif /* _SNMP_MIB_H */
