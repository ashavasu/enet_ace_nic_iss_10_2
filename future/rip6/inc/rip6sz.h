/* $Id: rip6sz.h,v 1.6 2013/11/22 11:39:54 siva Exp $*/
enum {
    MAX_RIP6_BUF_SIZING_ID,
    MAX_RIP6_IFACES_INSTANCE_MAP_SIZING_ID,
    MAX_RIP6_IFACES_SIZING_ID,
    MAX_RIP6_INSTANCE_SIZING_ID,
    MAX_RIP6_PROFILES_SIZING_ID,
    MAX_RIP6_RM_QUEUE_DEPTH_SIZING_ID,
    MAX_RIP6_ROUTE_ENTRIES_SIZING_ID,
    MAX_RIP6_RRD_ROUTES_SIZING_ID,
    MAX_RIP6_IF_INFO_ENTRIES_SIZING_ID,
    MAX_RIP6_INST_IF_ENTRIES_SIZING_ID,
    RIP6_MAX_SIZING_ID
};


#ifdef  _RIP6SZ_C
tMemPoolId RIP6MemPoolIds[ RIP6_MAX_SIZING_ID];
INT4  Rip6SizingMemCreateMemPools(VOID);
VOID  Rip6SizingMemDeleteMemPools(VOID);
INT4  Rip6SzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _RIP6SZ_C  */
extern tMemPoolId RIP6MemPoolIds[ ];
extern INT4  Rip6SizingMemCreateMemPools(VOID);
extern VOID  Rip6SizingMemDeleteMemPools(VOID);
extern INT4  Rip6SzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _RIP6SZ_C  */


#ifdef  _RIP6SZ_C
tFsModSizingParams FsRIP6SizingParams [] = {
{ "tRip6Buf", "MAX_RIP6_BUF", sizeof(tRip6Buf),MAX_RIP6_BUF, MAX_RIP6_BUF,0 },
{ "tInstanceInterfaceMap", "MAX_RIP6_IFACES_INSTANCE_MAP", sizeof(tInstanceInterfaceMap),MAX_RIP6_IFACES_INSTANCE_MAP, MAX_RIP6_IFACES_INSTANCE_MAP,0 },
{ "tRip6If", "MAX_RIP6_IFACES", sizeof(tRip6If),MAX_RIP6_IFACES, MAX_RIP6_IFACES,0 },
{ "tInstanceDatabase", "MAX_RIP6_INSTANCE", sizeof(tInstanceDatabase),MAX_RIP6_INSTANCE, MAX_RIP6_INSTANCE,0 },
{ "tRip6Profile", "MAX_RIP6_PROFILES", sizeof(tRip6Profile),MAX_RIP6_PROFILES, MAX_RIP6_PROFILES,0 },
{ "tRip6RmMsg", "MAX_RIP6_RM_QUEUE_DEPTH", sizeof(tRip6RmMsg),MAX_RIP6_RM_QUEUE_DEPTH, MAX_RIP6_RM_QUEUE_DEPTH,0 },
{ "tRip6RtEntry", "MAX_RIP6_ROUTE_ENTRIES", sizeof(tRip6RtEntry),MAX_RIP6_ROUTE_ENTRIES, MAX_RIP6_ROUTE_ENTRIES,0 },
{ "tRip6RrdRouteInfo", "MAX_RIP6_RRD_ROUTES", sizeof(tRip6RrdRouteInfo),MAX_RIP6_RRD_ROUTES, MAX_RIP6_RRD_ROUTES,0 },
{ "tRip6IfInfoNode", "MAX_RIP6_IF_INFO_ENTRIES", sizeof(tRip6IfInfoNode),MAX_RIP6_IF_INFO_ENTRIES, MAX_RIP6_IF_INFO_ENTRIES,0 },
{ "tRip6InstIfNode", "MAX_RIP6_INST_IF_ENTRIES", sizeof(tRip6InstIfNode),MAX_RIP6_INST_IF_ENTRIES, MAX_RIP6_INST_IF_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _RIP6SZ_C  */
extern tFsModSizingParams FsRIP6SizingParams [];
#endif /*  _RIP6SZ_C  */


