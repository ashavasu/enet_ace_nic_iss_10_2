/*******************************************************************
 ** Copyright (C) 2006 Aricent Inc . All Rights Reserved
 **
 ** $Id: rip6trace.h,v 1.4 2011/04/07 11:36:40 siva Exp $
 ********************************************************************/
/*
+--------------------------------------------------------------------------+
 * |   FILE  NAME             :  rip6trace.h                                   |
 * |                                                                          |
 * |   PRINCIPAL AUTHOR       :  Aricent Inc.                      |
 * |                                                                          |
 * |   SUBSYSTEM NAME         :  Linux Router                                 |
 * |                                                                          |
 * |   MODULE NAME            :  UtlTrcLog module                             |
 * |                                                                          |
 * |   LANGUAGE               :  C                                            |
 * |                                                                          |
 * |   TARGET ENVIRONMENT     :  ANY                                          |
 * |                                                                          |
 * |   DATE OF FIRST RELEASE  :                                               |
 * |                                                                          |
 * |   DESCRIPTION            :  Provides general purpose macros for tracing  |
 * |                                                                          |
 * |   Author                 :                                               |
 * |                                                                          |
 * +--------------------------------------------------------------------------+
 */

#ifndef _RIP6TRACE_H_
#define _RIP6TRACE_H_

extern UINT4  gu4Rip6DbgMap; 

 /* Common bitmasks for various events. */
#define RIP6_NAME "RIP6"

#define  RIP6_INIT_SHUT_TRC   INIT_SHUT_TRC     
#define  RIP6_MGMT_TRC        MGMT_TRC          
#define  RIP6_DATA_PATH_TRC   DATA_PATH_TRC     
#define  RIP6_CTRL_PLANE_TRC  CONTROL_PLANE_TRC 
#define  RIP6_DATA_CTRL_TRC   DATA_CTRL_PLANE_TRC
#define  RIP6_NOT_TRC         NO_TRC
#define  RIP6_DUMP_TRC        DUMP_TRC          
#define  RIP6_OS_RES_TRC      OS_RESOURCE_TRC   
#define  RIP6_ALL_FAIL_TRC    ALL_FAILURE_TRC   
#define  RIP6_BUFFER_TRC      BUFFER_TRC        
#define  RIP6_MOD_TRC         gu4Rip6DbgMap      
#define  MBSM_RIP6            "MbsmRip6"


 /* General Macros */

#define  RIP6_TRC_ARG   MOD_TRC      
#define  RIP6_TRC_ARG1  MOD_TRC_ARG1 
#define  RIP6_TRC_ARG2  MOD_TRC_ARG2 
#define  RIP6_TRC_ARG3  MOD_TRC_ARG3 
#define  RIP6_TRC_ARG4  MOD_TRC_ARG4 
#define  RIP6_TRC_ARG5  MOD_TRC_ARG5 
#define  RIP6_TRC_ARG6  MOD_TRC_ARG6 

#define  RIP6_MODULE         0x64

/*
 * Error Handling related constants
 */

#define  ERROR_FATAL          1
#define  ERROR_MINOR          2

#define  ERROR_FATAL_STR      "FATAL"
#define  ERROR_MINOR_STR      "MINOR"

#define  ERR_BUF_ALLOC        0
#define  ERR_BUF_RELEASE      1
#define  ERR_BUF_READ         2
#define  ERR_BUF_WRITE        3
#define  ERR_BUF_PPTR         4
#define  ERR_BUF_WPTR         5
#define  ERR_BUF_WOFFSET      6

#define  ERR_MEM_CREATE       0
#define  ERR_MEM_GET          1
#define  ERR_MEM_RELEASE      2
#define  ERR_MEM_DELETE       3
#define  ERR_MEM_SET_PARAM    4
#define  ERR_MEM_GET_PARAM    5

#define  ERR_TIMER_INIT       0
#define  ERR_TIMER_START      1
#define  ERR_TIMER_STOP       2
#define  ERR_TIMER_RESTART    3
#define  ERR_TIMER_TIMEOUT    4

#define  ERR_GEN_NULL_PTR     0
#define  ERR_GEN_INVALID_VAL  1
#define  ERR_GEN_TQUEUE_FAIL  2



#endif /* _RIP6TRACE_H_ */

