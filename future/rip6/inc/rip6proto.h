/*******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: rip6proto.h,v 1.12 2017/02/21 14:06:42 siva Exp $
 **********************************************************************/
#ifndef _RIP6PROTO_H
#define _RIP6PROTO_H
/*----------------------------------------------------------------------------
 *    PRINCIPAL AUTHOR             :    Aricent Inc. 
 *
 *    SUBSYSTEM NAME               :    IPv6
 *
 *    MODULE NAME                  :    All Modules
 *
 *    LANGUAGE                     :    C
 *
 *    TARGET ENVIRONMENT           :    UNIX
 *
 *    DATE OF FIRST RELEASE        :    DDD-MM-1996
 *
 *    DESCRIPTION                  :    This file contains prototypes of
 *                                      functions which are used by multiple 
 *                                      modules of the RIP6 subsystem.
 *
 *-----------------------------------------------------------------------------
 */

typedef struct _TrieOutParams
{
   tRip6RtEntry *pRtProfile;
}tRip6TrieOutParams;


/*
 * rip6main.c
 *************/

INT4 Rip6SpawnTasks PROTO ((VOID));

INT4 Rip6InstanceCreation PROTO ((UINT4 u2InstanceId));

INT4 Rip6InstanceDeletion PROTO ((UINT4 u2InstanceId));

INT4    Rip6TaskSemInit          PROTO ((VOID));

INT4 Rip6InterfaceInstanceAttach  PROTO ((UINT4 u4InstanceId, UINT4 u4InterfaceId));

INT4 Rip6InterfaceInstanceDetach PROTO ((UINT4 u4InterfaceId));

VOID Rip6RtRelease PROTO ((tRip6RtEntry * pRt));

INT4 Rip6IfEnable   PROTO ((UINT4 u4Index));

INT4 Rip6IfDisable  PROTO ((UINT4 u4Index));

INT4 Rip6IfUp       PROTO ((UINT4 u4Index));

INT4 Rip6IfDown     PROTO ((UINT4 u4Index));

INT4 Rip6IfCreate   PROTO ((UINT4 u4Index));

INT4 Rip6IfDelete   PROTO ((UINT4 u4Index));

INT4 Rip6IfLLAddrEnable         PROTO ((UINT4 u4Index, tIp6Addr *pLLAddr));

INT4 Rip6IfLLAddrDisable        PROTO ((UINT4 u4Index, tIp6Addr *pLLAddr));

INT4 Rip6ProcLinkLocalAddrChg   PROTO ((tIp6Addr *pLLAddr, UINT4 u4Index,
                                        UINT1 u1AddrStatus));

VOID Rip6TimerHandler           PROTO ((VOID));

VOID Rip6WrapperTimer          PROTO ((VOID));

VOID Rip6IntfWrapperTimer          PROTO ((VOID));

INT4 Rip6PeriodicTimeoutHandler PROTO ((tTmrAppTimer * pTimer));

INT4 Rip6PendPeriodicUpdateHandler PROTO ((tTmrAppTimer *pTimer));

INT4 Rip6CreatePktRcvSocket     PROTO ((VOID));

INT4 Rip6ProcessControlMsg      PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));

VOID Rip6WrapperControl PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Rip6TrigDelayHandler       PROTO ((tTmrAppTimer * pTimer));

VOID Rip6RtTimeoutHandler       PROTO ((UINT1 u1Id, tRip6RtEntry * pRtEntry));

VOID Rip6DisableDefRouteAdvt    PROTO ((UINT4 u4Index));

INT4 Rip6SendRouteMapUpdateMsg  PROTO ((UINT1 *pu1RMapName, UINT4 u4Status));

INT4 Rip6ProcIp6AddrChg PROTO ((tIp6Addr * pIp6Addr, UINT1 u1PrefixLen,
                                UINT4 u4Index, UINT1 u1AddrStatus));

INT4 Rip6IfIp6AddrEnable PROTO ((UINT4 u4Index, tIp6Addr * pIp6Addr,
                                 UINT1 u1PrefLen));

INT4 Rip6IfIp6AddrDisable PROTO ((UINT4 u4Index, tIp6Addr * pIp6Addr,
                                  UINT1 u1PrefLen));

/* 
 * rip6rt.c 
 ***********/

tRip6RtEntry    *Rip6RouteFindEntry PROTO ((tIp6Addr * pIp6Dest,
                                            UINT1 u1PrefixLen,
                                            INT1 i1Proto, UINT4 u4Index));
tRip6RtEntry    *Rip6RtEntryFill    PROTO ((tIp6Addr * pDest,
                                            UINT1 u1Prefixlen,
                                            tIp6Addr * pNexthop,
                                            UINT1 u1Metric,
                                            INT1 i1Type, INT1 i1Proto,
                                            UINT2 u2Tag, UINT4 u4Index, UINT1 u1Level));

tRip6RtEntry    *Rip6RtFindEntry    PROTO ((tIp6Addr * pAddr,
                                               UINT1 u1Prefixlen,UINT4 u4Index));

VOID Rip6Shutdown     PROTO ((VOID));


VOID Rip6AgeoutOverIf PROTO ((UINT4 u4Index));

VOID Rip6IfInfoDel    PROTO ((UINT4 u4Index));

VOID rip6_static_add  PROTO ((tIp6Addr * pDest,
                              UINT1 u1Prefixlen,
                              tIp6Addr * pNexthop,
                              UINT1 u1Metric, INT1 i1Type,
                              INT1 i1Proto, UINT4 u4Index));

VOID Rip6SetBestMetricLookup    PROTO ((UINT4));

VOID Rip6SetStaticLookup        PROTO ((UINT4));

VOID Rip6SetDynamicLookup       PROTO ((UINT4));

VOID Rip6EnableProtocol         PROTO ((VOID));

VOID Rip6DisableProtocol        PROTO ((VOID));

VOID Rip6AddToProfTbl           PROTO ((UINT2 u2Index, UINT1 u1Horizon,
                                        UINT4 u4PeriodicTime,
                                        UINT4 u4DelayTime,
                                        UINT4 u4RouteAge, UINT4 u4GcTime));

INT4 Rip6SendRtOnIf      PROTO ((UINT4 u4Index, INT2 i2SendFlag));

INT4 Rip6DelFromProfTbl  PROTO ((UINT2 u2Index));

INT4 Rip6IfTimerStart    PROTO ((UINT1 u1Id, UINT4 u4Index));

VOID Rip6IfInfoAdd       PROTO ((UINT4 u4Index, UINT1 u1Cost,
                                 UINT2 u2ProfIndex));

VOID Rip6RouteAgeout     PROTO ((tRip6RtEntry * pRtEntry));

INT4 Rip6PurgeRtLearnt   PROTO ((UINT1 u1Purge, UINT4 u4Index));

INT4 Rip6RemoveAllRtLearnt   PROTO ((VOID));

VOID Rip6PurgeAllDynamic PROTO ((VOID));

VOID Rip6MIPurgeAll PROTO ((UINT4 u4InstId));

INT4 Rip6SendResponse   PROTO ((UINT2 u2RespFlag,
                                 UINT2 u2DstPort,
                                 UINT4 u4Index,
                                 UINT4 u4Combine, tIp6Addr * pDst));

INT4 Rip6CheckTrigUpdateSend PROTO ((UINT1 u1CheckFlag, UINT4 u4Index,
                                     tRip6RtEntry  *pRtEntry));

/* 
 * rip6ip6if.c 
 *************/

INT1 Rip6Ip6AddrStatusNotify PROTO ((tNetIpv6HliParams * pNetIpv6HlParams));

INT1 Rip6Ip6ifStatusNotify     PROTO ((tNetIpv6HliParams *));

INT4 Rip6SendRtChgNotification PROTO ((tIp6Addr *pIp6Addr, 
                                       UINT1 u1Prefixlen, tIp6Addr *pNextHop, 
                                       UINT1 u1Metric, INT1 i1Proto,
                                       UINT4 u4Index, UINT1 u1Distance,
                                       UINT1 u1Flag));
/*
 * rip6trie.c
 */
 
INT4 Rip6TrieCreate   PROTO ((UINT4 u4Instance));
INT4 Rip6TrieBestEntry   PROTO ((tIp6Addr *pLookupAddr, UINT1 u1Preference,
                                 UINT1 u1PrefixLen,UINT4 u4InstanceId,
                                 tRip6RtEntry **, VOID **));
INT4 Rip6TrieLookupExactEntry PROTO ((tInputParams *, tRip6TrieOutParams *,
                                      tRip6RtEntry *));
INT4 Rip6TriecbBestEntry PROTO ((UINT2 u2KeySize,tInputParams *pInputParams,
                                 tRip6TrieOutParams * pOutParams,
                                 tRip6RtEntry * pAppSpecInfo, tKey Key));

INT4 Rip6TrieAddEntry PROTO ((tRip6RtEntry *, UINT4));
INT4 Rip6TrieDeleteEntry PROTO ((tRip6RtEntry *, UINT4));
INT4 Rip6TriecbAddEntry PROTO ((VOID *, VOID *, tRip6RtEntry **, tRip6RtEntry *));
INT4 Rip6TrieUpdate  PROTO ((tRip6RtEntry *, UINT4 , tRip6RtEntry *));
INT4 Rip6TriecbUpdate PROTO ((tInputParams *, VOID *, tRip6RtEntry **,
                              tRip6RtEntry *, UINT4));
INT4 Rip6TriecbDeleteEntry PROTO ((tInputParams *, tRip6RtEntry **,
                                   VOID *, tRip6RtEntry *, tKey ));
INT4 Rip6TrieDelete PROTO ((UINT4 ));
VOID * Rip6TrieDelCb PROTO ((tInputParams *pInputParams , 
                             VOID (*AppSpecDelFunc) (VOID *),
                             tDeleteOutParams *pDelParams));
VOID Rip6TrieIfDelete PROTO ((VOID *));
VOID Rip6TrieStopTimer PROTO ((tDeleteOutParams *));
INT4 Rip6TrieGetFirstEntry PROTO ((tRip6RtEntry **pRtEntry, VOID **ppNode, 
                                   UINT4 u4InstanceId));
INT4 Rip6TrieGetNextEntry PROTO ((tRip6RtEntry **pRtEntry , VOID *pCurrNode, 
                                  VOID **ppNode, UINT4 u4InstanceId));

INT4 Rip6TrieScan PROTO ((tRip6ScanParam *pScanParams));
VOID *Rip6TrieCbInitScanCtx PROTO  ((tInputParams *pInputParams,
                                     VOID (*AppSpecScanFunc) (VOID *),
                                     VOID *pScanOutParams));
VOID Rip6TrieCbDeInitScanCtx PROTO  ((VOID *pScanCtx));
INT4 Rip6TrieCbScan PROTO ((VOID  *pScanCtx, VOID **AppPtr, tKey Key));

/*
 * rip6netip6.c
 */
 
UINT4 Rip6GetIfMtu PROTO ((UINT4 u4IfIndex));
tIp6Addr * Rip6GetLlocalAddr PROTO ((UINT4 u4IfIndex)); 
INT4 Rip6GetGlobalAddr PROTO ((UINT4 u4IfIndex, tIp6Addr *pDstAddr, tIp6Addr *pSrcAddr));
INT4 Rip6IsPktToMe PROTO ((UINT1 *pu1Type, UINT1 u1LlocalChk, tIp6Addr *pDstAddr,
                           tNetIpv6IfInfo *pNetIpv6IfInfo));
INT4 Rip6IsOurAddr PROTO ((tIp6Addr *pDstAddr, UINT4 *pu4Index));

INT4 Rip6RecvRtm6Msg PROTO ((tRtm6RespInfo *pRtm6RespInfo, tRtm6MsgHdr *pRtm6Hdr));
INT4 Rip6Rtm6Register PROTO ((VOID));
VOID Rip6InitRedistribution PROTO ((VOID));
INT4 Rip6SendMsgToRTMQueue PROTO ((UINT4 u4RRDSrcProtoMaskEnable, UINT1 u1MessageType, UINT1 *pu1RMapName));
INT4 Rip6Rtm6RtAdd PROTO ((tRtm6RespInfo *pRtm6RespInfo, tRtm6MsgHdr  *pRtm6Hdr));
INT4 Rip6Rtm6RtDel PROTO ((tRtm6RespInfo *pRtm6RespInfo, tRtm6MsgHdr  *pRtm6Hdr));
VOID Rip6CreateKey PROTO ((tRip6RtKey *Rip6Key, tRip6RtEntry *pRtEntry));
INT4 Rip6DeleteRedisRoutes PROTO ((INT4)); 
INT4 Rip6ComapreRoutes PROTO ((tRBElem *pRt1, tRBElem *pRt2));
/* 
 * rip6buf.c 
 ************/

VOID *Rip6BufRead PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 *pData,
                          UINT4 u4Offset, UINT4 u4Size, UINT1 u1Copy));

INT4 Rip6BufWrite PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, UINT1 *pData, 
                          UINT4 u4Offset, UINT4 u4Size, UINT1 u1Copy));

VOID Rip6TmrStart PROTO ((UINT1 u1TimerId,
                          tTimerListId timerListId,
                          tTmrAppTimer * pAppTimer, 
                          UINT4 u4Duration));

VOID Rip6TmrStop  PROTO ((UINT1 u1TimerId,
                          tTimerListId timerListId,
                          tTmrAppTimer * pAppTimer));

UINT4 Rip6TimerStart PROTO ((tRip6RtEntry *pRtEntry, UINT1 u1TimerId,
                          UINT4 u4Duration));

UINT4 Rip6TimerStop  PROTO ((tRip6RtEntry *pRtEntry));

UINT4 Rip6TimerUpdate PROTO ((tRip6RtEntry *pRtEntry, UINT1 u1TimerId,
                          UINT4 u4Duration));

INT4 TmrRip6MIRemainingTime PROTO ((tRip6RtEntry *pRtEntry, UINT4 *pu4RemainingTime));


/* 
 * rip6snif.c 
 *************/

INT4 Rip6ProfileGetNextIndex PROTO ((UINT4 u4InstanceId, UINT2 *u2ProfIndex));

INT4 Rip6ProfileGetFirstIndex PROTO ((UINT2 *u2ProfIndex));

INT1 Rip6ProfileEntryExists PROTO ((UINT4 u4InstanceId, UINT2 u2ProfIndex));

INT1 Rip6IfEntryExists PROTO ((UINT4 u4IfIndex));

INT1 Rip6IsPrefixGreater PROTO ((tIp6Addr * pIp6Addr, UINT1 u1PrefixLen, 
                                 INT1 i1Proto, tRip6RtEntry * pRtEntry));

INT1 Rip6IsPrefixInBetween PROTO ((tIp6Addr * pIp6Addr,
                                       UINT1 u1PrefixLen, INT1 i1Proto,
                                       tRip6RtEntry * pRtTmp,
                                       tRip6RtEntry * pRtEntry));


/* 
 *rip6io.c 
 *********/

INT4 Rip6Input       PROTO ((tRip6Udp6Params * p_udp6_io,
                             tCRU_BUF_CHAIN_HEADER * pBuf));
        
VOID Rip6WrapperInput  PROTO ((tRip6Udp6Params * p_udp6_io,
                             tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Rip6RcvRequest  PROTO ((UINT2 u2SrcPort,
                             UINT2 u2DstPort,
                             UINT4 u4Index, UINT4 u4Len,
                             tIp6Addr * pSrc, tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Rip6GetPeerFilterStatus PROTO ((tInstanceDatabase *pContext, 
   tIp6Addr *pSrcAddr));


INT4 Rip6RcvResponse PROTO ((UINT1 u1Hoplimit, UINT4 u4Index,
                             UINT4 u4Len, tIp6Addr * pSrc,
                             tIp6Addr *pDst, tCRU_BUF_CHAIN_HEADER * pBuf)); 

INT4 Rip6CheckTrigUpdateSendForResponseMsg PROTO ((UINT4 u4InstanceId, UINT1 u1BulkDeletionFlag));

INT4 Rip6SendRequest PROTO ((UINT4 u4Index));

INT4 Rip6SendDatagram PROTO ((UINT1 u1SendFlag,
                              UINT2 u2DstPort,
                              UINT4 u4Index, UINT4 u4Len,
                              tIp6Addr * pDstAddr,
                              tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Rip6ResponseProcessRte PROTO ((UINT1 *pu1TrigSend,
                                    UINT4 u4Index, UINT4 u4Rte,
                                    tIp6Addr * pSrc,
                                    tCRU_BUF_CHAIN_HEADER * pBuf));

INT4 Rip6SendRtLearntOverIf PROTO ((UINT2 u2RespFlag,
                                    UINT1 u1Horizon,
                                    UINT4 u4Index, UINT4 u4Combine,
                                    UINT2 u2DstPort,
                                    UINT4 *pu4RouteCounter,
                                    UINT4 u4MaxNoOfRte,
                                    UINT4 *pu4Len,
                                    tIp6Addr * pDstAddr,
                                    tCRU_BUF_CHAIN_HEADER ** pBuf,
                                    tRip6RtEntry *pBestRt,
                                    UINT4 u4InstanceId));
INT4 Rip6SendUpdateOrNot     PROTO ((tRip6RtEntry *pRtEntry, UINT4 u4Interface, UINT1 u1BulkDeletionFlag));
          
INT4 Rip6PutHdrInBuf         PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                     UINT1 u1Flag));

INT4 Rip6PutRteInBuf         PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                     UINT4 u4Index,
                                     UINT1 u1Horizon,
                                     UINT2 u2RespFlag,
                                     tRip6RtEntry * pRtEntry));

tCRU_BUF_CHAIN_HEADER *Rip6BufAlloc PROTO ((UINT4 u4MaxNoRte, UINT4 u4InstanceId));

INT4 Rip6AddPeerEntry PROTO ((UINT4 , tIp6Addr * ));
INT4 Rip6DeletePeerEntry PROTO ((UINT4 , tIp6Addr * ));
INT4 Rip6IsTrustedPeer PROTO ((UINT4 , tIp6Addr * ));
INT4 Rip6AddAdvFilterEntry PROTO ((UINT4 , tIp6Addr * ));
INT4 Rip6DeleteAdvFilterEntry PROTO ((UINT4 , tIp6Addr * ));
INT4 Rip6IsNetFilterEnabled PROTO ((UINT4 , tIp6Addr * ));
INT4 Rip6SendExtRoutes PROTO ((UINT2 , UINT2 , UINT4 , tIp6Addr *));
INT4  Rip6CheckRouteInRbTree PROTO ((tIp6Addr *pAddr, UINT1 u1PrefixLen));
UINT1 Rip6FilterRouteSource PROTO ((tIp6Addr* pSrcAddr));

INT1 Rip6ApplyInOutFilter PROTO (( tFilteringRMap*, tRip6RtEntry* ));

/*
 * rip6red.c
 * ***********/

VOID Rip6RedDynDataDescInit PROTO ((VOID));
VOID Rip6RedDescrTblInit PROTO ((VOID));
VOID Rip6RedDbNodeInit PROTO ((tDbTblNode * pDBNode, UINT4 u4Type));
INT4 Rip6RedInitGlobalInfo PROTO ((VOID));
INT4 Rip6RedDeInitGlobalInfo PROTO ((VOID));
INT4 Rip6RedRmCallBack PROTO ((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
VOID Rip6RedHandleRmEvents PROTO ((UINT1 u1MsgProcessFlag));
VOID Rip6RedHandleGoActive PROTO ((VOID));
VOID Rip6RedHandleGoStandby PROTO ((tRip6RmMsg * pMsg));
VOID Rip6RedHandleIdleToActive PROTO ((VOID));
VOID Rip6RedHandleIdleToStandby PROTO ((VOID));
INT4 Rip6RedStartTimers PROTO ((VOID));
INT4 Rip6RedNotifyRestartRTM PROTO ((UINT4  u4CxtId));
VOID Rip6RedHandleStandbyToActive PROTO ((VOID));
VOID Rip6RedHandleActiveToStandby PROTO ((VOID));
VOID Rip6RedProcessPeerMsgAtActive PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
VOID Rip6RedProcessPeerMsgAtStandby PROTO ((tRmMsg * pMsg, UINT2 u2DataLen));
INT4 Rip6RedRmReleaseMemoryForMsg PROTO ((UINT1 *pu1Block));
INT4 Rip6RedSendMsgToRm PROTO ((tRmMsg * pMsg, UINT2 u2Length));
VOID Rip6RedSendBulkReqMsg PROTO ((VOID));
VOID Rip6RedDbUtilAddTblNode PROTO ((tDbTblDescriptor *pRip6DataDesc, tDbTblNode *pRip6DbNode));
VOID Rip6RedSyncDynInfo PROTO ((VOID));
VOID Rip6RedAddAllRouteNodeInDbTbl PROTO ((VOID));
VOID Rip6RedSendBulkUpdMsg PROTO ((VOID));
VOID Rip6RedSendBulkUpdTailMsg PROTO ((VOID));
VOID Rip6RedProcessBulkTailMsg PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet));
VOID Rip6RedProcessDynamicRtInfo PROTO ((tRmMsg * pMsg, UINT4 *pu4OffSet));


/*---------------------------------------------------------------*/
/*-----------------rip6rbutl.c-----------------------------------*/
/*---------------------------------------------------------------*/
INT4
Rip6CreateRip6IfInfoTbl PROTO ((VOID));
VOID
Rip6DeleteRip6IfInfoTbl PROTO ((VOID));
INT4
Rip6IfInfoTblCmp PROTO ((tRBElem * Node, tRBElem * NodeIn));
INT4
Rip6AddRip6IfInfoTblEntry PROTO ((UINT4 u4Index, 
                                  tRip6IfInfoNode **ppRip6IfRaNode));
tRip6IfInfoNode *
Rip6GetRip6IfInfoTblEntry PROTO ((UINT4 u4Index));
tRip6IfInfoNode  *
Rip6GetFirstRip6IfInfoTblEntry PROTO ((VOID));
tRip6IfInfoNode *
Rip6GetNextRip6IfInfoTblEntry PROTO ((UINT4 u4Index));
INT4
Rip6DelRip6IfInfoTblEntry PROTO ((tRip6IfInfoNode *pRip6IfRaNode));
VOID
Rip6DelAllRip6IfInfoTblEntries PROTO ((VOID));
VOID
Rip6SetInstIfaceMapInRip6IfInfoTbl PROTO ((UINT4 u4Index, 
        tInstanceInterfaceMap *pRip6InstIfaceMap));
tInstanceInterfaceMap *
Rip6GetInstIfaceMapInRip6IfInfoTbl PROTO ((UINT4 u4Index));
VOID
Rip6SetIfInRip6IfInfoTbl PROTO ((UINT4 u4Index, tRip6If *pRip6If));
tRip6If *
Rip6GetIfInRip6IfInfoTbl PROTO ((UINT4 u4Index));
INT4
Rip6UtilRBTreeCreateStub PROTO ((VOID));
INT4
Rip6CreateRip6InstIfTbl PROTO ((VOID));
VOID
Rip6DeleteRip6InstIfTbl PROTO ((VOID));
INT4
Rip6InstIfTblCmp PROTO ((tRBElem * Node, tRBElem * NodeIn));
INT4
Rip6AddRip6InstIfTblEntry PROTO ((UINT4 u4InstId, UINT4 u4Index, 
                                  tRip6InstIfNode **ppRip6InstIfNode));
tRip6InstIfNode *
Rip6GetRip6InstIfTblEntry PROTO ((UINT4 u4InstId, UINT4 u4Index));
tRip6InstIfNode  *
Rip6GetFirstRip6InstIfTblEntry PROTO ((VOID));
tRip6InstIfNode *
Rip6GetNextRip6InstIfTblEntry PROTO ((UINT4 u4InstId, UINT4 u4Index));
INT4
Rip6DelRip6InstIfTblEntry PROTO ((tRip6InstIfNode *pRip6InstIfNode));
VOID
Rip6DelAllRip6InstIfTblEntries PROTO ((VOID));
VOID
Rip6SetIfEntryInRip6InstIfTbl PROTO ((UINT4 u4InstId, UINT4 u4Index,
        tRip6If *pRip6If));
tRip6If *
Rip6GetIfEntryInRip6InstIfTbl PROTO ((UINT4 u4InstId, UINT4 u4Index));
INT4 Rip6CheckEcmpRoutePresentInDb (tIp6Addr * pDestination, UINT1 u1Prefixlen,
                                    UINT1 u1Metric, UINT1 * u1NextMetric);
INT4 Rip6TriggerRouteChangeforEcmpRoute (tIp6Addr * dst, UINT1 u1Prefixlen,
                                         UINT1 u1Metric);

#endif /*_RIP6PROTO_H */
