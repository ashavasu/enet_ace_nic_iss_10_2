
/* $Id: rip6cmd.def,v 1.12 2014/10/13 12:06:01 siva Exp $*/
DEFINE GROUP: RIP6_PEXCFG_GRP 
COMMAND : debug ipv6 rip { all | data | control}
   ACTION  : {
                 UINT4 u4Value = 0;
                 if ($3 != NULL) 
                 {
                     u4Value = RIP6_ALL_TRC;
                 }
                 else if ($4 != NULL)
                 {
                     u4Value = RIP6_DATA_PATH;
                 }
                 else if ($5 != NULL)
                 {
                     u4Value = RIP6_CONTROL_PLANE;
                 }
               
                 cli_process_rip6_cmd (CliHandle, CLI_RIP6_DEBUG, NULL,u4Value);
             }
   SYNTAX  : debug ipv6 rip { all | data | control }
   PRVID   : 15
   HELP    : IPv6 RIP routing protocol debugging
CXT_HELP : debug Configures trace for the specified protocol|
           ipv6 IPV6 related configuration|
           rip RIP related configuration|
           all All resources| 
           data Data path messages| 
           control Control plane messages|
           <CR> IPv6 RIP routing protocol debugging

   COMMAND : no debug ipv6 rip
   ACTION  : { 
              UINT4 u4Value = RIP6_NO_TRC;
              cli_process_rip6_cmd (CliHandle, CLI_RIP6_DEBUG, NULL, u4Value);
             }
   SYNTAX  : no debug ipv6 rip
   PRVID   : 15
   HELP    : Disable IPv6 RIP routing protocol debugging
CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
           debug Configures trace for the specified protocol|
           ipv6 IPV6 related configuration|
           rip RIP related configuration|
           <CR> Disable IPv6 RIP routing protocol debugging
  

   COMMAND : show ipv6 rip peer-table-status 
   ACTION  : {
	    	cli_process_rip6_cmd(CliHandle, CLI_RIP6_PEER_FLTR_STATUS_SHOW,
				NULL);
	     }
   SYNTAX  : show ipv6 rip peer-table-status
   HELP    : Display the status of the peers filter.
   CXT_HELP: show Peers Responses configured in Peer Table status |
	         ipv6 IPV6 related configuration |
             rip RIP related configuration |
	         peer-table-status Filters responses to be displayed |
	         <CR> Shows Enables / Disables responses from Peers.

   COMMAND : show ipv6 rip peer-trig-update-interval
   ACTION  : {
		cli_process_rip6_cmd(CliHandle, 
				CLI_RIP6_TRIG_UPDATE_INTERVAL_SHOW, NULL);
	     }
   SYNTAX  : show ipv6 rip peer-trig-update-interval
   HELP    : Display the  delayed triggered time interval.
   CXT_HELP: show the delayed triggered time interval |
	         ipv6 IPV6 related configuration |
             rip RIP related configuration |
	         peer-trig-update-interval peer`s time interval, updates are delayed after one triggered |
	         <CR> Shows the Profile Trigger Delay Timer.
 
   COMMAND : show ipv6 rip {[database]}
   ACTION  : {
                 UINT4 u4Cmd;
                 if ($3 != NULL)
                 {
                     u4Cmd = CLI_RIP6_ROUTE_SHOW;
                 }
                 else
                 {
                     u4Cmd = CLI_RIP6_PROFILE_SHOW;
                 }
                 cli_process_rip6_cmd (CliHandle, CLI_RIP6_SHOW_RIP, NULL, 
                                      u4Cmd);
             }    
   SYNTAX  : show ipv6 rip [ database ]
   HELP    : Display IPv6 Local RIB and routing protocol information 
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPV6 related configuration|
              rip RIP related configuration|
              database IPv6 RIP protocol database|
              <CR> Display IPv6 Local RIB and routing protocol information

   COMMAND : show ipv6 rip stats
   ACTION  : cli_process_rip6_cmd (CliHandle, CLI_RIP6_IF_STATS_SHOW, NULL);
   SYNTAX  : show ipv6 rip stats 
   HELP    : Displays all the interface statistics.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPV6 related configuration|
              rip RIP related configuration|
              stats Interface statistics|
              <CR> Displays all the interface statistics

   COMMAND : show ipv6 rip filter
   ACTION  : cli_process_rip6_cmd (CliHandle, CLI_RIP6_PEERADV_TABLE_SHOW, 
                                                                      NULL);
   SYNTAX  : show ipv6 rip filter
   HELP    : Displays peer and Advfilter table.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPV6 related configuration|
              rip RIP related configuration|
              filter Peer and Advfilter table|
              <CR> Displays peer and Advfilter table

#ifdef RIP6_TEST_WANTED

COMMAND	: execute rip6 ut { all | [case <integer> in] file {rip6red} }
ACTION	:
    {
        UINT4 u4FileNumber = 0;
        if( $8 != NULL )
        {
            u4FileNumber = 1;
        }
        cli_process_rip6_test_cmd (CliHandle, NULL,
                            $3, &u4FileNumber, $5);
    }
SYNTAX	: execute rip6 ut { all | [case <integer> in] file {rip6red}
PRVID	: 1
HELP	: RIP6 Unit Testing
CXT_HELP : execute Executes a framework |
           rip6 RIP6 related configuration |
           ut Unit Testing related configuration |
           all All test cases |
           case Specifies a test case |
           (1-x) Test case number in file (x is the max number of cases in a file) |
           in In |
           file File name |
           <CR> Executes the specified test cases

#endif

END GROUP

DEFINE GROUP: RIP6_UEXCFG_GRP 

   COMMAND : show ipv6 rip {[database]}
   ACTION  : {
                 UINT4 u4Cmd;
                 if ($3 != NULL)
                 {
                     u4Cmd = CLI_RIP6_ROUTE_SHOW;
                 }
                 else
                 {
                     u4Cmd = CLI_RIP6_PROFILE_SHOW;
                 }
                 cli_process_rip6_cmd (CliHandle, CLI_RIP6_SHOW_RIP, NULL, 
                                      u4Cmd);
             }    
   SYNTAX  : show ipv6 rip {database}
   HELP    : Display IPv6 Local RIB and routing protocol information 
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPV6 related configuration|
              rip RIP related configuration|
              database IPv6 RIP protocol database|
              <CR> Display IPv6 Local RIB and routing protocol information

   COMMAND : show ipv6 rip stats
   ACTION  : cli_process_rip6_cmd (CliHandle, CLI_RIP6_IF_STATS_SHOW, NULL);
   SYNTAX  : show ipv6 rip stats 
   HELP    : Displays all the interface statistics.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPV6 related configuration|
              rip RIP related configuration|
              stats Interface statistics|
              <CR> Displays all the interface statistics

   COMMAND : show ipv6 rip filter
   ACTION  : cli_process_rip6_cmd (CliHandle, CLI_RIP6_PEERADV_TABLE_SHOW, 
                                                                      NULL);
   SYNTAX  : show ipv6 rip filter
   HELP    : Displays peer and Advfilter table.
   CXT_HELP : show Displays the configuration/statistics/general information|
              ipv6 IPV6 related configuration|
              rip RIP related configuration|
              filter Peer and Advfilter table|
              <CR> Displays peer and Advfilter table

END GROUP


DEFINE GROUP: RIP6_GLBCFG_GRP 

   COMMAND : ipv6 router rip 
   ACTION  : { 
                  cli_process_rip6_cmd (CliHandle, CLI_RIP6_ROUTER, NULL);
             }
   SYNTAX  : ipv6 router rip 
   PRVID   : 15
   HELP    : Enter the router configuration mode
   CXT_HELP : ipv6 Configures IPV6 related protocol|
              router Router configuration mode|
              rip Enables RIP6|
              <CR> Enter the router configuration mode

   COMMAND : no ipv6 router rip 
   ACTION  : {
                 cli_process_rip6_cmd (CliHandle, CLI_RIP6_NO_ROUTER, NULL);
             }    
   SYNTAX  : no ipv6 router rip
   PRVID   : 15
   HELP    : Disable RIP6 on all the interfaces 
   CXT_HELP : no Disables the configuration/deletes the entry/resets to default value|
              ipv6 IPV6 related configuration|
              router Router configuration mode|
              rip Enables RIP6|
              <CR> Disable RIP6 on all the interfaces
END GROUP

DEFINE GROUP: RIP6_IVRINTCFG_GRP 

   COMMAND : ipv6 split-horizon 
   ACTION  : {
                cli_process_rip6_cmd(CliHandle, CLI_RIP6_SPLIT_HORIZON, 
                                                             NULL, $2);
             }                          
   SYNTAX  : ipv6 split-horizon 
   PRVID   : 15
   HELP    : Enable the split horizon updates
   CXT_HELP : ipv6 Configures IPV6 related protocol|
              split-horizon Split horizon updates|
              <CR> Enable the split horizon updates

   COMMAND : no ipv6 split-horizon
   ACTION  : {
               cli_process_rip6_cmd(CliHandle, CLI_RIP6_NO_SPLIT_HORIZON, NULL); 
             }                          
   SYNTAX  : no ipv6 split-horizon 
   PRVID   : 15
   HELP    : Disable the split horizon  updates
   CXT_HELP : no Disables the configuration/deletes the entry/resets to default value|
              ipv6 IPV6 related configuration|
              split-horizon Split horizon updates|
              <CR> Disable the split horizon  updates

   COMMAND : ipv6 rip enable
   ACTION  : {
             cli_process_rip6_cmd(CliHandle, CLI_RIP6_IF, NULL,CLI_RIP6_ENABLE); 
             }                          
   SYNTAX  : ipv6 rip enable
   PRVID   : 15
   HELP    : Enable RIP Routing
   CXT_HELP : ipv6 Configures IPV6 related protocol|
              rip RIP related configuration|
              enable Enables RIP routing|
              <CR> Enable RIP Routing

   COMMAND : no ipv6 rip
   ACTION  : {
            cli_process_rip6_cmd(CliHandle, CLI_RIP6_IF, NULL,CLI_RIP6_DISABLE); 
             }                          
   SYNTAX  : no ipv6 rip
   PRVID   : 15
   HELP    : Disable RIP Routing
   CXT_HELP : no Disables the configuration/deletes the entry/resets to default value|
              ipv6 IPV6 related configuration|
              rip RIP related configuration|
              <CR> Disable RIP Routing

   COMMAND : ipv6 poison reverse 
   ACTION  : {
                cli_process_rip6_cmd(CliHandle, CLI_RIP6_POISON_REVERSE, NULL, CLI_RIP6_ENABLE);
             }                          
   SYNTAX  : ipv6 poison reverse 
   PRVID   : 15
   HELP    : Enable poison reverse
   CXT_HELP : ipv6 Configures IPV6 related protocol|
              poison Poison reverse algorithm|
              reverse Poison reverse algorithm|
              <CR> Enable poison reverse

   COMMAND : ipv6 rip default-information originate
   ACTION  : cli_process_rip6_cmd (CliHandle, CLI_RIP6_DEFROUTE,NULL,
CLI_RIP6_TRUE);
   SYNTAX  : ipv6 rip default-information originate
   HELP    : Configure Handling of default route originate
   CXT_HELP : ipv6 Configures IPV6 related protocol|
              rip RIP related configuration|
              default-information Default route originate|
              originate Default route originate|
              <CR> Configure Handling of default route originate

   COMMAND : no ipv6 rip default-information
   ACTION  : cli_process_rip6_cmd (CliHandle, CLI_RIP6_DEFROUTE,NULL ,CLI_RIP6_FALSE);
   SYNTAX  : no ipv6 rip default-information
   HELP    : Disable handling of default route originate
   CXT_HELP : no Disables the configuration/deletes the entry/resets to default value|
              ipv6 IPV6 related configuration|
              rip RIP related configuration|
              default-information Default route originate|
              <CR> Disable handling of default route originate

   COMMAND : ipv6 rip metric-offset <integer (1-15)>
   ACTION  : {
                cli_process_rip6_cmd(CliHandle, CLI_RIP6_METRIC, NULL, $3); 
             }                          
   SYNTAX  : ipv6 rip metric-offset <integer (1-15)>
   PRVID   : 15
   HELP    : Adjust Default metric increment
   CXT_HELP : ipv6 Configures IPV6 related protocol|
              rip RIP related configuration|
              metric-offset Metric value for all redistributed routes|
              <integer(1-15)> Metric value|
              <CR> Adjust Default metric increment
END GROUP

DEFINE GROUP: RIP6_ROUTERCFG_GRP 

   COMMAND : redistribute {static | connected | ospf } [metric <integer(0-16)>] [route-map <string(20)>]
   ACTION  :
            {
               UINT4    u4Proto=0;
               UINT4    u4Metric=0;

               if ( $1 != NULL)
               {
                  u4Proto = RIP6_IMPORT_STATIC;
               }
               else if ( $2 != NULL)
               {
                  u4Proto = RIP6_IMPORT_DIRECT;
               }
               else if ( $3 != NULL)
               {
                  u4Proto = RIP6_IMPORT_OSPF;
               }
               if ($4 != NULL)
               { 
                 u4Metric = *(UINT4 *) $5 ;
                 cli_process_rip6_cmd (CliHandle, CLI_RIP6_REDISTRIBUTE, NULL, u4Proto, u4Metric, CLI_RIP6_ENABLE, $7);
               }
               else 
               {
                 cli_process_rip6_cmd (CliHandle, CLI_RIP6_REDISTRIBUTE, NULL, u4Proto, RIP6_DFLT_COST, CLI_RIP6_ENABLE, $7);
               }   
            }
   SYNTAX  : redistribute { static | connected | ospf } [metric <integer(0-16)>] [route-map <string(20)>]
   HELP    : Sets redistribution of IPv6 prefix from another protocol into RIP6
   CXT_HELP : redistribute Configures redistribution related parameters | 
              static Static Routes |
              connected Directly connected network routes |
              ospf OSPF Routes |
              metric Metric related configuration |
              (0-16) Routing metric|
              route-map Route Map related configuration |
              <string(20)> Route-map name|
              <CR> Sets redistribution of IPv6 prefix from another protocol into RIP6


   COMMAND : no redistribute {static | connected | ospf } [route-map <string(20)>]
   ACTION  :
            {
               UINT4    u4Proto=0;
               UINT4    u4Metric=0;  

               if ( $2 != NULL)
               {
                  u4Proto = RIP6_IMPORT_STATIC;
               }
               else if ( $3 != NULL)
               {
                  u4Proto = RIP6_IMPORT_DIRECT;
               }
               else if ( $4 != NULL)
               {
                  u4Proto = RIP6_IMPORT_OSPF;
               }
               cli_process_rip6_cmd (CliHandle, CLI_RIP6_REDISTRIBUTE, NULL, u4Proto, u4Metric, CLI_RIP6_DISABLE, $6);
            }
   SYNTAX  : no redistribute {static|connected|ospf} [route-map <string(20)>]
   HELP    : Resets redistribution of IPv6 prefix from another protocol into RIP6
   CXT_HELP : no Disables the configuration/deletes the entry/resets to default value|
              redistribute Route Redistribution related configuration |
              static Static Routes |
              connected Directly connected network routes |
              ospf OSPF Routes |
              route-map Route Map related configuration |
              <string(20)> Route-map name|
              <CR> Resets redistribution of IPv6 prefix from another protocol into RIP6

   COMMAND : redistribute { isis } [{level-1 | level-2 | level-1-2}] [metric <integer(0-16)>] [route-map <string(20)>]
   ACTION  :
            {
               UINT4    u4Proto=0;
               UINT4    u4Metric=0;

               if ( $1 != NULL)
               {
                  u4Proto = RIP6_IMPORT_ISISL2;
                  if ( $2 != NULL)
                  {
                     u4Proto = RIP6_IMPORT_ISISL1;
                  }
                  else if ( $4 != NULL)
                  {
                     u4Proto = RIP6_IMPORT_ISISL1L2;
                  }
               }
               if ($5 != NULL)
               { 
                 u4Metric = *(UINT4 *) $6 ;
                 cli_process_rip6_cmd (CliHandle, CLI_RIP6_REDISTRIBUTE, NULL, u4Proto, u4Metric, CLI_RIP6_ENABLE, $8);
               }
               else 
               {
                 cli_process_rip6_cmd (CliHandle, CLI_RIP6_REDISTRIBUTE, NULL, u4Proto, RIP6_DFLT_COST, CLI_RIP6_ENABLE, $8);
               }   
            }
   SYNTAX  : redistribute { isis } [{level-1 | level-2 | level-1-2}] metric <integer(0-16)> [route-map <string(20)>]
   HELP    : Sets redistribution of IPv6 prefix from another protocol into RIP6
   CXT_HELP : redistribute Configures redistribution related parameters | 
              isis ISIS Routes |
              level-1 ISIS Level 1 Routes |
              level-2 ISIS Level 2 Routes |
              level-1-2 ISIS Level 1-2 Routes |
              metric Metric related configuration |
              (0-16) Routing metric|
              route-map Route Map related configuration |
              <string(20)> Route-map name|
              <CR> Sets redistribution of IPv6 prefix from another protocol into RIP6


   COMMAND : no redistribute { isis } [{level-1 | level-2 | level-1-2 }] [route-map <string(20)>]
   ACTION  :
            {
               UINT4    u4Proto=0;
               UINT4    u4Metric=0;  

               if ( $2 != NULL)
               {
                  u4Proto = RIP6_IMPORT_ISISL2;
                  if ( $3 != NULL)
                  {
                     u4Proto = RIP6_IMPORT_ISISL1;
                  }  
                  else if ( $5 != NULL)
                  {
                     u4Proto = RIP6_IMPORT_ISISL1L2;
                  }  
                }
               cli_process_rip6_cmd (CliHandle, CLI_RIP6_REDISTRIBUTE, NULL, u4Proto, u4Metric, CLI_RIP6_DISABLE, $7);
            }
   SYNTAX  : no redistribute { isis } [{level-1 | level-2 | level-1-2}] [route-map <string(20)>]
   HELP    : Resets redistribution of IPv6 prefix from another protocol into RIP6
   CXT_HELP : no Disables the configuration/deletes the entry/resets to default value|
              redistribute Route Redistribution related configuration |
              isis ISIS Routes |
              level-1 ISIS Level 1 Routes |
              level-2 ISIS Level 2 Routes |
              level-1-2 ISIS Level 1-2 Routes |
              route-map Route Map related configuration |
              <string(20)> Route-map name|
              <CR> Resets redistribution of IPv6 prefix from another protocol into RIP6

   COMMAND : distribute prefix <ip6_addr>  {in | out}
   ACTION  :
            {
               UINT4 u4Filter = 0;

               if ( $3 != NULL)
               {
                  u4Filter = RIP6_PEER_FILTER;
               }
               else if ( $4 != NULL)
               {
                  u4Filter = RIP6_ADV_FILTER;
               }

               cli_process_rip6_cmd (CliHandle, CLI_RIP6_FILTER, NULL, $2,
                                     u4Filter, CLI_RIP6_ENABLE);
            }
   SYNTAX  : distribute prefix <ip6_addr> {in | out}
   HELP    : Enable Filter network in routing updates send or received.
   CXT_HELP : distribute Enables Filter network in routing updates|
              prefix Filtering is controlled by distribute lists|
              AAAA::BBBB IPV6 address|
              in Filter network in routing updates received| 
              out Filter network in routing updates sent out|
              <CR> Enable Filter network in routing updates send or received

   COMMAND : no distribute prefix <ip6_addr> {in | out}
   ACTION  : {
               UINT4    u4Filter = 0;

               if ( $4 != NULL)
               {
                  u4Filter = RIP6_PEER_FILTER;
               }
               else if ( $5 != NULL)
               {
                  u4Filter = RIP6_ADV_FILTER;
               }

               cli_process_rip6_cmd (CliHandle, CLI_RIP6_FILTER, NULL, $3,
                                     u4Filter, CLI_RIP6_DISABLE);
            }
   SYNTAX  : no distribute prefix <ip6_addr> {in | out}
   HELP    : Disable Filter network in routing updates send or received.
   CXT_HELP : no Disables the configuration/deletes the entry/resets to default value| 
              distribute Enables Filter network in routing updates| 
              prefix Filtering is controlled by distribute lists|
              AAAA::BBBB IPV6 address|
              in Filter network in routing updates received| 
              out Filter network in routing updates sent out|
              <CR> Disable Filter network in routing updates send or received

   COMMAND : distribute-list route-map <string(20)> {in | out}
   ACTION  :
            {
               UINT4    u4Filter = 0;

               if ( $3 != NULL)
               {
                  u4Filter = CLI_RIP6_IN_FILTER;
               }
               else if ( $4 != NULL)
               {
                  u4Filter = CLI_RIP6_OUT_FILTER;
               }

               cli_process_rip6_cmd(CliHandle, CLI_RIP6_DISTRIBUTE_LIST, NULL, $2, 
                                    u4Filter, CLI_RIP6_ENABLE);
            }
   SYNTAX  : distribute-list route-map <name(1-20)> {in | out}
   HELP    : Enable route map filtering for inbound or outbound routes
   CXT_HELP : distribute-list Enables Filter network in routing updates|
              route-map Route Map to be applied during redistribution of routes
              from Route Table Manager to RIP|
              <string(20)> Route-map name|
              in Filter network in routing updates received| 
              out Filter network in routing updates sent out|
              <CR> Enable route map filtering for inbound or outbound routes

   COMMAND : ipv6 rip peer status { disable | enable }
   ACTION :
      {
      UINT4 u4Status = 0;
          if($4 != NULL )
          {
              u4Status = RIP6_DENY; 
          }
          else if($5!= NULL )
          {
             u4Status = RIP6_ALLOW;
          }
      cli_process_rip6_cmd(CliHandle, CLI_RIP6_PEER_FILTER_SET,
		      NULL, u4Status);
      }
   SYNTAX : ipv6 rip peer status { disable | enable }
   HELP : Flag to set peer list to allow or deny.
   CXT_HELP : ipv6 IPV6 related configuration |
              rip RIP related configuration |
              Filters the responses from Peers |
              disable Disables peer filtering |
              enable Eisables peer filtering |
              <CR> Enables / Disables responses from Peers.

   COMMAND : ipv6 rip peer triggered-updated-interval [<short(1-10)>]
   ACTION :
            {
            UINT1 *pu1Distance = $4;
            UINT4 u4Time = 5;
            if ( $4 != NULL)
	       {
                  u4Time = *pu1Distance;
	       }
            cli_process_rip6_cmd(CliHandle, CLI_RIP6_DELAY_TRIG_INTERVAL_SET,
			    NULL, u4Time);
            }
   SYNTAX : ipv6 rip peer triggered-updated-interval <seconds(1-10)>
   PRVID : 15
   HELP : Triggered time interval after one triggered delay update is sent.
   CXT_HELP : ipv6 IPV6 related configuration |
              rip RIP related configuration |
              Time-interval Triggered updates |
              (1-10) Delay time for Triggered update Interval |
              <CR> Sets the Profile Trigger Delay Timer - Default : 5.



   COMMAND : no distribute-list route-map <string(20)> {in | out}
   ACTION  :
            {
               UINT4    u4Filter = 0;

               if ( $4 != NULL)
               {
                  u4Filter = CLI_RIP6_IN_FILTER;
               }
               else if ( $5 != NULL)
               {
                  u4Filter = CLI_RIP6_OUT_FILTER;
               }
               cli_process_rip6_cmd(CliHandle, CLI_RIP6_DISTRIBUTE_LIST, NULL, $3, 
                                    u4Filter, CLI_RIP6_DISABLE);
            }
   SYNTAX  : no distribute-list route-map <name(1-20)> {in | out}
   HELP    : Disable route map filtering for inbound or outbound routes
   CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
              distribute-list Enables Filter network in routing updates|
              route-map Route Map to be applied during redistribution of routes from Route Table Manager to RIP|
              <string(20)> Route-map name|
              in Filter network in routing updates received| 
              out Filter network in routing updates sent out|
              <CR> Disable route map filtering for inbound or outbound routes
  COMMAND : exit  
  ACTION  : {
                 CliChangePath ("..");
             }
  SYNTAX  : exit
  PRVID   : 15
  HELP    : Exit from RIP6 router configuration mode
  CXT_HELP : exit Exits from RIP6 router config mode|
             <CR> Exit from RIP6 router configuration mode

  COMMAND : distance <short(1-255)>  [route-map <string(20)>]
  ACTION  : {
                UINT1 *pu1Distance = $1;
                UINT1 *pu1RMapName = NULL;
                
                if( $2 != NULL )
                {
                    pu1RMapName=$3;
                }
                cli_process_rip6_cmd (CliHandle, CLI_RIP6_ROUTE_DISTANCE, NULL, pu1Distance, pu1RMapName);
           }
  SYNTAX  : distance <1-255> [route-map <name(1-20)>]
  PRVID   : 15
  HELP    : Enable distance
  CXT_HELP : distance Enables distance|
             <short(1-255)> Distance value|
             route-map Route Map to be applied during redistribution of routes from Route Table Manager to RIP|
             <string(20)> Route-map name|
             <CR> Enable distance

  COMMAND : no distance [route-map <string(20)>]
  ACTION  : {
                UINT1 *pu1RMapName = NULL;
                 
                if( $2 != NULL )
                {
                    pu1RMapName=$3;
                }
                
               cli_process_rip6_cmd (CliHandle, CLI_RIP6_NO_ROUTE_DISTANCE, NULL, pu1RMapName);
           }
  SYNTAX  : no distance [route-map <name(1-20)>]
  PRVID   : 15
  HELP    : Disable distance
  CXT_HELP : no Disables the configuration / deletes the entry / resets to default value|
             distance Enables distance|
             route-map Route Map to be applied during redistribution of routes from Route Table Manager to RIP|
             <string(20)> Route-map name|
             <CR> Disable distance
END GROUP

