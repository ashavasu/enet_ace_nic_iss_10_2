/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rip6inc.h,v 1.11 2014/04/10 13:23:04 siva Exp $
 *
 * Description:
 *             
 *
 *******************************************************************/
/* FILE HEADER
 *
 * ----------------------------------------------------------------------------
 *  FILE NAME             : rip6inc.h
 *  PRINCIPAL AUTHOR      : Aricent Inc.
 *  MODULE NAME           : RIP6
 *  LANGUAGE              : C
 *  TARGET ENVIRONMENT    : Any
 *  DESCRIPTION           : This file contains all include files 
 * ---------------------------------------------------------------------------
*/
#ifndef _RIP6_INC_H_
#define _RIP6_INC_H_

#include "lr.h"
#include "cfa.h"
#include "cust.h"
#include "vcm.h"
#include "fssocket.h"
#include "iss.h"
#if defined (LNXIP4_WANTED) && defined (IP6_WANTED) && !defined (LNXIP6_WANTED)
#include "fssli.h"
#endif
#include "ipv6.h"
#include "ripv6.h"
#include "ip6util.h"
#include "snmccons.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "bufmacs.h"

#include "rmap.h"
#include "trieinc.h"
#include "dbutil.h"
#include "rip6.h"
#include "rip6trace.h"
#include "rip6snmp.h"
#include "rtm6.h"
#include "redblack.h"
#include "rip6proto.h"
#include "fsrip6low.h"
#include "rip6filt.h"
#include "rmgr.h"
#include "rip6red.h"
#include "ip.h"
#ifdef L3_SWITCHING_WANTED
#include "npapi.h"
#include "ip6np.h"
#include "rip6npwr.h"
#endif
#include "rip6sz.h"
#include "fssyslog.h"
#endif
