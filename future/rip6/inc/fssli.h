/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fssli.h,v 1.1 
 *
 * Description: This file contains the  header files and mappings of
 *              socket calls for FS_SLI.
 *
 *******************************************************************/

#ifndef _FSSOCKET_H
#define _FSSOCKET_H

/* FS SLI IS INCLUDED FOR LNXIP-FSIP6 COMBINATION TO SUPPORT UP6 SOCKET */ 
#if defined (LNXIP4_WANTED) && defined (IP6_WANTED)
#ifdef SLI_WANTED
#include "sli.h"
#endif

/* FutureSLI Select Call Specific Definiton Required for FS  Stack */
#ifdef SLI_WANTED
#undef   FD_ZERO
#undef   FD_SET
#undef   FD_CLR
#undef   FD_ISSET
#undef   F_SETFL
#undef   F_GETFL
#undef   O_NONBLOCK
#undef   SHUT_RD
#undef   SHUT_WR
#undef   SHUT_RDWR
#define  FD_ZERO(x)              SLI_FD_ZERO(x)
#define  FD_SET(x,y)             SLI_FD_SET(x,y)
#define  FD_CLR(x,y)             SLI_FD_CLR(x,y)
#define  FD_ISSET(x,y)           SLI_FD_ISSET(x,y)
#define  F_SETFL                 SLI_F_SETFL
#define  F_GETFL                 SLI_F_GETFL
#define  O_NONBLOCK              SLI_O_NONBLOCK
#define  SHUT_RD                 SHUTDOWN_RX
#define  SHUT_WR                 SHUTDOWN_TX
#define  SHUT_RDWR               SHUTDOWN_ALL
#define  socket      SliSocket
#define  bind        SliBind
#define  accept      SliAccept
#define  fcntl       SliFcntl
#define  connect     SliConnect
#define  recvfrom    SliRecvfrom
#define  sendto      SliSendto
#define  close       SliClose
#define  select      SliSelect
#define  setsockopt  SliSetsockopt
#define  getsockopt  SliGetsockopt
#define  sendmsg     SliSendmsg
#define  recvmsg     SliRecvmsg
#define  fd_set      SliFdSet
#endif

#endif
#endif /* _FSSOCKET_H */
