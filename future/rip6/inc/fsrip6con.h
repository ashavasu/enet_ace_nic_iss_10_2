/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrip6con.h,v 1.4 2013/06/23 13:34:03 siva Exp $
*
* Description: 
*********************************************************************/

# ifndef fsripOCON_H
# define fsripOCON_H
/*
 *  The Constant Declarations for
 *  fsrip6Scalars
 */

# define FSRIP6ROUTEPREFERENCE                             (1)
# define FSRIP6ROUTEPROPAGATE                              (2)
# define FSRIP6GLOBALDEBUG                                 (3)
# define FSRIP6GLOBALINSTANCEINDEX                         (4)
# define FSRIP6PEERFILTER                                  (5)
# define FSRIP6ADVFILTER                                   (6)
# define FSRIP6ROUTECOUNT                                  (7)
/*
 *  The Constant Declarations for
 *  fsrip6InstanceTable
 */

# define FSRIP6INSTANCEINDEX                               (1)
# define FSRIP6INSTANCESTATUS                              (2)

/*
 *  The Constant Declarations for
 *  fsrip6InstIfMapTable
 */

# define FSRIP6IFINDEX                                     (1)
# define FSRIP6INSTIFMAPINSTID                             (2)
# define FSRIP6INSTIFMAPIFATCHSTATUS                       (3)

/*
 *  The Constant Declarations for
 *  fsrip6RipIfTable
 */

# define FSRIP6RIPIFINDEX                                  (1)
# define FSRIP6RIPIFPROFILEINDEX                           (2)
# define FSRIP6RIPIFCOST                                   (3)
# define FSRIP6RIPIFOPERSTATUS                             (4)
# define FSRIP6RIPIFPROTOCOLENABLE                         (5)
# define FSRIP6RIPIFINMESSAGES                             (6)
# define FSRIP6RIPIFINREQUESTS                             (7)
# define FSRIP6RIPIFINRESPONSES                            (8)
# define FSRIP6RIPIFUNKNOWNCMDS                            (9)
# define FSRIP6RIPIFINOTHERVER                             (10)
# define FSRIP6RIPIFINDISCARDS                             (11)
# define FSRIP6RIPIFOUTMESSAGES                            (12)
# define FSRIP6RIPIFOUTREQUESTS                            (13)
# define FSRIP6RIPIFOUTRESPONSES                           (14)
# define FSRIP6RIPIFOUTTRIGUPDATES                         (15)

/*
 *  The Constant Declarations for
 *  fsrip6RipProfileTable
 */

# define FSRIP6RIPPROFILEINDEX                             (1)
# define FSRIP6RIPPROFILESTATUS                            (2)
# define FSRIP6RIPPROFILEHORIZON                           (3)
# define FSRIP6RIPPROFILEPERIODICUPDTIME                   (4)
# define FSRIP6RIPPROFILETRIGDELAYTIME                     (5)
# define FSRIP6RIPPROFILEROUTEAGE                          (6)
# define FSRIP6RIPPROFILEGARBAGECOLLECTTIME                (7)

/*
 *  The Constant Declarations for
 *  fsrip6RipRouteTable
 */

# define FSRIP6RIPROUTEDEST                                (1)
# define FSRIP6RIPROUTEPFXLENGTH                           (2)
# define FSRIP6RIPROUTEPROTOCOL                            (3)
# define FSRIP6RIPROUTEIFINDEX                             (4)
# define FSRIP6RIPROUTENEXTHOP                             (5)
# define FSRIP6RIPROUTEMETRIC                              (6)
# define FSRIP6RIPROUTETYPE                                (7)
# define FSRIP6RIPROUTETAG                                 (8)
# define FSRIP6RIPROUTEAGE                                 (9)

/*
 *  The Constant Declarations for
 *  fsrip6RipPeerTable
 */

# define FSRIP6RIPPEERADDR                                 (1)
# define FSRIP6RIPPEERENTRYSTATUS                          (2)

/*
 *  The Constant Declarations for
 *  fsrip6RipAdvFilterTable
 */

# define FSRIP6RIPADVFILTERADDRESS                         (1)
# define FSRIP6RIPADVFILTERSTATUS                          (2)

#endif /*  fsrip6OCON_H  */
