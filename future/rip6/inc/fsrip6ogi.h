
# ifndef fsripOGP_H
# define fsripOGP_H

 /* The Definitions of the OGP Index Constants.  */

# define SNMP_OGP_INDEX_FSRIP6SCALARS                                (0)
# define SNMP_OGP_INDEX_FSRIP6INSTANCETABLE                          (1)
# define SNMP_OGP_INDEX_FSRIP6INSTIFMAPTABLE                         (2)
# define SNMP_OGP_INDEX_FSRIP6RIPIFTABLE                             (3)
# define SNMP_OGP_INDEX_FSRIP6RIPPROFILETABLE                        (4)
# define SNMP_OGP_INDEX_FSRIP6RIPROUTETABLE                          (5)
# define SNMP_OGP_INDEX_FSRIP6RIPPEERTABLE                           (6)
# define SNMP_OGP_INDEX_FSRIP6RIPADVFILTERTABLE                      (7)

#endif /*  fsrip6OGP_H  */
