#!/bin/csh
# $Id: make.h,v 1.8 2013/11/22 11:40:07 siva Exp $
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :  Aricent Inc.                    |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX ( Slackware 1.2.1 )                     |
# |                                                                          |
# |   DATE                   : 9th January 2002                              |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Cleanall option                            |
# |                            3. (Sample) Packaging (eg. make review)       |
# |                            4. Project/Module Backup (eg. daily, weekly)  |
# +--------------------------------------------------------------------------+
# | REVISION HISTORY   :                 :                                   |
# | Date               :       Author    :           Description             |
# +--------------------------------------------------------------------------+
# |  10-01-2002        :   A. Sivarama   :  Original is created              |
# |                    :      Krishnan   :                                   |
# + -------------------------------------------------------------------------+

# Top level make include files 
# ----------------------------

include ../LR/make.h
include ../LR/make.rule

# COMPILATION SWITCHES  #
#------------------------
# This switch enables Multiple Instance Support for RIP6
PROTOCOL_OPNS = -DRIP6_SINGLE_INSTANCE \
                -DNETIP6_WANTED


# This is used to specify the compiler option
COMPILER_TYPE = -DGNU_CC -DANSI -DUNIX -DINCLUDE_IN_OSS 

GLOBAL_OPNS = ${TRACE_OPNS} ${DEBUG_OPNS} ${PROTOCOL_OPNS} \
              ${COMPILER_TYPE} ${PACKING_OPNS}   \
              ${GENERAL_COMPILATION_SWITCHES} ${SYSTEM_COMPILATION_SWITCHES} \
              ${RIP6_COMPILATION_SWITCHES}

# Directories 
#------------

IPV6_INCD   = ${BASE_DIR}/ip6/ipv6/inc
RIP6_BASE_DIR  = ${BASE_DIR}/rip6
RIP6_DIR       = ${RIP6_BASE_DIR}
RIP6_SRCD      = ${RIP6_BASE_DIR}/src
RIP6_INCD      = ${RIP6_BASE_DIR}/inc
RIP6_OBJD      = ${RIP6_BASE_DIR}/obj
RIP6_LIB_INCD  = ${BASE_DIR}/util/ip6
RIP6_LIBD      = ${BASE_DIR}/util/ip6
RIP6_TRIE_INCD = ${BASE_DIR}/util/trie2/inc
TRIELIBD      = ${BASE_DIR}/util/trie2/lib

# INCLUDE FILES 
#--------------

RIP6_DPNDS     =  ${RIP6_INCD}/rip6.h \
                  ${RIP6_INCD}/rip6trace.h \
                  ${RIP6_INCD}/rip6inc.h \
                  ${RIP6_INCD}/rip6proto.h \
                  ${RIP6_INCD}/rip6snmp.h \
                  ${RIP6_INCD}/rip6cliproto.h \
                  ${RIP6_INCD}/rip6filt.h \
                  ${RIP6_LIB_INCD}/ip6util.h \
                  ${RIP6_TRIE_INCD}/trieinc.h

# INCLUDE DIR 
#------------

RIP6_GLOBAL_INCLUDES  = -I${RIP6_INCD} -I${RIP6_LIB_INCD} -I${RIP6_TRIE_INCD} \
                          $(COMMON_INCLUDE_DIRS)

# DEPENDENCIES 
#-------------

RIP6_DEPENDENCIES = $(RIP6_DPNDS) \
                    $(COMMON_DEPENDENCIES) \
                    $(RIP6_BASE_DIR)/Makefile \
                    $(RIP6_BASE_DIR)/make.h

#############################################################################
# END 
