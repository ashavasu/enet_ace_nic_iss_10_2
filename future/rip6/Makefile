#####################################################################
#### Copyright (C) 2006 Aricent Inc . All Rights Reserved                          ####
#####################################################################
####  MAKEFILE HEADER ::                                         ####
##|###############################################################|##
##|    FILE NAME               ::  Makefile                       |##
##|                                                               |##
##|    PRINCIPAL    AUTHOR     ::  Aricent Inc.      |##
##|                                                               |##
##|    SUBSYSTEM NAME          ::  RIP6  Protocol Router          |##
##|                                                               |##
##|    MODULE NAME             ::  TOP most level Makefile        |##
##|                                                               |##
##|    TARGET ENVIRONMENT      ::  UNIX                           |##
##|                                                               |##
##|    DATE OF FIRST RELEASE   ::  10-01-2002                     |##
##|                                                               |##
##|    DESCRIPTION             ::                                 |##
##|                                MAKEFILE for Future            |##
##|                                RIP6 Router Final Object       |##
##|###############################################################|##
#### CHANGE RECORD ::                                            ####
##|#######|###############|#######################################|##
##|VERSION|  DATE/AUTHOR  |  DESCRIPTION OF CHANGE                |##
##|#######|###############|#######################################|##
##|  1.0  |  10-01-2002   |              Create Original          |##
##|       |  A. Sivarama  |  Original makefiles were renamed and  |##
##|       |     Krishnan  |  modified to remove the environmental |##
##|       |               |  variables                            |##
##|#######|###############|#######################################|##

# The make.h of RIPng. System level make.h included this make.h when needed.
# ------------------------------------------------------------------------

include ../LR/make.h
include ../LR/make.rule
include make.h

#RIP6 OBJ FILES
#--------------
#CC=codewizard

RIP6_OBJ_FILES = ${RIP6_OBJD}/rip6main.o \
		 ${RIP6_OBJD}/rip6io.o \
                 ${RIP6_OBJD}/rip6rt.o \
                 ${RIP6_OBJD}/rip6ip6if.o \
                 ${RIP6_OBJD}/rip6trie.o \
                 ${RIP6_OBJD}/rip6netip6.o \
                 ${RIP6_OBJD}/rip6buf.o \
                 ${RIP6_OBJD}/rip6snif.o \
                 ${RIP6_OBJD}/rip6filt.o \
                 ${RIP6_OBJD}/rip6mbsm.o \
                 ${RIP6_OBJD}/rip6sz.o \
                 ${RIP6_OBJD}/fsrip6low.o \
				 ${RIP6_OBJD}/rip6red.o  

ifeq (DRIP6_ARRAY_TO_RBTREE_WANTED, $(findstring DRIP6_ARRAY_TO_RBTREE_WANTED, $(GLOBAL_OPNS)))
RIP6_OBJ_FILES += ${RIP6_OBJD}/rip6rbutl.o
endif

ifeq (${SNMP}, YES)
RIP6_OBJ_FILES +=  ${RIP6_OBJD}/fsrip6mid.o
endif

ifeq (${SNMP_2}, YES)
RIP6_OBJ_FILES +=  ${RIP6_OBJD}/fsrip6wr.o
endif

ifeq (${NPAPI}, YES)
RIP6_OBJ_FILES +=  ${RIP6_OBJD}/rip6npapi.o
endif

### CLI SUPPORT ###

ifeq (${CLI}, YES)
RIP6_OBJ_FILES +=  ${RIP6_OBJD}/rip6cli.o
endif

ifeq (DRIP6_TEST_WANTED, $(findstring DRIP6_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
RIP6_TEST_BASE_DIR = ${RIP6_BASE_DIR}/test
RIP6_TEST_OBJ_DIR  = ${RIP6_BASE_DIR}/test/obj
RIP6_OBJ_FILES += \
	${RIP6_TEST_OBJ_DIR}/FutureRip6Test.o	
endif

RIP6_TRIE_LIB = ${TRIELIBD}/libTrie2.a

# RIP6 final object file
# ---------------------

RIP6_FINAL_OBJ = ${RIP6_OBJD}/FutureRIP6.o

all : ${RIP6_FINAL_OBJ}


# RIP6 individual object files
# ----------------------------
obj:
ifdef MKDIR
	$(MKDIR) $(MKDIR_FLAGS) obj
endif

ifeq (DRIP6_TEST_WANTED, $(findstring DRIP6_TEST_WANTED,$(SYSTEM_COMPILATION_SWITCHES)))
${RIP6_TEST_OBJ_DIR}/FutureRip6Test.o: FORCE
	${MAKE} -C ${RIP6_TEST_BASE_DIR} -f Makefile
endif

FORCE:

$(RIP6_FINAL_OBJ) : obj $(RIP6_OBJ_FILES)
	${LD} ${LD_FLAGS} -o $(RIP6_FINAL_OBJ) $(RIP6_OBJ_FILES) $(CC_COMMON_OPTIONS)
 
${RIP6_OBJD}/rip6main.o : ${RIP6_SRCD}/rip6main.c ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6main.c -o ${RIP6_OBJD}/rip6main.o

${RIP6_OBJD}/rip6rbutl.o : ${RIP6_SRCD}/rip6rbutl.c ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6rbutl.c -o ${RIP6_OBJD}/rip6rbutl.o

${RIP6_OBJD}/rip6io.o : ${RIP6_SRCD}/rip6io.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6io.c -o ${RIP6_OBJD}/rip6io.o

${RIP6_OBJD}/rip6rt.o : ${RIP6_SRCD}/rip6rt.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6rt.c -o ${RIP6_OBJD}/rip6rt.o

${RIP6_OBJD}/rip6ip6if.o : ${RIP6_SRCD}/rip6ip6if.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6ip6if.c -o ${RIP6_OBJD}/rip6ip6if.o

${RIP6_OBJD}/rip6trie.o : ${RIP6_SRCD}/rip6trie.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6trie.c -o ${RIP6_OBJD}/rip6trie.o

${RIP6_OBJD}/rip6netip6.o : ${RIP6_SRCD}/rip6netip6.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6netip6.c -o ${RIP6_OBJD}/rip6netip6.o

${RIP6_OBJD}/rip6buf.o : ${RIP6_SRCD}/rip6buf.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6buf.c -o ${RIP6_OBJD}/rip6buf.o

${RIP6_OBJD}/rip6snif.o : ${RIP6_SRCD}/rip6snif.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6snif.c -o ${RIP6_OBJD}/rip6snif.o

${RIP6_OBJD}/rip6filt.o : ${RIP6_SRCD}/rip6filt.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6filt.c -o ${RIP6_OBJD}/rip6filt.o

${RIP6_OBJD}/rip6mbsm.o : ${RIP6_SRCD}/rip6mbsm.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6mbsm.c -o ${RIP6_OBJD}/rip6mbsm.o

${RIP6_OBJD}/fsrip6mid.o : ${RIP6_SRCD}/fsrip6mid.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/fsrip6mid.c -o ${RIP6_OBJD}/fsrip6mid.o

${RIP6_OBJD}/fsrip6wr.o : ${RIP6_SRCD}/fsrip6wr.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/fsrip6wr.c -o ${RIP6_OBJD}/fsrip6wr.o

${RIP6_OBJD}/fsrip6low.o : ${RIP6_SRCD}/fsrip6low.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/fsrip6low.c -o ${RIP6_OBJD}/fsrip6low.o

${RIP6_OBJD}/rip6cli.o : ${RIP6_SRCD}/rip6cli.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6cli.c -o ${RIP6_OBJD}/rip6cli.o

${RIP6_OBJD}/rip6sz.o : ${RIP6_SRCD}/rip6sz.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6sz.c -o ${RIP6_OBJD}/rip6sz.o

${RIP6_OBJD}/rip6red.o : ${RIP6_SRCD}/rip6red.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6red.c -o ${RIP6_OBJD}/rip6red.o

${RIP6_OBJD}/rip6npapi.o : ${RIP6_SRCD}/rip6npapi.c  ${RIP6_DEPENDENCIES}
	${CC} ${CC_FLAGS} ${GLOBAL_OPNS} ${RIP6_GLOBAL_INCLUDES} ${RIP6_SRCD}/rip6npapi.c -o ${RIP6_OBJD}/rip6npapi.o


ECHO  : 
	@echo Making RIP6 module...
DONE  : 
	@echo RIP6 module done.

#Clean Unwanted
#--------------
clean:
	@echo Cleaning RIPNG Object files!!!
	rm -f ${RIP6_OBJD}/*.o

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf ${ISS_PKG_CREATE_DIR}/rip6.tgz -T ${BASE_DIR}/rip6/FILES.NEW;\
        cd ${CUR_PWD};


###############################################################################
#END
