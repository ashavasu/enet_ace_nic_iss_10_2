/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacsdb.h,v 1.1 2015/04/28 12:49:58 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSTACSDB_H
#define _FSTACSDB_H

UINT1 FsTacClntExtServerTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fstacs [] ={1,3,6,1,4,1,29601,2,29};
tSNMP_OID_TYPE fstacsOID = {9, fstacs};


UINT4 FsTacClntExtActiveServerAddressType [ ] ={1,3,6,1,4,1,29601,2,29,1,1};
UINT4 FsTacClntExtActiveServer [ ] ={1,3,6,1,4,1,29601,2,29,1,2};
UINT4 FsTacClntExtTraceLevel [ ] ={1,3,6,1,4,1,29601,2,29,1,3};
UINT4 FsTacClntExtRetransmit [ ] ={1,3,6,1,4,1,29601,2,29,1,4};
UINT4 FsTacClntExtAuthenStartRequests [ ] ={1,3,6,1,4,1,29601,2,29,1,5,1};
UINT4 FsTacClntExtAuthenContinueRequests [ ] ={1,3,6,1,4,1,29601,2,29,1,5,2};
UINT4 FsTacClntExtAuthenEnableRequests [ ] ={1,3,6,1,4,1,29601,2,29,1,5,3};
UINT4 FsTacClntExtAuthenAbortRequests [ ] ={1,3,6,1,4,1,29601,2,29,1,5,4};
UINT4 FsTacClntExtAuthenPassReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,5};
UINT4 FsTacClntExtAuthenFailReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,6};
UINT4 FsTacClntExtAuthenGetUserReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,7};
UINT4 FsTacClntExtAuthenGetPassReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,8};
UINT4 FsTacClntExtAuthenGetDataReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,9};
UINT4 FsTacClntExtAuthenErrorReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,10};
UINT4 FsTacClntExtAuthenFollowReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,11};
UINT4 FsTacClntExtAuthenRestartReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,12};
UINT4 FsTacClntExtAuthenSessionTimouts [ ] ={1,3,6,1,4,1,29601,2,29,1,5,13};
UINT4 FsTacClntExtAuthorRequests [ ] ={1,3,6,1,4,1,29601,2,29,1,5,14};
UINT4 FsTacClntExtAuthorPassAddReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,15};
UINT4 FsTacClntExtAuthorPassReplReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,16};
UINT4 FsTacClntExtAuthorFailReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,17};
UINT4 FsTacClntExtAuthorErrorReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,18};
UINT4 FsTacClntExtAuthorFollowReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,19};
UINT4 FsTacClntExtAuthorSessionTimeouts [ ] ={1,3,6,1,4,1,29601,2,29,1,5,20};
UINT4 FsTacClntExtAcctStartRequests [ ] ={1,3,6,1,4,1,29601,2,29,1,5,21};
UINT4 FsTacClntExtAcctWdRequests [ ] ={1,3,6,1,4,1,29601,2,29,1,5,22};
UINT4 FsTacClntExtAcctStopRequests [ ] ={1,3,6,1,4,1,29601,2,29,1,5,23};
UINT4 FsTacClntExtAcctSuccessReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,24};
UINT4 FsTacClntExtAcctErrorReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,25};
UINT4 FsTacClntExtAcctFollowReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,26};
UINT4 FsTacClntExtAcctSessionTimeouts [ ] ={1,3,6,1,4,1,29601,2,29,1,5,27};
UINT4 FsTacClntExtMalformedPktsReceived [ ] ={1,3,6,1,4,1,29601,2,29,1,5,28};
UINT4 FsTacClntExtSocketFailures [ ] ={1,3,6,1,4,1,29601,2,29,1,5,29};
UINT4 FsTacClntExtConnectionFailures [ ] ={1,3,6,1,4,1,29601,2,29,1,5,30};
UINT4 FsTacClntExtServerAddressType [ ] ={1,3,6,1,4,1,29601,2,29,2,1,1,1};
UINT4 FsTacClntExtServerAddress [ ] ={1,3,6,1,4,1,29601,2,29,2,1,1,2};
UINT4 FsTacClntExtServerStatus [ ] ={1,3,6,1,4,1,29601,2,29,2,1,1,3};
UINT4 FsTacClntExtServerSingleConnect [ ] ={1,3,6,1,4,1,29601,2,29,2,1,1,4};
UINT4 FsTacClntExtServerPort [ ] ={1,3,6,1,4,1,29601,2,29,2,1,1,5};
UINT4 FsTacClntExtServerTimeout [ ] ={1,3,6,1,4,1,29601,2,29,2,1,1,6};
UINT4 FsTacClntExtServerKey [ ] ={1,3,6,1,4,1,29601,2,29,2,1,1,7};


tMbDbEntry fstacsMibEntry[]= {

{{11,FsTacClntExtActiveServerAddressType}, NULL, FsTacClntExtActiveServerAddressTypeGet, FsTacClntExtActiveServerAddressTypeSet, FsTacClntExtActiveServerAddressTypeTest, FsTacClntExtActiveServerAddressTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsTacClntExtActiveServer}, NULL, FsTacClntExtActiveServerGet, FsTacClntExtActiveServerSet, FsTacClntExtActiveServerTest, FsTacClntExtActiveServerDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsTacClntExtTraceLevel}, NULL, FsTacClntExtTraceLevelGet, FsTacClntExtTraceLevelSet, FsTacClntExtTraceLevelTest, FsTacClntExtTraceLevelDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsTacClntExtRetransmit}, NULL, FsTacClntExtRetransmitGet, FsTacClntExtRetransmitSet, FsTacClntExtRetransmitTest, FsTacClntExtRetransmitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{12,FsTacClntExtAuthenStartRequests}, NULL, FsTacClntExtAuthenStartRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenContinueRequests}, NULL, FsTacClntExtAuthenContinueRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenEnableRequests}, NULL, FsTacClntExtAuthenEnableRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenAbortRequests}, NULL, FsTacClntExtAuthenAbortRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenPassReceived}, NULL, FsTacClntExtAuthenPassReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenFailReceived}, NULL, FsTacClntExtAuthenFailReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenGetUserReceived}, NULL, FsTacClntExtAuthenGetUserReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenGetPassReceived}, NULL, FsTacClntExtAuthenGetPassReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenGetDataReceived}, NULL, FsTacClntExtAuthenGetDataReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenErrorReceived}, NULL, FsTacClntExtAuthenErrorReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenFollowReceived}, NULL, FsTacClntExtAuthenFollowReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenRestartReceived}, NULL, FsTacClntExtAuthenRestartReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthenSessionTimouts}, NULL, FsTacClntExtAuthenSessionTimoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthorRequests}, NULL, FsTacClntExtAuthorRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthorPassAddReceived}, NULL, FsTacClntExtAuthorPassAddReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthorPassReplReceived}, NULL, FsTacClntExtAuthorPassReplReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthorFailReceived}, NULL, FsTacClntExtAuthorFailReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthorErrorReceived}, NULL, FsTacClntExtAuthorErrorReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthorFollowReceived}, NULL, FsTacClntExtAuthorFollowReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAuthorSessionTimeouts}, NULL, FsTacClntExtAuthorSessionTimeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAcctStartRequests}, NULL, FsTacClntExtAcctStartRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAcctWdRequests}, NULL, FsTacClntExtAcctWdRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAcctStopRequests}, NULL, FsTacClntExtAcctStopRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAcctSuccessReceived}, NULL, FsTacClntExtAcctSuccessReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAcctErrorReceived}, NULL, FsTacClntExtAcctErrorReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAcctFollowReceived}, NULL, FsTacClntExtAcctFollowReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtAcctSessionTimeouts}, NULL, FsTacClntExtAcctSessionTimeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtMalformedPktsReceived}, NULL, FsTacClntExtMalformedPktsReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtSocketFailures}, NULL, FsTacClntExtSocketFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntExtConnectionFailures}, NULL, FsTacClntExtConnectionFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsTacClntExtServerAddressType}, GetNextIndexFsTacClntExtServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsTacClntExtServerTableINDEX, 2, 0, 0, NULL},

{{13,FsTacClntExtServerAddress}, GetNextIndexFsTacClntExtServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsTacClntExtServerTableINDEX, 2, 0, 0, NULL},

{{13,FsTacClntExtServerStatus}, GetNextIndexFsTacClntExtServerTable, FsTacClntExtServerStatusGet, FsTacClntExtServerStatusSet, FsTacClntExtServerStatusTest, FsTacClntExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacClntExtServerTableINDEX, 2, 0, 1, NULL},

{{13,FsTacClntExtServerSingleConnect}, GetNextIndexFsTacClntExtServerTable, FsTacClntExtServerSingleConnectGet, FsTacClntExtServerSingleConnectSet, FsTacClntExtServerSingleConnectTest, FsTacClntExtServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacClntExtServerTableINDEX, 2, 0, 0, "2"},

{{13,FsTacClntExtServerPort}, GetNextIndexFsTacClntExtServerTable, FsTacClntExtServerPortGet, FsTacClntExtServerPortSet, FsTacClntExtServerPortTest, FsTacClntExtServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTacClntExtServerTableINDEX, 2, 0, 0, "49"},

{{13,FsTacClntExtServerTimeout}, GetNextIndexFsTacClntExtServerTable, FsTacClntExtServerTimeoutGet, FsTacClntExtServerTimeoutSet, FsTacClntExtServerTimeoutTest, FsTacClntExtServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTacClntExtServerTableINDEX, 2, 0, 0, "5"},

{{13,FsTacClntExtServerKey}, GetNextIndexFsTacClntExtServerTable, FsTacClntExtServerKeyGet, FsTacClntExtServerKeySet, FsTacClntExtServerKeyTest, FsTacClntExtServerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsTacClntExtServerTableINDEX, 2, 0, 0, NULL},
};
tMibData fstacsEntry = { 41, fstacsMibEntry };
#endif /* _FSTACSDB_H */

