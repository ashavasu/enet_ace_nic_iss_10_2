/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpinc.h,v 1.6 2015/04/28 12:49:58 siva Exp $
 *
 * Description: This header file for TACACS.
 *******************************************************************/
#ifndef __TACINC_H__
#define __TACINC_H__

#include "lr.h"
#include "ip.h"
#include "fssnmp.h"
#include "tpprot.h"
#include "ipv6.h"
#include "fstacawr.h"
#include "fstacswr.h"
#include "snmccons.h"
#include "utilipvx.h"
#include "arMD5_inc.h"
#include "ip6util.h"
#include "tacsz.h"
#include "fsutil.h"
#include "dns.h"
#endif /* __TACINC_H__ */
