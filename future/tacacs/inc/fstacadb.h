/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacadb.h,v 1.4 2015/04/28 12:49:58 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSTACADB_H
#define _FSTACADB_H

UINT1 FsTacClntServerTableINDEX [] = {SNMP_DATA_TYPE_IP_ADDR_PRIM};

UINT4 fstaca [] ={1,3,6,1,4,1,2076,77};
tSNMP_OID_TYPE fstacaOID = {8, fstaca};


UINT4 FsTacClntActiveServer [ ] ={1,3,6,1,4,1,2076,77,1,1};
UINT4 FsTacClntTraceLevel [ ] ={1,3,6,1,4,1,2076,77,1,2};
UINT4 FsTacClntRetransmit [ ] ={1,3,6,1,4,1,2076,77,1,3};
UINT4 FsTacClntAuthenStartRequests [ ] ={1,3,6,1,4,1,2076,77,1,4,1};
UINT4 FsTacClntAuthenContinueRequests [ ] ={1,3,6,1,4,1,2076,77,1,4,2};
UINT4 FsTacClntAuthenEnableRequests [ ] ={1,3,6,1,4,1,2076,77,1,4,3};
UINT4 FsTacClntAuthenAbortRequests [ ] ={1,3,6,1,4,1,2076,77,1,4,4};
UINT4 FsTacClntAuthenPassReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,5};
UINT4 FsTacClntAuthenFailReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,6};
UINT4 FsTacClntAuthenGetUserReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,7};
UINT4 FsTacClntAuthenGetPassReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,8};
UINT4 FsTacClntAuthenGetDataReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,9};
UINT4 FsTacClntAuthenErrorReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,10};
UINT4 FsTacClntAuthenFollowReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,11};
UINT4 FsTacClntAuthenRestartReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,12};
UINT4 FsTacClntAuthenSessionTimouts [ ] ={1,3,6,1,4,1,2076,77,1,4,13};
UINT4 FsTacClntAuthorRequests [ ] ={1,3,6,1,4,1,2076,77,1,4,14};
UINT4 FsTacClntAuthorPassAddReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,15};
UINT4 FsTacClntAuthorPassReplReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,16};
UINT4 FsTacClntAuthorFailReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,17};
UINT4 FsTacClntAuthorErrorReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,18};
UINT4 FsTacClntAuthorFollowReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,19};
UINT4 FsTacClntAuthorSessionTimeouts [ ] ={1,3,6,1,4,1,2076,77,1,4,20};
UINT4 FsTacClntAcctStartRequests [ ] ={1,3,6,1,4,1,2076,77,1,4,21};
UINT4 FsTacClntAcctWdRequests [ ] ={1,3,6,1,4,1,2076,77,1,4,22};
UINT4 FsTacClntAcctStopRequests [ ] ={1,3,6,1,4,1,2076,77,1,4,23};
UINT4 FsTacClntAcctSuccessReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,24};
UINT4 FsTacClntAcctErrorReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,25};
UINT4 FsTacClntAcctFollowReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,26};
UINT4 FsTacClntAcctSessionTimeouts [ ] ={1,3,6,1,4,1,2076,77,1,4,27};
UINT4 FsTacClntMalformedPktsReceived [ ] ={1,3,6,1,4,1,2076,77,1,4,28};
UINT4 FsTacClntSocketFailures [ ] ={1,3,6,1,4,1,2076,77,1,4,29};
UINT4 FsTacClntConnectionFailures [ ] ={1,3,6,1,4,1,2076,77,1,4,30};
UINT4 FsTacClntServerAddress [ ] ={1,3,6,1,4,1,2076,77,2,1,1,1};
UINT4 FsTacClntServerStatus [ ] ={1,3,6,1,4,1,2076,77,2,1,1,2};
UINT4 FsTacClntServerSingleConnect [ ] ={1,3,6,1,4,1,2076,77,2,1,1,3};
UINT4 FsTacClntServerPort [ ] ={1,3,6,1,4,1,2076,77,2,1,1,4};
UINT4 FsTacClntServerTimeout [ ] ={1,3,6,1,4,1,2076,77,2,1,1,5};
UINT4 FsTacClntServerKey [ ] ={1,3,6,1,4,1,2076,77,2,1,1,6};


tMbDbEntry fstacaMibEntry[]= {

{{10,FsTacClntActiveServer}, NULL, FsTacClntActiveServerGet, FsTacClntActiveServerSet, FsTacClntActiveServerTest, FsTacClntActiveServerDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsTacClntTraceLevel}, NULL, FsTacClntTraceLevelGet, FsTacClntTraceLevelSet, FsTacClntTraceLevelTest, FsTacClntTraceLevelDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsTacClntRetransmit}, NULL, FsTacClntRetransmitGet, FsTacClntRetransmitSet, FsTacClntRetransmitTest, FsTacClntRetransmitDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsTacClntAuthenStartRequests}, NULL, FsTacClntAuthenStartRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenContinueRequests}, NULL, FsTacClntAuthenContinueRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenEnableRequests}, NULL, FsTacClntAuthenEnableRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenAbortRequests}, NULL, FsTacClntAuthenAbortRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenPassReceived}, NULL, FsTacClntAuthenPassReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenFailReceived}, NULL, FsTacClntAuthenFailReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenGetUserReceived}, NULL, FsTacClntAuthenGetUserReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenGetPassReceived}, NULL, FsTacClntAuthenGetPassReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenGetDataReceived}, NULL, FsTacClntAuthenGetDataReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenErrorReceived}, NULL, FsTacClntAuthenErrorReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenFollowReceived}, NULL, FsTacClntAuthenFollowReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenRestartReceived}, NULL, FsTacClntAuthenRestartReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthenSessionTimouts}, NULL, FsTacClntAuthenSessionTimoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthorRequests}, NULL, FsTacClntAuthorRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthorPassAddReceived}, NULL, FsTacClntAuthorPassAddReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthorPassReplReceived}, NULL, FsTacClntAuthorPassReplReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthorFailReceived}, NULL, FsTacClntAuthorFailReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthorErrorReceived}, NULL, FsTacClntAuthorErrorReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthorFollowReceived}, NULL, FsTacClntAuthorFollowReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAuthorSessionTimeouts}, NULL, FsTacClntAuthorSessionTimeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAcctStartRequests}, NULL, FsTacClntAcctStartRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAcctWdRequests}, NULL, FsTacClntAcctWdRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAcctStopRequests}, NULL, FsTacClntAcctStopRequestsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAcctSuccessReceived}, NULL, FsTacClntAcctSuccessReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAcctErrorReceived}, NULL, FsTacClntAcctErrorReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAcctFollowReceived}, NULL, FsTacClntAcctFollowReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntAcctSessionTimeouts}, NULL, FsTacClntAcctSessionTimeoutsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntMalformedPktsReceived}, NULL, FsTacClntMalformedPktsReceivedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntSocketFailures}, NULL, FsTacClntSocketFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsTacClntConnectionFailures}, NULL, FsTacClntConnectionFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,FsTacClntServerAddress}, GetNextIndexFsTacClntServerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsTacClntServerTableINDEX, 1, 0, 0, NULL},

{{12,FsTacClntServerStatus}, GetNextIndexFsTacClntServerTable, FsTacClntServerStatusGet, FsTacClntServerStatusSet, FsTacClntServerStatusTest, FsTacClntServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacClntServerTableINDEX, 1, 0, 1, NULL},

{{12,FsTacClntServerSingleConnect}, GetNextIndexFsTacClntServerTable, FsTacClntServerSingleConnectGet, FsTacClntServerSingleConnectSet, FsTacClntServerSingleConnectTest, FsTacClntServerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTacClntServerTableINDEX, 1, 0, 0, "2"},

{{12,FsTacClntServerPort}, GetNextIndexFsTacClntServerTable, FsTacClntServerPortGet, FsTacClntServerPortSet, FsTacClntServerPortTest, FsTacClntServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTacClntServerTableINDEX, 1, 0, 0, "49"},

{{12,FsTacClntServerTimeout}, GetNextIndexFsTacClntServerTable, FsTacClntServerTimeoutGet, FsTacClntServerTimeoutSet, FsTacClntServerTimeoutTest, FsTacClntServerTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTacClntServerTableINDEX, 1, 0, 0, "5"},

{{12,FsTacClntServerKey}, GetNextIndexFsTacClntServerTable, FsTacClntServerKeyGet, FsTacClntServerKeySet, FsTacClntServerKeyTest, FsTacClntServerTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsTacClntServerTableINDEX, 1, 0, 0, NULL},
};
tMibData fstacaEntry = { 39, fstacaMibEntry };
#endif /* _FSTACADB_H */

