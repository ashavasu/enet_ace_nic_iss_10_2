#ifndef _FSTACAWR_H
#define _FSTACAWR_H

VOID RegisterFSTACA(VOID);

VOID UnRegisterFSTACA(VOID);
INT4 FsTacClntActiveServerGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntRetransmitGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntActiveServerSet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntRetransmitSet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntActiveServerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacClntTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacClntRetransmitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacClntActiveServerDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTacClntTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsTacClntRetransmitDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);


INT4 FsTacClntAuthenStartRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenContinueRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenEnableRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenAbortRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenPassReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenFailReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenGetUserReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenGetPassReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenGetDataReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenErrorReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenFollowReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenRestartReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthenSessionTimoutsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthorRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthorPassAddReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthorPassReplReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthorFailReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthorErrorReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthorFollowReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAuthorSessionTimeoutsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAcctStartRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAcctWdRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAcctStopRequestsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAcctSuccessReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAcctErrorReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAcctFollowReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntAcctSessionTimeoutsGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntMalformedPktsReceivedGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntSocketFailuresGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntConnectionFailuresGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsTacClntServerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsTacClntServerStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerSingleConnectGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerPortGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerTimeoutGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerKeyGet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerSingleConnectSet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerPortSet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerTimeoutSet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerKeySet(tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerSingleConnectTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerTimeoutTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerKeyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsTacClntServerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


#endif /* _FSTACAWR_H */
