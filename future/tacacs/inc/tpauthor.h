/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpauthor.h,v 1.7 2015/04/28 12:49:58 siva Exp $
 *
 * Description: This header file for TACACS.
 *******************************************************************/
#ifndef __TPAUTHOR_H__
#define __TPAUTHOR_H__

/* Constant definitions for authorization */

/* Authorization response status */

#define   TAC_AUTHOR_STATUS_PASS_ADD   0x01
#define   TAC_AUTHOR_STATUS_PASS_REPL   0x02
#define   TAC_AUTHOR_STATUS_FAIL   0x10
#define   TAC_AUTHOR_STATUS_ERROR   0x11
#define   TAC_AUTHOR_STATUS_FOLLOW   0x21


#define   TAC_AUTHOR_PKT_SIZE   4096

#define   TAC_AUTHOR_REQ_LEN_FIXED_FIELDS   8
#define   TAC_AUTHOR_RESP_LEN_FIXED_FIELDS   6

#define   TAC_AUTHOR_REQ_AUTHEN_METHOD_OFFSET  TAC_PKT_HEADER_LEN
#define   TAC_AUTHOR_REQ_PL_OFFSET   TAC_AUTHOR_REQ_AUTHEN_METHOD_OFFSET + 1
#define   TAC_AUTHOR_REQ_AUTHEN_TYPE_OFFSET   TAC_AUTHOR_REQ_PL_OFFSET +1
#define   TAC_AUTHOR_REQ_SVC_OFFSET   TAC_AUTHOR_REQ_AUTHEN_TYPE_OFFSET + 1
#define   TAC_AUTHOR_REQ_USER_LEN_OFFSET   TAC_AUTHOR_REQ_SVC_OFFSET + 1
#define   TAC_AUTHOR_REQ_PORT_LEN_OFFSET   TAC_AUTHOR_REQ_USER_LEN_OFFSET + 1
#define   TAC_AUTHOR_REQ_REM_ADDR_LEN_OFFSET   TAC_AUTHOR_REQ_PORT_LEN_OFFSET + 1
#define   TAC_AUTHOR_REQ_ARG_CNT_OFFSET   TAC_AUTHOR_REQ_REM_ADDR_LEN_OFFSET + 1
#define   TAC_AUTHOR_REQ_ARG1_LEN_OFFSET   TAC_AUTHOR_REQ_ARG_CNT_OFFSET  + 1

#define   TAC_AUTHOR_RESP_STATUS_OFFSET   TAC_PKT_HEADER_LEN
#define   TAC_AUTHOR_RESP_ARG_CNT_OFFSET   TAC_AUTHOR_RESP_STATUS_OFFSET + 1
#define   TAC_AUTHOR_RESP_SRV_MSG_LEN_OFFSET   TAC_AUTHOR_RESP_ARG_CNT_OFFSET + 1
#define   TAC_AUTHOR_RESP_DATA_LEN_OFFSET   TAC_AUTHOR_RESP_SRV_MSG_LEN_OFFSET + 2
#define   TAC_AUTHOR_RESP_ARG1_LEN_OFFSET   TAC_AUTHOR_RESP_DATA_LEN_OFFSET + 2

/* Macro definitions for authorization */

#define   TAC_AUTHOR_GLOBAL_DATA   gTacAuthorInfo
#define   TAC_AUTHOR_SESSION_TABLE   gTacAuthorInfo.apTacAuthorSessionTable
#define   TAC_AUTHOR_SESSION_POOL_ID  TACACSMemPoolIds[MAX_TAC_AUTHOR_SESSION_SIZING_ID] 
#define   TAC_PACKET_POOL_ID  TACACSMemPoolIds[MAX_TAC_PKT_COUNT_SIZING_ID] 
#define   TAC_AUTHOR_OUTPUT_POOL_ID   TACACSMemPoolIds[MAX_TAC_AUTHOR_OUTPUT_SIZING_ID]
#define   TAC_AUTHOR_NO_OF_CURRENT_SESSIONS   \
                                    gTacAuthorInfo.u1NoOfAuthorSessions

/* Data structures definition for authorization */


typedef struct _tacauthorsessioninfo {
    tTacAuthorInput   AuthorInput;
    tTacPkt   TacTempPkt;   /* Used to store the packet (incompletely received
                             * packets) till the packet is received completely
                             * in multiple connection mode of operation
                             */

    UINT4   u4TacServer;
    UINT4   u4SessionIdentifier;
    UINT4   u4Timer;
    UINT4   u4ConnectionId;
    UINT1   u1SequenceNoRcvd;
    UINT1   u1MinorVersionSent;
    UINT1   au1Padding[2];
} tTacAuthorSessionInfo;

typedef struct _tacauthorgen {
    tTacAuthorSessionInfo   
        *apTacAuthorSessionTable[TAC_MAX_AUTHOR_SESSION_LIMIT];
    UINT1   u1NoOfAuthorSessions;
    UINT1   au1Padding[3];
} tTacAuthorGen;

/* Global data declarations for authorization */

#ifdef __TPAUTHOR_C__
tTacAuthorGen   gTacAuthorInfo;
#else
extern tTacAuthorGen   gTacAuthorInfo;
#endif

#endif /*  __TPAUTHOR_H__ */
