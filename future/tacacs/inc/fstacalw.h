/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacalw.h,v 1.4 2015/04/28 12:49:58 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/
/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacClntActiveServer ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntTraceLevel ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntRetransmit ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTacClntActiveServer ARG_LIST((UINT4 ));

INT1
nmhSetFsTacClntTraceLevel ARG_LIST((UINT4 ));

INT1
nmhSetFsTacClntRetransmit ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTacClntActiveServer ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsTacClntTraceLevel ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsTacClntRetransmit ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTacClntActiveServer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTacClntTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTacClntRetransmit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacClntAuthenStartRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenContinueRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenEnableRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenAbortRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenPassReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenFailReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenGetUserReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenGetPassReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenGetDataReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenErrorReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenFollowReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenRestartReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthenSessionTimouts ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthorRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthorPassAddReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthorPassReplReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthorFailReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthorErrorReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthorFollowReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAuthorSessionTimeouts ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAcctStartRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAcctWdRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAcctStopRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAcctSuccessReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAcctErrorReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAcctFollowReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntAcctSessionTimeouts ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntMalformedPktsReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntSocketFailures ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntConnectionFailures ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsTacClntServerTable. */
INT1
nmhValidateIndexInstanceFsTacClntServerTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTacClntServerTable  */

INT1
nmhGetFirstIndexFsTacClntServerTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTacClntServerTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacClntServerStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTacClntServerSingleConnect ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTacClntServerPort ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTacClntServerTimeout ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTacClntServerKey ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTacClntServerStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTacClntServerSingleConnect ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTacClntServerPort ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTacClntServerTimeout ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTacClntServerKey ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTacClntServerStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTacClntServerSingleConnect ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTacClntServerPort ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTacClntServerTimeout ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTacClntServerKey ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTacClntServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
