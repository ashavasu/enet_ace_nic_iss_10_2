/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpaaa.h,v 1.10 2017/11/17 12:10:41 siva Exp $
 *
 * Description: This header file for TACACS.
 *******************************************************************/
#ifndef __TPAAA_H__
#define __TPAAA_H__
/* Constant definitions common to Authenticatin, Authorizatin and Accounting */
#include "utilipvx.h"

#define TAC_INIT_COMPLETE(u4Status)     lrInitComplete(u4Status)

#define   TAC_PROT_LOCK   TacProtocolLock
#define   TAC_PROT_UNLOCK TacProtocolUnlock
/* Events from application to TACACS+ client */

#define   TAC_EVT_IN_INPUT   0x00000001
#define   TAC_EVT_IN_TIMER   0x00000002
#define   TAC_FD_SET_EVENT   0x00000004

/* Event from TACACS+ client to application */

#define   TAC_EVT_OUT_OUTPUT   0x00000001

/* TACACS+ protocol TCP Port */
#define   TAC_TCP_IPV4_PORT   49
#define   TAC_TCP_IPV6_PORT   4949
#define   TAC_ZERO            0

#define   TAC_IS_ADDR_CLASS_A(u4Addr)   ((u4Addr != 0 ) && ((u4Addr & 0x80000000) == 0))
#define   TAC_IS_ADDR_CLASS_B(u4Addr)   ((u4Addr & 0xc0000000) == 0x80000000)
#define   TAC_IS_ADDR_CLASS_C(u4Addr)   ((u4Addr & 0xe0000000) == 0xc0000000)
#define   TAC_IS_ADDR_CLASS_D(u4Addr)   ((u4Addr & 0xf0000000) == 0xe0000000)
#define   TAC_IS_ADDR_CLASS_E(u4Addr)   ((u4Addr & 0xf0000000) == 0xf0000000)
#define   TAC_IS_CLASS_C_BROADCAST_ADDR(u4Addr) ((u4Addr & 0xdfffffff) == 0xdfffffff)

#define TAC_DEFAULT_SERVER  0x00000000
#define TAC_DEFAULT_SERVER_LEN 4

/* TACACS+ packet major and minor versions */

#define   TAC_MAJOR_VER   0x0C
#define   TAC_MINOR_VER_DEFAULT   0x00
#define   TAC_MINOR_VER_ONE   0x01
#define   TAC_MINOR_VER_MASK  0x0F

/* TACACS+ packet fixed privilege level values */

#define   TAC_PRIV_LVL_MAX   0x0F
#define   TAC_PRIV_LVL_ROOT   0x0F
#define   TAC_PRIV_LVL_USER   0x01
#define   TAC_PRIV_LVL_MIN   0x00

#define   TAC_UNENCRYPTED_FLAG   0x01
#define   TAC_SINGLE_CONNECT_FLAG   0x04

/* Authentication types */

#define   TAC_AUTHEN_TYPE_ASCII   0x01
#define   TAC_AUTHEN_TYPE_ARAP   0x04
#define   TAC_AUTHEN_TYPE_MSCHAP   0x05

/* Authentication services */

#define   TAC_AUTHEN_SVC_NONE   0x00
#define   TAC_AUTHEN_SVC_ENABLE   0x02
#define   TAC_AUTHEN_SVC_PPP   0x03
#define   TAC_AUTHEN_SVC_ARAP   0x04
#define   TAC_AUTHEN_SVC_PT   0x05
#define   TAC_AUTHEN_SVC_RCMD   0x06
#define   TAC_AUTHEN_SVC_X25   0x07
#define   TAC_AUTHEN_SVC_NASI   0x08
#define   TAC_AUTHEN_SVC_FWPROXY   0x09

/* Authentication methods */

#define   TAC_AUTHEN_METH_NOT_SET   0x00
#define   TAC_AUTHEN_METH_NONE   0x01
#define   TAC_AUTHEN_METH_KRB5   0x02
#define   TAC_AUTHEN_METH_LINE   0x03
#define   TAC_AUTHEN_METH_ENABLE   0x04
#define   TAC_AUTHEN_METH_LOCAL   0x05
#define   TAC_AUTHEN_METH_TACACSPLUS   0x06
#define   TAC_AUTHEN_METH_GUEST   0x08
#define   TAC_AUTHEN_METH_RADIUS   0x10
#define   TAC_AUTHEN_METH_KRB4   0x11
#define   TAC_AUTHEN_METH_RCMD   0x20

#define   TAC_CLNT_STARTED   0x01

/* for SNMP enumerations */

#define   TAC_ACTIVE   ACTIVE
#define   TAC_NOT_IN_SRVICE   NOT_IN_SERVICE
#define   TAC_NOT_READY   NOT_READY
#define   TAC_DESTROY   DESTROY

#define   TAC_SC_FLAG_SNMP_OTHER   0
#define   TAC_SC_FLAG_SNMP_YES   1
#define   TAC_SC_FLAG_SNMP_NO   2
#define   TAC_SRV_MAX_TIMEOUT   255

#define   TAC_SRV_DEFAULT_TIMEOUT   5
#define   TAC_NO_SINGLE_CONNECT   0x00
#define   TAC_SRV_DEFAULT_KEY   "Aricent"

#define   TAC_MAX_RCVD_PKTS  30
#define   TAC_MAX_SERVERS   8
#define   TAC_MAX_DNS_DOMAIN_NAME   64

#define   TAC_PKT_RX_NEW    0
#define   TAC_PKT_RX_PEND   1

#define   TAC_SC_VERIFY_COUNT_MAX  2 /* As per standard */

#define   TAC_SECRET_KEY_LEN   64
#define   TAC_MD5_OUT_LEN   16 

/* should be > 4 + TAC_SECRET_KEY_LEN + 1 + 1 + TAC_MD5_OUT_LEN */
#define   TAC_MD5_IN_LEN   100

/* TACACS+ packet header offsets */

#define   TAC_PKT_HEADER_LEN   12

#define   TAC_PKT_VERSION_OFFSET   0
#define   TAC_PKT_TYPE_OFFSET   TAC_PKT_VERSION_OFFSET + 1
#define   TAC_PKT_SEQ_NO_OFFSET   TAC_PKT_TYPE_OFFSET + 1
#define   TAC_PKT_FLAGS_OFFSET   TAC_PKT_SEQ_NO_OFFSET + 1
#define   TAC_PKT_SESSION_ID_OFFSET   TAC_PKT_FLAGS_OFFSET + 1
#define   TAC_PKT_LEN_OFFSET   TAC_PKT_SESSION_ID_OFFSET + 4
#define   TAC_PKT_BODY_OFFSET   TAC_PKT_LEN_OFFSET + 4

#define   TAC_APP_TIMEOUT  180  /* The duration for which the client will wait
                                  to get input from application for continue
                                  packets. Timeout value in seconds */
#define   TAC_MIN_RETRANSMITS 1
#define   TAC_MAX_RETRANSMITS 100
#define   TAC_DEF_RETRIES  2
#define   TAC_CLNT_ALWAYS  1 

#define   TAC_ONE_SECOND   SYS_NUM_OF_TIME_UNITS_IN_A_SEC

/* Poll time 0.3 second */
#define   TAC_SESSION_TIMEOUT   TAC_ONE_SECOND
#define   TAC_EVT_WAIT_FLAGS   (OSIX_WAIT | OSIX_EV_ANY) 
#define   TAC_EVT_WAIT_TIMEOUT   0                         

/* Trace realted definitions */

#define   TAC_MAX_LOG_STR_LEN   256
#define   TAC_MAX_CHARS_FOR_BYTE_DUMP 6
#define   TAC_MAX_CHARS_PER_LINE 60


/* for SNMP enumerations */
#define   TAC_TRC_SNMP_OTHER 0
#define   TAC_TRC_SNMP_NOTRACE 1
#define   TAC_TRC_SNMP_ALL 2
#define   TAC_TRC_SNMP_INFO 3
#define   TAC_TRC_SNMP_ERROR 4

#define   TAC_MODULE   "TACACS+"
#define   TAC_TASK_NAME   "TACT"
#define   TAC_PROTOCOL_SEMAPHORE   "TACS"
#define   TAC_QUEUE_NAME   "TACQ"
#define   TAC_TASK_PRIORITY   20        
#define   TAC_STACK_SIZE   20000                     
#define   TAC_QUEUE_DEPTH   25

/* TACACS+ Client Errors */
#define   TAC_SUCCESS      1
#define   TAC_FAILURE      0

#define   TAC_CLNT_RES_NOT_OK  0x7FFFFFFF
#define   TAC_CLNT_ERROR_GEN   0x7FFFFFFE
#define   TAC_CLNT_ERROR_MEM   0x7FFFFFFD
#define   TAC_CLNT_ERROR_TBL_FULL   0x7FFFFFFC
#define   TAC_CLNT_ERROR_NOT_STARTED   0x7FFFFFFB
#define   TAC_CLNT_ERROR_Q_CREATION   0x7FFFFFFA
#define   TAC_CLNT_ERROR_APP_INFO   0x7FFFFFF9
#define   TAC_CLNT_ERROR_CONN_OPEN   0x7FFFFFF8
#define   TAC_CLNT_ERROR_CONN_CLOSE   0x7FFFFFF7
#define   TAC_CLNT_ERROR_SOCK_OPEN   0x7FFFFFF6
#define   TAC_CLNT_ERROR_SEND_PKT   0x7FFFFFF5
#define   TAC_CLNT_ERROR_INPUT   0x7FFFFFF4
#define   TAC_CLNT_ERROR_NO_SUPPORT   0x7FFFFFF3
#define   TAC_CLNT_ERROR_RCVD_PKT   0x7FFFFFF2
#define   TAC_CLNT_ERROR_SEND_MSG_TO_APP   0x7FFFFFF1
#define   TAC_CLNT_ERROR_TIMER_CREATION   0x7FFFFFF0
#define   TAC_CLNT_ERROR_TIMER_START   0x7FFFFFEF
#define   TAC_CLNT_ERROR_SEQ_NO_WRAPPED   0x7FFFFFEE
#define   TAC_CLNT_ERROR_NO_ACTIVE_SRV   0x7FFFFFED
#define   TAC_CLNT_ERROR_SESSION_TIMEOUT   0x7FFFFFEC

/* Macro definitions */

#define   TAC_CMN_GLOBAL_DATA   gTacClntCmnInfo
#define   TAC_SERVER_TABLE   gTacClntCmnInfo.aTacServerTable
#define   TAC_SC_TEMP_PKT   gTacClntCmnInfo.aTacSCTempPkt
#define   TAC_TIMER_LIST_ID   gTacClntCmnInfo.TacTimerListId
#define   TAC_SESSION_TIMER   gTacClntCmnInfo.TacSessionTimer
#define   TAC_CLNT_LOG   gTacClntCmnInfo.TacClntLog
#define   TAC_TASK_ID   gTacClntCmnInfo.u4TacClntTaskId
#define   TAC_QUEUE_ID   gTacClntCmnInfo.u4TacClntQueueId
#define   TAC_TRACE_LEVEL   gTacClntCmnInfo.u4TacTraceLevel
#define   TAC_MAX_RETRIES   gTacClntCmnInfo.u4TacRetries
#define   TAC_RCVD_PKTS   gTacClntCmnInfo.apu1ReceivedPackets
#define   TAC_ACTIVE_SERVER   gTacClntCmnInfo.u1ActiveServer
#define   TAC_ADDRESS_TYPE    gTacClntCmnInfo.u1AddressType
#define   TAC_NO_OF_REG_APP   gTacClntCmnInfo.u1NoOfRegisteredApp
#define   TAC_TASK_STARTED   gTacClntCmnInfo.u1TacClientStarted
#define   TAC_UNENC_FLAG  gTacClntCmnInfo.u1TacUnEncryptedFlag

#define TAC_IPVX_LENGTH_FROM_TYPE(t) \
    (((t) == IPVX_ADDR_FMLY_IPV4 )?\
     IPVX_IPV4_ADDR_LEN:IPVX_IPV6_ADDR_LEN)

#define TAC_IPVX_TYPE_FROM_LENGTH(t) \
    (((t) == IPVX_IPV4_ADDR_LEN)?\
     IPVX_ADDR_FMLY_IPV4:IPVX_ADDR_FMLY_IPV6)


#define   TAC_MD5(out, in, len) tacMD5Calculate(out, in, len)
#define   TAC_CREATE_TIMER_LIST(TaskName, Event, Fp, ListId) \
                    TmrCreateTimerList(TaskName, Event, Fp, ListId)
#define   TAC_DELETE_TIMER_LIST(ListId) TmrDeleteTimerList(ListId)
#define   TAC_START_TIMER(ListId, Timer, Duration) \
                    TmrStartTimer(ListId, Timer, Duration)
#define   TAC_STOP_TIMER(ListId, Timer) \
                    TmrStopTimer(ListId, Timer)
#define   TAC_CREATE_MEM_POOL(BlockSize, NoOfBlocks, MemType, PoolId) \
                    MemCreateMemPool(BlockSize, NoOfBlocks, MemType, PoolId)
#define   TAC_DELETE_MEM_POOL(PoolId) MemDeleteMemPool(PoolId)
#define   TAC_ALLOCATE_MEM_BLOCK(PoolId, MemBlock) \
                    (MemBlock = MemAllocMemBlk(PoolId))
#define   TAC_RELEASE_MEM_BLOCK(PoolId, MemBlock) \
                    MemReleaseMemBlock(PoolId, MemBlock)

#define   TAC_SOCKET   socket
#define   TAC_CONNECT  connect
#define   TAC_CLOSE   close
#define   TAC_READ   read
#define   TAC_SEND   send
#define   TAC_RANDOM   random

#define TAC_COMP(pTacSrv, pTacServerAddress) \
                tacCompare(pTacSrv, pTacServerAddress)

    
#ifdef TRACE_WANTED
#define   TAC_GLB_TRC   (TAC_TRACE_LEVEL)

#define   TAC_DBG(Flag, Module, Fmt) \
          UtlTrcLog(TAC_GLB_TRC, Flag, Module, Fmt)

#define   TAC_DBG1(Flag, Module, Fmt, Arg) \
          UtlTrcLog(TAC_GLB_TRC, Flag, Module, Fmt, Arg)

#define   TAC_DBG2(Flag, Module, Fmt, Arg1, Arg2) \
          UtlTrcLog(TAC_GLB_TRC, Flag, Module, Fmt, Arg1, Arg2)

#define   TAC_DBG3(Flag, Module, Fmt, Arg1, Arg2, Arg3) \
          UtlTrcLog(TAC_GLB_TRC, Flag, Module, Fmt, Arg1, Arg2, Arg3)

#define   TAC_DBG4(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4) \
          UtlTrcLog(TAC_GLB_TRC, Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4)

#define   TAC_DBG5(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5) \
          UtlTrcLog(TAC_GLB_TRC, Flag, Module, Fmt,\
          Arg1, Arg2, Arg3, Arg4, Arg5)

#define   TAC_DBG6(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) \
          UtlTrcLog(TAC_GLB_TRC, Flag, Module, Fmt,\
          Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define   TAC_DBG_PKT(Flag, Module, Packet) \
          tacDumpPkt(TAC_GLB_TRC, Flag, Module, Packet)

#else

#define   TAC_DBG(Flag, Module, Fmt)
#define   TAC_DBG1(Flag, Module, Fmt, Arg)
#define   TAC_DBG2(Flag, Module, Fmt, Arg1, Arg2)
#define   TAC_DBG3(Flag, Module, Fmt, Arg1, Arg2, Arg3)
#define   TAC_DBG4(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4)
#define   TAC_DBG5(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define   TAC_DBG6(Flag, Module, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define   TAC_DBG_PKT(Flag, Module, Packet)

#endif /* TRACE_WANTED */

/* Data structures definitions */

typedef struct _tacclnterrorstatisticslog {
    UINT4   u4AuthenStarts;           /* No. of authenticaton start sent */
    UINT4   u4AuthenConts;        /* No. of authenticatn continue sent */
    UINT4   u4IbASCIILoginReqs; /* No. of inbound ASCII login request sent */
    UINT4   u4IbPAPLoginReqs; /* No. of inbound PAP loging request sent */
    UINT4   u4IbCHAPLoginReqs; /* No. of inbound CHAP loging request sent */
    UINT4   u4IbMSCHAPLoginReqs; /* No. of inbound MSCHAP login req. sent */
    UINT4   u4IbARAPLoginReqs; /* No. of inbound RARP login req. sent */
    UINT4   u4EnableReqs; /* No. of enable request sent */
    UINT4   u4ObPAPReqs; /* No. of outbound PAP request sent */
    UINT4   u4ObCHAPReqs; /* No. of outbound CHAP req. sent */
    UINT4   u4ObMSCHAPReqs; /* No. of outbound MSCHAP req. sent */
    UINT4   u4ChgPwdASCIIs; /* No. of ASCII change password req. sent */
    UINT4   u4ChgPwdARAPs; /* No. of ARAP change password req. sent */
    UINT4   u4AuthenAborts; /* No. of aborts sent */
    UINT4   u4AuthenFails; /* No.of authentication fail received */
    UINT4   u4AuthenPass; /* No. of. authentication pass received */
    UINT4   u4AuthenGetUsers; /* No. of authentication get user received */
    UINT4   u4AuthenGetPass; /* No. of authentication get pass received */
    UINT4   u4AuthenGetData; /* No. of authentication get data received */
    UINT4   u4AuthenErrors; /* No. of authentication errors received */
    UINT4   u4AuthenFollows; /* No. of authentication follow received */
    UINT4   u4AuthenRestarts; /* No. of authentication re-start received */
    UINT4   u4AuthenSessionTimeouts; /* No. of session time outs */

    UINT4   u4AuthorReqs; /* No. of authorization req. sent */
    UINT4   u4AuthorPassAdds; /* No. of authorization pass add received */
    UINT4   u4AuthorPassRepls; /* No. of authorization pass repl received */
    UINT4   u4AuthorFails; /* No. of authorization fail received */
    UINT4   u4AuthorErrors; /* No. of authorization error received */
    UINT4   u4AuthorFollows; /* No. of authorization follows received */
    UINT4   u4AuthorSessionTimeouts; /* No. of session time outs */

    UINT4   u4AcctStartReqs; /* No. of accounting start req. sent */
    UINT4   u4AcctWdReqs; /* No. of accounting watch dog req. sent */
    UINT4   u4AcctStopReqs; /* No. of accounting stop req. sent */
    UINT4   u4AcctSuccess; /* No. of accounting success received */
    UINT4   u4AcctErrors; /* No. of accounting error received */
    UINT4   u4AcctFollows; /* No. of accounting follows received */
    UINT4   u4AcctSessionTimeouts; /* No. of session time outs */

    UINT4   u4SocketFailures; /* No. of socket open failures */
    UINT4   u4ConnOpenFailures; /* No. of connection open failuers */
    UINT4   u4MalformedPkts; /* No. of Malformed pkts received */
} tTacClntErrorStatisticsLog;

typedef struct _tacpkt {
   UINT4   u4BytesRead;
   UINT4   u4BytesLeft;
   UINT1   *pu1TacPkt;
   UINT1   au1Header[TAC_PKT_HEADER_LEN];
   UINT1   u1PktStatus;
   UINT1   au1Padding[3];
} tTacPkt;

typedef struct _tacserverinfo {
    tIPvXAddr   IpAddress;
    UINT4   u4Status;
    UINT4   u4Timeout;
    INT4    i4SingleConnId;
    UINT1   au1SecretKey[TAC_SECRET_KEY_LEN];
    UINT2   u2Port;
    UINT1   u1SCFlag;
    UINT1   u1SCVerifyCount;
    UINT1   au1HostName[255];
    UINT1   au1Pad[1];
} tTacServerInfo;

typedef struct _tacgeneral {
    tTacServerInfo   aTacServerTable[TAC_MAX_SERVERS];
    tTacPkt   aTacSCTempPkt[TAC_MAX_SERVERS];
    tTimerListId   TacTimerListId;
    tTmrAppTimer   TacSessionTimer;
    tTacClntErrorStatisticsLog   TacClntLog;
    tOsixQId   u4TacClntQueueId;
    tOsixTaskId   u4TacClntTaskId;
    UINT4   u4TacTraceLevel;
    UINT4   u4TacRetries;
    UINT1   *apu1ReceivedPackets[TAC_MAX_RCVD_PKTS];
    UINT1   u1ActiveServer;   /* The active server. Value starts from 1 */
    UINT1   u1AddressType;
    UINT1   u1TacClientStarted;
    UINT1   u1TacUnEncryptedFlag;
} tTacGeneral;

/* Global data declarations */

extern tTacGeneral   gTacClntCmnInfo;
extern fd_set        gReadSockFds;
extern tOsixSemId    gTacSemId;

#endif /*  __TPAAA_H__ */

