/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpprot.h,v 1.5 2015/04/28 12:49:58 siva Exp $
 *
 * Description: This header file for TACACS.
 *******************************************************************/

#ifndef __TPPROT_H__
#define __TPPROT_H__
#include "tacacs.h"
#include "fssocket.h"
#include "tpaaa.h"
#include "tpauthen.h"
#include "tpauthor.h"
#include "tpacct.h"



/* Common function prototypes */
INT4 tacClientInit ARG_LIST((VOID));
INT4 tacClientStart ARG_LIST((VOID));
INT4 tacClientStop ARG_LIST((VOID));
INT4 tacOpenConnection ARG_LIST((UINT4 , UINT4 *));
INT4 tacGenerateSessionId ARG_LIST((UINT4 *));
INT4 tacCompare ARG_LIST((tTacServerInfo * , tSNMP_OCTET_STRING_TYPE *));
INT4 tacCloseConnection ARG_LIST((UINT4 , UINT4));
INT4 tacacsHandleTimerExpiry ARG_LIST((VOID));
INT4 tacReadPacket ARG_LIST((UINT4 , tTacPkt *, UINT1 *));
INT4 tacProcessReceivedPacket ARG_LIST((UINT1));
INT4 tacValidateCmnHdrFields ARG_LIST((UINT1 *));
VOID tacMD5Calculate ARG_LIST((UINT1 *, UINT1 *, UINT1));
INT4 tacEncryptDecrypt ARG_LIST((UINT1 *, UINT1 *));
INT4 tacSendPacket ARG_LIST((UINT1 *, UINT4));
INT4 tacSendInfoToApp ARG_LIST((UINT1 , tTacAppInfo * , VOID *));

VOID tacDumpPkt ARG_LIST((UINT4 , UINT4 , CONST UINT1 *, UINT1 *));
INT4 TacProtocolLock (VOID);
INT4 TacProtocolUnlock (VOID);

/* Authentication module function prototypes */
INT4 tacacsAuthentication ARG_LIST((tTacAuthenInput *));

INT4 tacAuthenValidateInput ARG_LIST((tTacAuthenInput *, 
                                      tTacAuthenSessionInfo *, UINT1));
INT4 tacAuthenCreateSession ARG_LIST((tTacAuthenInput *, UINT4 , 
                            tTacAuthenSessionInfo **, UINT1 *));
INT4 tacAuthenReleaseSession ARG_LIST((tTacAuthenSessionInfo *));
INT4 tacAuthenConstructPacket ARG_LIST((tTacAuthenSessionInfo *, 
                                        tTacAuthenInput *, UINT1, UINT1 *));
INT4 tacAuthenConstructStartBody ARG_LIST((tTacAuthenSessionInfo *, 
                                           tTacAuthenInput *, UINT1 *));
INT4 tacAuthenConstructGenStartBody ARG_LIST((tTacAuthenSessionInfo *, 
                                              tTacAuthenInput *, UINT1 *));
INT4 tacAuthenInboundLogin ARG_LIST((tTacAuthenSessionInfo *, 
                                     tTacAuthenInput *, UINT1 *));
INT4 tacAuthenInboundASCII ARG_LIST((tTacAuthenSessionInfo *, 
                                     tTacAuthenInput *, UINT1 *));
INT4 tacAuthenInboundPAP ARG_LIST((tTacAuthenSessionInfo *, tTacAuthenInput *, 
                                   UINT1 *));
INT4 tacAuthenInboundCHAP ARG_LIST((tTacAuthenSessionInfo *, tTacAuthenInput *,
                                    UINT1 *));
INT4 tacAuthenInboundMSCHAP ARG_LIST((tTacAuthenSessionInfo *, 
                                      tTacAuthenInput *, UINT1 *));
INT4 tacAuthenInboundARAP ARG_LIST((tTacAuthenSessionInfo *, tTacAuthenInput *,
                                    UINT1 *));
INT4 tacAuthenInboundEnable ARG_LIST((tTacAuthenSessionInfo *, 
                                      tTacAuthenInput *, UINT1 *));
INT4 tacAuthenOutboundLogin ARG_LIST((tTacAuthenSessionInfo *, 
                                      tTacAuthenInput *, UINT1 *));
INT4 tacAuthenOutboundPAP ARG_LIST((tTacAuthenSessionInfo *, tTacAuthenInput *,
                                    UINT1 *));
INT4 tacAuthenOutboundCHAP ARG_LIST((tTacAuthenSessionInfo *, 
                                     tTacAuthenInput *, UINT1 *));
INT4 tacAuthenOutboundMSCHAP ARG_LIST((tTacAuthenSessionInfo *, 
                                       tTacAuthenInput *, UINT1 *));
INT4 tacAuthenChangePassword ARG_LIST((tTacAuthenSessionInfo *, 
                                       tTacAuthenInput *, UINT1 *));
INT4 tacAuthenChgPwdASCII ARG_LIST((tTacAuthenSessionInfo *, tTacAuthenInput *,
                                    UINT1 *));
INT4 tacAuthenChgPwdARAP ARG_LIST((tTacAuthenSessionInfo *, tTacAuthenInput *, 
                                   UINT1 *));
INT4 tacAuthenConstructContinueBody ARG_LIST((tTacAuthenSessionInfo *, 
                                              tTacAuthenInput *, UINT1 *));
INT4 tacAuthenContTerminateSession ARG_LIST((tTacAuthenSessionInfo *, 
                                             tTacAuthenInput *, UINT1 *));
INT4 tacAuthenConstructHeader ARG_LIST((tTacAuthenSessionInfo *, 
                                        tTacAuthenInput *, UINT1 *));
INT4 tacAuthenReceivedPkt ARG_LIST((UINT1 *));
INT4 tacProcessAuthenReply ARG_LIST((UINT1 *, tTacAuthenSessionInfo **, 
                                     tTacAuthenOutput *));
INT4 tacValidateAuthenReply ARG_LIST((UINT1 *, tTacAuthenSessionInfo  *));
INT4 tacAuthenOutputFillCmn ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                     tTacAuthenOutput *));
INT4 tacAuthenOutputFillData ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                       tTacAuthenOutput *));
INT4 tacProcessAuthenPass ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                    tTacAuthenOutput *));
INT4 tacProcessAuthenFail ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                    tTacAuthenOutput *));
INT4 tacProcessAuthenGetData ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                       tTacAuthenOutput *));
INT4 tacProcessAuthenGetUser ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                       tTacAuthenOutput *));
INT4 tacProcessAuthenGetPass ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                       tTacAuthenOutput *));
INT4 tacProcessAuthenFollow ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                      tTacAuthenOutput *));
INT4 tacProcessAuthenRestart ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                       tTacAuthenOutput *));
INT4 tacProcessAuthenError ARG_LIST((UINT1 *, tTacAuthenSessionInfo *, 
                                     tTacAuthenOutput *));
INT4 tacAuthenChkSessionTimeout ARG_LIST((VOID));

INT4 TacAuthenEstablishSession (tTacAuthenInput *,
                                UINT4, tTacAuthenSessionInfo **, UINT1 *);

/* Authorization module function prototypes */
INT4 tacacsAuthorization ARG_LIST((tTacAuthorInput *));

INT4 tacAuthorValidateInput ARG_LIST((tTacAuthorInput *));

INT4 tacAuthorCreateSession ARG_LIST((tTacAuthorInput *, UINT4 , 
                                      tTacAuthorSessionInfo **));
INT4 tacAuthorReleaseSession ARG_LIST((tTacAuthorSessionInfo *));
INT4 tacAuthorConstructPacket ARG_LIST((tTacAuthorSessionInfo *, 
                                        tTacAuthorInput *, UINT1 *));
INT4 tacAuthorConstructReqBody ARG_LIST((tTacAuthorSessionInfo *, 
                                         tTacAuthorInput *, UINT1 *));
INT4 tacAuthorConstructHeader ARG_LIST((tTacAuthorSessionInfo *, 
                                        tTacAuthorInput *, UINT1 *));
INT4 tacAuthorReceivedPkt ARG_LIST((UINT1  *));
INT4 tacProcessAuthorResponse ARG_LIST((UINT1 *, tTacAuthorSessionInfo **, 
                                        tTacAuthorOutput *));
INT4 tacValidateAuthorResponse ARG_LIST((UINT1 *, tTacAuthorSessionInfo  *));
INT4 tacAuthorOutputFillCmn ARG_LIST((UINT1 *, tTacAuthorSessionInfo *, 
                                      tTacAuthorOutput *));
INT4 tacAuthorRespExtractArgs ARG_LIST((UINT1 *, tTacAuthorSessionInfo *, 
                                        tTacAuthorOutput *));
INT4 tacAuthorOutputFillData ARG_LIST((UINT1 *, tTacAuthorSessionInfo *, 
                                       tTacAuthorOutput *));
INT4 tacProcessAuthorPassAdd ARG_LIST((UINT1 *, tTacAuthorSessionInfo *, 
                                       tTacAuthorOutput *));
INT4 tacProcessAuthorPassRepl ARG_LIST((UINT1 *, tTacAuthorSessionInfo *, 
                                        tTacAuthorOutput *));
INT4 tacProcessAuthorFail ARG_LIST((UINT1 *, tTacAuthorSessionInfo *, 
                                    tTacAuthorOutput *));
INT4 tacProcessAuthorError ARG_LIST((UINT1 *, tTacAuthorSessionInfo *, 
                                     tTacAuthorOutput *));
INT4 tacProcessAuthorFollow ARG_LIST((UINT1 *, tTacAuthorSessionInfo *, 
                                      tTacAuthorOutput *));
INT4 tacAuthorChkSessionTimeout ARG_LIST((VOID));

INT4 TacAuthorEstablishSession (tTacAuthorInput *, UINT4,
                                tTacAuthorSessionInfo **);

/* Accounting module function prototypes */
INT4 tacacsAccounting ARG_LIST((tTacAcctInput *));

INT4 tacAcctValidateInput ARG_LIST((tTacAcctInput *));

INT4 tacAcctCreateSession ARG_LIST((tTacAcctInput *, UINT4 , 
                          tTacAcctSessionInfo **));
INT4 tacAcctReleaseSession ARG_LIST((tTacAcctSessionInfo *));
INT4 tacAcctConstructPacket ARG_LIST((tTacAcctSessionInfo *, tTacAcctInput *, 
                                      UINT1 *));
INT4 tacAcctConstructReqBody ARG_LIST((tTacAcctSessionInfo *, tTacAcctInput *, 
                                       UINT1 *));
INT4 tacAcctConstructHeader ARG_LIST((tTacAcctSessionInfo *, tTacAcctInput *, 
                                      UINT1 *));
INT4 tacAcctReceivedPkt ARG_LIST((UINT1  *));
INT4 tacProcessAcctReply ARG_LIST((UINT1 *, tTacAcctSessionInfo **, 
                                   tTacAcctOutput *));
INT4 tacValidateAcctReply ARG_LIST((UINT1 *, tTacAcctSessionInfo  *));
INT4 tacAcctOutputFillCmn ARG_LIST((UINT1 *, tTacAcctSessionInfo *, 
                                    tTacAcctOutput *));
INT4 tacAcctOutputFillData ARG_LIST((UINT1 *, tTacAcctSessionInfo *, 
                                     tTacAcctOutput *));
INT4 tacProcessAcctSuccess ARG_LIST((UINT1 *, tTacAcctSessionInfo *, 
                                     tTacAcctOutput *));
INT4 tacProcessAcctError ARG_LIST((UINT1 *, tTacAcctSessionInfo *, 
                                   tTacAcctOutput *));
INT4 tacProcessAcctFollow ARG_LIST((UINT1 *, tTacAcctSessionInfo *, 
                                    tTacAcctOutput *));
INT4 tacAcctChkSessionTimeout ARG_LIST((VOID));

INT4 TacAcctEstablishSession (tTacAcctInput *, UINT4 ,
                              tTacAcctSessionInfo **);

#endif /* __TPPROT_H__ */

