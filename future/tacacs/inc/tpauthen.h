/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpauthen.h,v 1.7 2015/04/28 12:49:58 siva Exp $
 *
 * Description: This header file for TACACS.
 *******************************************************************/
#ifndef __TPAUTHEN_H__
#define __TPAUTHEN_H__
/* Constants definitions for Authentication */

/* Authentication actions */

#define   TAC_AUTHEN_CHPASS   0x02
#define   TAC_AUTHEN_SENDPASS   0x03
#define   TAC_AUTHEN_SENDAUTH   0x04

#define   TAC_AUTHEN_TERMINATE   0x05  /* Implementation specific */

/* Authentication reply status */

#define   TAC_AUTHEN_STATUS_GETDATA   0x03
#define   TAC_AUTHEN_STATUS_GETUSER   0x04
#define   TAC_AUTHEN_STATUS_GETPASS   0x05
#define   TAC_AUTHEN_STATUS_RESTART   0x06
#define   TAC_AUTHEN_STATUS_ERROR   0x07
#define   TAC_AUTHEN_STATUS_FOLLOW   0x21

#define   TAC_REPLY_FLAG_NOECHO   0x01
#define   TAC_CONTINUE_FLAG_ABORT   0x01

/* Authentication session states */

#define   TAC_AUTHEN_START_SENT   1
#define   TAC_AUTHEN_CONTINUE_SENT   2
#define   TAC_AUTHEN_PASS_RCVD   3
#define   TAC_AUTHEN_FAIL_RCVD   4
#define   TAC_AUTHEN_GETDATA_RCVD   5
#define   TAC_AUTHEN_GETUSER_RCVD   6
#define   TAC_AUTHEN_GETPASS_RCVD   7
#define   TAC_AUTHEN_RESTART_RCVD   8
#define   TAC_AUTHEN_ERROR_RCVD   9
#define   TAC_AUTHEN_FOLLOW_RCVD   10

#define   TAC_AUTHEN_PKT_SIZE   3072
#define   TAC_AUTHEN_PKT_START   0x01
#define   TAC_AUTHEN_PKT_CONT   0x02

#define   TAC_AUTHEN_START_LEN_FIXED_FIELDS   8
#define   TAC_AUTHEN_CONT_LEN_FIXED_FIELDS   5
#define   TAC_AUTHEN_REPLY_LEN_FIXED_FIELDS   6

#define   TAC_AUTHEN_START_ACTION_OFFSET   TAC_PKT_HEADER_LEN
#define   TAC_AUTHEN_START_PL_OFFSET   TAC_AUTHEN_START_ACTION_OFFSET + 1
#define   TAC_AUTHEN_START_AUTHEN_TYPE_OFFSET   TAC_AUTHEN_START_PL_OFFSET +1
#define   TAC_AUTHEN_START_SVC_OFFSET   TAC_AUTHEN_START_AUTHEN_TYPE_OFFSET + 1
#define   TAC_AUTHEN_START_USER_LEN_OFFSET   TAC_AUTHEN_START_SVC_OFFSET + 1
#define   TAC_AUTHEN_START_PORT_LEN_OFFSET   TAC_AUTHEN_START_USER_LEN_OFFSET + 1
#define   TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET   TAC_AUTHEN_START_PORT_LEN_OFFSET + 1
#define   TAC_AUTHEN_START_DATA_LEN_OFFSET   TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET + 1
#define   TAC_AUTHEN_START_USER_OFFSET   TAC_AUTHEN_START_DATA_LEN_OFFSET + 1

#define   TAC_AUTHEN_CONT_USER_MSG_LEN_OFFSET   TAC_PKT_HEADER_LEN
#define   TAC_AUTHEN_CONT_DATA_LEN_OFFSET   TAC_AUTHEN_CONT_USER_MSG_LEN_OFFSET + 2
#define   TAC_AUTHEN_CONT_FLAGS_OFFSET   TAC_AUTHEN_CONT_DATA_LEN_OFFSET + 2
#define   TAC_AUTHEN_CONT_USER_MSG_OFFSET   TAC_AUTHEN_CONT_FLAGS_OFFSET + 1

#define   TAC_AUTHEN_REPLY_STATUS_OFFSET   TAC_PKT_HEADER_LEN
#define   TAC_AUTHEN_REPLY_FLAGS_OFFSET   TAC_AUTHEN_REPLY_STATUS_OFFSET + 1
#define   TAC_AUTHEN_REPLY_SRV_MSG_LEN_OFFSET   TAC_AUTHEN_REPLY_FLAGS_OFFSET + 1
#define   TAC_AUTHEN_REPLY_DATA_LEN_OFFSET   TAC_AUTHEN_REPLY_SRV_MSG_LEN_OFFSET + 2
#define   TAC_AUTHEN_REPLY_SRV_MSG_OFFSET   TAC_AUTHEN_REPLY_DATA_LEN_OFFSET + 2

/* Macro definitions for authentication */

#define   TAC_AUTHEN_GLOBAL_DATA   gTacAuthenInfo
#define   TAC_AUTHEN_SESSION_TABLE   gTacAuthenInfo.apTacAuthenSessionTable
#define   TAC_AUTHEN_SESSION_POOL_ID  TACACSMemPoolIds[MAX_TAC_AUTHEN_SESSION_SIZING_ID] 
#define   TAC_AUTHEN_NO_OF_CURRENT_SESSIONS   \
                                        gTacAuthenInfo.u1NoOfAuthenSessions
#define   TAC_AUTHEN_OUTPUT_POOL_ID  TACACSMemPoolIds[MAX_TAC_AUTHEN_OUTPUT_SIZING_ID] 


typedef struct _tacauthensessioninfo {
    tTacAuthenInput   AuthenInput;
    tTacPkt   TacTempPkt;   /* Used to store the packet (incompletely received
                             * packets) till the packet is received completely
                             * in multiple connection mode of operation
                             */

    UINT4   u4TacServer;
    UINT4   u4SessionIdentifier;
    UINT4   u4Timer;
    UINT4   u4ConnectionId;
    UINT1   u1SessionState;
    UINT1   u1SequenceNoRcvd;
    UINT1   u1MinorVersionSent;
    UINT1   au1Padding[1];
} tTacAuthenSessionInfo;

typedef struct _tacauthengen {
    tTacAuthenSessionInfo   
        *apTacAuthenSessionTable[TAC_MAX_AUTHEN_SESSION_LIMIT];
    UINT1   u1NoOfAuthenSessions;
    UINT1   au1Padding[3];
} tTacAuthenGen;

/* Global data declarations for authentication */

#ifdef __TPAUTHEN_C__
tTacAuthenGen   gTacAuthenInfo;
#else
extern tTacAuthenGen   gTacAuthenInfo;
#endif

#endif /*  __TPAUTHEN_H__ */
