/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacslw.h,v 1.1 2015/04/28 12:49:58 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacClntExtActiveServerAddressType ARG_LIST((INT4 *));

INT1
nmhGetFsTacClntExtActiveServer ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTacClntExtTraceLevel ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtRetransmit ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTacClntExtActiveServerAddressType ARG_LIST((INT4 ));

INT1
nmhSetFsTacClntExtActiveServer ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsTacClntExtTraceLevel ARG_LIST((UINT4 ));

INT1
nmhSetFsTacClntExtRetransmit ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTacClntExtActiveServerAddressType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsTacClntExtActiveServer ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsTacClntExtTraceLevel ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsTacClntExtRetransmit ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTacClntExtActiveServerAddressType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTacClntExtActiveServer ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTacClntExtTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsTacClntExtRetransmit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacClntExtAuthenStartRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenContinueRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenEnableRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenAbortRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenPassReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenFailReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenGetUserReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenGetPassReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenGetDataReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenErrorReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenFollowReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenRestartReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthenSessionTimouts ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthorRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthorPassAddReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthorPassReplReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthorFailReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthorErrorReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthorFollowReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAuthorSessionTimeouts ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAcctStartRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAcctWdRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAcctStopRequests ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAcctSuccessReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAcctErrorReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAcctFollowReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtAcctSessionTimeouts ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtMalformedPktsReceived ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtSocketFailures ARG_LIST((UINT4 *));

INT1
nmhGetFsTacClntExtConnectionFailures ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsTacClntExtServerTable. */
INT1
nmhValidateIndexInstanceFsTacClntExtServerTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsTacClntExtServerTable  */

INT1
nmhGetFirstIndexFsTacClntExtServerTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTacClntExtServerTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTacClntExtServerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsTacClntExtServerSingleConnect ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsTacClntExtServerPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsTacClntExtServerTimeout ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetFsTacClntExtServerKey ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTacClntExtServerStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsTacClntExtServerSingleConnect ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsTacClntExtServerPort ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsTacClntExtServerTimeout ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsTacClntExtServerKey ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTacClntExtServerStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsTacClntExtServerSingleConnect ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsTacClntExtServerPort ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsTacClntExtServerTimeout ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsTacClntExtServerKey ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTacClntExtServerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
