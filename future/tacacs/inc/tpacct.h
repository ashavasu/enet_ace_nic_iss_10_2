/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpacct.h,v 1.7 2015/04/28 12:49:58 siva Exp $
 *
 * Description: This header file for TACACS.
 *******************************************************************/
#ifndef __TPACCT_H__
#define __TPACCT_H__

/* Constant definitions for accounting */

/* Accounting request flags */

#define   TAC_ACCT_FLAG_MORE   0x01
#define   TAC_ACCT_FLAG_START   0x02
#define   TAC_ACCT_FLAG_STOP   0x04
#define   TAC_ACCT_FLAG_WD   0x08
#define   TAC_ACCT_FLAG_WD_START  (TAC_ACCT_FLAG_WD | TAC_ACCT_FLAG_START)

/* Accounting reply status */

#define   TAC_ACCT_STATUS_SUCCESS   0x01
#define   TAC_ACCT_STATUS_ERROR   0x02
#define   TAC_ACCT_STATUS_FOLLOW   0x21


#define   TAC_ACCT_PKT_SIZE   4096


#define   TAC_ACCT_REQ_LEN_FIXED_FIELDS   9
#define   TAC_ACCT_REPLY_LEN_FIXED_FIELDS   5

#define   TAC_ACCT_REQ_FLAG_OFFSET   TAC_PKT_HEADER_LEN
#define   TAC_ACCT_REQ_AUTHEN_METHOD_OFFSET   TAC_ACCT_REQ_FLAG_OFFSET + 1
#define   TAC_ACCT_REQ_PL_OFFSET   TAC_ACCT_REQ_AUTHEN_METHOD_OFFSET + 1
#define   TAC_ACCT_REQ_AUTHEN_TYPE_OFFSET   TAC_ACCT_REQ_PL_OFFSET +1
#define   TAC_ACCT_REQ_SVC_OFFSET   TAC_ACCT_REQ_AUTHEN_TYPE_OFFSET + 1
#define   TAC_ACCT_REQ_USER_LEN_OFFSET   TAC_ACCT_REQ_SVC_OFFSET + 1
#define   TAC_ACCT_REQ_PORT_LEN_OFFSET   TAC_ACCT_REQ_USER_LEN_OFFSET + 1
#define   TAC_ACCT_REQ_REM_ADDR_LEN_OFFSET   TAC_ACCT_REQ_PORT_LEN_OFFSET + 1
#define   TAC_ACCT_REQ_ARG_CNT_OFFSET   TAC_ACCT_REQ_REM_ADDR_LEN_OFFSET + 1
#define   TAC_ACCT_REQ_ARG1_LEN_OFFSET   TAC_ACCT_REQ_ARG_CNT_OFFSET  + 1

#define   TAC_ACCT_REPLY_SRV_MSG_LEN_OFFSET   TAC_PKT_HEADER_LEN
#define   TAC_ACCT_REPLY_DATA_LEN_OFFSET   TAC_ACCT_REPLY_SRV_MSG_LEN_OFFSET + 2
#define   TAC_ACCT_REPLY_STATUS_OFFSET   TAC_ACCT_REPLY_DATA_LEN_OFFSET + 2
#define   TAC_ACCT_REPLY_SRV_MSG_OFFSET   TAC_ACCT_REPLY_STATUS_OFFSET + 1

/* Macro definitions for accounting */

#define   TAC_ACCT_GLOBAL_DATA   gTacAcctInfo
#define   TAC_ACCT_SESSION_TABLE   gTacAcctInfo.apTacAcctSessionTable
#define   TAC_ACCT_SESSION_POOL_ID    TACACSMemPoolIds[MAX_TAC_ACCT_SESSION_SIZING_ID]
#define   TAC_ACCT_NO_OF_CURRENT_SESSIONS   gTacAcctInfo.u1NoOfAcctSessions

/* Data structures definition for accounting */

typedef struct _tacacctsessioninfo {
    tTacAcctInput    AcctInput;
    tTacPkt   TacTempPkt;   /* Used to store the packet (incompletely received
                             * packets) till the packet is received completely
                             * in multiple connection mode of operation
                             */

    UINT4   u4TacServer;
    UINT4   u4SessionIdentifier;
    UINT4   u4Timer;
    UINT4   u4ConnectionId;
    UINT1   u1SequenceNoRcvd;
    UINT1   u1MinorVersionSent;
    UINT1   au1Padding[2];
} tTacAcctSessionInfo;

typedef struct _tacacctgen {
    tTacAcctSessionInfo   *apTacAcctSessionTable[TAC_MAX_ACCT_SESSION_LIMIT];
    UINT1   u1NoOfAcctSessions;
    UINT1   au1Padding[3];
} tTacAcctGen;

/* Global data declarations for accounting */

#ifdef __TPACCT_C__
tTacAcctGen   gTacAcctInfo;
#else
extern tTacAcctGen   gTacAcctInfo;
#endif

#endif /* __TPACCT_H__ */
