/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: tacsz.h,v 1.1 2015/04/28 12:49:58 siva Exp $
 *
 * Description:This file contains prototypes of mempools Creation/
 *             Deletion functions.
 *
 *******************************************************************/

enum {
    MAX_TAC_ACCT_SESSION_SIZING_ID,
    MAX_TAC_AUTHEN_OUTPUT_SIZING_ID,
    MAX_TAC_AUTHEN_SESSION_SIZING_ID,
    MAX_TAC_AUTHOR_OUTPUT_SIZING_ID,
    MAX_TAC_AUTHOR_SESSION_SIZING_ID,
    MAX_TAC_PKT_COUNT_SIZING_ID,
    TACACS_MAX_SIZING_ID
};


#ifdef  _TACACSSZ_C
tMemPoolId TACACSMemPoolIds[ TACACS_MAX_SIZING_ID];
INT4  TacacsSizingMemCreateMemPools(VOID);
VOID  TacacsSizingMemDeleteMemPools(VOID);
INT4  TacacsSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _TACACSSZ_C  */
extern tMemPoolId TACACSMemPoolIds[ ];
extern INT4  TacacsSizingMemCreateMemPools(VOID);
extern VOID  TacacsSizingMemDeleteMemPools(VOID);
extern INT4  TacacsSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _TACACSSZ_C  */


#ifdef  _TACACSSZ_C
tFsModSizingParams FsTACACSSizingParams [] = {
{ "tTacAcctSessionInfo", "MAX_TAC_ACCT_SESSION", sizeof(tTacAcctSessionInfo),MAX_TAC_ACCT_SESSION, MAX_TAC_ACCT_SESSION,0 },
{ "tTacAuthenOutput", "MAX_TAC_AUTHEN_OUTPUT", sizeof(tTacAuthenOutput),MAX_TAC_AUTHEN_OUTPUT, MAX_TAC_AUTHEN_OUTPUT,0 },
{ "tTacAuthenSessionInfo", "MAX_TAC_AUTHEN_SESSION", sizeof(tTacAuthenSessionInfo),MAX_TAC_AUTHEN_SESSION, MAX_TAC_AUTHEN_SESSION,0 },
{ "tTacAuthorOutput", "MAX_TAC_AUTHOR_OUTPUT", sizeof(tTacAuthorOutput),MAX_TAC_AUTHOR_OUTPUT, MAX_TAC_AUTHOR_OUTPUT,0 },
{ "tTacAuthorSessionInfo", "MAX_TAC_AUTHOR_SESSION", sizeof(tTacAuthorSessionInfo),MAX_TAC_AUTHOR_SESSION, MAX_TAC_AUTHOR_SESSION,0 },
{ "MAX_TACACS_PKT_SIZE", "MAX_TAC_PKT_COUNT", MAX_TACACS_PKT_SIZE, MAX_TAC_PKT_COUNT,MAX_TAC_PKT_COUNT,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _TACACSSZ_C  */
extern tFsModSizingParams FsTACACSSizingParams [];
#endif /*  _TACACSSZ_C  */


