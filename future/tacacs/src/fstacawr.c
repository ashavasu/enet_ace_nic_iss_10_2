# include  "lr.h"
# include  "fssnmp.h"
# include  "fstacalw.h"
# include  "fstacawr.h"
# include  "fstacadb.h"
# include  "tpprot.h"

VOID
RegisterFSTACA ()
{
    SNMPRegisterMibWithLock (&fstacaOID, &fstacaEntry, TacProtocolLock,
                             TacProtocolUnlock, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fstacaOID, (const UINT1 *) "fstacacs");
}

VOID
UnRegisterFSTACA ()
{
    SNMPUnRegisterMib (&fstacaOID, &fstacaEntry);
    SNMPDelSysorEntry (&fstacaOID, (const UINT1 *) "fstacacs");
}

INT4
FsTacClntActiveServerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntActiveServer (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntTraceLevel (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntRetransmitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntRetransmit (&(pMultiData->i4_SLongValue)));
}

INT4
FsTacClntActiveServerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsTacClntActiveServer (pMultiData->u4_ULongValue));
}

INT4
FsTacClntTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsTacClntTraceLevel (pMultiData->u4_ULongValue));
}

INT4
FsTacClntRetransmitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsTacClntRetransmit (pMultiData->i4_SLongValue));
}

INT4
FsTacClntActiveServerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsTacClntActiveServer
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsTacClntTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsTacClntTraceLevel (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsTacClntRetransmitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsTacClntRetransmit (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsTacClntActiveServerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntActiveServer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsTacClntTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsTacClntRetransmitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntRetransmit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsTacClntAuthenStartRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenStartRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenContinueRequestsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenContinueRequests
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenEnableRequestsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenEnableRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenAbortRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenAbortRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenPassReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenPassReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenFailReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenFailReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenGetUserReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenGetUserReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenGetPassReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenGetPassReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenGetDataReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenGetDataReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenErrorReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenErrorReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenFollowReceivedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenFollowReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenRestartReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenRestartReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthenSessionTimoutsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthenSessionTimouts (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthorRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthorRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthorPassAddReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthorPassAddReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthorPassReplReceivedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthorPassReplReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthorFailReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthorFailReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthorErrorReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthorErrorReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthorFollowReceivedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthorFollowReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAuthorSessionTimeoutsGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAuthorSessionTimeouts
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAcctStartRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAcctStartRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAcctWdRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAcctWdRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAcctStopRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAcctStopRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAcctSuccessReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAcctSuccessReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAcctErrorReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAcctErrorReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAcctFollowReceivedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAcctFollowReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntAcctSessionTimeoutsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntAcctSessionTimeouts (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntMalformedPktsReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntMalformedPktsReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntSocketFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntSocketFailures (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntConnectionFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntConnectionFailures (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexFsTacClntServerTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsTacClntServerTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsTacClntServerTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsTacClntServerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntServerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntServerStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue)));

}

INT4
FsTacClntServerSingleConnectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntServerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntServerSingleConnect
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsTacClntServerPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntServerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntServerPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
FsTacClntServerTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntServerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntServerTimeout (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsTacClntServerKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntServerTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntServerKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsTacClntServerStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntServerStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
FsTacClntServerSingleConnectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntServerSingleConnect
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsTacClntServerPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntServerPort (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
FsTacClntServerTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntServerTimeout (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsTacClntServerKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntServerKey (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue));

}

INT4
FsTacClntServerStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntServerStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));

}

INT4
FsTacClntServerSingleConnectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntServerSingleConnect (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsTacClntServerPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntServerPort (pu4Error,
                                          pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsTacClntServerTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntServerTimeout (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsTacClntServerKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntServerKey (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsTacClntServerTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntServerTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
