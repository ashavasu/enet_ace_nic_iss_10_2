/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacslw.c,v 1.3 2016/10/07 13:25:15 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fstacslw.h"
# include  "tpinc.h"
# include  "tpcli.h"
extern tIssBool MsrGetSaveStatus PROTO ((VOID));
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacClntExtActiveServerAddressType
 Input       :  The Indices

                The Object 
                retValFsTacClntExtActiveServerAddressType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtActiveServerAddressType (INT4
                                           *pi4RetValFsTacClntExtActiveServerAddressType)
{
    *pi4RetValFsTacClntExtActiveServerAddressType = (INT4) TAC_ADDRESS_TYPE;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtActiveServer
 Input       :  The Indices

                The Object 
                retValFsTacClntExtActiveServer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtActiveServer (tSNMP_OCTET_STRING_TYPE *
                                pRetValFsTacClntExtActiveServer)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT4               u4IfAddr = TAC_ZERO;

    if (TAC_ACTIVE_SERVER == TAC_ZERO)
    {
        MEMCPY (pRetValFsTacClntExtActiveServer->pu1_OctetList, &u4IfAddr,
                TAC_DEFAULT_SERVER_LEN);
        pRetValFsTacClntExtActiveServer->i4_Length = TAC_DEFAULT_SERVER_LEN;
        TAC_ADDRESS_TYPE = TAC_ZERO;
    }
    else
    {
        if ((TAC_ACTIVE_SERVER - 1) < TAC_MAX_SERVERS)
	{
	    pTacSrv = &(TAC_SERVER_TABLE[TAC_ACTIVE_SERVER - 1]);
	    if ( pTacSrv->IpAddress.u1Afi != IPVX_DNS_FAMILY )
	    {
		MEMCPY (pRetValFsTacClntExtActiveServer->pu1_OctetList,
			pTacSrv->IpAddress.au1Addr, pTacSrv->IpAddress.u1AddrLen);
		pRetValFsTacClntExtActiveServer->i4_Length =
		    pTacSrv->IpAddress.u1AddrLen;
	    }
	    else
	    {
		STRNCPY (pRetValFsTacClntExtActiveServer->pu1_OctetList,
			 pTacSrv->au1HostName,
                         STRLEN(pTacSrv->au1HostName)+1);
                pRetValFsTacClntExtActiveServer->pu1_OctetList
                     [STRLEN(pTacSrv->au1HostName)] = '\0';
		pRetValFsTacClntExtActiveServer->i4_Length =
		    STRLEN(pTacSrv->au1HostName);
	    }

	}
        else
        {

            MEMSET (pRetValFsTacClntExtActiveServer->pu1_OctetList,
                    TAC_DEFAULT_SERVER,
                    pRetValFsTacClntExtActiveServer->i4_Length);
            pRetValFsTacClntExtActiveServer->i4_Length = TAC_DEFAULT_SERVER_LEN;
            TAC_ADDRESS_TYPE = TAC_ZERO;

        }
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtTraceLevel
 Input       :  The Indices

                The Object 
                retValFsTacClntExtTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtTraceLevel (UINT4 *pu4RetValFsTacClntExtTraceLevel)
{
    *pu4RetValFsTacClntExtTraceLevel = TAC_TRACE_LEVEL;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtRetransmit
 Input       :  The Indices

                The Object 
                retValFsTacClntExtRetransmit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtRetransmit (INT4 *pi4RetValFsTacClntExtRetransmit)
{
    *pi4RetValFsTacClntExtRetransmit = TAC_MAX_RETRIES;
    return (SNMP_SUCCESS);

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTacClntExtActiveServerAddressType
 Input       :  The Indices

                The Object 
                setValFsTacClntExtActiveServerAddressType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtActiveServerAddressType (INT4
                                           i4SetValFsTacClntExtActiveServerAddressType)
{
    TAC_ADDRESS_TYPE = (UINT1) i4SetValFsTacClntExtActiveServerAddressType;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsTacClntExtActiveServer
 Input       :  The Indices

                The Object 
                setValFsTacClntExtActiveServer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtActiveServer (tSNMP_OCTET_STRING_TYPE *
                                pSetValFsTacClntExtActiveServer)
{

    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    UINT1               au1Tnp[IPVX_IPV6_ADDR_LEN];
    UINT1               au1HostName[DNS_MAX_QUERY_LEN];

    /* Get first index (numerically first index) */
    MEMSET (au1Tnp, TAC_ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1HostName, TAC_ZERO, DNS_MAX_QUERY_LEN);

    /*If a active server is set and if we want to disable the active server
       concept and to use retries, the active server should be set as TAC_ZERO */
    if ((pSetValFsTacClntExtActiveServer->i4_Length == 0) ||
        ((MEMCMP (pSetValFsTacClntExtActiveServer->pu1_OctetList, au1Tnp,                                                                       pSetValFsTacClntExtActiveServer->i4_Length) == TAC_ZERO) &&
        (MEMCMP (pSetValFsTacClntExtActiveServer->pu1_OctetList, au1HostName,
                 pSetValFsTacClntExtActiveServer->i4_Length) == TAC_ZERO)))
    {
        TAC_ACTIVE_SERVER = TAC_ZERO;
        TAC_ADDRESS_TYPE = TAC_ZERO;
        return (SNMP_SUCCESS);
    }
    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if (TAC_COMP(pTacSrv,pSetValFsTacClntExtActiveServer) == TAC_SUCCESS)
        {
            TAC_ACTIVE_SERVER = (UINT1) (u1Index + 1);
            TAC_ADDRESS_TYPE = pTacSrv->IpAddress.u1Afi;
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* IP Address is not matching with IP address of any server */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsTacClntExtTraceLevel
 Input       :  The Indices

                The Object 
                setValFsTacClntExtTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtTraceLevel (UINT4 u4SetValFsTacClntExtTraceLevel)
{
    TAC_TRACE_LEVEL = u4SetValFsTacClntExtTraceLevel;
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhSetFsTacClntExtRetransmit
 Input       :  The Indices

                The Object 
                setValFsTacClntExtRetransmit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtRetransmit (INT4 i4SetValFsTacClntExtRetransmit)
{
    TAC_MAX_RETRIES = i4SetValFsTacClntExtRetransmit;
    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtActiveServerAddressType
 Input       :  The Indices

                The Object 
                testValFsTacClntExtActiveServerAddressType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtActiveServerAddressType (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4TestValFsTacClntExtActiveServerAddressType)
{
    if (i4TestValFsTacClntExtActiveServerAddressType == IPVX_ADDR_FMLY_IPV4 ||
        i4TestValFsTacClntExtActiveServerAddressType == TAC_ZERO)
    {
        return SNMP_SUCCESS;
    }

#ifdef IP6_WANTED
    if (i4TestValFsTacClntExtActiveServerAddressType == IPVX_ADDR_FMLY_IPV6)
    {
        return SNMP_SUCCESS;
    }
#endif
    if (i4TestValFsTacClntExtActiveServerAddressType == IPVX_DNS_FAMILY)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtActiveServer
 Input       :  The Indices

                The Object 
                testValFsTacClntExtActiveServer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtActiveServer (UINT4 *pu4ErrorCode,
	tSNMP_OCTET_STRING_TYPE *
	pTestValFsTacClntExtActiveServer)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    UINT1               au1TmpArray[IPVX_IPV6_ADDR_LEN];
    UINT1               au1HostTmpArray[DNS_MAX_QUERY_LEN];

    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1HostTmpArray, TAC_ZERO, DNS_MAX_QUERY_LEN);
    
    if ((pTestValFsTacClntExtActiveServer->i4_Length == 0) ||
	    ((MEMCMP (pTestValFsTacClntExtActiveServer->pu1_OctetList, au1TmpArray,
				  pTestValFsTacClntExtActiveServer->i4_Length) == TAC_ZERO) &&
	    (MEMCMP (pTestValFsTacClntExtActiveServer->pu1_OctetList, au1HostTmpArray,
		     pTestValFsTacClntExtActiveServer->i4_Length)== TAC_ZERO)))
    {
	return SNMP_SUCCESS;
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
	pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
	if ((TAC_COMP(pTacSrv,pTestValFsTacClntExtActiveServer) == TAC_SUCCESS)
		&& (pTacSrv->u4Status != DESTROY))
	{
	    break;
	}
    }
    if (u1Index >= TAC_MAX_SERVERS)
    {
	/* IP Address is not matching with IP address of any server */
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CLI_SET_ERR (CLI_TAC_INV_SRV);
	return (SNMP_FAILURE);
    }
    else
    {
	if ((pTacSrv->u4Status != ACTIVE)
		&& (pTacSrv->u4Status != NOT_IN_SERVICE))
	{
	    /* Server is not active. This can not be used by Tacacs+ client */
	    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	    CLI_SET_ERR (CLI_TAC_INV_SRV);
	    return (SNMP_FAILURE);
	}
    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtTraceLevel
 Input       :  The Indices

                The Object 
                testValFsTacClntExtTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtTraceLevel (UINT4 *pu4ErrorCode,
                                 UINT4 u4TestValFsTacClntExtTraceLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValFsTacClntExtTraceLevel);

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtRetransmit
 Input       :  The Indices

                The Object 
                testValFsTacClntExtRetransmit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtRetransmit (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsTacClntExtRetransmit)
{
    if (i4TestValFsTacClntExtRetransmit < TAC_MIN_RETRANSMITS ||
        i4TestValFsTacClntExtRetransmit > TAC_MAX_RETRANSMITS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_TAC_INV_RETRIES);
        return (SNMP_FAILURE);
    }

/* This check is added as to ensure that we need not to travese more than the MAX SERVERS supported*/

    if (i4TestValFsTacClntExtRetransmit > TAC_MAX_SERVERS)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_TAC_RTRY_EXCEED_SERVER);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTacClntExtActiveServerAddressType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntExtActiveServerAddressType (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacClntExtActiveServer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntExtActiveServer (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacClntExtTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntExtTraceLevel (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacClntExtRetransmit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntExtRetransmit (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenStartRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenStartRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenStartRequests (UINT4
                                       *pu4RetValFsTacClntExtAuthenStartRequests)
{
    *pu4RetValFsTacClntExtAuthenStartRequests = TAC_CLNT_LOG.u4AuthenStarts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenContinueRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenContinueRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenContinueRequests (UINT4
                                          *pu4RetValFsTacClntExtAuthenContinueRequests)
{
    *pu4RetValFsTacClntExtAuthenContinueRequests = TAC_CLNT_LOG.u4AuthenConts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenEnableRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenEnableRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenEnableRequests (UINT4
                                        *pu4RetValFsTacClntExtAuthenEnableRequests)
{
    *pu4RetValFsTacClntExtAuthenEnableRequests = TAC_CLNT_LOG.u4EnableReqs;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenAbortRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenAbortRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenAbortRequests (UINT4
                                       *pu4RetValFsTacClntExtAuthenAbortRequests)
{
    *pu4RetValFsTacClntExtAuthenAbortRequests = TAC_CLNT_LOG.u4AuthenAborts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenPassReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenPassReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenPassReceived (UINT4
                                      *pu4RetValFsTacClntExtAuthenPassReceived)
{
    *pu4RetValFsTacClntExtAuthenPassReceived = TAC_CLNT_LOG.u4AuthenPass;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenFailReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenFailReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenFailReceived (UINT4
                                      *pu4RetValFsTacClntExtAuthenFailReceived)
{
    *pu4RetValFsTacClntExtAuthenFailReceived = TAC_CLNT_LOG.u4AuthenFails;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenGetUserReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenGetUserReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenGetUserReceived (UINT4
                                         *pu4RetValFsTacClntExtAuthenGetUserReceived)
{
    *pu4RetValFsTacClntExtAuthenGetUserReceived = TAC_CLNT_LOG.u4AuthenGetUsers;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenGetPassReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenGetPassReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenGetPassReceived (UINT4
                                         *pu4RetValFsTacClntExtAuthenGetPassReceived)
{
    *pu4RetValFsTacClntExtAuthenGetPassReceived = TAC_CLNT_LOG.u4AuthenGetPass;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenGetDataReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenGetDataReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenGetDataReceived (UINT4
                                         *pu4RetValFsTacClntExtAuthenGetDataReceived)
{
    *pu4RetValFsTacClntExtAuthenGetDataReceived = TAC_CLNT_LOG.u4AuthenGetData;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenErrorReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenErrorReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenErrorReceived (UINT4
                                       *pu4RetValFsTacClntExtAuthenErrorReceived)
{
    *pu4RetValFsTacClntExtAuthenErrorReceived = TAC_CLNT_LOG.u4AuthenErrors;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenFollowReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenFollowReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenFollowReceived (UINT4
                                        *pu4RetValFsTacClntExtAuthenFollowReceived)
{
    *pu4RetValFsTacClntExtAuthenFollowReceived = TAC_CLNT_LOG.u4AuthenFollows;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenRestartReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenRestartReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenRestartReceived (UINT4
                                         *pu4RetValFsTacClntExtAuthenRestartReceived)
{
    *pu4RetValFsTacClntExtAuthenRestartReceived = TAC_CLNT_LOG.u4AuthenRestarts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthenSessionTimouts
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthenSessionTimouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthenSessionTimouts (UINT4
                                        *pu4RetValFsTacClntExtAuthenSessionTimouts)
{
    *pu4RetValFsTacClntExtAuthenSessionTimouts =
        TAC_CLNT_LOG.u4AuthenSessionTimeouts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthorRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthorRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthorRequests (UINT4 *pu4RetValFsTacClntExtAuthorRequests)
{
    *pu4RetValFsTacClntExtAuthorRequests = TAC_CLNT_LOG.u4AuthorReqs;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthorPassAddReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthorPassAddReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthorPassAddReceived (UINT4
                                         *pu4RetValFsTacClntExtAuthorPassAddReceived)
{
    *pu4RetValFsTacClntExtAuthorPassAddReceived = TAC_CLNT_LOG.u4AuthorPassAdds;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthorPassReplReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthorPassReplReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthorPassReplReceived (UINT4
                                          *pu4RetValFsTacClntExtAuthorPassReplReceived)
{
    *pu4RetValFsTacClntExtAuthorPassReplReceived =
        TAC_CLNT_LOG.u4AuthorPassRepls;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthorFailReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthorFailReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthorFailReceived (UINT4
                                      *pu4RetValFsTacClntExtAuthorFailReceived)
{
    *pu4RetValFsTacClntExtAuthorFailReceived = TAC_CLNT_LOG.u4AuthorFails;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthorErrorReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthorErrorReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthorErrorReceived (UINT4
                                       *pu4RetValFsTacClntExtAuthorErrorReceived)
{
    *pu4RetValFsTacClntExtAuthorErrorReceived = TAC_CLNT_LOG.u4AuthorErrors;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthorFollowReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthorFollowReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthorFollowReceived (UINT4
                                        *pu4RetValFsTacClntExtAuthorFollowReceived)
{
    *pu4RetValFsTacClntExtAuthorFollowReceived = TAC_CLNT_LOG.u4AuthorFollows;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAuthorSessionTimeouts
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAuthorSessionTimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAuthorSessionTimeouts (UINT4
                                         *pu4RetValFsTacClntExtAuthorSessionTimeouts)
{
    *pu4RetValFsTacClntExtAuthorSessionTimeouts =
        TAC_CLNT_LOG.u4AuthorSessionTimeouts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAcctStartRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAcctStartRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAcctStartRequests (UINT4
                                     *pu4RetValFsTacClntExtAcctStartRequests)
{
    *pu4RetValFsTacClntExtAcctStartRequests = TAC_CLNT_LOG.u4AcctStartReqs;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAcctWdRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAcctWdRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAcctWdRequests (UINT4 *pu4RetValFsTacClntExtAcctWdRequests)
{
    *pu4RetValFsTacClntExtAcctWdRequests = TAC_CLNT_LOG.u4AcctWdReqs;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAcctStopRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAcctStopRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAcctStopRequests (UINT4
                                    *pu4RetValFsTacClntExtAcctStopRequests)
{
    *pu4RetValFsTacClntExtAcctStopRequests = TAC_CLNT_LOG.u4AcctStopReqs;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAcctSuccessReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAcctSuccessReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAcctSuccessReceived (UINT4
                                       *pu4RetValFsTacClntExtAcctSuccessReceived)
{
    *pu4RetValFsTacClntExtAcctSuccessReceived = TAC_CLNT_LOG.u4AcctSuccess;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAcctErrorReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAcctErrorReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAcctErrorReceived (UINT4
                                     *pu4RetValFsTacClntExtAcctErrorReceived)
{
    *pu4RetValFsTacClntExtAcctErrorReceived = TAC_CLNT_LOG.u4AcctErrors;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAcctFollowReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAcctFollowReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAcctFollowReceived (UINT4
                                      *pu4RetValFsTacClntExtAcctFollowReceived)
{
    *pu4RetValFsTacClntExtAcctFollowReceived = TAC_CLNT_LOG.u4AcctFollows;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtAcctSessionTimeouts
 Input       :  The Indices

                The Object 
                retValFsTacClntExtAcctSessionTimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtAcctSessionTimeouts (UINT4
                                       *pu4RetValFsTacClntExtAcctSessionTimeouts)
{
    *pu4RetValFsTacClntExtAcctSessionTimeouts =
        TAC_CLNT_LOG.u4AcctSessionTimeouts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtMalformedPktsReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntExtMalformedPktsReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtMalformedPktsReceived (UINT4
                                         *pu4RetValFsTacClntExtMalformedPktsReceived)
{
    *pu4RetValFsTacClntExtMalformedPktsReceived = TAC_CLNT_LOG.u4MalformedPkts;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtSocketFailures
 Input       :  The Indices

                The Object 
                retValFsTacClntExtSocketFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtSocketFailures (UINT4 *pu4RetValFsTacClntExtSocketFailures)
{
    *pu4RetValFsTacClntExtSocketFailures = TAC_CLNT_LOG.u4SocketFailures;
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtConnectionFailures
 Input       :  The Indices

                The Object 
                retValFsTacClntExtConnectionFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtConnectionFailures (UINT4
                                      *pu4RetValFsTacClntExtConnectionFailures)
{
    *pu4RetValFsTacClntExtConnectionFailures = TAC_CLNT_LOG.u4ConnOpenFailures;
    return (SNMP_SUCCESS);
}

/* LOW LEVEL Routines for Table : FsTacClntExtServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTacClntExtServerTable
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTacClntExtServerTable (INT4
	i4FsTacClntExtServerAddressType,
	tSNMP_OCTET_STRING_TYPE *
	pFsTacClntExtServerAddress)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    
    if (i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);

        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
            && (pTacSrv->u4Status != DESTROY))
        {
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
	/* IP Address is not matching with IP address of any server */
	return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTacClntExtServerTable
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsTacClntExtServerTable (INT4 *pi4FsTacClntExtServerAddressType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pFsTacClntExtServerAddress)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    UINT1               au1Tnp[IPVX_IPV6_ADDR_LEN];
    INT4                i4Flag = TAC_ZERO;
    INT4                i4FoundValue = TAC_ZERO;
    INT4                i4IPV4EntryFoundValue = TAC_ZERO;
    INT4                i4IPV6EntryFoundValue = TAC_ZERO;

    /* Get first index (numerically first index) */

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if (i4Flag == TAC_ZERO)
        {
            MEMSET (au1Tnp, 0xFF, pTacSrv->IpAddress.u1AddrLen);
            MEMCPY (pFsTacClntExtServerAddress->pu1_OctetList,
                    &au1Tnp, pTacSrv->IpAddress.u1AddrLen);
            pFsTacClntExtServerAddress->i4_Length =
                pTacSrv->IpAddress.u1AddrLen;

        }
        if (pTacSrv->IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
        {
            if ((pTacSrv->u4Status != DESTROY)
                && (MEMCMP (pTacSrv->IpAddress.au1Addr,
                            pFsTacClntExtServerAddress->pu1_OctetList,
                            pFsTacClntExtServerAddress->i4_Length)) < TAC_ZERO)
            {
                MEMSET (pFsTacClntExtServerAddress->pu1_OctetList, TAC_ZERO,
                        pTacSrv->IpAddress.u1AddrLen);
                MEMCPY (pFsTacClntExtServerAddress->pu1_OctetList,
                        pTacSrv->IpAddress.au1Addr,
                        pTacSrv->IpAddress.u1AddrLen);
                *pi4FsTacClntExtServerAddressType = pTacSrv->IpAddress.u1Afi;
                pFsTacClntExtServerAddress->i4_Length =
                    pTacSrv->IpAddress.u1AddrLen;
                i4Flag = 1;
                i4FoundValue = 1;
                i4IPV4EntryFoundValue = 1;
            }
        }
    }
    /* If No IPV4 entry is found then check for IPV6 entry */
    if (i4IPV4EntryFoundValue == TAC_ZERO)
    {
        for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
        {
            pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
            if (i4Flag == TAC_ZERO)
            {
                MEMSET (au1Tnp, 0xFF, pTacSrv->IpAddress.u1AddrLen);
                MEMCPY (pFsTacClntExtServerAddress->pu1_OctetList,
                        &au1Tnp, pTacSrv->IpAddress.u1AddrLen);
                pFsTacClntExtServerAddress->i4_Length =
                    pTacSrv->IpAddress.u1AddrLen;

            }
        if (pTacSrv->IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV6)
        {
            if ((pTacSrv->u4Status != DESTROY)
                && (MEMCMP (pTacSrv->IpAddress.au1Addr,
                            pFsTacClntExtServerAddress->pu1_OctetList,
                            pFsTacClntExtServerAddress->i4_Length)) < TAC_ZERO)
            {
                MEMSET (pFsTacClntExtServerAddress->pu1_OctetList, TAC_ZERO,
                        pTacSrv->IpAddress.u1AddrLen);
                MEMCPY (pFsTacClntExtServerAddress->pu1_OctetList,
                        pTacSrv->IpAddress.au1Addr,
                        pTacSrv->IpAddress.u1AddrLen);
                *pi4FsTacClntExtServerAddressType = pTacSrv->IpAddress.u1Afi;
                pFsTacClntExtServerAddress->i4_Length =
                    pTacSrv->IpAddress.u1AddrLen;
                i4Flag = 1;
                i4FoundValue = 1;
                i4IPV6EntryFoundValue = 1;
            }
        }
      }   
    }

    /* If No IPVX entry is found then check for DNS entry */
    if ((i4IPV4EntryFoundValue == TAC_ZERO) && 
        (i4IPV6EntryFoundValue == TAC_ZERO))
    {
        for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
        {
            pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
            if ((pTacSrv->u4Status != DESTROY)
                && (pTacSrv->IpAddress.u1Afi == IPVX_DNS_FAMILY))
            {
                STRNCPY (pFsTacClntExtServerAddress->pu1_OctetList,
                        pTacSrv->au1HostName,
                        STRLEN (pTacSrv->au1HostName));
                *pi4FsTacClntExtServerAddressType = pTacSrv->IpAddress.u1Afi;
                pFsTacClntExtServerAddress->pu1_OctetList
                    [STRLEN (pTacSrv->au1HostName)] = '\0';
                pFsTacClntExtServerAddress->i4_Length =
                    STRLEN (pTacSrv->au1HostName);
                i4Flag = 1;
                i4FoundValue = 1;
            }
            if (i4FoundValue == 1)
            {
                break;
            }
        }
    }


    if (i4FoundValue == TAC_ZERO)
    {
        return (SNMP_FAILURE);
    }
    /* No entry is available */
    if (pFsTacClntExtServerAddress->i4_Length == IPVX_IPV4_ADDR_LEN)
    {
        if (MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
                    &au1Tnp, IPVX_IPV4_ADDR_LEN) == TAC_ZERO)
        {
            return (SNMP_FAILURE);
        }
    }
    else
    {
        if (MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
                    &au1Tnp, IPVX_IPV6_ADDR_LEN) == TAC_ZERO)
        {
            return (SNMP_FAILURE);
        }

    }

    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTacClntExtServerTable
 Input       :  The Indices
                FsTacClntExtServerAddressType
                nextFsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                nextFsTacClntExtServerAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTacClntExtServerTable (INT4 i4FsTacClntExtServerAddressType,
                                        INT4
                                        *pi4NextFsTacClntExtServerAddressType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsTacClntExtServerAddress,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextFsTacClntExtServerAddress)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    UINT1               u1FirstNextFound = TAC_ZERO;
    UINT1               u1IPV4EntryFoundValue = TAC_ZERO;
    UINT1               u1IPV6EntryFoundValue = TAC_ZERO;
    UINT1               au1Temp[DNS_MAX_QUERY_LEN];

    MEMSET (au1Temp, 0, DNS_MAX_QUERY_LEN);
    if (i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }
    MEMSET (pNextFsTacClntExtServerAddress->pu1_OctetList, TAC_ZERO,
            DNS_MAX_QUERY_LEN);

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        /* check is added to get all IPV4  entries */
        if ((pTacSrv->IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4) &&
           (i4FsTacClntExtServerAddressType == IPVX_ADDR_FMLY_IPV4))
        {
            if ((pTacSrv->u4Status != DESTROY) &&
                (MEMCMP (pTacSrv->IpAddress.au1Addr,
                         pFsTacClntExtServerAddress->pu1_OctetList,
                         pFsTacClntExtServerAddress->i4_Length)) > TAC_ZERO)
            {
                    if (u1FirstNextFound == TAC_ZERO)
                    {
                        MEMCPY (pNextFsTacClntExtServerAddress->pu1_OctetList,
                                pTacSrv->IpAddress.au1Addr,
                                pTacSrv->IpAddress.u1AddrLen);
                        *pi4NextFsTacClntExtServerAddressType =
                            pTacSrv->IpAddress.u1Afi;
                        pNextFsTacClntExtServerAddress->i4_Length =
                            pTacSrv->IpAddress.u1AddrLen;
                        u1FirstNextFound = 1;
                        u1IPV4EntryFoundValue = 1;
                    }
                /* Below check is added to print the server address in sorted order */
                else if (MEMCMP (pTacSrv->IpAddress.au1Addr,
                                 pNextFsTacClntExtServerAddress->pu1_OctetList,
                                 pNextFsTacClntExtServerAddress->i4_Length) <
                         TAC_ZERO)

                {
                    MEMCPY (pNextFsTacClntExtServerAddress->pu1_OctetList,
                            pTacSrv->IpAddress.au1Addr,
                            pTacSrv->IpAddress.u1AddrLen);
                    *pi4NextFsTacClntExtServerAddressType =
                        pTacSrv->IpAddress.u1Afi;
                    pNextFsTacClntExtServerAddress->i4_Length =
                        pTacSrv->IpAddress.u1AddrLen;
                }
            }
        }
    }
    /* Check is added to get IPV6 Entries */
    if (u1IPV4EntryFoundValue == TAC_ZERO)
    {
        for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
        {
            pTacSrv = &(TAC_SERVER_TABLE[u1Index]);

            /* While updating the first IPV6 entry, don't do 
             * any comparsion with the entry present in 
             * pFsTacClntExtServerAddress if the address type is IPV4*/

            if ((i4FsTacClntExtServerAddressType == IPVX_ADDR_FMLY_IPV4) &&
                (pTacSrv->IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV6))
            {
                if (u1FirstNextFound == TAC_ZERO)
                {
                    MEMCPY (pNextFsTacClntExtServerAddress->pu1_OctetList,
                            pTacSrv->IpAddress.au1Addr,
                            pTacSrv->IpAddress.u1AddrLen);
                    *pi4NextFsTacClntExtServerAddressType =
                        pTacSrv->IpAddress.u1Afi;
                    pNextFsTacClntExtServerAddress->i4_Length =
                        pTacSrv->IpAddress.u1AddrLen;
                    u1FirstNextFound = 1;
                    u1IPV6EntryFoundValue = 1;
                }
                /* Below check is added to print the server address in sorted order */
                else if (MEMCMP (pTacSrv->IpAddress.au1Addr,
                                 pNextFsTacClntExtServerAddress->pu1_OctetList,
                                 pNextFsTacClntExtServerAddress->i4_Length) <
                         TAC_ZERO)
                {
                    MEMCPY (pNextFsTacClntExtServerAddress->pu1_OctetList,
                            pTacSrv->IpAddress.au1Addr,
                            pTacSrv->IpAddress.u1AddrLen);
                    *pi4NextFsTacClntExtServerAddressType =
                        pTacSrv->IpAddress.u1Afi;
                    pNextFsTacClntExtServerAddress->i4_Length =
                        pTacSrv->IpAddress.u1AddrLen;
                }

            }
            else
            {
                if ((i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)&&
                    (pTacSrv->IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV6))
                {
                    if ((pTacSrv->u4Status != DESTROY) &&
                        (MEMCMP (pTacSrv->IpAddress.au1Addr,
                                 pFsTacClntExtServerAddress->pu1_OctetList,
                                 pFsTacClntExtServerAddress->i4_Length)) >
                        TAC_ZERO)
                    {
                        if (u1FirstNextFound == TAC_ZERO)
                        {
                            MEMCPY (pNextFsTacClntExtServerAddress->
                                    pu1_OctetList, pTacSrv->IpAddress.au1Addr,
                                    pTacSrv->IpAddress.u1AddrLen);
                            *pi4NextFsTacClntExtServerAddressType =
                                pTacSrv->IpAddress.u1Afi;
                            pNextFsTacClntExtServerAddress->i4_Length =
                                pTacSrv->IpAddress.u1AddrLen;
                            u1FirstNextFound = 1;
                            u1IPV6EntryFoundValue = 1;
                        }
                        /* Below check is added to print the server address in sorted order */
                        else if (MEMCMP (pTacSrv->IpAddress.au1Addr,
                                         pNextFsTacClntExtServerAddress->
                                         pu1_OctetList,
                                         pNextFsTacClntExtServerAddress->
                                         i4_Length) < TAC_ZERO)
                        {
                            MEMCPY (pNextFsTacClntExtServerAddress->
                                    pu1_OctetList, pTacSrv->IpAddress.au1Addr,
                                    pTacSrv->IpAddress.u1AddrLen);
                            *pi4NextFsTacClntExtServerAddressType =
                                pTacSrv->IpAddress.u1Afi;
                            pNextFsTacClntExtServerAddress->i4_Length =
                                pTacSrv->IpAddress.u1AddrLen;
                        }
                    }
                }
            }
        }
    }

    if ((u1IPV4EntryFoundValue == TAC_ZERO) && 
        (u1IPV6EntryFoundValue == TAC_ZERO))
    {
        for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
        {

            pTacSrv = &(TAC_SERVER_TABLE[u1Index]);

            if ((i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY) &&
                (pTacSrv->IpAddress.u1Afi == IPVX_DNS_FAMILY))
            {

                if (u1FirstNextFound == TAC_ZERO)
                {
                    STRNCPY (pNextFsTacClntExtServerAddress->pu1_OctetList,
                             pTacSrv->au1HostName,
                             STRLEN (pTacSrv->au1HostName));
                    *pi4NextFsTacClntExtServerAddressType =
                        pTacSrv->IpAddress.u1Afi;
                    pNextFsTacClntExtServerAddress->pu1_OctetList
                        [STRLEN (pTacSrv->au1HostName)] = '\0';
                    pNextFsTacClntExtServerAddress->i4_Length =
                        STRLEN (pTacSrv->au1HostName);
                    u1FirstNextFound = 1;
                    break;
                }

            }
            else
            {
                if (pTacSrv->IpAddress.u1Afi == IPVX_DNS_FAMILY)
                {
                    if (u1FirstNextFound == 1)
                    {
                        break;
                    }
                    STRNCPY (au1Temp, pFsTacClntExtServerAddress->pu1_OctetList,
                             pFsTacClntExtServerAddress->i4_Length);
                    au1Temp[pFsTacClntExtServerAddress->i4_Length] = '\0';
                    if ((STRNCMP (pTacSrv->au1HostName,au1Temp,
                                  DNS_MAX_QUERY_LEN) == 0))
                    {
                        pTacSrv = &(TAC_SERVER_TABLE[u1Index + 1]);
                        if (pTacSrv->IpAddress.u1Afi == IPVX_DNS_FAMILY)
                        {
                            STRNCPY (pNextFsTacClntExtServerAddress->pu1_OctetList,
                                     pTacSrv->au1HostName,
                                     STRLEN (pTacSrv->au1HostName));
                            *pi4NextFsTacClntExtServerAddressType =
                                pTacSrv->IpAddress.u1Afi;
                            pNextFsTacClntExtServerAddress->pu1_OctetList
                                [STRLEN (pTacSrv->au1HostName)] = '\0';
                            pNextFsTacClntExtServerAddress->i4_Length =
                                STRLEN (pTacSrv->au1HostName);
                            u1FirstNextFound = 1;
                        }
                    }
                }
            }
        }
    }

    if ((i4FsTacClntExtServerAddressType == IPVX_DNS_FAMILY) && 
        (pTacSrv->IpAddress.u1Afi == IPVX_DNS_FAMILY))
    {
        if (STRNCMP (pNextFsTacClntExtServerAddress->pu1_OctetList,
                     pFsTacClntExtServerAddress->pu1_OctetList,
                     DNS_MAX_QUERY_LEN) == TAC_ZERO)
        {
            return (SNMP_FAILURE);

        }
    }
    else
    {
        if ((MEMCMP (pNextFsTacClntExtServerAddress->pu1_OctetList,
                     pFsTacClntExtServerAddress->pu1_OctetList,
                     pFsTacClntExtServerAddress->i4_Length) == TAC_ZERO) ||
            (u1FirstNextFound == TAC_ZERO))
        {
            return (SNMP_FAILURE);
        }
    }

    return (SNMP_SUCCESS);
}



/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacClntExtServerStatus
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                retValFsTacClntExtServerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtServerStatus (INT4 i4FsTacClntExtServerAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsTacClntExtServerAddress,
                                INT4 *pi4RetValFsTacClntExtServerStatus)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY )
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }
    else
    {
        pFsTacClntExtServerAddress->i4_Length =
            STRLEN(pFsTacClntExtServerAddress->pu1_OctetList);
    }
    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            *pi4RetValFsTacClntExtServerStatus = pTacSrv->u4Status;
            break;
        }

    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* IP Address is not matching with IP address of any server */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtServerSingleConnect
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                retValFsTacClntExtServerSingleConnect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtServerSingleConnect (INT4 i4FsTacClntExtServerAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsTacClntExtServerAddress,
                                       INT4
                                       *pi4RetValFsTacClntExtServerSingleConnect)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY )
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);

        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
            && (pTacSrv->u4Status != DESTROY))
        {
            if (pTacSrv->u1SCFlag == TAC_SINGLE_CONNECT_FLAG)
            {
                *pi4RetValFsTacClntExtServerSingleConnect =
                    TAC_SC_FLAG_SNMP_YES;
            }
            else if (pTacSrv->u1SCFlag == TAC_NO_SINGLE_CONNECT)
            {
                *pi4RetValFsTacClntExtServerSingleConnect = TAC_SC_FLAG_SNMP_NO;
            }
            else
            {
                *pi4RetValFsTacClntExtServerSingleConnect =
                    TAC_SC_FLAG_SNMP_OTHER;
            }
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* IP Address is not matching with IP address of any server */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtServerPort
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                retValFsTacClntExtServerPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtServerPort (INT4 i4FsTacClntExtServerAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsTacClntExtServerAddress,
                              INT4 *pi4RetValFsTacClntExtServerPort)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY )
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            *pi4RetValFsTacClntExtServerPort = (INT4) (pTacSrv->u2Port);
            break;
        }

    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* IP Address is not matching with IP address of any server */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtServerTimeout
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                retValFsTacClntExtServerTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtServerTimeout (INT4 i4FsTacClntExtServerAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacClntExtServerAddress,
                                 INT4 *pi4RetValFsTacClntExtServerTimeout)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    INT1                u1FoundValue = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY )
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {

            u1FoundValue = 1;
        }

        if (u1FoundValue == 1)
        {
            *pi4RetValFsTacClntExtServerTimeout = (INT4) (pTacSrv->u4Timeout);
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* IP Address is not matching with IP address of any server */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetFsTacClntExtServerKey
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                retValFsTacClntExtServerKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntExtServerKey (INT4 i4FsTacClntExtServerAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsTacClntExtServerAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsTacClntExtServerKey)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    UINT4               u4KeyLen = TAC_ZERO;
    INT1                u1FoundValue = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY )
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            u1FoundValue = 1;
        }
        if (u1FoundValue == 1)
        {
#ifdef RM_WANTED
            if ((MsrGetSaveStatus () == ISS_TRUE) ||
                    (RmGetStaticConfigStatus () == RM_STATIC_CONFIG_IN_PROGRESS))
#else
                if (MsrGetSaveStatus () == ISS_TRUE)
#endif
                {
                    u4KeyLen = STRLEN (pTacSrv->au1SecretKey);
                    MEMCPY (pRetValFsTacClntExtServerKey->pu1_OctetList,
                            pTacSrv->au1SecretKey, u4KeyLen);
                    pRetValFsTacClntExtServerKey->i4_Length = (INT4) u4KeyLen;
                }
                else
                {
                    pRetValFsTacClntExtServerKey->i4_Length = 0;
                }
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* IP Address is not matching with IP address of any server */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTacClntExtServerStatus
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                setValFsTacClntExtServerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtServerStatus (INT4 i4FsTacClntExtServerAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsTacClntExtServerAddress,
                                INT4 i4SetValFsTacClntExtServerStatus)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    UINT1               u1KeyLen = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);

        for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
        {
            pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
            if ((MEMCMP (pTacSrv->IpAddress.au1Addr,
                            pFsTacClntExtServerAddress->pu1_OctetList,
                            pFsTacClntExtServerAddress->i4_Length) == TAC_ZERO))
            {
                break;
            }
        }
    }
    else
    {

        for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
        {
            pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
            if ((MEMCMP (pTacSrv->au1HostName,
                            pFsTacClntExtServerAddress->pu1_OctetList,
                            pFsTacClntExtServerAddress->i4_Length) == TAC_ZERO))
            {
                break;
            }
        }

    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* New entry */
        for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
        {
            pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
            if (pTacSrv->u4Status == DESTROY)
            {
                /* Free entry found */
                break;
            }
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Server Table full */
        return (SNMP_FAILURE);
    }

    switch (i4SetValFsTacClntExtServerStatus)
    {
        case CREATE_AND_WAIT:
            /* Reset all fields of this server */
            MEMSET (pTacSrv, TAC_ZERO, sizeof (tTacServerInfo));

            /* Set default value for this server */
            pTacSrv->u4Timeout = TAC_SRV_DEFAULT_TIMEOUT;

            pTacSrv->i4SingleConnId = (-1);
            pTacSrv->u1SCFlag = TAC_NO_SINGLE_CONNECT;
            pTacSrv->u1SCVerifyCount = TAC_ZERO;
            u1KeyLen = (UINT1) STRLEN (pTacSrv->au1SecretKey);
            if (u1KeyLen == TAC_ZERO)
            {
                STRNCPY (pTacSrv->au1SecretKey, TAC_SRV_DEFAULT_KEY, STRLEN(TAC_SRV_DEFAULT_KEY));
		pTacSrv->au1SecretKey[STRLEN(TAC_SRV_DEFAULT_KEY)] = '\0';
            }
            if (i4FsTacClntExtServerAddressType == IPVX_ADDR_FMLY_IPV4)
            {
                pTacSrv->u2Port = TAC_TCP_IPV4_PORT;
            }
            else
            {
                pTacSrv->u2Port = TAC_TCP_IPV6_PORT;
            }
            if (i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
            {
                IPVX_ADDR_INIT (pTacSrv->IpAddress,
                        (UINT1) i4FsTacClntExtServerAddressType,
                        pFsTacClntExtServerAddress->pu1_OctetList);
            }
            else
            {
                pTacSrv->IpAddress.u1Afi = IPVX_DNS_FAMILY;
                STRNCPY (pTacSrv->au1HostName,
                        pFsTacClntExtServerAddress->pu1_OctetList,
                        pFsTacClntExtServerAddress->i4_Length);
                pTacSrv->au1HostName
                     [pFsTacClntExtServerAddress->i4_Length] = '\0';
            }
            pTacSrv->u4Status = NOT_IN_SERVICE;
            break;
        case DESTROY:
        case NOT_IN_SERVICE:
            if ((pTacSrv->u1SCFlag == TAC_SINGLE_CONNECT_FLAG) &&
                (pTacSrv->i4SingleConnId != (-1)))
            {
                /*-------------------------------------------------------*
                 * If single connect is enbled and if there is connection*
                 * available, close the connection                       *
                 *-------------------------------------------------------*/
                TAC_CLOSE (pTacSrv->i4SingleConnId);
            }
            if (i4SetValFsTacClntExtServerStatus == DESTROY)
            {
                MEMSET (pTacSrv, TAC_ZERO, sizeof (tTacServerInfo));
            }
            /*----------------------------------------------------------*
             * Do not put "break" instruction here. The status should be *
             * set to DESTROY/NOT_IN_SERVICE                             *
             *-----------------------------------------------------------*/
        case ACTIVE:
            pTacSrv->u4Status = i4SetValFsTacClntExtServerStatus;
            break;
    }

    return (SNMP_SUCCESS);
}


/****************************************************************************
 Function    :  nmhSetFsTacClntExtServerSingleConnect
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                setValFsTacClntExtServerSingleConnect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtServerSingleConnect (INT4 i4FsTacClntExtServerAddressType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsTacClntExtServerAddress,
                                       INT4
                                       i4SetValFsTacClntExtServerSingleConnect)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;

    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY )
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }
    else
    {
        pFsTacClntExtServerAddress->i4_Length =
            STRLEN(pFsTacClntExtServerAddress->pu1_OctetList);
    }
    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Entry is not found */
        return (SNMP_FAILURE);
    }

    if (i4SetValFsTacClntExtServerSingleConnect == TAC_SC_FLAG_SNMP_YES)
    {
        /* If already SC flag set, do not do anything */
        if (pTacSrv->u1SCFlag != TAC_SINGLE_CONNECT_FLAG)
        {
            pTacSrv->u1SCFlag = TAC_SINGLE_CONNECT_FLAG;
            pTacSrv->i4SingleConnId = (-1);
            pTacSrv->u1SCVerifyCount = TAC_ZERO;
        }
    }
    else if (i4SetValFsTacClntExtServerSingleConnect == TAC_SC_FLAG_SNMP_NO)
    {
        /* If already in multiple connection mode, do not do anything */
        if (pTacSrv->u1SCFlag != TAC_ZERO)
        {
            pTacSrv->u1SCFlag = TAC_ZERO;

            /* Close the single connection if already opened */
            if (pTacSrv->i4SingleConnId != (-1))
            {
                TAC_CLOSE (pTacSrv->i4SingleConnId);
                pTacSrv->i4SingleConnId = (-1);
                pTacSrv->u1SCVerifyCount = TAC_ZERO;
            }
        }
    }
    else
    {
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsTacClntExtServerPort
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                setValFsTacClntExtServerPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtServerPort (INT4 i4FsTacClntExtServerAddressType,
                              tSNMP_OCTET_STRING_TYPE *
                              pFsTacClntExtServerAddress,
                              INT4 i4SetValFsTacClntExtServerPort)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    UINT1               u1FoundValue = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }
    else
    {
        pFsTacClntExtServerAddress->i4_Length =
            STRLEN(pFsTacClntExtServerAddress->pu1_OctetList);
    }
    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);

        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            u1FoundValue = 1;

        }
        if (u1FoundValue == 1)
        {
            pTacSrv->u2Port = (UINT2) i4SetValFsTacClntExtServerPort;
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Entry is not found */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsTacClntExtServerTimeout
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                setValFsTacClntExtServerTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtServerTimeout (INT4 i4FsTacClntExtServerAddressType,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pFsTacClntExtServerAddress,
                                 INT4 i4SetValFsTacClntExtServerTimeout)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    INT1                u1FoundValue = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }
    else
    {
        pFsTacClntExtServerAddress->i4_Length =
            STRLEN(pFsTacClntExtServerAddress->pu1_OctetList);
    }
    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);

        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            u1FoundValue = 1;

        }
        if (u1FoundValue == 1)
        {
            pTacSrv->u4Timeout = i4SetValFsTacClntExtServerTimeout;
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Entry is not found */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhSetFsTacClntExtServerKey
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                setValFsTacClntExtServerKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntExtServerKey (INT4 i4FsTacClntExtServerAddressType,
                             tSNMP_OCTET_STRING_TYPE *
                             pFsTacClntExtServerAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsTacClntExtServerKey)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    INT1                u1FoundValue = TAC_ZERO;
    if ( i4FsTacClntExtServerAddressType !=IPVX_DNS_FAMILY )
    {
        pFsTacClntExtServerAddress->i4_Length =
            TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType);
    }
    else
    {
        pFsTacClntExtServerAddress->i4_Length =
                     STRLEN(pFsTacClntExtServerAddress->pu1_OctetList);
    }
    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            u1FoundValue = 1;

        }
        if (u1FoundValue == 1)
        {
            MEMCPY (pTacSrv->au1SecretKey,
                    pSetValFsTacClntExtServerKey->pu1_OctetList,
                    pSetValFsTacClntExtServerKey->i4_Length);
            /* NULL terminate the secret key */
            pTacSrv->au1SecretKey[pSetValFsTacClntExtServerKey->i4_Length] =
                '\0';
            if (pTacSrv->u4Status == NOT_READY)
            {
                pTacSrv->u4Status = NOT_IN_SERVICE;
            }
            break;
        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Entry is not found */
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtServerStatus
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                testValFsTacClntExtServerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtServerStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4FsTacClntExtServerAddressType,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pFsTacClntExtServerAddress,
                                   INT4 i4TestValFsTacClntExtServerStatus)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    UINT4               u4IfAddr = TAC_ZERO;
    UINT1               au1Tnp[IPVX_IPV6_ADDR_LEN];
    UINT1               au1IpAdd[IPVX_IPV6_ADDR_LEN];
    UINT1               au1HostName[DNS_MAX_QUERY_LEN];
    UINT4               u4TacSrvAdd;
#ifdef IP6_WANTED
    UINT4               u4Index = 0;
    tIp6Addr            TempAddr;
#endif

    /* Get first index (numerically first index) */
    MEMSET (au1Tnp, 0xFF, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1IpAdd, TAC_ZERO, IPVX_IPV6_ADDR_LEN);
    MEMSET (au1HostName, TAC_ZERO, DNS_MAX_QUERY_LEN);
        /* Check for self Ip address */

        if (i4FsTacClntExtServerAddressType == IPVX_ADDR_FMLY_IPV4)
        {
            MEMCPY (&u4TacSrvAdd, pFsTacClntExtServerAddress->pu1_OctetList,
                    pFsTacClntExtServerAddress->i4_Length);
            if (NetIpv4IfIsOurAddress (OSIX_NTOHL (u4TacSrvAdd)) == 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CLI_SET_ERR (CLI_TAC_INV_SRV);
                return (SNMP_FAILURE);
            }
        }
#ifdef IP6_WANTED
	if (i4FsTacClntExtServerAddressType == IPVX_ADDR_FMLY_IPV6)
	{
	    MEMSET (&TempAddr, 0, sizeof (tIp6Addr));
	    MEMCPY (&TempAddr, pFsTacClntExtServerAddress->pu1_OctetList,
		    pFsTacClntExtServerAddress->i4_Length);

	    /* Check for self Ip address */
	    if (NetIpv6IsOurAddress (&TempAddr, &u4Index) == NETIPV6_SUCCESS)
	    {
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
		CLI_SET_ERR (CLI_TAC_INV_SRV);
		return (SNMP_FAILURE);
	    }
	}
#endif

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
	pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
	if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
		&&(pTacSrv->u4Status != DESTROY))
	{
	    break;

	}
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
	/* New entry */
	for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
	{
	    pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
	    if (pTacSrv->u4Status == DESTROY)
	    {
		/* Free entry found */
		break;
	    }
	}
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
	/* Server Table full */
	*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	CLI_SET_ERR (CLI_TAC_MAX_SRV);
	return (SNMP_FAILURE);
    }

    switch (i4TestValFsTacClntExtServerStatus)
    {
	case ACTIVE:
	    {
		if (((MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
				    au1Tnp,
				    pFsTacClntExtServerAddress->i4_Length) ||
				(MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
					 au1HostName,
					 pFsTacClntExtServerAddress->i4_Length))) == TAC_ZERO) 
			|| (STRLEN (pTacSrv->au1SecretKey) == TAC_ZERO)
			|| (pTacSrv->u2Port == TAC_ZERO)
			|| (pTacSrv->u4Timeout == TAC_ZERO))
		{

		    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		    return (SNMP_FAILURE);
		}
		break;
	    }
	case NOT_IN_SERVICE:
	    {
		if (pTacSrv->u4Status == ACTIVE)
		{
		    /*---------------------------------------------------*
		     * Server is active. If this is being used by client,*
		     * return error                                      *
		     *---------------------------------------------------*/
		    if (((TAC_ACTIVE_SERVER - 1) < TAC_MAX_SERVERS) &&
			    (TAC_ACTIVE_SERVER != TAC_ZERO))
		    {
			if (MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
				    &TAC_SERVER_TABLE[TAC_ACTIVE_SERVER - 1].
				    IpAddress.au1Addr,
				    pTacSrv->IpAddress.u1AddrLen) == TAC_ZERO)
			{
			    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			    CLI_SET_ERR (CLI_TAC_ACT_SRV);
			    return (SNMP_FAILURE);
			}
		    }
		}
		break;
	    }
	case NOT_READY:
	    /* Read only value of Row status */
	    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	    CLI_SET_ERR (CLI_TAC_INV_STATE);
	    return (SNMP_FAILURE);
	case CREATE_AND_WAIT:
	    {
		/*-----------------------------------------------------------*
		 * Check if entry is available. If entry is already available*
		 * creation is invalid                                       *
		 *-----------------------------------------------------------*/

		if (i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
		{
#ifdef IP6_WANTED
		    if ((i4FsTacClntExtServerAddressType != IPVX_ADDR_FMLY_IPV4)
			    && (i4FsTacClntExtServerAddressType != IPVX_ADDR_FMLY_IPV6))
		    {
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			return SNMP_FAILURE;
		    }
#endif
		}


		if (pTacSrv->u4Status != DESTROY)
		{
		    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		    CLI_SET_ERR (CLI_TAC_INV_STATE);
		    return (SNMP_FAILURE);
		}


		if ((MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
				au1Tnp, pFsTacClntExtServerAddress->i4_Length) == 0) ||
			(MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
				 au1IpAdd, pFsTacClntExtServerAddress->i4_Length) == 0) ||
			(MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
				 au1HostName, pFsTacClntExtServerAddress->i4_Length) == 0))

		{
		    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
		    CLI_SET_ERR (CLI_TAC_INV_SRV);
		    return (SNMP_FAILURE);
		}

		if (pFsTacClntExtServerAddress->i4_Length == IPVX_IPV4_ADDR_LEN)
		{
		    MEMCPY (&u4IfAddr, pTacSrv->IpAddress.au1Addr,
			    IPVX_IPV4_ADDR_LEN);

		    if (TAC_IS_ADDR_CLASS_D (u4IfAddr) ||
			    TAC_IS_ADDR_CLASS_E (u4IfAddr) ||
			    TAC_IS_CLASS_C_BROADCAST_ADDR (u4IfAddr))
		    {
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CLI_SET_ERR (CLI_TAC_INV_IP);
			return (SNMP_FAILURE);
		    }
		}

/*since domain is one of the index,max length of 255 causes problem while doing MSR
 * to address that restricting the host name size to 63*/
		if (pFsTacClntExtServerAddress->i4_Length >= TAC_MAX_DNS_DOMAIN_NAME)
        {
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		    CLI_SET_ERR (CLI_TAC_MAX_DOMAIN_LEN);
            return SNMP_FAILURE;
        }
		break;
	    }
	case DESTROY:
	    {
		/* Check if entry is available */
		if (pTacSrv->u4Status == DESTROY)
		{
		    /* Entry is not available */
		    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		    CLI_SET_ERR (CLI_TAC_INV_STATE);
		    return (SNMP_FAILURE);
		}
		else
        {
            if (((TAC_ACTIVE_SERVER - 1) < TAC_MAX_SERVERS) &&
                (TAC_ACTIVE_SERVER != TAC_ZERO))
            {
                /* Entry is available. Check if this is used by client */
                if (pTacSrv->u4Status == ACTIVE) 
                {
                    /*checking the active server address type,if its of type 
                     * IPVX, IpAddress should be compared, else au1HostName 
                     * should be compared.*/
                    if (TAC_SERVER_TABLE[TAC_ACTIVE_SERVER - 1].IpAddress.u1Afi 
                        != IPVX_DNS_FAMILY)
                    {
                        if ((MEMCMP (pFsTacClntExtServerAddress->pu1_OctetList,
                                     &TAC_SERVER_TABLE[TAC_ACTIVE_SERVER - 1].
                                     IpAddress.au1Addr,
                                     TAC_SERVER_TABLE[TAC_ACTIVE_SERVER - 1].
                                     IpAddress.u1AddrLen) 
                             == TAC_ZERO))
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            CLI_SET_ERR (CLI_TAC_ACT_SRV);
                            return (SNMP_FAILURE);

                        }
                    }
                    else
                    {
                        if ((STRNCMP (pFsTacClntExtServerAddress->pu1_OctetList,
                                      &TAC_SERVER_TABLE[TAC_ACTIVE_SERVER - 1].
                                      au1HostName,
                                      DNS_MAX_QUERY_LEN) == TAC_ZERO))
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            CLI_SET_ERR (CLI_TAC_ACT_SRV);
                            return (SNMP_FAILURE);
                        }
                    }
                }
            }
        }
        break;
        }
	default:
	    /* Wrong value of Row status */
	    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
	    return (SNMP_FAILURE);
    }
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtServerSingleConnect
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                testValFsTacClntExtServerSingleConnect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtServerSingleConnect (UINT4 *pu4ErrorCode,
                                          INT4 i4FsTacClntExtServerAddressType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsTacClntExtServerAddress,
                                          INT4
                                          i4TestValFsTacClntExtServerSingleConnect)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;

    if(i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {
        if (TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType)
                != pFsTacClntExtServerAddress->i4_Length)
        {
            return (SNMP_FAILURE);
        }
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY) && (pTacSrv->u4Status != ACTIVE))
        {
            break;

        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Entry is not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_TAC_INV_SRV);
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsTacClntExtServerSingleConnect != TAC_SC_FLAG_SNMP_YES) &&
        (i4TestValFsTacClntExtServerSingleConnect != TAC_SC_FLAG_SNMP_NO))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtServerPort
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                testValFsTacClntExtServerPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtServerPort (UINT4 *pu4ErrorCode,
        INT4 i4FsTacClntExtServerAddressType,
        tSNMP_OCTET_STRING_TYPE *
        pFsTacClntExtServerAddress,
        INT4 i4TestValFsTacClntExtServerPort)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;
    if (i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {

        if (TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType)
                != pFsTacClntExtServerAddress->i4_Length)
        {
            return (SNMP_FAILURE);
        }
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            break;

        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Entry is not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_TAC_INV_SRV);
        return (SNMP_FAILURE);
    }

    if (i4TestValFsTacClntExtServerPort == TAC_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_TAC_INV_PORT);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtServerTimeout
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                testValFsTacClntExtServerTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtServerTimeout (UINT4 *pu4ErrorCode,
                                    INT4 i4FsTacClntExtServerAddressType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsTacClntExtServerAddress,
                                    INT4 i4TestValFsTacClntExtServerTimeout)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;

    if (i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {

        if (TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType)
                != pFsTacClntExtServerAddress->i4_Length)
        {
            return (SNMP_FAILURE);
        }
    }
    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);

        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            break;

        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Entry is not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_TAC_INV_SRV);
        return (SNMP_FAILURE);
    }

    if ((i4TestValFsTacClntExtServerTimeout == TAC_ZERO) ||
        (i4TestValFsTacClntExtServerTimeout > TAC_SRV_MAX_TIMEOUT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CLI_SET_ERR (CLI_TAC_INV_TIME);
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntExtServerKey
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress

                The Object 
                testValFsTacClntExtServerKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntExtServerKey (UINT4 *pu4ErrorCode,
                                INT4 i4FsTacClntExtServerAddressType,
                                tSNMP_OCTET_STRING_TYPE *
                                pFsTacClntExtServerAddress,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsTacClntExtServerKey)
{
    tTacServerInfo     *pTacSrv = NULL;
    UINT1               u1Index = TAC_ZERO;

    if (i4FsTacClntExtServerAddressType != IPVX_DNS_FAMILY)
    {

        if (TAC_IPVX_LENGTH_FROM_TYPE (i4FsTacClntExtServerAddressType)
                != pFsTacClntExtServerAddress->i4_Length)
        {
            return (SNMP_FAILURE);
        }
    }

    for (u1Index = TAC_ZERO; u1Index < TAC_MAX_SERVERS; u1Index++)
    {
        pTacSrv = &(TAC_SERVER_TABLE[u1Index]);
        if ((TAC_COMP(pTacSrv,pFsTacClntExtServerAddress) == TAC_SUCCESS)
                && (pTacSrv->u4Status != DESTROY))
        {
            break;

        }
    }

    if (u1Index >= TAC_MAX_SERVERS)
    {
        /* Entry is not found */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (CLI_TAC_INV_SRV);
        return (SNMP_FAILURE);
    }

    if ((pTestValFsTacClntExtServerKey->i4_Length == TAC_ZERO) ||
        (pTestValFsTacClntExtServerKey->i4_Length >= TAC_SECRET_KEY_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return (SNMP_FAILURE);
    }

    return (SNMP_SUCCESS);
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTacClntExtServerTable
 Input       :  The Indices
                FsTacClntExtServerAddressType
                FsTacClntExtServerAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntExtServerTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
