/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpcli.c,v 1.12 2015/11/09 09:43:21 siva Exp $
 *
 * Description: This file will have the CLI configuration routines
 * for TACACS client
 *******************************************************************/

#ifndef __TPCLI_C__
#define __TPCLI_C__

#include "tpinc.h"
#include "tpcli.h"
#include "fstacalw.h"
#include "fstacslw.h"
#include "snmputil.h"
#include "fstacscli.h"

VOID
cli_process_tacacs_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    tTacCliInputParams  uTacCliParams;
    UINT4              *args[10];
    UINT1              *pu1Inst = NULL;
    UINT4               u4KeyLen;
    INT1                argno = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    UINT4               u4ErrCode;
    UINT4               u4IpAddr = 0;
    tIp6Addr            Ip6ServerAddr;

    UNUSED_PARAM (pu1Inst);
    va_start (ap, u4Command);

    /* second arguement is always InterfaceName/Index */
    pu1Inst = va_arg (ap, UINT1 *);    /* This is done to advance the pointer */

    CLI_MEMSET (&uTacCliParams, 0, sizeof (tTacCliInputParams));

    /* Walk through the rest of the arguements and store in args array */

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == 10)
            break;
    }
    va_end (ap);

    CliRegisterLock (CliHandle, TacProtocolLock, TacProtocolUnlock);
    TAC_PROT_LOCK ();

    switch (u4Command)
    {
        case TAC_CMD_SHOW_TACACS:
            /* To show all the servers */
            u4IpAddr = TAC_VALUE_NOT_PRESENT;
            u4IpAddr = OSIX_NTOHL (u4IpAddr);
            MEMCPY (uTacCliParams.IpAddress.au1Addr, &u4IpAddr,
                    IPVX_IPV4_ADDR_LEN);
            uTacCliParams.TacSrv.IpAddress.u1AddrLen = IPVX_IPV4_ADDR_LEN;
            uTacCliParams.TacSrv.IpAddress.u1Afi = IPVX_ADDR_FMLY_IPV4;
            i4RetStatus = tacCmdShowTacacs (CliHandle, &uTacCliParams);
            break;
        case TAC_CMD_SET_SERVER:
            /* args[0] contains the ip addr         
             * args[1] contains Single connect flag
             * args[2] contains TCP port no.
             * args[3] contains time out value
             * args[4] contains secret key
             * args[5] contains address type
             */
            if (STRLEN (args[0]) <= DNS_MAX_QUERY_LEN)
            {
                uTacCliParams.TacSrv.IpAddress.u1Afi =
                    (UINT1) (CLI_PTR_TO_U4 (args[5]));
                if (uTacCliParams.TacSrv.IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    uTacCliParams.TacSrv.IpAddress.u1AddrLen = IPVX_IPV4_ADDR_LEN;
                    u4IpAddr = *args[0];
                    u4IpAddr = OSIX_NTOHL (u4IpAddr);
                    MEMCPY (uTacCliParams.TacSrv.IpAddress.au1Addr, &u4IpAddr,
                            IPVX_IPV4_ADDR_LEN);
                }
                if (uTacCliParams.TacSrv.IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    MEMSET (&Ip6ServerAddr, 0, sizeof (tIp6Addr));
                    INET_ATON6 (args[0], &Ip6ServerAddr);
                    IPVX_ADDR_INIT_FROMV6 (uTacCliParams.TacSrv.IpAddress,
                                           (VOID *) &Ip6ServerAddr);
                }

                if (uTacCliParams.TacSrv.IpAddress.u1Afi == IPVX_DNS_FAMILY)
                {
                    MEMSET (uTacCliParams.TacSrv.au1HostName, 0, STRLEN (args[0]));
                    STRNCPY (uTacCliParams.TacSrv.au1HostName, args[0], STRLEN (args[0]));
                    uTacCliParams.TacSrv.au1HostName
                        [STRLEN (args[0])] = '\0';
                    *uTacCliParams.TacSrv.au1HostName = 
                        UtilStrToLower ((UINT1 *)uTacCliParams.TacSrv.au1HostName);
                }

                if (args[1] == NULL)
                {
                    uTacCliParams.TacSrv.u4SingleConnect = TAC_SC_FLAG_SNMP_NO;
                }
                else
                {
                    uTacCliParams.TacSrv.u4SingleConnect = TAC_SC_FLAG_SNMP_YES;
                }
                if (args[2] == NULL)
                {
                    uTacCliParams.TacSrv.u4Port = TAC_VALUE_NOT_PRESENT;
                }
                else
                {
                    MEMCPY (&uTacCliParams.TacSrv.u4Port, args[2], sizeof (UINT4));
                }
                if (args[3] == NULL)
                {
                    uTacCliParams.TacSrv.u4Timeout = TAC_VALUE_NOT_PRESENT;
                }
                else
                {
                    uTacCliParams.TacSrv.u4Timeout = *args[3];
                }
                if (args[4] == NULL)
                {
                    uTacCliParams.TacSrv.au1Key[0] = '\0';
                }
                else
                {
                    u4KeyLen = STRLEN (args[4]);
                    if (u4KeyLen >= TAC_CLI_SECRET_KEY_LEN)
                    {
                        CLI_SET_ERR (CLI_TAC_KEY_TOO_LONG);
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    MEMCPY (uTacCliParams.TacSrv.au1Key, args[4], u4KeyLen);
                }
                i4RetStatus = tacCmdSetServer (CliHandle, &uTacCliParams);
            }
            break;
        case TAC_CMD_NO_SERVER:
            if (STRLEN (args[0]) <= DNS_MAX_QUERY_LEN)
            {
                uTacCliParams.TacSrv.IpAddress.u1Afi =
                    (UINT1) (CLI_PTR_TO_U4 (args[1]));
                if (uTacCliParams.TacSrv.IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
                {
                    uTacCliParams.IpAddress.u1AddrLen = IPVX_IPV4_ADDR_LEN;
                    u4IpAddr = *args[0];
                    u4IpAddr = OSIX_NTOHL (u4IpAddr);
                    MEMCPY (uTacCliParams.IpAddress.au1Addr, &u4IpAddr,
                            IPVX_IPV4_ADDR_LEN);
                }
                if (uTacCliParams.TacSrv.IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV6)
                {
                    MEMSET (&Ip6ServerAddr, 0, sizeof (tIp6Addr));
                    INET_ATON6 (args[0], &Ip6ServerAddr);
                    IPVX_ADDR_INIT_FROMV6 (uTacCliParams.TacSrv.IpAddress,
                                           (VOID *) &Ip6ServerAddr);
                }

                if (uTacCliParams.TacSrv.IpAddress.u1Afi == IPVX_DNS_FAMILY)
                {

                    STRNCPY (&uTacCliParams.TacSrv.au1HostName, args[0], STRLEN(args[0])+1);
                    uTacCliParams.TacSrv.au1HostName
                        [STRLEN (args[0])] = '\0';
                    *uTacCliParams.TacSrv.au1HostName = 
                        UtilStrToLower ((UINT1 *)uTacCliParams.TacSrv.au1HostName);
                }


                i4RetStatus = tacCmdNoServer (CliHandle, &uTacCliParams);
            }
            break;
        case TAC_CMD_SET_ACTIVE_SERVER:

            uTacCliParams.IpAddress.u1Afi = (UINT1) (CLI_PTR_TO_U4 (args[1]));
            if (uTacCliParams.IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV4)
            {
                uTacCliParams.IpAddress.u1AddrLen = IPVX_IPV4_ADDR_LEN;
                u4IpAddr = *args[0];
                u4IpAddr = OSIX_NTOHL (u4IpAddr);
                MEMCPY (uTacCliParams.IpAddress.au1Addr, &u4IpAddr,
                        IPVX_IPV4_ADDR_LEN);
            }
            if (uTacCliParams.IpAddress.u1Afi == IPVX_ADDR_FMLY_IPV6)
            {
                MEMSET (&Ip6ServerAddr, 0, sizeof (tIp6Addr));
                INET_ATON6 (args[0], &Ip6ServerAddr);
                IPVX_ADDR_INIT_FROMV6 (uTacCliParams.IpAddress,
                                       (VOID *) &Ip6ServerAddr);
            }
            
            if (uTacCliParams.TacSrv.IpAddress.u1Afi == IPVX_DNS_FAMILY)
            {

                    STRNCPY (&uTacCliParams.TacSrv.au1HostName, args[0], STRLEN(args[0])+1);
                    uTacCliParams.TacSrv.au1HostName
                             [STRLEN (args[0])] = '\0';
                    *uTacCliParams.TacSrv.au1HostName = 
                        UtilStrToLower ((UINT1 *)uTacCliParams.TacSrv.au1HostName);
            }

            i4RetStatus = tacCmdSetActiveServer (CliHandle, &uTacCliParams);
            break;
        case TAC_CMD_SET_NO_ACTIVE_SERVER:
            i4RetStatus = tacCmdSetNoActiveServer (CliHandle);
            break;
        case TAC_CMD_SET_TRACE_LEVEL:
            uTacCliParams.u4TraceLevel = CLI_PTR_TO_U4 (args[0]);
            i4RetStatus = tacCmdSetTraceLevel (CliHandle, &uTacCliParams);
            break;
        case TAC_CMD_SET_RETRANSMIT:
            uTacCliParams.u4Retries = *(UINT4 *) args[0];
            i4RetStatus = tacCmdSetRetransmit (CliHandle, &uTacCliParams);
            break;
        case TAC_CMD_SET_NO_RETRANSMIT:
            i4RetStatus = tacCmdSetNoRetransmit (CliHandle);
            break;
        default:
            CliPrintf (CliHandle, "\r%% Unknown Command\n");
            CliUnRegisterLock (CliHandle);
            TAC_PROT_UNLOCK ();
            return;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_TAC_MAX_ERR))
        {
            CliPrintf (CliHandle, "%s", TacCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);
    TAC_PROT_UNLOCK ();

}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdSetServer                                    */
/*     DESCRIPTION      : This function configures a server                  */
/*     INPUT            : pTacCliInput - A pointer to a Structure            */
/*                                       containing the Input params         */
/*                        CliHandle - a structure used to find the context   */
/*     OUTPUT           : None.                                              */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE.                           */
/*****************************************************************************/
INT4
tacCmdSetServer (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput)
{
    tSNMP_OCTET_STRING_TYPE *pKey = NULL;
    tSNMP_OCTET_STRING_TYPE *pIpAddress = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4SnmpValue = 0;
    INT4                i4Result = 0;
    INT4                i4KeyLen = 0;
    INT4                i4Created = 0;
    INT4                i4TacClntServerStatus = 0;
    UINT1               au1HostName[DNS_MAX_QUERY_LEN];
    MEMSET (au1HostName, 0, DNS_MAX_QUERY_LEN);
    STRNCPY (au1HostName, pTacCliInput->TacSrv.au1HostName, 
	    STRLEN(pTacCliInput->TacSrv.au1HostName)+1);
    au1HostName[STRLEN(pTacCliInput->TacSrv.au1HostName)] = '\0';
    if ( pTacCliInput->TacSrv.IpAddress.u1Afi != IPVX_DNS_FAMILY )
    { 
	pIpAddress = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
	if (pIpAddress == NULL)
	{
	    return CLI_FAILURE;
	}
	MEMSET (pIpAddress->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
	MEMCPY (pIpAddress->pu1_OctetList, pTacCliInput->TacSrv.IpAddress.au1Addr,
		pTacCliInput->TacSrv.IpAddress.u1AddrLen);
	pIpAddress->i4_Length = pTacCliInput->TacSrv.IpAddress.u1AddrLen;
	i4Result =
	    nmhGetFsTacClntExtServerStatus (pTacCliInput->TacSrv.IpAddress.u1Afi,
		    pIpAddress, &i4SnmpValue);
    }
    else 
    {
	pIpAddress = allocmem_octetstring (DNS_MAX_QUERY_LEN);
	if (pIpAddress == NULL)
	{
	    return CLI_FAILURE;
	}
	MEMSET (pIpAddress->pu1_OctetList, 0, STRLEN(au1HostName)+1);
	STRNCPY (pIpAddress->pu1_OctetList, au1HostName, STRLEN(au1HostName)+1);
	pIpAddress->i4_Length = STRLEN(au1HostName);
        pIpAddress->pu1_OctetList[pIpAddress->i4_Length] = '\0';
	i4Result =
	    nmhGetFsTacClntExtServerStatus (pTacCliInput->TacSrv.IpAddress.u1Afi,
		    pIpAddress, &i4SnmpValue);

    }

    if (i4Result == SNMP_SUCCESS)
    {
        /* Entry update. So disable the row */

        if (nmhTestv2FsTacClntExtServerStatus
            (&u4ErrorCode, pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            free_octetstring (pIpAddress);
            return CLI_FAILURE;
        }
        i4Result =
            nmhSetFsTacClntExtServerStatus (pTacCliInput->TacSrv.IpAddress.
                                            u1Afi, pIpAddress, NOT_IN_SERVICE);
        if (i4Result == SNMP_FAILURE)
        {
            free_octetstring (pIpAddress);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;

        }
        i4Created = 1;
    }
    else
    {
        if (nmhTestv2FsTacClntExtServerStatus
            (&u4ErrorCode, pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            free_octetstring (pIpAddress);
            return CLI_FAILURE;
        }
        if (nmhSetFsTacClntExtServerStatus
            (pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
             CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            free_octetstring (pIpAddress);
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }
        i4Created = 0;
    }

    /************** First test all values ***************/
    /* Test single connect flag */
    i4Result =
	nmhTestv2FsTacClntExtServerSingleConnect (&u4ErrorCode,
		pTacCliInput->TacSrv.
		IpAddress.u1Afi, pIpAddress,
		pTacCliInput->TacSrv.
		u4SingleConnect);
    if (i4Result == SNMP_FAILURE)
    {
	if (i4Created == 1)
	{
	    i4TacClntServerStatus = i4SnmpValue;
	}
	else
	{
	    i4TacClntServerStatus = DESTROY;
	}
	if (nmhSetFsTacClntExtServerStatus
		(pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
		 i4TacClntServerStatus) == SNMP_FAILURE)
	{
	    free_octetstring (pIpAddress);
	    CLI_FATAL_ERROR (CliHandle);
	    return CLI_FAILURE;
	}
	free_octetstring (pIpAddress);
	return CLI_FAILURE;
    }

    /* Test Port no. */
    if ((pTacCliInput->TacSrv.u4Port) != TAC_VALUE_NOT_PRESENT)
    {
	i4Result =
	    nmhTestv2FsTacClntExtServerPort (&u4ErrorCode,
		    pTacCliInput->TacSrv.IpAddress.
		    u1Afi, pIpAddress,
		    pTacCliInput->TacSrv.u4Port);

	if (i4Result == SNMP_FAILURE)
	{
	    if (i4Created == 1)
	    {
		i4TacClntServerStatus = i4SnmpValue;
	    }
	    else
	    {
		i4TacClntServerStatus = DESTROY;
	    }
	    if (nmhSetFsTacClntExtServerStatus
		    (pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
		     i4TacClntServerStatus) == SNMP_FAILURE)
	    {
		free_octetstring (pIpAddress);
		CLI_FATAL_ERROR (CliHandle);
		return CLI_FAILURE;
	    }
	    free_octetstring (pIpAddress);
	    return CLI_FAILURE;
	}
    }

    /* Test Timeout value */
    if ((pTacCliInput->TacSrv.u4Timeout) != TAC_VALUE_NOT_PRESENT)
    {
	i4Result =
	    nmhTestv2FsTacClntExtServerTimeout (&u4ErrorCode,
		    pTacCliInput->TacSrv.IpAddress.
		    u1Afi, pIpAddress,
		    pTacCliInput->TacSrv.u4Timeout);

    }
    if (i4Result == SNMP_FAILURE)
    {
	if (i4Created == 1)
	{
	    i4TacClntServerStatus = i4SnmpValue;
	}
	else
	{
	    i4TacClntServerStatus = DESTROY;
	}
	if (nmhSetFsTacClntExtServerStatus
		(pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
		 i4TacClntServerStatus) == SNMP_FAILURE)
	{
	    free_octetstring (pIpAddress);
	    CLI_FATAL_ERROR (CliHandle);
	    return CLI_FAILURE;
	}
	free_octetstring (pIpAddress);
	return CLI_FAILURE;
    }

    /* Test the secret key */
    if ((i4KeyLen = STRLEN (pTacCliInput->TacSrv.au1Key)) != 0)
    {
	pKey = allocmem_octetstring (TAC_CLI_SECRET_KEY_LEN + 1);
	if (pKey == NULL)
	{
	    if (i4Created == 1)
	    {
		i4TacClntServerStatus = i4SnmpValue;
	    }
	    else
	    {
		i4TacClntServerStatus = DESTROY;
	    }
	    if (nmhSetFsTacClntExtServerStatus
		    (pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
		     i4TacClntServerStatus) == SNMP_FAILURE)
	    {
		free_octetstring (pKey);
		free_octetstring (pIpAddress);
		CLI_FATAL_ERROR (CliHandle);
		return CLI_FAILURE;
	    }

	}
	else
	{
	    CLI_MEMSET (pKey->pu1_OctetList, 0, (TAC_CLI_SECRET_KEY_LEN + 1));
	    STRNCPY (pKey->pu1_OctetList, pTacCliInput->TacSrv.au1Key, STRLEN(pTacCliInput->TacSrv.au1Key));
	    pKey->pu1_OctetList[STRLEN(pTacCliInput->TacSrv.au1Key)] ='\0';
	    pKey->i4_Length = i4KeyLen;

	    i4Result =
		nmhTestv2FsTacClntExtServerKey (&u4ErrorCode,
			pTacCliInput->TacSrv.IpAddress.
			u1Afi, pIpAddress, pKey);

	    if (i4Result == SNMP_FAILURE)
	    {
		if (i4Created == 1)
		{
		    i4TacClntServerStatus = i4SnmpValue;
		}
		else
		{
		    i4TacClntServerStatus = DESTROY;
		}
		if (nmhSetFsTacClntExtServerStatus
			(pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
			 i4TacClntServerStatus) == SNMP_FAILURE)
		{
		    CLI_FATAL_ERROR (CliHandle);
		    free_octetstring (pIpAddress);
		    free_octetstring (pKey);
		    return CLI_FAILURE;
		}
		free_octetstring (pIpAddress);
		free_octetstring (pKey);
		return CLI_FAILURE;
	    }
	    /* Set the secret key */
	    if (nmhSetFsTacClntExtServerKey
		    (pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
		     pKey) == SNMP_FAILURE)
	    {
		CLI_FATAL_ERROR (CliHandle);
		free_octetstring (pIpAddress);
		free_octetstring (pKey);
		return CLI_FAILURE;
	    }
	}
    }

    /************** Set all values ***************/
    /* Set single connect flag */
    if (nmhSetFsTacClntExtServerSingleConnect
	    (pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
	     pTacCliInput->TacSrv.u4SingleConnect) == SNMP_FAILURE)
    {
	CLI_FATAL_ERROR (CliHandle);
	return CLI_FAILURE;
    }

    /* Set Port no. */
    if ((pTacCliInput->TacSrv.u4Port) != TAC_VALUE_NOT_PRESENT)
    {
	if (nmhSetFsTacClntExtServerPort
		(pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
		 pTacCliInput->TacSrv.u4Port) == SNMP_FAILURE)
	{
	    CLI_FATAL_ERROR (CliHandle);
	    free_octetstring (pIpAddress);
	    return CLI_FAILURE;
	}

    }

    /* Set Timeout value */
    if ((pTacCliInput->TacSrv.u4Timeout) != TAC_VALUE_NOT_PRESENT)
    {
	if (nmhSetFsTacClntExtServerTimeout
		(pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
		 pTacCliInput->TacSrv.u4Timeout) == SNMP_FAILURE)
	{
	    CLI_FATAL_ERROR (CliHandle);
	    free_octetstring (pIpAddress);
	    return CLI_FAILURE;
	}

    }

    /* Enable the server */
    if (nmhSetFsTacClntExtServerStatus
	    (pTacCliInput->TacSrv.IpAddress.u1Afi, pIpAddress,
	     ACTIVE) == SNMP_FAILURE)
    {
	CLI_FATAL_ERROR (CliHandle);
	free_octetstring (pKey);
	free_octetstring (pIpAddress);
	return CLI_FAILURE;
    }
    free_octetstring (pKey);
    free_octetstring (pIpAddress);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdNoServer                                     */
/*     DESCRIPTION      : This function deletes a server                     */
/*     INPUT            : pTacCliInput                                        */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdNoServer (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput)
{

    tSNMP_OCTET_STRING_TYPE *pIpAddress = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4Result = 0;
    INT4                i4AddressType = 0;

    if(pTacCliInput->TacSrv.IpAddress.u1Afi != IPVX_DNS_FAMILY)
    {
	pIpAddress = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
	if (pIpAddress == NULL)
	{
	    return CLI_FAILURE;
	}
	MEMSET (pIpAddress->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);

	MEMCPY (pIpAddress->pu1_OctetList, pTacCliInput->TacSrv.IpAddress.au1Addr,
		pTacCliInput->TacSrv.IpAddress.u1AddrLen);
	pIpAddress->i4_Length = pTacCliInput->TacSrv.IpAddress.u1AddrLen;
	i4AddressType = pTacCliInput->TacSrv.IpAddress.u1Afi;

	i4Result =
	    nmhValidateIndexInstanceFsTacClntExtServerTable (i4AddressType,
		    pIpAddress);
    }

    else
    {       
	pIpAddress = allocmem_octetstring (DNS_MAX_QUERY_LEN);
	if (pIpAddress == NULL)
	{
	    return CLI_FAILURE;
	}

	MEMSET (pIpAddress->pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
	STRNCPY (pIpAddress->pu1_OctetList, pTacCliInput->TacSrv.au1HostName,
                 STRLEN(pTacCliInput->TacSrv.au1HostName)+1);
        pIpAddress->pu1_OctetList
                   [STRLEN(pTacCliInput->TacSrv.au1HostName)] = '\0';
	pIpAddress->i4_Length = STRLEN(pIpAddress->pu1_OctetList);
	i4AddressType = pTacCliInput->TacSrv.IpAddress.u1Afi;
	i4Result =
	    nmhValidateIndexInstanceFsTacClntExtServerTable (i4AddressType,
		    pIpAddress);

    } 
    if (i4Result == SNMP_FAILURE)
    {
	free_octetstring (pIpAddress);
	CliPrintf (CliHandle, "\r%% Invalid TACACS server\r\n");
	return CLI_FAILURE;
    }
    i4Result =
	nmhTestv2FsTacClntExtServerStatus (&u4ErrorCode, i4AddressType,
		pIpAddress, DESTROY);
    if (i4Result == SNMP_SUCCESS)
    {
	/* Entry is avaialble. Delete the server now */
	if (nmhSetFsTacClntExtServerStatus (i4AddressType, pIpAddress, DESTROY)
		== SNMP_FAILURE)
	{
	    free_octetstring (pIpAddress);
	    CLI_FATAL_ERROR (CliHandle);
	    return CLI_FAILURE;
	}
    }
    else
    {
	free_octetstring (pIpAddress);
	return CLI_FAILURE;
    }

    free_octetstring (pIpAddress);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdSetActiveServer                              */
/*     DESCRIPTION      : This function chooses the server to be used        */
/*     INPUT            : pTacCliInput                                        */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdSetActiveServer (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput)
{

    tSNMP_OCTET_STRING_TYPE *pIpAddress = NULL;
    UINT4               u4ErrorCode = 0;
    INT4                i4Result = 0;
    INT4                i4AddressType = 0;
    if ( pTacCliInput->TacSrv.IpAddress.u1Afi != IPVX_DNS_FAMILY)
    {
	pIpAddress = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
	if (pIpAddress == NULL)
	{
	    return CLI_FAILURE;
	}
	MEMSET (pIpAddress->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);

	MEMCPY (pIpAddress->pu1_OctetList, pTacCliInput->TacSrv.IpAddress.au1Addr,
		pTacCliInput->TacSrv.IpAddress.u1AddrLen);
	pIpAddress->i4_Length = pTacCliInput->TacSrv.IpAddress.u1AddrLen;
	i4AddressType = pTacCliInput->TacSrv.IpAddress.u1Afi;

	i4Result =
	    nmhValidateIndexInstanceFsTacClntExtServerTable (i4AddressType,
		    pIpAddress);
    }

    else
    {
	pIpAddress = allocmem_octetstring (DNS_MAX_QUERY_LEN);
	if (pIpAddress == NULL)
	{
	    return CLI_FAILURE;
	}

	MEMSET (pIpAddress->pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
	STRNCPY (pIpAddress->pu1_OctetList, pTacCliInput->TacSrv.au1HostName,
		STRLEN(pTacCliInput->TacSrv.au1HostName)+1);
        pIpAddress->pu1_OctetList
                   [STRLEN(pTacCliInput->TacSrv.au1HostName)] = '\0';
	pIpAddress->i4_Length = STRLEN(pTacCliInput->TacSrv.au1HostName);
	i4AddressType = pTacCliInput->TacSrv.IpAddress.u1Afi;
	i4Result =
	    nmhValidateIndexInstanceFsTacClntExtServerTable (i4AddressType,
		    pIpAddress);

    }    
    if (i4Result == SNMP_FAILURE)
    {
	free_octetstring (pIpAddress);
	CliPrintf (CliHandle, "\r%% Invalid TACACS server\r\n");
	return CLI_FAILURE;
    }
    if (nmhTestv2FsTacClntExtActiveServerAddressType
	    (&u4ErrorCode, pTacCliInput->IpAddress.u1Afi) == SNMP_SUCCESS)
    {
	if (nmhSetFsTacClntExtActiveServerAddressType
		(pTacCliInput->IpAddress.u1Afi) == SNMP_FAILURE)
	{
	    free_octetstring (pIpAddress);
	    CliPrintf (CliHandle, "\r%% Invalid Address Type\r\n");
	    return CLI_FAILURE;
	}
    }
    else
    {
	free_octetstring (pIpAddress);
	CliPrintf (CliHandle, "\r%% Invalid Address Type\r\n");
	return CLI_FAILURE;
    }

    i4Result = nmhTestv2FsTacClntExtActiveServer (&u4ErrorCode, pIpAddress);
    if (i4Result == SNMP_SUCCESS)
    {
	if (nmhSetFsTacClntExtActiveServer (pIpAddress) == SNMP_FAILURE)
	{
	    free_octetstring (pIpAddress);
	    CLI_FATAL_ERROR (CliHandle);
	    return CLI_FAILURE;
	}

    }
    else
    {
	free_octetstring (pIpAddress);
	return CLI_FAILURE;
    }
    free_octetstring (pIpAddress);
    return CLI_SUCCESS;

}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdSetNoActiveServer                            */
/*     DESCRIPTION      : This disables the configured active server         */
/*     INPUT            : pTacCliInput                                       */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdSetNoActiveServer (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pIpAddress = NULL;

    pIpAddress = allocmem_octetstring (IPVX_MAX_INET_ADDR_LEN);
    if (pIpAddress == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pIpAddress->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);

    if (nmhSetFsTacClntExtActiveServer (pIpAddress) == SNMP_FAILURE)
    {
        free_octetstring (pIpAddress);
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    free_octetstring (pIpAddress);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdSetTraceLevel                                */
/*     DESCRIPTION      : This function sets the debug trace level           */
/*     INPUT            : pTacCliInput                                        */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdSetTraceLevel (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput)
{
    UNUSED_PARAM (CliHandle);

    nmhSetFsTacClntTraceLevel (pTacCliInput->u4TraceLevel);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdSetRetransmit                                */
/*     DESCRIPTION      : This function sets the retransmit value            */
/*     INPUT            : pTacCliInput                                       */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdSetRetransmit (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Result = 0;

    i4Result = nmhTestv2FsTacClntExtRetransmit (&u4ErrorCode,
                                                pTacCliInput->u4Retries);
    if (i4Result == SNMP_SUCCESS)
    {
        if (nmhSetFsTacClntExtRetransmit (pTacCliInput->u4Retries) ==
            SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdSetNoRetransmit                              */
/*     DESCRIPTION      : This function sets the default retranmit value     */
/*     INPUT            : pTacCliInput                                       */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdSetNoRetransmit (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Result = 0;

    i4Result = nmhTestv2FsTacClntExtRetransmit (&u4ErrorCode, TAC_DEF_RETRIES);
    if (i4Result == SNMP_SUCCESS)
    {
        if (nmhSetFsTacClntExtRetransmit (TAC_DEF_RETRIES) == SNMP_FAILURE)
        {
            CLI_FATAL_ERROR (CliHandle);
            return CLI_FAILURE;
        }

    }
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdShowServers                                  */
/*     DESCRIPTION      : This function displays the server configurations   */
/*     INPUT            : pTacCliInput                                        */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdShowTacacs (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput)
{
    if (tacCmdShowServers (CliHandle, pTacCliInput) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    tacCmdShowStatistics (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdShowServers                                  */
/*     DESCRIPTION      : This function displays the server configurations   */
/*     INPUT            : pTacCliInput                                       */
/*     OUTPUT           : None.                                              */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdShowServers (tCliHandle CliHandle, tTacCliInputParams * pTacCliInput)
{
    tSNMP_OCTET_STRING_TYPE *pKey = NULL;
    tSNMP_OCTET_STRING_TYPE *pIpAddress = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrentServerIpAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextServerIpAddr = NULL;
    CHR1               *pu1String = NULL;
    UINT1               au1IfAddr[IPVX_MAX_INET_ADDR_LEN];
    UINT4               u4ServerNo = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4Address = 0;
    INT4                u4CurrentAddrType = 0;
    INT4                u4NextAddrType = 0;
    INT4                i4SnmpValue = 0;
    INT4                i4Result = 0;
    INT4                i4AddrType = 0;
    UINT1               au1HostName[DNS_MAX_QUERY_LEN];
    UINT1               au1TcaHostServer[DNS_MAX_QUERY_LEN];
    
    MEMSET (au1HostName, 0, DNS_MAX_QUERY_LEN);
    pIpAddress = allocmem_octetstring (DNS_MAX_QUERY_LEN);
    if (pIpAddress == NULL)
    {
        return CLI_FAILURE;
    }
    pCurrentServerIpAddr = allocmem_octetstring (DNS_MAX_QUERY_LEN);
    if (pCurrentServerIpAddr == NULL)
    {
        free_octetstring (pIpAddress);
        return CLI_FAILURE;
    }
    pNextServerIpAddr = allocmem_octetstring (DNS_MAX_QUERY_LEN);
    if (pNextServerIpAddr == NULL)
    {

        free_octetstring (pCurrentServerIpAddr);
        free_octetstring (pIpAddress);
        return CLI_FAILURE;
    }

    MEMSET (pIpAddress->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (pCurrentServerIpAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (pNextServerIpAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);

    /* In SNMP IP address is used as index to the server table */
    u4IpAddr = TAC_VALUE_NOT_PRESENT;
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    if (MEMCMP
        (pTacCliInput->TacSrv.IpAddress.au1Addr, &u4IpAddr,
	 IPVX_IPV4_ADDR_LEN) != 0)
    {
	MEMCPY (pNextServerIpAddr->pu1_OctetList,
		pTacCliInput->TacSrv.IpAddress.au1Addr,
		pTacCliInput->TacSrv.IpAddress.u1AddrLen);
	if (pTacCliInput->TacSrv.IpAddress.u1AddrLen == IPVX_IPV4_ADDR_LEN)
	{
	    u4NextAddrType = IPVX_ADDR_FMLY_IPV4;
	}
	else if (pTacCliInput->TacSrv.IpAddress.u1AddrLen == IPVX_IPV6_ADDR_LEN)
	{
	    u4NextAddrType = IPVX_ADDR_FMLY_IPV6;
	}
    
	else
	{
	    u4NextAddrType = IPVX_DNS_FAMILY;
	    MEMSET (pNextServerIpAddr->pu1_OctetList, 0,
                    STRLEN(pTacCliInput->TacSrv.au1HostName));
	    STRNCPY (pNextServerIpAddr->pu1_OctetList,
		    pTacCliInput->TacSrv.au1HostName,
		    STRLEN(pTacCliInput->TacSrv.au1HostName)+1);
            pNextServerIpAddr->pu1_OctetList
                   [STRLEN(pTacCliInput->TacSrv.au1HostName)] = '\0';
	}
        /* Check if this entry exists */
        i4Result =
            nmhValidateIndexInstanceFsTacClntExtServerTable (u4NextAddrType,
                                                             pNextServerIpAddr);
        if (i4Result == SNMP_FAILURE)
        {
            free_octetstring (pIpAddress);
            free_octetstring (pCurrentServerIpAddr);
            free_octetstring (pNextServerIpAddr);
            CliPrintf (CliHandle, "\r%% Entry does not exist\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        i4Result =
            nmhGetFirstIndexFsTacClntExtServerTable (&u4NextAddrType,
                                                     pNextServerIpAddr);

        if (i4Result == SNMP_FAILURE)
        {
            free_octetstring (pIpAddress);
            free_octetstring (pCurrentServerIpAddr);
            free_octetstring (pNextServerIpAddr);
            return CLI_FAILURE;
        }
    }

    pKey = (tSNMP_OCTET_STRING_TYPE *)
        allocmem_octetstring (TAC_CLI_SECRET_KEY_LEN + 1);
    if (pKey == NULL)
    {
        free_octetstring (pIpAddress);
        free_octetstring (pCurrentServerIpAddr);
        free_octetstring (pNextServerIpAddr);
        CliPrintf (CliHandle, "\r%% Not able to allocate "
                   "Memory for Key \r\n");
        return CLI_FAILURE;
    }
    CLI_MEMSET (pKey->pu1_OctetList, 0, (TAC_CLI_SECRET_KEY_LEN + 1));

    u4ServerNo = 1;
    do
    {
	if ( u4NextAddrType == IPVX_DNS_FAMILY)
	{
	    MEMSET (pCurrentServerIpAddr->pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
	    STRNCPY (pCurrentServerIpAddr->pu1_OctetList,
		    pNextServerIpAddr->pu1_OctetList, 
		    STRLEN(pNextServerIpAddr->pu1_OctetList)+1);
	    pCurrentServerIpAddr->pu1_OctetList
		[STRLEN(pNextServerIpAddr->pu1_OctetList)] = '\0';
	    pCurrentServerIpAddr->i4_Length = pNextServerIpAddr->i4_Length;
	    u4CurrentAddrType = u4NextAddrType;
	}
	else
	{
	    MEMSET (pCurrentServerIpAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
	    MEMCPY (pCurrentServerIpAddr->pu1_OctetList,
		    pNextServerIpAddr->pu1_OctetList, pNextServerIpAddr->i4_Length);
	    pCurrentServerIpAddr->i4_Length = pNextServerIpAddr->i4_Length;
	    u4CurrentAddrType = u4NextAddrType;
	}
	/* Print the header */
	CliPrintf (CliHandle, "Server : %d\r\n", u4ServerNo);

	/* Print the IP address */

	if (u4CurrentAddrType == IPVX_ADDR_FMLY_IPV4)
	{
	    PTR_FETCH4 (u4Address, pCurrentServerIpAddr->pu1_OctetList);
	    /*    u4Address = OSIX_NTOHL(u4Address); */
	    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
	    CliPrintf (CliHandle, "\rServer address           : %-17s\r\n",
		    pu1String);
	    CliPrintf (CliHandle, "\rAddress Type             : IPV4\r\n");
	}

	if (u4CurrentAddrType == IPVX_ADDR_FMLY_IPV6)
	{
	    MEMCPY (au1IfAddr, pCurrentServerIpAddr->pu1_OctetList,
		    IPVX_IPV6_ADDR_LEN);
	    CliPrintf (CliHandle, "\rServer address           : %-17s\r\n",
		    Ip6PrintNtop ((tIp6Addr *) (VOID *) au1IfAddr));
	    CliPrintf (CliHandle, "\rAddress Type             : IPV6\r\n");
	}

	if (u4CurrentAddrType == IPVX_DNS_FAMILY)
	{
	    STRNCPY (au1HostName, pCurrentServerIpAddr->pu1_OctetList,
		    STRLEN(pCurrentServerIpAddr->pu1_OctetList)+1);
	    au1HostName[STRLEN(pCurrentServerIpAddr->pu1_OctetList)] = '\0';
	    CliPrintf (CliHandle, "\rServer address           : %-17s\r\n",
		    au1HostName);
	    CliPrintf (CliHandle, "\rAddress Type             : DNS\r\n");
	}
	/* Get Single connect option */
	nmhGetFsTacClntExtServerSingleConnect (u4CurrentAddrType,
		pCurrentServerIpAddr,
		&i4SnmpValue);
	if (i4SnmpValue == TAC_SC_FLAG_SNMP_YES)
	{
	    CliPrintf (CliHandle, "       Single Connection : yes\r\n");
	}
	else
	{
	    CliPrintf (CliHandle, "       Single Connection : no\r\n");
	}

	/* Get the Port no. */
	nmhGetFsTacClntExtServerPort (u4CurrentAddrType, pCurrentServerIpAddr,
		&i4SnmpValue);
	CliPrintf (CliHandle, "       TCP port          : %d\r\n", i4SnmpValue);

	/* Get the Time out */
	nmhGetFsTacClntExtServerTimeout (u4CurrentAddrType,
		pCurrentServerIpAddr, &i4SnmpValue);
	CliPrintf (CliHandle, "       Timeout           : %d\r\n", i4SnmpValue);

	/* Get the secret key */
	nmhGetFsTacClntExtServerKey (u4CurrentAddrType, pCurrentServerIpAddr,
		pKey);
	if ((pKey->i4_Length) >= TAC_CLI_SECRET_KEY_LEN)
	{
	    pKey->pu1_OctetList[TAC_CLI_SECRET_KEY_LEN] = '\0';
	}
	else
	{
	    pKey->pu1_OctetList[pKey->i4_Length] = '\0';
	}
	CliPrintf (CliHandle, "       Secret Key        : %s\r\n",
		pKey->pu1_OctetList);

	/* Display the server information */

	CLI_MEMSET (pKey->pu1_OctetList, 0, (TAC_CLI_SECRET_KEY_LEN + 1));
	u4IpAddr = TAC_VALUE_NOT_PRESENT;
	u4IpAddr = OSIX_NTOHL (u4IpAddr);

	if (MEMCMP
		(&pTacCliInput->TacSrv.IpAddress.au1Addr, &u4IpAddr,
		 IPVX_IPV4_ADDR_LEN) != 0)
	{
	    /* single entry display. So break from loop */
	    /*    break; */
	}

	u4ServerNo++;
	MEMSET (pNextServerIpAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    }
    while (nmhGetNextIndexFsTacClntExtServerTable
		    (u4CurrentAddrType, &u4NextAddrType, pCurrentServerIpAddr,
		     pNextServerIpAddr) == SNMP_SUCCESS);
    /* Print the active server */
    nmhGetFsTacClntExtActiveServerAddressType (&i4AddrType);
    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
	    MEMSET (pCurrentServerIpAddr->pu1_OctetList, 0, IPVX_IPV4_ADDR_LEN);
    }
    else if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
	    MEMSET (pCurrentServerIpAddr->pu1_OctetList, 0, IPVX_MAX_INET_ADDR_LEN);
    }
    else
    {
	    MEMSET (pCurrentServerIpAddr->pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
    }
    nmhGetFsTacClntExtActiveServer (pCurrentServerIpAddr);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
	    PTR_FETCH4 (u4Address, pCurrentServerIpAddr->pu1_OctetList);
	    CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);

	    CliPrintf (CliHandle, "\r Active Server address: %-17s\r\n", pu1String);
    }

    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
	    MEMCPY (au1IfAddr, pCurrentServerIpAddr->pu1_OctetList,
			    IPVX_IPV6_ADDR_LEN);
	    CliPrintf (CliHandle, "\r Active Server address: %-17s\r\n",
			    Ip6PrintNtop ((tIp6Addr *) (VOID *) au1IfAddr));
    }
    if ( i4AddrType == IPVX_DNS_FAMILY )
    {
	    STRNCPY (au1TcaHostServer, pCurrentServerIpAddr->pu1_OctetList,
			    STRLEN(pCurrentServerIpAddr->pu1_OctetList)+1);
            au1TcaHostServer[STRLEN(pCurrentServerIpAddr->pu1_OctetList)] = '\0';
	    CliPrintf (CliHandle, "\r Active Server address: %-17s\r\n",au1TcaHostServer);
    }
    nmhGetFsTacClntExtRetransmit(&i4SnmpValue);	
	CliPrintf (CliHandle, "\r Retransmit Time         : %d\r\n", i4SnmpValue);

    free_octetstring (pKey);
    free_octetstring (pIpAddress);
    free_octetstring (pCurrentServerIpAddr);
    free_octetstring (pNextServerIpAddr);
    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : tacCmdShowStatistics                               */
/*     DESCRIPTION      : This function displays the server statistics       */
/*     INPUT            : pTacCliInput                                        */
/*     OUTPUT           : ppu1OutMsg - Output pointer which stores the       */
/*                                     error if set fails.                   */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
tacCmdShowStatistics (tCliHandle CliHandle)
{
    UINT4               u4SnmpValue = 0;

    nmhGetFsTacClntAuthenStartRequests (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Starts sent    : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenContinueRequests (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Continues sent : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenEnableRequests (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Enables sent   : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenAbortRequests (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Aborts sent    : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenPassReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Pass rvcd.     : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenFailReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Fails rcvd.    : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenGetUserReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Get User rcvd. : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenGetPassReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Get Pass rcvd. : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenGetDataReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Get Data rcvd. : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenErrorReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Errors rcvd.   : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenFollowReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Follows rcvd.  : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenRestartReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Restart rcvd.  : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthenSessionTimouts (&u4SnmpValue);
    CliPrintf (CliHandle, "Authen. Sess. timeouts : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthorRequests (&u4SnmpValue);
    CliPrintf (CliHandle, "Author. Requests sent  : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthorPassAddReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Author. Pass Add rcvd. : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthorPassReplReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Author. Pass Repl rcvd : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthorFailReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Author. Fails rcvd.    : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthorErrorReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Author. Errors rcvd.   : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthorFollowReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Author Follows rcvd.   : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAuthorSessionTimeouts (&u4SnmpValue);
    CliPrintf (CliHandle, "Author. Sess. timeouts : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAcctStartRequests (&u4SnmpValue);
    CliPrintf (CliHandle, "Acct. start reqs. sent : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAcctWdRequests (&u4SnmpValue);
    CliPrintf (CliHandle, "Acct. WD reqs. sent    : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAcctStopRequests (&u4SnmpValue);
    CliPrintf (CliHandle, "Acct. Stop reqs. sent  : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAcctSuccessReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Acct. Success rcvd.    : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAcctErrorReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Acct. Errors rcvd.     : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAcctFollowReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Acct. Follows rcvd.    : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntAcctSessionTimeouts (&u4SnmpValue);
    CliPrintf (CliHandle, "Acct. Sess. timeouts   : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntMalformedPktsReceived (&u4SnmpValue);
    CliPrintf (CliHandle, "Malformed Pkts. rcvd.  : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntSocketFailures (&u4SnmpValue);
    CliPrintf (CliHandle, "Socket failures        : %d\r\n", u4SnmpValue);

    nmhGetFsTacClntConnectionFailures (&u4SnmpValue);
    CliPrintf (CliHandle, "Connection failures    : %ld\r\n", u4SnmpValue);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*     FUNCTION NAME    : TacacsShowRunningConfig                            */
/*     DESCRIPTION      : This function displays the current configurations  */
/*     INPUT            : CliHandle - Handle to the cli context              */
/*     OUTPUT           : None.                                              */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*****************************************************************************/
INT4
TacacsShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE *pKey = NULL;
    tSNMP_OCTET_STRING_TYPE *pIpAddress = NULL;
    tSNMP_OCTET_STRING_TYPE *pCurrentServerIpAddr = NULL;
    tSNMP_OCTET_STRING_TYPE *pNextServerIpAddr = NULL;
    CHR1               *pu1String = 0;
    UINT4               u4PagingStatus = CLI_SUCCESS;
    UINT1               au1IfAddr[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4SnmpValue = 0;
    INT4                i4Result = 0;
    INT4                i4Retransmit = 0;
    INT4                u4CurrentAddrType = 0;
    INT4                u4NextAddrType = 0;
    INT4                i4AddrType = 0;
    UINT4               u4Address = 0;
    UINT1               au1HostName[DNS_MAX_QUERY_LEN];
    UINT1               au1HostName1[DNS_MAX_QUERY_LEN];
    
    pIpAddress = allocmem_octetstring (DNS_MAX_QUERY_LEN);
    if (pIpAddress == NULL)
    {
        return CLI_FAILURE;
    }
    pCurrentServerIpAddr = allocmem_octetstring (DNS_MAX_QUERY_LEN);
    if (pCurrentServerIpAddr == NULL)
    {
        free_octetstring (pIpAddress);
        return CLI_FAILURE;
    }
    pNextServerIpAddr = allocmem_octetstring (DNS_MAX_QUERY_LEN);
    if (pNextServerIpAddr == NULL)
    {

        free_octetstring (pCurrentServerIpAddr);
        free_octetstring (pIpAddress);
        return CLI_FAILURE;
    }

    MEMSET (pIpAddress->pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
    MEMSET (pCurrentServerIpAddr->pu1_OctetList, 0, DNS_MAX_QUERY_LEN);
    MEMSET (pNextServerIpAddr->pu1_OctetList, 0, DNS_MAX_QUERY_LEN);

    if (nmhGetFirstIndexFsTacClntExtServerTable
        (&u4NextAddrType, pNextServerIpAddr) == SNMP_SUCCESS)
    {
        pKey = (tSNMP_OCTET_STRING_TYPE *)
            allocmem_octetstring (TAC_CLI_SECRET_KEY_LEN + 1);
        if (pKey == NULL)
        {
            CliPrintf (CliHandle, "\r%% Not able to allocate "
                       "Memory for Key \r\n");
            free_octetstring (pIpAddress);
            free_octetstring (pCurrentServerIpAddr);
            free_octetstring (pNextServerIpAddr);
            return CLI_FAILURE;
        }
        CLI_MEMSET (pKey->pu1_OctetList, 0, (TAC_CLI_SECRET_KEY_LEN + 1));

        do
        {
            MEMSET (pCurrentServerIpAddr->pu1_OctetList, 0,
                    DNS_MAX_QUERY_LEN);
            MEMCPY (pCurrentServerIpAddr->pu1_OctetList,
                    pNextServerIpAddr->pu1_OctetList,
                    pNextServerIpAddr->i4_Length);
            pCurrentServerIpAddr->i4_Length = pNextServerIpAddr->i4_Length;
            u4CurrentAddrType = u4NextAddrType;
            if (u4CurrentAddrType == IPVX_ADDR_FMLY_IPV4)
            {
                PTR_FETCH4 (u4Address, pCurrentServerIpAddr->pu1_OctetList);
                CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);
                CliPrintf (CliHandle, "\rtacacs-server host %s", pu1String);
            }
            if (u4CurrentAddrType == IPVX_ADDR_FMLY_IPV6)
            {
                MEMCPY (au1IfAddr, pCurrentServerIpAddr->pu1_OctetList,
                        IPVX_IPV6_ADDR_LEN);
                CliPrintf (CliHandle,
                           "\rtacacs-server host %s",
                           Ip6PrintNtop ((tIp6Addr *) (VOID *) au1IfAddr));
            }
	    if (u4CurrentAddrType == IPVX_DNS_FAMILY)
	    {
		STRNCPY (au1HostName1, pCurrentServerIpAddr->pu1_OctetList,
                               pCurrentServerIpAddr->i4_Length);
                au1HostName1[pCurrentServerIpAddr->i4_Length] = '\0';
		CliPrintf (CliHandle,
			"\rtacacs-server host %s",
			au1HostName1);
	    }


            /* Get Single connect option */
            nmhGetFsTacClntExtServerSingleConnect (u4CurrentAddrType,
                                                   pCurrentServerIpAddr,
                                                   &i4SnmpValue);
            if (i4SnmpValue == TAC_SC_FLAG_SNMP_YES)
            {
                CliPrintf (CliHandle, " %s", "single-connection");
            }
            /* Get the Port no. */
            nmhGetFsTacClntExtServerPort (u4CurrentAddrType,
                                          pCurrentServerIpAddr, &i4SnmpValue);
            if ((i4SnmpValue != TAC_TCP_IPV4_PORT)
                && (i4SnmpValue != TAC_TCP_IPV6_PORT))
            {
                CliPrintf (CliHandle, " port %d", i4SnmpValue);
            }
            /* Get the Time out */
            nmhGetFsTacClntExtServerTimeout (u4CurrentAddrType,
                                             pCurrentServerIpAddr,
                                             &i4SnmpValue);
            if (i4SnmpValue != TAC_SRV_DEFAULT_TIMEOUT)
            {
                CliPrintf (CliHandle, " timeout %d", i4SnmpValue);
            }
            /* Get the secret key */
            nmhGetFsTacClntExtServerKey (u4CurrentAddrType,
                                         pCurrentServerIpAddr, pKey);
            if ((STRCMP (pKey->pu1_OctetList, TAC_SRV_DEFAULT_KEY) != 0))
            {
                CliPrintf (CliHandle, " key %s", pKey->pu1_OctetList);
            }
            u4PagingStatus = CliPrintf (CliHandle, "\r\n");
            /* Display the server information */
            CLI_MEMSET (pKey->pu1_OctetList, 0, (TAC_CLI_SECRET_KEY_LEN + 1));
            MEMSET (pNextServerIpAddr->pu1_OctetList, 0,
                    IPVX_MAX_INET_ADDR_LEN);
            if (u4PagingStatus == CLI_FAILURE)
            {
                /* User pressed 'q' at more prompt,
                 * no more print required, exit */
                break;
            }
        }
        while (nmhGetNextIndexFsTacClntExtServerTable
               (u4CurrentAddrType, &u4NextAddrType, pCurrentServerIpAddr,
                pNextServerIpAddr) == SNMP_SUCCESS);
    }
    /* Print the active server */
    nmhGetFsTacClntExtActiveServerAddressType (&i4AddrType);
    nmhGetFsTacClntExtActiveServer (pCurrentServerIpAddr);

    if (i4AddrType == IPVX_ADDR_FMLY_IPV4)
    {
        PTR_FETCH4 (u4Address, pCurrentServerIpAddr->pu1_OctetList);
        CLI_CONVERT_IPADDR_TO_STR (pu1String, u4Address);

        CliPrintf (CliHandle, "\rtacacs use-server address %s\r\n", pu1String);
    }

    if (i4AddrType == IPVX_ADDR_FMLY_IPV6)
    {
        MEMCPY (au1IfAddr, pCurrentServerIpAddr->pu1_OctetList,
                IPVX_IPV6_ADDR_LEN);
        CliPrintf (CliHandle, "\rtacacs use-server address %s\r\n",
                   Ip6PrintNtop ((tIp6Addr *) (VOID *) au1IfAddr));
    }

    if (i4AddrType == IPVX_DNS_FAMILY)
    {
        STRNCPY (au1HostName, pCurrentServerIpAddr->pu1_OctetList,
                 STRLEN(pCurrentServerIpAddr->pu1_OctetList)+1);
        au1HostName[STRLEN(pCurrentServerIpAddr->pu1_OctetList)] = '\0';

        CliPrintf (CliHandle, "\rtacacs use-server address %s\r\n", au1HostName);
    }


    /* Print Retransmit */
    i4Result = nmhGetFsTacClntExtRetransmit (&i4Retransmit);
    if (i4Result != SNMP_FAILURE)
    {
        if (i4Retransmit != TAC_DEF_RETRIES)
        {
            CliPrintf (CliHandle, "\rtacacs-server retransmit %d\r\n",
                       i4Retransmit);
        }
    }

    free_octetstring (pKey);
    free_octetstring (pIpAddress);
    free_octetstring (pCurrentServerIpAddr);
    free_octetstring (pNextServerIpAddr);

    return CLI_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssTacacShowDebugging                              */
/*                                                                           */
/*     DESCRIPTION      : This function prints the TACAC debug level         */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : None                                               */
/*                                                                           */
/*****************************************************************************/

VOID
IssTacacShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4TraceLevel = 0;

    nmhGetFsTacClntTraceLevel (&u4TraceLevel);

    if (u4TraceLevel == 0)
    {
        return;
    }
    else
    {
        CliPrintf (CliHandle, "\rTACACS :");
        if ((u4TraceLevel & TAC_TRACE_INFO) != 0)
        {
            CliPrintf (CliHandle, "\r\n  TACACS Information debugging is on");
        }
        if ((u4TraceLevel & TAC_TRACE_ERROR) != 0)
        {
            CliPrintf (CliHandle, "\r\n  TACACS Error debugging is on");
        }
        if ((u4TraceLevel & TAC_TRACE_PKT_TX) != 0)
        {
            CliPrintf (CliHandle, "\r\n  TACACS Tx-Dump debugging is on");
        }
        if ((u4TraceLevel & TAC_TRACE_PKT_RX) != 0)
        {
            CliPrintf (CliHandle, "\r\n  TACACS Rx-Dump debugging is on");
        }
        CliPrintf (CliHandle, "\r\n");
    }

    return;
}

#endif
/********************************** End of file *******************************/
