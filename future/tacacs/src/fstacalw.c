/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fstacalw.c,v 1.8 2015/05/13 10:59:22 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fstacalw.h"
# include  "tpinc.h"
#include   "tpcli.h"
#include "fstacslw.h"

/*#include   "fstacscli.h"*/
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacClntActiveServer
 Input       :  The Indices

                The Object 
                retValFsTacClntActiveServer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntActiveServer (UINT4 *pu4RetValFsTacClntActiveServer)
{

    tSNMP_OCTET_STRING_TYPE RetValFsTacClntExtActiveServer;
    UINT1               au1TmpArray[IPVX_MAX_INET_ADDR_LEN];

    MEMSET (&RetValFsTacClntExtActiveServer, TAC_ZERO,
            sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_MAX_INET_ADDR_LEN);
    RetValFsTacClntExtActiveServer.pu1_OctetList = au1TmpArray;
    RetValFsTacClntExtActiveServer.i4_Length = IPVX_MAX_INET_ADDR_LEN;
    MEMSET (RetValFsTacClntExtActiveServer.pu1_OctetList, TAC_ZERO,
            IPVX_MAX_INET_ADDR_LEN);

    if (nmhGetFsTacClntExtActiveServer (&RetValFsTacClntExtActiveServer)
        == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    if (RetValFsTacClntExtActiveServer.i4_Length == IPVX_MAX_INET_ADDR_LEN)
    {
	     /* IPv6 address cannot be stored in ipv4 address variable.
	       * so return zero as IP */
	     *pu4RetValFsTacClntActiveServer = 0;
	      return (SNMP_SUCCESS);
    }
    PTR_FETCH4 (*pu4RetValFsTacClntActiveServer,
                RetValFsTacClntExtActiveServer.pu1_OctetList);
    return (SNMP_SUCCESS);

}

/****************************************************************************
 Function    :  nmhGetFsTacClntTraceLevel
 Input       :  The Indices

                The Object 
                retValFsTacClntTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntTraceLevel (UINT4 *pu4RetValFsTacClntTraceLevel)
{
    return (nmhGetFsTacClntExtTraceLevel (pu4RetValFsTacClntTraceLevel));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntRetransmit
 Input       :  The Indices

                The Object
                retValFsTacClntRetransmit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntRetransmit (INT4 *pi4RetValFsTacClntRetransmit)
{
    return (nmhGetFsTacClntExtRetransmit (pi4RetValFsTacClntRetransmit));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTacClntActiveServer
 Input       :  The Indices

                The Object 
                setValFsTacClntActiveServer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntActiveServer (UINT4 u4SetValFsTacClntActiveServer)
{
    tSNMP_OCTET_STRING_TYPE ClntExtActiveServer;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtActiveServer, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtActiveServer.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtActiveServer.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtActiveServer.pu1_OctetList,
                 u4SetValFsTacClntActiveServer);
    ClntExtActiveServer.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhSetFsTacClntExtActiveServer (&ClntExtActiveServer));
}

/****************************************************************************
 Function    :  nmhSetFsTacClntTraceLevel
 Input       :  The Indices

                The Object 
                setValFsTacClntTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntTraceLevel (UINT4 u4SetValFsTacClntTraceLevel)
{
    return (nmhSetFsTacClntExtTraceLevel (u4SetValFsTacClntTraceLevel));
}

/****************************************************************************
 Function    :  nmhSetFsTacClntRetransmit
 Input       :  The Indices

                The Object
                setValFsTacClntRetransmit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntRetransmit (INT4 i4SetValFsTacClntRetransmit)
{
    return (nmhSetFsTacClntExtRetransmit (i4SetValFsTacClntRetransmit));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTacClntActiveServer
 Input       :  The Indices

                The Object 
                testValFsTacClntActiveServer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntActiveServer (UINT4 *pu4ErrorCode,
                                UINT4 u4TestValFsTacClntActiveServer)
{
    tSNMP_OCTET_STRING_TYPE ClntExtActiveServer;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtActiveServer, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtActiveServer.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtActiveServer.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    PTR_ASSIGN4 (ClntExtActiveServer.pu1_OctetList,
                 u4TestValFsTacClntActiveServer);
    ClntExtActiveServer.i4_Length = IPVX_IPV4_ADDR_LEN;

    return (nmhTestv2FsTacClntExtActiveServer (pu4ErrorCode,
                                               &ClntExtActiveServer));
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntTraceLevel
 Input       :  The Indices

                The Object 
                testValFsTacClntTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntTraceLevel (UINT4 *pu4ErrorCode,
                              UINT4 u4TestValFsTacClntTraceLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValFsTacClntTraceLevel);

    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntRetransmit
 Input       :  The Indices

                The Object
                testValFsTacClntRetransmit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntRetransmit (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsTacClntRetransmit)
{
    return (nmhTestv2FsTacClntExtRetransmit (pu4ErrorCode,
                                             i4TestValFsTacClntRetransmit));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTacClntActiveServer
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntActiveServer (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacClntTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntTraceLevel (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsTacClntRetransmit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntRetransmit (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenStartRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenStartRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenStartRequests (UINT4
                                    *pu4RetValFsTacClntAuthenStartRequests)
{
    return (nmhGetFsTacClntExtAuthenStartRequests
            (pu4RetValFsTacClntAuthenStartRequests));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenContinueRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenContinueRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenContinueRequests (UINT4
                                       *pu4RetValFsTacClntAuthenContinueRequests)
{
    return (nmhGetFsTacClntExtAuthenContinueRequests
            (pu4RetValFsTacClntAuthenContinueRequests));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenEnableRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenEnableRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenEnableRequests (UINT4
                                     *pu4RetValFsTacClntAuthenEnableRequests)
{
    return (nmhGetFsTacClntExtAuthenEnableRequests
            (pu4RetValFsTacClntAuthenEnableRequests));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenAbortRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenAbortRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenAbortRequests (UINT4
                                    *pu4RetValFsTacClntAuthenAbortRequests)
{
    return (nmhGetFsTacClntExtAuthenAbortRequests
            (pu4RetValFsTacClntAuthenAbortRequests));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenPassReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenPassReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenPassReceived (UINT4 *pu4RetValFsTacClntAuthenPassReceived)
{
    return (nmhGetFsTacClntExtAuthenPassReceived
            (pu4RetValFsTacClntAuthenPassReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenFailReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenFailReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenFailReceived (UINT4 *pu4RetValFsTacClntAuthenFailReceived)
{
    return (nmhGetFsTacClntExtAuthenFailReceived
            (pu4RetValFsTacClntAuthenFailReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenGetUserReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenGetUserReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenGetUserReceived (UINT4
                                      *pu4RetValFsTacClntAuthenGetUserReceived)
{
    return (nmhGetFsTacClntExtAuthenGetUserReceived
            (pu4RetValFsTacClntAuthenGetUserReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenGetPassReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenGetPassReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenGetPassReceived (UINT4
                                      *pu4RetValFsTacClntAuthenGetPassReceived)
{
    return (nmhGetFsTacClntExtAuthenGetPassReceived
            (pu4RetValFsTacClntAuthenGetPassReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenGetDataReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenGetDataReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenGetDataReceived (UINT4
                                      *pu4RetValFsTacClntAuthenGetDataReceived)
{
    return (nmhGetFsTacClntExtAuthenGetDataReceived
            (pu4RetValFsTacClntAuthenGetDataReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenErrorReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenErrorReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenErrorReceived (UINT4
                                    *pu4RetValFsTacClntAuthenErrorReceived)
{
    return (nmhGetFsTacClntExtAuthenErrorReceived
            (pu4RetValFsTacClntAuthenErrorReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenFollowReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenFollowReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenFollowReceived (UINT4
                                     *pu4RetValFsTacClntAuthenFollowReceived)
{
    return (nmhGetFsTacClntExtAuthenFollowReceived
            (pu4RetValFsTacClntAuthenFollowReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenRestartReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenRestartReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenRestartReceived (UINT4
                                      *pu4RetValFsTacClntAuthenRestartReceived)
{
    return (nmhGetFsTacClntExtAuthenRestartReceived
            (pu4RetValFsTacClntAuthenRestartReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthenSessionTimouts
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthenSessionTimouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthenSessionTimouts (UINT4
                                     *pu4RetValFsTacClntAuthenSessionTimouts)
{
    return (nmhGetFsTacClntExtAuthenSessionTimouts
            (pu4RetValFsTacClntAuthenSessionTimouts));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthorRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthorRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthorRequests (UINT4 *pu4RetValFsTacClntAuthorRequests)
{
    return (nmhGetFsTacClntExtAuthorRequests
            (pu4RetValFsTacClntAuthorRequests));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthorPassAddReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthorPassAddReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthorPassAddReceived (UINT4
                                      *pu4RetValFsTacClntAuthorPassAddReceived)
{
    return (nmhGetFsTacClntExtAuthorPassAddReceived
            (pu4RetValFsTacClntAuthorPassAddReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthorPassReplReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthorPassReplReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthorPassReplReceived (UINT4
                                       *pu4RetValFsTacClntAuthorPassReplReceived)
{
    return (nmhGetFsTacClntExtAuthorPassReplReceived
            (pu4RetValFsTacClntAuthorPassReplReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthorFailReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthorFailReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthorFailReceived (UINT4 *pu4RetValFsTacClntAuthorFailReceived)
{
    return (nmhGetFsTacClntExtAuthorFailReceived
            (pu4RetValFsTacClntAuthorFailReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthorErrorReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthorErrorReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthorErrorReceived (UINT4
                                    *pu4RetValFsTacClntAuthorErrorReceived)
{
    return (nmhGetFsTacClntExtAuthorErrorReceived
            (pu4RetValFsTacClntAuthorErrorReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthorFollowReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthorFollowReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthorFollowReceived (UINT4
                                     *pu4RetValFsTacClntAuthorFollowReceived)
{
    return (nmhGetFsTacClntExtAuthorFollowReceived
            (pu4RetValFsTacClntAuthorFollowReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAuthorSessionTimeouts
 Input       :  The Indices

                The Object 
                retValFsTacClntAuthorSessionTimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAuthorSessionTimeouts (UINT4
                                      *pu4RetValFsTacClntAuthorSessionTimeouts)
{
    return (nmhGetFsTacClntExtAuthorSessionTimeouts
            (pu4RetValFsTacClntAuthorSessionTimeouts));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAcctStartRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntAcctStartRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAcctStartRequests (UINT4 *pu4RetValFsTacClntAcctStartRequests)
{
    return (nmhGetFsTacClntExtAcctStartRequests
            (pu4RetValFsTacClntAcctStartRequests));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAcctWdRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntAcctWdRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAcctWdRequests (UINT4 *pu4RetValFsTacClntAcctWdRequests)
{
    return (nmhGetFsTacClntExtAcctWdRequests
            (pu4RetValFsTacClntAcctWdRequests));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAcctStopRequests
 Input       :  The Indices

                The Object 
                retValFsTacClntAcctStopRequests
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAcctStopRequests (UINT4 *pu4RetValFsTacClntAcctStopRequests)
{
    return (nmhGetFsTacClntExtAcctStopRequests
            (pu4RetValFsTacClntAcctStopRequests));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAcctSuccessReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAcctSuccessReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAcctSuccessReceived (UINT4
                                    *pu4RetValFsTacClntAcctSuccessReceived)
{
    return (nmhGetFsTacClntExtAcctSuccessReceived
            (pu4RetValFsTacClntAcctSuccessReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAcctErrorReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAcctErrorReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAcctErrorReceived (UINT4 *pu4RetValFsTacClntAcctErrorReceived)
{
    return (nmhGetFsTacClntExtAcctErrorReceived
            (pu4RetValFsTacClntAcctErrorReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAcctFollowReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntAcctFollowReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAcctFollowReceived (UINT4 *pu4RetValFsTacClntAcctFollowReceived)
{
    return (nmhGetFsTacClntExtAcctFollowReceived
            (pu4RetValFsTacClntAcctFollowReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntAcctSessionTimeouts
 Input       :  The Indices

                The Object 
                retValFsTacClntAcctSessionTimeouts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntAcctSessionTimeouts (UINT4
                                    *pu4RetValFsTacClntAcctSessionTimeouts)
{
    return (nmhGetFsTacClntExtAcctSessionTimeouts
            (pu4RetValFsTacClntAcctSessionTimeouts));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntMalformedPktsReceived
 Input       :  The Indices

                The Object 
                retValFsTacClntMalformedPktsReceived
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntMalformedPktsReceived (UINT4
                                      *pu4RetValFsTacClntMalformedPktsReceived)
{
    return (nmhGetFsTacClntExtMalformedPktsReceived
            (pu4RetValFsTacClntMalformedPktsReceived));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntSocketFailures
 Input       :  The Indices

                The Object 
                retValFsTacClntSocketFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntSocketFailures (UINT4 *pu4RetValFsTacClntSocketFailures)
{
    return (nmhGetFsTacClntExtSocketFailures
            (pu4RetValFsTacClntSocketFailures));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntConnectionFailures
 Input       :  The Indices

                The Object 
                retValFsTacClntConnectionFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntConnectionFailures (UINT4 *pu4RetValFsTacClntConnectionFailures)
{
    return (nmhGetFsTacClntExtConnectionFailures
            (pu4RetValFsTacClntConnectionFailures));
}

/* LOW LEVEL Routines for Table : FsTacClntServerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTacClntServerTable
 Input       :  The Indices
                FsTacClntServerAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTacClntServerTable (UINT4 u4FsTacClntServerAddress)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhValidateIndexInstanceFsTacClntExtServerTable
            (IPVX_ADDR_FMLY_IPV4, &ClntExtServerAddress));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTacClntServerTable
 Input       :  The Indices
                FsTacClntServerAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsTacClntServerTable (UINT4 *pu4FsTacClntServerAddress)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4FsTacClntExtServerAddressType = IPVX_ADDR_FMLY_IPV4;

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_MAX_INET_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO,
            IPVX_MAX_INET_ADDR_LEN);

    ClntExtServerAddress.i4_Length = IPVX_MAX_INET_ADDR_LEN;

    if (nmhGetFirstIndexFsTacClntExtServerTable
        (&i4FsTacClntExtServerAddressType,
         &ClntExtServerAddress) == SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    if (i4FsTacClntExtServerAddressType == IPVX_ADDR_FMLY_IPV6)
    {
        return (SNMP_FAILURE);
    }
    PTR_FETCH4 (*pu4FsTacClntServerAddress, ClntExtServerAddress.pu1_OctetList);
    return (SNMP_SUCCESS);
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTacClntServerTable
 Input       :  The Indices
                FsTacClntServerAddress
                nextFsTacClntServerAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTacClntServerTable (UINT4 u4FsTacClntServerAddress,
                                     UINT4 *pu4NextFsTacClntServerAddress)
{
    tSNMP_OCTET_STRING_TYPE ExtServerAddress;
    tSNMP_OCTET_STRING_TYPE NextExtServerAddress;
    UINT1               au1TmpArray[IPVX_MAX_INET_ADDR_LEN];
    UINT1               au1TmpNextArray[IPVX_MAX_INET_ADDR_LEN];
    INT4                i4NextServerAddressType = IPVX_ADDR_FMLY_IPV4;

    MEMSET (&ExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&NextExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (au1TmpNextArray, TAC_ZERO, IPVX_MAX_INET_ADDR_LEN);

    ExtServerAddress.pu1_OctetList = au1TmpArray;
    NextExtServerAddress.pu1_OctetList = au1TmpNextArray;

    MEMSET (ExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_MAX_INET_ADDR_LEN);
    MEMSET (NextExtServerAddress.pu1_OctetList, TAC_ZERO,
            IPVX_MAX_INET_ADDR_LEN);

    PTR_ASSIGN4 (ExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    if (nmhGetNextIndexFsTacClntExtServerTable (IPVX_ADDR_FMLY_IPV4,
                                                &i4NextServerAddressType,
                                                &ExtServerAddress,
                                                &NextExtServerAddress) ==
        SNMP_FAILURE)
    {
        return (SNMP_FAILURE);
    }
    if (i4NextServerAddressType == IPVX_ADDR_FMLY_IPV6)
    {
        return (SNMP_FAILURE);
    }
    PTR_FETCH4 (*pu4NextFsTacClntServerAddress,
                NextExtServerAddress.pu1_OctetList);
    return (SNMP_SUCCESS);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTacClntServerStatus
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                retValFsTacClntServerStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntServerStatus (UINT4 u4FsTacClntServerAddress,
                             INT4 *pi4RetValFsTacClntServerStatus)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhGetFsTacClntExtServerStatus (IPVX_ADDR_FMLY_IPV4,
                                            &ClntExtServerAddress,
                                            pi4RetValFsTacClntServerStatus));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntServerSingleConnect
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                retValFsTacClntServerSingleConnect
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntServerSingleConnect (UINT4 u4FsTacClntServerAddress,
                                    INT4 *pi4RetValFsTacClntServerSingleConnect)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhGetFsTacClntExtServerSingleConnect (IPVX_ADDR_FMLY_IPV4,
                                                   &ClntExtServerAddress,
                                                   pi4RetValFsTacClntServerSingleConnect));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntServerPort
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                retValFsTacClntServerPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntServerPort (UINT4 u4FsTacClntServerAddress,
                           INT4 *pi4RetValFsTacClntServerPort)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhGetFsTacClntExtServerPort
            (IPVX_ADDR_FMLY_IPV4,
             &ClntExtServerAddress, pi4RetValFsTacClntServerPort));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntServerTimeout
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                retValFsTacClntServerTimeout
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntServerTimeout (UINT4 u4FsTacClntServerAddress,
                              INT4 *pi4RetValFsTacClntServerTimeout)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhGetFsTacClntExtServerTimeout
            (IPVX_ADDR_FMLY_IPV4,
             &ClntExtServerAddress, pi4RetValFsTacClntServerTimeout));
}

/****************************************************************************
 Function    :  nmhGetFsTacClntServerKey
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                retValFsTacClntServerKey
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTacClntServerKey (UINT4 u4FsTacClntServerAddress,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsTacClntServerKey)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    return (nmhGetFsTacClntExtServerKey
            (IPVX_ADDR_FMLY_IPV4,
             &ClntExtServerAddress, pRetValFsTacClntServerKey));
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTacClntServerStatus
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                setValFsTacClntServerStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntServerStatus (UINT4 u4FsTacClntServerAddress,
                             INT4 i4SetValFsTacClntServerStatus)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    return (nmhSetFsTacClntExtServerStatus
            (IPVX_ADDR_FMLY_IPV4,
             &ClntExtServerAddress, i4SetValFsTacClntServerStatus));

}

/****************************************************************************
 Function    :  nmhSetFsTacClntServerSingleConnect
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                setValFsTacClntServerSingleConnect
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntServerSingleConnect (UINT4 u4FsTacClntServerAddress,
                                    INT4 i4SetValFsTacClntServerSingleConnect)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhSetFsTacClntExtServerSingleConnect
            (IPVX_ADDR_FMLY_IPV4,
             &ClntExtServerAddress, i4SetValFsTacClntServerSingleConnect));
}

/****************************************************************************
 Function    :  nmhSetFsTacClntServerPort
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                setValFsTacClntServerPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntServerPort (UINT4 u4FsTacClntServerAddress,
                           INT4 i4SetValFsTacClntServerPort)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhSetFsTacClntExtServerPort (IPVX_ADDR_FMLY_IPV4,
                                          &ClntExtServerAddress,
                                          i4SetValFsTacClntServerPort));
}

/****************************************************************************
 Function    :  nmhSetFsTacClntServerTimeout
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                setValFsTacClntServerTimeout
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntServerTimeout (UINT4 u4FsTacClntServerAddress,
                              INT4 i4SetValFsTacClntServerTimeout)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhSetFsTacClntExtServerTimeout (IPVX_ADDR_FMLY_IPV4,
                                             &ClntExtServerAddress,
                                             i4SetValFsTacClntServerTimeout));
}

/****************************************************************************
 Function    :  nmhSetFsTacClntServerKey
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                setValFsTacClntServerKey
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTacClntServerKey (UINT4 u4FsTacClntServerAddress,
                          tSNMP_OCTET_STRING_TYPE * pSetValFsTacClntServerKey)
{

    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;

    return (nmhSetFsTacClntExtServerKey (IPVX_IPV4_ADDR_LEN,
                                         &ClntExtServerAddress,
                                         pSetValFsTacClntServerKey));
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTacClntServerStatus
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                testValFsTacClntServerStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntServerStatus (UINT4 *pu4ErrorCode,
                                UINT4 u4FsTacClntServerAddress,
                                INT4 i4TestValFsTacClntServerStatus)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhTestv2FsTacClntExtServerStatus (pu4ErrorCode,
                                               IPVX_ADDR_FMLY_IPV4,
                                               &ClntExtServerAddress,
                                               i4TestValFsTacClntServerStatus));
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntServerSingleConnect
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                testValFsTacClntServerSingleConnect
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntServerSingleConnect (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsTacClntServerAddress,
                                       INT4
                                       i4TestValFsTacClntServerSingleConnect)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    /* Copy the UINT4 Ip Address into the octet list */
    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhTestv2FsTacClntExtServerSingleConnect (pu4ErrorCode,
                                                      IPVX_ADDR_FMLY_IPV4,
                                                      &ClntExtServerAddress,
                                                      i4TestValFsTacClntServerSingleConnect));
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntServerPort
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                testValFsTacClntServerPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntServerPort (UINT4 *pu4ErrorCode,
                              UINT4 u4FsTacClntServerAddress,
                              INT4 i4TestValFsTacClntServerPort)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhTestv2FsTacClntExtServerPort (pu4ErrorCode,
                                             IPVX_ADDR_FMLY_IPV4,
                                             &ClntExtServerAddress,
                                             i4TestValFsTacClntServerPort));
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntServerTimeout
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                testValFsTacClntServerTimeout
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntServerTimeout (UINT4 *pu4ErrorCode,
                                 UINT4 u4FsTacClntServerAddress,
                                 INT4 i4TestValFsTacClntServerTimeout)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhTestv2FsTacClntExtServerTimeout (pu4ErrorCode,
                                                IPVX_ADDR_FMLY_IPV4,
                                                &ClntExtServerAddress,
                                                i4TestValFsTacClntServerTimeout));
}

/****************************************************************************
 Function    :  nmhTestv2FsTacClntServerKey
 Input       :  The Indices
                FsTacClntServerAddress

                The Object 
                testValFsTacClntServerKey
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTacClntServerKey (UINT4 *pu4ErrorCode,
                             UINT4 u4FsTacClntServerAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValFsTacClntServerKey)
{
    tSNMP_OCTET_STRING_TYPE ClntExtServerAddress;
    UINT1               au1TmpArray[IPVX_IPV4_ADDR_LEN];

    MEMSET (&ClntExtServerAddress, TAC_ZERO, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (au1TmpArray, TAC_ZERO, IPVX_IPV4_ADDR_LEN);
    ClntExtServerAddress.pu1_OctetList = au1TmpArray;
    MEMSET (ClntExtServerAddress.pu1_OctetList, TAC_ZERO, IPVX_IPV4_ADDR_LEN);

    PTR_ASSIGN4 (ClntExtServerAddress.pu1_OctetList, u4FsTacClntServerAddress);
    ClntExtServerAddress.i4_Length = IPVX_IPV4_ADDR_LEN;
    return (nmhTestv2FsTacClntExtServerKey (pu4ErrorCode,
                                            IPVX_ADDR_FMLY_IPV4,
                                            &ClntExtServerAddress,
                                            pTestValFsTacClntServerKey));
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTacClntServerTable
 Input       :  The Indices
                FsTacClntServerAddress
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTacClntServerTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
