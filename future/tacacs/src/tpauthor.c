/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpauthor.c,v 1.9 2015/04/28 12:49:59 siva Exp $
 *
 * Description: This file will have the authorization routines
 * for TACACS
 *******************************************************************/
#define __TPAUTHOR_C__
#include "tpinc.h"
#include "tpcli.h"

/******************************************************************************
Function Name: TacacsAuthorizeUser()
Description  : This module does the authorization functionality of the 
               tacacsClient. This frames TACACS+ authorization packet as per 
               input given by application. 
Input(s)     : pTacAuthorInput
                    input to authorization
Output(s)    : tTacAuthorOutput
                    if the interface is messaging, a message is contructed with
                    this data and sent to application to indicate error, if 
                    there is any error. If the authorization request processing
                    is successful nothing is given to applicationIf the 
                    interface is procedural no out put
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_MEM
               TAC_CLNT_ERROR_GEN
******************************************************************************/
INT4
TacacsAuthorizeUser (tTacAuthorInput * pTacAuthorInput)
{
    INT4                i4Result = 0;

    TAC_PROT_LOCK ();
    i4Result = tacacsAuthorization (pTacAuthorInput);
    TAC_PROT_UNLOCK ();

    return (i4Result);
}

/******************************************************************************
Function Name: tacacsAuthorization()
Description  : This module does the authorization functionality of the 
               tacacsClient. This frames TACACS+ authorization packet as per 
               input given by application. 
Input(s)     : pTacAuthorInput
                    input to authorization
Output(s)    : tTacAuthorOutput
                    if the interface is messaging, a message is contructed with
                    this data and sent to application to indicate error, if 
                    there is any error. If the authorization request processing
                    is successful nothing is given to applicationIf the 
                    interface is procedural no out put
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_MEM
               TAC_CLNT_ERROR_GEN
******************************************************************************/
INT4
tacacsAuthorization (tTacAuthorInput * pTacAuthorInput)
{
    tTacClntErrorStatisticsLog TacLog;
    tTacAuthorSessionInfo *pTacAuthorSession = NULL;
    tTacServerInfo     *pTacServer = NULL;
    UINT4               u4ActiveServer;
    UINT4               u4ServerStatus;
    UINT4               u4ServersContacted = 0;
    INT4                i4RetVal = TAC_CLNT_RES_NOT_OK;
    INT4                i4Result;
    PRIVATE UINT1       gau1TacAuthorPacket[TAC_AUTHOR_PKT_SIZE];
    UINT4               u4Flag = 0;

    /* Create Authorization session */
    u4ServerStatus = TAC_DESTROY;
    u4ActiveServer = (UINT4) TAC_ACTIVE_SERVER;
    if ((u4ActiveServer != 0) && ((u4ActiveServer - 1) < TAC_MAX_SERVERS))
    {
        pTacServer = &(TAC_SERVER_TABLE[u4ActiveServer - 1]);
        u4ServerStatus = pTacServer->u4Status;
        if (u4ServerStatus != TAC_ACTIVE)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "TACACS+ servers not available\n");
            return (TAC_CLNT_ERROR_NO_ACTIVE_SRV);
        }
        i4Result = TacAuthorEstablishSession (pTacAuthorInput, u4ActiveServer,
                                              &pTacAuthorSession);
        if (i4Result != TAC_CLNT_RES_OK)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Unable to establish connection\n");
            u4Flag = 1;
        }
    }
    if ((u4Flag == 1 || u4ActiveServer == 0))
    {
        for (u4ActiveServer = 1; ((u4ActiveServer <= TAC_MAX_SERVERS)
                                  && (u4ServersContacted < TAC_MAX_RETRIES));
             u4ActiveServer++)
        {
            pTacServer = &(TAC_SERVER_TABLE[u4ActiveServer - 1]);
            u4ServerStatus = pTacServer->u4Status;
            if (u4ServerStatus == TAC_ACTIVE)
            {
                u4ServersContacted++;
                i4RetVal =
                    TacAuthorEstablishSession (pTacAuthorInput, u4ActiveServer,
                                               &pTacAuthorSession);
                if (i4RetVal != TAC_CLNT_RES_OK)
                {
                    continue;
                }
                else
                {
                    break;
                }

            }
            else
            {
                continue;
            }
        }
        if (i4RetVal != TAC_CLNT_RES_OK)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "TACACS+ servers not available\n");
            return (TAC_CLNT_ERROR_NO_ACTIVE_SRV);
        }
    }
    if (pTacAuthorSession == NULL)
    {
        return TAC_CLNT_ERROR_MEM;
    }
    MEMSET (gau1TacAuthorPacket, 0, TAC_AUTHOR_PKT_SIZE);
   /*-----------------------------------------------------------------*
    * Take a copy of the statistics log. The "sent" fields are before *
    * actually sending the packet. So if "send" fails, those counters *
    * which are incremented shuold be decremented appropriately. For  *
    * This purpose TacLog is used                                     *
    *-----------------------------------------------------------------*/
    TacLog = TAC_CLNT_LOG;

   /*-----------------------------------------------------------------
    * Construct TACACS+ authorization packet. This is expected to    *
    * return TAC_CLNT_RES_OK always                                  *
    *----------------------------------------------------------------*/
    i4Result = tacAuthorConstructPacket
        (pTacAuthorSession, &(pTacAuthorSession->AuthorInput),
         gau1TacAuthorPacket);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Packet construction failed\n");
        tacCloseConnection (pTacAuthorSession->u4TacServer,
                            pTacAuthorSession->u4ConnectionId);
        tacAuthorReleaseSession (pTacAuthorSession);
        MEMSET (gau1TacAuthorPacket, 0, TAC_AUTHOR_PKT_SIZE);
        return (i4Result);
    }

    TAC_DBG_PKT (TAC_TRACE_PKT_TX, (CONST UINT1 *) TAC_MODULE,
                 gau1TacAuthorPacket);

    /* Encrypt the packet */
    tacEncryptDecrypt (gau1TacAuthorPacket, pTacServer->au1SecretKey);

    /* Send the packet */
    i4Result =
        tacSendPacket (gau1TacAuthorPacket, pTacAuthorSession->u4ConnectionId);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Send packet failed\n");

        tacCloseConnection (pTacAuthorSession->u4TacServer,
                            pTacAuthorSession->u4ConnectionId);
        tacAuthorReleaseSession (pTacAuthorSession);
        MEMSET (gau1TacAuthorPacket, 0, TAC_AUTHOR_PKT_SIZE);
        TAC_CLNT_LOG = TacLog;
        return (i4Result);
    }

    pTacAuthorSession->u4Timer = pTacServer->u4Timeout;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: TacAuthorEstablishSession()
Description  : To Establish connection with the given server. 
Input(s)     : pTacAuthorInput
                    The information required for authorizing a user
               ppTacAuthorSession
                    The authorization session information
               u4ActiveServer 
                    This gives the index of the active server.
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_TBL_FULL
               TAC_CLNT_ERROR_APP_INFO
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_SOCK_OPEN
               TAC_CLNT_ERROR_CONN_OPEN  
*****************************************************************************/
INT4
TacAuthorEstablishSession (tTacAuthorInput * pTacAuthorInput,
                           UINT4 u4ActiveServer,
                           tTacAuthorSessionInfo ** ppTacAuthorSession)
{
    INT4                i4Result = 0;

    i4Result = tacAuthorCreateSession (pTacAuthorInput, u4ActiveServer,
                                       ppTacAuthorSession);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Authorization session creation failed\n");
        return (i4Result);
    }

    /* Validate the input given for authorization */
    i4Result = tacAuthorValidateInput (&((*ppTacAuthorSession)->AuthorInput));
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Input validation failed\n");
        tacAuthorReleaseSession (*ppTacAuthorSession);
        return (i4Result);
    }

    i4Result = tacOpenConnection ((*ppTacAuthorSession)->u4TacServer,
                                  &((*ppTacAuthorSession)->u4ConnectionId));
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Connection open failed\n");
        tacAuthorReleaseSession (*ppTacAuthorSession);
        return (i4Result);
    }

    return TAC_CLNT_RES_OK;
}

/*****************************************************************************
Function Name: tacAuthorValidateInput()
Description  : To validate the authorization input given by application
Input(s)     : pTacAuthorInput
                    The information required for authorizing a user
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_APP_INFO
*****************************************************************************/
INT4
tacAuthorValidateInput (tTacAuthorInput * pTacAuthorInput)
{

    tTacAppInfo        *pTacApp;

    pTacApp = &(pTacAuthorInput->AppInfo);
    if (((pTacApp->TaskId == 0) ||
         (pTacApp->QueueId == 0)) && ((pTacApp->fpAppCallBackFn) == NULL))
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Invalid application resource info.\n");
        return (TAC_CLNT_ERROR_APP_INFO);
    }

    /* Check for the presence of user name */
    if (STRLEN (pTacAuthorInput->au1UserName) == 0)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "User name not present\n");
        return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate privilege level */
    if ((INT1) (pTacAuthorInput->u1PrivilegeLevel) < TAC_PRIV_LVL_MIN ||
        (pTacAuthorInput->u1PrivilegeLevel) > TAC_PRIV_LVL_MAX)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Invalid privilege level\n");
        return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate authentication type */
    switch (pTacAuthorInput->u1AuthenType)
    {
        case TAC_AUTHEN_TYPE_ASCII:
        case TAC_AUTHEN_TYPE_PAP:
        case TAC_AUTHEN_TYPE_CHAP:
        case TAC_AUTHEN_TYPE_MSCHAP:
        case TAC_AUTHEN_TYPE_ARAP:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authentication type\n");
            return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate authentication Method */
    switch (pTacAuthorInput->u1AuthenMethod)
    {
        case TAC_AUTHEN_METH_NOT_SET:
        case TAC_AUTHEN_METH_NONE:
        case TAC_AUTHEN_METH_KRB5:
        case TAC_AUTHEN_METH_LINE:
        case TAC_AUTHEN_METH_ENABLE:
        case TAC_AUTHEN_METH_LOCAL:
        case TAC_AUTHEN_METH_TACACSPLUS:
        case TAC_AUTHEN_METH_GUEST:
        case TAC_AUTHEN_METH_RADIUS:
        case TAC_AUTHEN_METH_KRB4:
        case TAC_AUTHEN_METH_RCMD:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authentication method\n");
            return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate authentication service */
    switch (pTacAuthorInput->u1Service)
    {
        case TAC_AUTHEN_SVC_NONE:
        case TAC_AUTHEN_SVC_LOGIN:
        case TAC_AUTHEN_SVC_ENABLE:
        case TAC_AUTHEN_SVC_PPP:
        case TAC_AUTHEN_SVC_ARAP:
        case TAC_AUTHEN_SVC_PT:
        case TAC_AUTHEN_SVC_RCMD:
        case TAC_AUTHEN_SVC_X25:
        case TAC_AUTHEN_SVC_NASI:
        case TAC_AUTHEN_SVC_FWPROXY:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authentication service\n");
            return (TAC_CLNT_ERROR_INPUT);
    }

    if (                        /* (pTacAuthorInput->AuthorArgs.u1NoOfArgs) == 0 || */
           (pTacAuthorInput->AuthorArgs.u1NoOfArgs) > TAC_AUTHOR_MAX_AVP)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "No arguments OR Lot of arguments\n");
        return (TAC_CLNT_ERROR_INPUT);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorCreateSession()
Description  : Creates a session info data structure for an authorization 
               session
Input(s)     : pTacAuthorInput
                    The authorization request input given by application
               u4ActiveServer
                    The index of the TACACS+ server to be contacted
Output(s)    : ppTacAuthorSession
                    The session created
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_TBL_FULL
               TAC_CLNT_ERROR_GEN
****************************************************************************/
INT4
tacAuthorCreateSession (tTacAuthorInput * pTacAuthorInput,
                        UINT4 u4ActiveServer,
                        tTacAuthorSessionInfo ** ppTacAuthorSession)
{
    tTacAuthorSessionInfo *pSession;
    UINT1              *pu1MemBlock = NULL;

    if (TAC_AUTHOR_NO_OF_CURRENT_SESSIONS >= TAC_MAX_AUTHOR_SESSION_LIMIT)
    {
        return (TAC_CLNT_ERROR_TBL_FULL);
    }

    /* Allocal session from authorization session */
    TAC_ALLOCATE_MEM_BLOCK (TAC_AUTHOR_SESSION_POOL_ID, pu1MemBlock);
    if (pu1MemBlock == NULL)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unable to allocate block from "
                 "Authorization session pool\n");
        return (TAC_CLNT_ERROR_TBL_FULL);
    }
    pSession = (tTacAuthorSessionInfo *) (VOID *) pu1MemBlock;

    /* Initialize session */
    MEMSET (pSession, 0, sizeof (tTacAuthorSessionInfo));

    /* Copy authorization input */
    pSession->AuthorInput = *pTacAuthorInput;

    /* Copy the TACACS+ server index */
    pSession->u4TacServer = u4ActiveServer;

    /* Generate session identifier */
    tacGenerateSessionId (&(pSession->u4SessionIdentifier));
    *ppTacAuthorSession = pSession;

    /* Update the authorization session table */
    TAC_AUTHOR_SESSION_TABLE[TAC_AUTHOR_NO_OF_CURRENT_SESSIONS] = pSession;
    TAC_AUTHOR_NO_OF_CURRENT_SESSIONS++;
    /*start the timer */
    TAC_STOP_TIMER (TAC_TIMER_LIST_ID, &TAC_SESSION_TIMER);
    if (TAC_START_TIMER (TAC_TIMER_LIST_ID, &TAC_SESSION_TIMER,
                         TAC_SESSION_TIMEOUT) != TMR_SUCCESS)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Timer start failed\n");
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorReleaseSession()
Description  : To release an authorization session
Input(s)     : pTacAuthorSession
                    The session to be released
Output(s)    :   None
Return(s)    :   TAC_CLNT_RES_OK
                 TAC_CLNT_ERROR_MEM
*****************************************************************************/
INT4
tacAuthorReleaseSession (tTacAuthorSessionInfo * pTacAuthorSession)
{
    tTacAuthorSessionInfo *pSession = NULL;
    INT4                i4Result;
    UINT1               u1Index = 0;

    /* To play safe ! */
    if (pTacAuthorSession == NULL)
    {
        return (TAC_CLNT_RES_OK);
    }

    for (u1Index = 0; u1Index < TAC_AUTHOR_NO_OF_CURRENT_SESSIONS; u1Index++)
    {
        if (u1Index < TAC_MAX_AUTHOR_SESSION_LIMIT)
        {
            pSession = TAC_AUTHOR_SESSION_TABLE[u1Index];
            if (pTacAuthorSession == pSession)
            {
                /* Session found */
                break;
            }
        }
    }

    if (u1Index >= TAC_AUTHOR_NO_OF_CURRENT_SESSIONS)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Session not found in the session table \n");
        return (TAC_CLNT_ERROR_GEN);
    }

    /* Release session */
    i4Result = TAC_RELEASE_MEM_BLOCK (TAC_AUTHOR_SESSION_POOL_ID,
                                      (UINT1 *) pSession);
    if (i4Result != MEM_SUCCESS)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Memory release failed \n");
        return (TAC_CLNT_ERROR_MEM);
    }

    if (u1Index < TAC_MAX_AUTHOR_SESSION_LIMIT)
        /* Reset the session table */
        TAC_AUTHOR_SESSION_TABLE[u1Index] = (tTacAuthorSessionInfo *) NULL;

    /*----------------------------------------------------------------------
     * Adjust the session table so that, there is no hole in the session   *
     * table. If the session released is the last entry in the table, then *
     * no adjustment is needed. If the entry is not last entry, then there *
     * will be hole in the table. Take the last entry of the table and fill*
     * in the hole.                                                        *
     *-------------------------------------------------------------------- */
    if (u1Index != (TAC_AUTHOR_NO_OF_CURRENT_SESSIONS - 1) &&
        (u1Index < TAC_MAX_AUTHOR_SESSION_LIMIT) &&
        ((TAC_AUTHOR_NO_OF_CURRENT_SESSIONS - 1) <
         TAC_MAX_AUTHOR_SESSION_LIMIT))
    {
        TAC_AUTHOR_SESSION_TABLE[u1Index] =
            TAC_AUTHOR_SESSION_TABLE[TAC_AUTHOR_NO_OF_CURRENT_SESSIONS - 1];
        TAC_AUTHOR_SESSION_TABLE[TAC_AUTHOR_NO_OF_CURRENT_SESSIONS - 1] =
            (tTacAuthorSessionInfo *) NULL;
    }

    TAC_AUTHOR_NO_OF_CURRENT_SESSIONS--;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorConstructPacket()
Description  : To construct an authorization packet
Input(s)     : pTacAuthorInput
                    Authorization input given by application
               pTacAuthorSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authorization request
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthorConstructPacket (tTacAuthorSessionInfo * pTacAuthorSession,
                          tTacAuthorInput * pTacAuthorInput,
                          UINT1 *pu1TacPacket)
{
    INT4                i4Result = TAC_CLNT_RES_OK;

    tacAuthorConstructReqBody (pTacAuthorSession, pTacAuthorInput,
                               pu1TacPacket);
    i4Result = tacAuthorConstructHeader (pTacAuthorSession,
                                         pTacAuthorInput, pu1TacPacket);
    if (i4Result == (INT4) TAC_CLNT_ERROR_SEQ_NO_WRAPPED)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Packet header construction failed \n");
    }

    return (i4Result);
}

/*****************************************************************************
Function Name: tacAuthorConstructReqBody()
Description  : To construct authorization request body
Input(s)     : pTacAuthorInput
                   Authorization input given by application
               pTacAuthorSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authorization request body
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthorConstructReqBody (tTacAuthorSessionInfo * pTacAuthorSession,
                           tTacAuthorInput * pTacAuthorInput,
                           UINT1 *pu1TacPacket)
{
    UINT2               u2Offset;
    UINT1               u1UserLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT1               u1ArgLen;
    UINT1               u1ArgCnt;

    UNUSED_PARAM (pTacAuthorSession);

    *(pu1TacPacket + TAC_AUTHOR_REQ_AUTHEN_METHOD_OFFSET) =
        pTacAuthorInput->u1AuthenMethod;
    *(pu1TacPacket + TAC_AUTHOR_REQ_PL_OFFSET) =
        pTacAuthorInput->u1PrivilegeLevel;
    *(pu1TacPacket + TAC_AUTHOR_REQ_AUTHEN_TYPE_OFFSET) =
        pTacAuthorInput->u1AuthenType;
    *(pu1TacPacket + TAC_AUTHOR_REQ_SVC_OFFSET) = pTacAuthorInput->u1Service;

    /* Fill user name length */
    u1UserLen = (UINT1) STRLEN (pTacAuthorInput->au1UserName);
    *(pu1TacPacket + TAC_AUTHOR_REQ_USER_LEN_OFFSET) = u1UserLen;

    /* Fill Port len */
    u1PortLen = (UINT1) STRLEN (pTacAuthorInput->au1Port);
    *(pu1TacPacket + TAC_AUTHOR_REQ_PORT_LEN_OFFSET) = u1PortLen;

    /* Fill Remote adderss len */
    u1RemAddrLen = (UINT1) STRLEN (pTacAuthorInput->au1RemAddr);
    *(pu1TacPacket + TAC_AUTHOR_REQ_REM_ADDR_LEN_OFFSET) = u1RemAddrLen;

    /* Fill argument count */
    u1ArgCnt = pTacAuthorInput->AuthorArgs.u1NoOfArgs;
    *(pu1TacPacket + TAC_AUTHOR_REQ_ARG_CNT_OFFSET) = u1ArgCnt;

    /* Fill user name */
    u2Offset = (UINT2) (TAC_AUTHOR_REQ_ARG1_LEN_OFFSET + u1ArgCnt);
    MEMCPY ((pu1TacPacket + u2Offset), pTacAuthorInput->au1UserName, u1UserLen);

    /* Fill port name */
    u2Offset += u1UserLen;
    MEMCPY ((pu1TacPacket + u2Offset), pTacAuthorInput->au1Port, u1PortLen);

    /* Fill remote address value */
    u2Offset += u1PortLen;
    MEMCPY ((pu1TacPacket + u2Offset), pTacAuthorInput->au1RemAddr,
            u1RemAddrLen);

    /* Fill arguments and their lengths */
    u2Offset += u1RemAddrLen;
    for (u1ArgCnt = 0; u1ArgCnt < (pTacAuthorInput->AuthorArgs.u1NoOfArgs);
         u1ArgCnt++)
    {
        u1ArgLen =
            (UINT1) STRLEN (pTacAuthorInput->AuthorArgs.au1AVP[u1ArgCnt]);

        /* Copy the attribute value */
        MEMCPY ((pu1TacPacket + u2Offset),
                (pTacAuthorInput->AuthorArgs.au1AVP[u1ArgCnt]), u1ArgLen);

        /* Set the argument length */
        *(pu1TacPacket + TAC_AUTHOR_REQ_ARG1_LEN_OFFSET + u1ArgCnt) = u1ArgLen;

        /* Update the packet offset */
        u2Offset += u1ArgLen;

        if (u2Offset >= TAC_AUTHOR_PKT_SIZE)
        {
            /*------------------------------------------------------
             * This part will never be executed. To play safe!. But *
             * the packet will be erraneous packet.                  *
             *------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unable to accommodate all "
                     "arguments. Not all arguments are filled! \n");
            break;
        }
    }

    /* Update the log for no. of authorization requests sent */
    (TAC_CLNT_LOG.u4AuthorReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorContstructHeader()
Description  : To construct the header of the TACACS+ packet
Input(s)     : pTacAuthroInput
                    Authorization input given by application
               pTacAuthorSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet with header filled
Return(s)    :   TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthorConstructHeader (tTacAuthorSessionInfo * pTacAuthorSession,
                          tTacAuthorInput * pTacAuthorInput,
                          UINT1 *pu1TacPacket)
{
    tTacServerInfo     *pTacServer;
    UINT4               u4SessionId;
    UINT4               u4PktLen;
    UINT1               u1TempLen;
    UINT1               u1ArgCnt;

    /* Sequence no. wrapped around. Session must be terminated and restarted */
    if (pTacAuthorSession->u1SequenceNoRcvd == TAC_MAX_SEQNO)
    {
        TAC_DBG (TAC_TRACE_INFO, TAC_MODULE, "Sequence no. wrapped around! \n");
        return (TAC_CLNT_ERROR_SEQ_NO_WRAPPED);

    }

    *(pu1TacPacket + TAC_PKT_TYPE_OFFSET) = TAC_AUTHOR;

    *(pu1TacPacket + TAC_PKT_SEQ_NO_OFFSET) = (UINT1)
        (pTacAuthorSession->u1SequenceNoRcvd + 1);

    pTacServer = &(TAC_SERVER_TABLE[pTacAuthorSession->u4TacServer - 1]);
    *(pu1TacPacket + TAC_PKT_FLAGS_OFFSET) =
        TAC_UNENC_FLAG | pTacServer->u1SCFlag;

    u4SessionId = OSIX_HTONL (pTacAuthorSession->u4SessionIdentifier);
    *((UINT4 *) (VOID *) (pu1TacPacket + TAC_PKT_SESSION_ID_OFFSET)) =
        u4SessionId;

    *(pu1TacPacket + TAC_PKT_VERSION_OFFSET) = (TAC_MAJOR_VER << 4) |
        TAC_MINOR_VER_DEFAULT;
    pTacAuthorSession->u1MinorVersionSent = TAC_MINOR_VER_DEFAULT;

    /* Calculate the length of authorization request body */
    /* User length */
    u1TempLen = *(pu1TacPacket + TAC_AUTHOR_REQ_USER_LEN_OFFSET);
    u4PktLen = (UINT4) u1TempLen;

    /* Port length */
    u1TempLen = *(pu1TacPacket + TAC_AUTHOR_REQ_PORT_LEN_OFFSET);
    u4PktLen += (UINT4) u1TempLen;

    /* Remote address length */
    u1TempLen = *(pu1TacPacket + TAC_AUTHOR_REQ_REM_ADDR_LEN_OFFSET);
    u4PktLen += (UINT4) u1TempLen;

    for (u1ArgCnt = 0; u1ArgCnt < (pTacAuthorInput->AuthorArgs.u1NoOfArgs);
         u1ArgCnt++)
    {
        u1TempLen = *(pu1TacPacket + TAC_AUTHOR_REQ_ARG1_LEN_OFFSET + u1ArgCnt);
        u4PktLen += (UINT4) u1TempLen;
    }

    u4PktLen += (UINT4) u1ArgCnt;    /* No of bytes for arguments lengths */

    u4PktLen += TAC_AUTHOR_REQ_LEN_FIXED_FIELDS;
    u4PktLen = OSIX_HTONL (u4PktLen);
    *((UINT4 *) (VOID *) (pu1TacPacket + TAC_PKT_LEN_OFFSET)) = u4PktLen;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorReceivedPkt()
Description  : To poll all connections to check if there is any packet        
Input(s)     : pu1NoOfPkts
                  The no. of packets already available in the receive buffer.
Output(s)    : pu1NoOfPkts
                  The no. packets received. + initial no (given as input)
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthorReceivedPkt (UINT1 *pu1PktCnt)
{
    tTacAuthorSessionInfo *pTacAuthorSession;
    tTacServerInfo     *pTacServer;
    tTacPkt            *pTacTempPkt;
    tTacAuthorOutput   *pTacAuthorOutput = NULL;
    UINT1               u1NoOfPkts;
    UINT1               u1Index;

    u1NoOfPkts = *pu1PktCnt;

    for (u1Index = 0; ((u1Index < TAC_MAX_AUTHOR_SESSION_LIMIT)
                       && (u1Index < TAC_AUTHOR_NO_OF_CURRENT_SESSIONS));
         u1Index++)
    {
        pTacAuthorSession = TAC_AUTHOR_SESSION_TABLE[u1Index];
        /* To play safe! */
        if (pTacAuthorSession == NULL)
        {
            continue;
        }

        pTacServer = &(TAC_SERVER_TABLE[pTacAuthorSession->u4TacServer - 1]);
        if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
        {
            pTacTempPkt =
                &(TAC_SC_TEMP_PKT[pTacAuthorSession->u4TacServer - 1]);
        }
        else
        {
            pTacTempPkt = &(pTacAuthorSession->TacTempPkt);
        }

        if (FD_ISSET (pTacAuthorSession->u4ConnectionId, &gReadSockFds))
        {
            tacReadPacket (pTacAuthorSession->u4ConnectionId, pTacTempPkt,
                           &u1NoOfPkts);
            if (u1NoOfPkts > 0)
            {
                SelAddFd (pTacAuthorSession->u4ConnectionId,
                          (VOID *) TacNotifyTask);
            }
            else
            {
                TAC_DBG (TAC_TRACE_INFO, TAC_MODULE,
                         "Authorization session closed by server\n");

                if (TAC_ALLOCATE_MEM_BLOCK (TAC_AUTHOR_OUTPUT_POOL_ID,
                                            pTacAuthorOutput) == NULL)
                {
                    TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                             "tacAuthorReceivedPkt: Memory Allocate"
                             " failed from Authorization output pool\n");
                    return TAC_CLNT_RES_NOT_OK;
                }

                MEMSET (pTacAuthorOutput, 0, sizeof (tTacAuthorOutput));

                pTacAuthorOutput->u4AppReqId =
                    pTacAuthorSession->AuthorInput.u4AppReqId;
                pTacAuthorOutput->i4AuthorStatus = TAC_CLNT_RES_NOT_OK;
                pTacAuthorOutput->i4ErrorValue = TAC_CLNT_ERROR_CONN_CLOSE;
                tacSendInfoToApp (TAC_AUTHOR,
                                  &(pTacAuthorSession->AuthorInput.AppInfo),
                                  pTacAuthorOutput);
                tacCloseConnection (pTacAuthorSession->u4TacServer,
                                    pTacAuthorSession->u4ConnectionId);
                tacAuthorReleaseSession (pTacAuthorSession);
                if (TAC_RELEASE_MEM_BLOCK (TAC_AUTHOR_OUTPUT_POOL_ID,
                                           (UINT1 *) pTacAuthorOutput)
                    == MEM_FAILURE)
                {
                    TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                             "tacAuthorReceivedPkt: Memory Release"
                             "failed for Authorization output pool\n");
                    return TAC_CLNT_RES_NOT_OK;
                }
            }

        }
        if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
        {
                /*--------------------------------------------------------*
                 * If single connection mode, all sessions will be having *
                 * same connection identifier. So polling once is enough  *
                 *--------------------------------------------------------*/
            break;
        }
        /* for all sessions */
    }                            /* select */

    *pu1PktCnt = u1NoOfPkts;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthorResponse()
Description  : To process the received authorization response packet
Input(s)     : pu1TacAuthorRespPkt
                  Received authorization response packet
Output(s)    : pTacSession
                  The authorization session corresponding to this packet
               pTacAuthorOutput   The out put of the authorization response
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_RCVD_PKT
******************************************************************************/
INT4
tacProcessAuthorResponse (UINT1 *pu1TacAuthorRespPkt,
                          tTacAuthorSessionInfo ** ppTacSession,
                          tTacAuthorOutput * pTacAuthorOutput)
{
    tTacAuthorSessionInfo *pTacAuthorSession = NULL;
    tTacServerInfo     *pTacServer;
    UINT4               u4SessionId;
    INT4                i4Result;
    UINT1               u1Index;

    /* Find the session corresponding to this packet */
    u4SessionId =
        *((UINT4 *) (VOID *) (pu1TacAuthorRespPkt + TAC_PKT_SESSION_ID_OFFSET));
    u4SessionId = OSIX_NTOHL (u4SessionId);

    for (u1Index = 0; ((u1Index < TAC_MAX_AUTHOR_SESSION_LIMIT) &&
                       (u1Index < TAC_AUTHOR_NO_OF_CURRENT_SESSIONS));
         u1Index++)
    {
        pTacAuthorSession = TAC_AUTHOR_SESSION_TABLE[u1Index];
        if (u4SessionId == (pTacAuthorSession->u4SessionIdentifier))
        {
            /* Session found */
            break;
        }
    }

    if (u1Index >= TAC_AUTHOR_NO_OF_CURRENT_SESSIONS)
    {
        /* Session not found */
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "No corresponding session available for the received packet \n");
        *ppTacSession = NULL;
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    *ppTacSession = pTacAuthorSession;

    /* Decrypt the packet */
    pTacServer = &(TAC_SERVER_TABLE[pTacAuthorSession->u4TacServer - 1]);
    tacEncryptDecrypt (pu1TacAuthorRespPkt, pTacServer->au1SecretKey);

    TAC_DBG_PKT (TAC_TRACE_PKT_RX, (CONST UINT1 *) TAC_MODULE,
                 pu1TacAuthorRespPkt);

    /* Validate the packet */
    i4Result = tacValidateAuthorResponse (pu1TacAuthorRespPkt,
                                          pTacAuthorSession);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Author. response pkt validation failed \n");

        /* Update the log. for no. malformed pkts received */
        (TAC_CLNT_LOG.u4MalformedPkts)++;

        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Set the sequence no. in the authorization session data structure */
    pTacAuthorSession->u1SequenceNoRcvd =
        *(pu1TacAuthorRespPkt + TAC_PKT_SEQ_NO_OFFSET);

    /* Fill the common fields of the authorization out put data */
    tacAuthorOutputFillCmn (pu1TacAuthorRespPkt, pTacAuthorSession,
                            pTacAuthorOutput);

    switch (pTacAuthorOutput->i4AuthorStatus)
    {
        case TAC_AUTHOR_STATUS_PASS_ADD:
            tacProcessAuthorPassAdd (pu1TacAuthorRespPkt, pTacAuthorSession,
                                     pTacAuthorOutput);
            break;
        case TAC_AUTHOR_STATUS_PASS_REPL:
            tacProcessAuthorPassRepl (pu1TacAuthorRespPkt, pTacAuthorSession,
                                      pTacAuthorOutput);
            break;
        case TAC_AUTHOR_STATUS_FAIL:
            tacProcessAuthorFail (pu1TacAuthorRespPkt, pTacAuthorSession,
                                  pTacAuthorOutput);
            break;
        case TAC_AUTHOR_STATUS_ERROR:
            tacProcessAuthorError (pu1TacAuthorRespPkt, pTacAuthorSession,
                                   pTacAuthorOutput);
            break;
        case TAC_AUTHOR_STATUS_FOLLOW:
            tacProcessAuthorFollow (pu1TacAuthorRespPkt, pTacAuthorSession,
                                    pTacAuthorOutput);
            break;
        default:
            /*-----------------------------------------------------------*
             * This part will never be executed, as the packet is already*
             * validated for correct authorization response status       *
             *-----------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authorization status rcvd \n");
            return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacValidateAuthorResponse()
Description  : To validate received authorization response
Input(s)     : pu1TacAuthorRespPkt
                  Received authorization response packet
               pTacAuthorSession
                  The authorization session corresponding to this 
                  authorization response packet
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_RCVD_PKT
******************************************************************************/
INT4
tacValidateAuthorResponse (UINT1 *pu1TacAuthorRespPkt,
                           tTacAuthorSessionInfo * pTacAuthorSession)
{
    tTacServerInfo     *pTacServer;
    UINT4               u4PktLen;
    UINT4               u4BodyLen;
    INT4                i4Result;
    UINT2               u2CompLen;
    UINT1               u1ArgCnt;
    UINT1               u1Index;

    i4Result = tacValidateCmnHdrFields (pu1TacAuthorRespPkt);

    if (i4Result != TAC_CLNT_RES_OK)
    {
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /*-----------------------------------------------------------------*
     * Check for single connect flag. This need to be checked only for *
     * first two packets.                                              *
     *---------------------------------------------------------------- */
    pTacServer = &(TAC_SERVER_TABLE[pTacAuthorSession->u4TacServer - 1]);
    if ((pTacServer->u1SCVerifyCount) < TAC_SC_VERIFY_COUNT_MAX)
    {
        if (*(pu1TacAuthorRespPkt + TAC_PKT_FLAGS_OFFSET) !=
            (pTacServer->u1SCFlag))
        {
            if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
            {
                /*----------------------------------------------------*
                 * Client operating in Single connect mode and server *
                 * suggests to operate in multiple connection mode. So*
                 * switch back to multiple connection mode            *
                 *----------------------------------------------------*/
                pTacServer->u1SCFlag = 0;
                pTacServer->i4SingleConnId = 0;
            }
            else
            {
                /*----------------------------------------------------*
                 * Client operating in multiple connection mode and   *
                 * server suggests to operate in single connection    *
                 * mode. So switch back to single connection mode     *
                 *----------------------------------------------------*/
                pTacServer->u1SCFlag = TAC_SINGLE_CONNECT_FLAG;
                pTacServer->i4SingleConnId =
                    (INT4) (pTacAuthorSession->u4ConnectionId);
            }
            TAC_DBG (TAC_TRACE_INFO, TAC_MODULE,
                     "Connection mode changed as server suggestion\n");
        }
        pTacServer->u1SCVerifyCount++;
    }

    /* Validate minor version */
    if (((*(pu1TacAuthorRespPkt + TAC_PKT_VERSION_OFFSET)) &
         TAC_MINOR_VER_MASK) != (pTacAuthorSession->u1MinorVersionSent))
    {

        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Minor version not matching with server!\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /*--------------------------------------------------------------------*
     * Validate sequence no. received. The received sequence no. should be*
     * greater than previous received sequence no. by 2.                  *
     *--------------------------------------------------------------------*/
    if (*(pu1TacAuthorRespPkt + TAC_PKT_SEQ_NO_OFFSET) !=
        ((pTacAuthorSession->u1SequenceNoRcvd) + 2))
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Invalid sequence no received\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Validate length */
    u4PktLen = *((UINT4 *) (VOID *) (pu1TacAuthorRespPkt + TAC_PKT_LEN_OFFSET));
    u4PktLen = OSIX_NTOHL (u4PktLen);
    if (u4PktLen == 0)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Serve unable to understand the packet sent\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    u1ArgCnt = *(pu1TacAuthorRespPkt + TAC_AUTHOR_RESP_ARG_CNT_OFFSET);

    u4BodyLen = TAC_AUTHOR_RESP_LEN_FIXED_FIELDS + u1ArgCnt;
    for (u1Index = 0; u1Index < u1ArgCnt; u1Index++)
    {
        u2CompLen = *(pu1TacAuthorRespPkt +
                      TAC_AUTHOR_RESP_ARG1_LEN_OFFSET + u1Index);
        u4BodyLen += (UINT4) u2CompLen;
    }

    u2CompLen = *((UINT2 *) (VOID *) (pu1TacAuthorRespPkt +
                                      TAC_AUTHOR_RESP_SRV_MSG_LEN_OFFSET));
    u2CompLen = OSIX_NTOHS (u2CompLen);
    u4BodyLen += (UINT4) u2CompLen;
    u2CompLen = *((UINT2 *) (VOID *) (pu1TacAuthorRespPkt +
                                      TAC_AUTHOR_RESP_DATA_LEN_OFFSET));
    u2CompLen = OSIX_NTOHS (u2CompLen);
    u4BodyLen += (UINT4) u2CompLen;

    if (u4PktLen != u4BodyLen)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Length in the header and total reply body length are not equal\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Validate authorization status received */
    switch (*(pu1TacAuthorRespPkt + TAC_AUTHOR_RESP_STATUS_OFFSET))
    {
        case TAC_AUTHOR_STATUS_PASS_ADD:
        case TAC_AUTHOR_STATUS_PASS_REPL:
        case TAC_AUTHOR_STATUS_FAIL:
            break;
        case TAC_AUTHOR_STATUS_FOLLOW:
        {
            if (u1ArgCnt != 0)
            {
                TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                         "Author. resp. follow received with arg_cnt != 0\n");
                return (TAC_CLNT_ERROR_RCVD_PKT);
            }
        }
        case TAC_AUTHOR_STATUS_ERROR:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authorization status in response packet\n");
            return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorOutputFillCmn()
Description  : To fill the common fields of authorization out put
Input(s)     : pu1TacAuthorRespPkt
                  Validated authorization response packet
               pTacAuthorSession
                  The authorization session corresponding to this packet
Output(s)    : pTacAuthorOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthorOutputFillCmn (UINT1 *pu1TacAuthorRespPkt,
                        tTacAuthorSessionInfo * pTacAuthorSession,
                        tTacAuthorOutput * pTacAuthorOutput)
{
    UINT2               u2Len;
    UINT2               u2Offset;

    /*-----------------------------------------------------------------* 
     * Copy the Application request identifier, so that the application* 
     * will match this response with the request it made and will do   *
     * further processing                                              *
     *-----------------------------------------------------------------*/
    pTacAuthorOutput->u4AppReqId = pTacAuthorSession->AuthorInput.u4AppReqId;

    /* Set the authorization status */
    pTacAuthorOutput->i4AuthorStatus =
        *(pu1TacAuthorRespPkt + TAC_AUTHOR_RESP_STATUS_OFFSET);

    /* If server message is available, send the message to application */
    u2Len = *((UINT2 *) (VOID *) (pu1TacAuthorRespPkt +
                                  TAC_AUTHOR_RESP_SRV_MSG_LEN_OFFSET));
    u2Len = OSIX_NTOHS (u2Len);
    if (u2Len != 0)
    {
        if (u2Len > TAC_SRV_MSG_LEN)
        {
            u2Len = TAC_SRV_MSG_LEN;
        }
        u2Offset = (UINT2) (TAC_AUTHOR_RESP_ARG1_LEN_OFFSET +
                            (*
                             (pu1TacAuthorRespPkt +
                              TAC_AUTHOR_RESP_ARG_CNT_OFFSET)));
        MEMCPY (pTacAuthorOutput->au1ServerMessage,
                (pu1TacAuthorRespPkt + u2Offset), u2Len);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorRespExtractArgs()
Description  : To extract the arguments from received packet and fill the out  
               put data structure
Input(s)     : pu1TacAuthorRespPkt
                  Validated authorization response packet
               pTacAuthorSession
                  The authorization  session corresponding to this packet
Output(s)    : pTacAuthorOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthorRespExtractArgs (UINT1 *pu1TacAuthorRespPkt,
                          tTacAuthorSessionInfo * pTacAuthorSession,
                          tTacAuthorOutput * pTacAuthorOutput)
{
    UINT4               u4Offset;
    UINT2               u2MsgLen;
    UINT2               u2DataLen;
    UINT1               u1ArgCnt;
    UINT1               u1ArgLen;
    UINT1               u1Index;
    UINT1               u1AvpCnt;

    UNUSED_PARAM (pTacAuthorSession);

    /* Copy the arguments into out put data structure */
    u1ArgCnt = *(pu1TacAuthorRespPkt + TAC_AUTHOR_RESP_ARG_CNT_OFFSET);

    if (u1ArgCnt != 0)
    {
        /* To extract the arguments, first calculate argument offset */
        u2MsgLen = *((UINT2 *) (VOID *) (pu1TacAuthorRespPkt +
                                         TAC_AUTHOR_RESP_SRV_MSG_LEN_OFFSET));
        u2MsgLen = OSIX_NTOHS (u2MsgLen);

        u2DataLen = *((UINT2 *) (VOID *) (pu1TacAuthorRespPkt +
                                          TAC_AUTHOR_RESP_DATA_LEN_OFFSET));
        u2DataLen = OSIX_NTOHS (u2DataLen);

        u4Offset = (UINT4) (TAC_AUTHOR_RESP_ARG1_LEN_OFFSET + u1ArgCnt +
                            u2MsgLen + u2DataLen);

         /*------------------------------------------------------------*
         * If th argument count exceeds the maximum arguments supported*
         * extract only possible arguments                             *
         *-------------------------------------------------------------*/
        if (u1ArgCnt >= TAC_AUTHOR_MAX_AVP)
        {
            /*-------------------------------------------------------*
             * No. of received arguments > supported arguments. Take *
             * only supported arguments                              *
             *-------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_INFO, TAC_MODULE,
                     "Received more no. arguments than supported!\n");
            u1ArgCnt = TAC_AUTHOR_MAX_AVP;
        }

        u1AvpCnt = 0;
        for (u1Index = 0; u1Index < u1ArgCnt; u1Index++)
        {
            u1ArgLen = (*(pu1TacAuthorRespPkt +
                          TAC_AUTHOR_RESP_ARG1_LEN_OFFSET + u1Index));
            if (u1ArgLen >= TAC_AUTHOR_AVP_MAX_SIZE)
            {
                /* Arg. size > max. arg size. Skip this argument */
                TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                         "Received arg. size > supported arg. size\n");

                /* Update the packet offset */
                u4Offset += u1ArgLen;
                continue;
            }

            /* Copy the attribute value pair */
            MEMCPY ((pTacAuthorOutput->AuthorArgs.au1AVP[u1AvpCnt]),
                    (pu1TacAuthorRespPkt + u4Offset), u1ArgLen);
            u1AvpCnt++;

            /* Update the packet offset */
            u4Offset += u1ArgLen;
        }

        /* Fill the no. of arguments extracted from received packet */
        pTacAuthorOutput->AuthorArgs.u1NoOfArgs = u1AvpCnt;
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorOutputFillData()
Description  : To fill the data field of authorization out put
Input(s)     : pu1TacAuthorReplyPkt
                  Validated authorization reply packet
               pTacAuthorSession
                  The authorization session corresponding to this packet
Output(s)    : pTacAuthorOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthorOutputFillData (UINT1 *pu1TacAuthorRespPkt,
                         tTacAuthorSessionInfo * pTacAuthorSession,
                         tTacAuthorOutput * pTacAuthorOutput)
{
    UINT2               u2DataLen;
    UINT2               u2MsgLen;
    UINT4               u4Offset;

    UNUSED_PARAM (pTacAuthorSession);

    u2DataLen = *((UINT2 *) (VOID *) (pu1TacAuthorRespPkt +
                                      TAC_AUTHOR_RESP_DATA_LEN_OFFSET));
    u2DataLen = OSIX_NTOHS (u2DataLen);
    if (u2DataLen != 0)
    {
        if (u2DataLen > TAC_SRV_DATA_LEN)
        {
            u2DataLen = TAC_SRV_DATA_LEN;
        }

        /*------------------------------------------------------------*
         * To calculate the data offset, first get the server message *
         * length. The add this with server message offset            *
         *------------------------------------------------------------*/
        u2MsgLen = *((UINT2 *) (VOID *) (pu1TacAuthorRespPkt +
                                         TAC_AUTHOR_RESP_SRV_MSG_LEN_OFFSET));
        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        u4Offset = (UINT4) (u2MsgLen + TAC_AUTHOR_RESP_ARG1_LEN_OFFSET +
                            (*
                             (pu1TacAuthorRespPkt +
                              TAC_AUTHOR_RESP_ARG_CNT_OFFSET)));

        MEMCPY (pTacAuthorOutput->au1ServerData,
                (pu1TacAuthorRespPkt + u4Offset), u2DataLen);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthorPassAdd()
Description  : To process the authorization response for authorization pass add
Input(s)     : pu1TacAuthorRespPkt
                  Validated authorization response packet
               pTacAuthorSession
                  The authorization  session corresponding to this packet
Output(s)    : pTacAuthorOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthorPassAdd (UINT1 *pu1TacAuthorRespPkt,
                         tTacAuthorSessionInfo * pTacAuthorSession,
                         tTacAuthorOutput * pTacAuthorOutput)
{
    tacAuthorRespExtractArgs (pu1TacAuthorRespPkt, pTacAuthorSession,
                              pTacAuthorOutput);

    /* Update the log for no. of authorization pass adds received */
    (TAC_CLNT_LOG.u4AuthorPassAdds)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthorPassRepl()
Description  : To process the authorization response for authorization pass
               replace
Input(s)     : pu1TacAuthorRespPkt
                  Validated authorization response packet
               pTacAuthorSession
                  The authorization  session corresponding to this packet
Output(s)    : pTacAuthorOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthorPassRepl (UINT1 *pu1TacAuthorRespPkt,
                          tTacAuthorSessionInfo * pTacAuthorSession,
                          tTacAuthorOutput * pTacAuthorOutput)
{

    tacAuthorRespExtractArgs (pu1TacAuthorRespPkt, pTacAuthorSession,
                              pTacAuthorOutput);

    /* Update the log for no. of authorization pass repl received */
    (TAC_CLNT_LOG.u4AuthorPassRepls)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthorFail()
Description  : To process the authorization response for authorization fail
Input(s)     : pu1TacAuthorRespPkt
                  Validated authorization response packet
               pTacAuthorSession
                  The authorization  session corresponding to this packet
Output(s)    : pTacAuthorOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthorFail (UINT1 *pu1TacAuthorRespPkt,
                      tTacAuthorSessionInfo * pTacAuthorSession,
                      tTacAuthorOutput * pTacAuthorOutput)
{
    /*---------------------------------------------------------------*
     * Send the data to application. So that this would be printed on *
     * administrative console                                        *
     *---------------------------------------------------------------*/
    tacAuthorOutputFillData (pu1TacAuthorRespPkt, pTacAuthorSession,
                             pTacAuthorOutput);

    /* Update the log for no. of authorization fails received */
    (TAC_CLNT_LOG.u4AuthorFails)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthorError()
Description  : To process the authorization response for authorization error
Input(s)     : pu1TacAuthorRespPkt
                  Validated authorization response packet
               pTacAuthorSession
                  The authorization  session corresponding to this packet
Output(s)    : pTacAuthorOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthorError (UINT1 *pu1TacAuthorRespPkt,
                       tTacAuthorSessionInfo * pTacAuthorSession,
                       tTacAuthorOutput * pTacAuthorOutput)
{
    /*---------------------------------------------------------------*
     * Send the data to application. So that this would be printed on*
     * administrative console                                        *
     *---------------------------------------------------------------*/
    tacAuthorOutputFillData (pu1TacAuthorRespPkt, pTacAuthorSession,
                             pTacAuthorOutput);

    /* Update the log for no. of authorization errors received */
    (TAC_CLNT_LOG.u4AuthorErrors)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthorFollow()
Description  : To process the authorization response for authorization error
Input(s)     : pu1TacAuthorRespPkt
                  Validated authorization response packet
               pTacAuthorSession
                  The authorization  session corresponding to this packet
Output(s)    : pTacAuthorOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthorFollow (UINT1 *pu1TacAuthorRespPkt,
                        tTacAuthorSessionInfo * pTacAuthorSession,
                        tTacAuthorOutput * pTacAuthorOutput)
{
    /*----------------------------------------------------------------*
     * If server data is available, send the message to application   *
     * This data will be list of alternate servers suggested by the   *
     * current server for futher authorization. The client does not   *
     * use the alternate servers by itself. The alternate server list *
     * will be given to application. It is upto the application to    *
     * treat this authorization failure or restart the authorization  *
     * with any of the servers mentioned in the list                  *
     *----------------------------------------------------------------*/
    tacAuthorOutputFillData (pu1TacAuthorRespPkt, pTacAuthorSession,
                             pTacAuthorOutput);

    /* Update the log for no. of authorization errors received */
    (TAC_CLNT_LOG.u4AuthorFollows)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthorChkSessionTimeout()
Description  : To check if any session has timed out. The time out sessions are
               released
Input(s)     : None
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthorChkSessionTimeout ()
{
    tTacAuthorOutput   *pTacAuthorOutput = NULL;
    tTacAuthorSessionInfo *pTacAuthorSession;
    UINT1               u1Index;

    if (TAC_ALLOCATE_MEM_BLOCK (TAC_AUTHOR_OUTPUT_POOL_ID,
                                pTacAuthorOutput) == NULL)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unable to allocate block from"
                 "Authorization output pool\n");
        return TAC_CLNT_RES_NOT_OK;
    }

    for (u1Index = 0; ((u1Index < TAC_MAX_AUTHOR_SESSION_LIMIT) &&
                       (u1Index < TAC_AUTHOR_NO_OF_CURRENT_SESSIONS));
         u1Index++)
    {
        pTacAuthorSession = TAC_AUTHOR_SESSION_TABLE[u1Index];
        (pTacAuthorSession->u4Timer)--;
        if ((INT4) (pTacAuthorSession->u4Timer) == 0)
        {
            /* Time out has occured. Release the session */
            TAC_DBG (TAC_TRACE_INFO, TAC_MODULE,
                     "Authorization session timed out and released\n");

            /* Update the log for no. of session time outs */
            (TAC_CLNT_LOG.u4AuthorSessionTimeouts)++;

            MEMSET (pTacAuthorOutput, 0, sizeof (tTacAuthorOutput));

            pTacAuthorOutput->u4AppReqId =
                pTacAuthorSession->AuthorInput.u4AppReqId;
            pTacAuthorOutput->i4AuthorStatus = TAC_CLNT_RES_NOT_OK;
            pTacAuthorOutput->i4ErrorValue = TAC_CLNT_ERROR_SESSION_TIMEOUT;
            tacSendInfoToApp (TAC_AUTHOR,
                              &(pTacAuthorSession->AuthorInput.AppInfo),
                              pTacAuthorOutput);
            tacCloseConnection (pTacAuthorSession->u4TacServer,
                                pTacAuthorSession->u4ConnectionId);
            tacAuthorReleaseSession (pTacAuthorSession);
        }
    }

    if (TAC_RELEASE_MEM_BLOCK (TAC_AUTHOR_OUTPUT_POOL_ID,
                               (UINT1 *) pTacAuthorOutput) == MEM_FAILURE)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "tacProcessReceivedPacket:"
                 "Memory Release failed for Authorization output pool\n");
        return TAC_CLNT_RES_NOT_OK;
    }

    return (TAC_CLNT_RES_OK);
}

/******************************** End of file *********************************/
