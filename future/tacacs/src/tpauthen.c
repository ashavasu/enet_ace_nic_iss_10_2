/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpauthen.c,v 1.9 2015/04/28 12:49:59 siva Exp $
 *
 * Description: This file will have the authentication routines
 * for TACACS
 *******************************************************************/
#define __TPAUTHEN_C__
#include "tpinc.h"

/*****************************************************************************
Function Name: TacacsAuthenticateUser ()
Description  : This module does the authentication functionality of the 
               tacacsClient. This frames TACACS+ authentication packet for the 
               input given by application. 
Input(s)     : pTacAuthenInput
                        input to authentication
Output(s)    : None 
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_MEM
*****************************************************************************/
INT4
TacacsAuthenticateUser (tTacAuthenInput * pTacAuthenInput)
{
    INT4                i4Result = 0;

    TAC_PROT_LOCK ();
    i4Result = tacacsAuthentication (pTacAuthenInput);
    TAC_PROT_UNLOCK ();

    return (i4Result);
}

/*****************************************************************************
Function Name: tacacsAuthenticaion()
Description  : This module does the authentication functionality of the 
               tacacsClient. This frames TACACS+ authentication packet for the 
               input given by application. 
Input(s)     : pTacAuthenInput
                        input to authentication
Output(s)    : None 
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_MEM
*****************************************************************************/
INT4
tacacsAuthentication (tTacAuthenInput * pTacAuthenInput)
{
    tTacClntErrorStatisticsLog TacLog;
    tTacAuthenSessionInfo *pTacAuthenSession = NULL;
    tTacServerInfo     *pTacServer = NULL;
    UINT4               u4ActiveServer;
    UINT4               u4ServerStatus;
    UINT4               u4ServersContacted = 0;
    INT4                i4RetVal = TAC_CLNT_RES_NOT_OK;
    UINT4               u4Flag = 0;
    INT4                i4Result;
    UINT1               u1TacAuthenPktType = 0;
    PRIVATE UINT1       gau1TacAuthenPacket[TAC_AUTHEN_PKT_SIZE];

    /* Create Authentication session */
    u4ServerStatus = TAC_DESTROY;
    u4ActiveServer = (UINT4) TAC_ACTIVE_SERVER;
    if ((u4ActiveServer != 0) && ((u4ActiveServer - 1) < TAC_MAX_SERVERS))
    {
        pTacServer = &(TAC_SERVER_TABLE[u4ActiveServer - 1]);
        u4ServerStatus = pTacServer->u4Status;
        if (u4ServerStatus != TAC_ACTIVE)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "TACACS+ servers not available\n");
            return (TAC_CLNT_ERROR_NO_ACTIVE_SRV);
        }
        i4Result = TacAuthenEstablishSession (pTacAuthenInput, u4ActiveServer,
                                              &pTacAuthenSession,
                                              &u1TacAuthenPktType);
        if (i4Result != TAC_CLNT_RES_OK)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Unable to establish connection\n");
            u4Flag = 1;
        }
    }
    if (u4Flag == 1 || u4ActiveServer == 0)
    {
        for (u4ActiveServer = 1; ((u4ActiveServer <= TAC_MAX_RETRIES)
                                  && (u4ServersContacted < TAC_MAX_RETRIES));
             u4ActiveServer++)
        {
            pTacServer = &(TAC_SERVER_TABLE[u4ActiveServer - 1]);

            u4ServerStatus = pTacServer->u4Status;
            if (u4ServerStatus == TAC_ACTIVE)
            {
                u4ServersContacted++;
                i4RetVal =
                    TacAuthenEstablishSession (pTacAuthenInput, u4ActiveServer,
                                               &pTacAuthenSession,
                                               &u1TacAuthenPktType);
                if (i4RetVal != TAC_CLNT_RES_OK)
                {
                    continue;
                }
                else
                {
                    break;
                }

            }
            else
            {
                continue;
            }
        }
        if (i4RetVal != TAC_CLNT_RES_OK)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Unable to connect TACACS+ servers \n");
            return (TAC_CLNT_ERROR_NO_ACTIVE_SRV);
        }
    }
    if (pTacAuthenSession == NULL)
    {
        return (TAC_CLNT_ERROR_MEM);
    }
    MEMSET (gau1TacAuthenPacket, 0, TAC_AUTHEN_PKT_SIZE);

   /*-----------------------------------------------------------------*
    * Take a copy of the statistics log. The "sent" fields are before *
    * actually sending the packet. So if "send" fails, those counters *
    * which are incremented shuold be decremented appropriately. For  *
    * This purpose TacLog is used                                     *
    *-----------------------------------------------------------------*/
    TacLog = TAC_CLNT_LOG;

   /*----------------------------------------------------------------*
    * Construct TACACS+ authentication packet. If this returns       *
    * TAC_CLNT_ERROR_SEQ_NO_WRAPPED, the session should be terminated*
    * - As per PROTOCOL. But if continue need to be sent with ABORT, *
    * no valid sequence no. is available. So closing the connection  *
    * is the possible action. But for single connection mode, what   *
    * should be action?. The standard is not clear about this.       *
    *----------------------------------------------------------------*/
    i4Result = tacAuthenConstructPacket (pTacAuthenSession,
                                         &(pTacAuthenSession->AuthenInput),
                                         u1TacAuthenPktType,
                                         gau1TacAuthenPacket);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Packet construction failed\n");
        tacCloseConnection (pTacAuthenSession->u4TacServer,
                            pTacAuthenSession->u4ConnectionId);
        tacAuthenReleaseSession (pTacAuthenSession);
        MEMSET (gau1TacAuthenPacket, 0, TAC_AUTHEN_PKT_SIZE);
        return (i4Result);
    }

    TAC_DBG_PKT (TAC_TRACE_PKT_TX, (CONST UINT1 *) TAC_MODULE,
                 gau1TacAuthenPacket);

    /* Encrypt the packet */
    tacEncryptDecrypt (gau1TacAuthenPacket, pTacServer->au1SecretKey);

    /* Send the packet */
    i4Result =
        tacSendPacket (gau1TacAuthenPacket, pTacAuthenSession->u4ConnectionId);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Send packet failed\n");
        tacCloseConnection (pTacAuthenSession->u4TacServer,
                            pTacAuthenSession->u4ConnectionId);
        tacAuthenReleaseSession (pTacAuthenSession);
        MEMSET (gau1TacAuthenPacket, 0, TAC_AUTHEN_PKT_SIZE);
        TAC_CLNT_LOG = TacLog;
        return (i4Result);
    }

    /* Set authentication session state */
    if (u1TacAuthenPktType == TAC_AUTHEN_PKT_START)
    {
        pTacAuthenSession->u1SessionState = TAC_AUTHEN_START_SENT;

        /* Set the timer for the authentication session */
        pTacAuthenSession->u4Timer = pTacServer->u4Timeout;
    }
    else
    {
        /* Continue was authentication termination, release the session */
        if ((pTacAuthenSession->AuthenInput.u1Action) == TAC_AUTHEN_TERMINATE)
        {
            /* Abort would have been successfuly sent. So relese the session */
            tacCloseConnection (pTacAuthenSession->u4TacServer,
                                pTacAuthenSession->u4ConnectionId);
            tacAuthenReleaseSession (pTacAuthenSession);
        }
        else
        {
            pTacAuthenSession->u1SessionState = TAC_AUTHEN_CONTINUE_SENT;
            /* Set the timer for the authentication session */
            pTacAuthenSession->u4Timer = pTacServer->u4Timeout;
        }
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: TacAuthenEstablishSession()
Description  : To Establish connection with the given server. 
Input(s)     : pTacAuthenInput
                    The information required for authenticating a user
               ppTacAuthenSession
                    The authentication session information
               pu1TacAuthenPktType
                    This gives info that, the packet that need to be
                    constructed of authentication start or authentication
                    continue. The validation would be carried out based on this.
               u4ActiveServer 
                    This gives the index of the active server.
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_TBL_FULL
               TAC_CLNT_ERROR_APP_INFO
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_SOCK_OPEN
               TAC_CLNT_ERROR_CONN_OPEN  
*****************************************************************************/
INT4
TacAuthenEstablishSession (tTacAuthenInput * pTacAuthenInput,
                           UINT4 u4ActiveServer,
                           tTacAuthenSessionInfo ** ppTacAuthenSession,
                           UINT1 *pu1TacAuthenPktType)
{
    INT4                i4Result = 0;

    i4Result = tacAuthenCreateSession (pTacAuthenInput, u4ActiveServer,
                                       ppTacAuthenSession, pu1TacAuthenPktType);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Authentication session creation failed\n");
        return (i4Result);
    }

    /* Validate the input given for authentication */
    i4Result = tacAuthenValidateInput (&((*ppTacAuthenSession)->AuthenInput),
                                       *ppTacAuthenSession,
                                       *pu1TacAuthenPktType);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Input validation failed\n");
        tacAuthenReleaseSession (*ppTacAuthenSession);
        return (i4Result);
    }

    /* If this is going to be authentication start, open a new connection */
    if (*pu1TacAuthenPktType == TAC_AUTHEN_PKT_START)
    {
        i4Result = tacOpenConnection ((*ppTacAuthenSession)->u4TacServer,
                                      &((*ppTacAuthenSession)->u4ConnectionId));
        if (i4Result != TAC_CLNT_RES_OK)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Connection open failed\n");
            tacAuthenReleaseSession (*ppTacAuthenSession);
            return (i4Result);
        }
    }
    return TAC_CLNT_RES_OK;
}

/*****************************************************************************
Function Name: tacAuthenValidateInput()
Description  : To validate the authentication input given by application
Input(s)     : pTacAuthenInput
                    The information required for authenticating a user
               pTacAuthenSession
                    The authentication session information
               pu1PktType
                    This gives info that, the packet that need to be 
                    constructed of authentication start or authentication 
                    continue. The validation would be carried out based on this.
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_APP_INFO
*****************************************************************************/
INT4
tacAuthenValidateInput (tTacAuthenInput * pTacAuthenInput,
                        tTacAuthenSessionInfo * pTacAuthenSession,
                        UINT1 u1PktType)
{
    tTacAuthenUserPAP  *pUserPAP;
    tTacAuthenUserCHAP *pUserCHAP;
    tTacAuthenUserMSCHAP *pUserMSCHAP;
    tTacAuthenUserARAP *pUserARAP;
    tTacAppInfo        *pTacApp;

   /*---------------------------------------------------------------*
    * Validate the application information given by the application *
    * If Task name is not given return error. Task name is a MUST   *
    * in all requests. This, together with request id, is used to   *
    * identify a session                                            *
    *---------------------------------------------------------------*/
    pTacApp = &(pTacAuthenInput->AppInfo);
    if (pTacApp->TaskId == 0)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Application task name missing\n");
        return (TAC_CLNT_ERROR_APP_INFO);
    }

    if (((pTacApp->TaskId == 0) ||
         (pTacApp->QueueId == 0) ||
         (pTacApp->u4AppEvent) == 0) && ((pTacApp->fpAppCallBackFn) == NULL))
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Invalid application resource info.\n");
        return (TAC_CLNT_ERROR_APP_INFO);
    }

    /* Validate for Authentication continue input */
    if (u1PktType == TAC_AUTHEN_PKT_CONT)
    {
        /* Check the session state for continue input */
        if (((pTacAuthenSession->u1SessionState) != TAC_AUTHEN_GETUSER_RCVD) &&
            ((pTacAuthenSession->u1SessionState) != TAC_AUTHEN_GETDATA_RCVD) &&
            ((pTacAuthenSession->u1SessionState) != TAC_AUTHEN_GETPASS_RCVD))
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Unsolicited continue input\n");
            return (TAC_CLNT_ERROR_INPUT);
        }

        /* If session state is O.K, check for presence of input for continue */
        if ((STRLEN (pTacAuthenInput->au1UserMessage) == 0) &&
            (pTacAuthenInput->u1Action) != TAC_AUTHEN_TERMINATE)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "No data available for continue request\n");
            return (TAC_CLNT_ERROR_INPUT);
        }
    }

    /* Validate for authentication start input */
    if (u1PktType == TAC_AUTHEN_PKT_START)
    {
        /* Validate privilege level */
        if ((INT1) (pTacAuthenInput->u1PrivilegeLevel) < TAC_PRIV_LVL_MIN ||
            (pTacAuthenInput->u1PrivilegeLevel) > TAC_PRIV_LVL_MAX)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Invalid privilege level\n");
            return (TAC_CLNT_ERROR_INPUT);
        }

        /* Validate authentication service */
        switch (pTacAuthenInput->u1Service)
        {
            case TAC_AUTHEN_SVC_NONE:
            case TAC_AUTHEN_SVC_LOGIN:
            case TAC_AUTHEN_SVC_ENABLE:
            case TAC_AUTHEN_SVC_PPP:
            case TAC_AUTHEN_SVC_ARAP:
            case TAC_AUTHEN_SVC_PT:
            case TAC_AUTHEN_SVC_RCMD:
            case TAC_AUTHEN_SVC_X25:
            case TAC_AUTHEN_SVC_NASI:
            case TAC_AUTHEN_SVC_FWPROXY:
                break;
            default:
                TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                         "Invalid authentication service\n");
                return (TAC_CLNT_ERROR_INPUT);
        }

        /* Validate authentication actions and related inputs */
        switch (pTacAuthenInput->u1Action)
        {
            case TAC_AUTHEN_LOGIN:
               /*-----------------------------------------------------------
                * For login, if service is set to TAC_AUTHEN_SVC_ENABLE,   *
                * the action is assumed as "enable" request. All other     *
                * inputs are ignored. No further input validation required *
                * for enable login request                                 * 
                *----------------------------------------------------------*/
                if ((pTacAuthenInput->u1Service) == TAC_AUTHEN_SVC_ENABLE)
                {
                    break;
                }
                switch (pTacAuthenInput->u1AuthenType)
                {
                    case TAC_AUTHEN_TYPE_ASCII:
                       /*----------------------------------------------------
                        * No validation is required . Even the user name is *
                        * optional for ASCII login                          *
                        *---------------------------------------------------*/
                        break;
                    case TAC_AUTHEN_TYPE_PAP:
                        pUserPAP = &(pTacAuthenInput->UserInfo.UserPAP);
                        if ((STRLEN (pTacAuthenInput->au1UserName) == 0) ||
                            (STRLEN (pUserPAP->au1Password) == 0))
                        {
                            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                     "Insufficient input for PAP login\n");
                            return (TAC_CLNT_ERROR_INPUT);
                        }
                        break;
                    case TAC_AUTHEN_TYPE_CHAP:
                        pUserCHAP = &(pTacAuthenInput->UserInfo.UserCHAP);
                        if ((STRLEN (pTacAuthenInput->au1UserName) == 0) ||
                            (STRLEN (pUserCHAP->au1Challenge) == 0) ||
                            (STRLEN (pUserCHAP->au1Response) !=
                             TAC_CHAP_RESPONSE_LEN))
                        {
                            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                     "Insufficient input for CHAP login\n");
                            return (TAC_CLNT_ERROR_INPUT);
                        }
                        break;
                    case TAC_AUTHEN_TYPE_MSCHAP:
                        pUserMSCHAP = &(pTacAuthenInput->UserInfo.UserMSCHAP);
                        if ((STRLEN (pTacAuthenInput->au1UserName) == 0) ||
                            (STRLEN (pUserMSCHAP->au1Challenge) == 0) ||
                            (STRLEN (pUserMSCHAP->au1Response) !=
                             TAC_MSCHAP_RESPONSE_LEN))
                        {
                            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                     "Insufficient input for MSCHAP login\n");
                            return (TAC_CLNT_ERROR_INPUT);
                        }
                        break;
                    case TAC_AUTHEN_TYPE_ARAP:
                        pUserARAP = &(pTacAuthenInput->UserInfo.UserARAP);
                        if ((STRLEN (pTacAuthenInput->au1UserName) == 0) ||
                            (STRLEN (pUserARAP->au1ChallengeToRemote) !=
                             TAC_ARAP_CHALLENGE_LEN) ||
                            (STRLEN (pUserARAP->au1ChallengeFromRemote) !=
                             TAC_ARAP_CHALLENGE_LEN) ||
                            (STRLEN (pUserARAP->au1ResponseFromRemote) !=
                             TAC_ARAP_RESPONSE_LEN))
                        {
                            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                     "Insufficient input for ARAP login\n");
                            return (TAC_CLNT_ERROR_INPUT);
                        }
                        break;
                    default:
                        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                 "Invalid authentication type for Login action\n");
                        return (TAC_CLNT_ERROR_INPUT);
                }                /* pTacAuthenInput->u1AuthenType (for login ) */
                break;
            case TAC_AUTHEN_CHPASS:
                /* For change password, service should not be set to "enable" */
                if ((pTacAuthenInput->u1Service) == TAC_AUTHEN_SVC_ENABLE)
                {
                    TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                             "Invalid service for change password action\n");
                    return (TAC_CLNT_ERROR_INPUT);
                }
                switch (pTacAuthenInput->u1AuthenType)
                {
                    case TAC_AUTHEN_TYPE_ASCII:
                        /* No validation is needed. Even user name is optional */
                        break;
                    case TAC_AUTHEN_TYPE_ARAP:
                       /*-----------------------------------------------------
                        * As of now the format to send ARAP password         *
                        * infomation is not known. So return error. In future*
                        * if this is supported, validation should be done for*
                        * old and new passwords                              *
                        *----------------------------------------------------*/
                        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "ARAP change "
                                 "password is not supported as format is not "
                                 "known\n");
                        return (TAC_CLNT_ERROR_INPUT);
                    case TAC_AUTHEN_TYPE_PAP:
                    case TAC_AUTHEN_TYPE_CHAP:
                    case TAC_AUTHEN_TYPE_MSCHAP:
                    default:
                        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Invalid "
                                 "authentication type for change password "
                                 "action\n");
                        return (TAC_CLNT_ERROR_INPUT);
                }                /* pTacAuthenInput->u1AuthenType (for chpass ) */
                break;
            case TAC_AUTHEN_SENDAUTH:
                /* For send auth, service should not be set to "enable" */
                if ((pTacAuthenInput->u1Service) == TAC_AUTHEN_SVC_ENABLE)
                {
                    TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                             "Invalid service for send auth. action\n");
                    return (TAC_CLNT_ERROR_INPUT);
                }
                switch (pTacAuthenInput->u1AuthenType)
                {
                    case TAC_AUTHEN_TYPE_PAP:
                        if (STRLEN (pTacAuthenInput->au1UserName) == 0)
                        {
                            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                     "Insufficient input for PAP send auth\n");
                            return (TAC_CLNT_ERROR_INPUT);
                        }
                        break;
                    case TAC_AUTHEN_TYPE_CHAP:
                        pUserCHAP = &(pTacAuthenInput->UserInfo.UserCHAP);
                        if ((STRLEN (pTacAuthenInput->au1UserName) == 0) ||
                            (STRLEN (pUserCHAP->au1Challenge) == 0))
                        {
                            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                     "Insufficient in put for CHAP send auth\n");
                            return (TAC_CLNT_ERROR_INPUT);
                        }
                        break;
                    case TAC_AUTHEN_TYPE_MSCHAP:
                        pUserMSCHAP = &(pTacAuthenInput->UserInfo.UserMSCHAP);
                        if ((STRLEN (pTacAuthenInput->au1UserName) == 0) ||
                            (STRLEN (pUserMSCHAP->au1Challenge) == 0))
                        {
                            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                     "Insufficient in put for MSCHAP send auth\n");
                            return (TAC_CLNT_ERROR_INPUT);
                        }
                        break;
                    case TAC_AUTHEN_TYPE_ASCII:
                    case TAC_AUTHEN_TYPE_ARAP:
                    default:
                        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                                 "Invalid authentication type for send auth action\n");
                        return (TAC_CLNT_ERROR_INPUT);
                }                /* pTacAuthenInput->u1AuthenType (for send auth) */
                break;
            default:
                TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                         "Invalid authentication action\n");
                return (TAC_CLNT_ERROR_INPUT);
        }                        /* switch (pTacAuthenInput->u1Action) */
    }                            /* u1PktType == TAC_AUTHEN_PKT_START */

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenCreateSession()
Description  : Creates a session info data structure for an authentication 
               session if this is a new session. If session is already 
               available, the existing session is given as out put
Input(s)     : pTacAuthenInput
                    The authentication request input given by application
               u4ActiveServer
                    The index of the TACACS+ server to be contacted
Output(s)    : ppTacAuthenSession
                    The session created or already available
               pu1PktType
                    The packet type. If session is newly created, this will 
                    indicate START other wise this will indicate CONTINUE
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_TBL_FULL
****************************************************************************/
INT4
tacAuthenCreateSession (tTacAuthenInput * pTacAuthenInput,
                        UINT4 u4ActiveServer,
                        tTacAuthenSessionInfo ** ppTacAuthenSession,
                        UINT1 *pu1PktType)
{
    tTacAuthenSessionInfo *pSession = NULL;
    UINT1              *pu1MemBlock = NULL;
    UINT1               u1Index = 0;;

    /* Check if session already exists, using request identifier */
    for (u1Index = 0; ((u1Index < TAC_MAX_AUTHEN_SESSION_LIMIT) &&
                       (u1Index < TAC_AUTHEN_NO_OF_CURRENT_SESSIONS));
         u1Index++)
    {
        pSession = TAC_AUTHEN_SESSION_TABLE[u1Index];
        if ((pTacAuthenInput->u4AppReqId) == (pSession->AuthenInput.u4AppReqId))
        {
            /*------------------------------------------------------------*
             * If application request Id matches, if the application task *
             * name. This is done, because, two application may send the  *
             * same application requeset id                               *
             *------------------------------------------------------------*/
            if (pTacAuthenInput->AppInfo.TaskId ==
                pSession->AuthenInput.AppInfo.TaskId)
            {
                /* Session already exists */
                break;
            }
        }
    }

    if (u1Index >= TAC_AUTHEN_NO_OF_CURRENT_SESSIONS)
    {
        /* New session */
        if (TAC_AUTHEN_NO_OF_CURRENT_SESSIONS >= TAC_MAX_AUTHEN_SESSION_LIMIT)
        {
            return (TAC_CLNT_ERROR_TBL_FULL);
        }

        TAC_ALLOCATE_MEM_BLOCK (TAC_AUTHEN_SESSION_POOL_ID, pu1MemBlock);
        if (pu1MemBlock == NULL)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unable to allocate block "
                     "from Authentication session pool\n");
            return (TAC_CLNT_ERROR_TBL_FULL);
        }
        pSession = (tTacAuthenSessionInfo *) (VOID *) pu1MemBlock;
        *pu1PktType = TAC_AUTHEN_PKT_START;
        /* Initialize session */
        MEMSET (pSession, 0, sizeof (tTacAuthenSessionInfo));

        /* Copy authentication input */
        pSession->AuthenInput = *pTacAuthenInput;

        /* Copy the TACACS+ server index */
        pSession->u4TacServer = u4ActiveServer;

        /* Generate session identifier */
        tacGenerateSessionId (&(pSession->u4SessionIdentifier));
        *ppTacAuthenSession = pSession;

        /* Update the authentication session table */
        TAC_AUTHEN_SESSION_TABLE[TAC_AUTHEN_NO_OF_CURRENT_SESSIONS] = pSession;
        TAC_AUTHEN_NO_OF_CURRENT_SESSIONS++;
        /*Start the timer */
        TAC_STOP_TIMER (TAC_TIMER_LIST_ID, &TAC_SESSION_TIMER);
        if (TAC_START_TIMER (TAC_TIMER_LIST_ID, &TAC_SESSION_TIMER,
                             TAC_SESSION_TIMEOUT) != TMR_SUCCESS)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Timer start failed\n");
        }
    }
    else
    {
        /*-------------------------------------------------------------------
         * For authentication continue request, update only action and      *
         * the user message. Other already existing inputs should not be    *
         * disturbed                                                        *
         *------------------------------------------------------------------*/
        *ppTacAuthenSession = pSession;
        *pu1PktType = TAC_AUTHEN_PKT_CONT;
        pSession->AuthenInput.u1Action = pTacAuthenInput->u1Action;
        MEMSET (pSession->AuthenInput.au1UserMessage, 0, TAC_USER_MSG_LEN);
        MEMSET (pSession->AuthenInput.au1UserData, 0, TAC_USER_DATA_LEN);
        STRNCPY (pSession->AuthenInput.au1UserMessage,
                pTacAuthenInput->au1UserMessage, STRLEN(pTacAuthenInput->au1UserMessage));
	pSession->AuthenInput.au1UserMessage[STRLEN(pTacAuthenInput->au1UserMessage)] = '\0';
        STRNCPY (pSession->AuthenInput.au1UserData,
                pTacAuthenInput->au1UserData, STRLEN(pTacAuthenInput->au1UserData));
	pSession->AuthenInput.au1UserData[STRLEN(pTacAuthenInput->au1UserData)] ='\0';
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenReleaseSession()
Description  : To release an authentication session
Input(s)     : pTacAuthenSession
                    The session to be released
Output(s)    :   None
Return(s)    :   TAC_CLNT_RES_OK
                 TAC_CLNT_ERROR_MEM
*****************************************************************************/
INT4
tacAuthenReleaseSession (tTacAuthenSessionInfo * pTacAuthenSession)
{
    tTacAuthenSessionInfo *pSession = NULL;
    INT4                i4Result;
    UINT1               u1Index;

    /* To play safe ! */
    if (pTacAuthenSession == NULL)
    {
        return (TAC_CLNT_RES_OK);
    }

    /* Check if session already exists, using request identifier */
    for (u1Index = 0; ((u1Index < TAC_MAX_AUTHEN_SESSION_LIMIT) &&
                       (u1Index < TAC_AUTHEN_NO_OF_CURRENT_SESSIONS));
         u1Index++)
    {
        pSession = TAC_AUTHEN_SESSION_TABLE[u1Index];
        if (pTacAuthenSession == pSession)
        {
            /* Session found */
            break;
        }
    }

    if (u1Index >= TAC_AUTHEN_NO_OF_CURRENT_SESSIONS)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Session not found in the session table \n");
        return (TAC_CLNT_ERROR_GEN);
    }

    /* Release session */
    i4Result = TAC_RELEASE_MEM_BLOCK (TAC_AUTHEN_SESSION_POOL_ID,
                                      (UINT1 *) pSession);
    if (i4Result != MEM_SUCCESS)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Memory release failed \n");
        return (TAC_CLNT_ERROR_MEM);
    }

    if (u1Index < TAC_MAX_AUTHEN_SESSION_LIMIT)
        /* Reset the session table */
        TAC_AUTHEN_SESSION_TABLE[u1Index] = (tTacAuthenSessionInfo *) NULL;

    /*----------------------------------------------------------------------
     * Adjust the session table so that, there is no hole in the session   *
     * table. If the session released is the last entry in the table, then *
     * no adjustment is needed. If the entry is not last entry, then there *
     * will be hole in the table. Take the last entry of the table and fill*
     * in the hole.                                                        *
     *-------------------------------------------------------------------- */
    if ((u1Index != (TAC_AUTHEN_NO_OF_CURRENT_SESSIONS - 1)) &&
        (u1Index < TAC_MAX_AUTHEN_SESSION_LIMIT) &&
        ((TAC_AUTHEN_NO_OF_CURRENT_SESSIONS - 1) <
         TAC_MAX_AUTHEN_SESSION_LIMIT))
    {
        TAC_AUTHEN_SESSION_TABLE[u1Index] =
            TAC_AUTHEN_SESSION_TABLE[TAC_AUTHEN_NO_OF_CURRENT_SESSIONS - 1];
        TAC_AUTHEN_SESSION_TABLE[TAC_AUTHEN_NO_OF_CURRENT_SESSIONS - 1] =
            (tTacAuthenSessionInfo *) NULL;
    }

    TAC_AUTHEN_NO_OF_CURRENT_SESSIONS--;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenConstructPacket()
Description  : To construct an authentication packet
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
               u1TacAuthPktType
                    To indicate START or CONTINUE
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start or 
                    continue body
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_SEQ_NO_WRAPPED
******************************************************************************/
INT4
tacAuthenConstructPacket (tTacAuthenSessionInfo * pTacAuthenSession,
                          tTacAuthenInput * pTacAuthenInput,
                          UINT1 u1TacAuthPktType, UINT1 *pu1TacPacket)
{
    INT4                i4Result;

    if (u1TacAuthPktType == TAC_AUTHEN_PKT_START)
    {
        /* Construct authentication start body. Always returns SUCCESS */
        tacAuthenConstructStartBody (pTacAuthenSession, pTacAuthenInput,
                                     pu1TacPacket);
    }
    else
    {
        /* Construct authentication continue body. Always returns SUCCESS */
        tacAuthenConstructContinueBody (pTacAuthenSession, pTacAuthenInput,
                                        pu1TacPacket);
    }

    /* Construct Header */
    i4Result = tacAuthenConstructHeader (pTacAuthenSession, pTacAuthenInput,
                                         pu1TacPacket);
    if (i4Result == (INT4) TAC_CLNT_ERROR_SEQ_NO_WRAPPED)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Packet header construction failed \n");
    }

    return (i4Result);
}

/*****************************************************************************
Function Name: tacAuthenConstructStartBody()
Description  : To construct authentication start body
Input(s)     : pTacAuthenInput
                    Authentication input given by applicatio
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenConstructStartBody (tTacAuthenSessionInfo * pTacAuthenSession,
                             tTacAuthenInput * pTacAuthenInput,
                             UINT1 *pu1TacPacket)
{

    /* Fill the common fields of authentication start body */
    tacAuthenConstructGenStartBody (pTacAuthenSession, pTacAuthenInput,
                                    pu1TacPacket);

    /* Fill data field for inbound authentication */
    if ((pTacAuthenInput->u1Action) == TAC_AUTHEN_LOGIN)
    {
        tacAuthenInboundLogin (pTacAuthenSession, pTacAuthenInput,
                               pu1TacPacket);
    }

    /* Fill data field for outbound authentication */
    else if ((pTacAuthenInput->u1Action) == TAC_AUTHEN_SENDAUTH)
    {
        tacAuthenOutboundLogin (pTacAuthenSession, pTacAuthenInput,
                                pu1TacPacket);
    }

    /* Fill Data field for Change password */
    else if ((pTacAuthenInput->u1Action) == TAC_AUTHEN_CHPASS)
    {
        tacAuthenChangePassword (pTacAuthenSession, pTacAuthenInput,
                                 pu1TacPacket);
    }

    /* Update the log information for no. of start packets sent */
    (TAC_CLNT_LOG.u4AuthenStarts)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenConstructGenStartBody()
Description  : This function fill the common fields of the authentication start
               body. Only the data field will be different for different
               operations. All other fields are directly taken from already
               validated input
Input(s)     : pTacAuthenInput
                    Authentication input given by applicatio
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for common fields of 
                    authentication start body
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenConstructGenStartBody (tTacAuthenSessionInfo * pTacAuthenSession,
                                tTacAuthenInput * pTacAuthenInput,
                                UINT1 *pu1TacPacket)
{
    UINT1               u1UserLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT2               u2Offset;

    UNUSED_PARAM (pTacAuthenSession);

    *(pu1TacPacket + TAC_AUTHEN_START_ACTION_OFFSET) =
        pTacAuthenInput->u1Action;
    *(pu1TacPacket + TAC_AUTHEN_START_PL_OFFSET) =
        pTacAuthenInput->u1PrivilegeLevel;
    *(pu1TacPacket + TAC_AUTHEN_START_AUTHEN_TYPE_OFFSET) =
        pTacAuthenInput->u1AuthenType;
    *(pu1TacPacket + TAC_AUTHEN_START_SVC_OFFSET) = pTacAuthenInput->u1Service;

    /*-------------------------------------------------------------------- 
     * User length might be zero for ASCII login, Enable login and ASCII *
     * change password                                                   *
     *-------------------------------------------------------------------*/
    u1UserLen = (UINT1) STRLEN (pTacAuthenInput->au1UserName);
    *(pu1TacPacket + TAC_AUTHEN_START_USER_LEN_OFFSET) = u1UserLen;

    u1PortLen = (UINT1) STRLEN (pTacAuthenInput->au1Port);
    *(pu1TacPacket + TAC_AUTHEN_START_PORT_LEN_OFFSET) = u1PortLen;

    u1RemAddrLen = (UINT1) STRLEN (pTacAuthenInput->au1RemAddr);
    *(pu1TacPacket + TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET) = u1RemAddrLen;

    /* Data length will be appropriately filled in the specific functions */

    /* Fill the User name */
    MEMCPY ((pu1TacPacket + TAC_AUTHEN_START_USER_OFFSET),
            pTacAuthenInput->au1UserName, u1UserLen);

    /* Fill the Port name */
    u2Offset = (UINT2) (TAC_AUTHEN_START_USER_OFFSET + u1UserLen);
    MEMCPY ((pu1TacPacket + u2Offset), pTacAuthenInput->au1Port, u1PortLen);

    /* Fill the Remote Address value  */
    u2Offset = (UINT2) (TAC_AUTHEN_START_USER_OFFSET + u1UserLen + u1PortLen);
    MEMCPY ((pu1TacPacket + u2Offset), pTacAuthenInput->au1RemAddr,
            u1RemAddrLen);
    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenInboundLogin()
Description  : To construct TACACS+ start body for inbound authentication 
               functionalities
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenInboundLogin (tTacAuthenSessionInfo * pTacAuthenSession,
                       tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{

    if ((pTacAuthenInput->u1Service) == TAC_AUTHEN_SVC_ENABLE)
    {
        /* For Enable login */
        tacAuthenInboundEnable (pTacAuthenSession, pTacAuthenInput,
                                pu1TacPacket);
        return (TAC_CLNT_RES_OK);
    }
    switch (pTacAuthenInput->u1AuthenType)
    {
        case TAC_AUTHEN_TYPE_ASCII:
            /* For ASCII login */
            tacAuthenInboundASCII (pTacAuthenSession, pTacAuthenInput,
                                   pu1TacPacket);
            break;
        case TAC_AUTHEN_TYPE_PAP:
            /* For PAP login */
            tacAuthenInboundPAP (pTacAuthenSession, pTacAuthenInput,
                                 pu1TacPacket);
            break;
        case TAC_AUTHEN_TYPE_CHAP:
            /* For CHAP login */
            tacAuthenInboundCHAP (pTacAuthenSession, pTacAuthenInput,
                                  pu1TacPacket);
            break;
        case TAC_AUTHEN_TYPE_MSCHAP:
            /* For MSCHAP login */
            tacAuthenInboundMSCHAP (pTacAuthenSession, pTacAuthenInput,
                                    pu1TacPacket);
            break;
        case TAC_AUTHEN_TYPE_ARAP:
            /* For ARAP login */
            tacAuthenInboundARAP (pTacAuthenSession, pTacAuthenInput,
                                  pu1TacPacket);
            break;
        default:
            /*---------------------------------------------------------------
             *  This part will never be executed as the inputs were already * 
             *  validated                                                   *
             *--------------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unknown Error! \n");
            break;
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenInboundASCII()
Description  : To construct authentication start body for inbound ASCII login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenInboundASCII (tTacAuthenSessionInfo * pTacAuthenSession,
                       tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UNUSED_PARAM (pTacAuthenSession);
    UNUSED_PARAM (pTacAuthenInput);

    /* Data field is not used for inbound ASCII login */
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = 0;

    /* Update the log information for no. of inbound ASCII logins sent */
    (TAC_CLNT_LOG.u4IbASCIILoginReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenInboundPAP()
Description  : To construct authentication start body for inbound PAP login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenInboundPAP (tTacAuthenSessionInfo * pTacAuthenSession,
                     tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UINT1               u1UserLen;
    UINT1               u1DataLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT2               u2Offset;

    UNUSED_PARAM (pTacAuthenSession);

    u1UserLen = *(pu1TacPacket + TAC_AUTHEN_START_USER_LEN_OFFSET);
    u1PortLen = *(pu1TacPacket + TAC_AUTHEN_START_PORT_LEN_OFFSET);
    u1RemAddrLen = *(pu1TacPacket + TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET);

    u1DataLen = (UINT1) STRLEN (pTacAuthenInput->UserInfo.UserPAP.au1Password);
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = u1DataLen;

    /* Fill the PAP password in the data field */
    u2Offset = (UINT2) (TAC_AUTHEN_START_USER_OFFSET + u1UserLen + u1PortLen +
                        u1RemAddrLen);
    MEMCPY ((pu1TacPacket + u2Offset),
            pTacAuthenInput->UserInfo.UserPAP.au1Password, u1DataLen);

    /* Update the log information for no. of inbound PAP logins sent */
    (TAC_CLNT_LOG.u4IbPAPLoginReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenInboundCHAP()
Description  : To construct authentication start body for inbound CHAP login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenInboundCHAP (tTacAuthenSessionInfo * pTacAuthenSession,
                      tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UINT1               u1UserLen;
    UINT1               u1ChalLen;
    UINT1               u1DataLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT2               u2Offset;

    UNUSED_PARAM (pTacAuthenSession);

    u1UserLen = *(pu1TacPacket + TAC_AUTHEN_START_USER_LEN_OFFSET);
    u1PortLen = *(pu1TacPacket + TAC_AUTHEN_START_PORT_LEN_OFFSET);
    u1RemAddrLen = *(pu1TacPacket + TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET);

    u1ChalLen = (UINT1)
        STRLEN (pTacAuthenInput->UserInfo.UserCHAP.au1Challenge);
    u1DataLen = (UINT1) (u1ChalLen + TAC_CHAP_RESPONSE_LEN + 1);
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = u1DataLen;

    /* Fill PPP identifier */
    u2Offset = (UINT2) (TAC_AUTHEN_START_USER_OFFSET + u1UserLen + u1PortLen +
                        u1RemAddrLen);
    *(pu1TacPacket + u2Offset) =
        pTacAuthenInput->UserInfo.UserCHAP.u1Identifier;

    /* Fill CHAP challenge */
    u2Offset += 1;
    MEMCPY ((pu1TacPacket + u2Offset),
            pTacAuthenInput->UserInfo.UserCHAP.au1Challenge, u1ChalLen);

    /* Fill CHAP response */
    u2Offset += u1ChalLen;
    MEMCPY ((pu1TacPacket + u2Offset),
            pTacAuthenInput->UserInfo.UserCHAP.au1Response,
            TAC_CHAP_RESPONSE_LEN);

    /* Update the log information for no. of inbound CHAP logins sent */
    (TAC_CLNT_LOG.u4IbCHAPLoginReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenInboundMSCHAP()
Description  : To construct authentication start body for inbound MSCHAP login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenInboundMSCHAP (tTacAuthenSessionInfo * pTacAuthenSession,
                        tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UINT1               u1UserLen;
    UINT1               u1ChalLen;
    UINT1               u1DataLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT2               u2Offset;

    UNUSED_PARAM (pTacAuthenSession);

    u1UserLen = *(pu1TacPacket + TAC_AUTHEN_START_USER_LEN_OFFSET);
    u1PortLen = *(pu1TacPacket + TAC_AUTHEN_START_PORT_LEN_OFFSET);
    u1RemAddrLen = *(pu1TacPacket + TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET);

    u1ChalLen =
        (UINT1) (STRLEN (pTacAuthenInput->UserInfo.UserMSCHAP.au1Challenge));
    u1DataLen = (UINT1) (u1ChalLen + TAC_MSCHAP_RESPONSE_LEN + 1);
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = u1DataLen;

    /* Fill PPP identifier */
    u2Offset = (UINT2) (TAC_AUTHEN_START_USER_OFFSET + u1UserLen + u1PortLen +
                        u1RemAddrLen);
    *(pu1TacPacket + u2Offset) =
        pTacAuthenInput->UserInfo.UserMSCHAP.u1Identifier;

    /* Fill MSCHAP challenge */
    u2Offset += 1;
    MEMCPY ((pu1TacPacket + u2Offset),
            pTacAuthenInput->UserInfo.UserMSCHAP.au1Challenge, u1ChalLen);

    /* Fill MSCHAP response */
    u2Offset += u1ChalLen;
    MEMCPY ((pu1TacPacket + u2Offset),
            pTacAuthenInput->UserInfo.UserMSCHAP.au1Response,
            TAC_MSCHAP_RESPONSE_LEN);

    /* Update the log information for no. of inbound MSCHAP logins sent */
    (TAC_CLNT_LOG.u4IbMSCHAPLoginReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenInboundARAP()
Description  : To construct authentication start body for inbound ARAP login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenInboundARAP (tTacAuthenSessionInfo * pTacAuthenSession,
                      tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UINT1               u1UserLen;
    UINT1               u1DataLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT2               u2Offset;

    UNUSED_PARAM (pTacAuthenSession);

    u1UserLen = *(pu1TacPacket + TAC_AUTHEN_START_USER_LEN_OFFSET);
    u1PortLen = *(pu1TacPacket + TAC_AUTHEN_START_PORT_LEN_OFFSET);
    u1RemAddrLen = *(pu1TacPacket + TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET);

    u1DataLen = TAC_ARAP_CHALLENGE_LEN + TAC_ARAP_CHALLENGE_LEN +
        TAC_ARAP_RESPONSE_LEN;
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = u1DataLen;

    /* Fill ARAP Challenge to Remote */
    u2Offset = (UINT2) (TAC_AUTHEN_START_USER_OFFSET + u1UserLen + u1PortLen +
                        u1RemAddrLen);
    MEMCPY ((pu1TacPacket + u2Offset),
            pTacAuthenInput->UserInfo.UserARAP.au1ChallengeToRemote,
            TAC_ARAP_CHALLENGE_LEN);

    /* Fill ARAP Challenge from remote */
    u2Offset += TAC_ARAP_CHALLENGE_LEN;
    MEMCPY ((pu1TacPacket + u2Offset),
            pTacAuthenInput->UserInfo.UserARAP.au1ChallengeFromRemote,
            TAC_ARAP_CHALLENGE_LEN);
    /* Fill ARAP Response from remote */
    u2Offset += TAC_ARAP_RESPONSE_LEN;
    MEMCPY ((pu1TacPacket + u2Offset),
            pTacAuthenInput->UserInfo.UserARAP.au1ResponseFromRemote,
            TAC_ARAP_RESPONSE_LEN);

    /* Update the log information for no. of inbound ARAP logins sent */
    (TAC_CLNT_LOG.u4IbARAPLoginReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenInboundEnable()
Description  : To construct authentication start body for inbound Enable login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenInboundEnable (tTacAuthenSessionInfo * pTacAuthenSession,
                        tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UNUSED_PARAM (pTacAuthenSession);
    UNUSED_PARAM (pTacAuthenInput);

    /* Set the authentication type as 0, as authenticatio type is not used */
    *(pu1TacPacket + TAC_AUTHEN_START_AUTHEN_TYPE_OFFSET) = 0;

    /* Data field is not used for Enable login */
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = 0;

    /* Update the log information for no. of inbound Enable logins sent */
    (TAC_CLNT_LOG.u4EnableReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenOutboundLogin()
Description  : To construct TACACS+ start body for Outbound authentication 
               functionalities
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenOutboundLogin (tTacAuthenSessionInfo * pTacAuthenSession,
                        tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{

    switch (pTacAuthenInput->u1AuthenType)
    {
        case TAC_AUTHEN_TYPE_PAP:
            /* For PAP send auth */
            tacAuthenOutboundPAP (pTacAuthenSession, pTacAuthenInput,
                                  pu1TacPacket);
            break;
        case TAC_AUTHEN_TYPE_CHAP:
            /* For CHAP send auth */
            tacAuthenOutboundCHAP (pTacAuthenSession, pTacAuthenInput,
                                   pu1TacPacket);
            break;
        case TAC_AUTHEN_TYPE_MSCHAP:
            /* For MSCHAP send auth */
            tacAuthenOutboundMSCHAP (pTacAuthenSession, pTacAuthenInput,
                                     pu1TacPacket);
            break;
        default:
            /*---------------------------------------------------------------
             *  This part will never be executed as the inputs were already * 
             *  validated                                                   *
             *--------------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unknown Error! \n");
            break;
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenOutboundPAP()
Description  : To construct authentication start body for Outbound PAP login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenOutboundPAP (tTacAuthenSessionInfo * pTacAuthenSession,
                      tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{

    UNUSED_PARAM (pTacAuthenSession);
    UNUSED_PARAM (pTacAuthenInput);

    /* Data field is not used for out bound PAP login */
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = 0;

    /* Update the log information for no. of outbound PAP logins sent */
    (TAC_CLNT_LOG.u4ObPAPReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenOutboundCHAP()
Description  : To construct authentication start body for Outbound CHAP login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenOutboundCHAP (tTacAuthenSessionInfo * pTacAuthenSession,
                       tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UINT1               u1UserLen;
    UINT1               u1ChalLen;
    UINT1               u1DataLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT2               u2Offset;

    UNUSED_PARAM (pTacAuthenSession);

    u1UserLen = *(pu1TacPacket + TAC_AUTHEN_START_USER_LEN_OFFSET);
    u1PortLen = *(pu1TacPacket + TAC_AUTHEN_START_PORT_LEN_OFFSET);
    u1RemAddrLen = *(pu1TacPacket + TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET);

    u1ChalLen = (UINT1)
        STRLEN (pTacAuthenInput->UserInfo.UserCHAP.au1Challenge);
    u1DataLen = (UINT1) (u1ChalLen + 1);
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = u1DataLen;

    /* Fill PPP identifier */
    u2Offset = (UINT2) (TAC_AUTHEN_START_USER_OFFSET + u1UserLen + u1PortLen +
                        u1RemAddrLen);
    *(pu1TacPacket + u2Offset) =
        pTacAuthenInput->UserInfo.UserCHAP.u1Identifier;

    /* Fill CHAP challenge */
    u2Offset += 1;
    MEMCPY ((pu1TacPacket + u2Offset),
            (pTacAuthenInput->UserInfo.UserCHAP.au1Challenge), u1ChalLen);

    /* Update the log information for no. of outbound CHAP logins sent */
    (TAC_CLNT_LOG.u4ObCHAPReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenOutboundMSCHAP()
Description  : To construct authentication start body for Outbound MSCHAP login
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenOutboundMSCHAP (tTacAuthenSessionInfo * pTacAuthenSession,
                         tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UINT1               u1UserLen;
    UINT1               u1ChalLen;
    UINT1               u1DataLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT2               u2Offset;

    UNUSED_PARAM (pTacAuthenSession);

    u1UserLen = *(pu1TacPacket + TAC_AUTHEN_START_USER_LEN_OFFSET);
    u1PortLen = *(pu1TacPacket + TAC_AUTHEN_START_PORT_LEN_OFFSET);
    u1RemAddrLen = *(pu1TacPacket + TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET);

    u1ChalLen = (UINT1)
        STRLEN (pTacAuthenInput->UserInfo.UserMSCHAP.au1Challenge);
    u1DataLen = (UINT1) (u1ChalLen + 1);
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = u1DataLen;

    /* Fill PPP identifier */
    u2Offset = (UINT2) (TAC_AUTHEN_START_USER_OFFSET + u1UserLen + u1PortLen +
                        u1RemAddrLen);
    *(pu1TacPacket + u2Offset) =
        pTacAuthenInput->UserInfo.UserMSCHAP.u1Identifier;

    /* Fill MSCHAP challenge */
    u2Offset += 1;
    MEMCPY ((pu1TacPacket + u2Offset),
            (pTacAuthenInput->UserInfo.UserMSCHAP.au1Challenge), u1ChalLen);

    /* Update the log information for no. of outbound MSCHAP logins sent */
    (TAC_CLNT_LOG.u4ObMSCHAPReqs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenChangePassword()
Description  : To construct TACACS+ start body for Change Password  
               functionalities
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenChangePassword (tTacAuthenSessionInfo * pTacAuthenSession,
                         tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{

    switch (pTacAuthenInput->u1AuthenType)
    {
        case TAC_AUTHEN_TYPE_ASCII:
            /* For ASCII change password */
            tacAuthenChgPwdASCII (pTacAuthenSession, pTacAuthenInput,
                                  pu1TacPacket);
            break;
        case TAC_AUTHEN_TYPE_ARAP:
            /* For ARAP change password */
            tacAuthenChgPwdARAP (pTacAuthenSession, pTacAuthenInput,
                                 pu1TacPacket);
            break;
        default:
            /*---------------------------------------------------------------
             *  This part will never be executed as the inputs were already * 
             *  validated                                                   *
             *--------------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unknown Error! \n");
            break;
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenChgPwdASCII()
Description  : To construct authentication start body for ASCII change password
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenChgPwdASCII (tTacAuthenSessionInfo * pTacAuthenSession,
                      tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UNUSED_PARAM (pTacAuthenSession);
    UNUSED_PARAM (pTacAuthenInput);

    /* Data field is not used for ASCII change password */
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = 0;

    /* Update the log information for no. of ASCII change passwords sent */
    (TAC_CLNT_LOG.u4ChgPwdASCIIs)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenChgPwdARAP()
Description  : To construct authentication start body for ARAP change password
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication start body
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAuthenChgPwdARAP (tTacAuthenSessionInfo * pTacAuthenSession,
                     tTacAuthenInput * pTacAuthenInput, UINT1 *pu1TacPacket)
{
    UNUSED_PARAM (pTacAuthenSession);
    UNUSED_PARAM (pTacAuthenInput);

    /*---------------------------------------------------------------------
     * The format of sending old password and new passwords is not known. *
     * So this is not supported. This may be enhanced in future           * 
     *--------------------------------------------------------------------*/

    /* Data field is not used for ASCII change password */
    *(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET) = 0;

    /* Update the log information for no. of ARAP change passwords sent */
    (TAC_CLNT_LOG.u4ChgPwdARAPs)++;

    return (TAC_CLNT_RES_OK);
}

/******************************************************************************
Function Name: tacAuthenConstructContinueBody()
Description  : To construct authentication continue body
Input(s)     : pTacAuthenInput
                    Authentication input given by application for continue 
                    packet
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication continue body
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenConstructContinueBody (tTacAuthenSessionInfo * pTacAuthenSession,
                                tTacAuthenInput * pTacAuthenInput,
                                UINT1 *pu1TacPacket)
{
    UINT2               u2UserMsgLen;

    /* Fill common fields of continue body */

    /*------------------------------------------------------------------------*
     * Fill the user message. This is user name, if continue is response for  *
     * GETUSER. This will be password, if continue is response for GETDATA or *
     * GETPASS                                                                *
     *------------------------------------------------------------------------*/
    u2UserMsgLen = (UINT1) STRLEN (pTacAuthenInput->au1UserMessage);
    MEMCPY ((pu1TacPacket + TAC_AUTHEN_CONT_USER_MSG_OFFSET),
            pTacAuthenInput->au1UserMessage, u2UserMsgLen);

    /* Fill the user message length in network byte order */
    u2UserMsgLen = OSIX_HTONS (u2UserMsgLen);
    *((UINT2 *) (VOID *) (pu1TacPacket + TAC_AUTHEN_CONT_USER_MSG_LEN_OFFSET)) =
        u2UserMsgLen;

    *((UINT2 *) (VOID *) (pu1TacPacket + TAC_AUTHEN_CONT_DATA_LEN_OFFSET)) = 0;
    *(pu1TacPacket + TAC_AUTHEN_CONT_FLAGS_OFFSET) = 0;

    if ((pTacAuthenInput->u1Action) == TAC_AUTHEN_TERMINATE)
    {
        tacAuthenContTerminateSession (pTacAuthenSession, pTacAuthenInput,
                                       pu1TacPacket);

        /* Update the log for no. of terminations sent */
        (TAC_CLNT_LOG.u4AuthenAborts)++;
        (TAC_CLNT_LOG.u4AuthenConts)++;
        return (TAC_CLNT_RES_OK);
    }

    switch (pTacAuthenSession->u1SessionState)
    {
        case TAC_AUTHEN_GETUSER_RCVD:
        case TAC_AUTHEN_GETPASS_RCVD:
        case TAC_AUTHEN_GETDATA_RCVD:
            /*---------------------------------------------------------------
             * No action is needed for these, as these operations do not use*
             * data field. But this code given, so that in future, if any   *
             * data is planned for these, this area could be changed        *
             *--------------------------------------------------------------*/
            break;
        default:
            /*---------------------------------------------------------------
             *  This part will never be executed as the inputs were already * 
             *  validated                                                   *
             *--------------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unknown Error! \n");
            break;
    }

    /* Update the log information for no. of continue packets sent */
    (TAC_CLNT_LOG.u4AuthenConts)++;

    return (TAC_CLNT_RES_OK);
}

/******************************************************************************
Function Name: tacAuthenContTerminateSession()
Description  : To construct authentication continue packet to terminate an 
               authentication session
Input(s)     : pTacAuthenInput
                    Authentication input given by application to terminate the 
                    session
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for authentication continue body 
                    for user name
Return(s)    : TAC_CLNT_RES_OK
*******************************************************************************/
INT4
tacAuthenContTerminateSession (tTacAuthenSessionInfo * pTacAuthenSession,
                               tTacAuthenInput * pTacAuthenInput,
                               UINT1 *pu1TacPacket)
{
    UINT2               u2DataLen;
    UINT2               u2MsgLen;

    UNUSED_PARAM (pTacAuthenSession);

    /* Set abort flag */
    *(pu1TacPacket + TAC_AUTHEN_CONT_FLAGS_OFFSET) = TAC_CONTINUE_FLAG_ABORT;

    /* Fill Data if anything is present. This could be reason for abort */
    u2MsgLen =
        *((UINT2 *) (VOID *) (pu1TacPacket +
                              TAC_AUTHEN_CONT_USER_MSG_LEN_OFFSET));
    u2MsgLen = OSIX_NTOHS (u2MsgLen);

    u2DataLen = (UINT1) STRLEN (pTacAuthenInput->au1UserData);
    MEMCPY ((pu1TacPacket + TAC_AUTHEN_CONT_USER_MSG_OFFSET + u2MsgLen),
            pTacAuthenInput->au1UserData, u2DataLen);

    u2DataLen = OSIX_HTONS (u2DataLen);
    *((UINT2 *) (VOID *) (pu1TacPacket + TAC_AUTHEN_CONT_DATA_LEN_OFFSET)) =
        u2DataLen;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenContstructHeader()
Description  : To construct the header of the TACACS+ packet
Input(s)     : pTacAuthenInput
                    Authentication input given by application
               pTacAuthenSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet with header filled
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_SEQ_NO_WRAPPED
******************************************************************************/
INT4
tacAuthenConstructHeader (tTacAuthenSessionInfo * pTacAuthenSession,
                          tTacAuthenInput * pTacAuthenInput,
                          UINT1 *pu1TacPacket)
{
    tTacServerInfo     *pTacServer;
    UINT4               u4SessionId;
    UINT4               u4PktLen;
    UINT2               u2Temp;

    /* Sequence no. wrapped around. Session must be terminated and restarted */
    if (pTacAuthenSession->u1SequenceNoRcvd == TAC_MAX_SEQNO)
    {
        TAC_DBG (TAC_TRACE_INFO, TAC_MODULE, "Sequence no. wrapped around! \n");
        return (TAC_CLNT_ERROR_SEQ_NO_WRAPPED);

    }

    *(pu1TacPacket + TAC_PKT_TYPE_OFFSET) = TAC_AUTHEN;
    *(pu1TacPacket + TAC_PKT_SEQ_NO_OFFSET) = (UINT1)
        (pTacAuthenSession->u1SequenceNoRcvd + 1);

    pTacServer = &(TAC_SERVER_TABLE[pTacAuthenSession->u4TacServer - 1]);
    *(pu1TacPacket + TAC_PKT_FLAGS_OFFSET) =
        TAC_UNENC_FLAG | pTacServer->u1SCFlag;

    /* Fill session identifier */
    u4SessionId = OSIX_HTONL (pTacAuthenSession->u4SessionIdentifier);
    *((UINT4 *) (VOID *) (pu1TacPacket + TAC_PKT_SESSION_ID_OFFSET)) =
        u4SessionId;

    *(pu1TacPacket + TAC_PKT_VERSION_OFFSET) = TAC_MAJOR_VER << 4;

    switch (pTacAuthenInput->u1Action)
    {
        case TAC_AUTHEN_LOGIN:
            if ((pTacAuthenInput->u1Service) == TAC_AUTHEN_SVC_ENABLE)
            {
                *(pu1TacPacket + TAC_PKT_VERSION_OFFSET) |=
                    TAC_MINOR_VER_DEFAULT;
                break;
            }
            switch (pTacAuthenInput->u1AuthenType)
            {
                case TAC_AUTHEN_TYPE_ASCII:
                    *(pu1TacPacket + TAC_PKT_VERSION_OFFSET) |=
                        TAC_MINOR_VER_DEFAULT;
                    break;
                default:
                    *(pu1TacPacket + TAC_PKT_VERSION_OFFSET) |=
                        TAC_MINOR_VER_ONE;
                    break;

            }
            break;
        case TAC_AUTHEN_SENDAUTH:
            *(pu1TacPacket + TAC_PKT_VERSION_OFFSET) |= TAC_MINOR_VER_ONE;
            break;
        default:
            *(pu1TacPacket + TAC_PKT_VERSION_OFFSET) |= TAC_MINOR_VER_DEFAULT;
            break;
    }

    if ((pTacAuthenSession->u1SessionState) == TAC_AUTHEN_GETUSER_RCVD ||
        (pTacAuthenSession->u1SessionState) == TAC_AUTHEN_GETPASS_RCVD ||
        (pTacAuthenSession->u1SessionState) == TAC_AUTHEN_GETDATA_RCVD)
    {
        /* Calculate continure body length */
        u2Temp =
            *((UINT2 *) (VOID *) (pu1TacPacket +
                                  TAC_AUTHEN_CONT_USER_MSG_LEN_OFFSET));
        u4PktLen = (UINT4) OSIX_NTOHS (u2Temp);
        u2Temp = *((UINT2 *) (VOID *)
                   (pu1TacPacket + TAC_AUTHEN_CONT_DATA_LEN_OFFSET));
        u2Temp = OSIX_NTOHS (u2Temp);
        u4PktLen = u4PktLen + (UINT4) u2Temp + TAC_AUTHEN_CONT_LEN_FIXED_FIELDS;

    }
    else
    {
        /* Calculate start body length */
        u4PktLen = (UINT4) (*(pu1TacPacket + TAC_AUTHEN_START_USER_LEN_OFFSET));
        u4PktLen +=
            (UINT4) (*(pu1TacPacket + TAC_AUTHEN_START_PORT_LEN_OFFSET));
        u4PktLen +=
            (UINT4) (*(pu1TacPacket + TAC_AUTHEN_START_REM_ADDR_LEN_OFFSET));
        u4PktLen +=
            (UINT4) (*(pu1TacPacket + TAC_AUTHEN_START_DATA_LEN_OFFSET));
        u4PktLen += TAC_AUTHEN_START_LEN_FIXED_FIELDS;
    }

    /* Fill the packet length */
    u4PktLen = OSIX_HTONL (u4PktLen);
    *((UINT4 *) (VOID *) (pu1TacPacket + TAC_PKT_LEN_OFFSET)) = u4PktLen;

    pTacAuthenSession->u1MinorVersionSent =
        (*(pu1TacPacket + TAC_PKT_VERSION_OFFSET)) & TAC_MINOR_VER_MASK;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenReceivedPkt()
Description  : To poll all connections to check if there is any packet        
Input(s)     : pu1NoOfPkts
                  The no. of packets already available in the receive buffer.
Output(s)    : pu1NoOfPkts
                  The no. packets received. + initial no (given as input)
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenReceivedPkt (UINT1 *pu1PktCnt)
{
    tTacAuthenSessionInfo *pTacAuthenSession;
    tTacServerInfo     *pTacServer;
    tTacPkt            *pTacTempPkt;
    tTacAuthenOutput    TacAuthenOutput;
    UINT1               u1NoOfPkts;
    UINT1               u1Index;

    u1NoOfPkts = *pu1PktCnt;

    for (u1Index = 0; ((u1Index < TAC_MAX_AUTHEN_SESSION_LIMIT) &&
                       (u1Index < TAC_AUTHEN_NO_OF_CURRENT_SESSIONS));
         u1Index++)
    {
        pTacAuthenSession = TAC_AUTHEN_SESSION_TABLE[u1Index];
        /* To play safe! */
        if (pTacAuthenSession == NULL)
        {
            continue;
        }

        pTacServer = &(TAC_SERVER_TABLE[pTacAuthenSession->u4TacServer - 1]);
        if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
        {
            pTacTempPkt =
                &(TAC_SC_TEMP_PKT[pTacAuthenSession->u4TacServer - 1]);
        }
        else
        {
            pTacTempPkt = &(pTacAuthenSession->TacTempPkt);
        }

        if (FD_ISSET (pTacAuthenSession->u4ConnectionId, &gReadSockFds))
        {
            tacReadPacket (pTacAuthenSession->u4ConnectionId, pTacTempPkt,
                           &u1NoOfPkts);
            if (u1NoOfPkts > 0)
            {
                SelAddFd (pTacAuthenSession->u4ConnectionId,
                          (VOID *) TacNotifyTask);
            }
            else
            {
                MEMSET (&TacAuthenOutput, 0, sizeof (tTacAuthenOutput));

                TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                         "Authen Connenction closed by server. Failed!\n");
                TacAuthenOutput.u4AppReqId =
                    pTacAuthenSession->AuthenInput.u4AppReqId;
                TacAuthenOutput.i4AuthenStatus = TAC_CLNT_RES_NOT_OK;
                TacAuthenOutput.i4ErrorValue = TAC_CLNT_ERROR_CONN_CLOSE;
                /* Send out put to application */
                tacSendInfoToApp (TAC_AUTHEN,
                                  &(pTacAuthenSession->AuthenInput.AppInfo),
                                  &TacAuthenOutput);
                tacCloseConnection (pTacAuthenSession->u4TacServer,
                                    pTacAuthenSession->u4ConnectionId);
                tacAuthenReleaseSession (pTacAuthenSession);
            }

        }
        if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
        {
                /*--------------------------------------------------------*
                 * If single connection mode, all sessions will be having *
                 * same connection identifier. So polling once is enough  *
                 *--------------------------------------------------------*/
            break;
        }
    }                            /* for all sessions */
    /* select */

    *pu1PktCnt = u1NoOfPkts;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenReply()
Description  : To process the received authentication reply packet
Input(s)     : pu1TacAuthenReplyPkt
                    Received authentication reply packet
Output(s)    : pTacSession
                    The authentication session corresponding to this 
                    packet
               pTacAuthenOutput
                    The out put of the authentication reply.
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_RCVD_PKT
*****************************************************************************/
INT4
tacProcessAuthenReply (UINT1 *pu1TacAuthenReplyPkt,
                       tTacAuthenSessionInfo ** ppTacSession,
                       tTacAuthenOutput * pTacAuthenOutput)
{
    tTacAuthenSessionInfo *pTacAuthenSession = NULL;
    tTacServerInfo     *pTacServer;
    UINT4               u4SessionId;
    INT4                i4Result;
    UINT1               u1Index;

    /* Find the session corresponding to this packet */
    u4SessionId =
        *((UINT4 *) (VOID *) (pu1TacAuthenReplyPkt +
                              TAC_PKT_SESSION_ID_OFFSET));
    u4SessionId = OSIX_NTOHL (u4SessionId);

    for (u1Index = 0; ((u1Index < TAC_MAX_AUTHEN_SESSION_LIMIT) &&
                       (u1Index < TAC_AUTHEN_NO_OF_CURRENT_SESSIONS));
         u1Index++)
    {
        pTacAuthenSession = TAC_AUTHEN_SESSION_TABLE[u1Index];
        if (u4SessionId == (pTacAuthenSession->u4SessionIdentifier))
        {
            /* Session found */
            break;
        }
    }

    if (u1Index >= TAC_AUTHEN_NO_OF_CURRENT_SESSIONS)
    {
        /* Session not found */
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "No corresponding session available for the received packet \n");
        *ppTacSession = NULL;
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    *ppTacSession = pTacAuthenSession;

    /* Decrypt the packet */
    pTacServer = &(TAC_SERVER_TABLE[pTacAuthenSession->u4TacServer - 1]);
    tacEncryptDecrypt (pu1TacAuthenReplyPkt, pTacServer->au1SecretKey);

    TAC_DBG_PKT (TAC_TRACE_PKT_RX, (CONST UINT1 *) TAC_MODULE,
                 pu1TacAuthenReplyPkt);

    /* Validate the packet */
    i4Result = tacValidateAuthenReply (pu1TacAuthenReplyPkt, pTacAuthenSession);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Authen. reply pkt validation failed \n");

        /* Update the log. for no. malformed pkts received */
        (TAC_CLNT_LOG.u4MalformedPkts)++;

        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Set the sequence no. in the authentication data structure */
    pTacAuthenSession->u1SequenceNoRcvd =
        *(pu1TacAuthenReplyPkt + TAC_PKT_SEQ_NO_OFFSET);

    /* Fill the common fields of the authentication out put data */
    tacAuthenOutputFillCmn (pu1TacAuthenReplyPkt, pTacAuthenSession,
                            pTacAuthenOutput);

    switch (pTacAuthenOutput->i4AuthenStatus)
    {
        case TAC_AUTHEN_STATUS_PASS:
            tacProcessAuthenPass (pu1TacAuthenReplyPkt, pTacAuthenSession,
                                  pTacAuthenOutput);
            break;
        case TAC_AUTHEN_STATUS_FAIL:
            tacProcessAuthenFail (pu1TacAuthenReplyPkt, pTacAuthenSession,
                                  pTacAuthenOutput);
            break;
        case TAC_AUTHEN_STATUS_GETDATA:
            tacProcessAuthenGetData (pu1TacAuthenReplyPkt, pTacAuthenSession,
                                     pTacAuthenOutput);
            break;
        case TAC_AUTHEN_STATUS_GETUSER:
            tacProcessAuthenGetUser (pu1TacAuthenReplyPkt, pTacAuthenSession,
                                     pTacAuthenOutput);
            break;
        case TAC_AUTHEN_STATUS_GETPASS:
            tacProcessAuthenGetPass (pu1TacAuthenReplyPkt, pTacAuthenSession,
                                     pTacAuthenOutput);
            break;
        case TAC_AUTHEN_STATUS_RESTART:
            tacProcessAuthenRestart (pu1TacAuthenReplyPkt, pTacAuthenSession,
                                     pTacAuthenOutput);
            break;
        case TAC_AUTHEN_STATUS_ERROR:
            tacProcessAuthenError (pu1TacAuthenReplyPkt, pTacAuthenSession,
                                   pTacAuthenOutput);
            break;
        case TAC_AUTHEN_STATUS_FOLLOW:
            tacProcessAuthenFollow (pu1TacAuthenReplyPkt, pTacAuthenSession,
                                    pTacAuthenOutput);
            break;
        default:
            /*-----------------------------------------------------------*
             * This part will never be executed, as the packet is already*
             * validated for correct authenticaion reply status          *
             *-----------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authentication status rcvd \n");
            return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacValidateAuthenReply()
Description  : To validate received authentication  reply
Input(s)     : pu1TacAuthenReplyPkt
                  Received authentication reply 
               packetpTacAuthenSession
                  The authentication session corresponding to this 
                  authentication reply packet
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_RCVD_PKT
******************************************************************************/
INT4
tacValidateAuthenReply (UINT1 *pu1TacAuthenReplyPkt,
                        tTacAuthenSessionInfo * pTacAuthenSession)
{
    tTacServerInfo     *pTacServer;
    UINT4               u4PktLen;
    UINT4               u4BodyLen;
    INT4                i4Result;
    UINT2               u2CompLen;

    i4Result = tacValidateCmnHdrFields (pu1TacAuthenReplyPkt);

    if (i4Result != TAC_CLNT_RES_OK)
    {
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /*-----------------------------------------------------------------*
     * Check for single connect flag. This need to be checked only for *
     * first two packets.                                              *
     *---------------------------------------------------------------- */
    pTacServer = &(TAC_SERVER_TABLE[pTacAuthenSession->u4TacServer - 1]);
    if ((pTacServer->u1SCVerifyCount) < TAC_SC_VERIFY_COUNT_MAX)
    {
        if (*(pu1TacAuthenReplyPkt + TAC_PKT_FLAGS_OFFSET) !=
            (pTacServer->u1SCFlag))
        {
            if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
            {
                /*----------------------------------------------------*
                 * Client operating in Single connect mode and server *
                 * suggests to operate in multiple connection mode. So*
                 * switch back to multiple connection mode            *
                 *----------------------------------------------------*/
                pTacServer->u1SCFlag = 0;
                pTacServer->i4SingleConnId = 0;
            }
            else
            {
                /*----------------------------------------------------*
                 * Client operating in multiple connection mode and   *
                 * server suggests to operate in single connection    *
                 * mode. So switch back to single connection mode     *
                 *----------------------------------------------------*/
                pTacServer->u1SCFlag = TAC_SINGLE_CONNECT_FLAG;
                pTacServer->i4SingleConnId =
                    (INT4) (pTacAuthenSession->u4ConnectionId);
            }
            TAC_DBG (TAC_TRACE_INFO, TAC_MODULE, "Connection mode changed as "
                     "per server suggestion\n");
        }
        pTacServer->u1SCVerifyCount++;
    }

    /* Validate minor version */
    if (((*(pu1TacAuthenReplyPkt + TAC_PKT_VERSION_OFFSET)) &
         TAC_MINOR_VER_MASK) != (pTacAuthenSession->u1MinorVersionSent))
    {

        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Minor version not matching with server!\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /*--------------------------------------------------------------------*
     * Validate sequence no. received. The received sequence no. should be*
     * greater than previous received sequence no. by 2.                  *
     *--------------------------------------------------------------------*/
    if (*(pu1TacAuthenReplyPkt + TAC_PKT_SEQ_NO_OFFSET) !=
        ((pTacAuthenSession->u1SequenceNoRcvd) + 2))
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Invalid sequence no received\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Validate length */
    u4PktLen =
        *((UINT4 *) (VOID *) (pu1TacAuthenReplyPkt + TAC_PKT_LEN_OFFSET));
    u4PktLen = OSIX_NTOHL (u4PktLen);
    if (u4PktLen == 0)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Serve unable to understand the packet sent\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    u2CompLen = *((UINT2 *) (VOID *) (pu1TacAuthenReplyPkt +
                                      TAC_AUTHEN_REPLY_SRV_MSG_LEN_OFFSET));
    u2CompLen = OSIX_NTOHS (u2CompLen);
    u4BodyLen = (UINT4) u2CompLen;
    u2CompLen = *((UINT2 *) (VOID *) (pu1TacAuthenReplyPkt +
                                      TAC_AUTHEN_REPLY_DATA_LEN_OFFSET));
    u2CompLen = OSIX_NTOHS (u2CompLen);
    u4BodyLen += ((UINT4) u2CompLen + TAC_AUTHEN_REPLY_LEN_FIXED_FIELDS);

    if (u4PktLen != u4BodyLen)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Length in the header and total reply body length are notequal\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Validate authentication status received */
    switch (*(pu1TacAuthenReplyPkt + TAC_AUTHEN_REPLY_STATUS_OFFSET))
    {
        case TAC_AUTHEN_STATUS_PASS:
        case TAC_AUTHEN_STATUS_FAIL:
        case TAC_AUTHEN_STATUS_GETDATA:
        case TAC_AUTHEN_STATUS_GETUSER:
        case TAC_AUTHEN_STATUS_GETPASS:
        case TAC_AUTHEN_STATUS_RESTART:
        case TAC_AUTHEN_STATUS_ERROR:
        case TAC_AUTHEN_STATUS_FOLLOW:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authentication status in reply packet\n");
            return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenOutputFillCmn()
Description  : To fill the common fields of authentication out put
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenOutputFillCmn (UINT1 *pu1TacAuthenReplyPkt,
                        tTacAuthenSessionInfo * pTacAuthenSession,
                        tTacAuthenOutput * pTacAuthenOutput)
{
    UINT2               u2Len;

    /*-----------------------------------------------------------------* 
     * Copy the Application request identifier, so that the application* 
     * will match this response with the request it made and will do   *
     * further processing                                              *
     *-----------------------------------------------------------------*/
    pTacAuthenOutput->u4AppReqId = pTacAuthenSession->AuthenInput.u4AppReqId;

    /* Set the authentication status */
    pTacAuthenOutput->i4AuthenStatus =
        *(pu1TacAuthenReplyPkt + TAC_AUTHEN_REPLY_STATUS_OFFSET);

    /* If server message is available, send the message to application */
    u2Len = *((UINT2 *) (VOID *) (pu1TacAuthenReplyPkt +
                                  TAC_AUTHEN_REPLY_SRV_MSG_LEN_OFFSET));
    u2Len = OSIX_NTOHS (u2Len);
    if (u2Len != 0)
    {
        if (u2Len > TAC_SRV_MSG_LEN)
        {
            u2Len = TAC_SRV_MSG_LEN;
        }
        MEMCPY (pTacAuthenOutput->au1ServerMessage,
                (pu1TacAuthenReplyPkt + TAC_AUTHEN_REPLY_SRV_MSG_OFFSET),
                u2Len);
    }

    /* Set No echo flag */
    pTacAuthenOutput->u1NoEchoFlag =
        (*(pu1TacAuthenReplyPkt + TAC_AUTHEN_REPLY_FLAGS_OFFSET) &
         TAC_REPLY_FLAG_NOECHO);

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenOutputFillData()
Description  : To fill the data field of authentication out put
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenOutputFillData (UINT1 *pu1TacAuthenReplyPkt,
                         tTacAuthenSessionInfo * pTacAuthenSession,
                         tTacAuthenOutput * pTacAuthenOutput)
{
    UINT2               u2DataLen;
    UINT2               u2MsgLen;
    UINT4               u4Offset;

    UNUSED_PARAM (pTacAuthenSession);

    u2DataLen = *((UINT2 *) (VOID *) (pu1TacAuthenReplyPkt +
                                      TAC_AUTHEN_REPLY_DATA_LEN_OFFSET));
    u2DataLen = OSIX_NTOHS (u2DataLen);
    if (u2DataLen != 0)
    {
        if (u2DataLen > TAC_SRV_DATA_LEN)
        {
            u2DataLen = TAC_SRV_DATA_LEN;
        }

        /*------------------------------------------------------------*
         * To calculate the data offset, first get the server message *
         * length. The add this with server message offset            *
         *------------------------------------------------------------*/
        u2MsgLen = *((UINT2 *) (VOID *) (pu1TacAuthenReplyPkt +
                                         TAC_AUTHEN_REPLY_SRV_MSG_LEN_OFFSET));
        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        u4Offset = (UINT4) (u2MsgLen + TAC_AUTHEN_REPLY_SRV_MSG_OFFSET);

        MEMCPY (pTacAuthenOutput->au1ServerData,
                (pu1TacAuthenReplyPkt + u4Offset), u2DataLen);

        pTacAuthenOutput->au1ServerData[TAC_SRV_DATA_LEN - 1] = '\0';
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenPass()
Description  : To process the authentication reply for authentication pass
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthenPass (UINT1 *pu1TacAuthenReplyPkt,
                      tTacAuthenSessionInfo * pTacAuthenSession,
                      tTacAuthenOutput * pTacAuthenOutput)
{
    /* Set session state in session data structure */
    pTacAuthenSession->u1SessionState = TAC_AUTHEN_PASS_RCVD;

    tacAuthenOutputFillData (pu1TacAuthenReplyPkt, pTacAuthenSession,
                             pTacAuthenOutput);

    /* Update the log for no. of authen. pass received */
    (TAC_CLNT_LOG.u4AuthenPass)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenFail()
Description  : To process the authentication reply for authentication fail
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthenFail (UINT1 *pu1TacAuthenReplyPkt,
                      tTacAuthenSessionInfo * pTacAuthenSession,
                      tTacAuthenOutput * pTacAuthenOutput)
{
    UNUSED_PARAM (pu1TacAuthenReplyPkt);
    UNUSED_PARAM (pTacAuthenOutput);

    /* Set session state in session data structure */
    pTacAuthenSession->u1SessionState = TAC_AUTHEN_FAIL_RCVD;

    /* Update the log for no. of authen. fails received */
    (TAC_CLNT_LOG.u4AuthenFails)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenGetData()
Description  : To process the authentication reply for authentication Get data
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthenGetData (UINT1 *pu1TacAuthenReplyPkt,
                         tTacAuthenSessionInfo * pTacAuthenSession,
                         tTacAuthenOutput * pTacAuthenOutput)
{
    UNUSED_PARAM (pu1TacAuthenReplyPkt);
    UNUSED_PARAM (pTacAuthenOutput);

    /* Set session state in session data structure */
    pTacAuthenSession->u1SessionState = TAC_AUTHEN_GETDATA_RCVD;

    /* Update the log for no. of authen. get data received */
    (TAC_CLNT_LOG.u4AuthenGetData)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenGetUser()
Description  : To process the authentication reply for authentication Get user
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthenGetUser (UINT1 *pu1TacAuthenReplyPkt,
                         tTacAuthenSessionInfo * pTacAuthenSession,
                         tTacAuthenOutput * pTacAuthenOutput)
{
    UNUSED_PARAM (pu1TacAuthenReplyPkt);
    UNUSED_PARAM (pTacAuthenOutput);

    /* Set session state in session data structure */
    pTacAuthenSession->u1SessionState = TAC_AUTHEN_GETUSER_RCVD;

    /* Update the log for no. of authen. get user received */
    (TAC_CLNT_LOG.u4AuthenGetUsers)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenGetPass()
Description  : To process the authentication reply for authentication Get pass
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthenGetPass (UINT1 *pu1TacAuthenReplyPkt,
                         tTacAuthenSessionInfo * pTacAuthenSession,
                         tTacAuthenOutput * pTacAuthenOutput)
{
    UNUSED_PARAM (pu1TacAuthenReplyPkt);
    UNUSED_PARAM (pTacAuthenOutput);

    /* Set session state in session data structure */
    pTacAuthenSession->u1SessionState = TAC_AUTHEN_GETPASS_RCVD;

    /* Update the log for no. of authen. get pass received */
    (TAC_CLNT_LOG.u4AuthenGetPass)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenFollow()
Description  : To process the authentication reply for authentication follow
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthenFollow (UINT1 *pu1TacAuthenReplyPkt,
                        tTacAuthenSessionInfo * pTacAuthenSession,
                        tTacAuthenOutput * pTacAuthenOutput)
{
    /* Set session state in session data structure */
    pTacAuthenSession->u1SessionState = TAC_AUTHEN_FOLLOW_RCVD;

    /*----------------------------------------------------------------*
     * If server data is available, send the message to application   *
     * This data will be list of alternate servers suggested by the   *
     * current server for futher authentication. The client does not  *
     * use the alternate servers by itself. The alternate server list *
     * will be given to application. It is upto the application to    *
     * treat this authentication failure or restart the authentication*
     * with any of the servers mentioned in the list                  *
     *----------------------------------------------------------------*/
    tacAuthenOutputFillData (pu1TacAuthenReplyPkt, pTacAuthenSession,
                             pTacAuthenOutput);

    /* Update the log for no. of authen. follow received */
    (TAC_CLNT_LOG.u4AuthenFollows)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenRestart()
Description  : To process the authentication reply for authentication restart
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthenRestart (UINT1 *pu1TacAuthenReplyPkt,
                         tTacAuthenSessionInfo * pTacAuthenSession,
                         tTacAuthenOutput * pTacAuthenOutput)
{

    /* Set session state in session data structure */
    pTacAuthenSession->u1SessionState = TAC_AUTHEN_RESTART_RCVD;

    /*----------------------------------------------------------------*
     * If server data is available, send the message to application   *
     * This data will be list of authentication types supported for   *
     * this user. This is for future PPP extentions. So give the list *
     * of authentication types available to application. Let the      *
     * application handle this authentication types. OR this could be *
     * treated as authentication failure.                             *
     *----------------------------------------------------------------*/
    tacAuthenOutputFillData (pu1TacAuthenReplyPkt, pTacAuthenSession,
                             pTacAuthenOutput);

    /* Update the log for no. of authen. restart received */
    (TAC_CLNT_LOG.u4AuthenRestarts)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAuthenError()
Description  : To process the authentication reply for authentication error
Input(s)     : pu1TacAuthenReplyPkt
                  Validated authentication reply packet
               pTacAuthenSession
                  The authentication session corresponding to this packet
Output(s)    : pTacAuthenOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAuthenError (UINT1 *pu1TacAuthenReplyPkt,
                       tTacAuthenSessionInfo * pTacAuthenSession,
                       tTacAuthenOutput * pTacAuthenOutput)
{

    /* Set session state in session data structure */
    pTacAuthenSession->u1SessionState = TAC_AUTHEN_ERROR_RCVD;

    /*---------------------------------------------------------------*
     * Send the data to application. So that this would be printed on*
     * administrative console                                        *
     *---------------------------------------------------------------*/
    tacAuthenOutputFillData (pu1TacAuthenReplyPkt, pTacAuthenSession,
                             pTacAuthenOutput);

    /* Update the log for no. of authen. error received */
    (TAC_CLNT_LOG.u4AuthenErrors)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAuthenChkSessionTimeout()
Description  : To check if any session has timed out. The time out sessions are
               released
Input(s)     : None
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAuthenChkSessionTimeout ()
{
    tTacAuthenOutput   *pTacAuthenOutput = NULL;
    tTacAuthenSessionInfo *pTacAuthenSession;
    UINT1               u1Index;

    if (TAC_ALLOCATE_MEM_BLOCK (TAC_AUTHEN_OUTPUT_POOL_ID,
                                pTacAuthenOutput) == NULL)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "tacAuthenChkSessionTimeout: Memory Allocate failed"
                 " from Authentication output pool\n");
        return (TAC_CLNT_RES_NOT_OK);
    }
    for (u1Index = 0; ((u1Index < TAC_MAX_AUTHEN_SESSION_LIMIT) &&
                       (u1Index < TAC_AUTHEN_NO_OF_CURRENT_SESSIONS));
         u1Index++)
    {
        pTacAuthenSession = TAC_AUTHEN_SESSION_TABLE[u1Index];
        (pTacAuthenSession->u4Timer)--;
        if ((INT4) (pTacAuthenSession->u4Timer) == 0)
        {
            /* Update the log. for no. of session time outs */
            (TAC_CLNT_LOG.u4AuthenSessionTimeouts)++;

            MEMSET (pTacAuthenOutput, 0, sizeof (tTacAuthenOutput));

            pTacAuthenOutput->u4AppReqId =
                pTacAuthenSession->AuthenInput.u4AppReqId;
            pTacAuthenOutput->i4AuthenStatus = TAC_AUTHEN_STATUS_TIMEOUT;
            pTacAuthenOutput->i4ErrorValue = TAC_CLNT_ERROR_SESSION_TIMEOUT;
            tacSendInfoToApp (TAC_AUTHEN,
                              &(pTacAuthenSession->AuthenInput.AppInfo),
                              pTacAuthenOutput);

            if ((pTacAuthenSession->u1SessionState == TAC_AUTHEN_GETDATA_RCVD)
                || (pTacAuthenSession->u1SessionState ==
                    TAC_AUTHEN_GETUSER_RCVD)
                || (pTacAuthenSession->u1SessionState ==
                    TAC_AUTHEN_GETPASS_RCVD))
            {
                TAC_DBG (TAC_TRACE_INFO, TAC_MODULE, "Application did not "
                         "respond for continue. Aborting session..\n");
                /*-----------------------------------------------------------*
                 * This is the application time out. i.e. The application did*
                 * not give input within TAC_APP_TIMEOUT. So the session     *
                 * should be aborted and session should be released. Session *
                 * release will be taken care in tacacsAuthentication, after *
                 * sending Abort to server                                   *
                 *-----------------------------------------------------------*/
                pTacAuthenSession->AuthenInput.u1Action = TAC_AUTHEN_TERMINATE;
                STRNCPY (pTacAuthenSession->AuthenInput.au1UserData,
                        "Application did not respond for continue...", STRLEN("Application did not respond for continue..."));
		pTacAuthenSession->AuthenInput.au1UserData[STRLEN("Application did not respond for continue...")] = '\0';
                tacacsAuthentication (&(pTacAuthenSession->AuthenInput));
                continue;
            }

            /* Time out has occured. Release the session */
            TAC_DBG (TAC_TRACE_INFO, TAC_MODULE,
                     "Authentication session timed out and released\n");

            tacCloseConnection (pTacAuthenSession->u4TacServer,
                                pTacAuthenSession->u4ConnectionId);
            tacAuthenReleaseSession (pTacAuthenSession);
        }
    }
    if (TAC_RELEASE_MEM_BLOCK (TAC_AUTHEN_OUTPUT_POOL_ID,
                               (UINT1 *) pTacAuthenOutput) == MEM_FAILURE)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "tacAuthenChkSessionTimeout: Memory Release failed"
                 " for Authentication Output pool\n");
        return (TAC_CLNT_RES_NOT_OK);
    }
    return (TAC_CLNT_RES_OK);
}

/******************************** End of file *********************************/
