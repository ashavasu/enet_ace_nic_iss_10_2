/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tpacct.c,v 1.9 2015/04/28 12:49:59 siva Exp $
 *
 * Description: This file will have the accouting routines
 * for TACACS
 *******************************************************************/
#define __TPACCT_C__
#include "tpinc.h"

/******************************************************************************
Function Name: TacacsAccountUser()
Description  : This module does the accounting functionality of the 
               tacacsClient. This frames TACACS+ accounting packet as per input
               given by application. 
Input(s)     : pTacAcctInput
                    input to accounting
Output(s)    : tTacAcctOuput
                    if the interface is messaging, a message is constructed 
                    with this data and sent to application to indicate error, 
                    if there is any error. If the accounting request processing
                    is successful nothing is given to applicationIf the 
                    interface is procedural no out put
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_MEM
               TAC_CLNT_ERROR_GEN
*****************************************************************************/
INT4
TacacsAccountUser (tTacAcctInput * pTacAcctInput)
{
    INT4                i4Result = 0;

    TAC_PROT_LOCK ();
    i4Result = tacacsAccounting (pTacAcctInput);
    TAC_PROT_UNLOCK ();

    return (i4Result);
}

/******************************************************************************
Function Name: tacacsAccounting()
Description  : This module does the accounting functionality of the 
               tacacsClient. This frames TACACS+ accounting packet as per input
               given by application. 
Input(s)     : pTacAcctInput
                    input to accounting
Output(s)    : tTacAcctOuput
                    if the interface is messaging, a message is constructed 
                    with this data and sent to application to indicate error, 
                    if there is any error. If the accounting request processing
                    is successful nothing is given to applicationIf the 
                    interface is procedural no out put
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_MEM
               TAC_CLNT_ERROR_GEN
*****************************************************************************/
INT4
tacacsAccounting (tTacAcctInput * pTacAcctInput)
{
    tTacClntErrorStatisticsLog TacLog;
    tTacAcctSessionInfo *pTacAcctSession = NULL;
    tTacServerInfo     *pTacServer = NULL;
    UINT4               u4ServerStatus;
    UINT4               u4ActiveServer;
    UINT4               u4ServersContacted = 0;
    INT4                i4RetVal = TAC_CLNT_RES_NOT_OK;
    INT4                i4Result;
    PRIVATE UINT1       gau1TacAcctPacket[TAC_ACCT_PKT_SIZE];
    UINT4               u4Flag = 0;

    /* Create Accounting session */
    u4ServerStatus = TAC_DESTROY;
    u4ActiveServer = (UINT4) TAC_ACTIVE_SERVER;
    if (u4ActiveServer != 0 && ((u4ActiveServer - 1) < TAC_MAX_SERVERS))
    {
        pTacServer = &(TAC_SERVER_TABLE[u4ActiveServer - 1]);

        u4ServerStatus = pTacServer->u4Status;
        if (u4ServerStatus != TAC_ACTIVE)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "TACACS+ servers not available\n");
            return (TAC_CLNT_ERROR_NO_ACTIVE_SRV);
        }
        i4Result = TacAcctEstablishSession (pTacAcctInput, u4ActiveServer,
                                            &pTacAcctSession);
        if (i4Result != TAC_CLNT_RES_OK)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Unable to establish connection\n");
            u4Flag = 1;
        }
    }
    if (u4Flag == 1 || u4ActiveServer == 0)
    {
        for (u4ActiveServer = 1; ((u4ActiveServer <= TAC_MAX_SERVERS)
                                  && (u4ServersContacted < TAC_MAX_RETRIES));
             u4ActiveServer++)
        {
            pTacServer = &(TAC_SERVER_TABLE[u4ActiveServer - 1]);
            u4ServerStatus = pTacServer->u4Status;
            if (u4ServerStatus == TAC_ACTIVE)
            {
                u4ServersContacted++;
                i4RetVal =
                    TacAcctEstablishSession (pTacAcctInput, u4ActiveServer,
                                             &pTacAcctSession);
                if (i4RetVal != TAC_CLNT_RES_OK)
                {
                    continue;
                }
                else
                {
                    break;
                }

            }
            else
            {
                continue;
            }
        }
        if (i4RetVal != TAC_CLNT_RES_OK)
        {
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "TACACS+ servers not available\n");
            return (TAC_CLNT_ERROR_NO_ACTIVE_SRV);
        }
    }
    if (pTacAcctSession == NULL)
    {
        return (TAC_CLNT_ERROR_MEM);
    }
    MEMSET (gau1TacAcctPacket, 0, TAC_ACCT_PKT_SIZE);
   /*-----------------------------------------------------------------*
    * Take a copy of the statistics log. The "sent" fields are before *
    * actually sending the packet. So if "send" fails, those counters *
    * which are incremented shuold be decremented appropriately. For  *
    * This purpose TacLog is used                                     *
    *-----------------------------------------------------------------*/
    TacLog = TAC_CLNT_LOG;

   /*-----------------------------------------------------------------
    * Construct TACACS+ accounting packet. This is expected to       *
    * return TAC_CLNT_RES_OK always                                  *
    *----------------------------------------------------------------*/
    i4Result = tacAcctConstructPacket
        (pTacAcctSession, &(pTacAcctSession->AcctInput), gau1TacAcctPacket);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Packet construction failed\n");
        tacCloseConnection (pTacAcctSession->u4TacServer,
                            pTacAcctSession->u4ConnectionId);
        tacAcctReleaseSession (pTacAcctSession);
        MEMSET (gau1TacAcctPacket, 0, TAC_ACCT_PKT_SIZE);
        return (i4Result);
    }

    TAC_DBG_PKT (TAC_TRACE_PKT_TX, (CONST UINT1 *) TAC_MODULE,
                 gau1TacAcctPacket);

    /* Encrypt the packet */
    tacEncryptDecrypt (gau1TacAcctPacket, pTacServer->au1SecretKey);

    /* Send the packet */
    i4Result =
        tacSendPacket (gau1TacAcctPacket, pTacAcctSession->u4ConnectionId);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Send packet failed\n");

        tacCloseConnection (pTacAcctSession->u4TacServer,
                            pTacAcctSession->u4ConnectionId);
        tacAcctReleaseSession (pTacAcctSession);
        MEMSET (gau1TacAcctPacket, 0, TAC_ACCT_PKT_SIZE);
        TAC_CLNT_LOG = TacLog;
        return (i4Result);
    }

    pTacAcctSession->u4Timer = pTacServer->u4Timeout;
    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: TacAcctEstablishSession()
Description  : To Establish connection with the given server. 
Input(s)     : pTacAcctInput
                    The information required for acconting a user
               ppTacAcctSession
                    The accounting session information
               u4ActiveServer 
                    This gives the index of the active server.
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_TBL_FULL
               TAC_CLNT_ERROR_APP_INFO
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_SOCK_OPEN
               TAC_CLNT_ERROR_CONN_OPEN  
*****************************************************************************/
INT4
TacAcctEstablishSession (tTacAcctInput * pTacAcctInput,
                         UINT4 u4ActiveServer,
                         tTacAcctSessionInfo ** ppTacAcctSession)
{
    INT4                i4Result = 0;

    i4Result = tacAcctCreateSession (pTacAcctInput, u4ActiveServer,
                                     ppTacAcctSession);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Accounting session creation failed\n");
        return (i4Result);
    }

    /* Validate the input given for accounting */
    i4Result = tacAcctValidateInput (&((*ppTacAcctSession)->AcctInput));
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Input validation failed\n");
        tacAcctReleaseSession (*ppTacAcctSession);
        return (i4Result);
    }

    i4Result = tacOpenConnection ((*ppTacAcctSession)->u4TacServer,
                                  &((*ppTacAcctSession)->u4ConnectionId));
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Connection open failed\n");
        tacAcctReleaseSession (*ppTacAcctSession);
        return (i4Result);
    }

    return TAC_CLNT_RES_OK;
}

/*****************************************************************************
Function Name: tacAcctValidateInput()
Description  : To validate the accounting  input given by application
Input(s)     : pTacAcctInput
                    The information required for accounting
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_INPUT
               TAC_CLNT_ERROR_APP_INFO
******************************************************************************/
INT4
tacAcctValidateInput (tTacAcctInput * pTacAcctInput)
{
    tTacAppInfo        *pTacApp;

    pTacApp = &(pTacAcctInput->AppInfo);
    if (((pTacApp->TaskId == 0) ||
         (pTacApp->QueueId == 0)) && ((pTacApp->fpAppCallBackFn) == NULL))
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Invalid application resource info.\n");
        return (TAC_CLNT_ERROR_APP_INFO);
    }

    /* Check for the presence of user name */
    if (STRLEN (pTacAcctInput->au1UserName) == 0)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "User name not present\n");
        return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate privilege level */
    if ((INT1) (pTacAcctInput->u1PrivilegeLevel) < TAC_PRIV_LVL_MIN ||
        (pTacAcctInput->u1PrivilegeLevel) > TAC_PRIV_LVL_MAX)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Invalid privilege level\n");
        return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate authentication type */
    switch (pTacAcctInput->u1AuthenType)
    {
        case TAC_AUTHEN_TYPE_ASCII:
        case TAC_AUTHEN_TYPE_PAP:
        case TAC_AUTHEN_TYPE_CHAP:
        case TAC_AUTHEN_TYPE_MSCHAP:
        case TAC_AUTHEN_TYPE_ARAP:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authentication type\n");
            return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate authentication Method */
    switch (pTacAcctInput->u1AuthenMethod)
    {
        case TAC_AUTHEN_METH_NOT_SET:
        case TAC_AUTHEN_METH_NONE:
        case TAC_AUTHEN_METH_KRB5:
        case TAC_AUTHEN_METH_LINE:
        case TAC_AUTHEN_METH_ENABLE:
        case TAC_AUTHEN_METH_LOCAL:
        case TAC_AUTHEN_METH_TACACSPLUS:
        case TAC_AUTHEN_METH_GUEST:
        case TAC_AUTHEN_METH_RADIUS:
        case TAC_AUTHEN_METH_KRB4:
        case TAC_AUTHEN_METH_RCMD:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authentication method\n");
            return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate authentication service */
    switch (pTacAcctInput->u1Service)
    {
        case TAC_AUTHEN_SVC_NONE:
        case TAC_AUTHEN_SVC_LOGIN:
        case TAC_AUTHEN_SVC_ENABLE:
        case TAC_AUTHEN_SVC_PPP:
        case TAC_AUTHEN_SVC_ARAP:
        case TAC_AUTHEN_SVC_PT:
        case TAC_AUTHEN_SVC_RCMD:
        case TAC_AUTHEN_SVC_X25:
        case TAC_AUTHEN_SVC_NASI:
        case TAC_AUTHEN_SVC_FWPROXY:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid authentication service\n");
            return (TAC_CLNT_ERROR_INPUT);
    }

    /* Validate accounting flag */
    switch (pTacAcctInput->u1AcctFlag)
    {
        case TAC_ACCT_FLAG_START:
        case TAC_ACCT_FLAG_WD:
        case TAC_ACCT_FLAG_STOP:
        case TAC_ACCT_FLAG_WD_START:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Invalid accounting flag\n");
            return (TAC_CLNT_ERROR_INPUT);
    }

    if ((pTacAcctInput->AuthorArgs.u1NoOfArgs) == 0 &&
        (pTacAcctInput->AcctArgs.u1NoOfArgs) == 0)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "No arguments is present\n");
        return (TAC_CLNT_ERROR_INPUT);
    }

    if ((pTacAcctInput->AuthorArgs.u1NoOfArgs) >= TAC_AUTHOR_MAX_AVP ||
        (pTacAcctInput->AcctArgs.u1NoOfArgs) >= TAC_ACCT_MAX_AVP)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "More than max. arguments supported\n");
        return (TAC_CLNT_ERROR_INPUT);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAcctCreateSession()
Description  : Creates a session info data structure for an accounting 
               session
Input(s)     : pTacAcctInput
                    The accounting request input given by application
               u4ActiveServer
                    The index of the TACACS+ server to be contacted
Output(s)    : ppTacAcctSession
                    The session created
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_TBL_FULL
               TAC_CLNT_ERROR_GEN
****************************************************************************/
INT4
tacAcctCreateSession (tTacAcctInput * pTacAcctInput,
                      UINT4 u4ActiveServer,
                      tTacAcctSessionInfo ** ppTacAcctSession)
{
    tTacAcctSessionInfo *pSession;
    UINT1              *pu1MemBlock = NULL;

    if (TAC_ACCT_NO_OF_CURRENT_SESSIONS >= TAC_MAX_ACCT_SESSION_LIMIT)
    {
        return (TAC_CLNT_ERROR_TBL_FULL);
    }

    /* Allocate session from accounting session table */
    TAC_ALLOCATE_MEM_BLOCK (TAC_ACCT_SESSION_POOL_ID, pu1MemBlock);
    if (pu1MemBlock == NULL)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Unable to allocate block from Accounting session pool\n");
        return (TAC_CLNT_ERROR_TBL_FULL);
    }
    pSession = (tTacAcctSessionInfo *) (VOID *) pu1MemBlock;

    /* Initialize session */
    MEMSET (pSession, 0, sizeof (tTacAcctSessionInfo));

    /* Copy accounting input */
    pSession->AcctInput = *pTacAcctInput;

    /* Copy the TACACS+ server index */
    pSession->u4TacServer = u4ActiveServer;

    /* Generate session identifier */
    tacGenerateSessionId (&(pSession->u4SessionIdentifier));
    *ppTacAcctSession = pSession;

    /* Update the accounting session table */
    TAC_ACCT_SESSION_TABLE[TAC_ACCT_NO_OF_CURRENT_SESSIONS] = pSession;
    TAC_ACCT_NO_OF_CURRENT_SESSIONS++;
    /* start the timer */
    TAC_STOP_TIMER (TAC_TIMER_LIST_ID, &TAC_SESSION_TIMER);
    if (TAC_START_TIMER (TAC_TIMER_LIST_ID, &TAC_SESSION_TIMER,
                         TAC_SESSION_TIMEOUT) != TMR_SUCCESS)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Timer start failed\n");
    }
    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAcctReleaseSession()
Description  : To release an accounting session
Input(s)     : pTacAcctSession
                    The session to be released
Output(s)    :   None
Return(s)    :   TAC_CLNT_RES_OK
                 TAC_CLNT_ERROR_MEM
*****************************************************************************/
INT4
tacAcctReleaseSession (tTacAcctSessionInfo * pTacAcctSession)
{
    tTacAcctSessionInfo *pSession = NULL;
    INT4                i4Result;
    UINT1               u1Index = 0;

    /* To play safe ! */
    if (pTacAcctSession == NULL)
    {
        return (TAC_CLNT_RES_OK);
    }

    for (u1Index = 0; ((u1Index < TAC_ACCT_NO_OF_CURRENT_SESSIONS) &&
                       (u1Index < TAC_MAX_ACCT_SESSION_LIMIT)); u1Index++)
    {
        pSession = TAC_ACCT_SESSION_TABLE[u1Index];
        if (pTacAcctSession == pSession)
        {
            /* Session found */
            break;
        }
    }

    if (u1Index >= TAC_ACCT_NO_OF_CURRENT_SESSIONS)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Session not found in the session table \n");
        return (TAC_CLNT_ERROR_GEN);
    }

    /* Release session */
    i4Result = TAC_RELEASE_MEM_BLOCK (TAC_ACCT_SESSION_POOL_ID,
                                      (UINT1 *) pSession);
    if (i4Result != MEM_SUCCESS)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Memory release failed \n");
        return (TAC_CLNT_ERROR_MEM);
    }

    /* Reset the session table */
    if (u1Index < TAC_MAX_ACCT_SESSION_LIMIT)
    {
        TAC_ACCT_SESSION_TABLE[u1Index] = (tTacAcctSessionInfo *) NULL;
    }

    /*----------------------------------------------------------------------
     * Adjust the session table so that, there is no hole in the session   *
     * table. If the session released is the last entry in the table, then *
     * no adjustment is needed. If the entry is not last entry, then there *
     * will be hole in the table. Take the last entry of the table and fill*
     * in the hole.                                                        *
     *-------------------------------------------------------------------- */
    if (u1Index != (TAC_ACCT_NO_OF_CURRENT_SESSIONS - 1))
    {
        if ((u1Index < TAC_MAX_ACCT_SESSION_LIMIT) &&
            ((TAC_ACCT_NO_OF_CURRENT_SESSIONS - 1) <
             TAC_MAX_ACCT_SESSION_LIMIT))
        {
            TAC_ACCT_SESSION_TABLE[u1Index] =
                TAC_ACCT_SESSION_TABLE[TAC_ACCT_NO_OF_CURRENT_SESSIONS - 1];
            TAC_ACCT_SESSION_TABLE[TAC_ACCT_NO_OF_CURRENT_SESSIONS - 1] =
                (tTacAcctSessionInfo *) NULL;
        }
    }

    TAC_ACCT_NO_OF_CURRENT_SESSIONS--;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAcctConstructPacket()
Description  : To construct an accounting packet
Input(s)     : pTacAcctInput
                    Accounting input given by application
               pTacAcctSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for accounting request
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_GEN
******************************************************************************/
INT4
tacAcctConstructPacket (tTacAcctSessionInfo * pTacAcctSession,
                        tTacAcctInput * pTacAcctInput, UINT1 *pu1TacPacket)
{
    INT4                i4Result = TAC_CLNT_RES_OK;
    tacAcctConstructReqBody (pTacAcctSession, pTacAcctInput, pu1TacPacket);
    i4Result = tacAcctConstructHeader (pTacAcctSession,
                                       pTacAcctInput, pu1TacPacket);
    if (i4Result == (INT4) TAC_CLNT_ERROR_SEQ_NO_WRAPPED)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Packet header construction failed \n");
    }

    return (i4Result);
}

/******************************************************************************
Function Name: tacAcctConstructReqBody()
Description  : To construct accounting request body
Input(s)     : pTacAcctInput
                    Accounting input given by application
               pTacAcctSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet filled for accounting request body
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAcctConstructReqBody (tTacAcctSessionInfo * pTacAcctSession,
                         tTacAcctInput * pTacAcctInput, UINT1 *pu1TacPacket)
{
    UINT2               u2Offset;
    UINT1               u1UserLen;
    UINT1               u1PortLen;
    UINT1               u1RemAddrLen;
    UINT1               u1ArgLen;
    UINT1               u1ArgCnt;

    UNUSED_PARAM (pTacAcctSession);

    *(pu1TacPacket + TAC_ACCT_REQ_FLAG_OFFSET) = pTacAcctInput->u1AcctFlag;
    *(pu1TacPacket + TAC_ACCT_REQ_AUTHEN_METHOD_OFFSET) =
        pTacAcctInput->u1AuthenMethod;
    *(pu1TacPacket + TAC_ACCT_REQ_PL_OFFSET) = pTacAcctInput->u1PrivilegeLevel;
    *(pu1TacPacket + TAC_ACCT_REQ_AUTHEN_TYPE_OFFSET) =
        pTacAcctInput->u1AuthenType;
    *(pu1TacPacket + TAC_ACCT_REQ_SVC_OFFSET) = pTacAcctInput->u1Service;

    /* Fill use name length */
    u1UserLen = (UINT1) STRLEN (pTacAcctInput->au1UserName);
    *(pu1TacPacket + TAC_ACCT_REQ_USER_LEN_OFFSET) = u1UserLen;

    /* Fill Port len */
    u1PortLen = (UINT1) STRLEN (pTacAcctInput->au1Port);
    *(pu1TacPacket + TAC_ACCT_REQ_PORT_LEN_OFFSET) = u1PortLen;

    /* Fill Remote adderss len */
    u1RemAddrLen = (UINT1) STRLEN (pTacAcctInput->au1RemAddr);
    *(pu1TacPacket + TAC_ACCT_REQ_REM_ADDR_LEN_OFFSET) = u1RemAddrLen;

    /* Fill argument count */
    u1ArgCnt = (UINT1) (pTacAcctInput->AcctArgs.u1NoOfArgs +
                        pTacAcctInput->AuthorArgs.u1NoOfArgs);
    *(pu1TacPacket + TAC_ACCT_REQ_ARG_CNT_OFFSET) = u1ArgCnt;

    /* Fill user name */
    u2Offset = (UINT2) (TAC_ACCT_REQ_ARG1_LEN_OFFSET + u1ArgCnt);
    MEMCPY ((pu1TacPacket + u2Offset), pTacAcctInput->au1UserName, u1UserLen);

    /* Fill port name */
    u2Offset += u1UserLen;
    MEMCPY ((pu1TacPacket + u2Offset), pTacAcctInput->au1Port, u1PortLen);

    /* Fill remote address value */
    u2Offset += u1PortLen;
    MEMCPY ((pu1TacPacket + u2Offset), pTacAcctInput->au1RemAddr, u1RemAddrLen);

    /* Fill Accounting arguments and their lengths */
    u2Offset += u1RemAddrLen;
    for (u1ArgCnt = 0; u1ArgCnt < (pTacAcctInput->AcctArgs.u1NoOfArgs);
         u1ArgCnt++)
    {
        u1ArgLen = (UINT1) STRLEN (pTacAcctInput->AcctArgs.au1AVP[u1ArgCnt]);

        /* Copy the attribute value */
        MEMCPY ((pu1TacPacket + u2Offset),
                (pTacAcctInput->AcctArgs.au1AVP[u1ArgCnt]), u1ArgLen);

        /* Set the argument length */
        *(pu1TacPacket + TAC_ACCT_REQ_ARG1_LEN_OFFSET + u1ArgCnt) = u1ArgLen;

        /* Update the packet offset */
        u2Offset += u1ArgLen;
    }

    /* Fill Authorization arguments and their lengths */
    for (u1ArgCnt = 0; u1ArgCnt < (pTacAcctInput->AuthorArgs.u1NoOfArgs);
         u1ArgCnt++)
    {
        u1ArgLen = (UINT1) STRLEN (pTacAcctInput->AuthorArgs.au1AVP[u1ArgCnt]);

        /* Copy the attribute value */
        MEMCPY ((pu1TacPacket + u2Offset),
                (pTacAcctInput->AuthorArgs.au1AVP[u1ArgCnt]), u1ArgLen);

        /* Set the argument length */
        *(pu1TacPacket + TAC_ACCT_REQ_ARG1_LEN_OFFSET +
          (pTacAcctInput->AcctArgs.u1NoOfArgs) + u1ArgCnt) = u1ArgLen;

        /* Update the packet offset */
        u2Offset += u1ArgLen;

        if (u2Offset >= TAC_ACCT_PKT_SIZE)
        {
            /*------------------------------------------------------
             * This part will never be executed. To play safe!. But *
             * the packet will be erraneous packet.                  *
             *------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Unable to accommodate all "
                     "arguments. Not all arguments are filled! \n");
            break;
        }
    }

    /* Update the log for no. of start/stop/watch dogs sent */
    switch (pTacAcctInput->u1AcctFlag)
    {
        case TAC_ACCT_FLAG_START:
            (TAC_CLNT_LOG.u4AcctStartReqs)++;
            break;
        case TAC_ACCT_FLAG_STOP:
            (TAC_CLNT_LOG.u4AcctStopReqs)++;
            break;
        case TAC_ACCT_FLAG_WD_START:
            (TAC_CLNT_LOG.u4AcctStartReqs)++;
            /*-----------------------------------------------------------*
             * Do not insert a "break" here. For WD_START, both start and*
             * WD counters should be updated                             *
             *-----------------------------------------------------------*/
        case TAC_ACCT_FLAG_WD:
            (TAC_CLNT_LOG.u4AcctWdReqs)++;
            break;
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAcctContstructHeader()
Description  : To construct the header of the TACACS+ packet
Input(s)     : pTacAuthroInput
                    Accounting input given by application
               pTacAcctSession
                    The session information for this session
Output(s)    : pu1TacPacket
                    The TACACS+ packet with header filled
Return(s)    : TAC_CLNT_RES_OK
*****************************************************************************/
INT4
tacAcctConstructHeader (tTacAcctSessionInfo * pTacAcctSession,
                        tTacAcctInput * pTacAcctInput, UINT1 *pu1TacPacket)
{
    tTacServerInfo     *pTacServer;
    UINT4               u4SessionId;
    UINT4               u4PktLen;
    UINT1               u1TempLen;
    UINT1               u1ArgCnt;
    UINT1               u1TotalNoOfArgs;

    /* Sequence no. wrapped around. Session must be terminated and restarted */
    if (pTacAcctSession->u1SequenceNoRcvd == TAC_MAX_SEQNO)
    {
        TAC_DBG (TAC_TRACE_INFO, TAC_MODULE, "Sequence no. wrapped around! \n");
        return (TAC_CLNT_ERROR_SEQ_NO_WRAPPED);

    }

    *(pu1TacPacket + TAC_PKT_TYPE_OFFSET) = TAC_ACCT;
    *(pu1TacPacket + TAC_PKT_SEQ_NO_OFFSET) =
        (UINT1) (pTacAcctSession->u1SequenceNoRcvd + 1);

    pTacServer = &(TAC_SERVER_TABLE[pTacAcctSession->u4TacServer - 1]);
    *(pu1TacPacket + TAC_PKT_FLAGS_OFFSET) =
        TAC_UNENC_FLAG | pTacServer->u1SCFlag;

    u4SessionId = OSIX_HTONL (pTacAcctSession->u4SessionIdentifier);
    *((UINT4 *) (VOID *) (pu1TacPacket + TAC_PKT_SESSION_ID_OFFSET)) =
        u4SessionId;

    *(pu1TacPacket + TAC_PKT_VERSION_OFFSET) = (TAC_MAJOR_VER << 4) |
        TAC_MINOR_VER_DEFAULT;
    pTacAcctSession->u1MinorVersionSent = TAC_MINOR_VER_DEFAULT;

    /* Calculate the length of accounting  request body */
    /* User length */
    u1TempLen = *(pu1TacPacket + TAC_ACCT_REQ_USER_LEN_OFFSET);
    u4PktLen = (UINT4) u1TempLen;

    /* Port length */
    u1TempLen = *(pu1TacPacket + TAC_ACCT_REQ_PORT_LEN_OFFSET);
    u4PktLen += (UINT4) u1TempLen;

    /* Remote address length */
    u1TempLen = *(pu1TacPacket + TAC_ACCT_REQ_REM_ADDR_LEN_OFFSET);
    u4PktLen += (UINT4) u1TempLen;

    u1TotalNoOfArgs = (UINT1) (pTacAcctInput->AcctArgs.u1NoOfArgs +
                               pTacAcctInput->AuthorArgs.u1NoOfArgs);
    for (u1ArgCnt = 0; u1ArgCnt < u1TotalNoOfArgs; u1ArgCnt++)
    {
        u1TempLen = *(pu1TacPacket + TAC_ACCT_REQ_ARG1_LEN_OFFSET + u1ArgCnt);
        u4PktLen += (UINT4) u1TempLen;
    }

    u4PktLen += (UINT4) u1ArgCnt;    /* No of bytes for arguments lengths */

    u4PktLen += TAC_ACCT_REQ_LEN_FIXED_FIELDS;
    u4PktLen = OSIX_HTONL (u4PktLen);
    *((UINT4 *) (VOID *) (pu1TacPacket + TAC_PKT_LEN_OFFSET)) = u4PktLen;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAcctReceivedPkt()
Description  : To poll all connections to check if there is any packet        
Input(s)     : pu1NoOfPkts
                  The no. of packets already available in the receive buffer.
Output(s)    : pu1NoOfPkts
                  The no. packets received. + initial no (given as input)
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAcctReceivedPkt (UINT1 *pu1PktCnt)
{
    tTacAcctSessionInfo *pTacAcctSession;
    tTacServerInfo     *pTacServer;
    tTacPkt            *pTacTempPkt;
    tTacAcctOutput      TacAcctOutput;
    UINT1               u1NoOfPkts;
    UINT1               u1Index;

    u1NoOfPkts = *pu1PktCnt;

    for (u1Index = 0;
         u1Index < TAC_ACCT_NO_OF_CURRENT_SESSIONS
         && u1Index < TAC_MAX_ACCT_SESSION_LIMIT; u1Index++)
    {
        pTacAcctSession = TAC_ACCT_SESSION_TABLE[u1Index];
        /* To play safe! */
        if (pTacAcctSession == NULL)
        {
            continue;
        }

        pTacServer = &(TAC_SERVER_TABLE[pTacAcctSession->u4TacServer - 1]);
        if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
        {
            pTacTempPkt = &(TAC_SC_TEMP_PKT[pTacAcctSession->u4TacServer - 1]);
        }
        else
        {
            pTacTempPkt = &(pTacAcctSession->TacTempPkt);
        }

        if (FD_ISSET (pTacAcctSession->u4ConnectionId, &gReadSockFds))
        {
            tacReadPacket (pTacAcctSession->u4ConnectionId, pTacTempPkt,
                           &u1NoOfPkts);
            if (u1NoOfPkts > 0)
            {
                SelAddFd (pTacAcctSession->u4ConnectionId,
                          (VOID *) TacNotifyTask);
            }
            else
            {
                TAC_DBG (TAC_TRACE_INFO, TAC_MODULE,
                         "Accounting session timed out and released\n");

                MEMSET (&TacAcctOutput, 0, sizeof (tTacAcctOutput));

                TacAcctOutput.u4AppReqId =
                    pTacAcctSession->AcctInput.u4AppReqId;
                TacAcctOutput.i4AcctStatus = TAC_CLNT_RES_NOT_OK;
                TacAcctOutput.i4ErrorValue = TAC_CLNT_ERROR_CONN_CLOSE;
                tacSendInfoToApp (TAC_ACCT,
                                  &(pTacAcctSession->AcctInput.AppInfo),
                                  &TacAcctOutput);
                tacCloseConnection (pTacAcctSession->u4TacServer,
                                    pTacAcctSession->u4ConnectionId);
                tacAcctReleaseSession (pTacAcctSession);
            }

        }
        if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
        {
                /*--------------------------------------------------------*
                 * If single connection mode, all sessions will be having *
                 * same connection identifier. So polling once is enough  *
                 *--------------------------------------------------------*/
            break;
        }
    }                            /* for all sessions */

    *pu1PktCnt = u1NoOfPkts;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAcctReply()
Description  : To process the received accounting reply packet
Input(s)     : pu1TacAcctReplyPkt
                  Received accounting reply packet
Output(s)    : pTacAcctOutput
                  The out put of the accounting reply
               pTacSession
                  The session that corresponds to this packet
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_RCVD_PKT
******************************************************************************/
INT4
tacProcessAcctReply (UINT1 *pu1TacAcctReplyPkt,
                     tTacAcctSessionInfo ** ppTacSession,
                     tTacAcctOutput * pTacAcctOutput)
{
    tTacAcctSessionInfo *pTacAcctSession = NULL;
    tTacServerInfo     *pTacServer;
    UINT4               u4SessionId;
    INT4                i4Result;
    UINT1               u1Index;

    /* Find the session corresponding to this packet */
    u4SessionId = *((UINT4 *) (VOID *)
                    (pu1TacAcctReplyPkt + TAC_PKT_SESSION_ID_OFFSET));
    u4SessionId = OSIX_NTOHL (u4SessionId);

    for (u1Index = 0;
         u1Index < TAC_ACCT_NO_OF_CURRENT_SESSIONS
         && u1Index < TAC_MAX_ACCT_SESSION_LIMIT; u1Index++)
    {
        pTacAcctSession = TAC_ACCT_SESSION_TABLE[u1Index];
        if (u4SessionId == (pTacAcctSession->u4SessionIdentifier))
        {
            /* Session found */
            break;
        }
    }

    if (u1Index >= TAC_ACCT_NO_OF_CURRENT_SESSIONS)
    {
        /* Session not found */
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "No corresponding session available for the received packet \n");
        *ppTacSession = NULL;
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    *ppTacSession = pTacAcctSession;

    /* Decrypt the packet */
    pTacServer = &(TAC_SERVER_TABLE[pTacAcctSession->u4TacServer - 1]);
    tacEncryptDecrypt (pu1TacAcctReplyPkt, pTacServer->au1SecretKey);

    TAC_DBG_PKT (TAC_TRACE_PKT_RX, (CONST UINT1 *) TAC_MODULE,
                 pu1TacAcctReplyPkt);

    /* Validate the packet */
    i4Result = tacValidateAcctReply (pu1TacAcctReplyPkt, pTacAcctSession);
    if (i4Result != TAC_CLNT_RES_OK)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Acct. response pkt validation failed \n");

        /* Update the log. for no. malformed pkts received */
        (TAC_CLNT_LOG.u4MalformedPkts)++;

        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Set the sequence no. in the accounting session data structure */
    pTacAcctSession->u1SequenceNoRcvd =
        *(pu1TacAcctReplyPkt + TAC_PKT_SEQ_NO_OFFSET);

    /* Fill the common fields of the accounting out put data */
    tacAcctOutputFillCmn (pu1TacAcctReplyPkt, pTacAcctSession, pTacAcctOutput);

    switch (pTacAcctOutput->i4AcctStatus)
    {
        case TAC_ACCT_STATUS_SUCCESS:
            tacProcessAcctSuccess (pu1TacAcctReplyPkt, pTacAcctSession,
                                   pTacAcctOutput);
            break;
        case TAC_ACCT_STATUS_ERROR:
            tacProcessAcctError (pu1TacAcctReplyPkt, pTacAcctSession,
                                 pTacAcctOutput);
            break;
        case TAC_ACCT_STATUS_FOLLOW:
            tacProcessAcctFollow (pu1TacAcctReplyPkt, pTacAcctSession,
                                  pTacAcctOutput);
            break;
        default:
            /*-----------------------------------------------------------*
             * This part will never be executed, as the packet is already*
             * validated for correct accounting reply status             *
             *-----------------------------------------------------------*/
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid accounting status rcvd \n");
            return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacValidateAcctReply()
Description  : To validate received accounting reply
Input(s)     : pu1TacAcctReplyPkt   
                  Received accounting reply packet
               pTacAcctSession   
                  The accounting session corresponding to this accounting 
                  reply packet
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
               TAC_CLNT_ERROR_RCVD_PKT
******************************************************************************/
INT4
tacValidateAcctReply (UINT1 *pu1TacAcctReplyPkt,
                      tTacAcctSessionInfo * pTacAcctSession)
{
    tTacServerInfo     *pTacServer;
    UINT4               u4BodyLen;
    UINT4               u4PktLen;
    INT4                i4Result;
    UINT2               u2CompLen;

    i4Result = tacValidateCmnHdrFields (pu1TacAcctReplyPkt);

    if (i4Result != TAC_CLNT_RES_OK)
    {
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /*-----------------------------------------------------------------*
     * Check for single connect flag. This need to be checked only for *
     * first two packets.                                              *
     *---------------------------------------------------------------- */
    pTacServer = &(TAC_SERVER_TABLE[pTacAcctSession->u4TacServer - 1]);
    if ((pTacServer->u1SCVerifyCount) < TAC_SC_VERIFY_COUNT_MAX)
    {
        if (*(pu1TacAcctReplyPkt + TAC_PKT_FLAGS_OFFSET) !=
            (pTacServer->u1SCFlag))
        {
            if ((pTacServer->u1SCFlag) == TAC_SINGLE_CONNECT_FLAG)
            {
                /*----------------------------------------------------*
                 * Client operating in Single connect mode and server *
                 * suggests to operate in multiple connection mode. So*
                 * switch back to multiple connection mode            *
                 *----------------------------------------------------*/
                pTacServer->u1SCFlag = 0;
                pTacServer->i4SingleConnId = 0;
            }
            else
            {
                /*----------------------------------------------------*
                 * Client operating in multiple connection mode and   *
                 * server suggests to operate in single connection    *
                 * mode. So switch back to single connection mode     *
                 *----------------------------------------------------*/
                pTacServer->u1SCFlag = TAC_SINGLE_CONNECT_FLAG;
                pTacServer->i4SingleConnId =
                    (INT4) (pTacAcctSession->u4ConnectionId);
            }
            TAC_DBG (TAC_TRACE_INFO, TAC_MODULE,
                     "Connection mode changed as server suggestion\n");
        }
        pTacServer->u1SCVerifyCount++;
    }

    /* Validate minor version */
    if (((*(pu1TacAcctReplyPkt + TAC_PKT_VERSION_OFFSET)) &
         TAC_MINOR_VER_MASK) != (pTacAcctSession->u1MinorVersionSent))
    {

        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Minor version not matching with server!\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /*--------------------------------------------------------------------*
     * Validate sequence no. received. The received sequence no. should be*
     * greater than previous received sequence no. by 2.                  *
     *--------------------------------------------------------------------*/
    if (*(pu1TacAcctReplyPkt + TAC_PKT_SEQ_NO_OFFSET) !=
        (pTacAcctSession->u1SequenceNoRcvd + 2))
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE, "Invalid sequence no received\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Validate length */
    u4PktLen = *((UINT4 *) (VOID *) (pu1TacAcctReplyPkt + TAC_PKT_LEN_OFFSET));
    u4PktLen = OSIX_NTOHL (u4PktLen);
    if (u4PktLen == 0)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Server unable to understand the packet sent\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    u2CompLen = *((UINT2 *) (VOID *) (pu1TacAcctReplyPkt +
                                      TAC_ACCT_REPLY_SRV_MSG_LEN_OFFSET));
    u2CompLen = OSIX_NTOHS (u2CompLen);
    u4BodyLen = (UINT4) u2CompLen;
    u2CompLen = *((UINT2 *) (VOID *) (pu1TacAcctReplyPkt +
                                      TAC_ACCT_REPLY_DATA_LEN_OFFSET));
    u2CompLen = OSIX_NTOHS (u2CompLen);
    u4BodyLen += ((UINT4) u2CompLen + TAC_ACCT_REPLY_LEN_FIXED_FIELDS);

    if (u4PktLen != u4BodyLen)
    {
        TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                 "Length in the header and total reply body length are not equal\n");
        return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    /* Validate accounting status received */
    switch (*(pu1TacAcctReplyPkt + TAC_ACCT_REPLY_STATUS_OFFSET))
    {
        case TAC_ACCT_STATUS_SUCCESS:
        case TAC_ACCT_STATUS_ERROR:
        case TAC_ACCT_STATUS_FOLLOW:
            break;
        default:
            TAC_DBG (TAC_TRACE_ERROR, TAC_MODULE,
                     "Invalid accounting status in reply packet\n");
            return (TAC_CLNT_ERROR_RCVD_PKT);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAcctOutputFillCmn()
Description  : To fill the common fields of accounting out put
Input(s)     : pu1TacAcctReplyPkt
                  Validated accounting response packet
               pTacAcctSession
                  The accounting session corresponding to this packet
Output(s)    : pTacAcctOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAcctOutputFillCmn (UINT1 *pu1TacAcctReplyPkt,
                      tTacAcctSessionInfo * pTacAcctSession,
                      tTacAcctOutput * pTacAcctOutput)
{
    UINT2               u2Len;
    UINT2               u2Offset;

    /*-----------------------------------------------------------------* 
     * Copy the Application request identifier, so that the application* 
     * will match this response with the request it made and will do   *
     * further processing                                              *
     *-----------------------------------------------------------------*/
    pTacAcctOutput->u4AppReqId = pTacAcctSession->AcctInput.u4AppReqId;

    /* Set the accounting status */
    pTacAcctOutput->i4AcctStatus =
        *(pu1TacAcctReplyPkt + TAC_ACCT_REPLY_STATUS_OFFSET);

    /* If server message is available, send the message to application */
    u2Len = *((UINT2 *) (VOID *) (pu1TacAcctReplyPkt +
                                  TAC_ACCT_REPLY_SRV_MSG_LEN_OFFSET));
    u2Len = OSIX_NTOHS (u2Len);
    if (u2Len != 0)
    {
        if (u2Len > TAC_SRV_MSG_LEN)
        {
            u2Len = TAC_SRV_MSG_LEN;
        }
        u2Offset = TAC_ACCT_REPLY_SRV_MSG_OFFSET;
        MEMCPY (pTacAcctOutput->au1ServerMessage,
                (pu1TacAcctReplyPkt + u2Offset), u2Len);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAcctOutputFillData()
Description  : To fill the data field of accounting out put
Input(s)     : pu1TacAcctReplyPkt
                  Validated accounting reply packet
               pTacAcctSession
                  The accounting session corresponding to this packet
Output(s)    : pTacAcctOutput
                 The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAcctOutputFillData (UINT1 *pu1TacAcctReplyPkt,
                       tTacAcctSessionInfo * pTacAcctSession,
                       tTacAcctOutput * pTacAcctOutput)
{
    UINT2               u2DataLen;
    UINT2               u2MsgLen;
    UINT4               u4Offset;

    UNUSED_PARAM (pTacAcctSession);

    u2DataLen = *((UINT2 *) (VOID *) (pu1TacAcctReplyPkt +
                                      TAC_ACCT_REPLY_DATA_LEN_OFFSET));
    u2DataLen = OSIX_NTOHS (u2DataLen);
    if (u2DataLen != 0)
    {
        if (u2DataLen > TAC_SRV_DATA_LEN)
        {
            u2DataLen = TAC_SRV_DATA_LEN;
        }

        /*------------------------------------------------------------*
         * To calculate the data offset, first get the server message *
         * length. The add this with server message offset            *
         *------------------------------------------------------------*/
        u2MsgLen = *((UINT2 *) (VOID *) (pu1TacAcctReplyPkt +
                                         TAC_ACCT_REPLY_SRV_MSG_LEN_OFFSET));
        u2MsgLen = OSIX_NTOHS (u2MsgLen);
        u4Offset = (UINT4) (u2MsgLen + TAC_ACCT_REPLY_SRV_MSG_OFFSET);

        MEMCPY (pTacAcctOutput->au1ServerData,
                (pu1TacAcctReplyPkt + u4Offset), u2DataLen);
    }

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAcctSuccess()
Description  : To process the accounting reply for accounting success
Input(s)     : pu1TacAcctReplyPkt   
                  Validated accounting reply packet
               pTacAcctSession   
                  The accounting  session corresponding to this packet
Output(s)    : pTacAcctOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAcctSuccess (UINT1 *pu1TacAcctReplyPkt,
                       tTacAcctSessionInfo * pTacAcctSession,
                       tTacAcctOutput * pTacAcctOutput)
{
    /* Copy data into out put */
    tacAcctOutputFillData (pu1TacAcctReplyPkt, pTacAcctSession, pTacAcctOutput);

    /* Update the log for no. of accounting success received */
    (TAC_CLNT_LOG.u4AcctSuccess)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAcctError()
Description  : To process the accounting reply for accounting error
Input(s)     : pu1TacAcctReplyPkt   
                  Validated accounting reply packet
               pTacAcctSession   
                  The accounting  session corresponding to this packet
Output(s)    : pTacAcctOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAcctError (UINT1 *pu1TacAcctReplyPkt,
                     tTacAcctSessionInfo * pTacAcctSession,
                     tTacAcctOutput * pTacAcctOutput)
{

    /*---------------------------------------------------------------*
     * Send the data to application. So that this would be printed on*
     * administrative console                                        *
     *---------------------------------------------------------------*/
    tacAcctOutputFillData (pu1TacAcctReplyPkt, pTacAcctSession, pTacAcctOutput);

    /* Update the log for no. of accounting error received */
    (TAC_CLNT_LOG.u4AcctErrors)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacProcessAcctFollow()
Description  : To process the accounting reply for accounting follow
Input(s)     : pu1TacAcctReplyPkt   
                  Validated accounting reply packet
               pTacAcctSession   
                  The accounting  session corresponding to this packet
Output(s)    : pTacAcctOutput
                  The out put information appropriately filled
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacProcessAcctFollow (UINT1 *pu1TacAcctReplyPkt,
                      tTacAcctSessionInfo * pTacAcctSession,
                      tTacAcctOutput * pTacAcctOutput)
{
    /*----------------------------------------------------------------*
     * If server data is available, send the message to application   *
     * This data will be list of alternate servers suggested by the   *
     * current server for futher accounting. The client does not      *
     * use the alternate servers by itself. The alternate server list *
     * will be given to application. It is upto the application to    *
     * treat this as accounting failure or restart the accounting     *
     * with any of the servers mentioned in the list                  *
     *----------------------------------------------------------------*/
    tacAcctOutputFillData (pu1TacAcctReplyPkt, pTacAcctSession, pTacAcctOutput);

    /* Update the log for no. of accounting error received */
    (TAC_CLNT_LOG.u4AcctFollows)++;

    return (TAC_CLNT_RES_OK);
}

/*****************************************************************************
Function Name: tacAcctChkSessionTimeout()
Description  : To check if any session has timed out. The time out sessions are
               released
Input(s)     : None
Output(s)    : None
Return(s)    : TAC_CLNT_RES_OK
******************************************************************************/
INT4
tacAcctChkSessionTimeout ()
{
    tTacAcctOutput      TacAcctOutput;
    tTacAcctSessionInfo *pTacAcctSession;
    UINT1               u1Index;

    for (u1Index = 0;
         u1Index < TAC_ACCT_NO_OF_CURRENT_SESSIONS
         && u1Index < TAC_MAX_ACCT_SESSION_LIMIT; u1Index++)
    {
        pTacAcctSession = TAC_ACCT_SESSION_TABLE[u1Index];
        (pTacAcctSession->u4Timer)--;
        if ((INT4) (pTacAcctSession->u4Timer) == 0)
        {
            /* Time out has occured. Release the session */
            TAC_DBG (TAC_TRACE_INFO, TAC_MODULE,
                     "Accounting session timed out and released\n");

            /* Update the log for no. of accounting session time outs */
            (TAC_CLNT_LOG.u4AcctSessionTimeouts)++;

            MEMSET (&TacAcctOutput, 0, sizeof (tTacAcctOutput));

            TacAcctOutput.u4AppReqId = pTacAcctSession->AcctInput.u4AppReqId;
            TacAcctOutput.i4AcctStatus = TAC_CLNT_RES_NOT_OK;
            TacAcctOutput.i4ErrorValue = TAC_CLNT_ERROR_SESSION_TIMEOUT;
            tacSendInfoToApp (TAC_ACCT,
                              &(pTacAcctSession->AcctInput.AppInfo),
                              &TacAcctOutput);
            tacCloseConnection (pTacAcctSession->u4TacServer,
                                pTacAcctSession->u4ConnectionId);
            tacAcctReleaseSession (pTacAcctSession);
        }
    }

    return (TAC_CLNT_RES_OK);
}

/******************************** End of file *********************************/
