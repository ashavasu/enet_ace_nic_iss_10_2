/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tacsz.c,v 1.1 2015/04/28 12:49:59 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/
#define _TACACSSZ_C
#include "tpinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
TacacsSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TACACS_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsTACACSSizingParams[i4SizingId].u4StructSize,
                              FsTACACSSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(TACACSMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            TacacsSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
TacacsSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsTACACSSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, TACACSMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
TacacsSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TACACS_MAX_SIZING_ID; i4SizingId++)
    {
        if (TACACSMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (TACACSMemPoolIds[i4SizingId]);
            TACACSMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
