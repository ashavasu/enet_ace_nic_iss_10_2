# include  "lr.h"
# include  "fssnmp.h"
# include  "fstacslw.h"
# include  "fstacswr.h"
# include  "fstacsdb.h"
# include  "tpprot.h"

VOID
RegisterFSTACS ()
{
    SNMPRegisterMibWithLock (&fstacsOID, &fstacsEntry, TacProtocolLock,
                             TacProtocolUnlock, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fstacsOID, (const UINT1 *) "fstacsxt");
}

VOID
UnRegisterFSTACS ()
{
    SNMPUnRegisterMib (&fstacsOID, &fstacsEntry);
    SNMPDelSysorEntry (&fstacsOID, (const UINT1 *) "fstacsxt");
}

INT4
FsTacClntExtActiveServerAddressTypeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtActiveServerAddressType
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsTacClntExtActiveServerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtActiveServer (pMultiData->pOctetStrValue));
}

INT4
FsTacClntExtTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtTraceLevel (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtRetransmitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtRetransmit (&(pMultiData->i4_SLongValue)));
}

INT4
FsTacClntExtActiveServerAddressTypeSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsTacClntExtActiveServerAddressType
            (pMultiData->i4_SLongValue));
}

INT4
FsTacClntExtActiveServerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsTacClntExtActiveServer (pMultiData->pOctetStrValue));
}

INT4
FsTacClntExtTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsTacClntExtTraceLevel (pMultiData->u4_ULongValue));
}

INT4
FsTacClntExtRetransmitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsTacClntExtRetransmit (pMultiData->i4_SLongValue));
}

INT4
FsTacClntExtActiveServerAddressTypeTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsTacClntExtActiveServerAddressType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsTacClntExtActiveServerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsTacClntExtActiveServer
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsTacClntExtTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsTacClntExtTraceLevel
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsTacClntExtRetransmitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsTacClntExtRetransmit
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsTacClntExtActiveServerAddressTypeDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntExtActiveServerAddressType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsTacClntExtActiveServerDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntExtActiveServer
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsTacClntExtTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntExtTraceLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsTacClntExtRetransmitDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntExtRetransmit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsTacClntExtAuthenStartRequestsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenStartRequests
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenContinueRequestsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenContinueRequests
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenEnableRequestsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenEnableRequests
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenAbortRequestsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenAbortRequests
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenPassReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenPassReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenFailReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenFailReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenGetUserReceivedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenGetUserReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenGetPassReceivedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenGetPassReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenGetDataReceivedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenGetDataReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenErrorReceivedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenErrorReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenFollowReceivedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenFollowReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenRestartReceivedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenRestartReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthenSessionTimoutsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthenSessionTimouts
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthorRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthorRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthorPassAddReceivedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthorPassAddReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthorPassReplReceivedGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthorPassReplReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthorFailReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthorFailReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthorErrorReceivedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthorErrorReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthorFollowReceivedGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthorFollowReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAuthorSessionTimeoutsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAuthorSessionTimeouts
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAcctStartRequestsGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAcctStartRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAcctWdRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAcctWdRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAcctStopRequestsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAcctStopRequests (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAcctSuccessReceivedGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAcctSuccessReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAcctErrorReceivedGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAcctErrorReceived (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAcctFollowReceivedGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAcctFollowReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtAcctSessionTimeoutsGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtAcctSessionTimeouts
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtMalformedPktsReceivedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtMalformedPktsReceived
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtSocketFailuresGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtSocketFailures (&(pMultiData->u4_ULongValue)));
}

INT4
FsTacClntExtConnectionFailuresGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsTacClntExtConnectionFailures
            (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexFsTacClntExtServerTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsTacClntExtServerTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsTacClntExtServerTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsTacClntExtServerStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntExtServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntExtServerStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsTacClntExtServerSingleConnectGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntExtServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntExtServerSingleConnect
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsTacClntExtServerPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntExtServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntExtServerPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
FsTacClntExtServerTimeoutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntExtServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntExtServerTimeout
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsTacClntExtServerKeyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsTacClntExtServerTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsTacClntExtServerKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsTacClntExtServerStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntExtServerStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsTacClntExtServerSingleConnectSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsTacClntExtServerSingleConnect
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsTacClntExtServerPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntExtServerPort (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
FsTacClntExtServerTimeoutSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntExtServerTimeout
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsTacClntExtServerKeySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsTacClntExtServerKey (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
FsTacClntExtServerStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntExtServerStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsTacClntExtServerSingleConnectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntExtServerSingleConnect (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
FsTacClntExtServerPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntExtServerPort (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[1].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
FsTacClntExtServerTimeoutTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntExtServerTimeout (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsTacClntExtServerKeyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsTacClntExtServerKey (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiData->pOctetStrValue));

}

INT4
FsTacClntExtServerTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsTacClntExtServerTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
