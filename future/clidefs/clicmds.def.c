/* $Id: clicmds.def.c,v 1.323 2017/12/26 13:34:19 siva Exp $ */
DEFINE              GROUP:ALL
#include "allcmds.def"
                    END GROUP
#ifdef L2TPV3_WANTED
#include "l2tpcmd.def"
#endif
#include "cliconf.def"
#include "stdallcmds.def"
#ifdef NPAPI_TEST_WANTED
#include "npapicmd.def"
#endif
#if defined(RSTP_WANTED) ||  defined(MSTP_WANTED)
#include "stpcmd.def"
#include "stpshcmd.def"
#endif                            /* RSTP_WANTED || MSTP_WANTED */
#ifdef LCM_WANTED
#include "lcmcmd.def"
#endif
#ifdef EVCPRO_WANTED
#include "evcprocmd.def"
#include "unicmd.def"
#endif
#ifdef ELMI_WANTED
#include "elmcmd.def"
#endif
#ifdef TCP_WANTED
#include "fstcpcmd.def"
#endif
#ifdef TCP_TEST_WANTED
#include "tcptstcmd.def"
#endif
#ifdef OPENFLOW_WANTED
#include "ofcshcmd.def"
#include "ofccmd.def"
#include "ofcifcmd.def"
#endif
#ifdef OFC_TEST_SERVER_WANTED
#include "ofctscmd.def"
#endif
#ifdef VXLAN_TEST_WANTED
#include "vxlantstcmd.def"
#endif
#ifdef BGP_WANTED
#include "bgp4cmd.def"
#endif
#ifdef FIREWALL_WANTED
#include "fwlcmd.def"
#endif
#if defined (SMOD_WANTED) && defined(FIREWALL_WANTED)
#include "seccmd.def"
#endif
#ifdef CFA_WANTED
#include "cfacmd.def"
#include "ipdbcmds.def"
#include "l2dscmds.def"
#ifdef CFA_TEST_WANTED
#include "cfautcmd.def"
#endif
#endif
#ifdef MPLS_WANTED
#include "mprtrcds.def"
#include "mpshwcds.def"
#include "mpintcds.def"
#include "mpoamcds.def"
#ifdef MPLS_TEST_WANTED
#include "mptstcmd.def"
#include "vctstcmd.def"
#include "l3vpnutcmd.def"
#include "ldputcmd.def"
#endif
#ifdef BFD_TEST_WANTED
#include "bftstcmd.def"
#endif
#ifdef LSPP_TEST_WANTED
#include "lststcmd.def"
#endif
#ifdef MPLS_SIG_WANTED
#include "ldpcmds.def"
#include "rptecmds.def"
#endif
#include "l2vpncds.def"
#ifdef MPLS_L3VPN_WANTED
#include "l3vpncmd.def"
#endif
#ifdef LSPP_WANTED
#include "lsppcmd.def"
#endif
#ifdef LDP_TEST_WANTED
#include "ldptstcd.def"
#endif
#endif
#ifdef LA_WANTED
#include "lacmd.def"
#endif
#ifdef LLDP_WANTED
#include "lldcmd.def"
#endif
#ifdef EOAM_WANTED
#include "eoamcmd.def"
#ifdef EOAM_FM_WANTED
#include "emfmcmd.def"
#endif
#ifdef EOAM_TEST_WANTED
#include "emtstcmd.def"
#endif
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
#include "pbbcmd.def"
#endif
#endif                            /* PBB_WANTED */
#ifdef VLAN_WANTED
#ifdef PB_WANTED
#include "vlnpbcmd.def"
#include "vlpbshcmd.def"
#endif
#include "vlancmd.def"
#include "vlnshcmd.def"
#ifdef GARP_WANTED
#include "garpcmd.def"
#include "grpshcmd.def"
#endif
#ifdef EVB_WANTED                /*VLAN_EVB WANTED */
#include "vlnevcmd.def"
#endif
#ifdef VLAN_TEST_WANTED
#include "vltstcmd.def"
#endif
#endif                            /* VLAN_WANTED */
#ifdef VXLAN_WANTED
#include "vxcmd.def"
#endif
#if defined IGS_WANTED || defined MLDS_WANTED
#include "snpcmd.def"
#include "snpshcmd.def"
#endif
#ifdef PNAC_WANTED
#include "pnaccmd.def"
#endif
#ifdef RSNA_WANTED
#include "rsnacmd.def"
#endif
#ifdef WPS_WANTED
#include "wpscmd.def"
#endif
#ifdef MRP_WANTED
#include "mrpcmd.def"
#endif
#ifdef VCM_WANTED
#include "vcmcmd.def"
#include "sispcmd.def"
#endif
#ifdef BEEP_SERVER_WANTED
#include "bpsrvcmd.def"
#endif
#ifdef RADIUS_WANTED
#include "radiuscmd.def"
#endif
#ifdef TACACS_WANTED
#include "tpcmds.def"
#endif
#ifdef RMON_WANTED
#include "rmoncmd.def"
#endif
#ifdef DSMON_WANTED
#include "dsmncmd.def"
#endif
#ifdef RMON2_WANTED
#include "rmn2cmd.def"
#endif
#ifdef RM_WANTED
#include "rmcmd.def"
#endif
#ifdef HB_WANTED
#include "hbcmd.def"
#endif
#ifdef ISSU_WANTED
#include "issucmd.def"
#endif
#ifdef ICCH_WANTED
#include "icchcmd.def"
#endif
#ifdef WEBNM_WANTED
#include "httpcmd.def"
#endif
#ifdef DNS_WANTED
#include "dnscmd.def"
#endif
#ifdef ISS_WANTED
#ifdef DIFFSRV_WANTED
#include "diffsrvcmd.def"
#endif
#include "syscmd.def"
#include "isspicmd.def"
#include "issexcmd.def"
#include "entcmd.def"
#if defined(XCAT) || defined(DX167) ||  defined(LION) ||  defined(LION_DB) || defined(XCAT3)
#include "aclcmd.def"
#else
#ifdef ISS_METRO_WANTED
#include "aclmet.def"
#else
#include "aclcmd.def"
#endif
#endif
#include "l4scmd.def"
#endif
#ifdef ISS_HEALTH_TEST_WANTED
#include "isshlthcmd.def"
#endif
#ifdef SYSLOG_WANTED
#include "syslgcmd.def"
#endif
#ifdef SNTP_WANTED
#include "sntpcmds.def"
#endif
#ifdef SSL_WANTED
#include "sslcmd.def"
#endif
#ifdef SSH_WANTED
#include "sshcmd.def"
#endif
#ifdef DHCP_RLY_WANTED
#include "dhcprcmd.def"
#endif
#ifdef DHCP_SRV_WANTED
#include "dhcpscmd.def"
#endif
#ifdef VRRP_WANTED
#include "vrrpcmd.def"
#endif
#ifdef DHCPC_WANTED
#include "dhcpccmd.def"
#endif
#ifdef DHCP6_SRV_WANTED
#include "d6srcmd.def"
#endif
#ifdef DHCP6_CLNT_WANTED
#include "d6clcmd.def"
#endif
#ifdef DHCP6_RLY_WANTED
#include "d6rlcmd.def"
#endif
#ifdef ARP_WANTED
#ifdef ARP_TEST_WANTED
#include "arptstcmd.def"
#endif
#endif
#ifdef PTP_WANTED
#ifdef PTP_TEST_WANTED
#include "ptptstcmd.def"
#endif
#endif
#ifdef IP_WANTED
#include "fsipcmd.def"
#endif
#if  defined (IP_WANTED) || defined(LNXIP4_WANTED)
#include "ipcmd.def"
#include "pingcmd.def"
#include "tcpcmd.def"
#include "udpcmd.def"
#include "tracecmd.def"
#endif
#ifdef NPAPI_WANTED
#include "npdbgcmd.def"
#endif
#ifdef ROUTEMAP_WANTED
#include "rmapcmd.def"
#endif
#ifdef IP6_WANTED
#include "ip6cmd.def"
#endif
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
#include "secv6cmd.def"
#endif
#ifdef OSPF_WANTED
#include "ospfcmd.def"
#endif
#ifdef OSPFTE_WANTED
#include "ostecmd.def"
#endif
#ifdef OSPF3_WANTED
#include "ospf3cmd.def"
#endif
#ifdef TLM_WANTED
#include "tlmcmd.def"
#endif
#ifdef ISIS_WANTED
#include "iscmds.def"
#endif
#ifdef RRD_WANTED
#include "rtmcmds.def"
#endif
#ifdef IP_RTMTEST_WANTED
#include "rtmtstcmd.def"
#endif
#ifdef IP6_WANTED
#include "rtm6cmd.def"
#endif
#ifdef RIP_WANTED
#include "ripcmd.def"
#endif
#ifdef RIP6_WANTED
#include "rip6cmd.def"
#endif
#ifdef DVMRP_WANTED
#include "dvmrpcmd.def"
#endif
#ifdef NAT_WANTED
#include "natcmds.def"
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
#include "pimcmd.def"
#endif
#ifdef MSDP_WANTED
#include "msdpcmd.def"
#include "msdp6cmd.def"
#endif
#ifdef IGMP_WANTED
#include "igmpcmd.def"
#endif
#ifdef IGMPPRXY_WANTED
#include "igpcmd.def"
#endif
#ifdef MLD_WANTED
#include "mldcmd.def"
#endif
#ifdef MRI_WANTED
#include "mricmd.def"
#endif
#ifdef MFWD_WANTED
#include "mfwdcmd.def"
#endif
#ifdef SNMP_3_WANTED
#include "snmp3cmd.def"
#include "snxcmd.def"
#endif
#ifdef MBSM_WANTED
#include "mbsmcmd.def"
#endif
#ifdef POE_WANTED
#include "poecmd.def"
#endif
#ifdef PPP_WANTED
#include "pppcmds.def"
#endif
#ifdef BCMX_WANTED
#ifdef KERNEL_WANTED
#include "kerncmd.def"
#endif
#endif
#ifdef CUST_CLI_WANTED
#include "custcmd.def"
#endif
#ifdef ECFM_WANTED
#include "cfmcmd.def"
#include "cfmshcmd.def"
#include "cfmmpcmd.def"
#include "cfmmpshcmd.def"
#endif
#ifdef TTCP_WANTED
#include "ttcp.def"
#endif
#ifdef QOSX_WANTED
#include "qosxcmds.def"
#endif
#ifdef DCBX_WANTED
#include "dcbxcmd.def"
#endif                            /*DCBX_WANTED */
#ifdef CN_WANTED                /* CN_WANTED */
#include "cncmd.def"
#endif                            /* CN_WANTED */
#ifdef RBRG_WANTED
#include "rbrgcmd.def"
#include "rbrgshcmd.def"
#endif
#ifdef TAC_WANTED
#include "taccmd.def"
#endif
#ifdef PBBTE_WANTED
#include "pbtcmd.def"
#endif
#ifdef ELPS_WANTED
#include "elpscmd.def"
#endif
#ifdef BFD_WANTED
#include "bfdcmd.def"
#endif
#ifdef PTP_WANTED
#include "ptpcmd.def"
#endif
#ifdef SYNCE_WANTED
#include "syncecmd.def"
#endif
#ifdef SYNCE_UT_WANTED
#include "syncetstcmd.def"
#endif
#ifdef CLKIWF_WANTED
#include "clkiwcmd.def"
#endif
#ifdef FIPS_WANTED
#include "fipscmd.def"
#endif
/* %% */
#ifdef ISSMEM_WANTED
#include "issszcmd.def"
#endif
#ifdef ERPS_WANTED
#include "erpscmd.def"
#endif
#ifdef Y1564_WANTED
#include "y1564cmd.def"
#include "y1564shcmd.def"
#endif
#ifdef RFC6374_WANTED
#include "r6374cmd.def"
#endif
#ifdef RFC2544_WANTED
#include "r2544cmd.def"
#ifdef RFC2544_TEST_WANTED
#include "r2544tstcmd.def"
#endif
#endif
#ifdef VPN_WANTED
#include "vpncmds.def"
#endif
#ifdef MEF_WANTED
#include "mefcmd.def"
#endif
#ifdef HOTSPOT2_WANTED
#include "hscmd.def"
#endif
#ifdef WLC_WANTED
#include "wsscfgcmd.def"
#include "wsscfgshcmd.def"
#include "fsaccmd.def"
#include "facshcmd.def"
#include "fs11ncmd.def"
#ifdef WSSCUST_WANTED
#include "radcfcmd.def"
#endif
#include "fswsscmd.def"
#include "fswssshcmd.def"
#ifdef WSSUSER_WANTED
#include "usercmd.def"
#endif
#ifdef RFMGMT_WANTED
#include "rfmcmd.def"
#include "rfmshcmd.def"
#endif
#endif
#ifdef CAPWAP_WANTED
#include "capwapclicmd.def"
#include "capwapclishcmd.def"
#endif
#ifdef FSB_WANTED
#include "fsbcmd.def"
#endif
 
    
    
    
    
    
    
    
               MODE:USEREXEC PROMPT:CliGetUserExecPrompt ADD_GROUP:USEREXEC_CMDS
#ifdef L2TPV3_WANTED
                    ADD_GROUP:L2TP_SHOW_CMDS
#endif
#ifdef RM_WANTED
                    ADD_GROUP:RM_USEREXEC_CMDS
#endif
#ifdef HB_WANTED
                    ADD_GROUP:HB_USEREXEC_CMDS
#endif
#ifdef ICCH_WANTED
                    ADD_GROUP:ICCH_USEREXEC_CMDS
#endif
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      ADD_GROUP:CFA_EXEC_CMDS ADD_GROUP:STD_CFA_EXEC_CMDS ADD_GROUP:CFA_DBG_CMDS
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_EXEC_CMDS
#endif
#ifdef LLDP_WANTED
                    ADD_GROUP:LLDP_SHOW_CMDS
#endif
#ifdef EOAM_WANTED
                    ADD_GROUP:EOAM_SHOW_CMDS
#ifdef EOAM_FM_WANTED
                    ADD_GROUP:FM_SHOW_CMDS
#endif
#endif
#ifdef ECFM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ECFM_SHOW_CMDS ADD_GROUP:ECFM_CLEAR_CMDS ADD_GROUP:ECFM_LBLT_CMDS
    ADD_GROUP:ECFMTP_LBLT_CMDS
    ADD_GROUP:ECFMTP_SHOW_CMDS ADD_GROUP:ECFMTP_CLEAR_CMDS
#endif
#ifdef LA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
          ADD_GROUP:LA_EXEC_CMDS ADD_GROUP:LA_DBG_CMDS ADD_GROUP:STD_LA_DBG_CMDS
#endif
#ifdef POE_WANTED
                    ADD_GROUP:POE_EXEC_CMDS
#endif
#if defined IGS_WANTED || defined MLDS_WANTED
                    ADD_GROUP:SNOOP_EXEC_CMDS ADD_GROUP:SNOOP_DEBUG_CMDS
#endif
#ifdef SNMP_3_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      ADD_GROUP:SNMP3_SHOW_CMDS ADD_GROUP:SNMP3_WALK_CMD ADD_GROUP:SNX_SHOW_CMDS
#endif
#ifdef BGP_WANTED
                    ADD_GROUP:BGP_SHOW_CMDS
#endif
#ifdef ISS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
         ADD_GROUP:SYS_EXEC_CMDS ADD_GROUP:STD_EXEC_CMDS ADD_GROUP:ENT_SHOW_CMDS
#ifdef DIFFSRV_WANTED
                    ADD_GROUP:DIFFSRV_EXEC_CMDS
#endif
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     ADD_GROUP:ACL_PEXCFG_GRP ADD_GROUP:L4S_USEREXEC_CMDS ADD_GROUP:PI_EXEC_CMDS
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_SHOW_CMDS ADD_GROUP:PBB_PEXCFG_CMDS
#endif
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_SHOW_CMDS ADD_GROUP:VLAN_DBG_CMDS
#ifdef VLAN_TEST_WANTED
                    ADD_GROUP:VLAN_TEST_CMDS
#endif
#ifdef PB_WANTED
                    ADD_GROUP:PB_SHOW_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_SHOW_CMDS ADD_GROUP:GARP_DBG_CMDS
#endif
#ifdef EVB_WANTED
                    ADD_GROUP:VLAN_EVB_SHOW_CMDS
#endif
#endif
#ifdef HOTSPOT2_WANTED
                    ADD_GROUP:HS_SHOW_CMDS
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_DEBUG_CMDS
#endif
#ifdef IP_WANTED
                    ADD_GROUP:FSIP_PEXCFG_GRP
#endif
#ifdef ARP_TEST_WANTED
                    ADD_GROUP:ARP_TEST_CMDS
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
 
    
    
    
    
    
    
    
    
    
    
    
    
          ADD_GROUP:IP_SHOW_CMDS ADD_GROUP:TCP_SHOW_CMDS ADD_GROUP:UDP_SHOW_CMDS
#endif
#ifdef NPAPI_WANTED
                    ADD_GROUP:NP_SHOW_CMDS
#endif
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_USEREXEC_GRP
#endif
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
                    ADD_GROUP:CRYPT_USER_EXEC_GRP
#endif
#ifdef PNAC_WANTED
                    ADD_GROUP:PNAC_EXEC_CMDS ADD_GROUP:STD_PNAC_EXEC_CMDS
#endif
#ifdef WPS_WANTED
                    ADD_GROUP:WPS_EXEC_CMDS
#endif
#ifdef RSNA_WANTED
                    ADD_GROUP:RSNA_EXEC_CMDS
#ifdef WPA_WANTED
                    ADD_GROUP:WPA_EXEC_CMDS
#endif
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_SHOW_CMDS
#endif
#ifdef RADIUS_WANTED
                    ADD_GROUP:RADIUS_EXEC_CMDS
#endif
#ifdef TACACS_WANTED
                    ADD_GROUP:TACACS_PEXCFG_GRP
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_PEXCFG_GRP
#endif
#ifdef DSMON_WANTED
                    ADD_GROUP:DSMON_PEXCFG_CMDS
#endif
#ifdef RMON2_WANTED
                    ADD_GROUP:RMON2_PEXCFG_CMDS
#endif
#ifdef SSL_WANTED
                    ADD_GROUP:SSL_PRVEX_CMDS
#endif
#ifdef SSH_WANTED
                    ADD_GROUP:SSH_PRVEX_CMDS
#endif
#ifdef SYSLOG_WANTED
                    ADD_GROUP:SYSLOG_PRVEX_CMDS
#endif
#ifdef SNTP_WANTED
                    ADD_GROUP:SNTP_EXEC_CMDS
#endif
#ifdef DHCP_SRV_WANTED
                    ADD_GROUP:DHCPS_PEXCFG_GRP
#endif
#ifdef DHCP_RLY_WANTED
                    ADD_GROUP:DHCPR_PEXCFG_GRP
#endif
#ifdef DHCPC_WANTED
                    ADD_GROUP:DHCPC_PEXCFG_GRP
#endif
#ifdef VRRP_WANTED
                    ADD_GROUP:VRRP_DISPLAY_CMDS
#endif
#ifdef RRD_WANTED
                    ADD_GROUP:RRD_DISPLAY_GRP
#endif
#if  defined (IP_WANTED) || defined(LNXIP4_WANTED)
                    ADD_GROUP:PING_PEXCFG_GRP
#endif
#ifdef RRD6_WANTED
                    ADD_GROUP:RRD6_DISPLAY_GRP
#endif
#ifdef RIP_WANTED
                    ADD_GROUP:RIP_MI_UEXCFG_GRP
#endif
#ifdef RIP6_WANTED
                    ADD_GROUP:RIP6_UEXCFG_GRP
#endif
#ifdef DVMRP_WANTED
                    ADD_GROUP:DVMRP_EXEC_CMD
#endif
#ifdef NAT_WANTED
                    ADD_GROUP:NAT_EXEC_CMDS
#endif
#ifdef FIREWALL_WANTED
                    ADD_GROUP:FWL_EXEC_CMDS
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
                    ADD_GROUP:PIM_SHOW_CMDS
#endif
#ifdef MSDP_WANTED
                    ADD_GROUP:MSDP_SHOW_CMDS ADD_GROUP:MSDPV6_SHOW_CMDS
#endif
#ifdef IGMP_WANTED
                    ADD_GROUP:IGMP_SHOW_CMDS
#endif
#ifdef MLD_WANTED
                    ADD_GROUP:MLD_SHOW_CMDS
#endif
#ifdef IGMPPRXY_WANTED
                    ADD_GROUP:IGP_SHOW_CMDS
#endif
#ifdef MRI_WANTED
                    ADD_GROUP:MRI_SHOW_CMDS
#endif
#ifdef MFWD_WANTED
                    ADD_GROUP:MFWD_SHOW_CMDS
#endif
#ifdef OSPF_WANTED
                    ADD_GROUP:OSPF_PRVEXEC_CMD_MI
#endif
#ifdef OSPF3_WANTED
                    ADD_GROUP:OSPF3_EXEC
#endif
#ifdef DHCP6_SRV_WANTED
                    ADD_GROUP:DHCP6_SRV_EXEC
#endif
#ifdef DHCP6_CLNT_WANTED
                    ADD_GROUP:DHCP6_CLNT_EXEC
#endif
#ifdef DHCP6_RLY_WANTED
                    ADD_GROUP:DHCP6_RLY_EXEC_CMDS
#endif
#ifdef MBSM_WANTED
                    ADD_GROUP:MBSM_SHOW_CMDS
#endif
#ifdef BCMX_WANTED
#ifdef KERNEL_WANTED
                    ADD_GROUP:KERN_SHOW_CMDS
#endif
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_SHOW_CMDS
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_EXEC_CMDS ADD_GROUP:QOS_DEBUG_CMDS
#endif
#ifdef DCBX_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ETS_EXEC_CMDS
    ADD_GROUP:PFC_EXEC_CMDS ADD_GROUP:DCBX_EXEC_CMDS ADD_GROUP:DCBX_DEBUG_CMDS
    ADD_GROUP:APP_PRI_EXEC_CMDS ADD_GROUP:CEE_EXEC_CMDS
#endif                            /*DCBX_WANTED */
#ifdef CN_WANTED
                    ADD_GROUP:CN_EXEC_CMDS
#endif                            /* CN_WANTED */
#ifdef RBRG_WANTED
                    ADD_GROUP:RBRG_EXEC_CMDS ADD_GROUP:RBRG_DEBUG_CMDS
#endif
#ifdef ELPS_WANTED
                    ADD_GROUP:ELPS_DEBUG_CMDS ADD_GROUP:ELPS_SHOW_CMD
#endif
#ifdef ERPS_WANTED
                    ADD_GROUP:ERPS_DEBUG_CMDS ADD_GROUP:ERPS_SHOW_CMD
#endif
#ifdef Y1564_WANTED
                    ADD_GROUP:Y1564_SHOW_CMDS ADD_GROUP:Y1564_DEBUG_CMDS
#ifdef Y1564_TEST_WANTED
                    ADD_GROUP:Y1564_TEST_CMDS
#endif
#endif
#ifdef RFC6374_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:MPLS_PM_SHOW_CMDS
    ADD_GROUP:MPLS_PM_TEST_CMDS
    ADD_GROUP:MPLS_PM_DEBUG_CMDS ADD_GROUP:MPLS_PM_CLEAR_CMDS
#endif
#ifdef RFC2544_WANTED
                    ADD_GROUP:R2544_SHOW_CMDS ADD_GROUP:R2544_DEBUG_CMDS
#ifdef RFC2544_TEST_WANTED
                    ADD_GROUP:R2544_TEST_CMDS
#endif
#endif
#ifdef PBBTE_WANTED
                    ADD_GROUP:PBBTE_DEBUG_CMDS ADD_GROUP:PBBTE_SHOW_CMDS
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_DEBUG_CMDS ADD_GROUP:PTP_SHOW_CMD
#endif
#ifdef BFD_WANTED
                    ADD_GROUP:BFD_DEBUG_CMDS ADD_GROUP:BFD_SHOW_CMD
#endif
#ifdef DNS_WANTED
                    ADD_GROUP:DNS_SHOW_CMDS
#endif
#ifdef PPP_WANTED
                    ADD_GROUP:PPP_EXEC_CMDS ADD_GROUP:PPP_INT_CMDS
#endif
#ifdef WLC_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:WSSCFG_SHOW_CMDS ADD_GROUP:FS11AC_SHOW_CMDS
    ADD_GROUP:BATCH_SHOW_CMDS
#ifdef WSSCUST_WANTED
                    ADD_GROUP:SQL_SHOW_CMDS
#endif
#endif
#ifdef CAPWAP_WANTED
                    ADD_GROUP:CAPWAP_SHOW_CMDS
#endif
#ifdef WLC_WANTED
#ifdef WSSUSER_WANTED
                    ADD_GROUP:WSSUSER_SHOW_CMDS
#endif
#ifdef RFMGMT_WANTED
                    ADD_GROUP:RFMGMT_SHOW_CMDS ADD_GROUP:RFM_11H_SHOW_CMDS
#endif
#ifdef ROGUEAP_WANTED
                    ADD_GROUP:RFM_ROGUE_SHOW_CMDS
#endif
#endif
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:EXEC
    PARENT:USEREXEC
    PROMPT:CliGetExecPrompt ADD_GROUP:USEREXEC_CMDS ADD_GROUP:PRVEXEC_CMDS
    ADD_GROUP:STD_PRVEXEC_CMDS
#ifdef L2TPV3_WANTED
                    ADD_GROUP:L2TP_SHOW_CMDS
#endif
#ifdef MPLS_WANTED
                    ADD_GROUP:FM_DEBUG_CMDS
#endif
#ifdef ISSMEM_WANTED
                    ADD_GROUP:ISSSZ_SHOW_CMDS
#endif
#ifdef MBSM_WANTED
                    ADD_GROUP:MBSM_SHOW_CMDS
#endif
#ifdef CLKIWF_WANTED
                    ADD_GROUP:CLK_IWF_SHOW_CMDS
#endif
#ifdef RM_WANTED
                    ADD_GROUP:RM_USEREXEC_CMDS ADD_GROUP:RM_EXEC_CMDS
#endif
#ifdef HB_WANTED
                    ADD_GROUP:HB_USEREXEC_CMDS
#endif
#ifdef ICCH_WANTED
                    ADD_GROUP:ICCH_USEREXEC_CMDS
#endif
#ifdef TLM_WANTED
                    ADD_GROUP:TLM_PRVEXEC_CMDS
#endif
#ifdef LA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
          ADD_GROUP:LA_EXEC_CMDS ADD_GROUP:LA_DBG_CMDS ADD_GROUP:STD_LA_DBG_CMDS
#endif
#ifdef ISIS_WANTED
                    ADD_GROUP:ISIS_GBL_CMDS
#endif
#ifdef LLDP_WANTED
                    ADD_GROUP:LLDP_PEXCFG_CMDS ADD_GROUP:LLDP_SHOW_CMDS
#endif
#ifdef EOAM_WANTED
                    ADD_GROUP:EOAM_PEXCFG_CMDS ADD_GROUP:EOAM_SHOW_CMDS
#ifdef EOAM_FM_WANTED
                    ADD_GROUP:FM_PEXCFG_CMDS ADD_GROUP:FM_SHOW_CMDS
#endif
#endif
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_EXCE_CMDS
#endif
#ifdef SYNCE_WANTED
                    ADD_GROUP:SYNCE_SHOW_CMDS ADD_GROUP:SYNCE_DEBUG_CMDS
#endif
#ifdef TTCP_WANTED
                    ADD_GROUP:TTCP_PACKET_CMDS
#endif
#ifdef ECFM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ECFM_SHOW_CMDS
    ADD_GROUP:ECFM_PEXCFG_CMDS
    ADD_GROUP:ECFM_CLEAR_CMDS ADD_GROUP:ECFM_LBLT_CMDS
    ADD_GROUP:ECFMTP_LBLT_CMDS
    ADD_GROUP:ECFMTP_PEXCFG_CMDS
    ADD_GROUP:ECFMTP_CLEAR_CMDS ADD_GROUP:ECFMTP_SHOW_CMDS
#endif
#ifdef VCM_WANTED
#ifdef MI_WANTED
                    ADD_GROUP:VCM_EXEC_CMDS ADD_GROUP:SISP_SHOW_CMDS
#endif
#ifdef VRF_WANTED
                    ADD_GROUP:VCM_IP_EXEC_CMDS
#endif
#endif
#ifdef POE_WANTED
                    ADD_GROUP:POE_EXEC_CMDS
#endif
#if defined IGS_WANTED || defined MLDS_WANTED
                    ADD_GROUP:SNOOP_EXEC_CMDS ADD_GROUP:SNOOP_DEBUG_CMDS
#endif
#ifdef SNMP_3_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      ADD_GROUP:SNMP3_SHOW_CMDS ADD_GROUP:SNMP3_WALK_CMD ADD_GROUP:SNX_SHOW_CMDS
#endif
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_EXEC_CMDS ADD_GROUP:STD_CFA_EXEC_CMDS ADD_GROUP:CFA_DBG_CMDS
    ADD_GROUP:IPDB_EXEC_CMDS
#ifdef CFA_TEST_WANTED
                    ADD_GROUP:CFA_TEST_EXEC_CMDS
#endif
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      ADD_GROUP:IPDB_SHOW_CMDS ADD_GROUP:L2DS_SHOW_CMDS ADD_GROUP:L2DS_EXEC_CMDS
#endif
#ifdef ISS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
         ADD_GROUP:SYS_EXEC_CMDS ADD_GROUP:STD_EXEC_CMDS ADD_GROUP:ENT_SHOW_CMDS
#ifdef FLFR_WANTED
                    ADD_GROUP:FLFR_PRV_EXEC_GRP
#endif
#ifdef DIFFSRV_WANTED
                    ADD_GROUP:DIFFSRV_EXEC_CMDS
#endif
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
     ADD_GROUP:ACL_PEXCFG_GRP ADD_GROUP:L4S_USEREXEC_CMDS ADD_GROUP:PI_EXEC_CMDS
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_SHOW_CMDS ADD_GROUP:PBB_PEXCFG_CMDS
#endif
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_SHOW_CMDS ADD_GROUP:VLAN_DBG_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_SHOW_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_SHOW_CMDS ADD_GROUP:GARP_DBG_CMDS
#ifdef EVB_WANTED
                    ADD_GROUP:VLAN_EVB_SHOW_CMDS
#endif
#endif
#endif
#ifdef HOTSPOT2_WANTED
                    ADD_GROUP:HS_SHOW_CMDS
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_DEBUG_CMDS
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_SHOW_CMDS
#endif
#ifdef ELMI_WANTED
                    ADD_GROUP:ELMI_PRV_EXEC_GRP
#endif
#ifdef DHCP6_SRV_WANTED
                    ADD_GROUP:DHCP6_SRV_EXEC
#endif
#ifdef DHCP6_CLNT_WANTED
                    ADD_GROUP:DHCP6_CLNT_EXEC
#endif
#ifdef DHCP6_RLY_WANTED
                    ADD_GROUP:DHCP6_RLY_EXEC_CMDS
#endif
#ifdef LCM_WANTED
                    ADD_GROUP:LCM_PRV_EXEC_GRP
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:PRV_EXEC_GRP ADD_GROUP:RSTP_EXEC_CFG_GRP
#endif
#ifdef IP_WANTED
                    ADD_GROUP:FSIP_PEXCFG_GRP
#endif
#ifdef IP_RTMTEST_WANTED
                    ADD_GROUP:RTM_TEST_CMDS
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
 
    
    
    
    
    
    
    
    
    
    
    
    
          ADD_GROUP:IP_SHOW_CMDS ADD_GROUP:TCP_SHOW_CMDS ADD_GROUP:UDP_SHOW_CMDS
#endif
#ifdef TCP_TEST_WANTED
                    ADD_GROUP:TCP_TEST_CMDS
#endif
#ifdef ISS_HEALTH_TEST_WANTED
                    ADD_GROUP:ISS_TEST_CMDS
#endif
#ifdef NPAPI_WANTED
                    ADD_GROUP:NP_SHOW_CMDS
#endif
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_PRUEXEC_GRP ADD_GROUP:IP6_USEREXEC_GRP
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_EXEC_CMDS
#endif
#if  defined (IP_WANTED) || defined (LNXIP4_WANTED)
                    ADD_GROUP:TRACE_CFG_CMDS
#endif
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
                    ADD_GROUP:CRYPT_PRIV_EXEC_GRP ADD_GROUP:CRYPT_USER_EXEC_GRP
#endif
#ifdef PNAC_WANTED
                    ADD_GROUP:PNAC_EXEC_CMDS ADD_GROUP:STD_PNAC_EXEC_CMDS
#endif
#ifdef WPS_WANTED
                    ADD_GROUP:WPS_EXEC_CMDS
#endif
#ifdef RSNA_WANTED
                    ADD_GROUP:RSNA_EXEC_CMDS
#ifdef WPA_WANTED
                    ADD_GROUP:WPA_EXEC_CMDS
#endif
#endif
#ifdef FSB_WANTED
                    ADD_GROUP:FSB_EXEC_CMDS
#endif
#ifdef RADIUS_WANTED
                    ADD_GROUP:RADIUS_EXEC_CMDS
#endif
#ifdef TACACS_WANTED
                    ADD_GROUP:TACACS_PEXCFG_GRP
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_PEXCFG_GRP
#endif
#ifdef RMON2_WANTED
                    ADD_GROUP:RMON2_PEXCFG_CMDS
#endif
#ifdef DSMON_WANTED
                    ADD_GROUP:DSMON_PEXCFG_CMDS
#endif
#ifdef SSL_WANTED
                    ADD_GROUP:SSL_PRVEX_CMDS
#endif
#ifdef SSH_WANTED
                    ADD_GROUP:SSH_PRVEX_CMDS
#endif
#ifdef SYSLOG_WANTED
                    ADD_GROUP:SYSLOG_PRVEX_CMDS
#endif
#ifdef SNTP_WANTED
                    ADD_GROUP:SNTP_EXEC_CMDS
#endif
#ifdef DHCP_SRV_WANTED
                    ADD_GROUP:DHCPS_PEXCFG_GRP
#endif
#ifdef DHCP_RLY_WANTED
                    ADD_GROUP:DHCPR_PEXCFG_GRP
#endif
#ifdef DHCPC_WANTED
                    ADD_GROUP:DHCPC_PEXCFG_GRP
#endif
#ifdef DVMRP_WANTED
                    ADD_GROUP:DVMRP_EXEC_CMD
#endif
#ifdef NAT_WANTED
                    ADD_GROUP:NAT_EXEC_CMDS
#endif
#ifdef FIREWALL_WANTED
                    ADD_GROUP:FWL_EXEC_CMDS
#endif
#if defined (IDS_WANTED)
                    ADD_GROUP:SEC_SHOW_CMDS
#endif
#if defined (SMOD_WANTED) && defined (FIREWALL_WANTED)
                    ADD_GROUP:SEC_DBG_COMMANDS
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
                    ADD_GROUP:PIM_SHOW_CMDS
#endif
#ifdef WEBNM_WANTED
                    ADD_GROUP:HTTP_SHOW_CMDS
#endif
#ifdef MSDP_WANTED
                    ADD_GROUP:MSDP_SHOW_CMDS ADD_GROUP:MSDPV6_SHOW_CMDS
#endif
#ifdef IGMP_WANTED
                    ADD_GROUP:IGMP_SHOW_CMDS
#endif
#ifdef MLD_WANTED
                    ADD_GROUP:MLD_SHOW_CMDS
#endif
#ifdef IGMPPRXY_WANTED
                    ADD_GROUP:IGP_SHOW_CMDS
#endif
#ifdef MRI_WANTED
                    ADD_GROUP:MRI_SHOW_CMDS
#endif
#ifdef MFWD_WANTED
                    ADD_GROUP:MFWD_SHOW_CMDS
#endif
#ifdef OSPF_WANTED
                    ADD_GROUP:OSPF_EXEC_CMDS_MI ADD_GROUP:OSPF_PRVEXEC_CMD_MI
#endif
#ifdef OSPFTE_WANTED
                    ADD_GROUP:OSPFTE_PRVEXEC_CMDS
#endif
#ifdef OSPF3_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        ADD_GROUP:OSPF3_EXEC ADD_GROUP:OSPF3_PRVEXEC ADD_GROUP:STD_OSPF3_PRVEXEC
#endif
#ifdef VRRP_WANTED
                    ADD_GROUP:VRRP_DISPLAY_CMDS
#endif
#ifdef RRD_WANTED
                    ADD_GROUP:RRD_DISPLAY_GRP
#endif
#if  defined (IP_WANTED) || defined(LNXIP4_WANTED)
                    ADD_GROUP:PING_PEXCFG_GRP
#endif
#ifdef ROUTEMAP_WANTED
                    ADD_GROUP:RMAP_SHOW_CMDS
#endif
#ifdef RRD6_WANTED
                    ADD_GROUP:RRD6_DISPLAY_GRP
#endif
#ifdef RIP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:RIP_MI_UEXCFG_GRP ADD_GROUP:RIP_MI_PEXCFG_GRP
    ADD_GROUP:STD_RIP_PEXCFG
#endif
#ifdef RIP6_WANTED
                    ADD_GROUP:RIP6_PEXCFG_GRP
#endif
#ifdef OPENFLOW_WANTED
                    ADD_GROUP:OFC_SHOW_CMDS
#endif
#ifdef MPLS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:MPLS_SHOW_CMDS ADD_GROUP:MPLS_OAM_SHOW_CMDS
    ADD_GROUP:PSWRED_SHOW_CMD
#ifdef L2VPN_TEST_WANTED
                    ADD_GROUP:L2VPN_UT_CMDS
#endif
#ifdef MPLS_TEST_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:MPLS_TP_TEST_CMDS ADD_GROUP:MPLS_VCCV_TEST_CMDS
    ADD_GROUP:MPLS_P2MP_TEST_CMDS
    ADD_GROUP:MPLS_L3VPN_TEST_CMD ADD_GROUP:MPLS_LDP_TEST_CMD
#endif
#ifdef BFD_TEST_WANTED
                    ADD_GROUP:BFD_TEST_CMDS
#endif
#ifdef LSPP_TEST_WANTED
                    ADD_GROUP:LSPP_TEST_CMDS
#endif
#ifdef MPLS_SIG_WANTED
                    ADD_GROUP:MPLS_UEXEC_CMDS
#endif
                    ADD_GROUP:MPLS_OAM_UEXEC_CMDS
#ifdef LSPP_WANTED
                    ADD_GROUP:LSPP_USER_EXEC_CMDS ADD_GROUP:LSPP_SHOW_CMDS
#endif
#endif
#ifdef BGP_WANTED
                    ADD_GROUP:BGP_SHOW_CMDS ADD_GROUP:BGP_PEXCFG_GRP
#endif
#ifdef BEEP_SERVER_WANTED
                    ADD_GROUP:BEEP_SRV_SHOW_GRP
#endif
#ifdef BCMX_WANTED
#ifdef KERNEL_WANTED
                    ADD_GROUP:KERN_SHOW_CMDS
#endif
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_SHOW_CMDS
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_EXEC_CMDS ADD_GROUP:QOS_DEBUG_CMDS
#endif
#ifdef DCBX_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ETS_EXEC_CMDS
    ADD_GROUP:PFC_EXEC_CMDS ADD_GROUP:DCBX_EXEC_CMDS ADD_GROUP:DCBX_DEBUG_CMDS
    ADD_GROUP:APP_PRI_EXEC_CMDS ADD_GROUP:CEE_EXEC_CMDS
#endif                            /*DCBX_WANTED */
#ifdef CN_WANTED
                    ADD_GROUP:CN_EXEC_CMDS
#endif                            /* CN_WANTED */
#ifdef RBRG_WANTED
                    ADD_GROUP:RBRG_EXEC_CMDS ADD_GROUP:RBRG_DEBUG_CMDS
#endif
#ifdef TAC_WANTED
                    ADD_GROUP:TACM_DEBUG_CMDS ADD_GROUP:TACM_EXEC_CMDS
#endif
#ifdef ERPS_WANTED
                    ADD_GROUP:ERPS_DEBUG_CMDS ADD_GROUP:ERPS_SHOW_CMD
#endif
#ifdef Y1564_WANTED
                    ADD_GROUP:Y1564_SHOW_CMDS ADD_GROUP:Y1564_DEBUG_CMDS
#ifdef Y1564_TEST_WANTED
                    ADD_GROUP:Y1564_TEST_CMDS
#endif
#endif
#ifdef RFC6374_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:MPLS_PM_SHOW_CMDS
    ADD_GROUP:MPLS_PM_TEST_CMDS
    ADD_GROUP:MPLS_PM_DEBUG_CMDS ADD_GROUP:MPLS_PM_CLEAR_CMDS
#endif
#ifdef RFC2544_WANTED
                    ADD_GROUP:R2544_SHOW_CMDS ADD_GROUP:R2544_DEBUG_CMDS
#ifdef RFC2544_TEST_WANTED
                    ADD_GROUP:R2544_TEST_CMDS
#endif
#endif
#ifdef PBBTE_WANTED
                    ADD_GROUP:PBBTE_DEBUG_CMDS ADD_GROUP:PBBTE_SHOW_CMDS
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_DEBUG_CMDS ADD_GROUP:PTP_SHOW_CMD
#ifdef PTP_TEST_WANTED
                    ADD_GROUP:PTP_TEST_CMDS
#endif
#endif
#ifdef ELPS_WANTED
                    ADD_GROUP:ELPS_DEBUG_CMDS ADD_GROUP:ELPS_SHOW_CMD
#endif
#ifdef DNS_WANTED
                    ADD_GROUP:DNS_SHOW_CMDS
#endif
#ifdef BFD_WANTED
                    ADD_GROUP:BFD_DEBUG_CMDS ADD_GROUP:BFD_SHOW_CMD
#endif
#ifdef PPP_WANTED
                    ADD_GROUP:PPP_EXEC_CMDS ADD_GROUP:PPP_INT_CMDS
#endif
#ifdef FIPS_WANTED
                    ADD_GROUP:FIPS_DEBUG_CMDS ADD_GROUP:FIPS_EXEC_CMDS
#endif
#ifdef SYNCE_UT_WANTED
                    ADD_GROUP:SYNCE_TEST_CMDS
#endif
#ifdef WLC_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:WSSCFG_SHOW_CMDS ADD_GROUP:FS11AC_SHOW_CMDS
    ADD_GROUP:BATCH_SHOW_CMDS
#ifdef WSSCUST_WANTED
                    ADD_GROUP:SQL_SHOW_CMDS
#endif
#endif
#ifdef CAPWAP_WANTED
                    ADD_GROUP:CAPWAP_SHOW_CMDS ADD_GROUP:CAPWAP_DEBUG_CMDS
#endif
#ifdef WLC_WANTED
#ifdef WSSUSER_WANTED
                    ADD_GROUP:WSSUSER_SHOW_CMDS ADD_GROUP:WSSUSER_EXEC_CMDS
#endif
#ifdef RFMGMT_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:RFMGMT_SHOW_CMDS ADD_GROUP:RFMGMT_DEBUG_CMDS
    ADD_GROUP:RFM_11H_SHOW_CMDS
#endif
#ifdef ROGUEAP_WANTED
                    ADD_GROUP:RFM_ROGUE_SHOW_CMDS
#endif
#endif
#ifdef VXLAN_WANTED
                    ADD_GROUP:VXLAN_SHOW_CMDS
#endif
#ifdef ISSU_WANTED
                    ADD_GROUP:ISSU_SHOW_CMDS ADD_GROUP:ISSU_CONFIG_CMDS
#endif
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:CONFIGURE
    PARENT:EXEC PROMPT:CliGetConfigurePrompt ADD_GROUP:CONFIG_CMDS
    ADD_GROUP:STD_CONFIG_CMDS
#ifdef DHCPC_WANTED
                    ADD_GROUP:DHCPC_CFG_GRP
#endif
#ifdef NPAPI_TEST_WANTED
                    ADD_GROUP:NPAPI_TEST_CMDS
#endif
#ifdef FIPS_WANTED
                    ADD_GROUP:FIPS_CONFIG_CMDS
#endif
#ifdef CLKIWF_WANTED
                    ADD_GROUP:CLK_IWF_CONFIG_CMDS
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_CONFIG_CMDS
#endif
#ifdef OPENFLOW_WANTED
                    ADD_GROUP:OFC_CFG_CMDS
#endif
#ifdef OFC_TEST_SERVER_WANTED
                    ADD_GROUP:OFS_TST_CFG_CMDS
#endif
#ifdef VXLAN_TEST_WANTED
                    ADD_GROUP:VXLAN_TEST_CMDS
#endif
#ifdef RM_WANTED
                    ADD_GROUP:RM_CONFIG_CMDS
#endif
#ifdef ICCH_WANTED
                    ADD_GROUP:ICCH_CONFIG_CMDS
#endif
#ifdef PPP_WANTED
                    ADD_GROUP:PPP_CONFIG_CMDS
#endif
#ifdef LA_WANTED
                    ADD_GROUP:LA_CONFIG_CMDS ADD_GROUP:STD_LA_CONFIG_CMDS
#endif
#ifdef ISIS_WANTED
                    ADD_GROUP:ISIS_CONFIG_CMDS
#endif
#ifdef POE_WANTED
                    ADD_GROUP:POE_GLBCFG_CMDS
#endif
#if defined IGS_WANTED || defined MLDS_WANTED
                    ADD_GROUP:SNOOP_CONFIG_CMDS
#endif
#ifdef IGS_WANTED
                    ADD_GROUP:STD_SNOOP_CON_CMDS
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:RSTP_GLBCFG_GRP ADD_GROUP:STD_RSTP_GLBCFG_GRP
#endif
#ifdef VCM_WANTED
                    ADD_GROUP:VCM_CONF_MODE_CMDS ADD_GROUP:SISP_CONFIG_CMDS
#endif
#ifdef VCM_WANTED
#ifdef VRF_WANTED
                    ADD_GROUP:VCM_VRF_CONF_CMDS
#endif
#endif
#ifdef ELMI_WANTED
                    ADD_GROUP:ELMI_GLBCFG_GRP
#endif
#ifdef ECFM_WANTED                /* SI_WANTED */
                    ADD_GROUP:ECFM_GLBCFG_CMDS
#endif
#ifdef ELPS_WANTED
                    ADD_GROUP:ELPS_GBLCFG_CMDS
#endif
#ifdef Y1564_WANTED
                    ADD_GROUP:Y1564_GBLCFG_CMDS
#endif
#ifdef RFC2544_WANTED
                    ADD_GROUP:R2544_GBLCFG_CMDS
#endif
#ifdef EVCPRO_WANTED
                    ADD_GROUP:EVCPRO_GLBCFG_GRP
#endif
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_GLBCFG_CMDS
#endif
#ifdef BGP_WANTED
                    ADD_GROUP:BGP_GBL_CMDS
#endif
#ifdef TCP_WANTED
                    ADD_GROUP:TCP_GBL_CMDS
#endif
#ifdef TLM_WANTED
                    ADD_GROUP:TLM_TE_MODE
#endif
#ifdef BFD_WANTED
                    ADD_GROUP:BFD_CXT_GBL_CMDS
#endif
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_CONFIG_CMDS ADD_GROUP:STD_CFA_CONFIG_CMDS
    ADD_GROUP:IPDB_GBL_CMDS ADD_GROUP:L2DS_GBL_CMDS
#endif
#ifdef MPLS_WANTED
#ifdef MPLS_L3VPN_WANTED
                    ADD_GROUP:MPLS_L3VPN_GBL_CMDS
#endif
                    ADD_GROUP:MPLS_CONFIG_CMDS
#ifdef LDP_TEST_WANTED
                    ADD_GROUP:LDP_TST_CFG_CMDS
#endif
                    ADD_GROUP:MPLS_OAM_CMDS ADD_GROUP:MPLS_TE_CFG_CMDS
#ifdef LSPP_WANTED
                    ADD_GROUP:LSPP_CONFIG_CMDS
#endif
                    ADD_GROUP:MPLS_VFI_CMDS
#endif
#ifdef ISS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:SYS_CONFIG_CMDS
    ADD_GROUP:STD_ACL_CFG_CMDS ADD_GROUP:STD_SYS_CONFIG_CMDS
    ADD_GROUP:ENT_CFG_CMDS ADD_GROUP:HW_CONFIG_CMDS
#ifdef DIFFSRV_WANTED
                    ADD_GROUP:DIFFSRV_CONFIG_CMDS
#endif
                    ADD_GROUP:ACL_GLBCFG_GRP ADD_GROUP:L4S_GLBCFG_GRP
#endif
#ifdef PBB_WANTED
                    ADD_GROUP:PBB_CONFIG_CMDS
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_GBL_CMDS
#endif
#endif
#ifdef HOTSPOT2_WANTED
                    ADD_GROUP:HS_GBL_CMDS
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_GBL_CMDS ADD_GROUP:STD_VLAN_GBL_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_GBL_CMDS
#endif
#ifdef HB_WANTED
                    ADD_GROUP:HB_MODE_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_GBL_CMDS ADD_GROUP:STD_GARP_GBL_CMDS
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_GBL_CMDS
#endif
#ifdef EVB_WANTED
                    ADD_GROUP:VLAN_EVB_GBL_CMDS
#endif
#endif
#ifdef ERPS_WANTED
                    ADD_GROUP:ERPS_CXT_GBL_CMDS
#endif
#ifdef Y1564_WANTED
                    ADD_GROUP:Y1564_CXT_GBL_CMDS
#endif
#ifdef RFC6374_WANTED
                    ADD_GROUP:MPLS_PM_CONFIG_CMDS
#endif
#ifdef RFC2544_WANTED
                    ADD_GROUP:R2544_CXT_GBL_CMDS
#endif
#ifdef ELPS_WANTED
                    ADD_GROUP:ELPS_CXT_GBL_CMDS
#endif
#ifdef MPLS_WANTED
                    ADD_GROUP:PSWRED_GLOBAL_CMDS
#endif
#ifdef ECFM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      ADD_GROUP:ECFM_CFG_CMDS ADD_GROUP:ECFM_VLAN_CMDS ADD_GROUP:ECFMTP_CFG_CMDS
#endif
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_CFG_CMDS
#endif
#ifdef PNAC_WANTED
                    ADD_GROUP:PNAC_CONFIG_CMDS ADD_GROUP:STD_PNAC_CONF_CMDS
#endif
#ifdef WPS_WANTED
                    ADD_GROUP:WPS_CONFIG_CMDS
#endif
#ifdef RSNA_WANTED
                    ADD_GROUP:RSNA_CONFIG_CMDS
#ifdef WPA_WANTED
                    ADD_GROUP:WPA_CONFIG_CMDS
#endif
#endif
#ifdef FSB_WANTED
                    ADD_GROUP:FSB_CONFIG_CMDS
#endif
#ifdef RADIUS_WANTED
                    ADD_GROUP:RADIUS_CONFIG_CMDS
#endif
#ifdef TACACS_WANTED
                    ADD_GROUP:TACACS_GLBCFG_GRP
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_GLBCFG_GRP
#endif
#ifdef RMON2_WANTED
                    ADD_GROUP:RMON2_GLBCFG_CMDS
#endif
#ifdef DSMON_WANTED
                    ADD_GROUP:DSMON_GLBCFG_CMDS
#endif
#ifdef SSL_WANTED
                    ADD_GROUP:SSL_GBLCFG_CMDS
#endif
#ifdef SSH_WANTED
                    ADD_GROUP:SSH_GBLCFG_CMDS
#endif
#ifdef SYSLOG_WANTED
                    ADD_GROUP:SYSLOG_GBLCFG_CMDS
#endif
#ifdef SNTP_WANTED
                    ADD_GROUP:SNTP_MODE
#endif
#ifdef BEEP_SERVER_WANTED
                    ADD_GROUP:BEEP_SRV_CFG_GRP
#endif
#ifdef DHCP_SRV_WANTED
                    ADD_GROUP:DHCPS_GLBCFG_GRP ADD_GROUP:STD_DHCPS_GLB_GRP
#endif
#ifdef DHCP_RLY_WANTED
                    ADD_GROUP:DHCPR_GLBCFG_GRP
#endif
#ifdef IP_WANTED
                    ADD_GROUP:FSIP_GLBCFG_GRP ADD_GROUP:STD_FSIP_GLBCFG
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                    ADD_GROUP:IP_CFG_CMDS ADD_GROUP:STD_IP_CFG_CMDS
#endif
#ifdef ROUTEMAP_WANTED
                    ADD_GROUP:RMAP_GBLCFG_CMDS
#endif
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_GLBCFG_GRP
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_MODE
#endif
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
                    ADD_GROUP:CRYPT_GLOB_CONF_GRP
#endif
#ifdef VRRP_WANTED
                    ADD_GROUP:VRRPGBL_CONFIG_CMDS
#endif
#ifdef RRD_WANTED
                    ADD_GROUP:RRD_GLBCFG_GRP
#endif
#ifdef RRD6_WANTED
                    ADD_GROUP:RRD6_GLBCFG_GRP
#endif
#ifdef RIP_WANTED
                    ADD_GROUP:RIP_MI_GLBCFG_GRP
#endif
#ifdef RIP6_WANTED
                    ADD_GROUP:RIP6_GLBCFG_GRP ADD_GROUP:STD_RIP6_GLBCFG
#endif
#ifdef DVMRP_WANTED
                    ADD_GROUP:DVMRP_GBL_CFG
#endif
#ifdef NAT_WANTED
                    ADD_GROUP:NAT_GLOBAL
#endif
#ifdef FIREWALL_WANTED
                    ADD_GROUP:FWL_MODE ADD_GROUP:BLK_WHITE_MODE
#endif
#ifdef L2TPV3_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ENT_CFG_CMDS ADD_GROUP:L2TP_CONFIG_CMDS
    ADD_GROUP:ENT_CFG_CMDS ADD_GROUP:SSN_MODE
#endif
#if defined (IDS_WANTED)
                    ADD_GROUP:SEC_CFG_CMDS
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
                    ADD_GROUP:PIM_GBL_CMDS ADD_GROUP:STD_PIM_GBL_CMDS
#endif
#ifdef MSDP_WANTED
                    ADD_GROUP:MSDP_GBL_CMDS ADD_GROUP:MSDPV6_GBL_CMDS
#endif
#ifdef WEBNM_WANTED
                    ADD_GROUP:HTTP_GBL_CMDS
#endif
#ifdef IGMP_WANTED
                    ADD_GROUP:IGMP_GBL_CMDS
#endif
#ifdef IGMPPRXY_WANTED
                    ADD_GROUP:IGP_GBL_CMDS ADD_GROUP:STD_IGP_GBL_CMDS
#endif
#ifdef MLD_WANTED
                    ADD_GROUP:MLD_GBL_CMDS
#endif
#ifdef MRI_WANTED
                    ADD_GROUP:MRI_GBLCFG_CMDS
#endif
#ifdef MFWD_WANTED
                    ADD_GROUP:MFWD_GBL_CMDS ADD_GROUP:STD_MFWD_GBL
#endif
#ifdef OSPF_WANTED
                    ADD_GROUP:OSPF_GLCFG_CMDS_MI ADD_GROUP:STD_OSPF_GLCFG_SI
#endif
#ifdef OSPF3_WANTED
                    ADD_GROUP:OSPF3_GLBCFG ADD_GROUP:STD_OSPF3_GLBCFG
#endif
#ifdef DHCP6_SRV_WANTED
                    ADD_GROUP:DHCP6_SRV_GLBCFG
#endif
#ifdef DHCP6_CLNT_WANTED
                    ADD_GROUP:DHCP6_CLNT_GLBCFG
#endif
#ifdef DHCP6_RLY_WANTED
                    ADD_GROUP:DHCP6_RLY_GBL_CMDS
#endif
#ifdef SNMP_3_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        ADD_GROUP:SNMP3_GBL_CMDS ADD_GROUP:SNMP3_WALK_CMD ADD_GROUP:SNX_GBL_CMDS
#endif
#ifdef MBSM_WANTED
                    ADD_GROUP:MBSM_CONFIG_CMDS
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_CONFIG_CMDS
#endif
#ifdef LLDP_WANTED
                    ADD_GROUP:LLDP_GLBCFG_CMDS
#endif
#ifdef EOAM_WANTED
                    ADD_GROUP:EOAM_GLBCFG_CMDS
#ifdef EOAM_FM_WANTED
                    ADD_GROUP:FM_GLBCFG_CMDS
#endif
#endif
#ifdef TAC_WANTED
                    ADD_GROUP:TACM_CONFIG_CMDS ADD_GROUP:STD_TACM_CMDS
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_CONFIG_CMDS ADD_GROUP:STD_QOS_CONFIG_CMDS
#endif
#ifdef MPLS_WANTED
                    ADD_GROUP:L2VPN_GBL_OAM_CMDS ADD_GROUP:L2VPN_PW_GBL_CMDS
#endif
#ifdef DCBX_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ETS_CONFIG_CMDS ADD_GROUP:PFC_CONFIG_CMDS
    ADD_GROUP:APP_PRI_CONFIG_CMDS ADD_GROUP:CEE_CONFIG_CMDS
#endif                            /*DCBX_WANTED */
#ifdef CN_WANTED
                    ADD_GROUP:CN_GLBL_CMDS ADD_GROUP:CN_CXT_GBL_CMDS
#endif                            /* CN_WANTED */
#ifdef RBRG_WANTED
                    ADD_GROUP:RBRG_CFG_CMDS
#endif
#ifdef DNS_WANTED
                    ADD_GROUP:DNS_CONFIG_CMDS
#endif
#ifdef SYNCE_WANTED
                    ADD_GROUP:SYNCE_GBL_CFG_CMDS
#endif
#ifdef WLC_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:WSSCFG_CONFIG_CMDS ADD_GROUP:FS11AC_CONFIG_CMDS
    ADD_GROUP:FS11N_CONFIG_CMDS ADD_GROUP:BATCH_CONFIG_CMD
#endif
#ifdef CAPWAP_WANTED
                    ADD_GROUP:CAPWAP_CONFIG_CMDS
#endif
#ifdef WLC_WANTED
#ifdef WSSCUST_WANTED
                    ADD_GROUP:SQL_CONFIG_CMD
#endif
#ifdef WSSUSER_WANTED
                    ADD_GROUP:WSSUSER_CONFIG_CMDS
#endif
#ifdef RFMGMT_WANTED
                    ADD_GROUP:RFMGMT_CONFIG_CMDS ADD_GROUP:RFMGMT_11H_CMDS
#endif
#ifdef ROGUEAP_WANTED
                    ADD_GROUP:RFMGMT_ROGUE_CMDS
#endif
#endif
#ifdef VXLAN_WANTED
                    ADD_GROUP:VXLAN_GBL_CFG_CMDS
#ifdef EVPN_VXLAN_WANTED
                    ADD_GROUP:VXLAN_EVID_CFG_GRP
#endif                            /* EVPN_VXLAN_WANTED */
#endif
#ifdef PTP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:PTP_GBL_CONFIG_CMDS
    MODE:CLKMODE
    PARENT:CONFIGURE PROMPT:PtpGetPtpCfgPrompt ADD_GROUP:PTP_CLK_CONFIG_CMDS
/* %% */
#endif
#ifdef EVCPRO_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:DOMAIN_EVC
    PARENT:CONFIGURE PROMPT:EvcProGetEvcConfigPrompt
    ADD_GROUP:EVCPRO_EVC_CFG_GRP
#ifdef LCM_WANTED
                    ADD_GROUP:LCM_EVC_CFG_GRP
#endif
#endif
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:LINE PARENT:CONFIGURE PROMPT:CliGetLinePrompt ADD_GROUP:LINECONFIG_CMDS
#ifdef SYSLOG_WANTED
                    ADD_GROUP:STD_LINECONFIG_CMDS
#endif
/* %% */
#ifdef RM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:REDUNDANCY
    PARENT:CONFIGURE PROMPT:RmCliGetConfigPrompt ADD_GROUP:RM_MODE_CMDS
#endif
#ifdef ICCH_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:ICCH_INST
    PARENT:CONFIGURE PROMPT:IcchCliGetConfigPrompt ADD_GROUP:ICCH_MODE_CMDS
#endif
#ifdef PPP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:TEST_PPP
    PARENT:CONFIGURE PROMPT:PppGetTestPppPrompt ADD_GROUP:PPP_TEST_CFG_CMDS
#endif
 
    
    
    
    
    
    
    
    
    
    
    
           MODE:L3SUBINTERFACE PARENT:CONFIGURE PROMPT:CfaGetL3SubIfConfigPrompt
#ifdef CFA_WANTED
                    ADD_GROUP:CFA_L3SUB_CMDS
#endif
#ifdef VRF_WANTED
                    ADD_GROUP:VCM_IP_INTF_CMDS
#endif
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_L3SUB_CMDS
#endif
                    ADD_GROUP:SYS_INT_CMDS
    /* Prompt function should be defined by the Interface Manager,
     * to get access to the interface mode commands.
     * Inside the prompt function the interface index should be updated
     * in CliContext using CLI_SET_IFINDEX. 
     * Interface manager should define commands/functions for implementing
     * mode change
     */
                    MODE:INTERFACE PARENT:CONFIGURE PROMPT:CfaGetIfConfigPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_INT_CMDS ADD_GROUP:CFA_INTERFACE_CMDS
    ADD_GROUP:CFA_INT_COMMON_CMDS ADD_GROUP:IPDB_IFACE_CMDS
    ADD_GROUP:L2DS_IFACE_CMDS
#ifdef OPENFLOW_WANTED
                    ADD_GROUP:OFC_PHY_INTF_CMDS
#endif
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_POLICY_INT_CMD
#endif
#ifdef EVCPRO_WANTED
                    ADD_GROUP:CFA_INT_UNI_CFG_GRP
#endif
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_INTF_CMDS
#endif
#ifdef DCBX_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ETS_INTF_CMDS ADD_GROUP:PFC_INTF_CMDS ADD_GROUP:DCBX_INTF_CMDS
    ADD_GROUP:APP_PRI_INTF_CMDS
#endif                            /*DCBX_WANTED */
#ifdef CN_WANTED
                    ADD_GROUP:CN_INTF_MODE_CMDS
#endif                            /* CN_WANTED */
#ifdef FSB_WANTED
                    ADD_GROUP:FSB_INTF_CMDS
#endif                            /* CN_WANTED */
#ifdef RBRG_WANTED
                    ADD_GROUP:RBRG_IF_CMDS
#endif
#ifdef EOAM_WANTED
                    ADD_GROUP:EOAM_INTFCFG_CMDS
#ifdef EOAM_TEST_WANTED
                    ADD_GROUP:TSTOAM_INTFCFG_CMDS
#endif
#ifdef EOAM_FM_WANTED
                    ADD_GROUP:FM_INTFCFG_CMDS
#endif
#endif
#ifdef LLDP_WANTED
                    ADD_GROUP:LLDP_INTFCFG_CMDS
#endif
#ifdef ECFM_WANTED
                    ADD_GROUP:ECFM_INTFCFG_CMDS
#endif
#ifdef ELMI_WANTED
                    ADD_GROUP:ELMI_INT_CFG_GRP
#endif
#ifdef LCM_WANTED
                    ADD_GROUP:LCM_INT_CFG_GRP
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:INT_CFG_GRP ADD_GROUP:STD_INT_CFG_GRP
#endif
#ifdef VCM_WANTED
#ifdef MI_WANTED
                    ADD_GROUP:VCM_INTF_MODE_CMDS ADD_GROUP:SISP_INT_CMDS
#endif
#endif
#ifdef LA_WANTED
                    ADD_GROUP:LA_INT_CMDS ADD_GROUP:STD_LA_INT_CMDS
#endif
#ifdef POE_WANTED
                    ADD_GROUP:POE_INT_CMDS
#endif
#ifdef MPLS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:L2VPN_PORTCFG_CMDS ADD_GROUP:L2VPN_PW_OAM_CMDS
    ADD_GROUP:L2VPN_PBKPCF_CMDS
#endif
#ifdef IGS_WANTED
                    ADD_GROUP:SNOOP_PORTCFG_CMDS ADD_GROUP:STD_SNP_PORT_CMDS
#endif
#ifdef ISS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:SYS_INT_CMDS ADD_GROUP:PI_INT_CMDS ADD_GROUP:ISS_EXT_INT_CMDS
    ADD_GROUP:ACL_INTCFG_GRP
#endif
#ifdef L2TPV3_WANTED
                    ADD_GROUP:XCNT_CONFIG_CMDS
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_IFACE_CMDS
#endif
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_IFACE_CMDS ADD_GROUP:STD_VLAN_IFACE_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_IFACE_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_IFACE_CMDS
#endif
#ifdef EVB_WANTED
                    ADD_GROUP:VLAN_EVB_INT_CMDS
#endif
#endif
#ifdef PNAC_WANTED
                    ADD_GROUP:PNAC_INT_CMDS ADD_GROUP:STD_PNAC_INT_CMDS
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_IFACE_CMDS
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_IFACE_CMDS
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_INTCFG_GRP
#endif
#ifdef SYNCE_WANTED
                    ADD_GROUP:SYNCE_INTF_CFG_CMDS
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_VLAN_CFG_CMDS ADD_GROUP:PTP_OPT_CFG_CMDS
#endif
#ifdef DIFFSRV_WANTED
#ifdef SWC
                    ADD_GROUP:DSCXE_IFACE_CMDS
#endif
                    ADD_GROUP:DIFFSRV_IFACE_CMDS
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_IFACE_CMDS
#endif
#ifdef DHCP_RLY_WANTED
                    ADD_GROUP:STD_DHCPR_IFACE_CMD
#endif
#ifdef SNTP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:SNTP PARENT:CONFIGURE PROMPT:SntpGetSntpConfigPrompt
    ADD_GROUP:SNTP_CONFIG_CMDS
#endif
#ifdef LCM_WANTED
 
    
    
    
                   MODE:DOMAIN_ESI PARENT:INTERFACE PROMPT:LcmGetEsiConfigPrompt
#ifdef EVCPRO_WANTED
                    ADD_GROUP:EVCPRO_ESI_CFG_GRP
#endif
#endif
#ifdef L2TPV3_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:XCNT PARENT:INTERFACE PROMPT:CliGetXconnectCfgPrompt
    ADD_GROUP:XCNT_CONFIG_IF_CMDS
#endif
#ifdef ECFM_WANTED
 
    
    
    
    
                  MODE:MEP_CONFIG PARENT:INTERFACE PROMPT:EcfmGetMepConfigPrompt
/* %% */
                    ADD_GROUP:ECFM_MEP_CFG_CMDS
#endif
#ifdef PPP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:PPP
    PARENT:CONFIGURE PROMPT:CfaGetPppPrompt
    ADD_GROUP:CFA_PPP_CMDS ADD_GROUP:PPP_INT_CMDS
#ifdef OSPF_WANTED
                    ADD_GROUP:OSPF_L3INTFCFG_CMDS
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_POLICY_INT_CMD
#endif
#ifdef NAT_WANTED
                    ADD_GROUP:NAT_CONFIG_CMDS
#endif
#ifdef RIP_WANTED
                    ADD_GROUP:RIP_IVRINTCFG_GRP
#endif
#ifdef IGMP_WANTED
                    ADD_GROUP:IGMP_IFACE_CMDS
#endif
#ifdef IGMPPRXY_WANTED
                    ADD_GROUP:IGP_IFACE_CMDS
#endif
#endif
#ifdef HDLC_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      MODE:HDLC PARENT:CONFIGURE PROMPT:CfaGetHdlcPrompt ADD_GROUP:CFA_HDLC_CMDS
#endif
/* %% */
#ifdef OPENFLOW_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:INTERFACE_OPENFLOW PARENT:CONFIGURE PROMPT:CfaGetIfOpenflowPrompt
    ADD_GROUP:OFC_INTF_MODE_CMDS ADD_GROUP:OFC_INTF_MAP_CMDS
#endif
 
    
    
    
    
    
    
    
    
              MODE:INTERFACE_ROUTER PARENT:CONFIGURE PROMPT:CfaGetIfRouterPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_IVR_CMDS ADD_GROUP:CFA_INTERFACE_CMDS
    ADD_GROUP:CFA_INT_COMMON_CMDS ADD_GROUP:STD_CFA_IVR_CMDS
    ADD_GROUP:CFA_ROUTERPORT_CMDS
#ifdef OPENFLOW_WANTED
                    ADD_GROUP:OFC_PHY_INTF_CMDS
#endif
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_POLICY_INT_CMD
#endif
#ifdef DHCPC_WANTED
                    ADD_GROUP:DHCPC_INTCFG_GRP
#endif
#ifdef DHCP_RLY_WANTED
                    ADD_GROUP:DHCPR_IVR_GRP ADD_GROUP:STD_DHCPR_IFACE_CMD
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_INTF_CFG_CMDS
#endif
#ifdef DHCP6_CLNT_WANTED
                    ADD_GROUP:DHCP6_CLNT_INTF_GRP
#endif
#ifdef DHCP6_SRV_WANTED
                    ADD_GROUP:DHCP6_SRV_INTF_GRP
#endif
#ifdef DHCP6_RLY_WANTED
                    ADD_GROUP:DHCP6_RLY_INTF_GRP
#endif
#ifdef VCM_WANTED
#ifdef VRF_WANTED
                    ADD_GROUP:VCM_IP_INTF_CMDS
#endif
#endif
#ifdef IP_WANTED
                    ADD_GROUP:IP_INTCFG_GRP
#endif
#ifdef LA_WANTED
                    ADD_GROUP:LA_INT_CMDS
#endif
#ifdef POE_WANTED
                    ADD_GROUP:POE_INT_CMDS
#endif
#ifdef MPLS_WANTED
                    ADD_GROUP:MPLS_IFACE_CMDS
#endif
#ifdef ISS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      ADD_GROUP:SYS_INT_CMDS ADD_GROUP:ISS_EXT_INT_CMDS ADD_GROUP:ACL_INTCFG_GRP
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_IFACE_CMDS
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_INTCFG_GRP
#endif
#ifdef DIFFSRV_WANTED
#ifdef SWC
                    ADD_GROUP:DSCXE_IFACE_CMDS
#endif
                    ADD_GROUP:DIFFSRV_IFACE_CMDS
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_IFACE_CMDS
#endif
#ifdef IP_WANTED
                    ADD_GROUP:IP_IVRINTCFG_GRP
#endif
#ifdef OSPF_WANTED
                    ADD_GROUP:OSPF_L3INTFCFG_CMDS
#endif
#ifdef OSPF3_WANTED
                    ADD_GROUP:OSPF3_L3INTFCFG
#endif
#ifdef ISIS_WANTED
                    ADD_GROUP:ISIS_IF_COMM_CMDS ADD_GROUP:ISIS_IFACE_CMDS
#endif
#ifdef RIP_WANTED
                    ADD_GROUP:RIP_IVRINTCFG_GRP ADD_GROUP:STD_RIP_IVRINTCFG
#endif
#ifdef RIP6_WANTED
                    ADD_GROUP:RIP6_IVRINTCFG_GRP ADD_GROUP:STD_RIP6_IVRINTCFG
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
                    ADD_GROUP:PIM_IFACE_CMDS
#endif
#ifdef IGMP_WANTED
                    ADD_GROUP:IGMP_IFACE_CMDS
#endif
#ifdef MLD_WANTED
                    ADD_GROUP:MLD_IFACE_CMDS
#endif
#ifdef IGMPPRXY_WANTED
                    ADD_GROUP:IGP_IFACE_CMDS ADD_GROUP:STD_IGP_IFACE
#endif
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_INTCFG_GRP ADD_GROUP:STD_IP6_INTCFG
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                    ADD_GROUP:IP_INT_CFG_GRP
#endif
#ifdef DVMRP_WANTED
                    ADD_GROUP:DVMRP_IVR_CFG
#endif
#ifdef MRI_WANTED
                    ADD_GROUP:MRI_IFACE_CMDS
#endif
#ifdef NAT_WANTED
                    ADD_GROUP:NAT_CONFIG_CMDS
#endif
#ifdef LA_WANTED
                    ADD_GROUP:LA_PO_CMDS
#endif
#ifdef FIREWALL_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:FWL_IFACE_CMDS
    MODE:FIREWALL PARENT:CONFIGURE PROMPT:FwlGetFwlConfigPrompt
    ADD_GROUP:FWL_CONFIG_CMDS
    MODE:BLK_WHITE PARENT:CONFIGURE PROMPT:FwlGetBlkWhiteConfigPrompt
    ADD_GROUP:BLK_WHITE_CFG_CMDS
#endif
/* %% */
                    MODE:IVR PARENT:CONFIGURE PROMPT:CfaGetIvrPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_IVR_CMDS ADD_GROUP:CFA_INT_COMMON_CMDS
    ADD_GROUP:STD_CFA_IVR_CMDS ADD_GROUP:CFA_IVR_COUNT_CMDS
#endif
#ifdef FIREWALL_WANTED
                    ADD_GROUP:FWL_IFACE_CMDS
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_IVR_CMDS
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_POLICY_INT_CMD
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_INTF_CFG_CMDS
#endif
#ifdef IP_WANTED
                    ADD_GROUP:IP_IVRINTCFG_GRP
#endif
#ifdef DHCPC_WANTED
                    ADD_GROUP:DHCPC_INTCFG_GRP
#endif
#ifdef DHCP_RLY_WANTED
                    ADD_GROUP:DHCPR_IVR_GRP ADD_GROUP:STD_DHCPR_IFACE_CMD
#endif
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_INTCFG_GRP ADD_GROUP:STD_IP6_INTCFG
#endif
#if defined (IP_WANTED) || defined (LNXIP4_WANTED)
                    ADD_GROUP:IP_INT_CFG_GRP
#endif
#ifdef OSPF_WANTED
                    ADD_GROUP:OSPF_L3INTFCFG_CMDS
#endif
#ifdef ISIS_WANTED
                    ADD_GROUP:ISIS_IF_COMM_CMDS ADD_GROUP:ISIS_IFACE_CMDS
#endif
#ifdef OSPF3_WANTED
                    ADD_GROUP:OSPF3_L3INTFCFG
#endif
#ifdef DHCP6_CLNT_WANTED
                    ADD_GROUP:DHCP6_CLNT_INTF_GRP
#endif
#ifdef DHCP6_SRV_WANTED
                    ADD_GROUP:DHCP6_SRV_INTF_GRP
#endif
#ifdef DHCP6_RLY_WANTED
                    ADD_GROUP:DHCP6_RLY_INTF_GRP
#endif
#ifdef RIP_WANTED
                    ADD_GROUP:RIP_IVRINTCFG_GRP ADD_GROUP:STD_RIP_IVRINTCFG
#endif
#ifdef RIP6_WANTED
                    ADD_GROUP:RIP6_IVRINTCFG_GRP ADD_GROUP:STD_RIP6_IVRINTCFG
#endif
#ifdef DVMRP_WANTED
                    ADD_GROUP:DVMRP_IVR_CFG
#endif
#ifdef NAT_WANTED
                    ADD_GROUP:NAT_CONFIG_CMDS
#endif
#ifdef MPLS_WANTED
                    ADD_GROUP:MPLS_IFACE_CMDS
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
                    ADD_GROUP:PIM_IFACE_CMDS
#endif
#ifdef IGMP_WANTED
                    ADD_GROUP:IGMP_IFACE_CMDS
#endif
#ifdef MLD_WANTED
                    ADD_GROUP:MLD_IFACE_CMDS
#endif
#ifdef IGMPPRXY_WANTED
                    ADD_GROUP:IGP_IFACE_CMDS ADD_GROUP:STD_IGP_IFACE
#endif
#ifdef MRI_WANTED
                    ADD_GROUP:MRI_IFACE_CMDS
#endif
#if defined (VCM_WANTED) && defined (VRF_WANTED)
                    ADD_GROUP:VCM_IP_INTF_CMDS
#endif
/* %% */
                    MODE:LNXIVR PARENT:CONFIGURE PROMPT:CfaGetLinuxIvrPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_IVR_CMDS ADD_GROUP:STD_CFA_IVR_CMDS
    ADD_GROUP:CFA_IVR_COUNT_CMDS
#endif
#ifdef DHCPC_WANTED
                    ADD_GROUP:DHCPC_INTCFG_GRP
#endif
#ifdef IP_WANTED
                    ADD_GROUP:IP_IVRINTCFG_GRP
#endif
/* %% */
                    MODE:LOIVR PARENT:CONFIGURE PROMPT:CfaGetLoopbackPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_IVR_CMDS ADD_GROUP:STD_CFA_IVR_CMDS
    ADD_GROUP:CFA_IVR_COUNT_CMDS
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_INTCFG_GRP ADD_GROUP:STD_IP6_INTCFG
#ifdef OSPF3_WANTED
                    ADD_GROUP:OSPF3_L3INTFCFG
#endif
#endif
#endif
#ifdef RIP6_WANTED
                    ADD_GROUP:RIP6_IVRINTCFG_GRP ADD_GROUP:STD_RIP6_IVRINTCFG
#endif
#ifdef DHCPC_WANTED
                    ADD_GROUP:DHCPC_INTCFG_GRP
#endif
#ifdef ISIS_WANTED
                    ADD_GROUP:ISIS_IF_COMM_CMDS
#endif
#ifdef IP_WANTED
                    ADD_GROUP:IP_IVRINTCFG_GRP
#endif
#if defined (CFA_WANTED) || defined (IP_WANTED)
                    ADD_GROUP:CFA_INT_COMMON_CMDS
#endif
#ifdef VRF_WANTED
                    ADD_GROUP:VCM_IP_INTF_CMDS
#endif
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
                    ADD_GROUP:PIM_IFACE_CMDS
#endif
/* %% */
#if defined (IPSECv6_WANTED) && !defined (VPN_WANTED)
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:CRYPTO
    PARENT:CONFIGURE PROMPT:IPSecv6GetCryptoPrompt ADD_GROUP:CRYPT_TRAN_CONF_GRP
#endif
/* %% */
                    MODE:TNL PARENT:CONFIGURE PROMPT:CfaGetTnlPrompt
#ifdef CFA_WANTED
                    ADD_GROUP:CFA_TNL_CMD
#endif
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_INTCFG_GRP ADD_GROUP:STD_IP6_INTCFG
#endif
#ifdef IP_WANTED
                    ADD_GROUP:IP_INT_CFG_GRP
#endif
#ifdef OSPF3_WANTED
                    ADD_GROUP:OSPF3_L3INTFCFG
#endif
#ifdef DHCP6_CLNT_WANTED
                    ADD_GROUP:DHCP6_CLNT_INTF_GRP
#endif
#ifdef DHCP6_SRV_WANTED
                    ADD_GROUP:DHCP6_SRV_INTF_GRP
#endif
#ifdef DHCP6_RLY_WANTED
                    ADD_GROUP:DHCP6_RLY_INTF_GRP
#endif
#ifdef RIP6_WANTED
                    ADD_GROUP:RIP6_IVRINTCFG_GRP
#endif
#ifdef VRF_WANTED
                    ADD_GROUP:VCM_IP_INTF_CMDS
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_POLICY_INT_CMD
#endif
/* %% */
                    MODE:PO PARENT:CONFIGURE PROMPT:CfaGetPortChannelPrompt
#ifdef CFA_WANTED
                    ADD_GROUP:CFA_PO_CMD ADD_GROUP:CFA_INTERFACE_CMDS
#endif
#ifdef LA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
         ADD_GROUP:LA_PO_CMDS ADD_GROUP:ACL_INTCFG_GRP ADD_GROUP:LA_PO_EXIT_CMDS
#endif
                    ADD_GROUP:SISP_INT_CMDS
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_INTCFG_GRP
#endif
#ifdef MPLS_WANTED
#ifdef HVPLS_WANTED
                    ADD_GROUP:L2VPN_PO_CH_CMD
#endif
#endif
#ifdef VCM_WANTED
#ifdef MI_WANTED
                    ADD_GROUP:VCM_INTF_MODE_CMDS
#endif
#endif
#ifdef ECFM_WANTED
                    ADD_GROUP:ECFM_INTFCFG_CMDS
#endif
#ifdef ELMI_WANTED
                    ADD_GROUP:ELMI_INT_CFG_GRP
#endif
#ifdef LCM_WANTED
                    ADD_GROUP:LCM_INT_CFG_GRP
#endif
#ifdef EVCPRO_WANTED
                    ADD_GROUP:CFA_INT_UNI_CFG_GRP
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_VLAN_CFG_CMDS ADD_GROUP:PTP_OPT_CFG_CMDS
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:INT_CFG_GRP ADD_GROUP:STD_INT_CFG_GRP
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_IFACE_CMDS
#endif
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_IFACE_CMDS ADD_GROUP:STD_VLAN_IFACE_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_IFACE_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_IFACE_CMDS
#endif
#endif
#ifdef ISS_WANTED
                    ADD_GROUP:ISS_EXT_INT_CMDS ADD_GROUP:PI_INT_CMDS
#endif
#ifdef DCBX_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ETS_INTF_CMDS ADD_GROUP:PFC_INTF_CMDS ADD_GROUP:DCBX_INTF_CMDS
    ADD_GROUP:APP_PRI_INTF_CMDS
#endif                            /*DCBX_WANTED */
#ifdef FSB_WANTED
                    ADD_GROUP:FSB_INTF_CMDS
#endif                            /* CN_WANTED */
#ifdef CN_WANTED
                    ADD_GROUP:CN_INTF_MODE_CMDS
#endif                            /* CN_WANTED */
#ifdef RBRG_WANTED
                    ADD_GROUP:RBRG_IF_CMDS
#endif
#ifdef FIREWALL_WANTED
                    ADD_GROUP:FWL_IFACE_CMDS
#endif
/* %% */
                    MODE:PSW PARENT:CONFIGURE PROMPT:CfaGetPswPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_INT_CMDS ADD_GROUP:CFA_INTERFACE_CMDS
    ADD_GROUP:CFA_INT_COMMON_CMDS ADD_GROUP:IPDB_IFACE_CMDS
    ADD_GROUP:L2DS_IFACE_CMDS ADD_GROUP:CFA_IVR_CMDS
    ADD_GROUP:CFA_IVR_COUNT_CMDS
#endif
#ifdef IP6_WANTED
                    ADD_GROUP:IP6_INTCFG_GRP
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_POLICY_INT_CMD
#endif
#ifdef EVCPRO_WANTED
                    ADD_GROUP:CFA_INT_UNI_CFG_GRP
#endif
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_INTF_CMDS
#endif
#ifdef EOAM_WANTED
                    ADD_GROUP:EOAM_INTFCFG_CMDS
#ifdef EOAM_TEST_WANTED
                    ADD_GROUP:TSTOAM_INTFCFG_CMDS
#endif
#ifdef EOAM_FM_WANTED
                    ADD_GROUP:FM_INTFCFG_CMDS
#endif
#endif
#ifdef LLDP_WANTED
                    ADD_GROUP:LLDP_INTFCFG_CMDS
#endif
#ifdef ECFM_WANTED
                    ADD_GROUP:ECFM_INTFCFG_CMDS
#endif
#ifdef ELMI_WANTED
                    ADD_GROUP:ELMI_INT_CFG_GRP
#endif
#ifdef LCM_WANTED
                    ADD_GROUP:LCM_INT_CFG_GRP
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:INT_CFG_GRP ADD_GROUP:STD_INT_CFG_GRP
#endif
#ifdef VCM_WANTED
#ifdef MI_WANTED
                    ADD_GROUP:VCM_INTF_MODE_CMDS ADD_GROUP:SISP_INT_CMDS
#endif
                    ADD_GROUP:VCM_IP_INTF_CMDS
#endif
#ifdef ISIS_WANTED
                    ADD_GROUP:ISIS_IF_COMM_CMDS ADD_GROUP:ISIS_IFACE_CMDS
#endif
#ifdef MLD_WANTED
                    ADD_GROUP:MLD_IFACE_CMDS
#endif
#if  defined (IP_WANTED) || defined (LNXIP4_WANTED)
                    ADD_GROUP:IP_INT_CFG_GRP
#endif
#ifdef OSPF_WANTED
                    ADD_GROUP:OSPF_L3INTFCFG_CMDS
#endif
#ifdef OSPF3_WANTED
                    ADD_GROUP:OSPF3_L3INTFCFG
#endif
#ifdef PIM_WANTED
                    ADD_GROUP:PIM_IFACE_CMDS
#endif
#ifdef RIP_WANTED
                    ADD_GROUP:RIP_IVRINTCFG_GRP
#endif
#ifdef RIP6_WANTED
                    ADD_GROUP:RIP6_IVRINTCFG_GRP
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_VLAN_CFG_CMDS ADD_GROUP:PTP_OPT_CFG_CMDS
#endif
#ifdef MPLS_WANTED
                    ADD_GROUP:L2VPN_PORTCFG_CMDS ADD_GROUP:L2VPN_PW_OAM_CMDS
#endif
#ifdef IGS_WANTED
                    ADD_GROUP:SNOOP_PORTCFG_CMDS ADD_GROUP:STD_SNP_PORT_CMDS
#endif
#ifdef ISS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:SYS_INT_CMDS ADD_GROUP:PI_INT_CMDS ADD_GROUP:ISS_EXT_INT_CMDS
    ADD_GROUP:ACL_INTCFG_GRP
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_IFACE_CMDS
#endif
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_IFACE_CMDS ADD_GROUP:STD_VLAN_IFACE_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_IFACE_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_IFACE_CMDS
#endif
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_IFACE_CMDS
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_IFACE_CMDS
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_INTCFG_GRP
#endif
#ifdef DIFFSRV_WANTED
#ifdef SWC
                    ADD_GROUP:DSCXE_IFACE_CMDS
#endif
                    ADD_GROUP:DIFFSRV_IFACE_CMDS
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_IFACE_CMDS
#endif
#ifdef DVMRP_WANTED
                    ADD_GROUP:DVMRP_IVR_CFG
#endif
#ifdef IGMP_WANTED
                    ADD_GROUP:IGMP_IFACE_CMDS
#endif
#ifdef IGMPPRXY_WANTED
                    ADD_GROUP:IGP_IFACE_CMDS
#endif
/* %% */
                    MODE:AC PARENT:CONFIGURE PROMPT:CfaGetACPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:AC_IFACE_CMDS
    ADD_GROUP:CFA_INT_CMDS ADD_GROUP:CFA_INTERFACE_CMDS
    ADD_GROUP:CFA_INT_COMMON_CMDS ADD_GROUP:IPDB_IFACE_CMDS
    ADD_GROUP:L2DS_IFACE_CMDS
#endif
#ifdef VPN_WANTED
                    ADD_GROUP:VPN_POLICY_INT_CMD
#endif
#ifdef EVCPRO_WANTED
                    ADD_GROUP:CFA_INT_UNI_CFG_GRP
#endif
#ifdef EOAM_WANTED
                    ADD_GROUP:EOAM_INTFCFG_CMDS
#ifdef EOAM_TEST_WANTED
                    ADD_GROUP:TSTOAM_INTFCFG_CMDS
#endif
#ifdef EOAM_FM_WANTED
                    ADD_GROUP:FM_INTFCFG_CMDS
#endif
#endif
#ifdef LLDP_WANTED
                    ADD_GROUP:LLDP_INTFCFG_CMDS
#endif
#ifdef ECFM_WANTED
                    ADD_GROUP:ECFM_INTFCFG_CMDS
#endif
#ifdef ELMI_WANTED
                    ADD_GROUP:ELMI_INT_CFG_GRP
#endif
#ifdef LCM_WANTED
                    ADD_GROUP:LCM_INT_CFG_GRP
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:INT_CFG_GRP ADD_GROUP:STD_INT_CFG_GRP
#endif
#ifdef VCM_WANTED
#ifdef MI_WANTED
                    ADD_GROUP:VCM_INTF_MODE_CMDS ADD_GROUP:SISP_INT_CMDS
#endif
#endif
#ifdef MPLS_WANTED
                    ADD_GROUP:L2VPN_PORTCFG_CMDS ADD_GROUP:L2VPN_PW_OAM_CMDS
#endif
#ifdef IGS_WANTED
                    ADD_GROUP:SNOOP_PORTCFG_CMDS ADD_GROUP:STD_SNP_PORT_CMDS
#endif
#ifdef ISS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:SYS_INT_CMDS ADD_GROUP:PI_INT_CMDS ADD_GROUP:ISS_EXT_INT_CMDS
    ADD_GROUP:ACL_INTCFG_GRP
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_IFACE_CMDS
#endif
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_IFACE_CMDS ADD_GROUP:STD_VLAN_IFACE_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_IFACE_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_IFACE_CMDS
#endif
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_IFACE_CMDS
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_IFACE_CMDS
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_INTCFG_GRP
#endif
#ifdef DIFFSRV_WANTED
#ifdef SWC
                    ADD_GROUP:DSCXE_IFACE_CMDS
#endif
                    ADD_GROUP:DIFFSRV_IFACE_CMDS
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_IFACE_CMDS
#endif
#ifdef IGMP_WANTED
                    ADD_GROUP:IGMP_IFACE_CMDS
#endif
#ifdef IGMPPRXY_WANTED
                    ADD_GROUP:IGP_IFACE_CMDS
#endif
/* %% */
#ifdef PBB_WANTED
                    MODE:VIRTUAL PARENT:CONFIGURE PROMPT:CfaGetVirtualPrompt
#ifdef CFA_WANTED
                    ADD_GROUP:CFA_PO_CMD
#endif
#ifdef LA_WANTED
                    ADD_GROUP:LA_PO_CMDS ADD_GROUP:LA_PO_EXIT_CMDS
#endif
#ifdef VCM_WANTED
#ifdef MI_WANTED
                    ADD_GROUP:VCM_INTF_MODE_CMDS
#endif
#endif
#ifdef ECFM_WANTED
                    ADD_GROUP:ECFM_INTFCFG_CMDS
#endif
#ifdef ELMI_WANTED
                    ADD_GROUP:ELMI_INT_CFG_GRP
#endif
#ifdef LCM_WANTED
                    ADD_GROUP:LCM_INT_CFG_GRP
#endif
#ifdef EVCPRO_WANTED
                    ADD_GROUP:CFA_INT_UNI_CFG_GRP
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:INT_CFG_GRP ADD_GROUP:STD_INT_CFG_GRP
#endif
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_IFACE_CMDS
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_IFACE_CMDS ADD_GROUP:STD_VLAN_IFACE_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_IFACE_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_IFACE_CMDS
#endif
#endif
#ifdef ECFM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VIR_MEP_CONFIG PARENT:VIRTUAL
    PROMPT:EcfmGetMepConfigPrompt ADD_GROUP:ECFM_MEP_CFG_CMDS
#endif
#endif
/* %% */
                    MODE:SISP PARENT:CONFIGURE PROMPT:CfaGetSispPrompt
#ifdef LA_WANTED
                    ADD_GROUP:LA_PO_CMDS ADD_GROUP:LA_PO_EXIT_CMDS
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:INT_CFG_GRP
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_IFACE_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_IFACE_CMDS
#endif
#endif
#ifdef IGS_WANTED
                    ADD_GROUP:SNOOP_PORTCFG_CMDS
#endif
#ifdef ECFM_WANTED
                    ADD_GROUP:ECFM_INTFCFG_CMDS
#endif
/* %% */
#ifdef ECFM_WANTED
                    MODE:POMEP_CONFIG PARENT:PO PROMPT:EcfmGetMepConfigPrompt
/* %% */
                    ADD_GROUP:ECFM_MEP_CFG_CMDS
#endif
#ifdef ECFM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:PWMEP_CONFIG PARENT:PSW PROMPT:EcfmGetMepConfigPrompt
    ADD_GROUP:ECFM_MEP_CFG_CMDS
/* %% */
#endif
#ifdef ECFM_WANTED
                    MODE:SI_MEP_CONFIG PARENT:SISP PROMPT:EcfmGetMepConfigPrompt
/* %% */
                    ADD_GROUP:ECFM_MEP_CFG_CMDS
#endif
#ifdef LCM_WANTED
                    MODE:CLI_ESI_MODE PARENT:SISP PROMPT:LcmGetEsiConfigPrompt
#ifdef EVCPRO_WANTED
                    ADD_GROUP:EVCPRO_ESI_CFG_GRP
#endif
#endif
/* %% */
#ifdef DIFFSRV_WANTED
#ifdef MRVLLS
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:QOSPRIMAP
    PARENT:CONFIGURE PROMPT:QoSPriMapPrompt ADD_GROUP:QOS_PRI_MAP_CMDS
#else
#ifdef SWC
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:TRAFFICCLASS
    PARENT:CONFIGURE
    PROMPT:DiffsrvGetTrafficClassPrompt ADD_GROUP:DSCXE_TRFCLASS_CMDS
/* %% */
#endif
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:CLASSMAP
    PARENT:CONFIGURE
    PROMPT:DiffsrvGetClassMapPrompt ADD_GROUP:DIFFSRV_CLASS_CMDS
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:POLICYMAP
    PARENT:CONFIGURE
    PROMPT:DiffsrvGetPolicyMapPrompt ADD_GROUP:DIFFSRV_POLICY_CMDS
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:POLCLMAP
    PARENT:POLICYMAP
    PROMPT:DiffsrvGetPolicyClassMapPrompt ADD_GROUP:DIFFSRV_POLCL_CMDS
/* %% */
#endif
#endif
#ifdef QOSX_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:QOSPRIMAP
    PARENT:CONFIGURE PROMPT:QoSPriMapPrompt ADD_GROUP:QOS_PRI_MAP_CMDS
    MODE:QOSVLANMAP
    PARENT:CONFIGURE PROMPT:QoSVlanQMapPrompt ADD_GROUP:QOS_VLAN_MAP_CMDS
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:QOSCLSMAP
    PARENT:CONFIGURE PROMPT:QoSClsMapPrompt ADD_GROUP:QOS_CLS_MAP_CMDS
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:QOSMETER
    PARENT:CONFIGURE PROMPT:QoSMeterPrompt ADD_GROUP:QOS_METER_CMDS
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:QOSPLYMAP
    PARENT:CONFIGURE PROMPT:QoSPlyMapPrompt ADD_GROUP:QOS_PLY_MAP_CMDS
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:QOSQTEMP
    PARENT:CONFIGURE PROMPT:QoSQTempPrompt ADD_GROUP:QOS_Q_TEMP_CMDS
/* %% */
#endif
#ifdef ISS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:STDACL PARENT:CONFIGURE PROMPT:StdAclGetCfgPrompt
    ADD_GROUP:ACL_STDCFG_GRP
    MODE:HW PARENT:CONFIGURE PROMPT:CliGetHwCfgPrompt
    ADD_GROUP:HW_CONFIG_IF_CMDS
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:EXTACL PARENT:CONFIGURE PROMPT:ExtAclGetCfgPrompt
    ADD_GROUP:ACL_EXTCFG_GRP
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MACACL PARENT:CONFIGURE PROMPT:MacAclGetCfgPrompt
    ADD_GROUP:ACL_MACCFG_GRP
    MODE:IPV6ACL PARENT:CONFIGURE PROMPT:AclIPv6GetCfgPrompt
    ADD_GROUP:ACL_IPV6CFG_GRP
    MODE:USERDEFFACL PARENT:CONFIGURE PROMPT:UserDefAclGetCfgPrompt
    ADD_GROUP:ACL_USRDEFCFG_GRP
/* %% */
#endif
#ifdef L2TPV3_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:PW PARENT:CONFIGURE PROMPT:CliGetPwCfgPrompt
    ADD_GROUP:PW_CONFIG_CMDS
    MODE:SSN PARENT:CONFIGURE PROMPT:CliGetSessionCfgPrompt
    ADD_GROUP:SSN_CONFIG_CMDS
    MODE:XCNT_ROUTER PARENT:INTERFACE_ROUTER PROMPT:CliGetXconnectCfgPrompt
    ADD_GROUP:XCNT_CONFIG_IF_CMDS
    MODE:XCNT_IVR PARENT:IVR PROMPT:CliGetXconnectCfgPrompt
    ADD_GROUP:XCNT_CONFIG_IF_CMDS
#endif
#ifdef BFD_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:BFD_CONFIG
    PARENT:CONFIGURE PROMPT:BfdGetBfdCfgPrompt ADD_GROUP:BFD_SESSION_CMDS
#endif
/* %% */
#ifdef EVB_WANTED
                    MODE:SBP PARENT:CONFIGURE PROMPT:CfaGetEvbSbpPrompt
#ifdef CFA_WANTED
                    ADD_GROUP:CFA_INTERFACE_CMDS ADD_GROUP:CFA_INT_COMMON_CMDS
#endif
#ifdef IGS_WANTED
                    ADD_GROUP:SNOOP_VLAN_CMDS ADD_GROUP:STD_SNP_PORT_CMDS
#endif
#ifdef VLAN_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:VLAN_IFACE_CMDS ADD_GROUP:STD_VLAN_IFACE_CMDS
    ADD_GROUP:VLAN_EVB_SCH_CMDS
#endif
#ifdef ISS_WANTED
                    ADD_GROUP:ACL_INTCFG_GRP
#endif
#ifdef FSB_WANTED
                    ADD_GROUP:FSB_INTF_CMDS
#endif
                    MODE:UAP PARENT:CONFIGURE PROMPT:CfaGetEvbUapPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:CFA_INT_CMDS ADD_GROUP:CFA_INTERFACE_CMDS
    ADD_GROUP:CFA_INT_COMMON_CMDS
#ifdef LLDP_WANTED
                    ADD_GROUP:LLDP_INTFCFG_CMDS
#endif
#ifdef DCBX_WANTED
                    ADD_GROUP:DCBX_INTF_CMDS ADD_GROUP:ETS_INTF_CMDS
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_IFACE_CMDS
#endif
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_IFACE_CMDS ADD_GROUP:STD_VLAN_IFACE_CMDS
#endif
#ifdef ISS_WANTED
                    ADD_GROUP:SYS_INT_CMDS
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:INT_CFG_GRP
#endif
                    ADD_GROUP:VLAN_EVB_INT_CMDS
#endif
/* %% */
#ifdef TLM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:CONFIG_TLM_TE
    PARENT:CONFIGURE PROMPT:TlmGetCfgTePrompt
    ADD_GROUP:TLM_TE_LINK_MODE
    MODE:TLM_TE_LINK
    PARENT:CONFIG_TLM_TE PROMPT:TlmGetTeLinkPrompt ADD_GROUP:TLM_TE_LINK_CMDS
    ADD_GROUP:TLM_INTERAFCE_MODE
    MODE:TLM_TE_LINK_IF
    PARENT:TLM_TE_LINK PROMPT:TlmGetTeIfConfigPrompt ADD_GROUP:TLM_TE_LINK_CMDS
    ADD_GROUP:TLM_INTERAFCE_MODE
    MODE:TLM_COMP_TE_LINK
    PARENT:TLM_TE_LINK PROMPT:TlmGetCompTeConfigPrompt
    ADD_GROUP:TLM_INTERFACE_CMDS MODE:TLM_COMP_IF_LINK PARENT:TLM_TE_LINK_IF
    PROMPT:TlmGetCompIfConfigPrompt ADD_GROUP:TLM_INTERFACE_CMDS
#endif
#ifdef WLC_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:CAPAB_CONFIG
    PARENT:CONFIGURE PROMPT:WssCfgGetCapabilityCfgPrompt
    ADD_GROUP:CAPAB_MODE_CMDS MODE:QOS_CONFIG PARENT:CONFIGURE
    PROMPT:WssCfgGetQosCfgPrompt ADD_GROUP:QOS_MODE_CMDS MODE:AUTH_CONFIG
    PARENT:CONFIGURE PROMPT:WssCfgGetAuthCfgPrompt ADD_GROUP:AUTH_MODE_CMDS
    MODE:BATCH_CONFIG_IP
    PARENT:CONFIGURE PROMPT:CapwapGetBatchDhcpPrompt
    ADD_GROUP:BATCH_IP_CMD
    MODE:BATCH_CONFIG_FIREWALL
    PARENT:CONFIGURE PROMPT:CapwapGetBatchFirewallPrompt
    ADD_GROUP:BATCH_FIREWALL_CMD
    MODE:BATCH_CONFIG_NAT
    PARENT:CONFIGURE PROMPT:CapwapGetBatchNatPrompt
    ADD_GROUP:BATCH_NAT_CMD
    MODE:BATCH_CONFIG_L3SUBIF
    PARENT:CONFIGURE PROMPT:CapwapGetBatchL3SubIfPrompt
    ADD_GROUP:BATCH_L3SUBIF_CMD
#ifdef WSSUSER_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:WSSUSER_CONFIG
    PARENT:CONFIGURE PROMPT:WssUserGetGroupCfgPrompt
    ADD_GROUP:WSSUSER_GROUP_CMDS
#endif
#ifdef WLC_WANTED
#ifdef WSSCUST_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:SQL_CONFIG
    PARENT:CONFIGURE PROMPT:WssCfgGetSqlCfgPrompt ADD_GROUP:SQL_GROUP_CMDS
#endif
#endif
#endif
#ifdef CAPWAP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:WTP_MODEL_CONFIG
    PARENT:CONFIGURE PROMPT:CapwapGetWtpModelCfgPrompt ADD_GROUP:WTP_MODEL_CMDS
#endif
#ifdef HOTSPOT2_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:HS_CONFIG
    PARENT:CONFIGURE PROMPT:HsGetHsCfgPrompt ADD_GROUP:HS_CONFIG_CMDS
#endif
#ifdef VLAN_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VLAN_CONFIG
    PARENT:CONFIGURE PROMPT:VlanGetVlanCfgPrompt ADD_GROUP:VLAN_MODE_CMDS
#ifdef OPENFLOW_WANTED
                    ADD_GROUP:OFC_VLAN_INTF_CMDS ADD_GROUP:OFC_VLAN_MAP_CMDS
#endif
#ifdef PB_WANTED
                    ADD_GROUP:VLAN_PB_MODE_CMDS
#endif
#if defined IGS_WANTED || defined MLDS_WANTED
                    ADD_GROUP:SNOOP_VLAN_CMDS
#endif
                    ADD_GROUP:L2DS_VLAN_CMDS
#ifdef MPLS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:L2VPN_VLAN_CMDS ADD_GROUP:L2VPN_PW_OAM_CMDS
    ADD_GROUP:L2VPN_PWBACKUP_CMD
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_VLAN_GRP
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_VLAN_CFG_CMDS ADD_GROUP:PTP_OPT_CFG_CMDS
#endif
#ifdef CUST_CLI_WANTED
                    ADD_GROUP:CUST_VLAN_CMDS
#endif
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_EVC_CMDS
#endif
#ifdef VXLAN_WANTED
                    ADD_GROUP:VXLAN_VLAN_VNI_CMDS
#endif
#endif                            /* VLAN_WANTED */
/* %% */
#ifdef ERPS_WANTED
#ifdef L2TPV3_WANTED
                    ADD_GROUP:XCNT_CONFIG_IF_CMDS
#endif
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:ERPS_CONFIG
    PARENT:CONFIGURE PROMPT:ErpsGetErpsCfgPrompt ADD_GROUP:ERPS_RING_PG_CMDS
#endif
#ifdef Y1564_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:Y1564_SLA_CONFIG
    PARENT:CONFIGURE
    PROMPT:Y1564GetSlaCfgPrompt
    ADD_GROUP:Y1564_SLA_CMDS
    MODE:Y1564_TRAFFIC_PROFILE_CONFIG
    PARENT:CONFIGURE
    PROMPT:Y1564GetTrafficProfileCfgPrompt ADD_GROUP:Y1564_TPROF_CMDS
#ifndef MEF_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:Y1564_SERVICE_CONF_CONFIG
    PARENT:CONFIGURE
    PROMPT:Y1564GetServiceConfCfgPrompt ADD_GROUP:Y1564_SER_CONF_CMDS
#endif
#endif
#ifdef RFC6374_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_PM_SERVICE_CONFIG
    PARENT:CONFIGURE
    PROMPT:R6374GetServiceConfCfgPrompt ADD_GROUP:MPLS_PM_SERV_CMDS
#endif
#ifdef RFC2544_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:R2544_SLA_CONFIG
    PARENT:CONFIGURE
    PROMPT:R2544GetSlaCfgPrompt
    ADD_GROUP:R2544_SLA_CMDS
    MODE:R2544_TRAFFIC_PROFILE_CONFIG
    PARENT:CONFIGURE
    PROMPT:R2544GetTrafficProfileCfgPrompt
    ADD_GROUP:R2544_TRAFPROF_CMDS
    MODE:R2544_SAC_CONFIG
    PARENT:CONFIGURE PROMPT:R2544GetSacCfgPrompt ADD_GROUP:R2544_SAC_CMDS
#endif
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:UFD_GROUP
    PARENT:CONFIGURE PROMPT:CliGetUfdGroupPrompt ADD_GROUP:CFA_UFD_CMDS
#endif
#ifdef ELPS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:ELPS_CONFIG
    PARENT:CONFIGURE PROMPT:ElpsGetElpsCfgPrompt ADD_GROUP:ELPS_PG_CONFIG_CMDS
#endif
/* %% */
#ifdef MPLS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:PWRED_GRP_CONFIG
    PARENT:CONFIGURE PROMPT:L2VpGetPwRedClassCfgPrompt
    ADD_GROUP:PSWRED_CLASS_CMDS
#endif
/* %% */
#ifdef  MSTP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MST PARENT:CONFIGURE PROMPT:MstpGetMstpCfgPrompt ADD_GROUP:MSTP_CFG_GRP
#endif
#ifdef VPN_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VPN PARENT:CONFIGURE PROMPT:VpnGetCryptoMapPrompt
    ADD_GROUP:VPN_CONFIG_CMDS
#endif
#ifdef ECFM_WANTED
 
    
    
    
    
    
    
    
    
    
    
            MODE:DOMAIN_CONFIG PARENT:CONFIGURE PROMPT:EcfmGetDomainConfigPrompt
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ECFM_DOM_CFG_CMDS
    MODE:DOMAIN_ECFMTP_CONFIG PARENT:CONFIGURE
    PROMPT:EcfmTpOamGetDomainConfigPrompt
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ECFMTP_DOM_CFG_CMDS
    MODE:MEP_ECFMTP_CONFIG PARENT:DOMAIN_ECFMTP_CONFIG
    PROMPT:EcfmTpOamGetMepConfigPrompt
/* %% */
                    ADD_GROUP:ECFMTP_MEP_CFG_CMDS
#endif
/* %% */
#ifdef DHCP_SRV_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:DHCPPOOL
    PARENT:CONFIGURE PROMPT:DhcpSrvGetPoolCfgPrompt ADD_GROUP:DHCPS_POOLCFG_GRP
#endif
/* %% */
#if defined (PIM_WANTED) || defined (PIMV6_WANTED)
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:COMPONENT
    PARENT:CONFIGURE PROMPT:PimGetCompConfigPrompt ADD_GROUP:PIM_COMP_CMDS
#endif
/* %% */
#ifdef OSPF_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:ROUTEROSPF
    PARENT:CONFIGURE PROMPT:OspfGetRouterCfgPrompt ADD_GROUP:OSPF_RTRCFG_CMDS
    ADD_GROUP:STD_OSPF_RTRCFG
#endif
#ifdef OSPFTE_WANTED
                    ADD_GROUP:STD_OSPFTE_RTRCFG
#endif
/* %% */
/* %% */
#ifdef ISIS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:ROUTERISIS
    PARENT:CONFIGURE PROMPT:IsIsGetRouterCfgPrompt ADD_GROUP:ISIS_MODE_CMDS
#endif
/* %% */
#ifdef OSPF3_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:ROUTEROSPF3
    PARENT:CONFIGURE PROMPT:Ospfv3GetRouterCfgPrompt ADD_GROUP:OSPF3_RTRCFG
    ADD_GROUP:STD_OSPF3_RTRCFG
#endif
#ifdef DHCP6_SRV_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:D6SRVPOOL
    PARENT:CONFIGURE PROMPT:Dhcp6SGetPoolCfgPrompt ADD_GROUP:DHCP6_SRV_POOL_GRP
#endif
/* %% */
#ifdef DHCP6_SRV_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:D6SRVVENDOR
    PARENT:D6SRVPOOL PROMPT:Dhcp6SGetVendorCfgPrompt ADD_GROUP:DHCP6_SRV_VENDOR
#endif
#ifdef DHCP6_SRV_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:D6SRVCLNT
    PARENT:CONFIGURE PROMPT:D6SrGetClientCfgPrompt ADD_GROUP:DHCP6_SRV_CLNT_GRP
#endif
/* %% */
#ifdef VRRP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VRRP PARENT:CONFIGURE PROMPT:VrrpGetVrrpCfgPrompt ADD_GROUP:VRRPIF_CMDS
    ADD_GROUP:VRRP_CONFIG_CMDS
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VRRP_IF
    PARENT:VRRP PROMPT:VrrpGetVrrpIfCfgPrompt ADD_GROUP:IF_VRRP_CONFIG_CMDS
    ADD_GROUP:STD_IF_VRRP_CONFIG
#endif
/* %% */
/* %% */
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:OOB_CONFIG PARENT:CONFIGURE PROMPT:CfaConfigOOBInterface
    ADD_GROUP:CFA_OOB_CMDS
#ifdef DHCPC_WANTED
                    ADD_GROUP:DHCPC_INTCFG_GRP
#endif
#endif
/* %% */
#ifdef RIP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:RIP
    PARENT:CONFIGURE PROMPT:RipGetRouterCfgPrompt ADD_GROUP:RIP_ROUTERCFG_GRP
    ADD_GROUP:STD_RIP_ROUTERCFG
#endif
/* %% */
#ifdef RIP6_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:RIP6
    PARENT:CONFIGURE PROMPT:Rip6GetRouterCfgPrompt ADD_GROUP:RIP6_ROUTERCFG_GRP
    ADD_GROUP:STD_RIP6_ROUTERCFG
#endif
/* %% */
#ifdef BGP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:BGP PARENT:CONFIGURE PROMPT:BgpGetBgpCfgPrompt ADD_GROUP:BGP_MODE_CMDS
    ADD_GROUP:STD_BGP_MODE_CMDS ADD_GROUP:BGP_AF_MODE_CMDS
    ADD_GROUP:BGP_VPN4_MODE_CMDS
    MODE:AF_BGP
    PARENT:BGP PROMPT:BgpGetBgpAddfamilyCfgPrompt ADD_GROUP:BGP_AF_MODE_CMDS
    ADD_GROUP:BGP_VPN4_MODE_CMDS ADD_GROUP:BGP_NBR_ACTIVE_CMD
    MODE:AF_VPNV4_BGP
    PARENT:BGP PROMPT:BgpGetBgpVpnv4AddfamilyCfgPrompt
    ADD_GROUP:BGP_VPN4_MODE_CMDS ADD_GROUP:BGP_NBR_ACTIVE_CMD
#endif
/* %% */
/* %% */
#ifdef ROUTEMAP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:ROUTEMAP
    PARENT:CONFIGURE PROMPT:RMapCliGetRMapPrompt ADD_GROUP:RMAP_MODE_CMDS
    ADD_GROUP:STD_RMAP_MODE
#endif
/* %% */
/* %% */
#ifdef CUST_CLI_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:CUSTOMER
    PARENT:CONFIGURE PROMPT:IssCustGetConfigPrompt ADD_GROUP:CUST_MODE_CMDS
#endif
/* %% */
/* %% */
#ifdef MI_WANTED
#ifdef VCM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        MODE:VCM PARENT:CONFIGURE PROMPT:VcmGetVcmPrompt ADD_GROUP:VCM_MODE_CMDS
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
                    ADD_GROUP:PBB_GBL_CMDS
#endif
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_GBL_CMDS ADD_GROUP:STD_VLAN_GBL_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_GBL_CMDS
#endif
#ifdef EVB_WANTED
                    ADD_GROUP:VLAN_EVB_GBL_CMDS
#endif
#ifdef HB_WANTED
                    ADD_GROUP:HB_MODE_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_GBL_CMDS ADD_GROUP:STD_GARP_GBL_CMDS
#endif
#ifdef ERPS_WANTED
                    ADD_GROUP:ERPS_CXT_GBL_CMDS
#endif
#ifdef Y1564_WANTED
                    ADD_GROUP:Y1564_CXT_GBL_CMDS
#endif
#ifdef RFC2544_WANTED
                    ADD_GROUP:R2544_CXT_GBL_CMDS
#endif
#ifdef MRP_WANTED
                    ADD_GROUP:MRP_GBL_CMDS
#endif
#ifdef MPLS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:MPLS_VFI_CMDS ADD_GROUP:L2VPN_MSPW_CMDS
    ADD_GROUP:L2VPN_PW_OAM_CMDS ADD_GROUP:L2VPN_PWBACKUP_CMD
#endif
#endif                            /* VLAN_WANTED */
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
          ADD_GROUP:CFA_CXT_CMDS ADD_GROUP:IPDB_GBL_CMDS ADD_GROUP:L2DS_GBL_CMDS
#endif
#ifdef ELPS_WANTED
                    ADD_GROUP:ELPS_CXT_GBL_CMDS
#endif
#ifdef CN_WANTED
                    ADD_GROUP:CN_CXT_GBL_CMDS
#endif                            /* CN_WANTED */
#ifdef RBRG_WANTED
                    ADD_GROUP:RBRG_CFG_CMDS
#endif
#ifdef PBBTE_WANTED
                    ADD_GROUP:PBBTE_VCM_CMDS
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:RSTP_GLBCFG_GRP ADD_GROUP:STD_RSTP_GLBCFG_GRP
#endif
#if defined (IGS_WANTED) || defined(MLDS_WANTED)
                    ADD_GROUP:SNOOP_CONFIG_CMDS
#endif
#ifdef IGS_WANTED
                    ADD_GROUP:STD_SNOOP_CON_CMDS
#endif
#ifdef FSB_WANTED
                    ADD_GROUP:FSB_CONFIG_CMDS
#endif
/* %% */
/* %% */
#ifdef ECFM_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
      ADD_GROUP:ECFM_CFG_CMDS ADD_GROUP:ECFM_VLAN_CMDS ADD_GROUP:ECFMTP_CFG_CMDS
#endif
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_CFG_CMDS
#endif
#ifdef ECFM_WANTED
 
    
    
    
    
    
    
    
               MODE:VCMDOMAIN_CONFIG PARENT:VCM PROMPT:EcfmGetDomainConfigPrompt
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ECFM_DOM_CFG_CMDS
    MODE:VCMDOMAIN_ECFMTP_CONFIG PARENT:VCM
    PROMPT:EcfmTpOamGetDomainConfigPrompt
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:ECFMTP_DOM_CFG_CMDS
    MODE:VCMMEP_ECFMTP_CONFIG PARENT:VCMDOMAIN_ECFMTP_CONFIG
    PROMPT:EcfmTpOamGetMepConfigPrompt
/* %% */
                    ADD_GROUP:ECFMTP_MEP_CFG_CMDS
#endif
/* %% */
/* %% */
#ifdef VLAN_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMVLAN_CONFIG
    PARENT:VCM PROMPT:VlanGetVcmVlanCfgPrompt ADD_GROUP:VLAN_MODE_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:VLAN_PB_MODE_CMDS
#endif
#if defined IGS_WANTED || defined MLDS_WANTED
                    ADD_GROUP:SNOOP_VLAN_CMDS
#endif
#ifdef MPLS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:L2VPN_VLAN_CMDS ADD_GROUP:L2VPN_PW_OAM_CMDS
    ADD_GROUP:L2VPN_PWBACKUP_CMD
#endif
#ifdef RMON_WANTED
                    ADD_GROUP:RMON_VLAN_GRP
#endif
#ifdef PTP_WANTED
                    ADD_GROUP:PTP_VLAN_CFG_CMDS ADD_GROUP:PTP_OPT_CFG_CMDS
#endif
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_EVC_CMDS
#endif
#endif                            /* VCM - VLAN_WANTED */
#ifdef ELPS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMELPS_CONFIG
    PARENT:VCM PROMPT:ElpsGetVcmElpsCfgPrompt ADD_GROUP:ELPS_PG_CONFIG_CMDS
#endif
/* %% */
#ifdef PBBTE_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMPBBTE_CONFIG
    PARENT:VCM PROMPT:PbbTeGetVcmPbbTeCfgPrompt ADD_GROUP:PBBTE_TE_MODE_CMDS
#endif
#ifdef PBB_WANTED
#ifdef ISS_METRO_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMPBB_CONFIG
    PARENT:VCM PROMPT:PbbGetVcmIsidCfgPrompt ADD_GROUP:PBB_MODE_CMDS
#ifdef ECFM_WANTED
                    ADD_GROUP:ECFM_PBB_CMDS
#endif
#if defined(RSTP_WANTED) ||  defined(MSTP_WANTED)
                    ADD_GROUP:PBB_STP_GRP
#endif
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_PBB_MODE ADD_GROUP:PB_ISID_CMDS
#endif
#ifdef CFA_WANTED
                    ADD_GROUP:PBB_CFA_MODE
#endif
#endif
#endif
#ifdef ERPS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMERPS_CONFIG
    PARENT:VCM PROMPT:ErpsGetVcmErpsCfgPrompt ADD_GROUP:ERPS_RING_PG_CMDS
#endif
#ifdef Y1564_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMY1564_SLA_CONFIG
    PARENT:VCM
    PROMPT:Y1564GetVcmSlaCfgPrompt
    ADD_GROUP:Y1564_SLA_CMDS
    MODE:VCMY1564_TRAFFIC_PROFILE_CONFIG
    PARENT:VCM
    PROMPT:Y1564GetVcmTrafficProfileCfgPrompt ADD_GROUP:Y1564_TPROF_CMDS
#ifndef MEF_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMY1564_SERVICE_CONF_CONFIG
    PARENT:VCM
    PROMPT:Y1564GetVcmServiceConfCfgPrompt ADD_GROUP:Y1564_SER_CONF_CMDS
#endif
#endif
#ifdef RFC2544_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMR2544_SLA_CONFIG
    PARENT:VCM
    PROMPT:R2544GetVcmSlaCfgPrompt
    ADD_GROUP:R2544_SLA_CMDS
    MODE:VCM_R2544_TRAFFIC_PROFILE_CONFIG
    PARENT:VCM
    PROMPT:R2544GetVcmTrafficProfileCfgPrompt ADD_GROUP:R2544_TRAFPROF_CMDS
    MODE:VCMR2544_SAC_CONFIG
    PARENT:VCM PROMPT:R2544GetVcmSacCfgPrompt ADD_GROUP:R2544_SAC_CMDS
#endif
/* %% */
#ifdef  MSTP_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VCMMST PARENT:VCM PROMPT:MstpGetVcmMstpCfgPrompt ADD_GROUP:MSTP_CFG_GRP
#endif
#endif                            /* VCM_WANTED */
#endif                            /* MI_WANTED */
/* %% */
#if defined MPLS_WANTED
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_TUNNEL_MODE
    PARENT:CONFIGURE
    PROMPT:MplsGetMplsTunnelCfgPrompt
    ADD_GROUP:MPLSTUNNEL_CFG_GRP ADD_GROUP:MPLSLSP_CFG_GRP
/* %% */
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_TE_EXPLICIT_PATH_MODE
    PARENT:CONFIGURE
    PROMPT:TeGetExplicitPathCfgPrompt ADD_GROUP:MPLS_TE_EXPPATH_CFG
/* %% */
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_TE_ATTR_MODE
    PARENT:CONFIGURE PROMPT:TeGetAttrCfgPrompt ADD_GROUP:MPLS_TE_ATTR_CFG
/* %% */
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_TUNNEL_LSP_MODE
    PARENT:MPLS_TUNNEL_MODE
    PROMPT:MplsGetMplsTunnelLspCfgPrompt ADD_GROUP:MPLSLSP_CFG_GRP
/* %% */
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_OAM_MEG_MODE
    PARENT:CONFIGURE PROMPT:MplsGetMplsOamMegPrompt ADD_GROUP:MPLS_MEG_CMDS
/* %% */
#ifdef MPLS_SIG_WANTED
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:RSVP_MODE
    PARENT:CONFIGURE PROMPT:RpteCliGetRsvpCfgPrompt ADD_GROUP:MPLS_RPTE_CFG_CMDS
/* %% */
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:RSVP_IF_MODE
    PARENT:RSVP_MODE
    PROMPT:RpteCliGetRsvpIfCfgPrompt ADD_GROUP:MPLS_RPTE_INTF_CMDS
/* %% */
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:LDP_MODE
    PARENT:CONFIGURE PROMPT:MplsGetMplsLdpCfgPrompt ADD_GROUP:MPLS_LDP_CFG
/* %% */
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:LDP_ENTITY_MODE
    PARENT:LDP_MODE
    PROMPT:MplsGetMplsLdpEntityCfgPrompt ADD_GROUP:MPLS_LDP_ENT_CFG
/* %% */
#endif
/* %% */
#ifdef MPLS_L3VPN_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_L3VPN_VRF_MODE
    PARENT:CONFIGURE PROMPT:MplsL3VpnGetMplsL3VpnVrfCfgPrompt
    ADD_GROUP:MPLS_L3VPN_VRF_CFG
#endif
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_VFI_MODE
    PARENT:VCM
    PROMPT:MplsGetMplsVfiCfgPrompt ADD_GROUP:MPLS_VFI_MODE_GRP
    ADD_GROUP:L2VPN_PW_OAM_CMDS
/* %% */
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_VCM_VFI_MODE
    PARENT:CONFIGURE
    PROMPT:MplsGetMplsVfiCfgPrompt ADD_GROUP:MPLS_VFI_MODE_GRP
    ADD_GROUP:L2VPN_PW_OAM_CMDS
#ifdef MEF_WANTED
                    ADD_GROUP:MEF_EVC_CMDS
#endif
/* %% */
#endif                            /* MPLS_WANTED */
/* %% */
#ifdef MPLS_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:MPLS_TUNNEL_ELSP_MODE
    PARENT:MPLS_TUNNEL_LSP_MODE
    PROMPT:MplsGetMplsTunnelElspCfgPrompt
    ADD_GROUP:MPLS_ELSP_CMDS
    MODE:MPLS_TUNNEL_LLSP_MODE
    PARENT:MPLS_TUNNEL_LSP_MODE
    PROMPT:MplsGetMplsTunnelLlspCfgPrompt
    ADD_GROUP:MPLS_LLSP_CMDS
    MODE:MPLS_TUNNEL_LSP_ELSP_MODE
    PARENT:MPLS_TUNNEL_MODE
    PROMPT:MplsGetMplsTunnelElspCfgPrompt
    ADD_GROUP:MPLS_ELSP_CMDS
    MODE:MPLS_TUNNEL_LSP_LLSP_MODE
    PARENT:MPLS_TUNNEL_MODE
    PROMPT:MplsGetMplsTunnelLlspCfgPrompt
    ADD_GROUP:MPLS_LLSP_CMDS
    MODE:CLASS_MODE
    PARENT:CONFIGURE
    PROMPT:MplsGetMplsTunnelClassCfgPrompt
    ADD_GROUP:MPLS_CLASS_CMDS
    MODE:EXP_PHB
    PARENT:CONFIGURE
    PROMPT:MplsGetMplsTunnelExpToPhbMapCfgPrompt
    ADD_GROUP:MPLS_EXP_PHB_CMDS
    MODE:ELSP_INFO_MODE
    PARENT:CONFIGURE
    PROMPT:MplsGetMplsTunnelElspInfoPhbMapCfgPrompt
    ADD_GROUP:MPLS_ELSP_INFO_CMDS
#endif
#ifdef VXLAN_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VXLAN_NVE_MODE
    PARENT:CONFIGURE
    PROMPT:CfaGetNvePrompt
    ADD_GROUP:VXLAN_NVE_CFG_GRP ADD_GROUP:CFA_INT_COMMON_CMDS
/* %% */
#ifdef EVPN_VXLAN_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VXLAN_CLI_EVPN_MODE
    PARENT:CONFIGURE PROMPT:VxlanEvpnVniMapPrompt ADD_GROUP:VXLAN_EVI_VNI_GRP
/* %% */
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:VXLAN_CLI_L2VNI_MODE
    PARENT:VXLAN_CLI_EVPN_MODE
    PROMPT:VxlanEvpnL2VniPrompt
    ADD_GROUP:VXLAN_EVI_L2VNI_GRP
    MODE:VXLAN_CLI_VRF_MODE
    PARENT:CONFIGURE
    PROMPT:VxlanEvpnVrfPrompt
    ADD_GROUP:VXLAN_EVI_VNI_GRP
    MODE:VXLAN_CLI_VRF_VNI_MODE
    PARENT:VXLAN_CLI_VRF_MODE
    PROMPT:VxlanEvpnVrfVniPrompt ADD_GROUP:VXLAN_EVI_L2VNI_GRP
/* %% */
#endif                            /* EVPN_VXLAN_WANTED */
/* %% */
#endif                            /* VXLAN_WANTED */
#ifdef TAC_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:TAC_PROFILE
    PARENT:CONFIGURE PROMPT:TacCliGetProfCfgPrompt ADD_GROUP:TACM_IP_PRF_CMDS
#endif
/* %% */
 
    
    
    
    
    
    
    
               MODE:CONF_RANGE PARENT:CONFIGURE PROMPT:CfaGetIfRangeConfigPrompt
#ifdef CFA_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:SYS_INT_CMDS ADD_GROUP:PI_INT_CMDS ADD_GROUP:ISS_EXT_INT_CMDS
    ADD_GROUP:CFA_INT_CMDS ADD_GROUP:CFA_INTERFACE_CMDS
    ADD_GROUP:CFA_INT_COMMON_CMDS
#ifdef VLAN_WANTED
                    ADD_GROUP:VLAN_IFACE_CMDS ADD_GROUP:STD_VLAN_IFACE_CMDS
#ifdef PB_WANTED
                    ADD_GROUP:PB_IFACE_CMDS
#endif
#ifdef GARP_WANTED
                    ADD_GROUP:GARP_IFACE_CMDS
#endif
#endif
#endif
#ifdef OPENFLOW_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    ADD_GROUP:OFC_PHY_INTF_CMDS ADD_GROUP:OFC_INTF_MODE_CMDS
    ADD_GROUP:OFC_INTF_MAP_CMDS
#endif
#ifdef EVCPRO_WANTED
                    ADD_GROUP:CFA_INT_UNI_CFG_GRP
#endif
#ifdef EOAM_WANTED
                    ADD_GROUP:EOAM_INTFCFG_CMDS
#ifdef EOAM_TEST_WANTED
                    ADD_GROUP:TSTOAM_INTFCFG_CMDS
#endif
#ifdef EOAM_FM_WANTED
                    ADD_GROUP:FM_INTFCFG_CMDS
#endif
#endif
#ifdef LLDP_WANTED
                    ADD_GROUP:LLDP_INTFCFG_CMDS
#endif
#ifdef QOSX_WANTED
                    ADD_GROUP:QOS_IFACE_CMDS
#endif
#ifdef ECFM_WANTED
                    ADD_GROUP:ECFM_INTFCFG_CMDS
#endif
#ifdef ELMI_WANTED
                    ADD_GROUP:ELMI_INT_CFG_GRP
#endif
#ifdef LCM_WANTED
                    ADD_GROUP:LCM_INT_CFG_GRP
#endif
#if defined(RSTP_WANTED) || defined(MSTP_WANTED)
                    ADD_GROUP:INT_CFG_GRP
#endif
#ifdef MI_WANTED
#ifdef VCM_WANTED
                    ADD_GROUP:VCM_INTF_MODE_CMDS
#endif
#endif
#ifdef LA_WANTED
                    ADD_GROUP:LA_INT_CMDS
#endif
#ifdef PNAC_WANTED
                    ADD_GROUP:PNAC_INT_CMDS
#endif
#ifdef POE_WANTED
                    ADD_GROUP:POE_INT_CMDS
#endif
#ifdef MPLS_WANTED
                    ADD_GROUP:L2VPN_PORTCFG_CMDS ADD_GROUP:L2VPN_PW_OAM_CMDS
#endif
#ifdef IGS_WANTED
                    ADD_GROUP:SNOOP_PORTCFG_CMDS
#endif
/* %% */
/* %% */
#ifdef CFA_WANTED
#ifdef VCPEMGR_WANTED
 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    MODE:TAP_MODE
    PARENT:CONFIGURE
    PROMPT:CfaGetTapPrompt
    ADD_GROUP:CFA_INT_COMMON_CMDS ADD_GROUP:VLAN_IFACE_CMDS
#endif
#endif
/* %% */
/* %% */
