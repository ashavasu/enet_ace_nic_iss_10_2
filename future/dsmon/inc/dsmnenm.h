 /********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains all DSMON enumerations
 *******************************************************************/
#ifndef _DSMNENM_H_
#define _DSMNENM_H_

typedef enum {
  DSMON_MATRIXSD_TABLE = 0,
  DSMON_MATRIXDS_TABLE = 1,
  DSMON_MAX_MATRIX_TABLES = 2
} enDsmonMatrixId;

typedef enum  {
    pdistTopNPkts = 1,
    pdistTopNOctets = 2,
    pdistTopNHCPkts = 3,
    pdistTopNHCOctets = 4   
} enPdistTopNRateBase;

typedef enum  {
    hostTopNInPkts = 1,
    hostTopNInOctets = 2,
    hostTopNOutPkts = 3,
    hostTopNOutOctets = 4,
    hostTopNTotalPkts  = 5,
    hostTopNTotalOctets = 6,
    hostTopNInHCPkts = 7,
    hostTopNInHCOctets = 8,
    hostTopNOutHCPkts = 9,
    hostTopNOutHCOctets = 10,
    hostTopNTotalHCPkts = 11,
    hostTopNTotalHCOctets = 12
} enHostTopNRateBase;

typedef enum  {
    matrixTopNPkts = 1,
    matrixTopNOctets = 2,
    matrixTopNHCPkts = 3,
    matrixTopNHCOctets = 4   
} enMatrixTopNRateBase;

#endif
