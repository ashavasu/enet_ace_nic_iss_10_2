/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved 
 * $Id: dsmnextn.h,v 1.3 2011/09/24 06:46:54 siva Exp $
 * Description: This file externs Global Variable declaration
 *******************************************************************/
#ifndef _DSMNEXTN_H_
#define _DSMNEXTN_H_

PUBLIC tDsmonGlobals gDsmonGlobals;

PUBLIC UINT4 u4CidrSubnetMask[];


#endif
