/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * $Id: dsmnprot.h,v 1.5 2011/09/24 06:46:54 siva Exp $
 * Description: This file contains the DSMON function prototypes
 *******************************************************************/
#ifndef _DSMNPROT_H_
#define _DSMNPROT_H_
/* Init APIs */
PUBLIC INT4 DsmonMainLock ARG_LIST((VOID));
PUBLIC INT4 DsmonMainUnLock ARG_LIST((VOID));
PUBLIC VOID DsmonMainUpdateTopNTables ARG_LIST((VOID));

INT4 IssSnmpLowGetPortCtrlDuplex ARG_LIST((INT4, INT4*));
 
/* Aggregation Control Group APIs */
PUBLIC VOID   DsmonAggCtlHandleLckChgs ARG_LIST((UINT1));
PUBLIC tDsmonAggCtl*  DsmonAggCtlGetNextIndex ARG_LIST((UINT4));
PUBLIC tDsmonAggCtl*  DsmonAggCtlAddEntry ARG_LIST((UINT4));
PUBLIC INT4   DsmonAggCtlDelEntry ARG_LIST((tDsmonAggCtl*));
PUBLIC tDsmonAggCtl*  DsmonAggCtlGetEntry ARG_LIST((UINT4));
PUBLIC tDsmonAggGroup*  DsmonAggCtlGetNextGrpIndex ARG_LIST((UINT4, UINT4));
PUBLIC tDsmonAggGroup*  DsmonAggCtlAddGrpEntry ARG_LIST((UINT4, UINT4));
PUBLIC INT4   DsmonAggCtlDelGrpEntry ARG_LIST((tDsmonAggGroup*));
PUBLIC tDsmonAggGroup*  DsmonAggCtlGetGrpEntry ARG_LIST((UINT4, UINT4));
PUBLIC INT4   DsmonAggCtlGetGrpToCtlIdxAndDscp ARG_LIST((UINT4, UINT4, UINT4*));
PUBLIC INT4   DsmonAggCtlSetGrpToCtlIdxAndDscp ARG_LIST((UINT4, UINT4, UINT4));

/* Packet Information Handler APIs */
PUBLIC tDsmonPktInfo*  DsmonPktInfoAddEntry ARG_LIST((tDsmonPktInfo *));
PUBLIC INT4   DsmonPktInfoDelEntry ARG_LIST((tDsmonPktInfo *));
PUBLIC VOID   DsmonPktInfoRemoveEntries ARG_LIST((UINT1));
PUBLIC tDsmonPktInfo*  DsmonPktInfoGetNextIndex ARG_LIST((tPktHeader *));
PUBLIC tDsmonPktInfo*  DsmonPktInfoGetEntry ARG_LIST((tPktHeader *));
PUBLIC tDsmonPktInfo*  DsmonPktInfoProcessIncomingPkt ARG_LIST((tDsmonPktInfo *));
PUBLIC VOID   DsmonPktInfoUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID   DsmonPktInfoUpdateIfaceChgs ARG_LIST((UINT4, UINT1));
PUBLIC VOID  DsmonPktInfoDelIdlePkt ARG_LIST((tDsmonPktInfo *));
PUBLIC VOID  DsmonPktInfoDelUnusedEntries ARG_LIST((VOID));

/* Porting APIs */
#ifdef NPAPI_WANTED
PUBLIC VOID DsmonFetchHwStats ARG_LIST((tDsmonPktInfo *));
PUBLIC VOID DsmonUpdateDataTables ARG_LIST((tDsmonPktInfo *, tRmon2Stats *));
#endif

/* Timers APIs */
PUBLIC VOID DsmonTmrInitTmrDesc ARG_LIST((VOID));
PUBLIC INT4 DsmonTmrInit ARG_LIST((VOID));
PUBLIC INT4 DsmonTmrDeInit ARG_LIST((VOID));
PUBLIC INT4 DsmonTmrStartTmr ARG_LIST((tDsmonTmr *, enDsmonTmrId , UINT4));
PUBLIC INT4 DsmonTmrStopTmr ARG_LIST((tDsmonTmr *));
PUBLIC INT4 DsmonTmrHandleExpiry ARG_LIST((VOID));
PUBLIC VOID DsmonTmrPollTmr ARG_LIST((VOID *));

/* Stats APIs */
PUBLIC INT4   DsmonStatsProcessPktInfo ARG_LIST((tDsmonPktInfo *, UINT4));
PUBLIC VOID   DsmonStatsUpdateAggCtlChgs ARG_LIST((UINT1));
PUBLIC VOID   DsmonStatsUpdateIfaceChgs ARG_LIST((UINT4, UINT1));
PUBLIC tDsmonStatsCtl*  DsmonStatsGetNextCtlIndex ARG_LIST((UINT4));
PUBLIC tDsmonStatsCtl*  DsmonStatsAddCtlEntry ARG_LIST((UINT4));
PUBLIC INT4   DsmonStatsDelCtlEntry ARG_LIST((tDsmonStatsCtl*));
PUBLIC tDsmonStatsCtl*  DsmonStatsGetCtlEntry ARG_LIST((UINT4));
PUBLIC tDsmonStats*  DsmonStatsGetNextDataIndex ARG_LIST((UINT4, UINT4));
PUBLIC tDsmonStats*  DsmonStatsAddDataEntry ARG_LIST((UINT4, UINT4));
PUBLIC VOID  DsmonStatsDelDataEntries ARG_LIST((UINT4));
PUBLIC INT4   DsmonStatsDelDataEntry ARG_LIST((tDsmonStats*));
PUBLIC tDsmonStats*  DsmonStatsGetDataEntry ARG_LIST((UINT4, UINT4));
PUBLIC VOID   DsmonStatsDelInterDep ARG_LIST((tDsmonStats*));
PUBLIC VOID   DsmonStatsPopulateDataEntries ARG_LIST((tDsmonStatsCtl*));

/* Pdist APIs */
PUBLIC INT4   DsmonPdistProcessPktInfo ARG_LIST((tDsmonPktInfo *, UINT4));
PUBLIC VOID   DsmonPdistUpdateAggCtlChgs ARG_LIST((UINT1));
PUBLIC VOID   DsmonPdistUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID   DsmonPdistUpdateIfaceChgs ARG_LIST((UINT4, UINT1));
PUBLIC tDsmonPdistCtl*  DsmonPdistGetNextCtlIndex ARG_LIST((UINT4));
PUBLIC tDsmonPdistCtl*  DsmonPdistAddCtlEntry ARG_LIST((UINT4));
PUBLIC INT4   DsmonPdistDelCtlEntry ARG_LIST((tDsmonPdistCtl*));
PUBLIC tDsmonPdistCtl*  DsmonPdistGetCtlEntry ARG_LIST((UINT4));
PUBLIC tDsmonPdistStats* DsmonPdistGetNextDataIndex ARG_LIST((UINT4, UINT4, UINT4));
PUBLIC tDsmonPdistStats* DsmonPdistAddDataEntry ARG_LIST((UINT4, UINT4, UINT4));
PUBLIC tDsmonPdistStats* DsmonPdistGetDataEntry ARG_LIST((UINT4, UINT4, UINT4));
PUBLIC INT4   DsmonPdistDelDataEntry ARG_LIST((tDsmonPdistStats*));
PUBLIC VOID  DsmonPdistDelDataEntries ARG_LIST((UINT4));
PUBLIC VOID   DsmonPdistDelInterDep ARG_LIST((tDsmonPdistStats*));
PUBLIC VOID   DsmonPdistPopulateDataEntries ARG_LIST((tDsmonPdistCtl*));
/* Pdist TopN APIs */
PUBLIC VOID   DsmonPdistTopNUpdateAggCtlChgs ARG_LIST((UINT1));
PUBLIC VOID   DsmonPdistTopNNotifyPdistCtlChgs ARG_LIST((UINT4, UINT4));
PUBLIC tDsmonPdistTopNCtl* DsmonPdistTopNGetNextCtlIndex ARG_LIST((UINT4));
PUBLIC tDsmonPdistTopNCtl* DsmonPdistTopNAddCtlEntry ARG_LIST((UINT4));
PUBLIC INT4   DsmonPdistTopNDelCtlEntry ARG_LIST((tDsmonPdistTopNCtl*));
PUBLIC tDsmonPdistTopNCtl* DsmonPdistTopNGetCtlEntry ARG_LIST((UINT4));
PUBLIC tDsmonPdistTopN* DsmonPdistTopNGetNextTopNIndex ARG_LIST((UINT4, UINT4));
PUBLIC tDsmonPdistTopN* DsmonPdistTopNAddTopNEntry ARG_LIST((UINT4, tDsmonPdistTopN *));
PUBLIC INT4   DsmonPdistTopNDelTopNEntry ARG_LIST((tDsmonPdistTopN *));
PUBLIC tDsmonPdistTopN* DsmonPdistTopNGetTopNEntry ARG_LIST((UINT4, UINT4));
PUBLIC VOID             DsmonPdistTopNDelTopNReport ARG_LIST((UINT4));
 

/* Host APIs */
PUBLIC INT4   DsmonHostProcessPktInfo ARG_LIST((tDsmonPktInfo *, UINT4));
PUBLIC VOID   DsmonHostUpdateAggCtlChgs ARG_LIST((UINT1));
PUBLIC VOID   DsmonHostUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC VOID   DsmonHostUpdateIfaceChgs ARG_LIST((UINT4, UINT1));
PUBLIC tDsmonHostCtl*  DsmonHostGetNextCtlIndex ARG_LIST((UINT4));
PUBLIC tDsmonHostCtl* DsmonHostAddCtlEntry ARG_LIST((UINT4));
PUBLIC INT4   DsmonHostDelCtlEntry ARG_LIST((tDsmonHostCtl*));
PUBLIC tDsmonHostCtl*  DsmonHostGetCtlEntry ARG_LIST((UINT4));
PUBLIC tDsmonHostStats* DsmonHostGetNextDataIndex ARG_LIST((UINT4, UINT4, UINT4, tIpAddr*));
PUBLIC tDsmonHostStats* DsmonHostAddDataEntry ARG_LIST((UINT4, UINT4, UINT4, tIpAddr*));
PUBLIC INT4   DsmonHostDelDataEntry ARG_LIST((tDsmonHostStats*));
PUBLIC VOID  DsmonHostDelDataEntries ARG_LIST((UINT4));
PUBLIC tDsmonHostStats* DsmonHostGetDataEntry ARG_LIST((UINT4, UINT4, UINT4, tIpAddr*));
PUBLIC VOID   DsmonHostDelInterDep ARG_LIST((tDsmonHostStats*));
PUBLIC VOID   DsmonHostPopulateDataEntries ARG_LIST((tDsmonHostCtl*));

/* Host TopN APIs */
PUBLIC VOID   DsmonHostTopNUpdateAggCtlChgs ARG_LIST((UINT1));
PUBLIC VOID   DsmonHostTopNNotifyHostCtlChgs ARG_LIST((UINT4, UINT4));
PUBLIC tDsmonHostTopNCtl* DsmonHostTopNGetNextCtlIndex ARG_LIST((UINT4));
PUBLIC tDsmonHostTopNCtl* DsmonHostTopNAddCtlEntry ARG_LIST((UINT4));
PUBLIC INT4   DsmonHostTopNDelCtlEntry ARG_LIST((tDsmonHostTopNCtl*));
PUBLIC tDsmonHostTopNCtl* DsmonHostTopNGetCtlEntry ARG_LIST((UINT4));
PUBLIC tDsmonHostTopN*  DsmonHostTopNGetNextTopNIndex ARG_LIST((UINT4, UINT4));
PUBLIC tDsmonHostTopN* DsmonHostTopNAddTopNEntry ARG_LIST((UINT4, tDsmonHostTopN *));
PUBLIC INT4   DsmonHostTopNDelTopNEntry ARG_LIST((tDsmonHostTopN *));
PUBLIC tDsmonHostTopN*  DsmonHostTopNGetTopNEntry ARG_LIST((UINT4, UINT4));
PUBLIC VOID             DsmonHostTopNDelTopNReport ARG_LIST((UINT4));
 

/* Matrix APIs */
PUBLIC INT4   DsmonMatrixProcessPktInfo ARG_LIST((tDsmonPktInfo *, UINT4, UINT4));
PUBLIC VOID   DsmonMatrixUpdateAggCtlChgs ARG_LIST((UINT1));
PUBLIC VOID   DsmonMatrixUpdateIfaceChgs ARG_LIST((UINT4, UINT1));
PUBLIC VOID   DsmonMatrixUpdateProtoChgs ARG_LIST((UINT4));
PUBLIC tDsmonMatrixCtl* DsmonMatrixGetNextCtlIndex ARG_LIST((UINT4));
PUBLIC tDsmonMatrixCtl* DsmonMatrixAddCtlEntry ARG_LIST((UINT4));
PUBLIC INT4   DsmonMatrixDelCtlEntry ARG_LIST((tDsmonMatrixCtl*));
PUBLIC tDsmonMatrixCtl* DsmonMatrixGetCtlEntry ARG_LIST((UINT4));
PUBLIC tDsmonMatrixSD*  DsmonMatrixGetNextDataIndex ARG_LIST((UINT4, UINT4, UINT4, UINT4, UINT4, tIpAddr*, tIpAddr*));
PUBLIC tDsmonMatrixSD*  DsmonMatrixAddDataEntry ARG_LIST((UINT4, UINT4, UINT4, UINT4, tIpAddr*, tIpAddr*));
PUBLIC INT4   DsmonMatrixDelDataEntry ARG_LIST((tDsmonMatrixSD*));
PUBLIC VOID  DsmonMatrixDelDataEntries ARG_LIST((UINT4));
PUBLIC tDsmonMatrixSD*  DsmonMatrixGetDataEntry ARG_LIST((UINT4, UINT4, UINT4, UINT4, UINT4, tIpAddr*, tIpAddr*));
PUBLIC VOID   DsmonMatrixDelInterDep ARG_LIST((tDsmonMatrixSD*));
PUBLIC VOID   DsmonMatrixPopulateDataEntries ARG_LIST((tDsmonMatrixCtl*));

/* Matrix TopN APIs */
PUBLIC VOID   DsmonMatrixTopNUpdateAggCtlChgs ARG_LIST((UINT1));
PUBLIC VOID   DsmonMatrixTopNNotifyMtxCtlChgs ARG_LIST((UINT4, UINT4));
PUBLIC tDsmonMatrixTopNCtl* DsmonMatrixTopNGetNextCtlIndex ARG_LIST((UINT4));
PUBLIC tDsmonMatrixTopNCtl* DsmonMatrixTopNAddCtlEntry ARG_LIST((UINT4));
PUBLIC INT4   DsmonMatrixTopNDelCtlEntry ARG_LIST((tDsmonMatrixTopNCtl*));
PUBLIC tDsmonMatrixTopNCtl* DsmonMatrixTopNGetCtlEntry ARG_LIST((UINT4));
PUBLIC tDsmonMatrixTopN* DsmonMatrixTopNGetNextTopNIndex ARG_LIST((UINT4, UINT4));
PUBLIC tDsmonMatrixTopN* DsmonMatrixTopNAddTopNEntry ARG_LIST((UINT4, tDsmonMatrixTopN *));
PUBLIC INT4   DsmonMatrixTopNDelTopNEntry ARG_LIST((tDsmonMatrixTopN *));
PUBLIC tDsmonMatrixTopN* DsmonMatrixTopNGetTopNEntry ARG_LIST((UINT4, UINT4));
PUBLIC VOID             DsmonMatrixTopNDelTopNReport ARG_LIST((UINT4));


/* RBTree compare util APIs */
PUBLIC INT4 DsmonUtlRBCmpPktInfo ARG_LIST((tRBElem *, tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpAggCtl ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpAggGroup ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpStatsCtl ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpStats ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpPdistCtl ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpPdistStats ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpPdistTopNCtl ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpPdistTopN ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpHostCtl ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpHostStats ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpHostTopNCtl ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpHostTopN ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpMatrixCtl ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpMatrixSD ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpMatrixDS ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpMatrixTopNCtl ARG_LIST((tRBElem * , tRBElem *));
PUBLIC INT4 DsmonUtlRBCmpMatrixTopN ARG_LIST((tRBElem * , tRBElem *));

PUBLIC INT4 DsmonUtlRBFreePktInfo ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeAggCtl ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeAggGroup ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeStatsCtl ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeStats ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreePdistCtl ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreePdistStats ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreePdistTopNCtl ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreePdistTopN ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeHostCtl ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeHostStats ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeHostTopNCtl ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeHostTopN ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeMatrixCtl ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeMatrixSD ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeMatrixTopNCtl ARG_LIST((tRBElem * , UINT4));
PUBLIC INT4 DsmonUtlRBFreeMatrixTopN ARG_LIST((tRBElem * , UINT4));

PUBLIC VOID DsmonUtlFormDataSourceOid ARG_LIST((tSNMP_OID_TYPE *, UINT4));
PUBLIC VOID DsmonUtlSetDataSourceOid ARG_LIST((tSNMP_OID_TYPE *, UINT4 *));
PUBLIC INT1 DsmonUtlTestDataSourceOid ARG_LIST((tSNMP_OID_TYPE *));

PUBLIC VOID DsmonPdistTopNUpdateTable ARG_LIST((VOID));
PUBLIC VOID DsmonHostTopNUpdateTable ARG_LIST((VOID));
PUBLIC VOID DsmonMatrixTopNUpdateTable ARG_LIST((VOID));
PUBLIC INT4 CfaGetIfDuplexStatus ARG_LIST((UINT4, INT4 *));
#endif
