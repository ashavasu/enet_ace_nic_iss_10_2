/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved 
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDDSMDB_H
#define _STDDSMDB_H

UINT1 DsmonAggControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonAggProfileTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonAggGroupTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonStatsControlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonPdistCtlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonPdistStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_TIME_TICKS ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonPdistTopNCtlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonPdistTopNTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonHostCtlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonHostTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_TIME_TICKS ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 DsmonHostTopNCtlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonHostTopNTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonMatrixCtlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonMatrixSDTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_TIME_TICKS ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonMatrixDSTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_TIME_TICKS ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonMatrixTopNCtlTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DsmonMatrixTopNTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stddsm [] ={1,3,6,1,2,1,16,26};
tSNMP_OID_TYPE stddsmOID = {8, stddsm};


UINT4 DsmonCapabilities [ ] ={1,3,6,1,2,1,16,26,1,5,1};
UINT4 DsmonMaxAggGroups [ ] ={1,3,6,1,2,1,16,26,1,1,1};
UINT4 DsmonAggControlLocked [ ] ={1,3,6,1,2,1,16,26,1,1,2};
UINT4 DsmonAggControlChanges [ ] ={1,3,6,1,2,1,16,26,1,1,3};
UINT4 DsmonAggControlLastChangeTime [ ] ={1,3,6,1,2,1,16,26,1,1,4};
UINT4 DsmonAggControlIndex [ ] ={1,3,6,1,2,1,16,26,1,1,5,1,1};
UINT4 DsmonAggControlDescr [ ] ={1,3,6,1,2,1,16,26,1,1,5,1,2};
UINT4 DsmonAggControlOwner [ ] ={1,3,6,1,2,1,16,26,1,1,5,1,3};
UINT4 DsmonAggControlStatus [ ] ={1,3,6,1,2,1,16,26,1,1,5,1,4};
UINT4 DsmonAggProfileDSCP [ ] ={1,3,6,1,2,1,16,26,1,1,6,1,1};
UINT4 DsmonAggGroupIndex [ ] ={1,3,6,1,2,1,16,26,1,1,6,1,2};
UINT4 DsmonAggGroupDescr [ ] ={1,3,6,1,2,1,16,26,1,1,7,1,1};
UINT4 DsmonAggGroupStatus [ ] ={1,3,6,1,2,1,16,26,1,1,7,1,2};
UINT4 DsmonStatsControlIndex [ ] ={1,3,6,1,2,1,16,26,1,2,1,1,1};
UINT4 DsmonStatsControlDataSource [ ] ={1,3,6,1,2,1,16,26,1,2,1,1,2};
UINT4 DsmonStatsControlAggProfile [ ] ={1,3,6,1,2,1,16,26,1,2,1,1,3};
UINT4 DsmonStatsControlDroppedFrames [ ] ={1,3,6,1,2,1,16,26,1,2,1,1,4};
UINT4 DsmonStatsControlCreateTime [ ] ={1,3,6,1,2,1,16,26,1,2,1,1,5};
UINT4 DsmonStatsControlOwner [ ] ={1,3,6,1,2,1,16,26,1,2,1,1,6};
UINT4 DsmonStatsControlStatus [ ] ={1,3,6,1,2,1,16,26,1,2,1,1,7};
UINT4 DsmonStatsInPkts [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,1};
UINT4 DsmonStatsInOctets [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,2};
UINT4 DsmonStatsInOvflPkts [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,3};
UINT4 DsmonStatsInOvflOctets [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,4};
UINT4 DsmonStatsInHCPkts [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,5};
UINT4 DsmonStatsInHCOctets [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,6};
UINT4 DsmonStatsOutPkts [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,7};
UINT4 DsmonStatsOutOctets [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,8};
UINT4 DsmonStatsOutOvflPkts [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,9};
UINT4 DsmonStatsOutOvflOctets [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,10};
UINT4 DsmonStatsOutHCPkts [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,11};
UINT4 DsmonStatsOutHCOctets [ ] ={1,3,6,1,2,1,16,26,1,2,2,1,12};
UINT4 DsmonPdistCtlIndex [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,1};
UINT4 DsmonPdistCtlDataSource [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,2};
UINT4 DsmonPdistCtlAggProfile [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,3};
UINT4 DsmonPdistCtlMaxDesiredEntries [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,4};
UINT4 DsmonPdistCtlDroppedFrames [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,5};
UINT4 DsmonPdistCtlInserts [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,6};
UINT4 DsmonPdistCtlDeletes [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,7};
UINT4 DsmonPdistCtlCreateTime [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,8};
UINT4 DsmonPdistCtlOwner [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,9};
UINT4 DsmonPdistCtlStatus [ ] ={1,3,6,1,2,1,16,26,1,3,1,1,10};
UINT4 DsmonPdistTimeMark [ ] ={1,3,6,1,2,1,16,26,1,3,2,1,1};
UINT4 DsmonPdistStatsPkts [ ] ={1,3,6,1,2,1,16,26,1,3,2,1,2};
UINT4 DsmonPdistStatsOctets [ ] ={1,3,6,1,2,1,16,26,1,3,2,1,3};
UINT4 DsmonPdistStatsOvflPkts [ ] ={1,3,6,1,2,1,16,26,1,3,2,1,4};
UINT4 DsmonPdistStatsOvflOctets [ ] ={1,3,6,1,2,1,16,26,1,3,2,1,5};
UINT4 DsmonPdistStatsHCPkts [ ] ={1,3,6,1,2,1,16,26,1,3,2,1,6};
UINT4 DsmonPdistStatsHCOctets [ ] ={1,3,6,1,2,1,16,26,1,3,2,1,7};
UINT4 DsmonPdistStatsCreateTime [ ] ={1,3,6,1,2,1,16,26,1,3,2,1,8};
UINT4 DsmonPdistTopNCtlIndex [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,1};
UINT4 DsmonPdistTopNCtlPdistIndex [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,2};
UINT4 DsmonPdistTopNCtlRateBase [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,3};
UINT4 DsmonPdistTopNCtlTimeRemaining [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,4};
UINT4 DsmonPdistTopNCtlGeneratedReprts [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,5};
UINT4 DsmonPdistTopNCtlDuration [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,6};
UINT4 DsmonPdistTopNCtlRequestedSize [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,7};
UINT4 DsmonPdistTopNCtlGrantedSize [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,8};
UINT4 DsmonPdistTopNCtlStartTime [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,9};
UINT4 DsmonPdistTopNCtlOwner [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,10};
UINT4 DsmonPdistTopNCtlStatus [ ] ={1,3,6,1,2,1,16,26,1,3,3,1,11};
UINT4 DsmonPdistTopNIndex [ ] ={1,3,6,1,2,1,16,26,1,3,4,1,1};
UINT4 DsmonPdistTopNPDLocalIndex [ ] ={1,3,6,1,2,1,16,26,1,3,4,1,2};
UINT4 DsmonPdistTopNAggGroup [ ] ={1,3,6,1,2,1,16,26,1,3,4,1,3};
UINT4 DsmonPdistTopNRate [ ] ={1,3,6,1,2,1,16,26,1,3,4,1,4};
UINT4 DsmonPdistTopNRateOvfl [ ] ={1,3,6,1,2,1,16,26,1,3,4,1,5};
UINT4 DsmonPdistTopNHCRate [ ] ={1,3,6,1,2,1,16,26,1,3,4,1,6};
UINT4 DsmonHostCtlIndex [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,1};
UINT4 DsmonHostCtlDataSource [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,2};
UINT4 DsmonHostCtlAggProfile [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,3};
UINT4 DsmonHostCtlMaxDesiredEntries [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,4};
UINT4 DsmonHostCtlIPv4PrefixLen [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,5};
UINT4 DsmonHostCtlIPv6PrefixLen [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,6};
UINT4 DsmonHostCtlDroppedFrames [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,7};
UINT4 DsmonHostCtlInserts [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,8};
UINT4 DsmonHostCtlDeletes [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,9};
UINT4 DsmonHostCtlCreateTime [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,10};
UINT4 DsmonHostCtlOwner [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,11};
UINT4 DsmonHostCtlStatus [ ] ={1,3,6,1,2,1,16,26,1,4,1,1,12};
UINT4 DsmonHostTimeMark [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,1};
UINT4 DsmonHostAddress [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,2};
UINT4 DsmonHostInPkts [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,3};
UINT4 DsmonHostInOctets [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,4};
UINT4 DsmonHostInOvflPkts [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,5};
UINT4 DsmonHostInOvflOctets [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,6};
UINT4 DsmonHostInHCPkts [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,7};
UINT4 DsmonHostInHCOctets [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,8};
UINT4 DsmonHostOutPkts [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,9};
UINT4 DsmonHostOutOctets [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,10};
UINT4 DsmonHostOutOvflPkts [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,11};
UINT4 DsmonHostOutOvflOctets [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,12};
UINT4 DsmonHostOutHCPkts [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,13};
UINT4 DsmonHostOutHCOctets [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,14};
UINT4 DsmonHostCreateTime [ ] ={1,3,6,1,2,1,16,26,1,4,2,1,15};
UINT4 DsmonHostTopNCtlIndex [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,1};
UINT4 DsmonHostTopNCtlHostIndex [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,2};
UINT4 DsmonHostTopNCtlRateBase [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,3};
UINT4 DsmonHostTopNCtlTimeRemaining [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,4};
UINT4 DsmonHostTopNCtlGeneratedReports [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,5};
UINT4 DsmonHostTopNCtlDuration [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,6};
UINT4 DsmonHostTopNCtlRequestedSize [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,7};
UINT4 DsmonHostTopNCtlGrantedSize [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,8};
UINT4 DsmonHostTopNCtlStartTime [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,9};
UINT4 DsmonHostTopNCtlOwner [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,10};
UINT4 DsmonHostTopNCtlStatus [ ] ={1,3,6,1,2,1,16,26,1,4,3,1,11};
UINT4 DsmonHostTopNIndex [ ] ={1,3,6,1,2,1,16,26,1,4,4,1,1};
UINT4 DsmonHostTopNPDLocalIndex [ ] ={1,3,6,1,2,1,16,26,1,4,4,1,2};
UINT4 DsmonHostTopNAddress [ ] ={1,3,6,1,2,1,16,26,1,4,4,1,3};
UINT4 DsmonHostTopNAggGroup [ ] ={1,3,6,1,2,1,16,26,1,4,4,1,4};
UINT4 DsmonHostTopNRate [ ] ={1,3,6,1,2,1,16,26,1,4,4,1,5};
UINT4 DsmonHostTopNRateOvfl [ ] ={1,3,6,1,2,1,16,26,1,4,4,1,6};
UINT4 DsmonHostTopNHCRate [ ] ={1,3,6,1,2,1,16,26,1,4,4,1,7};
UINT4 DsmonMatrixCtlIndex [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,1};
UINT4 DsmonMatrixCtlDataSource [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,2};
UINT4 DsmonMatrixCtlAggProfile [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,3};
UINT4 DsmonMatrixCtlMaxDesiredEntries [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,4};
UINT4 DsmonMatrixCtlDroppedFrames [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,5};
UINT4 DsmonMatrixCtlInserts [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,6};
UINT4 DsmonMatrixCtlDeletes [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,7};
UINT4 DsmonMatrixCtlCreateTime [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,8};
UINT4 DsmonMatrixCtlOwner [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,9};
UINT4 DsmonMatrixCtlStatus [ ] ={1,3,6,1,2,1,16,26,1,6,1,1,10};
UINT4 DsmonMatrixTimeMark [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,1};
UINT4 DsmonMatrixNLIndex [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,2};
UINT4 DsmonMatrixSourceAddress [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,3};
UINT4 DsmonMatrixDestAddress [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,4};
UINT4 DsmonMatrixALIndex [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,5};
UINT4 DsmonMatrixSDPkts [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,6};
UINT4 DsmonMatrixSDOvflPkts [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,7};
UINT4 DsmonMatrixSDHCPkts [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,8};
UINT4 DsmonMatrixSDOctets [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,9};
UINT4 DsmonMatrixSDOvflOctets [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,10};
UINT4 DsmonMatrixSDHCOctets [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,11};
UINT4 DsmonMatrixSDCreateTime [ ] ={1,3,6,1,2,1,16,26,1,6,2,1,12};
UINT4 DsmonMatrixDSPkts [ ] ={1,3,6,1,2,1,16,26,1,6,3,1,1};
UINT4 DsmonMatrixDSOvflPkts [ ] ={1,3,6,1,2,1,16,26,1,6,3,1,2};
UINT4 DsmonMatrixDSHCPkts [ ] ={1,3,6,1,2,1,16,26,1,6,3,1,3};
UINT4 DsmonMatrixDSOctets [ ] ={1,3,6,1,2,1,16,26,1,6,3,1,4};
UINT4 DsmonMatrixDSOvflOctets [ ] ={1,3,6,1,2,1,16,26,1,6,3,1,5};
UINT4 DsmonMatrixDSHCOctets [ ] ={1,3,6,1,2,1,16,26,1,6,3,1,6};
UINT4 DsmonMatrixDSCreateTime [ ] ={1,3,6,1,2,1,16,26,1,6,3,1,7};
UINT4 DsmonMatrixTopNCtlIndex [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,1};
UINT4 DsmonMatrixTopNCtlMatrixIndex [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,2};
UINT4 DsmonMatrixTopNCtlRateBase [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,3};
UINT4 DsmonMatrixTopNCtlTimeRemaining [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,4};
UINT4 DsmonMatrixTopNCtlGeneratedRpts [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,5};
UINT4 DsmonMatrixTopNCtlDuration [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,6};
UINT4 DsmonMatrixTopNCtlRequestedSize [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,7};
UINT4 DsmonMatrixTopNCtlGrantedSize [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,8};
UINT4 DsmonMatrixTopNCtlStartTime [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,9};
UINT4 DsmonMatrixTopNCtlOwner [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,10};
UINT4 DsmonMatrixTopNCtlStatus [ ] ={1,3,6,1,2,1,16,26,1,6,4,1,11};
UINT4 DsmonMatrixTopNIndex [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,1};
UINT4 DsmonMatrixTopNAggGroup [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,2};
UINT4 DsmonMatrixTopNNLIndex [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,3};
UINT4 DsmonMatrixTopNSourceAddress [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,4};
UINT4 DsmonMatrixTopNDestAddress [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,5};
UINT4 DsmonMatrixTopNALIndex [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,6};
UINT4 DsmonMatrixTopNPktRate [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,7};
UINT4 DsmonMatrixTopNPktRateOvfl [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,8};
UINT4 DsmonMatrixTopNHCPktRate [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,9};
UINT4 DsmonMatrixTopNRevPktRate [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,10};
UINT4 DsmonMatrixTopNRevPktRateOvfl [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,11};
UINT4 DsmonMatrixTopNHCRevPktRate [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,12};
UINT4 DsmonMatrixTopNOctetRate [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,13};
UINT4 DsmonMatrixTopNOctetRateOvfl [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,14};
UINT4 DsmonMatrixTopNHCOctetRate [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,15};
UINT4 DsmonMatrixTopNRevOctetRate [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,16};
UINT4 DsmonMatrixTopNRevOctetRateOvfl [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,17};
UINT4 DsmonMatrixTopNHCRevOctetRate [ ] ={1,3,6,1,2,1,16,26,1,6,5,1,18};


tMbDbEntry stddsmMibEntry[]= {

{{11,DsmonMaxAggGroups}, NULL, DsmonMaxAggGroupsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DsmonAggControlLocked}, NULL, DsmonAggControlLockedGet, DsmonAggControlLockedSet, DsmonAggControlLockedTest, DsmonAggControlLockedDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,DsmonAggControlChanges}, NULL, DsmonAggControlChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,DsmonAggControlLastChangeTime}, NULL, DsmonAggControlLastChangeTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,DsmonAggControlIndex}, GetNextIndexDsmonAggControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonAggControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonAggControlDescr}, GetNextIndexDsmonAggControlTable, DsmonAggControlDescrGet, DsmonAggControlDescrSet, DsmonAggControlDescrTest, DsmonAggControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonAggControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonAggControlOwner}, GetNextIndexDsmonAggControlTable, DsmonAggControlOwnerGet, DsmonAggControlOwnerSet, DsmonAggControlOwnerTest, DsmonAggControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonAggControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonAggControlStatus}, GetNextIndexDsmonAggControlTable, DsmonAggControlStatusGet, DsmonAggControlStatusSet, DsmonAggControlStatusTest, DsmonAggControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonAggControlTableINDEX, 1, 0, 1, NULL},

{{13,DsmonAggProfileDSCP}, GetNextIndexDsmonAggProfileTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonAggProfileTableINDEX, 2, 0, 0, NULL},

{{13,DsmonAggGroupIndex}, GetNextIndexDsmonAggProfileTable, DsmonAggGroupIndexGet, DsmonAggGroupIndexSet, DsmonAggGroupIndexTest, DsmonAggProfileTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonAggProfileTableINDEX, 2, 0, 0, "0"},

{{13,DsmonAggGroupDescr}, GetNextIndexDsmonAggGroupTable, DsmonAggGroupDescrGet, DsmonAggGroupDescrSet, DsmonAggGroupDescrTest, DsmonAggGroupTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonAggGroupTableINDEX, 2, 0, 0, NULL},

{{13,DsmonAggGroupStatus}, GetNextIndexDsmonAggGroupTable, DsmonAggGroupStatusGet, DsmonAggGroupStatusSet, DsmonAggGroupStatusTest, DsmonAggGroupTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonAggGroupTableINDEX, 2, 0, 1, NULL},

{{13,DsmonStatsControlIndex}, GetNextIndexDsmonStatsControlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonStatsControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonStatsControlDataSource}, GetNextIndexDsmonStatsControlTable, DsmonStatsControlDataSourceGet, DsmonStatsControlDataSourceSet, DsmonStatsControlDataSourceTest, DsmonStatsControlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DsmonStatsControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonStatsControlAggProfile}, GetNextIndexDsmonStatsControlTable, DsmonStatsControlAggProfileGet, DsmonStatsControlAggProfileSet, DsmonStatsControlAggProfileTest, DsmonStatsControlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonStatsControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonStatsControlDroppedFrames}, GetNextIndexDsmonStatsControlTable, DsmonStatsControlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonStatsControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonStatsControlCreateTime}, GetNextIndexDsmonStatsControlTable, DsmonStatsControlCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonStatsControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonStatsControlOwner}, GetNextIndexDsmonStatsControlTable, DsmonStatsControlOwnerGet, DsmonStatsControlOwnerSet, DsmonStatsControlOwnerTest, DsmonStatsControlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonStatsControlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonStatsControlStatus}, GetNextIndexDsmonStatsControlTable, DsmonStatsControlStatusGet, DsmonStatsControlStatusSet, DsmonStatsControlStatusTest, DsmonStatsControlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonStatsControlTableINDEX, 1, 0, 1, NULL},

{{13,DsmonStatsInPkts}, GetNextIndexDsmonStatsTable, DsmonStatsInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonStatsTableINDEX, 2, 0, 0, NULL},

{{13,DsmonStatsInOctets}, GetNextIndexDsmonStatsTable, DsmonStatsInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonStatsTableINDEX, 2, 0, 0, NULL},

{{13,DsmonStatsInOvflPkts}, GetNextIndexDsmonStatsTable, DsmonStatsInOvflPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonStatsTableINDEX, 2, 1, 0, NULL},

{{13,DsmonStatsInOvflOctets}, GetNextIndexDsmonStatsTable, DsmonStatsInOvflOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonStatsTableINDEX, 2, 1, 0, NULL},

{{13,DsmonStatsInHCPkts}, GetNextIndexDsmonStatsTable, DsmonStatsInHCPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonStatsTableINDEX, 2, 0, 0, NULL},

{{13,DsmonStatsInHCOctets}, GetNextIndexDsmonStatsTable, DsmonStatsInHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonStatsTableINDEX, 2, 0, 0, NULL},

{{13,DsmonStatsOutPkts}, GetNextIndexDsmonStatsTable, DsmonStatsOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonStatsTableINDEX, 2, 0, 0, NULL},

{{13,DsmonStatsOutOctets}, GetNextIndexDsmonStatsTable, DsmonStatsOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonStatsTableINDEX, 2, 0, 0, NULL},

{{13,DsmonStatsOutOvflPkts}, GetNextIndexDsmonStatsTable, DsmonStatsOutOvflPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonStatsTableINDEX, 2, 1, 0, NULL},

{{13,DsmonStatsOutOvflOctets}, GetNextIndexDsmonStatsTable, DsmonStatsOutOvflOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonStatsTableINDEX, 2, 1, 0, NULL},

{{13,DsmonStatsOutHCPkts}, GetNextIndexDsmonStatsTable, DsmonStatsOutHCPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonStatsTableINDEX, 2, 0, 0, NULL},

{{13,DsmonStatsOutHCOctets}, GetNextIndexDsmonStatsTable, DsmonStatsOutHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonStatsTableINDEX, 2, 0, 0, NULL},

{{13,DsmonPdistCtlIndex}, GetNextIndexDsmonPdistCtlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlDataSource}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlDataSourceGet, DsmonPdistCtlDataSourceSet, DsmonPdistCtlDataSourceTest, DsmonPdistCtlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlAggProfile}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlAggProfileGet, DsmonPdistCtlAggProfileSet, DsmonPdistCtlAggProfileTest, DsmonPdistCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlMaxDesiredEntries}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlMaxDesiredEntriesGet, DsmonPdistCtlMaxDesiredEntriesSet, DsmonPdistCtlMaxDesiredEntriesTest, DsmonPdistCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlDroppedFrames}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlInserts}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlDeletes}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlCreateTime}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlOwner}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlOwnerGet, DsmonPdistCtlOwnerSet, DsmonPdistCtlOwnerTest, DsmonPdistCtlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonPdistCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistCtlStatus}, GetNextIndexDsmonPdistCtlTable, DsmonPdistCtlStatusGet, DsmonPdistCtlStatusSet, DsmonPdistCtlStatusTest, DsmonPdistCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonPdistCtlTableINDEX, 1, 0, 1, NULL},

{{13,DsmonPdistTimeMark}, GetNextIndexDsmonPdistStatsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_NOACCESS, DsmonPdistStatsTableINDEX, 4, 0, 0, NULL},

{{13,DsmonPdistStatsPkts}, GetNextIndexDsmonPdistStatsTable, DsmonPdistStatsPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonPdistStatsTableINDEX, 4, 0, 0, NULL},

{{13,DsmonPdistStatsOctets}, GetNextIndexDsmonPdistStatsTable, DsmonPdistStatsOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonPdistStatsTableINDEX, 4, 0, 0, NULL},

{{13,DsmonPdistStatsOvflPkts}, GetNextIndexDsmonPdistStatsTable, DsmonPdistStatsOvflPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonPdistStatsTableINDEX, 4, 1, 0, NULL},

{{13,DsmonPdistStatsOvflOctets}, GetNextIndexDsmonPdistStatsTable, DsmonPdistStatsOvflOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonPdistStatsTableINDEX, 4, 1, 0, NULL},

{{13,DsmonPdistStatsHCPkts}, GetNextIndexDsmonPdistStatsTable, DsmonPdistStatsHCPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonPdistStatsTableINDEX, 4, 0, 0, NULL},

{{13,DsmonPdistStatsHCOctets}, GetNextIndexDsmonPdistStatsTable, DsmonPdistStatsHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonPdistStatsTableINDEX, 4, 0, 0, NULL},

{{13,DsmonPdistStatsCreateTime}, GetNextIndexDsmonPdistStatsTable, DsmonPdistStatsCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonPdistStatsTableINDEX, 4, 0, 0, NULL},

{{13,DsmonPdistTopNCtlIndex}, GetNextIndexDsmonPdistTopNCtlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistTopNCtlPdistIndex}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlPdistIndexGet, DsmonPdistTopNCtlPdistIndexSet, DsmonPdistTopNCtlPdistIndexTest, DsmonPdistTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistTopNCtlRateBase}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlRateBaseGet, DsmonPdistTopNCtlRateBaseSet, DsmonPdistTopNCtlRateBaseTest, DsmonPdistTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistTopNCtlTimeRemaining}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlTimeRemainingGet, DsmonPdistTopNCtlTimeRemainingSet, DsmonPdistTopNCtlTimeRemainingTest, DsmonPdistTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, "1800"},

{{13,DsmonPdistTopNCtlGeneratedReprts}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlGeneratedReprtsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistTopNCtlDuration}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistTopNCtlRequestedSize}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlRequestedSizeGet, DsmonPdistTopNCtlRequestedSizeSet, DsmonPdistTopNCtlRequestedSizeTest, DsmonPdistTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, "150"},

{{13,DsmonPdistTopNCtlGrantedSize}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlGrantedSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistTopNCtlStartTime}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistTopNCtlOwner}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlOwnerGet, DsmonPdistTopNCtlOwnerSet, DsmonPdistTopNCtlOwnerTest, DsmonPdistTopNCtlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonPdistTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonPdistTopNCtlStatus}, GetNextIndexDsmonPdistTopNCtlTable, DsmonPdistTopNCtlStatusGet, DsmonPdistTopNCtlStatusSet, DsmonPdistTopNCtlStatusTest, DsmonPdistTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonPdistTopNCtlTableINDEX, 1, 0, 1, NULL},

{{13,DsmonPdistTopNIndex}, GetNextIndexDsmonPdistTopNTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonPdistTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonPdistTopNPDLocalIndex}, GetNextIndexDsmonPdistTopNTable, DsmonPdistTopNPDLocalIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonPdistTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonPdistTopNAggGroup}, GetNextIndexDsmonPdistTopNTable, DsmonPdistTopNAggGroupGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonPdistTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonPdistTopNRate}, GetNextIndexDsmonPdistTopNTable, DsmonPdistTopNRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonPdistTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonPdistTopNRateOvfl}, GetNextIndexDsmonPdistTopNTable, DsmonPdistTopNRateOvflGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonPdistTopNTableINDEX, 2, 1, 0, NULL},

{{13,DsmonPdistTopNHCRate}, GetNextIndexDsmonPdistTopNTable, DsmonPdistTopNHCRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonPdistTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonHostCtlIndex}, GetNextIndexDsmonHostCtlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlDataSource}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlDataSourceGet, DsmonHostCtlDataSourceSet, DsmonHostCtlDataSourceTest, DsmonHostCtlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlAggProfile}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlAggProfileGet, DsmonHostCtlAggProfileSet, DsmonHostCtlAggProfileTest, DsmonHostCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlMaxDesiredEntries}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlMaxDesiredEntriesGet, DsmonHostCtlMaxDesiredEntriesSet, DsmonHostCtlMaxDesiredEntriesTest, DsmonHostCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlIPv4PrefixLen}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlIPv4PrefixLenGet, DsmonHostCtlIPv4PrefixLenSet, DsmonHostCtlIPv4PrefixLenTest, DsmonHostCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonHostCtlTableINDEX, 1, 0, 0, "32"},

{{13,DsmonHostCtlIPv6PrefixLen}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlIPv6PrefixLenGet, DsmonHostCtlIPv6PrefixLenSet, DsmonHostCtlIPv6PrefixLenTest, DsmonHostCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonHostCtlTableINDEX, 1, 0, 0, "128"},

{{13,DsmonHostCtlDroppedFrames}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlInserts}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlDeletes}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlCreateTime}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlOwner}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlOwnerGet, DsmonHostCtlOwnerSet, DsmonHostCtlOwnerTest, DsmonHostCtlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonHostCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostCtlStatus}, GetNextIndexDsmonHostCtlTable, DsmonHostCtlStatusGet, DsmonHostCtlStatusSet, DsmonHostCtlStatusTest, DsmonHostCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonHostCtlTableINDEX, 1, 0, 1, NULL},

{{13,DsmonHostTimeMark}, GetNextIndexDsmonHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_NOACCESS, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostAddress}, GetNextIndexDsmonHostTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostInPkts}, GetNextIndexDsmonHostTable, DsmonHostInPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostInOctets}, GetNextIndexDsmonHostTable, DsmonHostInOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostInOvflPkts}, GetNextIndexDsmonHostTable, DsmonHostInOvflPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTableINDEX, 5, 1, 0, NULL},

{{13,DsmonHostInOvflOctets}, GetNextIndexDsmonHostTable, DsmonHostInOvflOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTableINDEX, 5, 1, 0, NULL},

{{13,DsmonHostInHCPkts}, GetNextIndexDsmonHostTable, DsmonHostInHCPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostInHCOctets}, GetNextIndexDsmonHostTable, DsmonHostInHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostOutPkts}, GetNextIndexDsmonHostTable, DsmonHostOutPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostOutOctets}, GetNextIndexDsmonHostTable, DsmonHostOutOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostOutOvflPkts}, GetNextIndexDsmonHostTable, DsmonHostOutOvflPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTableINDEX, 5, 1, 0, NULL},

{{13,DsmonHostOutOvflOctets}, GetNextIndexDsmonHostTable, DsmonHostOutOvflOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTableINDEX, 5, 1, 0, NULL},

{{13,DsmonHostOutHCPkts}, GetNextIndexDsmonHostTable, DsmonHostOutHCPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostOutHCOctets}, GetNextIndexDsmonHostTable, DsmonHostOutHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostCreateTime}, GetNextIndexDsmonHostTable, DsmonHostCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonHostTableINDEX, 5, 0, 0, NULL},

{{13,DsmonHostTopNCtlIndex}, GetNextIndexDsmonHostTopNCtlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonHostTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostTopNCtlHostIndex}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlHostIndexGet, DsmonHostTopNCtlHostIndexSet, DsmonHostTopNCtlHostIndexTest, DsmonHostTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonHostTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostTopNCtlRateBase}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlRateBaseGet, DsmonHostTopNCtlRateBaseSet, DsmonHostTopNCtlRateBaseTest, DsmonHostTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonHostTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostTopNCtlTimeRemaining}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlTimeRemainingGet, DsmonHostTopNCtlTimeRemainingSet, DsmonHostTopNCtlTimeRemainingTest, DsmonHostTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonHostTopNCtlTableINDEX, 1, 0, 0, "1800"},

{{13,DsmonHostTopNCtlGeneratedReports}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlGeneratedReportsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonHostTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostTopNCtlDuration}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonHostTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostTopNCtlRequestedSize}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlRequestedSizeGet, DsmonHostTopNCtlRequestedSizeSet, DsmonHostTopNCtlRequestedSizeTest, DsmonHostTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonHostTopNCtlTableINDEX, 1, 0, 0, "150"},

{{13,DsmonHostTopNCtlGrantedSize}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlGrantedSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonHostTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostTopNCtlStartTime}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonHostTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostTopNCtlOwner}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlOwnerGet, DsmonHostTopNCtlOwnerSet, DsmonHostTopNCtlOwnerTest, DsmonHostTopNCtlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonHostTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonHostTopNCtlStatus}, GetNextIndexDsmonHostTopNCtlTable, DsmonHostTopNCtlStatusGet, DsmonHostTopNCtlStatusSet, DsmonHostTopNCtlStatusTest, DsmonHostTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonHostTopNCtlTableINDEX, 1, 0, 1, NULL},

{{13,DsmonHostTopNIndex}, GetNextIndexDsmonHostTopNTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonHostTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonHostTopNPDLocalIndex}, GetNextIndexDsmonHostTopNTable, DsmonHostTopNPDLocalIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonHostTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonHostTopNAddress}, GetNextIndexDsmonHostTopNTable, DsmonHostTopNAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, DsmonHostTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonHostTopNAggGroup}, GetNextIndexDsmonHostTopNTable, DsmonHostTopNAggGroupGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonHostTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonHostTopNRate}, GetNextIndexDsmonHostTopNTable, DsmonHostTopNRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonHostTopNRateOvfl}, GetNextIndexDsmonHostTopNTable, DsmonHostTopNRateOvflGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonHostTopNTableINDEX, 2, 1, 0, NULL},

{{13,DsmonHostTopNHCRate}, GetNextIndexDsmonHostTopNTable, DsmonHostTopNHCRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonHostTopNTableINDEX, 2, 0, 0, NULL},

{{11,DsmonCapabilities}, NULL, DsmonCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,DsmonMatrixCtlIndex}, GetNextIndexDsmonMatrixCtlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlDataSource}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlDataSourceGet, DsmonMatrixCtlDataSourceSet, DsmonMatrixCtlDataSourceTest, DsmonMatrixCtlTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlAggProfile}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlAggProfileGet, DsmonMatrixCtlAggProfileSet, DsmonMatrixCtlAggProfileTest, DsmonMatrixCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlMaxDesiredEntries}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlMaxDesiredEntriesGet, DsmonMatrixCtlMaxDesiredEntriesSet, DsmonMatrixCtlMaxDesiredEntriesTest, DsmonMatrixCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlDroppedFrames}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlDroppedFramesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlInserts}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlInsertsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlDeletes}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlDeletesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlCreateTime}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlOwner}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlOwnerGet, DsmonMatrixCtlOwnerSet, DsmonMatrixCtlOwnerTest, DsmonMatrixCtlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonMatrixCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixCtlStatus}, GetNextIndexDsmonMatrixCtlTable, DsmonMatrixCtlStatusGet, DsmonMatrixCtlStatusSet, DsmonMatrixCtlStatusTest, DsmonMatrixCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonMatrixCtlTableINDEX, 1, 0, 1, NULL},

{{13,DsmonMatrixTimeMark}, GetNextIndexDsmonMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_NOACCESS, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixNLIndex}, GetNextIndexDsmonMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixSourceAddress}, GetNextIndexDsmonMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixDestAddress}, GetNextIndexDsmonMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixALIndex}, GetNextIndexDsmonMatrixSDTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixSDPkts}, GetNextIndexDsmonMatrixSDTable, DsmonMatrixSDPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixSDOvflPkts}, GetNextIndexDsmonMatrixSDTable, DsmonMatrixSDOvflPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixSDTableINDEX, 7, 1, 0, NULL},

{{13,DsmonMatrixSDHCPkts}, GetNextIndexDsmonMatrixSDTable, DsmonMatrixSDHCPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixSDOctets}, GetNextIndexDsmonMatrixSDTable, DsmonMatrixSDOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixSDOvflOctets}, GetNextIndexDsmonMatrixSDTable, DsmonMatrixSDOvflOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixSDTableINDEX, 7, 1, 0, NULL},

{{13,DsmonMatrixSDHCOctets}, GetNextIndexDsmonMatrixSDTable, DsmonMatrixSDHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixSDCreateTime}, GetNextIndexDsmonMatrixSDTable, DsmonMatrixSDCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonMatrixSDTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixDSPkts}, GetNextIndexDsmonMatrixDSTable, DsmonMatrixDSPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixDSTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixDSOvflPkts}, GetNextIndexDsmonMatrixDSTable, DsmonMatrixDSOvflPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixDSTableINDEX, 7, 1, 0, NULL},

{{13,DsmonMatrixDSHCPkts}, GetNextIndexDsmonMatrixDSTable, DsmonMatrixDSHCPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonMatrixDSTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixDSOctets}, GetNextIndexDsmonMatrixDSTable, DsmonMatrixDSOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixDSTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixDSOvflOctets}, GetNextIndexDsmonMatrixDSTable, DsmonMatrixDSOvflOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixDSTableINDEX, 7, 1, 0, NULL},

{{13,DsmonMatrixDSHCOctets}, GetNextIndexDsmonMatrixDSTable, DsmonMatrixDSHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonMatrixDSTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixDSCreateTime}, GetNextIndexDsmonMatrixDSTable, DsmonMatrixDSCreateTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonMatrixDSTableINDEX, 7, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlIndex}, GetNextIndexDsmonMatrixTopNCtlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlMatrixIndex}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlMatrixIndexGet, DsmonMatrixTopNCtlMatrixIndexSet, DsmonMatrixTopNCtlMatrixIndexTest, DsmonMatrixTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlRateBase}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlRateBaseGet, DsmonMatrixTopNCtlRateBaseSet, DsmonMatrixTopNCtlRateBaseTest, DsmonMatrixTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlTimeRemaining}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlTimeRemainingGet, DsmonMatrixTopNCtlTimeRemainingSet, DsmonMatrixTopNCtlTimeRemainingTest, DsmonMatrixTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, "1800"},

{{13,DsmonMatrixTopNCtlGeneratedRpts}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlGeneratedRptsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlDuration}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlDurationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlRequestedSize}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlRequestedSizeGet, DsmonMatrixTopNCtlRequestedSizeSet, DsmonMatrixTopNCtlRequestedSizeTest, DsmonMatrixTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, "150"},

{{13,DsmonMatrixTopNCtlGrantedSize}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlGrantedSizeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlStartTime}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlStartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlOwner}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlOwnerGet, DsmonMatrixTopNCtlOwnerSet, DsmonMatrixTopNCtlOwnerTest, DsmonMatrixTopNCtlTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DsmonMatrixTopNCtlTableINDEX, 1, 0, 0, NULL},

{{13,DsmonMatrixTopNCtlStatus}, GetNextIndexDsmonMatrixTopNCtlTable, DsmonMatrixTopNCtlStatusGet, DsmonMatrixTopNCtlStatusSet, DsmonMatrixTopNCtlStatusTest, DsmonMatrixTopNCtlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DsmonMatrixTopNCtlTableINDEX, 1, 0, 1, NULL},

{{13,DsmonMatrixTopNIndex}, GetNextIndexDsmonMatrixTopNTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNAggGroup}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNAggGroupGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNNLIndex}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNNLIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNSourceAddress}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNSourceAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNDestAddress}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNDestAddressGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNALIndex}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNALIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNPktRate}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNPktRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNPktRateOvfl}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNPktRateOvflGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 1, 0, NULL},

{{13,DsmonMatrixTopNHCPktRate}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNHCPktRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNRevPktRate}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNRevPktRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNRevPktRateOvfl}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNRevPktRateOvflGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 1, 0, NULL},

{{13,DsmonMatrixTopNHCRevPktRate}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNHCRevPktRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNOctetRate}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNOctetRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNOctetRateOvfl}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNOctetRateOvflGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 1, 0, NULL},

{{13,DsmonMatrixTopNHCOctetRate}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNHCOctetRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNRevOctetRate}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNRevOctetRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},

{{13,DsmonMatrixTopNRevOctetRateOvfl}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNRevOctetRateOvflGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 1, 0, NULL},

{{13,DsmonMatrixTopNHCRevOctetRate}, GetNextIndexDsmonMatrixTopNTable, DsmonMatrixTopNHCRevOctetRateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DsmonMatrixTopNTableINDEX, 2, 0, 0, NULL},
};
tMibData stddsmEntry = { 170, stddsmMibEntry };
#endif /* _STDDSMDB_H */

