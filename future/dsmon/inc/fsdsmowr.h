/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
*
* Description: Proto types for Low Level wrapper Routines
*********************************************************************/

#ifndef _FSDSMOWR_H
#define _FSDSMOWR_H

VOID RegisterFSDSMO(VOID);

VOID UnRegisterFSDSMO(VOID);
INT4 FsDsmonTraceGet(tSnmpIndex *, tRetVal *);
INT4 FsDsmonAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsDsmonTraceSet(tSnmpIndex *, tRetVal *);
INT4 FsDsmonAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsDsmonTraceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDsmonAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsDsmonTraceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsDsmonAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSDSMOWR_H */
