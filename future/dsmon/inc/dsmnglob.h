/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved 
 *
 * Description: This file externs Global Variable declaration
 *******************************************************************/
#ifndef _DSMNGLOB_H_
#define _DSMNGLOB_H_

tDsmonGlobals gDsmonGlobals;
tDsmonPktInfo gDsmonPktInfo;

#endif
