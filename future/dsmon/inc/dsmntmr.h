/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved 
 *
 * Description: This file contains definitions of DSMON Timer
 *******************************************************************/

#ifndef __DSMNTMR_H_
#define __DSMNTMR_H_

/* constants for timer types */
typedef enum {
	DSMON_POLL_TMR = 0,
	DSMON_MAX_TMRS
} enDsmonTmrId;

typedef struct _DSMON_TIMER {
       tTmrAppTimer    tmrNode;     /* Timer node information */
       enDsmonTmrId    eDsmonTmrId; /* TimerId of the timer */
} tDsmonTmr;

#endif  /* __DSMNTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  dsmntmr.h                      */
/*-----------------------------------------------------------------------*/
