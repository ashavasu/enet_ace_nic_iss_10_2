/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains definitions of DSMON Trace
 *******************************************************************/
#ifndef __DSMNTRC_H_
#define __DSMNTRC_H_

#ifdef TRACE_WANTED

#define  DSMON_TRC_FLAG gDsmonGlobals.u4DsmonTrace
#define  DSMON_NAME      "DSMON"
#define DSMON_TRC(Value, Fmt)           UtlTrcLog(DSMON_TRC_FLAG, \
                                                Value,        \
                                                DSMON_NAME,        \
                                                Fmt)

#define DSMON_TRC1(Value, Fmt, Arg)     UtlTrcLog(DSMON_TRC_FLAG, \
                                                Value,        \
                                                DSMON_NAME,        \
                                                Fmt,          \
                                                Arg)

#define DSMON_TRC2(Value, Fmt, Arg1, Arg2)                      \
                                      UtlTrcLog(DSMON_TRC_FLAG, \
                                                Value,        \
                                                DSMON_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2)

#define DSMON_TRC3(Value, Fmt, Arg1, Arg2, Arg3)                \
                                      UtlTrcLog(DSMON_TRC_FLAG, \
                                                Value,        \
                                                DSMON_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3)

#define DSMON_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)          \
                                      UtlTrcLog(DSMON_TRC_FLAG, \
                                                Value,        \
                                                DSMON_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2,   \
                                                Arg3, Arg4)

#define DSMON_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)    \
                                      UtlTrcLog(DSMON_TRC_FLAG, \
                                                Value,        \
                                                DSMON_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5)

#define DSMON_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
                                      UtlTrcLog(DSMON_TRC_FLAG, \
                                                Value,        \
                                                DSMON_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6)

#define DSMON_TRC7(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)  \
                                      UtlTrcLog(DSMON_TRC_FLAG, \
                                                Value,        \
                                                DSMON_NAME,        \
                                                Fmt,          \
                                                Arg1, Arg2, Arg3, \
                                                Arg4, Arg5, Arg6, Arg7)
#else /* TRACE_WANTED */

#define  DSMON_TRC_FLAG
#define  DSMON_NAME
#define  DSMON_TRC(Value, Fmt)
#define  DSMON_TRC1(Value, Fmt, Arg)
#define  DSMON_TRC2(Value, Fmt, Arg1, Arg2)
#define  DSMON_TRC3(Value, Fmt, Arg1, Arg2, Arg3)
#define  DSMON_TRC4(Value, Fmt, Arg1, Arg2, Arg3, Arg4)
#define  DSMON_TRC5(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5)
#define  DSMON_TRC6(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#define  DSMON_TRC7(Value, Fmt, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7)

#endif /* TRACE_WANTED */

#define DSMON_MIN_TRACE_LEVEL 1
#define DSMON_MAX_TRACE_LEVEL 6
/* DSMON FUNCTION Specific Trace Categories */
#define  DSMON_FN_ENTRY  	0x00000001
#define  DSMON_FN_EXIT   	0x00000002

/* Critical Trace */
#define  DSMON_CRITICAL_TRC  	0x00000004

/* DSMON MEMORY RESOURCE Specific Trace Categories */
#define  DSMON_MEM_FAIL  	0x00000008
#define  DSMON_DEBUG_TRC	0x00000010
#endif /* __DSMNTRACE_H__ */

