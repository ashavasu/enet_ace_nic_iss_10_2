/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved 
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonCapabilities ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonMaxAggGroups ARG_LIST((INT4 *));

INT1
nmhGetDsmonAggControlLocked ARG_LIST((INT4 *));

INT1
nmhGetDsmonAggControlChanges ARG_LIST((UINT4 *));

INT1
nmhGetDsmonAggControlLastChangeTime ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonAggControlLocked ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonAggControlLocked ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonAggControlLocked ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonAggControlTable. */
INT1
nmhValidateIndexInstanceDsmonAggControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonAggControlTable  */

INT1
nmhGetFirstIndexDsmonAggControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonAggControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonAggControlDescr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonAggControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonAggControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonAggControlDescr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonAggControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonAggControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonAggControlDescr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonAggControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonAggControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonAggControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonAggProfileTable. */
INT1
nmhValidateIndexInstanceDsmonAggProfileTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonAggProfileTable  */

INT1
nmhGetFirstIndexDsmonAggProfileTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonAggProfileTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonAggGroupIndex ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonAggGroupIndex ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonAggGroupIndex ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonAggProfileTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonAggGroupTable. */
INT1
nmhValidateIndexInstanceDsmonAggGroupTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonAggGroupTable  */

INT1
nmhGetFirstIndexDsmonAggGroupTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonAggGroupTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonAggGroupDescr ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonAggGroupStatus ARG_LIST((INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonAggGroupDescr ARG_LIST((INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonAggGroupStatus ARG_LIST((INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonAggGroupDescr ARG_LIST((UINT4 *  ,INT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonAggGroupStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonAggGroupTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonStatsControlTable. */
INT1
nmhValidateIndexInstanceDsmonStatsControlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonStatsControlTable  */

INT1
nmhGetFirstIndexDsmonStatsControlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonStatsControlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonStatsControlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDsmonStatsControlAggProfile ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonStatsControlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsControlCreateTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsControlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonStatsControlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonStatsControlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDsmonStatsControlAggProfile ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonStatsControlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonStatsControlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonStatsControlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DsmonStatsControlAggProfile ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonStatsControlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonStatsControlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonStatsControlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonStatsTable. */
INT1
nmhValidateIndexInstanceDsmonStatsTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonStatsTable  */

INT1
nmhGetFirstIndexDsmonStatsTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonStatsTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonStatsInPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsInOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsInOvflPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsInOvflOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsInHCPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonStatsInHCOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonStatsOutPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsOutOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsOutOvflPkts ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsOutOvflOctets ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonStatsOutHCPkts ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonStatsOutHCOctets ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for DsmonPdistCtlTable. */
INT1
nmhValidateIndexInstanceDsmonPdistCtlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonPdistCtlTable  */

INT1
nmhGetFirstIndexDsmonPdistCtlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonPdistCtlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonPdistCtlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDsmonPdistCtlAggProfile ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonPdistCtlMaxDesiredEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonPdistCtlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistCtlInserts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistCtlDeletes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistCtlCreateTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistCtlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonPdistCtlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonPdistCtlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDsmonPdistCtlAggProfile ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonPdistCtlMaxDesiredEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonPdistCtlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonPdistCtlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonPdistCtlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DsmonPdistCtlAggProfile ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonPdistCtlMaxDesiredEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonPdistCtlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonPdistCtlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonPdistCtlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonPdistStatsTable. */
INT1
nmhValidateIndexInstanceDsmonPdistStatsTable ARG_LIST((INT4  , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonPdistStatsTable  */

INT1
nmhGetFirstIndexDsmonPdistStatsTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonPdistStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonPdistStatsPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistStatsOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistStatsOvflPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistStatsOvflOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistStatsHCPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonPdistStatsHCOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonPdistStatsCreateTime ARG_LIST((INT4  , UINT4  , INT4  , INT4 ,UINT4 *));

/* Proto Validate Index Instance for DsmonPdistTopNCtlTable. */
INT1
nmhValidateIndexInstanceDsmonPdistTopNCtlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonPdistTopNCtlTable  */

INT1
nmhGetFirstIndexDsmonPdistTopNCtlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonPdistTopNCtlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonPdistTopNCtlPdistIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonPdistTopNCtlRateBase ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonPdistTopNCtlTimeRemaining ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonPdistTopNCtlGeneratedReprts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistTopNCtlDuration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonPdistTopNCtlRequestedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonPdistTopNCtlGrantedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonPdistTopNCtlStartTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistTopNCtlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonPdistTopNCtlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonPdistTopNCtlPdistIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonPdistTopNCtlRateBase ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonPdistTopNCtlTimeRemaining ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonPdistTopNCtlRequestedSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonPdistTopNCtlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonPdistTopNCtlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonPdistTopNCtlPdistIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonPdistTopNCtlRateBase ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonPdistTopNCtlTimeRemaining ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonPdistTopNCtlRequestedSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonPdistTopNCtlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonPdistTopNCtlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonPdistTopNCtlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonPdistTopNTable. */
INT1
nmhValidateIndexInstanceDsmonPdistTopNTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonPdistTopNTable  */

INT1
nmhGetFirstIndexDsmonPdistTopNTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonPdistTopNTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonPdistTopNPDLocalIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDsmonPdistTopNAggGroup ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDsmonPdistTopNRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistTopNRateOvfl ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonPdistTopNHCRate ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for DsmonHostCtlTable. */
INT1
nmhValidateIndexInstanceDsmonHostCtlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonHostCtlTable  */

INT1
nmhGetFirstIndexDsmonHostCtlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonHostCtlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonHostCtlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDsmonHostCtlAggProfile ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostCtlMaxDesiredEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostCtlIPv4PrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostCtlIPv6PrefixLen ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostCtlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonHostCtlInserts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonHostCtlDeletes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonHostCtlCreateTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonHostCtlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonHostCtlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonHostCtlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDsmonHostCtlAggProfile ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonHostCtlMaxDesiredEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonHostCtlIPv4PrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonHostCtlIPv6PrefixLen ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonHostCtlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonHostCtlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonHostCtlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DsmonHostCtlAggProfile ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonHostCtlMaxDesiredEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonHostCtlIPv4PrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonHostCtlIPv6PrefixLen ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonHostCtlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonHostCtlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonHostCtlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonHostTable. */
INT1
nmhValidateIndexInstanceDsmonHostTable ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for DsmonHostTable  */

INT1
nmhGetFirstIndexDsmonHostTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonHostTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonHostInPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetDsmonHostInOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetDsmonHostInOvflPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetDsmonHostInOvflOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetDsmonHostInHCPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonHostInHCOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonHostOutPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetDsmonHostOutOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetDsmonHostOutOvflPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetDsmonHostOutOvflOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetDsmonHostOutHCPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonHostOutHCOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonHostCreateTime ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for DsmonHostTopNCtlTable. */
INT1
nmhValidateIndexInstanceDsmonHostTopNCtlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonHostTopNCtlTable  */

INT1
nmhGetFirstIndexDsmonHostTopNCtlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonHostTopNCtlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonHostTopNCtlHostIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostTopNCtlRateBase ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostTopNCtlTimeRemaining ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostTopNCtlGeneratedReports ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonHostTopNCtlDuration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostTopNCtlRequestedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostTopNCtlGrantedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonHostTopNCtlStartTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonHostTopNCtlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonHostTopNCtlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonHostTopNCtlHostIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonHostTopNCtlRateBase ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonHostTopNCtlTimeRemaining ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonHostTopNCtlRequestedSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonHostTopNCtlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonHostTopNCtlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonHostTopNCtlHostIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonHostTopNCtlRateBase ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonHostTopNCtlTimeRemaining ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonHostTopNCtlRequestedSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonHostTopNCtlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonHostTopNCtlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonHostTopNCtlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonHostTopNTable. */
INT1
nmhValidateIndexInstanceDsmonHostTopNTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonHostTopNTable  */

INT1
nmhGetFirstIndexDsmonHostTopNTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonHostTopNTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonHostTopNPDLocalIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDsmonHostTopNAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonHostTopNAggGroup ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDsmonHostTopNRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonHostTopNRateOvfl ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonHostTopNHCRate ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for DsmonMatrixCtlTable. */
INT1
nmhValidateIndexInstanceDsmonMatrixCtlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonMatrixCtlTable  */

INT1
nmhGetFirstIndexDsmonMatrixCtlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonMatrixCtlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonMatrixCtlDataSource ARG_LIST((INT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetDsmonMatrixCtlAggProfile ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixCtlMaxDesiredEntries ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixCtlDroppedFrames ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixCtlInserts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixCtlDeletes ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixCtlCreateTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixCtlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonMatrixCtlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonMatrixCtlDataSource ARG_LIST((INT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetDsmonMatrixCtlAggProfile ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonMatrixCtlMaxDesiredEntries ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonMatrixCtlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonMatrixCtlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonMatrixCtlDataSource ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2DsmonMatrixCtlAggProfile ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonMatrixCtlMaxDesiredEntries ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonMatrixCtlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonMatrixCtlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonMatrixCtlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonMatrixSDTable. */
INT1
nmhValidateIndexInstanceDsmonMatrixSDTable ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonMatrixSDTable  */

INT1
nmhGetFirstIndexDsmonMatrixSDTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonMatrixSDTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonMatrixSDPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixSDOvflPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixSDHCPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonMatrixSDOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixSDOvflOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixSDHCOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonMatrixSDCreateTime ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

/* Proto Validate Index Instance for DsmonMatrixDSTable. */
INT1
nmhValidateIndexInstanceDsmonMatrixDSTable ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonMatrixDSTable  */

INT1
nmhGetFirstIndexDsmonMatrixDSTable ARG_LIST((INT4 * , UINT4 * , INT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonMatrixDSTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonMatrixDSPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixDSOvflPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixDSHCPkts ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonMatrixDSOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixDSOvflOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixDSHCOctets ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonMatrixDSCreateTime ARG_LIST((INT4  , UINT4  , INT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , INT4 ,UINT4 *));

/* Proto Validate Index Instance for DsmonMatrixTopNCtlTable. */
INT1
nmhValidateIndexInstanceDsmonMatrixTopNCtlTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonMatrixTopNCtlTable  */

INT1
nmhGetFirstIndexDsmonMatrixTopNCtlTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonMatrixTopNCtlTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonMatrixTopNCtlMatrixIndex ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNCtlRateBase ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNCtlTimeRemaining ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNCtlGeneratedRpts ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNCtlDuration ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNCtlRequestedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNCtlGrantedSize ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNCtlStartTime ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNCtlOwner ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonMatrixTopNCtlStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetDsmonMatrixTopNCtlMatrixIndex ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonMatrixTopNCtlRateBase ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonMatrixTopNCtlTimeRemaining ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonMatrixTopNCtlRequestedSize ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetDsmonMatrixTopNCtlOwner ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetDsmonMatrixTopNCtlStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2DsmonMatrixTopNCtlMatrixIndex ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonMatrixTopNCtlRateBase ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonMatrixTopNCtlTimeRemaining ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonMatrixTopNCtlRequestedSize ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2DsmonMatrixTopNCtlOwner ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2DsmonMatrixTopNCtlStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2DsmonMatrixTopNCtlTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for DsmonMatrixTopNTable. */
INT1
nmhValidateIndexInstanceDsmonMatrixTopNTable ARG_LIST((INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for DsmonMatrixTopNTable  */

INT1
nmhGetFirstIndexDsmonMatrixTopNTable ARG_LIST((INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexDsmonMatrixTopNTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetDsmonMatrixTopNAggGroup ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNNLIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNSourceAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonMatrixTopNDestAddress ARG_LIST((INT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetDsmonMatrixTopNALIndex ARG_LIST((INT4  , INT4 ,INT4 *));

INT1
nmhGetDsmonMatrixTopNPktRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNPktRateOvfl ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNHCPktRate ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonMatrixTopNRevPktRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNRevPktRateOvfl ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNHCRevPktRate ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonMatrixTopNOctetRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNOctetRateOvfl ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNHCOctetRate ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetDsmonMatrixTopNRevOctetRate ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNRevOctetRateOvfl ARG_LIST((INT4  , INT4 ,UINT4 *));

INT1
nmhGetDsmonMatrixTopNHCRevOctetRate ARG_LIST((INT4  , INT4 ,tSNMP_COUNTER64_TYPE *));
