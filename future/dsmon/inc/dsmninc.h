/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved 
 * 
 * $Id: dsmninc.h,v 1.7 2013/01/10 12:46:10 siva Exp $           *  
 * 
 * Description: This file includes all header files used by DSMON
 *******************************************************************/
#ifndef _DSMNINC_H_
#define _DSMNINC_H_

#include "lr.h"
#include "iss.h"
#include "cfa.h"
#include "l2iwf.h"
#include "rmn2inc.h"
#include "rmon2.h"
#include "dsmon.h"
#include "dsmndefn.h"
#include "dsmnenm.h"
#include "dsmntmr.h"
#include "dsmntdfs.h"
#include "fsutil.h"

#ifdef _DSMNMAIN_C_
 #include "dsmnglob.h"
#else
 #include "dsmnextn.h"
#endif /* _DSMNMAIN_C_ */

#include "rmon2np.h"
#include "dsmonnp.h"

#include "dsmntrc.h"
#include "dsmnprot.h"

#include "stddsmwr.h"
#include "stddsmlw.h"
#include "fsdsmowr.h"
#include "fsdsmolw.h"

#include "dsmonsz.h"

#ifdef NPAPI_WANTED
#include "dsmnnpwr.h"
#endif

#endif /* _DSMNINC_H_ */
