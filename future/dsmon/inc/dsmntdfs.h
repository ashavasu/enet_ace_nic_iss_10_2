/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 * $Id: dsmntdfs.h,v 1.5 2012/04/03 13:16:40 siva Exp $
 * Description: This file contains definitions of DSMON Tables
 *******************************************************************/
#ifndef _DSMNTDFS_H_
#define _DSMNTDFS_H_

typedef struct DSMONGLOBALS {
    struct DSMONPKTINFO *pDsmonLastVisted;    /* Last Visited PktInfo Tree Entry */
    tRBTree dsmonAggCtlRBTree;                /* RBTree for Agg control */
    tRBTree dsmonAggCtlGroupRBTree;           /* RBTree for Agg control group */
    tRBTree dsmonStatsCtlRBTree;       /* RBTree for StatsCtl */
    tRBTree dsmonStatsRBTree;              /* RBTree for Stats */
    tRBTree dsmonPdistCtlRBTree;             /* RBTree for PdistCtl */
    tRBTree dsmonPdistRBTree;             /* RBTree for PdistStats */
    tRBTree dsmonPdistTopNCtlRBTree;          /* RBTree for TopN PdistCtl */
    tRBTree dsmonPdistTopNRBTree;             /* RBTree for TopN Pdist */
    tRBTree dsmonHostCtlRBTree;               /* RBTree for HostCtl */
    tRBTree dsmonHostRBTree;                  /* RBTree for HostStats */
    tRBTree dsmonHostTopNCtlRBTree;           /* RBTree for TopN HostCtl */
    tRBTree dsmonHostTopNRBTree;              /* RBTree for TopN Host */
    tRBTree dsmonMatrixCtlRBTree;            /* RBTree for MatrixCtl */
    tRBTree dsmonMatrixRBTrees [DSMON_MAX_MATRIX_TABLES];
               /* RBTrees for MatrixSD and Matrix DS */
    tRBTree dsmonMatrixTopNCtlRBTree;       /* RBTree for TopN MatrixCtl */
    tRBTree dsmonMatrixTopNRBTree;       /* RBTree for TopN Matrix */
    tRBTree dsmonPktInfoRBTree;              /* RBTree for PktInfo */
    tMemPoolId dsmonAggCtlPoolId;             /* Pool for Agg control */
    tMemPoolId dsmonAggGroupPoolId;           /* Pool for Agg group control */
    tMemPoolId dsmonStatsCtlPoolId;           /* Pool for Stats control */
    tMemPoolId dsmonStatsPoolId;              /* Pool for Stats */
    tMemPoolId dsmonPdistCtlPoolId;           /* Pool for Pdist control */
    tMemPoolId dsmonPdistStatsPoolId;         /* Pool for Pdist stats */
    tMemPoolId dsmonPdistTopNCtlPoolId;       /* Pool for Pdist topN control */
    tMemPoolId dsmonPdistTopNPoolId;          /* Pool for Pdist topN */
    tMemPoolId dsmonHostCtlPoolId;            /* Pool for Host control */
    tMemPoolId dsmonHostStatsPoolId;          /* Pool for Host stats */
    tMemPoolId dsmonHostTopNCtlPoolId;        /* Pool for Host topN control */
    tMemPoolId dsmonHostTopNPoolId;           /* Pool for Host topN */
    tMemPoolId dsmonMatrixCtlPoolId;          /* Pool for Matrix control */
    tMemPoolId dsmonMatrixSDPoolId;           /* Pool for Matrix SD */
    tMemPoolId dsmonMatrixDSPoolId;           /* Pool for Matrix DS*/
    tMemPoolId dsmonMatrixTopNCtlPoolId;      /* Pool for Matrix topN control */
    tMemPoolId dsmonMatrixTopNPoolId;         /* Pool for Matrix topN */
    tMemPoolId dsmonPktInfoPoolId;           /* Pool for PktInfo */
    tMemPoolId dsmonHostSampleTopNPoolId;     /* Pool for Host Sample Top N */
    tMemPoolId dsmonMatrixSampleTopNPoolId;   /* Pool for Matrix Sample Top N */
    tOsixSemId dsmonSemId;                    /* Dsmon Semaphore */
    tTmrDesc aDsmonTmrDesc[DSMON_MAX_TMRS];   /* Timer data struct that contains
            * func ptrs for timer handling and
            * offsets to identify the data
            * struct containing timer block.
            * Timer ID is the index to this
            * data structure */
    tTimerListId dsmonTmrLst;                 /* Dsmon Timer List Id */
    tDsmonTmr dsmonPollTmr;                   /* Poll timer node */
    tSNMP_OID_TYPE interfaceOid;              /* OID of interface index */
    UINT4 u4DsmonAdminStatus;                 /* Dsmon Admin Status */
    UINT4 u4DsmonTrace;                       /* Dsmon Trace ststus */
    UINT4 u4DsmonMaxAggGroups;                /* Maximum Aggregation groups that 
                                               * the agent can support */
    UINT4 u4DsmonAggCtlChanges;               /* Number of time u1DsmonAggCtlLock
                                               * has changed */
    UINT4 u4DsmonAggCtlLastChgTime;           /* Time when u1DsmonAggCtlLock was 
                                               * last changed */
    UINT2 u2DsmonCapabilities;                /* DSMON groups supported by 
                                               * the agent */
    UINT1 u1DsmonAggCtlLock;                  /* Counter Aggregation group 
                                               * control Lock */
    UINT1 u1DsmonRsvd;                        /* Padding */
} tDsmonGlobals;

/* ---------- Counter Aggregation Group ----------*/

typedef struct DSMONAGGCTLENTRY {   
    tRBNodeEmbd         RbNode;     /* RBTree */
    UINT4 au4DsmonAggProfile[DSMON_MAX_AGG_PROFILE_DSCP];
                                    /* Array of aggregate profiles per 
                                     * control entry */
    UINT4 u4DsmonAggCtlIndex;       /* Aggregate control index */
    UINT4 u4DsmonAggCtlRowStatus;   /* Row Status */
    UINT4 u4DsmonAggGroupsConfigured; /* No. of groups configured */
    UINT1 au1DsmonAggCtlDesc[DSMON_MAX_DESC_LEN + 4];
                                    /* Description of the control entry */
    UINT1 au1DsmonAggCtlOwner[DSMON_MAX_OWNER_LEN + 1];
                                    /* Owner of the control entry*/
} tDsmonAggCtl;

typedef struct DSMONAGGGROUPENTRY {
    tRBNodeEmbd         RbNode;     /* RBTree */
    UINT4 u4DsmonAggCtlIndex;       /* Aggregate control index */
    UINT4 u4DsmonAggGroupIndex;     /* Group Index */
    UINT4 u4DsmonAggRowStatus;      /* Status of the row */
    UINT1 au1DsmonGroupDesc[DSMON_MAX_DESC_LEN + 4];
                                    /* Description of the group entry */
} tDsmonAggGroup;

/* ---------- Statistics Group ----------*/

typedef struct DSMONSTATSCTLENTRY {
    tDsmonAggCtl *pAggCtl;  /* Reference to Aggregation Control Entry */
    tRBNodeEmbd  RbNode;          /* RBTree for Statctl*/
    UINT4 u4DsmonStatsCtlIndex;         /* Stats control index */
    UINT4 u4DsmonStatsDataSource;     /* Interface Index */
    UINT4 u4DsmonStatsCtlProfileIndex;  /* Aggregate control index */
    UINT4 u4DsmonStatsCtlDroppedFrames; /* Number of dropped frames */
    UINT4 u4DsmonStatsCtlCreateTime;    /* Sys up time when the entry was 
                                         * last activated */
    UINT4 u4DsmonStatsCtlRowStatus;     /* Status of the row */
    UINT1 au1DsmonStatsCtlOwner[DSMON_MAX_OWNER_LEN + 1];
                                        /* Owner of the control entry */
} tDsmonStatsCtl;

typedef struct DSMONSTATSENTRY {
    tDsmonStatsCtl    *pStatsCtl;   /* Reference to Stats Control Entry */
    struct DSMONSTATSENTRY   *pPrevStats;  /* Reference to Previous Stats Entry */
    struct DSMONSTATSENTRY   *pNextStats;  /* Reference to Next Stats Entry */
    tRBNodeEmbd  RbNode;            /* RBTree for Stats*/
    tUint8 u8DsmonStatsInHCPkts;     /* High capacity counter for
                                     * incoming packets */
    tUint8 u8DsmonStatsInHCOctets;   /* High capacity counter for
                                     * incoming octets */
    tUint8 u8DsmonStatsOutHCPkts;    /* High capacity counter for
                                     * outgoing packets */
    tUint8 u8DsmonStatsOutHCOctets;  /* High capacity counter for
                                     * outgoing octets */
    UINT4 u4DsmonStatsInPkts;       /* Incoming packet counter */
    UINT4 u4DsmonStatsInOctets;     /* Incoming octet counter */
    UINT4 u4DsmonStatsOutPkts;      /* Outgoing packet counter */
    UINT4 u4DsmonStatsOutOctets;    /* Outgoing octet counter */
    UINT4 u4DsmonStatsCtlIndex;     /* Stats control index */
    UINT4 u4DsmonStatsAggGroupIndex;/* Aggregate group index */
    UINT4 u4PktRefCount;  /* Packet Reference count */
} tDsmonStats;

/* ---------- Protocol Distribution Group ----------*/

typedef struct DSMONPDISTCTLENTRY {
    tDsmonAggCtl    *pAggCtl;  /* Reference to Aggregation Control Entry */
    tRBNodeEmbd     RbNode;          /* RBTree */
    UINT4 u4DsmonPdistCtlIndex;         /* Protocol dist control index */
    UINT4 u4DsmonPdistDataSource;     /* Interface Index */
    UINT4 u4DsmonPdistCtlProfileIndex;  /* Aggregate control index */
    INT4  i4DsmonPdistCtlMaxDesiredEntries;
                                        /* Number of entries desired 
                                         * by the administrator */
    UINT4 u4DsmonPdistCtlMaxDesiredSupported;
                                        /* Number of entries desired supported in
      * Pdist Ctl entry  */

    UINT4 u4DsmonPdistCtlDroppedFrames; /* Number of dropped frames */
    UINT4 u4DsmonPdistCtlInserts;       /* Number of times entry has been 
                                         * inserted to PdistStats table */
    UINT4 u4DsmonPdistCtlDeletes;       /* Number of times entry has been 
                                         * deleted from PdistStats Table */
    UINT4 u4DsmonPdistCtlCreateTime;    /* Sys up time when this entry was last 
                                         * activated */
    UINT4 u4DsmonPdistCtlRowStatus;     /* Status of the row */
    UINT1 au1DsmonPdistCtlOwner[DSMON_MAX_OWNER_LEN + 1];
                                        /* Owner of the control entry */
} tDsmonPdistCtl;

typedef struct DSMONPDISTSAMPLE {
    tUint8 u8DsmonPdistStatsHCPkts;      /* High capacity counter for 
                                         * protocol specific packets */
    tUint8 u8DsmonPdistStatsHCOctets;    /* High capacity counter for 
                                         * protocol specific octets */
    UINT4 u4DsmonPdistStatsPkts;        /* Protocol specific packet counter */
    UINT4 u4DsmonPdistStatsOctets;      /* Protocol specific octet counter */
} tDsmonPdistSample;

typedef struct DSMONPDISTSTATSENTRY {
    tDsmonPdistCtl   *pPdistCtl; /* Reference to Pdist Control entry */
    struct DSMONPDISTSTATSENTRY *pPrevPdist; /* Reference to Previous Pdist entry */
    struct DSMONPDISTSTATSENTRY *pNextPdist; /* Reference to Next Pdist entry */
    tRBNodeEmbd       RbNode;           /* RBTree */
    tDsmonPdistSample DsmonCurSample;   /* Current Pdist Stats Sample */
    tDsmonPdistSample DsmonPrevSample;  /* Previous Pdist Stats Sample */
    UINT4 u4DsmonPdistCtlIndex;         /* Protocol dist control index */
    UINT4 u4DsmonPdistTimeMark;         /* Time filter index */
    UINT4 u4DsmonPdistAggGroupIndex;    /* Aggregate group index */
    UINT4 u4DsmonPdistProtDirLocalIndex;/* Protocol directory local index */
    UINT4 u4PktRefCount;  /* Packet Reference count */
    UINT4 u4DsmonPdistStatsCreateTime;  /* Sys up time when the entry was last
                                         * activated */
} tDsmonPdistStats;

typedef struct DSMONPISTTOPNENTRY {
    tRBNodeEmbd      RbNode;        /* RBTree */
    tUint8 u8DsmonPdistTopNHCRate;       /* High capacity counter for the 
                                         * change in selected interval */
    UINT4 u4DsmonPdistTopNPDLocalIndex; /* AL Protocol directory local index */
    UINT4 u4DsmonPdistTopNAggGroupIndex;/* Aggregate Group Index */ 
    UINT4 u4DsmonPdistTopNRate;         /* Change in selected interval */
    UINT4 u4DsmonPdistTopNCtlIndex;     /* Protocol TopN control index */
    UINT4 u4DsmonPdistTopNIndex;      /* Protocol TopN index */
} tDsmonPdistTopN;

typedef struct DSMONPDISTTOPNCTLENTRY {
    tRBNodeEmbd     RbNode;               /* RBTree */
    UINT4 u4DsmonPdistTopNCtlIndex;       /* Protocol TopN control index */
    UINT4 u4DsmonPdistTopNCtlPdistIndex;  /* Protocol control index reference */
    UINT4 u4DsmonPdistTopNCtlRateBase;    /* Variable on which TopN has 
                                           * to be done */
    UINT4 u4DsmonPdistTopNCtlTimeRem;     /* Time remaining for the 
                                           * sample to be taken */
    UINT4 u4DsmonPdistTopNCtlGenReports;  /* Number of reports generated */
    UINT4 u4DsmonPdistTopNCtlDuration;    /* Total duration for collecting 
                                           * sample */
    UINT4 u4DsmonPdistTopNCtlReqSize;     /* Number of TopN entries requested 
                                           * by the administrator */
    UINT4 u4DsmonPdistTopNCtlGranSize;    /* Number of TopN entries granted */
    UINT4 u4DsmonPdistTopNCtlStartTime;   /* Sys up time when the collection 
                                           * was last started */
    UINT4 u4DsmonPdistTopNCtlRowStatus;   /* Status of the row */
    UINT1 au1DsmonPdistTopNCtlOwner[DSMON_MAX_OWNER_LEN + 1];
                                          /* Owner of the control entry */
} tDsmonPdistTopNCtl;

/* ---------- Host Distribution Group ----------*/

typedef struct DSMONHOSTCTLENTRY {
    tDsmonAggCtl *pAggCtl;     /* Reference to Aggregation Control Entry */
    tRBNodeEmbd  RbNode;            /* RBTree */
    UINT4 u4DsmonHostCtlIndex;             /* Host control index */
    UINT4 u4DsmonHostDataSource;    /* Interface Index */
    UINT4 u4DsmonHostCtlProfileIndex;      /* Aggregate control index */
    INT4  i4DsmonHostCtlMaxDesiredEntries; /* Number of entries desired in 
                                            * dsmonHost entry corresponding to 
                                            * control entry */
    UINT4 u4DsmonHostCtlMaxDesiredSupported;
                                           /* Number of entries desired supported in
         * Host Ctl entry  */
    UINT4 u4DsmonHostCtlIPv4PrefixLen;     /* Number of leftmost continuous bits
                                            * that are set in IPv4 hostaddress 
                                            * field in hostStats entry */
    UINT4 u4DsmonHostCtlIPv6PrefixLen;     /* Number of leftmost continuous bits
                                            * that are set in IPv6 hostaddress
                                            * field in hostStats entry */
    UINT4 u4DsmonHostCtlDroppedFrames;     /* Number of dropped frames */
    UINT4 u4DsmonHostCtlInserts;           /* Number of times dsmonHost entry is
                                            * inserted to dsmonHost table */
    UINT4 u4DsmonHostCtlDeletes;           /* Number of times dsmonHost entry is 
                                            * deleted from dsmonHost table */
    UINT4 u4DsmonHostCtlCreateTime;        /* Sys up time when entry was 
                                            * activated last */
    UINT4 u4DsmonHostCtlRowStatus;         /* Status of the row */
    UINT1 au1DsmonHostCtlOwner[DSMON_MAX_OWNER_LEN + 1];
                                           /* Owner of the control entry */
} tDsmonHostCtl;

typedef struct DSMONHOSTSAMPLE {
    tUint8 u8DsmonHostInHCPkts;     /* High capacity counter for 
                                      incoming packets per host */
    tUint8 u8DsmonHostInHCOctets;   /* High capacity counter for 
                                    * incomong octets per host */
    tUint8 u8DsmonHostOutHCPkts;    /* High capacity counter for 
                                    * outgoing packets per host */
    tUint8 u8DsmonHostOutHCOctets;  /* High capacity counter for 
                                    * outgoing octets per host */
    UINT4 u4DsmonHostInPkts;       /* Number of incoming packets per host */
    UINT4 u4DsmonHostInOctets;     /* Number of incoming octets per host */
    UINT4 u4DsmonHostOutPkts;      /* Number of outgoing packets per host */
    UINT4 u4DsmonHostOutOctets;    /* Number of outgoing octets per host*/
} tDsmonHostSample;

typedef struct DSMONHOSTSTATSENTRY {
    tDsmonHostCtl    *pHostCtl;  /* Reference to Host Control Entry */
    struct DSMONHOSTSTATSENTRY  *pPrevHost; /* Reference to Previous Host Entry */
    struct DSMONHOSTSTATSENTRY  *pNextHost; /* Reference to Next Host Entry */
    tRBNodeEmbd      RbNode;     /* RBTree */
    tDsmonHostSample DsmonCurSample; /* Current Host Stats Sample */
    tDsmonHostSample DsmonPrevSample;   /* Prev Host Stats Sample */
    tIpAddr DsmonHostNLAddress;           /* Network address of this entry */
    UINT4 u4DsmonHostIpAddrLen;     /* IP Address Length */
    UINT4 u4DsmonHostCtlIndex;             /* Host control index */
    UINT4 u4DsmonHostTimeMark;      /* Time filter entry */
    UINT4 u4DsmonHostAggGroupIndex;     /* Aggregate group index */
    UINT4 u4DsmonHostProtDirLocalIndex; /* Protocol directory local index */
    UINT4 u4PktRefCount;  /* Packet Reference count */
    UINT4 u4DsmonHostCreateTime;    /* Sys up time when the entry was last 
                                      * activated */
} tDsmonHostStats;

typedef struct DSMONHOSTTOPNENTRY {
    tRBNodeEmbd      RbNode;         /* RBTree */
    tIpAddr DsmonHostNLAddress;     /* Network address of this entry */
    tUint8 u8DsmonHostTopNHCRate;           /* High capacity counter for the 
                                            * change in selected variable for the
                                            * sampling time*/
    UINT4 u4DsmonHostTopNPDLocalIndex;    /* NL Protocol Dir Local Index */
    UINT4 u4DsmonHostTopNAggGroupIndex;    /* Aggregate group index */
    UINT4 u4DsmonHostTopNRate;             /* Change in selected variable for the
                                            * sampling time */
    UINT4 u4DsmonHostIpAddrLen;     /* IP Address Length */
    UINT4 u4DsmonHostTopNCtlIndex;         /* Host topN control index */
    UINT4 u4DsmonHostTopNIndex;            /* Host topN index */
} tDsmonHostTopN;

typedef struct DSMONHOSTTOPNCTLENTRY {
    tRBNodeEmbd     RbNode;         /* RBTree */
    UINT4 u4DsmonHostTopNCtlIndex;         /* Host topN control index */
    UINT4 u4DsmonHostTopNCtlHostIndex;     /* Host control index reference */
    UINT4 u4DsmonHostTopNCtlRateBase;      /* Variable on which topN has 
                                            * to be done */
    UINT4 u4DsmonHostTopNCtlTimeRem;       /* Time remaining for the sample 
                                            * to be taken */
    UINT4 u4DsmonHostTopNCtlGenReports;    /* Number of reports generated */
    UINT4 u4DsmonHostTopNCtlDuration;      /* Total duration for collecting 
                                            * sample */
    UINT4 u4DsmonHostTopNCtlReqSize;       /* Number of topN entries requested 
                                            * by the administrator */
    UINT4 u4DsmonHostTopNCtlGranSize;      /* Number of samples granted */
    UINT4 u4DsmonHostTopNCtlStartTime;     /* Sys up time when the collection 
                                            * was last started */
    UINT4 u4DsmonHostTopNCtlRowStatus;     /* Status of the row */
    UINT1 au1DsmonHostTopNCtlOwner[DSMON_MAX_OWNER_LEN + 1];
                                           /* Owner of the control entry */
} tDsmonHostTopNCtl;

/* ---------- Matrix Distribution Group ----------*/

typedef struct DSMONMATRIXCTLENTRY {
    tDsmonAggCtl   *pAggCtl;   /* Reference to Aggregation Control Entry */
    tRBNodeEmbd    RbNode;           /* RBTree */
    UINT4 u4DsmonMatrixCtlIndex;         /* Matrix control index */
    UINT4 u4DsmonMatrixDataSource;  /* Interface Index */
    UINT4 u4DsmonMatrixCtlAggProfile;    /* Aggregate control index */
    INT4  i4DsmonMatrixCtlMaxDesiredEnt; /* Number of entries desired in 
                                            dsmonMatrix table */
    UINT4 u4DsmonMatrixCtlMaxDesiredSupported;
                                        /* Number of entries desired supported in
      * Matrix Ctl entry  */


    UINT4 u4DsmonMatrixCtlDroppedFrames; /* Number of frames dropped */
    UINT4 u4DsmonMatrixCtlInserts;       /* Number of times dsmonMatrix entries 
                                          * has been inserted */
    UINT4 u4DsmonMatrixCtlDeletes;       /* Number of times dsmonMatrix entries
                                          * has been deleted */
    UINT4 u4DsmonMatrixCtlCreateTime;    /* Sys up time when the entry was 
                                          * last created */
    UINT4 u4DsmonMatrixCtlRowStatus;     /* Status of the row */
    UINT1 au1DsmonMatrixCtlOwner[DSMON_MAX_OWNER_LEN + 1];
                                         /* Owner of the control entry */
} tDsmonMatrixCtl;

typedef struct DSMONMATRIXSDSAMPLE {
    tUint8 u8DsmonMatrixSDHCPkts;         /* High capacity counter for the number 
                                          * of packets of this AL protocol */
    tUint8 u8DsmonMatrixSDHCOctets;       /* High capacity counter for the number
                                          * of octets of this AL protocol */
    UINT4 u4DsmonMatrixSDPkts;           /* Number of packets of this AL 
                                            protocol */
    UINT4 u4DsmonMatrixSDOctets;         /* Number of octets of this AL 
                                          * protocol */
} tDsmonMatrixSDSample;

typedef struct DSMONMATRIXSDTABLE {
    tDsmonMatrixCtl   *pMatrixCtl;  /* Reference to Matrixctl */
    struct DSMONMATRIXSDTABLE   *pPrevMatrixSD; /* Reference to Previous MatrixSD Entry */
    struct DSMONMATRIXSDTABLE  *pNextMatrixSD; /* Reference to Next MatrixSD Entry */
    tRBNodeEmbd  MatrixSDRbNode;         /* RBTree for Matrix SD */
    tRBNodeEmbd  MatrixDSRbNode;         /* RBTree for Matrix SD */
    tDsmonMatrixSDSample DsmonPrevMatrixSDSample;
          /* Prev MatrixSD Stats Sample */
    tDsmonMatrixSDSample DsmonCurMatrixSDSample;
          /* Current MatrixSD Stats Sample */
    tIpAddr DsmonMatrixSDSourceAddress;  /* Source address */
    tIpAddr DsmonMatrixSDDestAddress;  /* Destination address */
    UINT4 u4DsmonMatrixIpAddrLen;    /* IP Address Length */
    UINT4 u4DsmonMatrixCtlIndex;  /* Matrix control index */
    UINT4 u4DsmonMatrixSDTimeMark;       /* Time filter */
    UINT4 u4DsmonMatrixSDGroupIndex;     /* Aggregate group index */
    UINT4 u4DsmonMatrixSDNLIndex;  /* Protocol directory local index of Network Layer */
    UINT4 u4DsmonMatrixSDALIndex;  /* Protocol directory local index of Application Layer*/
    UINT4 u4DsmonMatrixSDCreateTime;     /* Sys up time when the entry was 
                                          * last activated */
    UINT4 u4PktRefCount;                 /* Packet Reference count */
} tDsmonMatrixSD;

typedef struct DSMONMATRIXTOPNENTRY {
    tRBNodeEmbd      RbNode;         /* RBTree */
    tIpAddr DsmonMatrixTopNSrcAddr;    /* Address of the source host 
             * identified in this entry */
    tIpAddr DsmonMatrixTopNDestAddr;    /* Address of the destination host
         * identified in this entry */
    tUint8 u8DsmonMatrixTopNHCPktRate;      /* High capacity counter for packets 
                                            * from source to destination in the
                                            * sampling interval for 
                                            * this protocol*/
    tUint8 u8DsmonMatrixTopNHCRevPktRate;   /* High capacity counter for packets 
                                            * from destination to source in the
                                            * sampling interval for 
                                            * this protocol */
    tUint8 u8DsmonMatrixTopNHCOctetRate;    /* High capacity counter for octets 
                                            * from source to destination in the
                                            * sampling interval for 
                                            * this protocol */
    tUint8 u8DsmonMatirxTopNHCRevOctetRate; /* High capacity counter for octets 
                                            * from destination to source in the
                                            * sampling interval for 
                                            * this protocol */
    UINT4 u4DsmonMatrixTopNPktRate;        /* Number of packets from source to 
                                            * destination in the sampling 
                                            * interval for this protocol */
    UINT4 u4DsmonMatrixTopNRevPktRate;     /* Number of packets from destination 
                                            * source in the sampling interval 
                                            * for this protocol */
    UINT4 u4DsmonMatrixTopNOctetRate;      /* Number of octets from source to 
                                            * destination in the sampling 
                                            * interval for this protocol */
    UINT4 u4DsmonMatrixTopNRevOctetRate;   /* Number or octets from destination 
                                            * to source in the sampling interval
                                            * for this protocol */
    UINT4 u4DsmonMatrixIpAddrLen;    /* IP Address Length */
    UINT4 u4DsmonMatrixTopNAggGroup;       /* Aggregate group index */
    UINT4 u4DsmonMatrixTopNNLIndex;        /* Protocol dir Local index of the 
                                            * network level protocol */
    UINT4 u4DsmonMatrixTopNALIndex;        /* Protocol dir local indes of the
                                            * application level protocol */
    UINT4 u4DsmonMatrixTopNCtlIndex;       /* Control index for topN table */
    UINT4 u4DsmonMatrixTopNIndex;      /* Index for topN table */
} tDsmonMatrixTopN;

typedef struct DSMONMATRIXTOPNCTLENTRY {
    tRBNodeEmbd      RbNode;         /* RBTree */
    UINT4 u4DsmonMatrixTopNCtlIndex;       /* Control index for topN table */
    UINT4 u4DsmonMatrixTopNCtlMatrixIndex; /* Matrix Control index reference */
    UINT4 u4DsmonMatrixTopNCtlRateBase;    /* Variable on which topN has 
                                            * to be done */
    UINT4 u4DsmonMatrixTopNCtlTimeRem;     /* Time remaining for the sample to
                                            * be taken */
    UINT4 u4DsmonMatrixTopNCtlGenReports;  /* Number of reports generated */
    UINT4 u4DsmonMatrixTopNCtlDuration;    /* Total duration for collecting 
                                            * sample */
    UINT4 u4DsmonMatrixTopNCtlReqSize;     /* Number of topN entries requested 
                                            * by the administrator */
    UINT4 u4DsmonMatrixTopNCtlGranSize;    /* Number of topN entries granted */
    UINT4 u4DsmonMatrixTopNCtlStartTime;   /* Sys up time when the collection 
                                            * was last started */
    UINT4 u4DsmonMatrixTopNCtlRowStatus;   /* Status of the row */
    UINT1 au1DsmonMatrixTopNCtlOwner[DSMON_MAX_OWNER_LEN + 1];
                                           /* Owner of the control entry */
} tDsmonMatrixTopNCtl;

typedef struct DSMONPKTINFO
{
  tRBNodeEmbd      RbNode;           /* RBTree */
  tDsmonMatrixSD *pMatrixSD;  /* Reference to Matrix SD */
  tDsmonHostStats  *pInHost;     /* Reference to Host Entry */
  tDsmonHostStats  *pOutHost;     /* Reference to Host Entry */
  tDsmonPdistStats *pPdist;  /* Reference to Pdist Entry */
  tDsmonStats        *pInStats;  /* Reference to Stats Entry */
  tDsmonStats        *pOutStats;  /* Reference to Stats Entry */
  tPktHeader PktHdr;    /* Pkt Header Information */
  UINT4 u4IdleRef;    /* Idle counter reference */
  UINT4 u4ProtoNLIndex;    /* Network layer protocol local index */
  UINT4 u4ProtoTLIndex;    /* Transport layer protocol local index */
  UINT4 u4ProtoALIndex;    /* Application layer protocol local index */
  UINT1 u1IsL3Vlan;    /* Is L3 VLAN Ifindex is configured */
  UINT1 u1IsNPFlowAdded;   /* Is NP Flow Entry Added */
  UINT1 u1Rsvd[2];
} tDsmonPktInfo;

typedef struct HostSampleTopN 
{
  tDsmonHostTopN      SampleTopN[DSMON_MAX_TOPN_ENTRY];
}tDsmonHostSampleTopN;

typedef struct MatrixSampleTopN
{
  tDsmonMatrixTopN      SampleTopN[DSMON_MAX_TOPN_ENTRY];
}tDsmonMatrixSampleTopN;





#endif
