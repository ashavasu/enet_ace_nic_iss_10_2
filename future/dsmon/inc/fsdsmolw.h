/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved 
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsDsmonTrace ARG_LIST((UINT4 *));

INT1
nmhGetFsDsmonAdminStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsDsmonTrace ARG_LIST((UINT4 ));

INT1
nmhSetFsDsmonAdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsDsmonTrace ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsDsmonAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsDsmonTrace ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsDsmonAdminStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
