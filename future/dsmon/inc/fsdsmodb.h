/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved 
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSDSMODB_H
#define _FSDSMODB_H


UINT4 fsdsmo [] ={1,3,6,1,4,1,29601,3,4};
tSNMP_OID_TYPE fsdsmoOID = {9, fsdsmo};


UINT4 FsDsmonTrace [ ] ={1,3,6,1,4,1,29601,3,4,1};
UINT4 FsDsmonAdminStatus [ ] ={1,3,6,1,4,1,29601,3,4,2};


tMbDbEntry fsdsmoMibEntry[]= {

{{10,FsDsmonTrace}, NULL, FsDsmonTraceGet, FsDsmonTraceSet, FsDsmonTraceTest, FsDsmonTraceDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{10,FsDsmonAdminStatus}, NULL, FsDsmonAdminStatusGet, FsDsmonAdminStatusSet, FsDsmonAdminStatusTest, FsDsmonAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData fsdsmoEntry = { 2, fsdsmoMibEntry };
#endif /* _FSDSMODB_H */

