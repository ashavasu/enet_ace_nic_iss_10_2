/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved
 *
   $Id: dsmndefn.h,v 1.4 2012/04/03 13:16:40 siva Exp $
 * Description: This file contains definitions of DSMON constants
 *******************************************************************/
#ifndef _DSMNDEFN_H_
#define _DSMNDEFN_H_

/* DSMON Capabilities supported by Probe */
#define DSMON_COUNTER_AGG_CTL      0x0001 /* Counter Aggregation Group */
#define DSMON_STATS                0x0002 /* Statistics Counter 
         (32 bits) Support */
#define DSMON_STATS_OVFL           0x0004 /* Statistics Overflow 
         Counter Support */
#define DSMON_STATS_HC             0x0008 /* Statistics High Capacity 
         Counter (64 bits) Support */
#define DSMON_PDIST                0x0010 /* Pdist Counter 
         (32 bits) Support */
#define DSMON_PDIST_OVFL           0x0020 /* Pdist Overflow Counter Support */
#define DSMON_PDIST_HC             0x0040 /* Pdist High Capacity 
         Counter (64 bits) Support */
#define DSMON_HOST                 0x0080 /* Host Counter (32 bits) Support */
#define DSMON_HOST_OVFL            0x0100 /* Host Overflow Counter Support */
#define DSMON_HOST_HC              0x0200 /* Host High Capacity 
         Counter (64 bits) Support */
#define DSMON_CAPS                 0x0400 /* Capability Group Support */
#define DSMON_MATRIX               0x0800 /* Matrix Counter 
         (32 bits) Support */
#define DSMON_MATRIX_OVFL          0x1000 /* Matrix Overflow Counter Support */
#define DSMON_MATRIX_HC            0x2000 /* Matrix High Capacity Counter 
         (32 bits) Support */

#define DSMON_MAX_AGG_PROFILE_DSCP  64 /* Maximum DSCP value */
#define DSMON_MAX_AGG_GROUPS  10 /* Maximum number of DSMON Groups per 
         Aggregation Control Entry */
#define DSMON_MAX_CTL_INDEX     65535 /* Maximum Index Value */
#define DSMON_MAX_DESC_LEN          64 /* Maximum description length */
#define DSMON_MAX_OWNER_LEN         127 /* Maximum owner name length */
#define DSMON_MAX_IDLE_CNT     600 /* Max. Idle limit */
#define DSMON_MAX_STATS_PER_CYCLE 20 /* Maximum Statistics collection 
         allowed per timer cycle */
#define IFINDEX_OID_SIZE            10 /* Data source OID size */

/* FSAP Related Macros */
#define DSMON_SEM_NAME         ((const UINT1 *)"DMNS")
#define DSMON_TASK_NAME        ((const UINT1 *)"DSMN")

#define DSMON_SEM_COUNT        1
#define DSMON_SEM_ID           gDsmonGlobals.dsmonSemId

#define DSMON_IS_ENABLED()     (gDsmonGlobals.u4DsmonAdminStatus == DSMON_ENABLE)

/* Timer Macros */

#define DSMON_ONE_SECOND                1
#define DSMON_ONE_SECOND_TIMER_EVENT    0x01
#define DSMON_PKTINFO_ARIVAL_EVENT      0x02

#define DSMON_ENABLE   1
#define DSMON_DISABLE   2
#define DSMON_INIT_VAL   0x0
#define DSMON_ZERO    0
#define DSMON_ONE    1

#define DSMON_AGGLOCK_TRUE              1
#define DSMON_AGGLOCK_FALSE             2

#define DSMON_IPV4PREFIXLEN_MIN  8 /* IPv4 Minimum prefix length */
#define DSMON_IPV4PREFIXLEN_MAX  32 /* IPv4 Maximum prefix length */

#define DSMON_IPV6PREFIXLEN_MIN  8 /* IPv6 Minimum prefix length */
#define DSMON_IPV6PREFIXLEN_MAX  128 /* IPv6 Maximum prefix length */

#define DSMON_TIMEREM_DEFVAL   1800 /* Default TopN Time remaining */
#define DSMON_REQSIZE_DEFVAL   150 /* Default TopN Requested Report Size */

#define DSMON_PKT_RATE    1 /* TopN Packet Rate Type */
#define DSMON_OCTET_RATE   2 /* TopN Octet Rate Type */

#define DSMON_IPV4_HDR_VER  0x4 
#define DSMON_IPV6_HDR_VER  0x6

#define DSMON_IPV4_MAX_LEN  4 /* IPv4 Address length */
#define DSMON_IPV6_MAX_LEN  16 /* IPv6 Address length */

/* Dsmon RBTree Table Macros */
#define DSMON_PKTINFO_TABLE  gDsmonGlobals.dsmonPktInfoRBTree
#define DSMON_AGGCTL_TABLE  gDsmonGlobals.dsmonAggCtlRBTree
#define DSMON_AGGCTL_GROUP_TABLE gDsmonGlobals.dsmonAggCtlGroupRBTree
#define DSMON_STATSCTL_TABLE  gDsmonGlobals.dsmonStatsCtlRBTree
#define DSMON_STATS_TABLE  gDsmonGlobals.dsmonStatsRBTree
#define DSMON_PDISTCTL_TABLE  gDsmonGlobals.dsmonPdistCtlRBTree
#define DSMON_PDIST_TABLE  gDsmonGlobals.dsmonPdistRBTree
#define DSMON_PDIST_TOPNCTL_TABLE  gDsmonGlobals.dsmonPdistTopNCtlRBTree
#define DSMON_PDIST_TOPN_TABLE   gDsmonGlobals.dsmonPdistTopNRBTree
#define DSMON_HOSTCTL_TABLE  gDsmonGlobals.dsmonHostCtlRBTree
#define DSMON_HOST_TABLE  gDsmonGlobals.dsmonHostRBTree
#define DSMON_HOST_TOPNCTL_TABLE gDsmonGlobals.dsmonHostTopNCtlRBTree
#define DSMON_HOST_TOPN_TABLE  gDsmonGlobals.dsmonHostTopNRBTree
#define DSMON_MATRIXCTL_TABLE  gDsmonGlobals.dsmonMatrixCtlRBTree
#define DSMON_MATRIX_TABLE  gDsmonGlobals.dsmonMatrixRBTrees
#define DSMON_MATRIX_TOPNCTL_TABLE gDsmonGlobals.dsmonMatrixTopNCtlRBTree
#define DSMON_MATRIX_TOPN_TABLE  gDsmonGlobals.dsmonMatrixTopNRBTree

/* Macro used for RBTree Comparisions */

#define DSMON_RB_EQUAL             0
#define DSMON_RB_GREATER           1
#define DSMON_RB_LESS             -1

/* Dsmon Mempool Macros */

#define DSMON_PKTINFO_POOL  gDsmonGlobals.dsmonPktInfoPoolId
#define DSMON_AGGCTL_POOL  gDsmonGlobals.dsmonAggCtlPoolId
#define DSMON_AGGCTL_GROUP_POOL  gDsmonGlobals.dsmonAggGroupPoolId
#define DSMON_STATSCTL_POOL  gDsmonGlobals.dsmonStatsCtlPoolId
#define DSMON_STATS_POOL  gDsmonGlobals.dsmonStatsPoolId
#define DSMON_PDISTCTL_POOL  gDsmonGlobals.dsmonPdistCtlPoolId
#define DSMON_PDIST_POOL  gDsmonGlobals.dsmonPdistStatsPoolId
#define DSMON_PDIST_TOPNCTL_POOL gDsmonGlobals.dsmonPdistTopNCtlPoolId
#define DSMON_PDIST_TOPN_POOL  gDsmonGlobals.dsmonPdistTopNPoolId
#define DSMON_MATRICCTL_POOL  gDsmonGlobals.dsmonMatrixCtlPoolId
#define DSMON_HOSTCTL_POOL  gDsmonGlobals.dsmonHostCtlPoolId
#define DSMON_HOST_POOL   gDsmonGlobals.dsmonHostStatsPoolId
#define DSMON_HOST_TOPNCTL_POOL  gDsmonGlobals.dsmonHostTopNCtlPoolId
#define DSMON_HOST_TOPN_POOL  gDsmonGlobals.dsmonHostTopNPoolId
#define DSMON_MATRIXCTL_POOL  gDsmonGlobals.dsmonMatrixCtlPoolId
#define DSMON_MATRIXSD_POOL  gDsmonGlobals.dsmonMatrixSDPoolId
#define DSMON_MATRIX_TOPNCTL_POOL gDsmonGlobals.dsmonMatrixTopNCtlPoolId
#define DSMON_MATRIX_TOPN_POOL  gDsmonGlobals.dsmonMatrixTopNPoolId
#define DSMON_HOST_SAMPLE_TOPN_POOL     gDsmonGlobals.dsmonHostSampleTopNPoolId   
#define DSMON_MATRIX_SAMPLE_TOPN_POOL   gDsmonGlobals.dsmonMatrixSampleTopNPoolId


#define DSMON_ADMIN_STATUS  gDsmonGlobals.u4DsmonAdminStatus
#define DSMON_TRACE_FLAG    gDsmonGlobals.u4DsmonTrace;
#define DSMON_CAPABILITIES  gDsmonGlobals.u2DsmonCapabilities

#define DSMON_GET_DATA_SOURCE_OID      DsmonUtlFormDataSourceOid
#define DSMON_SET_DATA_SOURCE_OID      DsmonUtlSetDataSourceOid
#define DSMON_TEST_DATA_SOURCE_OID     DsmonUtlTestDataSourceOid
#define DSMON_INIT_COMPLETE(u4Status)  lrInitComplete(u4Status)

#endif
