# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Telchnologies (Holdings). Ltd.        |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |  
# |   DESCRIPTION            : Provide the following information in order -  |
# |                            1. Number of Submodules present if Main       |
# |                               makefile.                                  |
# |                            2. Clean option                               |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################


TOTAL_OPNS =  $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION_SWITCHES)
############################################################################
#                         Directories                                      #
############################################################################

DSMON_BASE_DIR = ${BASE_DIR}/dsmon
RMON2_BASE_DIR = ${BASE_DIR}/rmon2
RMON2_INC_DIR  = ${RMON2_BASE_DIR}/inc
DSMON_INC_DIR  = ${DSMON_BASE_DIR}/inc
DSMON_SRC_DIR  = ${DSMON_BASE_DIR}/src
DSMON_OBJ_DIR  = ${DSMON_BASE_DIR}/obj


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

GLOBAL_INCLUDES  =  -I${DSMON_INC_DIR} \
		    -I${RMON2_INC_DIR}
INCLUDES         = ${GLOBAL_INCLUDES} ${COMMON_INCLUDE_DIRS}

#############################################################################

