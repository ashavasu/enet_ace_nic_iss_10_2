/********************************************************************
 * Description: This file contains the functional		    *
 *              routines for Counter Aggregation Group              *
 ********************************************************************/
#ifndef _DSMNAGGR_C_
#define _DSMNAGGR_C_
#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHandleAggCtlLckChgs                                   */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/* 		  tables entries into NOTREADY and cleanup all data tables.  */
/*		  If u1DsmonAggControlStatus is TRUE, update control tables  */
/*		  and resume data collection				     */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None			                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID 
DsmonHandleAggCtlLckChgs (UINT1 u1DsmonAggControlStatus)
{
  /* Clear PktInfo Table */
  DsmonPktInfoRemoveEntries ();

  /* Update Stats Tables */
  DsmonStatsUpdateAggCtlChgs (u1DsmonAggControlStatus);

  /* Update Pdist Tables */
  DsmonPdistUpdateAggCtlChgs (u1DsmonAggControlStatus);

  /* Update Pdist TopN Tables */
  DsmonPdistTopNUpdateAggCtlChgs (u1DsmonAggControlStatus);

  /* Update Host Tables */
  DsmonHostUpdateAggCtlChgs (u1DsmonAggControlStatus);

  /* Update Host TopN Tables */
  DsmonHostTopNUpdateAggCtlChgs (u1DsmonAggControlStatus);

  /* Update Matrix Tables */
  DsmonMatrixUpdateAggCtlChgs (u1DsmonAggControlStatus);

  /* Update Matrix TopN Tables */
  DsmonMatrixTopNUpdateAggCtlChgs (u1DsmonAggControlStatus);
  
  return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonGetAggCtlIndex                                        */
/*                                                                           */
/* Description  : Get First / Next Agg Ctl Index 			     */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggCtl / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggCtl*
DsmonGetAggCtlIndex (UINT4 u4DsmonAggControlIndex)
{
  tDsmonAggCtl *pAggCtl = NULL;
  tDsmonAggCtl AggCtl;

  if (u4DsmonAggControlIndex == 0)
  {
    pAggCtl = (tDsmonAggCtl *) RBTreeGetFirst (DSMON_AGGCTL_TABLE);
  }
  else 
  {
    AggCtl.u4DsmonAggCtlIndex = u4DsmonAggControlIndex;
    pAggCtl = (tDsmonAggCtl *) RBTreeGetNext (DSMON_AGGCTL_TABLE,
					(tRBElem *) &AggCtl, NULL);
  }
  return pAggCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAddAggCtlEntry                                        */
/*                                                                           */
/* Description  : Allocates memory and Adds Aggregation Control Entry into   */
/*		  dsmonAggCtlRBTree      				     */
/*                                                                           */
/* Input        : pAggCtl		                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggCtl / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggCtl*
DsmonAddAggCtlEntry (UINT4 u4DsmonAggControlIndex)
{
  tDsmonAggCtl *pAggCtl = NULL;

  if ((pAggCtl = (tDsmonAggCtl *)
	(MemAllocMemBlk (DSMON_AGGCTL_POOL))) == NULL)
  {
    DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
	"Memory Allocation failed for Agg. Ctl.\r\n");
    return NULL;
  }

  MEMSET (pAggCtl, DSMON_INIT_VAL, sizeof(tDsmonAggCtl));

  pAggCtl->u4DsmonAggCtlIndex = u4DsmonAggControlIndex;

  /* Initiate Prfile Table with default Group Index - 0 */

  MEMSET (pAggCtl->au4DsmonAggProfile, DSMON_INIT_VAL,
          sizeof (pAggCtl->au4DsmonAggProfile));
  
  if (RBTreeAdd (DSMON_AGGCTL_TABLE, (tRBElem *) pAggCtl)
      == RB_SUCCESS)
  {
    return pAggCtl;
  }
  
  /* Release Memory allocated on Failure */
  MemReleaseMemBlock (DSMON_AGGCTL_POOL, (UINT1 *) pAggCtl);
  
  return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonDelAggCtlEntry                                        */
/*                                                                           */
/* Description  : Releases memory and Removes Aggregation Control Entry from */
/*		  dsmonAggCtlRBTree      				     */
/*                                                                           */
/* Input        : pAggCtl		                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonDelAggCtlEntry (tDsmonAggCtl* pAggCtl)
{
  /* Remove Agg. Ctl node from dsmonAggCtlRBTree */
  RBTreeRem (DSMON_AGGCTL_TABLE, (tRBElem *) pAggCtl);

  /* Release Memory to MemPool */
  if (MemReleaseMemBlock (DSMON_AGGCTL_POOL, 
	(UINT1 *) pAggCtl)  != MEM_SUCCESS)
  {
    DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
	"MemBlock Release failed for  Agg. Ctl.\r\n");
    return OSIX_FAILURE;
  }
  return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonGetAggCtlEntry                                        */
/*                                                                           */
/* Description  : Get Aggregation Control Entry for the given control index  */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggCtl / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggCtl*
DsmonGetAggCtlEntry (UINT4 u4DsmonAggControlIndex)
{
  tDsmonAggCtl *pAggCtl = NULL;
  tDsmonAggCtl AggCtl;

  if (u4DsmonAggControlIndex == 0)
  {
    pAggCtl = (tDsmonAggCtl *) RBTreeGetFirst (DSMON_AGGCTL_TABLE);
  }
  else
  {
    AggCtl.u4DsmonAggCtlIndex = u4DsmonAggControlIndex;
    
    pAggCtl = (tDsmonAggCtl *) RBTreeGet (DSMON_AGGCTL_TABLE,
      				(tRBElem *) &AggCtl);
  }
  return pAggCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonGetAggGrpIndex                                        */
/*                                                                           */
/* Description  : Get First / Next Agg.Group Index for the given index       */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggGroup / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggGroup*
DsmonGetAggGrpIndex (UINT4 u4DsmonAggControlIndex, UINT4 u4DsmonAggGroupIndex)
{
  tDsmonAggGroup *pAggGrp = NULL;
  tDsmonAggGroup AggGrp;

  if (u4DsmonAggControlIndex == 0 && u4DsmonAggGroupIndex == 0)
  {
    pAggGrp = (tDsmonAggGroup *) RBTreeGetFirst (DSMON_AGGCTL_GROUP_TABLE);
  }
  else
  {
    AggGrp.u4DsmonAggCtlIndex = u4DsmonAggControlIndex;
    AggGrp.u4DsmonAggGroupIndex = u4DsmonAggGroupIndex;
    pAggGrp = (tDsmonAggGroup *) RBTreeGetNext (DSMON_AGGCTL_GROUP_TABLE,
      				(tRBElem *) &AggGrp, NULL);
  }
  return pAggGrp;
}
/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAddAggGrpEntry                                        */
/*                                                                           */
/* Description  : Adds Aggregation Group Entry into dsmonAggGroupRBTree      */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex		     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggGroup / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggGroup*
DsmonAddAggGrpEntry (UINT4 u4DsmonAggControlIndex, UINT4 u4DsmonAggGroupIndex)
{
  tDsmonAggGroup *pAggGrp = NULL;
  
  if ((pAggGrp = (tDsmonAggGroup *)
              (MemAllocMemBlk (DSMON_AGGCTL_GROUP_POOL))) == NULL)
  {
    DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
            "Memory Allocation failed for Agg. Group.\r\n");
    return NULL;
  }

  MEMSET (pAggGrp, DSMON_INIT_VAL, sizeof(tDsmonAggGroup));

  pAggGrp->u4DsmonAggCtlIndex = u4DsmonAggControlIndex;
  pAggGrp->u4DsmonAggGroupIndex = u4DsmonAggGroupIndex;

  if (RBTreeAdd (DSMON_AGGCTL_GROUP_TABLE, (tRBElem *) pAggGrp)
      == RB_SUCCESS)
  {
    return pAggGrp;
  }
  
  /* Release Memory allocated on Failure */
  MemReleaseMemBlock (DSMON_AGGCTL_GROUP_POOL, (UINT1 *) pAggGrp);  
  return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonDelAggGrpEntry                                        */
/*                                                                           */
/* Description  : Releases memory and Removes Aggregation Group Entry from   */
/*		  dsmonAggGroupRBTree      				     */
/*                                                                           */
/* Input        : pAggGrp		                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonDelAggGrpEntry (tDsmonAggGroup* pAggGrp)
{
  /* Remove Agg. Grp node from dsmonAggCtlRBTree */
  RBTreeRem (DSMON_AGGCTL_GROUP_TABLE, (tRBElem *) pAggGrp);

  /* Release Memory to MemPool */
  if (MemReleaseMemBlock (DSMON_AGGCTL_POOL,
	(UINT1 *) pAggGrp)  != MEM_SUCCESS)
  {
    DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
	"MemBlock Release failed for  Agg. Grp.\r\n");
    return OSIX_FAILURE;
  }
  return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonGetAggGrpEntry                                        */
/*                                                                           */
/* Description  : Get Aggregation Group Entry for the given control index    */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggGroup / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggGroup*
DsmonGetAggGrpEntry (UINT4 u4DsmonAggControlIndex, UINT4 u4DsmonAggGroupIndex)
{
  tDsmonAggGroup *pAggGrp = NULL;
  tDsmonAggGroup AggGrp;

  if (u4DsmonAggControlIndex == 0 && u4DsmonAggGroupIndex == 0)
  {
    pAggGrp = (tDsmonAggGroup *) RBTreeGetFirst (DSMON_AGGCTL_GROUP_TABLE);
  }
  else
  {
    AggGrp.u4DsmonAggCtlIndex = u4DsmonAggControlIndex;
    AggGrp.u4DsmonAggGroupIndex = u4DsmonAggGroupIndex;
    pAggGrp = (tDsmonAggGroup *) RBTreeGet (DSMON_AGGCTL_GROUP_TABLE,
      				(tRBElem *) &AggGrp);
  }
  return pAggGrp;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmnAggGetGroupToCtlIndexAndDscp                           */
/*                                                                           */
/* Description  : This function is provides the next aggregate group index   */
/*		  to the provided group index corresponding to the input     */
/*		  control index.					     */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex               */
/*                                                                           */
/* Output       : pu4AggGroupIndex                                           */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmnAggGetGroupToCtlIndexAndDscp (
		UINT4 		u4AggCtlIndex,
		UINT4           u4AggDSCP,
		UINT4          *pu4AggGroupIndex)

{
  tDsmonAggCtl *pAggCtl = NULL;

  pAggCtl = DsmonGetAggCtlEntry (u4AggCtlIndex);

  if (pAggCtl != NULL)
  {
    *pu4AggGroupIndex = pAggCtl->au4DsmonAggProfile[u4AggDSCP];

    return OSIX_SUCCESS;
  }
  return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmnAggSetGroupToCtlIndexAndDscp                           */
/*                                                                           */
/* Description  : This function is provides the next aggregate group index   */
/*                to the provided group index corresponding to the input     */
/*                control index.                                             */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex               */
/*                                                                           */
/* Output       : pu4AggGroupIndex                                           */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmnAggSetGroupToCtlIndexAndDscp (
                UINT4           u4AggCtlIndex,
                UINT4           u4AggDSCP,
                UINT4           u4AggGroupIndex)

{
  tDsmonAggCtl *pAggCtl = NULL;

  pAggCtl = DsmonGetAggCtlEntry (u4AggCtlIndex);

  if (pAggCtl != NULL)
  {
    pAggCtl->au4DsmonAggProfile[u4AggDSCP] = u4AggGroupIndex;

    return OSIX_SUCCESS;
  }
  return OSIX_FAILURE;
}

#endif

