
/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved             *
 *                                    *
 * $Id: dsmncli.c,v 1.9 2014/01/25 13:55:19 siva Exp $        *
 *                                    *
 * Description: This file contains CLI SET/GET routines for the mib *
 *        objects specified in fsdsmon.mib                          *
 *                                                                  *
 *******************************************************************/

#ifndef __DSMNCLI_C__
#define __DSMNCLI_C__

#include "dsmndefn.h"
#include "dsmninc.h"
#include "dsmncli.h"

/*************************************************************************
 *
 *  FUNCTION NAME   : cli_process_dsmon_cmd
 *
 *  DESCRIPTION     : Protocol CLI message handler function
 *
 *  INPUT           : CliHandle - CliContext ID
 *                    u4Command - Command identifier
 *                    ... -Variable command argument list
 *
 *  OUTPUT          : None
 *
 *  RETURNS         : CLI_SUCCESS/CLI_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
cli_process_dsmon_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT1              *args[DSMON_MAX_ARGS];
    INT1                argno = 0;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4StatusVal = 0;
    UINT4               u4ErrCode = 0;
    UINT1              *pu1Inst = NULL;

    va_start (ap, u4Command);

    pu1Inst = va_arg (ap, UINT1 *);
    while (1)
    {
        args[argno++] = va_arg (ap, UINT1 *);
        if (argno == DSMON_MAX_ARGS)
            break;
    }

    va_end (ap);

    UNUSED_PARAM (pu1Inst);

    switch (u4Command)
    {
        case DSMON_CLI_STATUS:

            /* apu1args[0] contains enable/disable status */

            i4StatusVal =
                (CLI_PTR_TO_I4 (args[0]) ==
                 DSMON_ENABLE) ? DSMON_ENABLE : DSMON_DISABLE;

            i4RetStatus = DsmonCliSetModuleStatus (CliHandle, i4StatusVal);

            break;

        case DSMON_CLI_SHOW_TRACE:

            DsmonCliShowTrace (CliHandle);

            break;

        case DSMON_CLI_TRACE:

            /* apu1args[0] contains Debug level */

            i4RetStatus = DsmonCliSetTrace (CliHandle, CLI_PTR_TO_I4 (args[0]));

            break;

        case DSMON_CLI_RESET_TRACE:

            i4RetStatus = DsmonCliResetTrace (CliHandle);

            break;

        default:

            CliPrintf (CliHandle, "%% Unknown command \r\n");

            return CLI_FAILURE;

    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_DSMON_MAX_ERR))
        {
            CliPrintf (CliHandle, "%% %s", DsmonCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }

    return CLI_SUCCESS;
}

/****************************************************************************
*
*     FUNCTION NAME    : DsmonCliSetModuleStatus
*
*     DESCRIPTION      : This function will enable/disable Dsmon module
*
*     INPUT            : CliHandle  - CliContext ID
*                        i4Status   - Dsmon Module status
*
*     OUTPUT           : None
*
*     RETURNS          : CLI_SUCCESS/CLI_FAILURE
*
*****************************************************************************/
PUBLIC INT4
DsmonCliSetModuleStatus (tCliHandle CliHandle, INT4 i4Status)
{

    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsDsmonAdminStatus (&u4ErrorCode, i4Status) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsDsmonAdminStatus (i4Status) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : DsmonCliSetTrace
 *
 *     DESCRIPTION      : This function will set the debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val - Dsmon Debug Level
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
DsmonCliSetTrace (tCliHandle CliHandle, INT4 i4Val)
{
    UINT4               u4ErrorCode = 0;
    INT4                u4Count = 0;

    nmhSetFsDsmonTrace (DSMON_MAX_TRACE_LEVEL);

    for (u4Count = 1; u4Count < DSMON_MAX_TRACE_LEVEL; u4Count++)
    {
        if ((DSMON_ONE << (u4Count - 1)) & (i4Val))
        {
            nmhTestv2FsDsmonTrace (&u4ErrorCode, u4Count);
            nmhSetFsDsmonTrace (u4Count);
        }
    }
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/***************************************************************************
 *
 *     FUNCTION NAME    : DsmonCliResetTrace
 *
 *     DESCRIPTION      : This function will reset the debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *                        i4Val - Dsmon Debug Level
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
DsmonCliResetTrace (tCliHandle CliHandle)
{

    nmhSetFsDsmonTrace (DSMON_MAX_TRACE_LEVEL);
    UNUSED_PARAM (CliHandle);
    return CLI_SUCCESS;
}

/****************************************************************************
 *
 *     FUNCTION NAME    : DsmonCliShowTrace
 *
 *     DESCRIPTION      : This function prints the DSMON debug level
 *
 *     INPUT            : CliHandle  - CliContext ID
 *
 *     OUTPUT           : None
 *
 *     RETURNS          : CLI_SUCCESS/CLI_FAILURE
 *
 *****************************************************************************/
PUBLIC VOID
DsmonCliShowTrace (tCliHandle CliHandle)
{
    UINT4               u4DbgLevel = 0;
    INT4                i4Status = 0;

    nmhGetFsDsmonAdminStatus (&i4Status);

    if (i4Status == DSMON_DISABLE)
    {
        return;
    }

    nmhGetFsDsmonTrace (&u4DbgLevel);

    if (u4DbgLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rDSMON :");

    if ((u4DbgLevel & DSMON_FN_ENTRY) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DSMON Function entry trace is on");
    }
    if ((u4DbgLevel & DSMON_FN_EXIT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DSMON Function exit trace is on");
    }
    if ((u4DbgLevel & DSMON_CRITICAL_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DSMON Critical trace is on");
    }
    if ((u4DbgLevel & DSMON_MEM_FAIL) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DSMON Memory fail trace is on");
    }
    if ((u4DbgLevel & DSMON_DEBUG_TRC) != 0)
    {
        CliPrintf (CliHandle, "\r\n  DSMON Debug trace is on");
    }
    CliPrintf (CliHandle, "\r\n");

    return;

}

PUBLIC VOID
DsmonShowRunningConfig (tCliHandle CliHandle)
{

    INT4                i4Status = 0;

    nmhGetFsDsmonAdminStatus (&i4Status);

    if (i4Status == DSMON_DISABLE)
    {
        return;
    }

    CliPrintf (CliHandle, "\r \n dsmon enable");

    return;
}

#endif
