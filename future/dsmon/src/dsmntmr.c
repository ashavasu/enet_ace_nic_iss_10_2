/*****************************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved                 * 
 *
 * $Id: dsmntmr.c,v 1.3 2013/12/18 12:50:13 siva Exp $
 *
 * Description : This file contains procedures containing Dsmon Timer         *
 *               related operations                         *
 *****************************************************************************/

#include "dsmninc.h"

/****************************************************************************
*                                                                           *
* Function     : DsmonTmrInitTmrDesc                                        *
*                                                                           *
* Description  : Initialize Dsmon Timer Decriptors                          *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
DsmonTmrInitTmrDesc ()
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonTmrInitTmrDesc\n");

    gDsmonGlobals.aDsmonTmrDesc[DSMON_POLL_TMR].TmrExpFn = DsmonTmrPollTmr;
    gDsmonGlobals.aDsmonTmrDesc[DSMON_POLL_TMR].i2Offset =
        (INT2) (FSAP_OFFSETOF (tDsmonGlobals, dsmonPollTmr));

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonTmrInitTmrDesc\n");

}

/****************************************************************************
 *                                                                          *
 *    FUNCTION NAME    : DsmonTmrInit                                       *
 *                                                                          *
 *    DESCRIPTION      : This function creates a timer list                 *
 *                       for all the timers in DSMON module.                *
 *                                                                          *
 *    INPUT            : None                                               *
 *                                                                          *
 *    OUTPUT           : None                                               *
 *                                                                          *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          *
 *                                                                          *
 ****************************************************************************/

PUBLIC INT4
DsmonTmrInit (VOID)
{
    if (TmrCreateTimerList (DSMON_TASK_NAME, DSMON_ONE_SECOND_TIMER_EVENT, NULL,
                            &(gDsmonGlobals.dsmonTmrLst)) == TMR_FAILURE)
    {
        DSMON_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                   "DsmonTmrInitTmrDesc: Timer list creation FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    DsmonTmrInitTmrDesc ();

    return OSIX_SUCCESS;
}

/****************************************************************************
 *                                                                          *
 *    FUNCTION NAME    : DsmonTmrDeInit                                     *
 *                                                                          *
 *    DESCRIPTION      : This function de-initializes the timer list        *
 *                                                                          *
 *    INPUT            : None                                               *
 *                                                                          *
 *    OUTPUT           : None                                               *
 *                                                                          *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                          *
 *                                                                          *
 ****************************************************************************/

PUBLIC INT4
DsmonTmrDeInit (VOID)
{
    if (TmrDeleteTimerList (gDsmonGlobals.dsmonTmrLst) == TMR_FAILURE)
    {
        DSMON_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
                   "DsmonTmrDeInit: Timer list deletion FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    DSMON_TRC ((INIT_SHUT_TRC | ALL_FAILURE_TRC | CONTROL_PLANE_TRC),
               "DsmonTmrDelInit: Timer list deletion successful !!!\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : DsmonTmrStartTmr                                           *
*                                                                           *
* Description  : Starts Dsmom Timer                                         *
*                                                                           *
* Input        : pDsmonTmr   - pointer to DsmonTmr structure                *
*                eDsmonTmrId - DSMON timer ID                               *
*                u4Secs      - ticks in seconds                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
DsmonTmrStartTmr (tDsmonTmr * pDsmonTmr, enDsmonTmrId eDsmonTmrId, UINT4 u4Secs)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonTmrStartTmr\n");

    pDsmonTmr->eDsmonTmrId = eDsmonTmrId;

    if (TmrStartTimer (gDsmonGlobals.dsmonTmrLst,
                       &(pDsmonTmr->tmrNode),
                       (UINT4) SYS_NUM_OF_TIME_UNITS_IN_A_SEC * u4Secs) !=
        TMR_SUCCESS)
    {
        DSMON_TRC (OS_RESOURCE_TRC, "Tmr Start Failure\n");
        u4RetVal = OSIX_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonTmrStartTmr\n");
    return u4RetVal;
}

/****************************************************************************
*                                                                           *
* Function     : DsmonTmrStopTmr                                            *
*                                                                           *
* Description  : Stops Dsmon Timer                                          *
*                                                                           *
* Input        : pDsmonTmr - pointer to DsmonTmr structure                  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
DsmonTmrStopTmr (tDsmonTmr * pDsmonTmr)
{
    UINT4               u4RetVal = OSIX_SUCCESS;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonTmrStopTmr\n");

    if (TmrStopTimer (gDsmonGlobals.dsmonTmrLst,
                      &(pDsmonTmr->tmrNode)) != TMR_SUCCESS)
    {
        DSMON_TRC (OS_RESOURCE_TRC, "Tmr Stop Failure\n");
        u4RetVal = OSIX_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonTmrStopTmr\n");
    return u4RetVal;
}

/****************************************************************************
*                                                                           *
* Function     : DsmonTmrHandleExpiry                                       *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC INT4
DsmonTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    enDsmonTmrId        eDsmonTmrId;
    INT2                i2Offset;
    UINT4               u4RetVal = OSIX_SUCCESS;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonTmrHandleExpiry\n");

    MEMSET (&eDsmonTmrId, DSMON_INIT_VAL, sizeof (enDsmonTmrId));

    while ((pExpiredTimers =
            TmrGetNextExpiredTimer (gDsmonGlobals.dsmonTmrLst)) != NULL)
    {
        DSMON_TRC1 (OS_RESOURCE_TRC,
                    "Tmr Blk %x To Be Processed\n", pExpiredTimers);

        eDsmonTmrId = ((tDsmonTmr *) pExpiredTimers)->eDsmonTmrId;
        i2Offset = gDsmonGlobals.aDsmonTmrDesc[eDsmonTmrId].i2Offset;

        if (i2Offset == -1)
        {

            /* The timer function does not take any parameter */

            (*(gDsmonGlobals.aDsmonTmrDesc[eDsmonTmrId].TmrExpFn)) (NULL);

        }
        else
        {

            /* The timer function requires a parameter */

            (*(gDsmonGlobals.aDsmonTmrDesc[eDsmonTmrId].TmrExpFn))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
        /* Restart Timer */
        if (DsmonTmrStartTmr (&gDsmonGlobals.dsmonPollTmr,
                              eDsmonTmrId, DSMON_ONE_SECOND) != OSIX_SUCCESS)
        {
            u4RetVal = OSIX_FAILURE;
        }
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonTmrHandleExpiry\n");
    return u4RetVal;
}

/****************************************************************************
*                                                                           *
* Function     : DsmonTmrPollTmr                                            *
*                                                                           *
* Description  : Handles Poll timer has expiry.                             *
*                                                                           *
* Input        : pDsmonTmr - pointer to DsmonTmr structure                  *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
DsmonTmrPollTmr (VOID *pArg)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonTmrPollTmr\n");
    UNUSED_PARAM (pArg);
#ifdef NPAPI_WANTED
    /* Update Data Tables */
    DsmonFetchHwStats (gDsmonGlobals.pDsmonLastVisted);
#endif
    /* Update TopN Tables */
    DsmonMainUpdateTopNTables ();

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonTmrPollTmr\n");
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  dsmntmr.c                      */
/*-----------------------------------------------------------------------*/
