/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved 
* $Id: fsdsmolw.c,v 1.6 2013/07/04 13:08:24 siva Exp $
* Description: Protocol Low Level Routines
*********************************************************************/
#include "dsmninc.h"
#include "dsmncli.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsDsmonTrace
 Input       :  The Indices

                The Object 
                retValFsDsmonTrace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsmonTrace (UINT4 *pu4RetValFsDsmonTrace)
{

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetFsDsmonTrace \n");

    *pu4RetValFsDsmonTrace = gDsmonGlobals.u4DsmonTrace;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsDsmonAdminStatus
 Input       :  The Indices

                The Object 
                retValFsDsmonAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsDsmonAdminStatus (INT4 *pi4RetValFsDsmonAdminStatus)
{

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetFsDsmonAdminStatus \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        *pi4RetValFsDsmonAdminStatus = (INT4) DSMON_DISABLE;

        return SNMP_SUCCESS;
    }

    *pi4RetValFsDsmonAdminStatus = (INT4) (DSMON_ADMIN_STATUS);

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsDsmonTrace
 Input       :  The Indices

                The Object 
                setValFsDsmonTrace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsmonTrace (UINT4 u4SetValFsDsmonTrace)
{

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetFsDsmonTrace \n");

    if (u4SetValFsDsmonTrace == DSMON_MAX_TRACE_LEVEL)
    {
        /* Reset Trace Support */
        gDsmonGlobals.u4DsmonTrace = 0;
    }
    else
    {
        gDsmonGlobals.u4DsmonTrace |= (DSMON_ONE << (u4SetValFsDsmonTrace - 1));
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsDsmonAdminStatus
 Input       :  The Indices

                The Object 
                setValFsDsmonAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsDsmonAdminStatus (INT4 i4SetValFsDsmonAdminStatus)
{
    tDsmonStatsCtl     *pStatsCtlNode = NULL;
    tDsmonPdistCtl     *pPdistCtlNode = NULL;
    tDsmonHostCtl      *pHostCtlNode = NULL;
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    UINT4               u4CtlIndex = 0;
    INT4                i4Result = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetFsDsmonAdminStatus \n");

    if (DSMON_ADMIN_STATUS == (UINT4) i4SetValFsDsmonAdminStatus)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "Same Admin Status \n");

        return SNMP_SUCCESS;
    }

    if (i4SetValFsDsmonAdminStatus == DSMON_ENABLE)
    {
        if (!(RMON2_IS_ENABLED ()))
        {
            DSMON_TRC (DSMON_CRITICAL_TRC, "First Enable RMONv2\n");
            return SNMP_FAILURE;
        }
        if (DsmonTmrStartTmr (&(gDsmonGlobals.dsmonPollTmr),
                              DSMON_POLL_TMR, DSMON_ONE_SECOND) != OSIX_SUCCESS)
        {
            DSMON_TRC (DSMON_CRITICAL_TRC, "Tmr Start Failure\n");

            return SNMP_FAILURE;
        }
#ifdef NPAPI_WANTED
        DsmonFsDSMONHwSetStatus ((UINT4) i4SetValFsDsmonAdminStatus);
#endif

        DSMON_ADMIN_STATUS = (UINT4) i4SetValFsDsmonAdminStatus;
    }
    else if (i4SetValFsDsmonAdminStatus == DSMON_DISABLE)
    {
        DsmonTmrStopTmr (&gDsmonGlobals.dsmonPollTmr);

        pStatsCtlNode = DsmonStatsGetNextCtlIndex (u4CtlIndex);

        while (pStatsCtlNode != NULL)
        {
            u4CtlIndex = pStatsCtlNode->u4DsmonStatsCtlIndex;

            i4Result = DsmonStatsDelCtlEntry (pStatsCtlNode);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Stats Control Index \
                RBTreeRemove failed - Stats Control Table\n");

            }

            pStatsCtlNode = DsmonStatsGetNextCtlIndex (u4CtlIndex);
        }
        u4CtlIndex = 0;

        pPdistCtlNode = DsmonPdistGetNextCtlIndex (u4CtlIndex);

        while (pPdistCtlNode != NULL)
        {
            u4CtlIndex = pPdistCtlNode->u4DsmonPdistCtlIndex;

            DsmonPdistTopNNotifyPdistCtlChgs (u4CtlIndex, DESTROY);

            i4Result = DsmonPdistDelCtlEntry (pPdistCtlNode);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Pdist Control Index \
                RBTreeRemove failed - Pdist Control Table\n");

            }

            pPdistCtlNode = DsmonPdistGetNextCtlIndex (u4CtlIndex);
        }

        u4CtlIndex = 0;

        pHostCtlNode = DsmonHostGetNextCtlIndex (u4CtlIndex);

        while (pHostCtlNode != NULL)
        {
            u4CtlIndex = pHostCtlNode->u4DsmonHostCtlIndex;

            DsmonHostTopNNotifyHostCtlChgs (u4CtlIndex, DESTROY);

            i4Result = DsmonHostDelCtlEntry (pHostCtlNode);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Host Control Index \
                RBTreeRemove failed - Host Control Table\n");

            }

            pHostCtlNode = DsmonHostGetNextCtlIndex (u4CtlIndex);
        }

        u4CtlIndex = 0;

        pMatrixCtlNode = DsmonMatrixGetNextCtlIndex (u4CtlIndex);

        while (pMatrixCtlNode != NULL)
        {
            u4CtlIndex = pMatrixCtlNode->u4DsmonMatrixCtlIndex;

            DsmonMatrixTopNNotifyMtxCtlChgs (u4CtlIndex, DESTROY);

            i4Result = DsmonMatrixDelCtlEntry (pMatrixCtlNode);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Matrix Control Index \
                RBTreeRemove failed - Matrix Control Table\n");

            }

            pMatrixCtlNode = DsmonMatrixGetNextCtlIndex (u4CtlIndex);
        }

        DsmonPktInfoDelUnusedEntries ();

#ifdef NPAPI_WANTED
        DsmonFsDSMONHwSetStatus ((UINT4) i4SetValFsDsmonAdminStatus);
#endif

        DSMON_ADMIN_STATUS = (UINT4) i4SetValFsDsmonAdminStatus;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsDsmonTrace
 Input       :  The Indices

                The Object 
                testValFsDsmonTrace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsmonTrace (UINT4 *pu4ErrorCode, UINT4 u4TestValFsDsmonTrace)
{

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2FsDsmonTrace \n");

    if ((u4TestValFsDsmonTrace < DSMON_MIN_TRACE_LEVEL) ||
        (u4TestValFsDsmonTrace > DSMON_MAX_TRACE_LEVEL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsDsmonAdminStatus
 Input       :  The Indices

                The Object 
                testValFsDsmonAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsDsmonAdminStatus (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsDsmonAdminStatus)
{

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2FsDsmonAdminStatus\n");

    if ((i4TestValFsDsmonAdminStatus != DSMON_ENABLE) &&
        (i4TestValFsDsmonAdminStatus != DSMON_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    if (!(RMON2_IS_ENABLED ()))
    {
        CLI_SET_ERR (CLI_DS_RMON2_NOT_ENABLED);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsDsmonTrace
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDsmonTrace (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsDsmonAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsDsmonAdminStatus (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
