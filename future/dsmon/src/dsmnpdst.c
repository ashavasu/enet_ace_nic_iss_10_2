/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *
 * $Id: dsmnpdst.c,v 1.10 2012/12/13 14:25:10 siva Exp $           *
 *                                                                   *
 * Description: This file contains the functional routines         *
 *        for DSMON Pdist module                                *
 *                                                                  *
 *******************************************************************/
#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistGiveLRUEntry                                     */
/*                                                                           */
/* Description  : Gives the LRU entry                         */
/*                                                                           */
/* Input        : u4PdistCtlIndex                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Valid Pdist Data Entry / NULL                              */
/*                                                                           */
/*****************************************************************************/
PRIVATE tDsmonPdistStats *
DsmonPdistGiveLRUEntry (UINT4 u4PdistCtlIndex)
{
    tDsmonPdistStats   *pPdistStats = NULL, *pLRUEntry = NULL;
    UINT4               u4PdistGrpIndex = 0, u4PdistProtoIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistGiveLRUEntry\r\n");

    /* Get First Entry in Pdist Table */
    pPdistStats = DsmonPdistGetNextDataIndex
        (u4PdistCtlIndex, u4PdistGrpIndex, u4PdistProtoIndex);

    /* Mark First Entry as LRU entry */
    pLRUEntry = pPdistStats;

    while (pPdistStats != NULL)
    {
        if (pPdistStats->u4DsmonPdistCtlIndex != u4PdistCtlIndex)
        {
            break;
        }
        u4PdistGrpIndex = pPdistStats->u4DsmonPdistAggGroupIndex;
        u4PdistProtoIndex = pPdistStats->u4DsmonPdistProtDirLocalIndex;

        if (pLRUEntry->u4DsmonPdistTimeMark > pPdistStats->u4DsmonPdistTimeMark)
        {
            pLRUEntry = pPdistStats;
        }

        pPdistStats = DsmonPdistGetNextDataIndex (u4PdistCtlIndex,
                                                  u4PdistGrpIndex,
                                                  u4PdistProtoIndex);
    }                            /* End of While */
    return pLRUEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistProcessPktInfo                                 */
/*                                                                           */
/* Description  : Once incoming packet is recieved by Control Plane, for     */
/*          every valid pdist control entry, new data entry will be    */
/*          created and establish inter links                 */
/*                                                                           */
/* Input        : PktInfo                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonPdistProcessPktInfo (tDsmonPktInfo * pPktInfo, UINT4 u4PktSize)
{
    tDsmonPdistCtl     *pPdistCtl = NULL;
    tDsmonPdistStats   *pCurPdist = NULL, *pPrevPdist = NULL;
    UINT4               u4PdistCtlIndex = 0, u4PdistGrpIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistProcessPktInfo\r\n");
    if (pPktInfo->u4ProtoALIndex == 0)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonPdistProcessPktInfo Invalid AL Index\r\n");
        return i4RetVal;
    }

    /* Check for valid Control Entry */
    pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);

    while (pPdistCtl != NULL)
    {
        u4PdistCtlIndex = pPdistCtl->u4DsmonPdistCtlIndex;
        if ((pPdistCtl->u4DsmonPdistCtlRowStatus == ACTIVE) &&
            ((pPdistCtl->u4DsmonPdistDataSource == pPktInfo->PktHdr.u4IfIndex)
             || (pPdistCtl->u4DsmonPdistDataSource ==
                 pPktInfo->PktHdr.u4VlanIfIndex))
            && (pPktInfo->PktHdr.u1DSCP < DSMON_MAX_AGG_PROFILE_DSCP))
        {
            if (pPdistCtl->u4DsmonPdistDataSource ==
                pPktInfo->PktHdr.u4VlanIfIndex)
            {
                pPktInfo->u1IsL3Vlan = OSIX_TRUE;
            }
            u4PdistGrpIndex =
                pPdistCtl->pAggCtl->au4DsmonAggProfile[pPktInfo->PktHdr.u1DSCP];

            /* Check whether entry already present */
            pCurPdist = DsmonPdistGetDataEntry (u4PdistCtlIndex,
                                                u4PdistGrpIndex,
                                                pPktInfo->u4ProtoALIndex);

            if (pCurPdist == NULL)
            {
                if (pPdistCtl->u4DsmonPdistCtlMaxDesiredSupported == 0)
                {
                    pPdistCtl->u4DsmonPdistCtlDroppedFrames++;
                    return OSIX_FAILURE;
                }
                /* Check whether data entries reached max limit */
                if ((pPdistCtl->u4DsmonPdistCtlInserts -
                     pPdistCtl->u4DsmonPdistCtlDeletes) >=
                    pPdistCtl->u4DsmonPdistCtlMaxDesiredSupported)
                {
                    pCurPdist = DsmonPdistGiveLRUEntry
                        (pPdistCtl->u4DsmonPdistCtlIndex);
                    if (pCurPdist != NULL)
                    {
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "Deleting Pdist LRU Entry\r\n");

                        /* Remove Inter table dependencies */
                        DsmonPdistDelInterDep (pCurPdist);

                        pCurPdist->pPdistCtl->u4DsmonPdistCtlDeletes++;

                        /* After Reference changes, free Pdist stats entry */
                        DsmonPdistDelDataEntry (pCurPdist);

                        pCurPdist = NULL;
                    }
                }

                pCurPdist = DsmonPdistAddDataEntry (u4PdistCtlIndex,
                                                    u4PdistGrpIndex,
                                                    pPktInfo->u4ProtoALIndex);
                if (pCurPdist == NULL)
                {
                    pPdistCtl->u4DsmonPdistCtlDroppedFrames++;

                    /* Get next Stats Ctl */
                    pPdistCtl =
                        DsmonPdistGetNextCtlIndex (pPdistCtl->
                                                   u4DsmonPdistCtlIndex);
                    continue;
                }

                pCurPdist->pPdistCtl = pPdistCtl;

                pPdistCtl->u4DsmonPdistCtlInserts++;
            }
            pCurPdist->u4PktRefCount++;
            pCurPdist->DsmonCurSample.u4DsmonPdistStatsPkts++;
            pCurPdist->DsmonCurSample.u4DsmonPdistStatsOctets += u4PktSize;
            UINT8_ADD (pCurPdist->DsmonCurSample.u8DsmonPdistStatsHCPkts, 1);
            UINT8_ADD (pCurPdist->DsmonCurSample.u8DsmonPdistStatsHCOctets,
                       u4PktSize);
            pCurPdist->u4DsmonPdistTimeMark = OsixGetSysUpTime ();

            if (pPrevPdist == NULL)
            {
                pPktInfo->pPdist = pCurPdist;
            }
            else
            {
                pPrevPdist->pNextPdist = pCurPdist;
                pCurPdist->pPrevPdist = pPrevPdist;
            }
            i4RetVal = OSIX_SUCCESS;
            pPrevPdist = pCurPdist;
        }
        pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);
    }                            /* End of While */

    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonPdistProcessPktInfo\r\n");
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistUpdateAggCtlChgs                                 */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/*                tables entries into NOTREADY and cleanup all data tables.  */
/*                If u1DsmonAggControlStatus is TRUE, update control tables  */
/*                and resume data collection                                 */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistUpdateAggCtlChgs (UINT1 u1DsmonAggControlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tDsmonPdistCtl     *pPdistCtl = NULL;
    tDsmonAggCtl       *pAggCtl = NULL;
    UINT4               u4PdistCtlIndex = 0;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonPdistUpdateAggCtlChgs: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));
    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistUpdateAggCtlChgs\r\n");
    switch (u1DsmonAggControlStatus)
    {
        case DSMON_AGGLOCK_FALSE:
            /* Get First PdistCtl Entry */
            pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);
            while (pPdistCtl != NULL)
            {
                u4PdistCtlIndex = pPdistCtl->u4DsmonPdistCtlIndex;
                if (pPdistCtl->u4DsmonPdistCtlRowStatus == ACTIVE)
                {
                    pPdistCtl->u4DsmonPdistCtlRowStatus = NOT_READY;
                    if (CfaGetIfInfo
                        (pPdistCtl->u4DsmonPdistDataSource,
                         &IfInfo) != CFA_FAILURE)
                    {
                        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                            (CfaGetVlanId
                             (pPdistCtl->u4DsmonPdistDataSource,
                              &u2VlanId) != CFA_FAILURE))
                        {
                            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                        }
                    }
#ifdef NPAPI_WANTED
                    DsmonFsDSMONDisableProbe (pPdistCtl->u4DsmonPdistDataSource,
                                              u2VlanId, *pVlanPortList);
#endif
                }
                /* Get Next PdistCtl Entry */
                pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);
            }

            /* Delete Statistics Table */
            RBTreeDrain (DSMON_PDIST_TABLE, DsmonUtlRBFreePdistStats, 0);
            break;
        case DSMON_AGGLOCK_TRUE:
            /* Get First PdistCtl Entry */
            pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);
            while (pPdistCtl != NULL)
            {
                u4PdistCtlIndex = pPdistCtl->u4DsmonPdistCtlIndex;
                /* Validate Aggregation Control Index */
                pAggCtl =
                    DsmonAggCtlGetEntry (pPdistCtl->
                                         u4DsmonPdistCtlProfileIndex);
                if ((pAggCtl != NULL)
                    && (pAggCtl->u4DsmonAggCtlRowStatus == ACTIVE))
                {
                    if (CfaGetIfInfo
                        (pPdistCtl->u4DsmonPdistDataSource,
                         &IfInfo) == CFA_FAILURE)
                    {
                        /* ifindex is invalid */
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "DsmonPdistUpdateAggCtlChgs: Invalid interface Index \n");
                        FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                        return;
                    }

                    if ((IfInfo.u1IfOperStatus != CFA_IF_UP)
                        || (IfInfo.u1IfAdminStatus != CFA_IF_UP))
                    {
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "DsmonPdistUpdateAggCtlChgs: \
                Interface OperStatus or Admin Status is in invalid state \n");
                        FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                        return;
                    }

                    if (IfInfo.u1IfType == CFA_L3IPVLAN)
                    {
                        /* Get the VlanId from interface index */
                        if (CfaGetVlanId
                            (pPdistCtl->u4DsmonPdistDataSource,
                             &u2VlanId) == CFA_FAILURE)
                        {
                            DSMON_TRC1 (DSMON_DEBUG_TRC, "DsmonPdistUpdateAggCtlChgs: \
                    Unable to Fetch VlanId of IfIndex %d\n",
                                        pPdistCtl->u4DsmonPdistDataSource);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return;
                        }

                        if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList)
                            == L2IWF_FAILURE)
                        {
                            DSMON_TRC1 (DSMON_DEBUG_TRC, "DsmonPdistUpdateAggCtlChgs: \
                    Get Member Port List for VlanId %d failed \n",
                                        u2VlanId);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return;
                        }
                    }
#ifdef NPAPI_WANTED
                    DsmonFsDSMONEnableProbe (pPdistCtl->u4DsmonPdistDataSource,
                                             u2VlanId, *pVlanPortList);
#endif
                    /* Set back RowStatus of PdistCtl as ACTIVE */
                    pPdistCtl->u4DsmonPdistCtlRowStatus = ACTIVE;
                }
                pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);
            }
            break;
        default:
            break;
    }
    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistUpdateProtoChgs                                  */
/*                                                                           */
/* Description  : If RMONv2 removes an existing protocol support, the same   */
/*                needs to be updated on Pdist Data Table                    */
/*                                                                           */
/* Input        : u1DsmonProtoLocalIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistUpdateProtoChgs (UINT4 u4DsmonPdistProtDirLocalIndex)
{
    tDsmonPdistStats   *pPdistStats = NULL;
    UINT4               u4PdistCtlIndex = 0, u4PdistGrpIndex =
        0, u4PdistProtoIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistUpdateProtoChgs\r\n");

    /* Get First Entry in Pdist Table */
    pPdistStats = DsmonPdistGetNextDataIndex
        (u4PdistCtlIndex, u4PdistGrpIndex, u4PdistProtoIndex);

    while (pPdistStats != NULL)
    {
        u4PdistCtlIndex = pPdistStats->u4DsmonPdistCtlIndex;
        u4PdistGrpIndex = pPdistStats->u4DsmonPdistAggGroupIndex;
        u4PdistProtoIndex = pPdistStats->u4DsmonPdistProtDirLocalIndex;

        if (pPdistStats->u4DsmonPdistProtDirLocalIndex ==
            u4DsmonPdistProtDirLocalIndex)
        {
            /* Remove Inter table dependencies */
            DsmonPdistDelInterDep (pPdistStats);

            pPdistStats->pPdistCtl->u4DsmonPdistCtlDeletes++;

            /* After Reference changes, free Pdist stats entry */
            DsmonPdistDelDataEntry (pPdistStats);
        }

        pPdistStats = DsmonPdistGetNextDataIndex (u4PdistCtlIndex,
                                                  u4PdistGrpIndex,
                                                  u4PdistProtoIndex);
    }                            /* End of While */
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistUpdateIfaceChgs                                  */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then set appropriate control */
/*                entries to NOT_READY state thus deleting the associated    */
/*                Data Table. If OperStatus is CFA_IF_UP, set NOT_READY      */
/*                entries to ACTIVE state                                    */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistUpdateIfaceChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tDsmonPdistCtl     *pPdistCtl = NULL;
    UINT4               u4PdistCtlIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistUpdateIfaceChgs\r\n");

    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:
            /* Get First Pdist Control Table */

            pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);

            while (pPdistCtl != NULL)
            {
                u4PdistCtlIndex = pPdistCtl->u4DsmonPdistCtlIndex;
                if ((pPdistCtl->u4DsmonPdistCtlRowStatus == ACTIVE) &&
                    (pPdistCtl->u4DsmonPdistDataSource == u4IfIndex))
                {
                    DsmonPdistDelDataEntries (u4PdistCtlIndex);

                    /* Set RowStatus as NOT_READY */

                    pPdistCtl->u4DsmonPdistCtlRowStatus = NOT_READY;
                }

                pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);
            }
            break;
        case CFA_IF_UP:
            /* Get First Pdist Control Table */

            pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);

            while (pPdistCtl != NULL)
            {
                u4PdistCtlIndex = pPdistCtl->u4DsmonPdistCtlIndex;
                if ((pPdistCtl->u4DsmonPdistCtlRowStatus == NOT_READY) &&
                    (pPdistCtl->u4DsmonPdistDataSource == u4IfIndex))
                {
                    /* Set RowStatus as ACTIVE */
                    nmhSetDsmonPdistCtlStatus (u4PdistCtlIndex, ACTIVE);
                }

                pPdistCtl = DsmonPdistGetNextCtlIndex (u4PdistCtlIndex);
            }
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistGetNextCtlIndex                                  */
/*                                                                           */
/* Description  : Get First / Next Pdist Ctl Index                  */
/*                                                                           */
/* Input        : u4DsmonPdistCtlIndex                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistCtl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistCtl *
DsmonPdistGetNextCtlIndex (UINT4 u4DsmonPdistCtlIndex)
{
    tDsmonPdistCtl     *pPdistCtl = NULL, PdistCtl;

    MEMSET (&PdistCtl, DSMON_INIT_VAL, sizeof (tDsmonPdistCtl));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistGetNextCtlIndex\r\n");

    PdistCtl.u4DsmonPdistCtlIndex = u4DsmonPdistCtlIndex;
    pPdistCtl = (tDsmonPdistCtl *) RBTreeGetNext (DSMON_PDISTCTL_TABLE,
                                                  (tRBElem *) & PdistCtl, NULL);

    return pPdistCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistAddCtlEntry                                      */
/*                                                                           */
/* Description  : Allocates memory and Adds Pdist Control Entry into         */
/*          dsmonPdistCtlRBTree                           */
/*                                                                           */
/* Input        : pPdistCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistCtl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistCtl *
DsmonPdistAddCtlEntry (UINT4 u4DsmonPdistControlIndex)
{
    tDsmonPdistCtl     *pPdistCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistAddCtlEntry \r\n");

    if ((pPdistCtl = (tDsmonPdistCtl *)
         (MemAllocMemBlk (DSMON_PDISTCTL_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Pdist. Ctl.\r\n");
        return NULL;
    }

    MEMSET (pPdistCtl, DSMON_INIT_VAL, sizeof (tDsmonPdistCtl));

    pPdistCtl->u4DsmonPdistCtlIndex = u4DsmonPdistControlIndex;
    pPdistCtl->u4DsmonPdistCtlRowStatus = NOT_READY;
    pPdistCtl->i4DsmonPdistCtlMaxDesiredEntries = DSMON_MAX_DATA_PER_CTL;
    pPdistCtl->u4DsmonPdistCtlMaxDesiredSupported = DSMON_MAX_DATA_PER_CTL;

    if (RBTreeAdd (DSMON_PDISTCTL_TABLE, (tRBElem *) pPdistCtl) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "DsmonPdistAddCtlEntry Entry is Added\r\n");
        return pPdistCtl;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonPdistAddCtlEntry Entry Addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_PDISTCTL_POOL, (UINT1 *) pPdistCtl);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistDelCtlEntry                                      */
/*                                                                           */
/* Description  : Releases memory and Removes Pdist Control Entry from       */
/*          dsmonPdistCtlRBTree                           */
/*                                                                           */
/* Input        : pPdistCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonPdistDelCtlEntry (tDsmonPdistCtl * pPdistCtl)
{
    tPortList          *pVlanPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonPdistDelCtlEntry: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistDelCtlEntry\r\n");
    if (CfaGetIfInfo (pPdistCtl->u4DsmonPdistDataSource, &IfInfo) !=
        CFA_FAILURE)
    {
        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
            (CfaGetVlanId (pPdistCtl->u4DsmonPdistDataSource, &u2VlanId) !=
             CFA_FAILURE))
        {
            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
        }
    }

#ifdef NPAPI_WANTED
    /* Remove Probe support from H/W Table */
    DsmonFsDSMONDisableProbe (pPdistCtl->u4DsmonPdistDataSource, u2VlanId,
                              *pVlanPortList);
#endif
    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    /* Remove Data Entries for this control index from Pdist Table */
    DsmonPdistDelDataEntries (pPdistCtl->u4DsmonPdistCtlIndex);

    /* Remove Pdist. Ctl node from dsmonPdistCtlRBTree */
    RBTreeRem (DSMON_PDISTCTL_TABLE, (tRBElem *) pPdistCtl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_PDISTCTL_POOL,
                            (UINT1 *) pPdistCtl) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Pdist. Ctl.\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistGetCtlEntry                                      */
/*                                                                           */
/* Description  : Get Pdist Control Entry for the given control index        */
/*                                                                           */
/* Input        : u4DsmonPdistControlIndex                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistCtl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistCtl *
DsmonPdistGetCtlEntry (UINT4 u4DsmonPdistControlIndex)
{
    tDsmonPdistCtl     *pPdistCtl = NULL, PdistCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistGetCtlEntry\r\n");
    MEMSET (&PdistCtl, DSMON_INIT_VAL, sizeof (tDsmonPdistCtl));

    PdistCtl.u4DsmonPdistCtlIndex = u4DsmonPdistControlIndex;

    pPdistCtl = (tDsmonPdistCtl *) RBTreeGet (DSMON_PDISTCTL_TABLE,
                                              (tRBElem *) & PdistCtl);

    return pPdistCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistGetNextDataIndex                                 */
/*                                                                           */
/* Description  : Get First / Next Pdist Stats Index                      */
/*                                                                           */
/* Input        : u4DsmonPdistCtlIndex                                       */
/*          u4DsmonPdistAggGroupIndex                     */
/*          u4DsmonPdistProtDirLocalIndex                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistStats / NULL Pointer                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistStats *
DsmonPdistGetNextDataIndex (UINT4 u4DsmonPdistCtlIndex,
                            UINT4 u4DsmonPdistAggGroupIndex,
                            UINT4 u4DsmonPdistProtDirLocalIndex)
{
    tDsmonPdistStats   *pPdistStats = NULL, PdistStats;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistGetNextDataIndex\r\n");
    MEMSET (&PdistStats, DSMON_INIT_VAL, sizeof (tDsmonPdistStats));

    if ((u4DsmonPdistCtlIndex != 0) && (u4DsmonPdistProtDirLocalIndex == 0))
    {
        pPdistStats = (tDsmonPdistStats *) RBTreeGetFirst (DSMON_PDIST_TABLE);
        while (pPdistStats != NULL)
        {
            if (pPdistStats->u4DsmonPdistCtlIndex == u4DsmonPdistCtlIndex)
            {
                break;
            }
            MEMCPY (&PdistStats, pPdistStats, sizeof (tDsmonPdistStats));

            pPdistStats = (tDsmonPdistStats *) RBTreeGetNext (DSMON_PDIST_TABLE,
                                                              (tRBElem *) &
                                                              PdistStats, NULL);
        }
    }
    else
    {
        PdistStats.u4DsmonPdistCtlIndex = u4DsmonPdistCtlIndex;
        PdistStats.u4DsmonPdistAggGroupIndex = u4DsmonPdistAggGroupIndex;
        PdistStats.u4DsmonPdistProtDirLocalIndex =
            u4DsmonPdistProtDirLocalIndex;

        pPdistStats = (tDsmonPdistStats *) RBTreeGetNext (DSMON_PDIST_TABLE,
                                                          (tRBElem *) &
                                                          PdistStats, NULL);
    }
    return pPdistStats;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistAddDataEntry                                    */
/*                                                                           */
/* Description  : Allocates memory and Adds Pdist Stats Entry into              */
/*          dsmonPdistRBTree                           */
/*                                                                           */
/* Input        : pPdistStats                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistStats / NULL Pointer                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistStats *
DsmonPdistAddDataEntry (UINT4 u4DsmonPdistControlIndex,
                        UINT4 u4DsmonPdistAggGroupIndex,
                        UINT4 u4DsmonPdistProtDirLocalIndex)
{
    tDsmonPdistStats   *pPdistStats = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistAddDataEntry\r\n");
    if ((pPdistStats = (tDsmonPdistStats *)
         (MemAllocMemBlk (DSMON_PDIST_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Pdist. Stats\r\n");
        return NULL;
    }

    MEMSET (pPdistStats, DSMON_INIT_VAL, sizeof (tDsmonPdistStats));

    pPdistStats->u4DsmonPdistCtlIndex = u4DsmonPdistControlIndex;
    pPdistStats->u4DsmonPdistAggGroupIndex = u4DsmonPdistAggGroupIndex;
    pPdistStats->u4DsmonPdistProtDirLocalIndex = u4DsmonPdistProtDirLocalIndex;
    pPdistStats->u4DsmonPdistTimeMark = OsixGetSysUpTime ();
    pPdistStats->u4DsmonPdistStatsCreateTime = OsixGetSysUpTime ();

    if (RBTreeAdd (DSMON_PDIST_TABLE, (tRBElem *) pPdistStats) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonPdistAddDataEntry entry is added\r\n");
        return pPdistStats;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonPdistAddDataEntry entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_PDIST_POOL, (UINT1 *) pPdistStats);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistDelDataEntry                                     */
/*                                                                           */
/* Description  : Releases memory and Removes Pdist Entry from                 */
/*          dsmonPdistRBTree                           */
/*                                                                           */
/* Input        : pPdistStats                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonPdistDelDataEntry (tDsmonPdistStats * pPdistStats)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistDelDataEntry\r\n");
    /* Remove Pdist. node from dsmonPdistRBTree */
    RBTreeRem (DSMON_PDIST_TABLE, (tRBElem *) pPdistStats);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_PDIST_POOL,
                            (UINT1 *) pPdistStats) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Pdist.\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistDelDataEntries                                   */
/*                                                                           */
/* Description  : Removes Pdist Entries from dsmonPdistRBTree for given      */
/*                Control Index                                              */
/*                                                                           */
/* Input        : u4PdistCtlIndex                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistDelDataEntries (UINT4 u4PdistCtlIndex)
{
    tDsmonPdistStats   *pPdistStats = NULL;
    UINT4               u4PdistGrpIndex = 0, u4PdistProtoIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistDelDataEntries\r\n");

    pPdistStats = DsmonPdistGetNextDataIndex
        (u4PdistCtlIndex, u4PdistGrpIndex, u4PdistProtoIndex);

    while (pPdistStats != NULL)
    {
        if (pPdistStats->u4DsmonPdistCtlIndex != u4PdistCtlIndex)
        {
            break;
        }

        u4PdistGrpIndex = pPdistStats->u4DsmonPdistAggGroupIndex;
        u4PdistProtoIndex = pPdistStats->u4DsmonPdistProtDirLocalIndex;

        /* Remove Inter table dependencies */
        DsmonPdistDelInterDep (pPdistStats);

        pPdistStats->pPdistCtl->u4DsmonPdistCtlDeletes++;

        /* After Reference changes, free stats entry */
        DsmonPdistDelDataEntry (pPdistStats);

        pPdistStats = DsmonPdistGetNextDataIndex (u4PdistCtlIndex,
                                                  u4PdistGrpIndex,
                                                  u4PdistProtoIndex);
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonPdistDelDataEntries\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistGetDataEntry                                    */
/*                                                                           */
/* Description  : Get Pdist Control for the given control index                 */
/*                                                                           */
/* Input        : u4DsmonPdistControlIndex                                   */
/*          u4DsmonPdistAggGroupIndex                     */
/*          u4DsmonPdistProtDirLocalIndex                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistStats / NULL Pointer                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistStats *
DsmonPdistGetDataEntry (UINT4 u4DsmonPdistControlIndex,
                        UINT4 u4DsmonPdistAggGroupIndex,
                        UINT4 u4DsmonPdistProtDirLocalIndex)
{
    tDsmonPdistStats   *pPdistStats = NULL, PdistStats;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistGetDataEntry\r\n");
    MEMSET (&PdistStats, DSMON_INIT_VAL, sizeof (tDsmonPdistStats));

    PdistStats.u4DsmonPdistCtlIndex = u4DsmonPdistControlIndex;
    PdistStats.u4DsmonPdistAggGroupIndex = u4DsmonPdistAggGroupIndex;
    PdistStats.u4DsmonPdistProtDirLocalIndex = u4DsmonPdistProtDirLocalIndex;

    pPdistStats = (tDsmonPdistStats *) RBTreeGet (DSMON_PDIST_TABLE,
                                                  (tRBElem *) & PdistStats);
    return pPdistStats;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistDelInterDep                                      */
/*                                                                           */
/* Description  : Removes the inter-dependencies and re-order the links         */
/*                                                                           */
/* Input        : pPdist                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistDelInterDep (tDsmonPdistStats * pPdistStats)
{
    tDsmonPktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistDelInterDep\r\n");
    MEMSET (&PktHdr, DSMON_INIT_VAL, sizeof (tPktHeader));

    if (pPdistStats->pPrevPdist == NULL)
    {
        /* Update PktInfo nodes to point next pPdist Entry */
        pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            MEMCPY (&PktHdr, &pPktInfo->PktHdr, sizeof (tPktHeader));
            if (pPktInfo->pPdist == pPdistStats)
            {
                pPktInfo->pPdist = pPdistStats->pNextPdist;
                if (pPktInfo->pPdist != NULL)
                {
                    pPktInfo->pPdist->pPrevPdist = NULL;
                }
            }
            pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);
        }
    }
    else if (pPdistStats->pNextPdist != NULL)
    {
        pPdistStats->pPrevPdist->pNextPdist = pPdistStats->pNextPdist;
    }
    else
    {
        pPdistStats->pPrevPdist->pNextPdist = NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistPopulateDataEntries                              */
/*                                                                           */
/* Description  : Populates data entries if the same data source is already  */
/*          present and Adds the inter-dependencies b/w other tables   */
/*                                                                           */
/* Input        : pPdistCtlParam                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistPopulateDataEntries (tDsmonPdistCtl * pPdistCtlParam)
{
    tDsmonPktInfo      *pPktInfo = NULL;
    tDsmonPdistStats   *pPdist = NULL, *pNewPdist = NULL;
    tPktHeader          PktHdr;
    UINT4               u4PdistGrpIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPdistPopulateDataEntries\r\n");

    MEMSET (&PktHdr, DSMON_INIT_VAL, sizeof (tPktHeader));

    /* Check for the presence of data source in pktinfo table */
    pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);
    while (pPktInfo != NULL)
    {
        if ((pPktInfo->PktHdr.u4IfIndex ==
             pPdistCtlParam->u4DsmonPdistDataSource)
            && (pPktInfo->PktHdr.u1DSCP < DSMON_MAX_AGG_PROFILE_DSCP)
            && (pPktInfo->u4ProtoALIndex != 0))
        {
            u4PdistGrpIndex =
                pPdistCtlParam->pAggCtl->au4DsmonAggProfile[pPktInfo->PktHdr.
                                                            u1DSCP];

            /* Check whether data entries reached max limit */
            if ((pPdistCtlParam->u4DsmonPdistCtlInserts -
                 pPdistCtlParam->u4DsmonPdistCtlDeletes) >=
                pPdistCtlParam->u4DsmonPdistCtlMaxDesiredSupported)
            {
                pNewPdist = DsmonPdistGiveLRUEntry
                    (pPdistCtlParam->u4DsmonPdistCtlIndex);
                if (pNewPdist != NULL)
                {
                    DSMON_TRC (DSMON_DEBUG_TRC, "Deleting Pdist LRU Entry\r\n");

                    /* Remove Inter table dependencies */
                    DsmonPdistDelInterDep (pNewPdist);

                    pNewPdist->pPdistCtl->u4DsmonPdistCtlDeletes++;

                    /* After Reference changes, free Pdist stats entry */
                    DsmonPdistDelDataEntry (pNewPdist);

                    pNewPdist = NULL;
                }
            }
            /* Add Pdist Entry */
            pNewPdist =
                DsmonPdistAddDataEntry (pPdistCtlParam->u4DsmonPdistCtlIndex,
                                        u4PdistGrpIndex,
                                        pPktInfo->u4ProtoALIndex);
            if (pNewPdist == NULL)
            {
                pPdistCtlParam->u4DsmonPdistCtlDroppedFrames++;

                pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }

            pNewPdist->pPdistCtl = pPdistCtlParam;
            pNewPdist->u4PktRefCount++;

            pPdistCtlParam->u4DsmonPdistCtlInserts++;

            pPdist = pPktInfo->pPdist;
            if (pPdist == NULL)
            {
                pPktInfo->pPdist = pNewPdist;
            }
            else
            {
                while (pPdist->pNextPdist != NULL)
                {
                    pPdist = pPdist->pNextPdist;
                }
                pPdist->pNextPdist = pNewPdist;
                pNewPdist->pPrevPdist = pPdist;
            }
        }

        pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
    }
    return;
}
