/********************************************************************
 *                                                                  *
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            *
 *                                                                  *
 * $Id: dsmnnpwr.c,v 1.2 2013/01/10 12:46:12 siva Exp $           *
 *                                                                  *
 * Description:                                                     *
 *          Contains wrapper functions for DSMON NP calls.
 *                                                                  *
 *******************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : DsmonNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tDsmonNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __DSMON_NP_WR_C
#define __DSMON_NP_WR_C

#include "nputil.h"
#include "dsmonnp.h"

PUBLIC UINT1
DsmonNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    UINT1               u1RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pDsmonNpModInfo = &(pFsHwNp->DsmonNpModInfo);

    if (NULL == pDsmonNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_DSMON_ADD_FLOW_STATS:
        {
            tDsmonNpWrFsDSMONAddFlowStats *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONAddFlowStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsDSMONAddFlowStats (pEntry->FlowStatsTuple);
            break;
        }
        case FS_DSMON_COLLECT_STATS:
        {
            tDsmonNpWrFsDSMONCollectStats *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONCollectStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsDSMONCollectStats (pEntry->FlowStatsTuple, pEntry->pStatsCnt);
            break;
        }
        case FS_DSMON_ENABLE_PROBE:
        {
            tDsmonNpWrFsDSMONEnableProbe *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONEnableProbe;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsDSMONEnableProbe (pEntry->u4InterfaceIdx, pEntry->u2VlanId,
                                    pEntry->PortList);
            break;
        }
        case FS_DSMON_DISABLE_PROBE:
        {
            tDsmonNpWrFsDSMONDisableProbe *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONDisableProbe;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal =
                FsDSMONDisableProbe (pEntry->u4InterfaceIdx, pEntry->u2VlanId,
                                     pEntry->PortList);
            break;
        }
        case FS_DSMON_HW_SET_STATUS:
        {
            tDsmonNpWrFsDSMONHwSetStatus *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONHwSetStatus;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsDSMONHwSetStatus (pEntry->u4DSMONStatus);
            break;
        }
        case FS_DSMON_HW_GET_STATUS:
        {
            u1RetVal = FsDSMONHwGetStatus ();
            break;
        }
        case FS_DSMON_REMOVE_FLOW_STATS:
        {
            tDsmonNpWrFsDSMONRemoveFlowStats *pEntry = NULL;
            pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONRemoveFlowStats;
            if (NULL == pEntry)
            {
                u1RetVal = FNP_FAILURE;
                break;
            }
            u1RetVal = FsDSMONRemoveFlowStats (pEntry->FlowStatsTuple);
            break;
        }
        default:
            u1RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return (u1RetVal);
}

#endif /* __DSMON_NP_WR_C */
