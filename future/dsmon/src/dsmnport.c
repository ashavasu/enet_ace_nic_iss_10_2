/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *
 * $Id: dsmnport.c,v 1.4 2012/07/20 10:38:15 siva Exp $           *
 *                                                                  *
 * Description: This file contains the functional routines         *
 *        of DSMON Porting module                 *
 *                                                                  *
 *******************************************************************/
#include "dsmninc.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonFetchHWStats                                          */
/*                                                                           */
/* Description  : This routine collects the statistics from hardware         */
/*                                                                           */
/* Input        : None                                                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
DsmonFetchHwStats (tDsmonPktInfo * pPktInfoParam)
{
    tDsmonPktInfo      *pPktInfo = NULL, PktInfo;
    tRmon2Stats         HwStats;
    UINT4               u4EntriesVistited = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonFetchHwStats\r\n");

    MEMSET (&PktInfo, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));

    pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfoParam->PktHdr);
    /* If last visited entry reaches end of tree,
     * then reset the entry values to start it from start
     * of the RBTree */
    if (pPktInfo == NULL)
    {
        MEMSET (pPktInfoParam, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));
        pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfoParam->PktHdr);
        if (pPktInfo == NULL)
        {
            DSMON_TRC (DSMON_DEBUG_TRC,
                       "DsmonFetchHwStats No PktInfo entry\r\n");
            return;
        }

    }

    while ((++u4EntriesVistited < DSMON_MAX_STATS_PER_CYCLE) &&
           (pPktInfo != NULL))
    {
        MEMSET (&HwStats, 0, sizeof (tRmon2Stats));
        MEMCPY (&PktInfo, pPktInfo, sizeof (tDsmonPktInfo));

        DsmonFsDSMONCollectStats (pPktInfo->PktHdr, &HwStats);

        DsmonUpdateDataTables (pPktInfo, &HwStats);

        pPktInfo = DsmonPktInfoGetNextIndex (&PktInfo.PktHdr);
    }
    if (pPktInfo == NULL)
    {
        MEMSET (pPktInfoParam, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));
    }
    else
    {
        MEMCPY (pPktInfoParam, pPktInfo, sizeof (tDsmonPktInfo));
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonFetchHwStats\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonUpdateDataTables                                      */
/*                                                                           */
/* Description  : This function will be invoked by NPAPI to update data      */
/*                entries for the given tuple                                */
/*                                                                           */
/* Input        : pPktInfo, Counter value                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
DsmonUpdateDataTables (tDsmonPktInfo * pPktInfo, tRmon2Stats * pHwStats)
{
    tDsmonStats        *pStatsNode = NULL;
    tDsmonPdistStats   *pPdistNode = NULL;
    tDsmonHostStats    *pHostNode = NULL;
    tDsmonMatrixSD     *pMatrixSD = NULL;
    INT4                i4DuplexStatus = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonUpdateDataTables\r\n");

    if (pPktInfo == NULL)
    {
        return;
    }

    /* If Packet and Octet counts are Zero
     * then increment Idle reference and if it reaches
     * the max value then remove PktInfo and associated data entries */
    if ((pHwStats->u8PktCnt.u4LoWord == 0) &&
        (pHwStats->u8OctetCnt.u4LoWord == 0))
    {
        if (++pPktInfo->u4IdleRef >= DSMON_MAX_IDLE_CNT)
        {
            DsmonPktInfoDelIdlePkt (pPktInfo);
        }
        return;
    }
    else
    {
        /* Reset Reference Count */
        pPktInfo->u4IdleRef = 0;
    }
    /* Update Matrix Table */
    pMatrixSD = pPktInfo->pMatrixSD;

    while (pMatrixSD != NULL)
    {
        pMatrixSD->DsmonCurMatrixSDSample.u4DsmonMatrixSDPkts +=
            pHwStats->u8PktCnt.u4LoWord;
        UINT8_ADD (pMatrixSD->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCPkts,
                   pHwStats->u8PktCnt.u4LoWord);
        pMatrixSD->DsmonCurMatrixSDSample.u4DsmonMatrixSDOctets +=
            pHwStats->u8OctetCnt.u4LoWord;
        UINT8_ADD (pMatrixSD->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCOctets,
                   pHwStats->u8OctetCnt.u4LoWord);
        pMatrixSD->u4DsmonMatrixSDTimeMark = OsixGetSysUpTime ();

        pMatrixSD = pMatrixSD->pNextMatrixSD;
    }

    /* Update Host Table */
    pHostNode = pPktInfo->pOutHost;

    while (pHostNode != NULL)
    {

        pHostNode->DsmonCurSample.u4DsmonHostOutPkts +=
            pHwStats->u8PktCnt.u4LoWord;
        pHostNode->DsmonCurSample.u4DsmonHostOutOctets +=
            pHwStats->u8OctetCnt.u4LoWord;
        UINT8_ADD (pHostNode->DsmonCurSample.u8DsmonHostOutHCPkts,
                   pHwStats->u8PktCnt.u4LoWord);
        UINT8_ADD (pHostNode->DsmonCurSample.u8DsmonHostOutHCOctets,
                   pHwStats->u8OctetCnt.u4LoWord);
        pHostNode->u4DsmonHostTimeMark = (UINT4) OsixGetSysUpTime ();

        pHostNode = pHostNode->pNextHost;
    }

    pHostNode = pPktInfo->pInHost;

    while (pHostNode != NULL)
    {
        pHostNode->DsmonCurSample.u4DsmonHostInPkts +=
            pHwStats->u8PktCnt.u4LoWord;
        pHostNode->DsmonCurSample.u4DsmonHostInOctets +=
            pHwStats->u8OctetCnt.u4LoWord;
        UINT8_ADD (pHostNode->DsmonCurSample.u8DsmonHostInHCPkts,
                   pHwStats->u8PktCnt.u4LoWord);
        UINT8_ADD (pHostNode->DsmonCurSample.u8DsmonHostInHCOctets,
                   pHwStats->u8OctetCnt.u4LoWord);
        pHostNode->u4DsmonHostTimeMark = (UINT4) OsixGetSysUpTime ();

        pHostNode = pHostNode->pNextHost;
    }

    /* Update Pdist Taiiible */
    pPdistNode = pPktInfo->pPdist;
    while (pPdistNode != NULL)
    {
        pPdistNode->DsmonCurSample.u4DsmonPdistStatsPkts +=
            pHwStats->u8PktCnt.u4LoWord;
        UINT8_ADD (pPdistNode->DsmonCurSample.u8DsmonPdistStatsHCPkts,
                   pHwStats->u8PktCnt.u4LoWord);
        pPdistNode->DsmonCurSample.u4DsmonPdistStatsOctets +=
            pHwStats->u8OctetCnt.u4LoWord;
        UINT8_ADD (pPdistNode->DsmonCurSample.u8DsmonPdistStatsHCOctets,
                   pHwStats->u8OctetCnt.u4LoWord);
        pPdistNode->u4DsmonPdistTimeMark = OsixGetSysUpTime ();

        pPdistNode = pPdistNode->pNextPdist;
    }

    /* Update Stats Table */

    IssSnmpLowGetPortCtrlDuplex (pPktInfo->PktHdr.u4IfIndex, &i4DuplexStatus);

    if (i4DuplexStatus == ISS_FULLDUP)
    {
        pStatsNode = pPktInfo->pOutStats;

        while (pStatsNode != NULL)
        {
            pStatsNode->u4DsmonStatsOutPkts += pHwStats->u8PktCnt.u4LoWord;
            pStatsNode->u4DsmonStatsOutOctets += pHwStats->u8OctetCnt.u4LoWord;
            UINT8_ADD (pStatsNode->u8DsmonStatsOutHCPkts,
                       pHwStats->u8PktCnt.u4LoWord);
            UINT8_ADD (pStatsNode->u8DsmonStatsOutHCOctets,
                       pHwStats->u8OctetCnt.u4LoWord);

            pStatsNode = pStatsNode->pNextStats;
        }

        pStatsNode = pPktInfo->pInStats;

        while (pStatsNode != NULL)
        {
            pStatsNode->u4DsmonStatsInPkts += pHwStats->u8PktCnt.u4LoWord;
            pStatsNode->u4DsmonStatsInOctets += pHwStats->u8OctetCnt.u4LoWord;
            UINT8_ADD (pStatsNode->u8DsmonStatsInHCPkts,
                       pHwStats->u8PktCnt.u4LoWord);
            UINT8_ADD (pStatsNode->u8DsmonStatsInHCOctets,
                       pHwStats->u8OctetCnt.u4LoWord);

            pStatsNode = pStatsNode->pNextStats;
        }

    }
    else if (i4DuplexStatus == ISS_HALFDUP)
    {
        pStatsNode = pPktInfo->pOutStats;

        while (pStatsNode != NULL)
        {
            pStatsNode->u4DsmonStatsInPkts += pHwStats->u8PktCnt.u4LoWord;
            pStatsNode->u4DsmonStatsInOctets += pHwStats->u8OctetCnt.u4LoWord;
            UINT8_ADD (pStatsNode->u8DsmonStatsInHCPkts,
                       pHwStats->u8PktCnt.u4LoWord);
            UINT8_ADD (pStatsNode->u8DsmonStatsInHCOctets,
                       pHwStats->u8OctetCnt.u4LoWord);

            pStatsNode = pStatsNode->pNextStats;
        }
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonUpdateDataTables\r\n");
}
