/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *                                                                  *
 * $Id: dsmnstat.c,v 1.9 2012/12/13 14:25:10 siva Exp $           *
 *
 *  Description: This file contains the functional routines         *
 *        for DSMON Statistics module                         *
 *                                                                  *
 *******************************************************************/
#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsProcessPktInfo                                 */
/*                                                                           */
/* Description  : Once incoming packet is recieved by Control Plane, for     */
/*          every valid stats control entry, new data entry will be    */
/*          created and establish inter links                 */
/*                                                                           */
/* Input        : PktInfo                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonStatsProcessPktInfo (tDsmonPktInfo * pPktInfo, UINT4 u4PktSize)
{
    tDsmonStatsCtl     *pStatsCtl = NULL;
    tDsmonStats        *pCurStats = NULL, *pPrevStats = NULL;
    UINT4               u4StatsCtlIndex = 0, u4StatsGrpIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    INT4                i4DuplexStatus = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsProcessPktInfo\r\n");
    /* Check for valid Control Entry */
    pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);

    while (pStatsCtl != NULL)
    {
        u4StatsCtlIndex = pStatsCtl->u4DsmonStatsCtlIndex;
        if ((pStatsCtl->u4DsmonStatsCtlRowStatus == ACTIVE) &&
            ((pStatsCtl->u4DsmonStatsDataSource == pPktInfo->PktHdr.u4IfIndex)
             || (pStatsCtl->u4DsmonStatsDataSource ==
                 pPktInfo->PktHdr.u4VlanIfIndex))
            && (pPktInfo->PktHdr.u1DSCP < DSMON_MAX_AGG_PROFILE_DSCP))
        {
            if (pStatsCtl->u4DsmonStatsDataSource ==
                pPktInfo->PktHdr.u4VlanIfIndex)
            {
                pPktInfo->u1IsL3Vlan = OSIX_TRUE;
            }
            u4StatsGrpIndex =
                pStatsCtl->pAggCtl->au4DsmonAggProfile[pPktInfo->PktHdr.u1DSCP];

            /* Check whether the entry already present */
            pCurStats = DsmonStatsGetDataEntry (u4StatsCtlIndex,
                                                u4StatsGrpIndex);
            if (pCurStats == NULL)
            {
                pCurStats = DsmonStatsAddDataEntry (u4StatsCtlIndex,
                                                    u4StatsGrpIndex);
                if (pCurStats == NULL)
                {
                    pStatsCtl->u4DsmonStatsCtlDroppedFrames++;

                    /* Get next Stats Ctl */
                    pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);
                    continue;
                }
                pCurStats->pStatsCtl = pStatsCtl;
            }
            pCurStats->u4PktRefCount++;

            IssSnmpLowGetPortCtrlDuplex (pPktInfo->PktHdr.u4IfIndex,
                                         &i4DuplexStatus);
            if (i4DuplexStatus == ISS_FULLDUP)
            {
                pCurStats->u4DsmonStatsOutPkts++;
                pCurStats->u4DsmonStatsOutOctets += u4PktSize;
                UINT8_ADD (pCurStats->u8DsmonStatsOutHCPkts, 1);
                UINT8_ADD (pCurStats->u8DsmonStatsOutHCOctets, u4PktSize);
            }
            if (pPrevStats == NULL)
            {
                pPktInfo->pOutStats = pCurStats;
            }
            else
            {
                pPrevStats->pNextStats = pCurStats;
                pCurStats->pPrevStats = pPrevStats;
            }
            i4RetVal = OSIX_SUCCESS;
            pPrevStats = pCurStats;
        }
        pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);
    }                            /* End of While */
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonStatsProcessPktInfo\r\n");
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsUpdateAggCtlChgs                                 */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/*                tables entries into NOT_READY and cleanup all data tables. */
/*                If u1DsmonAggControlStatus is TRUE, update control tables  */
/*                and resume data collection                                 */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonStatsUpdateAggCtlChgs (UINT1 u1DsmonAggControlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tDsmonStatsCtl     *pStatsCtl = NULL;
    tDsmonAggCtl       *pAggCtl = NULL;
    UINT4               u4StatsCtlIndex = 0;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonStatsUpdateAggCtlChgs: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsUpdateAggCtlChgs\r\n");
    switch (u1DsmonAggControlStatus)
    {
        case DSMON_AGGLOCK_FALSE:
            /* Get First StatsCtl Entry */
            pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);
            while (pStatsCtl != NULL)
            {
                u4StatsCtlIndex = pStatsCtl->u4DsmonStatsCtlIndex;
                if (pStatsCtl->u4DsmonStatsCtlRowStatus == ACTIVE)
                {
                    pStatsCtl->u4DsmonStatsCtlRowStatus = NOT_READY;
                    if (CfaGetIfInfo
                        (pStatsCtl->u4DsmonStatsDataSource,
                         &IfInfo) != CFA_FAILURE)
                    {
                        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                            (CfaGetVlanId
                             (pStatsCtl->u4DsmonStatsDataSource,
                              &u2VlanId) != CFA_FAILURE))
                        {
                            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                        }
                    }
#ifdef NPAPI_WANTED
                    DsmonFsDSMONDisableProbe (pStatsCtl->u4DsmonStatsDataSource,
                                              u2VlanId, *pVlanPortList);
#endif
                }
                /* Get Next StatsCtl Entry */
                pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);
            }

            /* Delete Statistics Table */
            RBTreeDrain (DSMON_STATS_TABLE, DsmonUtlRBFreeStats, 0);
            break;
        case DSMON_AGGLOCK_TRUE:
            /* Get First StatsCtl Entry */
            pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);
            while (pStatsCtl != NULL)
            {
                u4StatsCtlIndex = pStatsCtl->u4DsmonStatsCtlIndex;
                /* Validate Aggregation Control Index */
                pAggCtl =
                    DsmonAggCtlGetEntry (pStatsCtl->
                                         u4DsmonStatsCtlProfileIndex);
                if ((pAggCtl != NULL)
                    && (pAggCtl->u4DsmonAggCtlRowStatus == ACTIVE))
                {
                    if (CfaGetIfInfo
                        (pStatsCtl->u4DsmonStatsDataSource,
                         &IfInfo) == CFA_FAILURE)
                    {
                        /* ifindex is invalid */
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "DsmonStatsUpdateAggCtlChgs: Invalid interface Index \n");
                        FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                        return;
                    }

                    if ((IfInfo.u1IfOperStatus != CFA_IF_UP)
                        || (IfInfo.u1IfAdminStatus != CFA_IF_UP))
                    {
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "DsmonStatsUpdateAggCtlChgs: \
                Interface OperStatus or Admin Status is in invalid state \n");
                        FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                        return;
                    }

                    if (IfInfo.u1IfType == CFA_L3IPVLAN)
                    {
                        /* Get the VlanId from interface index */
                        if (CfaGetVlanId
                            (pStatsCtl->u4DsmonStatsDataSource,
                             &u2VlanId) == CFA_FAILURE)
                        {
                            DSMON_TRC1 (DSMON_DEBUG_TRC, "DsmonStatsUpdateAggCtlChgs: \
                    Unable to Fetch VlanId of IfIndex %d\n",
                                        pStatsCtl->u4DsmonStatsDataSource);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return;
                        }

                        if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList)
                            == L2IWF_FAILURE)
                        {
                            DSMON_TRC1 (DSMON_DEBUG_TRC, "DsmonStatsUpdateAggCtlChgs: \
                    Get Member Port List for VlanId %d failed \n",
                                        u2VlanId);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return;
                        }
                    }
#ifdef NPAPI_WANTED
                    DsmonFsDSMONEnableProbe (pStatsCtl->u4DsmonStatsDataSource,
                                             u2VlanId, *pVlanPortList);
#endif
                    /* Set back RowStatus of StatsCtl as ACTIVE */
                    pStatsCtl->u4DsmonStatsCtlRowStatus = ACTIVE;
                }
                pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);
            }
            break;
        default:
            break;
    }
    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsUpdateIfaceChgs                                  */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then set appropriate control */
/*          entries to NOT_READY state thus deleting the associated    */
/*          Data Table. If OperStatus is CFA_IF_UP, set NOT_READY      */
/*          entries to ACTIVE state                          */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonStatsUpdateIfaceChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tDsmonStatsCtl     *pStatsCtl = NULL;
    UINT4               u4StatsCtlIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsUpdateIfaceChgs\r\n");
    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:
            /* Get First Stats Control Table */
            pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);

            while (pStatsCtl != NULL)
            {
                u4StatsCtlIndex = pStatsCtl->u4DsmonStatsCtlIndex;
                if ((pStatsCtl->u4DsmonStatsCtlRowStatus == ACTIVE) &&
                    (pStatsCtl->u4DsmonStatsDataSource == u4IfIndex))
                {
                    /* Remove Data Entries */
                    DsmonStatsDelDataEntries (u4StatsCtlIndex);

                    /* Set RowStatus as NOT_READY */

                    pStatsCtl->u4DsmonStatsCtlRowStatus = NOT_READY;
                }

                pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);
            }
            break;
        case CFA_IF_UP:
            /* Get First Stats Control Table */

            pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);

            while (pStatsCtl != NULL)
            {
                u4StatsCtlIndex = pStatsCtl->u4DsmonStatsCtlIndex;
                if ((pStatsCtl->u4DsmonStatsCtlRowStatus == NOT_READY) &&
                    (pStatsCtl->u4DsmonStatsDataSource == u4IfIndex))
                {
                    /* Set RowStatus as ACTIVE */
                    nmhSetDsmonStatsControlStatus (u4StatsCtlIndex, ACTIVE);
                }

                pStatsCtl = DsmonStatsGetNextCtlIndex (u4StatsCtlIndex);
            }
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsGetNextCtlIndex                                  */
/*                                                                           */
/* Description  : Get First / Next Stats Ctl Index                  */
/*                                                                           */
/* Input        : u4DsmonStatsCtlIndex                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonStatsCtl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonStatsCtl *
DsmonStatsGetNextCtlIndex (UINT4 u4DsmonStatsCtlIndex)
{
    tDsmonStatsCtl     *pStatsCtl = NULL, StatsCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsGetNextCtlIndex\r\n");
    MEMSET (&StatsCtl, DSMON_INIT_VAL, sizeof (tDsmonStatsCtl));

    StatsCtl.u4DsmonStatsCtlIndex = u4DsmonStatsCtlIndex;
    pStatsCtl = (tDsmonStatsCtl *) RBTreeGetNext (DSMON_STATSCTL_TABLE,
                                                  (tRBElem *) & StatsCtl, NULL);
    return pStatsCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsAddCtlEntry                                      */
/*                                                                           */
/* Description  : Allocates memory and Adds Stats Control Entry into         */
/*          dsmonStatsCtlRBTree                           */
/*                                                                           */
/* Input        : pStatsCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonStatsCtl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonStatsCtl *
DsmonStatsAddCtlEntry (UINT4 u4DsmonStatsControlIndex)
{
    tDsmonStatsCtl     *pStatsCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsAddCtlEntry\r\n");
    if ((pStatsCtl = (tDsmonStatsCtl *)
         (MemAllocMemBlk (DSMON_STATSCTL_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Stats. Ctl.\r\n");
        return NULL;
    }

    MEMSET (pStatsCtl, DSMON_INIT_VAL, sizeof (tDsmonStatsCtl));

    pStatsCtl->u4DsmonStatsCtlIndex = u4DsmonStatsControlIndex;
    pStatsCtl->u4DsmonStatsCtlRowStatus = NOT_READY;

    if (RBTreeAdd (DSMON_STATSCTL_TABLE, (tRBElem *) pStatsCtl) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "DsmonStatsAddCtlEntry entry is added\r\n");
        return pStatsCtl;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonStatsAddCtlEntry entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_STATSCTL_POOL, (UINT1 *) pStatsCtl);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsDelCtlEntry                                      */
/*                                                                           */
/* Description  : Releases memory and Removes Stats Control Entry from       */
/*          dsmonStatsCtlRBTree                           */
/*                                                                           */
/* Input        : pStatsCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonStatsDelCtlEntry (tDsmonStatsCtl * pStatsCtl)
{
    tPortList          *pVlanPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonStatsDelCtlEntry: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsDelCtlEntry\r\n");

    if (CfaGetIfInfo (pStatsCtl->u4DsmonStatsDataSource, &IfInfo) !=
        CFA_FAILURE)
    {
        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
            (CfaGetVlanId (pStatsCtl->u4DsmonStatsDataSource, &u2VlanId) !=
             CFA_FAILURE))
        {
            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
        }
    }

#ifdef NPAPI_WANTED
    /* Remove Probe support from H/W Table */
    DsmonFsDSMONDisableProbe (pStatsCtl->u4DsmonStatsDataSource, u2VlanId,
                              *pVlanPortList);
#endif
    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    DsmonStatsDelDataEntries (pStatsCtl->u4DsmonStatsCtlIndex);

    /* Remove Stats. Ctl node from dsmonStatsCtlRBTree */
    RBTreeRem (DSMON_STATSCTL_TABLE, (tRBElem *) pStatsCtl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_STATSCTL_POOL,
                            (UINT1 *) pStatsCtl) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Stats. Ctl.\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsGetCtlEntry                                      */
/*                                                                           */
/* Description  : Get Stats Control Entry for the given control index        */
/*                                                                           */
/* Input        : u4DsmonStatsControlIndex                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonStatsCtl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonStatsCtl *
DsmonStatsGetCtlEntry (UINT4 u4DsmonStatsControlIndex)
{
    tDsmonStatsCtl     *pStatsCtl = NULL, StatsCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsGetCtlEntry\r\n");
    MEMSET (&StatsCtl, DSMON_INIT_VAL, sizeof (tDsmonStatsCtl));

    StatsCtl.u4DsmonStatsCtlIndex = u4DsmonStatsControlIndex;

    pStatsCtl = (tDsmonStatsCtl *) RBTreeGet (DSMON_STATSCTL_TABLE,
                                              (tRBElem *) & StatsCtl);
    return pStatsCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsGetNextDataIndex                                 */
/*                                                                           */
/* Description  : Get First / Next Stats Index                           */
/*                                                                           */
/* Input        : u4DsmonStatsCtlIndex, u4DsmonAggGroupIndex                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonStats / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonStats *
DsmonStatsGetNextDataIndex (UINT4 u4DsmonStatsCtlIndex,
                            UINT4 u4DsmonStatsAggGroupIndex)
{
    tDsmonStats        *pStats = NULL, Stats;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsGetNextDataIndex\r\n");
    MEMSET (&Stats, DSMON_INIT_VAL, sizeof (tDsmonStats));

    Stats.u4DsmonStatsCtlIndex = u4DsmonStatsCtlIndex;
    Stats.u4DsmonStatsAggGroupIndex = u4DsmonStatsAggGroupIndex;
    pStats = (tDsmonStats *) RBTreeGetNext (DSMON_STATS_TABLE,
                                            (tRBElem *) & Stats, NULL);

    return pStats;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsAddDataEntry                                     */
/*                                                                           */
/* Description  : Allocates memory and Adds Stats Entry into                  */
/*          dsmonStatsRBTree                           */
/*                                                                           */
/* Input        : pStats                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonStats / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonStats *
DsmonStatsAddDataEntry (UINT4 u4DsmonStatsControlIndex,
                        UINT4 u4DsmonStatsAggGroupIndex)
{
    tDsmonStats        *pStats = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsAddDataEntry\r\n");

    if ((pStats = (tDsmonStats *) (MemAllocMemBlk (DSMON_STATS_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Stats.\r\n");
        return NULL;
    }

    MEMSET (pStats, DSMON_INIT_VAL, sizeof (tDsmonStats));

    pStats->u4DsmonStatsCtlIndex = u4DsmonStatsControlIndex;
    pStats->u4DsmonStatsAggGroupIndex = u4DsmonStatsAggGroupIndex;
    pStats->pStatsCtl = DsmonStatsGetCtlEntry (u4DsmonStatsControlIndex);

    if (RBTreeAdd (DSMON_STATS_TABLE, (tRBElem *) pStats) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonStatsAddDataEntry entry is added\r\n");
        return pStats;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonStatsAddDataEntry entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_STATS_POOL, (UINT1 *) pStats);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsDelDataEntry                                     */
/*                                                                           */
/* Description  : Releases memory and Removes Stats Entry from                */
/*          dsmonStatsRBTree                           */
/*                                                                           */
/* Input        : pStats                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonStatsDelDataEntry (tDsmonStats * pStats)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsDelDataEntry\r\n");
    /* Remove Stats. Ctl node from dsmonStatsCtlRBTree */
    RBTreeRem (DSMON_STATS_TABLE, (tRBElem *) pStats);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_STATS_POOL, (UINT1 *) pStats) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Stats.\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsDelDataEntries                                   */
/*                                                                           */
/* Description  : Removes Stats Entries from dsmonStatsRBTree for given      */
/*          Control Index                                              */
/*                                                                           */
/* Input        : u4StatsCtlIndex                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonStatsDelDataEntries (UINT4 u4StatsCtlIndex)
{
    tDsmonStats        *pStats = NULL;
    UINT4               u4StatsGrpIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsDelDataEntries\r\n");
    /* Remove Data Entries for this control index from Stats Table */
    pStats = DsmonStatsGetNextDataIndex (u4StatsCtlIndex, u4StatsGrpIndex);

    while (pStats != NULL)
    {

        if (pStats->u4DsmonStatsCtlIndex != u4StatsCtlIndex)
        {
            break;
        }

        u4StatsGrpIndex = pStats->u4DsmonStatsAggGroupIndex;

        /* Remove Inter table dependencies */
        DsmonStatsDelInterDep (pStats);

        /* After Reference changes, free stats entry */
        DsmonStatsDelDataEntry (pStats);

        pStats = DsmonStatsGetNextDataIndex (u4StatsCtlIndex, u4StatsGrpIndex);
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonStatsDelDataEntries\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsGetDataEntry                                     */
/*                                                                           */
/* Description  : Get Stats Entry for the given index                     */
/*                                                                           */
/* Input        : u4DsmonStatsControlIndex, u4DsmonStatsAggGroupIndex        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonStats / NULL Pointer                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonStats *
DsmonStatsGetDataEntry (UINT4 u4DsmonStatsControlIndex,
                        UINT4 u4DsmonStatsAggGroupIndex)
{
    tDsmonStats        *pStats = NULL, Stats;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsGetDataEntry\r\n");
    MEMSET (&Stats, DSMON_INIT_VAL, sizeof (tDsmonStats));

    Stats.u4DsmonStatsCtlIndex = u4DsmonStatsControlIndex;
    Stats.u4DsmonStatsAggGroupIndex = u4DsmonStatsAggGroupIndex;

    pStats = (tDsmonStats *) RBTreeGet (DSMON_STATS_TABLE, (tRBElem *) & Stats);
    return pStats;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsDelInterDep                                      */
/*                                                                           */
/* Description  : Removes the inter-dependencies and re-order the links         */
/*                                                                           */
/* Input        : pStats                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonStatsDelInterDep (tDsmonStats * pStats)
{
    tDsmonPktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsDelInterDep\r\n");
    MEMSET (&PktHdr, DSMON_INIT_VAL, sizeof (tPktHeader));

    if (pStats->pPrevStats == NULL)
    {
        /* Update PktInfo nodes to point next pHost Entry */
        pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            if (pPktInfo->pOutStats == pStats)
            {
                pPktInfo->pOutStats = pStats->pNextStats;
                if (pPktInfo->pOutStats != NULL)
                {
                    pPktInfo->pOutStats->pPrevStats = NULL;
                }
            }

            pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
        }
    }
    else if (pStats->pNextStats != NULL)
    {
        pStats->pPrevStats->pNextStats = pStats->pNextStats;
    }
    else
    {
        pStats->pPrevStats->pNextStats = NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonStatsPopulateDataEntries                              */
/*                                                                           */
/* Description  : Populates data entries if the same data source is already  */
/*          present and Adds the inter-dependencies b/w other tables   */
/*                                                                           */
/* Input        : pStatsCtlParam                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonStatsPopulateDataEntries (tDsmonStatsCtl * pStatsCtlParam)
{
    tDsmonPktInfo      *pPktInfo = NULL;
    tDsmonStats        *pStats = NULL, *pNewStats = NULL;
    tPktHeader          PktHdr;
    UINT4               u4DsmonAggGroupIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonStatsPopulateDataEntries\r\n");

    MEMSET (&PktHdr, DSMON_INIT_VAL, sizeof (tPktHeader));

    /* Check for the presence of data source in pktinfo table */
    pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);

    while (pPktInfo != NULL)
    {
        if ((pPktInfo->PktHdr.u4IfIndex ==
             pStatsCtlParam->u4DsmonStatsDataSource)
            && (pPktInfo->PktHdr.u1DSCP < DSMON_MAX_AGG_PROFILE_DSCP))
        {
            u4DsmonAggGroupIndex =
                pStatsCtlParam->pAggCtl->au4DsmonAggProfile[pPktInfo->PktHdr.
                                                            u1DSCP];

            pNewStats =
                DsmonStatsAddDataEntry (pStatsCtlParam->u4DsmonStatsCtlIndex,
                                        u4DsmonAggGroupIndex);
            if (pNewStats == NULL)
            {
                pStatsCtlParam->u4DsmonStatsCtlDroppedFrames++;

                pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }
            pNewStats->pStatsCtl = pStatsCtlParam;
            pNewStats->u4PktRefCount++;

            pStats = pPktInfo->pOutStats;
            if (pStats == NULL)
            {
                pPktInfo->pOutStats = pNewStats;
            }
            else
            {
                while (pStats->pNextStats != NULL)
                {
                    pStats = pStats->pNextStats;
                }
                pStats->pNextStats = pNewStats;
                pNewStats->pPrevStats = pStats;
            }
        }

        pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
    }
    return;
}
