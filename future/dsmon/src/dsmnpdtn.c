/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *                                                                  *
 * Description: This file contains the functional routines         *
 *        for DSMON Pdist TopN module                         *
 *                                                                  *
 ********************************************************************/
#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNUpdateAggCtlChgs                             */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/*                tables entries into NOTREADY and cleanup all TopN report.  */
/*                If u1DsmonAggControlStatus is TRUE, update control tables  */
/*                and resume TopN report process                             */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistTopNUpdateAggCtlChgs (UINT1 u1DsmonAggControlStatus)
{
    tDsmonPdistCtl     *pPdistCtl = NULL;
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;
    UINT4               u4PdistTopNCtlIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonPdistTopNUpdateAggCtlChgs \r\n");

    switch (u1DsmonAggControlStatus)
    {
        case DSMON_AGGLOCK_FALSE:
            /* Get First PdistTopNCtl Entry */
            pPdistTopNCtl = DsmonPdistTopNGetNextCtlIndex (u4PdistTopNCtlIndex);

            while (pPdistTopNCtl != NULL)
            {
                u4PdistTopNCtlIndex = pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;
                if (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus == ACTIVE)
                {
                    pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus = NOT_READY;
                }

                /* Get Next PdistTopNCtl Entry */
                pPdistTopNCtl =
                    DsmonPdistTopNGetNextCtlIndex (u4PdistTopNCtlIndex);
            }

            /* Delete TopN Report Table */
            RBTreeDrain (DSMON_PDIST_TOPN_TABLE, DsmonUtlRBFreePdistTopN, 0);
            break;
        case DSMON_AGGLOCK_TRUE:
            /* Get First PdistTopNCtl Entry */
            pPdistTopNCtl = DsmonPdistTopNGetNextCtlIndex (u4PdistTopNCtlIndex);
            while (pPdistTopNCtl != NULL)
            {
                u4PdistTopNCtlIndex = pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;
                pPdistCtl =
                    DsmonPdistGetNextCtlIndex (pPdistTopNCtl->
                                               u4DsmonPdistTopNCtlPdistIndex);

                if ((pPdistCtl != NULL) &&
                    (pPdistCtl->u4DsmonPdistCtlRowStatus == ACTIVE))
                {
                    /* Set back RowStatus of PdistTopNCtl as ACTIVE */
                    pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus = ACTIVE;
                }
                pPdistTopNCtl =
                    DsmonPdistTopNGetNextCtlIndex (u4PdistTopNCtlIndex);
            }
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNNotifyPdistCtlChgs                           */
/*                                                                           */
/* Description  : Notifies RowStatus changes to TopN Table                   */
/*                                                                           */
/* Input        : u4DsmonPdistCtlIndex, u4DsmonPdistCtlStatus                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistTopNNotifyPdistCtlChgs (UINT4 u4DsmonPdistCtlIndex,
                                  UINT4 u4DsmonPdistCtlStatus)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;
    tDsmonPdistTopN    *pPdistTopN = NULL;
    UINT4               u4PdistTopNCtlIndex = DSMON_ZERO, u4PdistTopNIndex =
        DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonPdistTopNNotifyPdistCtlChgs \r\n");

    pPdistTopNCtl = DsmonPdistTopNGetNextCtlIndex (u4PdistTopNCtlIndex);

    while (pPdistTopNCtl != NULL)
    {
        u4PdistTopNCtlIndex = pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;
        if (pPdistTopNCtl->u4DsmonPdistTopNCtlPdistIndex ==
            u4DsmonPdistCtlIndex)
        {
            if (u4DsmonPdistCtlStatus != ACTIVE)
            {
                pPdistTopN = DsmonPdistTopNGetNextTopNIndex
                    (u4PdistTopNCtlIndex, u4PdistTopNIndex);

                while (pPdistTopN != NULL)
                {
                    if (pPdistTopN->u4DsmonPdistTopNCtlIndex !=
                        u4PdistTopNCtlIndex)
                    {
                        break;
                    }

                    u4PdistTopNIndex = pPdistTopN->u4DsmonPdistTopNIndex;

                    DsmonPdistTopNDelTopNEntry (pPdistTopN);

                    pPdistTopN = DsmonPdistTopNGetNextTopNIndex
                        (u4PdistTopNCtlIndex, u4PdistTopNIndex);
                }

                pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem =
                    pPdistTopNCtl->u4DsmonPdistTopNCtlDuration;
                pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus = NOT_READY;
            }
            else if ((u4DsmonPdistCtlStatus == ACTIVE) &&
                     (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus == NOT_READY))
            {
                pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus = ACTIVE;
                pPdistTopNCtl->u4DsmonPdistTopNCtlStartTime =
                    (UINT4) OsixGetSysUpTime ();
            }
        }
        pPdistTopNCtl = DsmonPdistTopNGetNextCtlIndex (u4PdistTopNCtlIndex);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNGetNextCtlIndex                              */
/*                                                                           */
/* Description  : Get First / Next Pdist TopN Ctl Index                      */
/*                                                                           */
/* Input        : u4DsmonPdistTopNCtlIndex                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistTopNCtl / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistTopNCtl *
DsmonPdistTopNGetNextCtlIndex (UINT4 u4DsmonPdistTopNCtlIndex)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL, PdistTopNCtl;

    MEMSET (&PdistTopNCtl, DSMON_INIT_VAL, sizeof (tDsmonPdistTopNCtl));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPdistTopNGetNextCtlIndex \r\n");

    PdistTopNCtl.u4DsmonPdistTopNCtlIndex = u4DsmonPdistTopNCtlIndex;
    pPdistTopNCtl = (tDsmonPdistTopNCtl *) RBTreeGetNext
        (DSMON_PDIST_TOPNCTL_TABLE, (tRBElem *) & PdistTopNCtl, NULL);

    return pPdistTopNCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNAddCtlEntry                                  */
/*                                                                           */
/* Description  : Allocates memory and Adds Pdist TopN Control Entry into    */
/*                dsmonPdistTopNCtlRBTree                                    */
/*                                                                           */
/* Input        : u4DsmonPdistTopNControlIndex                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistCtl / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistTopNCtl *
DsmonPdistTopNAddCtlEntry (UINT4 u4DsmonPdistTopNControlIndex)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPdistTopNAddCtlEntry \r\n");

    if ((pPdistTopNCtl = (tDsmonPdistTopNCtl *)
         (MemAllocMemBlk (DSMON_PDIST_TOPNCTL_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Protocol distribution TopN Ctl.\r\n");
        return NULL;
    }

    MEMSET (pPdistTopNCtl, DSMON_ZERO, sizeof (tDsmonPdistTopNCtl));

    pPdistTopNCtl->u4DsmonPdistTopNCtlIndex = u4DsmonPdistTopNControlIndex;

    pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem = DSMON_TIMEREM_DEFVAL;

    pPdistTopNCtl->u4DsmonPdistTopNCtlGenReports = DSMON_ZERO;

    pPdistTopNCtl->u4DsmonPdistTopNCtlDuration = DSMON_TIMEREM_DEFVAL;

    pPdistTopNCtl->u4DsmonPdistTopNCtlReqSize = DSMON_REQSIZE_DEFVAL;

    pPdistTopNCtl->u4DsmonPdistTopNCtlGranSize = DSMON_MAX_TOPN_ENTRY;

    pPdistTopNCtl->u4DsmonPdistTopNCtlStartTime = DSMON_ZERO;

    pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus = NOT_READY;

    if (RBTreeAdd (DSMON_PDIST_TOPNCTL_TABLE, (tRBElem *) pPdistTopNCtl)
        == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonPdistTopNAddCtlEntry Entry is added\r\n");
        return pPdistTopNCtl;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonPdistTopNAddCtlEntry entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_PDIST_TOPNCTL_POOL, (UINT1 *) pPdistTopNCtl);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNDelCtlEntry                                  */
/*                                                                           */
/* Description  : Releases memory and Removes Pdist TopN Control Entry from  */
/*                dsmonPdistTopNCtlRBTree                                    */
/*                                                                           */
/* Input        : pPdistTopNCtl                                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonPdistTopNDelCtlEntry (tDsmonPdistTopNCtl * pPdistTopNCtl)
{
    tDsmonPdistTopN    *pPdistTopN = NULL;
    UINT4               u4PdistTopNCtlIndex = 0, u4PdistTopNIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPdistTopNDelCtlEntry \r\n");
    u4PdistTopNCtlIndex = pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;

    pPdistTopN = (tDsmonPdistTopN *) DsmonPdistTopNGetNextTopNIndex
        (u4PdistTopNCtlIndex, u4PdistTopNIndex);

    while (pPdistTopN != NULL)
    {
        if (pPdistTopN->u4DsmonPdistTopNCtlIndex != u4PdistTopNCtlIndex)
        {
            break;
        }
        u4PdistTopNIndex = pPdistTopN->u4DsmonPdistTopNIndex;

        DsmonPdistTopNDelTopNEntry (pPdistTopN);

        pPdistTopN = (tDsmonPdistTopN *) DsmonPdistTopNGetNextTopNIndex
            (u4PdistTopNCtlIndex, u4PdistTopNIndex);
    }

    if (RBTreeRemove (DSMON_PDIST_TOPNCTL_TABLE, pPdistTopNCtl) == RB_FAILURE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonPdistTopNDelCtlEntry entry is added\r\n");
        return OSIX_FAILURE;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonPdistTopNDelCtlEntry entry addition failed\r\n");
    MemReleaseMemBlock (DSMON_PDIST_TOPNCTL_POOL, (UINT1 *) pPdistTopNCtl);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNGetCtlEntry                                  */
/*                                                                           */
/* Description  : Get Pdist TopN Control Entry for the given control index   */
/*                                                                           */
/* Input        : u4DsmonPdistTopNControlIndex                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistTopNCtl / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistTopNCtl *
DsmonPdistTopNGetCtlEntry (UINT4 u4DsmonPdistTopNControlIndex)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL, PdistTopNCtl;

    MEMSET (&PdistTopNCtl, DSMON_INIT_VAL, sizeof (tDsmonPdistTopNCtl));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPdistTopNGetCtlEntry\r\n");

    PdistTopNCtl.u4DsmonPdistTopNCtlIndex = u4DsmonPdistTopNControlIndex;

    pPdistTopNCtl = (tDsmonPdistTopNCtl *) RBTreeGet (DSMON_PDIST_TOPNCTL_TABLE,
                                                      (tRBElem *) &
                                                      PdistTopNCtl);

    return pPdistTopNCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNGetNextTopNIndex                             */
/*                                                                           */
/* Description  : Get First / Next PdistTopN Index                           */
/*                                                                           */
/* Input        : u4DsmonPdistTopNCtlIndex                                   */
/*                u4DsmonPdistTopNIndex                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistTopN / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistTopN *
DsmonPdistTopNGetNextTopNIndex (UINT4 u4DsmonPdistTopNCtlIndex,
                                UINT4 u4DsmonPdistTopNIndex)
{
    tDsmonPdistTopN    *pPdistTopN = NULL, PdistTopN;

    MEMSET (&PdistTopN, DSMON_INIT_VAL, sizeof (tDsmonPdistTopN));

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonPdistTopNGetNextTopNIndex \r\n");

    if ((u4DsmonPdistTopNCtlIndex != DSMON_ZERO) &&
        (u4DsmonPdistTopNIndex == DSMON_ZERO))
    {
        pPdistTopN =
            (tDsmonPdistTopN *) RBTreeGetFirst (DSMON_PDIST_TOPN_TABLE);

        while (pPdistTopN != NULL)
        {
            if (pPdistTopN->u4DsmonPdistTopNCtlIndex ==
                u4DsmonPdistTopNCtlIndex)
            {
                break;
            }
            MEMCPY (&PdistTopN, pPdistTopN, sizeof (tDsmonPdistTopN));

            pPdistTopN =
                (tDsmonPdistTopN *) RBTreeGetNext (DSMON_PDIST_TOPN_TABLE,
                                                   (tRBElem *) & PdistTopN,
                                                   NULL);
        }
    }
    else
    {
        PdistTopN.u4DsmonPdistTopNCtlIndex = u4DsmonPdistTopNCtlIndex;
        PdistTopN.u4DsmonPdistTopNIndex = u4DsmonPdistTopNIndex;

        pPdistTopN = (tDsmonPdistTopN *) RBTreeGetNext (DSMON_PDIST_TOPN_TABLE,
                                                        (tRBElem *) & PdistTopN,
                                                        NULL);
    }
    return pPdistTopN;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNAddTopNEntry                                 */
/*                                                                           */
/* Description  : Allocates memory and Adds Pdist TopN Entry into            */
/*                dsmonPdistTopNRBTree                                       */
/*                                                                           */
/* Input        : pPdistTopN                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdist (SD/DS) / NULL Pointer                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistTopN *
DsmonPdistTopNAddTopNEntry (UINT4 u4DsmonPdistTopNIndex,
                            tDsmonPdistTopN * pPdistTopN)
{
    tDsmonPdistTopN    *pTopNEntry = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPdistTopNAddTopNEntry \r\n");

    if ((pTopNEntry = (tDsmonPdistTopN *)
         (MemAllocMemBlk (DSMON_PDIST_TOPN_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Protocol distribution TopN\r\n");
        return NULL;
    }

    MEMSET (pTopNEntry, DSMON_INIT_VAL, sizeof (tDsmonPdistTopN));
    MEMCPY (pTopNEntry, pPdistTopN, sizeof (tDsmonPdistTopN));
    pTopNEntry->u4DsmonPdistTopNIndex = u4DsmonPdistTopNIndex;

    if (RBTreeAdd (DSMON_PDIST_TOPN_TABLE, (tRBElem *) pTopNEntry)
        == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonPdistTopNAddTopNEntry entry is added\r\n");
        return pTopNEntry;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonPdistTopNAddTopNEntry entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_PDIST_TOPN_POOL, (UINT1 *) pTopNEntry);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNDelTopNEntry                                 */
/*                                                                           */
/* Description  : Releases memory and Removes Pdist Entry from               */
/*                dsmonPdistTopNRBTree                                       */
/*                                                                           */
/* Input        :  pPdistTopN                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonPdistTopNDelTopNEntry (tDsmonPdistTopN * pPdistTopN)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPdistTopNDelTopNEntry\r\n");

    /* Remove PdistTopN. node from dsmonPdistTopNRBTree */
    RBTreeRem (DSMON_PDIST_TOPN_TABLE, (tRBElem *) pPdistTopN);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_PDIST_TOPN_POOL,
                            (UINT1 *) pPdistTopN) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Protocol distribution TopN.\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNGetTopNEntry                                 */
/*                                                                           */
/* Description  : Get Pdist TopN Entry for the given control index           */
/*                                                                           */
/* Input        : u4DsmonPdistTopNIndex                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPdistTopN / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPdistTopN *
DsmonPdistTopNGetTopNEntry (UINT4 u4DsmonPdistTopNControlIndex,
                            UINT4 u4DsmonPdistTopNIndex)
{
    tDsmonPdistTopN    *pPdistTopN = NULL, PdistTopN;

    MEMSET (&PdistTopN, DSMON_INIT_VAL, sizeof (tDsmonPdistTopN));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPdistTopNGetTopNEntry\r\n");

    PdistTopN.u4DsmonPdistTopNCtlIndex = u4DsmonPdistTopNControlIndex;
    PdistTopN.u4DsmonPdistTopNIndex = u4DsmonPdistTopNIndex;

    pPdistTopN = (tDsmonPdistTopN *) RBTreeGet (DSMON_PDIST_TOPN_TABLE,
                                                (tRBElem *) & PdistTopN);
    return pPdistTopN;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPdistTopNDelTopNReport                                  */
/*                                                                           */
/* Description  : Delete the  TopN Entries if they are already present in    */
/*                dsmonPdistTopNRBTree                                       */
/*                                                                           */
/* Input        : u4PdistTopNCtlIndex                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPdistTopNDelTopNReport (UINT4 u4DsmonPdistTopNCtlIndex)
{
    tDsmonPdistTopN    *pTopNEntry = NULL;
    UINT4               u4PdistTopNIndex = DSMON_ZERO;
    pTopNEntry = DsmonPdistTopNGetNextTopNIndex (u4DsmonPdistTopNCtlIndex,
                                                 u4PdistTopNIndex);

    while (pTopNEntry != NULL)
    {
        u4PdistTopNIndex = pTopNEntry->u4DsmonPdistTopNIndex;

        if (u4DsmonPdistTopNCtlIndex != pTopNEntry->u4DsmonPdistTopNCtlIndex)
        {
            break;
        }
        DsmonPdistTopNDelTopNEntry (pTopNEntry);

        pTopNEntry = DsmonPdistTopNGetNextTopNIndex (u4DsmonPdistTopNCtlIndex,
                                                     u4PdistTopNIndex);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetSamplesFromPdistTable                                   */
/*                                                                           */
/* Description  : Get the initial samples from pdist table, when the time    */
/*                remaining and duration values are same                     */
/*                                                                           */
/* Input        : u4PdistCtlIndex                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GetSamplesFromPdistTable (UINT4 u4PdistCtlIndex)
{
    tDsmonPdistStats   *pPdistStatsNode = NULL;
    UINT4               u4PdistGrpIndex = 0, u4PdistProtoIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY GetSamplesFromPdistTable \n");

    pPdistStatsNode = DsmonPdistGetNextDataIndex (u4PdistCtlIndex,
                                                  u4PdistGrpIndex,
                                                  u4PdistProtoIndex);

    while (pPdistStatsNode != NULL)
    {
        if (pPdistStatsNode->u4DsmonPdistCtlIndex != u4PdistCtlIndex)
        {
            break;
        }
        MEMCPY (&pPdistStatsNode->DsmonPrevSample,
                &pPdistStatsNode->DsmonCurSample, sizeof (tDsmonPdistSample));

        pPdistStatsNode =
            DsmonPdistGetNextDataIndex (pPdistStatsNode->u4DsmonPdistCtlIndex,
                                        pPdistStatsNode->
                                        u4DsmonPdistAggGroupIndex,
                                        pPdistStatsNode->
                                        u4DsmonPdistProtDirLocalIndex);

    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : FillPdistSampleTopNRate                                    */
/*                                                                           */
/* Description  : Fill the topn rate values                                  */
/*                                                                           */
/* Input        : pSampleTopN,pPdistStatsNode,u4Count                        */
/*                u4Value,pPdistTopNCtl                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
FillPdistSampleTopNRate (tDsmonPdistTopN * pSampleTopN, UINT4 u4Count,
                         tDsmonPdistStats * pPdistStatsNode,
                         UINT4 u4Value, tDsmonPdistTopNCtl * pPdistTopNCtl)
{

    UINT4               u4Index = DSMON_ZERO;
    UINT4               u4MinValue = DSMON_ZERO;
    UINT4               u4MinIndex = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY FillPdistSampleTopNRate\r\n");

    if (u4Count < pPdistTopNCtl->u4DsmonPdistTopNCtlGranSize)

    {
        pSampleTopN[u4Count].u4DsmonPdistTopNRate = u4Value;

        UINT8_RESET (pSampleTopN[u4Count].u8DsmonPdistTopNHCRate);

        pSampleTopN[u4Count].u4DsmonPdistTopNPDLocalIndex =
            pPdistStatsNode->u4DsmonPdistProtDirLocalIndex;

        pSampleTopN[u4Count].u4DsmonPdistTopNAggGroupIndex =
            pPdistStatsNode->u4DsmonPdistAggGroupIndex;

        pSampleTopN[u4Count].u4DsmonPdistTopNCtlIndex =
            pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;
    }
    else
    {
        /* find the least rate in the entries,
           and replace that with the new entry -- 
           this approach is chosen inorder to reduce the memory allocated, 
           though the performance is not high */

        u4MinValue = pSampleTopN[u4MinIndex].u4DsmonPdistTopNRate;

        while (u4Index < pPdistTopNCtl->u4DsmonPdistTopNCtlGranSize)
        {
            if (u4MinValue > pSampleTopN[u4Index].u4DsmonPdistTopNRate)
            {
                u4MinValue = pSampleTopN[u4Index].u4DsmonPdistTopNRate;
                u4MinIndex = u4Index;
            }
            u4Index++;
        }

        if (u4MinValue <= u4Value)

        {
            pSampleTopN[u4MinIndex].u4DsmonPdistTopNRate = u4Value;

            UINT8_RESET (pSampleTopN[u4MinIndex].u8DsmonPdistTopNHCRate);

            pSampleTopN[u4MinIndex].u4DsmonPdistTopNPDLocalIndex =
                pPdistStatsNode->u4DsmonPdistProtDirLocalIndex;

            pSampleTopN[u4MinIndex].u4DsmonPdistTopNAggGroupIndex =
                pPdistStatsNode->u4DsmonPdistAggGroupIndex;

            pSampleTopN[u4MinIndex].u4DsmonPdistTopNCtlIndex =
                pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;
        }

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FillPdistSampleTopNHCRate                                  */
/*                                                                           */
/* Description  : Fill the topn high capacity rate values                    */
/*                                                                           */
/* Input        : pSampleTopN,pPdistStatsNode                                */
/*                u4MsnValue,u4LsnValue,u4PdistTopNCtlIndex                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
FillPdistSampleTopNHCRate (tDsmonPdistTopN * pSampleTopN, UINT4 u4Count,
                           tDsmonPdistStats * pPdistStatsNode,
                           UINT4 u4MsnValue, UINT4 u4LsnValue,
                           tDsmonPdistTopNCtl * pPdistTopNCtl)
{
    UINT4               u4Index = DSMON_ZERO;
    tUint8              u8MinValue;
    tUint8              u8NewValue;
    UINT4               u4MinIndex = DSMON_ZERO;
    INT4                i4Value = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY FillPdistSampleTopNHCRate \r\n");

    if (u4Count < pPdistTopNCtl->u4DsmonPdistTopNCtlGranSize)
    {
        pSampleTopN[u4Count].u8DsmonPdistTopNHCRate.u4HiWord = u4MsnValue;
        pSampleTopN[u4Count].u8DsmonPdistTopNHCRate.u4LoWord = u4LsnValue;

        /* contains the least significant 32 bits */

        pSampleTopN[u4Count].u4DsmonPdistTopNRate = u4LsnValue;

        pSampleTopN[u4Count].u4DsmonPdistTopNPDLocalIndex =
            pPdistStatsNode->u4DsmonPdistProtDirLocalIndex;

        pSampleTopN[u4Count].u4DsmonPdistTopNAggGroupIndex =
            pPdistStatsNode->u4DsmonPdistAggGroupIndex;

        pSampleTopN[u4Count].u4DsmonPdistTopNCtlIndex =
            pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;
    }
    else
    {
        u8MinValue.u4HiWord =
            pSampleTopN[u4MinIndex].u8DsmonPdistTopNHCRate.u4HiWord;

        u8MinValue.u4LoWord =
            pSampleTopN[u4MinIndex].u8DsmonPdistTopNHCRate.u4LoWord;

        /* find the least rate in the entries, 
           and replace that with the new entry --
           this approach is chosen inorder to reduce the memory allocated,
           though the performance is not high */

        while (u4Index < pPdistTopNCtl->u4DsmonPdistTopNCtlGranSize)
        {
            UINT8_CMP (pSampleTopN[u4Index].u8DsmonPdistTopNHCRate,
                       u8MinValue, i4Value);

            if (i4Value == -1)
            {
                u8MinValue.u4HiWord =
                    pSampleTopN[u4Index].u8DsmonPdistTopNHCRate.u4HiWord;
                u8MinValue.u4LoWord =
                    pSampleTopN[u4Index].u8DsmonPdistTopNHCRate.u4LoWord;
                u4MinIndex = u4Index;
            }
            u4Index++;
        }

        u8NewValue.u4HiWord = u4MsnValue;

        u8NewValue.u4LoWord = u4LsnValue;

        UINT8_CMP (u8MinValue, u8NewValue, i4Value);

        if ((i4Value == -1) || (i4Value == 0))
        {

            pSampleTopN[u4MinIndex].u8DsmonPdistTopNHCRate.u4HiWord =
                u4MsnValue;

            pSampleTopN[u4MinIndex].u8DsmonPdistTopNHCRate.u4LoWord =
                u4LsnValue;

            /* contains the least significant 32 bits */

            pSampleTopN[u4MinIndex].u4DsmonPdistTopNRate = u4LsnValue;

            pSampleTopN[u4MinIndex].u4DsmonPdistTopNPDLocalIndex =
                pPdistStatsNode->u4DsmonPdistProtDirLocalIndex;

            pSampleTopN[u4MinIndex].u4DsmonPdistTopNAggGroupIndex =
                pPdistStatsNode->u4DsmonPdistAggGroupIndex;

            pSampleTopN[u4MinIndex].u4DsmonPdistTopNCtlIndex =
                pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetPdistTopNEntries                                        */
/*                                                                           */
/* Description  : Get the current samples from Pdist table                   */
/*                                                                           */
/* Input        : pPdistTopNCtl,pSampleTopN                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u4Count                                                    */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
GetPdistTopNEntries (tDsmonPdistTopNCtl * pPdistTopNCtl,
                     tDsmonPdistTopN * pSampleTopN)
{
    UINT4               u4Count = DSMON_ZERO;
    UINT4               u4Delta = DSMON_ZERO;
    UINT4               u4PdistGrpIndex = 0, u4PdistProtoIndex = 0;

    tDsmonPdistStats   *pPdistStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY GetPdistTopNEntries \n");

    pPdistStatsNode = DsmonPdistGetNextDataIndex
        (pPdistTopNCtl->u4DsmonPdistTopNCtlPdistIndex, u4PdistGrpIndex,
         u4PdistProtoIndex);

    while (pPdistStatsNode != NULL)
    {
        if ((pPdistStatsNode->u4DsmonPdistCtlIndex) !=
            (pPdistTopNCtl->u4DsmonPdistTopNCtlPdistIndex))
        {
            break;
        }

        switch (pPdistTopNCtl->u4DsmonPdistTopNCtlRateBase)
        {
            case pdistTopNPkts:

                if ((pPdistStatsNode->DsmonPrevSample.u4DsmonPdistStatsPkts)
                    != DSMON_ZERO)
                {

                    u4Delta =
                        pPdistStatsNode->DsmonCurSample.u4DsmonPdistStatsPkts -
                        pPdistStatsNode->DsmonPrevSample.u4DsmonPdistStatsPkts;

                    if (u4Delta > DSMON_ZERO)
                    {
                        FillPdistSampleTopNRate (pSampleTopN, u4Count,
                                                 pPdistStatsNode, u4Delta,
                                                 pPdistTopNCtl);

                        u4Count++;
                    }
                }

                break;

            case pdistTopNOctets:

                if ((pPdistStatsNode->DsmonPrevSample.u4DsmonPdistStatsOctets)
                    != DSMON_ZERO)
                {

                    u4Delta =
                        pPdistStatsNode->DsmonCurSample.
                        u4DsmonPdistStatsOctets -
                        pPdistStatsNode->DsmonPrevSample.
                        u4DsmonPdistStatsOctets;

                    if (u4Delta > DSMON_ZERO)
                    {
                        FillPdistSampleTopNRate (pSampleTopN, u4Count,
                                                 pPdistStatsNode, u4Delta,
                                                 pPdistTopNCtl);

                        u4Count++;
                    }
                }
                break;

            case pdistTopNHCPkts:

                if ((pPdistStatsNode->DsmonPrevSample.u8DsmonPdistStatsHCPkts.
                     u4HiWord != DSMON_ZERO)
                    && (pPdistStatsNode->DsmonPrevSample.
                        u8DsmonPdistStatsHCPkts.u4LoWord != DSMON_ZERO))
                {
                    UINT8_SUB (pPdistStatsNode->DsmonCurSample.
                               u8DsmonPdistStatsHCPkts,
                               pPdistStatsNode->DsmonPrevSample.
                               u8DsmonPdistStatsHCPkts,
                               pPdistStatsNode->DsmonPrevSample.
                               u8DsmonPdistStatsHCPkts);

                    if ((pPdistStatsNode->DsmonPrevSample.
                         u8DsmonPdistStatsHCPkts.u4HiWord > DSMON_ZERO)
                        || (pPdistStatsNode->DsmonPrevSample.
                            u8DsmonPdistStatsHCPkts.u4LoWord > DSMON_ZERO))
                    {
                        FillPdistSampleTopNHCRate (pSampleTopN, u4Count,
                                                   pPdistStatsNode,
                                                   pPdistStatsNode->
                                                   DsmonPrevSample.
                                                   u8DsmonPdistStatsHCPkts.
                                                   u4HiWord,
                                                   pPdistStatsNode->
                                                   DsmonPrevSample.
                                                   u8DsmonPdistStatsHCPkts.
                                                   u4LoWord, pPdistTopNCtl);

                        u4Count++;
                    }

                }
                break;

            case pdistTopNHCOctets:

                if ((pPdistStatsNode->DsmonPrevSample.u8DsmonPdistStatsHCOctets.
                     u4HiWord != DSMON_ZERO)
                    && (pPdistStatsNode->DsmonPrevSample.
                        u8DsmonPdistStatsHCOctets.u4LoWord != DSMON_ZERO))
                {
                    UINT8_SUB (pPdistStatsNode->DsmonCurSample.
                               u8DsmonPdistStatsHCOctets,
                               pPdistStatsNode->DsmonPrevSample.
                               u8DsmonPdistStatsHCOctets,
                               pPdistStatsNode->DsmonPrevSample.
                               u8DsmonPdistStatsHCOctets);

                    if ((pPdistStatsNode->DsmonPrevSample.
                         u8DsmonPdistStatsHCOctets.u4HiWord > DSMON_ZERO)
                        || (pPdistStatsNode->DsmonPrevSample.
                            u8DsmonPdistStatsHCOctets.u4LoWord > DSMON_ZERO))
                    {
                        FillPdistSampleTopNHCRate (pSampleTopN, u4Count,
                                                   pPdistStatsNode,
                                                   pPdistStatsNode->
                                                   DsmonPrevSample.
                                                   u8DsmonPdistStatsHCOctets.
                                                   u4HiWord,
                                                   pPdistStatsNode->
                                                   DsmonPrevSample.
                                                   u8DsmonPdistStatsHCOctets.
                                                   u4LoWord, pPdistTopNCtl);

                        u4Count++;
                    }
                }

                break;

            default:
                break;
        }                        /* end of switch */

        pPdistStatsNode =
            DsmonPdistGetNextDataIndex (pPdistStatsNode->u4DsmonPdistCtlIndex,
                                        pPdistStatsNode->
                                        u4DsmonPdistAggGroupIndex,
                                        pPdistStatsNode->
                                        u4DsmonPdistProtDirLocalIndex);

    }                            /* end of while */

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT GetPdistTopNEntries \n");

    return u4Count;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SetSortedPdistTopNTable                                    */
/*                                                                           */
/* Description  : Generates topn report of size, granted size                */
/*                                                                           */
/* Input        : pSampleTopN,u4Count,u4GrantedSize                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
SetSortedPdistTopNTable (tDsmonPdistTopN * pSampleTopN, UINT4 u4Count,
                         UINT4 u4GrantedSize)
{
    UINT4               u4Pass = DSMON_ZERO;
    UINT4               u4EntryCount = DSMON_ZERO;
    tDsmonPdistTopN    *pPdistTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY SetSortedPdistTopNTable \n");

    /* add the topN entries in the Pdist TopN table */

    u4EntryCount = (u4Count < u4GrantedSize) ? u4Count : u4GrantedSize;

    u4Count = u4EntryCount - 1;

    for (u4Pass = DSMON_ONE; u4Pass <= u4EntryCount; u4Pass++)
    {
        pPdistTopN = DsmonPdistTopNAddTopNEntry (u4Pass, &pSampleTopN[u4Count]);

        if (pPdistTopN == NULL)
        {
            DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                       " Memory Allocation - RBTree Add Failed - \
            Protocol distribution TopN Table \n");

            return OSIX_FAILURE;
        }

        u4Count--;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT SetSortedPdistTopNTable \n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : ComparePdistTopNRate                                       */
/*                                                                           */
/* Description  : Compare function for qsort based on TopNRate               */
/*                                                                           */
/* Input        : pParam1, pParam2                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 - if pParam1 is less than pParam2                       */
/*                1  - if pParam1 is greater than pParam2                    */
/*                0  - if pParam1 and pParam2 are equal                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
ComparePdistTopNRate (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tDsmonPdistTopN *pSample1 = (CONST tDsmonPdistTopN *) pParam1;
    CONST tDsmonPdistTopN *pSample2 = (CONST tDsmonPdistTopN *) pParam2;

    if (pSample1->u4DsmonPdistTopNRate < pSample2->u4DsmonPdistTopNRate)
    {
        return -1;
    }
    if (pSample1->u4DsmonPdistTopNRate > pSample2->u4DsmonPdistTopNRate)
    {
        return 1;
    }
    if (pSample1->u4DsmonPdistTopNRate == pSample2->u4DsmonPdistTopNRate)
    {
        return 0;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : ComparePdistTopNHCRate                                     */
/*                                                                           */
/* Description  : Compare function for qsort based on TopNHCRate             */
/*                                                                           */
/* Input        : pParam1, pParam2                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 - if pParam1 is less than pParam2                       */
/*                1  - if pParam1 is greater than pParam2                    */
/*                0  - if pParam1 and pParam2 are equal                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
ComparePdistTopNHCRate (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tDsmonPdistTopN *pSample1 = (CONST tDsmonPdistTopN *) pParam1;
    CONST tDsmonPdistTopN *pSample2 = (CONST tDsmonPdistTopN *) pParam2;
    INT4                i4Value = DSMON_ZERO;

    UINT8_CMP (pSample1->u8DsmonPdistTopNHCRate,
               pSample2->u8DsmonPdistTopNHCRate, i4Value);

    return i4Value;
}

/***********************************************************************
 * Function           : DsmonPdistTopNUpdateTable                      *
 *                                                                     *
 * Input (s)          : None.                                          *
 *                                                                     *
 * Output (s)         : None.                                          *
 *                                                                     *
 * Returns            : None.                                          *
 *                                                                     *
 * Global Variables                                                    *
 *             Used   :                                                *
 *                                                                     *
 * Side effects       : None                                           *
 *                                                                     *
 * Action :                                                            *
 *                                                                     *
 **********************************************************************/

VOID                DsmonPdistTopNUpdateTable
ARG_LIST ((VOID))
{
    UINT4               u4Count = DSMON_ZERO, u4PdistTopNCtlIndex = DSMON_ZERO;
    UINT4               u4EntryCount = DSMON_ZERO;
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;
    tDsmonPdistTopN     SampleTopN[DSMON_MAX_TOPN_ENTRY];

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPdistTopNUpdateTable \n");

    MEMSET (&SampleTopN, 0, sizeof (SampleTopN));

    pPdistTopNCtl = DsmonPdistTopNGetNextCtlIndex (u4PdistTopNCtlIndex);

    while (pPdistTopNCtl != NULL)
    {
        u4PdistTopNCtlIndex = pPdistTopNCtl->u4DsmonPdistTopNCtlIndex;
        if (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus == ACTIVE)
        {
            if (pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem > DSMON_ZERO)
            {
                if (pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem ==
                    pPdistTopNCtl->u4DsmonPdistTopNCtlDuration)
                {
                    GetSamplesFromPdistTable (pPdistTopNCtl->
                                              u4DsmonPdistTopNCtlPdistIndex);
                }
                else
                {
                    if (pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem == DSMON_ONE)
                    {

                        /* timerem is 1, so take the second sample */
                        /* allocate memory */

                        u4Count = GetPdistTopNEntries (pPdistTopNCtl,
                                                       SampleTopN);

                        if (u4Count > 0)
                        {
                            /* Has to free TopN Entries if they were already
                             *  present in dsmonPdistTopNRBTree */
                            DsmonPdistTopNDelTopNReport (pPdistTopNCtl->
                                                         u4DsmonPdistTopNCtlIndex);

                            u4EntryCount =
                                (u4Count <
                                 pPdistTopNCtl->
                                 u4DsmonPdistTopNCtlGranSize) ? u4Count :
                                pPdistTopNCtl->u4DsmonPdistTopNCtlGranSize;

                            switch (pPdistTopNCtl->u4DsmonPdistTopNCtlRateBase)
                            {
                                case pdistTopNPkts:
                                case pdistTopNOctets:

                                    qsort (SampleTopN,
                                           u4EntryCount,
                                           sizeof (tDsmonPdistTopN),
                                           (CONST VOID *) ComparePdistTopNRate);

                                    break;

                                case pdistTopNHCPkts:
                                case pdistTopNHCOctets:

                                    qsort (SampleTopN,
                                           pPdistTopNCtl->
                                           u4DsmonPdistTopNCtlGranSize,
                                           sizeof (tDsmonPdistTopN),
                                           (CONST VOID *)
                                           ComparePdistTopNHCRate);
                            }

                            if (SetSortedPdistTopNTable (SampleTopN, u4Count,
                                                         pPdistTopNCtl->
                                                         u4DsmonPdistTopNCtlGranSize)
                                == OSIX_FAILURE)
                            {
                                /*  TopN Report generation failed */
                                DSMON_TRC (DSMON_DEBUG_TRC,
                                           "OSIX Failure: TopN Report Generation failed \n");
                            }
                            else
                            {
                                pPdistTopNCtl->u4DsmonPdistTopNCtlGenReports++;
                            }
                        }

                        /* start another collection */

                        pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem =
                            pPdistTopNCtl->u4DsmonPdistTopNCtlDuration + 1;
                        pPdistTopNCtl->u4DsmonPdistTopNCtlStartTime =
                            OsixGetSysUpTime ();
                    }
                }
                pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem--;
            }
        }

        pPdistTopNCtl = DsmonPdistTopNGetNextCtlIndex (u4PdistTopNCtlIndex);

    }                            /* End of while */

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonPdistTopNUpdateTable \n");
}
