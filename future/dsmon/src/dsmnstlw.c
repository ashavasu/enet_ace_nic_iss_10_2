/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *                                                                  *
 * $Id: dsmnstlw.c,v 1.5 2012/12/13 14:25:10 siva Exp $           *
 * 
 * Description: This file contains the low level nmh routines         *
 *        for DSMON Stats module                            *
 *                                                                  *
 ********************************************************************/
#include "dsmninc.h"

/* LOW LEVEL Routines for Table : DsmonStatsControlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonStatsControlTable
 Input       :  The Indices
                DsmonStatsControlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonStatsControlTable (INT4 i4DsmonStatsControlIndex)
{
    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonStatsControlTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON  Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonStatsControlIndex < DSMON_ONE) ||
        (i4DsmonStatsControlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: StatsControlIndex out of range \n");

        return SNMP_FAILURE;
    }

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT,
               "FUNC:EXIT nmhValidateIndexInstanceDsmonStatsControlTable \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonStatsControlTable
 Input       :  The Indices
                DsmonStatsControlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonStatsControlTable (INT4 *pi4DsmonStatsControlIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetFirstIndexDsmonStatsControlTable \n");

    return nmhGetNextIndexDsmonStatsControlTable (DSMON_ZERO,
                                                  pi4DsmonStatsControlIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonStatsControlTable
 Input       :  The Indices
                DsmonStatsControlIndex
                nextDsmonStatsControlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonStatsControlTable (INT4 i4DsmonStatsControlIndex,
                                       INT4 *pi4NextDsmonStatsControlIndex)
{
    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetNextIndexDsmonStatsControlTable \n");

    /* Get Stats Control Node corresponding to indices next to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetNextCtlIndex (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    /* Stats Control entry corresponding to indices next to StatsControlIndex exists */

    *pi4NextDsmonStatsControlIndex =
        (INT4) (pStatsCtlNode->u4DsmonStatsCtlIndex);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonStatsControlDataSource
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                        retValDsmonStatsControlDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsControlDataSource (INT4 i4DsmonStatsControlIndex,
                                   tSNMP_OID_TYPE *
                                   pRetValDsmonStatsControlDataSource)
{
    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonStatsControlDataSource \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_GET_DATA_SOURCE_OID (pRetValDsmonStatsControlDataSource,
                               pStatsCtlNode->u4DsmonStatsDataSource);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsControlAggProfile
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                retValDsmonStatsControlAggProfile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsControlAggProfile (INT4 i4DsmonStatsControlIndex,
                                   INT4 *pi4RetValDsmonStatsControlAggProfile)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonStatsControlAggProfile \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonStatsControlAggProfile =
        (INT4) (pStatsCtlNode->u4DsmonStatsCtlProfileIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsControlDroppedFrames
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                retValDsmonStatsControlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsControlDroppedFrames (INT4 i4DsmonStatsControlIndex,
                                      UINT4
                                      *pu4RetValDsmonStatsControlDroppedFrames)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonStatsControlDroppedFrames \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonStatsControlDroppedFrames =
        pStatsCtlNode->u4DsmonStatsCtlDroppedFrames;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsControlCreateTime
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                retValDsmonStatsControlCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsControlCreateTime (INT4 i4DsmonStatsControlIndex,
                                   UINT4 *pu4RetValDsmonStatsControlCreateTime)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonStatsControlCreateTime \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonStatsControlCreateTime =
        pStatsCtlNode->u4DsmonStatsCtlCreateTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsControlOwner
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                retValDsmonStatsControlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsControlOwner (INT4 i4DsmonStatsControlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValDsmonStatsControlOwner)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsControlOwner \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDsmonStatsControlOwner->pu1_OctetList,
            pStatsCtlNode->au1DsmonStatsCtlOwner, DSMON_MAX_OWNER_LEN);

    pRetValDsmonStatsControlOwner->i4_Length =
        STRLEN (pStatsCtlNode->au1DsmonStatsCtlOwner);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonStatsControlStatus
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                retValDsmonStatsControlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsControlStatus (INT4 i4DsmonStatsControlIndex,
                               INT4 *pi4RetValDsmonStatsControlStatus)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsControlStatus \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonStatsControlStatus =
        (INT4) (pStatsCtlNode->u4DsmonStatsCtlRowStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDsmonStatsControlDataSource
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                setValDsmonStatsControlDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonStatsControlDataSource (INT4 i4DsmonStatsControlIndex,
                                   tSNMP_OID_TYPE *
                                   pSetValDsmonStatsControlDataSource)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonStatsControlDataSource \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_SET_DATA_SOURCE_OID (pSetValDsmonStatsControlDataSource,
                               &(pStatsCtlNode->u4DsmonStatsDataSource));

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonStatsControlAggProfile
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                setValDsmonStatsControlAggProfile
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonStatsControlAggProfile (INT4 i4DsmonStatsControlIndex,
                                   INT4 i4SetValDsmonStatsControlAggProfile)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonStatsControlAggProfile \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    pStatsCtlNode->u4DsmonStatsCtlProfileIndex =
        (UINT4) (i4SetValDsmonStatsControlAggProfile);

    pStatsCtlNode->pAggCtl =
        DsmonAggCtlGetEntry (i4SetValDsmonStatsControlAggProfile);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonStatsControlOwner
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                setValDsmonStatsControlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonStatsControlOwner (INT4 i4DsmonStatsControlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValDsmonStatsControlOwner)
{

    UINT4               u4Minimum = 0;

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonStatsControlOwner \n");

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    u4Minimum =
        (pSetValDsmonStatsControlOwner->i4_Length <
         DSMON_MAX_OWNER_LEN) ? pSetValDsmonStatsControlOwner->
        i4_Length : DSMON_MAX_OWNER_LEN;

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Stats Control Index \n");

        return SNMP_FAILURE;
    }

    MEMSET (pStatsCtlNode->au1DsmonStatsCtlOwner, DSMON_INIT_VAL,
            DSMON_MAX_OWNER_LEN);

    MEMCPY (pStatsCtlNode->au1DsmonStatsCtlOwner,
            pSetValDsmonStatsControlOwner->pu1_OctetList, u4Minimum);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonStatsControlStatus
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                setValDsmonStatsControlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonStatsControlStatus (INT4 i4DsmonStatsControlIndex,
                               INT4 i4SetValDsmonStatsControlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tDsmonStatsCtl     *pStatsCtlNode = NULL;
    tDsmonAggCtl       *pAggCtlNode = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;
    INT4                i4Result = 0;

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonStatsControlStatus \n");

    switch (i4SetValDsmonStatsControlStatus)
    {
        case CREATE_AND_WAIT:

            pStatsCtlNode = DsmonStatsAddCtlEntry (i4DsmonStatsControlIndex);

            if (pStatsCtlNode == NULL)
            {
                DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                           " Memory Allocation / \
                 RBTree Add Failed - Stats Control Table \n");

                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:

            pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

            if (pStatsCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Stats Control Index \n");

                return SNMP_FAILURE;
            }

            /* check for valid agg profile index */

            pAggCtlNode =
                DsmonAggCtlGetEntry (pStatsCtlNode->
                                     u4DsmonStatsCtlProfileIndex);

            if (pAggCtlNode == NULL)
            {

                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure:\
                 Invalid Stats Control Agg Profile Index \n");

                return SNMP_FAILURE;
            }

            /* check for valid datasource */

            if (CfaGetIfInfo (pStatsCtlNode->u4DsmonStatsDataSource, &IfInfo) ==
                CFA_FAILURE)
            {
                /* ifindex is invalid */

                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid interface Index \n");

                return SNMP_FAILURE;
            }

            if (((IfInfo.u1IfOperStatus != CFA_IF_UP) ||
                 (IfInfo.u1IfAdminStatus != CFA_IF_UP)) &&
                ((gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)))
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Interface OperStatus or Admin Status is in invalid state \n");
                return SNMP_FAILURE;
            }
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                DSMON_TRC (DSMON_FN_ENTRY,
                           "nmhSetDsmonStatsControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));

            if (IfInfo.u1IfType == CFA_L3IPVLAN)
            {
                /* Get the VlanId from interface index */
                if (CfaGetVlanId
                    (pStatsCtlNode->u4DsmonStatsDataSource,
                     &u2VlanId) == CFA_FAILURE)
                {
                    DSMON_TRC1 (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Unable to Fetch VlanId of IfIndex %d\n", pStatsCtlNode->u4DsmonStatsDataSource);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }

                if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList) ==
                    L2IWF_FAILURE)
                {
                    DSMON_TRC1 (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Get Member Port List for VlanId %d failed \n", u2VlanId);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }
            }

            pStatsCtlNode->u4DsmonStatsCtlRowStatus = ACTIVE;

            pStatsCtlNode->u4DsmonStatsCtlCreateTime =
                (UINT4) OsixGetSysUpTime ();
#ifdef NPAPI_WANTED
            DsmonFsDSMONEnableProbe (pStatsCtlNode->u4DsmonStatsDataSource,
                                     u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            DsmonStatsPopulateDataEntries (pStatsCtlNode);
            DsmonPktInfoDelUnusedEntries ();

            break;

        case NOT_IN_SERVICE:

            pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

            if (pStatsCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Stats Control Index \n");

                return SNMP_FAILURE;
            }
            /* check for valid datasource */

            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                DSMON_TRC (DSMON_FN_ENTRY,
                           "nmhSetDsmonStatsControlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (CfaGetIfInfo (pStatsCtlNode->u4DsmonStatsDataSource, &IfInfo) !=
                CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId (pStatsCtlNode->u4DsmonStatsDataSource,
                                   &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }
            pStatsCtlNode->u4DsmonStatsCtlRowStatus = NOT_IN_SERVICE;
#ifdef NPAPI_WANTED
            DsmonFsDSMONDisableProbe (pStatsCtlNode->u4DsmonStatsDataSource,
                                      u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            DsmonStatsDelDataEntries (i4DsmonStatsControlIndex);
            DsmonPktInfoDelUnusedEntries ();

            break;

        case DESTROY:

            pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

            if (pStatsCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Stats Control Index \n");
                return SNMP_FAILURE;
            }

            i4Result = DsmonStatsDelCtlEntry (pStatsCtlNode);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Stats Control Index \
                 RBTreeRemove failed - Stats Control Table\n");

                return SNMP_FAILURE;
            }

            DsmonPktInfoDelUnusedEntries ();
            break;
        default:
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DsmonStatsControlDataSource
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                testValDsmonStatsControlDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonStatsControlDataSource (UINT4 *pu4ErrorCode,
                                      INT4 i4DsmonStatsControlIndex,
                                      tSNMP_OID_TYPE *
                                      pTestValDsmonStatsControlDataSource)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonStatsControlDataSource \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON  Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonStatsControlIndex < DSMON_ONE) ||
        (i4DsmonStatsControlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: StatsControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (DSMON_TEST_DATA_SOURCE_OID (pTestValDsmonStatsControlDataSource) ==
        FALSE)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_DEBUG_TRC, "Invalid DataSource");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pStatsCtlNode->u4DsmonStatsCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT, "Leaving function, \
              nmhTestv2DsmonStatsControlDataSource \n");

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonStatsControlAggProfile
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                testValDsmonStatsControlAggProfile
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonStatsControlAggProfile (UINT4 *pu4ErrorCode,
                                      INT4 i4DsmonStatsControlIndex,
                                      INT4 i4TestValDsmonStatsControlAggProfile)
{
    tDsmonStatsCtl     *pStatsCtlNode = NULL;
    tDsmonAggCtl       *pAggCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonStatsControlAggProfile \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonStatsControlIndex < DSMON_ONE) ||
        (i4DsmonStatsControlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: StatsControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pAggCtlNode = DsmonAggCtlGetEntry (i4TestValDsmonStatsControlAggProfile);

    if (pAggCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: AggCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: StatsControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pStatsCtlNode->u4DsmonStatsCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonStatsControlOwner
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                testValDsmonStatsControlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonStatsControlOwner (UINT4 *pu4ErrorCode,
                                 INT4 i4DsmonStatsControlIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValDsmonStatsControlOwner)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonStatsControlOwner \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonStatsControlIndex < DSMON_ONE) ||
        (i4DsmonStatsControlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: StatsControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((pTestValDsmonStatsControlOwner->i4_Length < DSMON_ZERO) ||
        (pTestValDsmonStatsControlOwner->i4_Length > DSMON_MAX_OWNER_LEN))
    {

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid Owner length \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pStatsCtlNode->u4DsmonStatsCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonStatsControlStatus
 Input       :  The Indices
                DsmonStatsControlIndex

                The Object
                testValDsmonStatsControlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonStatsControlStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4DsmonStatsControlIndex,
                                  INT4 i4TestValDsmonStatsControlStatus)
{

    tDsmonStatsCtl     *pStatsCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonStatsControlStatus \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4DsmonStatsControlIndex < DSMON_ONE) ||
        (i4DsmonStatsControlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: StatsControlIndex out of range \n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    /* Get Stats Control Node corresponding to StatsControlIndex */

    pStatsCtlNode = DsmonStatsGetCtlEntry (i4DsmonStatsControlIndex);

    if (pStatsCtlNode == NULL)
    {
        if (i4TestValDsmonStatsControlStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValDsmonStatsControlStatus == ACTIVE) ||
            (i4TestValDsmonStatsControlStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonStatsControlStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DsmonStatsControlTable
 Input       :  The Indices
                DsmonStatsControlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DsmonStatsControlTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonStatsTable
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonStatsTable (INT4 i4DsmonStatsControlIndex,
                                         INT4 i4DsmonAggGroupIndex)
{
    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonStatsTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonStatsTable
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonStatsTable (INT4 *pi4DsmonStatsControlIndex,
                                 INT4 *pi4DsmonAggGroupIndex)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetFirstIndexDsmonStatsTable \n");

    return nmhGetNextIndexDsmonStatsTable (DSMON_ZERO,
                                           pi4DsmonStatsControlIndex,
                                           DSMON_ZERO, pi4DsmonAggGroupIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonStatsTable
 Input       :  The Indices
                DsmonStatsControlIndex
                nextDsmonStatsControlIndex
                DsmonAggGroupIndex
                nextDsmonAggGroupIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonStatsTable (INT4 i4DsmonStatsControlIndex,
                                INT4 *pi4NextDsmonStatsControlIndex,
                                INT4 i4DsmonAggGroupIndex,
                                INT4 *pi4NextDsmonAggGroupIndex)
{

    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetNextIndexDsmonStatsTable \n");

    pStatsNode = DsmonStatsGetNextDataIndex (i4DsmonStatsControlIndex,
                                             i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonStatsControlIndex = (INT4) (pStatsNode->u4DsmonStatsCtlIndex);

    *pi4NextDsmonAggGroupIndex = (INT4) (pStatsNode->u4DsmonStatsAggGroupIndex);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonStatsInPkts
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsInPkts (INT4 i4DsmonStatsControlIndex,
                        INT4 i4DsmonAggGroupIndex,
                        UINT4 *pu4RetValDsmonStatsInPkts)
{
    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsInPkts \n");

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonStatsInPkts = pStatsNode->u4DsmonStatsInPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsInOctets
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsInOctets (INT4 i4DsmonStatsControlIndex,
                          INT4 i4DsmonAggGroupIndex,
                          UINT4 *pu4RetValDsmonStatsInOctets)
{
    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsInOctets \n");

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonStatsInOctets = pStatsNode->u4DsmonStatsInOctets;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsInOvflPkts
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsInOvflPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsInOvflPkts (INT4 i4DsmonStatsControlIndex,
                            INT4 i4DsmonAggGroupIndex,
                            UINT4 *pu4RetValDsmonStatsInOvflPkts)
{
    /* deprecated */

    UNUSED_PARAM (i4DsmonStatsControlIndex);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (pu4RetValDsmonStatsInOvflPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsInOvflOctets
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsInOvflOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsInOvflOctets (INT4 i4DsmonStatsControlIndex,
                              INT4 i4DsmonAggGroupIndex,
                              UINT4 *pu4RetValDsmonStatsInOvflOctets)
{
    /* deprecated */

    UNUSED_PARAM (i4DsmonStatsControlIndex);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (pu4RetValDsmonStatsInOvflOctets);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonStatsInHCPkts
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsInHCPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsInHCPkts (INT4 i4DsmonStatsControlIndex,
                          INT4 i4DsmonAggGroupIndex,
                          tSNMP_COUNTER64_TYPE * pu8RetValDsmonStatsInHCPkts)
{

    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsInHCPkts \n");

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonStatsInHCPkts->msn =
        pStatsNode->u8DsmonStatsInHCPkts.u4HiWord;

    pu8RetValDsmonStatsInHCPkts->lsn =
        pStatsNode->u8DsmonStatsInHCPkts.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsInHCOctets
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsInHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsInHCOctets (INT4 i4DsmonStatsControlIndex,
                            INT4 i4DsmonAggGroupIndex,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValDsmonStatsInHCOctets)
{

    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsInHCOctets \n");

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonStatsInHCOctets->msn =
        pStatsNode->u8DsmonStatsInHCOctets.u4HiWord;

    pu8RetValDsmonStatsInHCOctets->lsn =
        pStatsNode->u8DsmonStatsInHCOctets.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsOutPkts
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsOutPkts (INT4 i4DsmonStatsControlIndex,
                         INT4 i4DsmonAggGroupIndex,
                         UINT4 *pu4RetValDsmonStatsOutPkts)
{

    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsOutPkts \n");

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonStatsOutPkts = pStatsNode->u4DsmonStatsOutPkts;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonStatsOutOctets
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsOutOctets (INT4 i4DsmonStatsControlIndex,
                           INT4 i4DsmonAggGroupIndex,
                           UINT4 *pu4RetValDsmonStatsOutOctets)
{
    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsOutOctets \n");

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonStatsOutOctets = pStatsNode->u4DsmonStatsOutOctets;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonStatsOutOvflPkts
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsOutOvflPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsOutOvflPkts (INT4 i4DsmonStatsControlIndex,
                             INT4 i4DsmonAggGroupIndex,
                             UINT4 *pu4RetValDsmonStatsOutOvflPkts)
{

    /* Deprecated */

    UNUSED_PARAM (i4DsmonStatsControlIndex);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (pu4RetValDsmonStatsOutOvflPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsOutOvflOctets
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsOutOvflOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsOutOvflOctets (INT4 i4DsmonStatsControlIndex,
                               INT4 i4DsmonAggGroupIndex,
                               UINT4 *pu4RetValDsmonStatsOutOvflOctets)
{

    /* Deprecated */

    UNUSED_PARAM (i4DsmonStatsControlIndex);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (pu4RetValDsmonStatsOutOvflOctets);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsOutHCPkts
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsOutHCPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsOutHCPkts (INT4 i4DsmonStatsControlIndex,
                           INT4 i4DsmonAggGroupIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValDsmonStatsOutHCPkts)
{

    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsOutHCPkts \n");

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonStatsOutHCPkts->msn =
        pStatsNode->u8DsmonStatsOutHCPkts.u4HiWord;

    pu8RetValDsmonStatsOutHCPkts->lsn =
        pStatsNode->u8DsmonStatsOutHCPkts.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonStatsOutHCOctets
 Input       :  The Indices
                DsmonStatsControlIndex
                DsmonAggGroupIndex

                The Object
                retValDsmonStatsOutHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonStatsOutHCOctets (INT4 i4DsmonStatsControlIndex,
                             INT4 i4DsmonAggGroupIndex,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValDsmonStatsOutHCOctets)
{
    tDsmonStats        *pStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonStatsOutHCOctets \n");

    /* Get Stats Control Node corresponding to StatsControlIndex and AggGroupIndex */

    pStatsNode =
        DsmonStatsGetDataEntry (i4DsmonStatsControlIndex, i4DsmonAggGroupIndex);

    if (pStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                   Agg Group Index is invalid \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonStatsOutHCOctets->msn =
        pStatsNode->u8DsmonStatsOutHCOctets.u4HiWord;

    pu8RetValDsmonStatsOutHCOctets->lsn =
        pStatsNode->u8DsmonStatsOutHCOctets.u4LoWord;

    return SNMP_SUCCESS;
}
