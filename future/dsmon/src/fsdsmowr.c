/********************************************************************
* Copyright (C) 2009 Aricent Inc . All Rights Reserved
* $Id: fsdsmowr.c,v 1.3 2011/09/24 06:46:55 siva Exp $
* Description: Protocol Low Level Wrapper Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "dsmninc.h"
# include  "fsdsmowr.h"
# include  "fsdsmodb.h"

VOID
RegisterFSDSMO ()
{
    SNMPRegisterMibWithLock (&fsdsmoOID, &fsdsmoEntry, DsmonMainLock,
                             DsmonMainUnLock, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsdsmoOID, (const UINT1 *) "fsdsmon");
}

VOID
UnRegisterFSDSMO ()
{
    SNMPUnRegisterMib (&fsdsmoOID, &fsdsmoEntry);
    SNMPDelSysorEntry (&fsdsmoOID, (const UINT1 *) "fsdsmon");
}

INT4
FsDsmonTraceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDsmonTrace (&(pMultiData->u4_ULongValue)));
}

INT4
FsDsmonAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsDsmonAdminStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsDsmonTraceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDsmonTrace (pMultiData->u4_ULongValue));
}

INT4
FsDsmonAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsDsmonAdminStatus (pMultiData->i4_SLongValue));
}

INT4
FsDsmonTraceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDsmonTrace (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsDsmonAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsDsmonAdminStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsDsmonTraceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDsmonTrace (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsDsmonAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsDsmonAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
