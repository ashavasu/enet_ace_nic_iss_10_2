/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            *
 *                                    *
 * $Id: dsmnutil.c,v 1.4 2012/07/20 10:38:15 siva Exp $           *
 *
 *  Description: This file contains the utility procedures         *
 *         used by DSMON module.                    *
 ********************************************************************/
#include "dsmninc.h"

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpPktInfo                           *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonPktInfoRBTree.*
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonPktInfo node1   *
 *                       pRBElem2 - Pointer to the DsmonPktInfo node2   *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpPktInfo (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonPktInfo      *pLocEntry1 = (tDsmonPktInfo *) pRBElem1;
    tDsmonPktInfo      *pLocEntry2 = (tDsmonPktInfo *) pRBElem2;
    INT4                i4MemcmpRetVal = 0;

    /* First Index - Interface Index */
    if (pLocEntry1->PktHdr.u4IfIndex > pLocEntry2->PktHdr.u4IfIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->PktHdr.u4IfIndex < pLocEntry2->PktHdr.u4IfIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Second Index - EtherType */
    if (pLocEntry1->PktHdr.u2EtherType > pLocEntry2->PktHdr.u2EtherType)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->PktHdr.u2EtherType < pLocEntry2->PktHdr.u2EtherType)
    {
        return DSMON_RB_LESS;
    }

    /* Third Index - Ip Protocol */
    if (pLocEntry1->PktHdr.u1IpProtocol > pLocEntry2->PktHdr.u1IpProtocol)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->PktHdr.u1IpProtocol < pLocEntry2->PktHdr.u1IpProtocol)
    {
        return DSMON_RB_LESS;
    }

    /* Forth Index - Source IP */
    i4MemcmpRetVal =
        MEMCMP (&pLocEntry1->PktHdr.SrcIp, &pLocEntry2->PktHdr.SrcIp,
                sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return DSMON_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return DSMON_RB_LESS;
    }

    /* Fifth Index - Destination IP */
    i4MemcmpRetVal =
        MEMCMP (&pLocEntry1->PktHdr.DstIp, &pLocEntry2->PktHdr.DstIp,
                sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return DSMON_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return DSMON_RB_LESS;
    }

    /* Sixth Index - Source Port */
    if (pLocEntry1->PktHdr.u2SrcPort > pLocEntry2->PktHdr.u2SrcPort)
    {
        return RMON2_RB_GREATER;
    }
    else if (pLocEntry1->PktHdr.u2SrcPort < pLocEntry2->PktHdr.u2SrcPort)
    {
        return RMON2_RB_LESS;
    }

    /* Seventh Index - Destination Port */
    if (pLocEntry1->PktHdr.u2DstPort > pLocEntry2->PktHdr.u2DstPort)
    {
        return RMON2_RB_GREATER;
    }
    else if (pLocEntry1->PktHdr.u2DstPort < pLocEntry2->PktHdr.u2DstPort)
    {
        return RMON2_RB_LESS;
    }

    /* Eighth Index - DSCP value */
    if (pLocEntry1->PktHdr.u1DSCP > pLocEntry2->PktHdr.u1DSCP)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->PktHdr.u1DSCP < pLocEntry2->PktHdr.u1DSCP)
    {
        return DSMON_RB_LESS;
    }

    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpAggCtl                            *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonAggCtlRBTree. *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonAggCtl node1    *
 *                       pRBElem2 - Pointer to the DsmonAggCtl node2    *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpAggCtl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonAggCtl       *pLocEntry1 = (tDsmonAggCtl *) pRBElem1;
    tDsmonAggCtl       *pLocEntry2 = (tDsmonAggCtl *) pRBElem2;

    /* Index - Agg. Ctl. Index */
    if (pLocEntry1->u4DsmonAggCtlIndex > pLocEntry2->u4DsmonAggCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonAggCtlIndex < pLocEntry2->u4DsmonAggCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpAggGroup                          *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for             *
 *                 dsmonAggCtlGroupRBTree.             *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonAggGroup node1  *
 *                       pRBElem2 - Pointer to the DsmonAggGroup node2  *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpAggGroup (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonAggGroup     *pLocEntry1 = (tDsmonAggGroup *) pRBElem1;
    tDsmonAggGroup     *pLocEntry2 = (tDsmonAggGroup *) pRBElem2;

    /* First Index - Agg. Ctl Index */
    if (pLocEntry1->u4DsmonAggCtlIndex > pLocEntry2->u4DsmonAggCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonAggCtlIndex < pLocEntry2->u4DsmonAggCtlIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Second Index - Agg. Group Index */
    if (pLocEntry1->u4DsmonAggGroupIndex > pLocEntry2->u4DsmonAggGroupIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonAggGroupIndex <
             pLocEntry2->u4DsmonAggGroupIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpStatsCtl                          *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonStatsCtlRBTree*
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonStatsCtl node1  *
 *                       pRBElem2 - Pointer to the DsmonStatsCtl node2  *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpStatsCtl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonStatsCtl     *pLocEntry1 = (tDsmonStatsCtl *) pRBElem1;
    tDsmonStatsCtl     *pLocEntry2 = (tDsmonStatsCtl *) pRBElem2;

    /* Index - Stats Ctl. */
    if (pLocEntry1->u4DsmonStatsCtlIndex > pLocEntry2->u4DsmonStatsCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonStatsCtlIndex <
             pLocEntry2->u4DsmonStatsCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpStats                             *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonStatsRBTree.  *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonStats node1     *
 *                       pRBElem2 - Pointer to the DsmonStats node2     *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpStats (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonStats        *pLocEntry1 = (tDsmonStats *) pRBElem1;
    tDsmonStats        *pLocEntry2 = (tDsmonStats *) pRBElem2;

    /* First Index - Stats Control Index */
    if (pLocEntry1->u4DsmonStatsCtlIndex > pLocEntry2->u4DsmonStatsCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonStatsCtlIndex <
             pLocEntry2->u4DsmonStatsCtlIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Second Index - Agg. Group Index */
    if (pLocEntry1->u4DsmonStatsAggGroupIndex >
        pLocEntry2->u4DsmonStatsAggGroupIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonStatsAggGroupIndex <
             pLocEntry2->u4DsmonStatsAggGroupIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpPdistCtl                          *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonPdistCtlRBTree*
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonPdistCtl node1  *
 *                       pRBElem2 - Pointer to the DsmonPdistCtl node2  *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpPdistCtl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonPdistCtl     *pLocEntry1 = (tDsmonPdistCtl *) pRBElem1;
    tDsmonPdistCtl     *pLocEntry2 = (tDsmonPdistCtl *) pRBElem2;

    /* Index - Pdist Control Index */
    if (pLocEntry1->u4DsmonPdistCtlIndex > pLocEntry2->u4DsmonPdistCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonPdistCtlIndex <
             pLocEntry2->u4DsmonPdistCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpPdistStats                        *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonPdistRBTree.  *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonPdistStats node1*
 *                       pRBElem2 - Pointer to the DsmonPdistStats node2*
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpPdistStats (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonPdistStats   *pLocEntry1 = (tDsmonPdistStats *) pRBElem1;
    tDsmonPdistStats   *pLocEntry2 = (tDsmonPdistStats *) pRBElem2;

    /* First Index - Pdist Ctl Index */
    if (pLocEntry1->u4DsmonPdistCtlIndex > pLocEntry2->u4DsmonPdistCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonPdistCtlIndex <
             pLocEntry2->u4DsmonPdistCtlIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Second Index - Time Mark - Ignore */

    /* Third Index - Agg. Group Index */
    if (pLocEntry1->u4DsmonPdistAggGroupIndex >
        pLocEntry2->u4DsmonPdistAggGroupIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonPdistAggGroupIndex <
             pLocEntry2->u4DsmonPdistAggGroupIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Forth Index - Protocol Dir Local Index */
    if (pLocEntry1->u4DsmonPdistProtDirLocalIndex >
        pLocEntry2->u4DsmonPdistProtDirLocalIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonPdistProtDirLocalIndex <
             pLocEntry2->u4DsmonPdistProtDirLocalIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpPdistTopNCtl                      *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for             *
 *                 dsmonPdistTopNCtlRBTree.             *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to DsmonPdistTopNCtl node1  *
 *                       pRBElem2 - Pointer to DsmonPdistTopNCtl node2  *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpPdistTopNCtl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonPdistTopNCtl *pLocEntry1 = (tDsmonPdistTopNCtl *) pRBElem1;
    tDsmonPdistTopNCtl *pLocEntry2 = (tDsmonPdistTopNCtl *) pRBElem2;

    /* Index - Pdist TopN Ctl Index */
    if (pLocEntry1->u4DsmonPdistTopNCtlIndex >
        pLocEntry2->u4DsmonPdistTopNCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonPdistTopNCtlIndex <
             pLocEntry2->u4DsmonPdistTopNCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpPdistTopN                         *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for             *
 *                 dsmonPdistTopNRBTree.                 *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to DsmonPdistTopN node1      *
 *                       pRBElem2 - Pointer to DsmonPdistTopN node2      *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpPdistTopN (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonPdistTopN    *pLocEntry1 = (tDsmonPdistTopN *) pRBElem1;
    tDsmonPdistTopN    *pLocEntry2 = (tDsmonPdistTopN *) pRBElem2;

    /* First Index - Pdist TopN Ctl Index */
    if (pLocEntry1->u4DsmonPdistTopNCtlIndex >
        pLocEntry2->u4DsmonPdistTopNCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonPdistTopNCtlIndex <
             pLocEntry2->u4DsmonPdistTopNCtlIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Second Index - Pdist TopN Index */
    if (pLocEntry1->u4DsmonPdistTopNIndex > pLocEntry2->u4DsmonPdistTopNIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonPdistTopNIndex <
             pLocEntry2->u4DsmonPdistTopNIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpHostCtl                           *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonHostCtlRBTree *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonHostCtl node1   *
 *                       pRBElem2 - Pointer to the DsmonHostCtl node2   *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpHostCtl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonHostCtl      *pLocEntry1 = (tDsmonHostCtl *) pRBElem1;
    tDsmonHostCtl      *pLocEntry2 = (tDsmonHostCtl *) pRBElem2;

    /* Index - Host Ctl Index */
    if (pLocEntry1->u4DsmonHostCtlIndex > pLocEntry2->u4DsmonHostCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonHostCtlIndex < pLocEntry2->u4DsmonHostCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpHostStats                         *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonHostRBTree.   *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonHostStats node1 *
 *                       pRBElem2 - Pointer to the DsmonHostStats node2 *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpHostStats (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonHostStats    *pLocEntry1 = (tDsmonHostStats *) pRBElem1;
    tDsmonHostStats    *pLocEntry2 = (tDsmonHostStats *) pRBElem2;
    INT4                i4MemcmpRetVal = 0;

    /* First Index - Host Ctl Index */
    if (pLocEntry1->u4DsmonHostCtlIndex > pLocEntry2->u4DsmonHostCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonHostCtlIndex < pLocEntry2->u4DsmonHostCtlIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Second Index - Time Mark - Ignore */

    /* Third Index - Agg. Group Index */
    if (pLocEntry1->u4DsmonHostAggGroupIndex >
        pLocEntry2->u4DsmonHostAggGroupIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonHostAggGroupIndex <
             pLocEntry2->u4DsmonHostAggGroupIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Forth Index - Protocol Dir Local Index */
    if (pLocEntry1->u4DsmonHostProtDirLocalIndex >
        pLocEntry2->u4DsmonHostProtDirLocalIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonHostProtDirLocalIndex <
             pLocEntry2->u4DsmonHostProtDirLocalIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Fifth Index - Host address */
    i4MemcmpRetVal = MEMCMP (&pLocEntry1->DsmonHostNLAddress,
                             &pLocEntry2->DsmonHostNLAddress, sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return DSMON_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;

}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpHostTopNCtl                       *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for             *
 *                 dsmonHostTopNCtlRBTree.             *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to DsmonHostTopNCtl node1   *
 *                       pRBElem2 - Pointer to DsmonHostTopNCtl node2   *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpHostTopNCtl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonHostTopNCtl  *pLocEntry1 = (tDsmonHostTopNCtl *) pRBElem1;
    tDsmonHostTopNCtl  *pLocEntry2 = (tDsmonHostTopNCtl *) pRBElem2;

    /* Index - Host TopN Ctl Index */
    if (pLocEntry1->u4DsmonHostTopNCtlIndex >
        pLocEntry2->u4DsmonHostTopNCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonHostTopNCtlIndex <
             pLocEntry2->u4DsmonHostTopNCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpHostTopN                        *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for             *
 *                 dsmonHostTopNRBTree.                 *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to DsmonHostTopN node1       *
 *                       pRBElem2 - Pointer to DsmonHostTopN node2       *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpHostTopN (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonHostTopN     *pLocEntry1 = (tDsmonHostTopN *) pRBElem1;
    tDsmonHostTopN     *pLocEntry2 = (tDsmonHostTopN *) pRBElem2;

    /* First Index - Host TopN Ctl Index */
    if (pLocEntry1->u4DsmonHostTopNCtlIndex >
        pLocEntry2->u4DsmonHostTopNCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonHostTopNCtlIndex <
             pLocEntry2->u4DsmonHostTopNCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    /* Second Index - Host TopN Index */
    if (pLocEntry1->u4DsmonHostTopNIndex > pLocEntry2->u4DsmonHostTopNIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonHostTopNIndex <
             pLocEntry2->u4DsmonHostTopNIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpMatrixCtl                         *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for             *
 *                 dsmonMatrixCtlRBTree.                 *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonMatrixCtl node1 *
 *                       pRBElem2 - Pointer to the DsmonMatrixCtl node2 *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpMatrixCtl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonMatrixCtl    *pLocEntry1 = (tDsmonMatrixCtl *) pRBElem1;
    tDsmonMatrixCtl    *pLocEntry2 = (tDsmonMatrixCtl *) pRBElem2;

    /* Index - Matrix Ctl Index */
    if (pLocEntry1->u4DsmonMatrixCtlIndex > pLocEntry2->u4DsmonMatrixCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixCtlIndex <
             pLocEntry2->u4DsmonMatrixCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;

}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpMatrixSD                          *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonMatrixSDRBTree*
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonMatrixSD node1    *
 *                       pRBElem2 - Pointer to the DsmonMatrixSD node2  *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpMatrixSD (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonMatrixSD     *pLocEntry1 = (tDsmonMatrixSD *) pRBElem1;
    tDsmonMatrixSD     *pLocEntry2 = (tDsmonMatrixSD *) pRBElem2;
    INT4                i4MemcmpRetVal = 0;

    /* First Index - Matrix Control Index */
    if (pLocEntry1->u4DsmonMatrixCtlIndex > pLocEntry2->u4DsmonMatrixCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixCtlIndex <
             pLocEntry2->u4DsmonMatrixCtlIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Second Index - Matrix Time Mark - Ignore */

    /* Third Index - Agg. Group Index */
    if (pLocEntry1->u4DsmonMatrixSDGroupIndex >
        pLocEntry2->u4DsmonMatrixSDGroupIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixSDGroupIndex <
             pLocEntry2->u4DsmonMatrixSDGroupIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Forth Index - Protocol NLIndex */
    if (pLocEntry1->u4DsmonMatrixSDNLIndex > pLocEntry2->u4DsmonMatrixSDNLIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixSDNLIndex <
             pLocEntry2->u4DsmonMatrixSDNLIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Fifth Index - Source Address */
    i4MemcmpRetVal = MEMCMP (&pLocEntry1->DsmonMatrixSDSourceAddress,
                             &pLocEntry2->DsmonMatrixSDSourceAddress,
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return DSMON_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return DSMON_RB_LESS;
    }

    /* Sixth Index - Destination Address */
    i4MemcmpRetVal = MEMCMP (&pLocEntry1->DsmonMatrixSDDestAddress,
                             &pLocEntry2->DsmonMatrixSDDestAddress,
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return DSMON_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return DSMON_RB_LESS;
    }

    /* Seventh Index - Matrix ALIndex */
    if (pLocEntry1->u4DsmonMatrixSDALIndex > pLocEntry2->u4DsmonMatrixSDALIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixSDALIndex <
             pLocEntry2->u4DsmonMatrixSDALIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;

}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpMatrixDS                          *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for dsmonMatrixDSRBTree*
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to the DsmonMatrixSD node1    *
 *                       pRBElem2 - Pointer to the DsmonMatrixSD node2  *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpMatrixDS (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonMatrixSD     *pLocEntry1 = (tDsmonMatrixSD *) pRBElem1;
    tDsmonMatrixSD     *pLocEntry2 = (tDsmonMatrixSD *) pRBElem2;
    INT4                i4MemcmpRetVal = 0;

    /* First Index - Matrix Control Index */
    if (pLocEntry1->u4DsmonMatrixCtlIndex > pLocEntry2->u4DsmonMatrixCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixCtlIndex <
             pLocEntry2->u4DsmonMatrixCtlIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Second Index - Matrix Time Mark - Ignore */

    /* Third Index - Agg. Group Index */
    if (pLocEntry1->u4DsmonMatrixSDGroupIndex >
        pLocEntry2->u4DsmonMatrixSDGroupIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixSDGroupIndex <
             pLocEntry2->u4DsmonMatrixSDGroupIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Forth Index - Protocol NLIndex */
    if (pLocEntry1->u4DsmonMatrixSDNLIndex > pLocEntry2->u4DsmonMatrixSDNLIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixSDNLIndex <
             pLocEntry2->u4DsmonMatrixSDNLIndex)
    {
        return DSMON_RB_LESS;
    }

    /* Fifth Index - Destination Address */
    i4MemcmpRetVal = MEMCMP (&pLocEntry1->DsmonMatrixSDDestAddress,
                             &pLocEntry2->DsmonMatrixSDDestAddress,
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return DSMON_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return DSMON_RB_LESS;
    }

    /* Sixth Index - Source Address */
    i4MemcmpRetVal = MEMCMP (&pLocEntry1->DsmonMatrixSDSourceAddress,
                             &pLocEntry2->DsmonMatrixSDSourceAddress,
                             sizeof (tIpAddr));
    if (i4MemcmpRetVal > 0)
    {
        return DSMON_RB_GREATER;
    }
    else if (i4MemcmpRetVal < 0)
    {
        return DSMON_RB_LESS;
    }

    /* Seventh Index - Matrix ALIndex */
    if (pLocEntry1->u4DsmonMatrixSDALIndex > pLocEntry2->u4DsmonMatrixSDALIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixSDALIndex <
             pLocEntry2->u4DsmonMatrixSDALIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;

}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpMatrixTopNCtl                     *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for             *
 *                 dsmonAggCtlRBTree.                 *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to DsmonMatrixTopNCtl node1 *
 *                       pRBElem2 - Pointer to DsmonMatrixTopNCtl node2 *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpMatrixTopNCtl (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonMatrixTopNCtl *pLocEntry1 = (tDsmonMatrixTopNCtl *) pRBElem1;
    tDsmonMatrixTopNCtl *pLocEntry2 = (tDsmonMatrixTopNCtl *) pRBElem2;

    /* Index - Matrix TopN Ctl Index */
    if (pLocEntry1->u4DsmonMatrixTopNCtlIndex >
        pLocEntry2->u4DsmonMatrixTopNCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixTopNCtlIndex <
             pLocEntry2->u4DsmonMatrixTopNCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *                                                                      *
 *    FUNCTION NAME    : DsmonUtlRBCmpMatrixTopN                    *
 *                                                                      *
 *    DESCRIPTION      : RBTree compare function for             *
 *                 dsmonAggCtlRBTree.                 *
 *                                                                      *
 *    INPUT            : pRBElem1 - Pointer to DsmonMatrixTopN node1     *
 *                       pRBElem2 - Pointer to DsmonMatrixTopN node2    *
 *                                                                      *
 *    OUTPUT           : NONE                                           *
 *                                                                      *
 *    RETURNS          : DSMON_RB_EQUAL - if all the keys matched       *
 *                                          for both the nodes.         *
 *                       DSMON_RB_LESS - if node pRBElem1's key is      *
 *                                       less than node pRBElem2's key. *
 *                       DSMON_RB_GREATER - if node pRBElem1's key is   *
 *                                      greater than node pRBElem2's key*
 *                                                                      *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBCmpMatrixTopN (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tDsmonMatrixTopN   *pLocEntry1 = (tDsmonMatrixTopN *) pRBElem1;
    tDsmonMatrixTopN   *pLocEntry2 = (tDsmonMatrixTopN *) pRBElem2;

    /* First Index - Matrix TopN Ctl Index */
    if (pLocEntry1->u4DsmonMatrixTopNCtlIndex >
        pLocEntry2->u4DsmonMatrixTopNCtlIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixTopNCtlIndex <
             pLocEntry2->u4DsmonMatrixTopNCtlIndex)
    {
        return DSMON_RB_LESS;
    }
    /* Second Index - Matrix TopN Index */
    if (pLocEntry1->u4DsmonMatrixTopNIndex > pLocEntry2->u4DsmonMatrixTopNIndex)
    {
        return DSMON_RB_GREATER;
    }
    else if (pLocEntry1->u4DsmonMatrixTopNIndex <
             pLocEntry2->u4DsmonMatrixTopNIndex)
    {
        return DSMON_RB_LESS;
    }
    return DSMON_RB_EQUAL;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreePktInfo                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with PktInfo.                    *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreePktInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
#ifdef NPAPI_WANTED
        DsmonFsDSMONRemoveFlowStats (((tDsmonPktInfo *) pRBElem)->PktHdr);
#endif
        MemReleaseMemBlock (DSMON_PKTINFO_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeAggCtl                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Aggregation Control.            *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeAggCtl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_AGGCTL_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeAggGroup                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Aggregation Group.            *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeAggGroup (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_AGGCTL_GROUP_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeStatsCtl                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Statistics Control.            *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeStatsCtl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_STATSCTL_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeStats                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Statistics.                *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeStats (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_STATS_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreePdistCtl                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Pdist Control.                *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreePdistCtl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_PDISTCTL_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreePdistStats            *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Pdist Stats.                *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreePdistStats (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_PDIST_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreePdistTopNCtl            *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Pdist TopN Control.            *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreePdistTopNCtl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_PDIST_TOPNCTL_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreePdistTopN            *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Pdist TopN.                *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreePdistTopN (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_PDIST_TOPN_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeHostCtl                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Host Control.                *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeHostCtl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_HOSTCTL_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeHostStats            *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Host Statistics.                *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeHostStats (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_HOST_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeHostTopNCtl            *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Host TopN Control.            *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeHostTopNCtl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_HOST_TOPNCTL_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeHostTopN                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Host TopN.            *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeHostTopN (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_HOST_TOPN_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeMatrixCtl            *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Matrix Control.                *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeMatrixCtl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_MATRICCTL_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeMatrixSD                *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with MatrixSD.                    *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeMatrixSD (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_MATRIXSD_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeMatrixTopNCtl            *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Matrix TopN Control.            *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeMatrixTopNCtl (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_MATRIX_TOPNCTL_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DsmonUtlRBFreeMatrixTopN            *
 *                                    *
 *    DESCRIPTION      : This function releases the memory associated     *
 *                 with Matrix TopN.                *
 *                                    *
 *    INPUT            : pRBElem - pointer to RBElement of which memory *
 *                 should be released                *
 *    OUTPUT           : None                        *
 *                                    *
 *    RETURNS          : OSIX_SUCCESS/OSIX_FAILURE            *
 ************************************************************************/
PUBLIC INT4
DsmonUtlRBFreeMatrixTopN (tRBElem * pRBElem, UINT4 u4Arg)
{
    UNUSED_PARAM (u4Arg);
    if (pRBElem != NULL)
    {
        MemReleaseMemBlock (DSMON_MATRIX_TOPN_POOL, (UINT1 *) pRBElem);
    }
    return OSIX_SUCCESS;
}

/************************************************************************
 *    FUNCTION NAME    : DmonUtlFormDataSourceOid                       *
 *                                                                      *
 *    DESCRIPTION      : DmonFormDataSourceOid function  forms the      *
 *                       DataSource Oid from the interface number       *
 *                                                                      *
 *    INPUT            : pDataSourceOid, u4IfIndex                      *
 *                                                                      *
 *    OUTPUT           : None                                           *
 *                                                                      *
 *    RETURNS          : None                                           *
 ************************************************************************/

PUBLIC VOID
DsmonUtlFormDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid, UINT4 u4IfIndex)
{
    DSMON_TRC (DSMON_FN_ENTRY, " FUNC: ENTRY DsmonUtlFormDataSourceOid \n");

    MEMCPY (pDataSourceOid->pu4_OidList, gDsmonGlobals.interfaceOid.pu4_OidList,
            (gDsmonGlobals.interfaceOid.u4_Length * sizeof (UINT4)));
    pDataSourceOid->pu4_OidList[gDsmonGlobals.interfaceOid.u4_Length] =
        u4IfIndex;
    pDataSourceOid->u4_Length = (gDsmonGlobals.interfaceOid.u4_Length + 1);

    DSMON_TRC (DSMON_FN_EXIT, " FUNC: EXIT DsmonUtlFormDataSourceOid \n");
}

/************************************************************************
 *    FUNCTION NAME    : DmonUtlSetDataSourceOid                        *
 *                                                                      *
 *    DESCRIPTION      : DmonUtlSetDataSourceOid function extracts the  *
 *                       interface number from the DataSource Oid       *
 *                                                                      *
 *    INPUT            : pDataSourceOid, u4IfIndex                      *
 *                                                                      *
 *    OUTPUT           : None                                           *
 *                                                                      *
 *    RETURNS          : None                                           *
 ************************************************************************/
PUBLIC VOID
DsmonUtlSetDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid, UINT4 *pu4IfIndex)
{
    DSMON_TRC (DSMON_FN_ENTRY, " FUNC: ENTRY DmonUtlSetDataSourceOid \n");

    *pu4IfIndex =
        *(pDataSourceOid->pu4_OidList + gDsmonGlobals.interfaceOid.u4_Length);

    DSMON_TRC (DSMON_FN_EXIT, " FUNC: EXIT DmonUtlSetDataSourceOid \n");
}

/************************************************************************
 *    FUNCTION NAME    : RmonUtlTestDataSourceOid                       *
 *                                                                      *
 *    DESCRIPTION      : RmonUtlTestDataSourceOid function validates the*
 *                       DataSource Oid                                 *
 *                                                                      *
 *    INPUT            : pDataSourceOid                                 *
 *                                                                      *
 *    OUTPUT           : None                                           *
 *                                                                      *
 *    RETURNS          : TRUE / FALSE                                   *
 ************************************************************************/
INT1
DsmonUtlTestDataSourceOid (tSNMP_OID_TYPE * pDataSourceOid)
{
    DSMON_TRC (DSMON_FN_ENTRY, " FUNC: ENTRY DsmonUtlTestDataSourceOid\n");

    if ((pDataSourceOid->u4_Length ==
         (gDsmonGlobals.interfaceOid.u4_Length + 1))
        &&
        (MEMCMP
         (pDataSourceOid->pu4_OidList, gDsmonGlobals.interfaceOid.pu4_OidList,
          gDsmonGlobals.interfaceOid.u4_Length) == 0)
        &&
        (CfaValidateIfIndex
         ((UINT4) pDataSourceOid->
          pu4_OidList[gDsmonGlobals.interfaceOid.u4_Length]) == CFA_SUCCESS))
    {
        return TRUE;
    }

    DSMON_TRC (DSMON_FN_EXIT, " FUNC: EXIT DsmonUtlTestDataSourceOid\n");
    return FALSE;
}
