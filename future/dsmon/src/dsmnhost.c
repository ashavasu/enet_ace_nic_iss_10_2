/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *
 * $Id: dsmnhost.c,v 1.10 2012/12/13 14:25:09 siva Exp $           *
 *                                                                  *
 * Description: This file contains the functional routine for DSMON *
 *              Host module                                    *
 *                                                                  *
 *******************************************************************/
#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostGiveLRUEntry                                     */
/*                                                                           */
/* Description  : Gives the LRU entry                                        */
/*                                                                           */
/* Input        : u4HostCtlIndex                                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Valid Host Data Entry / NULL                              */
/*                                                                           */
/*****************************************************************************/
PRIVATE tDsmonHostStats *
DsmonHostGiveLRUEntry (UINT4 u4HostCtlIndex)
{
    tDsmonHostStats    *pHostStats = NULL, *pLRUEntry = NULL;
    UINT4               u4HostGrpIndex = 0, u4HostProtoIndex = 0;
    tIpAddr             HostAddr;

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostGiveLRUEntry\r\n");

    /* Get First Entry in Host Table */
    pHostStats = DsmonHostGetNextDataIndex
        (u4HostCtlIndex, u4HostGrpIndex, u4HostProtoIndex, &HostAddr);

    /* Mark First Entry as LRU entry */
    pLRUEntry = pHostStats;

    while (pHostStats != NULL)
    {
        if (pHostStats->u4DsmonHostCtlIndex != u4HostCtlIndex)
        {
            break;
        }
        u4HostGrpIndex = pHostStats->u4DsmonHostAggGroupIndex;
        u4HostProtoIndex = pHostStats->u4DsmonHostProtDirLocalIndex;
        MEMCPY (&HostAddr, &pHostStats->DsmonHostNLAddress, sizeof (tIpAddr));

        if (pLRUEntry->u4DsmonHostTimeMark > pHostStats->u4DsmonHostTimeMark)
        {
            pLRUEntry = pHostStats;
        }

        pHostStats = DsmonHostGetNextDataIndex (u4HostCtlIndex,
                                                u4HostGrpIndex,
                                                u4HostProtoIndex, &HostAddr);
    }                            /* End of While */
    return pLRUEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostApplyIPv6Prefix                                   */
/*                                                                           */
/* Description  : Applies Prefixlen to the given ip6 address and return the  */
/*                network address part of the IP address                     */
/*                                                                           */
/* Input        : pPrefAddr, pAddr, NumBits                                  */
/*                                                                           */
/* Output       : tIpAddr                                                    */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
DsmonHostApplyIPv6Prefix (tIpAddr * pPref, tIpAddr * pAddr, UINT4 u4NumBits)
{
    INT4                i4Dw = 0, i4Bit = 0;
    UINT4               u4B1 = 0, u4Mask = 0, u4_set_bit = 0;

    MEMSET (pPref, 0, sizeof (tIpAddr));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostApplyIPv6Prefix\r\n");

    for (u4_set_bit = 0; u4_set_bit < u4NumBits; u4_set_bit++)
    {
        i4Dw = u4B1 = u4Mask = 0;
        i4Bit = u4_set_bit;

        i4Dw = i4Bit >> 0x05;

        u4B1 = pAddr->u4_addr[i4Dw];
        i4Bit = ~i4Bit;
        i4Bit &= 0x1f;
        u4Mask = OSIX_HTONL (1 << i4Bit);

        pPref->u4_addr[i4Dw] |= (u4B1 & u4Mask);
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostApplyIPv6Prefix\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostApplyPrefix                                     */
/*                                                                           */
/* Description  : Applies Prefixlen to the given ip address and return the   */
/*          network address part of the IP address                     */
/*                                                                           */
/* Input        : pPktInfo, pHostCtl                                          */
/*                                                                           */
/* Output       : tIpAddr                                                    */
/*                                                                           */
/* Returns      : None                                             */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
DsmonHostApplyPrefix (tDsmonPktInfo * pPktInfo,
                      tDsmonHostCtl * pHostCtl, tIpAddr * pHostAddr)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostApplyPrefix\r\n");
    switch (pPktInfo->PktHdr.u1IpVersion)
    {
        case DSMON_IPV4_HDR_VER:
        {
            pHostAddr->u4_addr[3] = pPktInfo->PktHdr.SrcIp.u4_addr[3] &
                u4CidrSubnetMask[pHostCtl->u4DsmonHostCtlIPv4PrefixLen];
        }
            break;
        case DSMON_IPV6_HDR_VER:
        {
            DsmonHostApplyIPv6Prefix (pHostAddr, &pPktInfo->PktHdr.SrcIp,
                                      pHostCtl->u4DsmonHostCtlIPv6PrefixLen);
        }
            break;
        default:
            break;
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostApplyPrefix\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostProcessPktInfo                                 */
/*                                                                           */
/* Description  : Once incoming packet is recieved by Control Plane, for     */
/*          every valid pdist control entry, new data entry will be    */
/*          created and establish inter links                 */
/*                                                                           */
/* Input        : PktInfo                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonHostProcessPktInfo (tDsmonPktInfo * pPktInfo, UINT4 u4PktSize)
{
    tDsmonHostCtl      *pHostCtl = NULL;
    tDsmonHostStats    *pCurHost = NULL, *pPreviousHost = NULL, *pTravHost =
        NULL;
    UINT4               u4DsmonHostAggGroupIndex = 0, u4PrevPrefixLen =
        0, u4CurPrefixLen = 0;
    UINT4               u4HostCtlIndex = 0;
    tIpAddr             HostAddr;
    INT4                i4RetVal = OSIX_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostProcessPktInfo\r\n");

    if (pPktInfo->u4ProtoNLIndex == 0)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonHostProcessPktInfo Invalid NL Index\r\n");
        return i4RetVal;
    }

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    /* Check for valid Control Entry */
    pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);

    while (pHostCtl != NULL)
    {
        u4HostCtlIndex = pHostCtl->u4DsmonHostCtlIndex;

        if ((pHostCtl->u4DsmonHostCtlRowStatus == ACTIVE) &&
            ((pHostCtl->u4DsmonHostDataSource == pPktInfo->PktHdr.u4IfIndex) ||
             (pHostCtl->u4DsmonHostDataSource ==
              pPktInfo->PktHdr.u4VlanIfIndex))
            && (pPktInfo->PktHdr.u1DSCP < DSMON_MAX_AGG_PROFILE_DSCP))
        {
            if (pHostCtl->u4DsmonHostDataSource ==
                pPktInfo->PktHdr.u4VlanIfIndex)
            {
                pPktInfo->u1IsL3Vlan = OSIX_TRUE;
            }
            u4DsmonHostAggGroupIndex =
                pHostCtl->pAggCtl->au4DsmonAggProfile[pPktInfo->PktHdr.u1DSCP];

            /* Apply Prefix Length on Incoming Packet */

            DsmonHostApplyPrefix (pPktInfo, pHostCtl, &HostAddr);

            /* Check whether entry present already */

            pCurHost = DsmonHostGetDataEntry (u4HostCtlIndex,
                                              u4DsmonHostAggGroupIndex,
                                              pPktInfo->u4ProtoNLIndex,
                                              &HostAddr);

            if (pCurHost == NULL)
            {
                /* Check whether data entries reached max limit */
                if ((pHostCtl->u4DsmonHostCtlInserts -
                     pHostCtl->u4DsmonHostCtlDeletes)
                    >= pHostCtl->u4DsmonHostCtlMaxDesiredSupported)
                {
                    pCurHost = DsmonHostGiveLRUEntry
                        (pHostCtl->u4DsmonHostCtlIndex);
                    if (pCurHost != NULL)
                    {
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "Deleting Host LRU Entry\r\n");

                        /* Remove Inter table dependencies */
                        DsmonHostDelInterDep (pCurHost);

                        pCurHost->pHostCtl->u4DsmonHostCtlDeletes++;

                        /* After Reference changes, free Host stats entry */
                        DsmonHostDelDataEntry (pCurHost);

                        pCurHost = NULL;
                    }
                }

                /* Add new Host Entry */

                pCurHost = DsmonHostAddDataEntry (u4HostCtlIndex,
                                                  u4DsmonHostAggGroupIndex,
                                                  pPktInfo->u4ProtoNLIndex,
                                                  &HostAddr);
                if (pCurHost == NULL)
                {
                    pHostCtl->u4DsmonHostCtlDroppedFrames++;

                    /* Get next Stats Ctl */
                    pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);
                    continue;
                }

                pCurHost->pHostCtl = pHostCtl;

                if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV4_HDR_VER)
                {
                    pCurHost->u4DsmonHostIpAddrLen = DSMON_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV6_HDR_VER)
                {
                    pCurHost->u4DsmonHostIpAddrLen = DSMON_IPV6_MAX_LEN;
                }

                pHostCtl->u4DsmonHostCtlInserts++;
            }
            pCurHost->DsmonCurSample.u4DsmonHostOutPkts++;
            pCurHost->DsmonCurSample.u4DsmonHostOutOctets += u4PktSize;
            UINT8_ADD (pCurHost->DsmonCurSample.u8DsmonHostOutHCPkts, 1);
            UINT8_ADD (pCurHost->DsmonCurSample.u8DsmonHostOutHCOctets,
                       u4PktSize);
            pCurHost->u4DsmonHostTimeMark = OsixGetSysUpTime ();

            pCurHost->u4PktRefCount++;

            if (pPreviousHost == NULL)
            {
                pPreviousHost = pCurHost;
            }
            else
            {
                if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV4_HDR_VER)
                {
                    u4PrevPrefixLen =
                        pPreviousHost->pHostCtl->u4DsmonHostCtlIPv4PrefixLen;
                    u4CurPrefixLen = pHostCtl->u4DsmonHostCtlIPv4PrefixLen;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV6_HDR_VER)
                {
                    u4PrevPrefixLen =
                        pPreviousHost->pHostCtl->u4DsmonHostCtlIPv6PrefixLen;
                    u4CurPrefixLen = pHostCtl->u4DsmonHostCtlIPv6PrefixLen;
                }

                if (u4PrevPrefixLen <= u4CurPrefixLen)
                {
                    if (pPreviousHost->pPrevHost == NULL)
                    {
                        pPreviousHost->pPrevHost = pCurHost;
                    }

                    pCurHost->pNextHost = pPreviousHost;

                    pPreviousHost = pCurHost;
                }
                else
                {
                    pTravHost = pPreviousHost;

                    while (pTravHost->pNextHost != NULL)
                    {
                        pTravHost = pTravHost->pNextHost;
                    }

                    pTravHost->pNextHost = pCurHost;
                    if (pCurHost->pPrevHost == NULL)
                    {
                        pCurHost->pPrevHost = pTravHost;
                    }
                }
            }                    /* End of Else */
            i4RetVal = OSIX_SUCCESS;
        }                        /* End of If */

        pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);
    }                            /* End of While */

    /* Add a link with PktInfo */
    pPktInfo->pOutHost = pPreviousHost;

    /* Now find a host entry with Destination address and if so, 
     * establish a link between PktInfo and the Host entry */
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostProcessPktInfo\r\n");
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostUpdateAggCtlChgs                                  */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/*                tables entries into NOTREADY and cleanup all data tables.  */
/*                If u1DsmonAggControlStatus is TRUE, update control tables  */
/*                and resume data collection                                 */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostUpdateAggCtlChgs (UINT1 u1DsmonAggControlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tDsmonHostCtl      *pHostCtl = NULL;
    tDsmonAggCtl       *pAggCtl = NULL;
    UINT4               u4HostCtlIndex = 0;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonHostUpdateAggCtlChgs: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostUpdateAggCtlChgs \r\n");
    switch (u1DsmonAggControlStatus)
    {
        case DSMON_AGGLOCK_FALSE:
            /* Get First HostCtl Entry */
            pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);
            while (pHostCtl != NULL)
            {
                u4HostCtlIndex = pHostCtl->u4DsmonHostCtlIndex;
                if (pHostCtl->u4DsmonHostCtlRowStatus == ACTIVE)
                {
                    if (CfaGetIfInfo (pHostCtl->u4DsmonHostDataSource, &IfInfo)
                        != CFA_FAILURE)
                    {
                        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                            (CfaGetVlanId
                             (pHostCtl->u4DsmonHostDataSource,
                              &u2VlanId) != CFA_FAILURE))
                        {
                            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                        }
                    }

                    pHostCtl->u4DsmonHostCtlRowStatus = NOT_READY;
#ifdef NPAPI_WANTED
                    DsmonFsDSMONDisableProbe (pHostCtl->u4DsmonHostDataSource,
                                              u2VlanId, *pVlanPortList);
#endif
                }
                /* Get Next HostCtl Entry */
                pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);
            }

            /* Clear Host Statistics Table */
            RBTreeDrain (DSMON_HOST_TABLE, DsmonUtlRBFreeHostStats, 0);
            break;
        case DSMON_AGGLOCK_TRUE:
            /* Get First HostCtl Entry */
            pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);
            while (pHostCtl != NULL)
            {
                u4HostCtlIndex = pHostCtl->u4DsmonHostCtlIndex;
                /* Validate Aggregation Control Index */
                pAggCtl =
                    DsmonAggCtlGetEntry (pHostCtl->u4DsmonHostCtlProfileIndex);
                if ((pAggCtl != NULL)
                    && (pAggCtl->u4DsmonAggCtlRowStatus == ACTIVE))
                {
                    if (CfaGetIfInfo (pHostCtl->u4DsmonHostDataSource, &IfInfo)
                        == CFA_FAILURE)
                    {
                        /* ifindex is invalid */
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "DsmonHostUpdateAggCtlChgs: Invalid interface Index \n");
                        FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                        return;
                    }

                    if ((IfInfo.u1IfOperStatus != CFA_IF_UP)
                        || (IfInfo.u1IfAdminStatus != CFA_IF_UP))
                    {
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "DsmonHostUpdateAggCtlChgs: \
                Interface OperStatus or Admin Status is in invalid state \n");
                        FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                        return;
                    }

                    if (IfInfo.u1IfType == CFA_L3IPVLAN)
                    {
                        /* Get the VlanId from interface index */
                        if (CfaGetVlanId
                            (pHostCtl->u4DsmonHostDataSource,
                             &u2VlanId) == CFA_FAILURE)
                        {
                            DSMON_TRC1 (DSMON_DEBUG_TRC, "DsmonHostUpdateAggCtlChgs: \
                    Unable to Fetch VlanId of IfIndex %d\n",
                                        pHostCtl->u4DsmonHostDataSource);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return;
                        }

                        if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList)
                            == L2IWF_FAILURE)
                        {
                            DSMON_TRC1 (DSMON_DEBUG_TRC, "DsmonHostUpdateAggCtlChgs: \
                    Get Member Port List for VlanId %d failed \n",
                                        u2VlanId);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return;
                        }
                    }
#ifdef NPAPI_WANTED
                    DsmonFsDSMONEnableProbe (pHostCtl->u4DsmonHostDataSource,
                                             u2VlanId, *pVlanPortList);
#endif
                    /* Set back RowStatus of HostCtl as ACTIVE */
                    pHostCtl->u4DsmonHostCtlRowStatus = ACTIVE;
                }
                pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);
            }
            break;
        default:
            break;
    }
    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostUpdateAggCtlChgs \r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostUpdateProtoChgs                                   */
/*                                                                           */
/* Description  : If RMONv2 removes an existing protocol support, the same   */
/*          needs to be updated on Host Data Table             */
/*                                                                           */
/* Input        : u1DsmonProtoLocalIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostUpdateProtoChgs (UINT4 u4DsmonHostProtDirLocalIndex)
{
    tDsmonHostStats    *pHostStats = NULL;
    UINT4               u4DsmonHostCtlIndex = 0, u4DsmonHostGrpIndex = 0;
    UINT4               u4DsmonHostProtoIndex = 0;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostUpdateProtoChgs \r\n");
    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    /* Get First Entry in Host Table */

    pHostStats = DsmonHostGetNextDataIndex
        (u4DsmonHostCtlIndex,
         u4DsmonHostGrpIndex, u4DsmonHostProtoIndex, &HostAddr);

    while (pHostStats != NULL)
    {
        u4DsmonHostCtlIndex = pHostStats->u4DsmonHostCtlIndex;
        u4DsmonHostGrpIndex = pHostStats->u4DsmonHostAggGroupIndex;
        u4DsmonHostProtoIndex = pHostStats->u4DsmonHostProtDirLocalIndex;
        MEMCPY (&HostAddr, &pHostStats->DsmonHostNLAddress, sizeof (tIpAddr));

        if (pHostStats->u4DsmonHostProtDirLocalIndex ==
            u4DsmonHostProtDirLocalIndex)
        {
            /* Remove Inter table dependencies */
            DsmonHostDelInterDep (pHostStats);

            pHostStats->pHostCtl->u4DsmonHostCtlDeletes++;

            /* After Reference changes, free Host stats entry */
            DsmonHostDelDataEntry (pHostStats);
        }

        pHostStats = DsmonHostGetNextDataIndex (u4DsmonHostCtlIndex,
                                                u4DsmonHostGrpIndex,
                                                u4DsmonHostProtoIndex,
                                                &HostAddr);
    }                            /* End of While */
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostUpdateProtoChgs \r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostUpdateIfaceChgs                                   */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then set appropriate control */
/*                entries to NOT_READY state thus deleting the associated    */
/*                Data Table. If OperStatus is CFA_IF_UP, set NOT_READY      */
/*                entries to ACTIVE state                                    */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostUpdateIfaceChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tDsmonHostCtl      *pHostCtl = NULL;
    UINT4               u4HostCtlIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostUpdateIfaceChgs \r\n");
    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:
            /* Get First Host Control Table */

            pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);

            while (pHostCtl != NULL)
            {
                u4HostCtlIndex = pHostCtl->u4DsmonHostCtlIndex;
                if ((pHostCtl->u4DsmonHostCtlRowStatus == ACTIVE) &&
                    (pHostCtl->u4DsmonHostDataSource == u4IfIndex))
                {
                    /* Remove Data Entries for this Control Index */

                    DsmonHostDelDataEntries (u4HostCtlIndex);

                    /* Set RowStatus as NOT_READY */

                    pHostCtl->u4DsmonHostCtlRowStatus = NOT_READY;
                }

                pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);
            }
            break;
        case CFA_IF_UP:
            /* Get First Host Control Table */
            pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);

            while (pHostCtl != NULL)
            {
                u4HostCtlIndex = pHostCtl->u4DsmonHostCtlIndex;
                if ((pHostCtl->u4DsmonHostCtlRowStatus == NOT_READY) &&
                    (pHostCtl->u4DsmonHostDataSource == u4IfIndex))
                {
                    /* Set RowStatus as ACTIVE */
                    nmhSetDsmonHostCtlStatus (u4HostCtlIndex, ACTIVE);
                }

                pHostCtl = DsmonHostGetNextCtlIndex (u4HostCtlIndex);
            }
            break;
        default:
            break;
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostUpdateIfaceChgs \r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostGetNextCtlIndex                                   */
/*                                                                           */
/* Description  : Get First / Next Host Ctl Index                  */
/*                                                                           */
/* Input        : u4DsmonHostCtlIndex                                        */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostCtl / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostCtl *
DsmonHostGetNextCtlIndex (UINT4 u4DsmonHostCtlIndex)
{
    tDsmonHostCtl      *pHostCtl = NULL;
    tDsmonHostCtl       HostCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostGetNextCtlIndex \r\n");
    MEMSET (&HostCtl, DSMON_INIT_VAL, sizeof (tDsmonHostCtl));

    HostCtl.u4DsmonHostCtlIndex = u4DsmonHostCtlIndex;
    pHostCtl = (tDsmonHostCtl *) RBTreeGetNext (DSMON_HOSTCTL_TABLE,
                                                (tRBElem *) & HostCtl, NULL);

    return pHostCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostAddCtlEntry                                       */
/*                                                                           */
/* Description  : Allocates memory and Adds Host Control Entry into          */
/*          dsmonHostCtlRBTree                           */
/*                                                                           */
/* Input        : pHostCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostCtl / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostCtl *
DsmonHostAddCtlEntry (UINT4 u4DsmonHostControlIndex)
{
    tDsmonHostCtl      *pHostCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostAddCtlEntry\r\n");
    if ((pHostCtl = (tDsmonHostCtl *)
         (MemAllocMemBlk (DSMON_HOSTCTL_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Host. Ctl.\r\n");
        return NULL;
    }

    MEMSET (pHostCtl, DSMON_INIT_VAL, sizeof (tDsmonHostCtl));

    pHostCtl->u4DsmonHostCtlIndex = u4DsmonHostControlIndex;
    pHostCtl->u4DsmonHostCtlIPv4PrefixLen = DSMON_IPV4PREFIXLEN_MAX;
    pHostCtl->u4DsmonHostCtlIPv6PrefixLen = DSMON_IPV6PREFIXLEN_MAX;
    pHostCtl->u4DsmonHostCtlRowStatus = NOT_READY;
    pHostCtl->i4DsmonHostCtlMaxDesiredEntries = DSMON_MAX_DATA_PER_CTL;
    pHostCtl->u4DsmonHostCtlMaxDesiredSupported = DSMON_MAX_DATA_PER_CTL;

    if (RBTreeAdd (DSMON_HOSTCTL_TABLE, (tRBElem *) pHostCtl) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonHostAddCtlEntry: Host Entry is added\r\n");
        return pHostCtl;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonHostAddCtlEntry: Entry addition Failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_HOSTCTL_POOL, (UINT1 *) pHostCtl);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostDelCtlEntry                                       */
/*                                                                           */
/* Description  : Releases memory and Removes Host Control Entry from        */
/*          dsmonHostCtlRBTree                           */
/*                                                                           */
/* Input        : pHostCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonHostDelCtlEntry (tDsmonHostCtl * pHostCtl)
{
    tPortList          *pVlanPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonHostDelCtlEntry: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostDelCtlEntry\r\n");
    if (CfaGetIfInfo (pHostCtl->u4DsmonHostDataSource, &IfInfo) != CFA_FAILURE)
    {
        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
            (CfaGetVlanId (pHostCtl->u4DsmonHostDataSource, &u2VlanId) !=
             CFA_FAILURE))
        {
            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
        }
    }
#ifdef NPAPI_WANTED
    /* Remove Probe support from H/W Table */
    DsmonFsDSMONDisableProbe (pHostCtl->u4DsmonHostDataSource, u2VlanId,
                              *pVlanPortList);
#endif
    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    /* Remove Data Entries for this control index from Host Table */
    DsmonHostDelDataEntries (pHostCtl->u4DsmonHostCtlIndex);

    /* Remove Host. Ctl node from dsmonHostCtlRBTree */
    RBTreeRem (DSMON_HOSTCTL_TABLE, (tRBElem *) pHostCtl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_HOSTCTL_POOL,
                            (UINT1 *) pHostCtl) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Host. Ctl.\r\n");
        return OSIX_FAILURE;
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostDelCtlEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostGetCtlEntry                                       */
/*                                                                           */
/* Description  : Get Host Control Entry for the given control index         */
/*                                                                           */
/* Input        : u4DsmonHostControlIndex                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostCtl / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostCtl *
DsmonHostGetCtlEntry (UINT4 u4DsmonHostControlIndex)
{
    tDsmonHostCtl      *pHostCtl = NULL;
    tDsmonHostCtl       HostCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostGetCtlEntry \r\n");
    MEMSET (&HostCtl, DSMON_INIT_VAL, sizeof (tDsmonHostCtl));

    HostCtl.u4DsmonHostCtlIndex = u4DsmonHostControlIndex;

    pHostCtl = (tDsmonHostCtl *) RBTreeGet (DSMON_HOSTCTL_TABLE,
                                            (tRBElem *) & HostCtl);
    return pHostCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostGetNextDataIndex                                  */
/*                                                                           */
/* Description  : Get First / Next Host Stats Index                       */
/*                                                                           */
/* Input        : u4DsmonHostCtlIndex                                        */
/*          u4DsmonHostAggGroupIndex                     */
/*          u4DsmonHostProtDirLocalIndex                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostStats / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostStats *
DsmonHostGetNextDataIndex (UINT4 u4DsmonHostCtlIndex,
                           UINT4 u4DsmonHostAggGroupIndex,
                           UINT4 u4DsmonHostProtDirLocalIndex,
                           tIpAddr * pHostAddr)
{
    tDsmonHostStats    *pHostStats = NULL;
    tDsmonHostStats     HostStats;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostGetNextDataIndex \r\n");

    MEMSET (&HostStats, DSMON_INIT_VAL, sizeof (tDsmonHostStats));

    if ((u4DsmonHostCtlIndex != 0) && (u4DsmonHostProtDirLocalIndex == 0))
    {
        pHostStats = (tDsmonHostStats *) RBTreeGetFirst (DSMON_HOST_TABLE);
        while (pHostStats != NULL)
        {
            if (pHostStats->u4DsmonHostCtlIndex == u4DsmonHostCtlIndex)
            {
                break;
            }

            MEMCPY (&HostStats, pHostStats, sizeof (tDsmonHostStats));

            pHostStats = (tDsmonHostStats *) RBTreeGetNext (DSMON_HOST_TABLE,
                                                            (tRBElem *) &
                                                            HostStats, NULL);
        }
    }
    else
    {
        HostStats.u4DsmonHostCtlIndex = u4DsmonHostCtlIndex;
        HostStats.u4DsmonHostAggGroupIndex = u4DsmonHostAggGroupIndex;
        HostStats.u4DsmonHostProtDirLocalIndex = u4DsmonHostProtDirLocalIndex;
        MEMCPY (&HostStats.DsmonHostNLAddress, pHostAddr, sizeof (tIpAddr));

        pHostStats = (tDsmonHostStats *) RBTreeGetNext (DSMON_HOST_TABLE,
                                                        (tRBElem *) & HostStats,
                                                        NULL);
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostGetNextDataIndex \r\n");
    return pHostStats;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostAddDataEntry                                      */
/*                                                                           */
/* Description  : Allocates memory and Adds Host Stats Entry into              */
/*          dsmonHostRBTree                               */
/*                                                                           */
/* Input        : pHostStats                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostStats / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostStats *
DsmonHostAddDataEntry (UINT4 u4DsmonHostControlIndex,
                       UINT4 u4DsmonHostAggGroupIndex,
                       UINT4 u4DsmonHostProtDirLocalIndex, tIpAddr * pHostAddr)
{
    tDsmonHostStats    *pHostStats = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostAddDataEntry\r\n");

    if ((pHostStats = (tDsmonHostStats *)
         (MemAllocMemBlk (DSMON_HOST_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Host.\r\n");
        return NULL;
    }

    MEMSET (pHostStats, DSMON_INIT_VAL, sizeof (tDsmonHostStats));

    pHostStats->u4DsmonHostCtlIndex = u4DsmonHostControlIndex;
    pHostStats->u4DsmonHostAggGroupIndex = u4DsmonHostAggGroupIndex;
    pHostStats->u4DsmonHostProtDirLocalIndex = u4DsmonHostProtDirLocalIndex;
    pHostStats->u4DsmonHostTimeMark = (UINT4) OsixGetSysUpTime ();
    pHostStats->u4DsmonHostCreateTime = OsixGetSysUpTime ();
    MEMCPY (&pHostStats->DsmonHostNLAddress, pHostAddr, sizeof (tIpAddr));

    if (RBTreeAdd (DSMON_HOST_TABLE, (tRBElem *) pHostStats) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonHostAddDataEntry Host Entry is added\r\n");
        return pHostStats;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonHostAddDataEntry Entry Addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_HOST_POOL, (UINT1 *) pHostStats);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostDelDataEntry                                      */
/*                                                                           */
/* Description  : Releases memory and Removes Host Stats Entry from          */
/*          dsmonHostRBTree                                    */
/*                                                                           */
/* Input        : pHostStats                                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonHostDelDataEntry (tDsmonHostStats * pHostStats)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostDelDataEntry\r\n");
    /* Remove Host. node from dsmonHostRBTree */
    RBTreeRem (DSMON_HOST_TABLE, (tRBElem *) pHostStats);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_HOST_POOL,
                            (UINT1 *) pHostStats) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Host. Stats\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostDelDataEntries                                    */
/*                                                                           */
/* Description  : Removes Host Entries from dsmonHostRBTree for given        */
/*                Control Index                                              */
/*                                                                           */
/* Input        : u4HostCtlIndex                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostDelDataEntries (UINT4 u4HostCtlIndex)
{
    tDsmonHostStats    *pHostStats = NULL;
    UINT4               u4DsmonHostAggGroupIndex =
        0, u4DsmonHostProtDirLocalIndex = 0;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostDelDataEntries\r\n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    pHostStats = DsmonHostGetNextDataIndex (u4HostCtlIndex,
                                            u4DsmonHostAggGroupIndex,
                                            u4DsmonHostProtDirLocalIndex,
                                            &HostAddr);

    while (pHostStats != NULL)
    {
        if (pHostStats->u4DsmonHostCtlIndex != u4HostCtlIndex)
        {
            break;
        }

        u4DsmonHostAggGroupIndex = pHostStats->u4DsmonHostAggGroupIndex;
        u4DsmonHostProtDirLocalIndex = pHostStats->u4DsmonHostProtDirLocalIndex;
        MEMCPY (&HostAddr, &pHostStats->DsmonHostNLAddress, sizeof (tIpAddr));

        /* Remove Inter table dependencies */
        DsmonHostDelInterDep (pHostStats);

        pHostStats->pHostCtl->u4DsmonHostCtlDeletes++;

        /* After Reference changes, free stats entry */
        DsmonHostDelDataEntry (pHostStats);

        pHostStats = DsmonHostGetNextDataIndex (u4HostCtlIndex,
                                                u4DsmonHostAggGroupIndex,
                                                u4DsmonHostProtDirLocalIndex,
                                                &HostAddr);
    }

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: EXIT DsmonHostDelDataEntries\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostGetDataEntry                                      */
/*                                                                           */
/* Description  : Get Host Stats for the given control index                 */
/*                                                                           */
/* Input        : u4DsmonHostControlIndex                                    */
/*          u4DsmonHostAggGroupIndex                     */
/*          u4DsmonHostProtDirLocalIndex                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostStats / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostStats *
DsmonHostGetDataEntry (UINT4 u4DsmonHostControlIndex,
                       UINT4 u4DsmonHostAggGroupIndex,
                       UINT4 u4DsmonHostProtDirLocalIndex, tIpAddr * pHostAddr)
{
    tDsmonHostStats    *pHostStats = NULL;
    tDsmonHostStats     HostStats;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostGetDataEntry\r\n");
    MEMSET (&HostStats, DSMON_INIT_VAL, sizeof (tDsmonHostStats));

    HostStats.u4DsmonHostCtlIndex = u4DsmonHostControlIndex;
    HostStats.u4DsmonHostAggGroupIndex = u4DsmonHostAggGroupIndex;
    HostStats.u4DsmonHostProtDirLocalIndex = u4DsmonHostProtDirLocalIndex;
    MEMCPY (&HostStats.DsmonHostNLAddress, pHostAddr, sizeof (tIpAddr));

    pHostStats = (tDsmonHostStats *) RBTreeGet (DSMON_HOST_TABLE,
                                                (tRBElem *) & HostStats);
    return pHostStats;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostDelInterDep                                       */
/*                                                                           */
/* Description  : Removes the inter-dependencies and re-order the links         */
/*                                                                           */
/* Input        : pHost                                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostDelInterDep (tDsmonHostStats * pHostStats)
{
    tDsmonPktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostDelInterDep\r\n");
    MEMSET (&PktHdr, DSMON_INIT_VAL, sizeof (tPktHeader));

    if (pHostStats->pPrevHost == NULL)
    {
        /* Update PktInfo nodes to point next pHost Entry */
        pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            MEMCPY (&PktHdr, &pPktInfo->PktHdr, sizeof (tPktHeader));

            if (pPktInfo->pOutHost == pHostStats)
            {
                pPktInfo->pOutHost = pHostStats->pNextHost;
                if (pPktInfo->pOutHost != NULL)
                {
                    pPktInfo->pOutHost->pPrevHost = NULL;
                }
            }
            pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);
        }
    }
    else if (pHostStats->pNextHost != NULL)
    {
        pHostStats->pPrevHost->pNextHost = pHostStats->pNextHost;
    }
    else
    {
        pHostStats->pPrevHost->pNextHost = NULL;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostDelInterDep\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostPopulateDataEntries                               */
/*                                                                           */
/* Description  : Populates data entries if the same data source is already  */
/*          present and Adds the inter-dependencies b/w other tables   */
/*                                                                           */
/* Input        : pHostCtl                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostPopulateDataEntries (tDsmonHostCtl * pHostCtl)
{
    tDsmonPktInfo      *pPktInfo = NULL;
    tDsmonHostStats    *pHost = NULL, *pNewHost = NULL;
    tIpAddr             HostAddr;
    tPktHeader          PktHdr;
    UINT4               u4DsmonAggGroupIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonHostPopulateDataEntries\r\n");
    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
    MEMSET (&PktHdr, DSMON_INIT_VAL, sizeof (tPktHeader));

    /* Check for the presence of data source in pktinfo table */
    pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);
    while (pPktInfo != NULL)
    {
        if ((pPktInfo->PktHdr.u4IfIndex == pHostCtl->u4DsmonHostDataSource) &&
            (pPktInfo->PktHdr.u1DSCP < DSMON_MAX_AGG_PROFILE_DSCP) &&
            (pPktInfo->u4ProtoNLIndex != 0))
        {
            u4DsmonAggGroupIndex =
                pHostCtl->pAggCtl->au4DsmonAggProfile[pPktInfo->PktHdr.u1DSCP];

            DsmonHostApplyPrefix (pPktInfo, pHostCtl, &HostAddr);

            /* Check whether data entries reached max limit */
            if ((pHostCtl->u4DsmonHostCtlInserts -
                 pHostCtl->u4DsmonHostCtlDeletes)
                >= pHostCtl->u4DsmonHostCtlMaxDesiredSupported)
            {
                pNewHost = DsmonHostGiveLRUEntry
                    (pHostCtl->u4DsmonHostCtlIndex);
                if (pNewHost != NULL)
                {
                    DSMON_TRC (DSMON_DEBUG_TRC, "Deleting Host LRU Entry\r\n");

                    /* Remove Inter table dependencies */
                    DsmonHostDelInterDep (pNewHost);

                    pNewHost->pHostCtl->u4DsmonHostCtlDeletes++;

                    /* After Reference changes, free Host stats entry */
                    DsmonHostDelDataEntry (pNewHost);

                    pNewHost = NULL;
                }
            }

            /* Add Host Entry */
            pNewHost = DsmonHostAddDataEntry (pHostCtl->u4DsmonHostCtlIndex,
                                              u4DsmonAggGroupIndex,
                                              pPktInfo->u4ProtoNLIndex,
                                              &HostAddr);
            if (pNewHost == NULL)
            {
                pHostCtl->u4DsmonHostCtlDroppedFrames++;

                pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
                continue;
            }
            pNewHost->pHostCtl = pHostCtl;

            if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV4_HDR_VER)
            {
                pNewHost->u4DsmonHostIpAddrLen = DSMON_IPV4_MAX_LEN;
            }
            else if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV6_HDR_VER)
            {
                pNewHost->u4DsmonHostIpAddrLen = DSMON_IPV6_MAX_LEN;
            }

            pHostCtl->u4DsmonHostCtlInserts++;

            pHost = pPktInfo->pOutHost;
            if (pHost == NULL)
            {
                pPktInfo->pOutHost = pNewHost;
            }
            else
            {
                while (pHost->pNextHost != NULL)
                {
                    pHost = pHost->pNextHost;
                }

                pHost->pNextHost = pNewHost;
                pNewHost->pPrevHost = pHost;
            }

        }                        /* End of If */

        pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);

    }                            /* End of While - PktInfo Table */
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonHostPopulateDataEntries\r\n");

    return;
}
