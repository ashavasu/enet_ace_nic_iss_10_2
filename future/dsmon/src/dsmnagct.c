/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved 
 *
 * Description:This file contains the functional routines for the     
 *             Counter Aggregation Control Module of the DSMON.
 *
 *******************************************************************/
#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlHandleLckChgs                                   */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/*           tables entries into NOTREADY and cleanup all data tables.  */
/*          If u1DsmonAggControlStatus is TRUE, update control tables  */
/*          and resume data collection                     */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonAggCtlHandleLckChgs (UINT1 u1DsmonAggControlStatus)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlHandleLckChgs\r\n");

    /* Clear PktInfo Table */
    DsmonPktInfoRemoveEntries (u1DsmonAggControlStatus);

    /* Update Stats Tables */
    DsmonStatsUpdateAggCtlChgs (u1DsmonAggControlStatus);

    /* Update Pdist Tables */
    DsmonPdistUpdateAggCtlChgs (u1DsmonAggControlStatus);

    /* Update Pdist TopN Tables */
    DsmonPdistTopNUpdateAggCtlChgs (u1DsmonAggControlStatus);

    /* Update Host Tables */
    DsmonHostUpdateAggCtlChgs (u1DsmonAggControlStatus);

    /* Update Host TopN Tables */
    DsmonHostTopNUpdateAggCtlChgs (u1DsmonAggControlStatus);

    /* Update Matrix Tables */
    DsmonMatrixUpdateAggCtlChgs (u1DsmonAggControlStatus);

    /* Update Matrix TopN Tables */
    DsmonMatrixTopNUpdateAggCtlChgs (u1DsmonAggControlStatus);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlHandleLckChgs\r\n");

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlGetNextIndex                                    */
/*                                                                           */
/* Description  : Get First / Next Agg Ctl Index                  */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggCtl / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggCtl *
DsmonAggCtlGetNextIndex (UINT4 u4DsmonAggControlIndex)
{
    tDsmonAggCtl       *pAggCtl = NULL;
    tDsmonAggCtl        AggCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlGetNextIndex\r\n");

    MEMSET (&AggCtl, DSMON_INIT_VAL, sizeof (tDsmonAggCtl));

    AggCtl.u4DsmonAggCtlIndex = u4DsmonAggControlIndex;

    pAggCtl = (tDsmonAggCtl *) RBTreeGetNext (DSMON_AGGCTL_TABLE,
                                              (tRBElem *) & AggCtl, NULL);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlGetNextIndex\r\n");

    return pAggCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlAddEntry                                        */
/*                                                                           */
/* Description  : Allocates memory and Adds Aggregation Control Entry into   */
/*          dsmonAggCtlRBTree                           */
/*                                                                           */
/* Input        : pAggCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggCtl / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggCtl *
DsmonAggCtlAddEntry (UINT4 u4DsmonAggControlIndex)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlAddEntry\r\n");

    if ((pAggCtl = (tDsmonAggCtl *)
         (MemAllocMemBlk (DSMON_AGGCTL_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "DsmonAggCtlAddEntry: Memory Allocation failed for Agg. Ctl.\r\n");
        return NULL;
    }

    MEMSET (pAggCtl, DSMON_INIT_VAL, sizeof (tDsmonAggCtl));

    pAggCtl->u4DsmonAggCtlIndex = u4DsmonAggControlIndex;

    /* Initiate Prfile Table with default Group Index - 0 */

    MEMSET (pAggCtl->au4DsmonAggProfile, DSMON_INIT_VAL,
            sizeof (pAggCtl->au4DsmonAggProfile));

    if (RBTreeAdd (DSMON_AGGCTL_TABLE, (tRBElem *) pAggCtl) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonAggCtlAddEntry: Entry added in Agg. Ctl. Tree\r\n");
        return pAggCtl;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonAggCtlAddEntry: Agg. Ctl. Addition Failed\r\n");

    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_AGGCTL_POOL, (UINT1 *) pAggCtl);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlAddEntry\r\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlDelEntry                                        */
/*                                                                           */
/* Description  : Releases memory and Removes Aggregation Control Entry from */
/*          dsmonAggCtlRBTree                           */
/*                                                                           */
/* Input        : pAggCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonAggCtlDelEntry (tDsmonAggCtl * pAggCtl)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlDelEntry\r\n");

    /* Remove Agg. Ctl node from dsmonAggCtlRBTree */
    RBTreeRem (DSMON_AGGCTL_TABLE, (tRBElem *) pAggCtl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_AGGCTL_POOL,
                            (UINT1 *) pAggCtl) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Agg. Ctl.\r\n");
        return OSIX_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlDelEntry\r\n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlGetEntry                                        */
/*                                                                           */
/* Description  : Get Aggregation Control Entry for the given control index  */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggCtl / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggCtl *
DsmonAggCtlGetEntry (UINT4 u4DsmonAggControlIndex)
{
    tDsmonAggCtl       *pAggCtl = NULL;
    tDsmonAggCtl        AggCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlGetEntry\r\n");

    MEMSET (&AggCtl, DSMON_INIT_VAL, sizeof (tDsmonAggCtl));

    AggCtl.u4DsmonAggCtlIndex = u4DsmonAggControlIndex;

    pAggCtl = (tDsmonAggCtl *) RBTreeGet (DSMON_AGGCTL_TABLE,
                                          (tRBElem *) & AggCtl);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlGetEntry\r\n");
    return pAggCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlGetNextGrpIndex                                 */
/*                                                                           */
/* Description  : Get First / Next Agg.Group Index for the given index       */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggGroup / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggGroup *
DsmonAggCtlGetNextGrpIndex (UINT4 u4DsmonAggControlIndex,
                            UINT4 u4DsmonAggGroupIndex)
{
    tDsmonAggGroup     *pAggGrp = NULL;
    tDsmonAggGroup      AggGrp;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlGetNextGrpIndex\r\n");

    MEMSET (&AggGrp, DSMON_INIT_VAL, sizeof (tDsmonAggGroup));

    AggGrp.u4DsmonAggCtlIndex = u4DsmonAggControlIndex;
    AggGrp.u4DsmonAggGroupIndex = u4DsmonAggGroupIndex;
    pAggGrp = (tDsmonAggGroup *) RBTreeGetNext (DSMON_AGGCTL_GROUP_TABLE,
                                                (tRBElem *) & AggGrp, NULL);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlGetNextGrpIndex\r\n");

    return pAggGrp;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlAddGrpEntry                                     */
/*                                                                           */
/* Description  : Adds Aggregation Group Entry into dsmonAggGroupRBTree      */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggGroup / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggGroup *
DsmonAggCtlAddGrpEntry (UINT4 u4DsmonAggControlIndex,
                        UINT4 u4DsmonAggGroupIndex)
{
    tDsmonAggGroup     *pAggGrp = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlAddGrpEntry\r\n");

    if ((pAggGrp = (tDsmonAggGroup *)
         (MemAllocMemBlk (DSMON_AGGCTL_GROUP_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Agg. Group.\r\n");
        return NULL;
    }

    MEMSET (pAggGrp, DSMON_INIT_VAL, sizeof (tDsmonAggGroup));

    pAggGrp->u4DsmonAggCtlIndex = u4DsmonAggControlIndex;
    pAggGrp->u4DsmonAggGroupIndex = u4DsmonAggGroupIndex;

    if (RBTreeAdd (DSMON_AGGCTL_GROUP_TABLE, (tRBElem *) pAggGrp) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "Entry Added in Aggregation Group Tree\r\n");
        return pAggGrp;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonAggCtlAddGrpEntry: Agg. Group Addition Failed\r\n");

    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_AGGCTL_GROUP_POOL, (UINT1 *) pAggGrp);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlAddGrpEntry\r\n");
    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlDelGrpEntry                                     */
/*                                                                           */
/* Description  : Releases memory and Removes Aggregation Group Entry from   */
/*          dsmonAggGroupRBTree                           */
/*                                                                           */
/* Input        : pAggGrp                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonAggCtlDelGrpEntry (tDsmonAggGroup * pAggGrp)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlDelGrpEntry\r\n");

    /* Remove Agg. Grp node from dsmonAggCtlRBTree */
    RBTreeRem (DSMON_AGGCTL_GROUP_TABLE, (tRBElem *) pAggGrp);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_AGGCTL_GROUP_POOL,
                            (UINT1 *) pAggGrp) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Agg. Grp.\r\n");
        return OSIX_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlDelGrpEntry\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlGetGrpEntry                                     */
/*                                                                           */
/* Description  : Get Aggregation Group Entry for the given control index    */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonAggGroup / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonAggGroup *
DsmonAggCtlGetGrpEntry (UINT4 u4DsmonAggControlIndex,
                        UINT4 u4DsmonAggGroupIndex)
{
    tDsmonAggGroup     *pAggGrp = NULL;
    tDsmonAggGroup      AggGrp;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonAggCtlGetGrpEntry\r\n");

    MEMSET (&AggGrp, DSMON_INIT_VAL, sizeof (tDsmonAggGroup));

    AggGrp.u4DsmonAggCtlIndex = u4DsmonAggControlIndex;
    AggGrp.u4DsmonAggGroupIndex = u4DsmonAggGroupIndex;
    pAggGrp = (tDsmonAggGroup *) RBTreeGet (DSMON_AGGCTL_GROUP_TABLE,
                                            (tRBElem *) & AggGrp);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonAggCtlGetGrpEntry\r\n");
    return pAggGrp;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlGetGrpToCtlIdxAndDscp                         */
/*                                                                           */
/* Description  : This function provides the next aggregate group index      */
/*          to the provided group index corresponding to the input     */
/*          control index.                         */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex               */
/*                                                                           */
/* Output       : pu4AggGroupIndex                                           */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonAggCtlGetGrpToCtlIdxAndDscp (UINT4 u4AggCtlIndex,
                                  UINT4 u4AggDSCP, UINT4 *pu4AggGroupIndex)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonAggCtlGetGrpToCtlIdxAndDscp\r\n");

    pAggCtl = DsmonAggCtlGetEntry (u4AggCtlIndex);

    if (pAggCtl != NULL)
    {
        *pu4AggGroupIndex = pAggCtl->au4DsmonAggProfile[u4AggDSCP];

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonAggCtlGetGrpToCtlIdxAndDscp: Agg. Ctl. Entry found\r\n");
        return OSIX_SUCCESS;
    }

    DSMON_TRC (DSMON_DEBUG_TRC,
               "DsmonAggCtlGetGrpToCtlIdxAndDscp: Agg. Ctl. Entry not found\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonAggCtlSetGrpToCtlIdxAndDscp                           */
/*                                                                           */
/* Description  : This function provides the next aggregate group index      */
/*                to the provided group index corresponding to the input     */
/*                control index.                                             */
/*                                                                           */
/* Input        : u4DsmonAggControlIndex, u4DsmonAggGroupIndex               */
/*                                                                           */
/* Output       : pu4AggGroupIndex                                           */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonAggCtlSetGrpToCtlIdxAndDscp (UINT4 u4AggCtlIndex,
                                  UINT4 u4AggDSCP, UINT4 u4AggGroupIndex)
{
    tDsmonAggCtl       *pAggCtl = NULL;
    INT4                i4Index = 0;
    UINT1               u1IsFound = FALSE;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonAggCtlSetGrpToCtlIdxAndDscp\r\n");

    pAggCtl = DsmonAggCtlGetEntry (u4AggCtlIndex);

    if (pAggCtl != NULL)
    {
        if (pAggCtl->u4DsmonAggGroupsConfigured <= DSMON_MAX_AGG_GROUPS)
        {
            for (; i4Index < DSMON_MAX_AGG_PROFILE_DSCP; i4Index++)
            {
                if (pAggCtl->au4DsmonAggProfile[i4Index] == u4AggGroupIndex)
                {
                    u1IsFound = TRUE;
                    break;
                }
            }
            pAggCtl->au4DsmonAggProfile[u4AggDSCP] = u4AggGroupIndex;
            if (u1IsFound == FALSE)
            {
                pAggCtl->u4DsmonAggGroupsConfigured++;
            }
        }
        else
        {
            DSMON_TRC (DSMON_CRITICAL_TRC,
                       "Maximum Groups are configured for this Aggregation \
            control entry\r\n");
            return OSIX_FAILURE;
        }

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonAggCtlSetGrpToCtlIdxAndDscp: Agg. Ctl. Entry found\r\n");
        return OSIX_SUCCESS;
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "DsmonAggCtlSetGrpToCtlIdxAndDscp: Agg. Ctl. Entry not found\r\n");
    return OSIX_FAILURE;
}
