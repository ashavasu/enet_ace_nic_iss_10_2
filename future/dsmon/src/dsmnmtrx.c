/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *                                                                  *
 * $Id: dsmnmtrx.c,v 1.10 2012/12/13 14:25:10 siva Exp $           *
 *
 * Description: This file contains the functional routines         *
 *        for DSMON Matrix module                                *
 *                                                                  *
 *******************************************************************/

#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixGiveLRUEntry                                    */
/*                                                                           */
/* Description  : Gives the LRU entry                                        */
/*                                                                           */
/* Input        : u4MatrixCtlIndex                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Valid Matrix Data Entry / NULL                             */
/*                                                                           */
/*****************************************************************************/
PRIVATE tDsmonMatrixSD *
DsmonMatrixGiveLRUEntry (UINT4 u4MatrixCtlIndex)
{
    tDsmonMatrixSD     *pMatrixSD = NULL, *pLRUEntry = NULL;
    UINT4               u4DsmonMatrixAggGroupIndex = 0, u4DsmonMatrixNLIndex =
        0, u4DsmonMatrixALIndex = 0;
    tIpAddr             SrcIp, DstIp;

    MEMSET (&SrcIp, DSMON_INIT_VAL, sizeof (tIpAddr));
    MEMSET (&DstIp, DSMON_INIT_VAL, sizeof (tIpAddr));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixGiveLRUEntry\r\n");

    /* Get First Entry in Matrix Table */
    pMatrixSD = DsmonMatrixGetNextDataIndex
        (DSMON_MATRIXSD_TABLE,
         u4MatrixCtlIndex,
         u4DsmonMatrixAggGroupIndex,
         u4DsmonMatrixNLIndex, u4DsmonMatrixALIndex, &SrcIp, &DstIp);

    /* Mark First Entry as LRU entry */
    pLRUEntry = pMatrixSD;

    while (pMatrixSD != NULL)
    {
        if (pMatrixSD->u4DsmonMatrixCtlIndex != u4MatrixCtlIndex)
        {
            break;
        }
        u4DsmonMatrixAggGroupIndex = pMatrixSD->u4DsmonMatrixSDGroupIndex;
        u4DsmonMatrixNLIndex = pMatrixSD->u4DsmonMatrixSDNLIndex;
        u4DsmonMatrixALIndex = pMatrixSD->u4DsmonMatrixSDALIndex;

        MEMCPY (&SrcIp, &pMatrixSD->DsmonMatrixSDSourceAddress,
                sizeof (tIpAddr));
        MEMCPY (&DstIp, &pMatrixSD->DsmonMatrixSDDestAddress, sizeof (tIpAddr));

        if (pLRUEntry->u4DsmonMatrixSDTimeMark >
            pMatrixSD->u4DsmonMatrixSDTimeMark)
        {
            pLRUEntry = pMatrixSD;
        }

        pMatrixSD = DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                                 u4MatrixCtlIndex,
                                                 u4DsmonMatrixAggGroupIndex,
                                                 u4DsmonMatrixNLIndex,
                                                 u4DsmonMatrixALIndex,
                                                 &SrcIp, &DstIp);

    }                            /* End of While */
    return pLRUEntry;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixProcessPktInfo                                  */
/*                                                                           */
/* Description  : Once incoming packet is recieved by Control Plane, for     */
/*          every valid pdist control entry, new data entry will be    */
/*          created and establish inter links                 */
/*                                                                           */
/* Input        : PktInfo                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonMatrixProcessPktInfo (tDsmonPktInfo * pPktInfo, UINT4 u4ProtoALIndex,
                           UINT4 u4PktSize)
{
    tDsmonMatrixCtl    *pMatrixCtl = NULL;
    tDsmonMatrixSD     *pCurMatrixSD = NULL, *pPrevMatrixSD = NULL;
    tDsmonMatrixSD     *pTravMatrixSD = NULL;
    UINT4               u4DsmonMatrixAggGroupIndex = 0, u4MatrixCtlIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixProcessPktinfo\r\n");

    /* Check for valid Control Entry */
    pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);

    while (pMatrixCtl != NULL)
    {
        u4MatrixCtlIndex = pMatrixCtl->u4DsmonMatrixCtlIndex;
        if ((pMatrixCtl->u4DsmonMatrixCtlRowStatus == ACTIVE) &&
            ((pMatrixCtl->u4DsmonMatrixDataSource ==
              pPktInfo->PktHdr.u4IfIndex) ||
             (pMatrixCtl->u4DsmonMatrixDataSource ==
              pPktInfo->PktHdr.u4VlanIfIndex))
            && (pPktInfo->PktHdr.u1DSCP < DSMON_MAX_AGG_PROFILE_DSCP))
        {
            if (pMatrixCtl->u4DsmonMatrixDataSource ==
                pPktInfo->PktHdr.u4VlanIfIndex)
            {
                pPktInfo->u1IsL3Vlan = OSIX_TRUE;
            }
            u4DsmonMatrixAggGroupIndex =
                pMatrixCtl->pAggCtl->au4DsmonAggProfile[pPktInfo->PktHdr.
                                                        u1DSCP];

            pCurMatrixSD = DsmonMatrixGetDataEntry (DSMON_MATRIXSD_TABLE,
                                                    pMatrixCtl->
                                                    u4DsmonMatrixCtlIndex,
                                                    u4DsmonMatrixAggGroupIndex,
                                                    pPktInfo->u4ProtoNLIndex,
                                                    u4ProtoALIndex,
                                                    &pPktInfo->PktHdr.SrcIp,
                                                    &pPktInfo->PktHdr.DstIp);
            if (pCurMatrixSD == NULL)
            {
                /* Check whether data entries reached max limit */
                if ((pMatrixCtl->u4DsmonMatrixCtlInserts -
                     pMatrixCtl->u4DsmonMatrixCtlDeletes) >=
                    pMatrixCtl->u4DsmonMatrixCtlMaxDesiredSupported)
                {
                    pCurMatrixSD = DsmonMatrixGiveLRUEntry
                        (pMatrixCtl->u4DsmonMatrixCtlIndex);
                    if (pCurMatrixSD != NULL)
                    {
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "Deleting Matrix LRU Entry\r\n");

                        /* Remove Inter table dependencies */
                        DsmonMatrixDelInterDep (pCurMatrixSD);

                        pCurMatrixSD->pMatrixCtl->u4DsmonMatrixCtlDeletes += 2;

                        /* After Reference changes, free Matrix stats entry */
                        DsmonMatrixDelDataEntry (pCurMatrixSD);

                        pCurMatrixSD = NULL;
                    }
                }

                /* Add Matrix SD & DS Entry */

                pCurMatrixSD =
                    (tDsmonMatrixSD *) DsmonMatrixAddDataEntry (pMatrixCtl->
                                                                u4DsmonMatrixCtlIndex,
                                                                u4DsmonMatrixAggGroupIndex,
                                                                pPktInfo->
                                                                u4ProtoNLIndex,
                                                                u4ProtoALIndex,
                                                                &pPktInfo->
                                                                PktHdr.SrcIp,
                                                                &pPktInfo->
                                                                PktHdr.DstIp);
                if (pCurMatrixSD == NULL)
                {
                    pMatrixCtl->u4DsmonMatrixCtlDroppedFrames++;

                    /* Get next Stats Ctl */
                    pMatrixCtl =
                        DsmonMatrixGetNextCtlIndex (pMatrixCtl->
                                                    u4DsmonMatrixCtlIndex);
                    continue;
                }
                else
                {
                    pCurMatrixSD->pMatrixCtl = pMatrixCtl;
                    pCurMatrixSD->u4PktRefCount++;
                    pCurMatrixSD->DsmonCurMatrixSDSample.u4DsmonMatrixSDPkts++;
                    pCurMatrixSD->DsmonCurMatrixSDSample.
                        u4DsmonMatrixSDOctets += u4PktSize;
                    UINT8_ADD (pCurMatrixSD->DsmonCurMatrixSDSample.
                               u8DsmonMatrixSDHCPkts, 1);
                    UINT8_ADD (pCurMatrixSD->DsmonCurMatrixSDSample.
                               u8DsmonMatrixSDHCOctets, u4PktSize);
                    pCurMatrixSD->u4DsmonMatrixSDTimeMark = OsixGetSysUpTime ();

                    if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV4_HDR_VER)
                    {
                        pCurMatrixSD->u4DsmonMatrixIpAddrLen =
                            DSMON_IPV4_MAX_LEN;
                    }
                    else if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV6_HDR_VER)
                    {
                        pCurMatrixSD->u4DsmonMatrixIpAddrLen =
                            DSMON_IPV6_MAX_LEN;
                    }

                    /* Increase by 2 for SD and DS table entry */
                    pMatrixCtl->u4DsmonMatrixCtlInserts += 2;

                    /* If Matrix Ctl table has multile entries for Same Interface,
                     * Establish the links b/w data entries careated for same Packet */

                    if (pPktInfo->pMatrixSD == NULL)
                    {
                        pPktInfo->pMatrixSD = pCurMatrixSD;
                    }
                    else if (pPrevMatrixSD == NULL)
                    {
                        pPrevMatrixSD = pPktInfo->pMatrixSD;
                        while (pPrevMatrixSD->pNextMatrixSD != NULL)
                            pPrevMatrixSD = pPrevMatrixSD->pNextMatrixSD;

                        pPrevMatrixSD->pNextMatrixSD = pCurMatrixSD;
                        pCurMatrixSD->pPrevMatrixSD = pPrevMatrixSD;
                    }
                    else
                    {
                        pPrevMatrixSD->pNextMatrixSD = pCurMatrixSD;
                        pCurMatrixSD->pPrevMatrixSD = pPrevMatrixSD;
                    }
                    i4RetVal = OSIX_SUCCESS;
                }
            }
            else
            {
                pCurMatrixSD->DsmonCurMatrixSDSample.u4DsmonMatrixSDPkts++;
                pCurMatrixSD->DsmonCurMatrixSDSample.u4DsmonMatrixSDOctets +=
                    u4PktSize;
                UINT8_ADD (pCurMatrixSD->DsmonCurMatrixSDSample.
                           u8DsmonMatrixSDHCPkts, 1);
                UINT8_ADD (pCurMatrixSD->DsmonCurMatrixSDSample.
                           u8DsmonMatrixSDHCOctets, u4PktSize);
                pCurMatrixSD->u4DsmonMatrixSDTimeMark = OsixGetSysUpTime ();
                pTravMatrixSD = pPktInfo->pMatrixSD;
                if (pTravMatrixSD == NULL)
                {
                    pPktInfo->pMatrixSD = pCurMatrixSD;
                }
                else
                {
                    while (pTravMatrixSD->pNextMatrixSD != NULL)
                    {
                        pTravMatrixSD = pTravMatrixSD->pNextMatrixSD;
                    }
                    pTravMatrixSD->pNextMatrixSD = pCurMatrixSD;
                }
                pCurMatrixSD->u4PktRefCount++;
                return OSIX_SUCCESS;
            }
            pPrevMatrixSD = pCurMatrixSD;

        }                        /* End of If Statsment */

        pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);

    }                            /* End of While */
    return i4RetVal;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixUpdateAggCtlChgs                                */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/*                tables entries into NOTREADY and cleanup all data tables.  */
/*                If u1DsmonAggControlStatus is TRUE, update control tables  */
/*                and resume data collection                                 */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonMatrixUpdateAggCtlChgs (UINT1 u1DsmonAggControlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tDsmonMatrixCtl    *pMatrixCtl = NULL;
    tDsmonAggCtl       *pAggCtl = NULL;
    UINT4               u4MatrixCtlIndex = 0;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonMatrixUpdateAggCtlChgs: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixUpdateAggCtlChgs\r\n");

    switch (u1DsmonAggControlStatus)
    {
        case DSMON_AGGLOCK_FALSE:
            /* Get First MatrixCtl Entry */
            pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);
            while (pMatrixCtl != NULL)
            {
                u4MatrixCtlIndex = pMatrixCtl->u4DsmonMatrixCtlIndex;
                if (pMatrixCtl->u4DsmonMatrixCtlRowStatus == ACTIVE)
                {
                    pMatrixCtl->u4DsmonMatrixCtlRowStatus = NOT_READY;
                    if (CfaGetIfInfo
                        (pMatrixCtl->u4DsmonMatrixDataSource,
                         &IfInfo) != CFA_FAILURE)
                    {
                        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                            (CfaGetVlanId
                             (pMatrixCtl->u4DsmonMatrixDataSource,
                              &u2VlanId) != CFA_FAILURE))
                        {
                            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                        }
                    }

#ifdef NPAPI_WANTED
                    DsmonFsDSMONDisableProbe (pMatrixCtl->
                                              u4DsmonMatrixDataSource, u2VlanId,
                                              *pVlanPortList);
#endif
                }
                /* Get Next MatrixCtl Entry */
                pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);
            }

            /* Delete MatrixSD Table */
            RBTreeDrain (DSMON_MATRIX_TABLE[DSMON_MATRIXSD_TABLE],
                         DsmonUtlRBFreeMatrixSD, 0);

            /* Delete MatrixSD Table */
            RBTreeDrain (DSMON_MATRIX_TABLE[DSMON_MATRIXDS_TABLE], NULL, 0);
            break;
        case DSMON_AGGLOCK_TRUE:
            /* Get First MatrixCtl Entry */
            pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);
            while (pMatrixCtl != NULL)
            {
                u4MatrixCtlIndex = pMatrixCtl->u4DsmonMatrixCtlIndex;
                /* Validate Aggregation Control Index */
                pAggCtl =
                    DsmonAggCtlGetEntry (pMatrixCtl->
                                         u4DsmonMatrixCtlAggProfile);
                if ((pAggCtl != NULL)
                    && (pAggCtl->u4DsmonAggCtlRowStatus == ACTIVE))
                {
                    if (CfaGetIfInfo
                        (pMatrixCtl->u4DsmonMatrixDataSource,
                         &IfInfo) == CFA_FAILURE)
                    {
                        /* ifindex is invalid */
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "DsmonMatrixUpdateAggCtlChgs: Invalid interface Index \n");
                        FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                        return;
                    }

                    if ((IfInfo.u1IfOperStatus != CFA_IF_UP)
                        || (IfInfo.u1IfAdminStatus != CFA_IF_UP))
                    {
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "DsmonMatrixUpdateAggCtlChgs: \
                Interface OperStatus or Admin Status is in invalid state \n");
                        FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                        return;
                    }

                    if (IfInfo.u1IfType == CFA_L3IPVLAN)
                    {
                        /* Get the VlanId from interface index */
                        if (CfaGetVlanId
                            (pMatrixCtl->u4DsmonMatrixDataSource,
                             &u2VlanId) == CFA_FAILURE)
                        {
                            DSMON_TRC1 (DSMON_DEBUG_TRC, "DsmonMatrixUpdateAggCtlChgs: \
                    Unable to Fetch VlanId of IfIndex %d\n",
                                        pMatrixCtl->u4DsmonMatrixDataSource);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return;
                        }

                        if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList)
                            == L2IWF_FAILURE)
                        {
                            DSMON_TRC1 (DSMON_DEBUG_TRC, "DsmonMatrixUpdateAggCtlChgs: \
                    Get Member Port List for VlanId %d failed \n",
                                        u2VlanId);
                            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                            return;
                        }
                    }
#ifdef NPAPI_WANTED
                    DsmonFsDSMONEnableProbe (pMatrixCtl->
                                             u4DsmonMatrixDataSource, u2VlanId,
                                             *pVlanPortList);
#endif
                    /* Set back RowStatus of MatrixCtl as ACTIVE */
                    pMatrixCtl->u4DsmonMatrixCtlRowStatus = ACTIVE;
                }
                pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);
            }
            break;
        default:
            break;
    }
    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixUpdateIfaceChgs                                 */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then set appropriate control */
/*                entries to NOT_READY state thus deleting the associated    */
/*                Data Table. If OperStatus is CFA_IF_UP, set NOT_READY      */
/*                entries to ACTIVE state                                    */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonMatrixUpdateIfaceChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tDsmonMatrixCtl    *pMatrixCtl = NULL;
    UINT4               u4MatrixCtlIndex = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixUpdateIfaceChgs\r\n");

    switch (u1OperStatus)
    {
        case CFA_IF_DOWN:
            /* Get First Matrix Control Table */

            pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);

            while (pMatrixCtl != NULL)
            {
                u4MatrixCtlIndex = pMatrixCtl->u4DsmonMatrixCtlIndex;
                if ((pMatrixCtl->u4DsmonMatrixCtlRowStatus == ACTIVE) &&
                    (pMatrixCtl->u4DsmonMatrixDataSource == u4IfIndex))
                {
                    DsmonMatrixDelDataEntries (u4MatrixCtlIndex);

                    /* Set RowStatus as NOT_READY */

                    pMatrixCtl->u4DsmonMatrixCtlRowStatus = NOT_READY;
                }

                pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);
            }
            break;
        case CFA_IF_UP:
            /* Get First Matrix Control Table */

            pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);

            while (pMatrixCtl != NULL)
            {
                u4MatrixCtlIndex = pMatrixCtl->u4DsmonMatrixCtlIndex;
                if ((pMatrixCtl->u4DsmonMatrixCtlRowStatus == NOT_READY) &&
                    (pMatrixCtl->u4DsmonMatrixDataSource == u4IfIndex))
                {
                    /* Set RowStatus as ACTIVE */
                    nmhSetDsmonMatrixCtlStatus (pMatrixCtl->
                                                u4DsmonMatrixCtlIndex, ACTIVE);
                }

                pMatrixCtl = DsmonMatrixGetNextCtlIndex (u4MatrixCtlIndex);
            }
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixUpdateProtoChgs                                 */
/*                                                                           */
/* Description  : If RMONv2 removes an existing protocol support, the same   */
/*                needs to be updated on Matrix Data Table                   */
/*                                                                           */
/* Input        : u1DsmonProtoLocalIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonMatrixUpdateProtoChgs (UINT4 u4DsmonMatrixProtDirLocalIndex)
{
    tDsmonMatrixSD     *pMatrixSD = NULL;
    UINT4               u4DsmonMatrixCtlIndex = 0, u4DsmonMatrixGrpIndex = 0;
    UINT4               u4DsmonMatrixALIndex = 0, u4DsmonMatrixNLIndex = 0;
    tIpAddr             DsmonMatrixSrcAddr, DsmonMatrixDstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixUpdateProtoChgs\r\n");
    MEMSET (&DsmonMatrixSrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
    MEMSET (&DsmonMatrixDstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    /* Get First Entry in Matrix SD Table */

    pMatrixSD = DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                             u4DsmonMatrixCtlIndex,
                                             u4DsmonMatrixGrpIndex,
                                             u4DsmonMatrixNLIndex,
                                             u4DsmonMatrixALIndex,
                                             &DsmonMatrixSrcAddr,
                                             &DsmonMatrixDstAddr);

    while (pMatrixSD != NULL)
    {
        u4DsmonMatrixCtlIndex = pMatrixSD->u4DsmonMatrixCtlIndex;
        u4DsmonMatrixGrpIndex = pMatrixSD->u4DsmonMatrixSDGroupIndex;
        u4DsmonMatrixNLIndex = pMatrixSD->u4DsmonMatrixSDNLIndex;
        u4DsmonMatrixALIndex = pMatrixSD->u4DsmonMatrixSDALIndex;

        MEMCPY (&DsmonMatrixSrcAddr, &pMatrixSD->DsmonMatrixSDSourceAddress,
                sizeof (tIpAddr));
        MEMCPY (&DsmonMatrixDstAddr, &pMatrixSD->DsmonMatrixSDDestAddress,
                sizeof (tIpAddr));

        if ((pMatrixSD->u4DsmonMatrixSDNLIndex ==
             u4DsmonMatrixProtDirLocalIndex)
            || (pMatrixSD->u4DsmonMatrixSDALIndex ==
                u4DsmonMatrixProtDirLocalIndex))
        {
            /* Remove Inter table dependencies */
            DsmonMatrixDelInterDep (pMatrixSD);

            pMatrixSD->pMatrixCtl->u4DsmonMatrixCtlDeletes += 2;

            /* After Reference changes, free MatrixSD stats entry */
            DsmonMatrixDelDataEntry (pMatrixSD);
        }

        pMatrixSD = DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                                 u4DsmonMatrixCtlIndex,
                                                 u4DsmonMatrixGrpIndex,
                                                 u4DsmonMatrixNLIndex,
                                                 u4DsmonMatrixALIndex,
                                                 &DsmonMatrixSrcAddr,
                                                 &DsmonMatrixDstAddr);
    }                            /* End of While */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixGetNextCtlIndex                                 */
/*                                                                           */
/* Description  : Get First / Next Matrix Ctl Index                  */
/*                                                                           */
/* Input        : u4DsmonMatrixCtlIndex                                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixCtl / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixCtl *
DsmonMatrixGetNextCtlIndex (UINT4 u4DsmonMatrixCtlIndex)
{
    tDsmonMatrixCtl    *pMatrixCtl = NULL;
    tDsmonMatrixCtl     MatrixCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixGetNextCtlIndex\r\n");
    MEMSET (&MatrixCtl, DSMON_INIT_VAL, sizeof (tDsmonMatrixCtl));

    MatrixCtl.u4DsmonMatrixCtlIndex = u4DsmonMatrixCtlIndex;
    pMatrixCtl = (tDsmonMatrixCtl *) RBTreeGetNext (DSMON_MATRIXCTL_TABLE,
                                                    (tRBElem *) & MatrixCtl,
                                                    NULL);
    return pMatrixCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixAddCtlEntry                                     */
/*                                                                           */
/* Description  : Allocates memory and Adds Matrix Control Entry into        */
/*          dsmonMatrixCtlRBTree                           */
/*                                                                           */
/* Input        : u4DsmonMatrixControlIndex                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixCtl / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixCtl *
DsmonMatrixAddCtlEntry (UINT4 u4DsmonMatrixControlIndex)
{
    tDsmonMatrixCtl    *pMatrixCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixAddCtlEntry\r\n");
    if ((pMatrixCtl = (tDsmonMatrixCtl *)
         (MemAllocMemBlk (DSMON_MATRIXCTL_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Matrix. Ctl.\r\n");
        return NULL;
    }

    MEMSET (pMatrixCtl, DSMON_INIT_VAL, sizeof (tDsmonMatrixCtl));

    pMatrixCtl->u4DsmonMatrixCtlIndex = u4DsmonMatrixControlIndex;
    pMatrixCtl->u4DsmonMatrixCtlRowStatus = NOT_READY;
    pMatrixCtl->i4DsmonMatrixCtlMaxDesiredEnt = DSMON_MAX_DATA_PER_CTL;
    pMatrixCtl->u4DsmonMatrixCtlMaxDesiredSupported = DSMON_MAX_DATA_PER_CTL;

    if (RBTreeAdd (DSMON_MATRIXCTL_TABLE, (tRBElem *) pMatrixCtl) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonMatrixAddCtlEntry Matrix Ctl entry is Added\r\n");
        return pMatrixCtl;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonMatrixAddCtlEntry Entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_MATRIXCTL_POOL, (UINT1 *) pMatrixCtl);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixDelCtlEntry                                     */
/*                                                                           */
/* Description  : Releases memory and Removes Matrix Control Entry from      */
/*          dsmonMatrixCtlRBTree                           */
/*                                                                           */
/* Input        : pMatrixCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonMatrixDelCtlEntry (tDsmonMatrixCtl * pMatrixCtl)
{
    tPortList          *pVlanPortList = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));
    pVlanPortList = (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

    if (pVlanPortList == NULL)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonMatrixDelCtlEntry: "
                   "Error in allocating memory for pVlanPortList\r\n");
        return OSIX_FAILURE;
    }
    MEMSET (pVlanPortList, 0, sizeof (tPortList));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixDelCtlEntry\r\n");

    if (CfaGetIfInfo (pMatrixCtl->u4DsmonMatrixDataSource, &IfInfo) !=
        CFA_FAILURE)
    {
        if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
            (CfaGetVlanId (pMatrixCtl->u4DsmonMatrixDataSource, &u2VlanId) !=
             CFA_FAILURE))
        {
            L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
        }
    }

#ifdef NPAPI_WANTED
    /* Remove Probe support from H/W Table */
    DsmonFsDSMONDisableProbe (pMatrixCtl->u4DsmonMatrixDataSource, u2VlanId,
                              *pVlanPortList);
#endif
    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
    /* Remove Data Entries for this control index from Matrix SD Table */
    DsmonMatrixDelDataEntries (pMatrixCtl->u4DsmonMatrixCtlIndex);

    /* Remove Matrix. Ctl node from dsmonMatrixCtlRBTree */
    RBTreeRem (DSMON_MATRIXCTL_TABLE, (tRBElem *) pMatrixCtl);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_MATRIXCTL_POOL,
                            (UINT1 *) pMatrixCtl) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  Matrix. Ctl.\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixGetCtlEntry                                     */
/*                                                                           */
/* Description  : Get Matrix Control Entry for the given control index       */
/*                                                                           */
/* Input        : u4DsmonMatrixControlIndex                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixCtl / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixCtl *
DsmonMatrixGetCtlEntry (UINT4 u4DsmonMatrixControlIndex)
{
    tDsmonMatrixCtl    *pMatrixCtl = NULL;
    tDsmonMatrixCtl     MatrixCtl;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixGetCtlEntry\r\n");
    MEMSET (&MatrixCtl, DSMON_INIT_VAL, sizeof (tDsmonMatrixCtl));

    MatrixCtl.u4DsmonMatrixCtlIndex = u4DsmonMatrixControlIndex;

    pMatrixCtl = (tDsmonMatrixCtl *) RBTreeGet (DSMON_MATRIXCTL_TABLE,
                                                (tRBElem *) & MatrixCtl);
    return pMatrixCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixGetNextDataIndex                                */
/*                                                                           */
/* Description  : Get First / Next Matrix Index                          */
/*                                                                           */
/* Input        : u4DsmonMatrixTableId                              */
/*          u4DsmonMatrixCtlIndex                                      */
/*          u4DsmonMatrixGroupIndex                     */
/*          u4DsmonMatrixNLIndex                         */
/*          u4DsmonMatrixALIndex                         */
/*          DsmonMatrixSourceAddress                     */
/*          DsmonMatrixDestAddress                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrix / NULL Pointer                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixSD *
DsmonMatrixGetNextDataIndex (UINT4 u4DsmonMatrixTableId,
                             UINT4 u4DsmonMatrixCtlIndex,
                             UINT4 u4DsmonMatrixGroupIndex,
                             UINT4 u4DsmonMatrixNLIndex,
                             UINT4 u4DsmonMatrixALIndex,
                             tIpAddr * pDsmonMatrixSourceAddress,
                             tIpAddr * pDsmonMatrixDestAddress)
{
    tDsmonMatrixSD     *pMatrixSD = NULL, MatrixSD;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixGetNextDataIndex\r\n");
    MEMSET (&MatrixSD, DSMON_INIT_VAL, sizeof (tDsmonMatrixSD));

    if ((u4DsmonMatrixCtlIndex != 0) && (u4DsmonMatrixNLIndex == 0))
    {
        pMatrixSD = (tDsmonMatrixSD *) RBTreeGetFirst
            (DSMON_MATRIX_TABLE[u4DsmonMatrixTableId]);
        while (pMatrixSD != NULL)
        {
            if (pMatrixSD->u4DsmonMatrixCtlIndex == u4DsmonMatrixCtlIndex)
            {
                break;
            }
            MEMCPY (&MatrixSD, pMatrixSD, sizeof (tDsmonMatrixSD));

            pMatrixSD = (tDsmonMatrixSD *) RBTreeGetNext
                (DSMON_MATRIX_TABLE[u4DsmonMatrixTableId],
                 (tRBElem *) & MatrixSD, NULL);
        }
    }
    else
    {
        MatrixSD.u4DsmonMatrixCtlIndex = u4DsmonMatrixCtlIndex;
        MatrixSD.u4DsmonMatrixSDGroupIndex = u4DsmonMatrixGroupIndex;
        MatrixSD.u4DsmonMatrixSDNLIndex = u4DsmonMatrixNLIndex;
        MatrixSD.u4DsmonMatrixSDALIndex = u4DsmonMatrixALIndex;

        MEMCPY (&MatrixSD.DsmonMatrixSDSourceAddress, pDsmonMatrixSourceAddress,
                sizeof (tIpAddr));
        MEMCPY (&MatrixSD.DsmonMatrixSDDestAddress, pDsmonMatrixDestAddress,
                sizeof (tIpAddr));
        pMatrixSD = (tDsmonMatrixSD *) RBTreeGetNext
            (DSMON_MATRIX_TABLE[u4DsmonMatrixTableId],
             (tRBElem *) & MatrixSD, NULL);
    }

    return pMatrixSD;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixAddDataEntry                                    */
/*                                                                           */
/* Description  : Allocates memory and Adds Matrix Entry into                  */
/*          dsmonMatrixSDRBTree & dsmonMatrixSDRBTree             */
/*                                                                           */
/* Input        : pMatrix                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixSD / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixSD *
DsmonMatrixAddDataEntry (UINT4 u4DsmonMatrixCtlIndex,
                         UINT4 u4DsmonMatrixGroupIndex,
                         UINT4 u4DsmonMatrixNLIndex,
                         UINT4 u4DsmonMatrixALIndex,
                         tIpAddr * pDsmonMatrixSourceAddress,
                         tIpAddr * pDsmonMatrixDestAddress)
{
    tDsmonMatrixSD     *pMatrixSD = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixAddDataEntry\r\n");
    if ((pMatrixSD = (tDsmonMatrixSD *)
         (MemAllocMemBlk (DSMON_MATRIXSD_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Matrix.\r\n");
        return NULL;
    }

    MEMSET (pMatrixSD, DSMON_INIT_VAL, sizeof (tDsmonMatrixSD));

    pMatrixSD->u4DsmonMatrixCtlIndex = u4DsmonMatrixCtlIndex;
    pMatrixSD->u4DsmonMatrixSDGroupIndex = u4DsmonMatrixGroupIndex;
    pMatrixSD->u4DsmonMatrixSDNLIndex = u4DsmonMatrixNLIndex;
    pMatrixSD->u4DsmonMatrixSDALIndex = u4DsmonMatrixALIndex;
    pMatrixSD->u4DsmonMatrixSDTimeMark = OsixGetSysUpTime ();
    pMatrixSD->u4DsmonMatrixSDCreateTime = OsixGetSysUpTime ();

    MEMCPY (&pMatrixSD->DsmonMatrixSDSourceAddress,
            pDsmonMatrixSourceAddress, sizeof (tIpAddr));

    MEMCPY (&pMatrixSD->DsmonMatrixSDDestAddress,
            pDsmonMatrixDestAddress, sizeof (tIpAddr));

    /* Add Entry into Matrix SD table */

    if (RBTreeAdd (DSMON_MATRIX_TABLE[DSMON_MATRIXSD_TABLE],
                   (tRBElem *) pMatrixSD) == RB_SUCCESS)
    {
        /* Add the same node into Mareix DS Table */
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonMatrixAddDataEntry MatrixSD Entry is added\r\n");

        if (RBTreeAdd (DSMON_MATRIX_TABLE[DSMON_MATRIXDS_TABLE],
                       (tRBElem *) pMatrixSD) == RB_SUCCESS)
        {
            DSMON_TRC (DSMON_DEBUG_TRC,
                       "DsmonMatrixAddDataEntry MatrixDS Entry is added\r\n");
            return pMatrixSD;
        }

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonMatrixAddDataEntry MatrixDS Entry addition failed\r\n");
        /* Remove MatrixSD. node from dsmonMatrixSDRBTree */
        RBTreeRem (DSMON_MATRIX_TABLE[DSMON_MATRIXSD_TABLE],
                   (tRBElem *) pMatrixSD);
    }

    DSMON_TRC (DSMON_DEBUG_TRC,
               "DsmonMatrixAddDataEntry MatrixSD Entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_MATRIXSD_POOL, (UINT1 *) pMatrixSD);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixDelDataEntry                                    */
/*                                                                           */
/* Description  : Releases memory and Removes Matrix Entry from                 */
/*          dsmonMatrixRBTrees                           */
/*                                                                           */
/* Input        : pMatrixSD                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonMatrixDelDataEntry (tDsmonMatrixSD * pMatrixSD)
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixDelDataEntry\r\n");
    /* Remove MatrixSD. node from dsmonMatrixSDRBTree */
    RBTreeRem (DSMON_MATRIX_TABLE[DSMON_MATRIXSD_TABLE], (tRBElem *) pMatrixSD);

    /* Remove MatrixDS. node from dsmonMatrixDSRBTree */
    RBTreeRem (DSMON_MATRIX_TABLE[DSMON_MATRIXDS_TABLE], (tRBElem *) pMatrixSD);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_MATRIXSD_POOL,
                            (UINT1 *) pMatrixSD) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "DsmonMatrixDelDataEntry MemBlock Release failed for  MatrixSD.\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixDelDataEntries                                  */
/*                                                                           */
/* Description  : Removes Matrix Entries from dsmonMatrixRBTrees for the     */
/*                given control index                                        */
/*                                                                           */
/* Input        : u4MatrixCtlIndex                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                E                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonMatrixDelDataEntries (UINT4 u4MatrixCtlIndex)
{
    tDsmonMatrixSD     *pMatrixSD = NULL;
    UINT4               u4DsmonMatrixAggGroupIndex = 0, u4DsmonMatrixNLIndex =
        0, u4DsmonMatrixALIndex = 0;
    tIpAddr             SrcIp, DstIp;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixDelDataEntries\r\n");

    MEMSET (&SrcIp, DSMON_INIT_VAL, sizeof (tIpAddr));
    MEMSET (&DstIp, DSMON_INIT_VAL, sizeof (tIpAddr));

    pMatrixSD = DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                             u4MatrixCtlIndex,
                                             u4DsmonMatrixAggGroupIndex,
                                             u4DsmonMatrixNLIndex,
                                             u4DsmonMatrixALIndex,
                                             &SrcIp, &DstIp);

    while (pMatrixSD != NULL)
    {
        if (pMatrixSD->u4DsmonMatrixCtlIndex != u4MatrixCtlIndex)
        {
            break;
        }

        u4DsmonMatrixAggGroupIndex = pMatrixSD->u4DsmonMatrixSDGroupIndex;
        u4DsmonMatrixNLIndex = pMatrixSD->u4DsmonMatrixSDNLIndex;
        u4DsmonMatrixALIndex = pMatrixSD->u4DsmonMatrixSDALIndex;

        MEMCPY (&SrcIp, &pMatrixSD->DsmonMatrixSDSourceAddress,
                sizeof (tIpAddr));
        MEMCPY (&DstIp, &pMatrixSD->DsmonMatrixSDDestAddress, sizeof (tIpAddr));

        /* Remove Inter table dependencies */
        DsmonMatrixDelInterDep (pMatrixSD);

        pMatrixSD->pMatrixCtl->u4DsmonMatrixCtlDeletes += 2;

        /* After Reference changes, free stats entry */
        DsmonMatrixDelDataEntry (pMatrixSD);

        pMatrixSD = DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                                 u4MatrixCtlIndex,
                                                 u4DsmonMatrixAggGroupIndex,
                                                 u4DsmonMatrixNLIndex,
                                                 u4DsmonMatrixALIndex,
                                                 &SrcIp, &DstIp);
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonMatrixDelDataEntries\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixGetDataEntry                                    */
/*                                                                           */
/* Description  : Get First / Next Matrix Entry                          */
/*                                                                           */
/* Input        : u4DsmonMatrixTableId                              */
/*          u4DsmonMatrixCtlIndex                                      */
/*          u4DsmonMatrixGroupIndex                     */
/*          u4DsmonMatrixNLIndex                         */
/*          u4DsmonMatrixALIndex                         */
/*          DsmonMatrixSourceAddress                     */
/*          DsmonMatrixDestAddress                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixSD / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixSD *
DsmonMatrixGetDataEntry (UINT4 u4DsmonMatrixTableId,
                         UINT4 u4DsmonMatrixCtlIndex,
                         UINT4 u4DsmonMatrixGroupIndex,
                         UINT4 u4DsmonMatrixNLIndex,
                         UINT4 u4DsmonMatrixALIndex,
                         tIpAddr * pDsmonMatrixSourceAddress,
                         tIpAddr * pDsmonMatrixDestAddress)
{
    tDsmonMatrixSD     *pMatrixSD = NULL, MatrixSD;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixGetDataEntry\r\n");

    MEMSET (&MatrixSD, DSMON_INIT_VAL, sizeof (tDsmonMatrixSD));

    MatrixSD.u4DsmonMatrixCtlIndex = u4DsmonMatrixCtlIndex;
    MatrixSD.u4DsmonMatrixSDGroupIndex = u4DsmonMatrixGroupIndex;
    MatrixSD.u4DsmonMatrixSDNLIndex = u4DsmonMatrixNLIndex;
    MatrixSD.u4DsmonMatrixSDALIndex = u4DsmonMatrixALIndex;

    MEMCPY (&MatrixSD.DsmonMatrixSDSourceAddress, pDsmonMatrixSourceAddress,
            sizeof (tIpAddr));

    MEMCPY (&MatrixSD.DsmonMatrixSDDestAddress, pDsmonMatrixDestAddress,
            sizeof (tIpAddr));

    pMatrixSD =
        (tDsmonMatrixSD *) RBTreeGet (DSMON_MATRIX_TABLE[u4DsmonMatrixTableId],
                                      (tRBElem *) & MatrixSD);

    return pMatrixSD;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixDelInterDep                                     */
/*                                                                           */
/* Description  : Removes the inter-dependencies and re-order the links         */
/*                                                                           */
/* Input        : pMatrixSD                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonMatrixDelInterDep (tDsmonMatrixSD * pMatrixSD)
{
    tDsmonPktInfo      *pPktInfo = NULL;
    tPktHeader          PktHdr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonMatrixDelInterDep\r\n");

    MEMSET (&PktHdr, DSMON_INIT_VAL, sizeof (tPktHeader));

    if (pMatrixSD->pPrevMatrixSD == NULL)
    {
        /* Update PktInfo nodes to point next pMatrix Entry */
        pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);

        while (pPktInfo != NULL)
        {
            if (pPktInfo->pMatrixSD == pMatrixSD)
            {
                pPktInfo->pMatrixSD = pMatrixSD->pNextMatrixSD;
                if (pPktInfo->pMatrixSD != NULL)
                {
                    pPktInfo->pMatrixSD->pPrevMatrixSD = NULL;
                }
            }
            pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
        }
    }
    else if (pMatrixSD->pNextMatrixSD != NULL)
    {
        pMatrixSD->pPrevMatrixSD->pNextMatrixSD = pMatrixSD->pNextMatrixSD;
    }
    else
    {
        pMatrixSD->pPrevMatrixSD->pNextMatrixSD = NULL;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixPopulateDataEntries                             */
/*                                                                           */
/* Description  : Populates data entries if the same data source is already  */
/*          present and Adds the inter-dependencies b/w other tables   */
/*                                                                           */
/* Input        : pMatrixCtl                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                 */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonMatrixPopulateDataEntries (tDsmonMatrixCtl * pMatrixCtl)
{
    tDsmonPktInfo      *pPktInfo = NULL;
    tDsmonMatrixSD     *pMatrixSD = NULL, *pNewMatrixSD = NULL;
    UINT4               u4DsmonAggGroupIndex = 0;
    tPktHeader          PktHdr;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC: ENTRY DsmonMatrixPopulateDataEntries\r\n");
    MEMSET (&PktHdr, DSMON_INIT_VAL, sizeof (tPktHeader));

    /* Check for the presence of data source in PktInfo table */
    pPktInfo = DsmonPktInfoGetNextIndex (&PktHdr);
    while (pPktInfo != NULL)
    {
        if ((pPktInfo->PktHdr.u4IfIndex == pMatrixCtl->u4DsmonMatrixDataSource)
            && (pPktInfo->PktHdr.u1DSCP < DSMON_MAX_AGG_PROFILE_DSCP))
        {
            u4DsmonAggGroupIndex =
                pMatrixCtl->pAggCtl->au4DsmonAggProfile[pPktInfo->PktHdr.
                                                        u1DSCP];

            /* Check whether data entries reached max limit */
            if ((pMatrixCtl->u4DsmonMatrixCtlInserts -
                 pMatrixCtl->u4DsmonMatrixCtlDeletes) >=
                pMatrixCtl->u4DsmonMatrixCtlMaxDesiredSupported)
            {
                pNewMatrixSD = DsmonMatrixGiveLRUEntry
                    (pMatrixCtl->u4DsmonMatrixCtlIndex);
                if (pNewMatrixSD != NULL)
                {
                    DSMON_TRC (DSMON_DEBUG_TRC,
                               "Deleting Matrix LRU Entry\r\n");

                    /* Remove Inter table dependencies */
                    DsmonMatrixDelInterDep (pNewMatrixSD);

                    pNewMatrixSD->pMatrixCtl->u4DsmonMatrixCtlDeletes += 2;

                    /* After Reference changes, free Matrix stats entry */
                    DsmonMatrixDelDataEntry (pNewMatrixSD);

                    pNewMatrixSD = NULL;
                }
            }

            /* Add Matrix SD and DS Entry for AL Index */
            if (pPktInfo->u4ProtoALIndex != 0)
            {

                pNewMatrixSD =
                    DsmonMatrixAddDataEntry (pMatrixCtl->u4DsmonMatrixCtlIndex,
                                             u4DsmonAggGroupIndex,
                                             pPktInfo->u4ProtoNLIndex,
                                             pPktInfo->u4ProtoALIndex,
                                             &pPktInfo->PktHdr.SrcIp,
                                             &pPktInfo->PktHdr.DstIp);
                if (pNewMatrixSD == NULL)
                {
                    pMatrixCtl->u4DsmonMatrixCtlDroppedFrames++;

                    pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
                    continue;
                }
                pNewMatrixSD->pMatrixCtl = pMatrixCtl;
                pNewMatrixSD->u4PktRefCount++;

                if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV4_HDR_VER)
                {
                    pNewMatrixSD->u4DsmonMatrixIpAddrLen = DSMON_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV6_HDR_VER)
                {
                    pNewMatrixSD->u4DsmonMatrixIpAddrLen = DSMON_IPV6_MAX_LEN;
                }

                /* Increase by 2 for SD and DS table entry */
                pMatrixCtl->u4DsmonMatrixCtlInserts += 2;

                /* Fetch the Matrix SD Entries for the given control index */
                pMatrixSD = pPktInfo->pMatrixSD;

                if (pMatrixSD == NULL)
                {
                    pPktInfo->pMatrixSD = pNewMatrixSD;
                }
                else
                {
                    while (pMatrixSD->pNextMatrixSD != NULL)
                    {
                        pMatrixSD = pMatrixSD->pNextMatrixSD;
                    }

                    pMatrixSD->pNextMatrixSD = pNewMatrixSD;
                    pNewMatrixSD->pPrevMatrixSD = pMatrixSD;

                }
            }

            /* Add Matrix SD and DS Entry for TL Index */
            if (pPktInfo->u4ProtoTLIndex != 0)
            {

                /* Check whether data entries reached max limit */
                if ((pMatrixCtl->u4DsmonMatrixCtlInserts -
                     pMatrixCtl->u4DsmonMatrixCtlDeletes) >=
                    pMatrixCtl->u4DsmonMatrixCtlMaxDesiredSupported)
                {
                    pNewMatrixSD = DsmonMatrixGiveLRUEntry
                        (pMatrixCtl->u4DsmonMatrixCtlIndex);
                    if (pNewMatrixSD != NULL)
                    {
                        DSMON_TRC (DSMON_DEBUG_TRC,
                                   "Deleting Matrix LRU Entry\r\n");

                        /* Remove Inter table dependencies */
                        DsmonMatrixDelInterDep (pNewMatrixSD);

                        pNewMatrixSD->pMatrixCtl->u4DsmonMatrixCtlDeletes += 2;

                        /* After Reference changes, free Matrix stats entry */
                        DsmonMatrixDelDataEntry (pNewMatrixSD);

                        pNewMatrixSD = NULL;
                    }
                }

                pNewMatrixSD =
                    DsmonMatrixAddDataEntry (pMatrixCtl->u4DsmonMatrixCtlIndex,
                                             u4DsmonAggGroupIndex,
                                             pPktInfo->u4ProtoNLIndex,
                                             pPktInfo->u4ProtoTLIndex,
                                             &pPktInfo->PktHdr.SrcIp,
                                             &pPktInfo->PktHdr.DstIp);
                if (pNewMatrixSD == NULL)
                {
                    pMatrixCtl->u4DsmonMatrixCtlDroppedFrames++;

                    pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);
                    continue;
                }
                pNewMatrixSD->pMatrixCtl = pMatrixCtl;

                if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV4_HDR_VER)
                {
                    pNewMatrixSD->u4DsmonMatrixIpAddrLen = DSMON_IPV4_MAX_LEN;
                }
                else if (pPktInfo->PktHdr.u1IpVersion == DSMON_IPV6_HDR_VER)
                {
                    pNewMatrixSD->u4DsmonMatrixIpAddrLen = DSMON_IPV6_MAX_LEN;
                }

                /* Increase by 2 for SD and DS table entry */
                pMatrixCtl->u4DsmonMatrixCtlInserts += 2;

                /* Fetch the Matrix SD Entries for the given control index */
                pMatrixSD = pPktInfo->pMatrixSD;

                if (pMatrixSD == NULL)
                {
                    pPktInfo->pMatrixSD = pNewMatrixSD;
                }
                else
                {
                    while (pMatrixSD->pNextMatrixSD != NULL)
                    {
                        pMatrixSD = pMatrixSD->pNextMatrixSD;
                    }

                    pMatrixSD->pNextMatrixSD = pNewMatrixSD;
                    pNewMatrixSD->pPrevMatrixSD = pMatrixSD;

                }
            }

        }                        /* End of If */

        pPktInfo = DsmonPktInfoGetNextIndex (&pPktInfo->PktHdr);

    }                            /* End of While - PktInfo */

    return;
}
