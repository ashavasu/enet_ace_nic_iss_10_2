/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 * 
   $Id: dsmnmxtn.c,v 1.8 2012/04/03 13:16:37 siva Exp $
 
                                                               *
 * Description: This file contains the functional routine for DSMON *
 *              Matrix TopN module                                *
 *                                                                  *
 *******************************************************************/

#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNUpdateAggCtlChgs                            */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/*                tables entries into NOTREADY and cleanup all TopN report.  */
/*                If u1DsmonAggControlStatus is TRUE, update control tables  */
/*                and resume TopN report process                             */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonMatrixTopNUpdateAggCtlChgs (UINT1 u1DsmonAggControlStatus)
{
    tDsmonMatrixCtl    *pMatrixCtl = NULL;
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;
    UINT4               u4MatrixTopNCtlIndex = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonMatrixTopNUpdateAggCtlChgs \r\n");

    switch (u1DsmonAggControlStatus)
    {
        case DSMON_AGGLOCK_FALSE:
            /* Get First MatrixTopNCtl Entry */
            pMatrixTopNCtl =
                DsmonMatrixTopNGetNextCtlIndex (u4MatrixTopNCtlIndex);
            while (pMatrixTopNCtl != NULL)
            {
                u4MatrixTopNCtlIndex =
                    pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;
                if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus == ACTIVE)
                {
                    pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus = NOT_READY;
                }

                /* Get Next MatrixTopNCtl Entry */
                pMatrixTopNCtl = DsmonMatrixTopNGetNextCtlIndex
                    (u4MatrixTopNCtlIndex);
            }

            /* Delete TopN Report Table */
            RBTreeDrain (DSMON_MATRIX_TOPN_TABLE, DsmonUtlRBFreeMatrixTopN,
                         DSMON_ZERO);
            break;
        case DSMON_AGGLOCK_TRUE:
            /* Get First MatrixTopNCtl Entry */
            pMatrixTopNCtl =
                DsmonMatrixTopNGetNextCtlIndex (u4MatrixTopNCtlIndex);
            while (pMatrixTopNCtl != NULL)
            {
                u4MatrixTopNCtlIndex =
                    pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;
                pMatrixCtl =
                    DsmonMatrixGetNextCtlIndex (pMatrixTopNCtl->
                                                u4DsmonMatrixTopNCtlMatrixIndex);

                if ((pMatrixCtl != NULL) &&
                    (pMatrixCtl->u4DsmonMatrixCtlRowStatus == ACTIVE))
                {
                    /* Set back RowStatus of MatrixTopNCtl as ACTIVE */
                    pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus = ACTIVE;
                }
                pMatrixTopNCtl = DsmonMatrixTopNGetNextCtlIndex
                    (u4MatrixTopNCtlIndex);
            }
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNNotifyMtxCtlChgs                            */
/*                                                                           */
/* Description  : Notifies RowStatus changes to TopN Table                   */
/*                                                                           */
/* Input        : u4DsmonMatrixCtlIndex, u4DsmonMatrixCtlStatus              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonMatrixTopNNotifyMtxCtlChgs (UINT4 u4DsmonMatrixCtlIndex,
                                 UINT4 u4DsmonMatrixCtlStatus)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;
    tDsmonMatrixTopN   *pMatrixTopN = NULL;
    UINT4               u4MatrixTopNIndex = DSMON_ZERO, u4MatrixTopNCtlIndex =
        DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonMatrixTopNNotifyMtxCtlChgs \r\n");

    pMatrixTopNCtl = DsmonMatrixTopNGetNextCtlIndex (u4MatrixTopNCtlIndex);

    while (pMatrixTopNCtl != NULL)
    {
        u4MatrixTopNCtlIndex = pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;
        if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlMatrixIndex ==
            u4DsmonMatrixCtlIndex)
        {
            if (u4DsmonMatrixCtlStatus != ACTIVE)
            {
                /* Free TopN Report */
                pMatrixTopN =
                    DsmonMatrixTopNGetNextTopNIndex (u4MatrixTopNCtlIndex,
                                                     u4MatrixTopNIndex);

                while (pMatrixTopN != NULL)
                {
                    if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex !=
                        pMatrixTopN->u4DsmonMatrixTopNCtlIndex)
                    {
                        break;
                    }
                    u4MatrixTopNIndex = pMatrixTopN->u4DsmonMatrixTopNIndex;

                    DsmonMatrixTopNDelTopNEntry (pMatrixTopN);

                    pMatrixTopN =
                        DsmonMatrixTopNGetNextTopNIndex (u4MatrixTopNCtlIndex,
                                                         u4MatrixTopNIndex);
                }

                pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem =
                    pMatrixTopNCtl->u4DsmonMatrixTopNCtlDuration;
                pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus = NOT_READY;
            }
            if ((u4DsmonMatrixCtlStatus == ACTIVE) &&
                (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus == NOT_READY))
            {
                pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus = ACTIVE;
                pMatrixTopNCtl->u4DsmonMatrixTopNCtlStartTime =
                    (UINT4) OsixGetSysUpTime ();
            }
        }
        pMatrixTopNCtl = DsmonMatrixTopNGetNextCtlIndex (u4MatrixTopNCtlIndex);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNGetNextCtlIndex                             */
/*                                                                           */
/* Description  : Get First / Next Matrix TopN Ctl Index                     */
/*                                                                           */
/* Input        : u4DsmonMatrixTopNCtlIndex                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixTopNCtl / NULL Pointer                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixTopNCtl *
DsmonMatrixTopNGetNextCtlIndex (UINT4 u4DsmonMatrixTopNCtlIndex)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;
    tDsmonMatrixTopNCtl MatrixTopNCtl;

    MEMSET (&MatrixTopNCtl, DSMON_INIT_VAL, sizeof (tDsmonMatrixTopNCtl));

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonMatrixTopNGetNextCtlIndex \r\n");

    MatrixTopNCtl.u4DsmonMatrixTopNCtlIndex = u4DsmonMatrixTopNCtlIndex;
    pMatrixTopNCtl = (tDsmonMatrixTopNCtl *) RBTreeGetNext
        (DSMON_MATRIX_TOPNCTL_TABLE, (tRBElem *) & MatrixTopNCtl, NULL);
    return pMatrixTopNCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNAddCtlEntry                                 */
/*                                                                           */
/* Description  : Allocates memory and Adds Matrix TopN Control Entry into   */
/*                dsmonMatrixTopNCtlRBTree                                   */
/*                                                                           */
/* Input        : u4DsmonMatrixTopNControlIndex                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixCtl / NULL Pointer                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixTopNCtl *
DsmonMatrixTopNAddCtlEntry (UINT4 u4DsmonMatrixTopNControlIndex)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMatrixTopNAddCtlEntry \r\n");

    if ((pMatrixTopNCtl = (tDsmonMatrixTopNCtl *)
         (MemAllocMemBlk (DSMON_MATRIX_TOPNCTL_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Matrix. TopN Ctl.\r\n");
        return NULL;
    }

    MEMSET (pMatrixTopNCtl, DSMON_INIT_VAL, sizeof (tDsmonMatrixTopNCtl));

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex = u4DsmonMatrixTopNControlIndex;

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem = DSMON_TIMEREM_DEFVAL;

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlGenReports = DSMON_ZERO;

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlDuration = DSMON_TIMEREM_DEFVAL;

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlReqSize = DSMON_REQSIZE_DEFVAL;

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize = DSMON_MAX_TOPN_ENTRY;

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlStartTime = DSMON_ZERO;

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus = NOT_READY;

    if (RBTreeAdd (DSMON_MATRIX_TOPNCTL_TABLE, (tRBElem *) pMatrixTopNCtl)
        == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonMatrixTopNAddCtlEntry TopN Ctl entry added\r\n");
        return pMatrixTopNCtl;
    }

    DSMON_TRC (DSMON_DEBUG_TRC,
               "DsmonMatrixTopNAddCtlEntry TopN Ctl entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_MATRIX_TOPNCTL_POOL, (UINT1 *) pMatrixTopNCtl);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNDelCtlEntry                                 */
/*                                                                           */
/* Description  : Releases memory and Removes Matrix TopN Control Entry from */
/*                dsmonMatrixTopNCtlRBTree                                   */
/*                                                                           */
/* Input        : pMatrixTopNCtl                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonMatrixTopNDelCtlEntry (tDsmonMatrixTopNCtl * pMatrixTopNCtl)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL;
    UINT4               u4MatrixTopNCtlIndex = DSMON_ZERO, u4MatrixTopNIndex =
        DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMatrixTopNDelCtlEntry \r\n");
    u4MatrixTopNCtlIndex = pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;

    pMatrixTopN = (tDsmonMatrixTopN *) DsmonMatrixTopNGetNextTopNIndex
        (u4MatrixTopNCtlIndex, u4MatrixTopNIndex);

    while (pMatrixTopN != NULL)
    {
        if (pMatrixTopN->u4DsmonMatrixTopNCtlIndex != u4MatrixTopNCtlIndex)
        {
            break;
        }
        u4MatrixTopNIndex = pMatrixTopN->u4DsmonMatrixTopNIndex;

        DsmonMatrixTopNDelTopNEntry (pMatrixTopN);

        pMatrixTopN = (tDsmonMatrixTopN *) DsmonMatrixTopNGetNextTopNIndex
            (u4MatrixTopNCtlIndex, u4MatrixTopNIndex);
    }

    if (RBTreeRemove (DSMON_MATRIX_TOPNCTL_TABLE, pMatrixTopNCtl) == RB_FAILURE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonMatrixTopNDelCtlEntry Entry removal failed\r\n");
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (DSMON_MATRIX_TOPNCTL_POOL, (UINT1 *) pMatrixTopNCtl);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNGetCtlEntry                                 */
/*                                                                           */
/* Description  : Get Matrix TopN Control Entry for the given control index  */
/*                                                                           */
/* Input        : u4DsmonMatrixTopNControlIndex                              */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixTopNCtl / NULL Pointer                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixTopNCtl *
DsmonMatrixTopNGetCtlEntry (UINT4 u4DsmonMatrixTopNControlIndex)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;
    tDsmonMatrixTopNCtl MatrixTopNCtl;

    MEMSET (&MatrixTopNCtl, DSMON_INIT_VAL, sizeof (tDsmonMatrixTopNCtl));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMatrixTopNGetCtlEntry \r\n");

    MatrixTopNCtl.u4DsmonMatrixTopNCtlIndex = u4DsmonMatrixTopNControlIndex;

    pMatrixTopNCtl =
        (tDsmonMatrixTopNCtl *) RBTreeGet (DSMON_MATRIX_TOPNCTL_TABLE,
                                           (tRBElem *) & MatrixTopNCtl);
    return pMatrixTopNCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNGetNextTopNIndex                            */
/*                                                                           */
/* Description  : Get First / Next MatrixTopN Index                          */
/*                                                                           */
/* Input        : u4DsmonMatrixTopNCtlIndex                                  */
/*                u4DsmonMatrixTopNIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixTopN / NULL Pointer                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixTopN *
DsmonMatrixTopNGetNextTopNIndex (UINT4 u4DsmonMatrixTopNCtlIndex,
                                 UINT4 u4DsmonMatrixTopNIndex)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL, MatrixTopN;

    MEMSET (&MatrixTopN, DSMON_INIT_VAL, sizeof (tDsmonMatrixTopN));

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY DsmonMatrixTopNGetNextTopNIndex \r\n");

    if ((u4DsmonMatrixTopNCtlIndex != DSMON_ZERO) &&
        (u4DsmonMatrixTopNIndex == DSMON_ZERO))
    {
        pMatrixTopN =
            (tDsmonMatrixTopN *) RBTreeGetFirst (DSMON_MATRIX_TOPN_TABLE);

        while (pMatrixTopN != NULL)
        {
            if (pMatrixTopN->u4DsmonMatrixTopNCtlIndex ==
                u4DsmonMatrixTopNCtlIndex)
            {
                break;
            }
            MEMCPY (&MatrixTopN, pMatrixTopN, sizeof (tDsmonMatrixTopN));

            pMatrixTopN =
                (tDsmonMatrixTopN *) RBTreeGetNext (DSMON_MATRIX_TOPN_TABLE,
                                                    (tRBElem *) & MatrixTopN,
                                                    NULL);
        }
    }
    else
    {
        MatrixTopN.u4DsmonMatrixTopNCtlIndex = u4DsmonMatrixTopNCtlIndex;
        MatrixTopN.u4DsmonMatrixTopNIndex = u4DsmonMatrixTopNIndex;

        pMatrixTopN =
            (tDsmonMatrixTopN *) RBTreeGetNext (DSMON_MATRIX_TOPN_TABLE,
                                                (tRBElem *) & MatrixTopN, NULL);
    }
    return pMatrixTopN;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNAddTopNEntry                                */
/*                                                                           */
/* Description  : Allocates memory and Adds Matrix TopN Entry into           */
/*                dsmonMatrixTopNRBTree                                      */
/*                                                                           */
/* Input        : pMatrixTopN                                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixTopN / NULL Pointer                         */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixTopN *
DsmonMatrixTopNAddTopNEntry (UINT4 u4DsmonMatrixTopNIndex,
                             tDsmonMatrixTopN * pMatrixTopN)
{
    tDsmonMatrixTopN   *pTopNEntry = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMatrixTopNAddTopNEntry \r\n");

    if ((pTopNEntry = (tDsmonMatrixTopN *)
         (MemAllocMemBlk (DSMON_MATRIX_TOPN_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Matrix TopN\r\n");
        return NULL;
    }

    MEMSET (pTopNEntry, DSMON_INIT_VAL, sizeof (tDsmonMatrixTopN));
    MEMCPY (pTopNEntry, pMatrixTopN, sizeof (tDsmonMatrixTopN));
    pTopNEntry->u4DsmonMatrixTopNIndex = u4DsmonMatrixTopNIndex;

    if (RBTreeAdd (DSMON_MATRIX_TOPN_TABLE, (tRBElem *) pTopNEntry)
        == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonMatrixTopNAddTopNEntry Entry is Added\r\n");
        return pTopNEntry;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonMatrixTopNAddTopNEntry entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_MATRIX_TOPN_POOL, (UINT1 *) pTopNEntry);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNDelTopNEntry                                */
/*                                                                           */
/* Description  : Releases memory and Removes Matrix Entry from              */
/*                dsmonMatrixTopNRBTree                                      */
/*                                                                           */
/* Input        :  pMatrixTopN                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonMatrixTopNDelTopNEntry (tDsmonMatrixTopN * pMatrixTopN)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMatrixTopNDelTopNEntry \r\n");

    /* Remove MatrixTopN. node from dsmonMatrixTopNRBTree */
    RBTreeRem (DSMON_MATRIX_TOPN_TABLE, (tRBElem *) pMatrixTopN);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_MATRIX_TOPN_POOL,
                            (UINT1 *) pMatrixTopN) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  MatrixTopN.\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNGetTopNEntry                                */
/*                                                                           */
/* Description  : Get Matrix TopN Entry for the given control index          */
/*                                                                           */
/* Input        : u4DsmonMatrixTopNIndex                                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonMatrixTopN / NULL Pointer                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonMatrixTopN *
DsmonMatrixTopNGetTopNEntry (UINT4 u4DsmonMatrixTopNControlIndex,
                             UINT4 u4DsmonMatrixTopNIndex)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL, MatrixTopN;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMatrixTopNGetTopNEntry \r\n");

    MEMSET (&MatrixTopN, DSMON_INIT_VAL, sizeof (tDsmonMatrixTopN));

    MatrixTopN.u4DsmonMatrixTopNCtlIndex = u4DsmonMatrixTopNControlIndex;
    MatrixTopN.u4DsmonMatrixTopNIndex = u4DsmonMatrixTopNIndex;

    pMatrixTopN = (tDsmonMatrixTopN *) RBTreeGet (DSMON_MATRIX_TOPN_TABLE,
                                                  (tRBElem *) & MatrixTopN);
    return pMatrixTopN;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMatrixTopNDelTopNReport                               */
/*                                                                           */
/* Description  : Delete the  TopN Entries if they are already present in    */
/*                dsmonMatrixTopNRBTree                                      */
/*                                                                           */
/* Input        : u4HostTopNCtlIndex                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
DsmonMatrixTopNDelTopNReport (UINT4 u4DsmonMatrixTopNCtlIndex)
{
    tDsmonMatrixTopN   *pTopNEntry = NULL;
    UINT4               u4MatrixTopNIndex = DSMON_ZERO;
    pTopNEntry =
        DsmonMatrixTopNGetNextTopNIndex (u4DsmonMatrixTopNCtlIndex,
                                         u4MatrixTopNIndex);

    while (pTopNEntry != NULL)
    {
        u4MatrixTopNIndex = pTopNEntry->u4DsmonMatrixTopNIndex;
        if (u4DsmonMatrixTopNCtlIndex != pTopNEntry->u4DsmonMatrixTopNCtlIndex)
        {
            break;
        }

        DsmonMatrixTopNDelTopNEntry (pTopNEntry);

        pTopNEntry = DsmonMatrixTopNGetNextTopNIndex
            (u4DsmonMatrixTopNCtlIndex, u4MatrixTopNIndex);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetSamplesFromMatrixTable                                  */
/*                                                                           */
/* Description  : Get the initial samples from matrix table, when the time   */
/*                remaining and duration values are same                     */
/*                                                                           */
/* Input        : u4MatrixCtlIndex                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GetSamplesFromMatrixTable (UINT4 u4MatrixCtlIndex)
{
    tDsmonMatrixSD     *pMatrixSDNode = NULL;
    tIpAddr             SrcIp, DstIp;
    UINT4               u4MatrixGrpIndex = DSMON_ZERO, u4MatrixNLIndex =
        DSMON_ZERO, u4MatrixALIndex = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY GetSamplesFromMatrixTable \n");

    MEMSET (&SrcIp, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstIp, DSMON_INIT_VAL, sizeof (tIpAddr));

    pMatrixSDNode =
        DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE, u4MatrixCtlIndex,
                                     u4MatrixGrpIndex, u4MatrixNLIndex,
                                     u4MatrixALIndex, &SrcIp, &DstIp);

    while (pMatrixSDNode != NULL)
    {

        if (pMatrixSDNode->u4DsmonMatrixCtlIndex != u4MatrixCtlIndex)
        {
            break;
        }
        MEMCPY (&pMatrixSDNode->DsmonPrevMatrixSDSample,
                &pMatrixSDNode->DsmonCurMatrixSDSample,
                sizeof (tDsmonMatrixSDSample));

        pMatrixSDNode = DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                                     pMatrixSDNode->
                                                     u4DsmonMatrixCtlIndex,
                                                     pMatrixSDNode->
                                                     u4DsmonMatrixSDGroupIndex,
                                                     pMatrixSDNode->
                                                     u4DsmonMatrixSDNLIndex,
                                                     pMatrixSDNode->
                                                     u4DsmonMatrixSDALIndex,
                                                     &pMatrixSDNode->
                                                     DsmonMatrixSDSourceAddress,
                                                     &pMatrixSDNode->
                                                     DsmonMatrixSDDestAddress);

    }                            /*end of while */
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FillMatrixSampleTopNRate                                   */
/*                                                                           */
/* Description  : Fill the topn rate values                                  */
/*                                                                           */
/* Input        : pSampleTopN,pMatrixSDNode,                                 */
/*                u4Pktcount,u4Octetcount,u4MatrixTopNCtlIndex               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
FillMatrixSampleTopNRate (tDsmonMatrixTopN * pSampleTopN, UINT4 u4Count,
                          tDsmonMatrixSD * pMatrixSDNode,
                          UINT4 u4Pktcount, UINT4 u4Octetcount,
                          tDsmonMatrixTopNCtl * pMatrixTopNCtl)
{
    UINT4               u4Index = DSMON_ZERO;
    UINT4               u4MinValue = DSMON_ZERO;
    UINT4               u4MinIndex = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY FillMatrixSampleTopNRate\r\n");

    if (u4Count < pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize)
    {
        pSampleTopN[u4Count].u4DsmonMatrixTopNCtlIndex =
            pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;

        pSampleTopN[u4Count].u4DsmonMatrixTopNAggGroup =
            pMatrixSDNode->u4DsmonMatrixSDGroupIndex;

        pSampleTopN[u4Count].u4DsmonMatrixTopNNLIndex =
            pMatrixSDNode->u4DsmonMatrixSDNLIndex;

        pSampleTopN[u4Count].u4DsmonMatrixTopNALIndex =
            pMatrixSDNode->u4DsmonMatrixSDALIndex;

        MEMCPY (&pSampleTopN[u4Count].DsmonMatrixTopNSrcAddr,
                &pMatrixSDNode->DsmonMatrixSDSourceAddress,
                sizeof (pMatrixSDNode->DsmonMatrixSDSourceAddress));

        MEMCPY (&pSampleTopN[u4Count].DsmonMatrixTopNDestAddr,
                &pMatrixSDNode->DsmonMatrixSDDestAddress,
                sizeof (pMatrixSDNode->DsmonMatrixSDDestAddress));

        pSampleTopN[u4Count].u4DsmonMatrixIpAddrLen =
            pMatrixSDNode->u4DsmonMatrixIpAddrLen;

        pSampleTopN[u4Count].u4DsmonMatrixTopNPktRate = u4Pktcount;

        pSampleTopN[u4Count].u4DsmonMatrixTopNRevPktRate = u4Pktcount;

        UINT8_RESET (pSampleTopN[u4Count].u8DsmonMatrixTopNHCPktRate);

        UINT8_RESET (pSampleTopN[u4Count].u8DsmonMatrixTopNHCRevPktRate);

        pSampleTopN[u4Count].u4DsmonMatrixTopNOctetRate = u4Octetcount;

        pSampleTopN[u4Count].u4DsmonMatrixTopNRevOctetRate = u4Octetcount;

        UINT8_RESET (pSampleTopN[u4Count].u8DsmonMatrixTopNHCOctetRate);

        UINT8_RESET (pSampleTopN[u4Count].u8DsmonMatirxTopNHCRevOctetRate);
    }
    else
    {
        /* find the least rate in the entries,
           and replace that with the new entry --
           this approach is chosen inorder to reduce the memory allocated,
           though the performance is not high */

        if (u4Octetcount == DSMON_ZERO)
        {
            u4MinValue = pSampleTopN[u4MinIndex].u4DsmonMatrixTopNPktRate;
        }
        if (u4Pktcount == DSMON_ZERO)
        {
            u4MinValue = pSampleTopN[u4MinIndex].u4DsmonMatrixTopNOctetRate;
        }

        while (u4Index < pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize)
        {
            if (u4Octetcount == DSMON_ZERO)
            {
                if (u4MinValue > pSampleTopN[u4Index].u4DsmonMatrixTopNPktRate)
                {
                    u4MinValue = pSampleTopN[u4Index].u4DsmonMatrixTopNPktRate;
                    u4MinIndex = u4Index;
                }
            }
            if (u4Pktcount == DSMON_ZERO)
            {
                if (u4MinValue >
                    pSampleTopN[u4Index].u4DsmonMatrixTopNOctetRate)
                {
                    u4MinValue =
                        pSampleTopN[u4Index].u4DsmonMatrixTopNOctetRate;
                    u4MinIndex = u4Index;
                }
            }

            u4Index++;
        }

        if ((u4MinValue <= u4Pktcount) || (u4MinValue <= u4Octetcount))

        {

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNCtlIndex =
                pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNAggGroup =
                pMatrixSDNode->u4DsmonMatrixSDGroupIndex;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNNLIndex =
                pMatrixSDNode->u4DsmonMatrixSDNLIndex;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNALIndex =
                pMatrixSDNode->u4DsmonMatrixSDALIndex;

            MEMCPY (&pSampleTopN[u4MinIndex].DsmonMatrixTopNSrcAddr,
                    &pMatrixSDNode->DsmonMatrixSDSourceAddress,
                    sizeof (pMatrixSDNode->DsmonMatrixSDSourceAddress));

            MEMCPY (&pSampleTopN[u4MinIndex].DsmonMatrixTopNDestAddr,
                    &pMatrixSDNode->DsmonMatrixSDDestAddress,
                    sizeof (pMatrixSDNode->DsmonMatrixSDDestAddress));

            pSampleTopN[u4MinIndex].u4DsmonMatrixIpAddrLen =
                pMatrixSDNode->u4DsmonMatrixIpAddrLen;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNPktRate = u4Pktcount;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNRevPktRate = u4Pktcount;

            UINT8_RESET (pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCPktRate);

            UINT8_RESET (pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCRevPktRate);

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNOctetRate = u4Octetcount;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNRevOctetRate =
                u4Octetcount;

            UINT8_RESET (pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCOctetRate);

            UINT8_RESET (pSampleTopN[u4MinIndex].
                         u8DsmonMatirxTopNHCRevOctetRate);

        }

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FillMatrixSampleTopNHCRate                                 */
/*                                                                           */
/* Description  : Fill the sample topn high capacity rate values             */
/*                                                                           */
/* Input        : pSampleTopN,pMatrixSDNode                                  */
/*                u4MsnValue,u4LsnValue,                                     */
/*                u4MatrixTopNCtlIndex,u4Type                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
FillMatrixSampleTopNHCRate (tDsmonMatrixTopN * pSampleTopN, UINT4 u4Count,
                            tDsmonMatrixSD * pMatrixSDNode, UINT4 u4MsnValue,
                            UINT4 u4LsnValue,
                            tDsmonMatrixTopNCtl * pMatrixTopNCtl, UINT4 u4Type)
{
    UINT4               u4Index = DSMON_ZERO;
    tUint8              u8MinValue, u8NewValue;
    UINT4               u4MinIndex = DSMON_ZERO;
    INT4                i4Value = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY FillMatrixSampleTopNHCRate\r\n");

    UINT8_RESET (u8MinValue);
    UINT8_RESET (u8NewValue);

    if (u4Count < pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize)
    {
        pSampleTopN[u4Count].u4DsmonMatrixTopNCtlIndex =
            pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;

        pSampleTopN[u4Count].u4DsmonMatrixTopNAggGroup =
            pMatrixSDNode->u4DsmonMatrixSDGroupIndex;

        pSampleTopN[u4Count].u4DsmonMatrixTopNNLIndex =
            pMatrixSDNode->u4DsmonMatrixSDNLIndex;

        pSampleTopN[u4Count].u4DsmonMatrixTopNALIndex =
            pMatrixSDNode->u4DsmonMatrixSDALIndex;

        MEMCPY (&pSampleTopN[u4Count].DsmonMatrixTopNSrcAddr,
                &pMatrixSDNode->DsmonMatrixSDSourceAddress,
                sizeof (pMatrixSDNode->DsmonMatrixSDSourceAddress));

        MEMCPY (&pSampleTopN[u4Count].DsmonMatrixTopNDestAddr,
                &pMatrixSDNode->DsmonMatrixSDDestAddress,
                sizeof (pMatrixSDNode->DsmonMatrixSDDestAddress));

        pSampleTopN[u4Count].u4DsmonMatrixIpAddrLen =
            pMatrixSDNode->u4DsmonMatrixIpAddrLen;

        if (u4Type == DSMON_PKT_RATE)
        {
            pSampleTopN[u4Count].u4DsmonMatrixTopNPktRate = u4LsnValue;

            pSampleTopN[u4Count].u4DsmonMatrixTopNRevPktRate = u4LsnValue;

            pSampleTopN[u4Count].u8DsmonMatrixTopNHCPktRate.u4HiWord =
                u4MsnValue;
            pSampleTopN[u4Count].u8DsmonMatrixTopNHCPktRate.u4LoWord =
                u4LsnValue;

            pSampleTopN[u4Count].u8DsmonMatrixTopNHCRevPktRate.u4HiWord =
                u4MsnValue;
            pSampleTopN[u4Count].u8DsmonMatrixTopNHCRevPktRate.u4LoWord =
                u4LsnValue;
        }
        if (u4Type == DSMON_OCTET_RATE)
        {
            pSampleTopN[u4Count].u4DsmonMatrixTopNOctetRate = u4LsnValue;

            pSampleTopN[u4Count].u4DsmonMatrixTopNRevOctetRate = u4LsnValue;

            pSampleTopN[u4Count].u8DsmonMatrixTopNHCOctetRate.u4HiWord =
                u4MsnValue;
            pSampleTopN[u4Count].u8DsmonMatrixTopNHCOctetRate.u4LoWord =
                u4LsnValue;

            pSampleTopN[u4Count].u8DsmonMatirxTopNHCRevOctetRate.u4HiWord =
                u4MsnValue;
            pSampleTopN[u4Count].u8DsmonMatirxTopNHCRevOctetRate.u4LoWord =
                u4LsnValue;
        }
    }
    else
    {
        /* find the least rate in the entries,
           and replace that with the new entry --
           this approach is chosen inorder to reduce the memory allocated,
           though the performance is not high */
        u8NewValue.u4HiWord = u4MsnValue;
        u8NewValue.u4LoWord = u4LsnValue;

        if (u4Type == DSMON_PKT_RATE)
        {
            u8MinValue.u4HiWord =
                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCPktRate.u4HiWord;
            u8MinValue.u4LoWord =
                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCPktRate.u4LoWord;

            while (u4Index < pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize)
            {
                UINT8_CMP (pSampleTopN[u4Index].u8DsmonMatrixTopNHCPktRate,
                           u8MinValue, i4Value);
                if (i4Value == -1)
                {
                    u8MinValue.u4HiWord =
                        pSampleTopN[u4Index].u8DsmonMatrixTopNHCPktRate.
                        u4HiWord;
                    u8MinValue.u4LoWord =
                        pSampleTopN[u4Index].u8DsmonMatrixTopNHCPktRate.
                        u4LoWord;
                }
                u4Index++;
            }
        }
        if (u4Type == DSMON_OCTET_RATE)
        {
            u8MinValue.u4HiWord =
                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCOctetRate.u4HiWord;
            u8MinValue.u4LoWord =
                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCOctetRate.u4LoWord;

            while (u4Index < pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize)
            {
                UINT8_CMP (pSampleTopN[u4Index].u8DsmonMatrixTopNHCOctetRate,
                           u8MinValue, i4Value);
                if (i4Value == -1)
                {
                    u8MinValue.u4HiWord =
                        pSampleTopN[u4Index].u8DsmonMatrixTopNHCOctetRate.
                        u4HiWord;
                    u8MinValue.u4LoWord =
                        pSampleTopN[u4Index].u8DsmonMatrixTopNHCOctetRate.
                        u4LoWord;
                }
                u4Index++;
            }
        }

        UINT8_CMP (u8MinValue, u8NewValue, i4Value);

        if ((i4Value == -1) || (i4Value == 0))
        {

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNCtlIndex =
                pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNAggGroup =
                pMatrixSDNode->u4DsmonMatrixSDGroupIndex;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNNLIndex =
                pMatrixSDNode->u4DsmonMatrixSDNLIndex;

            pSampleTopN[u4MinIndex].u4DsmonMatrixTopNALIndex =
                pMatrixSDNode->u4DsmonMatrixSDALIndex;

            MEMCPY (&pSampleTopN[u4MinIndex].DsmonMatrixTopNSrcAddr,
                    &pMatrixSDNode->DsmonMatrixSDSourceAddress,
                    sizeof (pMatrixSDNode->DsmonMatrixSDSourceAddress));

            MEMCPY (&pSampleTopN[u4MinIndex].DsmonMatrixTopNDestAddr,
                    &pMatrixSDNode->DsmonMatrixSDDestAddress,
                    sizeof (pMatrixSDNode->DsmonMatrixSDDestAddress));

            pSampleTopN[u4MinIndex].u4DsmonMatrixIpAddrLen =
                pMatrixSDNode->u4DsmonMatrixIpAddrLen;

            if (u4Type == DSMON_PKT_RATE)
            {
                pSampleTopN[u4MinIndex].u4DsmonMatrixTopNPktRate = u4LsnValue;

                pSampleTopN[u4MinIndex].u4DsmonMatrixTopNRevPktRate =
                    u4LsnValue;

                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCPktRate.u4HiWord =
                    u4MsnValue;
                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCPktRate.u4LoWord =
                    u4LsnValue;

                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCRevPktRate.u4HiWord =
                    u4MsnValue;
                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCRevPktRate.u4LoWord =
                    u4LsnValue;
            }

            if (u4Type == DSMON_OCTET_RATE)
            {
                pSampleTopN[u4MinIndex].u4DsmonMatrixTopNOctetRate = u4LsnValue;

                pSampleTopN[u4MinIndex].u4DsmonMatrixTopNRevOctetRate =
                    u4LsnValue;

                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCOctetRate.u4HiWord =
                    u4MsnValue;
                pSampleTopN[u4MinIndex].u8DsmonMatrixTopNHCOctetRate.u4LoWord =
                    u4LsnValue;

                pSampleTopN[u4MinIndex].u8DsmonMatirxTopNHCRevOctetRate.
                    u4HiWord = u4MsnValue;
                pSampleTopN[u4MinIndex].u8DsmonMatirxTopNHCRevOctetRate.
                    u4LoWord = u4LsnValue;
            }
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetMatrixTopNEntries                                       */
/*                                                                           */
/* Description  : Get the current samples from Matrix table                  */
/*                                                                           */
/* Input        : pMatrixTopNCtl,pSampleTopN                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u4Count                                                    */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
GetMatrixTopNEntries (tDsmonMatrixTopNCtl * pMatrixTopNCtl,
                      tDsmonMatrixTopN * pSampleTopN)
{
    UINT4               u4Count = DSMON_ZERO;
    UINT4               u4DeltaPkt = DSMON_ZERO, u4DeltaOct = DSMON_ZERO;
    UINT4               u4MatrixCtlIndex = DSMON_ZERO, u4MatrixGrpIndex =
        DSMON_ZERO;
    UINT4               u4MatrixNLIndex = DSMON_ZERO, u4MatrixALIndex =
        DSMON_ZERO;
    tIpAddr             SrcIp, DstIp;

    tDsmonMatrixSD     *pMatrixSDNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY GetMatrixTopNEntries \n");

    MEMSET (&SrcIp, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstIp, DSMON_INIT_VAL, sizeof (tIpAddr));

    pMatrixSDNode = DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                                 u4MatrixCtlIndex,
                                                 u4MatrixGrpIndex,
                                                 u4MatrixNLIndex,
                                                 u4MatrixALIndex, &SrcIp,
                                                 &DstIp);

    while (pMatrixSDNode != NULL)
    {
        if (pMatrixSDNode->u4DsmonMatrixCtlIndex !=
            pMatrixTopNCtl->u4DsmonMatrixTopNCtlMatrixIndex)
        {
            break;
        }
        switch (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRateBase)
        {

            case matrixTopNPkts:

                if (pMatrixSDNode->DsmonPrevMatrixSDSample.
                    u4DsmonMatrixSDPkts != DSMON_ZERO)
                {
                    u4DeltaPkt =
                        pMatrixSDNode->DsmonCurMatrixSDSample.
                        u4DsmonMatrixSDPkts -
                        pMatrixSDNode->DsmonPrevMatrixSDSample.
                        u4DsmonMatrixSDPkts;

                    u4DeltaOct = DSMON_ZERO;

                    if (u4DeltaPkt > DSMON_ZERO)
                    {
                        FillMatrixSampleTopNRate (pSampleTopN, u4Count,
                                                  pMatrixSDNode, u4DeltaPkt,
                                                  u4DeltaOct, pMatrixTopNCtl);

                        u4Count++;
                    }

                }

                break;

            case matrixTopNOctets:

                if (pMatrixSDNode->DsmonPrevMatrixSDSample.
                    u4DsmonMatrixSDOctets != DSMON_ZERO)
                {
                    u4DeltaOct =
                        pMatrixSDNode->DsmonCurMatrixSDSample.
                        u4DsmonMatrixSDOctets -
                        pMatrixSDNode->DsmonPrevMatrixSDSample.
                        u4DsmonMatrixSDOctets;

                    u4DeltaPkt = DSMON_ZERO;

                    if (u4DeltaOct > DSMON_ZERO)
                    {
                        FillMatrixSampleTopNRate (pSampleTopN, u4Count,
                                                  pMatrixSDNode, u4DeltaPkt,
                                                  u4DeltaOct, pMatrixTopNCtl);

                        u4Count++;
                    }

                }

                break;

            case matrixTopNHCPkts:

                if ((pMatrixSDNode->DsmonPrevMatrixSDSample.
                     u8DsmonMatrixSDHCPkts.u4HiWord != DSMON_ZERO)
                    && (pMatrixSDNode->DsmonPrevMatrixSDSample.
                        u8DsmonMatrixSDHCPkts.u4LoWord != DSMON_ZERO))
                {
                    UINT8_SUB (pMatrixSDNode->DsmonCurMatrixSDSample.
                               u8DsmonMatrixSDHCPkts,
                               pMatrixSDNode->DsmonPrevMatrixSDSample.
                               u8DsmonMatrixSDHCPkts,
                               pMatrixSDNode->DsmonPrevMatrixSDSample.
                               u8DsmonMatrixSDHCPkts);

                    if ((pMatrixSDNode->DsmonPrevMatrixSDSample.
                         u8DsmonMatrixSDHCPkts.u4HiWord > DSMON_ZERO)
                        || (pMatrixSDNode->DsmonPrevMatrixSDSample.
                            u8DsmonMatrixSDHCPkts.u4LoWord > DSMON_ZERO))
                    {
                        FillMatrixSampleTopNHCRate (pSampleTopN, u4Count,
                                                    pMatrixSDNode,
                                                    pMatrixSDNode->
                                                    DsmonCurMatrixSDSample.
                                                    u8DsmonMatrixSDHCPkts.
                                                    u4HiWord,
                                                    pMatrixSDNode->
                                                    DsmonCurMatrixSDSample.
                                                    u8DsmonMatrixSDHCPkts.
                                                    u4LoWord, pMatrixTopNCtl,
                                                    1);

                        u4Count++;
                    }

                }

                break;

            case matrixTopNHCOctets:

                if ((pMatrixSDNode->DsmonPrevMatrixSDSample.
                     u8DsmonMatrixSDHCOctets.u4HiWord != DSMON_ZERO)
                    && (pMatrixSDNode->DsmonPrevMatrixSDSample.
                        u8DsmonMatrixSDHCOctets.u4LoWord != DSMON_ZERO))
                {
                    UINT8_SUB (pMatrixSDNode->DsmonCurMatrixSDSample.
                               u8DsmonMatrixSDHCOctets,
                               pMatrixSDNode->DsmonPrevMatrixSDSample.
                               u8DsmonMatrixSDHCOctets,
                               pMatrixSDNode->DsmonPrevMatrixSDSample.
                               u8DsmonMatrixSDHCOctets);

                    if ((pMatrixSDNode->DsmonPrevMatrixSDSample.
                         u8DsmonMatrixSDHCOctets.u4HiWord > DSMON_ZERO)
                        || (pMatrixSDNode->DsmonPrevMatrixSDSample.
                            u8DsmonMatrixSDHCOctets.u4LoWord > DSMON_ZERO))
                    {
                        FillMatrixSampleTopNHCRate (pSampleTopN, u4Count,
                                                    pMatrixSDNode,
                                                    pMatrixSDNode->
                                                    DsmonPrevMatrixSDSample.
                                                    u8DsmonMatrixSDHCOctets.
                                                    u4HiWord,
                                                    pMatrixSDNode->
                                                    DsmonPrevMatrixSDSample.
                                                    u8DsmonMatrixSDHCOctets.
                                                    u4LoWord, pMatrixTopNCtl,
                                                    2);

                        u4Count++;
                    }

                }

                break;

        }                        /* end of switch case */

        pMatrixSDNode = DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                                     pMatrixSDNode->
                                                     u4DsmonMatrixCtlIndex,
                                                     pMatrixSDNode->
                                                     u4DsmonMatrixSDGroupIndex,
                                                     pMatrixSDNode->
                                                     u4DsmonMatrixSDNLIndex,
                                                     pMatrixSDNode->
                                                     u4DsmonMatrixSDALIndex,
                                                     &pMatrixSDNode->
                                                     DsmonMatrixSDSourceAddress,
                                                     &pMatrixSDNode->
                                                     DsmonMatrixSDDestAddress);

    }                            /*end of while */

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT GetMatrixTopNEntries \n");

    return u4Count;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SetSortedMatrixTopNTable                                   */
/*                                                                           */
/* Description  : Generates topn report of size, granted size                */
/*                                                                           */
/* Input        : pSampleTopN,u4Count,u4GrantedSize                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
SetSortedMatrixTopNTable (tDsmonMatrixTopN * pSampleTopN, UINT4 u4Count,
                          UINT4 u4GrantedSize)
{
    UINT4               u4Pass = DSMON_ZERO;
    UINT4               u4EntryCount = DSMON_ZERO;
    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY SetSortedMatrixTopNTable \n");

    u4EntryCount = (u4Count < u4GrantedSize) ? u4Count : u4GrantedSize;

    u4Count = u4EntryCount - 1;

    for (u4Pass = DSMON_ONE; u4Pass <= u4EntryCount; u4Pass++)
    {
        pMatrixTopN =
            DsmonMatrixTopNAddTopNEntry (u4Pass, &pSampleTopN[u4Count]);

        if (pMatrixTopN == NULL)
        {
            DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                       " Memory Allocation / \
            RBTree Add Failed - Matrix TopN Table \n");

            return OSIX_FAILURE;
        }

        u4Count--;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT SetSortedMatrixTopNTable \n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CompareMatrixTopNPktRate                                   */
/*                                                                           */
/* Description  : Compare function for qsort based on TopN Pkt Rate          */
/*                                                                           */
/* Input        : pParam1, pParam2                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 - if pParam1 is less than pParam2                       */
/*                1  - if pParam1 is greater than pParam2                    */
/*                0  - if pParam1 and pParam2 are equal                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
CompareMatrixTopNPktRate (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tDsmonMatrixTopN *pSample1 = (CONST tDsmonMatrixTopN *) pParam1;
    CONST tDsmonMatrixTopN *pSample2 = (CONST tDsmonMatrixTopN *) pParam2;

    if (pSample1->u4DsmonMatrixTopNPktRate < pSample2->u4DsmonMatrixTopNPktRate)
    {
        return -1;
    }
    if (pSample1->u4DsmonMatrixTopNPktRate > pSample2->u4DsmonMatrixTopNPktRate)
    {
        return 1;
    }
    if (pSample1->u4DsmonMatrixTopNPktRate ==
        pSample2->u4DsmonMatrixTopNPktRate)
    {
        return 0;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CompareMatrixTopNOctetRate                                 */
/*                                                                           */
/* Description  : Compare function for qsort based on TopN Octet Rate        */
/*                                                                           */
/* Input        : pParam1, pParam2                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 - if pParam1 is less than pParam2                       */
/*                1  - if pParam1 is greater than pParam2                    */
/*                0  - if pParam1 and pParam2 are equal                      */
/*                                                                           */
/*****************************************************************************/
PRIVATE INT4
CompareMatrixTopNOctetRate (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tDsmonMatrixTopN *pSample1 = (CONST tDsmonMatrixTopN *) pParam1;
    CONST tDsmonMatrixTopN *pSample2 = (CONST tDsmonMatrixTopN *) pParam2;

    if (pSample1->u4DsmonMatrixTopNOctetRate <
        pSample2->u4DsmonMatrixTopNOctetRate)
    {
        return -1;
    }
    if (pSample1->u4DsmonMatrixTopNOctetRate >
        pSample2->u4DsmonMatrixTopNOctetRate)
    {
        return 1;
    }
    if (pSample1->u4DsmonMatrixTopNOctetRate ==
        pSample2->u4DsmonMatrixTopNOctetRate)
    {
        return 0;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CompareMatrixTopNHCPktRate                                 */
/*                                                                           */
/* Description  : Compare function for qsort based on TopN HCPkt Rate        */
/*                                                                           */
/* Input        : pParam1, pParam2                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 - if pParam1 is less than pParam2                       */
/*                1  - if pParam1 is greater than pParam2                    */
/*                0  - if pParam1 and pParam2 are equal                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
CompareMatrixTopNHCPktRate (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tDsmonMatrixTopN *pSample1 = (CONST tDsmonMatrixTopN *) pParam1;
    CONST tDsmonMatrixTopN *pSample2 = (CONST tDsmonMatrixTopN *) pParam2;
    INT4                i4Value = DSMON_ZERO;

    UINT8_CMP (pSample1->u8DsmonMatrixTopNHCPktRate,
               pSample2->u8DsmonMatrixTopNHCPktRate, i4Value);

    return i4Value;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CompareMatrixTopNHCOctetRate                               */
/*                                                                           */
/* Description  : Compare function for qsort based on TopN HCOctet Rate      */
/*                                                                           */
/* Input        : pParam1, pParam2                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 - if pParam1 is less than pParam2                       */
/*                1  - if pParam1 is greater than pParam2                    */
/*                0  - if pParam1 and pParam2 are equal                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
CompareMatrixTopNHCOctetRate (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tDsmonMatrixTopN *pSample1 = (CONST tDsmonMatrixTopN *) pParam1;
    CONST tDsmonMatrixTopN *pSample2 = (CONST tDsmonMatrixTopN *) pParam2;

    INT4                i4Value = DSMON_ZERO;

    UINT8_CMP (pSample1->u8DsmonMatrixTopNHCOctetRate,
               pSample2->u8DsmonMatrixTopNHCOctetRate, i4Value);

    return i4Value;
}

/***********************************************************************
 * Function           : DsmonMatrixTopNUpdateTable                     *
 *                                                                     *
 * Input (s)          : None.                                          *
 *                                                                     *
 * Output (s)         : None.                                          *
 *                                                                     *
 * Returns            : None.                                          *
 *                                                                     *
 * Global Variables                                                    *
 *             Used   :                                                *
 *                                                                     *
 * Side effects       : None                                           *
 *                                                                     *
 * Action :                                                            *
 *                                                                     *
 **********************************************************************/

VOID                DsmonMatrixTopNUpdateTable
ARG_LIST ((VOID))
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;
    tDsmonMatrixSampleTopN *pSampleTopN = NULL;
    UINT4               u4Count = DSMON_ZERO;
    UINT4               u4EntryCount = DSMON_ZERO;
    UINT4               u4MatrixTopNCtlIndex = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMatrixTopNUpdateTable \n");

    if ((pSampleTopN = (tDsmonMatrixSampleTopN *)
         (MemAllocMemBlk (DSMON_MATRIX_SAMPLE_TOPN_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Matrix Sample TopN\r\n");
        return;
    }

    MEMSET (pSampleTopN, 0, sizeof (tDsmonMatrixSampleTopN));

    pMatrixTopNCtl = DsmonMatrixTopNGetNextCtlIndex (u4MatrixTopNCtlIndex);

    while (pMatrixTopNCtl != NULL)
    {
        u4MatrixTopNCtlIndex = pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex;
        if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus == ACTIVE)
        {
            if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem > DSMON_ZERO)
            {
                if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem ==
                    pMatrixTopNCtl->u4DsmonMatrixTopNCtlDuration)
                {

                    GetSamplesFromMatrixTable (pMatrixTopNCtl->
                                               u4DsmonMatrixTopNCtlMatrixIndex);
                }
                else
                {
                    if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem == 1)
                    {

                        u4Count =
                            GetMatrixTopNEntries (pMatrixTopNCtl,
                                                  (tDsmonMatrixTopN *)
                                                  pSampleTopN->SampleTopN);

                        if (u4Count > 0)
                        {
                            /* Has to free TopN Entries if they were already present in
                             * dsmonMatrixTopNRBTree */

                            DsmonMatrixTopNDelTopNReport (pMatrixTopNCtl->
                                                          u4DsmonMatrixTopNCtlIndex);

                            u4EntryCount =
                                (u4Count <
                                 pMatrixTopNCtl->
                                 u4DsmonMatrixTopNCtlGranSize) ? u4Count :
                                pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize;

                            /* Sort the TopN entries based on the rate base */

                            switch (pMatrixTopNCtl->
                                    u4DsmonMatrixTopNCtlRateBase)
                            {
                                case matrixTopNPkts:

                                    qsort ((tDsmonMatrixTopN *) pSampleTopN->
                                           SampleTopN, u4EntryCount,
                                           sizeof (tDsmonMatrixTopN),
                                           (CONST VOID *)
                                           CompareMatrixTopNPktRate);

                                    break;

                                case matrixTopNOctets:

                                    qsort ((tDsmonMatrixTopN *) pSampleTopN->
                                           SampleTopN, u4EntryCount,
                                           sizeof (tDsmonMatrixTopN),
                                           (CONST VOID *)
                                           CompareMatrixTopNOctetRate);

                                    break;

                                case matrixTopNHCPkts:

                                    qsort ((tDsmonMatrixTopN *) pSampleTopN->
                                           SampleTopN, u4EntryCount,
                                           sizeof (tDsmonMatrixTopN),
                                           (CONST VOID *)
                                           CompareMatrixTopNHCPktRate);

                                    break;

                                case matrixTopNHCOctets:

                                    qsort ((tDsmonMatrixTopN *) pSampleTopN->
                                           SampleTopN, u4EntryCount,
                                           sizeof (tDsmonMatrixTopN),
                                           (CONST VOID *)
                                           CompareMatrixTopNHCOctetRate);

                                    break;
                            }

                            if (SetSortedMatrixTopNTable
                                ((tDsmonMatrixTopN *) pSampleTopN->SampleTopN,
                                 u4Count,
                                 pMatrixTopNCtl->
                                 u4DsmonMatrixTopNCtlGranSize) == OSIX_FAILURE)
                            {
                                /*  TopN Report generation failed */
                                DSMON_TRC (DSMON_DEBUG_TRC,
                                           "OSIX Failure: TopN Report Generation failed \n");
                            }
                            else
                            {
                                pMatrixTopNCtl->
                                    u4DsmonMatrixTopNCtlGenReports++;
                            }
                        }
                        /* start another collection */

                        pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem =
                            pMatrixTopNCtl->u4DsmonMatrixTopNCtlDuration + 1;
                        pMatrixTopNCtl->u4DsmonMatrixTopNCtlStartTime =
                            (UINT4) OsixGetSysUpTime ();

                    }

                }
                pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem--;
            }
        }                        /*end of if - row status active */

        pMatrixTopNCtl = DsmonMatrixTopNGetNextCtlIndex (u4MatrixTopNCtlIndex);

    }                            /*end of while */

    MemReleaseMemBlock (DSMON_MATRIX_SAMPLE_TOPN_POOL, ((UINT1 *) pSampleTopN));

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMatrixTopNUpdateTable \n");
}
