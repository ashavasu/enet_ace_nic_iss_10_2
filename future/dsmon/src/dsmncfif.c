/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *                                                                  *
 * Description: Contains Function to notify the DSMON Task abt the  *
 *              change in Interface Status                          *
 *                                                                  *
 *******************************************************************/

#include "dsmninc.h"

/*****************************************************************************/
/* Function        : DsmonCfaIfaceUpdateChgs                                 */
/* Description     : This function notifies the Interface status changes to  */
/*             DSMON module and is invoked by CFA              */
/* Input(s)        : u2Port, u1OperStatus                                    */
/*                                                                           */
/* Output(s)       : None                                                    */
/*                                                                           */
/* Returns         : None                                         */
/*                                                                           */
/*****************************************************************************/

VOID
DsmonCfaIfaceUpdateChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonCfaIfaceUpdateChgs \r\n");

    switch (u1OperStatus)
    {
        case CFA_IF_UP:
        case CFA_IF_DOWN:
        {
            /* Notify to PktInfo */
            DsmonPktInfoUpdateIfaceChgs (u4IfIndex, u1OperStatus);

            /* Notify to Stats Group */
            DsmonStatsUpdateIfaceChgs (u4IfIndex, u1OperStatus);

            /* Notify to Pdist Group */
            DsmonPdistUpdateIfaceChgs (u4IfIndex, u1OperStatus);

            /* Notify to Host Group */
            DsmonHostUpdateIfaceChgs (u4IfIndex, u1OperStatus);

            /* Notify to Matrix Group */
            DsmonMatrixUpdateIfaceChgs (u4IfIndex, u1OperStatus);
        }
            break;
            /* When Interface is UP, nothing to be done. */
        case CFA_IF_DORM:
            break;
        default:
            break;
    }                            /* End of Switch */

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonCfaIfaceUpdateChgs \r\n");
}
