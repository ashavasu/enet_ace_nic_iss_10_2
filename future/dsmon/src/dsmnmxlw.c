/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *                                                                  *
 * $Id: dsmnmxlw.c,v 1.12 2012/12/13 14:25:10 siva Exp $           *
 *
 * Description: This file contains the low level nmh routines         *
 *        for DSMON Matrix module                                *
 *                                                                  *
 *******************************************************************/
/* dsmon matrix group */
#include "dsmninc.h"

/* LOW LEVEL Routines for Table : DsmonMatrixCtlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonMatrixCtlTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonMatrixCtlTable (INT4 i4DsmonMatrixCtlIndex)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhValidateIndexInstanceDsmonMatrixCtlTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4DsmonMatrixCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControlIndex out of range \n");

        return SNMP_FAILURE;
    }

    /* Get matrix Control Node corresponding to MatrixControlIndex */

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonMatrixCtlTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonMatrixCtlTable (INT4 *pi4DsmonMatrixCtlIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY  \
              nmhGetFirstIndexDsmonMatrixCtlTable \n");

    return nmhGetNextIndexDsmonMatrixCtlTable (DSMON_ZERO,
                                               pi4DsmonMatrixCtlIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonMatrixCtlTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
                nextDsmonMatrixCtlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonMatrixCtlTable (INT4 i4DsmonMatrixCtlIndex,
                                    INT4 *pi4NextDsmonMatrixCtlIndex)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY  \
              nmhGetNextIndexDsmonMatrixCtlTable \n");

    pMatrixCtlNode = DsmonMatrixGetNextCtlIndex (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonMatrixCtlIndex =
        (INT4) (pMatrixCtlNode->u4DsmonMatrixCtlIndex);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlDataSource
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlDataSource (INT4 i4DsmonMatrixCtlIndex,
                                tSNMP_OID_TYPE *
                                pRetValDsmonMatrixCtlDataSource)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY  \
              nmhGetDsmonMatrixCtlDataSource \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_GET_DATA_SOURCE_OID (pRetValDsmonMatrixCtlDataSource,
                               pMatrixCtlNode->u4DsmonMatrixDataSource);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlAggProfile
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlAggProfile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlAggProfile (INT4 i4DsmonMatrixCtlIndex,
                                INT4 *pi4RetValDsmonMatrixCtlAggProfile)
{
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixCtlAggProfile \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixCtlAggProfile =
        (INT4) (pMatrixCtlNode->u4DsmonMatrixCtlAggProfile);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlMaxDesiredEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlMaxDesiredEntries (INT4 i4DsmonMatrixCtlIndex,
                                       INT4
                                       *pi4RetValDsmonMatrixCtlMaxDesiredEntries)
{
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixCtlMaxDesiredEntries \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixCtlMaxDesiredEntries =
        pMatrixCtlNode->i4DsmonMatrixCtlMaxDesiredEnt;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlDroppedFrames
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlDroppedFrames (INT4 i4DsmonMatrixCtlIndex,
                                   UINT4 *pu4RetValDsmonMatrixCtlDroppedFrames)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY  \
              nmhGetDsmonMatrixCtlDroppedFrames \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixCtlDroppedFrames =
        pMatrixCtlNode->u4DsmonMatrixCtlDroppedFrames;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlInserts
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlInserts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlInserts (INT4 i4DsmonMatrixCtlIndex,
                             UINT4 *pu4RetValDsmonMatrixCtlInserts)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixCtlInserts \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixCtlInserts = pMatrixCtlNode->u4DsmonMatrixCtlInserts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlDeletes
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlDeletes (INT4 i4DsmonMatrixCtlIndex,
                             UINT4 *pu4RetValDsmonMatrixCtlDeletes)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixCtlDeletes \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixCtlDeletes = pMatrixCtlNode->u4DsmonMatrixCtlDeletes;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlCreateTime
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlCreateTime (INT4 i4DsmonMatrixCtlIndex,
                                UINT4 *pu4RetValDsmonMatrixCtlCreateTime)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY  \
              nmhGetDsmonMatrixCtlCreateTime \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixCtlCreateTime =
        pMatrixCtlNode->u4DsmonMatrixCtlCreateTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlOwner
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlOwner (INT4 i4DsmonMatrixCtlIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValDsmonMatrixCtlOwner)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixCtlOwner \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDsmonMatrixCtlOwner->pu1_OctetList,
            pMatrixCtlNode->au1DsmonMatrixCtlOwner, DSMON_MAX_OWNER_LEN);

    pRetValDsmonMatrixCtlOwner->i4_Length =
        STRLEN (pMatrixCtlNode->au1DsmonMatrixCtlOwner);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixCtlStatus
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                retValDsmonMatrixCtlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixCtlStatus (INT4 i4DsmonMatrixCtlIndex,
                            INT4 *pi4RetValDsmonMatrixCtlStatus)
{
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY  \
              nmhGetDsmonMatrixCtlStatus \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixCtlStatus = pMatrixCtlNode->u4DsmonMatrixCtlRowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDsmonMatrixCtlDataSource
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                setValDsmonMatrixCtlDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixCtlDataSource (INT4 i4DsmonMatrixCtlIndex,
                                tSNMP_OID_TYPE *
                                pSetValDsmonMatrixCtlDataSource)
{
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
              nmhSetDsmonMatrixCtlDataSource \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_SET_DATA_SOURCE_OID (pSetValDsmonMatrixCtlDataSource,
                               &(pMatrixCtlNode->u4DsmonMatrixDataSource));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixCtlAggProfile
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                setValDsmonMatrixCtlAggProfile
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixCtlAggProfile (INT4 i4DsmonMatrixCtlIndex,
                                INT4 i4SetValDsmonMatrixCtlAggProfile)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY  \
              nmhSetDsmonMatrixCtlAggProfile \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    pMatrixCtlNode->u4DsmonMatrixCtlAggProfile =
        (UINT4) (i4SetValDsmonMatrixCtlAggProfile);
    pMatrixCtlNode->pAggCtl =
        DsmonAggCtlGetEntry (i4SetValDsmonMatrixCtlAggProfile);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                setValDsmonMatrixCtlMaxDesiredEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixCtlMaxDesiredEntries (INT4 i4DsmonMatrixCtlIndex,
                                       INT4
                                       i4SetValDsmonMatrixCtlMaxDesiredEntries)
{

    UINT4               u4Minimum = 0;

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixCtlMaxDesiredEntries \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    u4Minimum = (i4SetValDsmonMatrixCtlMaxDesiredEntries != -1) ?
        ((i4SetValDsmonMatrixCtlMaxDesiredEntries < DSMON_MAX_DATA_PER_CTL) ?
         i4SetValDsmonMatrixCtlMaxDesiredEntries : DSMON_MAX_DATA_PER_CTL)
        : DSMON_MAX_DATA_PER_CTL;

    pMatrixCtlNode->i4DsmonMatrixCtlMaxDesiredEnt =
        i4SetValDsmonMatrixCtlMaxDesiredEntries;

    pMatrixCtlNode->u4DsmonMatrixCtlMaxDesiredSupported = u4Minimum;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixCtlOwner
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                setValDsmonMatrixCtlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixCtlOwner (INT4 i4DsmonMatrixCtlIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValDsmonMatrixCtlOwner)
{

    UINT4               u4Minimum = 0;

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixCtlOwner \n");

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    /* Get Stats Control Node corresponding to StatsControlIndex */

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Matrix Control Index \n");

        return SNMP_FAILURE;
    }

    u4Minimum = (pSetValDsmonMatrixCtlOwner->i4_Length < DSMON_MAX_OWNER_LEN) ?
        pSetValDsmonMatrixCtlOwner->i4_Length : DSMON_MAX_OWNER_LEN;

    MEMSET (pMatrixCtlNode->au1DsmonMatrixCtlOwner, DSMON_INIT_VAL,
            DSMON_MAX_OWNER_LEN);

    MEMCPY (pMatrixCtlNode->au1DsmonMatrixCtlOwner,
            pSetValDsmonMatrixCtlOwner->pu1_OctetList, u4Minimum);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixCtlStatus
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                setValDsmonMatrixCtlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixCtlStatus (INT4 i4DsmonMatrixCtlIndex,
                            INT4 i4SetValDsmonMatrixCtlStatus)
{

    tPortList          *pVlanPortList = NULL;
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    tDsmonAggCtl       *pAggCtlNode = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    INT4                i4Result = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixCtlStatus \n");

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));

    switch (i4SetValDsmonMatrixCtlStatus)
    {
        case CREATE_AND_WAIT:

            pMatrixCtlNode = DsmonMatrixAddCtlEntry (i4DsmonMatrixCtlIndex);

            if (pMatrixCtlNode == NULL)
            {
                DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                           " Memory Allocation / \
                 RBTree Add Failed - Matrix Control Table \n");

                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:

            pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

            if (pMatrixCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Matrix Control Index \n");

                return SNMP_FAILURE;
            }

            /* check for valid agg profile index */

            pAggCtlNode =
                DsmonAggCtlGetEntry (pMatrixCtlNode->
                                     u4DsmonMatrixCtlAggProfile);

            if (pAggCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure:\
                 Invalid Matrix Control Agg Profile Index \n");

                return SNMP_FAILURE;
            }

            /* check for valid datasource */

            if (CfaGetIfInfo (pMatrixCtlNode->u4DsmonMatrixDataSource, &IfInfo)
                == CFA_FAILURE)
            {
                /* ifindex is invalid */
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid interface Index \n");

                return SNMP_FAILURE;
            }

            if (((IfInfo.u1IfOperStatus != CFA_IF_UP) ||
                 (IfInfo.u1IfAdminStatus != CFA_IF_UP)) &&
                ((gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)))
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Interface OperStatus or Admin Status is in invalid state \n");

                return SNMP_FAILURE;
            }
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                DSMON_TRC (DSMON_FN_ENTRY,
                           "DsmonPdistDelCtlEntry: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));

            if (IfInfo.u1IfType == CFA_L3IPVLAN)
            {
                /* Get the VlanId from interface index */
                if (CfaGetVlanId
                    (pMatrixCtlNode->u4DsmonMatrixDataSource,
                     &u2VlanId) == CFA_FAILURE)
                {
                    DSMON_TRC1 (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Unable to Fetch VlanId of IfIndex %d\n", pMatrixCtlNode->u4DsmonMatrixDataSource);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }

                if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList) ==
                    L2IWF_FAILURE)
                {
                    DSMON_TRC1 (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Get Member Port List for VlanId %d failed \n", u2VlanId);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }
            }

            pMatrixCtlNode->u4DsmonMatrixCtlRowStatus = ACTIVE;

            pMatrixCtlNode->u4DsmonMatrixCtlCreateTime =
                (UINT4) OsixGetSysUpTime ();
#ifdef NPAPI_WANTED
            DsmonFsDSMONEnableProbe (pMatrixCtlNode->u4DsmonMatrixDataSource,
                                     u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            DsmonMatrixPopulateDataEntries (pMatrixCtlNode);

            DsmonMatrixTopNNotifyMtxCtlChgs ((UINT4) i4DsmonMatrixCtlIndex,
                                             (UINT4)
                                             i4SetValDsmonMatrixCtlStatus);
            DsmonPktInfoDelUnusedEntries ();
            break;

        case NOT_IN_SERVICE:

            pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

            if (pMatrixCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Matrix Control Index \n");

                return SNMP_FAILURE;
            }

            pMatrixCtlNode->u4DsmonMatrixCtlRowStatus = NOT_IN_SERVICE;

            DsmonMatrixTopNNotifyMtxCtlChgs ((UINT4) i4DsmonMatrixCtlIndex,
                                             (UINT4)
                                             i4SetValDsmonMatrixCtlStatus);
            /* check for valid datasource */

            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                DSMON_TRC (DSMON_FN_ENTRY,
                           "DsmonPdistDelCtlEntry: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (CfaGetIfInfo (pMatrixCtlNode->u4DsmonMatrixDataSource, &IfInfo)
                != CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId (pMatrixCtlNode->u4DsmonMatrixDataSource,
                                   &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }
#ifdef NPAPI_WANTED
            DsmonFsDSMONDisableProbe (pMatrixCtlNode->u4DsmonMatrixDataSource,
                                      u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            DsmonMatrixDelDataEntries ((UINT4) i4DsmonMatrixCtlIndex);
            DsmonPktInfoDelUnusedEntries ();
            break;

        case DESTROY:

            pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

            if (pMatrixCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Matrix Control Index \n");

                return SNMP_FAILURE;
            }
            DsmonMatrixTopNNotifyMtxCtlChgs ((UINT4) i4DsmonMatrixCtlIndex,
                                             (UINT4)
                                             i4SetValDsmonMatrixCtlStatus);

            i4Result = DsmonMatrixDelCtlEntry (pMatrixCtlNode);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Matrix Control Index \
                 RBTreeRemove failed - Matrix Control Table\n");
                return SNMP_FAILURE;
            }

            DsmonPktInfoDelUnusedEntries ();
            break;
        default:
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixCtlDataSource
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                testValDsmonMatrixCtlDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixCtlDataSource (UINT4 *pu4ErrorCode,
                                   INT4 i4DsmonMatrixCtlIndex,
                                   tSNMP_OID_TYPE *
                                   pTestValDsmonMatrixCtlDataSource)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixCtlDataSource \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4DsmonMatrixCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (DSMON_TEST_DATA_SOURCE_OID (pTestValDsmonMatrixCtlDataSource) == FALSE)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_DEBUG_TRC, "Invalid DataSource");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixCtlNode->u4DsmonMatrixCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixCtlAggProfile
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                testValDsmonMatrixCtlAggProfile
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixCtlAggProfile (UINT4 *pu4ErrorCode,
                                   INT4 i4DsmonMatrixCtlIndex,
                                   INT4 i4TestValDsmonMatrixCtlAggProfile)
{

    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;
    tDsmonAggCtl       *pAggCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixCtlAggProfile \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }
    if ((i4DsmonMatrixCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pAggCtlNode = DsmonAggCtlGetEntry (i4TestValDsmonMatrixCtlAggProfile);

    if (pAggCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: AggControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixCtlNode->u4DsmonMatrixCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                testValDsmonMatrixCtlMaxDesiredEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixCtlMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                          INT4 i4DsmonMatrixCtlIndex,
                                          INT4
                                          i4TestValDsmonMatrixCtlMaxDesiredEntries)
{
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixCtlMaxDesiredEntries \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4DsmonMatrixCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValDsmonMatrixCtlMaxDesiredEntries < -1) ||
        (i4TestValDsmonMatrixCtlMaxDesiredEntries > DSMON_MAX_DATA_PER_CTL) ||
        (i4TestValDsmonMatrixCtlMaxDesiredEntries == DSMON_ZERO))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MaxDesiredEntries  out of range \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixCtlNode->u4DsmonMatrixCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixCtlOwner
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                testValDsmonMatrixCtlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixCtlOwner (UINT4 *pu4ErrorCode, INT4 i4DsmonMatrixCtlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValDsmonMatrixCtlOwner)
{
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixCtlOwner \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }
    if ((i4DsmonMatrixCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((pTestValDsmonMatrixCtlOwner->i4_Length < DSMON_ZERO) ||
        (pTestValDsmonMatrixCtlOwner->i4_Length > DSMON_MAX_OWNER_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid Owner length \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        return SNMP_FAILURE;

    }

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixCtlNode->u4DsmonMatrixCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixCtlStatus
 Input       :  The Indices
                DsmonMatrixCtlIndex

                The Object
                testValDsmonMatrixCtlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixCtlStatus (UINT4 *pu4ErrorCode, INT4 i4DsmonMatrixCtlIndex,
                               INT4 i4TestValDsmonMatrixCtlStatus)
{
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixCtlStatus \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }
    if ((i4DsmonMatrixCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pMatrixCtlNode = DsmonMatrixGetCtlEntry (i4DsmonMatrixCtlIndex);

    if (pMatrixCtlNode == NULL)
    {

        if (i4TestValDsmonMatrixCtlStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValDsmonMatrixCtlStatus == ACTIVE) ||
            (i4TestValDsmonMatrixCtlStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonMatrixCtlStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DsmonMatrixCtlTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DsmonMatrixCtlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonMatrixSDTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonMatrixSDTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonMatrixSDTable (INT4 i4DsmonMatrixCtlIndex,
                                            UINT4 u4DsmonMatrixTimeMark,
                                            INT4 i4DsmonAggGroupIndex,
                                            INT4 i4DsmonMatrixNLIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pDsmonMatrixSourceAddress,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pDsmonMatrixDestAddress,
                                            INT4 i4DsmonMatrixALIndex)
{

    tDsmonMatrixSD     *pMatrixSDNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    UNUSED_PARAM (u4DsmonMatrixTimeMark);

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhValidateIndexInstanceDsmonMatrixSDTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixSDNode = DsmonMatrixGetDataEntry (DSMON_MATRIXSD_TABLE,
                                             i4DsmonMatrixCtlIndex,
                                             i4DsmonAggGroupIndex,
                                             i4DsmonMatrixNLIndex,
                                             i4DsmonMatrixALIndex,
                                             &SrcAddr, &DstAddr);

    if (pMatrixSDNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Source Address or \
                   Matrix Dest Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonMatrixSDTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDsmonMatrixSDTable (INT4 *pi4DsmonMatrixCtlIndex,
                                    UINT4 *pu4DsmonMatrixTimeMark,
                                    INT4 *pi4DsmonAggGroupIndex,
                                    INT4 *pi4DsmonMatrixNLIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pDsmonMatrixSourceAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pDsmonMatrixDestAddress,
                                    INT4 *pi4DsmonMatrixALIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetFirstIndexDsmonMatrixSDTable \n");

    return nmhGetNextIndexDsmonMatrixSDTable (DSMON_ZERO,
                                              pi4DsmonMatrixCtlIndex,
                                              DSMON_ZERO,
                                              pu4DsmonMatrixTimeMark,
                                              DSMON_ZERO, pi4DsmonAggGroupIndex,
                                              DSMON_ZERO, pi4DsmonMatrixNLIndex,
                                              DSMON_ZERO,
                                              pDsmonMatrixSourceAddress,
                                              DSMON_ZERO,
                                              pDsmonMatrixDestAddress,
                                              DSMON_ZERO,
                                              pi4DsmonMatrixALIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonMatrixSDTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
                nextDsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                nextDsmonMatrixTimeMark
                DsmonAggGroupIndex
                nextDsmonAggGroupIndex
                DsmonMatrixNLIndex
                nextDsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                nextDsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                nextDsmonMatrixDestAddress
                DsmonMatrixALIndex
                nextDsmonMatrixALIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonMatrixSDTable (INT4 i4DsmonMatrixCtlIndex,
                                   INT4 *pi4NextDsmonMatrixCtlIndex,
                                   UINT4 u4DsmonMatrixTimeMark,
                                   UINT4 *pu4NextDsmonMatrixTimeMark,
                                   INT4 i4DsmonAggGroupIndex,
                                   INT4 *pi4NextDsmonAggGroupIndex,
                                   INT4 i4DsmonMatrixNLIndex,
                                   INT4 *pi4NextDsmonMatrixNLIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pDsmonMatrixSourceAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextDsmonMatrixSourceAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pDsmonMatrixDestAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextDsmonMatrixDestAddress,
                                   INT4 i4DsmonMatrixALIndex,
                                   INT4 *pi4NextDsmonMatrixALIndex)
{
    tDsmonMatrixSD     *pMatrixSDNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    UNUSED_PARAM (u4DsmonMatrixTimeMark);

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetNextIndexDsmonMatrixSDTable \n");

    if (pDsmonMatrixSourceAddress == NULL)
    {

        MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

        MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
        MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
        if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
        {
            MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                    DSMON_IPV6_MAX_LEN);
            MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                    DSMON_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&SrcAddr.u4_addr[3],
                    pDsmonMatrixSourceAddress->pu1_OctetList,
                    DSMON_IPV4_MAX_LEN);
            MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                    DSMON_IPV4_MAX_LEN);
        }
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixSDNode =
        DsmonMatrixGetNextDataIndex (DSMON_MATRIXSD_TABLE,
                                     i4DsmonMatrixCtlIndex,
                                     i4DsmonAggGroupIndex, i4DsmonMatrixNLIndex,
                                     i4DsmonMatrixALIndex, &SrcAddr, &DstAddr);

    if (pMatrixSDNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Source Address or \
                   Matrix Dest Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonMatrixCtlIndex = pMatrixSDNode->u4DsmonMatrixCtlIndex;

    *pu4NextDsmonMatrixTimeMark = DSMON_ZERO;

    *pi4NextDsmonAggGroupIndex = pMatrixSDNode->u4DsmonMatrixSDGroupIndex;

    *pi4NextDsmonMatrixNLIndex = pMatrixSDNode->u4DsmonMatrixSDNLIndex;

    *pi4NextDsmonMatrixALIndex = pMatrixSDNode->u4DsmonMatrixSDALIndex;

    pNextDsmonMatrixSourceAddress->i4_Length =
        pMatrixSDNode->u4DsmonMatrixIpAddrLen;
    pNextDsmonMatrixDestAddress->i4_Length =
        pMatrixSDNode->u4DsmonMatrixIpAddrLen;

    SrcAddr.u4_addr[0] =
        OSIX_HTONL (pMatrixSDNode->DsmonMatrixSDSourceAddress.u4_addr[0]);
    SrcAddr.u4_addr[1] =
        OSIX_HTONL (pMatrixSDNode->DsmonMatrixSDSourceAddress.u4_addr[1]);
    SrcAddr.u4_addr[2] =
        OSIX_HTONL (pMatrixSDNode->DsmonMatrixSDSourceAddress.u4_addr[2]);
    SrcAddr.u4_addr[3] =
        OSIX_HTONL (pMatrixSDNode->DsmonMatrixSDSourceAddress.u4_addr[3]);

    DstAddr.u4_addr[0] =
        OSIX_HTONL (pMatrixSDNode->DsmonMatrixSDDestAddress.u4_addr[0]);
    DstAddr.u4_addr[1] =
        OSIX_HTONL (pMatrixSDNode->DsmonMatrixSDDestAddress.u4_addr[1]);
    DstAddr.u4_addr[2] =
        OSIX_HTONL (pMatrixSDNode->DsmonMatrixSDDestAddress.u4_addr[2]);
    DstAddr.u4_addr[3] =
        OSIX_HTONL (pMatrixSDNode->DsmonMatrixSDDestAddress.u4_addr[3]);

    if (pMatrixSDNode->u4DsmonMatrixIpAddrLen == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (pNextDsmonMatrixSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr, DSMON_IPV6_MAX_LEN);
        MEMCPY (pNextDsmonMatrixDestAddress->pu1_OctetList,
                (UINT1 *) &DstAddr, DSMON_IPV4_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextDsmonMatrixSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr.u4_addr[3], DSMON_IPV4_MAX_LEN);
        MEMCPY (pNextDsmonMatrixDestAddress->pu1_OctetList,
                (UINT1 *) &DstAddr.u4_addr[3], DSMON_IPV4_MAX_LEN);
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDsmonMatrixSDPkts
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixSDPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixSDPkts (INT4 i4DsmonMatrixCtlIndex,
                         UINT4 u4DsmonMatrixTimeMark, INT4 i4DsmonAggGroupIndex,
                         INT4 i4DsmonMatrixNLIndex,
                         tSNMP_OCTET_STRING_TYPE * pDsmonMatrixSourceAddress,
                         tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                         INT4 i4DsmonMatrixALIndex,
                         UINT4 *pu4RetValDsmonMatrixSDPkts)
{

    tDsmonMatrixSD     *pMatrixSDNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDPkts \n");

    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixSDNode =
        DsmonMatrixGetDataEntry (DSMON_MATRIXSD_TABLE, i4DsmonMatrixCtlIndex,
                                 i4DsmonAggGroupIndex, i4DsmonMatrixNLIndex,
                                 i4DsmonMatrixALIndex, &SrcAddr, &DstAddr);

    if (pMatrixSDNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Source Address or \
                   Matrix Dest Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixSDNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixSDPkts =
        pMatrixSDNode->DsmonCurMatrixSDSample.u4DsmonMatrixSDPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixSDOvflPkts
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixSDOvflPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixSDOvflPkts (INT4 i4DsmonMatrixCtlIndex,
                             UINT4 u4DsmonMatrixTimeMark,
                             INT4 i4DsmonAggGroupIndex,
                             INT4 i4DsmonMatrixNLIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pDsmonMatrixSourceAddress,
                             tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                             INT4 i4DsmonMatrixALIndex,
                             UINT4 *pu4RetValDsmonMatrixSDOvflPkts)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonMatrixCtlIndex);
    UNUSED_PARAM (u4DsmonMatrixTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4DsmonMatrixNLIndex);
    UNUSED_PARAM (pDsmonMatrixSourceAddress);
    UNUSED_PARAM (pDsmonMatrixDestAddress);
    UNUSED_PARAM (i4DsmonMatrixALIndex);
    UNUSED_PARAM (pu4RetValDsmonMatrixSDOvflPkts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixSDHCPkts
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixSDHCPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixSDHCPkts (INT4 i4DsmonMatrixCtlIndex,
                           UINT4 u4DsmonMatrixTimeMark,
                           INT4 i4DsmonAggGroupIndex, INT4 i4DsmonMatrixNLIndex,
                           tSNMP_OCTET_STRING_TYPE * pDsmonMatrixSourceAddress,
                           tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                           INT4 i4DsmonMatrixALIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValDsmonMatrixSDHCPkts)
{

    tDsmonMatrixSD     *pMatrixSDNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDHCPkts \n");
    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixSDNode =
        DsmonMatrixGetDataEntry (DSMON_MATRIXSD_TABLE, i4DsmonMatrixCtlIndex,
                                 i4DsmonAggGroupIndex, i4DsmonMatrixNLIndex,
                                 i4DsmonMatrixALIndex, &SrcAddr, &DstAddr);

    if (pMatrixSDNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Source Address or \
                   Matrix Dest Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixSDNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonMatrixSDHCPkts->msn =
        pMatrixSDNode->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCPkts.u4HiWord;

    pu8RetValDsmonMatrixSDHCPkts->lsn =
        pMatrixSDNode->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCPkts.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixSDOctets
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixSDOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixSDOctets (INT4 i4DsmonMatrixCtlIndex,
                           UINT4 u4DsmonMatrixTimeMark,
                           INT4 i4DsmonAggGroupIndex, INT4 i4DsmonMatrixNLIndex,
                           tSNMP_OCTET_STRING_TYPE * pDsmonMatrixSourceAddress,
                           tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                           INT4 i4DsmonMatrixALIndex,
                           UINT4 *pu4RetValDsmonMatrixSDOctets)
{

    tDsmonMatrixSD     *pMatrixSDNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDOctets \n");
    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixSDNode =
        DsmonMatrixGetDataEntry (DSMON_MATRIXSD_TABLE, i4DsmonMatrixCtlIndex,
                                 i4DsmonAggGroupIndex, i4DsmonMatrixNLIndex,
                                 i4DsmonMatrixALIndex, &SrcAddr, &DstAddr);

    if (pMatrixSDNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Source Address or \
                   Matrix Dest Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixSDNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixSDOctets =
        pMatrixSDNode->DsmonCurMatrixSDSample.u4DsmonMatrixSDOctets;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixSDOvflOctets
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixSDOvflOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixSDOvflOctets (INT4 i4DsmonMatrixCtlIndex,
                               UINT4 u4DsmonMatrixTimeMark,
                               INT4 i4DsmonAggGroupIndex,
                               INT4 i4DsmonMatrixNLIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pDsmonMatrixSourceAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pDsmonMatrixDestAddress,
                               INT4 i4DsmonMatrixALIndex,
                               UINT4 *pu4RetValDsmonMatrixSDOvflOctets)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonMatrixCtlIndex);
    UNUSED_PARAM (u4DsmonMatrixTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4DsmonMatrixNLIndex);
    UNUSED_PARAM (pDsmonMatrixSourceAddress);
    UNUSED_PARAM (pDsmonMatrixDestAddress);
    UNUSED_PARAM (i4DsmonMatrixALIndex);
    UNUSED_PARAM (pu4RetValDsmonMatrixSDOvflOctets);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixSDHCOctets
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixSDHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixSDHCOctets (INT4 i4DsmonMatrixCtlIndex,
                             UINT4 u4DsmonMatrixTimeMark,
                             INT4 i4DsmonAggGroupIndex,
                             INT4 i4DsmonMatrixNLIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pDsmonMatrixSourceAddress,
                             tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                             INT4 i4DsmonMatrixALIndex,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValDsmonMatrixSDHCOctets)
{

    tDsmonMatrixSD     *pMatrixSDNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDHCOctets \n");

    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixSDNode =
        DsmonMatrixGetDataEntry (DSMON_MATRIXSD_TABLE, i4DsmonMatrixCtlIndex,
                                 i4DsmonAggGroupIndex, i4DsmonMatrixNLIndex,
                                 i4DsmonMatrixALIndex, &SrcAddr, &DstAddr);

    if (pMatrixSDNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Source Address or \
                   Matrix Dest Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixSDNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonMatrixSDHCOctets->msn =
        pMatrixSDNode->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCOctets.u4HiWord;

    pu8RetValDsmonMatrixSDHCOctets->lsn =
        pMatrixSDNode->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCOctets.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixSDCreateTime
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixSourceAddress
                DsmonMatrixDestAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixSDCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixSDCreateTime (INT4 i4DsmonMatrixCtlIndex,
                               UINT4 u4DsmonMatrixTimeMark,
                               INT4 i4DsmonAggGroupIndex,
                               INT4 i4DsmonMatrixNLIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pDsmonMatrixSourceAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pDsmonMatrixDestAddress,
                               INT4 i4DsmonMatrixALIndex,
                               UINT4 *pu4RetValDsmonMatrixSDCreateTime)
{

    tDsmonMatrixSD     *pMatrixSDNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDCreateTime \n");

    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixSDNode =
        DsmonMatrixGetDataEntry (DSMON_MATRIXSD_TABLE, i4DsmonMatrixCtlIndex,
                                 i4DsmonAggGroupIndex, i4DsmonMatrixNLIndex,
                                 i4DsmonMatrixALIndex, &SrcAddr, &DstAddr);

    if (pMatrixSDNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Source Address or \
                   Matrix Dest Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixSDNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixSDCreateTime =
        pMatrixSDNode->u4DsmonMatrixSDCreateTime;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonMatrixDSTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonMatrixDSTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonMatrixDSTable (INT4 i4DsmonMatrixCtlIndex,
                                            UINT4 u4DsmonMatrixTimeMark,
                                            INT4 i4DsmonAggGroupIndex,
                                            INT4 i4DsmonMatrixNLIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pDsmonMatrixDestAddress,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pDsmonMatrixSourceAddress,
                                            INT4 i4DsmonMatrixALIndex)
{

    tDsmonMatrixSD     *pMatrixDSNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    UNUSED_PARAM (u4DsmonMatrixTimeMark);

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhValidateIndexInstanceDsmonMatrixDSTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixDestAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixDSNode = DsmonMatrixGetDataEntry (DSMON_MATRIXDS_TABLE,
                                             i4DsmonMatrixCtlIndex,
                                             i4DsmonAggGroupIndex,
                                             i4DsmonMatrixNLIndex,
                                             i4DsmonMatrixALIndex,
                                             &SrcAddr, &DstAddr);

    if (pMatrixDSNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Dest Address or \
                   Matrix Source Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonMatrixDSTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonMatrixDSTable (INT4 *pi4DsmonMatrixCtlIndex,
                                    UINT4 *pu4DsmonMatrixTimeMark,
                                    INT4 *pi4DsmonAggGroupIndex,
                                    INT4 *pi4DsmonMatrixNLIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pDsmonMatrixDestAddress,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pDsmonMatrixSourceAddress,
                                    INT4 *pi4DsmonMatrixALIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetFirstIndexDsmonMatrixDSTable \n");

    return nmhGetNextIndexDsmonMatrixDSTable (DSMON_ZERO,
                                              pi4DsmonMatrixCtlIndex,
                                              DSMON_ZERO,
                                              pu4DsmonMatrixTimeMark,
                                              DSMON_ZERO, pi4DsmonAggGroupIndex,
                                              DSMON_ZERO, pi4DsmonMatrixNLIndex,
                                              DSMON_ZERO,
                                              pDsmonMatrixDestAddress,
                                              DSMON_ZERO,
                                              pDsmonMatrixSourceAddress,
                                              DSMON_ZERO,
                                              pi4DsmonMatrixALIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonMatrixDSTable
 Input       :  The Indices
                DsmonMatrixCtlIndex
                nextDsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                nextDsmonMatrixTimeMark
                DsmonAggGroupIndex
                nextDsmonAggGroupIndex
                DsmonMatrixNLIndex
                nextDsmonMatrixNLIndex
                DsmonMatrixDestAddress
                nextDsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                nextDsmonMatrixSourceAddress
                DsmonMatrixALIndex
                nextDsmonMatrixALIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonMatrixDSTable (INT4 i4DsmonMatrixCtlIndex,
                                   INT4 *pi4NextDsmonMatrixCtlIndex,
                                   UINT4 u4DsmonMatrixTimeMark,
                                   UINT4 *pu4NextDsmonMatrixTimeMark,
                                   INT4 i4DsmonAggGroupIndex,
                                   INT4 *pi4NextDsmonAggGroupIndex,
                                   INT4 i4DsmonMatrixNLIndex,
                                   INT4 *pi4NextDsmonMatrixNLIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pDsmonMatrixDestAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextDsmonMatrixDestAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pDsmonMatrixSourceAddress,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextDsmonMatrixSourceAddress,
                                   INT4 i4DsmonMatrixALIndex,
                                   INT4 *pi4NextDsmonMatrixALIndex)
{

    tDsmonMatrixSD     *pMatrixDSNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    UNUSED_PARAM (u4DsmonMatrixTimeMark);

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetNextIndexDsmonMatrixDSTable \n");

    if (pDsmonMatrixSourceAddress == NULL)
    {
        MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

        MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
        MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
        if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
        {
            MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                    DSMON_IPV6_MAX_LEN);
            MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                    DSMON_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&SrcAddr.u4_addr[3],
                    pDsmonMatrixSourceAddress->pu1_OctetList,
                    DSMON_IPV4_MAX_LEN);
            MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                    DSMON_IPV4_MAX_LEN);
        }
    }
    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixDSNode =
        DsmonMatrixGetNextDataIndex (DSMON_MATRIXDS_TABLE,
                                     i4DsmonMatrixCtlIndex,
                                     i4DsmonAggGroupIndex, i4DsmonMatrixNLIndex,
                                     i4DsmonMatrixALIndex, &SrcAddr, &DstAddr);

    if (pMatrixDSNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Dest Address or \
                   Matrix Source Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonMatrixCtlIndex = pMatrixDSNode->u4DsmonMatrixCtlIndex;

    *pu4NextDsmonMatrixTimeMark = DSMON_ZERO;

    *pi4NextDsmonAggGroupIndex = pMatrixDSNode->u4DsmonMatrixSDGroupIndex;

    *pi4NextDsmonMatrixNLIndex = pMatrixDSNode->u4DsmonMatrixSDNLIndex;

    *pi4NextDsmonMatrixALIndex = pMatrixDSNode->u4DsmonMatrixSDALIndex;

    pNextDsmonMatrixSourceAddress->i4_Length =
        pMatrixDSNode->u4DsmonMatrixIpAddrLen;
    pNextDsmonMatrixDestAddress->i4_Length =
        pMatrixDSNode->u4DsmonMatrixIpAddrLen;

    SrcAddr.u4_addr[0] =
        OSIX_HTONL (pMatrixDSNode->DsmonMatrixSDSourceAddress.u4_addr[0]);
    SrcAddr.u4_addr[1] =
        OSIX_HTONL (pMatrixDSNode->DsmonMatrixSDSourceAddress.u4_addr[1]);
    SrcAddr.u4_addr[2] =
        OSIX_HTONL (pMatrixDSNode->DsmonMatrixSDSourceAddress.u4_addr[2]);
    SrcAddr.u4_addr[3] =
        OSIX_HTONL (pMatrixDSNode->DsmonMatrixSDSourceAddress.u4_addr[3]);

    DstAddr.u4_addr[0] =
        OSIX_HTONL (pMatrixDSNode->DsmonMatrixSDDestAddress.u4_addr[0]);
    DstAddr.u4_addr[1] =
        OSIX_HTONL (pMatrixDSNode->DsmonMatrixSDDestAddress.u4_addr[1]);
    DstAddr.u4_addr[2] =
        OSIX_HTONL (pMatrixDSNode->DsmonMatrixSDDestAddress.u4_addr[2]);
    DstAddr.u4_addr[3] =
        OSIX_HTONL (pMatrixDSNode->DsmonMatrixSDDestAddress.u4_addr[3]);

    if (pMatrixDSNode->u4DsmonMatrixIpAddrLen == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (pNextDsmonMatrixSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr, DSMON_IPV6_MAX_LEN);
        MEMCPY (pNextDsmonMatrixDestAddress->pu1_OctetList,
                (UINT1 *) &DstAddr, DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextDsmonMatrixSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddr.u4_addr[3], DSMON_IPV4_MAX_LEN);
        MEMCPY (pNextDsmonMatrixDestAddress->pu1_OctetList,
                (UINT1 *) &DstAddr.u4_addr[3], DSMON_IPV4_MAX_LEN);
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDsmonMatrixSDPkts
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixDSPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixDSPkts (INT4 i4DsmonMatrixCtlIndex,
                         UINT4 u4DsmonMatrixTimeMark, INT4 i4DsmonAggGroupIndex,
                         INT4 i4DsmonMatrixNLIndex,
                         tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                         tSNMP_OCTET_STRING_TYPE * pDsmonMatrixSourceAddress,
                         INT4 i4DsmonMatrixALIndex,
                         UINT4 *pu4RetValDsmonMatrixDSPkts)
{

    tDsmonMatrixSD     *pMatrixDSNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixDSPkts \n");
    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixDSNode = DsmonMatrixGetDataEntry (DSMON_MATRIXDS_TABLE,
                                             i4DsmonMatrixCtlIndex,
                                             i4DsmonAggGroupIndex,
                                             i4DsmonMatrixNLIndex,
                                             i4DsmonMatrixALIndex,
                                             &SrcAddr, &DstAddr);

    if (pMatrixDSNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Dest Address or \
                   Matrix Source Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixDSNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixDSPkts =
        pMatrixDSNode->DsmonCurMatrixSDSample.u4DsmonMatrixSDPkts;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixDSOvflPkts
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixDSOvflPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixDSOvflPkts (INT4 i4DsmonMatrixCtlIndex,
                             UINT4 u4DsmonMatrixTimeMark,
                             INT4 i4DsmonAggGroupIndex,
                             INT4 i4DsmonMatrixNLIndex,
                             tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pDsmonMatrixSourceAddress,
                             INT4 i4DsmonMatrixALIndex,
                             UINT4 *pu4RetValDsmonMatrixDSOvflPkts)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonMatrixCtlIndex);
    UNUSED_PARAM (u4DsmonMatrixTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4DsmonMatrixNLIndex);
    UNUSED_PARAM (pDsmonMatrixDestAddress);
    UNUSED_PARAM (pDsmonMatrixSourceAddress);
    UNUSED_PARAM (i4DsmonMatrixALIndex);
    UNUSED_PARAM (pu4RetValDsmonMatrixDSOvflPkts);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixDSHCPkts
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixDSHCPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixDSHCPkts (INT4 i4DsmonMatrixCtlIndex,
                           UINT4 u4DsmonMatrixTimeMark,
                           INT4 i4DsmonAggGroupIndex, INT4 i4DsmonMatrixNLIndex,
                           tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                           tSNMP_OCTET_STRING_TYPE * pDsmonMatrixSourceAddress,
                           INT4 i4DsmonMatrixALIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValDsmonMatrixDSHCPkts)
{

    tDsmonMatrixSD     *pMatrixDSNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDHCPkts \n");

    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixDSNode = DsmonMatrixGetDataEntry (DSMON_MATRIXDS_TABLE,
                                             i4DsmonMatrixCtlIndex,
                                             i4DsmonAggGroupIndex,
                                             i4DsmonMatrixNLIndex,
                                             i4DsmonMatrixALIndex,
                                             &SrcAddr, &DstAddr);

    if (pMatrixDSNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Dest Address or \
                   Matrix Source Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixDSNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonMatrixDSHCPkts->msn =
        pMatrixDSNode->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCPkts.u4HiWord;

    pu8RetValDsmonMatrixDSHCPkts->lsn =
        pMatrixDSNode->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCPkts.u4LoWord;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixDSOctets
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixDSOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixDSOctets (INT4 i4DsmonMatrixCtlIndex,
                           UINT4 u4DsmonMatrixTimeMark,
                           INT4 i4DsmonAggGroupIndex, INT4 i4DsmonMatrixNLIndex,
                           tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                           tSNMP_OCTET_STRING_TYPE * pDsmonMatrixSourceAddress,
                           INT4 i4DsmonMatrixALIndex,
                           UINT4 *pu4RetValDsmonMatrixDSOctets)
{
    tDsmonMatrixSD     *pMatrixDSNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDOctets \n");
    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixDSNode = DsmonMatrixGetDataEntry (DSMON_MATRIXDS_TABLE,
                                             i4DsmonMatrixCtlIndex,
                                             i4DsmonAggGroupIndex,
                                             i4DsmonMatrixNLIndex,
                                             i4DsmonMatrixALIndex,
                                             &SrcAddr, &DstAddr);

    if (pMatrixDSNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Dest Address or \
                   Matrix Source Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixDSNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixDSOctets =
        pMatrixDSNode->DsmonCurMatrixSDSample.u4DsmonMatrixSDOctets;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixDSOvflOctets
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixDSOvflOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixDSOvflOctets (INT4 i4DsmonMatrixCtlIndex,
                               UINT4 u4DsmonMatrixTimeMark,
                               INT4 i4DsmonAggGroupIndex,
                               INT4 i4DsmonMatrixNLIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pDsmonMatrixDestAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pDsmonMatrixSourceAddress,
                               INT4 i4DsmonMatrixALIndex,
                               UINT4 *pu4RetValDsmonMatrixDSOvflOctets)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonMatrixCtlIndex);
    UNUSED_PARAM (u4DsmonMatrixTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4DsmonMatrixNLIndex);
    UNUSED_PARAM (pDsmonMatrixDestAddress);
    UNUSED_PARAM (pDsmonMatrixSourceAddress);
    UNUSED_PARAM (i4DsmonMatrixALIndex);
    UNUSED_PARAM (pu4RetValDsmonMatrixDSOvflOctets);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixDSHCOctets
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixDSHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixDSHCOctets (INT4 i4DsmonMatrixCtlIndex,
                             UINT4 u4DsmonMatrixTimeMark,
                             INT4 i4DsmonAggGroupIndex,
                             INT4 i4DsmonMatrixNLIndex,
                             tSNMP_OCTET_STRING_TYPE * pDsmonMatrixDestAddress,
                             tSNMP_OCTET_STRING_TYPE *
                             pDsmonMatrixSourceAddress,
                             INT4 i4DsmonMatrixALIndex,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValDsmonMatrixDSHCOctets)
{

    tDsmonMatrixSD     *pMatrixDSNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDHCOctets \n");
    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixDSNode = DsmonMatrixGetDataEntry (DSMON_MATRIXDS_TABLE,
                                             i4DsmonMatrixCtlIndex,
                                             i4DsmonAggGroupIndex,
                                             i4DsmonMatrixNLIndex,
                                             i4DsmonMatrixALIndex,
                                             &SrcAddr, &DstAddr);

    if (pMatrixDSNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Dest Address or \
                   Matrix Source Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixDSNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonMatrixDSHCOctets->msn =
        pMatrixDSNode->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCOctets.u4HiWord;

    pu8RetValDsmonMatrixDSHCOctets->lsn =
        pMatrixDSNode->DsmonCurMatrixSDSample.u8DsmonMatrixSDHCOctets.u4LoWord;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixDSCreateTime
 Input       :  The Indices
                DsmonMatrixCtlIndex
                DsmonMatrixTimeMark
                DsmonAggGroupIndex
                DsmonMatrixNLIndex
                DsmonMatrixDestAddress
                DsmonMatrixSourceAddress
                DsmonMatrixALIndex

                The Object
                retValDsmonMatrixDSCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixDSCreateTime (INT4 i4DsmonMatrixCtlIndex,
                               UINT4 u4DsmonMatrixTimeMark,
                               INT4 i4DsmonAggGroupIndex,
                               INT4 i4DsmonMatrixNLIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pDsmonMatrixDestAddress,
                               tSNMP_OCTET_STRING_TYPE *
                               pDsmonMatrixSourceAddress,
                               INT4 i4DsmonMatrixALIndex,
                               UINT4 *pu4RetValDsmonMatrixDSCreateTime)
{

    tDsmonMatrixSD     *pMatrixDSNode = NULL;
    tIpAddr             SrcAddr, DstAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixSDCreateTime \n");
    MEMSET (&SrcAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    MEMSET (&DstAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonMatrixSourceAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&SrcAddr, pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
        MEMCPY (&DstAddr, pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&SrcAddr.u4_addr[3], pDsmonMatrixSourceAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
        MEMCPY (&DstAddr.u4_addr[3], pDsmonMatrixDestAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    SrcAddr.u4_addr[0] = OSIX_NTOHL (SrcAddr.u4_addr[0]);
    SrcAddr.u4_addr[1] = OSIX_NTOHL (SrcAddr.u4_addr[1]);
    SrcAddr.u4_addr[2] = OSIX_NTOHL (SrcAddr.u4_addr[2]);
    SrcAddr.u4_addr[3] = OSIX_NTOHL (SrcAddr.u4_addr[3]);

    DstAddr.u4_addr[0] = OSIX_NTOHL (DstAddr.u4_addr[0]);
    DstAddr.u4_addr[1] = OSIX_NTOHL (DstAddr.u4_addr[1]);
    DstAddr.u4_addr[2] = OSIX_NTOHL (DstAddr.u4_addr[2]);
    DstAddr.u4_addr[3] = OSIX_NTOHL (DstAddr.u4_addr[3]);

    pMatrixDSNode = DsmonMatrixGetDataEntry (DSMON_MATRIXDS_TABLE,
                                             i4DsmonMatrixCtlIndex,
                                             i4DsmonAggGroupIndex,
                                             i4DsmonMatrixNLIndex,
                                             i4DsmonMatrixALIndex,
                                             &SrcAddr, &DstAddr);

    if (pMatrixDSNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Matrix Control Index or \
                   Agg Group Index or Matrix NL Index or Matrix Dest Address or \
                   Matrix Source Address or Matrix AL Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pMatrixDSNode->u4DsmonMatrixSDTimeMark < u4DsmonMatrixTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixDSCreateTime =
        pMatrixDSNode->u4DsmonMatrixSDCreateTime;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonMatrixTopNCtlTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonMatrixTopNCtlTable
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonMatrixTopNCtlTable (INT4 i4DsmonMatrixTopNCtlIndex)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhValidateIndexInstanceDsmonMatrixTopNCtlTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonMatrixTopNCtlTable
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonMatrixTopNCtlTable (INT4 *pi4DsmonMatrixTopNCtlIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetFirstIndexDsmonMatrixTopNCtlTable \n");

    return nmhGetNextIndexDsmonMatrixTopNCtlTable (DSMON_ZERO,
                                                   pi4DsmonMatrixTopNCtlIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonMatrixTopNCtlTable
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                nextDsmonMatrixTopNCtlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonMatrixTopNCtlTable (INT4 i4DsmonMatrixTopNCtlIndex,
                                        INT4 *pi4NextDsmonMatrixTopNCtlIndex)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetNextIndexDsmonMatrixTopNCtlTable \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetNextCtlIndex (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonMatrixTopNCtlIndex =
        (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlIndex);

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlMatrixIndex
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlMatrixIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlMatrixIndex (INT4 i4DsmonMatrixTopNCtlIndex,
                                     INT4
                                     *pi4RetValDsmonMatrixTopNCtlMatrixIndex)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlMatrixIndex \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNCtlMatrixIndex =
        (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlMatrixIndex);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlRateBase
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlRateBase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlRateBase (INT4 i4DsmonMatrixTopNCtlIndex,
                                  INT4 *pi4RetValDsmonMatrixTopNCtlRateBase)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlRateBase \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNCtlRateBase =
        (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRateBase);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlTimeRemaining
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlTimeRemaining (INT4 i4DsmonMatrixTopNCtlIndex,
                                       INT4
                                       *pi4RetValDsmonMatrixTopNCtlTimeRemaining)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlTimeRemaining \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
    {
        *pi4RetValDsmonMatrixTopNCtlTimeRemaining =
            (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlDuration);

        return SNMP_SUCCESS;

    }

    *pi4RetValDsmonMatrixTopNCtlTimeRemaining =
        (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlGeneratedRpts
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlGeneratedRpts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlGeneratedRpts (INT4 i4DsmonMatrixTopNCtlIndex,
                                       UINT4
                                       *pu4RetValDsmonMatrixTopNCtlGeneratedRpts)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlGeneratedRpts \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixTopNCtlGeneratedRpts =
        pMatrixTopNCtl->u4DsmonMatrixTopNCtlGenReports;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlDuration
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlDuration (INT4 i4DsmonMatrixTopNCtlIndex,
                                  INT4 *pi4RetValDsmonMatrixTopNCtlDuration)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlDuration \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNCtlDuration =
        (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlDuration);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlRequestedSize
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlRequestedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlRequestedSize (INT4 i4DsmonMatrixTopNCtlIndex,
                                       INT4
                                       *pi4RetValDsmonMatrixTopNCtlRequestedSize)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlRequestedSize \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNCtlRequestedSize =
        (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlReqSize);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlGrantedSize
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlGrantedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlGrantedSize (INT4 i4DsmonMatrixTopNCtlIndex,
                                     INT4
                                     *pi4RetValDsmonMatrixTopNCtlGrantedSize)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlGrantedSize \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNCtlGrantedSize =
        (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlStartTime
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlStartTime (INT4 i4DsmonMatrixTopNCtlIndex,
                                   UINT4 *pu4RetValDsmonMatrixTopNCtlStartTime)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlStartTime \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixTopNCtlStartTime =
        pMatrixTopNCtl->u4DsmonMatrixTopNCtlStartTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlOwner
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlOwner (INT4 i4DsmonMatrixTopNCtlIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValDsmonMatrixTopNCtlOwner)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlOwner \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDsmonMatrixTopNCtlOwner->pu1_OctetList,
            pMatrixTopNCtl->au1DsmonMatrixTopNCtlOwner, DSMON_MAX_OWNER_LEN);

    pRetValDsmonMatrixTopNCtlOwner->i4_Length =
        STRLEN (pMatrixTopNCtl->au1DsmonMatrixTopNCtlOwner);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNCtlStatus
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                retValDsmonMatrixTopNCtlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNCtlStatus (INT4 i4DsmonMatrixTopNCtlIndex,
                                INT4 *pi4RetValDsmonMatrixTopNCtlStatus)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNCtlStatus \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNCtlStatus =
        (INT4) (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus);

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDsmonMatrixTopNCtlMatrixIndex
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                setValDsmonMatrixTopNCtlMatrixIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixTopNCtlMatrixIndex (INT4 i4DsmonMatrixTopNCtlIndex,
                                     INT4 i4SetValDsmonMatrixTopNCtlMatrixIndex)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixTopNCtlMatrixIndex \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlMatrixIndex =
        (UINT4) (i4SetValDsmonMatrixTopNCtlMatrixIndex);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixTopNCtlRateBase
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                setValDsmonMatrixTopNCtlRateBase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixTopNCtlRateBase (INT4 i4DsmonMatrixTopNCtlIndex,
                                  INT4 i4SetValDsmonMatrixTopNCtlRateBase)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixTopNCtlRateBase \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlRateBase =
        (UINT4) (i4SetValDsmonMatrixTopNCtlRateBase);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                setValDsmonMatrixTopNCtlTimeRemaining
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixTopNCtlTimeRemaining (INT4 i4DsmonMatrixTopNCtlIndex,
                                       INT4
                                       i4SetValDsmonMatrixTopNCtlTimeRemaining)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixTopNCtlTimeRemaining \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem =
        (UINT4) (i4SetValDsmonMatrixTopNCtlTimeRemaining);

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlDuration =
        (UINT4) (i4SetValDsmonMatrixTopNCtlTimeRemaining);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixTopNCtlRequestedSize
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                setValDsmonMatrixTopNCtlRequestedSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixTopNCtlRequestedSize (INT4 i4DsmonMatrixTopNCtlIndex,
                                       INT4
                                       i4SetValDsmonMatrixTopNCtlRequestedSize)
{
    UINT4               u4Minimum = 0;

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixTopNCtlRequestedSize \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlReqSize =
        (UINT4) (i4SetValDsmonMatrixTopNCtlRequestedSize);

    u4Minimum =
        (i4SetValDsmonMatrixTopNCtlRequestedSize <
         DSMON_MAX_TOPN_ENTRY) ? i4SetValDsmonMatrixTopNCtlRequestedSize :
        DSMON_MAX_TOPN_ENTRY;

    pMatrixTopNCtl->u4DsmonMatrixTopNCtlGranSize = u4Minimum;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixTopNCtlOwner
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                setValDsmonMatrixTopNCtlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixTopNCtlOwner (INT4 i4DsmonMatrixTopNCtlIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValDsmonMatrixTopNCtlOwner)
{

    UINT4               u4Minimum = 0;

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixTopNCtlOwner \n");

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    u4Minimum =
        (pSetValDsmonMatrixTopNCtlOwner->i4_Length <
         DSMON_MAX_OWNER_LEN) ? pSetValDsmonMatrixTopNCtlOwner->
        i4_Length : DSMON_MAX_OWNER_LEN;

    MEMSET (pMatrixTopNCtl->au1DsmonMatrixTopNCtlOwner, DSMON_INIT_VAL,
            DSMON_MAX_OWNER_LEN);

    MEMCPY (pMatrixTopNCtl->au1DsmonMatrixTopNCtlOwner,
            pSetValDsmonMatrixTopNCtlOwner->pu1_OctetList, u4Minimum);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonMatrixTopNCtlStatus
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                setValDsmonMatrixTopNCtlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonMatrixTopNCtlStatus (INT4 i4DsmonMatrixTopNCtlIndex,
                                INT4 i4SetValDsmonMatrixTopNCtlStatus)
{

    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;
    tDsmonMatrixCtl    *pMatrixCtlNode = NULL;

    INT4                i4Result = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhSetDsmonMatrixTopNCtlStatus \n");

    switch (i4SetValDsmonMatrixTopNCtlStatus)
    {

        case CREATE_AND_WAIT:

            pMatrixTopNCtl =
                DsmonMatrixTopNAddCtlEntry (i4DsmonMatrixTopNCtlIndex);

            if (pMatrixTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                           " Memory Allocation / \
                 RBTree Add Failed - Matrix TopN Control Table \n");

                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:

            pMatrixTopNCtl =
                DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

            if (pMatrixTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Matrix TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            pMatrixCtlNode =
                DsmonMatrixGetCtlEntry (pMatrixTopNCtl->
                                        u4DsmonMatrixTopNCtlMatrixIndex);

            if (pMatrixCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Matrix Control Index \n");

                return SNMP_FAILURE;
            }

            if (pMatrixCtlNode->u4DsmonMatrixCtlRowStatus != ACTIVE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Matrix Control entry is not in active state \n");

                return SNMP_FAILURE;
            }

            pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus = ACTIVE;

            pMatrixTopNCtl->u4DsmonMatrixTopNCtlStartTime =
                (UINT4) OsixGetSysUpTime ();

            pMatrixTopNCtl->u4DsmonMatrixTopNCtlTimeRem =
                pMatrixTopNCtl->u4DsmonMatrixTopNCtlDuration;

            break;

        case NOT_IN_SERVICE:

            pMatrixTopNCtl =
                DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

            if (pMatrixTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Matrix TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            DsmonMatrixTopNDelTopNReport ((UINT4) i4DsmonMatrixTopNCtlIndex);

            pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus = NOT_IN_SERVICE;

            break;

        case DESTROY:

            pMatrixTopNCtl =
                DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

            if (pMatrixTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Matrix TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            i4Result = DsmonMatrixTopNDelCtlEntry (pMatrixTopNCtl);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Matrix TopN Control Index \
                 RBTreeRemove failed - Matrix TopN  Control Table\n");

                return SNMP_FAILURE;
            }

            break;
        default:
            break;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixTopNCtlMatrixIndex
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                testValDsmonMatrixTopNCtlMatrixIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixTopNCtlMatrixIndex (UINT4 *pu4ErrorCode,
                                        INT4 i4DsmonMatrixTopNCtlIndex,
                                        INT4
                                        i4TestValDsmonMatrixTopNCtlMatrixIndex)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixTopNCtlMatrixIndex \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }

    if ((i4DsmonMatrixTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceDsmonMatrixCtlTable
        (i4TestValDsmonMatrixTopNCtlMatrixIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:MatrixTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixTopNCtlRateBase
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                testValDsmonMatrixTopNCtlRateBase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixTopNCtlRateBase (UINT4 *pu4ErrorCode,
                                     INT4 i4DsmonMatrixTopNCtlIndex,
                                     INT4 i4TestValDsmonMatrixTopNCtlRateBase)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixTopNCtlRateBase \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }
    if ((i4DsmonMatrixTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValDsmonMatrixTopNCtlRateBase < matrixTopNPkts) ||
        (i4TestValDsmonMatrixTopNCtlRateBase > matrixTopNHCOctets))
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RateBase \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:MatrixTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                testValDsmonMatrixTopNCtlTimeRemaining
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixTopNCtlTimeRemaining (UINT4 *pu4ErrorCode,
                                          INT4 i4DsmonMatrixTopNCtlIndex,
                                          INT4
                                          i4TestValDsmonMatrixTopNCtlTimeRemaining)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixTopNCtlTimeRemaining \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }
    if ((i4DsmonMatrixTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (i4TestValDsmonMatrixTopNCtlTimeRemaining < DSMON_ZERO)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: TimeRem is less than Zero \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:MatrixTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixTopNCtlRequestedSize
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                testValDsmonMatrixTopNCtlRequestedSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixTopNCtlRequestedSize (UINT4 *pu4ErrorCode,
                                          INT4 i4DsmonMatrixTopNCtlIndex,
                                          INT4
                                          i4TestValDsmonMatrixTopNCtlRequestedSize)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixTopNCtlRequestedSize \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }
    if ((i4DsmonMatrixTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (i4TestValDsmonMatrixTopNCtlRequestedSize <= DSMON_ZERO)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid value \
        Requested size is less than equal to zero \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:MatrixTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixTopNCtlOwner
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                testValDsmonMatrixTopNCtlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixTopNCtlOwner (UINT4 *pu4ErrorCode,
                                  INT4 i4DsmonMatrixTopNCtlIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValDsmonMatrixTopNCtlOwner)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixTopNCtlOwner \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }
    if ((i4DsmonMatrixTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((pTestValDsmonMatrixTopNCtlOwner->i4_Length < DSMON_ZERO) ||
        (pTestValDsmonMatrixTopNCtlOwner->i4_Length > DSMON_MAX_OWNER_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid Owner length \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:MatrixTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pMatrixTopNCtl->u4DsmonMatrixTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonMatrixTopNCtlStatus
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex

                The Object
                testValDsmonMatrixTopNCtlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonMatrixTopNCtlStatus (UINT4 *pu4ErrorCode,
                                   INT4 i4DsmonMatrixTopNCtlIndex,
                                   INT4 i4TestValDsmonMatrixTopNCtlStatus)
{
    tDsmonMatrixTopNCtl *pMatrixTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhTestv2DsmonMatrixTopNCtlStatus \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");
        return SNMP_FAILURE;
    }
    if ((i4DsmonMatrixTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonMatrixTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MatrixTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pMatrixTopNCtl = DsmonMatrixTopNGetCtlEntry (i4DsmonMatrixTopNCtlIndex);

    if (pMatrixTopNCtl == NULL)
    {
        if (i4TestValDsmonMatrixTopNCtlStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValDsmonMatrixTopNCtlStatus == ACTIVE) ||
            (i4TestValDsmonMatrixTopNCtlStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonMatrixTopNCtlStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DsmonMatrixTopNCtlTable
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DsmonMatrixTopNCtlTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonMatrixTopNTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonMatrixTopNTable
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonMatrixTopNTable (INT4 i4DsmonMatrixTopNCtlIndex,
                                              INT4 i4DsmonMatrixTopNIndex)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhValidateIndexInstanceDsmonMatrixTopNTable \n");
    if (!(DSMON_IS_ENABLED ()))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonMatrixTopNTable
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonMatrixTopNTable (INT4 *pi4DsmonMatrixTopNCtlIndex,
                                      INT4 *pi4DsmonMatrixTopNIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetFirstIndexDsmonMatrixTopNTable \n");
    return nmhGetNextIndexDsmonMatrixTopNTable (DSMON_ZERO,
                                                pi4DsmonMatrixTopNCtlIndex,
                                                DSMON_ZERO,
                                                pi4DsmonMatrixTopNIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonMatrixTopNTable
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                nextDsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex
                nextDsmonMatrixTopNIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonMatrixTopNTable (INT4 i4DsmonMatrixTopNCtlIndex,
                                     INT4 *pi4NextDsmonMatrixTopNCtlIndex,
                                     INT4 i4DsmonMatrixTopNIndex,
                                     INT4 *pi4NextDsmonMatrixTopNIndex)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetNextIndexDsmonMatrixTopNTable \n");
    pMatrixTopN =
        DsmonMatrixTopNGetNextTopNIndex (i4DsmonMatrixTopNCtlIndex,
                                         i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   No valid next Index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonMatrixTopNCtlIndex = pMatrixTopN->u4DsmonMatrixTopNCtlIndex;

    *pi4NextDsmonMatrixTopNIndex = pMatrixTopN->u4DsmonMatrixTopNIndex;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNAggGroup
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNAggGroup
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNAggGroup (INT4 i4DsmonMatrixTopNCtlIndex,
                               INT4 i4DsmonMatrixTopNIndex,
                               INT4 *pi4RetValDsmonMatrixTopNAggGroup)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNAggGroup \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNAggGroup = pMatrixTopN->u4DsmonMatrixTopNAggGroup;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNNLIndex
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNNLIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNNLIndex (INT4 i4DsmonMatrixTopNCtlIndex,
                              INT4 i4DsmonMatrixTopNIndex,
                              INT4 *pi4RetValDsmonMatrixTopNNLIndex)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNNLIndex \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNNLIndex = pMatrixTopN->u4DsmonMatrixTopNNLIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNSourceAddress
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNSourceAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNSourceAddress (INT4 i4DsmonMatrixTopNCtlIndex,
                                    INT4 i4DsmonMatrixTopNIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValDsmonMatrixTopNSourceAddress)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;
    tIpAddr             SrcAddress;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNSourceAddress \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    pRetValDsmonMatrixTopNSourceAddress->i4_Length =
        pMatrixTopN->u4DsmonMatrixIpAddrLen;

    SrcAddress.u4_addr[0] =
        OSIX_NTOHL (pMatrixTopN->DsmonMatrixTopNSrcAddr.u4_addr[0]);
    SrcAddress.u4_addr[1] =
        OSIX_NTOHL (pMatrixTopN->DsmonMatrixTopNSrcAddr.u4_addr[1]);
    SrcAddress.u4_addr[2] =
        OSIX_NTOHL (pMatrixTopN->DsmonMatrixTopNSrcAddr.u4_addr[2]);
    SrcAddress.u4_addr[3] =
        OSIX_NTOHL (pMatrixTopN->DsmonMatrixTopNSrcAddr.u4_addr[3]);

    if (pMatrixTopN->u4DsmonMatrixIpAddrLen == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (pRetValDsmonMatrixTopNSourceAddress->pu1_OctetList,
                (UINT1 *) &SrcAddress, DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pRetValDsmonMatrixTopNSourceAddress->pu1_OctetList,
                &SrcAddress.u4_addr[3], DSMON_IPV4_MAX_LEN);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNDestAddress
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNDestAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNDestAddress (INT4 i4DsmonMatrixTopNCtlIndex,
                                  INT4 i4DsmonMatrixTopNIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValDsmonMatrixTopNDestAddress)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;
    tIpAddr             DestAddress;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNDestAddress \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    pRetValDsmonMatrixTopNDestAddress->i4_Length =
        pMatrixTopN->u4DsmonMatrixIpAddrLen;

    DestAddress.u4_addr[0] =
        OSIX_NTOHL (pMatrixTopN->DsmonMatrixTopNDestAddr.u4_addr[0]);
    DestAddress.u4_addr[1] =
        OSIX_NTOHL (pMatrixTopN->DsmonMatrixTopNDestAddr.u4_addr[1]);
    DestAddress.u4_addr[2] =
        OSIX_NTOHL (pMatrixTopN->DsmonMatrixTopNDestAddr.u4_addr[2]);
    DestAddress.u4_addr[3] =
        OSIX_NTOHL (pMatrixTopN->DsmonMatrixTopNDestAddr.u4_addr[3]);

    if (pMatrixTopN->u4DsmonMatrixIpAddrLen == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (pRetValDsmonMatrixTopNDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddress, DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pRetValDsmonMatrixTopNDestAddress->pu1_OctetList,
                (UINT1 *) &DestAddress.u4_addr[3], DSMON_IPV4_MAX_LEN);
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNALIndex
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNALIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNALIndex (INT4 i4DsmonMatrixTopNCtlIndex,
                              INT4 i4DsmonMatrixTopNIndex,
                              INT4 *pi4RetValDsmonMatrixTopNALIndex)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNALIndex \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonMatrixTopNALIndex = pMatrixTopN->u4DsmonMatrixTopNALIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNPktRate
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNPktRate (INT4 i4DsmonMatrixTopNCtlIndex,
                              INT4 i4DsmonMatrixTopNIndex,
                              UINT4 *pu4RetValDsmonMatrixTopNPktRate)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNPktRate \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixTopNPktRate = pMatrixTopN->u4DsmonMatrixTopNPktRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNPktRateOvfl
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNPktRateOvfl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNPktRateOvfl (INT4 i4DsmonMatrixTopNCtlIndex,
                                  INT4 i4DsmonMatrixTopNIndex,
                                  UINT4 *pu4RetValDsmonMatrixTopNPktRateOvfl)
{
    /* deprecated */

    UNUSED_PARAM (i4DsmonMatrixTopNCtlIndex);
    UNUSED_PARAM (i4DsmonMatrixTopNIndex);
    UNUSED_PARAM (*pu4RetValDsmonMatrixTopNPktRateOvfl);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNHCPktRate
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNHCPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNHCPktRate (INT4 i4DsmonMatrixTopNCtlIndex,
                                INT4 i4DsmonMatrixTopNIndex,
                                tSNMP_COUNTER64_TYPE *
                                pu8RetValDsmonMatrixTopNHCPktRate)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNHCPktRate \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonMatrixTopNHCPktRate->msn =
        pMatrixTopN->u8DsmonMatrixTopNHCPktRate.u4HiWord;

    pu8RetValDsmonMatrixTopNHCPktRate->lsn =
        pMatrixTopN->u8DsmonMatrixTopNHCPktRate.u4LoWord;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNRevPktRate
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNRevPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNRevPktRate (INT4 i4DsmonMatrixTopNCtlIndex,
                                 INT4 i4DsmonMatrixTopNIndex,
                                 UINT4 *pu4RetValDsmonMatrixTopNRevPktRate)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNRevPktRate \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixTopNRevPktRate =
        pMatrixTopN->u4DsmonMatrixTopNRevPktRate;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNRevPktRateOvfl
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNRevPktRateOvfl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNRevPktRateOvfl (INT4 i4DsmonMatrixTopNCtlIndex,
                                     INT4 i4DsmonMatrixTopNIndex,
                                     UINT4
                                     *pu4RetValDsmonMatrixTopNRevPktRateOvfl)
{
    /* deprecated */
    UNUSED_PARAM (i4DsmonMatrixTopNCtlIndex);
    UNUSED_PARAM (i4DsmonMatrixTopNIndex);
    UNUSED_PARAM (*pu4RetValDsmonMatrixTopNRevPktRateOvfl);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNHCRevPktRate
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNHCRevPktRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNHCRevPktRate (INT4 i4DsmonMatrixTopNCtlIndex,
                                   INT4 i4DsmonMatrixTopNIndex,
                                   tSNMP_COUNTER64_TYPE *
                                   pu8RetValDsmonMatrixTopNHCRevPktRate)
{
    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNHCRevPktRate \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonMatrixTopNHCRevPktRate->msn =
        pMatrixTopN->u8DsmonMatrixTopNHCRevPktRate.u4HiWord;

    pu8RetValDsmonMatrixTopNHCRevPktRate->lsn =
        pMatrixTopN->u8DsmonMatrixTopNHCRevPktRate.u4LoWord;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNOctetRate
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNOctetRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNOctetRate (INT4 i4DsmonMatrixTopNCtlIndex,
                                INT4 i4DsmonMatrixTopNIndex,
                                UINT4 *pu4RetValDsmonMatrixTopNOctetRate)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNOctetRate \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixTopNOctetRate =
        pMatrixTopN->u4DsmonMatrixTopNOctetRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNOctetRateOvfl
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNOctetRateOvfl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNOctetRateOvfl (INT4 i4DsmonMatrixTopNCtlIndex,
                                    INT4 i4DsmonMatrixTopNIndex,
                                    UINT4
                                    *pu4RetValDsmonMatrixTopNOctetRateOvfl)
{
    /* deprecated */

    UNUSED_PARAM (i4DsmonMatrixTopNCtlIndex);
    UNUSED_PARAM (i4DsmonMatrixTopNIndex);
    UNUSED_PARAM (*pu4RetValDsmonMatrixTopNOctetRateOvfl);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNHCOctetRate
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNHCOctetRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNHCOctetRate (INT4 i4DsmonMatrixTopNCtlIndex,
                                  INT4 i4DsmonMatrixTopNIndex,
                                  tSNMP_COUNTER64_TYPE *
                                  pu8RetValDsmonMatrixTopNHCOctetRate)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNHCOctetRate \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonMatrixTopNHCOctetRate->msn =
        pMatrixTopN->u8DsmonMatrixTopNHCOctetRate.u4HiWord;

    pu8RetValDsmonMatrixTopNHCOctetRate->lsn =
        pMatrixTopN->u8DsmonMatrixTopNHCOctetRate.u4LoWord;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNRevOctetRate
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNRevOctetRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNRevOctetRate (INT4 i4DsmonMatrixTopNCtlIndex,
                                   INT4 i4DsmonMatrixTopNIndex,
                                   UINT4 *pu4RetValDsmonMatrixTopNRevOctetRate)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNRevOctetRate \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonMatrixTopNRevOctetRate =
        pMatrixTopN->u4DsmonMatrixTopNRevOctetRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNRevOctetRateOvfl
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNRevOctetRateOvfl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNRevOctetRateOvfl (INT4 i4DsmonMatrixTopNCtlIndex,
                                       INT4 i4DsmonMatrixTopNIndex,
                                       UINT4
                                       *pu4RetValDsmonMatrixTopNRevOctetRateOvfl)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonMatrixTopNCtlIndex);
    UNUSED_PARAM (i4DsmonMatrixTopNIndex);
    UNUSED_PARAM (*pu4RetValDsmonMatrixTopNRevOctetRateOvfl);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonMatrixTopNHCRevOctetRate
 Input       :  The Indices
                DsmonMatrixTopNCtlIndex
                DsmonMatrixTopNIndex

                The Object
                retValDsmonMatrixTopNHCRevOctetRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonMatrixTopNHCRevOctetRate (INT4 i4DsmonMatrixTopNCtlIndex,
                                     INT4 i4DsmonMatrixTopNIndex,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValDsmonMatrixTopNHCRevOctetRate)
{

    tDsmonMatrixTopN   *pMatrixTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY \
              nmhGetDsmonMatrixTopNHCRevOctetRate \n");

    pMatrixTopN =
        DsmonMatrixTopNGetTopNEntry (i4DsmonMatrixTopNCtlIndex,
                                     i4DsmonMatrixTopNIndex);

    if (pMatrixTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Matrix TopN Control Index or Matrix TopN Index \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonMatrixTopNHCRevOctetRate->msn =
        pMatrixTopN->u8DsmonMatirxTopNHCRevOctetRate.u4HiWord;

    pu8RetValDsmonMatrixTopNHCRevOctetRate->lsn =
        pMatrixTopN->u8DsmonMatirxTopNHCRevOctetRate.u4LoWord;

    return SNMP_SUCCESS;

}
