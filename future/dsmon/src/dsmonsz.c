/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dsmonsz.c,v 1.3 2013/11/29 11:04:12 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _DSMONSZ_C
#include "dsmninc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
DsmonSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DSMON_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsDSMONSizingParams[i4SizingId].u4StructSize,
                              FsDSMONSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(DSMONMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            DsmonSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
DsmonSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsDSMONSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, DSMONMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
DsmonSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < DSMON_MAX_SIZING_ID; i4SizingId++)
    {
        if (DSMONMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (DSMONMemPoolIds[i4SizingId]);
            DSMONMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
