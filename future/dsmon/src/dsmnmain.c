/********************************************************************
 *                                                                  *
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *                                                                  *
 * $Id: dsmnmain.c,v 1.17 2013/12/18 12:50:13 siva Exp $           *
 *                                                                  *
 * Description:                                                     *
 *        1) Creates the timer lists and DSMON Task.                *
 *        2) On failure to create the timer lists, deletes the      *
 *           already created resources.                             *
 *        3) Probes and fetches counters periodically.              *
 *        4) At the configured intervals, snapshot of               *
 *           aperiodic tables will be taken for the periodic        *
 *           groups.                                                *
 *                                                                  *
 *******************************************************************/
#define _DSMNMAIN_C_
#include "dsmninc.h"

/************************************************************************
 * Function              : DsmonMainSetInterfaceOid                     *  
 * Input (s)             : None                                         *    
 * Output (s)            : None                                         *
 * Returns               : None                                         *
 * Global Variables Used : gDsmonGlobals.interfaceOid                   *
 * Side effects          : None                                         *
 * Action                : Sets the Interface Oid                       * 
 ************************************************************************/

PRIVATE INT4
DsmonMainSetInterfaceOid (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainSetInterfaceOid\r\n");

    gDsmonGlobals.interfaceOid.u4_Length = IFINDEX_OID_SIZE;
    gDsmonGlobals.interfaceOid.pu4_OidList = gIfEntryOIDTree;

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainSetInterfaceOid\r\n");

    return OSIX_SUCCESS;
}

/************************************************************************
 *                                                                      *
 * Function          : DsmonMainSetCapabilities                     *
 * Input (s)             : None                                         *
 * Output (s)            : None                                         *
 * Returns               : None                                         *
 * Global Variables Used : gDsmonGlobals.u2DsmonCapabilities            *
 * Side effects          : None                                         *
 * Action                : Sets the Dsmon Capabilities                  *
 ************************************************************************/
PRIVATE VOID
DsmonMainSetCapabilities (VOID)
{
    DSMON_CAPABILITIES = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainSetCapabilities\r\n");

    /* Aggregation Control Group Support */
    DSMON_CAPABILITIES |= DSMON_COUNTER_AGG_CTL;
    /* Statistica Group Support */
    DSMON_CAPABILITIES |= (DSMON_STATS | DSMON_STATS_HC);
    /* Pdist Group support */
    DSMON_CAPABILITIES |= (DSMON_PDIST | DSMON_PDIST_HC);
    /* Host Group Support */
    DSMON_CAPABILITIES |= (DSMON_HOST | DSMON_HOST_HC);
    /* Capabilities Support */
    DSMON_CAPABILITIES |= DSMON_CAPS;
    /* Matrix Support */
    DSMON_CAPABILITIES |= (DSMON_MATRIX | DSMON_MATRIX_HC);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainSetCapabilities\r\n");
}

/****************************************************************************
*                                                                           *
* Function     : DsmonMainMemInit                                           *
*                                                                           *
* Description  : Allocates all the memory that is required for DSMON.       *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if memory allocation successful              *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
DsmonMainMemInit (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainMemInit\r\n");

    if (DsmonSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        DSMON_TRC (OS_RESOURCE_TRC, "Memory Pool Creation Failed\r\n");
        return OSIX_FAILURE;
    }

    DSMON_PKTINFO_POOL = DSMONMemPoolIds[MAX_DSMON_PKTINFO_SIZING_ID];
    DSMON_AGGCTL_POOL = DSMONMemPoolIds[MAX_DSMON_AGGCTLS_SIZING_ID];
    DSMON_AGGCTL_GROUP_POOL =
        DSMONMemPoolIds[MAX_DSMON_AGGCTL_GROUPS_SIZING_ID];
    DSMON_STATSCTL_POOL = DSMONMemPoolIds[MAX_DSMON_STATSCTL_SIZING_ID];
    DSMON_STATS_POOL = DSMONMemPoolIds[MAX_DSMON_STATS_SIZING_ID];
    DSMON_PDISTCTL_POOL = DSMONMemPoolIds[MAX_DSMON_PDISTCTL_SIZING_ID];
    DSMON_PDIST_POOL = DSMONMemPoolIds[MAX_DSMON_PDIST_SIZING_ID];
    DSMON_PDIST_TOPNCTL_POOL =
        DSMONMemPoolIds[MAX_DSMON_PDIST_TOPNCTL_SIZING_ID];
    DSMON_PDIST_TOPN_POOL = DSMONMemPoolIds[MAX_DSMON_PDIST_TOPN_SIZING_ID];
    DSMON_HOSTCTL_POOL = DSMONMemPoolIds[MAX_DSMON_HOSTCTL_SIZING_ID];
    DSMON_HOST_POOL = DSMONMemPoolIds[MAX_DSMON_HOST_SIZING_ID];
    DSMON_HOST_TOPNCTL_POOL = DSMONMemPoolIds[MAX_DSMON_HOST_TOPNCTL_SIZING_ID];
    DSMON_HOST_TOPN_POOL = DSMONMemPoolIds[MAX_DSMON_HOST_TOPN_SIZING_ID];
    DSMON_MATRICCTL_POOL = DSMONMemPoolIds[MAX_DSMON_MATRICCTL_SIZING_ID];
    DSMON_MATRIXSD_POOL = DSMONMemPoolIds[MAX_DSMON_MATRIXSD_SIZING_ID];
    DSMON_MATRIX_TOPNCTL_POOL =
        DSMONMemPoolIds[MAX_DSMON_MATRIX_TOPNCTL_SIZING_ID];
    DSMON_MATRIX_TOPN_POOL = DSMONMemPoolIds[MAX_DSMON_MATRIX_TOPN_SIZING_ID];
    DSMON_HOST_SAMPLE_TOPN_POOL =
        DSMONMemPoolIds[MAX_DSMON_HOST_SAMPLE_TOPN_SIZING_ID];
    DSMON_MATRIX_SAMPLE_TOPN_POOL =
        DSMONMemPoolIds[MAX_DSMON_MATRIX_SAMPLE_TOPN_SIZING_ID];
    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainMemInit\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : DsmonMainRBTreeInit                                        *
*                                                                           *
* Description  : Creates all the Table RBTree that is required for DSMON.   *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
DsmonMainRBTreeInit (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainRBTreeInit\r\n");

    /* Creating RBTree for PktInfo Table */
    DSMON_PKTINFO_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonPktInfo, RbNode),
                              DsmonUtlRBCmpPktInfo);

    /* Creating RBTree for Aggregation Control Table */
    DSMON_AGGCTL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonAggCtl, RbNode),
                              DsmonUtlRBCmpAggCtl);

    /* Creating RBTree for Aggregation Group Table */
    DSMON_AGGCTL_GROUP_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonAggGroup, RbNode),
                              DsmonUtlRBCmpAggGroup);

    /* Creating RBTree for Stats Control Table */
    DSMON_STATSCTL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonStatsCtl, RbNode),
                              DsmonUtlRBCmpStatsCtl);

    /* Creating RBTree for Stats Table */
    DSMON_STATS_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonStats, RbNode),
                              DsmonUtlRBCmpStats);

    /* Creating RBTree for Pdist Control Table */
    DSMON_PDISTCTL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonPdistCtl, RbNode),
                              DsmonUtlRBCmpPdistCtl);

    /* Creating RBTree for Pdist Stats Table */
    DSMON_PDIST_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonPdistStats, RbNode),
                              DsmonUtlRBCmpPdistStats);

    /* Creating RBTree for Pdist TopN Control Table */
    DSMON_PDIST_TOPNCTL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonPdistTopNCtl, RbNode),
                              DsmonUtlRBCmpPdistTopNCtl);

    /* Creating RBTree for Pdist TopN Table */
    DSMON_PDIST_TOPN_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonPdistTopN, RbNode),
                              DsmonUtlRBCmpPdistTopN);

    /* Creating RBTree for Host Control Table */
    DSMON_HOSTCTL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonHostCtl, RbNode),
                              DsmonUtlRBCmpHostCtl);

    /* Creating RBTree for Host Stats Table */
    DSMON_HOST_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonHostStats, RbNode),
                              DsmonUtlRBCmpHostStats);

    /* Creating RBTree for Host TopN Control Table */
    DSMON_HOST_TOPNCTL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonHostTopNCtl, RbNode),
                              DsmonUtlRBCmpHostTopNCtl);

    /* Creating RBTree for Host TopN Table */
    DSMON_HOST_TOPN_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonHostTopN, RbNode),
                              DsmonUtlRBCmpHostTopN);

    /* Creating RBTree for Matrix Control Table */
    DSMON_MATRIXCTL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonMatrixCtl, RbNode),
                              DsmonUtlRBCmpMatrixCtl);

    /* Creating RBTree for MatrixSD Stats Table */
    DSMON_MATRIX_TABLE[DSMON_MATRIXSD_TABLE] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonMatrixSD, MatrixSDRbNode),
                              DsmonUtlRBCmpMatrixSD);

    /* Creating RBTree for MatrixDS Stats Table */
    DSMON_MATRIX_TABLE[DSMON_MATRIXDS_TABLE] =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonMatrixSD, MatrixDSRbNode),
                              DsmonUtlRBCmpMatrixDS);

    /* Creating RBTree for Matrix TopN Control Table */
    DSMON_MATRIX_TOPNCTL_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonMatrixTopNCtl, RbNode),
                              DsmonUtlRBCmpMatrixTopNCtl);

    /* Creating RBTree for Matrix TopN Table */
    DSMON_MATRIX_TOPN_TABLE =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tDsmonMatrixTopN, RbNode),
                              DsmonUtlRBCmpMatrixTopN);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainRBTreeInit\r\n");
    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : DsmonMainTaskInit                                          *
*                                                                           *
* Description  : Dsmon initialization routine.                              *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/
PRIVATE INT4
DsmonMainTaskInit (VOID)
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainTaskInit\r\n");

    gDsmonGlobals.pDsmonLastVisted = &gDsmonPktInfo;

    MEMSET (gDsmonGlobals.pDsmonLastVisted, DSMON_INIT_VAL,
            sizeof (tDsmonPktInfo));

    /* Create Dsmon Semaphore */
    if (OsixCreateSem (DSMON_SEM_NAME, DSMON_SEM_COUNT, OSIX_DEFAULT_SEM_MODE,
                       &(DSMON_SEM_ID)) != OSIX_SUCCESS)
    {
        DSMON_TRC1 (DSMON_CRITICAL_TRC,
                    "Semaphore Creation failure for %s \r\n", DSMON_SEM_NAME);
        return OSIX_FAILURE;
    }

    /* Initialize the InterfaceOid */
    DsmonMainSetInterfaceOid ();

    /* Initialize Dsmon Capabilities */
    DsmonMainSetCapabilities ();

    /* Initialize Aggregation Control Gloabls */
    gDsmonGlobals.u4DsmonMaxAggGroups = DSMON_MAX_AGG_GROUPS;

    gDsmonGlobals.u1DsmonAggCtlLock = DSMON_AGGLOCK_TRUE;

    /* Create buffer pools for data structures */
    if (DsmonMainMemInit () == OSIX_FAILURE)
    {
        DSMON_TRC (OS_RESOURCE_TRC,
                   "DsmonMainTaskInit: Memory Pool Creation Failed\r\n");
        return OSIX_FAILURE;
    }

    /* Create RB Trees */
    DsmonMainRBTreeInit ();

    /* Timer Initialization */
    if (DsmonTmrInit () == OSIX_FAILURE)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC,
                   "DsmonMainTaskInit: Timer Initialization failed \r\n");
        return OSIX_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainTaskInit\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMainMemDeInit                                         */
/*                                                                           */
/* Description  : Deletes all Memory pool.                                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
DsmonMainMemDeInit (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainMemDeInit\r\n");

    DsmonSizingMemDeleteMemPools ();

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainMemDeInit\r\n");
}

/****************************************************************************
*                                                                           *
* Function     : DsmonMainRBTreeDeInit                                      *
*                                                                           *
* Description  : Deletes all the RBTrees that are required for DSMON.       *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PRIVATE INT4
DsmonMainRBTreeDeInit (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainRBTreeDeInit\r\n");

    /* Delete Pktinfo Table */
    RBTreeDestroy (DSMON_PKTINFO_TABLE, DsmonUtlRBFreePktInfo, 0);

    /* Delete Aggregation Control Table */
    RBTreeDestroy (DSMON_AGGCTL_TABLE, DsmonUtlRBFreeAggCtl, 0);

    /* Delete Aggregation Group Table */
    RBTreeDestroy (DSMON_AGGCTL_GROUP_TABLE, DsmonUtlRBFreeAggGroup, 0);

    /* Delete Statistics Control Table */
    RBTreeDestroy (DSMON_STATSCTL_TABLE, DsmonUtlRBFreeStatsCtl, 0);

    /* Delete Statistics Table */
    RBTreeDestroy (DSMON_STATS_TABLE, DsmonUtlRBFreeStats, 0);

    /* Delete Pdist Control Table */
    RBTreeDestroy (DSMON_PDISTCTL_TABLE, DsmonUtlRBFreePdistCtl, 0);

    /* Delete Pdist Stats Table */
    RBTreeDestroy (DSMON_PDIST_TABLE, DsmonUtlRBFreePdistStats, 0);

    /* Delete Pdist TopN Control Table */
    RBTreeDestroy (DSMON_PDIST_TOPNCTL_TABLE, DsmonUtlRBFreePdistTopNCtl, 0);

    /* Delete Pdist TopN Table */
    RBTreeDestroy (DSMON_PDIST_TOPN_TABLE, DsmonUtlRBFreePdistTopN, 0);

    /* Delete Host Control Table */
    RBTreeDestroy (DSMON_HOSTCTL_TABLE, DsmonUtlRBFreeHostCtl, 0);

    /* Delete Host Stats Table */
    RBTreeDestroy (DSMON_HOST_TABLE, DsmonUtlRBFreeHostStats, 0);

    /* Delete Host TopN Control Table */
    RBTreeDestroy (DSMON_HOST_TOPNCTL_TABLE, DsmonUtlRBFreeHostTopNCtl, 0);

    /* Delete Host TopN Table */
    RBTreeDestroy (DSMON_HOST_TOPN_TABLE, DsmonUtlRBFreeHostTopN, 0);

    /* Delete Matrix Control Table */
    RBTreeDestroy (DSMON_MATRIXCTL_TABLE, DsmonUtlRBFreeMatrixCtl, 0);

    /* Delete MatrixSD Table */
    RBTreeDestroy (DSMON_MATRIX_TABLE[DSMON_MATRIXSD_TABLE],
                   DsmonUtlRBFreeMatrixSD, 0);

    /* Delete MatrixDS Table */
    RBTreeDestroy (DSMON_MATRIX_TABLE[DSMON_MATRIXDS_TABLE], NULL, 0);

    /* Delete Matrix TopN Control Table */
    RBTreeDestroy (DSMON_MATRIX_TOPNCTL_TABLE, DsmonUtlRBFreeMatrixTopNCtl, 0);

    /* Delete Matrix TopN Table */
    RBTreeDestroy (DSMON_MATRIX_TOPN_TABLE, DsmonUtlRBFreeMatrixTopN, 0);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainRBTreeDeInit\r\n");
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMainTaskDeInit                                        */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
DsmonMainTaskDeInit (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainTaskDeInit\r\n");

    /* Delete Semaphore */
    OsixDeleteSem (0, DSMON_SEM_NAME);

    /* Release All RBTrees */
    DsmonMainRBTreeDeInit ();

    /* Release All Memory Pools */
    DsmonMainMemDeInit ();

    /* Release Timer */
    if (gDsmonGlobals.dsmonTmrLst != 0)
    {
        DsmonTmrDeInit ();
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainTaskDeInit\r\n");
}

/****************************************************************************
*                                                                           *
* Function     : DsmonMainTaskMain                                          *
*                                                                           *
* Description  : Main function of Dsmon.                                    *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
DsmonMainTaskMain (INT1 *pTaskId)
{
    UINT4               u4Event = 0;

    UNUSED_PARAM (pTaskId);

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainTaskMain\r\n");

    if (DsmonMainTaskInit () == OSIX_FAILURE)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC,
                   " !!!!! DSMON TASK INIT FAILURE  !!!!! \r\n");
        DsmonMainTaskDeInit ();
        DSMON_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    DSMON_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef SNMP_2_WANTED
    /* Register the DSMON MIB with SNMP */
    RegisterSTDDSM ();
    RegisterFSDSMO ();
#endif
    while (1)
    {

        OsixReceiveEvent (DSMON_ONE_SECOND_TIMER_EVENT,
                          OSIX_WAIT, (UINT4) 0, (UINT4 *) &(u4Event));

        /* Mutual exclusion flag ON */
        DsmonMainLock ();

        DSMON_TRC1 (CONTROL_PLANE_TRC, "Rx Event %d\r\n", u4Event);

        if (u4Event & DSMON_ONE_SECOND_TIMER_EVENT)
        {
            DSMON_TRC (CONTROL_PLANE_TRC, "DSMON_TIMER_EVENT\r\n");
            if (DsmonTmrHandleExpiry () == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_CRITICAL_TRC,
                           "DsmonMainTaskMain: Timer Expiry Handler Failed\r\n");
                DsmonMainTaskDeInit ();
                return;
            }
        }

        /* Mutual exclusion flag OFF */
        DsmonMainUnLock ();

    }                            /* End of while loop */

}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMainLock                                              */
/*                                                                           */
/* Description  : Lock the Dsmon procedure                                   */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonMainLock (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainLock\r\n");

    if (OsixTakeSem (SELF, DSMON_SEM_NAME, OSIX_WAIT, 0) != OSIX_SUCCESS)
    {
        DSMON_TRC1 (OS_RESOURCE_TRC | DSMON_CRITICAL_TRC,
                    "TakeSem failure for %s \n", DSMON_SEM_NAME);
        return SNMP_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainLock\r\n");
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonMainUnLock                                            */
/*                                                                           */
/* Description  : UnLock the Dsmon procedure                                 */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonMainUnLock (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainUnLock\r\n");

    if (OsixGiveSem (SELF, DSMON_SEM_NAME) != OSIX_SUCCESS)
    {
        DSMON_TRC1 (OS_RESOURCE_TRC | DSMON_CRITICAL_TRC,
                    "GiveSem failure for %s \n", DSMON_SEM_NAME);
        return SNMP_FAILURE;
    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainUnLock\r\n");
    return SNMP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : DsmonMainProcessPktInfo                           */
/*                                    */
/*  Description     : The function process the incoming packet info     */
/*              which is invoked by RMONv2 module            */
/*                                    */
/*  Input(s)        : PktHdr, u4ProtoNLIndex, u4ProtoALIndex            */
/*                                    */
/*  Output(s)       : None.                                             */
/*                                    */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
DsmonMainProcessPktInfo (tPktHdrInfo * pPktHdrInfo, UINT4 u4ProtoNLIndex,
                         UINT4 u4ProtoTLIndex, UINT4 u4ProtoALIndex)
{
    tDsmonPktInfo       PktInfo, *pNewPktInfo = NULL, *pTravPktInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    BOOL1               b1IsEntryAdded = OSIX_FALSE;
    tDsmonHostStats    *pInHost = NULL;
    tDsmonStats        *pInStats = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainProcessPktInfo\r\n");

    MEMSET (&PktInfo, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));

    DsmonMainLock ();
    MEMCPY (&PktInfo.PktHdr, &pPktHdrInfo->PktHdr, sizeof (tPktHeader));
    PktInfo.u4ProtoNLIndex = u4ProtoNLIndex;
    PktInfo.u4ProtoTLIndex = u4ProtoTLIndex;
    PktInfo.u4ProtoALIndex = u4ProtoALIndex;

    /* Process Incoming PktInfo */
    pNewPktInfo = DsmonPktInfoProcessIncomingPkt (&PktInfo);

    if (pNewPktInfo != NULL)
    {
        /* Process Matrix Table */
        if (u4ProtoALIndex != 0)
        {
            if ((i4RetVal =
                 DsmonMatrixProcessPktInfo (pNewPktInfo,
                                            u4ProtoALIndex,
                                            pPktHdrInfo->u4PktSize)) ==
                OSIX_SUCCESS)
            {
                b1IsEntryAdded = OSIX_TRUE;
            }
        }
        if (u4ProtoTLIndex != 0)
        {
            if ((i4RetVal =
                 DsmonMatrixProcessPktInfo (pNewPktInfo,
                                            u4ProtoTLIndex,
                                            pPktHdrInfo->u4PktSize)) ==
                OSIX_SUCCESS)
            {
                b1IsEntryAdded = OSIX_TRUE;
            }

        }

        /* Process Host Table */
        if ((i4RetVal =
             DsmonHostProcessPktInfo (pNewPktInfo,
                                      pPktHdrInfo->u4PktSize)) == OSIX_SUCCESS)
        {
            b1IsEntryAdded = OSIX_TRUE;
        }

        /* Process Pdist Table */
        if ((i4RetVal =
             DsmonPdistProcessPktInfo (pNewPktInfo,
                                       pPktHdrInfo->u4PktSize)) == OSIX_SUCCESS)
        {
            b1IsEntryAdded = OSIX_TRUE;
        }

        /* Process Stats Table */
        if ((i4RetVal =
             DsmonStatsProcessPktInfo (pNewPktInfo,
                                       pPktHdrInfo->u4PktSize)) == OSIX_SUCCESS)
        {
            b1IsEntryAdded = OSIX_TRUE;
        }
        MEMSET (&PktInfo, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));

        pTravPktInfo = DsmonPktInfoGetNextIndex (&PktInfo.PktHdr);
        while (pTravPktInfo != NULL)
        {
            if ((MEMCMP
                 (&pTravPktInfo->PktHdr.SrcIp, &pPktHdrInfo->PktHdr.DstIp,
                  sizeof (tIpAddr)) == 0)
                &&
                (MEMCMP
                 (&pTravPktInfo->PktHdr.DstIp, &pPktHdrInfo->PktHdr.SrcIp,
                  sizeof (tIpAddr)) == 0)
                && (pTravPktInfo->PktHdr.u2SrcPort == PktInfo.PktHdr.u2DstPort)
                && (pTravPktInfo->PktHdr.u2DstPort == PktInfo.PktHdr.u2SrcPort))
            {
                pNewPktInfo->pInHost = pTravPktInfo->pOutHost;
                pNewPktInfo->pInStats = pTravPktInfo->pOutStats;
                pInHost = pNewPktInfo->pInHost;
                while (pInHost != NULL)
                {
                    pInHost->DsmonCurSample.u4DsmonHostInPkts++;
                    pInHost->DsmonCurSample.u4DsmonHostInOctets +=
                        pPktHdrInfo->u4PktSize;
                    UINT8_ADD (pInHost->DsmonCurSample.u8DsmonHostInHCPkts, 1);
                    UINT8_ADD (pInHost->DsmonCurSample.u8DsmonHostInHCOctets,
                               pPktHdrInfo->u4PktSize);
                    pInHost->u4DsmonHostTimeMark = OsixGetSysUpTime ();
                    pInHost = pInHost->pNextHost;
                }
                pInStats = pNewPktInfo->pInStats;
                while (pInStats != NULL)
                {
                    pInStats->u4DsmonStatsInPkts++;
                    pInStats->u4DsmonStatsInOctets += pPktHdrInfo->u4PktSize;
                    UINT8_ADD (pInStats->u8DsmonStatsInHCPkts, 1);
                    UINT8_ADD (pInStats->u8DsmonStatsInHCOctets,
                               pPktHdrInfo->u4PktSize);
                    pInStats = pInStats->pNextStats;
                }
                /* If the new packet has created data entries,
                 * then make the above links for the existing node */
                if (b1IsEntryAdded == OSIX_TRUE)
                {
                    pTravPktInfo->pInHost = pNewPktInfo->pOutHost;
                    pTravPktInfo->pInStats = pNewPktInfo->pOutStats;
                }
                b1IsEntryAdded = OSIX_TRUE;
                if (pTravPktInfo->u1IsL3Vlan == OSIX_TRUE)
                {
                    pNewPktInfo->u1IsL3Vlan = OSIX_TRUE;
                }
                break;
            }
            pTravPktInfo = DsmonPktInfoGetNextIndex (&pTravPktInfo->PktHdr);
        }
        if (b1IsEntryAdded == OSIX_FALSE)
        {
            /* Still entry not found in Pktinfo tree
             * then remove the newly allocated entry */
            DsmonPktInfoDelEntry (pNewPktInfo);
        }
        else
        {
            if (pNewPktInfo->u1IsL3Vlan == OSIX_FALSE)
            {
                pNewPktInfo->PktHdr.u2VlanId = 0;
            }
#ifdef NPAPI_WANTED
            DsmonFsDSMONAddFlowStats (pNewPktInfo->PktHdr);
#endif
            pNewPktInfo->u1IsNPFlowAdded = OSIX_TRUE;
        }
    }
    DsmonMainUnLock ();
    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainProcessPktInfo\r\n");
}

/************************************************************************/
/*  Function Name   : DsmonMainUpdateProtoChgs                             */
/*                                    */
/*  Description     : If RMONv2 removes a protocol entry from its       */
/*              ProtocolDir Table, this function will be invoked  */
/*              by RMONv2 to reflect the changes in DSMON        */
/*                                    */
/*  Input(s)        : u4DsmonProtDirLocalIndex                      */
/*                                    */
/*  Output(s)       : None.                                             */
/*                                    */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
DsmonMainUpdateProtoChgs (UINT4 u4DsmonProtDirLocalIndex)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainUpdateProtoChgs\r\n");

    /* Update Pktinfo Table */
    DsmonPktInfoUpdateProtoChgs (u4DsmonProtDirLocalIndex);

    /* Update Pdist Stats Table */
    DsmonPdistUpdateProtoChgs (u4DsmonProtDirLocalIndex);

    /* Update Host Stats Table */
    DsmonHostUpdateProtoChgs (u4DsmonProtDirLocalIndex);

    /* Update Matrix Stats Table */
    DsmonMatrixUpdateProtoChgs (u4DsmonProtDirLocalIndex);

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainUpdateProtoChgs\r\n");
}

/************************************************************************/
/*  Function Name   : DsmonMainUpdateTopNTables                            */
/*                                    */
/*  Description     : For every one sec timer event, Periodic module    */
/*              update TopN tables if valid topn control entries  */
/*              were present                    */
/*                                    */
/*  Input(s)        :                                       */
/*                                    */
/*  Output(s)       : None.                                             */
/*                                    */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
DsmonMainUpdateTopNTables (VOID)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonMainUpdateTopNTables\r\n");

    /* Update Pdist TopN Table */
    DsmonPdistTopNUpdateTable ();

    /* Update Host TopN Table */
    DsmonHostTopNUpdateTable ();

    /* Update Matrix TopN Table */
    DsmonMatrixTopNUpdateTable ();

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonMainUpdateTopNTables\r\n");
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  dsmnmain.c                     */
/*-----------------------------------------------------------------------*/
