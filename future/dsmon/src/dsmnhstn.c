/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved         * 
 *
   $Id: dsmnhstn.c,v 1.9 2014/01/25 13:55:19 siva Exp $ 
                                                                 *
 * Description: This file contains the functional routine for DSMON *
 *              Host TopN module                                *
 *                                                                  *
 *******************************************************************/
#include "dsmninc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNUpdateAggCtlChgs                              */
/*                                                                           */
/* Description  : If u1DsmonAggControlStatus is FALSE, sets all control      */
/*                tables entries into NOTREADY and cleanup all TopN report.  */
/*                If u1DsmonAggControlStatus is TRUE, update control tables  */
/*                and resume TopN report process                             */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostTopNUpdateAggCtlChgs (UINT1 u1DsmonAggControlStatus)
{
    tDsmonHostCtl      *pHostCtl = NULL;
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;
    UINT4               u4CtlIndex = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNUpdateAggCtlChgs\r\n");

    switch (u1DsmonAggControlStatus)
    {
        case DSMON_AGGLOCK_FALSE:
            /* Get First HostTopNCtl Entry */
            pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (u4CtlIndex);
            while (pHostTopNCtl != NULL)
            {
                u4CtlIndex = pHostTopNCtl->u4DsmonHostTopNCtlIndex;
                if (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus == ACTIVE)
                {
                    pHostTopNCtl->u4DsmonHostTopNCtlRowStatus = NOT_READY;
                }

                /* Get Next HostTopNCtl Entry */
                pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (u4CtlIndex);
            }

            /* Clear TopN Report Table */
            RBTreeDrain (DSMON_HOST_TOPN_TABLE,
                         DsmonUtlRBFreeHostTopN, DSMON_ZERO);
            break;
        case DSMON_AGGLOCK_TRUE:
            /* Get First HostTopNCtl Entry */
            pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (u4CtlIndex);
            while (pHostTopNCtl != NULL)
            {
                u4CtlIndex = pHostTopNCtl->u4DsmonHostTopNCtlIndex;

                pHostCtl =
                    DsmonHostGetNextCtlIndex (pHostTopNCtl->
                                              u4DsmonHostTopNCtlHostIndex);

                if ((pHostCtl != NULL) &&
                    (pHostCtl->u4DsmonHostCtlRowStatus == ACTIVE))
                {
                    /* Set back RowStatus of HostTopNCtl as ACTIVE */
                    pHostTopNCtl->u4DsmonHostTopNCtlRowStatus = ACTIVE;
                }
                pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (u4CtlIndex);
            }
            break;
        default:
            break;
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonHostTopNUpdateAggCtlChgs\r\n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNNotifyHostCtlChgs                             */
/*                                                                           */
/* Description  : Notifies RowStatus changes to TopN Table                   */
/*                                                                           */
/* Input        : u4DsmonHostCtlIndex, u4DsmonHostCtlStatus                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostTopNNotifyHostCtlChgs (UINT4 u4DsmonHostCtlIndex,
                                UINT4 u4DsmonHostCtlStatus)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;
    tDsmonHostTopN     *pHostTopN = NULL;
    UINT4               u4DsmonHostTopNIndex = DSMON_ZERO, u4CtlIndex =
        DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNNotifyHostCtlChgs\r\n");

    pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (u4CtlIndex);

    while (pHostTopNCtl != NULL)
    {
        u4CtlIndex = pHostTopNCtl->u4DsmonHostTopNCtlIndex;

        if (pHostTopNCtl->u4DsmonHostTopNCtlHostIndex == u4DsmonHostCtlIndex)
        {
            if (u4DsmonHostCtlStatus != ACTIVE)
            {
                /* Free TopN Report */
                pHostTopN =
                    DsmonHostTopNGetNextTopNIndex (pHostTopNCtl->
                                                   u4DsmonHostTopNCtlIndex,
                                                   u4DsmonHostTopNIndex);

                while (pHostTopN != NULL)
                {
                    if (pHostTopNCtl->u4DsmonHostTopNCtlHostIndex !=
                        pHostTopN->u4DsmonHostTopNCtlIndex)
                    {
                        break;
                    }
                    u4DsmonHostTopNIndex = pHostTopN->u4DsmonHostTopNIndex;

                    DsmonHostTopNDelTopNEntry (pHostTopN);

                    pHostTopN =
                        DsmonHostTopNGetNextTopNIndex (pHostTopNCtl->
                                                       u4DsmonHostTopNCtlIndex,
                                                       u4DsmonHostTopNIndex);
                }
                pHostTopNCtl->u4DsmonHostTopNCtlTimeRem =
                    pHostTopNCtl->u4DsmonHostTopNCtlDuration;
                pHostTopNCtl->u4DsmonHostTopNCtlRowStatus = NOT_READY;
            }
            else if ((u4DsmonHostCtlStatus == ACTIVE) &&
                     (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus == NOT_READY))
            {
                pHostTopNCtl->u4DsmonHostTopNCtlRowStatus = ACTIVE;
                pHostTopNCtl->u4DsmonHostTopNCtlStartTime =
                    (UINT4) OsixGetSysUpTime ();
            }
        }
        pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (u4CtlIndex);
    }

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNGetNextCtlIndex                               */
/*                                                                           */
/* Description  : Get First / Next Host TopN Ctl Index                       */
/*                                                                           */
/* Input        : u4DsmonHostTopNCtlIndex                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostCtl / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostTopNCtl *
DsmonHostTopNGetNextCtlIndex (UINT4 u4DsmonHostTopNCtlIndex)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;
    tDsmonHostTopNCtl   HostTopNCtl;

    MEMSET (&HostTopNCtl, DSMON_INIT_VAL, sizeof (tDsmonHostTopNCtl));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNGetNextCtlIndex\r\n");

    HostTopNCtl.u4DsmonHostTopNCtlIndex = u4DsmonHostTopNCtlIndex;
    pHostTopNCtl = (tDsmonHostTopNCtl *) RBTreeGetNext
        (DSMON_HOST_TOPNCTL_TABLE, (tRBElem *) & HostTopNCtl, NULL);
    return pHostTopNCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNAddCtlEntry                                   */
/*                                                                           */
/* Description  : Allocates memory and Adds Host TopN Control Entry into     */
/*                dsmonHostTopNCtlRBTree                                     */
/*                                                                           */
/* Input        : u4DsmonHostTopNControlIndex                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostCtl / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostTopNCtl *
DsmonHostTopNAddCtlEntry (UINT4 u4DsmonHostTopNControlIndex)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNAddCtlEntry\r\n");

    if ((pHostTopNCtl = (tDsmonHostTopNCtl *)
         (MemAllocMemBlk (DSMON_HOST_TOPNCTL_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Host. TopN Ctl.\r\n");
        return NULL;
    }

    MEMSET (pHostTopNCtl, DSMON_INIT_VAL, sizeof (tDsmonHostTopNCtl));

    pHostTopNCtl->u4DsmonHostTopNCtlIndex = u4DsmonHostTopNControlIndex;

    pHostTopNCtl->u4DsmonHostTopNCtlTimeRem = DSMON_TIMEREM_DEFVAL;

    pHostTopNCtl->u4DsmonHostTopNCtlGenReports = DSMON_ZERO;

    pHostTopNCtl->u4DsmonHostTopNCtlDuration = DSMON_TIMEREM_DEFVAL;

    pHostTopNCtl->u4DsmonHostTopNCtlReqSize = DSMON_REQSIZE_DEFVAL;

    pHostTopNCtl->u4DsmonHostTopNCtlGranSize = DSMON_MAX_TOPN_ENTRY;

    pHostTopNCtl->u4DsmonHostTopNCtlStartTime = DSMON_ZERO;

    pHostTopNCtl->u4DsmonHostTopNCtlRowStatus = NOT_READY;

    if (RBTreeAdd (DSMON_HOST_TOPNCTL_TABLE, (tRBElem *) pHostTopNCtl)
        == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonHostTopNAddCtlEntry TopN Ctl Entry Added\r\n");
        return pHostTopNCtl;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonHostTopNAddCtlEntry TopN Ctl Addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_HOST_TOPNCTL_POOL, (UINT1 *) pHostTopNCtl);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNDelCtlEntry                                   */
/*                                                                           */
/* Description  : Releases memory and Removes Host TopN Control Entry from   */
/*                dsmonHostTopNCtlRBTree                                     */
/*                                                                           */
/* Input        : pHostTopNCtl                                               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonHostTopNDelCtlEntry (tDsmonHostTopNCtl * pHostTopNCtl)
{
    tDsmonHostTopN     *pHostTopN = NULL;
    UINT4               u4DsmonHostTopNIndex = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNDelCtlEntry \r\n");

    pHostTopN = (tDsmonHostTopN *) DsmonHostTopNGetNextTopNIndex
        (pHostTopNCtl->u4DsmonHostTopNCtlIndex, u4DsmonHostTopNIndex);

    while (pHostTopN != NULL)
    {
        if (pHostTopN->u4DsmonHostTopNCtlIndex !=
            pHostTopNCtl->u4DsmonHostTopNCtlIndex)
        {
            break;
        }
        u4DsmonHostTopNIndex = pHostTopN->u4DsmonHostTopNIndex;

        DsmonHostTopNDelTopNEntry (pHostTopN);

        pHostTopN = (tDsmonHostTopN *) DsmonHostTopNGetNextTopNIndex
            (pHostTopNCtl->u4DsmonHostTopNCtlIndex, u4DsmonHostTopNIndex);
    }

    if (RBTreeRemove (DSMON_HOST_TOPNCTL_TABLE, pHostTopNCtl) == RB_FAILURE)
    {
        DSMON_TRC (DSMON_FN_ENTRY,
                   "DsmonHostTopNDelCtlEntry Entry Removal failed\r\n");
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (DSMON_HOST_TOPNCTL_POOL, (UINT1 *) pHostTopNCtl);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNGetCtlEntry                                   */
/*                                                                           */
/* Description  : Get Host TopN Control Entry for the given control index    */
/*                                                                           */
/* Input        : u4DsmonHostTopNControlIndex                                */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostTopNCtl / NULL Pointer                           */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostTopNCtl *
DsmonHostTopNGetCtlEntry (UINT4 u4DsmonHostTopNControlIndex)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;
    tDsmonHostTopNCtl   HostTopNCtl;

    MEMSET (&HostTopNCtl, DSMON_INIT_VAL, sizeof (tDsmonHostTopNCtl));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNGetCtlEntry \r\n");

    HostTopNCtl.u4DsmonHostTopNCtlIndex = u4DsmonHostTopNControlIndex;

    pHostTopNCtl = (tDsmonHostTopNCtl *) RBTreeGet (DSMON_HOST_TOPNCTL_TABLE,
                                                    (tRBElem *) & HostTopNCtl);
    return pHostTopNCtl;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNGetNextTopNIndex                              */
/*                                                                           */
/* Description  : Get First / Next HostTopN Index                            */
/*                                                                           */
/* Input        : u4DsmonHostTopNCtlIndex                                    */
/*                u4DsmonHostTopNIndex                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostTopN / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostTopN *
DsmonHostTopNGetNextTopNIndex (UINT4 u4DsmonHostTopNCtlIndex,
                               UINT4 u4DsmonHostTopNIndex)
{
    tDsmonHostTopN     *pHostTopN = NULL;
    tDsmonHostTopN      HostTopN;

    MEMSET (&HostTopN, DSMON_INIT_VAL, sizeof (tDsmonHostTopN));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNGetNextTopNIndex \r\n");

    if ((u4DsmonHostTopNCtlIndex != DSMON_ZERO) &&
        (u4DsmonHostTopNIndex == DSMON_ZERO))
    {
        pHostTopN = (tDsmonHostTopN *) RBTreeGetFirst (DSMON_HOST_TOPN_TABLE);

        while (pHostTopN != NULL)
        {
            if (pHostTopN->u4DsmonHostTopNCtlIndex == u4DsmonHostTopNCtlIndex)
            {
                break;
            }
            MEMCPY (&HostTopN, pHostTopN, sizeof (tDsmonHostTopN));

            pHostTopN = (tDsmonHostTopN *) RBTreeGetNext (DSMON_HOST_TOPN_TABLE,
                                                          (tRBElem *) &
                                                          HostTopN, NULL);
        }
    }
    else
    {
        HostTopN.u4DsmonHostTopNCtlIndex = u4DsmonHostTopNCtlIndex;
        HostTopN.u4DsmonHostTopNIndex = u4DsmonHostTopNIndex;

        pHostTopN = (tDsmonHostTopN *) RBTreeGetNext (DSMON_HOST_TOPN_TABLE,
                                                      (tRBElem *) & HostTopN,
                                                      NULL);
    }
    return pHostTopN;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNAddTopNEntry                                  */
/*                                                                           */
/* Description  : Allocates memory and Adds Host TopN Entry into             */
/*                dsmonHostTopNRBTree                                        */
/*                                                                           */
/* Input        : pHostTopN                                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHost (SD/DS) / NULL Pointer                          */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostTopN *
DsmonHostTopNAddTopNEntry (UINT4 u4DsmonHostTopNIndex,
                           tDsmonHostTopN * pHostTopN)
{
    tDsmonHostTopN     *pTopNEntry = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNAddTopNEntry \r\n");

    if ((pTopNEntry = (tDsmonHostTopN *)
         (MemAllocMemBlk (DSMON_HOST_TOPN_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Host TopN\r\n");
        return NULL;
    }

    MEMSET (pTopNEntry, DSMON_INIT_VAL, sizeof (tDsmonHostTopN));
    MEMCPY (pTopNEntry, pHostTopN, sizeof (tDsmonHostTopN));

    pTopNEntry->u4DsmonHostTopNIndex = u4DsmonHostTopNIndex;

    if (RBTreeAdd (DSMON_HOST_TOPN_TABLE, (tRBElem *) pTopNEntry) == RB_SUCCESS)
    {
        return pTopNEntry;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonHostTopNAddTopNEntry TopN Entry addition failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_HOST_TOPN_POOL, (UINT1 *) pTopNEntry);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNDelTopNEntry                                  */
/*                                                                           */
/* Description  : Releases memory and Removes Host Entry from                */
/*                dsmonHostTopNRBTree                                        */
/*                                                                           */
/* Input        :  pHostTopN                                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonHostTopNDelTopNEntry (tDsmonHostTopN * pHostTopN)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNDelTopNEntry \r\n");

    /* Remove HostTopN. node from dsmonHostTopNRBTree */
    RBTreeRem (DSMON_HOST_TOPN_TABLE, (tRBElem *) pHostTopN);

    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_HOST_TOPN_POOL,
                            (UINT1 *) pHostTopN) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  HostTopN.\r\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNGetTopNEntry                                  */
/*                                                                           */
/* Description  : Get Host TopN Entry for the given control index            */
/*                                                                           */
/* Input        : u4DsmonHostTopNIndex                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonHostTopN / NULL Pointer                              */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonHostTopN *
DsmonHostTopNGetTopNEntry (UINT4 u4DsmonHostTopNControlIndex,
                           UINT4 u4DsmonHostTopNIndex)
{
    tDsmonHostTopN     *pHostTopN = NULL;
    tDsmonHostTopN      HostTopN;

    MEMSET (&HostTopN, DSMON_INIT_VAL, sizeof (tDsmonHostTopN));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNGetTopNEntry \r\n");

    HostTopN.u4DsmonHostTopNCtlIndex = u4DsmonHostTopNControlIndex;
    HostTopN.u4DsmonHostTopNIndex = u4DsmonHostTopNIndex;

    pHostTopN = (tDsmonHostTopN *) RBTreeGet (DSMON_HOST_TOPN_TABLE,
                                              (tRBElem *) & HostTopN);

    return pHostTopN;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonHostTopNDelTopNReport                                 */
/*                                                                           */
/* Description  : Delete the  TopN Entries if they are already present in    */
/*                dsmonHostTopNRBTree                                        */
/*                                                                           */
/* Input        : u4HostTopNCtlIndex                                         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonHostTopNDelTopNReport (UINT4 u4DsmonHostTopNCtlIndex)
{
    tDsmonHostTopN     *pTopNEntry = NULL;
    UINT4               u4HostTopNIndex = DSMON_ZERO;
    pTopNEntry =
        DsmonHostTopNGetNextTopNIndex (u4DsmonHostTopNCtlIndex,
                                       u4HostTopNIndex);

    while (pTopNEntry != NULL)
    {
        u4HostTopNIndex = pTopNEntry->u4DsmonHostTopNIndex;
        if (u4DsmonHostTopNCtlIndex != pTopNEntry->u4DsmonHostTopNCtlIndex)
        {
            break;
        }

        DsmonHostTopNDelTopNEntry (pTopNEntry);

        pTopNEntry = DsmonHostTopNGetNextTopNIndex
            (u4DsmonHostTopNCtlIndex, u4HostTopNIndex);
    }

}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetSamplesFromHostTable                                    */
/*                                                                           */
/* Description  : Get the initial samples from host table, when the time     */
/*                remaining and duration values are same                     */
/*                                                                           */
/* Input        : u4HostCtlIndex                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
GetSamplesFromHostTable (UINT4 u4HostCtlIndex)
{
    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             DsmonHostAddr;
    UINT4               u4HostGrpIndex = DSMON_ZERO, u4HostProtoIndex =
        DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY GetSamplesFromHostTable \r\n");

    MEMSET (&DsmonHostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    pHostStatsNode = DsmonHostGetNextDataIndex
        (u4HostCtlIndex, u4HostGrpIndex, u4HostProtoIndex, &DsmonHostAddr);

    while (pHostStatsNode != NULL)
    {

        if (pHostStatsNode->u4DsmonHostCtlIndex != u4HostCtlIndex)
        {
            break;
        }
        MEMCPY (&pHostStatsNode->DsmonPrevSample,
                &pHostStatsNode->DsmonCurSample, sizeof (tDsmonHostSample));

        pHostStatsNode =
            DsmonHostGetNextDataIndex (pHostStatsNode->u4DsmonHostCtlIndex,
                                       pHostStatsNode->u4DsmonHostAggGroupIndex,
                                       pHostStatsNode->
                                       u4DsmonHostProtDirLocalIndex,
                                       &pHostStatsNode->DsmonHostNLAddress);

    }                            /* end of while */

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FillHostSampleTopNRate                                     */
/*                                                                           */
/* Description  : Fill the topn rate values                                  */
/*                                                                           */
/* Input        : pSampleTopN,pHostStatsNode,                                */
/*                u4Value,u4HostTopNCtlIndex                                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
FillHostSampleTopNRate (tDsmonHostTopN * pSampleTopN, UINT4 u4Count,
                        tDsmonHostStats * pHostStatsNode, UINT4 u4Value,
                        tDsmonHostTopNCtl * pHostTopNCtl)
{

    UINT4               u4Index = DSMON_ZERO;
    UINT4               u4MinValue = DSMON_ZERO;
    UINT4               u4MinIndex = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY FillHostSampleTopNRate \r\n");

    if (u4Count < pHostTopNCtl->u4DsmonHostTopNCtlGranSize)
    {
        pSampleTopN[u4Count].u4DsmonHostTopNRate = u4Value;

        UINT8_RESET (pSampleTopN[u4Count].u8DsmonHostTopNHCRate);

        pSampleTopN[u4Count].u4DsmonHostTopNAggGroupIndex =
            pHostStatsNode->u4DsmonHostAggGroupIndex;

        pSampleTopN[u4Count].u4DsmonHostTopNPDLocalIndex =
            pHostStatsNode->u4DsmonHostProtDirLocalIndex;

        pSampleTopN[u4Count].u4DsmonHostTopNCtlIndex =
            pHostTopNCtl->u4DsmonHostTopNCtlIndex;

        pSampleTopN[u4Count].u4DsmonHostIpAddrLen =
            pHostStatsNode->u4DsmonHostIpAddrLen;

        MEMCPY (&pSampleTopN[u4Count].DsmonHostNLAddress,
                &pHostStatsNode->DsmonHostNLAddress, sizeof (tIpAddr));
    }
    else
    {
        /* find the least rate in the entries,
           and replace that with the new entry --
           this approach is chosen inorder to reduce the memory allocated,
           though the performance is not high */
        u4MinValue = pSampleTopN[u4MinIndex].u4DsmonHostTopNRate;

        while (u4Index < pHostTopNCtl->u4DsmonHostTopNCtlGranSize)
        {
            if (u4MinValue > pSampleTopN[u4Index].u4DsmonHostTopNRate)
            {
                u4MinValue = pSampleTopN[u4Index].u4DsmonHostTopNRate;
                u4MinIndex = u4Index;
            }
            u4Index++;
        }

        if (u4MinValue <= u4Value)
        {

            pSampleTopN[u4MinIndex].u4DsmonHostTopNRate = u4Value;

            UINT8_RESET (pSampleTopN[u4MinIndex].u8DsmonHostTopNHCRate);

            pSampleTopN[u4MinIndex].u4DsmonHostTopNAggGroupIndex =
                pHostStatsNode->u4DsmonHostAggGroupIndex;

            pSampleTopN[u4MinIndex].u4DsmonHostTopNPDLocalIndex =
                pHostStatsNode->u4DsmonHostProtDirLocalIndex;

            pSampleTopN[u4MinIndex].u4DsmonHostTopNCtlIndex =
                pHostTopNCtl->u4DsmonHostTopNCtlIndex;

            pSampleTopN[u4MinIndex].u4DsmonHostIpAddrLen =
                pHostStatsNode->u4DsmonHostIpAddrLen;

            MEMCPY (&pSampleTopN[u4MinIndex].DsmonHostNLAddress,
                    &pHostStatsNode->DsmonHostNLAddress, sizeof (tIpAddr));
        }

    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FillHostSampleTopNHCRate                                   */
/*                                                                           */
/* Description  : Fill the sample topn high capacity rate values             */
/*                                                                           */
/* Input        : pSampleTopN,pHostStatsNode                                 */
/*                u4MsnValue,u4LsnValue,u4HostTopNCtlIndex                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PRIVATE VOID
FillHostSampleTopNHCRate (tDsmonHostTopN * pSampleTopN, UINT4 u4Count,
                          tDsmonHostStats * pHostStatsNode,
                          UINT4 u4MsnValue, UINT4 u4LsnValue,
                          tDsmonHostTopNCtl * pHostTopNCtl)
{
    UINT4               u4Index = DSMON_ZERO;
    tUint8              u8MinValue;
    tUint8              u8NewValue;
    UINT4               u4MinIndex = DSMON_ZERO;
    INT4                i4Value = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY FillHostSampleTopNHCRate \r\n");

    if (u4Count < pHostTopNCtl->u4DsmonHostTopNCtlGranSize)
    {
        pSampleTopN[u4Count].u4DsmonHostTopNRate = u4LsnValue;

        pSampleTopN[u4Count].u8DsmonHostTopNHCRate.u4HiWord = u4MsnValue;
        pSampleTopN[u4Count].u8DsmonHostTopNHCRate.u4LoWord = u4LsnValue;

        pSampleTopN[u4Count].u4DsmonHostTopNAggGroupIndex =
            pHostStatsNode->u4DsmonHostAggGroupIndex;

        pSampleTopN[u4Count].u4DsmonHostTopNPDLocalIndex =
            pHostStatsNode->u4DsmonHostProtDirLocalIndex;

        pSampleTopN[u4Count].u4DsmonHostTopNCtlIndex =
            pHostTopNCtl->u4DsmonHostTopNCtlIndex;

        pSampleTopN[u4Count].u4DsmonHostIpAddrLen =
            pHostStatsNode->u4DsmonHostIpAddrLen;

        MEMCPY (&pSampleTopN[u4Count].DsmonHostNLAddress,
                &pHostStatsNode->DsmonHostNLAddress,
                sizeof (pHostStatsNode->DsmonHostNLAddress));
    }
    else
    {
        /* find the least rate in the entries,
           and replace that with the new entry --
           this approach is chosen inorder to reduce the memory allocated,
           though the performance is not high */

        u8MinValue.u4HiWord =
            pSampleTopN[u4MinIndex].u8DsmonHostTopNHCRate.u4HiWord;
        u8MinValue.u4LoWord =
            pSampleTopN[u4MinIndex].u8DsmonHostTopNHCRate.u4LoWord;
        while (u4Index < pHostTopNCtl->u4DsmonHostTopNCtlGranSize)
        {
            UINT8_CMP (pSampleTopN[u4Index].u8DsmonHostTopNHCRate, u8MinValue,
                       i4Value);

            if (i4Value == -1)
            {
                u8MinValue.u4HiWord =
                    pSampleTopN[u4Index].u8DsmonHostTopNHCRate.u4HiWord;
                u8MinValue.u4LoWord =
                    pSampleTopN[u4Index].u8DsmonHostTopNHCRate.u4LoWord;
                u4MinIndex = u4Index;
            }
            u4Index++;
        }

        u8NewValue.u4HiWord = u4MsnValue;

        u8NewValue.u4LoWord = u4LsnValue;

        UINT8_CMP (u8MinValue, u8NewValue, i4Value);

        if ((i4Value == -1) || (i4Value == 0))
        {

            pSampleTopN[u4MinIndex].u4DsmonHostTopNRate = u4LsnValue;

            pSampleTopN[u4MinIndex].u8DsmonHostTopNHCRate.u4HiWord = u4MsnValue;
            pSampleTopN[u4MinIndex].u8DsmonHostTopNHCRate.u4LoWord = u4LsnValue;

            pSampleTopN[u4MinIndex].u4DsmonHostTopNAggGroupIndex =
                pHostStatsNode->u4DsmonHostAggGroupIndex;

            pSampleTopN[u4MinIndex].u4DsmonHostTopNPDLocalIndex =
                pHostStatsNode->u4DsmonHostProtDirLocalIndex;

            pSampleTopN[u4MinIndex].u4DsmonHostTopNCtlIndex =
                pHostTopNCtl->u4DsmonHostTopNCtlIndex;

            pSampleTopN[u4MinIndex].u4DsmonHostIpAddrLen =
                pHostStatsNode->u4DsmonHostIpAddrLen;

            MEMCPY (&pSampleTopN[u4MinIndex].DsmonHostNLAddress,
                    &pHostStatsNode->DsmonHostNLAddress,
                    sizeof (pHostStatsNode->DsmonHostNLAddress));
        }
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : GetHostTopNEntries                                         */
/*                                                                           */
/* Description  : Get the current samples from Host table                    */
/*                                                                           */
/* Input        : pHostTopNCtl,pSampleTopN                                   */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : u4Count                                                    */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
GetHostTopNEntries (tDsmonHostTopNCtl * pHostTopNCtl,
                    tDsmonHostTopN * pSampleTopN)
{
    UINT4               u4Count = DSMON_ZERO, u4Delta = DSMON_ZERO, u4DeltaIn =
        DSMON_ZERO, u4DeltaOut = DSMON_ZERO;
    UINT4               u4Total = DSMON_ZERO, u4CtlIndex =
        DSMON_ZERO, u4GrpIndex = DSMON_ZERO, u4ProtoLIndex = DSMON_ZERO;
    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             DsmonHostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY GetHostTopNEntries\r\n");

    MEMSET (&DsmonHostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    u4CtlIndex = pHostTopNCtl->u4DsmonHostTopNCtlHostIndex;

    pHostStatsNode = DsmonHostGetNextDataIndex (u4CtlIndex, u4GrpIndex,
                                                u4ProtoLIndex, &DsmonHostAddr);

    while (pHostStatsNode != NULL)
    {
        u4GrpIndex = pHostStatsNode->u4DsmonHostAggGroupIndex;
        u4ProtoLIndex = pHostStatsNode->u4DsmonHostProtDirLocalIndex;

        if (pHostStatsNode->u4DsmonHostCtlIndex !=
            pHostTopNCtl->u4DsmonHostTopNCtlHostIndex)
        {
            break;
        }
        switch (pHostTopNCtl->u4DsmonHostTopNCtlRateBase)
        {
            case hostTopNInPkts:

                if (pHostStatsNode->DsmonPrevSample.u4DsmonHostInPkts !=
                    DSMON_ZERO)
                {
                    u4Delta = pHostStatsNode->DsmonCurSample.u4DsmonHostInPkts -
                        pHostStatsNode->DsmonPrevSample.u4DsmonHostInPkts;

                    if (u4Delta > DSMON_ZERO)
                    {
                        FillHostSampleTopNRate (pSampleTopN, u4Count,
                                                pHostStatsNode, u4Delta,
                                                pHostTopNCtl);

                        u4Count++;
                    }
                }

                break;

            case hostTopNInOctets:

                if (pHostStatsNode->DsmonPrevSample.u4DsmonHostInOctets !=
                    DSMON_ZERO)
                {
                    u4Delta =
                        pHostStatsNode->DsmonCurSample.u4DsmonHostInOctets -
                        pHostStatsNode->DsmonPrevSample.u4DsmonHostInOctets;

                    if (u4Delta > DSMON_ZERO)
                    {
                        FillHostSampleTopNRate (pSampleTopN, u4Count,
                                                pHostStatsNode, u4Delta,
                                                pHostTopNCtl);

                        u4Count++;
                    }

                }

                break;

            case hostTopNOutPkts:

                if (pHostStatsNode->DsmonPrevSample.u4DsmonHostOutPkts !=
                    DSMON_ZERO)
                {
                    u4Delta =
                        pHostStatsNode->DsmonCurSample.u4DsmonHostOutPkts -
                        pHostStatsNode->DsmonPrevSample.u4DsmonHostOutPkts;

                    if (u4Delta > DSMON_ZERO)
                    {
                        FillHostSampleTopNRate (pSampleTopN, u4Count,
                                                pHostStatsNode, u4Delta,
                                                pHostTopNCtl);

                        u4Count++;
                    }

                }

                break;

            case hostTopNOutOctets:

                if (pHostStatsNode->DsmonPrevSample.u4DsmonHostOutOctets !=
                    DSMON_ZERO)
                {
                    u4Delta =
                        pHostStatsNode->DsmonCurSample.u4DsmonHostOutOctets -
                        pHostStatsNode->DsmonPrevSample.u4DsmonHostOutOctets;

                    if (u4Delta > DSMON_ZERO)
                    {
                        FillHostSampleTopNRate (pSampleTopN, u4Count,
                                                pHostStatsNode, u4Delta,
                                                pHostTopNCtl);

                        u4Count++;
                    }
                }

                break;

            case hostTopNTotalPkts:

                u4DeltaIn = pHostStatsNode->DsmonCurSample.u4DsmonHostInPkts -
                    pHostStatsNode->DsmonPrevSample.u4DsmonHostInPkts;

                u4DeltaOut = pHostStatsNode->DsmonCurSample.u4DsmonHostOutPkts -
                    pHostStatsNode->DsmonPrevSample.u4DsmonHostOutPkts;

                u4Total = u4DeltaIn + u4DeltaOut;

                if (u4Total > DSMON_ZERO)
                {
                    FillHostSampleTopNRate (pSampleTopN, u4Count,
                                            pHostStatsNode, u4Total,
                                            pHostTopNCtl);

                    u4Count++;
                }

                break;

            case hostTopNTotalOctets:

                u4DeltaIn = pHostStatsNode->DsmonCurSample.u4DsmonHostInOctets -
                    pHostStatsNode->DsmonPrevSample.u4DsmonHostInOctets;

                u4DeltaOut =
                    pHostStatsNode->DsmonCurSample.u4DsmonHostOutOctets -
                    pHostStatsNode->DsmonPrevSample.u4DsmonHostOutOctets;

                u4Total = u4DeltaIn + u4DeltaOut;

                if (u4Total > DSMON_ZERO)
                {
                    FillHostSampleTopNRate (pSampleTopN, u4Count,
                                            pHostStatsNode, u4Total,
                                            pHostTopNCtl);

                    u4Count++;
                }

                break;

            case hostTopNInHCPkts:

                if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts.
                     u4HiWord != DSMON_ZERO)
                    && (pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts.
                        u4LoWord != DSMON_ZERO))
                {
                    UINT8_SUB (pHostStatsNode->DsmonCurSample.
                               u8DsmonHostInHCPkts,
                               pHostStatsNode->DsmonPrevSample.
                               u8DsmonHostInHCPkts,
                               pHostStatsNode->DsmonPrevSample.
                               u8DsmonHostInHCPkts);

                    if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts.
                         u4HiWord > DSMON_ZERO)
                        || (pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts.
                            u4LoWord > DSMON_ZERO))
                    {
                        FillHostSampleTopNHCRate (pSampleTopN, u4Count,
                                                  pHostStatsNode,
                                                  pHostStatsNode->
                                                  DsmonPrevSample.
                                                  u8DsmonHostInHCPkts.u4HiWord,
                                                  pHostStatsNode->
                                                  DsmonPrevSample.
                                                  u8DsmonHostInHCPkts.u4LoWord,
                                                  pHostTopNCtl);

                        u4Count++;
                    }
                }

                break;

            case hostTopNInHCOctets:

                if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCOctets.
                     u4HiWord != DSMON_ZERO)
                    && (pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCOctets.
                        u4LoWord != DSMON_ZERO))
                {
                    UINT8_SUB (pHostStatsNode->DsmonCurSample.
                               u8DsmonHostInHCOctets,
                               pHostStatsNode->DsmonPrevSample.
                               u8DsmonHostInHCOctets,
                               pHostStatsNode->DsmonPrevSample.
                               u8DsmonHostInHCOctets);

                    if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCOctets.
                         u4HiWord > DSMON_ZERO)
                        || (pHostStatsNode->DsmonPrevSample.
                            u8DsmonHostInHCOctets.u4LoWord > DSMON_ZERO))
                    {
                        FillHostSampleTopNHCRate (pSampleTopN, u4Count,
                                                  pHostStatsNode,
                                                  pHostStatsNode->
                                                  DsmonPrevSample.
                                                  u8DsmonHostInHCOctets.
                                                  u4HiWord,
                                                  pHostStatsNode->
                                                  DsmonPrevSample.
                                                  u8DsmonHostInHCOctets.
                                                  u4LoWord, pHostTopNCtl);

                        u4Count++;
                    }
                }

                break;

            case hostTopNOutHCPkts:

                if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCPkts.
                     u4HiWord != DSMON_ZERO)
                    && (pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCPkts.
                        u4LoWord != DSMON_ZERO))
                {
                    UINT8_SUB (pHostStatsNode->DsmonCurSample.
                               u8DsmonHostOutHCPkts,
                               pHostStatsNode->DsmonPrevSample.
                               u8DsmonHostOutHCPkts,
                               pHostStatsNode->DsmonPrevSample.
                               u8DsmonHostOutHCPkts);

                    if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCPkts.
                         u4HiWord > DSMON_ZERO)
                        || (pHostStatsNode->DsmonPrevSample.
                            u8DsmonHostOutHCPkts.u4LoWord > DSMON_ZERO))
                    {
                        FillHostSampleTopNHCRate (pSampleTopN, u4Count,
                                                  pHostStatsNode,
                                                  pHostStatsNode->
                                                  DsmonPrevSample.
                                                  u8DsmonHostOutHCPkts.u4HiWord,
                                                  pHostStatsNode->
                                                  DsmonPrevSample.
                                                  u8DsmonHostOutHCPkts.u4LoWord,
                                                  pHostTopNCtl);

                        u4Count++;
                    }
                }

                break;

            case hostTopNOutHCOctets:

                if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCOctets.
                     u4HiWord != DSMON_ZERO)
                    && (pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCOctets.
                        u4HiWord != DSMON_ZERO))
                {
                    UINT8_SUB (pHostStatsNode->DsmonCurSample.
                               u8DsmonHostOutHCOctets,
                               pHostStatsNode->DsmonPrevSample.
                               u8DsmonHostOutHCOctets,
                               pHostStatsNode->DsmonPrevSample.
                               u8DsmonHostOutHCOctets);

                    if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCOctets.
                         u4HiWord > DSMON_ZERO)
                        || (pHostStatsNode->DsmonPrevSample.
                            u8DsmonHostOutHCOctets.u4LoWord > DSMON_ZERO))
                    {
                        FillHostSampleTopNHCRate (pSampleTopN, u4Count,
                                                  pHostStatsNode,
                                                  pHostStatsNode->
                                                  DsmonPrevSample.
                                                  u8DsmonHostOutHCOctets.
                                                  u4HiWord,
                                                  pHostStatsNode->
                                                  DsmonPrevSample.
                                                  u8DsmonHostOutHCOctets.
                                                  u4LoWord, pHostTopNCtl);

                        u4Count++;
                    }
                }

                break;

            case hostTopNTotalHCPkts:
                UINT8_SUB (pHostStatsNode->DsmonCurSample.u8DsmonHostInHCPkts,
                           pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts,
                           pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts);

                UINT8_SUB (pHostStatsNode->DsmonCurSample.u8DsmonHostOutHCPkts,
                           pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCPkts,
                           pHostStatsNode->DsmonPrevSample.
                           u8DsmonHostOutHCPkts);

                UINT8_ADD (pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts,
                           pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCPkts.
                           u4LoWord);

                pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts.u4HiWord +=
                    pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCPkts.
                    u4HiWord;

                if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts.
                     u4HiWord > DSMON_ZERO)
                    || (pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCPkts.
                        u4LoWord > DSMON_ZERO))
                {
                    FillHostSampleTopNHCRate (pSampleTopN, u4Count,
                                              pHostStatsNode,
                                              pHostStatsNode->DsmonPrevSample.
                                              u8DsmonHostInHCPkts.u4HiWord,
                                              pHostStatsNode->DsmonPrevSample.
                                              u8DsmonHostInHCPkts.u4LoWord,
                                              pHostTopNCtl);

                    u4Count++;

                }

                break;

            case hostTopNTotalHCOctets:
                UINT8_SUB (pHostStatsNode->DsmonCurSample.u8DsmonHostInHCOctets,
                           pHostStatsNode->DsmonPrevSample.
                           u8DsmonHostInHCOctets,
                           pHostStatsNode->DsmonPrevSample.
                           u8DsmonHostInHCOctets);

                UINT8_SUB (pHostStatsNode->DsmonCurSample.
                           u8DsmonHostOutHCOctets,
                           pHostStatsNode->DsmonPrevSample.
                           u8DsmonHostOutHCOctets,
                           pHostStatsNode->DsmonPrevSample.
                           u8DsmonHostOutHCOctets);

                UINT8_ADD (pHostStatsNode->DsmonPrevSample.
                           u8DsmonHostInHCOctets,
                           pHostStatsNode->DsmonPrevSample.
                           u8DsmonHostOutHCOctets.u4LoWord);

                pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCOctets.
                    u4HiWord +=
                    pHostStatsNode->DsmonPrevSample.u8DsmonHostOutHCOctets.
                    u4HiWord;

                if ((pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCOctets.
                     u4HiWord > DSMON_ZERO)
                    || (pHostStatsNode->DsmonPrevSample.u8DsmonHostInHCOctets.
                        u4LoWord > DSMON_ZERO))
                {
                    FillHostSampleTopNHCRate (pSampleTopN, u4Count,
                                              pHostStatsNode,
                                              pHostStatsNode->DsmonPrevSample.
                                              u8DsmonHostInHCOctets.u4HiWord,
                                              pHostStatsNode->DsmonPrevSample.
                                              u8DsmonHostInHCOctets.u4LoWord,
                                              pHostTopNCtl);

                    u4Count++;
                }

                break;

        }                        /* end of switch */

        pHostStatsNode =
            DsmonHostGetNextDataIndex (u4CtlIndex, u4GrpIndex, u4ProtoLIndex,
                                       &pHostStatsNode->DsmonHostNLAddress);

    }                            /* end of while */

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT GetHostTopNEntries \n");

    return u4Count;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : SetSortedHostTopNTable                                     */
/*                                                                           */
/* Description  : Generates topn report of size, granted size                */
/*                                                                           */
/* Input        : pSampleTopN,u4Count,u4GrantedSize                          */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/

PRIVATE UINT4
SetSortedHostTopNTable (tDsmonHostTopN * pSampleTopN, UINT4 u4Count,
                        UINT4 u4GrantedSize)
{
    UINT4               u4Pass = DSMON_ZERO;
    UINT4               u4EntryCount = DSMON_ZERO;
    tDsmonHostTopN     *pHostTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY SetSortedHostTopNTable \n");

    u4EntryCount = (u4Count < u4GrantedSize) ? u4Count : u4GrantedSize;

    /* Add the topN entries in the Host TopN Table */
    u4Count = u4EntryCount - 1;

    for (u4Pass = DSMON_ONE; u4Pass <= u4EntryCount; u4Pass++)
    {
        pHostTopN = DsmonHostTopNAddTopNEntry (u4Pass, &pSampleTopN[u4Count]);

        if (pHostTopN == NULL)
        {
            DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                       "Memory Allocation/ \
            RBTree Add Failed - Host TopN Table \n");

            return OSIX_FAILURE;
        }

        u4Count--;

    }

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT SetSortedHostTopNTable \n");

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CompareHostTopNRate                                        */
/*                                                                           */
/* Description  : Compare function for qsort based on TopNRate               */
/*                                                                           */
/* Input        : pParam1, pParam2                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 - if pParam1 is less than pParam2                       */
/*                1  - if pParam1 is greater than pParam2                    */
/*                0  - if pParam1 and pParam2 are equal                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
CompareHostTopNRate (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tDsmonHostTopN *pSample1 = (CONST tDsmonHostTopN *) pParam1;
    CONST tDsmonHostTopN *pSample2 = (CONST tDsmonHostTopN *) pParam2;

    if (pSample1->u4DsmonHostTopNRate < pSample2->u4DsmonHostTopNRate)
    {
        return -1;
    }
    if (pSample1->u4DsmonHostTopNRate > pSample2->u4DsmonHostTopNRate)
    {
        return 1;
    }
    if (pSample1->u4DsmonHostTopNRate == pSample2->u4DsmonHostTopNRate)
    {
        return 0;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : CompareHostTopNHCRate                                      */
/*                                                                           */
/* Description  : Compare function for qsort based on TopNHCRate             */
/*                                                                           */
/* Input        : pParam1, pParam2                                           */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : -1 - if pParam1 is less than pParam2                       */
/*                1  - if pParam1 is greater than pParam2                    */
/*                0  - if pParam1 and pParam2 are equal                      */
/*                                                                           */
/*****************************************************************************/

PRIVATE INT4
CompareHostTopNHCRate (CONST VOID *pParam1, CONST VOID *pParam2)
{
    CONST tDsmonHostTopN *pSample1 = (CONST tDsmonHostTopN *) pParam1;
    CONST tDsmonHostTopN *pSample2 = (CONST tDsmonHostTopN *) pParam2;
    INT4                i4Value = DSMON_ZERO;

    UINT8_CMP (pSample1->u8DsmonHostTopNHCRate, pSample2->u8DsmonHostTopNHCRate,
               i4Value);

    return i4Value;
}

/*****************************************************************************/
/* Function           : DsmonHostTopNUpdateTable                             */
/*                                                                           */
/* Input (s)          : None.                                                */
/*                                                                           */
/* Output (s)         : None.                                                */
/*                                                                           */
/* Returns            : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/*             Used   :                                                      */
/*                                                                           */
/* Side effects       : None                                                 */
/*                                                                           */
/* Action :                                                                  */
/*                                                                           */
/*****************************************************************************/

VOID                DsmonHostTopNUpdateTable
ARG_LIST ((VOID))
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;
    tDsmonHostSampleTopN *pSampleTopN = NULL;
    tDsmonHostCtl      *pHostCtlNode = NULL;
    UINT4               u4Count = DSMON_ZERO, u4TopNCtl = DSMON_ZERO;
    UINT4               u4EntryCount = DSMON_ZERO;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonHostTopNUpdateTable \n");

    if ((pSampleTopN = (tDsmonHostSampleTopN *)
         (MemAllocMemBlk (DSMON_HOST_SAMPLE_TOPN_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_MEM_FAIL | OS_RESOURCE_TRC,
                   "Memory Allocation failed for Host Sample TopN\r\n");
        return;
    }

    MEMSET (pSampleTopN, 0, sizeof (tDsmonHostSampleTopN));
    pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (u4TopNCtl);

    while (pHostTopNCtl != NULL)
    {
        u4TopNCtl = pHostTopNCtl->u4DsmonHostTopNCtlIndex;
        if (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus == ACTIVE)
        {
            if (pHostTopNCtl->u4DsmonHostTopNCtlTimeRem > DSMON_ZERO)
            {
                if (pHostTopNCtl->u4DsmonHostTopNCtlTimeRem ==
                    pHostTopNCtl->u4DsmonHostTopNCtlDuration)
                {
                    GetSamplesFromHostTable
                        (pHostTopNCtl->u4DsmonHostTopNCtlHostIndex);
                }
                else
                {
                    pHostCtlNode =
                        DsmonHostGetCtlEntry (pHostTopNCtl->
                                              u4DsmonHostTopNCtlHostIndex);

                    if (pHostTopNCtl->u4DsmonHostTopNCtlTimeRem == DSMON_ONE)
                    {

                        u4Count =
                            GetHostTopNEntries (pHostTopNCtl,
                                                (tDsmonHostTopN *) pSampleTopN->
                                                SampleTopN);

                        if (u4Count > 0)
                        {
                            /* Has to free TopN Entries if they were already present in
                             * dsmonHostTopNRBTree */

                            DsmonHostTopNDelTopNReport (pHostTopNCtl->
                                                        u4DsmonHostTopNCtlIndex);

                            /* Sort the TopN entries based on the rate base */
                            u4EntryCount =
                                (u4Count <
                                 pHostTopNCtl->
                                 u4DsmonHostTopNCtlGranSize) ? u4Count :
                                pHostTopNCtl->u4DsmonHostTopNCtlGranSize;

                            switch (pHostTopNCtl->u4DsmonHostTopNCtlRateBase)
                            {
                                case hostTopNInPkts:
                                case hostTopNInOctets:
                                case hostTopNOutPkts:
                                case hostTopNOutOctets:
                                case hostTopNTotalPkts:
                                case hostTopNTotalOctets:

                                    qsort ((tDsmonHostTopN *) pSampleTopN->
                                           SampleTopN, u4EntryCount,
                                           sizeof (tDsmonHostTopN),
                                           (CONST VOID *) CompareHostTopNRate);

                                    break;

                                case hostTopNInHCPkts:
                                case hostTopNInHCOctets:
                                case hostTopNOutHCPkts:
                                case hostTopNOutHCOctets:
                                case hostTopNTotalHCPkts:
                                case hostTopNTotalHCOctets:

                                    qsort ((tDsmonHostTopN *) pSampleTopN->
                                           SampleTopN, u4EntryCount,
                                           sizeof (tDsmonHostTopN),
                                           (CONST VOID *)
                                           CompareHostTopNHCRate);

                                    break;
                            }
                            if (SetSortedHostTopNTable
                                ((tDsmonHostTopN *) pSampleTopN->SampleTopN,
                                 u4Count,
                                 pHostTopNCtl->u4DsmonHostTopNCtlGranSize) ==
                                OSIX_FAILURE)
                            {
                                /*  TopN Report generation failed */
                                DSMON_TRC (DSMON_DEBUG_TRC,
                                           "OSIX Failure: TopN Report Generation failed \n");
                            }
                            else
                            {
                                pHostTopNCtl->u4DsmonHostTopNCtlGenReports++;
                            }
                        }
                        /* start another collection */
                        pHostTopNCtl->u4DsmonHostTopNCtlTimeRem =
                            pHostTopNCtl->u4DsmonHostTopNCtlDuration + 1;
                        pHostTopNCtl->u4DsmonHostTopNCtlStartTime =
                            (UINT4) OsixGetSysUpTime ();
                    }
                }
                pHostTopNCtl->u4DsmonHostTopNCtlTimeRem--;
            }
        }

        pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (u4TopNCtl);

    }                            /* end of while */

    MemReleaseMemBlock (DSMON_HOST_SAMPLE_TOPN_POOL, ((UINT1 *) pSampleTopN));

    DSMON_TRC (DSMON_FN_EXIT, "FUNC:EXIT DsmonHostTopNUpdateTable \n");

    UNUSED_PARAM (pHostCtlNode);

}
