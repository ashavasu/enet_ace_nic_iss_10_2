/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 *                                                                  *
 * $Id: dsmnhtlw.c,v 1.12 2012/12/13 14:25:09 siva Exp $           *
 *
 * Description: This file contains the low level nmh routines         *
 *        for DSMON Host TopN module                          *
 *                                                                  *
 *******************************************************************/
#include "dsmninc.h"
/* LOW LEVEL Routines for Table : DsmonHostCtlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonHostCtlTable
 Input       :  The Indices
                DsmonHostCtlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonHostCtlTable (INT4 i4DsmonHostCtlIndex)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonHostCtlTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostCtlIndex < DSMON_ONE) ||
        (i4DsmonHostCtlIndex > DSMON_MAX_CTL_INDEX))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControlIndex out of range \n");

        return SNMP_FAILURE;
    }

    /* Get Host Control Node corresponding to HostControlIndex */

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonHostCtlTable
 Input       :  The Indices
                DsmonHostCtlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonHostCtlTable (INT4 *pi4DsmonHostCtlIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetFirstIndexDsmonHostCtlTable \n");

    return nmhGetNextIndexDsmonHostCtlTable (DSMON_ZERO, pi4DsmonHostCtlIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonHostCtlTable
 Input       :  The Indices
                DsmonHostCtlIndex
                nextDsmonHostCtlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonHostCtlTable (INT4 i4DsmonHostCtlIndex,
                                  INT4 *pi4NextDsmonHostCtlIndex)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetNextIndexDsmonHostCtlTable \n");

    pHostCtlNode = DsmonHostGetNextCtlIndex (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonHostCtlIndex = (INT4) (pHostCtlNode->u4DsmonHostCtlIndex);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlDataSource
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlDataSource (INT4 i4DsmonHostCtlIndex,
                              tSNMP_OID_TYPE * pRetValDsmonHostCtlDataSource)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlDataSource \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_GET_DATA_SOURCE_OID (pRetValDsmonHostCtlDataSource,
                               pHostCtlNode->u4DsmonHostDataSource);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlAggProfile
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlAggProfile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlAggProfile (INT4 i4DsmonHostCtlIndex,
                              INT4 *pi4RetValDsmonHostCtlAggProfile)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlAggProfile \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostCtlAggProfile =
        (INT4) (pHostCtlNode->u4DsmonHostCtlProfileIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlMaxDesiredEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlMaxDesiredEntries (INT4 i4DsmonHostCtlIndex,
                                     INT4
                                     *pi4RetValDsmonHostCtlMaxDesiredEntries)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonHostCtlMaxDesiredEntries \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostCtlMaxDesiredEntries =
        pHostCtlNode->i4DsmonHostCtlMaxDesiredEntries;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlIPv4PrefixLen
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlIPv4PrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlIPv4PrefixLen (INT4 i4DsmonHostCtlIndex,
                                 INT4 *pi4RetValDsmonHostCtlIPv4PrefixLen)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlIPv4PrefixLen \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostCtlIPv4PrefixLen =
        (INT4) (pHostCtlNode->u4DsmonHostCtlIPv4PrefixLen);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlIPv6PrefixLen
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlIPv6PrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlIPv6PrefixLen (INT4 i4DsmonHostCtlIndex,
                                 INT4 *pi4RetValDsmonHostCtlIPv6PrefixLen)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlIPv6PrefixLen \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostCtlIPv6PrefixLen =
        (INT4) (pHostCtlNode->u4DsmonHostCtlIPv6PrefixLen);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlDroppedFrames
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlDroppedFrames (INT4 i4DsmonHostCtlIndex,
                                 UINT4 *pu4RetValDsmonHostCtlDroppedFrames)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlDroppedFrames \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostCtlDroppedFrames =
        pHostCtlNode->u4DsmonHostCtlDroppedFrames;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlInserts
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlInserts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlInserts (INT4 i4DsmonHostCtlIndex,
                           UINT4 *pu4RetValDsmonHostCtlInserts)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlInserts \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostCtlInserts = pHostCtlNode->u4DsmonHostCtlInserts;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlDeletes
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlDeletes (INT4 i4DsmonHostCtlIndex,
                           UINT4 *pu4RetValDsmonHostCtlDeletes)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlDeletes \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostCtlDeletes = pHostCtlNode->u4DsmonHostCtlDeletes;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlCreateTime
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlCreateTime (INT4 i4DsmonHostCtlIndex,
                              UINT4 *pu4RetValDsmonHostCtlCreateTime)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlCreateTime \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostCtlCreateTime = pHostCtlNode->u4DsmonHostCtlCreateTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlOwner
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlOwner (INT4 i4DsmonHostCtlIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValDsmonHostCtlOwner)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlOwner \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDsmonHostCtlOwner->pu1_OctetList,
            pHostCtlNode->au1DsmonHostCtlOwner, DSMON_MAX_OWNER_LEN);

    pRetValDsmonHostCtlOwner->i4_Length =
        STRLEN (pHostCtlNode->au1DsmonHostCtlOwner);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCtlStatus
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                retValDsmonHostCtlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCtlStatus (INT4 i4DsmonHostCtlIndex,
                          INT4 *pi4RetValDsmonHostCtlStatus)
{
    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCtlStatus \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostCtlStatus =
        (INT4) (pHostCtlNode->u4DsmonHostCtlRowStatus);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetDsmonHostCtlDataSource
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                setValDsmonHostCtlDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostCtlDataSource (INT4 i4DsmonHostCtlIndex,
                              tSNMP_OID_TYPE * pSetValDsmonHostCtlDataSource)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostCtlDataSource \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_SET_DATA_SOURCE_OID (pSetValDsmonHostCtlDataSource,
                               &(pHostCtlNode->u4DsmonHostDataSource));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonHostCtlAggProfile
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                setValDsmonHostCtlAggProfile
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostCtlAggProfile (INT4 i4DsmonHostCtlIndex,
                              INT4 i4SetValDsmonHostCtlAggProfile)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostCtlAggProfile \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    pHostCtlNode->u4DsmonHostCtlProfileIndex =
        (UINT4) (i4SetValDsmonHostCtlAggProfile);
    pHostCtlNode->pAggCtl =
        DsmonAggCtlGetEntry (i4SetValDsmonHostCtlAggProfile);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonHostCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                setValDsmonHostCtlMaxDesiredEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostCtlMaxDesiredEntries (INT4 i4DsmonHostCtlIndex,
                                     INT4 i4SetValDsmonHostCtlMaxDesiredEntries)
{

    UINT4               u4Minimum = 0;

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonHostCtlMaxDesiredEntries \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    u4Minimum = (i4SetValDsmonHostCtlMaxDesiredEntries != -1) ?
        ((i4SetValDsmonHostCtlMaxDesiredEntries <
          DSMON_MAX_DATA_PER_CTL) ?
         i4SetValDsmonHostCtlMaxDesiredEntries :
         DSMON_MAX_DATA_PER_CTL) : DSMON_MAX_DATA_PER_CTL;

    pHostCtlNode->i4DsmonHostCtlMaxDesiredEntries =
        i4SetValDsmonHostCtlMaxDesiredEntries;

    pHostCtlNode->u4DsmonHostCtlMaxDesiredSupported = u4Minimum;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonHostCtlIPv4PrefixLen
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                setValDsmonHostCtlIPv4PrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostCtlIPv4PrefixLen (INT4 i4DsmonHostCtlIndex,
                                 INT4 i4SetValDsmonHostCtlIPv4PrefixLen)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostCtlIPv4PrefixLen \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    pHostCtlNode->u4DsmonHostCtlIPv4PrefixLen =
        (UINT4) (i4SetValDsmonHostCtlIPv4PrefixLen);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonHostCtlIPv6PrefixLen
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                setValDsmonHostCtlIPv6PrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostCtlIPv6PrefixLen (INT4 i4DsmonHostCtlIndex,
                                 INT4 i4SetValDsmonHostCtlIPv6PrefixLen)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostCtlIPv6PrefixLen \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    pHostCtlNode->u4DsmonHostCtlIPv6PrefixLen =
        (UINT4) (i4SetValDsmonHostCtlIPv6PrefixLen);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonHostCtlOwner
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                setValDsmonHostCtlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostCtlOwner (INT4 i4DsmonHostCtlIndex,
                         tSNMP_OCTET_STRING_TYPE * pSetValDsmonHostCtlOwner)
{
    UINT4               u4Minimum = 0;

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostCtlOwner \n");

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Host Control Index \n");

        return SNMP_FAILURE;
    }

    u4Minimum = (pSetValDsmonHostCtlOwner->i4_Length < DSMON_MAX_OWNER_LEN) ?
        pSetValDsmonHostCtlOwner->i4_Length : DSMON_MAX_OWNER_LEN;

    MEMSET (pHostCtlNode->au1DsmonHostCtlOwner, DSMON_INIT_VAL,
            DSMON_MAX_OWNER_LEN);

    MEMCPY (pHostCtlNode->au1DsmonHostCtlOwner,
            pSetValDsmonHostCtlOwner->pu1_OctetList, u4Minimum);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonHostCtlStatus
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                setValDsmonHostCtlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostCtlStatus (INT4 i4DsmonHostCtlIndex,
                          INT4 i4SetValDsmonHostCtlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tDsmonHostCtl      *pHostCtlNode = NULL;

    tDsmonAggCtl       *pAggCtlNode = NULL;

    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;

    INT4                i4Result = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostCtlStatus \n");

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));

    switch (i4SetValDsmonHostCtlStatus)
    {
        case CREATE_AND_WAIT:

            pHostCtlNode = DsmonHostAddCtlEntry (i4DsmonHostCtlIndex);

            if (pHostCtlNode == NULL)
            {
                DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                           "Memory Allocation RBTree Add Failed-Host Control Table \n");

                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:

            pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

            if (pHostCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Host Control Index \n");

                return SNMP_FAILURE;
            }

            /* check for valid agg profile index */

            pAggCtlNode =
                DsmonAggCtlGetEntry (pHostCtlNode->u4DsmonHostCtlProfileIndex);

            if (pAggCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure:\
                 Invalid Host Control Agg Profile Index \n");

                return SNMP_FAILURE;
            }

            /* check for valid datasource */

            if (CfaGetIfInfo (pHostCtlNode->u4DsmonHostDataSource, &IfInfo)
                == CFA_FAILURE)
            {
                /* ifindex is invalid */
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid interface Index \n");

                return SNMP_FAILURE;
            }

            if (((IfInfo.u1IfOperStatus != CFA_IF_UP) ||
                 (IfInfo.u1IfAdminStatus != CFA_IF_UP)) &&
                ((gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)))
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Interface OperStatus or Admin Status is in invalid state \n");

                return SNMP_FAILURE;
            }
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                DSMON_TRC (DSMON_FN_ENTRY,
                           "nmhSetDsmonHostCtlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));

            if (IfInfo.u1IfType == CFA_L3IPVLAN)
            {
                /* Get the VlanId from interface index */
                if (CfaGetVlanId
                    (pHostCtlNode->u4DsmonHostDataSource,
                     &u2VlanId) == CFA_FAILURE)
                {
                    DSMON_TRC1 (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Unable to Fetch VlanId of IfIndex %d\n", pHostCtlNode->u4DsmonHostDataSource);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }

                if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList) ==
                    L2IWF_FAILURE)
                {
                    DSMON_TRC1 (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Get Member Port List for VlanId %d failed \n", u2VlanId);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }
            }
            pHostCtlNode->u4DsmonHostCtlRowStatus = ACTIVE;

            pHostCtlNode->u4DsmonHostCtlCreateTime =
                (UINT4) OsixGetSysUpTime ();
#ifdef NPAPI_WANTED
            DsmonFsDSMONEnableProbe (pHostCtlNode->u4DsmonHostDataSource,
                                     u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            DsmonHostPopulateDataEntries (pHostCtlNode);

            DsmonHostTopNNotifyHostCtlChgs ((UINT4) i4DsmonHostCtlIndex,
                                            (UINT4) i4SetValDsmonHostCtlStatus);
            DsmonPktInfoDelUnusedEntries ();

            break;

        case NOT_IN_SERVICE:

            pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

            if (pHostCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Host Control Index \n");

                return SNMP_FAILURE;
            }

            pHostCtlNode->u4DsmonHostCtlRowStatus = NOT_IN_SERVICE;

            DsmonHostTopNNotifyHostCtlChgs ((UINT4) i4DsmonHostCtlIndex,
                                            (UINT4) i4SetValDsmonHostCtlStatus);
            /* check for valid datasource */
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                DSMON_TRC (DSMON_FN_ENTRY,
                           "nmhSetDsmonHostCtlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));

            if (CfaGetIfInfo (pHostCtlNode->u4DsmonHostDataSource, &IfInfo)
                != CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId (pHostCtlNode->u4DsmonHostDataSource,
                                   &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }
            DsmonHostDelDataEntries ((UINT4) i4DsmonHostCtlIndex);
#ifdef NPAPI_WANTED
            DsmonFsDSMONDisableProbe (pHostCtlNode->u4DsmonHostDataSource,
                                      u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            DsmonPktInfoDelUnusedEntries ();
            break;

        case DESTROY:

            pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

            if (pHostCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Host Control Index \n");

                return SNMP_FAILURE;
            }

            DsmonHostTopNNotifyHostCtlChgs ((UINT4) i4DsmonHostCtlIndex,
                                            (UINT4) i4SetValDsmonHostCtlStatus);

            i4Result = DsmonHostDelCtlEntry (pHostCtlNode);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Host Control Index \
                 RBTreeRemove failed - Host Control Table\n");

                return SNMP_FAILURE;
            }

            DsmonPktInfoDelUnusedEntries ();
            break;
        default:
            break;

    }
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DsmonHostCtlDataSource
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                testValDsmonHostCtlDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostCtlDataSource (UINT4 *pu4ErrorCode,
                                 INT4 i4DsmonHostCtlIndex,
                                 tSNMP_OID_TYPE *
                                 pTestValDsmonHostCtlDataSource)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonHostCtlDataSource \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostCtlIndex < DSMON_ONE) ||
        (i4DsmonHostCtlIndex > DSMON_MAX_CTL_INDEX))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (DSMON_TEST_DATA_SOURCE_OID (pTestValDsmonHostCtlDataSource) == FALSE)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_DEBUG_TRC, "Invalid DataSource");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: HostCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostCtlNode->u4DsmonHostCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostCtlAggProfile
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                testValDsmonHostCtlAggProfile
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostCtlAggProfile (UINT4 *pu4ErrorCode,
                                 INT4 i4DsmonHostCtlIndex,
                                 INT4 i4TestValDsmonHostCtlAggProfile)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;
    tDsmonAggCtl       *pAggCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonHostCtlAggProfile \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostCtlIndex < DSMON_ONE) ||
        (i4DsmonHostCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pAggCtlNode = DsmonAggCtlGetEntry (i4TestValDsmonHostCtlAggProfile);

    if (pAggCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: AggCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: HostCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostCtlNode->u4DsmonHostCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                testValDsmonHostCtlMaxDesiredEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostCtlMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                        INT4 i4DsmonHostCtlIndex,
                                        INT4
                                        i4TestValDsmonHostCtlMaxDesiredEntries)
{
    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonHostCtlMaxDesiredEntries \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostCtlIndex < DSMON_ONE) ||
        (i4DsmonHostCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValDsmonHostCtlMaxDesiredEntries < -1) ||
        (i4TestValDsmonHostCtlMaxDesiredEntries > DSMON_MAX_DATA_PER_CTL) ||
        (i4TestValDsmonHostCtlMaxDesiredEntries == DSMON_ZERO))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MaxDesiredEntries  out of range \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: HostCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostCtlNode->u4DsmonHostCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostCtlIPv4PrefixLen
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                testValDsmonHostCtlIPv4PrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostCtlIPv4PrefixLen (UINT4 *pu4ErrorCode,
                                    INT4 i4DsmonHostCtlIndex,
                                    INT4 i4TestValDsmonHostCtlIPv4PrefixLen)
{
    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonHostCtlIPv4PrefixLen \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostCtlIndex < DSMON_ONE) ||
        (i4DsmonHostCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValDsmonHostCtlIPv4PrefixLen < DSMON_IPV4PREFIXLEN_MIN) ||
        (i4TestValDsmonHostCtlIPv4PrefixLen > DSMON_IPV4PREFIXLEN_MAX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControl IPv4PrefixLen out of range \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: HostCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostCtlNode->u4DsmonHostCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostCtlIPv6PrefixLen
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                testValDsmonHostCtlIPv6PrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostCtlIPv6PrefixLen (UINT4 *pu4ErrorCode,
                                    INT4 i4DsmonHostCtlIndex,
                                    INT4 i4TestValDsmonHostCtlIPv6PrefixLen)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonHostCtlIPv6PrefixLen \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostCtlIndex < DSMON_ONE) ||
        (i4DsmonHostCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValDsmonHostCtlIPv6PrefixLen < DSMON_IPV6PREFIXLEN_MIN) ||
        (i4TestValDsmonHostCtlIPv6PrefixLen > DSMON_IPV6PREFIXLEN_MAX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControl IPv6PrefixLen out of range \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: HostCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostCtlNode->u4DsmonHostCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostCtlOwner
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                testValDsmonHostCtlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostCtlOwner (UINT4 *pu4ErrorCode, INT4 i4DsmonHostCtlIndex,
                            tSNMP_OCTET_STRING_TYPE * pTestValDsmonHostCtlOwner)
{

    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonHostCtlOwner \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonHostCtlIndex < DSMON_ONE) ||
        (i4DsmonHostCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((pTestValDsmonHostCtlOwner->i4_Length < DSMON_ZERO) ||
        (pTestValDsmonHostCtlOwner->i4_Length > DSMON_MAX_OWNER_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid Owner length \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;

    }

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: HostCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostCtlNode->u4DsmonHostCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostCtlStatus
 Input       :  The Indices
                DsmonHostCtlIndex

                The Object
                testValDsmonHostCtlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostCtlStatus (UINT4 *pu4ErrorCode,
                             INT4 i4DsmonHostCtlIndex,
                             INT4 i4TestValDsmonHostCtlStatus)
{
    tDsmonHostCtl      *pHostCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonHostCtlStatus \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonHostCtlIndex < DSMON_ONE) ||
        (i4DsmonHostCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pHostCtlNode = DsmonHostGetCtlEntry (i4DsmonHostCtlIndex);

    if (pHostCtlNode == NULL)
    {
        if (i4TestValDsmonHostCtlStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValDsmonHostCtlStatus == ACTIVE) ||
            (i4TestValDsmonHostCtlStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonHostCtlStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DsmonHostCtlTable
 Input       :  The Indices
                DsmonHostCtlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DsmonHostCtlTable (UINT4 *pu4ErrorCode,
                           tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonHostTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonHostTable
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonHostTable (INT4 i4DsmonHostCtlIndex,
                                        UINT4 u4DsmonHostTimeMark,
                                        INT4 i4DsmonAggGroupIndex,
                                        INT4 i4ProtocolDirLocalIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pDsmonHostAddress)
{
    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonHostTable \n");

    UNUSED_PARAM (u4DsmonHostTimeMark);

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }
    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode = DsmonHostGetDataEntry (i4DsmonHostCtlIndex,
                                            i4DsmonAggGroupIndex,
                                            i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/***************************************************************************
 Function    :  nmhGetFirstIndexDsmonHostTable
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonHostTable (INT4 *pi4DsmonHostCtlIndex,
                                UINT4 *pu4DsmonHostTimeMark,
                                INT4 *pi4DsmonAggGroupIndex,
                                INT4 *pi4ProtocolDirLocalIndex,
                                tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress)
{

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetFirstIndexDsmonHostTable \n");

    return nmhGetNextIndexDsmonHostTable (DSMON_ZERO, pi4DsmonHostCtlIndex,
                                          DSMON_ZERO, pu4DsmonHostTimeMark,
                                          DSMON_ZERO, pi4DsmonAggGroupIndex,
                                          DSMON_ZERO, pi4ProtocolDirLocalIndex,
                                          DSMON_ZERO, pDsmonHostAddress);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonHostTable
 Input       :  The Indices
                DsmonHostCtlIndex
                nextDsmonHostCtlIndex
                DsmonHostTimeMark
                nextDsmonHostTimeMark
                DsmonAggGroupIndex
                nextDsmonAggGroupIndex
                ProtocolDirLocalIndex
                nextProtocolDirLocalIndex
                DsmonHostAddress
                nextDsmonHostAddress
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonHostTable (INT4 i4DsmonHostCtlIndex,
                               INT4 *pi4NextDsmonHostCtlIndex,
                               UINT4 u4DsmonHostTimeMark,
                               UINT4 *pu4NextDsmonHostTimeMark,
                               INT4 i4DsmonAggGroupIndex,
                               INT4 *pi4NextDsmonAggGroupIndex,
                               INT4 i4ProtocolDirLocalIndex,
                               INT4 *pi4NextProtocolDirLocalIndex,
                               tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                               tSNMP_OCTET_STRING_TYPE * pNextDsmonHostAddress)
{
    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr, IpAddress;

    UNUSED_PARAM (u4DsmonHostTimeMark);

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetNextIndexDsmonHostTable \n");

    if (pDsmonHostAddress == NULL)
    {
        MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
    }
    else
    {
        MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));
        if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
        {
            MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                    DSMON_IPV6_MAX_LEN);
        }
        else
        {
            MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                    DSMON_IPV4_MAX_LEN);
        }
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode = DsmonHostGetNextDataIndex (i4DsmonHostCtlIndex,
                                                i4DsmonAggGroupIndex,
                                                i4ProtocolDirLocalIndex,
                                                &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonHostCtlIndex = pHostStatsNode->u4DsmonHostCtlIndex;

    *pu4NextDsmonHostTimeMark = DSMON_ZERO;

    *pi4NextDsmonAggGroupIndex = pHostStatsNode->u4DsmonHostAggGroupIndex;

    *pi4NextProtocolDirLocalIndex =
        pHostStatsNode->u4DsmonHostProtDirLocalIndex;

    pNextDsmonHostAddress->i4_Length = pHostStatsNode->u4DsmonHostIpAddrLen;

    IpAddress.u4_addr[0] =
        OSIX_HTONL (pHostStatsNode->DsmonHostNLAddress.u4_addr[0]);
    IpAddress.u4_addr[1] =
        OSIX_HTONL (pHostStatsNode->DsmonHostNLAddress.u4_addr[1]);
    IpAddress.u4_addr[2] =
        OSIX_HTONL (pHostStatsNode->DsmonHostNLAddress.u4_addr[2]);
    IpAddress.u4_addr[3] =
        OSIX_HTONL (pHostStatsNode->DsmonHostNLAddress.u4_addr[3]);

    if (pHostStatsNode->u4DsmonHostIpAddrLen == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (pNextDsmonHostAddress->pu1_OctetList,
                &IpAddress, DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pNextDsmonHostAddress->pu1_OctetList,
                &IpAddress.u4_addr[3], DSMON_IPV4_MAX_LEN);
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonHostInPkts
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostInPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostInPkts (INT4 i4DsmonHostCtlIndex, UINT4 u4DsmonHostTimeMark,
                       INT4 i4DsmonAggGroupIndex, INT4 i4ProtocolDirLocalIndex,
                       tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                       UINT4 *pu4RetValDsmonHostInPkts)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostInPkts \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostInPkts =
        pHostStatsNode->DsmonCurSample.u4DsmonHostInPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostInOctets
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostInOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostInOctets (INT4 i4DsmonHostCtlIndex,
                         UINT4 u4DsmonHostTimeMark, INT4 i4DsmonAggGroupIndex,
                         INT4 i4ProtocolDirLocalIndex,
                         tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                         UINT4 *pu4RetValDsmonHostInOctets)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostInOctets \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostInOctets =
        pHostStatsNode->DsmonCurSample.u4DsmonHostInOctets;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostInOvflPkts
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostInOvflPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostInOvflPkts (INT4 i4DsmonHostCtlIndex,
                           UINT4 u4DsmonHostTimeMark, INT4 i4DsmonAggGroupIndex,
                           INT4 i4ProtocolDirLocalIndex,
                           tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                           UINT4 *pu4RetValDsmonHostInOvflPkts)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonHostCtlIndex);
    UNUSED_PARAM (u4DsmonHostTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4ProtocolDirLocalIndex);
    UNUSED_PARAM (pDsmonHostAddress);
    UNUSED_PARAM (pu4RetValDsmonHostInOvflPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostInOvflOctets
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostInOvflOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostInOvflOctets (INT4 i4DsmonHostCtlIndex,
                             UINT4 u4DsmonHostTimeMark,
                             INT4 i4DsmonAggGroupIndex,
                             INT4 i4ProtocolDirLocalIndex,
                             tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                             UINT4 *pu4RetValDsmonHostInOvflOctets)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonHostCtlIndex);
    UNUSED_PARAM (u4DsmonHostTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4ProtocolDirLocalIndex);
    UNUSED_PARAM (pDsmonHostAddress);
    UNUSED_PARAM (pu4RetValDsmonHostInOvflOctets);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostInHCPkts
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostInHCPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostInHCPkts (INT4 i4DsmonHostCtlIndex, UINT4 u4DsmonHostTimeMark,
                         INT4 i4DsmonAggGroupIndex,
                         INT4 i4ProtocolDirLocalIndex,
                         tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                         tSNMP_COUNTER64_TYPE * pu8RetValDsmonHostInHCPkts)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostInHCPkts \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonHostInHCPkts->msn =
        pHostStatsNode->DsmonCurSample.u8DsmonHostInHCPkts.u4HiWord;

    pu8RetValDsmonHostInHCPkts->lsn =
        pHostStatsNode->DsmonCurSample.u8DsmonHostInHCPkts.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostInHCOctets
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostInHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostInHCOctets (INT4 i4DsmonHostCtlIndex,
                           UINT4 u4DsmonHostTimeMark, INT4 i4DsmonAggGroupIndex,
                           INT4 i4ProtocolDirLocalIndex,
                           tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                           tSNMP_COUNTER64_TYPE * pu8RetValDsmonHostInHCOctets)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostInHCOctets \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonHostInHCOctets->msn =
        pHostStatsNode->DsmonCurSample.u8DsmonHostInHCOctets.u4HiWord;

    pu8RetValDsmonHostInHCOctets->lsn =
        pHostStatsNode->DsmonCurSample.u8DsmonHostInHCOctets.u4LoWord;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostOutPkts
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostOutPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostOutPkts (INT4 i4DsmonHostCtlIndex,
                        UINT4 u4DsmonHostTimeMark, INT4 i4DsmonAggGroupIndex,
                        INT4 i4ProtocolDirLocalIndex,
                        tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                        UINT4 *pu4RetValDsmonHostOutPkts)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostOutPkts \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostOutPkts =
        pHostStatsNode->DsmonCurSample.u4DsmonHostOutPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostOutOctets
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostOutOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostOutOctets (INT4 i4DsmonHostCtlIndex,
                          UINT4 u4DsmonHostTimeMark, INT4 i4DsmonAggGroupIndex,
                          INT4 i4ProtocolDirLocalIndex,
                          tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                          UINT4 *pu4RetValDsmonHostOutOctets)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostOutOctets \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostOutOctets =
        pHostStatsNode->DsmonCurSample.u4DsmonHostOutOctets;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostOutOvflPkts
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostOutOvflPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostOutOvflPkts (INT4 i4DsmonHostCtlIndex,
                            UINT4 u4DsmonHostTimeMark,
                            INT4 i4DsmonAggGroupIndex,
                            INT4 i4ProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                            UINT4 *pu4RetValDsmonHostOutOvflPkts)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonHostCtlIndex);
    UNUSED_PARAM (u4DsmonHostTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4ProtocolDirLocalIndex);
    UNUSED_PARAM (pDsmonHostAddress);
    UNUSED_PARAM (pu4RetValDsmonHostOutOvflPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostOutOvflOctets
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostOutOvflOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostOutOvflOctets (INT4 i4DsmonHostCtlIndex,
                              UINT4 u4DsmonHostTimeMark,
                              INT4 i4DsmonAggGroupIndex,
                              INT4 i4ProtocolDirLocalIndex,
                              tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                              UINT4 *pu4RetValDsmonHostOutOvflOctets)
{
    /* deprecated */

    UNUSED_PARAM (i4DsmonHostCtlIndex);
    UNUSED_PARAM (u4DsmonHostTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4ProtocolDirLocalIndex);
    UNUSED_PARAM (pDsmonHostAddress);
    UNUSED_PARAM (pu4RetValDsmonHostOutOvflOctets);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostOutHCPkts
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostOutHCPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostOutHCPkts (INT4 i4DsmonHostCtlIndex,
                          UINT4 u4DsmonHostTimeMark, INT4 i4DsmonAggGroupIndex,
                          INT4 i4ProtocolDirLocalIndex,
                          tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                          tSNMP_COUNTER64_TYPE * pu8RetValDsmonHostOutHCPkts)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostOutHCPkts \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonHostOutHCPkts->msn =
        pHostStatsNode->DsmonCurSample.u8DsmonHostOutHCPkts.u4HiWord;

    pu8RetValDsmonHostOutHCPkts->lsn =
        pHostStatsNode->DsmonCurSample.u8DsmonHostOutHCPkts.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostOutHCOctets
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostOutHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostOutHCOctets (INT4 i4DsmonHostCtlIndex,
                            UINT4 u4DsmonHostTimeMark,
                            INT4 i4DsmonAggGroupIndex,
                            INT4 i4ProtocolDirLocalIndex,
                            tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValDsmonHostOutHCOctets)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostOutHCOctets \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonHostOutHCOctets->msn =
        pHostStatsNode->DsmonCurSample.u8DsmonHostOutHCOctets.u4HiWord;

    pu8RetValDsmonHostOutHCOctets->lsn =
        pHostStatsNode->DsmonCurSample.u8DsmonHostOutHCOctets.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostCreateTime
 Input       :  The Indices
                DsmonHostCtlIndex
                DsmonHostTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
                DsmonHostAddress

                The Object
                retValDsmonHostCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostCreateTime (INT4 i4DsmonHostCtlIndex,
                           UINT4 u4DsmonHostTimeMark, INT4 i4DsmonAggGroupIndex,
                           INT4 i4ProtocolDirLocalIndex,
                           tSNMP_OCTET_STRING_TYPE * pDsmonHostAddress,
                           UINT4 *pu4RetValDsmonHostCreateTime)
{

    tDsmonHostStats    *pHostStatsNode = NULL;
    tIpAddr             HostAddr;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostCreateTime \n");

    MEMSET (&HostAddr, DSMON_INIT_VAL, sizeof (tIpAddr));

    if (pDsmonHostAddress->i4_Length == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (&HostAddr, pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (&HostAddr.u4_addr[3], pDsmonHostAddress->pu1_OctetList,
                DSMON_IPV4_MAX_LEN);
    }

    HostAddr.u4_addr[0] = OSIX_NTOHL (HostAddr.u4_addr[0]);
    HostAddr.u4_addr[1] = OSIX_NTOHL (HostAddr.u4_addr[1]);
    HostAddr.u4_addr[2] = OSIX_NTOHL (HostAddr.u4_addr[2]);
    HostAddr.u4_addr[3] = OSIX_NTOHL (HostAddr.u4_addr[3]);

    pHostStatsNode =
        DsmonHostGetDataEntry (i4DsmonHostCtlIndex, i4DsmonAggGroupIndex,
                               i4ProtocolDirLocalIndex, &HostAddr);

    if (pHostStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Stats Control Index or \
                                      Agg Group Index or Protocol Dir Local Index or \
                                      Host Address is invalid \n");

        return SNMP_FAILURE;
    }

    if (pHostStatsNode->u4DsmonHostTimeMark < u4DsmonHostTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostCreateTime = pHostStatsNode->u4DsmonHostCreateTime;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonHostTopNCtlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonHostTopNCtlTable
 Input       :  The Indices
                DsmonHostTopNCtlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonHostTopNCtlTable (INT4 i4DsmonHostTopNCtlIndex)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonHostTopNCtlTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonHostTopNCtlTable
 Input       :  The Indices
                DsmonHostTopNCtlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDsmonHostTopNCtlTable (INT4 *pi4DsmonHostTopNCtlIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetFirstIndexDsmonHostTopNCtlTable \n");

    return nmhGetNextIndexDsmonHostTopNCtlTable (DSMON_ZERO,
                                                 pi4DsmonHostTopNCtlIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonHostTopNCtlTable
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                nextDsmonHostTopNCtlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonHostTopNCtlTable (INT4 i4DsmonHostTopNCtlIndex,
                                      INT4 *pi4NextDsmonHostTopNCtlIndex)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetNextIndexDsmonHostTopNCtlTable \n");

    pHostTopNCtl = DsmonHostTopNGetNextCtlIndex (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonHostTopNCtlIndex =
        (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlIndex);

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlHostIndex
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlHostIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlHostIndex (INT4 i4DsmonHostTopNCtlIndex,
                                 INT4 *pi4RetValDsmonHostTopNCtlHostIndex)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNCtlHostIndex \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostTopNCtlHostIndex =
        (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlHostIndex);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlRateBase
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlRateBase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlRateBase (INT4 i4DsmonHostTopNCtlIndex,
                                INT4 *pi4RetValDsmonHostTopNCtlRateBase)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNCtlRateBase \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostTopNCtlRateBase =
        (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlRateBase);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlTimeRemaining
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlTimeRemaining (INT4 i4DsmonHostTopNCtlIndex,
                                     INT4
                                     *pi4RetValDsmonHostTopNCtlTimeRemaining)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonHostTopNCtlTimeRemaining \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
    {
        *pi4RetValDsmonHostTopNCtlTimeRemaining =
            (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlDuration);

        return SNMP_SUCCESS;

    }

    *pi4RetValDsmonHostTopNCtlTimeRemaining =
        (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlTimeRem);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlGeneratedReports
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlGeneratedReports
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlGeneratedReports (INT4 i4DsmonHostTopNCtlIndex,
                                        UINT4
                                        *pu4RetValDsmonHostTopNCtlGeneratedReports)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonHostTopNCtlGeneratedReports \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostTopNCtlGeneratedReports =
        pHostTopNCtl->u4DsmonHostTopNCtlGenReports;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlDuration
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlDuration (INT4 i4DsmonHostTopNCtlIndex,
                                INT4 *pi4RetValDsmonHostTopNCtlDuration)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNCtlDuration \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostTopNCtlDuration =
        (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlDuration);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlRequestedSize
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlRequestedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlRequestedSize (INT4 i4DsmonHostTopNCtlIndex,
                                     INT4
                                     *pi4RetValDsmonHostTopNCtlRequestedSize)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonHostTopNCtlRequestedSize \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostTopNCtlRequestedSize =
        (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlReqSize);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlGrantedSize
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlGrantedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlGrantedSize (INT4 i4DsmonHostTopNCtlIndex,
                                   INT4 *pi4RetValDsmonHostTopNCtlGrantedSize)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonHostTopNCtlGrantedSize \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostTopNCtlGrantedSize =
        (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlGranSize);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlStartTime
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlStartTime (INT4 i4DsmonHostTopNCtlIndex,
                                 UINT4 *pu4RetValDsmonHostTopNCtlStartTime)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNCtlStartTime \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostTopNCtlStartTime =
        pHostTopNCtl->u4DsmonHostTopNCtlStartTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlOwner
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlOwner (INT4 i4DsmonHostTopNCtlIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValDsmonHostTopNCtlOwner)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNCtlOwner \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDsmonHostTopNCtlOwner->pu1_OctetList,
            pHostTopNCtl->au1DsmonHostTopNCtlOwner, DSMON_MAX_OWNER_LEN);

    pRetValDsmonHostTopNCtlOwner->i4_Length =
        STRLEN (pHostTopNCtl->au1DsmonHostTopNCtlOwner);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNCtlStatus
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                retValDsmonHostTopNCtlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNCtlStatus (INT4 i4DsmonHostTopNCtlIndex,
                              INT4 *pi4RetValDsmonHostTopNCtlStatus)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNCtlStatus \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostTopNCtlStatus =
        (INT4) (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus);

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDsmonHostTopNCtlHostIndex
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                setValDsmonHostTopNCtlHostIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostTopNCtlHostIndex (INT4 i4DsmonHostTopNCtlIndex,
                                 INT4 i4SetValDsmonHostTopNCtlHostIndex)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostTopNCtlHostIndex \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pHostTopNCtl->u4DsmonHostTopNCtlHostIndex =
        (UINT4) (i4SetValDsmonHostTopNCtlHostIndex);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonHostTopNCtlRateBase
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                setValDsmonHostTopNCtlRateBase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostTopNCtlRateBase (INT4 i4DsmonHostTopNCtlIndex,
                                INT4 i4SetValDsmonHostTopNCtlRateBase)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostTopNCtlRateBase \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pHostTopNCtl->u4DsmonHostTopNCtlRateBase =
        (UINT4) (i4SetValDsmonHostTopNCtlRateBase);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonHostTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                setValDsmonHostTopNCtlTimeRemaining
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostTopNCtlTimeRemaining (INT4 i4DsmonHostTopNCtlIndex,
                                     INT4 i4SetValDsmonHostTopNCtlTimeRemaining)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonHostTopNCtlTimeRemaining \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pHostTopNCtl->u4DsmonHostTopNCtlTimeRem =
        (UINT4) (i4SetValDsmonHostTopNCtlTimeRemaining);

    pHostTopNCtl->u4DsmonHostTopNCtlDuration =
        (UINT4) (i4SetValDsmonHostTopNCtlTimeRemaining);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonHostTopNCtlRequestedSize
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                setValDsmonHostTopNCtlRequestedSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostTopNCtlRequestedSize (INT4 i4DsmonHostTopNCtlIndex,
                                     INT4 i4SetValDsmonHostTopNCtlRequestedSize)
{
    UINT4               u4Minimum = 0;

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonHostTopNCtlRequestedSize \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pHostTopNCtl->u4DsmonHostTopNCtlReqSize =
        (UINT4) (i4SetValDsmonHostTopNCtlRequestedSize);

    u4Minimum = (i4SetValDsmonHostTopNCtlRequestedSize < DSMON_MAX_TOPN_ENTRY) ?
        i4SetValDsmonHostTopNCtlRequestedSize : DSMON_MAX_TOPN_ENTRY;

    pHostTopNCtl->u4DsmonHostTopNCtlGranSize = u4Minimum;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonHostTopNCtlOwner
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                setValDsmonHostTopNCtlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostTopNCtlOwner (INT4 i4DsmonHostTopNCtlIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValDsmonHostTopNCtlOwner)
{

    UINT4               u4Minimum = 0;

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostTopNCtlOwner \n");

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    u4Minimum = (pSetValDsmonHostTopNCtlOwner->i4_Length <
                 DSMON_MAX_OWNER_LEN) ?
        pSetValDsmonHostTopNCtlOwner->i4_Length : DSMON_MAX_OWNER_LEN;

    MEMSET (pHostTopNCtl->au1DsmonHostTopNCtlOwner, DSMON_INIT_VAL,
            DSMON_MAX_OWNER_LEN);

    MEMCPY (pHostTopNCtl->au1DsmonHostTopNCtlOwner,
            pSetValDsmonHostTopNCtlOwner->pu1_OctetList, u4Minimum);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonHostTopNCtlStatus
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                setValDsmonHostTopNCtlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonHostTopNCtlStatus (INT4 i4DsmonHostTopNCtlIndex,
                              INT4 i4SetValDsmonHostTopNCtlStatus)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;
    tDsmonHostCtl      *pHostCtlNode = NULL;

    INT4                i4Result = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonHostTopNCtlStatus \n");

    switch (i4SetValDsmonHostTopNCtlStatus)
    {
        case CREATE_AND_WAIT:

            pHostTopNCtl = DsmonHostTopNAddCtlEntry (i4DsmonHostTopNCtlIndex);

            if (pHostTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                           " Memory Allocation / \
                 RBTree Add Failed - Host TopN Control Table \n");

                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:

            pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

            if (pHostTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                           Invalid Host TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            pHostCtlNode =
                DsmonHostGetCtlEntry (pHostTopNCtl->
                                      u4DsmonHostTopNCtlHostIndex);

            if (pHostCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Host Control Index \n");

                return SNMP_FAILURE;
            }

            if (pHostCtlNode->u4DsmonHostCtlRowStatus != ACTIVE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Host Control entry is not in active state \n");

                return SNMP_FAILURE;
            }

            pHostTopNCtl->u4DsmonHostTopNCtlRowStatus = ACTIVE;

            pHostTopNCtl->u4DsmonHostTopNCtlTimeRem =
                pHostTopNCtl->u4DsmonHostTopNCtlDuration;

            pHostTopNCtl->u4DsmonHostTopNCtlStartTime =
                (UINT4) OsixGetSysUpTime ();

            break;

        case NOT_IN_SERVICE:

            pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

            if (pHostTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                           Invalid Host TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            pHostTopNCtl->u4DsmonHostTopNCtlRowStatus = NOT_IN_SERVICE;

            DsmonHostTopNDelTopNReport ((UINT4) i4DsmonHostTopNCtlIndex);

            break;

        case DESTROY:

            pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

            if (pHostTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                           Invalid Host TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            i4Result = DsmonHostTopNDelCtlEntry (pHostTopNCtl);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Host TopN Control Index \
                 RBTreeRemove failed - Host TopN  Control Table\n");

                return SNMP_FAILURE;
            }

            break;
        default:
            break;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DsmonHostTopNCtlHostIndex

 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                testValDsmonHostTopNCtlHostIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostTopNCtlHostIndex (UINT4 *pu4ErrorCode,
                                    INT4 i4DsmonHostTopNCtlIndex,
                                    INT4 i4TestValDsmonHostTopNCtlHostIndex)
{
    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonHostTopNCtlHostIndex \n");

    if ((i4DsmonHostTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonHostTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceDsmonHostCtlTable
        (i4TestValDsmonHostTopNCtlHostIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:HostTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostTopNCtlRateBase
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                testValDsmonHostTopNCtlRateBase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostTopNCtlRateBase (UINT4 *pu4ErrorCode,
                                   INT4 i4DsmonHostTopNCtlIndex,
                                   INT4 i4TestValDsmonHostTopNCtlRateBase)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonHostTopNCtlRateBase \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonHostTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonHostTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValDsmonHostTopNCtlRateBase < hostTopNInPkts) ||
        (i4TestValDsmonHostTopNCtlRateBase > hostTopNTotalHCOctets))
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RateBase \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:HostTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                testValDsmonHostTopNCtlTimeRemaining
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostTopNCtlTimeRemaining (UINT4 *pu4ErrorCode,
                                        INT4 i4DsmonHostTopNCtlIndex,
                                        INT4
                                        i4TestValDsmonHostTopNCtlTimeRemaining)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonHostTopNCtlTimeRemaining \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonHostTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (i4TestValDsmonHostTopNCtlTimeRemaining < DSMON_ZERO)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: TimeRem is less than Zero \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:HostTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostTopNCtlRequestedSize
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                testValDsmonHostTopNCtlRequestedSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostTopNCtlRequestedSize (UINT4 *pu4ErrorCode,
                                        INT4 i4DsmonHostTopNCtlIndex,
                                        INT4
                                        i4TestValDsmonHostTopNCtlRequestedSize)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonHostTopNCtlRequestedSize \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonHostTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (i4TestValDsmonHostTopNCtlRequestedSize <= DSMON_ZERO)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid value \
        Requested size is less than equal to zero \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:HostTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostTopNCtlOwner
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                testValDsmonHostTopNCtlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostTopNCtlOwner (UINT4 *pu4ErrorCode,
                                INT4 i4DsmonHostTopNCtlIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValDsmonHostTopNCtlOwner)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonHostTopNCtlOwner \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonHostTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((pTestValDsmonHostTopNCtlOwner->i4_Length < DSMON_ZERO) ||
        (pTestValDsmonHostTopNCtlOwner->i4_Length > DSMON_MAX_OWNER_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid Owner length \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        return SNMP_FAILURE;
    }

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:HostTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pHostTopNCtl->u4DsmonHostTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonHostTopNCtlStatus
 Input       :  The Indices
                DsmonHostTopNCtlIndex

                The Object
                testValDsmonHostTopNCtlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonHostTopNCtlStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4DsmonHostTopNCtlIndex,
                                 INT4 i4TestValDsmonHostTopNCtlStatus)
{

    tDsmonHostTopNCtl  *pHostTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonHostTopNCtlStatus \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonHostTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonHostTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: HostTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pHostTopNCtl = DsmonHostTopNGetCtlEntry (i4DsmonHostTopNCtlIndex);

    if (pHostTopNCtl == NULL)
    {
        if (i4TestValDsmonHostTopNCtlStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValDsmonHostTopNCtlStatus == ACTIVE) ||
            (i4TestValDsmonHostTopNCtlStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonHostTopNCtlStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DsmonHostTopNCtlTable
 Input       :  The Indices
                DsmonHostTopNCtlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DsmonHostTopNCtlTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonHostTopNTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonHostTopNTable
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                DsmonHostTopNIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonHostTopNTable (INT4 i4DsmonHostTopNCtlIndex,
                                            INT4 i4DsmonHostTopNIndex)
{

    tDsmonHostTopN     *pHostTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonHostTopNTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pHostTopN = DsmonHostTopNGetTopNEntry (i4DsmonHostTopNCtlIndex,
                                           i4DsmonHostTopNIndex);

    if (pHostTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN Control Index or Host TopN Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonHostTopNTable
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                DsmonHostTopNIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonHostTopNTable (INT4 *pi4DsmonHostTopNCtlIndex,
                                    INT4 *pi4DsmonHostTopNIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
              nmhGetFirstIndexDsmonHostTopNTable \n");

    return nmhGetNextIndexDsmonHostTopNTable (0, pi4DsmonHostTopNCtlIndex,
                                              0, pi4DsmonHostTopNIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonHostTopNTable
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                nextDsmonHostTopNCtlIndex
                DsmonHostTopNIndex
                nextDsmonHostTopNIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonHostTopNTable (INT4 i4DsmonHostTopNCtlIndex,
                                   INT4 *pi4NextDsmonHostTopNCtlIndex,
                                   INT4 i4DsmonHostTopNIndex,
                                   INT4 *pi4NextDsmonHostTopNIndex)
{

    tDsmonHostTopN     *pHostTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetNextIndexDsmonHostTopNTable \n");

    pHostTopN = DsmonHostTopNGetNextTopNIndex (i4DsmonHostTopNCtlIndex,
                                               i4DsmonHostTopNIndex);

    if (pHostTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   No valid next index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonHostTopNCtlIndex = pHostTopN->u4DsmonHostTopNCtlIndex;
    *pi4NextDsmonHostTopNIndex = pHostTopN->u4DsmonHostTopNIndex;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDsmonHostTopNPDLocalIndex
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                DsmonHostTopNIndex

                The Object
                retValDsmonHostTopNPDLocalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNPDLocalIndex (INT4 i4DsmonHostTopNCtlIndex,
                                 INT4 i4DsmonHostTopNIndex,
                                 INT4 *pi4RetValDsmonHostTopNPDLocalIndex)
{
    tDsmonHostTopN     *pHostTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNPDLocalIndex \n");

    pHostTopN = DsmonHostTopNGetTopNEntry (i4DsmonHostTopNCtlIndex,
                                           i4DsmonHostTopNIndex);

    if (pHostTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN Control Index or Host TopN Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostTopNPDLocalIndex =
        (INT4) (pHostTopN->u4DsmonHostTopNPDLocalIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNAddress
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                DsmonHostTopNIndex

                The Object
                retValDsmonHostTopNAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNAddress (INT4 i4DsmonHostTopNCtlIndex,
                            INT4 i4DsmonHostTopNIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValDsmonHostTopNAddress)
{

    tDsmonHostTopN     *pHostTopN = NULL;
    tIpAddr             HostTopNAddress;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
              nmhGetDsmonHostTopNAddress \n");

    pHostTopN = DsmonHostTopNGetTopNEntry (i4DsmonHostTopNCtlIndex,
                                           i4DsmonHostTopNIndex);

    if (pHostTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN Control Index or Host TopN Index \n");

        return SNMP_FAILURE;
    }

    pRetValDsmonHostTopNAddress->i4_Length = pHostTopN->u4DsmonHostIpAddrLen;

    HostTopNAddress.u4_addr[0] =
        OSIX_NTOHL (pHostTopN->DsmonHostNLAddress.u4_addr[0]);
    HostTopNAddress.u4_addr[1] =
        OSIX_NTOHL (pHostTopN->DsmonHostNLAddress.u4_addr[1]);
    HostTopNAddress.u4_addr[2] =
        OSIX_NTOHL (pHostTopN->DsmonHostNLAddress.u4_addr[2]);
    HostTopNAddress.u4_addr[3] =
        OSIX_NTOHL (pHostTopN->DsmonHostNLAddress.u4_addr[3]);

    if (pHostTopN->u4DsmonHostIpAddrLen == DSMON_IPV6_MAX_LEN)
    {
        MEMCPY (pRetValDsmonHostTopNAddress->pu1_OctetList,
                &HostTopNAddress, DSMON_IPV6_MAX_LEN);
    }
    else
    {
        MEMCPY (pRetValDsmonHostTopNAddress->pu1_OctetList,
                &HostTopNAddress.u4_addr[3], DSMON_IPV4_MAX_LEN);
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNAggGroup
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                DsmonHostTopNIndex

                The Object
                retValDsmonHostTopNAggGroup
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNAggGroup (INT4 i4DsmonHostTopNCtlIndex,
                             INT4 i4DsmonHostTopNIndex,
                             INT4 *pi4RetValDsmonHostTopNAggGroup)
{

    tDsmonHostTopN     *pHostTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNAggGroup \n");

    pHostTopN = DsmonHostTopNGetTopNEntry (i4DsmonHostTopNCtlIndex,
                                           i4DsmonHostTopNIndex);

    if (pHostTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN Control Index or Host TopN Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonHostTopNAggGroup = pHostTopN->u4DsmonHostTopNAggGroupIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNRate
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                DsmonHostTopNIndex

                The Object
                retValDsmonHostTopNRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNRate (INT4 i4DsmonHostTopNCtlIndex,
                         INT4 i4DsmonHostTopNIndex,
                         UINT4 *pu4RetValDsmonHostTopNRate)
{

    tDsmonHostTopN     *pHostTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNRate \n");

    pHostTopN = DsmonHostTopNGetTopNEntry (i4DsmonHostTopNCtlIndex,
                                           i4DsmonHostTopNIndex);

    if (pHostTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN Control Index or Host TopN Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonHostTopNRate = pHostTopN->u4DsmonHostTopNRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNRateOvfl
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                DsmonHostTopNIndex

                The Object
                retValDsmonHostTopNRateOvfl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNRateOvfl (INT4 i4DsmonHostTopNCtlIndex,
                             INT4 i4DsmonHostTopNIndex,
                             UINT4 *pu4RetValDsmonHostTopNRateOvfl)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonHostTopNCtlIndex);
    UNUSED_PARAM (i4DsmonHostTopNIndex);
    UNUSED_PARAM (*pu4RetValDsmonHostTopNRateOvfl);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonHostTopNHCRate
 Input       :  The Indices
                DsmonHostTopNCtlIndex
                DsmonHostTopNIndex

                The Object
                retValDsmonHostTopNHCRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonHostTopNHCRate (INT4 i4DsmonHostTopNCtlIndex,
                           INT4 i4DsmonHostTopNIndex,
                           tSNMP_COUNTER64_TYPE * pu8RetValDsmonHostTopNHCRate)
{

    tDsmonHostTopN     *pHostTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonHostTopNHCRate \n");

    pHostTopN = DsmonHostTopNGetTopNEntry (i4DsmonHostTopNCtlIndex,
                                           i4DsmonHostTopNIndex);

    if (pHostTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Host TopN Control Index or Host TopN Index \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonHostTopNHCRate->msn =
        pHostTopN->u8DsmonHostTopNHCRate.u4HiWord;

    pu8RetValDsmonHostTopNHCRate->lsn =
        pHostTopN->u8DsmonHostTopNHCRate.u4LoWord;

    return SNMP_SUCCESS;

}
