/********************************************************************
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved 
 *
 * Description: This file contains the configuration and low level  
 *         routines for Counter Aggregation Group             
 *
 *******************************************************************/
#include "dsmninc.h"
/* Low Level GET Routine for All Objects  */
/****************************************************************************
Function    :  nmhGetDsmonMaxAggGroups
Input       :  The Indices

        The Object
        retValDsmonMaxAggGroups
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonMaxAggGroups (INT4 *pi4RetValDsmonMaxAggGroups)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
              nmhGetDsmonMaxAggGroups \n");

    *pi4RetValDsmonMaxAggGroups = (INT4) gDsmonGlobals.u4DsmonMaxAggGroups;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetDsmonAggControlLocked
Input       :  The Indices

        The Object
        retValDsmonAggControlLocked
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggControlLocked (INT4 *pi4RetValDsmonAggControlLocked)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggControlLocked \n");

    *pi4RetValDsmonAggControlLocked = (INT4) gDsmonGlobals.u1DsmonAggCtlLock;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetDsmonAggControlChanges
Input       :  The Indices

        The Object
        retValDsmonAggControlChanges
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggControlChanges (UINT4 *pu4RetValDsmonAggControlChanges)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggControlChanges\n");

    *pu4RetValDsmonAggControlChanges = gDsmonGlobals.u4DsmonAggCtlChanges;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetDsmonAggControlLastChangeTime
Input       :  The Indices

        The Object
        retValDsmonAggControlLastChangeTime
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggControlLastChangeTime (UINT4
                                     *pu4RetValDsmonAggControlLastChangeTime)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggControlLastChangeTime\n");

    *pu4RetValDsmonAggControlLastChangeTime =
        gDsmonGlobals.u4DsmonAggCtlLastChgTime;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDsmonAggControlLocked
Input       :  The Indices

        The Object
        setValDsmonAggControlLocked
Output      :  The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDsmonAggControlLocked (INT4 i4SetValDsmonAggControlLocked)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetDsmonAggControlLocked\n");

    if (gDsmonGlobals.u1DsmonAggCtlLock ==
        (UINT1) i4SetValDsmonAggControlLocked)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhSetDsmonAggControlLocked: No lock changes\r\n");
        /* No Changes */
        return SNMP_SUCCESS;
    }

    /* Update all Control and Data entries,Once the Agg Control Lock Changes */

    DsmonAggCtlHandleLckChgs ((UINT1) i4SetValDsmonAggControlLocked);

    gDsmonGlobals.u1DsmonAggCtlLock = (UINT1) i4SetValDsmonAggControlLocked;
    gDsmonGlobals.u4DsmonAggCtlLastChgTime = (UINT4) OsixGetSysUpTime ();
    gDsmonGlobals.u4DsmonAggCtlChanges++;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2DsmonAggControlLocked
Input       :  The Indices

        The Object
        testValDsmonAggControlLocked
Output      :  The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2DsmonAggControlLocked (UINT4 *pu4ErrorCode,
                                INT4 i4TestValDsmonAggControlLocked)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2DsmonAggControlLocked \n");

    if ((i4TestValDsmonAggControlLocked != DSMON_AGGLOCK_TRUE) &&
        (i4TestValDsmonAggControlLocked != DSMON_AGGLOCK_FALSE))
    {
        DSMON_TRC (MGMT_TRC, "The Value Should be Either 1 or 2 \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhTestv2DsmonAggControlLocked: Valid Agg. Ctl. Lock\r\n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2DsmonAggControlLocked
Output      :  The Dependency Low Lev Routine Take the Indices &
        check whether dependency is met or not.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2DsmonAggControlLocked (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonAggControlTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDsmonAggControlTable
Input       :  The Indices
        DsmonAggControlIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonAggControlTable (INT4 i4DsmonAggControlIndex)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhValidateIndexInstanceDsmonAggControlTable \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl != NULL)
    {
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhValidateIndexInstanceDsmonAggControlTable Invalid Index\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexDsmonAggControlTable
Input       :  The Indices
        DsmonAggControlIndex
Output      :  The Get First Routines gets the Lexicographicaly
        First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonAggControlTable (INT4 *pi4DsmonAggControlIndex)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetFirstIndexDsmonAggControlTable \n");

    return nmhGetNextIndexDsmonAggControlTable (0, pi4DsmonAggControlIndex);
}

/****************************************************************************
Function    :  nmhGetNextIndexDsmonAggControlTable
Input       :  The Indices
        DsmonAggControlIndex
        nextDsmonAggControlIndex
Output      :  The Get Next function gets the Next Index for
        the Index Value given in the Index Values. The
        Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonAggControlTable (INT4 i4DsmonAggControlIndex,
                                     INT4 *pi4NextDsmonAggControlIndex)
{
    tDsmonAggCtl       *pNextAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetNextIndexDsmonAggControlTable \n");

    pNextAggCtl = DsmonAggCtlGetNextIndex ((UINT4) i4DsmonAggControlIndex);
    if (pNextAggCtl != NULL)
    {
        *pi4NextDsmonAggControlIndex = (INT4) pNextAggCtl->u4DsmonAggCtlIndex;
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhGetNextIndexDsmonAggControlTable Entry not found\r\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDsmonAggControlDescr
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        retValDsmonAggControlDescr
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggControlDescr (INT4 i4DsmonAggControlIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValDsmonAggControlDescr)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggControlDescr \n");

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl != NULL)
    {
        pRetValDsmonAggControlDescr->i4_Length =
            STRLEN (pAggCtl->au1DsmonAggCtlDesc);
        MEMCPY (pRetValDsmonAggControlDescr->pu1_OctetList,
                pAggCtl->au1DsmonAggCtlDesc,
                pRetValDsmonAggControlDescr->i4_Length);
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhGetDsmonAggControlDescr Entry not found\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDsmonAggControlOwner
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        retValDsmonAggControlOwner
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggControlOwner (INT4 i4DsmonAggControlIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValDsmonAggControlOwner)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggControlOwner \n");

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl != NULL)
    {
        pRetValDsmonAggControlOwner->i4_Length =
            STRLEN (pAggCtl->au1DsmonAggCtlOwner);
        MEMCPY (pRetValDsmonAggControlOwner->pu1_OctetList,
                pAggCtl->au1DsmonAggCtlOwner,
                pRetValDsmonAggControlOwner->i4_Length);
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhGetDsmonAggControlOwner Entry not found\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDsmonAggControlStatus
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        retValDsmonAggControlStatus
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggControlStatus (INT4 i4DsmonAggControlIndex,
                             INT4 *pi4RetValDsmonAggControlStatus)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggControlStatus \n");

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl != NULL)
    {
        *pi4RetValDsmonAggControlStatus =
            (UINT4) pAggCtl->u4DsmonAggCtlRowStatus;
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhGetDsmonAggControlStatus Entry not found\r\n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDsmonAggControlDescr
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        setValDsmonAggControlDescr
Output      :  The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDsmonAggControlDescr (INT4 i4DsmonAggControlIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValDsmonAggControlDescr)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetDsmonAggControlDescr \n");

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);

    if (pAggCtl != NULL)
    {
        MEMCPY (pAggCtl->au1DsmonAggCtlDesc,
                pSetValDsmonAggControlDescr->pu1_OctetList,
                pSetValDsmonAggControlDescr->i4_Length);
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhSetDsmonAggControlDescr Entry not found\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetDsmonAggControlOwner
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        setValDsmonAggControlOwner
Output      :  The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDsmonAggControlOwner (INT4 i4DsmonAggControlIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pSetValDsmonAggControlOwner)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetDsmonAggControlOwner \n");

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl != NULL)
    {
        MEMCPY (pAggCtl->au1DsmonAggCtlOwner,
                pSetValDsmonAggControlOwner->pu1_OctetList,
                pSetValDsmonAggControlOwner->i4_Length);
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhSetDsmonAggControlOwner Entry not found\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetDsmonAggControlStatus
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        setValDsmonAggControlStatus
Output      :  The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDsmonAggControlStatus (INT4 i4DsmonAggControlIndex,
                             INT4 i4SetValDsmonAggControlStatus)
{
    tDsmonAggCtl       *pAggCtl = NULL;
    INT4                i4Result = OSIX_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetDsmonAggControlStatus \n");

    switch (i4SetValDsmonAggControlStatus)
    {
        case CREATE_AND_WAIT:
            pAggCtl = DsmonAggCtlAddEntry ((UINT4) i4DsmonAggControlIndex);
            if (pAggCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggControlStatus Entry not found\r\n");
                return SNMP_FAILURE;
            }
            pAggCtl->u4DsmonAggCtlRowStatus = NOT_READY;
            break;

        case ACTIVE:
            pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
            if (pAggCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggControlStatus Entry not found\r\n");
                return SNMP_FAILURE;
            }
            pAggCtl->u4DsmonAggCtlRowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
            if (pAggCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggControlStatus Entry not found\r\n");
                return SNMP_FAILURE;
            }
            pAggCtl->u4DsmonAggCtlRowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
            if (pAggCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggControlStatus Entry not found\r\n");
                return SNMP_FAILURE;
            }
            i4Result = DsmonAggCtlDelEntry (pAggCtl);
            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggControlStatus Entry deletion failed\r\n");
                return SNMP_FAILURE;
            }
            break;
        default:
            break;
    }
    DSMON_TRC (DSMON_FN_EXIT, "Exiting from the function, \
               nmhSetDsmonAggControlStatus \n");
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2DsmonAggControlDescr
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        testValDsmonAggControlDescr
Output      :  The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2DsmonAggControlDescr (UINT4 *pu4ErrorCode,
                               INT4 i4DsmonAggControlIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValDsmonAggControlDescr)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2DsmonAggControlDescr \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if (gDsmonGlobals.u1DsmonAggCtlLock == DSMON_AGGLOCK_TRUE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggControlDescr Agg. Ctl. is locked\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValDsmonAggControlDescr->i4_Length < 0) ||
        (pTestValDsmonAggControlDescr->i4_Length > DSMON_MAX_DESC_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggControlDescr Invalid Desc. Length\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggControlDescr Entry not found\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pAggCtl->u4DsmonAggCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggControlDescr RowStatus is in ACTIVE\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2DsmonAggControlOwner
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        testValDsmonAggControlOwner
Output      :  The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2DsmonAggControlOwner (UINT4 *pu4ErrorCode,
                               INT4 i4DsmonAggControlIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pTestValDsmonAggControlOwner)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2DsmonAggControlOwner \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((pTestValDsmonAggControlOwner->i4_Length < 0) ||
        (pTestValDsmonAggControlOwner->i4_Length > DSMON_MAX_OWNER_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggControlOwner Invalid Length\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggControlOwner Entry not found\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pAggCtl->u4DsmonAggCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggControlOwner RowStatus is in ACTIVE\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2DsmonAggControlStatus
Input       :  The Indices
        DsmonAggControlIndex

        The Object
        testValDsmonAggControlStatus
Output      :  The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2DsmonAggControlStatus (UINT4 *pu4ErrorCode,
                                INT4 i4DsmonAggControlIndex,
                                INT4 i4TestValDsmonAggControlStatus)
{
    INT1                i1Flag = SNMP_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2DsmonAggControlStatus \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if (gDsmonGlobals.u1DsmonAggCtlLock == DSMON_AGGLOCK_TRUE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggControlStatus Agg.Ctl. is Locked\r\n");
        return SNMP_FAILURE;
    }
    i1Flag = nmhValidateIndexInstanceDsmonAggControlTable
        (i4DsmonAggControlIndex);
    if (i1Flag == SNMP_SUCCESS)
    {
        if ((i4TestValDsmonAggControlStatus == ACTIVE) ||
            (i4TestValDsmonAggControlStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonAggControlStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValDsmonAggControlStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhTestv2DsmonAggControlStatus Invalid Status Value\r\n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2DsmonAggControlTable
Input       :  The Indices
        DsmonAggControlIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
        check whether dependency is met or not.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2DsmonAggControlTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonAggProfileTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDsmonAggProfileTable
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggProfileDSCP
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonAggProfileTable (INT4 i4DsmonAggControlIndex,
                                              INT4 i4DsmonAggProfileDSCP)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhValidateIndexInstanceDsmonAggProfileTable \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl != NULL)
    {
        if ((i4DsmonAggProfileDSCP >= 0) ||
            (i4DsmonAggProfileDSCP < DSMON_MAX_AGG_PROFILE_DSCP))
        {
            return SNMP_SUCCESS;
        }
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhValidateIndexInstanceDsmonAggProfileTable Invalid Index\r\n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexDsmonAggProfileTable
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggProfileDSCP
Output      :  The Get First Routines gets the Lexicographicaly
        First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonAggProfileTable (INT4 *pi4DsmonAggControlIndex,
                                      INT4 *pi4DsmonAggProfileDSCP)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetFirstIndexDsmonAggProfileTable \n");

    return nmhGetNextIndexDsmonAggProfileTable (0, pi4DsmonAggControlIndex,
                                                0, pi4DsmonAggProfileDSCP);
}

/****************************************************************************
Function    :  nmhGetNextIndexDsmonAggProfileTable
Input       :  The Indices
        DsmonAggControlIndex
        nextDsmonAggControlIndex
        DsmonAggProfileDSCP
        nextDsmonAggProfileDSCP
Output      :  The Get Next function gets the Next Index for
        the Index Value given in the Index Values. The
        Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonAggProfileTable (INT4 i4DsmonAggControlIndex,
                                     INT4 *pi4NextDsmonAggControlIndex,
                                     INT4 i4DsmonAggProfileDSCP,
                                     INT4 *pi4NextDsmonAggProfileDSCP)
{
    tDsmonAggCtl       *pAggCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetNextIndexDsmonAggProfileTable \n");

    if (i4DsmonAggProfileDSCP == (DSMON_MAX_AGG_PROFILE_DSCP - 1))
    {
        pAggCtl = DsmonAggCtlGetNextIndex ((UINT4) i4DsmonAggControlIndex);
        if (pAggCtl != NULL)
        {
            *pi4NextDsmonAggControlIndex = pAggCtl->u4DsmonAggCtlIndex;
            *pi4NextDsmonAggProfileDSCP = 0;    /* DSCP - 0 */
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4DsmonAggControlIndex == 0)
        {
            pAggCtl = DsmonAggCtlGetNextIndex ((UINT4) i4DsmonAggControlIndex);
        }
        else
        {
            pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
        }
        if (pAggCtl != NULL)
        {
            *pi4NextDsmonAggControlIndex = pAggCtl->u4DsmonAggCtlIndex;
            if (i4DsmonAggControlIndex == 0)
            {
                *pi4NextDsmonAggProfileDSCP = DSMON_ZERO;
            }
            else
            {
                *pi4NextDsmonAggProfileDSCP = ++i4DsmonAggProfileDSCP;
            }

            return SNMP_SUCCESS;
        }
    }
    DSMON_TRC (DSMON_DEBUG_TRC,
               "nmhGetNextIndexDsmonAggProfileTable GetNext Failed\r\n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDsmonAggGroupIndex
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggProfileDSCP

        The Object
        retValDsmonAggGroupIndex
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggGroupIndex (INT4 i4DsmonAggControlIndex,
                          INT4 i4DsmonAggProfileDSCP,
                          INT4 *pi4RetValDsmonAggGroupIndex)
{
    INT4                i4RetVal = OSIX_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggGroupIndex \n");

    i4RetVal = DsmonAggCtlGetGrpToCtlIdxAndDscp ((UINT4) i4DsmonAggControlIndex,
                                                 (UINT4) i4DsmonAggProfileDSCP,
                                                 (UINT4 *)
                                                 pi4RetValDsmonAggGroupIndex);

    if (i4RetVal == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_FN_EXIT, "Exiting from the function, \
               nmhGetDsmonAggGroupIndex \n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDsmonAggGroupIndex
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggProfileDSCP

        The Object
        setValDsmonAggGroupIndex
Output      :  The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDsmonAggGroupIndex (INT4 i4DsmonAggControlIndex,
                          INT4 i4DsmonAggProfileDSCP,
                          INT4 i4SetValDsmonAggGroupIndex)
{
    INT4                i4RetVal = OSIX_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetDsmonAggGroupIndex \n");

    i4RetVal = DsmonAggCtlSetGrpToCtlIdxAndDscp ((UINT4) i4DsmonAggControlIndex,
                                                 (UINT4) i4DsmonAggProfileDSCP,
                                                 (UINT4)
                                                 i4SetValDsmonAggGroupIndex);

    if (i4RetVal == OSIX_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_FN_EXIT, "Enxting from the function, \
               nmhSetDsmonAggGroupIndex \n");
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2DsmonAggGroupIndex
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggProfileDSCP

        The Object
        testValDsmonAggGroupIndex
Output      :  The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2DsmonAggGroupIndex (UINT4 *pu4ErrorCode,
                             INT4 i4DsmonAggControlIndex,
                             INT4 i4DsmonAggProfileDSCP,
                             INT4 i4TestValDsmonAggGroupIndex)
{
    tDsmonAggCtl       *pAggCtl = NULL;
    tDsmonAggGroup     *pAggGrp = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2DsmonAggGroupIndex \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if (gDsmonGlobals.u1DsmonAggCtlLock == DSMON_AGGLOCK_TRUE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggGroupIndex Agg. Ctl. is locked\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pAggCtl = DsmonAggCtlGetEntry ((UINT4) i4DsmonAggControlIndex);
    if (pAggCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggGroupIndex Ctl. Entry not found\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4DsmonAggProfileDSCP < 0) ||
        (i4DsmonAggProfileDSCP >= DSMON_MAX_AGG_PROFILE_DSCP))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggGroupIndex DSCP value should be 0-63\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                      (UINT4) i4TestValDsmonAggGroupIndex);
    if (pAggGrp == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggGroupIndex Grp. Entry not found\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2DsmonAggProfileTable
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggProfileDSCP
Output      :  The Dependency Low Lev Routine Take the Indices &
        check whether dependency is met or not.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2DsmonAggProfileTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonAggGroupTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceDsmonAggGroupTable
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonAggGroupTable (INT4 i4DsmonAggControlIndex,
                                            INT4 i4DsmonAggGroupIndex)
{
    tDsmonAggGroup     *pAggGrp = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhValidateIndexInstanceDsmonAggGroupTable \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                      (UINT4) i4DsmonAggGroupIndex);
    if (pAggGrp == NULL)
    {
        return SNMP_FAILURE;
    }
    DSMON_TRC (DSMON_FN_EXIT, "Exiting from the function, \
               nmhValidateIndexInstanceDsmonAggGroupTable \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexDsmonAggGroupTable
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex
Output      :  The Get First Routines gets the Lexicographicaly
        First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonAggGroupTable (INT4 *pi4DsmonAggControlIndex,
                                    INT4 *pi4DsmonAggGroupIndex)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetFirstIndexDsmonAggGroupTable \n");

    return nmhGetNextIndexDsmonAggGroupTable (0, pi4DsmonAggControlIndex,
                                              0, pi4DsmonAggGroupIndex);
}

/****************************************************************************
Function    :  nmhGetNextIndexDsmonAggGroupTable
Input       :  The Indices
        DsmonAggControlIndex
        nextDsmonAggControlIndex
        DsmonAggGroupIndex
        nextDsmonAggGroupIndex
Output      :  The Get Next function gets the Next Index for
        the Index Value given in the Index Values. The
        Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonAggGroupTable (INT4 i4DsmonAggControlIndex,
                                   INT4 *pi4NextDsmonAggControlIndex,
                                   INT4 i4DsmonAggGroupIndex,
                                   INT4 *pi4NextDsmonAggGroupIndex)
{
    tDsmonAggGroup     *pAggGrp = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetNextIndexDsmonAggGroupTable \n");

    pAggGrp = DsmonAggCtlGetNextGrpIndex ((UINT4) i4DsmonAggControlIndex,
                                          (UINT4) i4DsmonAggGroupIndex);
    if (pAggGrp != NULL)
    {
        *pi4NextDsmonAggControlIndex = (INT4) pAggGrp->u4DsmonAggCtlIndex;
        *pi4NextDsmonAggGroupIndex = (INT4) pAggGrp->u4DsmonAggGroupIndex;
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_FN_EXIT, "Exiting from the function, \
               nmhGetNextIndexDsmonAggGroupTable \n");
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetDsmonAggGroupDescr
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex

        The Object
        retValDsmonAggGroupDescr
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggGroupDescr (INT4 i4DsmonAggControlIndex,
                          INT4 i4DsmonAggGroupIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValDsmonAggGroupDescr)
{
    tDsmonAggGroup     *pAggGrp = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggGroupDescr \n");

    pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                      (UINT4) i4DsmonAggGroupIndex);
    if (pAggGrp != NULL)
    {
        pRetValDsmonAggGroupDescr->i4_Length =
            STRLEN (pAggGrp->au1DsmonGroupDesc);
        MEMCPY (pRetValDsmonAggGroupDescr->pu1_OctetList,
                pAggGrp->au1DsmonGroupDesc,
                pRetValDsmonAggGroupDescr->i4_Length);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetDsmonAggGroupStatus
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex

        The Object
        retValDsmonAggGroupStatus
Output      :  The Get Low Lev Routine Take the Indices &
        store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetDsmonAggGroupStatus (INT4 i4DsmonAggControlIndex,
                           INT4 i4DsmonAggGroupIndex,
                           INT4 *pi4RetValDsmonAggGroupStatus)
{
    tDsmonAggGroup     *pAggGrp = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonAggGroupStatus \n");

    pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                      (UINT4) i4DsmonAggGroupIndex);
    if (pAggGrp != NULL)
    {
        *pi4RetValDsmonAggGroupStatus = (INT4) pAggGrp->u4DsmonAggRowStatus;
        return SNMP_SUCCESS;
    }
    DSMON_TRC (DSMON_FN_EXIT, "Exiting from the function, \
               nmhGetDsmonAggGroupStatus \n");
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetDsmonAggGroupDescr
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex

        The Object
        setValDsmonAggGroupDescr
Output      :  The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDsmonAggGroupDescr (INT4 i4DsmonAggControlIndex,
                          INT4 i4DsmonAggGroupIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValDsmonAggGroupDescr)
{
    tDsmonAggGroup     *pAggGrp = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetDsmonAggGroupDescr \n");

    pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                      (UINT4) i4DsmonAggGroupIndex);
    if (pAggGrp != NULL)
    {
        MEMCPY (pAggGrp->au1DsmonGroupDesc,
                pSetValDsmonAggGroupDescr->pu1_OctetList,
                pSetValDsmonAggGroupDescr->i4_Length);
        return SNMP_SUCCESS;

    }
    DSMON_TRC (DSMON_FN_EXIT, "Exiting from the function, \
               nmhSetDsmonAggGroupDescr \n");
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetDsmonAggGroupStatus
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex

        The Object
        setValDsmonAggGroupStatus
Output      :  The Set Low Lev Routine Take the Indices &
        Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetDsmonAggGroupStatus (INT4 i4DsmonAggControlIndex,
                           INT4 i4DsmonAggGroupIndex,
                           INT4 i4SetValDsmonAggGroupStatus)
{
    tDsmonAggGroup     *pAggGrp = NULL;
    INT4                i4Result = OSIX_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhSetDsmonAggGroupStatus \n");

    switch (i4SetValDsmonAggGroupStatus)
    {
        case CREATE_AND_WAIT:

            pAggGrp = DsmonAggCtlAddGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                              (UINT4) i4DsmonAggGroupIndex);
            if (pAggGrp == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggGroupStatus Agg.Grp. not found\r\n");
                return SNMP_FAILURE;
            }
            pAggGrp->u4DsmonAggRowStatus = NOT_READY;
            break;

        case ACTIVE:
            pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                              (UINT4) i4DsmonAggGroupIndex);
            if (pAggGrp == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggGroupStatus Agg.Grp. not found\r\n");
                return SNMP_FAILURE;
            }
            pAggGrp->u4DsmonAggRowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                              (UINT4) i4DsmonAggGroupIndex);
            if (pAggGrp == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggGroupStatus Agg.Grp. not found\r\n");
                return SNMP_FAILURE;
            }
            pAggGrp->u4DsmonAggRowStatus = NOT_IN_SERVICE;
            break;

        case DESTROY:
            pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                              (UINT4) i4DsmonAggGroupIndex);
            if (pAggGrp == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggGroupStatus Agg.Grp. not found\r\n");
                return SNMP_FAILURE;
            }
            i4Result = DsmonAggCtlDelGrpEntry (pAggGrp);
            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "nmhSetDsmonAggGroupStatus Agg.Grp. deletion failed\r\n");
                return SNMP_FAILURE;
            }
            break;
        default:
            break;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2DsmonAggGroupDescr
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex

        The Object
        testValDsmonAggGroupDescr
Output      :  The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2DsmonAggGroupDescr (UINT4 *pu4ErrorCode,
                             INT4 i4DsmonAggControlIndex,
                             INT4 i4DsmonAggGroupIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValDsmonAggGroupDescr)
{
    tDsmonAggGroup     *pAggGrp = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2DsmonAggGroupDescr \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if (gDsmonGlobals.u1DsmonAggCtlLock == DSMON_AGGLOCK_TRUE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggGroupDescr Agg. Ctl is locked\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((pTestValDsmonAggGroupDescr->i4_Length < 0) ||
        (pTestValDsmonAggGroupDescr->i4_Length > DSMON_MAX_DESC_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggGroupDescr Invalid Desc Len\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pAggGrp = DsmonAggCtlGetGrpEntry ((UINT4) i4DsmonAggControlIndex,
                                      (UINT4) i4DsmonAggGroupIndex);
    if (pAggGrp == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggGroupDescr Entry not found\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pAggGrp->u4DsmonAggRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "nmhTestv2DsmonAggGroupDescr RowStatus in ACTIVE\r\n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2DsmonAggGroupStatus
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex

        The Object
        testValDsmonAggGroupStatus
Output      :  The Test Low Lev Routine Take the Indices &
        Test whether that Value is Valid Input for Set.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2DsmonAggGroupStatus (UINT4 *pu4ErrorCode,
                              INT4 i4DsmonAggControlIndex,
                              INT4 i4DsmonAggGroupIndex,
                              INT4 i4TestValDsmonAggGroupStatus)
{
    INT1                i1Flag = SNMP_FAILURE;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhTestv2DsmonAggGroupStatus \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if (gDsmonGlobals.u1DsmonAggCtlLock == DSMON_AGGLOCK_TRUE)
    {
        return SNMP_FAILURE;
    }
    i1Flag =
        nmhValidateIndexInstanceDsmonAggGroupTable (i4DsmonAggControlIndex,
                                                    i4DsmonAggGroupIndex);
    if (i1Flag == SNMP_SUCCESS)
    {
        if ((i4TestValDsmonAggGroupStatus == ACTIVE) ||
            (i4TestValDsmonAggGroupStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonAggGroupStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        if (i4TestValDsmonAggGroupStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }
    }
    DSMON_TRC (DSMON_FN_EXIT, "Exiting from the function, \
               nmhTestv2DsmonAggGroupStatus \n");
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2DsmonAggGroupTable
Input       :  The Indices
        DsmonAggControlIndex
        DsmonAggGroupIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
        check whether dependency is met or not.
        Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
        SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
        SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2DsmonAggGroupTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
