/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            * 
 * 
 * $Id: dsmnpkth.c,v 1.6 2012/07/20 10:38:15 siva Exp $           *
 *                                                                   *
 * Description: This file contains the functional routines         *
 *        for DSMON PktInfo module                            *
 *                                                                  *
 *******************************************************************/
#include "dsmninc.h"
/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoAddEntry                                   */
/*                                                                           */
/* Description  : Allocates memory and Adds PktInfo Entry into                  */
/*          dsmonPktInfoRBTree                           */
/*                                                                           */
/* Input        : PktInfo                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPktInfo / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPktInfo *
DsmonPktInfoAddEntry (tDsmonPktInfo * pPktInfoParam)
{
    tDsmonPktInfo      *pPktInfo = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPktInfoAddEntry\r\n");

    if ((pPktInfo = (tDsmonPktInfo *)
         (MemAllocMemBlk (DSMON_PKTINFO_POOL))) == NULL)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "Memory Allocation failed for PktInfo\r\n");
        return NULL;
    }

    MEMSET (pPktInfo, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));
    MEMCPY (pPktInfo, pPktInfoParam, sizeof (tDsmonPktInfo));

    if (RBTreeAdd (DSMON_PKTINFO_TABLE, (tRBElem *) pPktInfo) == RB_SUCCESS)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "DsmonPktInfoAddEntry PktInfo entry added\r\n");
        return pPktInfo;
    }

    DSMON_TRC (DSMON_CRITICAL_TRC,
               "DsmonPktInfoAddEntry entry addtion failed\r\n");
    /* Release Memory allocated on Failure */
    MemReleaseMemBlock (DSMON_PKTINFO_POOL, (UINT1 *) pPktInfo);

    return NULL;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoDelUnusedEntries                               */
/*                                                                           */
/* Description  : Releases memory and Removes PktInfo Entry from             */
/*                dsmonPktInfoRBTree                                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPktInfoDelUnusedEntries ()
{
    tDsmonPktInfo       PktInfo, *pPktInfo = NULL;
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPktInfoDelUnusedEntries\r\n");

    pPktInfo = (tDsmonPktInfo *) RBTreeGetFirst (DSMON_PKTINFO_TABLE);

    while (pPktInfo != NULL)
    {
        MEMCPY (&PktInfo, pPktInfo, sizeof (tDsmonPktInfo));
        if ((pPktInfo->pInStats == NULL) &&
            (pPktInfo->pOutStats == NULL) &&
            (pPktInfo->pPdist == NULL) &&
            (pPktInfo->pInHost == NULL) &&
            (pPktInfo->pOutHost == NULL) && (pPktInfo->pMatrixSD == NULL))
        {
            DsmonPktInfoDelEntry (pPktInfo);
        }

        pPktInfo = (tDsmonPktInfo *) RBTreeGetNext (DSMON_PKTINFO_TABLE,
                                                    (tRBElem *) & PktInfo,
                                                    NULL);
    }
    DSMON_TRC (DSMON_FN_EXIT, "FUNC: EXIT DsmonPktInfoDelUnusedEntries\r\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoDelEntry                                         */
/*                                                                           */
/* Description  : Releases memory and Removes PktInfo Entry from             */
/*          dsmonPktInfoRBTree                                 */
/*                                                                           */
/* Input        : pPktInfo                                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS / OSIX_FAILURE                                */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
DsmonPktInfoDelEntry (tDsmonPktInfo * pPktInfo)
{
#ifdef NPAPI_WANTED
    if (pPktInfo->u1IsNPFlowAdded == OSIX_TRUE)
    {
        DsmonFsDSMONRemoveFlowStats (pPktInfo->PktHdr);
    }
#endif
    /* Remove pktinfo node from dsmonPktInfoRBTree */
    RBTreeRem (DSMON_PKTINFO_TABLE, (tRBElem *) pPktInfo);

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPktInfoDelEntry\r\n");
    /* Release Memory to MemPool */
    if (MemReleaseMemBlock (DSMON_PKTINFO_POOL,
                            (UINT1 *) pPktInfo) != MEM_SUCCESS)
    {
        DSMON_TRC (DSMON_CRITICAL_TRC | OS_RESOURCE_TRC,
                   "MemBlock Release failed for  PktInfo\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoRemoveEntries                                   */
/*                                                                           */
/* Description  : De-Allocates memory and Frees dsmonPktInfoRBTree          */
/*                                                                           */
/* Input        : u1DsmonAggControlStatus                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                             */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPktInfoRemoveEntries (UINT1 u1DsmonAggControlStatus)
{
    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPktInfoRemoveEntries\r\n");
    if (u1DsmonAggControlStatus == DSMON_AGGLOCK_FALSE)
    {
        /* Delete Pktinfo Table */
        RBTreeDrain (DSMON_PKTINFO_TABLE, DsmonUtlRBFreePktInfo, 0);
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoGetNextIndex                                   */
/*                                                                           */
/* Description  : Gets next PktInfo entry from dsmonPktInfoRBTree          */
/*                                                                           */
/* Input        : u4IfIndex, u4ProtoNLIndex, u4ProtoALIndex, SrcIp, DstIp    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pPktInfo / NULL                                     */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPktInfo *
DsmonPktInfoGetNextIndex (tPktHeader * pPktHdr)
{
    tDsmonPktInfo      *pPktInfo = NULL, PktInfo;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPktInfoGetNextIndex \r\n");

    MEMCPY (&PktInfo.PktHdr, pPktHdr, sizeof (tPktHeader));

    pPktInfo = (tDsmonPktInfo *) RBTreeGetNext (DSMON_PKTINFO_TABLE,
                                                (tRBElem *) & PktInfo, NULL);
    return pPktInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoGetEntry                                     */
/*                                                                           */
/* Description  : Gets PktInfo entry from dsmonPktInfoRBTree                 */
/*                                                                           */
/* Input        : u4IfIndex, u4ProtoNLIndex, u4ProtoALIndex, SrcIp, DstIp    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : pPktInfo / NULL                                            */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPktInfo *
DsmonPktInfoGetEntry (tPktHeader * pPktHdr)
{
    tDsmonPktInfo      *pPktInfo = NULL, PktInfo;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPktInfoGetEntry \r\n");
    MEMSET (&PktInfo, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));

    MEMCPY (&PktInfo.PktHdr, pPktHdr, sizeof (tPktHeader));

    pPktInfo = (tDsmonPktInfo *) RBTreeGet (DSMON_PKTINFO_TABLE,
                                            (tRBElem *) & PktInfo);

    return pPktInfo;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoProcessIncomingPkt                             */
/*                                                                           */
/* Description  : Process incoming packet information from RMONv2 and add    */
/*          an entry in dsmonPktInfoRBTree                 */
/*                                                                           */
/* Input        : PktInfo                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : tDsmonPktInfo / NULL Pointer                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC tDsmonPktInfo *
DsmonPktInfoProcessIncomingPkt (tDsmonPktInfo * pPktInfoParam)
{
    tDsmonPktInfo      *pPktInfo = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC: ENTRY DsmonPktInfoProcessIncomingPkt \r\n");
    pPktInfo = DsmonPktInfoGetEntry (&pPktInfoParam->PktHdr);
    if (pPktInfo == NULL)
    {
        pPktInfo = DsmonPktInfoAddEntry (pPktInfoParam);
        return pPktInfo;
    }
    else
    {
        return NULL;
    }
}

/************************************************************************/
/*  Function Name   : DsmonPktInfoUpdateProtoChgs                       */
/*                                    */
/*  Description     : If RMONv2 removes a protocol entry from its       */
/*              ProtocolDir Table, this function will be invoked  */
/*              by RMONv2 to reflect the changes in DSMON        */
/*                                    */
/*  Input(s)        : u4DsmonProtDirLocalIndex                      */
/*                                    */
/*  Output(s)       : None.                                             */
/*                                    */
/*  Returns         : None                                              */
/************************************************************************/
PUBLIC VOID
DsmonPktInfoUpdateProtoChgs (UINT4 u4ProtDirLocalIndex)
{
    tDsmonPktInfo      *pPktInfo = NULL, PktInfo;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPktInfoUpdateProtoChgs\r\n");
    MEMSET (&PktInfo, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));

    /* Get First PktInfo entry */
    pPktInfo = DsmonPktInfoGetNextIndex (&PktInfo.PktHdr);

    while (pPktInfo != NULL)
    {
        MEMCPY (&PktInfo, pPktInfo, sizeof (tDsmonPktInfo));
        if ((pPktInfo->u4ProtoNLIndex == u4ProtDirLocalIndex) ||
            (pPktInfo->u4ProtoALIndex == u4ProtDirLocalIndex) ||
            (pPktInfo->u4ProtoTLIndex == u4ProtDirLocalIndex))
        {
            DsmonPktInfoDelIdlePkt (pPktInfo);
        }

        pPktInfo = DsmonPktInfoGetNextIndex (&PktInfo.PktHdr);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoUpdateIfaceChgs                                */
/*                                                                           */
/* Description  : If Operstatus is CFA_IF_DOWN, then deleting the associated */
/*                Pktinfo Table.                          */
/*                                                                           */
/* Input        : u4IfIndex, u1OperStatus                                    */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPktInfoUpdateIfaceChgs (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tDsmonPktInfo      *pPktInfo = NULL, PktInfo;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC: ENTRY DsmonPktInfoUpdateIfaceChgs\r\n");
    MEMSET (&PktInfo, DSMON_INIT_VAL, sizeof (tDsmonPktInfo));

    switch (u1OperStatus)
    {
        case CFA_IF_UP:
            break;
        case CFA_IF_DOWN:
            /* Get First PktInfo entry */
            pPktInfo = DsmonPktInfoGetNextIndex (&PktInfo.PktHdr);

            while (pPktInfo != NULL)
            {
                MEMCPY (&PktInfo, pPktInfo, sizeof (tDsmonPktInfo));
                if (pPktInfo->PktHdr.u4IfIndex == u4IfIndex)
                {
                    DsmonPktInfoDelEntry (pPktInfo);
                }

                pPktInfo = DsmonPktInfoGetNextIndex (&PktInfo.PktHdr);
            }
            break;
        default:
            break;
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : DsmonPktInfoDelIdlePkt                                     */
/*                                                                           */
/* Description  : This routine removes Least recently used pkt entry and its */
/*                associated data entries.                                   */
/*                                                                           */
/* Input        : pPktInfo                                             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
DsmonPktInfoDelIdlePkt (tDsmonPktInfo * pPktInfo)
{
    tDsmonStats        *pStatsNode = NULL;
    tDsmonPdistStats   *pPdistNode = NULL;
    tDsmonHostStats    *pHostNode = NULL;
    tDsmonMatrixSD     *pMatrixSD = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY DsmonPktInfoDelIdlePkt\r\n");

    /* Remove Matrix Entries */
    pMatrixSD = pPktInfo->pMatrixSD;

    while (pMatrixSD != NULL)
    {
        if (pMatrixSD->u4PktRefCount == DSMON_ONE)
        {
            DsmonMatrixDelInterDep (pMatrixSD);
            pMatrixSD->pMatrixCtl->u4DsmonMatrixCtlDeletes += 2;
            DsmonMatrixDelDataEntry (pMatrixSD);
            pMatrixSD = pPktInfo->pMatrixSD;
        }
        else
        {
            pMatrixSD->u4PktRefCount--;
            pMatrixSD = pMatrixSD->pNextMatrixSD;
        }
    }

    /* Remove Host Entries */
    pHostNode = pPktInfo->pOutHost;

    while (pHostNode != NULL)
    {
        if (pHostNode->u4PktRefCount == DSMON_ONE)
        {
            DsmonHostDelInterDep (pHostNode);
            pHostNode->pHostCtl->u4DsmonHostCtlDeletes++;
            DsmonHostDelDataEntry (pHostNode);
            pHostNode = pPktInfo->pOutHost;
        }
        else
        {
            pHostNode->u4PktRefCount--;
            pHostNode = pHostNode->pNextHost;
        }
    }

    pHostNode = pPktInfo->pInHost;

    while (pHostNode != NULL)
    {
        if (pHostNode->u4PktRefCount == DSMON_ONE)
        {
            DsmonHostDelInterDep (pHostNode);
            pHostNode->pHostCtl->u4DsmonHostCtlDeletes++;
            DsmonHostDelDataEntry (pHostNode);
            pHostNode = pPktInfo->pInHost;
        }
        else
        {
            pHostNode->u4PktRefCount--;
            pHostNode = pHostNode->pNextHost;
        }
    }

    /* Remove Pdist Entries */
    pPdistNode = pPktInfo->pPdist;

    while (pPdistNode != NULL)
    {
        if (pPdistNode->u4PktRefCount == DSMON_ONE)
        {
            DsmonPdistDelInterDep (pPdistNode);
            pPdistNode->pPdistCtl->u4DsmonPdistCtlDeletes++;
            DsmonPdistDelDataEntry (pPdistNode);
            pPdistNode = pPktInfo->pPdist;
        }
        else
        {
            pPdistNode->u4PktRefCount--;
            pPdistNode = pPdistNode->pNextPdist;
        }
    }

    /* Remove Stats Entries */
    pStatsNode = pPktInfo->pOutStats;

    while (pStatsNode != NULL)
    {
        if (pStatsNode->u4PktRefCount == DSMON_ONE)
        {
            DsmonStatsDelInterDep (pStatsNode);
            DsmonStatsDelDataEntry (pStatsNode);
            pStatsNode = pPktInfo->pOutStats;
        }
        else
        {
            pStatsNode->u4PktRefCount--;
            pStatsNode = pStatsNode->pNextStats;
        }
    }

    pStatsNode = pPktInfo->pInStats;

    while (pStatsNode != NULL)
    {
        if (pStatsNode->u4PktRefCount == DSMON_ONE)
        {
            DsmonStatsDelInterDep (pStatsNode);
            DsmonStatsDelDataEntry (pStatsNode);
            pStatsNode = pPktInfo->pInStats;
        }
        else
        {
            pStatsNode->u4PktRefCount--;
            pStatsNode = pStatsNode->pNextStats;
        }
    }

    /* Remove Idle Pkt Info */
    DsmonPktInfoDelEntry (pPktInfo);
}
