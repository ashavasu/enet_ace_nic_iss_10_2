/********************************************************************
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved             *
 *                                                                  *
 * Description: Contains low level routine of Capability Group        *
 *                                                                  *
 *******************************************************************/

/* Capabilities Group */
#include  "dsmninc.h"
/****************************************************************************
 Function    :  nmhGetDsmonCapabilities
 Input       :  The Indices

                The Object 
                retValDsmonCapabilities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonCapabilities (tSNMP_OCTET_STRING_TYPE * pRetValDsmonCapabilities)
{
    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
               nmhGetDsmonCapabilities \n");

    pRetValDsmonCapabilities->i4_Length = sizeof (DSMON_CAPABILITIES);

    MEMCPY (pRetValDsmonCapabilities->pu1_OctetList,
            &DSMON_CAPABILITIES, pRetValDsmonCapabilities->i4_Length);

    return SNMP_SUCCESS;
}
