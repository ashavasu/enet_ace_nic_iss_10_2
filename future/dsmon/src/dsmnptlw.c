/********************************************************************
 *                                                                  *
 *  Copyright (C) 2009 Aricent Inc . All Rights Reserved        *
 *
 * $Id: dsmnptlw.c,v 1.9 2012/12/13 14:25:10 siva Exp $           *
 *                                                                   *
 * Description: This file contains the low level nmh routines         *
 *        for DSMON Pdist module                            *
 *                                                                  *
 ********************************************************************/
/* LOW LEVEL Routines for Table : DsmonPdistCtlTable. */
#include "dsmninc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonPdistCtlTable
 Input       :  The Indices
                DsmonPdistCtlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonPdistCtlTable (INT4 i4DsmonPdistCtlIndex)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonPdistCtlTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonPdistCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistControlIndex out of range \n");

        return SNMP_FAILURE;
    }

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonPdistCtlTable
 Input       :  The Indices
                DsmonPdistCtlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonPdistCtlTable (INT4 *pi4DsmonPdistCtlIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetFirstIndexDsmonPdistCtlTable \n");

    return nmhGetNextIndexDsmonPdistCtlTable (DSMON_ZERO,
                                              pi4DsmonPdistCtlIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonPdistCtlTable
 Input       :  The Indices
                DsmonPdistCtlIndex
                nextDsmonPdistCtlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonPdistCtlTable (INT4 i4DsmonPdistCtlIndex,
                                   INT4 *pi4NextDsmonPdistCtlIndex)
{
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetNextIndexDsmonPdistCtlTable \n");

    pPdistCtlNode = DsmonPdistGetNextCtlIndex (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonPdistCtlIndex = (INT4) (pPdistCtlNode->u4DsmonPdistCtlIndex);

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlDataSource
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlDataSource
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlDataSource (INT4 i4DsmonPdistCtlIndex,
                               tSNMP_OID_TYPE * pRetValDsmonPdistCtlDataSource)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistCtlDataSource \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_GET_DATA_SOURCE_OID (pRetValDsmonPdistCtlDataSource,
                               pPdistCtlNode->u4DsmonPdistDataSource);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlAggProfile
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlAggProfile
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlAggProfile (INT4 i4DsmonPdistCtlIndex,
                               INT4 *pi4RetValDsmonPdistCtlAggProfile)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistCtlAggProfile \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistCtlAggProfile =
        (INT4) (pPdistCtlNode->u4DsmonPdistCtlProfileIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlMaxDesiredEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlMaxDesiredEntries (INT4 i4DsmonPdistCtlIndex,
                                      INT4
                                      *pi4RetValDsmonPdistCtlMaxDesiredEntries)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistCtlMaxDesiredEntries \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistCtlMaxDesiredEntries =
        pPdistCtlNode->i4DsmonPdistCtlMaxDesiredEntries;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlDroppedFrames
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlDroppedFrames
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlDroppedFrames (INT4 i4DsmonPdistCtlIndex,
                                  UINT4 *pu4RetValDsmonPdistCtlDroppedFrames)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistCtlDroppedFrames \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistCtlDroppedFrames =
        pPdistCtlNode->u4DsmonPdistCtlDroppedFrames;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlInserts
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlInserts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlInserts (INT4 i4DsmonPdistCtlIndex,
                            UINT4 *pu4RetValDsmonPdistCtlInserts)
{
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistCtlInserts \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistCtlInserts = pPdistCtlNode->u4DsmonPdistCtlInserts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlDeletes
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlDeletes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlDeletes (INT4 i4DsmonPdistCtlIndex,
                            UINT4 *pu4RetValDsmonPdistCtlDeletes)
{
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistCtlDeletes \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistCtlDeletes = pPdistCtlNode->u4DsmonPdistCtlDeletes;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlCreateTime
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlCreateTime (INT4 i4DsmonPdistCtlIndex,
                               UINT4 *pu4RetValDsmonPdistCtlCreateTime)
{
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistCtlCreateTime \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistCtlCreateTime =
        pPdistCtlNode->u4DsmonPdistCtlCreateTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlOwner
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlOwner (INT4 i4DsmonPdistCtlIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValDsmonPdistCtlOwner)
{
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistCtlOwner \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDsmonPdistCtlOwner->pu1_OctetList,
            pPdistCtlNode->au1DsmonPdistCtlOwner, DSMON_MAX_OWNER_LEN);

    pRetValDsmonPdistCtlOwner->i4_Length =
        STRLEN (pPdistCtlNode->au1DsmonPdistCtlOwner);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistCtlStatus
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                retValDsmonPdistCtlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistCtlStatus (INT4 i4DsmonPdistCtlIndex,
                           INT4 *pi4RetValDsmonPdistCtlStatus)
{
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistCtlStatus \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistCtlStatus = pPdistCtlNode->u4DsmonPdistCtlRowStatus;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDsmonPdistCtlDataSource
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                setValDsmonPdistCtlDataSource
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistCtlDataSource (INT4 i4DsmonPdistCtlIndex,
                               tSNMP_OID_TYPE * pSetValDsmonPdistCtlDataSource)
{
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonPdistCtlDataSource \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    DSMON_SET_DATA_SOURCE_OID (pSetValDsmonPdistCtlDataSource,
                               &(pPdistCtlNode->u4DsmonPdistDataSource));

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonPdistCtlAggProfile
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                setValDsmonPdistCtlAggProfile
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistCtlAggProfile (INT4 i4DsmonPdistCtlIndex,
                               INT4 i4SetValDsmonPdistCtlAggProfile)
{
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonPdistCtlAggProfile \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    pPdistCtlNode->u4DsmonPdistCtlProfileIndex =
        (UINT4) (i4SetValDsmonPdistCtlAggProfile);
    pPdistCtlNode->pAggCtl =
        DsmonAggCtlGetEntry (i4SetValDsmonPdistCtlAggProfile);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonPdistCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                setValDsmonPdistCtlMaxDesiredEntries
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistCtlMaxDesiredEntries (INT4 i4DsmonPdistCtlIndex,
                                      INT4
                                      i4SetValDsmonPdistCtlMaxDesiredEntries)
{
    UINT4               u4Minimum = 0;

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonPdistCtlMaxDesiredEntries \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    u4Minimum = (i4SetValDsmonPdistCtlMaxDesiredEntries != -1) ?
        ((i4SetValDsmonPdistCtlMaxDesiredEntries < DSMON_MAX_DATA_PER_CTL) ?
         i4SetValDsmonPdistCtlMaxDesiredEntries : DSMON_MAX_DATA_PER_CTL)
        : DSMON_MAX_DATA_PER_CTL;

    pPdistCtlNode->i4DsmonPdistCtlMaxDesiredEntries =
        i4SetValDsmonPdistCtlMaxDesiredEntries;

    pPdistCtlNode->u4DsmonPdistCtlMaxDesiredSupported = u4Minimum;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonPdistCtlOwner
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                setValDsmonPdistCtlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistCtlOwner (INT4 i4DsmonPdistCtlIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValDsmonPdistCtlOwner)
{

    UINT4               u4Minimum = 0;

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonPdistCtlOwner \n");

    /* Get Pdist Control Node corresponding to PdistControlIndex */

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    u4Minimum = (pSetValDsmonPdistCtlOwner->i4_Length < DSMON_MAX_OWNER_LEN) ?
        pSetValDsmonPdistCtlOwner->i4_Length : DSMON_MAX_OWNER_LEN;

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: Invalid Pdist Control Index \n");

        return SNMP_FAILURE;
    }

    MEMSET (pPdistCtlNode->au1DsmonPdistCtlOwner, DSMON_INIT_VAL,
            DSMON_MAX_OWNER_LEN);

    MEMCPY (pPdistCtlNode->au1DsmonPdistCtlOwner,
            pSetValDsmonPdistCtlOwner->pu1_OctetList, u4Minimum);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetDsmonPdistCtlStatus
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                setValDsmonPdistCtlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistCtlStatus (INT4 i4DsmonPdistCtlIndex,
                           INT4 i4SetValDsmonPdistCtlStatus)
{
    tPortList          *pVlanPortList = NULL;
    tDsmonPdistCtl     *pPdistCtlNode = NULL;
    tDsmonAggCtl       *pAggCtlNode = NULL;
    tCfaIfInfo          IfInfo;
    UINT2               u2VlanId = 0;
    INT4                i4Result = 0;

    MEMSET (&IfInfo, DSMON_INIT_VAL, sizeof (tCfaIfInfo));

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonPdistCtlStatus \n");

    switch (i4SetValDsmonPdistCtlStatus)
    {
        case CREATE_AND_WAIT:

            pPdistCtlNode = DsmonPdistAddCtlEntry (i4DsmonPdistCtlIndex);

            if (pPdistCtlNode == NULL)
            {
                DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                           " Memory Allocation / \
                 RBTree Add Failed - Pdist Control Table \n");

                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:

            pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

            if (pPdistCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Pdist Control Index \n");

                return SNMP_FAILURE;
            }

            /* check for valid agg profile index */

            pAggCtlNode =
                DsmonAggCtlGetEntry (pPdistCtlNode->
                                     u4DsmonPdistCtlProfileIndex);

            if (pAggCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure:\
                 Invalid Pdist Control Agg Profile Index \n");

                return SNMP_FAILURE;
            }

            /* check for valid datasource */

            if (CfaGetIfInfo (pPdistCtlNode->u4DsmonPdistDataSource, &IfInfo) ==
                CFA_FAILURE)
            {
                /* ifindex is invalid */
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid interface Index \n");

                return SNMP_FAILURE;
            }

            if (((IfInfo.u1IfOperStatus != CFA_IF_UP) ||
                 (IfInfo.u1IfAdminStatus != CFA_IF_UP)) &&
                ((gi4MibResStatus != MIB_RESTORE_IN_PROGRESS)))
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Interface OperStatus or Admin Status is in invalid state \n");

                return SNMP_FAILURE;
            }
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                DSMON_TRC (DSMON_FN_ENTRY,
                           "nmhSetDsmonPdistCtlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));
            if (IfInfo.u1IfType == CFA_L3IPVLAN)
            {
                /* Get the VlanId from interface index */
                if (CfaGetVlanId
                    (pPdistCtlNode->u4DsmonPdistDataSource,
                     &u2VlanId) == CFA_FAILURE)
                {
                    DSMON_TRC1 (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Unable to Fetch VlanId of IfIndex %d\n", pPdistCtlNode->u4DsmonPdistDataSource);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }

                if (L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList) ==
                    L2IWF_FAILURE)
                {
                    DSMON_TRC1 (DSMON_DEBUG_TRC, "SNMP Failure: \
                 Get Member Port List for VlanId %d failed \n", u2VlanId);
                    FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
                    return SNMP_FAILURE;
                }
            }

            pPdistCtlNode->u4DsmonPdistCtlRowStatus = ACTIVE;

            pPdistCtlNode->u4DsmonPdistCtlCreateTime =
                (UINT4) OsixGetSysUpTime ();
#ifdef NPAPI_WANTED
            DsmonFsDSMONEnableProbe (pPdistCtlNode->u4DsmonPdistDataSource,
                                     u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            DsmonPdistPopulateDataEntries (pPdistCtlNode);
            DsmonPdistTopNNotifyPdistCtlChgs ((UINT4) i4DsmonPdistCtlIndex,
                                              (UINT4)
                                              i4SetValDsmonPdistCtlStatus);
            DsmonPktInfoDelUnusedEntries ();
            break;

        case NOT_IN_SERVICE:

            pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

            if (pPdistCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Pdist Control Index \n");

                return SNMP_FAILURE;
            }

            pPdistCtlNode->u4DsmonPdistCtlRowStatus = NOT_IN_SERVICE;

            DsmonPdistDelDataEntries ((UINT4) i4DsmonPdistCtlIndex);

            DsmonPdistTopNNotifyPdistCtlChgs ((UINT4) i4DsmonPdistCtlIndex,
                                              (UINT4)
                                              i4SetValDsmonPdistCtlStatus);
            /* check for valid datasource */
            pVlanPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pVlanPortList == NULL)
            {
                DSMON_TRC (DSMON_FN_ENTRY,
                           "nmhSetDsmonPdistCtlStatus: "
                           "Error in allocating memory for pVlanPortList\r\n");
                return OSIX_FAILURE;
            }
            MEMSET (pVlanPortList, 0, sizeof (tPortList));

            if (CfaGetIfInfo (pPdistCtlNode->u4DsmonPdistDataSource, &IfInfo) !=
                CFA_FAILURE)
            {
                if ((IfInfo.u1IfType == CFA_L3IPVLAN) &&
                    (CfaGetVlanId (pPdistCtlNode->u4DsmonPdistDataSource,
                                   &u2VlanId) != CFA_FAILURE))
                {
                    L2IwfGetVlanEgressPorts (u2VlanId, *pVlanPortList);
                }
            }
#ifdef NPAPI_WANTED
            DsmonFsDSMONDisableProbe (pPdistCtlNode->u4DsmonPdistDataSource,
                                      u2VlanId, *pVlanPortList);
#endif
            FsUtilReleaseBitList ((UINT1 *) pVlanPortList);
            DsmonPktInfoDelUnusedEntries ();
            break;

        case DESTROY:

            pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

            if (pPdistCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Pdist Control Index \n");

                return SNMP_FAILURE;
            }
            DsmonPdistTopNNotifyPdistCtlChgs ((UINT4) i4DsmonPdistCtlIndex,
                                              (UINT4)
                                              i4SetValDsmonPdistCtlStatus);

            i4Result = DsmonPdistDelCtlEntry (pPdistCtlNode);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Pdist Control Index \
                 RBTreeRemove failed - Pdist Control Table\n");

                return SNMP_FAILURE;
            }

            DsmonPktInfoDelUnusedEntries ();
            break;
        default:
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistCtlDataSource
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                testValDsmonPdistCtlDataSource
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistCtlDataSource (UINT4 *pu4ErrorCode,
                                  INT4 i4DsmonPdistCtlIndex,
                                  tSNMP_OID_TYPE *
                                  pTestValDsmonPdistCtlDataSource)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonPdistCtlDataSource \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    if ((i4DsmonPdistCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (DSMON_TEST_DATA_SOURCE_OID (pTestValDsmonPdistCtlDataSource) == FALSE)
    {

        DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_DEBUG_TRC, "Invalid DataSource");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistCtlNode->u4DsmonPdistCtlRowStatus == ACTIVE)
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistCtlAggProfile
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                testValDsmonPdistCtlAggProfile
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistCtlAggProfile (UINT4 *pu4ErrorCode,
                                  INT4 i4DsmonPdistCtlIndex,
                                  INT4 i4TestValDsmonPdistCtlAggProfile)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;
    tDsmonAggCtl       *pAggCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonPdistCtlAggProfile \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistCtlIndex > DSMON_MAX_CTL_INDEX))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pAggCtlNode = DsmonAggCtlGetEntry (i4TestValDsmonPdistCtlAggProfile);

    if (pAggCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: AggCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistCtlNode->u4DsmonPdistCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistCtlMaxDesiredEntries
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                testValDsmonPdistCtlMaxDesiredEntries
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistCtlMaxDesiredEntries (UINT4 *pu4ErrorCode,
                                         INT4 i4DsmonPdistCtlIndex,
                                         INT4
                                         i4TestValDsmonPdistCtlMaxDesiredEntries)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonPdistCtlMaxDesiredEntries \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValDsmonPdistCtlMaxDesiredEntries < -1) ||
        (i4TestValDsmonPdistCtlMaxDesiredEntries > DSMON_MAX_DATA_PER_CTL) ||
        (i4TestValDsmonPdistCtlMaxDesiredEntries == DSMON_ZERO))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: MaxDesiredEntries  out of range \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistCtlNode->u4DsmonPdistCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistCtlOwner
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                testValDsmonPdistCtlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistCtlOwner (UINT4 *pu4ErrorCode, INT4 i4DsmonPdistCtlIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValDsmonPdistCtlOwner)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonPdistCtlOwner \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((pTestValDsmonPdistCtlOwner->i4_Length < DSMON_ZERO) ||
        (pTestValDsmonPdistCtlOwner->i4_Length > DSMON_MAX_OWNER_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid Owner length \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistCtl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistCtlNode->u4DsmonPdistCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistCtlStatus
 Input       :  The Indices
                DsmonPdistCtlIndex

                The Object
                testValDsmonPdistCtlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistCtlStatus (UINT4 *pu4ErrorCode, INT4 i4DsmonPdistCtlIndex,
                              INT4 i4TestValDsmonPdistCtlStatus)
{

    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonPdistCtlStatus \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pPdistCtlNode = DsmonPdistGetCtlEntry (i4DsmonPdistCtlIndex);

    if (pPdistCtlNode == NULL)
    {
        if (i4TestValDsmonPdistCtlStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValDsmonPdistCtlStatus == ACTIVE) ||
            (i4TestValDsmonPdistCtlStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonPdistCtlStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DsmonPdistCtlTable
 Input       :  The Indices
                DsmonPdistCtlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DsmonPdistCtlTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonPdistStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonPdistStatsTable
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonPdistStatsTable (INT4 i4DsmonPdistCtlIndex,
                                              UINT4 u4DsmonPdistTimeMark,
                                              INT4 i4DsmonAggGroupIndex,
                                              INT4 i4ProtocolDirLocalIndex)
{

    tDsmonPdistStats   *pPdistStatsNode = NULL;
    UNUSED_PARAM (u4DsmonPdistTimeMark);

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonPdistStatsTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pPdistStatsNode = DsmonPdistGetDataEntry (i4DsmonPdistCtlIndex,
                                              i4DsmonAggGroupIndex,
                                              i4ProtocolDirLocalIndex);

    if (pPdistStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Pdist Control Index or \
                   Agg Group Index or Protocol Dir Local Index is invalid \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonPdistStatsTable
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexDsmonPdistStatsTable (INT4 *pi4DsmonPdistCtlIndex,
                                      UINT4 *pu4DsmonPdistTimeMark,
                                      INT4 *pi4DsmonAggGroupIndex,
                                      INT4 *pi4ProtocolDirLocalIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetFirstIndexDsmonPdistStatsTable \n");

    return nmhGetNextIndexDsmonPdistStatsTable (DSMON_ZERO,
                                                pi4DsmonPdistCtlIndex,
                                                DSMON_ZERO,
                                                pu4DsmonPdistTimeMark,
                                                DSMON_ZERO,
                                                pi4DsmonAggGroupIndex,
                                                DSMON_ZERO,
                                                pi4ProtocolDirLocalIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonPdistStatsTable
 Input       :  The Indices
                DsmonPdistCtlIndex
                nextDsmonPdistCtlIndex
                DsmonPdistTimeMark
                nextDsmonPdistTimeMark
                DsmonAggGroupIndex
                nextDsmonAggGroupIndex
                ProtocolDirLocalIndex
                nextProtocolDirLocalIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonPdistStatsTable (INT4 i4DsmonPdistCtlIndex,
                                     INT4 *pi4NextDsmonPdistCtlIndex,
                                     UINT4 u4DsmonPdistTimeMark,
                                     UINT4 *pu4NextDsmonPdistTimeMark,
                                     INT4 i4DsmonAggGroupIndex,
                                     INT4 *pi4NextDsmonAggGroupIndex,
                                     INT4 i4ProtocolDirLocalIndex,
                                     INT4 *pi4NextProtocolDirLocalIndex)
{
    tDsmonPdistStats   *pPdistStatsNode = NULL;
    UNUSED_PARAM (u4DsmonPdistTimeMark);

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetNextIndexDsmonPdistStatsTable \n");

    pPdistStatsNode = DsmonPdistGetNextDataIndex (i4DsmonPdistCtlIndex,
                                                  i4DsmonAggGroupIndex,
                                                  i4ProtocolDirLocalIndex);

    if (pPdistStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Pdist Control Index or \
                   Agg Group Index or Protocol Dir Local Index is invalid \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonPdistCtlIndex = pPdistStatsNode->u4DsmonPdistCtlIndex;

    *pu4NextDsmonPdistTimeMark = DSMON_ZERO;

    *pi4NextDsmonAggGroupIndex = pPdistStatsNode->u4DsmonPdistAggGroupIndex;

    *pi4NextProtocolDirLocalIndex =
        pPdistStatsNode->u4DsmonPdistProtDirLocalIndex;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonPdistStatsPkts
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex

                The Object
                retValDsmonPdistStatsPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistStatsPkts (INT4 i4DsmonPdistCtlIndex,
                           UINT4 u4DsmonPdistTimeMark,
                           INT4 i4DsmonAggGroupIndex,
                           INT4 i4ProtocolDirLocalIndex,
                           UINT4 *pu4RetValDsmonPdistStatsPkts)
{
    tDsmonPdistStats   *pPdistStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistStatsPkts \n");

    pPdistStatsNode = DsmonPdistGetDataEntry (i4DsmonPdistCtlIndex,
                                              i4DsmonAggGroupIndex,
                                              i4ProtocolDirLocalIndex);

    if (pPdistStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Pdist Control Index or \
                   Agg Group Index or Protocol Dir Local Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pPdistStatsNode->u4DsmonPdistTimeMark < u4DsmonPdistTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistStatsPkts =
        pPdistStatsNode->DsmonCurSample.u4DsmonPdistStatsPkts;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistStatsOctets
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex

                The Object
                retValDsmonPdistStatsOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistStatsOctets (INT4 i4DsmonPdistCtlIndex,
                             UINT4 u4DsmonPdistTimeMark,
                             INT4 i4DsmonAggGroupIndex,
                             INT4 i4ProtocolDirLocalIndex,
                             UINT4 *pu4RetValDsmonPdistStatsOctets)
{
    tDsmonPdistStats   *pPdistStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "Entering into the function, \
              nmhGetDsmonPdistStatsOctets \n");

    pPdistStatsNode = DsmonPdistGetDataEntry (i4DsmonPdistCtlIndex,
                                              i4DsmonAggGroupIndex,
                                              i4ProtocolDirLocalIndex);

    if (pPdistStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Pdist Control Index or \
                   Agg Group Index or Protocol Dir Local Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pPdistStatsNode->u4DsmonPdistTimeMark < u4DsmonPdistTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistStatsOctets =
        pPdistStatsNode->DsmonCurSample.u4DsmonPdistStatsOctets;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistStatsOvflPkts
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex

                The Object
                retValDsmonPdistStatsOvflPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistStatsOvflPkts (INT4 i4DsmonPdistCtlIndex,
                               UINT4 u4DsmonPdistTimeMark,
                               INT4 i4DsmonAggGroupIndex,
                               INT4 i4ProtocolDirLocalIndex,
                               UINT4 *pu4RetValDsmonPdistStatsOvflPkts)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonPdistCtlIndex);
    UNUSED_PARAM (u4DsmonPdistTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4ProtocolDirLocalIndex);
    UNUSED_PARAM (pu4RetValDsmonPdistStatsOvflPkts);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistStatsOvflOctets
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex

                The Object
                retValDsmonPdistStatsOvflOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistStatsOvflOctets (INT4 i4DsmonPdistCtlIndex,
                                 UINT4 u4DsmonPdistTimeMark,
                                 INT4 i4DsmonAggGroupIndex,
                                 INT4 i4ProtocolDirLocalIndex,
                                 UINT4 *pu4RetValDsmonPdistStatsOvflOctets)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonPdistCtlIndex);
    UNUSED_PARAM (u4DsmonPdistTimeMark);
    UNUSED_PARAM (i4DsmonAggGroupIndex);
    UNUSED_PARAM (i4ProtocolDirLocalIndex);
    UNUSED_PARAM (pu4RetValDsmonPdistStatsOvflOctets);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistStatsHCPkts
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex

                The Object
                retValDsmonPdistStatsHCPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistStatsHCPkts (INT4 i4DsmonPdistCtlIndex,
                             UINT4 u4DsmonPdistTimeMark,
                             INT4 i4DsmonAggGroupIndex,
                             INT4 i4ProtocolDirLocalIndex,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValDsmonPdistStatsHCPkts)
{

    tDsmonPdistStats   *pPdistStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistStatsHCPkts \n");

    pPdistStatsNode = DsmonPdistGetDataEntry (i4DsmonPdistCtlIndex,
                                              i4DsmonAggGroupIndex,
                                              i4ProtocolDirLocalIndex);

    if (pPdistStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Pdist Control Index or \
                   Agg Group Index or Protocol Dir Local Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pPdistStatsNode->u4DsmonPdistTimeMark < u4DsmonPdistTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonPdistStatsHCPkts->msn =
        pPdistStatsNode->DsmonCurSample.u8DsmonPdistStatsHCPkts.u4HiWord;

    pu8RetValDsmonPdistStatsHCPkts->lsn =
        pPdistStatsNode->DsmonCurSample.u8DsmonPdistStatsHCPkts.u4LoWord;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistStatsHCOctets
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex

                The Object
                retValDsmonPdistStatsHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistStatsHCOctets (INT4 i4DsmonPdistCtlIndex,
                               UINT4 u4DsmonPdistTimeMark,
                               INT4 i4DsmonAggGroupIndex,
                               INT4 i4ProtocolDirLocalIndex,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValDsmonPdistStatsHCOctets)
{

    tDsmonPdistStats   *pPdistStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistStatsHCOctets \n");

    pPdistStatsNode = DsmonPdistGetDataEntry (i4DsmonPdistCtlIndex,
                                              i4DsmonAggGroupIndex,
                                              i4ProtocolDirLocalIndex);

    if (pPdistStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Pdist Control Index or \
                   Agg Group Index or Protocol Dir Local Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pPdistStatsNode->u4DsmonPdistTimeMark < u4DsmonPdistTimeMark)
    {
        return SNMP_FAILURE;
    }

    pu8RetValDsmonPdistStatsHCOctets->msn =
        pPdistStatsNode->DsmonCurSample.u8DsmonPdistStatsHCOctets.u4HiWord;

    pu8RetValDsmonPdistStatsHCOctets->lsn =
        pPdistStatsNode->DsmonCurSample.u8DsmonPdistStatsHCOctets.u4LoWord;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistStatsCreateTime
 Input       :  The Indices
                DsmonPdistCtlIndex
                DsmonPdistTimeMark
                DsmonAggGroupIndex
                ProtocolDirLocalIndex

                The Object
                retValDsmonPdistStatsCreateTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistStatsCreateTime (INT4 i4DsmonPdistCtlIndex,
                                 UINT4 u4DsmonPdistTimeMark,
                                 INT4 i4DsmonAggGroupIndex,
                                 INT4 i4ProtocolDirLocalIndex,
                                 UINT4 *pu4RetValDsmonPdistStatsCreateTime)
{

    tDsmonPdistStats   *pPdistStatsNode = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistStatsCreateTime \n");

    pPdistStatsNode = DsmonPdistGetDataEntry (i4DsmonPdistCtlIndex,
                                              i4DsmonAggGroupIndex,
                                              i4ProtocolDirLocalIndex);

    if (pPdistStatsNode == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Pdist Control Index or \
                   Agg Group Index or Protocol Dir Local Index is invalid \n");

        return SNMP_FAILURE;
    }

    if (pPdistStatsNode->u4DsmonPdistTimeMark < u4DsmonPdistTimeMark)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistStatsCreateTime =
        pPdistStatsNode->u4DsmonPdistStatsCreateTime;

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : DsmonPdistTopNCtlTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonPdistTopNCtlTable
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonPdistTopNCtlTable (INT4 i4DsmonPdistTopNCtlIndex)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonPdistTopNCtlTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonPdistTopNCtlTable
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonPdistTopNCtlTable (INT4 *pi4DsmonPdistTopNCtlIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetFirstIndexDsmonPdistTopNCtlTable \n");

    return nmhGetNextIndexDsmonPdistTopNCtlTable (DSMON_ZERO,
                                                  pi4DsmonPdistTopNCtlIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonPdistTopNCtlTable
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                nextDsmonPdistTopNCtlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonPdistTopNCtlTable (INT4 i4DsmonPdistTopNCtlIndex,
                                       INT4 *pi4NextDsmonPdistTopNCtlIndex)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetNextIndexDsmonPdistTopNCtlTable \n");

    pPdistTopNCtl = DsmonPdistTopNGetNextCtlIndex (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonPdistTopNCtlIndex =
        (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlIndex);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlPdistIndex
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlPdistIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlPdistIndex (INT4 i4DsmonPdistTopNCtlIndex,
                                   INT4 *pi4RetValDsmonPdistTopNCtlPdistIndex)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistTopNCtlPdistIndex \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistTopNCtlPdistIndex =
        (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlPdistIndex);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlRateBase
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlRateBase
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlRateBase (INT4 i4DsmonPdistTopNCtlIndex,
                                 INT4 *pi4RetValDsmonPdistTopNCtlRateBase)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistTopNCtlRateBase \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistTopNCtlRateBase =
        (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlRateBase);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlTimeRemaining
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlTimeRemaining (INT4 i4DsmonPdistTopNCtlIndex,
                                      INT4
                                      *pi4RetValDsmonPdistTopNCtlTimeRemaining)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistTopNCtlTimeRemaining \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    if (gi4MibSaveStatus == MIB_SAVE_IN_PROGRESS)
    {
        *pi4RetValDsmonPdistTopNCtlTimeRemaining =
            (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlDuration);

        return SNMP_SUCCESS;

    }

    *pi4RetValDsmonPdistTopNCtlTimeRemaining =
        (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlGeneratedReprts
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlGeneratedReprts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlGeneratedReprts (INT4 i4DsmonPdistTopNCtlIndex,
                                        UINT4
                                        *pu4RetValDsmonPdistTopNCtlGeneratedReprts)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistTopNCtlGeneratedReprts \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistTopNCtlGeneratedReprts =
        pPdistTopNCtl->u4DsmonPdistTopNCtlGenReports;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlDuration
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlDuration
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlDuration (INT4 i4DsmonPdistTopNCtlIndex,
                                 INT4 *pi4RetValDsmonPdistTopNCtlDuration)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistTopNCtlDuration \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistTopNCtlDuration =
        (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlDuration);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlRequestedSize
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlRequestedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlRequestedSize (INT4 i4DsmonPdistTopNCtlIndex,
                                      INT4
                                      *pi4RetValDsmonPdistTopNCtlRequestedSize)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistTopNCtlRequestedSize \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistTopNCtlRequestedSize =
        (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlReqSize);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlGrantedSize
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlGrantedSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlGrantedSize (INT4 i4DsmonPdistTopNCtlIndex,
                                    INT4 *pi4RetValDsmonPdistTopNCtlGrantedSize)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistTopNCtlGrantedSize \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistTopNCtlGrantedSize =
        (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlGranSize);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlStartTime
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlStartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlStartTime (INT4 i4DsmonPdistTopNCtlIndex,
                                  UINT4 *pu4RetValDsmonPdistTopNCtlStartTime)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistTopNCtlStartTime \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistTopNCtlStartTime =
        pPdistTopNCtl->u4DsmonPdistTopNCtlStartTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlOwner
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlOwner (INT4 i4DsmonPdistTopNCtlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValDsmonPdistTopNCtlOwner)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistTopNCtlOwner \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    MEMCPY (pRetValDsmonPdistTopNCtlOwner->pu1_OctetList,
            pPdistTopNCtl->au1DsmonPdistTopNCtlOwner, DSMON_MAX_OWNER_LEN);

    pRetValDsmonPdistTopNCtlOwner->i4_Length =
        STRLEN (pPdistTopNCtl->au1DsmonPdistTopNCtlOwner);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNCtlStatus
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                retValDsmonPdistTopNCtlStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNCtlStatus (INT4 i4DsmonPdistTopNCtlIndex,
                               INT4 *pi4RetValDsmonPdistTopNCtlStatus)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistTopNCtlStatus \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistTopNCtlStatus =
        (INT4) (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus);

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetDsmonPdistTopNCtlPdistIndex
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                setValDsmonPdistTopNCtlPdistIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistTopNCtlPdistIndex (INT4 i4DsmonPdistTopNCtlIndex,
                                   INT4 i4SetValDsmonPdistTopNCtlPdistIndex)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonPdistTopNCtlPdistIndex \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pPdistTopNCtl->u4DsmonPdistTopNCtlPdistIndex =
        (UINT4) (i4SetValDsmonPdistTopNCtlPdistIndex);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonPdistTopNCtlRateBase
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                setValDsmonPdistTopNCtlRateBase
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistTopNCtlRateBase (INT4 i4DsmonPdistTopNCtlIndex,
                                 INT4 i4SetValDsmonPdistTopNCtlRateBase)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonPdistTopNCtlRateBase \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pPdistTopNCtl->u4DsmonPdistTopNCtlRateBase =
        (UINT4) (i4SetValDsmonPdistTopNCtlRateBase);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonPdistTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                setValDsmonPdistTopNCtlTimeRemaining
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistTopNCtlTimeRemaining (INT4 i4DsmonPdistTopNCtlIndex,
                                      INT4
                                      i4SetValDsmonPdistTopNCtlTimeRemaining)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonPdistTopNCtlTimeRemaining \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem =
        (UINT4) (i4SetValDsmonPdistTopNCtlTimeRemaining);

    pPdistTopNCtl->u4DsmonPdistTopNCtlDuration =
        (UINT4) (i4SetValDsmonPdistTopNCtlTimeRemaining);

    pPdistTopNCtl->u4DsmonPdistTopNCtlStartTime = (UINT4) OsixGetSysUpTime ();

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonPdistTopNCtlRequestedSize
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                setValDsmonPdistTopNCtlRequestedSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistTopNCtlRequestedSize (INT4 i4DsmonPdistTopNCtlIndex,
                                      INT4
                                      i4SetValDsmonPdistTopNCtlRequestedSize)
{
    UINT4               u4Minimum = 0;

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhSetDsmonPdistTopNCtlRequestedSize \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    pPdistTopNCtl->u4DsmonPdistTopNCtlReqSize =
        (UINT4) (i4SetValDsmonPdistTopNCtlRequestedSize);

    u4Minimum =
        (i4SetValDsmonPdistTopNCtlRequestedSize <
         DSMON_MAX_TOPN_ENTRY) ? i4SetValDsmonPdistTopNCtlRequestedSize :
        DSMON_MAX_TOPN_ENTRY;

    pPdistTopNCtl->u4DsmonPdistTopNCtlGranSize = u4Minimum;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonPdistTopNCtlOwner
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                setValDsmonPdistTopNCtlOwner
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistTopNCtlOwner (INT4 i4DsmonPdistTopNCtlIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValDsmonPdistTopNCtlOwner)
{

    UINT4               u4Minimum = 0;

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonPdistTopNCtlOwner \n");

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN  Control Index \n");

        return SNMP_FAILURE;
    }

    u4Minimum =
        (pSetValDsmonPdistTopNCtlOwner->i4_Length <
         DSMON_MAX_OWNER_LEN) ? pSetValDsmonPdistTopNCtlOwner->
        i4_Length : DSMON_MAX_OWNER_LEN;

    MEMSET (pPdistTopNCtl->au1DsmonPdistTopNCtlOwner, DSMON_INIT_VAL,
            DSMON_MAX_OWNER_LEN);

    MEMCPY (pPdistTopNCtl->au1DsmonPdistTopNCtlOwner,
            pSetValDsmonPdistTopNCtlOwner->pu1_OctetList, u4Minimum);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetDsmonPdistTopNCtlStatus
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                setValDsmonPdistTopNCtlStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetDsmonPdistTopNCtlStatus (INT4 i4DsmonPdistTopNCtlIndex,
                               INT4 i4SetValDsmonPdistTopNCtlStatus)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;
    tDsmonPdistCtl     *pPdistCtlNode = NULL;

    INT4                i4Result = 0;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhSetDsmonPdistTopNCtlStatus \n");

    switch (i4SetValDsmonPdistTopNCtlStatus)
    {
        case CREATE_AND_WAIT:

            pPdistTopNCtl =
                DsmonPdistTopNAddCtlEntry (i4DsmonPdistTopNCtlIndex);

            if (pPdistTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_CRITICAL_TRC | DSMON_MEM_FAIL,
                           " Memory Allocation / \
                 RBTree Add Failed - Pdist TopN Control Table \n");

                return SNMP_FAILURE;
            }

            break;

        case ACTIVE:

            pPdistTopNCtl =
                DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

            if (pPdistTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Pdist TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            pPdistCtlNode =
                DsmonPdistGetCtlEntry (pPdistTopNCtl->
                                       u4DsmonPdistTopNCtlPdistIndex);

            if (pPdistCtlNode == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Pdist Control Index \n");

                return SNMP_FAILURE;
            }

            if (pPdistCtlNode->u4DsmonPdistCtlRowStatus != ACTIVE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Pdist Control entry is not in active state \n");

                return SNMP_FAILURE;
            }

            pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus = ACTIVE;

            pPdistTopNCtl->u4DsmonPdistTopNCtlStartTime =
                (UINT4) OsixGetSysUpTime ();

            pPdistTopNCtl->u4DsmonPdistTopNCtlTimeRem =
                pPdistTopNCtl->u4DsmonPdistTopNCtlDuration;

            break;

        case NOT_IN_SERVICE:

            pPdistTopNCtl =
                DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

            if (pPdistTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Pdist TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            DsmonPdistTopNDelTopNReport ((UINT4) i4DsmonPdistTopNCtlIndex);

            pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus = NOT_IN_SERVICE;

            break;

        case DESTROY:

            pPdistTopNCtl =
                DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

            if (pPdistTopNCtl == NULL)
            {
                DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                            Invalid Pdist TopN  Control Index \n");

                return SNMP_FAILURE;
            }

            i4Result = DsmonPdistTopNDelCtlEntry (pPdistTopNCtl);

            if (i4Result == OSIX_FAILURE)
            {
                DSMON_TRC (DSMON_DEBUG_TRC,
                           "SNMP Failure: Invalid Pdist TopN Control Index \
                 RBTreeRemove failed - Pdist TopN  Control Table\n");

                return SNMP_FAILURE;
            }

            break;
        default:
            break;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistTopNCtlPdistIndex
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                testValDsmonPdistTopNCtlPdistIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistTopNCtlPdistIndex (UINT4 *pu4ErrorCode,
                                      INT4 i4DsmonPdistTopNCtlIndex,
                                      INT4 i4TestValDsmonPdistTopNCtlPdistIndex)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonPdistTopNCtlPdistIndex \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceDsmonPdistCtlTable
        (i4TestValDsmonPdistTopNCtlPdistIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:PdistTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus == ACTIVE)
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistTopNCtlRateBase
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                testValDsmonPdistTopNCtlRateBase
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistTopNCtlRateBase (UINT4 *pu4ErrorCode,
                                    INT4 i4DsmonPdistTopNCtlIndex,
                                    INT4 i4TestValDsmonPdistTopNCtlRateBase)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonPdistTopNCtlRateBase \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((i4TestValDsmonPdistTopNCtlRateBase < pdistTopNPkts) ||
        (i4TestValDsmonPdistTopNCtlRateBase > pdistTopNHCOctets))
    {

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RateBase \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:PdistTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistTopNCtlTimeRemaining
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                testValDsmonPdistTopNCtlTimeRemaining
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistTopNCtlTimeRemaining (UINT4 *pu4ErrorCode,
                                         INT4 i4DsmonPdistTopNCtlIndex,
                                         INT4
                                         i4TestValDsmonPdistTopNCtlTimeRemaining)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonPdistTopNCtlTimeRemaining \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (i4TestValDsmonPdistTopNCtlTimeRemaining < DSMON_ZERO)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: TimeRem is less than Zero \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:PdistTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistTopNCtlRequestedSize
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                testValDsmonPdistTopNCtlRequestedSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistTopNCtlRequestedSize (UINT4 *pu4ErrorCode,
                                         INT4 i4DsmonPdistTopNCtlIndex,
                                         INT4
                                         i4TestValDsmonPdistTopNCtlRequestedSize)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonPdistTopNCtlRequestedSize \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {

        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if (i4TestValDsmonPdistTopNCtlRequestedSize <= DSMON_ZERO)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid value \
        Requested size is less than equal to zero \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;

        return SNMP_FAILURE;
    }

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:PdistTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistTopNCtlOwner
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                testValDsmonPdistTopNCtlOwner
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistTopNCtlOwner (UINT4 *pu4ErrorCode,
                                 INT4 i4DsmonPdistTopNCtlIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValDsmonPdistTopNCtlOwner)
{

    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhTestv2DsmonPdistTopNCtlOwner \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    if ((pTestValDsmonPdistTopNCtlOwner->i4_Length < DSMON_ZERO) ||
        (pTestValDsmonPdistTopNCtlOwner->i4_Length > DSMON_MAX_OWNER_LEN))
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid Owner length \n");

        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;

        return SNMP_FAILURE;
    }

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure:PdistTopNControl Entry not found \n");

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;

        return SNMP_FAILURE;
    }

    if (pPdistTopNCtl->u4DsmonPdistTopNCtlRowStatus == ACTIVE)
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: RowStatus is in active state \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2DsmonPdistTopNCtlStatus
 Input       :  The Indices
                DsmonPdistTopNCtlIndex

                The Object
                testValDsmonPdistTopNCtlStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2DsmonPdistTopNCtlStatus (UINT4 *pu4ErrorCode,
                                  INT4 i4DsmonPdistTopNCtlIndex,
                                  INT4 i4TestValDsmonPdistTopNCtlStatus)
{
    tDsmonPdistTopNCtl *pPdistTopNCtl = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhTestv2DsmonPdistTopNCtlStatus \n");
    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }
    if ((i4DsmonPdistTopNCtlIndex < DSMON_ONE) ||
        (i4DsmonPdistTopNCtlIndex > DSMON_MAX_CTL_INDEX))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: PdistTopN ControlIndex out of range \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;

        return SNMP_FAILURE;
    }

    pPdistTopNCtl = DsmonPdistTopNGetCtlEntry (i4DsmonPdistTopNCtlIndex);

    if (pPdistTopNCtl == NULL)
    {
        if (i4TestValDsmonPdistTopNCtlStatus == CREATE_AND_WAIT)
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    else
    {
        if ((i4TestValDsmonPdistTopNCtlStatus == ACTIVE) ||
            (i4TestValDsmonPdistTopNCtlStatus == NOT_IN_SERVICE) ||
            (i4TestValDsmonPdistTopNCtlStatus == DESTROY))
        {
            return SNMP_SUCCESS;
        }

        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: Invalid RowStatus \n");

        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }

    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2DsmonPdistTopNCtlTable
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2DsmonPdistTopNCtlTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : DsmonPdistTopNTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceDsmonPdistTopNTable
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                DsmonPdistTopNIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceDsmonPdistTopNTable (INT4 i4DsmonPdistTopNCtlIndex,
                                             INT4 i4DsmonPdistTopNIndex)
{
    tDsmonPdistTopN    *pPdistTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhValidateIndexInstanceDsmonPdistTopNTable \n");

    if (!(DSMON_IS_ENABLED ()))
    {
        DSMON_TRC (DSMON_DEBUG_TRC,
                   "SNMP Failure: DSMON / RMON2 Feature is shutdown.\n");

        return SNMP_FAILURE;
    }

    pPdistTopN =
        DsmonPdistTopNGetTopNEntry (i4DsmonPdistTopNCtlIndex,
                                    i4DsmonPdistTopNIndex);

    if (pPdistTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN Control Index or Pdist TopN Index \n");

        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexDsmonPdistTopNTable
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                DsmonPdistTopNIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexDsmonPdistTopNTable (INT4 *pi4DsmonPdistTopNCtlIndex,
                                     INT4 *pi4DsmonPdistTopNIndex)
{

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetFirstIndexDsmonPdistTopNTable \n");

    return nmhGetNextIndexDsmonPdistTopNTable (DSMON_ZERO,
                                               pi4DsmonPdistTopNCtlIndex,
                                               DSMON_ZERO,
                                               pi4DsmonPdistTopNIndex);

}

/****************************************************************************
 Function    :  nmhGetNextIndexDsmonPdistTopNTable
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                nextDsmonPdistTopNCtlIndex
                DsmonPdistTopNIndex
                nextDsmonPdistTopNIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexDsmonPdistTopNTable (INT4 i4DsmonPdistTopNCtlIndex,
                                    INT4 *pi4NextDsmonPdistTopNCtlIndex,
                                    INT4 i4DsmonPdistTopNIndex,
                                    INT4 *pi4NextDsmonPdistTopNIndex)
{

    tDsmonPdistTopN    *pPdistTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetNextIndexDsmonPdistTopNTable \n");

    pPdistTopN =
        DsmonPdistTopNGetNextTopNIndex (i4DsmonPdistTopNCtlIndex,
                                        i4DsmonPdistTopNIndex);

    if (pPdistTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   No valid next index \n");

        return SNMP_FAILURE;
    }

    *pi4NextDsmonPdistTopNCtlIndex =
        (INT4) (pPdistTopN->u4DsmonPdistTopNCtlIndex);

    *pi4NextDsmonPdistTopNIndex = (INT4) (pPdistTopN->u4DsmonPdistTopNIndex);

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNPDLocalIndex
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                DsmonPdistTopNIndex

                The Object
                retValDsmonPdistTopNPDLocalIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNPDLocalIndex (INT4 i4DsmonPdistTopNCtlIndex,
                                  INT4 i4DsmonPdistTopNIndex,
                                  INT4 *pi4RetValDsmonPdistTopNPDLocalIndex)
{
    tDsmonPdistTopN    *pPdistTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY,
               "FUNC:ENTRY nmhGetDsmonPdistTopNPDLocalIndex \n");

    pPdistTopN =
        DsmonPdistTopNGetTopNEntry (i4DsmonPdistTopNCtlIndex,
                                    i4DsmonPdistTopNIndex);

    if (pPdistTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN Control Index or Pdist TopN Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistTopNPDLocalIndex =
        (INT4) (pPdistTopN->u4DsmonPdistTopNPDLocalIndex);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNAggGroup
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                DsmonPdistTopNIndex

                The Object
                retValDsmonPdistTopNAggGroup
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNAggGroup (INT4 i4DsmonPdistTopNCtlIndex,
                              INT4 i4DsmonPdistTopNIndex,
                              INT4 *pi4RetValDsmonPdistTopNAggGroup)
{

    tDsmonPdistTopN    *pPdistTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistTopNAggGroup \n");

    pPdistTopN =
        DsmonPdistTopNGetTopNEntry (i4DsmonPdistTopNCtlIndex,
                                    i4DsmonPdistTopNIndex);

    if (pPdistTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN Control Index or Pdist TopN Index \n");

        return SNMP_FAILURE;
    }

    *pi4RetValDsmonPdistTopNAggGroup =
        (INT4) (pPdistTopN->u4DsmonPdistTopNAggGroupIndex);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNRate
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                DsmonPdistTopNIndex

                The Object
                retValDsmonPdistTopNRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNRate (INT4 i4DsmonPdistTopNCtlIndex,
                          INT4 i4DsmonPdistTopNIndex,
                          UINT4 *pu4RetValDsmonPdistTopNRate)
{

    tDsmonPdistTopN    *pPdistTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistTopNRate \n");

    pPdistTopN =
        DsmonPdistTopNGetTopNEntry (i4DsmonPdistTopNCtlIndex,
                                    i4DsmonPdistTopNIndex);

    if (pPdistTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN Control Index or Pdist TopN Index \n");

        return SNMP_FAILURE;
    }

    *pu4RetValDsmonPdistTopNRate = pPdistTopN->u4DsmonPdistTopNRate;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNRateOvfl
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                DsmonPdistTopNIndex

                The Object
                retValDsmonPdistTopNRateOvfl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNRateOvfl (INT4 i4DsmonPdistTopNCtlIndex,
                              INT4 i4DsmonPdistTopNIndex,
                              UINT4 *pu4RetValDsmonPdistTopNRateOvfl)
{

    /* deprecated */

    UNUSED_PARAM (i4DsmonPdistTopNCtlIndex);
    UNUSED_PARAM (i4DsmonPdistTopNIndex);
    UNUSED_PARAM (*pu4RetValDsmonPdistTopNRateOvfl);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetDsmonPdistTopNHCRate
 Input       :  The Indices
                DsmonPdistTopNCtlIndex
                DsmonPdistTopNIndex

                The Object
                retValDsmonPdistTopNHCRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetDsmonPdistTopNHCRate (INT4 i4DsmonPdistTopNCtlIndex,
                            INT4 i4DsmonPdistTopNIndex,
                            tSNMP_COUNTER64_TYPE *
                            pu8RetValDsmonPdistTopNHCRate)
{
    tDsmonPdistTopN    *pPdistTopN = NULL;

    DSMON_TRC (DSMON_FN_ENTRY, "FUNC:ENTRY nmhGetDsmonPdistTopNHCRate \n");
    pPdistTopN =
        DsmonPdistTopNGetTopNEntry (i4DsmonPdistTopNCtlIndex,
                                    i4DsmonPdistTopNIndex);

    if (pPdistTopN == NULL)
    {
        DSMON_TRC (DSMON_DEBUG_TRC, "SNMP Failure: \
                   Invalid Pdist TopN Control Index or Pdist TopN Index \n");

        return SNMP_FAILURE;
    }

    pu8RetValDsmonPdistTopNHCRate->msn =
        pPdistTopN->u8DsmonPdistTopNHCRate.u4HiWord;
    pu8RetValDsmonPdistTopNHCRate->lsn =
        pPdistTopN->u8DsmonPdistTopNHCRate.u4LoWord;

    return SNMP_SUCCESS;

}
