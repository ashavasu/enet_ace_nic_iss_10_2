/********************************************************************
 *                                                                  *
 *                                                                  *
 * Copyright (C) 2009 Aricent Inc . All Rights Reserved            *
 *                                                                  *
 * $Id: dsmnnpapi.c,v 1.3 2013/01/10 12:46:12 siva Exp $           *
 *                                                                  *
 * Description:                                                     *
 *          Contains invoking  functions for DSMON NP calls.
 *                                                                  *
 *******************************************************************/

/***************************************************************************
 *                                                                          
 *    Function Name       : DsmonFsDSMONAddFlowStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDSMONAddFlowStats
 *                                                                          
 *    Input(s)            : Arguments of FsDSMONAddFlowStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/
#ifndef __DSMON_NP_API_C
#define __DSMON_NP_API_C

#include "nputil.h"

UINT1
DsmonFsDSMONAddFlowStats (tPktHeader FlowStatsTuple)
{
    tFsHwNp             FsHwNp;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonNpWrFsDSMONAddFlowStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_DSMON_MOD,    /* Module ID */
                         FS_DSMON_ADD_FLOW_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pDsmonNpModInfo = &(FsHwNp.DsmonNpModInfo);
    pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONAddFlowStats;

    pEntry->FlowStatsTuple = FlowStatsTuple;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : DsmonFsDSMONCollectStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDSMONCollectStats
 *                                                                          
 *    Input(s)            : Arguments of FsDSMONCollectStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DsmonFsDSMONCollectStats (tPktHeader FlowStatsTuple, tRmon2Stats * pStatsCnt)
{
    tFsHwNp             FsHwNp;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonNpWrFsDSMONCollectStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_DSMON_MOD,    /* Module ID */
                         FS_DSMON_COLLECT_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pDsmonNpModInfo = &(FsHwNp.DsmonNpModInfo);
    pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONCollectStats;

    pEntry->FlowStatsTuple = FlowStatsTuple;
    pEntry->pStatsCnt = pStatsCnt;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : DsmonFsDSMONEnableProbe                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDSMONEnableProbe
 *                                                                          
 *    Input(s)            : Arguments of FsDSMONEnableProbe
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DsmonFsDSMONEnableProbe (UINT4 u4InterfaceIdx, UINT2 u2VlanId,
                         tPortList PortList)
{
    tFsHwNp             FsHwNp;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonNpWrFsDSMONEnableProbe *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_DSMON_MOD,    /* Module ID */
                         FS_DSMON_ENABLE_PROBE,    /* Function/OpCode */
                         u4InterfaceIdx,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         1);    /* No. of PortList Parms */
    pDsmonNpModInfo = &(FsHwNp.DsmonNpModInfo);
    pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONEnableProbe;

    pEntry->u4InterfaceIdx = u4InterfaceIdx;
    pEntry->u2VlanId = u2VlanId;
    pEntry->PortList = PortList;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : DsmonFsDSMONDisableProbe                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDSMONDisableProbe
 *                                                                          
 *    Input(s)            : Arguments of FsDSMONDisableProbe
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DsmonFsDSMONDisableProbe (UINT4 u4InterfaceIdx, UINT2 u2VlanId,
                          tPortList PortList)
{
    tFsHwNp             FsHwNp;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonNpWrFsDSMONDisableProbe *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_DSMON_MOD,    /* Module ID */
                         FS_DSMON_DISABLE_PROBE,    /* Function/OpCode */
                         u4InterfaceIdx,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         1);    /* No. of PortList Parms */
    pDsmonNpModInfo = &(FsHwNp.DsmonNpModInfo);
    pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONDisableProbe;

    pEntry->u4InterfaceIdx = u4InterfaceIdx;
    pEntry->u2VlanId = u2VlanId;
    pEntry->PortList = PortList;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : DsmonFsDSMONHwSetStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDSMONHwSetStatus
 *                                                                          
 *    Input(s)            : Arguments of FsDSMONHwSetStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DsmonFsDSMONHwSetStatus (UINT4 u4DSMONStatus)
{
    tFsHwNp             FsHwNp;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonNpWrFsDSMONHwSetStatus *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_DSMON_MOD,    /* Module ID */
                         FS_DSMON_HW_SET_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pDsmonNpModInfo = &(FsHwNp.DsmonNpModInfo);
    pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONHwSetStatus;

    pEntry->u4DSMONStatus = u4DSMONStatus;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : DsmonFsDSMONHwGetStatus                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDSMONHwGetStatus
 *                                                                          
 *    Input(s)            : Arguments of FsDSMONHwGetStatus
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DsmonFsDSMONHwGetStatus ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_DSMON_MOD,    /* Module ID */
                         FS_DSMON_HW_GET_STATUS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : DsmonFsDSMONRemoveFlowStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsDSMONRemoveFlowStats
 *                                                                          
 *    Input(s)            : Arguments of FsDSMONRemoveFlowStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
DsmonFsDSMONRemoveFlowStats (tPktHeader FlowStatsTuple)
{
    tFsHwNp             FsHwNp;
    tDsmonNpModInfo    *pDsmonNpModInfo = NULL;
    tDsmonNpWrFsDSMONRemoveFlowStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_DSMON_MOD,    /* Module ID */
                         FS_DSMON_REMOVE_FLOW_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pDsmonNpModInfo = &(FsHwNp.DsmonNpModInfo);
    pEntry = &pDsmonNpModInfo->DsmonNpFsDSMONRemoveFlowStats;

    pEntry->FlowStatsTuple = FlowStatsTuple;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* __DSMON_NP_API_C */
