/* $Id: qosproto.h,v 1.54 2018/01/04 09:55:15 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosproto.h                                      */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-PROTO                                       */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contain the functions prototype for   */
/*                          the Aricent QoS Module.                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_PROTO_H__
#define __QOS_PROTO_H__ 


/*****************************************************************************/
/* FileName : qossys.c  functions prototype                                  */
/*****************************************************************************/
/*****************************************************************************/
/*                          COMMON FUNCTIONS                                 */
/*****************************************************************************/
VOID QoSInit PROTO  ((INT1 *i1Params));
INT4 QoSStart PROTO ((VOID));
INT4 QoSShutdown PROTO ((VOID));
INT4 QoSEnable PROTO ((VOID));
INT4 QoSDisable PROTO ((VOID));
VOID QoSInitGlobal PROTO ((VOID));
VOID QoSDeleteMemPools PROTO ((VOID));
INT4 QoSCreateMemPools PROTO ((VOID));

INT4 QoSConfigMeter PROTO ((UINT4 u4CfgType));
INT4 QoSConfigPolicy PROTO ((UINT4 u4CfgType));
INT4 QoSConfigClsMap PROTO ((UINT4 u4CfgType));
INT4 QoSConfigSched PROTO ((UINT4 u4CfgType));
INT4 QoSConfigQueue PROTO ((UINT4 u4CfgType));
INT4 QoSConfigQMap PROTO ((UINT4 u4CfgType));
INT4 QoSConfigHierarchy PROTO ((UINT4 u4CfgType));
INT4 QoSConfigPDUP PROTO ((UINT4 u4CfgType));
INT4 QoSConfigPriMap PROTO ((UINT4 u4CfgType));
INT4 QoSConfigPbitPreference PROTO ((UINT4 u4CfgType));
INT4 QoSConfigQueueStat PROTO ((UINT4 u4CfgType));

INT4 QosxUtlFillPriToQidMap PROTO ((UINT4 u4IfIndex, UINT4 *pu4QueueId));

/******************************************************************************/                          
/*                                 QOS STD CALL                                 */
/******************************************************************************/


tQosStdClfrElementEntry *  QosGetClfrElementEntry PROTO ((UINT4 , UINT4));
tQosStdClfrEntry *  QosGetClfrEntry PROTO ((UINT4));
tIssL3FilterEntry *  QosGetMultiFieldClfrEntry PROTO ((UINT4 u4Index));
INT4 QosGetTBParamEntry PROTO ((UINT4 u4Index));
INT4 QosGetActionEntry PROTO ((UINT4 u4Index));
INT4 QosGetCountActEntry PROTO ((UINT4 u4Index));
INT4 QosGetAlgoDropEntry PROTO ((UINT4 u4Index));
INT4 QosGetRandomDropEntry PROTO ((UINT4 u4Index));
INT4 QosGetSchedularEntry PROTO ((UINT4 u4Index));

tQosStdSchedulerEntry * QosGetSchedulerEntry PROTO ((UINT4 u4Index));
tQosStdMinRateEntry * QosGetMinRateEntry PROTO ((UINT4 u4Index));
tQosStdMaxRateEntry *  QosGetMaxRateEntry PROTO ((UINT4 u4Index, UINT4 u4MaxRateLevel));

INT4
QoSGetNextClsMapTblEntry PROTO ((UINT4,UINT4 *,UINT4 ,UINT4 *));
extern tIssL3FilterEntry* IssGetL3FilterEntry PROTO ((INT4));
tIssL3FilterEntry  *IssExtGetL3FilterEntry PROTO ((INT4 );)
INT4 QoSGetNextValidClfrElemIndex PROTO ((UINT4,UINT4 *, UINT4,UINT4 *));
INT4 QoSGetNextClfrEntryValidateIndex PROTO ((UINT4,UINT4 *));
INT4 QoSValidateClfrEntry PROTO ((tQosStdClfrEntry * pClfrNode));

INT4
QosGetTBParamTypeFromOid PROTO ((UINT4 *u4Type, tSNMP_OID_TYPE * Oid));
tQosStdClfrEntry   *
QoSUtlGetClfrEntry PROTO ((UINT4 u4DServClfrId));

INT4
QoSClfrCmpFun PROTO ((tRBElem * e1, tRBElem * e2));

INT4
QoSClfrRBTFreeNodeFn PROTO((tRBElem * pRBElem, UINT4 u4Arg));

tQosStdClfrElementEntry   *
QoSUtlGetClfrElementEntry PROTO ((UINT4 u4DServClfrId,UINT4 u4DServClfrElementId));

tQosStdClfrElementEntry   *
QoSCreateClfrElementEntry PROTO ((UINT4 u4DServClfrId, UINT4 u4DServClfrElementId));
tQosStdClfrEntry   *
QoSCreateClfrEntry PROTO ((UINT4 u4DServClfrId));
INT4
QoSDeleteClfrElementEntry PROTO ((tQosStdClfrElementEntry * pClfrElementNode));

INT4
QoSValidateClfrElementEntry PROTO ((tQosStdClfrElementEntry * pClfrElementNode));

INT4
QoSDeleteDataPathEntry PROTO ((tQosStdDataPathEntry * pDataPathEntryNode));

INT4
QoSDeleteClfrEntry PROTO ((tQosStdClfrEntry * pClfrNode));

tQosStdQEntry * QosGetQEntry PROTO ((UINT4 u4DServQId));

INT4 QoSDataPathEntryCmpFun  PROTO ((tRBElem * e1, tRBElem * e2));

tQosStdDataPathEntry   *
QoSUtlGetDataPathEntry PROTO ((UINT4 i4IfIndex,UINT4 i4DSDataPathIfDir));

tQosStdDataPathEntry * QoSCreateDataPathEntry PROTO ((UINT4 i4IfIndex, UINT4 i4DSDataPathIfDir));
tQosStdMeterEntry      * QoSCreateStdMeterTblEntry PROTO ((UINT4 u4MeterId));
tQosStdMeterEntry      * QoSUtlGetStdMeterNode PROTO ((UINT4 u4MeterId));

tQosStdSchedulerEntry * QoSCreateStdSchedulerTblEntry PROTO ((UINT4 u4SchedulerId));
tQosStdMinRateEntry * QoSCreateStdMinRateTblEntry PROTO ((UINT4 u4MinRateId));
tQosStdMaxRateEntry * QoSCreateStdMaxRateTblEntry PROTO ((UINT4 u4DSMaxRateId, UINT4 u4DSMaxRateLevel));
INT4 QoSDeleteStdSchedulerTblEntry PROTO ((tQosStdSchedulerEntry * pSchedulerEntry));
INT4 QoSDeleteStdMinRateTblEntry PROTO ((tQosStdMinRateEntry * pMinRateEntry));
INT4 QoSDeleteStdMaxRateTblEntry PROTO ((tQosStdMaxRateEntry * pMaxRateEntry));
INT4 QoSValidateDataPathEntry PROTO ((tQosStdDataPathEntry * pDataPathEntryNode));

INT4
QoSGetNextValidDataPathIndex PROTO ((INT4 i4IfIndex ,INT4 *pi4NextIfIndex ,
                  INT4 i4DataPathIfDir ,INT4 *pi4NextDataPathIfDir));

INT4
QosIsTypeValid PROTO ((UINT4, UINT4 ,UINT4,UINT4,UINT4));

INT4 QosGetMeterEntry PROTO ((UINT4 u4Index ));

tQosStdDataPathEntry * QosGetDataPathEntry PROTO ((INT4 i4IfIndex,INT4 i4DSDataPathIfDir ));


/*****************************************************************************/
/* FileName : qosutl.c  functions prototype                                  */
/*****************************************************************************/
/*****************************************************************************/
/*                          COMMON FUNCTIONS                                 */
/*****************************************************************************/
INT4 QoSUtlValidateTableName PROTO ((tSNMP_OCTET_STRING_TYPE *pSNMPOctSrt, 
                                     UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateIfIndex PROTO ((UINT4 u4IfIndex, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateVlanId PROTO ((UINT4 u4VlanId, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateClass PROTO ((UINT4 u4ClassId, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidatePriMapTblIdxInst PROTO ((UINT4 u4FsQoSPriorityMapID,
                                            UINT4 *pu4ErrorCode));


INT4
QoSUtlValidateSched PROTO ((INT4 i4IfIndex, UINT4 u4SchedId, 
                            UINT4 *pu4ErrorCode));

INT4
QoSUtlValidateQTemp PROTO ((UINT4 u4QTempId, UINT4 *pu4ErrorCode));

INT4
QoSUtlValidatePortTblIdxInst PROTO ((INT4 i4IfIndex,INT4 i4PktType,
                                     UINT4 *pu4ErrorCode));
 
INT4
QoSUtlUpdateFilterRefCount PROTO ((UINT4 u4FilterId, UINT4 u4FilterType,
                                   UINT4 u4Action));

INT4
RbWalkAction PROTO ((tRBElem * e, eRBVisit visit, UINT4 level,
                     VOID *arg, VOID *out));

INT4 
QoSUtlIsUniqueName PROTO ((tSNMP_OCTET_STRING_TYPE 
                           *pTestValFsQoSPriorityMapName, UINT4 u4TblType));

INT4
QoSUtlQTempIsUniqueName PROTO ((tSNMP_OCTET_STRING_TYPE *pSnmpStr));

INT4
QoSUtlShapeIsUniqueName PROTO ((tSNMP_OCTET_STRING_TYPE *pSnmpStr));

INT4
QoSUtlValidatePriority PROTO ((UINT1 u1PriorityType, UINT4 u4PriorityVal));

INT4
QosUtilRBTreeStub PROTO ((VOID));
    
VOID
QoSUtilOsixBitlistIsBitSet PROTO ((UINT1 * au1BitArray, 
                                    UINT2 u2BitNumber, INT4 i4ArraySize, BOOL1 *bResult));

VOID
QoSUtilOsixBitlistSetBit PROTO ((UINT1 * au1BitArray, 
                                    UINT2 u2BitNumber, INT4 i4ArraySize));

INT4
QoSUtlValidateL2FilterIdForClassMapId PROTO ((UINT4 u4ClassMapId,UINT4 u4L2FilterId,
                                                UINT4 *pu4ErrorCode));
INT4
QoSUtlValidateL3FilterIdForClassMapId PROTO ((UINT4 u4ClassMapId,UINT4 u4L3FilterId,
                                                UINT4 *pu4ErrorCode));
/*****************************************************************************/
/*                     PRIORITY MAP TABLE FUNCTIONS                          */
/*****************************************************************************/
INT4 QoSInPriMapCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
tQoSInPriorityMapNode * QoSUtlGetPriorityMapNode PROTO ((UINT4 u4PriMapId));
tQoSInPriorityMapNode * QoSCreatePriMapTblEntry PROTO ((UINT4 u4PriMapId)); 
INT4 QoSDeletePriMapTblEntry PROTO ((tQoSInPriorityMapNode *pInPriMapNode));
INT4 QoSGetNextInPriMapTblEntryIndex PROTO ((UINT4 u4CurrentIndex, 
                                             UINT4 *pu4NextIndex));
INT4
QoSUtlValidatePriorityMapId PROTO ((UINT4 u4PriMapId, UINT4 *pu4ErrorCode));
INT4
QoSInPriTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

INT4
QoSAddPriMapUniTblEntry PROTO ((tQoSInPriorityMapNode *pPriMapNode));
INT4
QoSInPriMapUniCmpFun PROTO ((tRBElem * e1, tRBElem * e2));

tQoSInPriorityMapNode *
QoSUtlGetPriorityMapUniNode PROTO ((UINT4 u4IfIndex, UINT2 u2VlanId,
                                    UINT1 u1InPri, UINT1 u1InPriType));

INT4
QoSDeletePriMapUniTblEntry PROTO ((tQoSInPriorityMapNode * pPriMapNode));

INT4 QoSIsDefPriorityMapTblEntry PROTO ((tQoSInPriorityMapNode *pPriMapNode));
INT4 QoSAddDefPriorityMapTblEntries PROTO((VOID));

/*****************************************************************************/
/*                     VLAN QUEUEING MAP TABLE FUNCTIONS                             */
/*****************************************************************************/
INT4 QoSVlanQueueingEnable PROTO ((VOID));
INT4 QoSVlanQueueingDisable PROTO ((VOID));
INT4 QosUtilCreateSubscriberQueues PROTO ((VOID));
INT4 QosUtilCreateSubscriberQueuesForPort PROTO ((UINT4 u4Port));
INT4 QoSConfigVlanMapTables PROTO ((UINT4 u4CfgType));
INT4 QoSHwWrSubscriberQueueCreate PROTO ((INT4 i4IfIndex, UINT4 u4QId));
INT4 QoSConfigVlanMap PROTO ((UINT4 u4CfgType));
INT4 QoSUtlValidateVlanQMapTblIdxInst PROTO ((UINT4 u4FsQoSVlanMapID,
                                            UINT4 *pu4ErrorCode));

INT4
QoSVlanQMapCmpFun PROTO ((tRBElem * e1, tRBElem * e2));

tQoSInVlanMapNode * QoSUtlGetVlanQMapNode PROTO ((UINT4 u4VlanMapId));

tQoSInVlanMapNode * QoSCreateVlanQMapTblEntry PROTO ((UINT4 u4VlanMapId));

INT4 QoSDeleteVlanQMapTblEntry PROTO ((tQoSInVlanMapNode *pInVlanMapNode));

INT4 QoSGetNextVlanQMapTblEntryIndex PROTO ((UINT4 u4CurrentIndex,
                                             UINT4 *pu4NextIndex));
INT4
QoSUtlValidateVlanQMapId PROTO ((UINT4 u4PriMapId, UINT4 *pu4ErrorCode));

VOID
QoSCheckAndUpdateVlanMapIfIndex PROTO ((UINT4 u4CLASS, UINT2 *u2vlanId));

INT4
QoSHwWrMapVlanQMaptoClass PROTO ((tQoSInVlanMapNode *pVlanQMapNode, UINT1 u1Flag));


/*****************************************************************************/
/*                     CLASS MAP TABLE FUNCTIONS                             */
/*****************************************************************************/
INT4 QoSClsMapCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
tQoSClassMapNode * QoSUtlGetClassMapNode PROTO ((UINT4 u4ClsMapId));
tQoSClassMapNode * QoSCreateClsMapTblEntry PROTO ((UINT4 u4ClsMapId)); 
INT4 QoSValidateClsMapTblEntry PROTO ((tQoSClassMapNode *pClsMapNode));
INT4 QoSDeleteClsMapTblEntry PROTO ((tQoSClassMapNode *pClsMapNode));
INT4 QoSGetNextClsMapTblEntryIndex PROTO ((UINT4 u4CurrentIndex, 
                                           UINT4 *pu4NextIndex));
INT4 QoSUtlValidateL2Filter PROTO ((UINT4 u4L2FilterId, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateL3Filter PROTO ((UINT4 u4L3FilterId, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateClsMapTblIdxInst PROTO ((UINT4 u4FsQoSClassMapId, 
                                            UINT4 *pu4ErrorCode));
INT4
QoSClsMapTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

INT4 QoSIsDefClassMapTblEntry  PROTO ((tQoSClassMapNode *pClsMapNode));
INT4 QoSAddDefClassMapTblEntries PROTO ((VOID));
/*****************************************************************************/
/*               CLASS 2 PRIORITY MAP TABLE FUNCTIONS                        */
/*****************************************************************************/
INT4 QoSCls2PriMapCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
tQoSClass2PriorityNode * QoSUtlGetClass2PriorityNode 
                                         PROTO ((UINT4 u4Cls2PriId));
tQoSClass2PriorityNode * QoSCreateCls2PriTblEntry PROTO ((UINT4 u4Cls2PriId)); 
INT4 QoSValidateCls2PriTblEntry PROTO ((tQoSClass2PriorityNode *pCls2PriNode));
INT4 QoSDeleteCls2PriTblEntry PROTO ((tQoSClass2PriorityNode *pDelCls2PriNode));

INT4 QoSGetNextCls2PriTblEntryIndex PROTO ((UINT4 u4CurrentIndex,
                                            UINT4 *pu4NextIndex));
INT4 
QoSUtlValidateCls2PriMapTblIdxInst PROTO ((UINT4 u4FsQoSClassToPriorityCLASS,
                                           UINT4 *pu4ErrorCode));
INT4
QoSCls2PriTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

/*****************************************************************************/
/*                        METER TABLE FUNCTIONS                              */
/*****************************************************************************/
INT4 QoSMeterCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSStdMeterCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
tQoSMeterNode * QoSUtlGetMeterNode PROTO ((UINT4 u4MeterId));
tQoSMeterNode * QoSCreateMeterTblEntry PROTO ((UINT4 u4MeterId));
INT4 QoSValidateMeterTblEntry PROTO ((tQoSMeterNode *pMeterNode));
INT4 QoSDeleteMeterTblEntry PROTO ((tQoSMeterNode *pDelMeterNode));
INT4 QoSDeleteStdMeterTblEntry PROTO ((tQosStdMeterEntry *pDelMeterNode));
INT4 QoSGetNextMeterTblEntryIndex PROTO ((UINT4 u4CurrentIndex,
                                          UINT4 *pu4NextIndex));
INT4 QoSUtlValidatedNxtMeterId PROTO ((UINT4 u4MeterNextId,
                                       UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateMeterTblIdxInst PROTO ((UINT4 u4FsQoSMeterId, 
                                           UINT4 *pu4ErrorCode));
INT4
QoSMeterTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 
QoSStdMeterTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

INT4 
QoSValidateMeterTypeParams PROTO ((UINT4 u4MeterId, UINT4 u4ParamType, 
                                   UINT4 *pu4ErrorCode));


/*****************************************************************************/
/*                     POLICY MAP TABLE FUNCTIONS                            */
/*****************************************************************************/
INT4 QoSPlyMapCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
tQoSPolicyMapNode * QoSUtlGetPolicyMapNode PROTO ((UINT4 u4PlyMapId));
tQoSPolicyMapNode * QoSCreatePlyMapTblEntry PROTO ((UINT4 u4PlyMapId)); 
INT4 QoSValidatePlyMapTblEntry PROTO ((tQoSPolicyMapNode *pPlyMapNode));
INT4 QoSDeletePlyMapTblEntry PROTO ((tQoSPolicyMapNode *pPlyMapNode));
INT4 QoSGetNextPlyMapTblEntryIndex PROTO ((UINT4 u4CurrentIndex, 
                                             UINT4 *pu4NextIndex));
INT4 
QoSUtlValidatePlyMapClassId PROTO ((UINT4 u4PlyMapClass, UINT4 *pu4ErrorCode));
INT4 QoSUtlValidateMeter PROTO ((UINT4 u4MeterId, UINT4 *pu4ErrorCode));

INT4
QoSValidateInProConfActFlag PROTO ((UINT4 u4PlyMapId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pSNMPOctSrtInActFlag, UINT4 *pu4ErrorCode));
INT4
QoSValidateInProExcActFlag PROTO ((UINT4 u4PlyMapId,
                                   tSNMP_OCTET_STRING_TYPE * 
                                   pSNMPOctSrtInActFlag, UINT4 *pu4ErrorCode));

INT4
QoSValidateInProConfAction PROTO ((UINT4 u4PlyMapId, INT4 i4Value,
                                   UINT1 u1Type,     UINT4 *pu4ErrorCode));

INT4
QoSValidateInProExcAction PROTO ((UINT4 u4PlyMapId, INT4 i4Value,
                                  UINT1 u1Type,     UINT4 *pu4ErrorCode));
INT4
QoSValidateOutProfileActionFlag PROTO ((UINT4 u4PlyMapId, 
                               tSNMP_OCTET_STRING_TYPE * pSNMPOctSrtOutActFlag,
                               UINT4  *pu4ErrorCode));
INT4
QoSValidateOutProfileAction PROTO ((UINT4 u4PlyMapId, INT4 i4Value,
                                    UINT1 u1Type, UINT4 *pu4ErrorCode));
INT4
QoSUtlValidatePolicyTblIdxInst PROTO ((UINT4 u4FsQoSPolicyMapId,
                                       UINT4 *pu4ErrorCode));
INT4
QoSPlyMapTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSAddDefPolicyMapTblEntries PROTO ((VOID));
INT4 QoSIsDefPolicyMapTblEntry PROTO((tQoSPolicyMapNode  *pPlyMapNode));

#ifdef WLC_WANTED
INT4 QosWssWrMapClassToPolicy PROTO((tQoSPolicyMapNode *pPlyMapNode,
                                     INT4 i4SetValFsQoSPolicyMapStatus));
#endif

INT4 QoSUtlGetPolicyMapId PROTO((UINT4 u4PrevPlyId, UINT4 *u4CurPlyId));

VOID QosUtlDeleteExistMeterStats PROTO((VOID));
VOID QoSResetPlyMap PROTO((tQoSPolicyMapNode *pPlyMapNode));

/*****************************************************************************/
/*                     CLASS INFO TABLE FUNCTIONS                            */
/*****************************************************************************/
INT4 QoSClsInfoCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSTestClsInfoAddPolicy PROTO ((UINT4 u4ClassId, UINT4 u4PlyMapId,
                                     UINT4 *pu4ErrorCode));
INT4 QoSClsInfoTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
tQoSClassInfoNode * QoSUtlGetClassInfoNode PROTO ((UINT4 u4ClassId));
INT4 QoSClsInfoDelPolicy PROTO ((tQoSPolicyMapNode *pPlyMapNode));
INT4
QoSClsInfoAddPolicy PROTO ((UINT4 u4ClassId, tQoSPolicyMapNode *pPlyMapNode));
INT4 QoSClsInfoDelFilters PROTO ((tQoSClassMapNode *pClsMapNode));
INT4
QoSClsInfoAddFilters PROTO ((UINT4 u4ClassId, tQoSClassMapNode *pClsMapNode));
INT4 QoSGetNextClsInfoTblEntryIndex PROTO ((UINT4 u4CurrentIndex, 
                                       UINT4 *pu4NextIndex));

INT4 QoSDeleteClsInfoTblEntry PROTO ((tQoSClassInfoNode  *pClsInfoNode));

tQoSClassInfoNode * QoSCreateClsInfoTblEntry PROTO ((UINT4 u4ClassId));

INT4 QoSTestClsInfoDelFilters (tQoSClassMapNode *pClsMapNode , INT4 i4RowStatus);


/*****************************************************************************/
/*                   Q TEMPLATE TABLE FUNCTIONS                              */
/*****************************************************************************/
tQoSQTemplateNode *
QoSUtlGetQTempNode PROTO ((UINT4 u4QTempId));
INT4 
QoSGetNextQTempTblEntryIndex PROTO ((UINT4 u4CurrentIndex,
                                     UINT4 *pu4NextIndex));
tQoSQTemplateNode *
QoSCreateQTempTblEntry PROTO ((UINT4 u4QTemplateId));
INT4 
QoSDeleteQTempTblEntry PROTO ((tQoSQTemplateNode *pQTempNode));
INT4 
QoSUtlValidateQTempTblIdxInst PROTO ((UINT4 u4FsQoSQTemplateId, 
                                      UINT4 *pu4ErrorCode));
INT4 QoSAddDefQTempTblEntries PROTO ((VOID));
INT4 QoSIsDefQTempTblEntry PROTO ((tQoSQTemplateNode  *pQTempNode));
INT4 QoSValidateDeleteQTempTblEntry PROTO((tQoSQTemplateNode * pQTempNode));
/*****************************************************************************/
/*                   RD CFG TABLE FUNCTIONS                                  */
/*****************************************************************************/
tQoSRDCfgNode *
QoSUtlGetRDCfgNode PROTO ((UINT4 u4FsQoSQTemplateId, 
                           INT4 i4FsQoSRandomDetectCfgDP));

INT4 
QoSGetNextRDCfgTblEntryIndex PROTO ((UINT4 u4CurrentIndex1, 
                                     UINT4 *pu4NextIndex1, 
                                     INT4 i4CurrentIndex2, 
                                     INT4 *pi4NextIndex2));
tQoSRDCfgNode *
QoSCreateRDCfgTblEntry PROTO ((UINT4 u4FsQoSQTemplateId, 
                               INT4  i4FsQoSRandomDetectCfgDP));
INT4 
QoSDeleteRDCfgTblEntry PROTO ((tQoSRDCfgNode *pRDCfgNode));

INT4 
QoSUtlValidateRDCfgTblIdxInst PROTO ((UINT4 u4FsQoSQTemplateId, 
                                      INT4 i4FsQoSRandomDetectCfgDP, 
                                      UINT4 *pu4ErrorCode));

INT4 
QoSUtlCheckRDTblEntry PROTO ((UINT4 u4QTempId));

INT4
QoSUtlValidateRDCfgActionFlag PROTO ((UINT4 u4FsQoSQTemplateId,
                                      INT4 i4FsQoSRandomDetectCfgDP,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pSNMPOctSrtOutActFlag,
                                      UINT4 *pu4ErrorCode));
INT4
QoSUtlValidateQTemplateSize PROTO ((UINT4 u4FsQoSQTemplateId,
                                    UINT4 u4TestValFsQoSQTemplateSize,
                                    UINT4 *pu4ErrorCode));

    
/*****************************************************************************/
/*                 SHAPE TEMPLATE TABLE FUNCTIONS                            */
/*****************************************************************************/
tQoSShapeTemplateNode *
QoSUtlGetShapeTempNode PROTO ((UINT4 u4ShapeTempId));
INT4 
QoSGetNextShapeTempTblEntryIndex PROTO ((UINT4 u4CurrentIndex, 
                                         UINT4 *pu4NextIndex));
tQoSShapeTemplateNode *
QoSCreateShapeTempTblEntry PROTO ((UINT4 u4ShapeTemplateId));
INT4 
QoSDeleteShapeTempTblEntry PROTO ((tQoSShapeTemplateNode  *pShapeTempNode));
INT4 
QoSUtlValidateShapeTempTblIdxInst PROTO ((UINT4 u4FsQoSShapeTemplateId, 
                                          UINT4 *pu4ErrorCode));


/*****************************************************************************/
/*                     Q TABLE FUNCTIONS                                     */
/*****************************************************************************/
INT4 QoSQCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSQTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

tQoSQNode *
QoSUtlGetQNode PROTO ((INT4 i4IfIndex, UINT4 u4QId));

tQoSQNode *
QoSCreateQTblEntry PROTO ((INT4 i4IfIndex, UINT4 u4QId)); 

INT4 
QoSGetNextQTblEntryIndex PROTO ((INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                 UINT4 u4QId, UINT4 *pu4NextQId));

INT4 
QoSDeleteQTblEntry PROTO ((tQoSQNode *pQNode));

INT4
QoSUtlValidateQTblIdxInst PROTO ((INT4 i4IfIndex, UINT4 u4FsQoSQId,
                                  UINT4 *pu4ErrorCode));

INT4                    
QoSUtlCheckQTempReference PROTO ((UINT4 u4QTempId));

INT4
QoSValidateQTblEntry PROTO ((tQoSQNode *pQNode));

INT4 QoSAddDefQTblEntries PROTO ((UINT4 u4Port));
INT4 QoSIsDefQTblEntry PROTO ((tQoSQNode *   pQNode));
INT4 QoSUtilTestFsQoSShapeTemplateCIR PROTO((UINT4 *pu4ErrorCode, 
                                             UINT4 u4FsQoSMeterId,
                                             UINT4 u4TestValFsQoSMeterCIR,
                                             UINT4 u4TestValFsQoSMeterEIR));
INT4 QoSUtilTestFsQoSShapeTemplateEIR PROTO((UINT4 *pu4ErrorCode,
                                             UINT4 u4FsQoSShapeTemplateId,
                                       UINT4 u4TestValFsQoSShapeTemplateEIR,
                                       UINT4 u4TestValFsQoSShapeTemplateCIR));
VOID QosMainProcessQMsgEvent PROTO((VOID));

/*****************************************************************************/
/*                     Q TABLE FUNCTIONS                                     */
/*****************************************************************************/
INT4 DiffServQCmpFun PROTO ((tRBElem *e1, tRBElem *e2));

tQosStdQEntry          *
DiffServCreateQTblEntry PROTO ((UINT4 u4QId));

INT4
DiffServDeleteQTblEntry PROTO ((tQosStdQEntry * pQNode));


tQosStdQEntry *
QoSUtlGetStdQEntry PROTO ((UINT4 u4DiffServQId));

/*****************************************************************************/
/*                 Min Rate Max Rate TABLE FUNCTIONS                         */
/*****************************************************************************/
INT4 DiffServStdQCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSSchedulerCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSMinRateCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSMaxRateCmpFun PROTO ((tRBElem *e1, tRBElem *e2));


/*****************************************************************************/
/*                     SCHEDULER TABLE FUNCTIONS                             */
/*****************************************************************************/
INT4 QoSSchedCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSSchedMapCmpFun PROTO ((tRBElem *e1, tRBElem *e2));

INT4 QoSSchedTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

tQoSSchedNode * 
QoSUtlGetSchedNode PROTO ((INT4 i4IfIndex, UINT4 u4SchedId));

INT4 
QoSGetNextSchedTblEntryIndex PROTO ((INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                     UINT4 u4SchedId, UINT4 *pu4NextSchedId));

tQoSSchedNode *
QoSCreateSchedTblEntry PROTO ((INT4 i4IfIndex, UINT4 u4SchedId));

INT4 
QoSDeleteSchedTblEntry PROTO ((tQoSSchedNode *pSchedNode));

INT4
QoSUtlValidateSchedTblIdxInst PROTO ((INT4 i4IfIndex,
                                      UINT4 u4FsQoSSchedulerId, 
                                      UINT4 *pu4ErrorCode));

INT4
QoSUtlValidateShapeId PROTO ((UINT4 u4ShapeId, UINT4 *pu4ErrorCode));
INT4 QoSAddDefSchedTblEntries PROTO ((UINT4 u4Port));
INT4 QoSDelDefSchedTblEntries PROTO ((UINT4 u4Port));
INT4 QoSDelDefQTblEntries PROTO ((UINT4 u4Port));
INT4 QoSDelHwSchedTblEntries PROTO ((UINT4 u4Port));
INT4 QoSDelHwQTblEntries PROTO ((UINT4 u4Port));
INT4 QoSAddHwSchedTblEntries PROTO ((UINT4 u4Port));
INT4 QoSAddHwQTblEntries PROTO ((UINT4 u4Port));
INT4
QoSAddDefHierarchyTblEntries PROTO ((UINT4 u4Port, INT1 i1Levels));
INT4
QoSCreateHLSchedulers PROTO ((UINT4 u4Port, INT1 i1NumSchedulers,UINT4 *pu4SchedCount,UINT1 u1HLLevel));

INT4 QoSIsDefSchedTblEntry PROTO ((tQoSSchedNode *pSchedNode));
/*****************************************************************************/
/*                     Q MAP TABLE FUNCTIONS                                 */
/*****************************************************************************/
INT4 QoSQMapCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSQMapTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

tQoSQMapNode *
QoSUtlGetQMapNode PROTO ((INT4 i4IfIndex, UINT4 u4CLASS, 
                          INT4 i4RegenPriType, UINT4 u4RegenPri));

tQoSQMapNode       *
QoSUtlGetQMapEntry (UINT4 u4FsQoSQMapCLASS);

tQoSQMapNode *
QoSCreateQMapTblEntry PROTO ((INT4 i4IfIndex, UINT4 u4CLASS, 
                              INT4 i4RegenPriType, UINT4 u4RegenPri)); 

INT4 
QoSGetNextQMapTblEntryIndex PROTO ((INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                    UINT4 u4CLASS, UINT4 *pu4NextCLASS,
                                    INT4  i4RegenPriType, INT4 *pi4NextRGPType,
                                    UINT4 u4RegenPri, UINT4 *pu4NextRegenPri));

INT4 
QoSDeleteQMapTblEntry PROTO ((tQoSQMapNode *pQMapNode));

INT4
QoSUtlValidateQMapTblIdxInst PROTO ((INT4 i4IfIndex, UINT4 u4CLASS,
                                     INT4 i4RegenPriType, UINT4 u4RegenPri,
                                     UINT4 *pu4ErrorCode));

INT4
QoSUtlValidateEgressPort PROTO ((UINT4 u4EgrPort, UINT4 *pu4ErrorCode));

INT4
QoSUtlValidateQMapClass PROTO ((UINT4 u4Class, UINT4 *pu4ErrorCode));

INT4
QoSUtlValidateQMapQId PROTO ((INT4 i4IfIndex, UINT4 u4QId,
                              UINT4 *pu4ErrorCode));

INT4
QoSValidateQMapTblEntry PROTO ((tQoSQMapNode *pQMapNode));

INT4 QoSIsDefQMapTblEntry PROTO ((tQoSQMapNode *pQMapNode));
INT4 QoSAddDefQMapTblEntries PROTO ((INT4 i4NextIfIndex, UINT1 u1Flag));
INT4 QoSDelQMapTblEntries PROTO ((UINT4 u4Port));
/*****************************************************************************/
/*                     HIERARCHY TABLE FUNCTIONS                             */
/*****************************************************************************/
INT4 QoSHierarchyCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
INT4 QoSHierarchyTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

tQoSHierarchyNode *
QoSUtlGetHierarchyNode PROTO ((INT4 i4IfIndex, UINT4 u4HL, UINT4 u4SchedId));

INT4 
QoSGetNextHierarchyTblEntryIndex PROTO ((INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                         UINT4 u4HL, UINT4 *pu4NextHL,
                                         UINT4 u4SchedId,
                                         UINT4 *pu4NextSchedId));

tQoSHierarchyNode *
QoSCreateHierarchyTblEntry  PROTO ((INT4 i4IfIndex, UINT4 u4HL, 
                                    UINT4 u4SchedId)); 

INT4 
QoSDeleteHierarchyTblEntry PROTO ((tQoSHierarchyNode *pHierarchyNode));

INT4
QoSUtlValidateHierarchyTblIdxInst PROTO ((INT4 i4IfIndex, UINT4 u4HL, 
                                          UINT4 u4SchedId,
                                          UINT4 *pu4ErrorCode));

INT4
QoSUtlValidateNextQ PROTO ((INT4 i4IfIndex, UINT4 u4HQNext,
                            UINT4 *pu4ErrorCode));

INT4 
QoSValidateHierarchyTblEntry PROTO ((tQoSHierarchyNode *pHierarchyNode));

INT4
QoSUtlValidateHTIndex PROTO ((INT4 i4IfIndex, UINT4 u4SchedId, UINT4 u4HL, 
                              UINT4 *pu4ErrorCode));


UINT1 *
QoSGetPrefNode PROTO ((INT4 i4IfIndex));
/*****************************************************************************/
/*                    DUP TABLE FUNCTIONS                                    */
/*****************************************************************************/
UINT1 *
QoSUtlGetDUPNode PROTO ((INT4 i4IfIndex));

INT4
QoSGetNextDUPTblEntryIndex PROTO ((INT4 i4IfIndex, INT4 *pi4NextIfIndex));

/*****************************************************************************/
/*                    STATS TABLE FUNCTIONS                                  */
/*****************************************************************************/
INT4
QoSGetNextMeterStatsTblEntryIndex PROTO ((UINT4 u4FsQoSMeterId, 
                                          UINT4 *pu4NextFsQoSMeterId));
INT4
QoSUtlValidateCoSQStatsTblIdxInst PROTO ((INT4 i4IfIndex, UINT4 u4QId));
INT4
QoSGetNextCoSQStatsTblEntryIndex PROTO ((INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                         UINT4 u4QId, UINT4 *pu4NextQId));

INT4
QoSUtlCoSQGetNext PROTO ((UINT4 u4SchedId, UINT4 u4CoSQ, UINT4 *pu4CoSQ));

/*****************************************************************************/
/*                   CPU Q RATE LIMIT TABLE FUNCTIONS                        */
/*****************************************************************************/
INT4
QosUtlCpuQCmpFun PROTO ((tRBElem * e1, tRBElem * e2));

tQosCpuQEntry *
QoSUtlGetCpuQNode PROTO ((UINT4 u4QId));

tQosCpuQEntry *
QoSUtlCreateCpuQTblEntry PROTO ((UINT4 u4QId));

INT4
QoSUtlGetNextCpuQTblEntryIndex PROTO ((UINT4 u4QId, UINT4 *pu4NextQId));

INT4
QoSUtlDeleteCpuQTblEntry PROTO ((tQosCpuQEntry * pCpuQNode));

INT4
QoSUtlCpuQTblRBTFreeNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

INT4
QoSAddDefCpuQTblEntries PROTO ((VOID));

INT4
QoSPortTblCmpFun PROTO ((tRBElem *e1, tRBElem *e2));
 
tQoSPortTblEntry *
QoSUtlGetPortTblNode PROTO ((INT4 i4IfIndex, INT4 i4PcpPacketType));
 
tQoSPortTblEntry   *
QoSCreatePortTblEntry PROTO ((INT4 i4IfIndex, INT4 i4PktType));
 
INT4
QoSAddPortTblEntry PROTO ((tQoSPortTblEntry * pPortTblNode));
 
INT4
QoSAddConfPortTblEntry PROTO ((tQoSPortTblEntry * pPortTblNode));
 
INT4
QoSConfigPcpTblEntries PROTO ((tQoSPortTblEntry * pPortTblNode,
                               INT4 i4Action));
 
INT4
QoSCreatePcpTblEntries PROTO ((tQoSPortTblEntry * pPortTblNode));
 
INT4
QoSModifyPcpTblEntries PROTO ((tQoSPortTblEntry * pPortTblNode));
 
INT4
QoSAddPcpPriMapTblEntries PROTO ((tQoSPortTblEntry * pPortTblNode));
INT4
QoSAddPcpPlyMapTblEntries PROTO ((tQoSPortTblEntry * pPortTblNode));
 
INT4
QoSAddPcpQMapTblEntries PROTO ((tQoSPortTblEntry * pPortTblNode));
 
INT4
QoSDeletePortTblEntry PROTO ((tQoSPortTblEntry * pPortTblNode));
 
INT4
QoSGetNextPortTblEntryIndex PROTO ((INT4 i4IfIndex, INT4 *pi4NextIfIndex,
                                    INT4  i4FsQoSPcpPacketType,
                                    INT4  *pi4FsQoSPcpPacketType));
 
INT4
QoSPortTblRBTFreeNodeFn PROTO  ((tRBElem * pRBElem, UINT4 u4Arg));

INT4
QosUtlGetClassMapEntryType PROTO ((UINT4 u4ClassMapId, INT4 *pi4EntryType));

INT4
QosUtlGetPriMapEntryType PROTO ((UINT4 u4PriMapId, INT4 *pi4EntryType));

INT4
QosUtlGetPlyMapEntryType PROTO ((UINT4 u4PlyMapId, INT4 *pi4EntryType));

INT4
QosUtlGetQMapEntryType PROTO ((INT4 i4IfIdx, UINT4 u4CLASS, INT4 i4PriType,
                               UINT4 u4PriVal, INT4 *pi4EntryType));
INT4
QosUtlValidateDEValue PROTO ((tQoSPolicyMapNode * pPlyMapNode));

INT4
QoSUtlStoreOperEncodingInfo PROTO((UINT4 u4IfIndex, UINT1 u1Priority,
                                   UINT1 u1DEI, UINT1 u1PcpValue,
                                   INT4 i4PcpSelRow, INT4 i4PktType,
                                   INT4 i4Action));

INT4
QoSUtlStoreOperDecodingInfo PROTO((UINT4 u4IfIndex, UINT1 u1PcpValue,
                                   UINT1 u1Priority, UINT1 u1DEI,
                                   INT4 i4PcpSelRow, INT4 i4PktType,
                                   INT4 i4Action));
VOID QosUtlUpdInProExcActionDrop PROTO((tQoSPolicyMapNode  *pPlyMapNode));
/*****************************************************************************/
/* FileName : qosnpwr.c  functions prototype                                 */
/*****************************************************************************/
INT4
QoSHwWrInit PROTO ((VOID));
INT4
QoSHwWrMapClassToPolicy PROTO ((tQoSPolicyMapNode *pPlyMapNode,
                                tQoSClassMapNode  *pClsMapNode, UINT1 u1Flag));

INT4
QoSHwWrUnmapClassFromPolicy PROTO ((tQoSPolicyMapNode *pPlyMapNode,
                                    tQoSClassMapNode  *pClsMapNode,
                                    UINT1 u1Flag));
INT4
QoSHwWrDeleteClassMapEntry PROTO ((tQoSClassMapNode  *pClsMapNode));
INT4
QoSHwWrUpdatePolicyMapForClass PROTO ((tQoSPolicyMapNode * pPlyMapNode,
                                       UINT1 u1Flag));
INT4
QoSHwWrMeterCreate PROTO ((tQoSMeterNode *pMeterNode));
INT4
QoSHwWrMeterDelete PROTO ((tQoSMeterNode *pMeterNode));

INT4
QoSHwWrMeterStatsUpdate PROTO ((UINT4 ,tQoSMeterNode *));

INT4
QoSHwWrMeterStatsClear PROTO ((tQoSMeterNode *));

INT4
QoSHwWrSchedulerAdd PROTO ((INT4 i4IfIndex, UINT4 u4SchedId));

INT4
QoSHwWrSchedulerDelete PROTO ((INT4 i4IfIndex, UINT4 u4SchedId));

INT4 
QoSHwWrSchedulerUpdateParams PROTO ((INT4 i4IfIndex, UINT4 u4SchedId,
                                     UINT1 u1Flag));

INT4
QoSHwWrMapClassToQueue PROTO ((INT4 i4IfIndex, UINT4 u4CLASS,
                               INT4 i4RegenPriType, UINT4 u4Priority, 
                               UINT4 u4QId, UINT1 u1Flag));

INT4
QoSHwWrSchedulerHierarchyMap PROTO ((INT4 i4IfIndex, INT4 i4SchedulerId,
                                     UINT2 u2Sweight, UINT1 u1Spriority, 
                                     INT4 i4NextSchedId, INT4 i4NextQId,
                                     UINT1 i1HL, UINT1 u1Flag));

INT4
QoSHwWrGetMeterStats PROTO ((UINT4 u4FsQoSMeterId, UINT4 u4StatsType,
                             tSNMP_COUNTER64_TYPE *pu8MeterStatsCounter));
INT4
QoSHwWrGetCoSQStats PROTO ((INT4 i4IfIndex, UINT4 u4FsQoSCoSQId, 
                            UINT4 u4StatsType, 
                            tSNMP_COUNTER64_TYPE *pu8CoSQStatsCounter));

INT4
QoSHwWrQueueCreate PROTO ((INT4 i4IfIndex, UINT4 u4Id));

INT4
QoSHwWrQueueDelete PROTO ((INT4 i4IfIndex, UINT4 u4Id));

INT4
QoSHwWrSetDefUserPriority PROTO ((INT4 i4Port, INT4 i4DefPriority));


INT4
QoSHwWrSetPbitPreferenceOverDscp PROTO ((INT4 i4Port, INT4 i4Pref));

INT4
QoSHwWrUtilDeleteAllClsMapEntry PROTO ((UINT4 u4CLASS));

INT4
QoSUtilTestFsQoSMeterCIR PROTO((UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                                UINT4 u4TestValFsQoSMeterCIR,
                                UINT4 u4TestValFsQoSMeterEIR));
INT4
QoSUtilTestFsQoSMeterEIR PROTO((UINT4 *pu4ErrorCode, UINT4 u4FsQoSMeterId,
                                UINT4 u4TestValFsQoSMeterEIR,
                                UINT4 u4TestValFsQoSMeterCIR));
INT4
QoSHwWrSetCpuRateLimit PROTO ((INT4 i4CpuQueue, UINT4 u4MinRate, 
                               UINT4 u4MaxRate));
INT4
QosHwWrGetPfcStats PROTO ((tQosPfcStats *pQosPfcStats));
#ifdef NPAPI_WANTED

tQoSClassMapEntry *
QoSFiltersForClassMapEntry PROTO ((tQoSClassMapNode *pClsMapNode)); 
VOID
QoSUtlClassMapEntryMemFree PROTO ((tQoSClassMapEntry *pClassMapEntry));
tQoSClassMapEntry* QoSUtlClassMapEntryMemAlloc PROTO ((VOID));

VOID
QoSHwWrUtilFillMeterParams PROTO ((tQoSMeterEntry *pMeterEntry, 
                                   tQoSMeterNode *pMeterNode));

VOID
QoSHwWrUtilFillMeterStatsParams PROTO ((tQoSMeterEntry *pMeterEntry, 
                                   tQoSMeterNode *pMeterNode));

VOID
QoSHwWrUtilFillPolicyParams PROTO ((tQoSPolicyMapEntry *pPlyMapEntry,
                                    tQoSPolicyMapNode *pPlyMapNode));
VOID
QoSHwWrUtilFillInProParams PROTO ((tQoSInProfileActionEntry *pInProEntry,
                                   tQoSPolicyMapNode *pPlyMapNode));
VOID
QoSHwWrUtilFillOutProParams PROTO ((tQoSOutProfileActionEntry *pOutProEntry,
                                    tQoSPolicyMapNode *pPlyMapNode));

tQoSSchedulerEntry *
QoSHwWrUtilFillSchedEntry PROTO ((INT4 i4IfIndex, UINT4 u4SchedId));
VOID
QoSHwWrUtilFreeSchedEntry PROTO ((tQoSSchedulerEntry *pSchedEntry));
INT4          
QoSHwWrUtilFillQParams PROTO ((INT4 i4IfIndex, UINT4 u4QId,
                               tQoSQEntry **ppQEntry,
                               tQoSQtypeEntry **ppQTypeEntry, 
                               tQoSREDCfgEntry *apRDCfgEntry[]));
VOID
QoSHwWrUtilFreeQParams PROTO ((tQoSQEntry *pQEntry, 
                               tQoSQtypeEntry *pQTypeEntry, 
                               tQoSREDCfgEntry *papRDCfgEntry[]));


#endif /* NPAPI_WANTED */
/*****************************************************************************/
/*                     QOS PORTING FUNCTIONS                                 */
/*****************************************************************************/

INT4 QosGetNextValidPort PROTO ((UINT4 u4IfIndex, UINT4 *pu4NextPort));
PUBLIC UINT4 QOSRmRegisterProtocols PROTO((tRmRegParams * pRmReg));
PUBLIC UINT4 QOSRmDeRegisterProtocols  PROTO((VOID));
PUBLIC INT4 QosRmReleaseMemoryForMsg PROTO((UINT1 *pu1Block));
PUBLIC UINT4 QosPortRmGetNodeState PROTO((VOID));
PUBLIC UINT1 QosPortRmGetStandbyNodeCount PROTO((VOID));
PUBLIC UINT4 QosPortRmApiHandleProtocolEvent PROTO((tRmProtoEvt * pEvt));
PUBLIC INT4 QosPortRmApiSendProtoAckToRM PROTO((tRmProtoAck * pProtoAck));
PUBLIC INT4 QosPortEnqMsgToRm PROTO((tRmMsg * pRmMsg, UINT2 u2DataLen));
PUBLIC VOID QosPortRmSetBulkUpdatesStatus PROTO ((VOID));
INT4 QosL2IwfIsPortInPortChannel PROTO((UINT4 u4IfIndex));
/*****************************************************************************/
/*                     STD QOS PORTING FUNCTIONS                             */
/*****************************************************************************/
INT4 QosGetOidFromTBParamType PROTO ((UINT4 u4Type,tSNMP_OID_TYPE *Oid));
INT4 QoSClfrElementCmpFun PROTO ((tRBElem * e1, tRBElem * e2));
INT4 QoSGetNextActionTblEntryIndex PROTO ((UINT4 u4CurrentIndex, 
      UINT4 *pu4NextIndex));
INT4 QoSGetNextCountActEntryIndex PROTO ((UINT4 u4CurrentIndex, 
   UINT4 *pu4NextIndex));
tQosStdActionEntry * QoSUtlGetActionNode PROTO ((UINT4 u4ActionId));
tQosStdCountActEntry * QoSUtlGetCountActionNode PROTO ((UINT4 u4CntActId));
INT4 QoSDeleteCountActTblEntry PROTO ((tQosStdCountActEntry * pCountActNode));
INT4 QosValidateIndex PROTO ((UINT4 u4DiffServId, UINT4 u4EntryType));
INT4 QosValidateType PROTO ((UINT4 u4DropType, UINT4 u4DropSpecificType));
INT4 QosValidateActionIndex PROTO ((UINT4 u4DiffServActionId));
INT4 QoSActionCmpFun PROTO ((tRBElem * e1, tRBElem * e2));
INT4 QoSCountActCmpFun PROTO ((tRBElem * e1, tRBElem * e2));


INT4
QoSHwWrGetCountActOctets PROTO ((UINT4 u4DiffServCountActId, UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServCountActOctets));
INT4
QoSHwWrGetCountActPkts PROTO ((UINT4 u4DiffServCountActId, UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServCountActPkts));
INT4
QoSHwWrGetAlgDropOctets PROTO ((UINT4 u4DiffServAlgDropId, UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServAlgDropOctets));
INT4
QoSHwWrGetAlgDropPkts PROTO ((UINT4 u4DiffServAlgDropId, UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServAlgDropPkts));
INT4
QoSHwWrGetRandomDropOctets PROTO ((UINT4 u4DiffServAlgDropId, UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServRandomDropOctets));
INT4
QoSHwWrGetRandomDropPkts PROTO ((UINT4 u4DiffServAlgDropId, UINT4 u4StatsType,
                      tSNMP_COUNTER64_TYPE * pu8RetValDiffServRandomDropPkts));
INT4
QoSHwWrMapClassToIntPriority  PROTO ((tQoSClassMapNode *pClsMapNode, UINT1 u1RegenPri, UINT1 u1Flag));

INT4
QoSHwWrMapClasstoPriMap PROTO ((tQoSInPriorityMapNode *pPriMapNode, UINT1 u1Flag));

INT4
QoSHwWrGetDscpQueueMap  PROTO ((INT4 i4DscpVal,INT4 *i4QueueID));
INT4
QosHwWrSetTrafficClassToPcp PROTO ((INT4 i4IfIndex, INT4 i4UserPriority, INT4 i4TrafficClass));

INT4 QoSDeleteActionTblEntry PROTO ((tQosStdActionEntry * pActionNode));
tQosStdActionEntry * QoSCreateActionTblEntry PROTO ((UINT4 u4ActionId));
tQosStdCountActEntry      * QoSCreateCountActTblEntry PROTO ((UINT4 u4CountActId));

INT4 QoSDataPathEntryNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSClfrNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSClfrElementNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSStdMeterNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSActionNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSCountActNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSQueueNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSSchedulerNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSMinRateNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSMaxRateNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));
INT4 QoSSchedMapNodeFn PROTO ((tRBElem * pRBElem, UINT4 u4Arg));

INT4 QosUtlValidateDataPathEntry PROTO ((INT4 i4IfIndex, INT4 i4DiffServDataPathIfDirection, UINT4 *pu4ErrorCode));

INT4 QosUtlValidateClfrEntry PROTO ((UINT4 u4DiffServClfrId, UINT4 *pu4ErrorCode));

INT4 QosUtlValidateClfrElementEntry PROTO ((UINT4 u4DiffServClfrId, UINT4 u4DiffServClfrElemId, UINT4 *pu4ErrorCode));

INT4 QosUtlValidateMultiFieldClfrEntry PROTO ((UINT4 u4DiffServMultiFieldClfrId, 
tIssL3FilterEntry * pIssL3FilterEntry, UINT4 *pu4ErrorCode));

INT4 QosUtlValidateMeterEntry PROTO ((UINT4 u4DsMeterId, UINT4 *pu4ErrorCode));
INT4 QosUtlValidateTBParamEntry PROTO ((UINT4 u4DsTBParamId, UINT4 *pu4ErrorCode));
INT4 QosUtlValidateActionEntry PROTO ((UINT4 u4DiffServActionId, UINT4 *pu4ErrorCode));
INT4 QosUtlValidateCountActEntry PROTO ((UINT4 u4DiffServCountActId, UINT4 *pu4ErrorCode));
INT4 QosUtlValidateAlgDropEntry PROTO ((UINT4 u4DsAlgoDropId, UINT4 *pu4ErrorCode));
INT4 QosUtlValidateQEntry PROTO ((UINT4 u4DsQId, UINT4 *pu4ErrorCode));
INT4 QoSValidateDataPathTableStatus PROTO ((tQosStdDataPathEntry  *pDataPathEntry));
INT4 QoSValidateClassifierElementTableStatus PROTO ((tQosStdClfrElementEntry  *pClfrElementEntry));
INT4 QoSValidateMeterTableStatus PROTO ((tQosStdMeterEntry *pMeterNode));
INT4 QoSValidateActionTableStatus PROTO ((tQosStdActionEntry *pActionNode));
INT4 QoSValidateQueueTableStatus PROTO ((tQosStdQEntry        *pQNode));
INT4 QosUtlPointedToStatus PROTO ((UINT4 * pu4PointerType, UINT4 * pu4PointerId, UINT4 * pu4PointerId2, UINT4 u4Type, UINT4 u4Id, UINT4 u4Id2, UINT4 u4Status));
INT4 QosIsDropTypeValid PROTO ((INT4 i4AlgoDropType));


extern VOID Ip6AddrCopy ARG_LIST ((tIp6Addr * pDst, tIp6Addr * pSrc));
INT4
QosUtlUpdateClfrElemStatus PROTO ((UINT4 u4Type, UINT4 u4Id, UINT4 u4Id2,
                            UINT4 u4Status));
INT4
QosUtlGetPointerListForEntry PROTO ((UINT4 u4Type, UINT4 u4Id, UINT4 u4Id2,
                              tTMO_SLL ** pQosPointerList));
INT4
QosUpdateDiffServCLASSNextFree PROTO ((UINT4 u4Index, INT4 i4Status));
INT4
QoSAddDefDataPathTblEntries PROTO ((INT4 i4IfIndex, INT4 i4Direction)); 
INT4
QosSchedulerGetIfIdFromGlobalId PROTO ((UINT4 u4GlobalId, UINT4 *pu4Interface, UINT4 *pu4IfSchedId));
INT4
QosAddSchedulerMapEntry PROTO ((UINT4 u4GlobalId, UINT4 u4Interface, UINT4 u4IfSchedId));
INT4
QosSchedulerGetGlobalIdFromIfId PROTO ((UINT4 u4Interface, UINT4 u4IfSchedId, UINT4 *pu4GlobalId));
INT4 QosDeleteSchedMapEntry PROTO ((UINT4 u4GlobalId));

tQoSQNode *
QosGetQFromPri PROTO ((INT4 i4IfIndex,INT4 u4PriIdx));

INT4 
QosUtlHwCfgEtsParams PROTO ((tQoSQNode *pQNode));

#ifdef MBSM_WANTED
VOID QosMbsmProgramHw PROTO ((tMbsmProtoMsg * pProtoMsg));
INT4 QosMbsmScanAndProgramIngressTblInHw PROTO ((tMbsmSlotInfo      *pSlotInfo));
INT4 QosMbsmScanAndProgramEgressTblInHw PROTO ((tMbsmSlotInfo      *pSlotInfo));
INT4 QosFindPolicyAndProgramHw PROTO ((tQoSClassMapEntry  *pNewClassMapEntry,
                           tQoSPolicyMapEntry  *pNewPolicyMapEntry,
                           tQoSInProfileActionEntry *pNewInProActEntry,
                           tQoSOutProfileActionEntry *pNewOutProActEntry, 
                           tQoSMeterEntry *pNewMeterEntry ,
                           tMbsmSlotInfo *pSlotInfo));
/* DCB PFC MBSM  */
INT4
QosMbsmConfigPfc PROTO ((tMbsmPortInfo *pMbsmPortInfo,
                         tMbsmSlotInfo *pSlotInfo));
    
INT4
QosMbsmCheckPortRange (tMbsmPortInfo * pPortInfo, UINT4 u4IfIndex);
INT4
QosMbsmScanAndProgramSchedTblInHw (tMbsmPortInfo *pPortInfo,
                                   tMbsmSlotInfo *pSlotInfo);
INT4
QoSMbsmProgramMeterEntries (tMbsmSlotInfo *pSlotInfo);
INT4
QoSMbsmMapClassToIntPriority (tMbsmSlotInfo *pSlotInfo);
INT4
QosMbsmProgramDefUserPriority (tMbsmSlotInfo *pSlotInfo, tMbsmPortInfo *pPortInfo);
INT4
QosMbsmMapClasstoPriMap (tMbsmSlotInfo *pSlotInfo, tMbsmPortInfo *pPortInfo);
INT4
QosMbsmProgramCpuEntries (tMbsmSlotInfo *pSlotInfo);
INT4
QoSMbsmSchedulerHierarchyMap (tMbsmSlotInfo *pMbsmSlotInfo,
                              tMbsmPortInfo *pPortInfo);
INT4
QoSMbsmSchedulerUpdateParams (tMbsmSlotInfo *pMbsmSlotInfo,
                              tMbsmPortInfo *pPortInfo);
INT4
QosMbsmHwSetPbitPreferenceOverDscp (tMbsmSlotInfo *pMbsmSlotInfo,                                                              tMbsmPortInfo *pPortInfo);
INT4
QosMbsmMapClassToPolicy (tMbsmSlotInfo *pMbsmSlotInfo,
                         tMbsmPortInfo *pPortInfo);
#endif


/* Proto Validate Index Instance for IssAclL3FilterTable. */
extern INT1
nmhValidateIndexInstanceIssAclL3FilterTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for IssAclL3FilterTable  */

extern INT1
nmhGetFirstIndexIssAclL3FilterTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

extern INT1
nmhGetNextIndexIssAclL3FilterTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetIssAclL3FilterPriority ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterProtocol ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterMessageType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterMessageCode ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilteAddrType ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterDstIpAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIssAclL3FilterSrcIpAddr ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIssAclL3FilterDstIpAddrPrefixLength ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIssAclL3FilterSrcIpAddrPrefixLength ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIssAclL3FilterMinDstProtPort ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIssAclL3FilterMaxDstProtPort ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIssAclL3FilterMinSrcProtPort ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIssAclL3FilterMaxSrcProtPort ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIssAclL3FilterInPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIssAclL3FilterOutPortList ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

extern INT1
nmhGetIssAclL3FilterAckBit ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterRstBit ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterTos ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterDscp ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterDirection ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterAction ARG_LIST((INT4 ,INT4 *));

extern INT1
nmhGetIssAclL3FilterMatchCount ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIssAclL3FilterFlowId ARG_LIST((INT4 ,UINT4 *));

extern INT1
nmhGetIssAclL3FilterStatus ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetIssAclL3FilterPriority ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterProtocol ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterMessageType ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterMessageCode ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilteAddrType ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterDstIpAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIssAclL3FilterSrcIpAddr ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIssAclL3FilterDstIpAddrPrefixLength ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssAclL3FilterSrcIpAddrPrefixLength ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssAclL3FilterMinDstProtPort ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssAclL3FilterMaxDstProtPort ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssAclL3FilterMinSrcProtPort ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssAclL3FilterMaxSrcProtPort ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssAclL3FilterInPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIssAclL3FilterOutPortList ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhSetIssAclL3FilterAckBit ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterRstBit ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterTos ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterDscp ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterDirection ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterAction ARG_LIST((INT4  ,INT4 ));

extern INT1
nmhSetIssAclL3FilterFlowId ARG_LIST((INT4  ,UINT4 ));

extern INT1
nmhSetIssAclL3FilterStatus ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2IssAclL3FilterPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterProtocol ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterMessageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterMessageCode ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilteAddrType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterDstIpAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IssAclL3FilterSrcIpAddr ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IssAclL3FilterDstIpAddrPrefixLength ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssAclL3FilterSrcIpAddrPrefixLength ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssAclL3FilterMinDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssAclL3FilterMaxDstProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssAclL3FilterMinSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssAclL3FilterMaxSrcProtPort ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssAclL3FilterInPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IssAclL3FilterOutPortList ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

extern INT1
nmhTestv2IssAclL3FilterAckBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterRstBit ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterTos ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterAction ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

extern INT1
nmhTestv2IssAclL3FilterFlowId ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

extern INT1
nmhTestv2IssAclL3FilterStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

extern INT1
nmhDepv2IssAclL3FilterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));




/*---------------------------------------------------------------*/
/*-----------------qosred.c---------------------------------------*/
/*---------------------------------------------------------------*/
PUBLIC VOID QosRedHwAuditIncBlkCounter  PROTO((VOID));
PUBLIC VOID QosRedHwAuditDecBlkCounter  PROTO((VOID));
PUBLIC INT4 QosRedInitGlobalInfo  PROTO((VOID));
PUBLIC VOID QosRedDeInitGlobalInfo PROTO((VOID));
PUBLIC INT4 QosRedRmCallBack  PROTO((UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen));
VOID QosRedHandleRmEvents PROTO((tQosQueueMsg * pMsg));
PUBLIC VOID QosRedHandleGoActive PROTO((VOID));
PUBLIC VOID  QosRedHandleIdleToActive PROTO((void));
PUBLIC VOID QosRedHandleStandbyToActive PROTO((void));
PUBLIC VOID QosRedHandleGoStandby PROTO((VOID));
PUBLIC VOID QosRedHandleActiveToStandby  PROTO((VOID));
PUBLIC VOID QosRedHandleIdleToStandby PROTO((VOID));
PUBLIC VOID QosRedHwAudCrtNpSyncBufferTable PROTO((VOID));
PUBLIC VOID QosRedProcessPeerMsgAtStandby  PROTO((tRmMsg * pMsg, UINT2 u2DataLen));
PUBLIC VOID QosRedInitHardwareAudit PROTO((VOID));
PUBLIC VOID QoSRedHwAuditHandleMapClsToPlcy PROTO((tQoSNpSyncClassMapNode* pSyncClassMapNode, UINT4 u4PlyMapId));
PUBLIC VOID QoSHwAuditHandleDelClsMapEntry PROTO((tQoSNpSyncClassMapNode* pClassMapNode));
PUBLIC VOID QoSRedHwAuditHandleMeterAudit PROTO((INT4  u4MeterId));
PUBLIC VOID  QoSRedHwAuditHandleSchedAudit PROTO((INT4  u4QosIfIndex,  INT4 i4QosSchedulerId));
PUBLIC VOID QosRedHwAuditHandleQueueAudit PROTO((INT4 u4IfIndex, UINT4 u4QId));
PUBLIC VOID QosRedHwAuditHandleQmapAudit PROTO((INT4 u4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                      UINT4 u4QId, UINT1 u1Flag));
PUBLIC VOID QoSRedHwAuditHandleSetDefUsrPri PROTO((INT4 u4Port, INT4 i4DefPriority));
PUBLIC VOID QoSRedHwAuditHandleHierAudit PROTO((INT4 u4IfIndex, INT4 i4SchedulerId,
                              UINT2 u2Sweight, UINT1 u1Spriority, INT4 i4NextSchedId,
                              INT4 i4NextQId,  UINT2 u2HL, UINT1 u1Flag));
PUBLIC VOID QoSHwAuditHandleMapClsToQId PROTO((UINT4  u4ClassMapId,INT4 u4IfIndex, INT4 i4ClsOrPriType, UINT4 u4ClsOrPri,
                      UINT4 u4QId, UINT1 u1Flag));
PUBLIC VOID QosRedHwAuditHandleConfigPfc PROTO((UINT4 u4IfIndex,UINT1 u1PfcPriority,UINT1 u1Profile, UINT1 u1Flag));
/*---------------------------------------------------------------*/
/*-----------------qoshaset.c---------------------------------------*/
/*---------------------------------------------------------------*/
PUBLIC VOID QosHwProcessNpSyncMsg PROTO((tRmMsg  *pMsg, UINT2 *pu2OffSet));
VOID QosHwAuditCreateOrFlushBuffer PROTO((unNpSync * pNpSync, UINT4 u4NpApiId, UINT4 u4EventId));
/*--------------------------qoshaget.c-----------------------*/


extern tSNMP_OCTET_STRING_TYPE *
allocmem_octetstring      PROTO((INT4));

extern VOID 
free_octetstring          PROTO((tSNMP_OCTET_STRING_TYPE *));

/*---------------------------------------------------------------*/
/*-----------------qosrbutl.c---------------------------------------*/
/*---------------------------------------------------------------*/
INT4
QosCreateQosStdClfrPortTbl PROTO ((VOID));
VOID
QosDeleteQosStdClfrPortTbl PROTO ((VOID));
INT4
QosStdClfrPortTblCmp PROTO ((tRBElem * Node,
                             tRBElem * NodeIn));
INT4
QosAddStdClfrPortTblEntry PROTO ((UINT4 u4DsClfrId, UINT2 u2LocalPort,
                                  tQosStdClfrPortNode **ppStdClfrPortNode));
tQosStdClfrPortNode *
QosGetStdClfrPortTblEntry PROTO ((UINT4 u4DsClfrId, UINT2 u2LocalPort));
tQosStdClfrPortNode  *
QosGetFirstStdClfrPortTblEntry PROTO ((VOID));
tQosStdClfrPortNode *
QosGetNextStdClfrPortTblEntry PROTO ((UINT4 u4DsClfrId, UINT2 u2LocalPort));
INT4
QosDelStdClfrPortTblEntry PROTO ((tQosStdClfrPortNode *pStdClfrPortNode));
VOID
QosDelAllStdClfrPortTblEntries PROTO ((VOID));
VOID
QosAddPortNodeInStdClfrPortTbl PROTO ((UINT4 u4DsClfrId, UINT2 u2LocalPort,
                                        UINT2 u2Flag));
VOID
QosDelPortNodeInStdClfrPortTbl PROTO ((UINT4 u4DsClfrId, UINT2 u2LocalPort,
                                       UINT2 u2Flag));
UINT4
QoSInitWithMBSMandRM PROTO((VOID));

INT4
QoSUtlValidateClassTypeParams PROTO ((UINT4 u4QMapCLASS,INT4 i4IfIndex));

/*-----------------qosrbutl.c---------------------------------------*/







#endif /* __QOS_PROTO_H__ */

