/* $Id: qosinc.h,v 1.11 2017/09/20 11:39:18 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qosinc.h                                        */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-INC                                         */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File is the superset of all header file    */
/*                          for the Aricent QoS Module.                     */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_INC_H__
#define __QOS_INC_H__

/* Globale Header */
#include "lr.h"
#include "fssnmp.h" 
#include "trace.h"
#include "iss.h"
#include "msr.h"
#include "fsvlan.h"
#include "l2iwf.h"
#include "ipv6.h"
#include "fssyslog.h"
#ifdef NPAPI_WANTED
#include "qosxnp.h"
#include "qosxnpwr.h"
#endif
#include "qosred.h"
#include "hwaud.h"
#include "hwaudmap.h"
#include "qosxtd.h"
#include "rmgr.h"
/* Module Header */
#include "fsqosxlw.h"
#include "fsqosxwr.h"
#include "qosdefn.h"
#include "stdqoslw.h"
#include "stdqoswr.h"
#include "qostdfs.h"
#include "qosproto.h"
#include "qosglob.h"
#include "qostrc.h"
#include "qosxcli.h"
#include "dcbx.h"
#include "qossz.h"
#ifdef WLC_WANTED
#include "wssifinc.h"
#endif
#endif /* __QOS_INC_H__ */
