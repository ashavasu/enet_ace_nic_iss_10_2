#include "stdqoswr.h"

typedef struct QosFuncBlockTbl {
        tSNMP_OID_TYPE   ObjectID;
        UINT4            u4OidType;
} tQosFuncBlockTbl;

typedef struct QosTBParamTbl {
        tSNMP_OID_TYPE   ObjectID;
        UINT4            u4OidType;
} tQosTBParamTbl;

typedef struct QosSchedMethodTbl {
        tSNMP_OID_TYPE   ObjectID;
        UINT4            u4OidType;
} tQosSchedMethodTbl;

extern UINT4 DiffServClfrId [ ];
extern UINT4 DiffServClfrElementId [ ];
extern UINT4 DiffServMultiFieldClfrId [ ];
extern UINT4 DiffServMultiFieldClfrFlowId [ ];
extern UINT4 DiffServMeterId [ ];
extern UINT4 DiffServTBParamId [ ];
extern UINT4 DiffServDscpMarkActDscp [];
extern UINT4 DiffServActionId [ ];
extern UINT4 DiffServCountActId [ ];
extern UINT4 DiffServAlgDropId [ ];
extern UINT4 DiffServRandomDropId [ ];
extern UINT4 DiffServQId [ ];
extern UINT4 DiffServSchedulerId [ ];
extern UINT4 DiffServMinRateId [ ];
extern UINT4 DiffServMaxRateId [ ];

UINT4 DiffServTBParamSimpleTokenBucket [ ] ={1,3,6,1,2,1,97,3,1,1};
UINT4 DiffServTBParamAvgRate [ ] ={1,3,6,1,2,1,97,3,1,2};
UINT4 DiffServTBParamSrTCMBlind [ ] ={1,3,6,1,2,1,97,3,1,3};
UINT4 DiffServTBParamSrTCMAware [ ] ={1,3,6,1,2,1,97,3,1,4};
UINT4 DiffServTBParamTrTCMBlind [ ] ={1,3,6,1,2,1,97,3,1,5};
UINT4 DiffServTBParamTrTCMAware [ ] ={1,3,6,1,2,1,97,3,1,6};
UINT4 DiffServTBParamTswTCM [ ] ={1,3,6,1,2,1,97,3,1,7};

UINT4 DiffServSchedulerPriority [ ] ={1,3,6,1,2,1,97,3,2,1};
UINT4 DiffServSchedulerWRR [ ] ={1,3,6,1,2,1,97,3,2,2};
UINT4 DiffServSchedulerWFQ [ ] ={1,3,6,1,2,1,97,3,2,3};

UINT4 DefaultOid [] = {0};

tQosFuncBlockTbl  QosFuncBlockTbl[]= {
{{1, DefaultOid}, 0}, 
{{12,DiffServClfrId}, DS_CLFR},
{{12,DiffServClfrElementId}, DS_CLFR_ELEM},
{{12,DiffServMultiFieldClfrId}, DS_MF_CLFR},
{{12,DiffServMultiFieldClfrFlowId}, DS_CLFR_FLOW},
{{12,DiffServMeterId}, DS_METER},
{{12,DiffServTBParamId}, DS_TBPARM},
{{12,DiffServActionId}, DS_ACTION},
{{12,DiffServCountActId}, DS_COUNT_ACT},
{{12,DiffServAlgDropId}, DS_ALG_DROP},
{{12,DiffServRandomDropId}, DS_RANDOM_DROP},
{{12,DiffServQId}, DS_QUEUE},
{{12,DiffServSchedulerId}, DS_SCHEDULER},
{{12,DiffServMinRateId}, DS_MINRATE},
{{12,DiffServDscpMarkActDscp}, DS_STDDSCP},
{{12,DiffServMaxRateId}, DS_MAXRATE}
};

tQosTBParamTbl  QosTBParamTbl[]= {
{{1, DefaultOid}, 0}, 
{{10,DiffServTBParamSimpleTokenBucket}, DS_TB_STB},
{{10,DiffServTBParamAvgRate}, DS_TB_AVGRATE},
{{10,DiffServTBParamSrTCMBlind}, DS_TB_SR_TCM_BLIND},
{{10,DiffServTBParamSrTCMAware}, DS_TB_SR_TCM_AWARE},
{{10,DiffServTBParamTrTCMBlind}, DS_TB_TR_TCM_BLIND},
{{10,DiffServTBParamTrTCMAware}, DS_TB_TR_TCM_AWARE},
{{10,DiffServTBParamTswTCM}, DS_TB_TSQ_TCM}
};

tQosSchedMethodTbl  QosSchedMethodTable[]= {
{{1, DefaultOid}, 0}, 
{{10,DiffServSchedulerPriority}, DS_SCHED_SP},
{{10,DiffServSchedulerWRR}, DS_SCHED_WRR},
{{10,DiffServSchedulerWFQ}, DS_SCHED_WFQ}
};
