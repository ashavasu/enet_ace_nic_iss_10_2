/* $Id: qostdfs.h,v 1.37 2017/09/20 11:39:18 siva Exp $ */
/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*                                                                          */
/*  FILE NAME             : qostdfs.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-TDFS                                        */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contain the structures and enum  for  */
/*                          the Aricent QoS Module.                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/
#ifndef __QOS_TDFS_H__
#define __QOS_TDFS_H__ 

/* Incoming Priority Map Table  */
typedef struct _tQoSInPriorityMapNode
{
    tRBNodeEmbd RbNode;                  /* Link to Traverse the Table       */
    tRBNodeEmbd RbNodeUnique;            /* Link to Traverse the Table       */
    UINT1 au1Name[QOS_MAX_TABLE_NAME];   /* Name of the Table                */
    UINT4 u4Id;                          /* Index of the Table               */
    UINT4 u4HwFilterId;                  /* Hw Index of filter crated for mpls exp*/
    INT4  i4HwExpMapId;                  /*MPLS EXP map id in hardware       */       
    UINT4 u4RefCount;                    /* No. of usage of this Entry       */
    UINT4 u4IfIndex;                     /* Incoming Interface (port)        */
    UINT2 u2VlanId;                      /* Incoming VLAN ID                 */
    UINT1 u1InPriority;                  /* Incoming priority Value          */
    UINT1 u1InPriType;                   /* Incoming priority type 
                                            vlan/ip-dscp/ip-tos/mpls-exp     */
    UINT1 u1ConfStatus;                  /* Entry configuration status       */
    UINT1 u1RegenPriority;               /* ReGenerator Priority for a InPri.*/
    UINT1 u1RegenInnerPriority;          /* ReGenerator Inner Priority 
                                            for a InPriority.                */
    UINT1 u1Status;                      /* Status of this Entry             */
    INT4  i4RegenColor;                  /* Regenerator Color value          */
    INT4  i4InDEI;                       /* Incoming DEI value               */
    UINT1 u1Type;                        /* Entry Type                       */
    UINT1 au1Pad[3];
} tQoSInPriorityMapNode;

/* Incoming Vlan map Table */
typedef struct _tQoSInVlanMapNode
{
    tRBNodeEmbd RbNode;                  /* Link to Traverse the Table       */
    UINT4 u4Id;                          /* Index of the Table               */
    UINT4 u4IfIndex;                     /* Incoming Interface (port)        */
    UINT4 u4VlanMapHwId;                 /* Hardware Id of scheduler         */
    UINT4 u4RefCount;                    /* No. of usage of this Entry       */
    UINT4 u4ClassMapNode;                /* Class map node id                */
    UINT2 u2VlanId;                      /* Incoming VLAN ID                 */
    UINT1 u1Status;                      /* Status of this Entry             */
    UINT1 au1Pad[1];                     /* Padding  */

} tQoSInVlanMapNode;

/* MultiField ClassMap Table */
typedef struct _tQoSClassMapNode
{
    tRBNodeEmbd RbNode;                 /* Link to Traverse the Table       */
    UINT1 au1Name[QOS_MAX_TABLE_NAME];  /* Name of the Table                */
    UINT4 u4Id;                         /* Index of the Table               */
    /* input ACL/Priority-Map */
    UINT4 u4L2FilterId;                 /* L2(MAC) Filter Id                */
    UINT4 u4L3FilterId;                 /* L3(IP) Filter Id                 */
    UINT4 u4PriorityMapId;              /* priority Map Id from            
                                           tQoSInPriorityMapNode            */
    UINT4 u4VlanMapId;                  /* Vlan Map Id from            
                                           tQoSInVlanMapNode                */
    /* outputs */
    UINT4 u4ClassId;                    /* Class Id to the L2 and or     
                                           L3/PriorityMapId                 */
    UINT4 u4ClassMapClfrId;              /* Classifier related to the Class Map     
                                           in Standard QosMIB               */
    UINT2 u2PPPSessId;                  /* PPP session identifier           */
    UINT1 u1PreColor;                   /* Pre Color of the traffic         */ 
    UINT1 u1Status;                     /* Status of this Entry             */
    BOOL1 b1IsClsMapConfFromExt;       /* Configuration through QoS/Other protocol Flag */ 
    UINT1 u1Type;                       /* Status of this Entry             */

} tQoSClassMapNode;


/* Class To Priority Table */
typedef struct _tQoSClass2PriorityNode
{
    tRBNodeEmbd RbNode;                    /* Link to Traverse the Table    */
    UINT1 au1GroupName[QOS_MAX_TABLE_NAME];/* Group Name                    */
    UINT4 u4ClassId;                       /* Class Ids for this Group      */
    UINT1 u1RegenPri;                      /* Regenerator priority          */
    UINT1 u1Status;                        /* Status of this Entry          */
    UINT1 u1Resvd[2];                      /* 4Byte Alignment               */

} tQoSClass2PriorityNode;

/* Meter Table */
typedef struct _tQoSMeterNode
{
    tRBNodeEmbd RbNode;                 /* Link to Traverse the Table       */
    UINT1 au1Name[QOS_MAX_TABLE_NAME];  /* Name of the Table                */
    UINT4 u4Id;                         /* Index of the Table               */
    UINT4 u4RefCount;                   /* No. of usage of this Entry       */
    UINT4 u4Interval;                   /* Avg Inerval time for AVG_METER   */
    UINT4 u4CIR;                        /* Committed Information Rate       */ 
    UINT4 u4CBS;                        /* Committed Burst Size             */ 
    UINT4 u4EIR;                        /* Excess information Rate          */
    UINT4 u4EBS;                        /* Excess Burst Size                */
    UINT4 u4Next;                       /* Next Meter id if applicable      */
#ifndef QOS_ARRAY_TO_RBTREE_WANTED
    /* This port list is nowhere used.
     * In case of RBTree wanted case, this should be
     * enhanced. */
    tPortListExt DPPortList;
#endif
    INT4  i4StorageType;                 /* Whether the entry should be saved*/
    UINT1  u1StatsStatus;                /*Meter statistics status*/
    UINT1 u1Type;                       /* Meter Type                       */
    UINT1 u1ColorMode;                  /* Meter Color Mode aware or blind  */
    UINT1 u1Status;                     /* Status of this Entry             */
    UINT1 u1HwCallFlag;                  /* Whether h/w calls to be made or not*/
    UINT1 u1Resvd[3];                      /* 4Byte Alignment               */

} tQoSMeterNode;

/* Policy-Map Table ( Class To Policy Map Table )  */
typedef struct _tQoSPolicyMapNode
{
    tRBNodeEmbd RbNode;                 /* Link to Traverse the Table       */
    UINT1 au1Name[QOS_MAX_TABLE_NAME];  /* Name of the Table                */
    UINT4 u4Id;                         /* Index of the Table               */
    /* inputs */
    UINT4 u4IfIndex;                    /* Incoming If Index Id             */
    UINT4 u4ClassId;                    /* Class Id for this  
                                           policy (filters list)            */
    /* output policy */
    UINT4 u4MeterTableId;               /* Meter Id for this Policy Entry   */
    UINT4 u4FailMeterTableId;           /* Fail Meter Id for this Policy Entry   */
    INT4  i4HwExpMapId;                  /*Mpls Exp profile id*/    
    UINT2 u2DefaultPHB;                 /* PHB Value                        */
    UINT1 au1Resvd[2];                   /* 4Byte Alignment                  */
    UINT1 u1PHBType;                    /* PHB Type DSCP/MPLS/VLAN-PRI      */

    UINT1 u1InProfConfActFlag;          /* Flag indicate Valid Action fields
                                           for In profile Conform Actions   */ 
    UINT1 u1InProfExcActFlag;           /* Flag indicate Valid Action fields
                                           for In profile Exced Actions     */ 
    UINT1 u1OutProActionFlag;           /* Flag indicate Valid Action fields
                                           for Out profile Actions          */ 
    /* InProfile Action Values */
    /* Conform Actions */
    UINT4 u4InProActConfNewClassId;     /* New Class Id for the Changed vlaues*/
    UINT4 u4InProfileActionPort;
    UINT4 u4InProfileActionId;
    UINT4 u4InProfileTrafficClass;      /* Value for Traffic Class          */
    UINT1 u1InProActConfDscpOrToS;      /* Value for TOS or DSCP            */
    UINT1 u1InProActConfMplsExp;        /* Vlaue for MPLS Exp               */
    UINT2 u2InProActConfVlanPrio;       /* Vlan Priority Vlaue              */
    UINT2 u2InProActConfVlanDE;         /* Vlan Drop Eligible Value (DE bit)*/
    UINT2 u2InProActConfInnerVlanPrio;  /* Inner Vlan Priority Vlaue        */
    /* Exceed Actions */
    UINT4 u4InProActExccedNewClassId;  /* New Class Id for the Changed vlaues*/
    UINT4 u4InProActExceedActionId;    /* Value for TOS or DSCP            */
    UINT4 u4InProActExceedTrafficClass; /* Value for Traffic Class          */
    UINT1 u1InProActExceedDscpOrToS;    /* Value for TOS or DSCP            */
    UINT1 u1InProActExceedMplsExp;      /* Vlaue for MPLS Exp               */
    UINT2 u2InProActExceedVlanPrio;     /* Vlan Priority Vlaue              */
    UINT2 u2InProActExceedVlanDE;       /* Vlan Drop Eligible Value (DE bit)*/
    UINT2 u2InProActExceedInnerVlanPrio; /* Inner Vlan Priority Vlaue        */
    /* OutProfile Action Values */
    /* Voilate Actions  */
    UINT4 u4OutProActNewClassId;       /* New Class Id for the Changed vlaue */
    UINT4 u4OutProActionId;            /* Value for TOS or DSCP              */ 
    UINT4 u4OutProActTrafficClass;        /* Value for Traffic Class            */
    UINT1 u1OutProActDscpOrTos;        /* Value for TOS or DSCP              */ 
    UINT1 u1OutProActVlanPrio;         /* Vlan Priority Vlaue                */
    UINT2 u2OutProActVlanDE;           /* Vlan Drop Eligible Value (DE bit)  */
    UINT1 u1OutProActMplsExp;          /* Vlaue for MPLS Exp                 */
    UINT1 u1Status;                    /* Status of this Entry               */ 
    UINT2 u2OutProActInnerVlanPrio;    /* Inner Vlan Priority Vlaue          */
    UINT2 u2RefCount;    /* Inner Vlan Priority Vlaue          */

    /* For Structure Alignment InnerVlanDE actions have been added at bottom*/
    UINT2 u2InProActConfInnerVlanDE;    /* Inner Vlan DE Vlaue        */
    UINT2 u2InProActExceedInnerVlanDE;  /* Inner Vlan DE Vlaue        */
    UINT2 u2OutProActInnerVlanDE;      /* Inner Vlan DE Vlaue         */
    INT4  i4VlanDE;                       /* VLAN DEI Value                  */
    INT4  i4RegenColor;                   /*Regenerated color                */
    UINT1 u1StatsEnabled;                 /* Policer statistics status */
    BOOL1 b1IsPlyMapConfFromExt;       /* Configuration through QoS/Other protocol Flag */
    UINT1 u1Type;                         /* Entry Type                       */
    UINT1 au1Resrvd[1];                   /* 4Byte Alignment                  */

} tQoSPolicyMapNode;
/* Shaper Template Table */
typedef struct _tQoSShapeTemplateNode
{ 
    tTMO_SLL_NODE NextNode;             /* Next tQoSShapeTemplateNode       */
    UINT1 au1Name[QOS_MAX_TABLE_NAME];  /* Name of the Table                */
    UINT4 u4Id;                         /* Index of the Table               */
    UINT4 u4RefCount;                   /* No. of usage of this Entry       */
    UINT4 u4CIR;                        /* Committed Information Rate       */ 
    UINT4 u4CBS;                        /* Committed Burst Size             */ 
    UINT4 u4EIR;                        /* Excess information Rate          */
    UINT4 u4EBS;                        /* Excess Burst Size                */ 
    UINT1 u1Status;                     /* Status of this Entry             */
    UINT1 u1Resvd[3];                   /* 4Byte Alignment                  */

} tQoSShapeTemplateNode;


/* Queue Map Table */
typedef struct _tQoSQMapNode
{
    tRBNodeEmbd RbNode;                 /* Link to Traverse the Table       */
    UINT4 i4IfIndex;                    /* Egress Interface                 */ 
    UINT4 u4CLASS;                      /* Class Id for this Entry          */
    UINT4 u4QId;                        /* Q Id Mapped with this Entry      */ 
    INT4  i4RegenColor;                 /* Regenerator color value          */
    UINT1 u1RegenPriType;               /* Regenerator Priority Type        */ 
    UINT1 u1RegenPri;                   /* Regenerator Priority             */
    UINT1 u1Status;                     /* Status of this Entry             */
    UINT1 u1Type;                       /* Type of Entry                    */

} tQoSQMapNode;


/* Q Table */
typedef struct _tQoSQNode
{
    tRBNodeEmbd RbNode;                   /* Link to Traverse the Table      */
    UINT4 i4IfIndex;                      /* Egress Interface                */ 
    UINT4 u4Id;                           /* Index of the Table              */
    UINT4 u4QueueHwId;                    /* Hw ID of the Queue              */
    UINT4 u4RefCount;                     /* No. of usage of this Entry      */
    UINT4 u4CfgTemplateId;                /* Q Template ID                   */
    UINT4 u4SchedulerId;                  /* Scheduler id for this Q         */ 
    UINT4 u4ShapeId;                      /* Shape Id for this Q             */
    UINT4 u4QueueType;                    /* Type of Q -Unicast or Multicast */ 
    UINT2 u2Weight;                       /* Weight for this Q               */
    UINT1 u1Priority;                     /* Priority for this Q             */
    UINT1 u1Status;                       /* Status of this Entry            */

} tQoSQNode;


/* Hierarchy Table for hierarchical queuing/scheduling. */
typedef struct _tQoSHierarchyNode
{
    tRBNodeEmbd RbNode;                 /* Link to Traverse the Table      */
    INT4  i4IfIndex;                    /* Interface Index                 */
    UINT4 u4SchedId;                    /* Scheduler Id in this Hierarchy  */
    UINT4 u4QNext;                      /* Next Q Id for this 
                                           hierarchy Entry                  */
    UINT4 u4SchedNext;                  /* Next sched Id for this 
                                           hierarchy Entry                  */
    UINT2 u2Weight;                     /* Scheduler weight for next level  */ 
    UINT1 u1Level;                      /* Hierarchy Level                  */
    UINT1 u1Priority;                   /* Scheduler priority for next level*/
    UINT1 u1Status;                     /* Status of this Entry             */
    UINT1 au1Resvd[3];                   /* 4Byte Alignment                  */

} tQoSHierarchyNode;
typedef struct _tQoSPortTblNode
{
    tRBNodeEmbd RbNode;                    /* Link to Traverse the Table      */
    INT4  i4IfIndex;                       /* Interface Index                 */
    INT4  i4PcpSelRow;                     /* PCP Selection Row               */
    INT4  i4PktType;                       /* Packer Type                     */
    INT4  i4PcpTblIndex;                   /* PM/CM/PoM Index                 */
    UINT1 u1Status;                        /* Row status                      */
    UINT1 u1Type;                          /* Entry Type                      */
    UINT1 au1Pad[2];
 
} tQoSPortTblEntry;

typedef struct
{
     UINT4    u4IfIndex; /*Interface Index*/
     INT4     i4PcpSelRow;/*PCP Selection Row*/
     INT4     i4PktType;/*PCP Packet Type*/
     UINT1    u1InPriority; /*Priority*/
     UINT1    u1InDEI; /*Priority*/
     UINT1    u1PcpValue;/*PCP Encoding Value*/
    UINT1 au1Pad;
}tQoSPcpEncodingInfo;


typedef struct
{
     UINT4    u4IfIndex; /*Interface Index*/
     INT4     i4PcpSelRow;/*PCP Selection Row*/
     INT4     i4PktType;/*PCP Packet Type*/
     UINT1    u1OutPriority; /*Priority*/
     UINT1    u1OutDEI; /*Priority*/
     UINT1    u1PcpValue;/*PCP Encoding Value*/
    UINT1 au1Pad;
}tQoSPcpDecodingInfo;

typedef enum _tSysCntl {
    
   QOS_SYS_CNTL_SHUTDOWN = 0,  /* 0 */
   QOS_SYS_CNTL_START          /* 1 */

} etSysCntl;

typedef enum _tSysStatus {
    
    QOS_SYS_STATUS_DISABLE = 0,  /* 0 */
    QOS_SYS_STATUS_ENABLE        /* 1 */

} etSysStatus;

typedef enum _tVlanQueueingStatus {
    
    VLAN_Q_SYS_STATUS_DISABLE = 0,  /* 0 */
    VLAN_Q_SYS_STATUS_ENABLE        /* 1 */

} etVlanQStatus;

/******************************************************************************/
/******************Structures for stdqos.mib reference ***********************/
/*****************************************************************************/
/* QosStd NextFree Entry */
typedef struct QosStdNextFree
{
   UINT4    u4DataPathNextFree;
   UINT4    u4ClfrNextFree;
   UINT4    u4ClfrElementNextFree;
   UINT4    u4MultiFieldNextFree;
   UINT4    u4MeterNextFree;
   UINT4    u4TBParamNextFree;
   UINT4    u4ActionNextFree;
   UINT4    u4CountActNextFree;
   UINT4    u4AlgoDropNextFree;
   UINT4    u4RandomDropNextFree;
   UINT4    u4QNextFree;
   UINT4    u4SchedulerNextFree;
   UINT4    u4MinRateNextFree;
   UINT4    u4MaxRateNextFree;
   UINT4    u4TemplateNextFree;
   UINT4    u4CLASSNextFree;
}tQosStdNextFree;

typedef struct QosStdCount
{
   UINT4    u4DataPathCount;
   UINT4    u4ClfrCount;
   UINT4    u4ClfrElementCount;
   UINT4    u4MultiFieldCount;
   UINT4    u4MeterCount;
   UINT4    u4TBParamCount;
   UINT4    u4ActionCount;
   UINT4    u4CountActCount;
   UINT4    u4AlgoDropCount;
   UINT4    u4RandomDropCount;
   UINT4    u4QCount;
   UINT4    u4SchedulerCount;
   UINT4    u4MinRateCount;
   UINT4    u4MaxRateCount;
   UINT4    u4TemplateCount;
   UINT4    u4CLASSCount;
}tQosStdCount;



/* QosStd Data Path Entry */
typedef struct QosStdDataPathEntry
{
    tRBNodeEmbd  RbNode;
    INT4      i4IfIndex;                      /* Port IfIndex */
    INT4      i4DSDataPathIfDir;  /* Ingress or Egress Direction*/
    INT4      i4StorageType;   /* Storage type of this table*/
    UINT4     u4RowPointerId;/* Index of either classifier, meter, action,
                                Algo drop or Q table*/
    UINT4     u4RowPointerType;/* Type can be either classifier, meter, action,
                                  Algo drop or Q table*/
    UINT1     u1DataPathStatus;
    UINT1     au1Pack[3];
}tQosStdDataPathEntry;

/* QosStd Classifier Entry */
typedef struct QosStdClfrEntry
{
   tRBNodeEmbd RbNode; 
#ifndef QOS_ARRAY_TO_RBTREE_WANTED
    tPortListExt      DPPortList;
#endif
    UINT4          u4DsClfrId;
    INT4           i4StorageType;
    UINT1          u1DsClfrStatus;  
    UINT1          au1Pack [3];
} tQosStdClfrEntry;

/* QosStd Classifier Element Entry */
typedef struct QosStdClfrElementEntry
{ 
    tRBNodeEmbd RbNode;
    UINT4          u4DServClfrId;               /*Index of this entry*/
    UINT4          u4DServClfrElementId; /*Index of this entry*/
    UINT4          u4DSClfrElementPrecedence;
    UINT4          u4DsNextRowPointerId;
    UINT4          u4DSNextRowPointerType;
    UINT4          u4DSClfrElementSpecificIndex;
    UINT4          u4DSClfrElementSpecificType;
    INT4          i4StorageType;
    UINT4          u4FpHandle;
    UINT1          u1DSClfrElementStatus;
    UINT1          au1Pack [3];
} tQosStdClfrElementEntry;

/* Meter Entry */
typedef struct QosStdMeterEntry
{
    tRBNodeEmbd RbNode; 
    UINT4          u4DSMeterId;
    UINT4          u4DsMeterSucceedNextId;
    UINT4          u4DSMeterSucceedNextType; 
    UINT4          u4DsMeterFailNextId;
    UINT4          u4DSMeterFailNextType;
    UINT4          u4DSMeterSpecificIndex;
    UINT4          u4DSMeterSpecificType;
    INT4          i4StorageType;
    UINT1          u1DSMeterStatus;
#ifndef QOS_ARRAY_TO_RBTREE_WANTED
    /* This port list is nowhere used.
     * In case of RBTree wanted case, this should be
     * enhanced. */
    tPortListExt      DPPortList;
#endif
    UINT1          au1Pack [3];
} tQosStdMeterEntry;

/* Action Entry */
typedef struct QosStdActionEntry
{
    tRBNodeEmbd RbNode;
#ifndef QOS_ARRAY_TO_RBTREE_WANTED
    /* This port list is nowhere used.
     * In case of RBTree wanted case, this should be
     * enhanced. */
    tPortListExt      DPPortList;
#endif
    INT4           i4DSActionInterface;
    UINT4          u4DSActionId;
    UINT4          u4DsActionNextId;
    UINT4          u4DSActionNextType;
    UINT4          u4DSActionSpecificIndex;
    UINT4          u4DSActionSpecificType;
    INT4          i4StorageType;
    UINT1          u1DSActionStatus;
    UINT1          au1Pack [3];
} tQosStdActionEntry; 


/* CountAct Entry */
typedef struct QosStdCountActEntry
{
    tRBNodeEmbd RbNode;
    UINT4       u4DSCountActId;
    INT4       i4StorageType;
    UINT1       u1DSCountActStatus;
    UINT1       au1Pack[3];
}tQosStdCountActEntry;

/* Queue Template Table (Algo Drop Entry) */
typedef struct _tQoSQTemplateNode 
{
    tTMO_SLL_NODE NextNode;              /* Next tQoSQTemplateNode           */
    UINT1 au1Name [QOS_MAX_TABLE_NAME];   /* Name of the Table                */
    UINT4 u4Id;                          /* Index of the Table               */
    UINT4 u4RefCount;                    /* No. of usage of this Entry       */
    UINT4 u4Size;                        /* Size of Q ie QThreshold                  */
    UINT4       u4DsAlgoDropNextId;
    UINT4       u4DSAlgoDropNextType;
    UINT4       u4DSAlgDropQMeasureId;
    UINT4       u4DSAlgoDropQMeasureType;
    UINT4       u4DSAlgDropSpecificId; 
    UINT4       u4DSAlgDropSpecificType; 
    INT4       i4StorageType;
    UINT2 u2DropType;                    /* Drop Type of Q   AlgoDropType                */
    UINT1 u1DropAlgoEnableFlag;          /* Drop Algorithm (WRED/RED) Status */
    UINT1 u1Status;                      /* Status of this Entry             */
} tQoSQTemplateNode;

/* Random Detect Configuration Table */
typedef struct _tQoSRDCfgNode
{
    tTMO_SLL_NODE NextNode;          /* Next tQoSRDCfgNode */
    UINT4 u4QTemplateId;             /* Entry associate Q Template Id    */
    UINT4 u4RefCount;                /* No. of usage of this Entry       */
    UINT4 u4MinAvgThresh;            /* Min. Threshold of RD Alog        */
    UINT4 u4MaxAvgThresh;            /* Max. Threshold of RD Alog        */
    UINT4 u4MaxPktSize;              /* Max. packet size of RD Alog      */
    UINT4 u4DServMinThreshPkts;
    UINT4 u4DSServMaxThreshPkts;
    UINT4 u4DSRandomDropSamplingRate;
    UINT4 u4Gain;
    UINT4 u4ECNThresh;
    INT4 i4StorageType;
    UINT1 u1MaxProb;                 /* Drop Probability of RD           */ 
    UINT1 u1ExpWeight;               /* Exponential Weight of RD         */
    UINT1 u1DP;                      /* Drop precedence for this Entry   */
    UINT1 u1Status;                  /* Status of this Entry             */
    UINT1 u1DropThreshType;
    UINT1 u1RDActionFlag;
    UINT1 au1Pack[2];
} tQoSRDCfgNode;

/* Q Entry */
typedef struct QosStdQEntry
{
    tRBNodeEmbd RbNode;
    UINT4 u4DServQId;
    UINT4 u4DServQNextId;
    UINT4 u4DServQNextType;
    UINT4 u4DSQMinRateIndex;
    UINT4 u4DSQMaxRateIndex;
    INT4 i4StorageType;
    UINT1 u1DSQStatus;
    UINT1 au1Pack [3];
} tQosStdQEntry;


/* Scheduler Table */
typedef struct _tQoSSchedNode            
{   
    tRBNodeEmbd RbNode;                  /* Link to Traverse the Table      */
    UINT4 u4Id;                          /* Index of the Table               */
    UINT4 u4SchedulerHwId;               /* HW ID of the Scheduler           */
    UINT4 u4RefCount;                    /* No. of usage of this Entry       */
    UINT4 u4ShapeId;                     /* Shape Id if applicable at sched  */
    INT4  i4IfIndex;                     /* Ifindex of the QMap (Q's) Value  */
    UINT4 u4GlobalId;                    /* GLobal Id for standard*/
    UINT4 u4DSSchedNextId;
    UINT4 u4DSSchedNextType;
    UINT4 u4DSSchedulerMinRateType;
    UINT4 u4DSSchedulerMaxRateType;
    UINT4 u4DSSchedulerMinRateId;
    UINT4 u4DSSchedulerMaxRateId;
    UINT4 u4SchedChildren;
    UINT1 u1HierarchyLevel;              /* level of Hierarchy               */
    UINT1 u1SchedAlgorithm;              /* Scheduler Algorithm              */
    UINT1 u1Status;                      /* Status of this Entry             */
    UINT1 u1Pack[1];

} tQoSSchedNode;


/* DIffserv scheduler table */
typedef struct _tStdSchednode            
{  
    tRBNodeEmbd        RbNode; 
    UINT4              u4DSSchedulerId;
    UINT4              u4DSSchedNextId;
    UINT4              u4DSSchedNextType;
    UINT4              u4DSSchedulerMinRateId;
    UINT4              u4DSSchedulerMaxRateId;
    UINT4              u4DSSchedMethodType;
    INT4               i4DSSchedStorage;
    UINT1              u1DSchedStatus;
    UINT1              au1Pack[3];
} tQosStdSchedulerEntry;

/* Min Rate Entry */
typedef struct QosStdMinRateEntry
{
    tRBNodeEmbd RbNode;
    UINT4 u4DSMinRateId;
    UINT4 u4DSMinRatePriority;
    UINT4 u4DSMinRateAbsolute;
    UINT4 u4DSMinRateRelative;
    INT4 i4StorageType;
    UINT1 u1DSMinRateStatus;
    UINT1 au1Pack[3];
}tQosStdMinRateEntry;


/* Max Rate Entry */
typedef struct QosStdMaxRateEntry
{
    tRBNodeEmbd RbNode;
    UINT4 u4DSMaxRateId;
    UINT4 u4DSMaxRateLevel;
    UINT4 u4DSMaxRateAbsolute;
    UINT4 u4DSMaxRateRelative;
    UINT4 u4DSMaxRateThreshold;
    INT4 i4StorageType;
    UINT1 u1DSMaxRateStatus;
    UINT1 au1Pack [3];
} tQosStdMaxRateEntry;

/* CPU RateLimit QEntry */
typedef struct QosCpuQEntry
{
    tRBNodeEmbd RbNode;
    UINT4 u4CpuQId;
    UINT4 u4CpuQMinRate;
    UINT4 u4CpuQMaxRate;
    UINT1 u1CpuQStatus;
    UINT1 au1Pack [3];
} tQosCpuQEntry;


/* ------------------------------------------------------------------
 *                Global Redundancy Information
 * This structure contains all the Global Data required for QOS
 * Redundancy (High Availability) Operation .
 * ----------------------------------------------------------------- */

typedef struct _QosRedGlobalInfo
{
    tMemPoolId  HwAuditTablePoolId;
                  /* Hardware Audit Table Pool Id - Np Sync Buffer Table 
     * Pool Id. */
    tTMO_SLL    HwAuditTable;
                  /* SLL header of Hardware Audit Np-Sync Buffer Table */
    UINT1       u1NodeStatus;
                  /* Node Status - Idle/Active/Standby. 
     * Idle - Node Status on boot-up.
     * Active - Node where SEM runs and hardware programming 
     *          is done.
     * Standby - Node which is in sync with active of all the 
     *           information and becomes active on switchover
     */
    UINT1       u1NumOfStandbyNodesUp;
                  /* Number of Standby nodes which are booted up. If this
     * value is 0, then dynamic messages are not sent by 
     * active node. */
    UINT1       u1NpSyncBlockCount;
                  /* At Active Node - NP-Syncups are not sent when the value is 
                   * not 0.
                   * At Standby Node - NP-Syncup Buffer Table is not accessed 
                   * when the value is greater than 1.
                   * Block Counter is incremented when there is a need to block
                   * the Np Sync-ups. When there is a special event (Module 
                   * shutdown, Module Enable and Module disable per context) 
                   * are those events. And also during static, dynamic bulk 
                   * updates and Module Shutdown (for all contexts to handle 
                   * communication lost and restored scenario), the Block 
                   * counter is incremented. After the completion of these 
                   * special events the counter is decremented. The reason for 
                   * having a counter instead of flag is that during static 
                   * bulk updates, the Module enable will also be called. After
                   * the completion of Module enable, the block flag will be 
                   * reset. To avoid this the counter is used instead of flag.
                   */
    UINT1       u1BulkReqRcvd; /* Flag that indicates if Bulk request is received in 
                                * active node. It is updated to OSIX_TRUE upon bulk 
                                * request reception in active node, OSIX_FALSE
                                * otherwise. 
                                */ 
}tQosRedGlobalInfo;



/* Global Structure  */
typedef struct _tQosGlobalInfo
{
    /* Memory Pool Ids for QoS Tables */
    tOsixTaskId QoSTaskId;
    tMemPoolId QoSInPriMapTblMemPoolId; 
    tMemPoolId QoSVlanQueueingMapTblMemPoolId; 
    tMemPoolId QoSClsMapTblMemPoolId;
    tMemPoolId QoSCls2PriMapTblMemPoolId;
    tMemPoolId QoSMeterTblMemPoolId;
    tMemPoolId QoSPlyMapTblMemPoolId;
    tMemPoolId QoSQTempTblMemPoolId;
    tMemPoolId QoSRDTblMemPoolId;
    tMemPoolId QoSShapeTempTblMemPoolId;
    tMemPoolId QoSQMapTblMemPoolId;
    tMemPoolId QoSQTblMemPoolId;
    tMemPoolId QoSSchedTblMemPoolId;
    tMemPoolId QoSHierarchyTblMemPoolId;
    tMemPoolId QoSClsInfoTblMemPoolId;
    tMemPoolId QoSFltInfoTblMemPoolId;
    tMemPoolId QosQPktMemPoolId;
    tMemPoolId QosCpuQMemPoolId;
    tMemPoolId QosStdClfrPortPoolId;
    tMemPoolId QoSPortTblMemPoolId;
    /* Tables Headers */
    tRBTree    pRbInPriMapTbl; 
    tRBTree    pRbInPriMapUniTbl;
    tRBTree    pRbVlanQueueingMapTbl;
    tRBTree    pRbClsMapTbl;
    tRBTree    pRbCls2PriMapTbl;
    tRBTree    pRbMeterTbl;/*Node pointed by Meter Specific*/
    tRBTree    pRbPlyMapTbl;
    tRBTree    pRbQTbl;
    tRBTree    pRbSchedTbl;
    tRBTree    pRbSchedGlobTbl;
    tRBTree    pRbClsInfoTbl;
    tRBTree    pRbQMapTbl;
    tRBTree    pRbHierarchyTbl;
    tRBTree    QosCpuQTbl;
    tRBTree    pRbPortTbl;
    tTMO_SLL   SllQTempTbl;
    tTMO_SLL   SllRDTbl;
    tTMO_SLL   SllShapeTempTbl;
    tTMO_SLL   SllQosPointedTo;
    /* System Globals  */
    tQosRedGlobalInfo RedGlobalInfo;     /* Global Redundancy information.   */
    tOsixSemId        QoSSemId;
    etSysCntl         eSysControl;       /* QoS module shutdown status       */
    etSysStatus       eSysStatus;        /* QoS module enable/disable Status */
    etVlanQStatus     eVlanQStatus;      /* QoS Vlan queueing status         */
    UINT4             u4HLSubQueuesState;/* Subscriber queue attach status   */
    UINT4             u4RateUnit;
    UINT4             u4TrcFlag;
    UINT4             u4RateGranularity;
    INT4              gi4QoSxSysLogId;
    /* Default user priority per port */
    UINT1      au1PDUPTbl[BRG_MAX_PHY_PLUS_LOG_PORTS];
    /* Default preference per port */
    UINT1      au1PrefTbl[BRG_MAX_PHY_PLUS_LOG_PORTS];
   /*  Use for stdqos.mib ref*/ 
    tMemPoolId         DsDataPathPoolId;
    tMemPoolId         DsClfrPoolId;
    tMemPoolId         DsClfrElementPoolId;  
    tMemPoolId         DsMeterPoolId;
    tMemPoolId         DsActionPoolId;
    tMemPoolId         DsCountActPoolId;
    tMemPoolId         DsQPoolId;
    tMemPoolId         DsQosSchedulerPoolId;
    tMemPoolId         DsMinRatePoolId;
    tMemPoolId         DsMaxRatePoolId;
    tMemPoolId         QoSPonitedToQid;
    tMemPoolId         QoSPointerQid;
    tMemPoolId         QoSSchedulerMapPoolId;
    tRBTree            DsDataPathTbl;  
    tRBTree            DsClfrTbl;
    tRBTree            DsClfrElementTbl;
    tRBTree            DsMeterTbl;
    tRBTree            DsActionTbl;
    tRBTree            DsCountActTbl;
    tRBTree            DsQHead;
    tRBTree            DsMaxRateTbl;
    tRBTree            DsSchedulerTbl;
    tRBTree            DsMinRateTbl;
    tQosStdNextFree    DSNextFree;
    tQosStdCount       DSCount;
} tQoSGlobalInfo;


typedef struct _tQoSClassInfoNode
{
    tRBNodeEmbd RbNode;                /* Link to Traverse the Table       */
    tTMO_SLL    SllFltInfo;            /* list of ACLs/priority-map table 
                                          entries that belong to this 
                                          particular class                 */
    tQoSPolicyMapNode *pPlyMapNode;    /* Pointer to the Policy Map Table  */
    UINT4       u4ClassId;             /*  CLASS  Value (Index)            */

} tQoSClassInfoNode;

typedef struct _tQoSFilterInfoNode
{
    tTMO_SLL_NODE    NextNode;           /* Link to Traverse the Table       */
    tQoSClassMapNode *pClsMapNode;       /* Pointer to the Class Map Table   */
                                         /*  Entries                         */
} tQoSFilterInfoNode;


typedef struct _tQoSRBWalkInput
{
    tSNMP_OCTET_STRING_TYPE *pTblEntryName;
    UINT4                   u4TblType;

} tQoSRBWalkInput;
/* Data structures used for Messaging/Interface with RM  */

typedef struct  QosRmMsg {
    tRmMsg           *pData;     /* RM message pointer */
    UINT2             u2DataLen; /* Length of RM message */
    UINT1             u1Event;   /* RM event */
    UINT1             au1Reserved[1];
}tQosRmMsg;
/* Data Structure for NP-Sync Buffer Table */
typedef tNpSyncBufferEntry tQosRedNpSyncEntry;


typedef struct QosQueueMsg {
      UINT2                   u2MsgType; /* Message Type */
      UINT2                   u2Pad; 
      UINT4                   u4Port;    /* Physical Port Index */
#ifdef MBSM_WANTED
      tMbsmProtoMsg *pMbsmProtoMsg;
#endif
 tQosRmMsg              RmMsg;
} tQosQueueMsg;


typedef struct QosPointerList {
     tTMO_SLL_NODE  QosPointerListNode;
     UINT4          u4PointerType;
     UINT4          u4PointerId;
     UINT4          u4PointerId2;
}tQosPointerListNode;

typedef struct QosSchedMapList {
     tRBNodeEmbd RbNode;                 /* Link to Traverse the Table       */
     UINT4          u4GlobalId;
     UINT4          u4Interface;
     UINT4          u4IfSchedId;
}tQosSchedulerMapNode;


typedef struct QosPointNode  {
        tTMO_SLL_NODE  QosPointListNode;
        tTMO_SLL  SllQosPointerList;
        UINT4 u4Type;
        UINT4 u4Id;
        UINT4 u4Id2;
        UINT4 u4Count;
}tQosPointNode;

/* Below data structure is used only in case of QOS_ARRAY_TO_RBTREE_WANTED
 * flag is enabled. */
typedef  struct QosStdClfrPortNode {
    tRBNodeEmbd     RbNode;         /* Node to refer gQosStdClfrPortTable */
    UINT4           u4DsClfrId;     /* Primary index for the RBTree */
    UINT4           u4IfIndex;      /* Secondary index for the RBTree */
    UINT2           u2BitMask;      /* Bit mask for port property.
                                       QOS_STD_CLFR_PORT_BASIC */
    UINT1           au1Pad[2];      /* Padding  */
} tQosStdClfrPortNode;

#endif /* __QOS_TDFS_H__ */

