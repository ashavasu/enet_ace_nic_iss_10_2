/****************************************************************************/
/* Copyright (C) 2007 Aricent Inc . All Rights Reserved                     */
/*  $Id: qosglob.h,v 1.8 2016/07/05 07:38:43 siva Exp $       */
/*  FILE NAME             : qosglob.h                                       */
/*  PRINCIPAL AUTHOR      : Aricent                                         */
/*  SUBSYSTEM NAME        : QoS                                             */
/*  MODULE NAME           : QoS-GLOB                                        */
/*  LANGUAGE              : C                                               */
/*  TARGET ENVIRONMENT    : Linux                                           */
/*  DATE OF FIRST RELEASE :                                                 */
/*  AUTHOR                : Aricent                                         */
/*  FUNCTIONS DEFINED(Applicable for Source files) :                        */
/*  DESCRIPTION           : This File contain the Global Variables for      */
/*                          the Aricent QoS Module.                         */
/****************************************************************************/
/*                                                                          */
/*  Change History                                                          */
/*  Version               :                                                 */
/*  Date(DD/MM/YYYY)      :                                                 */
/*  Modified by           :                                                 */
/*  Description of change :                                                 */
/****************************************************************************/

#ifndef __QOS_GLOB_H__
#define __QOS_GLOB_H__ 

#ifdef __QOS_SYS_C__

tQoSGlobalInfo gQoSGlobalInfo;
tOsixQId       gQosPktQId;
UINT1       gau1QosBitMaskMap[QOS_PORTS_PER_BYTE] =
{ 0x01, 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02 };

tRBTree     gQosStdClfrPortTable; /* RBTree added in case of DPPortList 
                                     present in tQosStdClfrEntry is to 
                                     be tuned dynamically. */
UINT1 gu1HLSupportFlag=QOS_DEF_HL_SCHED_CONFIG_SUPPORT;
/*In this Array, PCP Encoding value will be calculated from*/
/*Vlan Priority and CFI in the incoming Packet*/
 
/*Example 1*/
/* Vlan Priority 2(010), CFI 1, 5P3D Model(Row3)*/
/*010 + 0 -> 0100(4)-> 4th element in array -> 3*/
/*Example 2*/
/* Vlan Priority 4(100), CFI 1, 8P0D Model(Row0)*/
/*100 + 1 -> 1001(9)-> 9th element in array -> 4*/
UINT1 gau1PcpVal[DEFAULT_MAX_ARRAYSIZE_PCP_MODEL]
        [DEFAULT_MAX_ARRAYSIZE_DOT1P] =
   {{0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7},
    {0, 0, 1, 1, 2, 2, 3, 3, 5, 4, 5, 4, 6, 6, 7, 7},
    {0, 0, 1, 1, 3, 2, 3, 2, 5, 4, 5, 4, 6, 6, 7, 7},
    {1, 0, 1, 0, 3, 2, 3, 2, 5, 4, 5, 4, 6, 6, 7, 7}
    };
/*In this Array, PCP Decoding value will be calculated from*/
/*Internal priority*/
 
/*Example 1*/
/* 5P3D Model(Row 3), Internal priority 5 */
/* 5th Element->8 ->(1000)                */
/*1000 &  1 -> 0000 ->DE=0                */
/*1000 >> 1 -> 0100 ->VLANPRI=4           */
/*Example 2*/
/* 8P0D Model(Row 3), Internal priority 7 */
/* 8th Element->14 ->(1110)                */
/*1110 &  1 -> 0000 ->DE=0                */
/*1110 >> 1 -> 0111 ->VLANPRI=7           */
UINT1 gau1DOT1PVal[DEFAULT_MAX_ARRAYSIZE_PCP_MODEL]
        [DEFAULT_MAX_ARRAYSIZE_PRI] =
    {{0, 2, 4, 6, 8, 10, 12, 14},
     {0, 2, 4, 6, 9, 8, 12, 14},
     {0, 2, 5, 4, 9, 8, 12, 14},
     {1, 0, 5, 4, 9, 8, 12, 14},
    };
/*In this array, Queue Value is derived from Int PRI*/
/*Example 1*/
/*6P2D Model(Row2),INT PRI 3-> 4(Queue5)*/
/*7P1D Model(Row1),INT PRI 5-> 5(Queue6)*/
UINT1 gau1QVal[DEFAULT_MAX_ARRAYSIZE_PCP_MODEL]
        [DEFAULT_MAX_ARRAYSIZE_PRI] =
    {{0, 1, 2, 3, 4, 5, 6, 7},
     {0, 1, 2, 3, 5, 5, 6, 7},
     {0, 1, 3, 3, 5, 5, 6, 7},
     {1, 1, 3, 3, 5, 5, 6, 7},
     };
/*In this array, Color Value is derived from Int PRI*/
/*Example 1*/
/*6P2D Model(Row2),INT PRI 4-> 1(yellow)*/
/*7P1D Model(Row1),INT PRI 5-> 0(Green)*/
UINT1 gau1ColorVal[DEFAULT_MAX_ARRAYSIZE_PCP_MODEL]
        [DEFAULT_MAX_ARRAYSIZE_PRI] =
    {{0, 0, 0, 0, 0, 0, 0, 0},
     {0, 0, 0, 0, 1, 0, 0, 0},
     {0, 0, 1, 0, 1, 0, 0, 0},
     {1, 0, 1, 0, 1, 0, 0, 0},
     };
UINT1 gau1DEArray[SYS_DEF_MAX_PHYSICAL_INTERFACES]
        [DEFAULT_MAX_ARRAYSIZE_PRI];

tQoSPcpEncodingInfo gaQoSPcpEncodingInfo[QOS_PCP_MAX_ENTRY];

tQoSPcpDecodingInfo gaQoSPcpDecodingInfo[QOS_PCP_MAX_ENTRY];
#else

extern  UINT1       gau1QosBitMaskMap[];

PUBLIC tQoSGlobalInfo gQoSGlobalInfo;
PUBLIC tOsixQId       gQosPktQId;
PUBLIC tRBTree     gQosStdClfrPortTable;
extern UINT1 gu1HLSupportFlag;
extern UINT1 gau1PcpVal[DEFAULT_MAX_ARRAYSIZE_PCP_MODEL]
                     [DEFAULT_MAX_ARRAYSIZE_DOT1P];
extern UINT1 gau1DOT1PVal[DEFAULT_MAX_ARRAYSIZE_PCP_MODEL]
                        [DEFAULT_MAX_ARRAYSIZE_PRI];
extern UINT1 gau1QVal[DEFAULT_MAX_ARRAYSIZE_PCP_MODEL]
                    [DEFAULT_MAX_ARRAYSIZE_PRI];
extern UINT1 gau1ColorVal[DEFAULT_MAX_ARRAYSIZE_PCP_MODEL]
                    [DEFAULT_MAX_ARRAYSIZE_PRI];
extern UINT1 gau1DEArray[SYS_DEF_MAX_PHYSICAL_INTERFACES]
                    [DEFAULT_MAX_ARRAYSIZE_PRI];
extern tQoSPcpDecodingInfo gaQoSPcpDecodingInfo[QOS_PCP_MAX_ENTRY];
extern tQoSPcpEncodingInfo gaQoSPcpEncodingInfo[QOS_PCP_MAX_ENTRY];
#endif /* __QOS_SYS_C__ */

#endif /* __QOS_GLOB_H__ */





