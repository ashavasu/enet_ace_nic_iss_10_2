/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: qosred.h,v 1.4 2014/08/23 11:50:29 siva Exp $
 *
 * Description: This file contains the definitions and data structures
 *                    to support High Availability for QOS module.   
 *
 ******************************************************************/
#ifndef _QOSRED_H_
#define _QOSRED_H_

typedef enum {
	QOS_BULK_REQ_MESSAGE = RM_BULK_UPDT_REQ_MSG,
	QOS_BULK_UPD_TAIL_MESSAGE = RM_BULK_UPDT_TAIL_MSG,
	QOSX_SYNC_SCHED_HW_ID,
	QOSX_SYNC_QUEUE_HW_ID
}tQosRmMsgType;

#define QOS_RED_MAX_MSG_SIZE   1500
#define QOS_NPSYNC_MSGTYPE_SIZE        1
#define QOS_RED_TYPE_FIELD_SIZE     1
#define QOS_RED_LEN_FIELD_SIZE      2
#define QOS_NPSYNC_MSG_LENGTH          2
#define QOS_SYNC_HW_ID_MSG_SIZE        (sizeof (UINT1) + sizeof (UINT2) + sizeof (UINT4) +\
                sizeof (UINT4) + sizeof (UINT4))
#define QOS_RED_BULK_REQ_MSG_SIZE          3
#define QOS_RED_NP_SYNC_INFO_MSG	   NPSYNC_MESSAGE 
#define QOS_MAX_HW_NP_BUF_ENTRY 	   15
#define QOS_RED_BULK_UPD_TAIL_MSG_SIZE    3

#define QOS_NPSYNC_BLK() (gQoSGlobalInfo.RedGlobalInfo.u1NpSyncBlockCount)

/* Macros to write into RM Buffer */

#define QOS_RM_PUT_1_BYTE(pMsg,pu4Offset,u1MesgType)\
	do{\
		RM_DATA_ASSIGN_1_BYTE(pMsg, *(pu4Offset), u1MesgType);\
		*(pu4Offset)+=1;\
		}while(0)

#define QOS_RM_PUT_2_BYTE(pMsg,pu4Offset,u2MesgType)\
	do{\
		RM_DATA_ASSIGN_2_BYTE(pMsg, *(pu4Offset), u2MesgType);\
		*(pu4Offset)+=2;\
		}while(0)

#define QOS_RM_PUT_4_BYTE(pMsg,pu4Offset,u4MesgType)\
	do{\
		RM_DATA_ASSIGN_4_BYTE(pMsg, *(pu4Offset), u4MesgType);\
		*(pu4Offset)+=4;\
		}while(0)

#define QOS_RM_PUT_N_BYTE(pdest,psrc,pu4Offset,u4Size)\
	do{\
		RM_COPY_TO_OFFSET(pdest,psrc, *(pu4Offset),u4Size)\
		*(pu4Offset)+=u4Size;\
		}while(0)

#define QOS_RM_GET_1_BYTE(pMsg, pu4Offset, u1MesgType) \
do { \
    RM_GET_DATA_1_BYTE (pMsg, *(pu4Offset), u1MesgType); \
        *(pu4Offset) += 1;\
}while (0)

#define QOS_RM_GET_2_BYTE(pMsg, pu4Offset, u2MesgType) \
do { \
    RM_GET_DATA_2_BYTE (pMsg, *(pu4Offset), u2MesgType); \
        *(pu4Offset) += 2;\
}while (0)

#define QOS_RM_GET_4_BYTE(pMsg, pu4Offset, u4MesgType) \
do { \
    RM_GET_DATA_4_BYTE (pMsg, *(pu4Offset), u4MesgType); \
        *(pu4Offset) += 4;\
}while (0)

#define QOS_RM_GET_N_BYTE(psrc, pdest, pu4Offset, u4Size) \
do { \
    RM_GET_DATA_N_BYTE (psrc, pdest, *(pu4Offset), u4Size); \
        *(pu4Offset) += u4Size; \
}while (0)

VOID
QosRedSyncSchedHwId (INT4 i4IfIndex, UINT4 u4QosSchedId, UINT4 u4QosSchedHwId);
VOID
QosRedSyncQueueHwId (INT4 i4IfIndex, UINT4 u4QosId, UINT4 u4QosQueueHwId);
VOID
QosRedProcessSchedHwSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID
QosRedProcessQueueHwSyncMsg (tRmMsg * pMsg, UINT2 *pu2OffSet);
VOID
QosRedSendBulkReqMsg (VOID);
VOID
QosRedHandleUpdates (tRmMsg * pMsg, UINT2 u2DataLen);
VOID
QosRedHandleStandbyUpEvent (VOID);
VOID
QosRedSendBulkUpdTailMsg (VOID);
VOID
QosRedSendBulkUpdates (VOID);
#endif








































