/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: fsqosxwr.h,v 1.15 2016/07/05 07:38:43 siva Exp $
 * *
 * * Description: Protocol Low Level Routines
 * *********************************************************************/


#ifndef _FSQOSXWR_H
#define _FSQOSXWR_H

VOID RegisterFSQOSX(VOID);

VOID UnRegisterFSQOSX(VOID);
INT4 FsQoSSystemControlGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSTrcFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRateUnitGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRateGranularityGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSystemControlSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSTrcFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSystemControlTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSTrcFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSSystemControlDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsQoSStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsQoSTrcFlagDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);



INT4 GetNextIndexFsQoSPriorityMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSPriorityMapNameGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInPriTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenInnerPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapConfigStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInDEIGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenColorGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapNameSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapVlanIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInPriTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenInnerPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInDEISet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenColorSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInPriTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenInnerPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapInDEITest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapRegenColorTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPriorityMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);








INT4 GetNextIndexFsQoSClassMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSClassMapNameGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapL2FilterIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapL3FilterIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapPriorityMapIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapCLASSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapClfrIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapPreColorGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapVlanMapIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapNameSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapL2FilterIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapL3FilterIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapPriorityMapIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapCLASSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapClfrIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapPreColorSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapVlanMapIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapL2FilterIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapL3FilterIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapPriorityMapIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapCLASSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapClfrIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapPreColorTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapVlanMapIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 GetNextIndexFsQoSClassToPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSClassToPriorityRegenPriGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityGroupNameGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityRegenPriSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityGroupNameSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityRegenPriTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityGroupNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSClassToPriorityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



INT4 GetNextIndexFsQoSMeterTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSMeterNameGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterColorModeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterCIRGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterCBSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterEIRGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterEBSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterNextGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterNameSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterColorModeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterCIRSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterCBSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterEIRSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterEBSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterNextSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterColorModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterCIRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterCBSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterEIRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterEBSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterNextTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSMeterTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);










INT4 GetNextIndexFsQoSPolicyMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSPolicyMapNameGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapCLASSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapPHBTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapDefaultPHBGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapMeterTableIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapFailMeterTableIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileConformActionFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileConformActionIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileActionSetPortGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetIpTOSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetVlanPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetVlanDEGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetInnerVlanPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetMplsExpGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetNewCLASSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileExceedActionFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileExceedActionIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetIpTOSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetInnerVlanPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetVlanPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetVlanDEGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetMplsExpGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetNewCLASSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetIPTOSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetInnerVlanPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetVlanPrioGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetVlanDEGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetMplsExpGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetNewCLASSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetInnerVlanDEGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetInnerVlanDEGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetInnerVlanDEGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyDefaultVlanDEGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapNameSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapCLASSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapPHBTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapDefaultPHBSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapMeterTableIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapFailMeterTableIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileConformActionFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileConformActionIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileActionSetPortSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetIpTOSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetVlanPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetVlanDESet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetInnerVlanPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetMplsExpSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetNewCLASSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileExceedActionFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileExceedActionIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetIpTOSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetInnerVlanPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetVlanPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetVlanDESet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetMplsExpSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetNewCLASSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetIPTOSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetInnerVlanPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetVlanPrioSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetVlanDESet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetMplsExpSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetNewCLASSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetInnerVlanDESet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetInnerVlanDESet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetInnerVlanDESet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyDefaultVlanDESet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapCLASSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapPHBTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapDefaultPHBTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapMeterTableIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapFailMeterTableIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileConformActionFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileConformActionIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileActionSetPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetIpTOSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetVlanPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetVlanDETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetInnerVlanPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetMplsExpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetNewCLASSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileExceedActionFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapInProfileExceedActionIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetIpTOSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetInnerVlanPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetVlanPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetVlanDETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetMplsExpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetNewCLASSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetIPTOSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetInnerVlanPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetVlanPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetVlanDETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetMplsExpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetNewCLASSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapConformActionSetInnerVlanDETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapExceedActionSetInnerVlanDETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapOutProfileActionSetInnerVlanDETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyDefaultVlanDETest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicyMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
































INT4 GetNextIndexFsQoSQTemplateTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSQTemplateNameGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateDropTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateDropAlgoEnableFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateNameSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateDropTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateDropAlgoEnableFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateDropTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateDropAlgoEnableFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQTemplateTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexFsQoSRandomDetectCfgTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSRandomDetectCfgMinAvgThreshGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxAvgThreshGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxPktSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxProbGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgExpWeightGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgGainGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgDropThreshTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgECNThreshGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgActionFlagGet(tSnmpIndex *, tRetVal *);

INT4 FsQoSRandomDetectCfgMinAvgThreshSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxAvgThreshSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxPktSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxProbSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgExpWeightSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgGainSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgDropThreshTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgECNThreshSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgActionFlagSet(tSnmpIndex *, tRetVal *);

INT4 FsQoSRandomDetectCfgMinAvgThreshTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxAvgThreshTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxPktSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgMaxProbTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgExpWeightTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgGainTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgDropThreshTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgECNThreshTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSRandomDetectCfgActionFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);

INT4 FsQoSRandomDetectCfgTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsQoSShapeTemplateTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSShapeTemplateNameGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateCIRGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateCBSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateEIRGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateEBSGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateNameSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateCIRSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateCBSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateEIRSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateEBSSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateCIRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateCBSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateEIRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateEBSTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSShapeTemplateTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsQoSQMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSQMapQIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapRegenDEIGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapRegenColorGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapQIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapRegenDEISet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapQIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapRegenDEITest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexFsQoSQTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSQCfgTemplateIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQSchedulerIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQWeightGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQShapeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQCfgTemplateIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQSchedulerIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQWeightSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQShapeIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSQCfgTemplateIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQSchedulerIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQWeightTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQShapeIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSQTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 GetNextIndexFsQoSSchedulerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSSchedulerSchedAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerShapeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerHierarchyLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerGlobalIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerSchedAlgorithmSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerShapeIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerHierarchyLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerGlobalIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerSchedAlgorithmTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerShapeIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerHierarchyLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerGlobalIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSSchedulerTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexFsQoSHierarchyTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSHierarchyQNextGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchySchedNextGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyWeightGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyQNextSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchySchedNextSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyWeightSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyQNextTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchySchedNextTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyWeightTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSHierarchyTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 GetNextIndexFsQoSDefUserPriorityTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSPortDefaultUserPriorityGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPortPbitPrefOverDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPortDefaultUserPrioritySet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPortPbitPrefOverDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPortDefaultUserPriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPortPbitPrefOverDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSDefUserPriorityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

INT4 GetNextIndexFsQoSPolicerStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSPolicerStatsConformPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsConformOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsExceedPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsExceedOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsViolatePktsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsViolateOctetsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsClearCounterGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsClearCounterSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsClearCounterTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPolicerStatsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsQoSCoSQStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSCoSQStatsEnQPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSCoSQStatsEnQBytesGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSCoSQStatsDeQPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSCoSQStatsDeQBytesGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSCoSQStatsDiscardPktsGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSCoSQStatsDiscardBytesGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSCoSQStatsOccupancyGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSCoSQStatsCongMgntAlgoDropGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexFsQosHwCpuRateLimitTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQosHwCpuMinRateGet(tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuMaxRateGet(tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuMinRateSet(tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuMaxRateSet(tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuMinRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuMaxRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQosHwCpuRateLimitTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsQoSPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSPcpSelRowGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPcpRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPcpSelRowSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPcpRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSPcpSelRowTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPcpRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

/**********************************************************
 *               VLAN QUEUEING TABLE
***********************************************************/
INT4 FsQoSVlanQueueingStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanQueueingStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanQueueingStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanQueueingStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsQoSVlanMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsQoSVlanMapIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapVlanIdGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapVlanIdSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapVlanIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsQoSVlanMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _FSQOSXWR_H */
