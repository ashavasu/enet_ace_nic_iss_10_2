/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* * $Id: fsqosxlw.h,v 1.16 2016/07/05 07:38:43 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSSystemControl ARG_LIST((INT4 *));

INT1
nmhGetFsQoSStatus ARG_LIST((INT4 *));

INT1
nmhGetFsQoSTrcFlag ARG_LIST((UINT4 *));

INT1
nmhGetFsQoSRateUnit ARG_LIST((INT4 *));

INT1
nmhGetFsQoSRateGranularity ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSSystemControl ARG_LIST((INT4 ));

INT1
nmhSetFsQoSStatus ARG_LIST((INT4 ));

INT1
nmhSetFsQoSTrcFlag ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSSystemControl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsQoSStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsQoSTrcFlag ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSSystemControl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsQoSStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsQoSTrcFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSPriorityMapTable. */
INT1
nmhValidateIndexInstanceFsQoSPriorityMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSPriorityMapTable  */

INT1
nmhGetFirstIndexFsQoSPriorityMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSPriorityMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSPriorityMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSPriorityMapIfIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPriorityMapVlanId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPriorityMapInPriority ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPriorityMapInPriType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPriorityMapRegenPriority ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPriorityMapRegenInnerPriority ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPriorityMapConfigStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPriorityMapStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPriorityMapInDEI ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPriorityMapRegenColor ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSPriorityMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSPriorityMapIfIndex ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPriorityMapVlanId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPriorityMapInPriority ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPriorityMapInPriType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPriorityMapRegenPriority ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPriorityMapRegenInnerPriority ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPriorityMapStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPriorityMapInDEI ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPriorityMapRegenColor ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSPriorityMapName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSPriorityMapIfIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPriorityMapVlanId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPriorityMapInPriority ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPriorityMapInPriType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPriorityMapRegenPriority ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPriorityMapRegenInnerPriority ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPriorityMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPriorityMapInDEI ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPriorityMapRegenColor ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSPriorityMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSClassMapTable. */
INT1
nmhValidateIndexInstanceFsQoSClassMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSClassMapTable  */

INT1
nmhGetFirstIndexFsQoSClassMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSClassMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSClassMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSClassMapL2FilterId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSClassMapL3FilterId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSClassMapPriorityMapId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSClassMapCLASS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSClassMapClfrId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSClassMapPreColor ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSClassMapStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSClassMapVlanMapId ARG_LIST((UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSClassMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSClassMapL2FilterId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSClassMapL3FilterId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSClassMapPriorityMapId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSClassMapCLASS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSClassMapClfrId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSClassMapPreColor ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSClassMapStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSClassMapVlanMapId ARG_LIST((UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSClassMapName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSClassMapL2FilterId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSClassMapL3FilterId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSClassMapPriorityMapId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSClassMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSClassMapClfrId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSClassMapPreColor ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSClassMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSClassMapVlanMapId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSClassMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSClassToPriorityTable. */
INT1
nmhValidateIndexInstanceFsQoSClassToPriorityTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSClassToPriorityTable  */

INT1
nmhGetFirstIndexFsQoSClassToPriorityTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSClassToPriorityTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSClassToPriorityRegenPri ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSClassToPriorityGroupName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSClassToPriorityStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSClassToPriorityRegenPri ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSClassToPriorityGroupName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSClassToPriorityStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSClassToPriorityRegenPri ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSClassToPriorityGroupName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSClassToPriorityStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSClassToPriorityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSMeterTable. */
INT1
nmhValidateIndexInstanceFsQoSMeterTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSMeterTable  */

INT1
nmhGetFirstIndexFsQoSMeterTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSMeterTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSMeterName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSMeterType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSMeterInterval ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSMeterColorMode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSMeterCIR ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSMeterCBS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSMeterEIR ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSMeterEBS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSMeterNext ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSMeterStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSMeterName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSMeterType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSMeterInterval ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSMeterColorMode ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSMeterCIR ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSMeterCBS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSMeterEIR ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSMeterEBS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSMeterNext ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSMeterStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSMeterName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSMeterType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSMeterInterval ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSMeterColorMode ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSMeterCIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSMeterCBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSMeterEIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSMeterEBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSMeterNext ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSMeterStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSMeterTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSPolicyMapTable. */
INT1
nmhValidateIndexInstanceFsQoSPolicyMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSPolicyMapTable  */

INT1
nmhGetFirstIndexFsQoSPolicyMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSPolicyMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSPolicyMapName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSPolicyMapIfIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapCLASS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapPHBType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPolicyMapDefaultPHB ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapMeterTableId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapFailMeterTableId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapInProfileConformActionFlag ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSPolicyMapInProfileConformActionId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapInProfileActionSetPort ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapConformActionSetIpTOS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapConformActionSetDscp ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPolicyMapConformActionSetVlanPrio ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapConformActionSetVlanDE ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapConformActionSetInnerVlanPrio ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapConformActionSetMplsExp ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapConformActionSetNewCLASS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapInProfileExceedActionFlag ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSPolicyMapInProfileExceedActionId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapExceedActionSetIpTOS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapExceedActionSetDscp ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPolicyMapExceedActionSetInnerVlanPrio ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapExceedActionSetVlanPrio ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapExceedActionSetVlanDE ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapExceedActionSetMplsExp ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapExceedActionSetNewCLASS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionFlag ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSPolicyMapOutProfileActionId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionSetIPTOS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionSetDscp ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionSetVlanPrio ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionSetVlanDE ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionSetMplsExp ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionSetNewCLASS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPolicyMapConformActionSetInnerVlanDE ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapExceedActionSetInnerVlanDE ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyMapOutProfileActionSetInnerVlanDE ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSPolicyDefaultVlanDE ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSPolicyMapName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSPolicyMapIfIndex ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapCLASS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapPHBType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPolicyMapDefaultPHB ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapMeterTableId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapFailMeterTableId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapInProfileConformActionFlag ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSPolicyMapInProfileConformActionId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapInProfileActionSetPort ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapConformActionSetIpTOS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapConformActionSetDscp ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPolicyMapConformActionSetVlanPrio ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapConformActionSetVlanDE ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapConformActionSetInnerVlanPrio ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapConformActionSetMplsExp ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapConformActionSetNewCLASS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapInProfileExceedActionFlag ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSPolicyMapInProfileExceedActionId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapExceedActionSetIpTOS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapExceedActionSetDscp ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPolicyMapExceedActionSetInnerVlanPrio ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapExceedActionSetVlanPrio ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapExceedActionSetVlanDE ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapExceedActionSetMplsExp ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapExceedActionSetNewCLASS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionFlag ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSPolicyMapOutProfileActionId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionSetIPTOS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionSetDscp ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanPrio ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionSetVlanPrio ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionSetVlanDE ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionSetMplsExp ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionSetNewCLASS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPolicyMapConformActionSetInnerVlanDE ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapExceedActionSetInnerVlanDE ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyMapOutProfileActionSetInnerVlanDE ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSPolicyDefaultVlanDE ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSPolicyMapName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSPolicyMapIfIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapPHBType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPolicyMapDefaultPHB ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapMeterTableId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapFailMeterTableId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapInProfileConformActionFlag ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSPolicyMapInProfileConformActionId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapInProfileActionSetPort ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapConformActionSetIpTOS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapConformActionSetDscp ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPolicyMapConformActionSetVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapConformActionSetVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapConformActionSetMplsExp ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapConformActionSetNewCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapInProfileExceedActionFlag ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSPolicyMapInProfileExceedActionId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapExceedActionSetIpTOS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapExceedActionSetDscp ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapExceedActionSetVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapExceedActionSetVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapExceedActionSetMplsExp ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapExceedActionSetNewCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionFlag ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetIPTOS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetDscp ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanPrio ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetMplsExp ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetNewCLASS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPolicyMapConformActionSetInnerVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapExceedActionSetInnerVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyMapOutProfileActionSetInnerVlanDE ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSPolicyDefaultVlanDE ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSPolicyMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSQTemplateTable. */
INT1
nmhValidateIndexInstanceFsQoSQTemplateTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSQTemplateTable  */

INT1
nmhGetFirstIndexFsQoSQTemplateTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSQTemplateTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSQTemplateName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSQTemplateDropType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSQTemplateDropAlgoEnableFlag ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSQTemplateSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSQTemplateStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSQTemplateName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSQTemplateDropType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSQTemplateDropAlgoEnableFlag ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSQTemplateSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSQTemplateStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSQTemplateName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSQTemplateDropType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSQTemplateDropAlgoEnableFlag ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSQTemplateSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSQTemplateStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSQTemplateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSRandomDetectCfgTable. */
INT1
nmhValidateIndexInstanceFsQoSRandomDetectCfgTable ARG_LIST((UINT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSRandomDetectCfgTable  */

INT1
nmhGetFirstIndexFsQoSRandomDetectCfgTable ARG_LIST((UINT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSRandomDetectCfgTable ARG_LIST((UINT4 , UINT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSRandomDetectCfgMinAvgThresh ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsQoSRandomDetectCfgMaxAvgThresh ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsQoSRandomDetectCfgMaxPktSize ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsQoSRandomDetectCfgMaxProb ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsQoSRandomDetectCfgExpWeight ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsQoSRandomDetectCfgStatus ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsQoSRandomDetectCfgGain ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsQoSRandomDetectCfgDropThreshType ARG_LIST((UINT4  , INT4 ,INT4 *));

INT1
nmhGetFsQoSRandomDetectCfgECNThresh ARG_LIST((UINT4  , INT4 ,UINT4 *));

INT1
nmhGetFsQoSRandomDetectCfgActionFlag ARG_LIST((UINT4  , INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSRandomDetectCfgMinAvgThresh ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsQoSRandomDetectCfgMaxAvgThresh ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsQoSRandomDetectCfgMaxPktSize ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsQoSRandomDetectCfgMaxProb ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsQoSRandomDetectCfgExpWeight ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsQoSRandomDetectCfgStatus ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsQoSRandomDetectCfgGain ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsQoSRandomDetectCfgDropThreshType ARG_LIST((UINT4  , INT4  ,INT4 ));

INT1
nmhSetFsQoSRandomDetectCfgECNThresh ARG_LIST((UINT4  , INT4  ,UINT4 ));

INT1
nmhSetFsQoSRandomDetectCfgActionFlag ARG_LIST((UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSRandomDetectCfgMinAvgThresh ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgMaxAvgThresh ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgMaxPktSize ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgMaxProb ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgExpWeight ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgStatus ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgGain ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgDropThreshType ARG_LIST((UINT4 *  ,UINT4  , INT4  ,INT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgECNThresh ARG_LIST((UINT4 *  ,UINT4  , INT4  ,UINT4 ));

INT1
nmhTestv2FsQoSRandomDetectCfgActionFlag ARG_LIST((UINT4 *  ,UINT4  , INT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSRandomDetectCfgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSShapeTemplateTable. */
INT1
nmhValidateIndexInstanceFsQoSShapeTemplateTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSShapeTemplateTable  */

INT1
nmhGetFirstIndexFsQoSShapeTemplateTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSShapeTemplateTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSShapeTemplateName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsQoSShapeTemplateCIR ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSShapeTemplateCBS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSShapeTemplateEIR ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSShapeTemplateEBS ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSShapeTemplateStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSShapeTemplateName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsQoSShapeTemplateCIR ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSShapeTemplateCBS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSShapeTemplateEIR ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSShapeTemplateEBS ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSShapeTemplateStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSShapeTemplateName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsQoSShapeTemplateCIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSShapeTemplateCBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSShapeTemplateEIR ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSShapeTemplateEBS ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSShapeTemplateStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSShapeTemplateTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSQMapTable. */
INT1
nmhValidateIndexInstanceFsQoSQMapTable ARG_LIST((INT4  , UINT4  , INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSQMapTable  */

INT1
nmhGetFirstIndexFsQoSQMapTable ARG_LIST((INT4 * , UINT4 * , INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSQMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSQMapQId ARG_LIST((INT4  , UINT4  , INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSQMapStatus ARG_LIST((INT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsQoSQMapRegenDEI ARG_LIST((INT4  , UINT4  , INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsQoSQMapRegenColor ARG_LIST((INT4  , UINT4  , INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSQMapQId ARG_LIST((INT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSQMapStatus ARG_LIST((INT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsQoSQMapRegenDEI ARG_LIST((INT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSQMapQId ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSQMapStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSQMapRegenDEI ARG_LIST((UINT4 *  ,INT4  , UINT4  , INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSQMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSQTable. */
INT1
nmhValidateIndexInstanceFsQoSQTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSQTable  */

INT1
nmhGetFirstIndexFsQoSQTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSQTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSQCfgTemplateId ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSQSchedulerId ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSQWeight ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSQPriority ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSQShapeId ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSQStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsQoSQType ARG_LIST((INT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSQCfgTemplateId ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSQSchedulerId ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSQWeight ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSQPriority ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSQShapeId ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSQStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsQoSQType ARG_LIST((INT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSQCfgTemplateId ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSQSchedulerId ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSQWeight ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSQPriority ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSQShapeId ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSQStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSQType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSQTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSSchedulerTable. */
INT1
nmhValidateIndexInstanceFsQoSSchedulerTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSSchedulerTable  */

INT1
nmhGetFirstIndexFsQoSSchedulerTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSSchedulerTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSSchedulerSchedAlgorithm ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsQoSSchedulerShapeId ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSSchedulerHierarchyLevel ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSSchedulerGlobalId ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSSchedulerStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSSchedulerSchedAlgorithm ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsQoSSchedulerShapeId ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSSchedulerHierarchyLevel ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSSchedulerGlobalId ARG_LIST((INT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSSchedulerStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSSchedulerSchedAlgorithm ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSSchedulerShapeId ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSSchedulerHierarchyLevel ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSSchedulerGlobalId ARG_LIST((UINT4 *  ,INT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSSchedulerStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSSchedulerTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSHierarchyTable. */
INT1
nmhValidateIndexInstanceFsQoSHierarchyTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSHierarchyTable  */

INT1
nmhGetFirstIndexFsQoSHierarchyTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSHierarchyTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSHierarchyQNext ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSHierarchySchedNext ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSHierarchyWeight ARG_LIST((INT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsQoSHierarchyPriority ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsQoSHierarchyStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSHierarchyQNext ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSHierarchySchedNext ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSHierarchyWeight ARG_LIST((INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsQoSHierarchyPriority ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsQoSHierarchyStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSHierarchyQNext ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSHierarchySchedNext ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSHierarchyWeight ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSHierarchyPriority ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSHierarchyStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSHierarchyTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSDefUserPriorityTable. */
INT1
nmhValidateIndexInstanceFsQoSDefUserPriorityTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSDefUserPriorityTable  */

INT1
nmhGetFirstIndexFsQoSDefUserPriorityTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSDefUserPriorityTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSPortDefaultUserPriority ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsQoSPortPbitPrefOverDscp ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSPortDefaultUserPriority ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsQoSPortPbitPrefOverDscp ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSPortDefaultUserPriority ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsQoSPortPbitPrefOverDscp ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSDefUserPriorityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSPolicerStatsTable. */
INT1
nmhValidateIndexInstanceFsQoSPolicerStatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSPolicerStatsTable  */

INT1
nmhGetFirstIndexFsQoSPolicerStatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSPolicerStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSPolicerStatsConformPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSPolicerStatsConformOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSPolicerStatsExceedPkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSPolicerStatsExceedOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSPolicerStatsViolatePkts ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSPolicerStatsViolateOctets ARG_LIST((UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSPolicerStatsStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsQoSPolicerStatsClearCounter ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSPolicerStatsStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsQoSPolicerStatsClearCounter ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSPolicerStatsStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsQoSPolicerStatsClearCounter ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSPolicerStatsTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSCoSQStatsTable. */
INT1
nmhValidateIndexInstanceFsQoSCoSQStatsTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSCoSQStatsTable  */

INT1
nmhGetFirstIndexFsQoSCoSQStatsTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSCoSQStatsTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSCoSQStatsEnQPkts ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSCoSQStatsEnQBytes ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSCoSQStatsDeQPkts ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSCoSQStatsDeQBytes ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSCoSQStatsDiscardPkts ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSCoSQStatsDiscardBytes ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSCoSQStatsOccupancy ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetFsQoSCoSQStatsCongMgntAlgoDrop ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for FsQosHwCpuRateLimitTable. */
INT1
nmhValidateIndexInstanceFsQosHwCpuRateLimitTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQosHwCpuRateLimitTable  */

INT1
nmhGetFirstIndexFsQosHwCpuRateLimitTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQosHwCpuRateLimitTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQosHwCpuMinRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQosHwCpuMaxRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQosHwCpuRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQosHwCpuMinRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQosHwCpuMaxRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQosHwCpuRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQosHwCpuMinRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQosHwCpuMaxRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQosHwCpuRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQosHwCpuRateLimitTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSVlanQueueingStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSVlanQueueingStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsQoSVlanQueueingStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSVlanQueueingStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsQoSVlanMapTable. */
INT1
nmhValidateIndexInstanceFsQoSVlanMapTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsQoSVlanMapTable  */

INT1
nmhGetFirstIndexFsQoSVlanMapTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsQoSVlanMapTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsQoSVlanMapIfIndex ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSVlanMapVlanId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsQoSVlanMapStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsQoSVlanMapIfIndex ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSVlanMapVlanId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsQoSVlanMapStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsQoSVlanMapIfIndex ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));               

INT1
nmhGetFsQoSDscpQueueMap ARG_LIST((INT4 ,INT4 *));
/* Proto Validate Index Instance for FsQoSPortTable. */
INT1
nmhValidateIndexInstanceFsQoSPortTable ARG_LIST((INT4  , INT4 ));
 
/* Proto Type for Low Level GET FIRST fn for FsQoSPortTable  */
 
INT1
nmhGetFirstIndexFsQoSPortTable ARG_LIST((INT4 * , INT4 *));
 
/* Proto type for GET_NEXT Routine.  */
 
INT1
nmhGetNextIndexFsQoSPortTable ARG_LIST((INT4 , INT4 * , INT4 , INT4 *));
 
/* Proto type for Low Level GET Routine All Objects.  */
 
INT1
nmhGetFsQoSPcpSelRow ARG_LIST((INT4  , INT4 ,INT4 *));
 
INT1
nmhGetFsQoSPcpRowStatus ARG_LIST((INT4  , INT4 ,INT4 *));
 
/* Low Level SET Routine for All Objects.  */
 
INT1
nmhSetFsQoSPcpSelRow ARG_LIST((INT4  , INT4  ,INT4 ));
 
INT1
nmhSetFsQoSPcpRowStatus ARG_LIST((INT4  , INT4  ,INT4 ));
 
/* Low Level TEST Routines for.  */
 
INT1
nmhTestv2FsQoSPcpSelRow ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
 
INT1
nmhTestv2FsQoSPcpRowStatus ARG_LIST((UINT4 *  ,INT4  , INT4  ,INT4 ));
 
/* Low Level DEP Routines for.  */
 
INT1
nmhDepv2FsQoSPortTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));


INT1
nmhTestv2FsQoSVlanMapVlanId ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsQoSVlanMapStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsQoSVlanMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
