/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdqosdb.h,v 1.1 2009/01/06 07:20:11 prabuc-iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDQOSDB_H
#define _STDQOSDB_H

UINT1 DiffServDataPathTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER};
UINT1 DiffServClfrTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServClfrElementTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServMultiFieldClfrTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServMeterTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServTBParamTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServActionTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServDscpMarkActTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 DiffServCountActTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServAlgDropTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServRandomDropTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServQTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServSchedulerTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServMinRateTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 DiffServMaxRateTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdqos [] ={1,3,6,1,2,1,97};
tSNMP_OID_TYPE stdqosOID = {7, stdqos};


UINT4 DiffServDataPathIfDirection [ ] ={1,3,6,1,2,1,97,1,1,1,1,1};
UINT4 DiffServDataPathStart [ ] ={1,3,6,1,2,1,97,1,1,1,1,2};
UINT4 DiffServDataPathStorage [ ] ={1,3,6,1,2,1,97,1,1,1,1,3};
UINT4 DiffServDataPathStatus [ ] ={1,3,6,1,2,1,97,1,1,1,1,4};
UINT4 DiffServClfrNextFree [ ] ={1,3,6,1,2,1,97,1,2,1};
UINT4 DiffServClfrId [ ] ={1,3,6,1,2,1,97,1,2,2,1,1};
UINT4 DiffServClfrStorage [ ] ={1,3,6,1,2,1,97,1,2,2,1,2};
UINT4 DiffServClfrStatus [ ] ={1,3,6,1,2,1,97,1,2,2,1,3};
UINT4 DiffServClfrElementNextFree [ ] ={1,3,6,1,2,1,97,1,2,3};
UINT4 DiffServClfrElementId [ ] ={1,3,6,1,2,1,97,1,2,4,1,1};
UINT4 DiffServClfrElementPrecedence [ ] ={1,3,6,1,2,1,97,1,2,4,1,2};
UINT4 DiffServClfrElementNext [ ] ={1,3,6,1,2,1,97,1,2,4,1,3};
UINT4 DiffServClfrElementSpecific [ ] ={1,3,6,1,2,1,97,1,2,4,1,4};
UINT4 DiffServClfrElementStorage [ ] ={1,3,6,1,2,1,97,1,2,4,1,5};
UINT4 DiffServClfrElementStatus [ ] ={1,3,6,1,2,1,97,1,2,4,1,6};
UINT4 DiffServMultiFieldClfrNextFree [ ] ={1,3,6,1,2,1,97,1,2,5};
UINT4 DiffServMultiFieldClfrId [ ] ={1,3,6,1,2,1,97,1,2,6,1,1};
UINT4 DiffServMultiFieldClfrAddrType [ ] ={1,3,6,1,2,1,97,1,2,6,1,2};
UINT4 DiffServMultiFieldClfrDstAddr [ ] ={1,3,6,1,2,1,97,1,2,6,1,3};
UINT4 DiffServMultiFieldClfrDstPrefixLength [ ] ={1,3,6,1,2,1,97,1,2,6,1,4};
UINT4 DiffServMultiFieldClfrSrcAddr [ ] ={1,3,6,1,2,1,97,1,2,6,1,5};
UINT4 DiffServMultiFieldClfrSrcPrefixLength [ ] ={1,3,6,1,2,1,97,1,2,6,1,6};
UINT4 DiffServMultiFieldClfrDscp [ ] ={1,3,6,1,2,1,97,1,2,6,1,7};
UINT4 DiffServMultiFieldClfrFlowId [ ] ={1,3,6,1,2,1,97,1,2,6,1,8};
UINT4 DiffServMultiFieldClfrProtocol [ ] ={1,3,6,1,2,1,97,1,2,6,1,9};
UINT4 DiffServMultiFieldClfrDstL4PortMin [ ] ={1,3,6,1,2,1,97,1,2,6,1,10};
UINT4 DiffServMultiFieldClfrDstL4PortMax [ ] ={1,3,6,1,2,1,97,1,2,6,1,11};
UINT4 DiffServMultiFieldClfrSrcL4PortMin [ ] ={1,3,6,1,2,1,97,1,2,6,1,12};
UINT4 DiffServMultiFieldClfrSrcL4PortMax [ ] ={1,3,6,1,2,1,97,1,2,6,1,13};
UINT4 DiffServMultiFieldClfrStorage [ ] ={1,3,6,1,2,1,97,1,2,6,1,14};
UINT4 DiffServMultiFieldClfrStatus [ ] ={1,3,6,1,2,1,97,1,2,6,1,15};
UINT4 DiffServMeterNextFree [ ] ={1,3,6,1,2,1,97,1,3,1};
UINT4 DiffServMeterId [ ] ={1,3,6,1,2,1,97,1,3,2,1,1};
UINT4 DiffServMeterSucceedNext [ ] ={1,3,6,1,2,1,97,1,3,2,1,2};
UINT4 DiffServMeterFailNext [ ] ={1,3,6,1,2,1,97,1,3,2,1,3};
UINT4 DiffServMeterSpecific [ ] ={1,3,6,1,2,1,97,1,3,2,1,4};
UINT4 DiffServMeterStorage [ ] ={1,3,6,1,2,1,97,1,3,2,1,5};
UINT4 DiffServMeterStatus [ ] ={1,3,6,1,2,1,97,1,3,2,1,6};
UINT4 DiffServTBParamNextFree [ ] ={1,3,6,1,2,1,97,1,4,1};
UINT4 DiffServTBParamId [ ] ={1,3,6,1,2,1,97,1,4,2,1,1};
UINT4 DiffServTBParamType [ ] ={1,3,6,1,2,1,97,1,4,2,1,2};
UINT4 DiffServTBParamRate [ ] ={1,3,6,1,2,1,97,1,4,2,1,3};
UINT4 DiffServTBParamBurstSize [ ] ={1,3,6,1,2,1,97,1,4,2,1,4};
UINT4 DiffServTBParamInterval [ ] ={1,3,6,1,2,1,97,1,4,2,1,5};
UINT4 DiffServTBParamStorage [ ] ={1,3,6,1,2,1,97,1,4,2,1,6};
UINT4 DiffServTBParamStatus [ ] ={1,3,6,1,2,1,97,1,4,2,1,7};
UINT4 DiffServActionNextFree [ ] ={1,3,6,1,2,1,97,1,5,1};
UINT4 DiffServActionId [ ] ={1,3,6,1,2,1,97,1,5,2,1,1};
UINT4 DiffServActionInterface [ ] ={1,3,6,1,2,1,97,1,5,2,1,2};
UINT4 DiffServActionNext [ ] ={1,3,6,1,2,1,97,1,5,2,1,3};
UINT4 DiffServActionSpecific [ ] ={1,3,6,1,2,1,97,1,5,2,1,4};
UINT4 DiffServActionStorage [ ] ={1,3,6,1,2,1,97,1,5,2,1,5};
UINT4 DiffServActionStatus [ ] ={1,3,6,1,2,1,97,1,5,2,1,6};
UINT4 DiffServDscpMarkActDscp [ ] ={1,3,6,1,2,1,97,1,5,3,1,1};
UINT4 DiffServCountActNextFree [ ] ={1,3,6,1,2,1,97,1,5,4};
UINT4 DiffServCountActId [ ] ={1,3,6,1,2,1,97,1,5,5,1,1};
UINT4 DiffServCountActOctets [ ] ={1,3,6,1,2,1,97,1,5,5,1,2};
UINT4 DiffServCountActPkts [ ] ={1,3,6,1,2,1,97,1,5,5,1,3};
UINT4 DiffServCountActStorage [ ] ={1,3,6,1,2,1,97,1,5,5,1,4};
UINT4 DiffServCountActStatus [ ] ={1,3,6,1,2,1,97,1,5,5,1,5};
UINT4 DiffServAlgDropNextFree [ ] ={1,3,6,1,2,1,97,1,6,1};
UINT4 DiffServAlgDropId [ ] ={1,3,6,1,2,1,97,1,6,2,1,1};
UINT4 DiffServAlgDropType [ ] ={1,3,6,1,2,1,97,1,6,2,1,2};
UINT4 DiffServAlgDropNext [ ] ={1,3,6,1,2,1,97,1,6,2,1,3};
UINT4 DiffServAlgDropQMeasure [ ] ={1,3,6,1,2,1,97,1,6,2,1,4};
UINT4 DiffServAlgDropQThreshold [ ] ={1,3,6,1,2,1,97,1,6,2,1,5};
UINT4 DiffServAlgDropSpecific [ ] ={1,3,6,1,2,1,97,1,6,2,1,6};
UINT4 DiffServAlgDropOctets [ ] ={1,3,6,1,2,1,97,1,6,2,1,7};
UINT4 DiffServAlgDropPkts [ ] ={1,3,6,1,2,1,97,1,6,2,1,8};
UINT4 DiffServAlgRandomDropOctets [ ] ={1,3,6,1,2,1,97,1,6,2,1,9};
UINT4 DiffServAlgRandomDropPkts [ ] ={1,3,6,1,2,1,97,1,6,2,1,10};
UINT4 DiffServAlgDropStorage [ ] ={1,3,6,1,2,1,97,1,6,2,1,11};
UINT4 DiffServAlgDropStatus [ ] ={1,3,6,1,2,1,97,1,6,2,1,12};
UINT4 DiffServRandomDropNextFree [ ] ={1,3,6,1,2,1,97,1,6,3};
UINT4 DiffServRandomDropId [ ] ={1,3,6,1,2,1,97,1,6,4,1,1};
UINT4 DiffServRandomDropMinThreshBytes [ ] ={1,3,6,1,2,1,97,1,6,4,1,2};
UINT4 DiffServRandomDropMinThreshPkts [ ] ={1,3,6,1,2,1,97,1,6,4,1,3};
UINT4 DiffServRandomDropMaxThreshBytes [ ] ={1,3,6,1,2,1,97,1,6,4,1,4};
UINT4 DiffServRandomDropMaxThreshPkts [ ] ={1,3,6,1,2,1,97,1,6,4,1,5};
UINT4 DiffServRandomDropProbMax [ ] ={1,3,6,1,2,1,97,1,6,4,1,6};
UINT4 DiffServRandomDropWeight [ ] ={1,3,6,1,2,1,97,1,6,4,1,7};
UINT4 DiffServRandomDropSamplingRate [ ] ={1,3,6,1,2,1,97,1,6,4,1,8};
UINT4 DiffServRandomDropStorage [ ] ={1,3,6,1,2,1,97,1,6,4,1,9};
UINT4 DiffServRandomDropStatus [ ] ={1,3,6,1,2,1,97,1,6,4,1,10};
UINT4 DiffServQNextFree [ ] ={1,3,6,1,2,1,97,1,7,1};
UINT4 DiffServQId [ ] ={1,3,6,1,2,1,97,1,7,2,1,1};
UINT4 DiffServQNext [ ] ={1,3,6,1,2,1,97,1,7,2,1,2};
UINT4 DiffServQMinRate [ ] ={1,3,6,1,2,1,97,1,7,2,1,3};
UINT4 DiffServQMaxRate [ ] ={1,3,6,1,2,1,97,1,7,2,1,4};
UINT4 DiffServQStorage [ ] ={1,3,6,1,2,1,97,1,7,2,1,5};
UINT4 DiffServQStatus [ ] ={1,3,6,1,2,1,97,1,7,2,1,6};
UINT4 DiffServSchedulerNextFree [ ] ={1,3,6,1,2,1,97,1,8,1};
UINT4 DiffServSchedulerId [ ] ={1,3,6,1,2,1,97,1,8,2,1,1};
UINT4 DiffServSchedulerNext [ ] ={1,3,6,1,2,1,97,1,8,2,1,2};
UINT4 DiffServSchedulerMethod [ ] ={1,3,6,1,2,1,97,1,8,2,1,3};
UINT4 DiffServSchedulerMinRate [ ] ={1,3,6,1,2,1,97,1,8,2,1,4};
UINT4 DiffServSchedulerMaxRate [ ] ={1,3,6,1,2,1,97,1,8,2,1,5};
UINT4 DiffServSchedulerStorage [ ] ={1,3,6,1,2,1,97,1,8,2,1,6};
UINT4 DiffServSchedulerStatus [ ] ={1,3,6,1,2,1,97,1,8,2,1,7};
UINT4 DiffServMinRateNextFree [ ] ={1,3,6,1,2,1,97,1,8,3};
UINT4 DiffServMinRateId [ ] ={1,3,6,1,2,1,97,1,8,4,1,1};
UINT4 DiffServMinRatePriority [ ] ={1,3,6,1,2,1,97,1,8,4,1,2};
UINT4 DiffServMinRateAbsolute [ ] ={1,3,6,1,2,1,97,1,8,4,1,3};
UINT4 DiffServMinRateRelative [ ] ={1,3,6,1,2,1,97,1,8,4,1,4};
UINT4 DiffServMinRateStorage [ ] ={1,3,6,1,2,1,97,1,8,4,1,5};
UINT4 DiffServMinRateStatus [ ] ={1,3,6,1,2,1,97,1,8,4,1,6};
UINT4 DiffServMaxRateNextFree [ ] ={1,3,6,1,2,1,97,1,8,5};
UINT4 DiffServMaxRateId [ ] ={1,3,6,1,2,1,97,1,8,6,1,1};
UINT4 DiffServMaxRateLevel [ ] ={1,3,6,1,2,1,97,1,8,6,1,2};
UINT4 DiffServMaxRateAbsolute [ ] ={1,3,6,1,2,1,97,1,8,6,1,3};
UINT4 DiffServMaxRateRelative [ ] ={1,3,6,1,2,1,97,1,8,6,1,4};
UINT4 DiffServMaxRateThreshold [ ] ={1,3,6,1,2,1,97,1,8,6,1,5};
UINT4 DiffServMaxRateStorage [ ] ={1,3,6,1,2,1,97,1,8,6,1,6};
UINT4 DiffServMaxRateStatus [ ] ={1,3,6,1,2,1,97,1,8,6,1,7};


tMbDbEntry stdqosMibEntry[]= {

{{12,DiffServDataPathIfDirection}, GetNextIndexDiffServDataPathTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, DiffServDataPathTableINDEX, 2, 0, 0, NULL},

{{12,DiffServDataPathStart}, GetNextIndexDiffServDataPathTable, DiffServDataPathStartGet, DiffServDataPathStartSet, DiffServDataPathStartTest, DiffServDataPathTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServDataPathTableINDEX, 2, 0, 0, NULL},

{{12,DiffServDataPathStorage}, GetNextIndexDiffServDataPathTable, DiffServDataPathStorageGet, DiffServDataPathStorageSet, DiffServDataPathStorageTest, DiffServDataPathTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServDataPathTableINDEX, 2, 0, 0, "3"},

{{12,DiffServDataPathStatus}, GetNextIndexDiffServDataPathTable, DiffServDataPathStatusGet, DiffServDataPathStatusSet, DiffServDataPathStatusTest, DiffServDataPathTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServDataPathTableINDEX, 2, 0, 1, NULL},

{{10,DiffServClfrNextFree}, NULL, DiffServClfrNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServClfrId}, GetNextIndexDiffServClfrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServClfrTableINDEX, 1, 0, 0, NULL},

{{12,DiffServClfrStorage}, GetNextIndexDiffServClfrTable, DiffServClfrStorageGet, DiffServClfrStorageSet, DiffServClfrStorageTest, DiffServClfrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServClfrTableINDEX, 1, 0, 0, "3"},

{{12,DiffServClfrStatus}, GetNextIndexDiffServClfrTable, DiffServClfrStatusGet, DiffServClfrStatusSet, DiffServClfrStatusTest, DiffServClfrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServClfrTableINDEX, 1, 0, 1, NULL},

{{10,DiffServClfrElementNextFree}, NULL, DiffServClfrElementNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServClfrElementId}, GetNextIndexDiffServClfrElementTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServClfrElementTableINDEX, 2, 0, 0, NULL},

{{12,DiffServClfrElementPrecedence}, GetNextIndexDiffServClfrElementTable, DiffServClfrElementPrecedenceGet, DiffServClfrElementPrecedenceSet, DiffServClfrElementPrecedenceTest, DiffServClfrElementTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServClfrElementTableINDEX, 2, 0, 0, NULL},

{{12,DiffServClfrElementNext}, GetNextIndexDiffServClfrElementTable, DiffServClfrElementNextGet, DiffServClfrElementNextSet, DiffServClfrElementNextTest, DiffServClfrElementTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServClfrElementTableINDEX, 2, 0, 0, NULL},

{{12,DiffServClfrElementSpecific}, GetNextIndexDiffServClfrElementTable, DiffServClfrElementSpecificGet, DiffServClfrElementSpecificSet, DiffServClfrElementSpecificTest, DiffServClfrElementTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServClfrElementTableINDEX, 2, 0, 0, NULL},

{{12,DiffServClfrElementStorage}, GetNextIndexDiffServClfrElementTable, DiffServClfrElementStorageGet, DiffServClfrElementStorageSet, DiffServClfrElementStorageTest, DiffServClfrElementTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServClfrElementTableINDEX, 2, 0, 0, "3"},

{{12,DiffServClfrElementStatus}, GetNextIndexDiffServClfrElementTable, DiffServClfrElementStatusGet, DiffServClfrElementStatusSet, DiffServClfrElementStatusTest, DiffServClfrElementTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServClfrElementTableINDEX, 2, 0, 1, NULL},

{{10,DiffServMultiFieldClfrNextFree}, NULL, DiffServMultiFieldClfrNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServMultiFieldClfrId}, GetNextIndexDiffServMultiFieldClfrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMultiFieldClfrAddrType}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrAddrTypeGet, DiffServMultiFieldClfrAddrTypeSet, DiffServMultiFieldClfrAddrTypeTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMultiFieldClfrDstAddr}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrDstAddrGet, DiffServMultiFieldClfrDstAddrSet, DiffServMultiFieldClfrDstAddrTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMultiFieldClfrDstPrefixLength}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrDstPrefixLengthGet, DiffServMultiFieldClfrDstPrefixLengthSet, DiffServMultiFieldClfrDstPrefixLengthTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "0"},

{{12,DiffServMultiFieldClfrSrcAddr}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrSrcAddrGet, DiffServMultiFieldClfrSrcAddrSet, DiffServMultiFieldClfrSrcAddrTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMultiFieldClfrSrcPrefixLength}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrSrcPrefixLengthGet, DiffServMultiFieldClfrSrcPrefixLengthSet, DiffServMultiFieldClfrSrcPrefixLengthTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "0"},

{{12,DiffServMultiFieldClfrDscp}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrDscpGet, DiffServMultiFieldClfrDscpSet, DiffServMultiFieldClfrDscpTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "-1"},

{{12,DiffServMultiFieldClfrFlowId}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrFlowIdGet, DiffServMultiFieldClfrFlowIdSet, DiffServMultiFieldClfrFlowIdTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMultiFieldClfrProtocol}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrProtocolGet, DiffServMultiFieldClfrProtocolSet, DiffServMultiFieldClfrProtocolTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "255"},

{{12,DiffServMultiFieldClfrDstL4PortMin}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrDstL4PortMinGet, DiffServMultiFieldClfrDstL4PortMinSet, DiffServMultiFieldClfrDstL4PortMinTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "0"},

{{12,DiffServMultiFieldClfrDstL4PortMax}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrDstL4PortMaxGet, DiffServMultiFieldClfrDstL4PortMaxSet, DiffServMultiFieldClfrDstL4PortMaxTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "65535"},

{{12,DiffServMultiFieldClfrSrcL4PortMin}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrSrcL4PortMinGet, DiffServMultiFieldClfrSrcL4PortMinSet, DiffServMultiFieldClfrSrcL4PortMinTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "0"},

{{12,DiffServMultiFieldClfrSrcL4PortMax}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrSrcL4PortMaxGet, DiffServMultiFieldClfrSrcL4PortMaxSet, DiffServMultiFieldClfrSrcL4PortMaxTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "65535"},

{{12,DiffServMultiFieldClfrStorage}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrStorageGet, DiffServMultiFieldClfrStorageSet, DiffServMultiFieldClfrStorageTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 0, "3"},

{{12,DiffServMultiFieldClfrStatus}, GetNextIndexDiffServMultiFieldClfrTable, DiffServMultiFieldClfrStatusGet, DiffServMultiFieldClfrStatusSet, DiffServMultiFieldClfrStatusTest, DiffServMultiFieldClfrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMultiFieldClfrTableINDEX, 1, 0, 1, NULL},

{{10,DiffServMeterNextFree}, NULL, DiffServMeterNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServMeterId}, GetNextIndexDiffServMeterTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServMeterTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMeterSucceedNext}, GetNextIndexDiffServMeterTable, DiffServMeterSucceedNextGet, DiffServMeterSucceedNextSet, DiffServMeterSucceedNextTest, DiffServMeterTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServMeterTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMeterFailNext}, GetNextIndexDiffServMeterTable, DiffServMeterFailNextGet, DiffServMeterFailNextSet, DiffServMeterFailNextTest, DiffServMeterTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServMeterTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMeterSpecific}, GetNextIndexDiffServMeterTable, DiffServMeterSpecificGet, DiffServMeterSpecificSet, DiffServMeterSpecificTest, DiffServMeterTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServMeterTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMeterStorage}, GetNextIndexDiffServMeterTable, DiffServMeterStorageGet, DiffServMeterStorageSet, DiffServMeterStorageTest, DiffServMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMeterTableINDEX, 1, 0, 0, "3"},

{{12,DiffServMeterStatus}, GetNextIndexDiffServMeterTable, DiffServMeterStatusGet, DiffServMeterStatusSet, DiffServMeterStatusTest, DiffServMeterTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMeterTableINDEX, 1, 0, 1, NULL},

{{10,DiffServTBParamNextFree}, NULL, DiffServTBParamNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServTBParamId}, GetNextIndexDiffServTBParamTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServTBParamTableINDEX, 1, 0, 0, NULL},

{{12,DiffServTBParamType}, GetNextIndexDiffServTBParamTable, DiffServTBParamTypeGet, DiffServTBParamTypeSet, DiffServTBParamTypeTest, DiffServTBParamTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServTBParamTableINDEX, 1, 0, 0, NULL},

{{12,DiffServTBParamRate}, GetNextIndexDiffServTBParamTable, DiffServTBParamRateGet, DiffServTBParamRateSet, DiffServTBParamRateTest, DiffServTBParamTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServTBParamTableINDEX, 1, 0, 0, NULL},

{{12,DiffServTBParamBurstSize}, GetNextIndexDiffServTBParamTable, DiffServTBParamBurstSizeGet, DiffServTBParamBurstSizeSet, DiffServTBParamBurstSizeTest, DiffServTBParamTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServTBParamTableINDEX, 1, 0, 0, NULL},

{{12,DiffServTBParamInterval}, GetNextIndexDiffServTBParamTable, DiffServTBParamIntervalGet, DiffServTBParamIntervalSet, DiffServTBParamIntervalTest, DiffServTBParamTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServTBParamTableINDEX, 1, 0, 0, NULL},

{{12,DiffServTBParamStorage}, GetNextIndexDiffServTBParamTable, DiffServTBParamStorageGet, DiffServTBParamStorageSet, DiffServTBParamStorageTest, DiffServTBParamTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServTBParamTableINDEX, 1, 0, 0, "3"},

{{12,DiffServTBParamStatus}, GetNextIndexDiffServTBParamTable, DiffServTBParamStatusGet, DiffServTBParamStatusSet, DiffServTBParamStatusTest, DiffServTBParamTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServTBParamTableINDEX, 1, 0, 1, NULL},

{{10,DiffServActionNextFree}, NULL, DiffServActionNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServActionId}, GetNextIndexDiffServActionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServActionTableINDEX, 1, 0, 0, NULL},

{{12,DiffServActionInterface}, GetNextIndexDiffServActionTable, DiffServActionInterfaceGet, DiffServActionInterfaceSet, DiffServActionInterfaceTest, DiffServActionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServActionTableINDEX, 1, 0, 0, NULL},

{{12,DiffServActionNext}, GetNextIndexDiffServActionTable, DiffServActionNextGet, DiffServActionNextSet, DiffServActionNextTest, DiffServActionTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServActionTableINDEX, 1, 0, 0, NULL},

{{12,DiffServActionSpecific}, GetNextIndexDiffServActionTable, DiffServActionSpecificGet, DiffServActionSpecificSet, DiffServActionSpecificTest, DiffServActionTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServActionTableINDEX, 1, 0, 0, NULL},

{{12,DiffServActionStorage}, GetNextIndexDiffServActionTable, DiffServActionStorageGet, DiffServActionStorageSet, DiffServActionStorageTest, DiffServActionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServActionTableINDEX, 1, 0, 0, "3"},

{{12,DiffServActionStatus}, GetNextIndexDiffServActionTable, DiffServActionStatusGet, DiffServActionStatusSet, DiffServActionStatusTest, DiffServActionTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServActionTableINDEX, 1, 0, 1, NULL},

{{12,DiffServDscpMarkActDscp}, GetNextIndexDiffServDscpMarkActTable, DiffServDscpMarkActDscpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, DiffServDscpMarkActTableINDEX, 1, 0, 0, NULL},

{{10,DiffServCountActNextFree}, NULL, DiffServCountActNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServCountActId}, GetNextIndexDiffServCountActTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServCountActTableINDEX, 1, 0, 0, NULL},

{{12,DiffServCountActOctets}, GetNextIndexDiffServCountActTable, DiffServCountActOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DiffServCountActTableINDEX, 1, 0, 0, NULL},

{{12,DiffServCountActPkts}, GetNextIndexDiffServCountActTable, DiffServCountActPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DiffServCountActTableINDEX, 1, 0, 0, NULL},

{{12,DiffServCountActStorage}, GetNextIndexDiffServCountActTable, DiffServCountActStorageGet, DiffServCountActStorageSet, DiffServCountActStorageTest, DiffServCountActTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServCountActTableINDEX, 1, 0, 0, "3"},

{{12,DiffServCountActStatus}, GetNextIndexDiffServCountActTable, DiffServCountActStatusGet, DiffServCountActStatusSet, DiffServCountActStatusTest, DiffServCountActTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServCountActTableINDEX, 1, 0, 1, NULL},

{{10,DiffServAlgDropNextFree}, NULL, DiffServAlgDropNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServAlgDropId}, GetNextIndexDiffServAlgDropTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgDropType}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropTypeGet, DiffServAlgDropTypeSet, DiffServAlgDropTypeTest, DiffServAlgDropTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgDropNext}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropNextGet, DiffServAlgDropNextSet, DiffServAlgDropNextTest, DiffServAlgDropTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgDropQMeasure}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropQMeasureGet, DiffServAlgDropQMeasureSet, DiffServAlgDropQMeasureTest, DiffServAlgDropTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgDropQThreshold}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropQThresholdGet, DiffServAlgDropQThresholdSet, DiffServAlgDropQThresholdTest, DiffServAlgDropTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgDropSpecific}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropSpecificGet, DiffServAlgDropSpecificSet, DiffServAlgDropSpecificTest, DiffServAlgDropTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgDropOctets}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgDropPkts}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgRandomDropOctets}, GetNextIndexDiffServAlgDropTable, DiffServAlgRandomDropOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgRandomDropPkts}, GetNextIndexDiffServAlgDropTable, DiffServAlgRandomDropPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, DiffServAlgDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServAlgDropStorage}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropStorageGet, DiffServAlgDropStorageSet, DiffServAlgDropStorageTest, DiffServAlgDropTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServAlgDropTableINDEX, 1, 0, 0, "3"},

{{12,DiffServAlgDropStatus}, GetNextIndexDiffServAlgDropTable, DiffServAlgDropStatusGet, DiffServAlgDropStatusSet, DiffServAlgDropStatusTest, DiffServAlgDropTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServAlgDropTableINDEX, 1, 0, 1, NULL},

{{10,DiffServRandomDropNextFree}, NULL, DiffServRandomDropNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServRandomDropId}, GetNextIndexDiffServRandomDropTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServRandomDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServRandomDropMinThreshBytes}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropMinThreshBytesGet, DiffServRandomDropMinThreshBytesSet, DiffServRandomDropMinThreshBytesTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServRandomDropMinThreshPkts}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropMinThreshPktsGet, DiffServRandomDropMinThreshPktsSet, DiffServRandomDropMinThreshPktsTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServRandomDropMaxThreshBytes}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropMaxThreshBytesGet, DiffServRandomDropMaxThreshBytesSet, DiffServRandomDropMaxThreshBytesTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServRandomDropMaxThreshPkts}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropMaxThreshPktsGet, DiffServRandomDropMaxThreshPktsSet, DiffServRandomDropMaxThreshPktsTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServRandomDropProbMax}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropProbMaxGet, DiffServRandomDropProbMaxSet, DiffServRandomDropProbMaxTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServRandomDropWeight}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropWeightGet, DiffServRandomDropWeightSet, DiffServRandomDropWeightTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServRandomDropSamplingRate}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropSamplingRateGet, DiffServRandomDropSamplingRateSet, DiffServRandomDropSamplingRateTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 0, NULL},

{{12,DiffServRandomDropStorage}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropStorageGet, DiffServRandomDropStorageSet, DiffServRandomDropStorageTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 0, "3"},

{{12,DiffServRandomDropStatus}, GetNextIndexDiffServRandomDropTable, DiffServRandomDropStatusGet, DiffServRandomDropStatusSet, DiffServRandomDropStatusTest, DiffServRandomDropTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServRandomDropTableINDEX, 1, 0, 1, NULL},

{{10,DiffServQNextFree}, NULL, DiffServQNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServQId}, GetNextIndexDiffServQTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServQTableINDEX, 1, 0, 0, NULL},

{{12,DiffServQNext}, GetNextIndexDiffServQTable, DiffServQNextGet, DiffServQNextSet, DiffServQNextTest, DiffServQTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServQTableINDEX, 1, 0, 0, NULL},

{{12,DiffServQMinRate}, GetNextIndexDiffServQTable, DiffServQMinRateGet, DiffServQMinRateSet, DiffServQMinRateTest, DiffServQTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServQTableINDEX, 1, 0, 0, NULL},

{{12,DiffServQMaxRate}, GetNextIndexDiffServQTable, DiffServQMaxRateGet, DiffServQMaxRateSet, DiffServQMaxRateTest, DiffServQTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServQTableINDEX, 1, 0, 0, NULL},

{{12,DiffServQStorage}, GetNextIndexDiffServQTable, DiffServQStorageGet, DiffServQStorageSet, DiffServQStorageTest, DiffServQTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServQTableINDEX, 1, 0, 0, "3"},

{{12,DiffServQStatus}, GetNextIndexDiffServQTable, DiffServQStatusGet, DiffServQStatusSet, DiffServQStatusTest, DiffServQTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServQTableINDEX, 1, 0, 1, NULL},

{{10,DiffServSchedulerNextFree}, NULL, DiffServSchedulerNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServSchedulerId}, GetNextIndexDiffServSchedulerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServSchedulerTableINDEX, 1, 0, 0, NULL},

{{12,DiffServSchedulerNext}, GetNextIndexDiffServSchedulerTable, DiffServSchedulerNextGet, DiffServSchedulerNextSet, DiffServSchedulerNextTest, DiffServSchedulerTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServSchedulerTableINDEX, 1, 0, 0, NULL},

{{12,DiffServSchedulerMethod}, GetNextIndexDiffServSchedulerTable, DiffServSchedulerMethodGet, DiffServSchedulerMethodSet, DiffServSchedulerMethodTest, DiffServSchedulerTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServSchedulerTableINDEX, 1, 0, 0, NULL},

{{12,DiffServSchedulerMinRate}, GetNextIndexDiffServSchedulerTable, DiffServSchedulerMinRateGet, DiffServSchedulerMinRateSet, DiffServSchedulerMinRateTest, DiffServSchedulerTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServSchedulerTableINDEX, 1, 0, 0, NULL},

{{12,DiffServSchedulerMaxRate}, GetNextIndexDiffServSchedulerTable, DiffServSchedulerMaxRateGet, DiffServSchedulerMaxRateSet, DiffServSchedulerMaxRateTest, DiffServSchedulerTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, DiffServSchedulerTableINDEX, 1, 0, 0, NULL},

{{12,DiffServSchedulerStorage}, GetNextIndexDiffServSchedulerTable, DiffServSchedulerStorageGet, DiffServSchedulerStorageSet, DiffServSchedulerStorageTest, DiffServSchedulerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServSchedulerTableINDEX, 1, 0, 0, "3"},

{{12,DiffServSchedulerStatus}, GetNextIndexDiffServSchedulerTable, DiffServSchedulerStatusGet, DiffServSchedulerStatusSet, DiffServSchedulerStatusTest, DiffServSchedulerTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServSchedulerTableINDEX, 1, 0, 1, NULL},

{{10,DiffServMinRateNextFree}, NULL, DiffServMinRateNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServMinRateId}, GetNextIndexDiffServMinRateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServMinRateTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMinRatePriority}, GetNextIndexDiffServMinRateTable, DiffServMinRatePriorityGet, DiffServMinRatePrioritySet, DiffServMinRatePriorityTest, DiffServMinRateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMinRateTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMinRateAbsolute}, GetNextIndexDiffServMinRateTable, DiffServMinRateAbsoluteGet, DiffServMinRateAbsoluteSet, DiffServMinRateAbsoluteTest, DiffServMinRateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMinRateTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMinRateRelative}, GetNextIndexDiffServMinRateTable, DiffServMinRateRelativeGet, DiffServMinRateRelativeSet, DiffServMinRateRelativeTest, DiffServMinRateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMinRateTableINDEX, 1, 0, 0, NULL},

{{12,DiffServMinRateStorage}, GetNextIndexDiffServMinRateTable, DiffServMinRateStorageGet, DiffServMinRateStorageSet, DiffServMinRateStorageTest, DiffServMinRateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMinRateTableINDEX, 1, 0, 0, "3"},

{{12,DiffServMinRateStatus}, GetNextIndexDiffServMinRateTable, DiffServMinRateStatusGet, DiffServMinRateStatusSet, DiffServMinRateStatusTest, DiffServMinRateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMinRateTableINDEX, 1, 0, 1, NULL},

{{10,DiffServMaxRateNextFree}, NULL, DiffServMaxRateNextFreeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,DiffServMaxRateId}, GetNextIndexDiffServMaxRateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServMaxRateTableINDEX, 2, 0, 0, NULL},

{{12,DiffServMaxRateLevel}, GetNextIndexDiffServMaxRateTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, DiffServMaxRateTableINDEX, 2, 0, 0, NULL},

{{12,DiffServMaxRateAbsolute}, GetNextIndexDiffServMaxRateTable, DiffServMaxRateAbsoluteGet, DiffServMaxRateAbsoluteSet, DiffServMaxRateAbsoluteTest, DiffServMaxRateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMaxRateTableINDEX, 2, 0, 0, NULL},

{{12,DiffServMaxRateRelative}, GetNextIndexDiffServMaxRateTable, DiffServMaxRateRelativeGet, DiffServMaxRateRelativeSet, DiffServMaxRateRelativeTest, DiffServMaxRateTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, DiffServMaxRateTableINDEX, 2, 0, 0, NULL},

{{12,DiffServMaxRateThreshold}, GetNextIndexDiffServMaxRateTable, DiffServMaxRateThresholdGet, DiffServMaxRateThresholdSet, DiffServMaxRateThresholdTest, DiffServMaxRateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMaxRateTableINDEX, 2, 0, 0, NULL},

{{12,DiffServMaxRateStorage}, GetNextIndexDiffServMaxRateTable, DiffServMaxRateStorageGet, DiffServMaxRateStorageSet, DiffServMaxRateStorageTest, DiffServMaxRateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMaxRateTableINDEX, 2, 0, 0, "3"},

{{12,DiffServMaxRateStatus}, GetNextIndexDiffServMaxRateTable, DiffServMaxRateStatusGet, DiffServMaxRateStatusSet, DiffServMaxRateStatusTest, DiffServMaxRateTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, DiffServMaxRateTableINDEX, 2, 0, 1, NULL},
};
tMibData stdqosEntry = { 114, stdqosMibEntry };
#endif /* _STDQOSDB_H */

