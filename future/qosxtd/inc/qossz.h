/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: qossz.h,v 1.2 2016/03/31 10:51:10 siva Exp $
 *
 *******************************************************************/

enum {
    MAX_QOS_STD_CLFR_PORT_ENTRIES_SIZING_ID,
    MAX_QOS_VLAN_MAP_ENTRIES_SIZING_ID,
    QOSX_MAX_SIZING_ID
};


#ifdef  _QOSXSZ_C
tMemPoolId QOSXMemPoolIds[ QOSX_MAX_SIZING_ID];
INT4  QosxSizingMemCreateMemPools(VOID);
VOID  QosxSizingMemDeleteMemPools(VOID);
INT4  QosxSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _QOSXSZ_C  */
extern tMemPoolId QOSXMemPoolIds[ ];
extern INT4  QosxSizingMemCreateMemPools(VOID);
extern VOID  QosxSizingMemDeleteMemPools(VOID);
#endif /*  _QOSXSZ_C  */


#ifdef  _QOSXSZ_C
tFsModSizingParams FsQOSXSizingParams [] = {
{ "tQosStdClfrPortNode", "MAX_QOS_STD_CLFR_PORT_ENTRIES", sizeof(tQosStdClfrPortNode),MAX_QOS_STD_CLFR_PORT_ENTRIES, MAX_QOS_STD_CLFR_PORT_ENTRIES,0 },
{ "tQoSInVlanMapNode", "MAX_QOS_VLAN_MAP_ENTRIES", sizeof(tQoSInVlanMapNode),MAX_QOS_STD_CLFR_PORT_ENTRIES, MAX_QOS_VLAN_MAP_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _QOSXSZ_C  */
extern tFsModSizingParams FsQOSXSizingParams [];
#endif /*  _QOSXSZ_C  */


